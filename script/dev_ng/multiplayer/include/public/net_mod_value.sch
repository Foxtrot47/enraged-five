USING "rage_builtins.sch" 
USING "globals.sch"
USING "shop_public.sch"
USING "shop_private.sch"
USING "carmod_shop_private.sch"

FUNC BOOL USE_CUSTOM_CAM_FOR_BIKER_MOD(VEHICLE_INDEX mVeh)
	IF IS_VEHICLE_DRIVEABLE(mVeh)
		IF (GET_ENTITY_MODEL(mVeh) = AVARUS)
		OR (GET_ENTITY_MODEL(mVeh) = CHIMERA)
		OR (GET_ENTITY_MODEL(mVeh) = DAEMON2)
		OR (GET_ENTITY_MODEL(mVeh) = DEFILER)
		OR (GET_ENTITY_MODEL(mVeh) = ESSKEY)
		OR (GET_ENTITY_MODEL(mVeh) = NIGHTBLADE)
		OR (GET_ENTITY_MODEL(mVeh) = RATBIKE)
		OR (GET_ENTITY_MODEL(mVeh) = ZOMBIEA)
		OR (GET_ENTITY_MODEL(mVeh) = ZOMBIEB)
		OR (GET_ENTITY_MODEL(mVeh) = SANCTUS)
		OR (GET_ENTITY_MODEL(mVeh) = MANCHEZ)
		OR (GET_ENTITY_MODEL(mVeh) = VORTEX)
		OR (GET_ENTITY_MODEL(mVeh) = WOLFSBANE)
		OR (GET_ENTITY_MODEL(mVeh) = FAGGIO3)
		OR (GET_ENTITY_MODEL(mVeh) = HAKUCHOU2)
		OR (GET_ENTITY_MODEL(mVeh) = BLAZER4)
		OR (GET_ENTITY_MODEL(mVeh) = FCR2)
		OR (GET_ENTITY_MODEL(mVeh) = DIABLOUS2)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_ARMOR_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out =  g_sMptunables.fcarmod_unlock_NoArmour_expenditure_tunable_special 
		ENDIF
		
		IF (lvl = 1)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_20_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_40_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_60_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_80_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 5)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_100_expenditure_tunable_special
		ENDIF		
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out =  g_sMptunables.fcarmod_unlock_NoArmour_expenditure_tunable_suv 
		ENDIF
		
		IF (lvl = 1)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_20_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_40_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_60_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_80_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 5)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_100_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out =  g_sMptunables.fcarmod_unlock_NoArmour_expenditure_tunable_sport 
		ENDIF
		
		IF (lvl = 1)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_20_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_40_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_60_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_80_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 5)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_100_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out =  g_sMptunables.fcarmod_unlock_NoArmour_expenditure_tunable_bike 
		ENDIF
		
		IF (lvl = 1)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_20_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_40_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_60_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_80_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 5)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_100_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
		IF (lvl = 0)
			out =  g_sMptunables.fcarmod_unlock_NoArmour_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_20_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_40_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_60_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_80_expenditure_tunable
		ENDIF
		
		IF (lvl >= 5)
			out =  g_sMPTunables.fcarmod_unlock_body_armour_100_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out
ENDFUNC

FUNC FLOAT GET_BRAKES_EXPENDITURE_TUNEABLE(INT lvl)

	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_brakes_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_brakes_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_brakes_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_brakes_l4_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_brakes_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_brakes_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_brakes_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_brakes_l4_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_brakes_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_brakes_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_brakes_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_brakes_l4_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_brakes_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_brakes_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_brakes_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_brakes_l4_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_brakes_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_brakes_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_brakes_l3_expenditure_tunable
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_brakes_l4_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fbrakes_group_modifier
ENDFUNC


FUNC FLOAT GET_ENGINE_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		
		/*
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_engine_expenditure_tunable_special
		ENDIF
		*/
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_engine_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_engine_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_engine_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_engine_l5_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
	
		/*
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_engine_expenditure_tunable_sport
		ENDIF
		*/
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_engine_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_engine_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_engine_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_engine_l5_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
	
		/*
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_engine_expenditure_tunable_suv
		ENDIF
		*/
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_engine_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_engine_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_engine_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_engine_l5_expenditure_tunable_suv
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_BIKE)
		/*
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_engine_expenditure_tunable_bike
		ENDIF
		*/
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_engine_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_engine_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_engine_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_engine_l5_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		/*
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_stock_engine_expenditure_tunable
		ENDIF
		*/
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_engine_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_engine_l3_expenditure_tunable
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_engine_l4_expenditure_tunable
		ENDIF
	
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_engine_l5_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fengine_group_modifier
ENDFUNC

FUNC FLOAT GET_GEARBOX_EXPENDITURE_TUNEABLE(INT lvl)

	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockTransmission_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l4_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockTransmission_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l4_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockTransmission_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l4_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockTransmission_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l4_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockTransmission_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l3_expenditure_tunable
		ENDIF
		
		IF (lvl >= 3)
			out = g_sMPTunables.fcarmod_unlock_gear_box_l4_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fengine_group_modifier
ENDFUNC


FUNC FLOAT GET_CHASSIS_EXPENDITURE_TUNEABLE(INT lvl)
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockChassis_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_chassis_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_chassis_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_chassis_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_chassis_l5_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockChassis_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_chassis_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_chassis_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_chassis_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_chassis_l5_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockChassis_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_chassis_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_chassis_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_chassis_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_chassis_l5_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockChassis_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_chassis_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_chassis_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_chassis_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_chassis_l5_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
		
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockChassis_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_chassis_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_chassis_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_chassis_l4_expenditure_tunable
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_chassis_l5_expenditure_tunable
		ENDIF
		
	ENDIF
	
	RETURN out
ENDFUNC

FUNC FLOAT GET_FBUMBER_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockFrontBumper_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l5_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l6_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l7_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockFrontBumper_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l5_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l6_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l7_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockFrontBumper_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l5_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l6_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l7_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockFrontBumper_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l5_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l6_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l7_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockFrontBumper_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l4_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l5_expenditure_tunable
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l6_expenditure_tunable
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_f_bumper_l7_expenditure_tunable
		ENDIF	
	ELIF g_eModPriceVariation = MPV_GR_LIGHT
		// Gunrunning tunable required - 
	ENDIF
	
	RETURN out
ENDFUNC

FUNC FLOAT GET_RBUMBER_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockRearBumper_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l5_expenditure_tunable_special
		ENDIF	
	ELIF (g_eModPriceVariation = MPV_SPORT)	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockRearBumper_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l5_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockRearBumper_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l5_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockRearBumper_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l5_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockRearBumper_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l4_expenditure_tunable
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_r_bumper_l5_expenditure_tunable
		ENDIF
		
	ELIF g_eModPriceVariation = MPV_GR_LIGHT
		// // Gunrunning tunable required - BUMPER_R
	ENDIF
	
	RETURN out
ENDFUNC

FUNC FLOAT GET_BONNET_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockHood_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l5_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l6_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l7_expenditure_tunable_special
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SPORT)
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockHood_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l5_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l6_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l7_expenditure_tunable_sport
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockHood_expenditure_tunable_suv
		ENDIF		
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l5_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l6_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l7_expenditure_tunable_suv
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_BIKE)
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockHood_expenditure_tunable_bike
		ENDIF	
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l5_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l6_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l7_expenditure_tunable_bike
		ENDIF

	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockHood_expenditure_tunable
		ENDIF		
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l4_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l5_expenditure_tunable
		ENDIF
		
		IF (lvl = 5)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l6_expenditure_tunable
		ENDIF
		
		IF (lvl >= 6)
			out = g_sMPTunables.fcarmod_unlock_bonnet_l7_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fhood_group_modifier
ENDFUNC

FUNC FLOAT GET_EXHAUST_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockExhaust_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l5_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l6_expenditure_tunable_special
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockExhaust_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l5_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l6_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockExhaust_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l5_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l6_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockExhaust_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l5_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l6_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockExhaust_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l4_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l5_expenditure_tunable
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_exhaust_l6_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fmuffler_group_modifier
ENDFUNC

FUNC FLOAT GET_EXHAUST_EXPLOSIVES_TUNEABLE(INT lvl)

	FLOAT out = 1.0
	
	IF (lvl = 0)
		out = g_sMPTunables.fcarmod_unlock_ignition_bomb_expenditure_tunable
		
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
		OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_ignition_bomb_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_ignition_bomb_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_ignition_bomb_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_ignition_bomb_expenditure_tunable_bike
		ENDIF
	ELSE
		out = g_sMPTunables.fcarmod_unlock_timed_bomb_expenditure_tunable 
		
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
		OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_timed_bomb_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_timed_bomb_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_timed_bomb_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_timed_bomb_expenditure_tunable_bike
		ENDIF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fexplosives_group_modifier
ENDFUNC

FUNC FLOAT GET_GRILL_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockGrille_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_grill_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_grill_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_grill_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_grill_l5_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_grill_l6_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockGrille_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_grill_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_grill_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_grill_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_grill_l5_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_grill_l6_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockGrille_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_grill_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_grill_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_grill_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_grill_l5_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_grill_l6_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockGrille_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_grill_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_grill_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_grill_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_grill_l5_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_grill_l6_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockGrille_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_grill_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_grill_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_grill_l4_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_grill_l5_expenditure_tunable
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_grill_l6_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fgrille_group_modifier
ENDFUNC

FUNC FLOAT GET_HORN_EXPENDITURE_TUNEABLE(CARMOD_HORN_ENUM eHorn)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (eHorn = HORN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockHorn_expenditure_tunable_special
		ENDIF

		IF (eHorn = HORN_TRUCK)
			out = g_sMPTunables.fcarmod_unlock_horn_l2_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_COP)
			out = g_sMPTunables.fcarmod_unlock_horn_l3_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_CLOWN)
			out = g_sMPTunables.fcarmod_unlock_horn_l4_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_1)
			out = g_sMPTunables.fcarmod_unlock_horn_l5_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_2)
			out = g_sMPTunables.fcarmod_unlock_horn_l6_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_3)
			out = g_sMPTunables.fcarmod_unlock_horn_l7_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_4)
			out = g_sMPTunables.fcarmod_unlock_horn_l8_expenditure_tunable_special
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_5)
			out = g_sMPTunables.fcarmod_unlock_horn_l9_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
	
		IF (eHorn = HORN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockHorn_expenditure_tunable_sport
		ENDIF

		IF (eHorn = HORN_TRUCK)
			out = g_sMPTunables.fcarmod_unlock_horn_l2_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_COP)
			out = g_sMPTunables.fcarmod_unlock_horn_l3_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_CLOWN)
			out = g_sMPTunables.fcarmod_unlock_horn_l4_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_1)
			out = g_sMPTunables.fcarmod_unlock_horn_l5_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_2)
			out = g_sMPTunables.fcarmod_unlock_horn_l6_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_3)
			out = g_sMPTunables.fcarmod_unlock_horn_l7_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_4)
			out = g_sMPTunables.fcarmod_unlock_horn_l8_expenditure_tunable_sport
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_5)
			out = g_sMPTunables.fcarmod_unlock_horn_l9_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
	
		IF (eHorn = HORN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockHorn_expenditure_tunable_suv
		ENDIF

		IF (eHorn = HORN_TRUCK)
			out = g_sMPTunables.fcarmod_unlock_horn_l2_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_COP)
			out = g_sMPTunables.fcarmod_unlock_horn_l3_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_CLOWN)
			out = g_sMPTunables.fcarmod_unlock_horn_l4_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_1)
			out = g_sMPTunables.fcarmod_unlock_horn_l5_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_2)
			out = g_sMPTunables.fcarmod_unlock_horn_l6_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_3)
			out = g_sMPTunables.fcarmod_unlock_horn_l7_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_4)
			out = g_sMPTunables.fcarmod_unlock_horn_l8_expenditure_tunable_suv
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_5)
			out = g_sMPTunables.fcarmod_unlock_horn_l9_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (eHorn = HORN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockHorn_expenditure_tunable_bike
		ENDIF

		IF (eHorn = HORN_TRUCK)
			out = g_sMPTunables.fcarmod_unlock_horn_l2_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_COP)
			out = g_sMPTunables.fcarmod_unlock_horn_l3_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_CLOWN)
			out = g_sMPTunables.fcarmod_unlock_horn_l4_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_1)
			out = g_sMPTunables.fcarmod_unlock_horn_l5_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_2)
			out = g_sMPTunables.fcarmod_unlock_horn_l6_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_3)
			out = g_sMPTunables.fcarmod_unlock_horn_l7_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_4)
			out = g_sMPTunables.fcarmod_unlock_horn_l8_expenditure_tunable_bike
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_5)
			out = g_sMPTunables.fcarmod_unlock_horn_l9_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (eHorn = HORN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockHorn_expenditure_tunable
		ENDIF

		IF (eHorn = HORN_TRUCK)
			out = g_sMPTunables.fcarmod_unlock_horn_l2_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_COP)
			out = g_sMPTunables.fcarmod_unlock_horn_l3_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_CLOWN)
			out = g_sMPTunables.fcarmod_unlock_horn_l4_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_1)
			out = g_sMPTunables.fcarmod_unlock_horn_l5_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_2)
			out = g_sMPTunables.fcarmod_unlock_horn_l6_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_3)
			out = g_sMPTunables.fcarmod_unlock_horn_l7_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_4)
			out = g_sMPTunables.fcarmod_unlock_horn_l8_expenditure_tunable
		ENDIF
		
		IF (eHorn = HORN_MUSICAL_5)
			out = g_sMPTunables.fcarmod_unlock_horn_l9_expenditure_tunable
		ENDIF
	
	ENDIF
		
	RETURN out * g_sMPTunableGroups.fhorn_group_modifier
ENDFUNC

FUNC FLOAT GET_PLATES_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite1_expenditure_tunable_special
		ENDIF	
		
		IF (lvl = 1)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlue_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlack_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite1_expenditure_tunable_sport
		ENDIF	
		
		IF (lvl = 1)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlue_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlack_expenditure_tunable_sport
		ENDIF
	ELIF  (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite1_expenditure_tunable_suv
		ENDIF	
		
		IF (lvl = 1)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlue_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlack_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite1_expenditure_tunable_bike
		ENDIF	
		
		IF (lvl = 1)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlue_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlack_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3

		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite1_expenditure_tunable
		ENDIF	
		
		IF (lvl = 1)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMptunables.fcarmod_unlock_PlatesBlueonWhite3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlue_expenditure_tunable
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMptunables.fcarmod_unlock_PlatesYellowonBlack_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fplates_group_modifier
ENDFUNC

FUNC FLOAT GET_ROOF_EXPENDITURE_TUNEABLE(INT lvl)
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stockroof_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_roof_l1_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_roof_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_roof_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_roof_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMptunables.fcarmod_unlock_roof_l5_expenditure_tunable_special
		ENDIF	
		
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stockroof_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_roof_l1_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_roof_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_roof_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_roof_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMptunables.fcarmod_unlock_roof_l5_expenditure_tunable_sport
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stockroof_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_roof_l1_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_roof_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_roof_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_roof_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_roof_l5_expenditure_tunable_suv
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stockroof_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_roof_l1_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_roof_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_roof_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_roof_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_roof_l5_expenditure_tunable_bike
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3

		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stockroof_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_roof_l1_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_roof_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_roof_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_roof_l4_expenditure_tunable
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_roof_l5_expenditure_tunable
		ENDIF
	
	ELIF g_eModPriceVariation = MPV_GR_HEAVY
		// // Gunrunning tunable required - ROOF
	ELIF g_eModPriceVariation = MPV_GR_LIGHT
		// // Gunrunning tunable required - ROOF
	ELIF g_eModPriceVariation = MPV_GR_TRAILERLARGE
		// // Gunrunning tunable required - ROOF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.froof_group_modifier
ENDFUNC

FUNC FLOAT GET_SKIRTS_EXPENDITURE_TUNEABLE(INT lvl)
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockSkirt_expenditure_tunable_special 
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_skirts_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_skirts_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_skirts_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_skirts_l5_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockSkirt_expenditure_tunable_sport 
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_skirts_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_skirts_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_skirts_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_skirts_l5_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockSkirt_expenditure_tunable_suv 
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_skirts_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_skirts_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_skirts_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_skirts_l5_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockSkirt_expenditure_tunable_bike 
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_skirts_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_skirts_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_skirts_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_skirts_l5_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3

		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockSkirt_expenditure_tunable 
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_skirts_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_skirts_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_skirts_l4_expenditure_tunable
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_skirts_l5_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fskirts_group_modifier
ENDFUNC

FUNC FLOAT GET_SPOILER_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockSpoiler_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l1_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l5_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockSpoiler_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l1_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l5_expenditure_tunable_sport
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SUV)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockSpoiler_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l1_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l5_expenditure_tunable_suv
		ENDIF
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockSpoiler_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l1_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l5_expenditure_tunable_bike
		ENDIF
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3

		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_StockSpoiler_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l1_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 4)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l4_expenditure_tunable
		ENDIF
		
		IF (lvl >= 5)
			out = g_sMPTunables.fcarmod_unlock_spoiler_l5_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fspoiler_group_modifier
ENDFUNC

FUNC FLOAT GET_SUSPENSION_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0 
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stocksuspension_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_suspension_l1_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_suspension_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_suspension_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_suspension_l4_expenditure_tunable_special
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SPORT)	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stocksuspension_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_suspension_l1_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_suspension_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_suspension_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_suspension_l4_expenditure_tunable_sport
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SUV)	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stocksuspension_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_suspension_l1_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_suspension_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_suspension_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_suspension_l4_expenditure_tunable_suv
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stocksuspension_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_suspension_l1_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_suspension_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_suspension_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_suspension_l4_expenditure_tunable_bike
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3

		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stocksuspension_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_suspension_l1_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_suspension_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_suspension_l3_expenditure_tunable
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_suspension_l4_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fsuspension_group_modifier
ENDFUNC

FUNC FLOAT GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = WHEEL_ACCS_DESIGN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockTires_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_DESIGN_CUSTOM_1)
			out = g_sMPTunables.fcarmod_unlock_custom_tyres_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_OPTION_BPT)
		OR (lvl = WHEEL_ACCS_OPTION_LGT)
		OR (lvl = WHEEL_ACCS_OPTION_STT)
			out = g_sMPTunables.fcarmod_unlock_bullet_proof_tyres_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_WHITE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_white_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLACK)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_black_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLUE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_blue_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_YELLOW)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_yellow_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_PURPLE)
			// business purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_purple_expenditure_tunable_special 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_ORANGE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_orange_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_GREEN)
			// green purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_green_expenditure_tunable_special 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_RED)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_red_expenditure_tunable_special
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_CREW)
			out = g_sMPTunables.fcarmod_unlock_CrewSmoke_expenditure_tunable_special
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SPORT)
	
		IF (lvl = WHEEL_ACCS_DESIGN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockTires_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_DESIGN_CUSTOM_1)
			out = g_sMPTunables.fcarmod_unlock_custom_tyres_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_OPTION_BPT)
		OR (lvl = WHEEL_ACCS_OPTION_LGT)
		OR (lvl = WHEEL_ACCS_OPTION_STT)
			out = g_sMPTunables.fcarmod_unlock_bullet_proof_tyres_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_WHITE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_white_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLACK)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_black_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLUE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_blue_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_YELLOW)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_yellow_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_PURPLE)
			// business purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_purple_expenditure_tunable_sport 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_ORANGE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_orange_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_GREEN)
			// green purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_green_expenditure_tunable_sport 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_RED)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_red_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_CREW)
			out = g_sMPTunables.fcarmod_unlock_CrewSmoke_expenditure_tunable_sport
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SUV)
		
		IF (lvl = WHEEL_ACCS_DESIGN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockTires_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_DESIGN_CUSTOM_1)
			out = g_sMPTunables.fcarmod_unlock_custom_tyres_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_OPTION_BPT)
		OR (lvl = WHEEL_ACCS_OPTION_LGT)
		OR (lvl = WHEEL_ACCS_OPTION_STT)
			out = g_sMPTunables.fcarmod_unlock_bullet_proof_tyres_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_WHITE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_white_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLACK)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_black_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLUE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_blue_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_YELLOW)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_yellow_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_PURPLE)
			// business purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_purple_expenditure_tunable_suv 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_ORANGE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_orange_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_GREEN)
			// green purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_green_expenditure_tunable_suv 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_RED)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_red_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_CREW)
			out = g_sMPTunables.fcarmod_unlock_CrewSmoke_expenditure_tunable_suv
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_BIKE)
		IF (lvl = WHEEL_ACCS_DESIGN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockTires_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_DESIGN_CUSTOM_1)
			out = g_sMPTunables.fcarmod_unlock_custom_tyres_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_OPTION_BPT)
		OR (lvl = WHEEL_ACCS_OPTION_LGT)
		OR (lvl = WHEEL_ACCS_OPTION_STT)
			out = g_sMPTunables.fcarmod_unlock_bullet_proof_tyres_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_WHITE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_white_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLACK)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_black_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLUE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_blue_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_YELLOW)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_yellow_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_PURPLE)
			// business purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_purple_expenditure_tunable_bike 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_ORANGE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_orange_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_GREEN)
			// green purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_green_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_RED)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_red_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_CREW)
			out = g_sMPTunables.fcarmod_unlock_CrewSmoke_expenditure_tunable_bike
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		
		IF (lvl = WHEEL_ACCS_DESIGN_STOCK)
			out = g_sMPTunables.fcarmod_unlock_StockTires_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_DESIGN_CUSTOM_1)
			out = g_sMPTunables.fcarmod_unlock_custom_tyres_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_OPTION_BPT)
		OR (lvl = WHEEL_ACCS_OPTION_LGT)
		OR (lvl = WHEEL_ACCS_OPTION_STT)
			out = g_sMPTunables.fcarmod_unlock_bullet_proof_tyres_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_WHITE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_white_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLACK)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_black_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_BLUE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_blue_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_YELLOW)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_yellow_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_PURPLE)
			// business purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_purple_expenditure_tunable 
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_ORANGE)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_orange_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_GREEN)
			// green purple
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_green_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_RED)
			out = g_sMPTunables.fcarmod_unlock_tyre_smoke_red_expenditure_tunable
		ENDIF
		
		IF (lvl = WHEEL_ACCS_SMOKE_CREW)
			out = g_sMPTunables.fcarmod_unlock_CrewSmoke_expenditure_tunable
		ENDIF
	
	ENDIF
		
	RETURN out * g_sMPTunableGroups.ftyres_group_modifier
ENDFUNC

FUNC FLOAT GET_WINDOWS_EXPENDITURE_TUNEABLE(INT lvl)
	
	FLOAT out = 1.0

	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_WindowsNone_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_light_smoke_windows_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_dark_smoke_windows_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_limo_windows_expenditure_tunable_special
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SPORT)
	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_WindowsNone_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_light_smoke_windows_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_dark_smoke_windows_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_limo_windows_expenditure_tunable_sport
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_SUV)

		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_WindowsNone_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_light_smoke_windows_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_dark_smoke_windows_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_limo_windows_expenditure_tunable_suv
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_BIKE)

		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_WindowsNone_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_light_smoke_windows_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_dark_smoke_windows_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_limo_windows_expenditure_tunable_bike
		ENDIF
	
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3
	
		IF (lvl = 0)
			out = g_sMptunables.fcarmod_unlock_WindowsNone_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_light_smoke_windows_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_dark_smoke_windows_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_limo_windows_expenditure_tunable
		ENDIF
	
	ENDIF
	
	RETURN out * g_sMPTunableGroups.fwindows_group_modifier
ENDFUNC

FUNC FLOAT GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(INT lvl)

	FLOAT out = 1.0
	
	IF (lvl = 0)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
		OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_StockLights_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_StockLights_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_StockLights_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_StockLights_expenditure_tunable_bike
		ELIF (g_eModPriceVariation = MPV_STANDARD)
		OR g_eModPriceVariation = MPV_IE_BIKE
		OR g_eModPriceVariation = MPV_BIKE
		OR g_eModPriceVariation = MPV_SULTANRS
		OR g_eModPriceVariation = MPV_SLAMVAN3
			out = g_sMPTunables.fcarmod_unlock_StockLights_expenditure_tunable
		ENDIF
	ELSE

		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
		OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_xenon_lights_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_xenon_lights_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_xenon_lights_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_xenon_lights_expenditure_tunable_bike
		ELIF (g_eModPriceVariation = MPV_STANDARD)
		OR g_eModPriceVariation = MPV_IE_BIKE
		OR g_eModPriceVariation = MPV_BIKE
		OR g_eModPriceVariation = MPV_SULTANRS
		OR g_eModPriceVariation = MPV_SLAMVAN3
			out = g_sMPTunables.fcarmod_unlock_xenon_lights_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.flights_group_modifier
ENDFUNC

FUNC FLOAT GET_TURBO_EXPENDITURE_TUNEABLE(INT lvl)
	FLOAT out = 1.0 
	
	IF (lvl = 0)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
		OR g_eModPriceVariation = MPV_RC_BANDITO
			out =  g_sMPTunables.fcarmod_unlock_NoTurbo_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_NoTurbo_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_NoTurbo_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_NoTurbo_expenditure_tunable_bike
		ELIF (g_eModPriceVariation = MPV_STANDARD)
		OR g_eModPriceVariation = MPV_IE_BIKE
		OR g_eModPriceVariation = MPV_BIKE
		OR g_eModPriceVariation = MPV_SULTANRS
		OR g_eModPriceVariation = MPV_SLAMVAN3	
			out = g_sMPTunables.fcarmod_unlock_NoTurbo_expenditure_tunable
		ENDIF
	ELSE
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
		OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_turbo_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_turbo_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_turbo_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_turbo_expenditure_tunable_bike
		ELIF (g_eModPriceVariation = MPV_STANDARD)
		OR g_eModPriceVariation = MPV_IE_BIKE
		OR g_eModPriceVariation = MPV_BIKE
		OR g_eModPriceVariation = MPV_SULTANRS
		OR g_eModPriceVariation = MPV_SLAMVAN3
			out = g_sMPTunables.fcarmod_unlock_turbo_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out 
ENDFUNC

FUNC FLOAT GET_L_WING_EXPENDITURE_TUNEABLE(INT lvl)
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockLeftFender_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l2_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l3_expenditure_tunable_special
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l4_expenditure_tunable_special
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l5_expenditure_tunable_special
		ENDIF
	ELIF (g_eModPriceVariation = MPV_SPORT)

		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockLeftFender_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l2_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l3_expenditure_tunable_sport
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l4_expenditure_tunable_sport
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l5_expenditure_tunable_sport
		ENDIF
			
	ELIF (g_eModPriceVariation = MPV_SUV)
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockLeftFender_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l2_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l3_expenditure_tunable_suv
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l4_expenditure_tunable_suv
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l5_expenditure_tunable_suv
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_BIKE)
	
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockLeftFender_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l2_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l3_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l4_expenditure_tunable_bike
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l5_expenditure_tunable_bike
		ENDIF
		
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3	
		
		
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_StockLeftFender_expenditure_tunable
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l2_expenditure_tunable
		ENDIF
		
		IF (lvl = 2)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l3_expenditure_tunable
		ENDIF
		
		IF (lvl = 3)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l4_expenditure_tunable
		ENDIF
		
		IF (lvl >= 4)
			out = g_sMPTunables.fcarmod_unlock_l_wing_l5_expenditure_tunable
		ENDIF
		
	ELIF g_eModPriceVariation = MPV_GR_BIKE
		// - Using roof tunables, in line with other bikes which use MOD_ROOF for fuel tanks
		IF (lvl = 0)
			out = g_sMPTunables.fcarmod_unlock_stockroof_expenditure_tunable_bike
		ENDIF
		
		IF (lvl = 1)
			out = g_sMPTunables.fcarmod_unlock_roof_l1_expenditure_tunable_bike
		ENDIF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.ffenders_group_modifier
ENDFUNC

FUNC FLOAT GET_R_WING_EXPENDITURE_TUNEABLE(INT lvl)
	FLOAT out = 1.0
	
	IF (lvl = 0)
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_StockRightFender_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_StockRightFender_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_StockRightFender_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_StockRightFender_expenditure_tunable_bike
		ELIF (g_eModPriceVariation = MPV_STANDARD)
		OR g_eModPriceVariation = MPV_IE_BIKE
		OR g_eModPriceVariation = MPV_BIKE
		OR g_eModPriceVariation = MPV_SULTANRS
		OR g_eModPriceVariation = MPV_SLAMVAN3		
			out = g_sMPTunables.fcarmod_unlock_StockRightFender_expenditure_tunable
		ENDIF
	ELSE	
		IF (g_eModPriceVariation = MPV_SPECIAL)
		OR g_eModPriceVariation = MPV_IE_HIGH
		OR g_eModPriceVariation = MPV_IE_RETRO
		OR g_eModPriceVariation = MPV_BANSHEE2
		OR g_eModPriceVariation = MPV_BTYPE3
		OR g_eModPriceVariation = MPV_VIRGO2
		OR g_eModPriceVariation = MPV_GR_HEAVY
		OR g_eModPriceVariation = MPV_GR_LIGHT
		OR g_eModPriceVariation = MPV_GR_BIKE
		OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
		OR g_eModPriceVariation = MPV_SMUG_STANDARD
		OR g_eModPriceVariation = MPV_SMUG_HEAVY
		OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_STANDARD
		OR g_eModPriceVariation = MPV_BATTLE_HEAVY
		OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
			out = g_sMPTunables.fcarmod_unlock_r_wing_l2_expenditure_tunable_special
		ELIF (g_eModPriceVariation = MPV_SPORT)
			out = g_sMPTunables.fcarmod_unlock_r_wing_l2_expenditure_tunable_sport
		ELIF (g_eModPriceVariation = MPV_SUV)
			out = g_sMPTunables.fcarmod_unlock_r_wing_l2_expenditure_tunable_suv
		ELIF (g_eModPriceVariation = MPV_BIKE)
			out = g_sMPTunables.fcarmod_unlock_r_wing_l2_expenditure_tunable_bike
		ELIF (g_eModPriceVariation = MPV_STANDARD)
		OR g_eModPriceVariation = MPV_IE_BIKE
		OR g_eModPriceVariation = MPV_BIKE
		OR g_eModPriceVariation = MPV_SULTANRS
		OR g_eModPriceVariation = MPV_SLAMVAN3
			out = g_sMPTunables.fcarmod_unlock_r_wing_l2_expenditure_tunable
		ENDIF
	ENDIF
	
	RETURN out * g_sMPTunableGroups.ffenders_group_modifier
ENDFUNC

FUNC FLOAT GET_TRACKER_EXPENDITURE_TUNEABLE()
	FLOAT out = 1.0
	
	IF (g_eModPriceVariation = MPV_SPECIAL)
	OR g_eModPriceVariation = MPV_IE_HIGH
	OR g_eModPriceVariation = MPV_IE_RETRO
	OR g_eModPriceVariation = MPV_BANSHEE2
	OR g_eModPriceVariation = MPV_BTYPE3
	OR g_eModPriceVariation = MPV_VIRGO2
	OR g_eModPriceVariation = MPV_GR_HEAVY
	OR g_eModPriceVariation = MPV_GR_LIGHT
	OR g_eModPriceVariation = MPV_GR_BIKE
	OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
	OR g_eModPriceVariation = MPV_SMUG_STANDARD
	OR g_eModPriceVariation = MPV_SMUG_HEAVY
	OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_STANDARD
	OR g_eModPriceVariation = MPV_BATTLE_HEAVY
	OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
		out = g_sMPTunables.fcarmod_unlock_Tracker_expenditure_tunable_special
	ELIF (g_eModPriceVariation = MPV_SPORT)
		out = g_sMPTunables.fcarmod_unlock_Tracker_expenditure_tunable_sport
	ELIF (g_eModPriceVariation = MPV_SUV)
		out = g_sMPTunables.fcarmod_unlock_Tracker_expenditure_tunable_suv
	ELIF (g_eModPriceVariation = MPV_BIKE)
		out = g_sMPTunables.fcarmod_unlock_Tracker_expenditure_tunable_bike
	ELIF (g_eModPriceVariation = MPV_STANDARD)
	OR g_eModPriceVariation = MPV_IE_BIKE
	OR g_eModPriceVariation = MPV_BIKE
	OR g_eModPriceVariation = MPV_SULTANRS
	OR g_eModPriceVariation = MPV_SLAMVAN3		
		out = g_sMPTunables.fcarmod_unlock_Tracker_expenditure_tunable
	ENDIF
	
	RETURN out * g_sMPTunableGroups.finsurance_group_modifier
ENDFUNC

FUNC FLOAT GET_SUPERMOD_EXPENDITURE_TUNEABLE(INT iOption, BOOL bUseSupermodeMultiplier = TRUE)
	IF (g_eModPriceVariation = MPV_ARENA_WARS
	OR g_eModPriceVariation = MPV_RC_BANDITO)
	AND !bUseSupermodeMultiplier
		RETURN 1.0 
	ELSE	
		
		SWITCH iOption
			CASE 0 RETURN 1.0 BREAK
			CASE 1 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_1st_Mod_Multiplier BREAK
			CASE 2 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_2nd_Mod_Multiplier BREAK
			CASE 3 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_3rd_Mod_Multiplier BREAK
			CASE 4 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_4th_Mod_Multiplier BREAK
			CASE 5 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_5thMod_Multiplier BREAK
			CASE 6 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_6th_Mod_Multiplier BREAK
			CASE 7 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_7th_Mod_Multiplier BREAK
			CASE 8 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_8th_Mod_Multiplier BREAK
			CASE 9 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_9th_Mod_Multiplier BREAK
			CASE 10 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_10th_Mod_Multiplier BREAK
			CASE 11 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_11h_Mod_Multiplier BREAK
			CASE 12 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_12h_Mod_Multiplier BREAK
			CASE 13 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_13th_Mod_Multiplier  BREAK
			CASE 14 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_14th_Mod_Multiplier  BREAK
			CASE 15 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_15th_Mod_Multiplier  BREAK
			CASE 16 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_16th_Mod_Multiplier  BREAK
			CASE 17 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_17th_Mod_Multiplier  BREAK
			CASE 18 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_18th_Mod_Multiplier  BREAK
			CASE 19 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_19th_Mod_Multiplier  BREAK
			CASE 20 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_20TH_Mod_Multiplier  BREAK
			CASE 21 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_21ST_Mod_Multiplier  BREAK
			CASE 22 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_22ND_Mod_Multiplier  BREAK
			CASE 23 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_23RD_Mod_Multiplier  BREAK
			CASE 24 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_24TH_Mod_Multiplier  BREAK
			CASE 25 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_25TH_Mod_Multiplier  BREAK
			CASE 26 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_26TH_Mod_Multiplier  BREAK
			CASE 27 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_27TH_Mod_Multiplier  BREAK
			CASE 28 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_28TH_Mod_Multiplier  BREAK
			CASE 29 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_29TH_Mod_Multiplier  BREAK
			CASE 30 RETURN g_sMPTunables.fCar_Mods_Supermod_Slot_30TH_Mod_Multiplier  BREAK
		ENDSWITCH
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC INT GET_SUPERMOD_STOCK_PRICE_TUNABLE(CARMOD_MENU_ENUM eMenu, BOOL bBuyNow = FALSE)
	MODEL_NAMES eModel = GET_ENTITY_MODEL(g_ModVehicle)
	INT iBuyNowMultiplier = 1
	IF bBuyNow
		iBuyNowMultiplier = FLOOR(g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER)
	ENDIF
		
	SWITCH eModel
		CASE SULTANRS
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plateholder BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Vanity_Plate BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Interior_Trim BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Interior_Dash BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Interior_Dials BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Interior_Doors BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Seats BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Steering_Wheel BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Column_Shift_Lever BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plaque BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_ICE BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Trunk BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Engine_Block BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Cam_Cover BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Strut_Brace BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Chassis_Headlight_Trim BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Chassis_Foglights BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Chassis_Roof_Scoop BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Tank BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Left_Door BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Right_Door BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iCarMods_Supermod_Slot_SultanRS_Stock_Livery BREAK
			ENDSWITCH
		BREAK
		CASE BANSHEE2
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plateholder BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Vanity_Plate BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Trim BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Interior_Dash BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Interior_Dials BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Interior_Doors BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Seats BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Steering_Wheel BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Column_Shift_Lever BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plaque BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_ICE BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Trunk BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Engine_Block BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Air_Filter BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Strut_Brace BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Arch_Covers BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Aerials BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Trim BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Tank BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Left_Door BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Right_Door BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Banshee2_Stock_Livery BREAK
			ENDSWITCH
		BREAK
		CASE BTYPE3
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plateholder BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Vanity_Plate BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Trim BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dash BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dials BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Doors BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Seats BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Steering_Wheel BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Column_Shift_Lever BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plaque BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_ICE BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Trunk BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Engine_Bay BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Air_Filter BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Fittings BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Btype3_Stock_Arch_Covers BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Btype3_Stock_Side_Hood_Grill BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Btype3_Stock_Front_Detail BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Tank BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iCarMods_Supermod_Slot_Btype3_Stock_Spare_Wheel BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Right_Door BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery BREAK
			ENDSWITCH
		BREAK
		CASE VIRGO2
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plateholder BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Vanity_Plate BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Trim BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dash BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dials BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Doors BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Seats BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Steering_Wheel BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Column_Shift_Lever BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plaque BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_ICE BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Trunk BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Engine_Bay BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Air_Filter BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Fittings BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Arch_Covers BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Aerials BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Virgo2_Stock_Chassis_Wipers BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Tank BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Left_Door BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Right_Door BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery BREAK
			ENDSWITCH
		BREAK
		CASE SLAMVAN3
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plateholder BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Vanity_Plate BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Trim BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dash BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dials BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Doors BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Seats BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Steering_Wheel BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Column_Shift_Lever BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plaque BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_ICE BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Trunk BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Engine_Bay BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Air_Filter BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Fittings BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Arch_Covers BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Aerials BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Trim BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Slamvan3_Stock_Chassis_Roll_Cage BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Slamvan3_Stock_Left_Door BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Right_Door BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery BREAK
			ENDSWITCH
		BREAK
		CASE DIABLOUS2
		CASE FCR2
		CASE FCR
		CASE DIABLOUS
			SWITCH eMenu
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_SEATS BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_STEERING_WHEEL  BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_COLUMN_SHIFT_LEVER BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_PLAQUE  BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_ICE BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_TRUNK BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_ENGINE_BAY BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_AIR_FILTER BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_FITTINGS BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_CHASSIS_ARCH_COVERS  BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_CHASSIS_AERIALS BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_CHASSIS_TRIM BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_CHASSIS_TANK  BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_LEFT_DOOR BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_RIGHT_DOOR  BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_LIVERY BREAK
				CASE CMM_SUPERMOD_HYDRO			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_BIKE_STOCK_HYDRAULICS  BREAK
			ENDSWITCH	
		BREAK
		CASE ITALIGTB2
		CASE SPECTER2
		CASE NERO2
		CASE ITALIGTB
		CASE SPECTER
		CASE NERO
		CASE YOUGA3		
		CASE GAUNTLET5	
		CASE MANANA2	
		CASE PEYOTE3	
		CASE YOSEMITE3	
		CASE GLENDALE2	
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_PLATEHOLDER BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_VANITY_PLATE  BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_INTERIOR_TRIM BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_INTERIOR_DASH  BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_INTERIOR_DIALS BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_INTERIOR_DOORS  BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_SEATS  BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_STEERING_WHEEL BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_COLUMN_SHIFT_LEVER BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_PLAQUE BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_ICE  BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_TRUNK BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_ENGINE_BAY BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_AIR_FILTER  BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_FITTINGS BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_CHASSIS_ARCH_COVERS   BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_CHASSIS_AERIALS   BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_CHASSIS_TRIM  BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_CHASSIS_TANK   BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_LEFT_DOOR BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_RIGHT_DOOR  BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_LIVERY  BREAK
				CASE CMM_SUPERMOD_HYDRO			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_HIGH_STOCK_HYDRAULICS BREAK
			ENDSWITCH
		BREAK
		
		CASE COMET3
		CASE ELEGY
		CASE COMET2
		CASE ELEGY2
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_PLATEHOLDER BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_VANITY_PLATE  BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_INTERIOR_TRIM BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_INTERIOR_DASH  BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_INTERIOR_DIALS BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_INTERIOR_DOORS  BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_SEATS  BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_STEERING_WHEEL BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_COLUMN_SHIFT_LEVER BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_PLAQUE BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_ICE  BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_TRUNK BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_ENGINE_BAY BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_AIR_FILTER  BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_FITTINGS BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_CHASSIS_ARCH_COVERS   BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_CHASSIS_AERIALS   BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_CHASSIS_TRIM  BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_CHASSIS_TANK   BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_LEFT_DOOR BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_RIGHT_DOOR  BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_LIVERY  BREAK
				CASE CMM_SUPERMOD_HYDRO			RETURN g_sMPTunables.iSUPERMOD_SLOT_IE_RETRO_STOCK_HYDRAULICS BREAK
			ENDSWITCH
		BREAK
		DEFAULT
			SWITCH eMenu
				CASE CMM_SUPERMOD_PLTHOLDER		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plateholder * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_PLTVANITY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Vanity_Plate  * iBuyNowMultiplier BREAK
				CASE CMM_TRIM
				CASE CMM_SUPERMOD_INTERIOR1		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Trim  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_INTERIOR3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dash  * iBuyNowMultiplier BREAK
				CASE CMM_DIALS
				CASE CMM_SUPERMOD_INTERIOR4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Dials  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_INTERIOR5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Interior_Doors * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_SEATS			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Seats  * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_STEERING		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Steering_Wheel * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_KNOB			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Column_Shift_Lever * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_PLAQUE		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Plaque  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_ICE			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_ICE * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_TRUNK			RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Trunk  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_ENGINEBAY1	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Engine_Bay  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_ENGINEBAY2	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Air_Filter  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_ENGINEBAY3	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Fittings * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_CHASSIS2		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Arch_Covers  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_CHASSIS3		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Aerials  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_CHASSIS4		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Trim * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_CHASSIS5		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Chassis_Tank  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_DOOR_L		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Left_Door  * iBuyNowMultiplier BREAK
				CASE CMM_SUPERMOD_DOOR_R		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Right_Door * iBuyNowMultiplier  BREAK
				CASE CMM_SUPERMOD_LIVERY		RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery  * iBuyNowMultiplier BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 12345
ENDFUNC

FUNC BOOL DOES_VEHICLE_USE_INTERIOR1_AS_SUNSTRIPS(MODEL_NAMES vehTocheck)
	SWITCH vehTocheck
		CASE COMET6
		CASE VECTRE 
		CASE WARRENER2
		CASE REMUS
		CASE JESTER4
		CASE RT3000
		CASE CYPHER
		CASE SULTAN3
		#IF FEATURE_DLC_1_2022
		CASE TENF2
		CASE KANJOSJ
		CASE POSTLUDE
		CASE SENTINEL4
		CASE BRIOSO3
		CASE WEEVIL2
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC 

FUNC INT GET_MP_CARMOD_MENU_OPTION_COST(CARMOD_MENU_ENUM eMenu, INT iOption, INT iStage = 0, STRING sLabel = NULL, BOOL bBuyNowPrice = FALSE, BOOL bSellValue = FALSE)	
	
	UNUSED_PARAMETER(bSellValue)
	
	FLOAT fBuyNowMultiplier = 1
	IF bBuyNowPrice
		fBuyNowMultiplier = g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER
	ENDIF
	
	IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
	OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()	
	#IF FEATURE_DLC_1_2022
	OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
	#ENDIF
		RETURN 0
	ENDIF

	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(g_ModVehicle)
	
	SWITCH eMenu
		CASE CMM_ARMOUR
			IF g_eModPriceVariation = MPV_SPECIAL
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(7500) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(12000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(20000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(35000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(50000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(650) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(6375) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(10625) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(17000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(29750) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(42500) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(5250) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(8750) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(14000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(24500) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(35000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(4500) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(7500) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(12000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(21000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(30000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(3600) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(6000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(9600) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(16800) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(24000) * GET_ARMOR_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_BRAKES
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_BRAKES) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_BRAKES)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_BRAKES
						ENDIF
					ELSE	
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("CMOD_BRA_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_BRAKES
							ENDSWITCH
						ENDIF	
					ENDIF					
				ENDIF
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(20000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(27000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(35000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(60000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH		
			ELIF g_eModPriceVariation = MPV_SPORT
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_BRAKES) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_BRAKES)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_BRAKES2
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("CMOD_BRA_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_BRAKES2
							ENDSWITCH
						ENDIF	
					ENDIF
				ENDIF	
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(650) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(13000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(17550) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(22750) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(60000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))BREAK
				ENDSWITCH		
			ELIF g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(10000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(13500) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(17500) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(60000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(4000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(5400) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(7000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(60000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(4000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(5400) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(7000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(60000) * GET_BRAKES_EXPENDITURE_TUNEABLE(iOption))BREAK
				ENDSWITCH
			ENDIF
		BREAK  
		CASE CMM_BULLBARS
			SWITCH iOption
				CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 1	RETURN FLOOR(TO_FLOAT(10000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 2	RETURN FLOOR(TO_FLOAT(12000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 3	RETURN FLOOR(TO_FLOAT(14000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 4	RETURN FLOOR(TO_FLOAT(16000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 5	RETURN FLOOR(TO_FLOAT(16500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 6	RETURN FLOOR(TO_FLOAT(16750) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 7	RETURN FLOOR(TO_FLOAT(17000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 8	RETURN FLOOR(TO_FLOAT(17250) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 9	RETURN FLOOR(TO_FLOAT(17500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 10	RETURN FLOOR(TO_FLOAT(17750) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 11	RETURN FLOOR(TO_FLOAT(18000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 12	RETURN FLOOR(TO_FLOAT(18250) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 13	RETURN FLOOR(TO_FLOAT(18500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 14	RETURN FLOOR(TO_FLOAT(18750) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 15	RETURN FLOOR(TO_FLOAT(19000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 16	RETURN FLOOR(TO_FLOAT(19250) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_INSURANCE
			#IF FEATURE_TUNER
			IF SHOULD_DISCOUNT_TUNER_AUTO_SHOP_MODS()
				RETURN 0
			ELSE	
			#ENDIF
				SWITCH iOption
					CASE 0 RETURN FLOOR(TO_FLOAT(2000) * GET_TRACKER_EXPENDITURE_TUNEABLE())		BREAK	// tracker
					CASE 1 RETURN 10000 BREAK
				ENDSWITCH
			#IF FEATURE_TUNER
			ENDIF
			#ENDIF
		BREAK
		CASE CMM_ROLLCAGE
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_ROLL_CAGE) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_1) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_2) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_3) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_4) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_4) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_5) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_5) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_6) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_6) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 7	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_7) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_7) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 8
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_8) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_8) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 9
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_9) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_9) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 10
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_10) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_10) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(100   * fBuyNowMultiplier)	BREAK
					CASE 1	RETURN FLOOR(7000  * fBuyNowMultiplier)	BREAK
					CASE 2	RETURN FLOOR(7950  * fBuyNowMultiplier)	BREAK
					CASE 3	RETURN FLOOR(8500  * fBuyNowMultiplier)	BREAK
					CASE 4	RETURN FLOOR(10000 * fBuyNowMultiplier)	BREAK
					CASE 5	RETURN FLOOR(11000 * fBuyNowMultiplier)	BREAK
					CASE 6	RETURN FLOOR(11950 * fBuyNowMultiplier)	BREAK
					CASE 7	RETURN FLOOR(13500 * fBuyNowMultiplier)	BREAK
					CASE 8	RETURN FLOOR(14000 * fBuyNowMultiplier)	BREAK
				ENDSWITCH
			ENDIF	
		BREAK
		CASE CMM_HOOD
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_HOOD_1		
						ELSE
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(3000) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
							ELSE
								RETURN FLOOR(TO_FLOAT(3000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
							ENDIF
						ENDIF
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(5000) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(5000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(8000) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(8000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(9600) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(9600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(11000) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(11000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(11600) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(11600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 7	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(12000) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(12000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(12400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(12400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(12500) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(12900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(13250) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(13600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(13900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(14150) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(14400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(14650) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(14850) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(15050) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(15250) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(15450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(15650) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(15850) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(16050) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(16250) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(16450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(16650) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(16850) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(17050) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(1700) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(2400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(2900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(4450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(5000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(5600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(6000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(6500) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(7000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(7200) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(7750) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(8150) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(8600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(9000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(9400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(9800) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(10200) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(10550) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(10900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(11250) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(11600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(11900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(12200) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(12500) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(12800) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(650) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1750) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(2900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(4450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(5000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(6000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(6450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(6800) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(7150) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(7500) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(7850) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(8150) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(8450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(8750) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(9050) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(9300) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(9550) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(9800) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(10050) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(10250) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(10450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(700) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(898) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(1250) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(1750) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(2000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(2400) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(2900) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(4450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5150) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5700) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6000) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(6300) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(9600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(9850) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(10100) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(10350) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(10600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(10850) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(11100) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(11350) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(11600) * GET_BONNET_EXPENDITURE_TUNEABLE(iOption))	BREAK
				ENDSWITCH
			ENDIF

		BREAK
		
		CASE CMM_BUMPERS
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(2200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_FRONT_BUMPER_1
						ELSE
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(4600) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
							ELSE
								RETURN FLOOR(TO_FLOAT(4600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
							ENDIF	
						ENDIF
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(7400) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(7400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(11700) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(11700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(14500) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(14500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(14700) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(14700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(14900) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(14900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 7	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(15100) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))
						ELSE
							RETURN FLOOR(TO_FLOAT(15100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))	
						ENDIF
					BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT( 15300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT( 15500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT( 15700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT( 15900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT( 16100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT( 16300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT( 16500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT( 16700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT( 16900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT( 17100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT( 17300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT( 17500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 20	RETURN FLOOR(TO_FLOAT(2200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(0))				BREAK
					CASE 21	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_REAR_BUMPER_1				
						ELSE
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(4600) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(1))
							ELSE
								RETURN FLOOR(TO_FLOAT(4600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(1))	
							ENDIF
						ENDIF
					BREAK
					CASE 22	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(7400) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(2))
						ELSE
							RETURN FLOOR(TO_FLOAT(7400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(2))	
						ENDIF
					BREAK
					CASE 23	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(11700) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_FBUMBER_EXPENDITURE_TUNEABLE(3))
						ELSE
							RETURN FLOOR(TO_FLOAT(11700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(3))	
						ENDIF
					BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT( 14500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))			BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT( 14700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))			BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT( 14900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))			BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT( 15100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(7))			BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT( 15300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(8))			BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT( 15500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(9))			BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT( 15700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(10))			BREAK
					CASE 31	RETURN FLOOR(TO_FLOAT( 15900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(11))			BREAK
					CASE 32	RETURN FLOOR(TO_FLOAT( 16100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(12))			BREAK
					CASE 33	RETURN FLOOR(TO_FLOAT( 16300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(13))			BREAK
					CASE 34	RETURN FLOOR(TO_FLOAT( 16500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(14))			BREAK
					CASE 35	RETURN FLOOR(TO_FLOAT( 16700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(15))			BREAK
					CASE 36	RETURN FLOOR(TO_FLOAT( 16900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(16))			BREAK
					CASE 37	RETURN FLOOR(TO_FLOAT( 17100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(17))			BREAK
					CASE 38	RETURN FLOOR(TO_FLOAT( 17300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(18))			BREAK
					CASE 39	RETURN FLOOR(TO_FLOAT( 17500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(19))			BREAK
					CASE 40	RETURN FLOOR(TO_FLOAT( 17700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(20))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(115000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(205000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(285000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(310000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(330000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 20	RETURN FLOOR(TO_FLOAT(1000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(0))				BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_SHARED_GRENADE_POD) 		* GET_RBUMBER_EXPENDITURE_TUNEABLE(1))			BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(310000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(2))			BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(330000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(3))			BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(350000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))			BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(365000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))			BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(365000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))			BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(365000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(7))			BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(365000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(8))			BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(365000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(9))			BREAK
				ENDSWITCH
			
			ELIF g_eModPriceVariation = MPV_SMUG_STANDARD
				SWITCH iOption	
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_CHAFF			BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_DECOY_FLARES		BREAK
					
					CASE WHEEL_ACCS_SMOKE_WHITE		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLACK		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLUE		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_GREEN		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_RED		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PINK		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_BROWN		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_INDI		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_CREW		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK	
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_SMUG_HEAVY	
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_CHAFF			BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_DECOY_FLARES		BREAK
					
					CASE WHEEL_ACCS_SMOKE_WHITE		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLACK		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLUE		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_GREEN		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_RED		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PINK		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_BROWN		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_INDI		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_CREW		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK	
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(2500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(3300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(3900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(4700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(4900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(5100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(5300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(5500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(6100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(6300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(6500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(6900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(7100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(7300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 20	RETURN FLOOR(TO_FLOAT(1500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(2500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(1))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(3300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(2))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(3900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(3))		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(4700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(4900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(5100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(5300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(7))		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(5500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(8))		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(5700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(9))		BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT(5900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(10))		BREAK
					CASE 31	RETURN FLOOR(TO_FLOAT(6100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(11))		BREAK
					CASE 32	RETURN FLOOR(TO_FLOAT(6300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(12))		BREAK
					CASE 33	RETURN FLOOR(TO_FLOAT(6500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(13))		BREAK
					CASE 34	RETURN FLOOR(TO_FLOAT(6700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(14))		BREAK
					CASE 35	RETURN FLOOR(TO_FLOAT(6900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(15))		BREAK
					CASE 36	RETURN FLOOR(TO_FLOAT(7100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(16))		BREAK
					CASE 37	RETURN FLOOR(TO_FLOAT(7300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(17))		BREAK
					CASE 38	RETURN FLOOR(TO_FLOAT(7500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(18))		BREAK
					CASE 39	RETURN FLOOR(TO_FLOAT(7700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(19))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(1000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(2000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2800) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(3400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(3600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(3800) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(4000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(4200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(4400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(4600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(4800) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(5000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(5200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(5400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(5600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 20	RETURN FLOOR(TO_FLOAT(500) *  GET_RBUMBER_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(1000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(1))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(1500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(2))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(2000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(3))		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(2600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(2800) * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(3000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(3200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(7))		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(3400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(8))		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(3600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(9))		BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT(3800) * GET_RBUMBER_EXPENDITURE_TUNEABLE(10))		BREAK
					CASE 31	RETURN FLOOR(TO_FLOAT(4000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(11))		BREAK
					CASE 32	RETURN FLOOR(TO_FLOAT(4200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(12))		BREAK
					CASE 33	RETURN FLOOR(TO_FLOAT(4400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(13))		BREAK
					CASE 34	RETURN FLOOR(TO_FLOAT(4600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(14))		BREAK
					CASE 35	RETURN FLOOR(TO_FLOAT(4800) * GET_RBUMBER_EXPENDITURE_TUNEABLE(15))		BREAK
					CASE 36	RETURN FLOOR(TO_FLOAT(5000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(16))		BREAK
					CASE 37	RETURN FLOOR(TO_FLOAT(5200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(17))		BREAK
					CASE 38	RETURN FLOOR(TO_FLOAT(5400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(18))		BREAK
					CASE 39	RETURN FLOOR(TO_FLOAT(5600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(19))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(298) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(390) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(750) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(1000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(1200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(1400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(1600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(1800) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(2000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(2200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(2400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(2600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(2800) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(3000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(3200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(3400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(3600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(3800) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(4000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 20	RETURN FLOOR(TO_FLOAT(500) *  GET_RBUMBER_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(1000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(1))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(1500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(2))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(2000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(3))		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(2600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(2800) * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(3000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(3200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(7))		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(3400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(8))		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(3600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(9))		BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT(3800) * GET_RBUMBER_EXPENDITURE_TUNEABLE(10))		BREAK
					CASE 31	RETURN FLOOR(TO_FLOAT(4000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(11))		BREAK
					CASE 32	RETURN FLOOR(TO_FLOAT(4200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(12))		BREAK
					CASE 33	RETURN FLOOR(TO_FLOAT(4400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(13))		BREAK
					CASE 34	RETURN FLOOR(TO_FLOAT(4600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(14))		BREAK
					CASE 35	RETURN FLOOR(TO_FLOAT(4800) * GET_RBUMBER_EXPENDITURE_TUNEABLE(15))		BREAK
					CASE 36	RETURN FLOOR(TO_FLOAT(5000) * GET_RBUMBER_EXPENDITURE_TUNEABLE(16))		BREAK
					CASE 37	RETURN FLOOR(TO_FLOAT(5200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(17))		BREAK
					CASE 38	RETURN FLOOR(TO_FLOAT(5400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(18))		BREAK
					CASE 39	RETURN FLOOR(TO_FLOAT(5600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(19))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GR_LIGHT
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(5000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(85000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 20	RETURN FLOOR(TO_FLOAT(3500) *  GET_RBUMBER_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(92500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(1))		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_CHASSIS
			IF vehicleModel = DUNE3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_DUNE3_LVL_1_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_DUNE3_LVL_2_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF vehicleModel = HALFTRACK
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_HALFTRACK_LVL_1_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_HALFTRACK_LVL_2_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_HALFTRACK_LVL_3_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF vehicleModel = TAMPA3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TAMPA3_LVL_1_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TAMPA3_LVL_2_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TAMPA3_LVL_3_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF vehicleModel = INSURGENT3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_INSURGENT3_LVL_1_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_INSURGENT3_LVL_2_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_INSURGENT3_LVL_3_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF vehicleModel = TECHNICAL3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TECHNICAL3_LVL_1_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TECHNICAL3_LVL_2_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TECHNICAL3_LVL_3_VEHICLE_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF vehicleModel = MENACER
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_MENACER_TOP_50_CAL_MINIGUN) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(285000) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH	
			ELIF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehicleModel)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_STOCK_ARMOR) 		* GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iFIXER_ARMOR_MOD_HEAVY_ARMOUR_PLATING) 	* GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_SMUG_STANDARD
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_WING_MOUNTED_MISSILES		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SMUG_HEAVY
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_REAR_EXPLOSIVE_CANNON_TURRETS		BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_MOUNTED_HOMING_MISSILES		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_ARMOR_MOD_DEFAULT_ARMOUR_PLATING_SHARED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_ARMOR_MOD_LIGHT_ARMOUR_PLATING_SHARED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_ARMOR_MOD_REINFORCED_ARMOUR_PLATING_SHARED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_ARMOR_MOD_HEAVY_ARMOUR_PLATING_SHARED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(225000) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(310000) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH		
			ELIF  g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_STOCK_ARMOR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_1) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 2
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_2) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_3) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_4) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_4) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_5) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_5) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_6) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ARMOR_6) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
				ENDSWITCH	
			ELIF  g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_STOCK_LID) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_OFFROAD) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_OFFROAD_SPOILER) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_OFFROAD_NETS) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_OFFROAD_COMBINED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_TROPHY_TRUCK) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_TROPHY_TRUCK_SPOILER) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_TROPHY_TRUCK_NETS) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_TROPHY_TRUCK_COMBINED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_GANG_BURRITO) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_GANG_BURRITO_SPOILER) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_GANG_BURRITO_BULLBAR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_GANG_BURRITO_COMBINED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_BIG_BRAT) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_BIG_BRAT_CAGE) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_BIG_BRAT_BULLBAR) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_BIG_BRAT_COMBINED) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_MIDNIGHT_PUMPING) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_MIDNIGHT_PUMPING_CAGE) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_MIDNIGHT_PUMPING_FLAMES) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_MIDNIGHT_PUMPING_COMBINED ) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(1100) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1350) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1500) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2750) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(3600) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(4900) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(5800) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(7000) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(8000) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(9150) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(10100) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(11050) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(11950) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(12900) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(13750) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(14600) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(15450) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(16250) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(17050) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(17800) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(18550) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(19250) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(19950) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(20600) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(21250) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(21850) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(22450) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(23000) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(23550) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT(24050) * GET_CHASSIS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
				ENDSWITCH
			ENDIF
		BREAK
				
		CASE CMM_ENGINE
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR  g_eModPriceVariation = MPV_ARENA_WARS
			OR  g_eModPriceVariation = MPV_RC_BANDITO
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_ENGINE) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_ENGINE)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_ENGINE
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("CMOD_ENG_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_ENGINE
							ENDSWITCH
						ENDIF	
					ENDIF	
				ENDIF
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))				BREAK
					CASE 1	RETURN  FLOOR(TO_FLOAT(9000) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN  FLOOR(TO_FLOAT(12500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN  FLOOR(TO_FLOAT(18000) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN  FLOOR(TO_FLOAT(33500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))							BREAK
					CASE 1	RETURN  FLOOR(TO_FLOAT(4500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN  FLOOR(TO_FLOAT(6250) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN  FLOOR(TO_FLOAT(9000) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN  FLOOR(TO_FLOAT(16750) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_ENGINE) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_ENGINE)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_ENGINE2
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("CMOD_ENG_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_ENGINE2
							ENDSWITCH
						ENDIF	
					ENDIF	
				ENDIF
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN  FLOOR(TO_FLOAT(5850) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN  FLOOR(TO_FLOAT(8125) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN  FLOOR(TO_FLOAT(11700) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN  FLOOR(TO_FLOAT(21775) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN  FLOOR(TO_FLOAT(1800) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN  FLOOR(TO_FLOAT(2500) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN  FLOOR(TO_FLOAT(3600) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN  FLOOR(TO_FLOAT(6700) * GET_ENGINE_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_EXHAUST
			IF g_eModPriceVariation = MPV_SMUG_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_ROCKET_ASSISTED_TAKE_OFF 		BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_SILENT_ROTOR							BREAK
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_SMUG_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_ROCKET_ASSISTED_TAKE_OFF 			BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_SILENT_ROTOR							BREAK
				ENDSWITCH	
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(260) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(750) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1800) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(3000) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(9550) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(9750) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(10500) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(12000) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(13750) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(14625) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(15500) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(16475) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(17250) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(18125) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(19100) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(19975) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(20900) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(21675) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(22550) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(23350) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(24175) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(25025) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(25900) * GET_EXHAUST_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	BREAK
				ENDSWITCH
			ENDIF	
		BREAK
		CASE CMM_EXPLOSIVES
			IF INT_TO_ENUM(FIXER_PAYPHONE_VARIATION, GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID())) = FPV_MOTEL
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_EXHAUST_EXPLOSIVES_TUNEABLE(iOption))	BREAK	// IGNITION BOMB
					CASE 1	RETURN FLOOR(TO_FLOAT(750) * GET_EXHAUST_EXPLOSIVES_TUNEABLE(iOption))	BREAK 	// REMOTE BOMB
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(5000) * GET_EXHAUST_EXPLOSIVES_TUNEABLE(iOption))	BREAK	// IGNITION BOMB
					CASE 1	RETURN FLOOR(TO_FLOAT(7500) * GET_EXHAUST_EXPLOSIVES_TUNEABLE(iOption))	BREAK 	// REMOTE BOMB
				ENDSWITCH
			ENDIF	
		BREAK
		CASE CMM_TRANSMISSION
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR  g_eModPriceVariation = MPV_ARENA_WARS
			OR  g_eModPriceVariation = MPV_RC_BANDITO
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_GEARBOX) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_GEARBOX)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_TRANSMISSION
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("CMOD_TRAN_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TRANSMISSION
							ENDSWITCH
						ENDIF	
					ENDIF	
				ENDIF
				#ENDIF
			
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(29500)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(32500)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(40000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(50000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(500)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(14750)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(16250)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(20000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(25000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_GEARBOX) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_GEARBOX)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_TRANSMISSION
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("CMOD_TRAN_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TRANSMISSION
							ENDSWITCH
						ENDIF	
					ENDIF	
				ENDIF
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(650)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(19175)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(21125)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(26000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(30000)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(5900) * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(6500) * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(8000) * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(12000) * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_GRILL
			IF g_eModPriceVariation = MPV_GR_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_CHAFF					BREAK
					CASE 2	RETURN g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_DECOY_FLARES       		BREAK
					
					CASE WHEEL_ACCS_SMOKE_WHITE		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLACK		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLUE		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_GREEN		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_RED		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PINK		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_BROWN		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_INDI		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_CREW		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK				
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 1	
						#IF FEATURE_GEN9_EXCLUSIVE
						IF vehicleModel = IGNUS2
							RETURN g_sMPTunables.iGEN9_MODS_COUNTER_CLEARCOAT
						ELSE
						#ENDIF
							RETURN FLOOR(TO_FLOAT(750) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		
						#IF FEATURE_GEN9_EXCLUSIVE
						ENDIF
						#ENDIF
					BREAK	
					CASE 2	RETURN FLOOR(TO_FLOAT(1340) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1650) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(3000) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(3200) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3650) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(4100) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4550) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5350) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5700) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(6050) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(6400) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6750) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(7100) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(7450) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(7800) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(8150) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(8500) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8850) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_HEADLIGHTS
			IF g_eModPriceVariation = MPV_GR_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700)  * GET_GEARBOX_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_CHAFF					BREAK
					CASE 2	RETURN g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_DECOY_FLARES       		BREAK
					
					CASE WHEEL_ACCS_SMOKE_WHITE		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLACK		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLUE		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_GREEN		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_RED		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PINK		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_BROWN		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_INDI		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_CREW		RETURN  g_sMPTunables.iBB_COUNTERMEASURE_MOD_OPPRESSOR2_SMOKE 	BREAK				
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(750) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1340) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1650) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(3000) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(3200) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3650) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(4100) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4550) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5350) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5700) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(6050) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(6400) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6750) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(7100) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(7450) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(7800) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(8150) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(8500) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8850) * GET_GRILL_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)	    BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_HORN
			CARMOD_HORN_ENUM eHorn
			eHorn = INT_TO_ENUM(CARMOD_HORN_ENUM, iOption)
			SWITCH eHorn
				CASE HORN_STOCK							RETURN FLOOR(TO_FLOAT(500) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_STOCK)) BREAK
				
				CASE HORN_INDEP_1						RETURN g_sMPTunables.iVehiclekit_IndependenceDay_Horn_1	BREAK 	//= 25000
				CASE HORN_INDEP_2						RETURN g_sMPTunables.iVehiclekit_IndependenceDay_Horn_2	BREAK 	//= 27500
				CASE HORN_INDEP_3						RETURN g_sMPTunables.iVehiclekit_IndependenceDay_Horn_3	BREAK 	//= 32500
				CASE HORN_INDEP_4						RETURN g_sMPTunables.iVehiclekit_IndependenceDay_Horn_4	BREAK 	//= 35000
				
				CASE HORN_HIPSTER_1						RETURN g_sMPTunables.iDLC_hipster_modifier_Jazz_Horn_1 BREAK 	// = 20000
				CASE HORN_HIPSTER_2						RETURN g_sMPTunables.iDLC_hipster_modifier_Jazz_Horn_2 BREAK 	// = 21500
				CASE HORN_HIPSTER_3						RETURN g_sMPTunables.iDLC_hipster_modifier_Jazz_Horn_3 BREAK 	// = 23000
				CASE HORN_HIPSTER_4						RETURN g_sMPTunables.iDLC_hipster_modifier_Jazz_Horn_4 BREAK 	// = 50000
				
				CASE HORN_BUSINESS2_SCALE_C0			RETURN g_sMPTunables.iHORN_CNOTE_C0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_D0			RETURN g_sMPTunables.iHORN_CNOTE_D0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_E0			RETURN g_sMPTunables.iHORN_CNOTE_E0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_F0			RETURN g_sMPTunables.iHORN_CNOTE_F0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_G0			RETURN g_sMPTunables.iHORN_CNOTE_G0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_A0			RETURN g_sMPTunables.iHORN_CNOTE_A0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_B0			RETURN g_sMPTunables.iHORN_CNOTE_B0 BREAK 						// = 25000
				CASE HORN_BUSINESS2_SCALE_C1			RETURN g_sMPTunables.iHORN_CNOTE_C1 BREAK 						// = 25000
			
				CASE HORN_BUSINESS1_1					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_1	 BREAK		// = 20000
				CASE HORN_BUSINESS1_2					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_2	 BREAK		// = 21500
				CASE HORN_BUSINESS1_3					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_3	 BREAK		// = 23000
				CASE HORN_BUSINESS1_4					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_4	 BREAK		// = 24500
				CASE HORN_BUSINESS1_5					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_5	 BREAK		// = 26000
				CASE HORN_BUSINESS1_6					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_6	 BREAK		// = 27500
				CASE HORN_BUSINESS1_7					RETURN g_sMPTunables.iDLC_CAR_MODS_Classical_Horn_7	 BREAK		// = 25000
				
				CASE HORN_LUXE_1_FULL 					RETURN g_sMPTunables.iDLC_CAR_MODS_Luxe_Horn_1 BREAK
				CASE HORN_LUXE_2_FULL 					RETURN g_sMPTunables.iDLC_CAR_MODS_Luxe_Horn_2 BREAK
				CASE HORN_LUXE_3_FULL 					RETURN g_sMPTunables.iDLC_CAR_MODS_Luxe_Horn_3 BREAK
				
				CASE HORN_LOWRIDER_1_FULL				RETURN g_sMPTunables.iCar_Mods_San_Andreas_Loop BREAK
				CASE HORN_LOWRIDER_2_FULL				RETURN g_sMPTunables.iCar_Mods_Liberty_City_Loop BREAK
				
				CASE HORN_HALLOWEEN_1_FULL				RETURN g_sMPTunables.iHalloween_2015_Halloween_Loop_1 BREAK
				CASE HORN_HALLOWEEN_2_FULL				RETURN g_sMPTunables.iHalloween_2015_Halloween_Loop_2 BREAK
				

				CASE HORN_XM15_HORN_1_FULL				RETURN g_sMPTunables.iXmas2015_Horn_Festive_Loop_1 BREAK
				CASE HORN_XM15_HORN_2_FULL				RETURN g_sMPTunables.iXmas2015_Horn_Festive_Loop_2 BREAK
				CASE HORN_XM15_HORN_3_FULL				RETURN g_sMPTunables.iXmas2015_Horn_Festive_Loop_3 BREAK
				
				CASE HORN_TRUCK							RETURN FLOOR(TO_FLOAT(2000) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_TRUCK))	BREAK
				CASE HORN_COP							RETURN FLOOR(TO_FLOAT(3000) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_COP))	BREAK
				CASE HORN_CLOWN							RETURN FLOOR(TO_FLOAT(5000) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_CLOWN))	BREAK
				CASE HORN_MUSICAL_1						RETURN FLOOR(TO_FLOAT(10000) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_MUSICAL_1)) BREAK
				CASE HORN_MUSICAL_2						RETURN FLOOR(TO_FLOAT(12500) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_MUSICAL_2)) BREAK
				CASE HORN_MUSICAL_3						RETURN FLOOR(TO_FLOAT(15000) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_MUSICAL_3)) BREAK
				CASE HORN_MUSICAL_4						RETURN FLOOR(TO_FLOAT(17500) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_MUSICAL_4)) BREAK
				CASE HORN_MUSICAL_5						RETURN FLOOR(TO_FLOAT(25000) * GET_HORN_EXPENDITURE_TUNEABLE(HORN_MUSICAL_5)) BREAK
				CASE HORN_SAD_TROMBONE					RETURN g_sMPTunables.iCar_Mods_Horn_Sad_Trombone			BREAK
				
				CASE HORN_AIRHORN_01					RETURN g_sMPTunables.iAW_SALE_PRICE_MODS_AIR_HORN_1     	BREAK
				CASE HORN_AIRHORN_02					RETURN g_sMPTunables.iAW_SALE_PRICE_MODS_AIR_HORN_2 		BREAK
				CASE HORN_AIRHORN_03					RETURN g_sMPTunables.iAW_SALE_PRICE_MODS_AIR_HORN_3 		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_LIGHTS_HEAD
		CASE CMM_LIGHTS
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR  g_eModPriceVariation = MPV_ARENA_WARS
			OR  g_eModPriceVariation = MPV_RC_BANDITO
			OR (iOption > 1  AND eMenu = CMM_LIGHTS_HEAD)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(600) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(7500) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_WHITE) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_WHITE) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_BLUE) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_BLUE) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_ELECTIC_BLUE) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_ELECTIC_BLUE) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_MINT_GREEN)  * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_MINT_GREEN) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 6
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_LIME_GREEN) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_LIME_GREEN) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 7	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_YELLOW) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_YELLOW) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 8	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_GOLDEN_SHOWER) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_GOLDEN_SHOWER) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 9	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_ORANGE) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_ORANGE) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 10	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_RED) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_RED) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 11	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_PONY_PINK) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_PONY_PINK) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 12	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_HOT_PINK) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_HOT_PINK) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 13	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_PURPLE) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_PURPLE) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					CASE 14	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_BLACKLIGHT) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_BLACKLIGHT) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))		
						ENDIF
					BREAK
					DEFAULT
						RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_VEHICLE_HEADLIGHTS_BLACKLIGHT) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))
					BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(400) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(5000) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					DEFAULT
						RETURN FLOOR(TO_FLOAT(5000) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))
					BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(3000) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))	BREAK
					DEFAULT
						RETURN FLOOR(TO_FLOAT(3000) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))
					BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(100) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(2000) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))	BREAK
					DEFAULT
						RETURN FLOOR(TO_FLOAT(2000) * GET_XENON_LIGHTS_EXPENDITURE_TUNEABLE(iOption))
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_LIGHTS_NEON
			IF iStage = 0 // Neon Layout
				SWITCH iOption
					CASE 0 	RETURN 100																BREAK // None
					CASE 1 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Front					BREAK // Front
					CASE 2 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Back                	BREAK // Back
					CASE 3 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Sides               	BREAK // Sides
					CASE 4 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Front_and_Back      	BREAK // Front and Back
					CASE 5 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Front_and_Sides     	BREAK // Front and Sides
					CASE 6 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Back_and_Sides      	BREAK // Back and Sides
					CASE 7 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Set_Up_Front_Back_and_Sides	BREAK // Front, Back and Sides
				ENDSWITCH
			ELIF iStage = 1 // Neon Colour
				SWITCH iOption
					CASE 0 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_White        			BREAK // White
					CASE 1 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Blue         			BREAK // Blue
					CASE 2 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Eclectic_Blue			BREAK // Eclectic blue
					CASE 3 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Mint_Green   			BREAK // Mint green
					CASE 4 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Lime_Green   			BREAK // Lime Green
					CASE 5 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Yellow       			BREAK // Yellow
					CASE 6 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Golden_Shower			BREAK // Golden shower
					CASE 7 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Orange       			BREAK // Orange
					CASE 8 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Red          			BREAK // Red
					CASE 9 	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Pony_Pink    			BREAK // Pony pink
					CASE 10	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Hot_Pink     			BREAK // Hot Pink
					CASE 11	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Purple       			BREAK // Purple
					CASE 12	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Blacklight   			BREAK // Blacklight#
					CASE 13	RETURN g_sMPTunables.iCar_Mods_Neon_Lights_Color_Crew         			BREAK // Crew
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH eMenu
		CASE CMM_PLATES
			SWITCH iOption
				CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_PLATES_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 1	RETURN FLOOR(TO_FLOAT(200) * GET_PLATES_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 2	RETURN FLOOR(TO_FLOAT(200) * GET_PLATES_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 3	RETURN FLOOR(TO_FLOAT(300) * GET_PLATES_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 4	RETURN FLOOR(TO_FLOAT(600) * GET_PLATES_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 10	RETURN FLOOR(TO_FLOAT(600) * GET_PLATES_EXPENDITURE_TUNEABLE(iOption))			BREAK
				DEFAULT	RETURN FLOOR(TO_FLOAT(600) * GET_PLATES_EXPENDITURE_TUNEABLE(99))		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ROOF
			IF IS_VEHICLE_MODEL(g_ModVehicle, AVENGER)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(0) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGO_UPGRADE_AVENGER_FRONT_TURRET) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGO_UPGRADE_AVENGER_FRONT_TOP_AND_REAR_TURRETS) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, CERBERUS)
			OR IS_VEHICLE_MODEL(g_ModVehicle, CERBERUS2)
			OR IS_VEHICLE_MODEL(g_ModVehicle, CERBERUS3)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_ADDITIONAL_FLAME_THROWER_TURRET) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ADDITIONAL_FLAME_THROWER_TURRET) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ADDITIONAL_FLAME_THROWER_TURRET) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						ENDIf	
					BREAK	
				ENDSWITCH
			ELIF  IS_VEHICLE_MODEL(g_ModVehicle, ISSI4)
			OR IS_VEHICLE_MODEL(g_ModVehicle, ISSI5)
			OR IS_VEHICLE_MODEL(g_ModVehicle, ISSI6)
			OR IS_VEHICLE_MODEL(g_ModVehicle, IMPERATOR)
			OR IS_VEHICLE_MODEL(g_ModVehicle, IMPERATOR2)
			OR IS_VEHICLE_MODEL(g_ModVehicle, IMPERATOR3)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_GRENADE_LAUNCHER) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_GRENADE_LAUNCHER_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_GRENADE_LAUNCHER_1) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						ENDIf	
					BREAK	
				ENDSWITCH			
			ELIF g_eModPriceVariation = MPV_GR_BIKE
				IF IS_VEHICLE_MODEL(g_ModVehicle, OPPRESSOR2)
					SWITCH iOption
						CASE 0	RETURN FLOOR(TO_FLOAT(750) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_OPPRESSOR2_EXPLOSIVE_MG) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_OPPRESSOR2_HOMING_MISSILES) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					ENDSWITCH
				ELSE
					SWITCH iOption
						CASE 0	RETURN FLOOR(TO_FLOAT(750) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_OPPRESSOR_ROCKETS) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					ENDSWITCH
				ENDIF
			ELIF g_eModPriceVariation = MPV_GR_LIGHT
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(55000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_DUNE3_40MM_GRENADE_LAUNCHER) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_DUNE3_762MM_MINIGUN) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GR_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(78500) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TRAILERSMALL2_MISSILE_BARRAGE) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TRAILERSMALL2_DUAL_20MM_FLAK_CANNONS) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GR_TRAILERLARGE
				PRINTLN("g_sMPTunables.iGR_UPGRADE_TRAILERLARGE_FRONT_TURRET: ", g_sMPTunables.iGR_UPGRADE_TRAILERLARGE_FRONT_TURRET)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(0) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TRAILERLARGE_FRONT_TURRET) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TRAILERLARGE_FRONT_AND_REAR_TURRETS) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
				
			ELIF vehicleModel = TORNADO5
				// Defaults
				RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption) * g_sMPTunables.fCar_Mods_Roof_Livery_Design_Multiplier)
			
			ELIF vehicleModel = DUKES
			OR vehicleModel = DUKES3
				CDEBUG1LN(DEBUG_SHOPS, "GET_MP_CARMOD_MENU_OPTION_COST(DUKES,", ENUM_TO_INT(g_eModPriceVariation)," CMM_ROOF, ", iOption, ")")
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(700) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN g_sMPTunables.iDukes_Roof_Mod_American       BREAK
					CASE 3	RETURN g_sMPTunables.iDukes_Roof_Mod_Argentinian    BREAK
					CASE 4	RETURN g_sMPTunables.iDukes_Roof_Mod_Austrian       BREAK
					CASE 5	RETURN g_sMPTunables.iDukes_Roof_Mod_Australian     BREAK
					CASE 6	RETURN g_sMPTunables.iDukes_Roof_Mod_Belgian        BREAK
					CASE 7	RETURN g_sMPTunables.iDukes_Roof_Mod_Brazilian      BREAK
					CASE 8	RETURN g_sMPTunables.iDukes_Roof_Mod_British        BREAK
					CASE 9	RETURN g_sMPTunables.iDukes_Roof_Mod_Canadian       BREAK
					CASE 10	RETURN g_sMPTunables.iDukes_Roof_Mod_Colombian      BREAK
					CASE 11	RETURN g_sMPTunables.iDukes_Roof_Mod_English        BREAK
					CASE 12	RETURN g_sMPTunables.iDukes_Roof_Mod_French         BREAK
					CASE 13	RETURN g_sMPTunables.iDukes_Roof_Mod_German         BREAK
					CASE 14	RETURN g_sMPTunables.iDukes_Roof_Mod_Italian        BREAK
					CASE 15	RETURN g_sMPTunables.iDukes_Roof_Mod_Irish          BREAK
					CASE 16	RETURN g_sMPTunables.iDukes_Roof_Mod_Jamaican       BREAK
					CASE 17	RETURN g_sMPTunables.iDukes_Roof_Mod_Japanese       BREAK
					CASE 18	RETURN g_sMPTunables.iDukes_Roof_Mod_Mexican        BREAK
					CASE 19	RETURN g_sMPTunables.iDukes_Roof_Mod_Norwegian      BREAK
					CASE 20	RETURN g_sMPTunables.iDukes_Roof_Mod_Russian        BREAK
					CASE 21	RETURN g_sMPTunables.iDukes_Roof_Mod_Scottish       BREAK
					CASE 22	RETURN g_sMPTunables.iDukes_Roof_Mod_Spanish        BREAK
					CASE 23	RETURN g_sMPTunables.iDukes_Roof_Mod_Swedish        BREAK
					CASE 24	RETURN g_sMPTunables.iDukes_Roof_Mod_Swiss          BREAK
					CASE 25	RETURN g_sMPTunables.iDukes_Roof_Mod_Turkish        BREAK
					CASE 26	RETURN g_sMPTunables.iDukes_Roof_Mod_Welsh          BREAK
					
					DEFAULT
						RETURN 2500
					BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SMUG_HEAVY	
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * 	GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_FRONT_EXPLOSIVE_CANNON_TURRETS 		BREAK
				ENDSWITCH		
			ELIF g_eModPriceVariation = MPV_SMUG_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_WING_MOUNTED_MACHINE_GUNS				BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_WING_MOUNTED_EXPLOSIVE_MACHINE_GUNS		BREAK
					CASE 3	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_WING_MOUNTED_EXTRA_WEAPON 			BREAK
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_GANG_OPS_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iH2_MODS_STANDARD_CUSTOM_ROOF_1							BREAK
					CASE 2	RETURN g_sMPTunables.iH2_MODS_STANDARD_CUSTOM_ROOF_2							BREAK
					CASE 3	RETURN g_sMPTunables.iH2_MODS_STANDARD_CUSTOM_ROOF_3							BREAK
				ENDSWITCH
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, REVOLTER)
			OR IS_VEHICLE_MODEL(g_ModVehicle, SAVESTRA)
			OR IS_VEHICLE_MODEL(g_ModVehicle, COMET4)
			OR IS_VEHICLE_MODEL(g_ModVehicle, VISERIS)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iH2_MODS_LIGHT_CUSTOM_ROOF_1								BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BATTLE_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_SHARED_DUAL_MINIGUNS) 			* GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_SHARED_DUAL_MISSILE_LAUNCHERS) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_SHARED_SAM_LAUNCHER) 			* GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(310000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(330000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BATTLE_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_MULE4_REMOTE_GRENADE_LAUNCHER) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(310000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(330000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(350000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(365000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH	
			ELSE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_ROOF_1		
						ELSE
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(700) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))		BREAK	
							ELSE
								RETURN FLOOR(TO_FLOAT(700) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))
							ENDIF			
						ENDIF
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(1150) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(1150) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))
						ENDIF
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(1600) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(1600) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))
						ENDIF
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(1950) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))			
						ELSE
							RETURN FLOOR(TO_FLOAT(1950) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption))
						ENDIF
					BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2400) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(2500) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(2850) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(3400) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(3950) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(4500) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5050) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5550) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(6000) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6350) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(6650) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6950) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(7250) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7550) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7850) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8150) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					DEFAULT
						RETURN FLOOR(TO_FLOAT(8150) * GET_ROOF_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)
					BREAK
			ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_SKIRTS
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR  g_eModPriceVariation = MPV_ARENA_WARS
			OR  g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(2500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_SKIRT_1		
						ELSE
							RETURN FLOOR(TO_FLOAT(5500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			
						ENDIF
					BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(8000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(10500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(14000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(14500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(15000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(15500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(16000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(16500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(17000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(17400) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(18300) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(19000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(20425) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(21025) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(21475) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(21925) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(22375) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(22825) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(23275) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(23725) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(24175) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(24625) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(24625) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(2500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(3000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(4000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(5500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(7800) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(8000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(8300) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(8500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(8550) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(8700) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(8800) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(9500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(11075) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(13525) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(16850) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(21050) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(600) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(830) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(14500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(15000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(15750) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(16200) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(16500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(18000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(18100) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(18325) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(18675) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(19150) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(19750) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(400) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(480) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(580) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(700) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(1000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(1200) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(1250) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(1330) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(1350) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(1750) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(2500) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(2550) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(2625) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(2725) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(2850) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(3000) * GET_SKIRTS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_SPOILER
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_ARENA_WARS
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_SPOILER) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_SPOILER)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER2
						ELIF GET_VEHICLE_MOD(g_ModVehicle, MOD_SPOILER) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_SPOILER) - 1
							RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("HSW_WING1") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER
								CASE HASH("HSW_WING2") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER2
							ENDSWITCH
						ENDIF	
					ENDIF	
				ENDIF
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(6000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_SPOILER_1		
						ELSE
							RETURN FLOOR(TO_FLOAT(7050) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption)  * fBuyNowMultiplier)			
						ENDIF
					BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(10000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(13000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(14000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(16000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(16500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(17000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(17500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(18000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(18500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(19000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(19500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(20000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(20500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(21000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(21500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(22750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(24000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(25250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(26500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(27750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(29000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(30250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(31500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(32750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(34000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(35250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(36500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(37750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
				ENDSWITCH

			ELIF g_eModPriceVariation = MPV_BATTLE_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_MULE4_MACHINE_GUN) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_MULE4_MISSILES) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(285000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(310000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(330000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				#IF FEATURE_GEN9_EXCLUSIVE
				IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
					IF bSellValue
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_SPOILER) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_SPOILER)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER2
						ELIF GET_VEHICLE_MOD(g_ModVehicle, MOD_SPOILER) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_SPOILER) - 1
							RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER
						ENDIF
					ELSE
						IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
							SWITCH GET_HASH_KEY(sLabel)
								CASE HASH("HSW_WING1") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER
								CASE HASH("HSW_WING2") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_SPOILER2
							ENDSWITCH
						ENDIF
					ENDIF	
				ENDIF
				#ENDIF
				
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(1500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1900) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(2600) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(3500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(4000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(4750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(5000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(5250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(6000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(6250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(6500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(7000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(7250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(8750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(10250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(11750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(13250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(13750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(14250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(14750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(15250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(15750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(16250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(16750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(17250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(17750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT(18250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(550) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(700) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1050) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(1500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(2300) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(2500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(3000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(3500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(4000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(4500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(6000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(6500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(7000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(7750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(8500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(9250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(10000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(400) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(600) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(800) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(1000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(1200) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(1500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(1600) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(1700) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(1800) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(1900) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(2000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(2100) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(2200) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(2300) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(2400) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(2500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(2850) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(3200) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(3550) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(3900) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GANG_OPS_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(700) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))			BREAK
					CASE 1	RETURN g_sMPTunables.iH2_MODS_STANDARD_CUSTOM_SPOILER_1							BREAK
					CASE 2	RETURN g_sMPTunables.iH2_MODS_STANDARD_CUSTOM_SPOILER_2							BREAK
					CASE 3	RETURN g_sMPTunables.iH2_MODS_STANDARD_CUSTOM_SPOILER_3							BREAK
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_BATTLE_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_POUNDER2_BARBED_WIRE_ROOF) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(12000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(18000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(26000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(39500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH	
			ENDIF
		BREAK
		CASE CMM_SUSPENSION
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
				IF bSellValue
					IF GET_VEHICLE_MOD(g_ModVehicle, MOD_SUSPENSION) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_SUSPENSION)
						RETURN g_sMPTunables.iGEN9_MODS_HSW_SUSPENSION
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
						SWITCH GET_HASH_KEY(sLabel)
							CASE HASH("CMOD_SUS_HSW") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_SUSPENSION
						ENDSWITCH
					ENDIF
				ENDIF	
			ENDIF
			#ENDIF
			
			SWITCH iOption
				CASE 0	RETURN FLOOR(TO_FLOAT(200) * GET_SUSPENSION_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 1	RETURN FLOOR(TO_FLOAT(1000) * GET_SUSPENSION_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 2	RETURN FLOOR(TO_FLOAT(2000) * GET_SUSPENSION_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 3	RETURN FLOOR(TO_FLOAT(3400) * GET_SUSPENSION_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 4	RETURN FLOOR(TO_FLOAT(4400) * GET_SUSPENSION_EXPENDITURE_TUNEABLE(iOption))			BREAK
				CASE 5	RETURN FLOOR(TO_FLOAT(4600) * GET_SUSPENSION_EXPENDITURE_TUNEABLE(iOption))			BREAK
			ENDSWITCH
		BREAK
		CASE CMM_TURBO
			IF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
			OR g_eModPriceVariation = MPV_BANSHEE2
			OR g_eModPriceVariation = MPV_BTYPE3
			OR g_eModPriceVariation = MPV_VIRGO2
			OR g_eModPriceVariation = MPV_GR_HEAVY
			OR g_eModPriceVariation = MPV_GR_LIGHT
			OR g_eModPriceVariation = MPV_GR_BIKE
			OR g_eModPriceVariation = MPV_GR_TRAILERLARGE
			OR g_eModPriceVariation = MPV_SMUG_STANDARD
			OR g_eModPriceVariation = MPV_GANG_OPS_STANDARD
			OR g_eModPriceVariation = MPV_SMUG_HEAVY
			OR g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
			OR  g_eModPriceVariation = MPV_ARENA_WARS
			OR  g_eModPriceVariation = MPV_RC_BANDITO

				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(5000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(50000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption)) 	BREAK
				ENDSWITCH					
			ELIF g_eModPriceVariation = MPV_SPORT
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(3250) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(42500) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption)) 	BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(2500) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(35000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption)) 	BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
			OR g_eModPriceVariation = MPV_SULTANRS
			OR g_eModPriceVariation = MPV_SLAMVAN3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(30000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption))	BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(1000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption))	BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(24000) * GET_TURBO_EXPENDITURE_TUNEABLE(iOption)) 	BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_WHEEL_ACCS
			SWITCH iOption
				CASE WHEEL_ACCS_OPTION_STT			RETURN g_sMPTunables.iTUNER_MODS_JUNKYARD_TIRES_STOCK							BREAK
				CASE WHEEL_ACCS_OPTION_BPT			RETURN FLOOR(TO_FLOAT(25000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption)) 	BREAK
				CASE WHEEL_ACCS_OPTION_LGT			RETURN g_sMPTunables.iTUNER_MODS_JUNKYARD_TIRES 								BREAK
				CASE WHEEL_ACCS_DESIGN_STOCK		RETURN FLOOR(TO_FLOAT(200) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_1		RETURN FLOOR(TO_FLOAT(5000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_2		RETURN g_sMPTunables.iCar_Mods_Lowrider_1_Bennys_Tire_Design_White_Lines        BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_3		RETURN g_sMPTunables.iCar_Mods_Lowrider_1_Bennys_Tire_Design_Classic_White_Wall	BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_4		RETURN g_sMPTunables.iCar_Mods_Lowrider_1_Bennys_Tire_Design_Retro_White_Wall  	BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_5		RETURN g_sMPTunables.iCar_Mods_Lowrider_1_Bennys_Tire_Design_Red_Lines         	BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_6		RETURN g_sMPTunables.iCar_Mods_Lowrider_1_Bennys_Tire_Design_Blue_Lines        	BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_7		RETURN g_sMPTunables.iCar_Mods_Lowrider_1_Bennys_Tire_Design_Atomic       		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_8		RETURN 12345
				CASE WHEEL_ACCS_DESIGN_CUSTOM_9		RETURN 12345
				CASE WHEEL_ACCS_SMOKE_WHITE			RETURN FLOOR(TO_FLOAT(1500) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE WHEEL_ACCS_SMOKE_BLACK			RETURN FLOOR(TO_FLOAT(5000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE WHEEL_ACCS_SMOKE_BLUE			RETURN FLOOR(TO_FLOAT(10000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))	BREAK
				CASE WHEEL_ACCS_SMOKE_YELLOW		RETURN FLOOR(TO_FLOAT(12500) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))	BREAK
				CASE WHEEL_ACCS_SMOKE_PURPLE		RETURN FLOOR(TO_FLOAT(14000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))	BREAK // Business 1 - Purple tyre smoke
				CASE WHEEL_ACCS_SMOKE_ORANGE		RETURN FLOOR(TO_FLOAT(15000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))	BREAK
				CASE WHEEL_ACCS_SMOKE_GREEN			RETURN FLOOR(TO_FLOAT(17500) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))	BREAK // Business 2 - Green tyre smoke
				CASE WHEEL_ACCS_SMOKE_RED			RETURN FLOOR(TO_FLOAT(20000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))	BREAK
				CASE WHEEL_ACCS_SMOKE_PINK			RETURN g_sMPTunables.iDLC_hipster_modifier_Pink_Tire_Smoke  					BREAK // = 22500				// CAR MODS
				CASE WHEEL_ACCS_SMOKE_BROWN			RETURN g_sMPTunables.iDLC_hipster_modifier_Brown_Tire_Smoke 					BREAK // = 16500				// CAR MODS
				CASE WHEEL_ACCS_SMOKE_INDI			RETURN g_sMPTunables.iVehiclekit_IndependenceDay_Patriot_Tire_Smoke 			BREAK // Indi - red, white, and blue
				CASE WHEEL_ACCS_SMOKE_CREW		
					#IF FEATURE_TUNER
					IF SHOULD_DISCOUNT_TUNER_AUTO_SHOP_MODS()	RETURN 0	ENDIF
					#ENDIF
					RETURN FLOOR(TO_FLOAT(25000) * GET_WHEEL_ACCS_EXPENDITURE_TUNEABLE(iOption))									BREAK				
			ENDSWITCH
		BREAK
		CASE CMM_SPAREWHEEL
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(6000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_SPOILER_1		
						ELSE
							RETURN FLOOR(TO_FLOAT(7050) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption)  * fBuyNowMultiplier)			
						ENDIF
					BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(10000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(13000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(14000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(16000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(16500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(17000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(17500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(18000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(18500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(19000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(19500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(20000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(20500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(21000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(21500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(22750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(24000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(25250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(26500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT(27750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(29000) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(30250) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(31500) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(32750) * GET_SPOILER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_WHEELS
			SWITCH iOption
				CASE 0	RETURN 1600		BREAK // Car
				CASE 1	RETURN 1600		BREAK // Bike
			ENDSWITCH
		BREAK
		CASE CMM_WHEEL_COL
			SWITCH iOption
				CASE 0	RETURN 1500		BREAK // Car
				CASE 1	RETURN 800		BREAK // Bike
			ENDSWITCH
		BREAK
		CASE CMM_WINDOWS
			SWITCH iOption
				CASE 0	RETURN FLOOR(TO_FLOAT(500) * GET_WINDOWS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 1	RETURN FLOOR(TO_FLOAT(1500) * GET_WINDOWS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 2	RETURN FLOOR(TO_FLOAT(3500) * GET_WINDOWS_EXPENDITURE_TUNEABLE(iOption))		BREAK
				CASE 3	RETURN FLOOR(TO_FLOAT(5000) * GET_WINDOWS_EXPENDITURE_TUNEABLE(iOption))	BREAK
				CASE 4	RETURN FLOOR(TO_FLOAT(6000) * GET_WINDOWS_EXPENDITURE_TUNEABLE(iOption))	BREAK // Tinted - not used
				
				CASE 5	RETURN g_sMPTunables.iDLC_hipster_modifier_Pure_Black BREAK // = 10000				// VEHICLES
			ENDSWITCH
		BREAK

		CASE CMM_FENDERS
			IF vehicleModel = APC
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_APC_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF vehicleModel = DUNE3
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_DUNE3_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF vehicleModel = HALFTRACK
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_HALFTRACK_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF vehicleModel = TAMPA3
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TAMPA3_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF vehicleModel = INSURGENT3
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_INSURGENT3_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF vehicleModel = TECHNICAL3
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TECHNICAL3_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			
			ELIF g_eModPriceVariation = MPV_GR_LIGHT
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(99000) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GR_HEAVY
				SWITCH iOption
					CASE 22	RETURN FLOOR(TO_FLOAT(99000) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SMUG_STANDARD
				iOption = iOption- 21
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_STANDARD_BOMBS		BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_INCENDIARY_BOMBS		BREAK
					CASE 3	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_GAS_BOMBS			BREAK
					CASE 4	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_CLUSTER_BOMBS		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SMUG_HEAVY
				iOption = iOption- 21
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_STANDARD_BOMBS		BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_INCENDIARY_BOMBS	BREAK
					CASE 3	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_GAS_BOMBS			BREAK
					CASE 4	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_CLUSTER_BOMBS		BREAK
				ENDSWITCH
			ELIF  g_eModPriceVariation = MPV_ARENA_WARS
				IF iOption > 20
					iOption = iOption- 21
					SWITCH iOption
						CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_MINE) * GET_R_WING_EXPENDITURE_TUNEABLE(0))		BREAK
						CASE 1	
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_R_WING_EXPENDITURE_TUNEABLE(1))
							ELSE
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_1) * GET_R_WING_EXPENDITURE_TUNEABLE(1))
							ENDIF	
						BREAK
						
						CASE 2	
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_R_WING_EXPENDITURE_TUNEABLE(2))		
							ELSE
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_2) * GET_R_WING_EXPENDITURE_TUNEABLE(2))	
							ENDIF	
						BREAK
						CASE 3	
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_R_WING_EXPENDITURE_TUNEABLE(3))
							ELSE
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_3) * GET_R_WING_EXPENDITURE_TUNEABLE(3))
							ENDIF	
						BREAK
						CASE 4	
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_4) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_R_WING_EXPENDITURE_TUNEABLE(4))		
							ELSE
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_4) * GET_R_WING_EXPENDITURE_TUNEABLE(4))
							ENDIF	
						BREAK
						CASE 5	
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_5) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_R_WING_EXPENDITURE_TUNEABLE(5))		
							ELSE
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_MINE_5) * GET_R_WING_EXPENDITURE_TUNEABLE(5))		
							ENDIF	
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iOption
						CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK	
						CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
						CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					ENDSWITCH	
				ENDIF
			ELIF  g_eModPriceVariation = MPV_RC_BANDITO
				iOption = iOption- 21
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_NO_MINE) * GET_R_WING_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_KINETIC_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_EMP_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(2))		BREAK
				ENDSWITCH
			ELIF USE_CUSTOM_CAM_FOR_BIKER_MOD(g_ModVehicle)
			AND vehicleModel != FCR2
			AND vehicleModel != DIABLOUS2
				iOption = iOption- 21
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_R_WING_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_R_WING_EXPENDITURE_TUNEABLE(1))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_R_WING_EXPENDITURE_TUNEABLE(2))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_R_WING_EXPENDITURE_TUNEABLE(3))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_R_WING_EXPENDITURE_TUNEABLE(4))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_R_WING_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_R_WING_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_R_WING_EXPENDITURE_TUNEABLE(6))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_R_WING_EXPENDITURE_TUNEABLE(7))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_R_WING_EXPENDITURE_TUNEABLE(8))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_R_WING_EXPENDITURE_TUNEABLE(9))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_R_WING_EXPENDITURE_TUNEABLE(10))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_R_WING_EXPENDITURE_TUNEABLE(11))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_R_WING_EXPENDITURE_TUNEABLE(12))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_R_WING_EXPENDITURE_TUNEABLE(13))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_R_WING_EXPENDITURE_TUNEABLE(14))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_R_WING_EXPENDITURE_TUNEABLE(15))		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GANG_OPS_STANDARD
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_GR_BIKE
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK	
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 21	RETURN FLOOR(TO_FLOAT(4500) * GET_R_WING_EXPENDITURE_TUNEABLE(0))			BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(99000) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(120000) * GET_R_WING_EXPENDITURE_TUNEABLE(2))			BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(145000) * GET_R_WING_EXPENDITURE_TUNEABLE(3))			BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(169000) * GET_R_WING_EXPENDITURE_TUNEABLE(4))			BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(200000) * GET_R_WING_EXPENDITURE_TUNEABLE(5))			BREAK
				ENDSWITCH	
			ELIF g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK	
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 21	RETURN FLOOR(TO_FLOAT(4500) * GET_R_WING_EXPENDITURE_TUNEABLE(0))			BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_SHARED_PROXIMITY_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(120000) * GET_R_WING_EXPENDITURE_TUNEABLE(2))			BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(145000) * GET_R_WING_EXPENDITURE_TUNEABLE(3))			BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(169000) * GET_R_WING_EXPENDITURE_TUNEABLE(4))			BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(200000) * GET_R_WING_EXPENDITURE_TUNEABLE(5))			BREAK
				ENDSWITCH		
			ELSE	
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK	
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					
					CASE 21	RETURN FLOOR(TO_FLOAT(300) * GET_R_WING_EXPENDITURE_TUNEABLE(0))		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT(900) * GET_R_WING_EXPENDITURE_TUNEABLE(1))		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT(1500) * GET_R_WING_EXPENDITURE_TUNEABLE(2))		BREAK
					CASE 24	RETURN FLOOR(TO_FLOAT(1700) * GET_R_WING_EXPENDITURE_TUNEABLE(3))		BREAK
					CASE 25	RETURN FLOOR(TO_FLOAT(2000) * GET_R_WING_EXPENDITURE_TUNEABLE(4))		BREAK
					CASE 26	RETURN FLOOR(TO_FLOAT(2750) * GET_R_WING_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 27	RETURN FLOOR(TO_FLOAT(3300) * GET_R_WING_EXPENDITURE_TUNEABLE(5))		BREAK
					CASE 28	RETURN FLOOR(TO_FLOAT(3900) * GET_R_WING_EXPENDITURE_TUNEABLE(6))		BREAK
					CASE 29	RETURN FLOOR(TO_FLOAT(4350) * GET_R_WING_EXPENDITURE_TUNEABLE(7))		BREAK
					CASE 30	RETURN FLOOR(TO_FLOAT(5000) * GET_R_WING_EXPENDITURE_TUNEABLE(8))		BREAK
					CASE 31	RETURN FLOOR(TO_FLOAT(5150) * GET_R_WING_EXPENDITURE_TUNEABLE(9))		BREAK
					CASE 32	RETURN FLOOR(TO_FLOAT(5300) * GET_R_WING_EXPENDITURE_TUNEABLE(10))		BREAK
					CASE 33	RETURN FLOOR(TO_FLOAT(5450) * GET_R_WING_EXPENDITURE_TUNEABLE(11))		BREAK
					CASE 34	RETURN FLOOR(TO_FLOAT(5600) * GET_R_WING_EXPENDITURE_TUNEABLE(12))		BREAK
					CASE 35	RETURN FLOOR(TO_FLOAT(5750) * GET_R_WING_EXPENDITURE_TUNEABLE(13))		BREAK
					CASE 36	RETURN FLOOR(TO_FLOAT(5900) * GET_R_WING_EXPENDITURE_TUNEABLE(14))		BREAK
					CASE 37	RETURN FLOOR(TO_FLOAT(6050) * GET_R_WING_EXPENDITURE_TUNEABLE(15))		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_FENDERSTWO
			IF g_eModPriceVariation = MPV_BATTLE_STANDARD
			OR g_eModPriceVariation = MPV_BATTLE_HEAVY
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(4500) * GET_R_WING_EXPENDITURE_TUNEABLE(0))			BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iBB_WEAPON_MOD_SHARED_PROXIMITY_MINES) * GET_R_WING_EXPENDITURE_TUNEABLE(1))			BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(120000) * GET_R_WING_EXPENDITURE_TUNEABLE(2))			BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(145000) * GET_R_WING_EXPENDITURE_TUNEABLE(3))			BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(169000) * GET_R_WING_EXPENDITURE_TUNEABLE(4))			BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(200000) * GET_R_WING_EXPENDITURE_TUNEABLE(5))			BREAK
				ENDSWITCH
			ELIF  g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_NO_BOMB) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_REMOTE_BOMB) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH	
			ELSE	
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
				ENDSWITCH
			ENDIF	
		BREAK
		
		CASE CMM_FRONTMUDGUARD
			IF vehicleModel = TAMPA3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(3500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TAMPA3_FRONT_MISSILE_LAUNCHERS) * GET_RBUMBER_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GR_LIGHT
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(5000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(85000) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SMUG_STANDARD	
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_CHAFF			BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_DECOY_FLARES		BREAK
					
					CASE WHEEL_ACCS_SMOKE_WHITE		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLACK		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLUE		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_GREEN		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_RED		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PINK		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_BROWN		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_INDI		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_CREW		RETURN g_sMPTunables.iSMUG_MODS_STANDARD_PRICE_SMOKE 	BREAK				
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SMUG_HEAVY	
				SWITCH iOption
					CASE 1	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_CHAFF			BREAK
					CASE 2	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_DECOY_FLARES		BREAK
					
					CASE WHEEL_ACCS_SMOKE_WHITE		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLACK		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_BLUE		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_YELLOW	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PURPLE	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_ORANGE	RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_GREEN		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_RED		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK
					CASE WHEEL_ACCS_SMOKE_PINK		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_BROWN		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_INDI		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK 
					CASE WHEEL_ACCS_SMOKE_CREW		RETURN g_sMPTunables.iSMUG_MODS_HEAVY_PRICE_SMOKE 	BREAK	
				ENDSWITCH
			ELIF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_ARENA_WARS
			OR g_eModPriceVariation = MPV_SPECIAL
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(2200) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_FRONT_BUMPER_1
						ELSE
							RETURN FLOOR(TO_FLOAT(4600) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)			
						ENDIF
					BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(7400) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT( 11700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT( 14500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT( 14700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT( 14900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT( 15100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT( 15300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT( 15500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT( 15700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT( 15900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT( 16100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT( 16300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT( 16500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT( 16700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT( 16900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT( 17100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT( 17300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT( 17500) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT( 17700) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 21	RETURN FLOOR(TO_FLOAT( 17900) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 22	RETURN FLOOR(TO_FLOAT( 18100) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
					CASE 23	RETURN FLOOR(TO_FLOAT( 18300) * GET_FBUMBER_EXPENDITURE_TUNEABLE(iOption) * fBuyNowMultiplier)		BREAK
				ENDSWITCH
			
			ENDIF
		BREAK
		
		CASE CMM_REARMUDGUARD
			IF vehicleModel = TAMPA3
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(3500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(g_sMPTunables.iGR_UPGRADE_TAMPA3_REAR_FIRING_MORTAR) * GET_RBUMBER_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_GR_LIGHT
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(3500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 1	RETURN FLOOR(TO_FLOAT(92500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(iOption))			BREAK
				ENDSWITCH
			ELIF (g_eModPriceVariation = MPV_SPECIAL)
			OR g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(2200) * GET_RBUMBER_EXPENDITURE_TUNEABLE(0))				BREAK
					CASE 1	
						IF	IS_VEHICLE_MODEL(g_ModVehicle, CHEBUREK)
							RETURN g_sMPTunables.iASSAULT_VEHICLES_MODS_CHEBUREK_CARDBOARD_REAR_BUMPER_1				
						ELSE
							IF bBuyNowPrice
								RETURN FLOOR(TO_FLOAT(4600) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_RBUMBER_EXPENDITURE_TUNEABLE(1))
							ELSE	
								RETURN FLOOR(TO_FLOAT(4600) * GET_RBUMBER_EXPENDITURE_TUNEABLE(1))				
							ENDIF	
						ENDIF
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(7400) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_RBUMBER_EXPENDITURE_TUNEABLE(2))
						ELSE	
							RETURN FLOOR(TO_FLOAT(7400) * GET_RBUMBER_EXPENDITURE_TUNEABLE(2))				
						ENDIF
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(11700) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_RBUMBER_EXPENDITURE_TUNEABLE(3))
						ELSE	
							RETURN FLOOR(TO_FLOAT(11700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(3))				
						ENDIF
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(14500) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))
						ELSE	
							RETURN FLOOR(TO_FLOAT(14500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(4))				
						ENDIF
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(14700) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))
						ELSE	
							RETURN FLOOR(TO_FLOAT(14700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(5))				
						ENDIF
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(14900) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))
						ELSE	
							RETURN FLOOR(TO_FLOAT(14900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(6))				
						ENDIF
					BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT( 15100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(7))			BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT( 15300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(8))			BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT( 15500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(9))			BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT( 15700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(10))			BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT( 15900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(11))			BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT( 16100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(12))			BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT( 16300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(13))			BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT( 16500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(14))			BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT( 16700) * GET_RBUMBER_EXPENDITURE_TUNEABLE(15))			BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT( 16900) * GET_RBUMBER_EXPENDITURE_TUNEABLE(16))			BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT( 17100) * GET_RBUMBER_EXPENDITURE_TUNEABLE(17))			BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT( 17300) * GET_RBUMBER_EXPENDITURE_TUNEABLE(18))			BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT( 17500) * GET_RBUMBER_EXPENDITURE_TUNEABLE(19))			BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	
	SWITCH eMenu
		CASE CMM_SUPERMOD_PLTHOLDER
		CASE CMM_SUPERMOD_PLTVANITY
		CASE CMM_SUPERMOD_INTERIOR3
		CASE CMM_DIALS
		CASE CMM_SUPERMOD_INTERIOR4
		CASE CMM_SUPERMOD_INTERIOR5
		CASE CMM_SUPERMOD_SEATS
		CASE CMM_SUPERMOD_STEERING
		CASE CMM_SUPERMOD_PLAQUE
		CASE CMM_SUPERMOD_TRUNK
		CASE CMM_SUPERMOD_ENGINEBAY1
		CASE CMM_SUPERMOD_DOOR_L
		CASE CMM_SUPERMOD_DOOR_R	
			RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu))  * fBuyNowMultiplier * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
		BREAK
		CASE CMM_SUPERMOD_KNOB
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
				IF bSellValue
					SWITCH iOption
						CASE 1 			RETURN 50000
						CASE 2 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_SINGLE
						CASE 3 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_TWIN
						CASE 4			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_QUAD
					ENDSWITCH
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
						SWITCH GET_HASH_KEY(sLabel)
							CASE HASH("HSW_VEHMODT1") 			RETURN 50000
							CASE HASH("HSW_VEHMODT2") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_SINGLE
							CASE HASH("HSW_VEHMODT3") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_TWIN
							CASE HASH("HSW_VEHMODT4") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_QUAD
							
							CASE HASH("HSW_VEHMODET1") 			RETURN 50000
							CASE HASH("HSW_VEHMODET2") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_SINGLE
							CASE HASH("HSW_VEHMODET3") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_TWIN
							CASE HASH("HSW_VEHMODET4") 			RETURN g_sMPTunables.iGEN9_MODS_HSW_TURBO_QUAD
						ENDSWITCH
					ENDIF	
				ENDIF	
			ENDIF
			#ENDIF
			RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu))  * fBuyNowMultiplier * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
		CASE CMM_SUPERMOD_ICE
			#IF FEATURE_GEN9_EXCLUSIVE
			SWITCH vehicleModel
				CASE BRIOSO					RETURN g_sMPTunables.iGEN9_HSW_PRICE_BRIOSO
				CASE SENTINEL				RETURN g_sMPTunables.iGEN9_HSW_PRICE_SENTINEL
				CASE HAKUCHOU2				RETURN g_sMPTunables.iGEN9_HSW_PRICE_HAKOUCHOU2
				CASE TURISMO2				RETURN g_sMPTunables.iGEN9_HSW_PRICE_TURISMO2
				CASE DEVESTE				RETURN g_sMPTunables.iGEN9_HSW_PRICE_DEVESTE
				CASE BANSHEE				RETURN g_sMPTunables.iGEN9_HSW_PRICE_BANSHEE
				CASE ARBITERGT				RETURN g_sMPTunables.iGEN9_HSW_PRICE_ARBITERGT
				CASE ASTRON2				RETURN g_sMPTunables.iGEN9_HSW_PRICE_ASTRON2
				CASE CYCLONE2				RETURN g_sMPTunables.iGEN9_HSW_PRICE_CYCLONE2
				CASE IGNUS2					RETURN g_sMPTunables.iGEN9_HSW_PRICE_IGNUS2
				CASE S95					RETURN g_sMPTunables.iGEN9_HSW_PRICE_S95
				#IF FEATURE_DLC_1_2022
				CASE VIGERO2				RETURN g_sMPTunables.iGEN9_HSW_PRICE_VIGERO2
				CASE FELTZER3				RETURN g_sMPTunables.iGEN9_HSW_PRICE_FELTZER3
				#ENDIF
			ENDSWITCH
			#ENDIF
			RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu))  * fBuyNowMultiplier * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
		BREAK
		CASE CMM_TRIM
			IF IS_VEHICLE_A_CAR_MEET_VEHICLE(vehicleModel)
			OR DOES_VEHICLE_USE_INTERIOR1_AS_SUNSTRIPS(vehicleModel)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_R_WING_EXPENDITURE_TUNEABLE(0))		
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_R_WING_EXPENDITURE_TUNEABLE(1))		
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_R_WING_EXPENDITURE_TUNEABLE(2))		
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_R_WING_EXPENDITURE_TUNEABLE(3))
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_R_WING_EXPENDITURE_TUNEABLE(4))
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_R_WING_EXPENDITURE_TUNEABLE(5))
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_R_WING_EXPENDITURE_TUNEABLE(5))
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_R_WING_EXPENDITURE_TUNEABLE(6))
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_R_WING_EXPENDITURE_TUNEABLE(7))
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_R_WING_EXPENDITURE_TUNEABLE(8))
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_R_WING_EXPENDITURE_TUNEABLE(9))
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_R_WING_EXPENDITURE_TUNEABLE(10))
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_R_WING_EXPENDITURE_TUNEABLE(11))
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_R_WING_EXPENDITURE_TUNEABLE(12))
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_R_WING_EXPENDITURE_TUNEABLE(13))
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_R_WING_EXPENDITURE_TUNEABLE(14))
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_R_WING_EXPENDITURE_TUNEABLE(15))
				
				ENDSWITCH
			ENDIF	
				
			RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu))  * fBuyNowMultiplier * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
		BREAK	
		CASE CMM_SUPERMOD_INTERIOR1
			IF vehicleModel = KANJO
			OR IS_VEHICLE_A_CAR_MEET_VEHICLE(vehicleModel)
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_R_WING_EXPENDITURE_TUNEABLE(0))		
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_R_WING_EXPENDITURE_TUNEABLE(1))		
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_R_WING_EXPENDITURE_TUNEABLE(2))		
				
				ENDSWITCH
			ELSE	
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu))  * fBuyNowMultiplier * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))		
			ENDIF	
		BREAK
		CASE CMM_SUPERMOD_CHASSIS2
			IF g_eModPriceVariation = MPV_ARENA_WARS
			#IF FEATURE_DLC_1_2022
			AND vehicleModel != GREENWOOD
			#ENDIF
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_RAM) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_1) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_2) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_3) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))	
						ENDIF	
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_4) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_4) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_5) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_5) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ENDIF	
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_6) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_6) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ENDIF	
					BREAK
					CASE 7
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_7) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RAM_7) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
			ENDIF
		BREAK
		
		CASE CMM_SUPERMOD_CHASSIS4
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_BLADES) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BLADES_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							#IF FEATURE_FIXER
							IF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehicleModel)
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iFIXER_MODS_REMOTE_CONTROL_UNIT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
							ENDIF
							#ENDIF
							
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BLADES_1) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BLADES_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							#IF FEATURE_FIXER
							IF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehicleModel)
								RETURN FLOOR(TO_FLOAT(g_sMPTunables.iFIXER_MODS_MISSILE_LOCKON_JAMMER) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
							ENDIF
							#ENDIF
							
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BLADES_2) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BLADES_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BLADES_3) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))	
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
			ENDIF
		BREAK
		
		CASE CMM_SUPERMOD_CHASSIS5
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_GUNS) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_GUNS_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_GUNS_1) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_GUNS_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_GUNS_2) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
			ENDIF
		BREAK
	
		CASE CMM_SUPERMOD_CHASSIS3
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_SPIKES) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SPIKES_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SPIKES_1) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SPIKES_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SPIKES_2) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SPIKES_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SPIKES_3) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
			ENDIF
		BREAK
		
		CASE CMM_SUPERMOD_ENGINEBAY2
			IF g_eModPriceVariation = MPV_ARENA_WARS
			#IF FEATURE_DLC_1_2022
			AND vehicleModel != GREENWOOD
			#ENDIF
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_BOOST) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BOOST_UPGRADE_20_PERCENT)  * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BOOST_UPGRADE_20_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BOOST_UPGRADE_60_PERCENT)  * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BOOST_UPGRADE_60_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BOOST_UPGRADE_100_PERCENT)  * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_BOOST_UPGRADE_100_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 4
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SHUNT_BOOST) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_SHUNT_BOOST) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
			ENDIF
		BREAK
		
		CASE CMM_SUPERMOD_ENGINEBAY3
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_JUMP) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_JUMP_UPGRADE_20_PERCENT) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_JUMP_UPGRADE_20_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_JUMP_UPGRADE_60_PERCENT) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_JUMP_UPGRADE_60_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_JUMP_UPGRADE_100_PERCENT) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_JUMP_UPGRADE_100_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_RC_BANDITO
				SWITCH iOption
				 	CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_NO_JUMP) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))	BREAK
					CASE 1 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_JUMP_UPGRADE_20_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))	BREAK
					CASE 2 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_JUMP_UPGRADE_60_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))	BREAK
					CASE 3 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_RC_BANDITO_JUMP_UPGRADE_100_PERCENT) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))	BREAK
				ENDSWITCH	
			ELSE
				RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
			ENDIF
		BREAK
		
		// Ornaments
		CASE CMM_SUPERMOD_INTERIOR2
			IF g_eModPriceVariation = MPV_ARENA_WARS
				SWITCH iOption
					CASE 0  RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_NO_ROLL_CAGE) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))		BREAK
					CASE 1	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_1) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_1) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 2	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_2) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_2) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 3	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_3) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_3) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 4	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_4) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_4) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 5	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_5) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_5) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 6	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_6) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_6) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 7	
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_7) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_7) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 8
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_8) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_8) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 9
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_9) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_9) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
					CASE 10
						IF bBuyNowPrice
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_10) * g_sMPTunables.fAW_VEHICLE_MODS_BUY_IT_NOW_PRICE_MULTIPLIER * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ELSE
							RETURN FLOOR(TO_FLOAT(g_sMPTunables.iAW_SALE_PRICE_MODS_ROLL_CAGE_10) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption, FALSE))
						ENDIF	
					BREAK
				ENDSWITCH
			ELIF vehicleModel = GAUNTLET5
			OR IS_VEHICLE_A_CAR_MEET_VEHICLE(vehicleModel)
				// headlight cover
				SWITCH iOption
					CASE 0	RETURN FLOOR(TO_FLOAT(300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK	
					CASE 1	RETURN FLOOR(TO_FLOAT(900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 2	RETURN FLOOR(TO_FLOAT(1500) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 3	RETURN FLOOR(TO_FLOAT(1700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 4	RETURN FLOOR(TO_FLOAT(2000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 5	RETURN FLOOR(TO_FLOAT(2750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 6	RETURN FLOOR(TO_FLOAT(3300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 7	RETURN FLOOR(TO_FLOAT(3900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 8	RETURN FLOOR(TO_FLOAT(4350) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 9	RETURN FLOOR(TO_FLOAT(5000) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 10	RETURN FLOOR(TO_FLOAT(5150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 11	RETURN FLOOR(TO_FLOAT(5300) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 12	RETURN FLOOR(TO_FLOAT(5450) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 13	RETURN FLOOR(TO_FLOAT(5600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 14	RETURN FLOOR(TO_FLOAT(5750) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 15	RETURN FLOOR(TO_FLOAT(5900) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 16	RETURN FLOOR(TO_FLOAT(6050) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 17	RETURN FLOOR(TO_FLOAT(6600) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 18	RETURN FLOOR(TO_FLOAT(7150) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 19	RETURN FLOOR(TO_FLOAT(7700) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
					CASE 20	RETURN FLOOR(TO_FLOAT(8250) * GET_L_WING_EXPENDITURE_TUNEABLE(iOption))		BREAK
				ENDSWITCH	
			ELSE
				IF iOption = 0
					RETURN 0
				ENDIF
				IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
					SWITCH GET_HASH_KEY(sLabel)
						CASE HASH("BOBBLE_BRD1") 	RETURN g_sMPTunables.iHalloween_2015_Brunette_Corpse_Bride_Bobblehead BREAK //Brunette Corpse Bride Bobblehead
						CASE HASH("BOBBLE_BRD2") 	RETURN g_sMPTunables.iHalloween_2015_White_Corpse_Bride_Bobblehead BREAK //White Corpse Bride Bobblehead
						CASE HASH("BOBBLE_BRD3") 	RETURN g_sMPTunables.iHalloween_2015_Pink_Corpse_Bride_Bobblehead BREAK //Pink Corpse Bride Bobblehead
						CASE HASH("BOBBLE_SLH1") 	RETURN g_sMPTunables.iHalloween_2015_White_Mask_Slasher_Bobblehead BREAK //White Mask Slasher Bobblehead
						CASE HASH("BOBBLE_SLH2") 	RETURN g_sMPTunables.iHalloween_2015_Red_Mask_Slasher_Bobblehead BREAK //Red Mask Slasher Bobblehead
						CASE HASH("BOBBLE_SLH3") 	RETURN g_sMPTunables.iHalloween_2015_Yellow_Mask_Slasher_Bobblehead BREAK //Yellow Mask Slasher Bobblehead
						CASE HASH("BOBBLE_ZOM1") 	RETURN g_sMPTunables.iHalloween_2015_Blue_Zombie_Bobblehead BREAK //Blue Zombie Bobblehead
						CASE HASH("BOBBLE_ZOM2") 	RETURN g_sMPTunables.iHalloween_2015_Green_Zombie_Bobblehead BREAK //Green Zombie Bobblehead
						CASE HASH("BOBBLE_ZOM3") 	RETURN g_sMPTunables.iHalloween_2015_Pale_Zombie_Bobblehead BREAK //Pale Zombie Bobblehead
						CASE HASH("BOBBLE_GC1") 	RETURN g_sMPTunables.iHalloween_2015_Possessed_Urchin_Bobblehead BREAK //Possessed Urchin Bobblehead
						CASE HASH("BOBBLE_GC2") 	RETURN g_sMPTunables.iHalloween_2015_Demonic_Urchin_Bobblehead BREAK //Demonic Urchin Bobblehead
						CASE HASH("BOBBLE_GC3") 	RETURN g_sMPTunables.iHalloween_2015_Gruesome_Urchin_Bobblehead BREAK //Gruesome Urchin Bobblehead
						CASE HASH("BOBBLE_FRNK1") 	RETURN g_sMPTunables.iHalloween_2015_Tuxedo_Frank_Bobblehead BREAK //Tuxedo Frank Bobblehead
						CASE HASH("BOBBLE_FRNK2") 	RETURN g_sMPTunables.iHalloween_2015_Purple_Suit_Frank_Bobblehead BREAK //Purple Suit Frank Bobblehead
						CASE HASH("BOBBLE_FRNK3")	RETURN g_sMPTunables.iHalloween_2015_Striped_Suit_Frank_Bobblehead BREAK //Striped Suit Frank Bobblehead
						CASE HASH("BOBBLE_MUM1")	RETURN g_sMPTunables.iHalloween_2015_Black_Mummy_Bobblehead BREAK //Black Mummy Bobblehead
						CASE HASH("BOBBLE_MUM2")	RETURN g_sMPTunables.iHalloween_2015_White_Mummy_Bobblehead BREAK //White Mummy Bobblehead
						CASE HASH("BOBBLE_MUM3") 	RETURN g_sMPTunables.iHalloween_2015_Brown_Mummy_Bobblehead BREAK //Brown Mummy Bobblehead
						CASE HASH("BOBBLE_WOLF1") 	RETURN g_sMPTunables.iHalloween_2015_Pale_Werewolf_Bobblehead BREAK //Pale Werewolf Bobblehead
						CASE HASH("BOBBLE_WOLF2") 	RETURN g_sMPTunables.iHalloween_2015_Dark_Werewolf_Bobblehead BREAK //Dark Werewolf Bobblehead
						CASE HASH("BOBBLE_WOLF3") 	RETURN g_sMPTunables.iHalloween_2015_Grey_Werewolf_Bobblehead BREAK //Gray Werewolf Bobblehead
						CASE HASH("BOBBLE_VAMP1") 	RETURN g_sMPTunables.iHalloween_2015_Fleshy_Vampire_Bobblehead BREAK //Fleshy Vampire Bobblehead
						CASE HASH("BOBBLE_VAMP2") 	RETURN g_sMPTunables.iHalloween_2015_Bloody_Vampire_Bobblehead BREAK //Bloody Vampire Bobblehead
						CASE HASH("BOBBLE_VAMP3") 	RETURN g_sMPTunables.iHalloween_2015_BandW_Vampire_Bobblehead BREAK //B&W Vampire Bobblehead
						
						CASE HASH("BOBBLE_MAR1") 	RETURN g_sMPTunables.iCar_Mods_Black_Mariachi_Bobblehead BREAK //Black Mariachi Bobblehead
						CASE HASH("BOBBLE_MAR2") 	RETURN g_sMPTunables.iCar_Mods_Green_Mariachi_Bobblehead BREAK //Green Mariachi Bobblehead
						CASE HASH("BOBBLE_MAR3") 	RETURN g_sMPTunables.iCar_Mods_Blue_Mariachi_Bobblehead BREAK //Blue Mariachi Bobblehead
						CASE HASH("BOBBLE_BAT1") 	RETURN g_sMPTunables.iCar_Mods_Boars_Bobblehead BREAK //Boars Bobblehead
						CASE HASH("BOBBLE_BAT2") 	RETURN g_sMPTunables.iCar_Mods_Corkers_Bobblehead BREAK //Corkers Bobblehead
						CASE HASH("BOBBLE_BAT3") 	RETURN g_sMPTunables.iCar_Mods_Feud_Bobblehead BREAK //Feud Bobblehead
						CASE HASH("BOBBLE_BSK1") 	RETURN g_sMPTunables.iCar_Mods_Panic_Bobblehead BREAK //Panic Bobblehead
						CASE HASH("BOBBLE_BSK2") 	RETURN g_sMPTunables.iCar_Mods_Salamanders_Bobblehead BREAK //Salamanders Bobblehead
						CASE HASH("BOBBLE_BSK3") 	RETURN g_sMPTunables.iCar_Mods_Liberty_Bobblehead BREAK //Liberty Bobblehead
						CASE HASH("BOBBLE_BSK4") 	RETURN g_sMPTunables.iCar_Mods_LS_Shrimps_Bobblehead BREAK //LS Shrimps Bobblehead
						CASE HASH("BOBBLE_OG1") 	RETURN g_sMPTunables.iCar_Mods_Magnetics_Bobblehead BREAK //Magnetics Bobblehead
						CASE HASH("BOBBLE_OG2") 	RETURN g_sMPTunables.iCar_Mods_LSC_Bobblehead BREAK //LSC Bobblehead
						CASE HASH("BOBBLE_OG3") 	RETURN g_sMPTunables.iCar_Mods_OG_Bobblehead BREAK //OG Bobblehead
						CASE HASH("BOBBLE_FOOT1") 	RETURN g_sMPTunables.iCar_Mods_LC_Wrath_Bobblehead BREAK //LC Wrath Bobblehead
						CASE HASH("BOBBLE_FOOT2") 	RETURN g_sMPTunables.iCar_Mods_Pounders_Bobblehead BREAK //Pounders Bobblehead
						CASE HASH("BOBBLE_FOOT3") 	RETURN g_sMPTunables.iCar_Mods_Bookworms_Bobblehead BREAK //Bookworms Bobblehead
						CASE HASH("BOBBLE_IMP1") 	RETURN g_sMPTunables.iCar_Mods_Impotent_Rage_Bobblehead BREAK //Impotent Rage Bobblehead
						CASE HASH("BOBBLE_IMP2") 	RETURN g_sMPTunables.iCar_Mods_Vintage_Impotent_Rage_Bobblehead BREAK //Vintage Impotent Rage Bobblehead
						CASE HASH("BOBBLE_POGO1") 	RETURN g_sMPTunables.iCar_Mods_White_Pogo_Bobblehead BREAK //White Pogo Bobblehead
						CASE HASH("BOBBLE_POGO2") 	RETURN g_sMPTunables.iCar_Mods_Red_Pogo_Bobblehead BREAK //Red Pogo Bobblehead
						CASE HASH("BOBBLE_REAP") 	RETURN g_sMPTunables.iCar_Mods_Grim_Reaper_Bobblehead BREAK //Grim Reaper Bobblehead
						DEFAULT
							SWITCH iOption
								CASE 1 	RETURN g_sMPTunables.iCar_Mods_Black_Mariachi_Bobblehead BREAK //Black Mariachi Bobblehead
								CASE 2 	RETURN g_sMPTunables.iCar_Mods_Green_Mariachi_Bobblehead BREAK //Green Mariachi Bobblehead
								CASE 3 	RETURN g_sMPTunables.iCar_Mods_Blue_Mariachi_Bobblehead BREAK //Blue Mariachi Bobblehead
								CASE 4 	RETURN g_sMPTunables.iCar_Mods_Boars_Bobblehead BREAK //Boars Bobblehead
								CASE 5 	RETURN g_sMPTunables.iCar_Mods_Corkers_Bobblehead BREAK //Corkers Bobblehead
								CASE 6 	RETURN g_sMPTunables.iCar_Mods_Feud_Bobblehead BREAK //Feud Bobblehead
								CASE 7 	RETURN g_sMPTunables.iCar_Mods_Panic_Bobblehead BREAK //Panic Bobblehead
							ENDSWITCH
						BREAK
					ENDSWITCH
				ENDIF
				RETURN 12345
			ENDIF	
		BREAK
		
		// Hydraluics
		CASE CMM_SUPERMOD_HYDRO	
			SWITCH iOption
				CASE 0 	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Hydraulics BREAK
				CASE 1 	RETURN g_sMPTunables.iSupermod_Slot_Hydraulics_1st_Mod 	BREAK
				CASE 2 	RETURN g_sMPTunables.iSupermod_Slot_Hydraulics_2nd_Mod 	BREAK
				CASE 3 	RETURN g_sMPTunables.iSupermod_Slot_Hydraulics_3rd_Mod 	BREAK
				CASE 4 	RETURN g_sMPTunables.iSupermod_Slot_Hydraulics_4th_Mod 	BREAK
				CASE 5 	RETURN g_sMPTunables.iSupermod_Slot_Hydraulics_5th_Mod 	BREAK
				CASE 6 	RETURN g_sMPTunables.iCar_Mods_Supermod_Slot_Hydraulics_6th_Mod 	BREAK
				DEFAULT RETURN g_sMPTunables.iSupermod_Slot_Hydraulics_5th_Mod 	BREAK
			ENDSWITCH
		BREAK
		
		// Liveries
		CASE CMM_SUPERMOD_LIVERY
			SWITCH vehicleModel
				CASE TRAILERLARGE
					SWITCH iOption
						CASE 0 RETURN 0 BREAK
						CASE 1 RETURN g_sMPTunables.iGR_MOC_LIVERIES_STARS_AND_STRIPES_PRICE BREAK
						CASE 2 RETURN g_sMPTunables.iGR_MOC_LIVERIES_EAGLE_CLAW_PRICE     BREAK
						CASE 3 RETURN g_sMPTunables.iGR_MOC_LIVERIES_EAGLE_CLAW_FLAG_PRICE  BREAK
						CASE 4 RETURN g_sMPTunables.iGR_MOC_LIVERIES_FIGHTING_FREEDOM_PRICE BREAK
					ENDSWITCH
				BREAK
				CASE LURCHER
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPTunables.iCar_Mods_Halloween_2015_Lurcher_Livery_Hangmans_Grave BREAK
						CASE 2 RETURN g_sMPTunables.iCar_Mods_Halloween_2015_Lurcher_Livery_The_Ripper BREAK
					ENDSWITCH
				BREAK
				CASE BTYPE2
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPTunables.iCar_Mods_Halloween_2015_Franken_Stange_Livery_Midnight_Potion BREAK
						CASE 2 RETURN g_sMPTunables.iCar_Mods_Halloween_2015_Franken_Stange_Livery_Hells_Furnace BREAK
						CASE 3 RETURN g_sMPTunables.iCar_Mods_Halloween_2015_Franken_Stange_Livery_Blazing_Death BREAK
						CASE 4 RETURN g_sMPTunables.iCar_Mods_Halloween_2015_Franken_Stange_Livery_Spider_Trap BREAK
					ENDSWITCH
				BREAK
				CASE FACTION2
					IF iOption = 10 RETURN g_sMPTunables.ilowrider_faction2_livery_special10 ENDIF
				BREAK
				CASE OMNIS
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Obey_Omnis   BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Obey_Omnis   BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE BF400
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Nagasaki_BF400 BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Nagasaki_BF400 BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE TROPOS
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Lampadati_Tropos_Rallye BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Lampadati_Tropos_Rallye BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE BRIOSO
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Grotti_Brioso_RA 	BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Grotti_Brioso_RA    BREAK
						DEFAULT 
							#IF FEATURE_GEN9_EXCLUSIVE								
							IF bSellValue
								// HSW_LIV2
								IF GET_VEHICLE_MOD(g_ModVehicle, MOD_LIVERY) = 3 
									RETURN g_sMPTunables.iGEN9_MODS_HSW_ELITE_LIVERY
								// HSW_LIV1
								ELIF GET_VEHICLE_MOD(g_ModVehicle, MOD_LIVERY) = 2
									RETURN g_sMPTunables.iGEN9_MODS_HSW_RACING_LIVERY
								ENDIF
							ELSE	
								IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
									SWITCH GET_HASH_KEY(sLabel)
										CASE HASH("HSW_LIV1")	RETURN g_sMPTunables.iGEN9_MODS_HSW_RACING_LIVERY 
										CASE HASH("HSW_LIV2")	RETURN g_sMPTunables.iGEN9_MODS_HSW_ELITE_LIVERY 	
									ENDSWITCH	
								ENDIF	
							ENDIF	
							#ENDIF
							RETURN 999	
						BREAK
					ENDSWITCH
				BREAK
				CASE TROPHYTRUCK
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Vapid_Trophy_Truck    BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Vapid_Trophy_Truck    BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE TROPHYTRUCK2
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Vapid_Desert_Raid    BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Vapid_Desert_Raid    BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE CLIFFHANGER
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Western_Cliffhanger     BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Western_Cliffhanger     BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE TAMPA2
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN  g_sMPtunables.iSTUNT_Livery_1_Decalsse_Drift_Tampa BREAK 
						CASE 2 RETURN  g_sMPtunables.iSTUNT_Livery_2_Decalsse_Drift_Tampa BREAK 
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE GARGOYLE
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Western_Bike_O_Death BREAK
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Western_Bike_O_Death BREAK
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK	
				CASE LE7B
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Annis_RE7B  
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Annis_RE7B  
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK	
				CASE TYRUS
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Progen_Tyrus  BREAK 
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Progen_Tyrus  BREAK 
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE LYNX
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Ocelot_Lynx  BREAK 
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Ocelot_Lynx  BREAK 
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
				CASE SHEAVA
					SWITCH iOption
						CASE 0 RETURN FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption)) BREAK
						CASE 1 RETURN g_sMPtunables.iSTUNT_Livery_1_Emperor_Sheava  BREAK 
						CASE 2 RETURN g_sMPtunables.iSTUNT_Livery_2_Emperor_Sheava  BREAK 
						DEFAULT RETURN 999	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
				IF bSellValue
					IF vehicleModel = DEVESTE
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_LIVERY) = 11
							RETURN g_sMPTunables.iGEN9_MODS_HSW_ELITE_LIVERY
						ELIF GET_VEHICLE_MOD(g_ModVehicle, MOD_LIVERY) = 10
							RETURN g_sMPTunables.iGEN9_MODS_HSW_RACING_LIVERY
						ENDIF
					ELSE
						IF GET_VEHICLE_MOD(g_ModVehicle, MOD_LIVERY) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_LIVERY)
							RETURN g_sMPTunables.iGEN9_MODS_HSW_ELITE_LIVERY
						ELIF GET_VEHICLE_MOD(g_ModVehicle, MOD_LIVERY) + 1 = GET_NUM_VEHICLE_MODS(g_ModVehicle, MOD_LIVERY) - 1
							RETURN g_sMPTunables.iGEN9_MODS_HSW_RACING_LIVERY
						ENDIF
					ENDIF
				ELSE	
					IF NOT IS_STRING_NULL_OR_EMPTY(sLabel)
						SWITCH GET_HASH_KEY(sLabel)
							CASE HASH("HSW_LIV1")	RETURN g_sMPTunables.iGEN9_MODS_HSW_RACING_LIVERY 
							CASE HASH("HSW_LIV2")	RETURN g_sMPTunables.iGEN9_MODS_HSW_ELITE_LIVERY 
							DEFAULT
								RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu, bBuyNowPrice)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
							BREAK	
						ENDSWITCH	
					ENDIF	
				ENDIF	
			ENDIF
			#ENDIF
			
			// Defaults
			RETURN FLOOR(TO_FLOAT(GET_SUPERMOD_STOCK_PRICE_TUNABLE(eMenu, bBuyNowPrice)) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iOption))
		BREAK
		
		// Upgrades
		CASE CMM_SUPERMOD
		CASE CMM_SUPERMOD_TWO
			IF IS_MODEL_VALID(vehicleModel)
				IF  IS_VEHICLE_MODEL_SAFE_FOR_SUPERMOD_GARAGE(vehicleModel)
					RETURN GET_SUPERMOD_UPGRADE_COST(GET_VEHICLE_SUPERMOD_MODEL(vehicleModel, iOption))
				ENDIF
			ELSE 
				PRINTLN("Vehicle model is not valid!")
			ENDIF	
		BREAK
	ENDSWITCH
	
	
	RETURN 700
ENDFUNC


FUNC INT GET_VEHICLE_LIVERY_COST(VEHICLE_INDEX vehID, INT iLivery)
	INT iLiveryCost = 575+(50*iLivery)
	
	SWITCH GET_ENTITY_MODEL(vehID)
		CASE BATI2
			iLiveryCost = 575+(50*iLivery)
		BREAK
		CASE SANCHEZ
			iLiveryCost = 575+(50*iLivery)
		BREAK
		CASE PARADISE
			iLiveryCost = 575+(50*iLivery)
		BREAK
		CASE WINDSOR

#IF NOT IS_NEXTGEN_BUILD
			IF iLivery = 0
				iLiveryCost = 100
				IF g_sMPtunables.iluxe1_car_mods_no_livery >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_no_livery	
				ENDIF
			ELIF iLivery = 1
				iLiveryCost = 100000	//Sessanta Nove Monogram|$100,000
				IF g_sMPtunables.iluxe1_car_mods_sessanta_nove_monogram >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_sessanta_nove_monogram	
				ENDIF
			ELIF iLivery = 2
				iLiveryCost = 90000		//Sessanta Nove Multi-Color|$90,000
				IF g_sMPtunables.iluxe1_car_mods_sessanta_nove_multi_color >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_sessanta_nove_multi_color	
				ENDIF
			ELIF iLivery = 3
				iLiveryCost = 80000		//Sessanta Nove Geometric|$80,000
				IF g_sMPtunables.iluxe1_car_mods_sessanta_nove_geometric >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_sessanta_nove_geometric	
				ENDIF
			ELIF iLivery = 4
				iLiveryCost = 75000		//Perseus Wings Monogram|$75,000
				IF g_sMPtunables.iluxe1_car_mods_perseus_wings_monogram >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_perseus_wings_monogram	
				ENDIF
//			ELIF iLivery = 5
//				iLiveryCost = 70000		//Perseus Green Wings Monogram|$70,000
//				IF g_sMPtunables.iluxe1_car_mods_perseus_green_wings_monogram >= 0
//					iLiveryCost = g_sMPtunables.iluxe1_car_mods_perseus_green_wings_monogram	
//				ENDIF
			ELIF iLivery = 5
				iLiveryCost = 60000		//Santo Capra Python|$60,000
				IF g_sMPtunables.iluxe1_car_mods_santo_capra_python >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_santo_capra_python	
				ENDIF
			ELIF iLivery = 6
				iLiveryCost = 55000		//Santo Capra Cheetah|$55,000
				IF g_sMPtunables.iluxe1_car_mods_santo_capra_cheetah >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_santo_capra_cheetah	
				ENDIF
			ELIF iLivery = 7
				iLiveryCost = 50000		//Yeti Mall Ninja|$50,000
				IF g_sMPtunables.iluxe1_car_mods_yeti_mall_ninja >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_yeti_mall_ninja	
				ENDIF
			ENDIF
#ENDIF
#IF IS_NEXTGEN_BUILD
			IF iLivery = 0
				iLiveryCost = 100
				IF g_sMPtunables.iluxe1_car_mods_no_livery >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_no_livery	
				ENDIF
			ELIF iLivery = 1
				iLiveryCost = 100000	//Sessanta Nove Monogram|$100,000
				IF g_sMPtunables.iluxe1_car_mods_sessanta_nove_monogram >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_sessanta_nove_monogram	
				ENDIF
			ELIF iLivery = 2
				iLiveryCost = 90000		//Sessanta Nove Multi-Color|$90,000
				IF g_sMPtunables.iluxe1_car_mods_sessanta_nove_multi_color >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_sessanta_nove_multi_color	
				ENDIF
			ELIF iLivery = 3
				iLiveryCost = 80000		//Sessanta Nove Geometric|$80,000
				IF g_sMPtunables.iluxe1_car_mods_sessanta_nove_geometric >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_sessanta_nove_geometric	
				ENDIF
			ELIF iLivery = 4
				iLiveryCost = 75000		//Perseus Wings Monogram|$75,000
				IF g_sMPtunables.iluxe1_car_mods_perseus_wings_monogram >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_perseus_wings_monogram	
				ENDIF
			ELIF iLivery = 5
				iLiveryCost = 70000		//Perseus Green Wings Monogram|$70,000
				IF g_sMPtunables.iluxe1_car_mods_perseus_green_wings_monogram >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_perseus_green_wings_monogram	
				ENDIF
			ELIF iLivery = 6
				iLiveryCost = 60000		//Santo Capra Python|$60,000
				IF g_sMPtunables.iluxe1_car_mods_santo_capra_python >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_santo_capra_python	
				ENDIF
			ELIF iLivery = 7
				iLiveryCost = 55000		//Santo Capra Cheetah|$55,000
				IF g_sMPtunables.iluxe1_car_mods_santo_capra_cheetah >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_santo_capra_cheetah	
				ENDIF
			ELIF iLivery = 8
				iLiveryCost = 50000		//Yeti Mall Ninja|$50,000
				IF g_sMPtunables.iluxe1_car_mods_yeti_mall_ninja >= 0
					iLiveryCost = g_sMPtunables.iluxe1_car_mods_yeti_mall_ninja	
				ENDIF
			ENDIF
#ENDIF
		BREAK
	ENDSWITCH
	RETURN FLOOR(TO_FLOAT(iLiveryCost) * g_sMPTunableGroups.flivery_group_modifier)
ENDFUNC

FUNC INT GET_VEHICLE_LIVERY2_COST(VEHICLE_INDEX vehID, INT iLivery)

	INT iLiveryCost = 12345
	
	CDEBUG1LN(DEBUG_SHOPS, "GET_VEHICLE_LIVERY2_COST iLivery ", iLivery)
	
	SWITCH GET_ENTITY_MODEL(vehID)
		CASE TORNADO5
			iLiveryCost = FLOOR(TO_FLOAT(g_sMPTunables.iCar_Mods_Supermod_Slot_Stock_Livery) * GET_SUPERMOD_EXPENDITURE_TUNEABLE(iLivery) * g_sMPTunables.fCar_Mods_Roof_Livery_Design_Multiplier)
			CDEBUG1LN(DEBUG_SHOPS, "GET_VEHICLE_LIVERY2_COST TORNADO5 iLiveryCost ", iLiveryCost)
		BREAK
	ENDSWITCH
	RETURN iLiveryCost
ENDFUNC

FUNC INT GET_SP_CARMOD_MENU_OPTION_COST(CARMOD_MENU_ENUM eMenu, INT iOption, INT iStage = 0)
	SWITCH eMenu
		CASE CMM_ARMOUR
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 500		BREAK
				CASE 2	RETURN 1250		BREAK
				CASE 3	RETURN 2000		BREAK
				CASE 4	RETURN 3500		BREAK
				CASE 5	RETURN 5000		BREAK
			ENDSWITCH
		BREAK
		
		CASE CMM_BRAKES
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 2000		BREAK
				CASE 2	RETURN 2700		BREAK
				CASE 3	RETURN 3500		BREAK
				CASE 4	RETURN 5000		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_BULLBARS
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 5000		BREAK
				CASE 2	RETURN 6000		BREAK
				CASE 3	RETURN 7000		BREAK
				CASE 4	RETURN 8000		BREAK
				CASE 5	RETURN 9000		BREAK
				CASE 6	RETURN 1000		BREAK
				CASE 7	RETURN 1100		BREAK
				CASE 8	RETURN 1200		BREAK
				CASE 9	RETURN 1300		BREAK
				CASE 10	RETURN 1400		BREAK
				CASE 11	RETURN 1500		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ROLLCAGE
			SWITCH iOption
				CASE 0	RETURN 50		BREAK
				CASE 1	RETURN 3500		BREAK
				CASE 2	RETURN 3975		BREAK
				CASE 3	RETURN 4250		BREAK
				CASE 4	RETURN 5000		BREAK
				CASE 5	RETURN 5500		BREAK
				CASE 6	RETURN 5975		BREAK
				CASE 7	RETURN 6750		BREAK
				CASE 8	RETURN 7000		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_EXPLOSIVES
			SWITCH iOption
				CASE 0	RETURN 500	BREAK 	// REMOTE BOMB
				CASE 1	RETURN 500	BREAK	// IGNITION BOMB
			ENDSWITCH
		BREAK
		CASE CMM_HOOD
			IF g_eModPriceVariation = MPV_SPECIAL
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
				SWITCH iOption
					CASE 0	RETURN 800		BREAK
					CASE 1	RETURN 1500		BREAK
					CASE 2	RETURN 2500		BREAK
					CASE 3	RETURN 4000		BREAK
					CASE 4	RETURN 4800		BREAK
					CASE 5	RETURN 5500		BREAK
					CASE 6	RETURN 5800		BREAK
					CASE 7	RETURN 6000		BREAK
					CASE 8	RETURN 6200		BREAK
					CASE 9	RETURN 6200		BREAK
					CASE 10	RETURN 6250		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN 450		BREAK
					CASE 1	RETURN 850		BREAK
					CASE 2	RETURN 1200		BREAK
					CASE 3	RETURN 1450		BREAK
					CASE 4	RETURN 2225		BREAK
					CASE 5	RETURN 2500		BREAK
					CASE 6	RETURN 2800		BREAK
					CASE 7	RETURN 3000		BREAK
					CASE 8	RETURN 3250		BREAK
					CASE 9	RETURN 3500		BREAK
					CASE 10	RETURN 3600		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
				SWITCH iOption
					CASE 0	RETURN 150		BREAK
					CASE 1	RETURN 325		BREAK
					CASE 2	RETURN 750		BREAK
					CASE 3	RETURN 875		BREAK
					CASE 4	RETURN 1000		BREAK
					CASE 5	RETURN 1200		BREAK
					CASE 6	RETURN 1450		BREAK
					CASE 7	RETURN 2225		BREAK
					CASE 8	RETURN 2500		BREAK
					CASE 9	RETURN 2800		BREAK
					CASE 10	RETURN 3000		BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN 100		BREAK
					CASE 1	RETURN 200		BREAK
					CASE 2	RETURN 350		BREAK
					CASE 3	RETURN 449		BREAK
					CASE 4	RETURN 625		BREAK
					CASE 5	RETURN 875		BREAK
					CASE 6	RETURN 1000		BREAK
					CASE 7	RETURN 1200		BREAK
					CASE 8	RETURN 1450		BREAK
					CASE 9	RETURN 2225		BREAK
					CASE 10	RETURN 2500		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_BUMPERS
			IF g_eModPriceVariation = MPV_SPECIAL
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
				SWITCH iOption
					CASE 0	RETURN 1100		BREAK
					CASE 1	RETURN 2300		BREAK
					CASE 2	RETURN 3700		BREAK
					CASE 3	RETURN 5850		BREAK
					CASE 4	RETURN 7250		BREAK
					CASE 5	RETURN 7350		BREAK
					CASE 6	RETURN 7450		BREAK
					CASE 7	RETURN 7550		BREAK
					CASE 8	RETURN 7650		BREAK
					
					CASE 20	RETURN 1100		BREAK
					CASE 21	RETURN 2300		BREAK
					CASE 22	RETURN 3700		BREAK
					CASE 23	RETURN 5850		BREAK
					CASE 24	RETURN 7250		BREAK
					CASE 25	RETURN 7350		BREAK
					CASE 26	RETURN 7450		BREAK
					CASE 27	RETURN 7550		BREAK
					CASE 28	RETURN 7650		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN 750		BREAK
					CASE 1	RETURN 1250		BREAK
					CASE 2	RETURN 1650		BREAK
					CASE 3	RETURN 1950		BREAK
					CASE 4	RETURN 2350		BREAK
					CASE 5	RETURN 2450		BREAK
					CASE 6	RETURN 2550		BREAK
					CASE 7	RETURN 2650		BREAK
					CASE 8	RETURN 2750		BREAK
					
					CASE 20	RETURN 750		BREAK
					CASE 21	RETURN 1250		BREAK
					CASE 22	RETURN 1650		BREAK
					CASE 23	RETURN 1950		BREAK
					CASE 24	RETURN 2350		BREAK
					CASE 25	RETURN 2450		BREAK
					CASE 26	RETURN 2550		BREAK
					CASE 27	RETURN 2650		BREAK
					CASE 28	RETURN 2750		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
				SWITCH iOption
					CASE 0	RETURN 250		BREAK
					CASE 1	RETURN 500		BREAK
					CASE 2	RETURN 750		BREAK
					CASE 3	RETURN 1000		BREAK
					CASE 4	RETURN 1300		BREAK
					CASE 5	RETURN 1400		BREAK
					CASE 6	RETURN 1500		BREAK
					CASE 7	RETURN 1600		BREAK
					CASE 8	RETURN 1700		BREAK
					
					CASE 20	RETURN 250		BREAK
					CASE 21	RETURN 500		BREAK
					CASE 22	RETURN 750		BREAK
					CASE 23	RETURN 1000		BREAK
					CASE 24	RETURN 1300		BREAK
					CASE 25	RETURN 1400		BREAK
					CASE 26	RETURN 1500		BREAK
					CASE 27	RETURN 1600		BREAK
					CASE 28	RETURN 1700		BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN 149		BREAK
					CASE 1	RETURN 195		BREAK
					CASE 2	RETURN 250		BREAK
					CASE 3	RETURN 375		BREAK
					CASE 4	RETURN 500		BREAK
					CASE 5	RETURN 600		BREAK
					CASE 6	RETURN 700		BREAK
					CASE 7	RETURN 800		BREAK
					CASE 8	RETURN 900		BREAK
								   
					CASE 20	RETURN 149		BREAK
					CASE 21	RETURN 195		BREAK
					CASE 22	RETURN 250		BREAK
					CASE 23	RETURN 375		BREAK
					CASE 24	RETURN 500		BREAK
					CASE 25	RETURN 600		BREAK
					CASE 26	RETURN 700		BREAK
					CASE 27	RETURN 800		BREAK
					CASE 28	RETURN 900		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_CHASSIS
			SWITCH iOption
				CASE 0	RETURN 350		BREAK
				CASE 1	RETURN 550		BREAK
				CASE 2	RETURN 675		BREAK
				CASE 3	RETURN 750		BREAK
				CASE 4	RETURN 1375		BREAK
				CASE 5	RETURN 2300		BREAK
				CASE 6	RETURN 2450		BREAK
				CASE 7	RETURN 2900		BREAK
				CASE 8	RETURN 3500		BREAK
				CASE 9	RETURN 4000		BREAK
			ENDSWITCH
		BREAK
		
		CASE CMM_ENGINE
			SWITCH iOption
				CASE 0	RETURN 500		BREAK
				CASE 1	RETURN 900		BREAK
				CASE 2	RETURN 1250		BREAK
				CASE 3	RETURN 1800		BREAK
				CASE 4	RETURN 3350		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_EXHAUST
			SWITCH iOption
				CASE 0	RETURN 130		BREAK
				CASE 1	RETURN 375		BREAK
				CASE 2	RETURN 899		BREAK
				CASE 3	RETURN 1499		BREAK
				CASE 4	RETURN 4770		BREAK
				CASE 5	RETURN 4870		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_TRANSMISSION
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 2950		BREAK
				CASE 2	RETURN 3250		BREAK
				CASE 3	RETURN 4000		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_GRILL
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 375		BREAK
				CASE 2	RETURN 670		BREAK
				CASE 3	RETURN 825		BREAK
				CASE 4	RETURN 1500		BREAK
				CASE 5	RETURN 1600		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_HORN
		
			CARMOD_HORN_ENUM eHorn
			eHorn = INT_TO_ENUM(CARMOD_HORN_ENUM, iOption)
			SWITCH eHorn
				CASE HORN_STOCK							RETURN 50 BREAK
				
				CASE HORN_INDEP_1						RETURN 1500		BREAK
				CASE HORN_INDEP_2						RETURN 1500		BREAK
				CASE HORN_INDEP_3						RETURN 1500		BREAK
				CASE HORN_INDEP_4						RETURN 1500		BREAK
				
				CASE HORN_HIPSTER_1						RETURN 1500		BREAK
				CASE HORN_HIPSTER_2						RETURN 1500		BREAK
				CASE HORN_HIPSTER_3						RETURN 1500		BREAK
				CASE HORN_HIPSTER_4						RETURN 1500		BREAK
				
				CASE HORN_BUSINESS2_SCALE_C0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_D0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_E0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_F0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_G0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_A0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_B0			RETURN 1500		BREAK
				CASE HORN_BUSINESS2_SCALE_C1			RETURN 1500		BREAK
			
				CASE HORN_BUSINESS1_1					RETURN 600		BREAK
				CASE HORN_BUSINESS1_2					RETURN 700		BREAK
				CASE HORN_BUSINESS1_3					RETURN 800		BREAK
				CASE HORN_BUSINESS1_4					RETURN 1000		BREAK
				CASE HORN_BUSINESS1_5					RETURN 1200		BREAK
				CASE HORN_BUSINESS1_6					RETURN 1300		BREAK
				CASE HORN_BUSINESS1_7					RETURN 1400		BREAK
				
													//	B* 2370691 - Amended the price for SP in accordance
//				CASE HORN_LUXE_1_PREVIEW				RETURN 1700		BREAK	//	to fit with the varied prices found in the business1
//				CASE HORN_LUXE_2_PREVIEW				RETURN 2000		BREAK	//	pack, & not the static prices found in the hipster &
//				CASE HORN_LUXE_3_PREVIEW				RETURN 2200		BREAK	//	indep packs. Speak to RowanJ or Kenneth if needs amending.
				CASE HORN_LUXE_1_FULL 					RETURN 1700		BREAK
				CASE HORN_LUXE_2_FULL 					RETURN 2000		BREAK
				CASE HORN_LUXE_3_FULL 					RETURN 2200		BREAK
				
				CASE HORN_TRUCK							RETURN 60		BREAK
				CASE HORN_COP							RETURN 75  		BREAK
				CASE HORN_CLOWN							RETURN 150 		BREAK
				CASE HORN_MUSICAL_1						RETURN 255 		BREAK
				CASE HORN_MUSICAL_2						RETURN 300 		BREAK
				CASE HORN_MUSICAL_3						RETURN 325 		BREAK
				CASE HORN_MUSICAL_4						RETURN 375 		BREAK
				CASE HORN_MUSICAL_5						RETURN 500 		BREAK
				CASE HORN_SAD_TROMBONE					RETURN 600 		BREAK
				
				
			ENDSWITCH
		BREAK
		CASE CMM_LIGHTS_HEAD
			IF g_eModPriceVariation = MPV_SPECIAL
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
				SWITCH iOption
					CASE 0	RETURN 300		BREAK
					CASE 1	RETURN 1450		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN 200		BREAK
					CASE 1	RETURN 1000		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
				SWITCH iOption
					CASE 0	RETURN 100		BREAK
					CASE 1	RETURN 300		BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN 100		BREAK
					CASE 1	RETURN 100		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_LIGHTS_NEON
			IF iStage = 0 // Neon Layout
				SWITCH iOption
					CASE 0 	RETURN 100	BREAK // None
					CASE 1 	RETURN 1000	BREAK // Front
					CASE 2 	RETURN 1000	BREAK // Back
					CASE 3 	RETURN 1250 BREAK // Sides
					CASE 4 	RETURN 1800	BREAK // Front and Back
					CASE 5 	RETURN 2000	BREAK // Front and Sides
					CASE 6 	RETURN 2000	BREAK // Back and Sides
					CASE 7 	RETURN 3000	BREAK // Front, Back and Sides
				ENDSWITCH
			ELIF iStage = 1 // Neon Colour
				SWITCH iOption
					CASE 0 	RETURN 650		BREAK // White
					CASE 1 	RETURN 650		BREAK // Blue
					CASE 2 	RETURN 650		BREAK // Eclectic blue
					CASE 3 	RETURN 650		BREAK // Mint green
					CASE 4 	RETURN 650		BREAK // Lime Green
					CASE 5 	RETURN 650		BREAK // Yellow
					CASE 6 	RETURN 650		BREAK // Golden shower
					CASE 7 	RETURN 650		BREAK // Orange
					CASE 8 	RETURN 650		BREAK // Red
					CASE 9 	RETURN 650		BREAK // Pony pink
					CASE 10	RETURN 650		BREAK // Hot Pink
					CASE 11	RETURN 650		BREAK // Purple
					CASE 12	RETURN 650		BREAK // Blacklight
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_PLATES
			SWITCH iOption
				CASE 0	RETURN 50		BREAK
				CASE 1	RETURN 50		BREAK
				CASE 2	RETURN 50		BREAK
				CASE 3	RETURN 75		BREAK
				CASE 4	RETURN 150		BREAK
				CASE 10	RETURN 600		BREAK
				DEFAULT	RETURN 600		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_ROOF
			IF GET_ENTITY_MODEL(g_ModVehicle) = DUKES
				CDEBUG1LN(DEBUG_SHOPS, "GET_SP_CARMOD_MENU_OPTION_COST(DUKES, CMM_ROOF, ", iOption, ")")
				
				SWITCH iOption
					CASE 0	RETURN 100		BREAK
					CASE 1	RETURN 350		BREAK
					DEFAULT
						RETURN 2500
					BREAK
				ENDSWITCH
			ELSE
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 350		BREAK
				CASE 2	RETURN 575		BREAK
				CASE 3	RETURN 800		BREAK
				CASE 4	RETURN 975		BREAK
				CASE 5	RETURN 1200		BREAK
					DEFAULT
						RETURN 1500
					BREAK
			ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_SKIRTS
			IF g_eModPriceVariation = MPV_SPECIAL
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
				SWITCH iOption
					CASE 0	RETURN 1250		BREAK
					CASE 1	RETURN 2750		BREAK
					CASE 2	RETURN 4000		BREAK
					CASE 3	RETURN 5250		BREAK
					CASE 4	RETURN 7000		BREAK
					CASE 5	RETURN 7350		BREAK
					CASE 6	RETURN 7600		BREAK
					CASE 7	RETURN 7900		BREAK
					CASE 8	RETURN 8300		BREAK
					CASE 9	RETURN 8500		BREAK
					CASE 10	RETURN 9000		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN 1250		BREAK
					CASE 1	RETURN 1500		BREAK
					CASE 2	RETURN 2000		BREAK
					CASE 3	RETURN 2750		BREAK
					CASE 4	RETURN 3900		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
				SWITCH iOption
					CASE 0	RETURN 300		BREAK
					CASE 1	RETURN 415		BREAK
					CASE 2	RETURN 500		BREAK
					CASE 3	RETURN 750		BREAK
					CASE 4	RETURN 1000		BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN 300		BREAK
					CASE 1	RETURN 415		BREAK
					CASE 2	RETURN 500		BREAK
					CASE 3	RETURN 750		BREAK
					CASE 4	RETURN 1000		BREAK
					CASE 5	RETURN 1250		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE CMM_SPOILER
			IF g_eModPriceVariation = MPV_SPECIAL
			OR g_eModPriceVariation = MPV_IE_HIGH
			OR g_eModPriceVariation = MPV_IE_RETRO
				SWITCH iOption
					CASE 0	RETURN 3000		BREAK
					CASE 1	RETURN 3750		BREAK
					CASE 2	RETURN 5000		BREAK
					CASE 3	RETURN 6500		BREAK
					CASE 4	RETURN 7000		BREAK
					CASE 5	RETURN 8000		BREAK
					CASE 6	RETURN 8500		BREAK
					CASE 7	RETURN 9000		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_SPORT
			OR g_eModPriceVariation = MPV_SUV
				SWITCH iOption
					CASE 0	RETURN 500		BREAK
					CASE 1	RETURN 750		BREAK
					CASE 2	RETURN 950		BREAK
					CASE 3	RETURN 1300		BREAK
					CASE 4	RETURN 1750		BREAK
					CASE 5	RETURN 2000		BREAK
					CASE 6	RETURN 2500		BREAK
					CASE 7	RETURN 3000		BREAK
				ENDSWITCH
			ELIF g_eModPriceVariation = MPV_STANDARD
			OR g_eModPriceVariation = MPV_IE_BIKE
				SWITCH iOption
					CASE 0	RETURN 150		BREAK
					CASE 1	RETURN 275		BREAK
					CASE 2	RETURN 350		BREAK
					CASE 3	RETURN 525		BREAK
					CASE 4	RETURN 750		BREAK
					CASE 5	RETURN 1000		BREAK
					CASE 6	RETURN 1250		BREAK
					CASE 7	RETURN 1500		BREAK
				ENDSWITCH
			ELSE
				SWITCH iOption
					CASE 0	RETURN 150		BREAK
					CASE 1	RETURN 275		BREAK
					CASE 2	RETURN 350		BREAK
					CASE 3	RETURN 525		BREAK
					CASE 4	RETURN 750		BREAK
					CASE 5	RETURN 1000		BREAK
					CASE 6	RETURN 1250		BREAK
					CASE 7	RETURN 1500		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE CMM_SUSPENSION
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 500		BREAK
				CASE 2	RETURN 1000		BREAK
				CASE 3	RETURN 1700		BREAK
				CASE 4	RETURN 2200		BREAK
				CASE 5	RETURN 2300		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_TURBO
			SWITCH iOption
				CASE 0	RETURN 500		BREAK
				CASE 1	RETURN 2500 	BREAK
			ENDSWITCH
		BREAK
		CASE CMM_WHEEL_ACCS
			SWITCH iOption
				CASE WHEEL_ACCS_OPTION_BPT			RETURN 4000 	BREAK
				CASE WHEEL_ACCS_DESIGN_STOCK		RETURN 100		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_1		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_2		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_3		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_4		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_5		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_6		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_7		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_8		RETURN 2500		BREAK
				CASE WHEEL_ACCS_DESIGN_CUSTOM_9		RETURN 2500		BREAK
				
				CASE WHEEL_ACCS_SMOKE_WHITE			RETURN 1000		BREAK
				CASE WHEEL_ACCS_SMOKE_BLACK			RETURN 1250		BREAK
				CASE WHEEL_ACCS_SMOKE_BLUE			RETURN 1500		BREAK
				CASE WHEEL_ACCS_SMOKE_YELLOW		RETURN 1750		BREAK
				CASE WHEEL_ACCS_SMOKE_PURPLE		RETURN 1900		BREAK
				CASE WHEEL_ACCS_SMOKE_ORANGE		RETURN 2000		BREAK
				CASE WHEEL_ACCS_SMOKE_GREEN			RETURN 2150		BREAK
				CASE WHEEL_ACCS_SMOKE_RED			RETURN 2250		BREAK
				CASE WHEEL_ACCS_SMOKE_PINK			RETURN 2500		BREAK
				CASE WHEEL_ACCS_SMOKE_BROWN			RETURN 2500		BREAK
				CASE WHEEL_ACCS_SMOKE_INDI			RETURN 2500		BREAK
			ENDSWITCH
		BREAK
		CASE CMM_WHEELS
			SWITCH iOption
				CASE 0	RETURN 1500		BREAK // Car
				CASE 1	RETURN 800		BREAK // Bike
			ENDSWITCH
		BREAK
		CASE CMM_WHEEL_COL
			SWITCH iOption
				CASE 0	RETURN 1500		BREAK // Car
				CASE 1	RETURN 800		BREAK // Bike
			ENDSWITCH
		BREAK
		CASE CMM_WINDOWS
			SWITCH iOption
				CASE 0	RETURN 100		BREAK
				CASE 1	RETURN 200		BREAK
				CASE 2	RETURN 450		BREAK
				CASE 3	RETURN 700		BREAK
				CASE 4	RETURN 900		BREAK // Tinted - not used
				CASE 5	RETURN 900		BREAK // Pure black - not used in NG
			ENDSWITCH
		BREAK
		CASE CMM_FENDERS
			SWITCH iOption
				CASE 0	RETURN 150		BREAK
				CASE 1	RETURN 450		BREAK
				CASE 2	RETURN 750		BREAK
				CASE 3	RETURN 850		BREAK
				CASE 4	RETURN 950		BREAK
				CASE 5	RETURN 1000		BREAK
				CASE 6	RETURN 1050		BREAK
				CASE 7	RETURN 1100		BREAK
				CASE 8	RETURN 1150		BREAK
				CASE 9	RETURN 1200		BREAK
				
				CASE 10	RETURN 150		BREAK
				CASE 11	RETURN 450		BREAK
				CASE 12	RETURN 750		BREAK
				CASE 13	RETURN 850		BREAK
				CASE 14	RETURN 950		BREAK
				CASE 15	RETURN 1000		BREAK
				CASE 16	RETURN 1050		BREAK
				CASE 17	RETURN 1100		BREAK
				CASE 18	RETURN 1150		BREAK
				CASE 19	RETURN 1200		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 350
ENDFUNC

FUNC INT GET_CARMOD_MENU_OPTION_COST(CARMOD_MENU_ENUM eMenu, INT iOption, INT iStage = 0, STRING sLabel = NULL, BOOL bBuyNowPrice = FALSE, BOOL bSellValue = FALSE)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// !!!
		// The call to UPDATE_ITEM_COST_FROM_CATALOGUE_DATA will override price when USE_SERVER_TRANSACTIONS() returns true. PC ONLY.
		// !!!
		INT iPrice = GET_MP_CARMOD_MENU_OPTION_COST(eMenu, iOption, iStage, sLabel, bBuyNowPrice, bSellValue)
		
		#IF FEATURE_TUNER
		IF SHOULD_DISCOUNT_TUNER_AUTO_SHOP_MODS()
		AND !bBuyNowPrice
		AND iPrice > 0
			iPrice = CEIL(iPrice * g_sMPTunables.fTUNER_AUTO_SHOP_OWNER_DISCOUNT_MULTIPLIER)
			PRINTLN("GET_CARMOD_MENU_OPTION_COST price after discount: ", iPrice , " menu: ", debug_GET_CARMOD_MENU_NAME(eMenu))
		ENDIF
		#ENDIF
		
		RETURN iPrice
	ENDIF
	
	RETURN GET_SP_CARMOD_MENU_OPTION_COST(eMenu, iOption, iStage)
ENDFUNC

FUNC BOOL HAS_BIKE_USING_OIL_TANK_FOR_VMT_BUMPER_R(VEHICLE_INDEX mVeh)
	IF IS_VEHICLE_DRIVEABLE(mVeh)
		IF (GET_ENTITY_MODEL(mVeh) = RATBIKE)
		OR (GET_ENTITY_MODEL(mVeh) = ZOMBIEA)
		OR (GET_ENTITY_MODEL(mVeh) = DAEMON2)
		OR (GET_ENTITY_MODEL(mVeh) = ZOMBIEB)
		OR (GET_ENTITY_MODEL(mVeh) = BLAZER4)
		OR (GET_ENTITY_MODEL(mVeh) = FAGGIO3)
		OR (GET_ENTITY_MODEL(mVeh) = WOLFSBANE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL DOES_VEHICLE_HAVE_BUMPER_ACCESSORY_MODS(VEHICLE_INDEX mVeh)
	IF IS_VEHICLE_DRIVEABLE(mVeh)
		IF (GET_ENTITY_MODEL(mVeh) = NERO2)
		OR (GET_ENTITY_MODEL(mVeh) = ELEGY)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_MINE_MOD_CONFLICT_WITH_FENDERS(VEHICLE_INDEX vehToCheck)
	SWITCH GET_ENTITY_MODEL(vehToCheck)
		CASE ZR380
		CASE ISSI4
		CASE SLAMVAN4
		CASE BRUISER
		CASE BRUTUS
		CASE ZR3802
		CASE ZR3803
		CASE ISSI5
		CASE SLAMVAN5
		CASE BRUISER2
		CASE BRUTUS2
		CASE ISSI6
		CASE SLAMVAN6
		CASE BRUISER3
		CASE BRUTUS3
		CASE OMNISEGT
			RETURN TRUE
		BREAK	
	
	ENDSWITCH
	RETURN FALSE
ENDFUNC 

//// PURPOSE:
 ///    
 /// PARAMS:
 ///    bCheckingForSellValue - only set true for when used for GET_MP_CARMOD_MENU_OPTION_COST
 /// RETURNS:
 ///    
FUNC CARMOD_MENU_ENUM GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_TYPE eModType, BOOL bCheckOverride = FALSE, BOOL bSkipModCheck = FALSE)
	
	TEXT_LABEL_15 tlModLabel
	INT iModNameHash
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(g_ModVehicle)
	
	IF HAS_VEHICLE_GOT_MOD(g_ModVehicle, eModType)
	OR bSkipModCheck
	
		IF bCheckOverride
			tlModLabel = GET_MOD_SLOT_NAME(g_ModVehicle, eModType)
			iModNameHash = GET_HASH_KEY(tlModLabel)

			SWITCH iModNameHash	
				CASE MOD_NAME_LIP_SPOILER		RETURN CMM_SUPERMOD_PLTHOLDER
				CASE MOD_NAME_VENTS				
					IF vehModel = GROWLER
					#IF FEATURE_DLC_1_2022
					OR vehModel = TORERO2
					#ENDIF
						RETURN CMM_SUPERMOD_DOOR_R 
					ELSE
						RETURN CMM_SUPERMOD_PLTVANITY 
					ENDIF
				BREAK	
				CASE MOD_NAME_VENT_BRACES		RETURN CMM_GRILL
				CASE MOD_NAME_HANDLEBARS		  RETURN CMM_HANDLEBARS BREAK 
				CASE MOD_NAME_TRUCKBED			  RETURN CMM_TRUCKBED BREAK
				CASE MOD_NAME_BODYWORK			  RETURN CMM_BODYWORK BREAK
				CASE MOD_NAME_BULLBARS			RETURN CMM_BULLBARS BREAK
				CASE MOD_NAME_BULLBARS2			RETURN CMM_BULLBARS BREAK
				CASE MOD_NAME_ROLLCAGE			  RETURN CMM_ROLLCAGE BREAK
				CASE MOD_NAME_ENGINEBAY			  RETURN CMM_ENGINEBAY	BREAK
				CASE MOD_NAME_HEADLIGHTCOV
				CASE MOD_NAME_FAIRING			  RETURN CMM_FAIRING	BREAK
				CASE MOD_NAME_FRONTFORKS		  RETURN CMM_FRONTFORKS	BREAK
				CASE MOD_NAME_FRAME				  RETURN CMM_FRAME	BREAK
				CASE MOD_NAME_SADDLEBAGS		  RETURN CMM_SADDLEBAGS	BREAK
				CASE MOD_NAME_HEADLIGHTS		  RETURN CMM_HEADLIGHTS	BREAK
				CASE MOD_NAME_MIRRORS			  RETURN CMM_MIRRORS	BREAK
				CASE MOD_NAME_INTERIORTYPE		  RETURN CMM_SUPERMOD_INTERIOR2	BREAK 
				CASE MOD_NAME_FBUMPER
					IF (IS_THIS_PERSONAL_CAR_MOD_IN_OFFICE(g_eModShopName,g_iPersonalCarModVar) OR g_eModShopName = CARMOD_SHOP_SUPERMOD)
					AND DOES_VEHICLE_HAVE_BUMPER_ACCESSORY_MODS(g_ModVehicle)
						RETURN CMM_FRONTMUDGUARD
					ELSE
						RETURN CMM_BUMPERS
					ENDIF
				BREAK	
				CASE MOD_NAME_LADDER			  RETURN CMM_HANDLEBARS		BREAK
				CASE MOD_NAME_FRONTMUD			  RETURN CMM_FRONTMUDGUARD BREAK
				CASE MOD_NAME_RACKS				
					IF vehModel = BARRAGE
						RETURN CMM_GRILL
					ELIF vehModel = CARACARA2
					OR vehModel = ZHABA
					#IF FEATURE_COPS_N_CROOKS
					OR vehModel = POLCARACARA
					#ENDIF
						RETURN CMM_SPOILER	
					ELIF vehModel = YOUGA3
						RETURN CMM_SUPERMOD_CHASSIS5
					ELSE	
						  RETURN CMM_REARMUDGUARD BREAK
					ENDIF
				BREAK
				CASE MOD_NAME_BACKRESTS			
					IF vehModel = REEVER
						RETURN CMM_SPOILER
					ELSE	
						RETURN CMM_REARMUDGUARD
					ENDIF		
				BREAK
				CASE MOD_NAME_OILTANK			
					IF vehModel = AVARUS
						RETURN CMM_GRILL 
					ELSE
						  RETURN CMM_REARMUDGUARD BREAK
					ENDIF
				BREAK
				CASE MOD_NAME_BUMPER_PANEL
					  RETURN CMM_FRONTMUDGUARD BREAK
				BREAK
				CASE MOD_NAME_SIDE_PANEL
					IF vehModel = THRAX
						RETURN CMM_SKIRTS
					ELSE
						  RETURN CMM_REARMUDGUARD BREAK
					ENDIF	
				BREAK
				CASE MOD_NAME_LOUVRES
					  RETURN CMM_REARMUDGUARD
				BREAK
				CASE MOD_NAME_SIDE_LOUVRES
					RETURN CMM_FENDERSTWO
				BREAK
				CASE MOD_NAME_ROOF_RAILS
					RETURN CMM_FENDERS
				BREAK	
				CASE MOD_NAME_DOORS
					RETURN CMM_FENDERS
				BREAK
				CASE MOD_NAME_TURRET_BRACKET
					RETURN CMM_SKIRTS
				BREAK	
				CASE MOD_NAME_STORAGE_BOX
					#IF FEATURE_HEIST_ISLAND
					IF vehModel = MANCHEZ2
						RETURN CMM_SPOILER 
					ELSE
					#ENDIF
						  RETURN CMM_PUSHBAR
					#IF FEATURE_HEIST_ISLAND
					ENDIF
					#ENDIF
				BREAK
				#IF FEATURE_COPS_N_CROOKS
				CASE MOD_NAME_CRASHBARS
					RETURN CMM_SUPERMOD_CHASSIS4
				BREAK
				#ENDIF
				CASE MOD_NAME_RBUMPER
					IF (IS_THIS_PERSONAL_CAR_MOD_IN_OFFICE(g_eModShopName,g_iPersonalCarModVar) OR g_eModShopName = CARMOD_SHOP_SUPERMOD)
					AND DOES_VEHICLE_HAVE_BUMPER_ACCESSORY_MODS(g_ModVehicle)
						  RETURN CMM_REARMUDGUARD BREAK
					ELIF vehModel = FUTO2
						RETURN CMM_SUPERMOD_PLTVANITY
					ELSE
						RETURN CMM_BUMPERS
					ENDIF
				BREAK	
				CASE MOD_NAME_INTERCOOLER		
					IF vehModel = PREVION
					OR vehModel = DOMINATOR7
					OR vehModel = ZR350
					OR vehModel = BUFFALO4
						RETURN CMM_GRILL
					ELSE	
						RETURN CMM_SUPERMOD_CHASSIS5
					ENDIF	
				BREAK	
				CASE MOD_NAME_REARMUD			  RETURN CMM_REARMUDGUARD BREAK
				CASE MOD_NAME_PLATEHOLDER		  RETURN CMM_PLATEHOLDER BREAK
				CASE MOD_NAME_PUSHBAR			  RETURN CMM_PUSHBAR	BREAK
				CASE MOD_NAME_FRONTSEAT			  RETURN CMM_FRONTSEAT	BREAK
				CASE MOD_NAME_REARSEAT			  RETURN CMM_REARSEAT	BREAK
				CASE MOD_NAME_SIDESTEP
					#IF FEATURE_FIXER
					IF vehModel = GRANGER2
						RETURN CMM_SKIRTS
					ENDIF
					#ENDIF
					RETURN CMM_SIDESTEP	
				BREAK
				CASE MOD_NAME_SPAREWHEEL		RETURN CMM_SPAREWHEEL BREAK
				CASE MOD_NAME_FUELTANK			  RETURN CMM_FUELTANK	BREAK
				CASE MOD_NAME_WHEELIEBAR		  RETURN CMM_WHEELIEBAR	BREAK
				CASE MOD_NAME_TAILGATE			  RETURN CMM_TAILGATE	BREAK
				CASE MOD_NAME_TRUNK_CATCHES
				CASE MOD_NAME_TRUNK				  RETURN CMM_TRUNK 	BREAK
				CASE MOD_NAME_DEFLECTORS 		  RETURN CMM_DEFLECTOR 	BREAK
				CASE MOD_NAME_ORNAMENTS			  RETURN CMM_ORNAMENTS	BREAK
				CASE MOD_NAME_SISSY				RETURN CMM_FENDERS BREAK
				CASE MOD_NAME_SHIFTER			
				CASE MOD_NAME_WINDSHEILD		
					IF vehModel = FORMULA
					OR vehModel = REEVER
						RETURN CMM_GRILL
					ELSE	
						RETURN CMM_FENDERSTWO
					ENDIF	
				BREAK
				CASE MOD_NAME_PANNIERS	
					#IF FEATURE_HEIST_ISLAND
					IF vehModel = MANCHEZ2
						RETURN CMM_ROOF BREAK
					ELSE
					#ENDIF
						RETURN CMM_SUPERMOD_CHASSIS3 BREAK
					#IF FEATURE_HEIST_ISLAND
					ENDIF
					#ENDIF
				BREAK	
				CASE MOD_NAME_REARANTENNA		RETURN CMM_ROOF BREAK
				CASE MOD_NAME_ANTENNA			RETURN CMM_SPOILER BREAK
				CASE MOD_NAME_ARCHCOVER			
					IF vehModel = BLAZER4
						RETURN CMM_ROOF
					ELIF vehModel = MICHELLI
					OR vehModel = ISSI3
					OR vehModel = S80
						RETURN CMM_FENDERS
					#IF FEATURE_TUNER
					ELIF vehModel = CYPHER
						RETURN CMM_SUPERMOD_CHASSIS2
					#ENDIF
					ELSE
						RETURN CMM_SKIRTS 
					ENDIF
				BREAK
				CASE MOD_NAME_AIRFILTER 		
					IF vehModel = BLAZER4
						RETURN CMM_GRILL
					ELIF vehModel = SWINGER
					OR vehModel = NEO
					OR vehModel = RROCKET
					OR vehModel = TAILGATER2
						RETURN CMM_CHASSIS
					ELIF vehModel = FUTO2
					OR vehModel = RT3000
					#IF FEATURE_DLC_1_2022
					OR vehModel = POSTLUDE
					#ENDIF
						RETURN CMM_SUPERMOD_DOOR_R
					#IF FEATURE_DLC_1_2022
					ELIF vehModel = SENTINEL4
						RETURN CMM_SUPERMOD_DOOR_L
					#ENDIF
					ELIF vehModel = PREVION
						RETURN CMM_SUPERMOD_CHASSIS2	
					ELIF vehModel = REMUS	
						RETURN CMM_SUPERMOD_PLTVANITY
					#IF FEATURE_DLC_1_2022
					//ELIF vehModel = GREENWOOD	
					ELIF vehModel = KANJOSJ
						RETURN CMM_SUPERMOD_ENGINEBAY2
					#ENDIF
					ELSE
						RETURN CMM_SKIRTS 
					ENDIF
				BREAK
				CASE MOD_NAME_ENGINEBLOCK		
					IF vehModel = INFERNUS2
					OR vehModel = TURISMO2
					OR vehModel = CHEETAH2
						RETURN CMM_FENDERS
					ELIF vehModel = XA21	
					#IF FEATURE_HEIST_ISLAND
					OR vehModel = VETO	
					OR vehModel = VETO2	
					#ENDIF
						RETURN CMM_SPOILER
					ELIF vehModel = IMPALER2
					OR vehModel = IMPALER3
					OR vehModel = IMPALER4
						RETURN CMM_FENDERSTWO
					ELSE
						RETURN CMM_CHASSIS 
					ENDIF	
				BREAK
				CASE MOD_NAME_FRONT_SHIELD
					RETURN CMM_BUMPERS
				BREAK
				CASE MOD_NAME_GRIP
					RETURN CMM_CHASSIS
				BREAK	
				CASE MOD_NAME_MUDRGUARDS 	
					IF vehModel = NEBULA
					OR vehModel = ZHABA
						RETURN CMM_SKIRTS
					#IF FEATURE_TUNER	
					ELIF vehModel = DOMINATOR7
					OR vehModel = TAILGATER2
						RETURN CMM_SUPERMOD_CHASSIS2
					ELIF vehModel = CALICO	
						RETURN CMM_SUPERMOD_CHASSIS5
					#ENDIF	
					ELSE
						RETURN CMM_FENDERSTWO 
					ENDIF
				BREAK	
				CASE MOD_NAME_BODY_TRIM	RETURN CMM_FENDERSTWO	BREAK
				CASE MOD_NAME_REAR_COVER	RETURN CMM_CHASSIS 		BREAK
				CASE MOD_NAME_ROOF_SPOILER	RETURN CMM_CHASSIS 		BREAK
				CASE MOD_NAME_REAR_DIFFUSER RETURN  CMM_BUMPERS 	BREAK
				CASE MOD_NAME_FRONT_DIFFUSER RETURN  CMM_BUMPERS 	BREAK
				CASE MOD_NAME_ENGINE_COVER 	
					IF vehModel = XA21
						RETURN CMM_FENDERSTWO
					ELIF vehModel = VISERIS
						RETURN CMM_GRILL
					#IF FEATURE_DLC_1_2022
					ELIF vehModel = KANJOSJ
						RETURN CMM_SUPERMOD_ENGINEBAY1
					ELIF vehModel = SENTINEL4
						RETURN CMM_SUPERMOD_ENGINEBAY2
					#ENDIF
					ELSE
						RETURN CMM_ROOF 
					ENDIF
				BREAK 
				CASE MOD_NAME_HOOD_CATCH 
					IF vehModel = NEO
					OR vehModel = VSTR
					OR vehModel = GAUNTLET5
					#IF FEATURE_TUNER
					OR vehModel = JESTER4
					#ENDIF
						RETURN CMM_FENDERSTWO
					ELIF vehModel = ZR350
					OR vehModel = COMET6
					OR vehModel = WARRENER2
					OR vehModel = REMUS
					OR vehModel = VECTRE
					OR vehModel = CYPHER
					OR vehModel = PREVION
					OR vehModel = DOMINATOR8
					OR vehModel = EUROS
					OR vehModel = GROWLER
					OR vehModel = DOMINATOR7
					OR vehModel = CALICO
					OR vehModel = TAILGATER2
					OR vehModel = SULTAN3
					#IF FEATURE_DLC_1_2022
					OR vehModel = KANJOSJ
					OR vehModel = TENF2
					OR vehModel = SENTINEL4
					#ENDIF
						RETURN   CMM_SUPERMOD_CHASSIS3 
					ELSE
						RETURN CMM_GRILL
					ENDIF
				BREAK	
				CASE MOD_NAME_SPLITTER 			
					IF vehModel = GAUNTLET5
					OR vehModel = YOUGA3
						RETURN    CMM_SUPERMOD_ENGINEBAY1
					#IF FEATURE_TUNER	
					ELIF vehModel = JESTER4
					OR vehModel = VECTRE
					OR vehModel = ZR350
					OR vehModel = WARRENER2
					OR vehModel = RT3000
					OR vehModel = PREVION
					OR vehModel = DOMINATOR8
					OR vehModel = EUROS
					OR vehModel = GROWLER
					OR vehModel = COMET6
					#IF FEATURE_DLC_1_2022
					OR vehModel = CORSITA
					OR vehModel = LM87
					OR vehModel = POSTLUDE
					#ENDIF
						  RETURN CMM_SUPERMOD_PLTVANITY	
					#ENDIF
					ELSE	
						RETURN CMM_GRILL 			
					ENDIF
				BREAK 
				CASE MOD_NAME_REAR_SPLITTER
					RETURN CMM_GRILL
				CASE MOD_NAME_HEADLIGHT_COV 	
					IF vehModel = COMET4
					OR vehModel = JESTER3
					OR vehModel = ASBO
					OR vehModel = GAUNTLET5
					OR vehModel = CLUB
					OR vehModel = YOUGA3
					OR vehModel = MANANA2
					#IF FEATURE_COPS_N_CROOKS
					OR vehModel = POLICET2
					OR vehModel = POLPANTO
					#ENDIF
					#IF FEATURE_HEIST_ISLAND
					OR vehModel = WINKY
					#ENDIF
					OR vehModel = CALICO
					OR vehModel = COMET6
					OR vehModel = WARRENER2
					OR vehModel = REMUS
					OR vehModel = SULTAN3
					OR vehModel = RT3000
					OR vehModel = VECTRE
					OR vehModel = DOMINATOR8
					OR vehModel = EUROS
					#IF FEATURE_DLC_1_2022
					OR vehModel = WEEVIL2
					OR vehModel = GREENWOOD
					OR vehModel = TENF2
					OR vehModel = POSTLUDE
					OR vehModel = PICADOR
					OR vehModel = BRIOSO3
					OR vehModel = SENTINEL4
					#ENDIF
						  RETURN CMM_FAIRING
					ELSE	
						RETURN CMM_FENDERS 			
					ENDIF
				BREAK
				CASE MOD_NAME_REMOTE_UNIT				  RETURN CMM_SUPERMOD_CHASSIS4
				CASE MOD_NAME_RAIL_COVERS				  RETURN CMM_SUPERMOD_ENGINEBAY2
				CASE MOD_NAME_TAIL_LIGHTS				  RETURN CMM_SUPERMOD_DOOR_L
				CASE MOD_NAME_SUNSTRIPS			
				CASE MOD_NAME_SUNSHADES
					IF vehModel = SCHLAGEN
					OR vehModel = ISSI7
					OR vehModel = PARAGON
					OR vehModel = PARAGON2
					OR vehModel = IMORGON
					OR vehModel = YOSEMITE2
					#IF FEATURE_COPS_N_CROOKS
					OR vehModel = SURFER3
					#ENDIF
					OR vehModel = PENUMBRA2
					#IF FEATURE_HEIST_ISLAND
					OR vehModel = BRIOSO2
					#ENDIF
					OR vehModel = BALLER7
						RETURN CMM_FENDERSTWO 
					ELIF  vehModel = GAUNTLET5
					OR  vehModel = CLUB
					OR vehModel = COQUETTE4
					OR vehModel = CINQUEMILA
						  RETURN CMM_VIN	
					ELIF vehModel = YOSEMITE3
						RETURN CMM_SUPERMOD_INTERIOR5
					ELIF vehModel = ZR350
					OR vehModel = PREVION
					OR vehModel = DOMINATOR7
					OR vehModel = TAILGATER2	
					OR vehModel = WARRENER2
					OR vehModel = COMET6
					OR vehModel = VECTRE
					OR vehModel = REMUS
					OR vehModel = JESTER4
					OR vehModel = RT3000
					OR vehModel = CYPHER
					OR vehModel = DOMINATOR8
					OR vehModel = FUTO2
					OR vehModel = EUROS
					OR vehModel = GROWLER
					OR vehModel = SULTAN3
					OR vehModel = ASTRON
					#IF FEATURE_GEN9_EXCLUSIVE
					OR vehModel = ASTRON2
					#ENDIF
					#IF FEATURE_DLC_1_2022
					OR vehModel = RHINEHART
					OR vehModel = CORSITA
					OR vehModel = ZENTORNO
					OR vehModel = SENTINEL
					OR vehModel = TENF2
					OR vehModel = KANJOSJ
					OR vehModel = WEEVIL2
					OR vehModel = POSTLUDE
					OR vehModel = RUINER4
					OR vehModel = PICADOR
					OR vehModel = SENTINEL4
					OR vehModel = BRIOSO3
					#ENDIF
						RETURN CMM_SPAREWHEEL
					#IF FEATURE_GEN9_EXCLUSIVE
					ELIF vehModel = CYCLONE2
					OR vehModel = S95
					OR vehModel = ARBITERGT
						RETURN CMM_CHASSIS
					#ENDIF
					#IF FEATURE_DLC_1_2022
					ELIF vehModel = VIGERO2
						RETURN CMM_CHASSIS
					#ENDIF
					ELSE	
						RETURN CMM_ROOF 			
					ENDIF
				BREAK
				CASE MOD_NAME_SEC_LIGHT 		
				CASE MOD_NAME_ROOF_LIGHT		
					IF vehModel = PATRIOT 
					OR vehModel = WINKY
					#IF FEATURE_FIXER
					OR vehModel = GRANGER2
					#ENDIF
						RETURN CMM_ROOF
					ELIF vehModel = YOSEMITE3
						RETURN CMM_SUPERMOD_CHASSIS4
					ELSE	
						RETURN CMM_FENDERSTWO 		
					ENDIF
				BREAK
				CASE MOD_NAME_ROOF_SCOOP		
					IF vehModel = WARRENER2
					OR vehModel = EUROS
						RETURN CMM_SUPERMOD_CHASSIS4
					ELSE
						RETURN CMM_HOOD	
					ENDIF
				BREAK
				CASE MOD_NAME_WEAPONS			RETURN CMM_ROOF 	 		BREAK
				CASE MOD_NAME_DEFLEC
				CASE MOD_NAME_ARMOR_PLATING		RETURN CMM_CHASSIS			BREAK
				CASE MOD_NAME_REAR_PANEL 		
					IF IS_VEHICLE_MODEL(g_ModVehicle, DOMINATOR3)
					OR IS_VEHICLE_MODEL(g_ModVehicle, FREECRAWLER)
					OR IS_VEHICLE_MODEL(g_ModVehicle, GAUNTLET4)
					#IF FEATURE_COPS_N_CROOKS
					OR IS_VEHICLE_MODEL(g_ModVehicle, POLGAUNTLET)
					OR IS_VEHICLE_MODEL(g_ModVehicle, POLICET2)
					#ENDIF
						  RETURN CMM_REARMUDGUARD BREAK
					ELSE	
						RETURN CMM_GRILL			
					ENDIF	
				BREAK
				CASE MOD_NAME_DOOR_PLATE 		RETURN CMM_FENDERS			BREAK
				CASE MOD_NAME_HEAD_LIGHT_PRO 	  RETURN CMM_FRONTMUDGUARD 	BREAK
				CASE MOD_NAME_REAR_LIGHT_PRO 	
					IF IS_VEHICLE_MODEL(g_ModVehicle, DOMINATOR4)
						RETURN CMM_SUPERMOD_INTERIOR3 
					ELSE
						  RETURN CMM_REARMUDGUARD BREAK
					ENDIF
				BREAK
				CASE MOD_NAME_HOOD_LIP			
					IF vehModel = SEMINOLE2
						RETURN CMM_SPOILER
					ELSE
						RETURN CMM_GRILL
					ENDIF
				BREAK	
				CASE MOD_NAME_CANARDS			
					IF vehModel = JESTER4
					OR vehModel = TAILGATER2
					#IF FEATURE_DLC_1_2022
					OR vehModel = TENF2
					#ENDIF
						RETURN CMM_SUPERMOD_PLTVANITY
					ENDIF	
					RETURN CMM_GRILL			
				BREAK
				CASE MOD_NAME_WINDOW_PLATE 		
					IF vehModel = HELLION
						RETURN CMM_SPOILER
					ELIF vehModel = GAUNTLET5
						RETURN CMM_SUPERMOD_PLTVANITY
					ELSE
						RETURN CMM_CHASSIS	
					ENDIF
				BREAK
				CASE MOD_NAME_REAR_DOOR			RETURN CMM_SPOILER			 BREAK
				CASE MOD_NAME_SKID_PLATE 		RETURN CMM_SUPERMOD_CHASSIS3 BREAK
				CASE MOD_NAME_PROXIMITY_MINE	RETURN CMM_FENDERS			 BREAK
				CASE MOD_NAME_SEAT 				RETURN CMM_HOOD				 BREAK
				CASE MOD_NAME_SNORKEL
				CASE MOD_NAME_PRIMARY_WEAPON	
					IF vehModel = IMPERATOR
					OR vehModel = IMPALER2
					OR vehModel = SCARAB
					OR vehModel = SLAMVAN4
					OR vehModel = DEATHBIKE
					OR vehModel = BRUISER
					OR vehModel = MONSTER3
					OR vehModel = DOMINATOR4
					OR vehModel = ISSI4
					OR vehModel = IMPERATOR2
					OR vehModel = IMPALER3
					OR vehModel = SCARAB2
					OR vehModel = SLAMVAN5
					OR vehModel = DEATHBIKE2
					OR vehModel = BRUISER2
					OR vehModel = MONSTER4
					OR vehModel = DOMINATOR5
					OR vehModel = ISSI5
					OR vehModel = IMPERATOR3
					OR vehModel = IMPALER4
					OR vehModel = SCARAB3
					OR vehModel = SLAMVAN6
					OR vehModel = DEATHBIKE3
					OR vehModel = BRUISER3
					OR vehModel = MONSTER5
					OR vehModel = DOMINATOR6
					OR vehModel = ISSI6
					OR vehModel = ZR380
					OR vehModel = ZR3802
					OR vehModel = ZR3803
					OR vehModel = BRUTUS
					OR vehModel = BRUTUS2
					OR vehModel = BRUTUS3
					OR IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehModel)
						RETURN CMM_SUPERMOD_CHASSIS5
					ELIF vehModel = YOSEMITE3
						RETURN CMM_SUPERMOD_DOOR_R
					ELSE	
						RETURN CMM_ROOF 			
					ENDIF
				BREAK
				CASE MOD_NAME_SUPPORT_WEAPON	
					IF vehModel = OPPRESSOR2
					#IF FEATURE_GEN9_EXCLUSIVE
					OR vehModel = IGNUS2
					#ENDIF
						RETURN CMM_GRILL
					ELSE
						  RETURN CMM_FRONTMUDGUARD BREAK
					ENDIF
				BREAK
				CASE MOD_NAME_BOMB 				RETURN CMM_FENDERS			BREAK
				CASE MOD_NAME_THRUST			
				CASE MOD_NAME_PROPELLER			RETURN CMM_EXHAUST			BREAK
				CASE MOD_NAME_SECONDARY_WEAPON	
					IF vehModel = BARRAGE
					OR vehModel = MULE4
					OR vehModel = SPEEDO4
						RETURN CMM_SPOILER		
					ELIF vehModel = POUNDER2
						RETURN CMM_BUMPERS	
					ELIF vehModel = IMPERATOR
					OR vehModel = CERBERUS
					OR vehModel = ZR380
					OR vehModel = ISSI4
					OR vehModel = IMPERATOR2
					OR vehModel = CERBERUS2
					OR vehModel = ZR3802
					OR vehModel = ISSI5
					OR vehModel = IMPERATOR3
					OR vehModel = CERBERUS3
					OR vehModel = ZR3803
					OR vehModel = ISSI6
						RETURN CMM_ROOF 	
					ELIF vehModel = JB7002
						RETURN CMM_FENDERS
					ELSE
						RETURN CMM_CHASSIS
					ENDIF
				BREAK
				CASE MOD_NAME_VISORS			
				CASE MOD_NAME_VALVE_COV			
					IF vehModel = SWINGER
						RETURN CMM_SKIRTS		
					ELIF vehModel = DYNASTY
					OR vehModel = YOUGA3
					#IF FEATURE_HEIST_ISLAND
					OR vehModel = SLAMTRUCK
					#ENDIF
						RETURN CMM_FENDERSTWO
					ELIF vehModel = YOSEMITE3
						RETURN CMM_SUPERMOD_INTERIOR5
					#IF FEATURE_DLC_1_2022
					ELIF vehModel = POSTLUDE
					OR vehModel = SENTINEL4
						RETURN CMM_SUPERMOD_ENGINEBAY1
					#ENDIF
					ELSE
						RETURN CMM_SPOILER			
					ENDIF
				BREAK
				CASE MOD_NAME_FOGLIGHT_COVERS	
				CASE MOD_NAME_FOGLI
					IF vehModel = CALICO
					OR vehModel = COMET6
						RETURN CMM_SUPERMOD_CHASSIS2		
					ELSE	
						  RETURN CMM_FAIRING
					ENDIF	
				BREAK
				CASE MOD_NAME_JUMP				RETURN CMM_SUPERMOD_ENGINEBAY3	BREAK
				CASE MOD_NAME_WIND_ARMOR		  RETURN CMM_FRONTMUDGUARD	BREAK
				CASE MOD_NAME_UNDERPLATING		  RETURN CMM_REARMUDGUARD	BREAK
				CASE MOD_NAME_WIND_DELFECTOR	
					IF vehModel = COMET6
						RETURN CMM_SUPERMOD_DOOR_L
					ELSE
						RETURN CMM_FENDERSTWO			
					ENDIF	
				BREAK
				CASE MOD_NAME_VORTEX_GEN		RETURN CMM_CHASSIS				BREAK
				CASE MOD_NAME_ROOF_ACC			
					IF vehModel = YOUGA3
					#IF FEATURE_TUNER
					OR vehModel = COMET6
					OR vehModel = REMUS
					OR vehModel = VECTRE
					OR vehModel = SULTAN3
					OR vehModel = TAILGATER2
					#ENDIF
						RETURN CMM_SUPERMOD_CHASSIS4
					ELIF vehModel = SEMINOLE2
					OR vehModel = GROWLER
						RETURN CMM_ROOF
					#IF FEATURE_FIXER
					ELIF vehModel = GRANGER2
						RETURN CMM_SPOILER
					#ENDIF
					ELSE	
						RETURN CMM_CHASSIS				
					ENDIF
				BREAK
				CASE MOD_NAME_HOOD_ACC
					#IF FEATURE_DLC_1_2022
					IF vehModel = WEEVIL2
						RETURN CMM_GRILL
					ENDIF
					#ENDIF
					
					RETURN CMM_SUPERMOD_CHASSIS3
				BREAK
				CASE MOD_NAME_TAIL_FIN			RETURN CMM_CHASSIS				BREAK
				CASE MOD_NAME_ROOF_PANEL		RETURN CMM_CHASSIS				BREAK
				CASE MOD_NAME_WINDOW_SPOILER	RETURN CMM_FENDERSTWO			BREAK
				#IF FEATURE_COPS_N_CROOKS
				CASE MOD_NAME_SEARCH_LIGHT		RETURN CMM_SUPERMOD_CHASSIS4	BREAK
				#ENDIF
				CASE MOD_NAME_NOSE_FIN
					RETURN CMM_CHASSIS				
				BREAK
				CASE MOD_NAME_NOSE_PANEL		
					#IF FEATURE_TUNER
					IF vehModel = WARRENER2
					OR vehModel = REMUS
						RETURN CMM_SUPERMOD_CHASSIS5
					ENDIF
					#ENDIF
					
					RETURN CMM_GRILL				
				BREAK
				CASE MOD_NAME_ROOF_FIN			
					IF vehModel = JESTER4
						RETURN CMM_SUPERMOD_CHASSIS2
					ELIF vehModel = CYPHER
					#IF FEATURE_DLC_1_2022
					OR vehModel = KANJOSJ
					OR vehModel = TENF2
					#ENDIF
						RETURN CMM_SUPERMOD_CHASSIS4
					#IF FEATURE_DLC_1_2022
					ELIF vehModel = TENF
					OR vehModel = SCHWARZER
						RETURN CMM_CHASSIS
					#ENDIF
					ENDIF
					
					RETURN CMM_ROOF					
				BREAK
				CASE MOD_NAME_STRUT_BRACE
					#IF FEATURE_DLC_1_2022
					IF vehModel = KANJOSJ
					OR vehModel = POSTLUDE
					OR vehModel = SENTINEL4
						RETURN CMM_SUPERMOD_ENGINEBAY3
					ENDIF
					#ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		// No overide set so return the default
		IF eModType = MOD_SPOILER			RETURN CMM_SPOILER
		ELIF eModType = MOD_BUMPER_F				
			IF vehModel = TAMPA3
			#IF FEATURE_COPS_N_CROOKS
			OR vehModel = ZOKU
			#ENDIF
				RETURN CMM_FRONTMUDGUARD
			ELSE
				RETURN CMM_BUMPERS
			ENDIF
		ELIF eModType = MOD_BUMPER_R				
			IF  HAS_BIKE_USING_OIL_TANK_FOR_VMT_BUMPER_R(g_ModVehicle)
				RETURN CMM_FENDERSTWO
			ELIF vehModel = TAMPA3
			OR IS_VEHICLE_MODEL(g_ModVehicle, DOMINATOR3)
			OR IS_VEHICLE_MODEL(g_ModVehicle, SEMINOLE2)
				RETURN CMM_REARMUDGUARD
			ELSE
				RETURN CMM_BUMPERS
			ENDIF	
		ELIF eModType = MOD_SKIRT					RETURN CMM_SKIRTS
		ELIF eModType = MOD_EXHAUST					RETURN CMM_EXHAUST
		ELIF eModType = MOD_CHASSIS					RETURN CMM_CHASSIS
		ELIF eModType = MOD_GRILL					RETURN CMM_GRILL
		ELIF eModType = MOD_BONNET					RETURN CMM_HOOD
		ELIF eModType = MOD_WING_L	
			IF vehModel = XA21
			OR DOES_VEHICLE_MINE_MOD_CONFLICT_WITH_FENDERS(g_ModVehicle)
				RETURN CMM_FENDERSTWO
			ENDIF
			IF USE_CUSTOM_CAM_FOR_BIKER_MOD(g_ModVehicle)
			OR vehModel = RCBANDITO
				RETURN CMM_FENDERSTWO
			ELSE
				RETURN CMM_FENDERS
			ENDIF
		ELIF eModType = MOD_WING_R					
			IF vehModel = CHEETAH2
			OR vehModel = Z190
			OR vehModel = COMET4
			OR vehModel = POUNDER2
			OR vehModel = DYNASTY
			OR vehModel = GAUNTLET3
				RETURN CMM_FENDERSTWO
			ELSE	
				RETURN CMM_FENDERS	
			ENDIF	
		ELIF eModType = MOD_ROOF					RETURN CMM_ROOF
		ELIF eModType = MOD_ENGINE					RETURN CMM_ENGINE
		ELIF eModType = MOD_BRAKES					RETURN CMM_BRAKES
		ELIF eModType = MOD_GEARBOX					RETURN CMM_TRANSMISSION
		ELIF eModType = MOD_HORN					RETURN CMM_HORN
		ELIF eModType = MOD_SUSPENSION				RETURN CMM_SUSPENSION
		ELIF eModType = MOD_ARMOUR					RETURN CMM_ARMOUR
		
		ELIF eModType = MOD_TOGGLE_TURBO			RETURN CMM_TURBO
		ELIF eModType = MOD_TOGGLE_TYRE_SMOKE		RETURN CMM_WHEEL_ACCS
		ELIF eModType = MOD_TOGGLE_XENON_LIGHTS		RETURN CMM_LIGHTS
		
		ELIF eModType = MOD_WHEELS					RETURN CMM_WHEELS
		ELIF eModType = MOD_REAR_WHEELS				RETURN CMM_WHEELS
		
		ELIF eModType = MOD_PLTHOLDER				RETURN CMM_SUPERMOD_PLTHOLDER
		ELIF eModType = MOD_PLTVANITY				RETURN CMM_SUPERMOD_PLTVANITY
		ELIF eModType = MOD_INTERIOR1				
			IF vehModel = KANJO
				RETURN CMM_FENDERSTWO
			ELSE	
				RETURN CMM_TRIM // <-- we split this up into trim and trim colour.
			ENDIF
		ELIF eModType = MOD_INTERIOR2				RETURN CMM_SUPERMOD_INTERIOR2
		ELIF eModType = MOD_INTERIOR3				RETURN CMM_SUPERMOD_INTERIOR3
		ELIF eModType = MOD_INTERIOR4				RETURN CMM_DIALS // <-- we split this up into dials and dial colour.
		ELIF eModType = MOD_INTERIOR5				RETURN CMM_SUPERMOD_INTERIOR5
		ELIF eModType = MOD_SEATS					RETURN CMM_SUPERMOD_SEATS
		ELIF eModType = MOD_STEERING				RETURN CMM_SUPERMOD_STEERING
		ELIF eModType = MOD_KNOB					RETURN CMM_SUPERMOD_KNOB
		ELIF eModType = MOD_PLAQUE					RETURN CMM_SUPERMOD_PLAQUE
		ELIF eModType = MOD_ICE						RETURN CMM_SUPERMOD_ICE
		ELIF eModType = MOD_TRUNK					RETURN CMM_SUPERMOD_TRUNK
		ELIF eModType = MOD_HYDRO					RETURN CMM_SUPERMOD_HYDRO
		ELIF eModType = MOD_ENGINEBAY1				RETURN CMM_SUPERMOD_ENGINEBAY1
		ELIF eModType = MOD_ENGINEBAY2				RETURN CMM_SUPERMOD_ENGINEBAY2
		ELIF eModType = MOD_ENGINEBAY3				RETURN CMM_SUPERMOD_ENGINEBAY3
		ELIF eModType = MOD_CHASSIS2				RETURN CMM_SUPERMOD_CHASSIS2
		ELIF eModType = MOD_CHASSIS3				RETURN CMM_SUPERMOD_CHASSIS3
		ELIF eModType = MOD_CHASSIS4				RETURN CMM_SUPERMOD_CHASSIS4
		ELIF eModType = MOD_CHASSIS5				RETURN CMM_SUPERMOD_CHASSIS5
		ELIF eModType = MOD_DOOR_L					RETURN CMM_SUPERMOD_DOOR_L
		ELIF eModType = MOD_DOOR_R					RETURN CMM_SUPERMOD_DOOR_R
		ELIF eModType = MOD_LIVERY					RETURN CMM_SUPERMOD_LIVERY
		
		ENDIF
	ENDIF
	
	RETURN CMM_MOD
ENDFUNC

FUNC CARMOD_MOD_TYPE_ENUM GET_DEFAULT_MOD_TYPE_FOR_MOD_SLOT(MOD_TYPE eModType)
	// No overide set so return the default
	SWITCH eModType
		CASE MOD_SPOILER				RETURN CMT_SPOILER BREAK
		CASE MOD_BUMPER_F				RETURN CMT_BUMPERS_F BREAK
		CASE MOD_BUMPER_R				RETURN CMT_BUMPERS_R BREAK
		CASE MOD_SKIRT					RETURN CMT_SKIRTS BREAK
		CASE MOD_EXHAUST				RETURN CMT_EXHAUST BREAK
		CASE MOD_CHASSIS				RETURN CMT_CHASSIS BREAK
		CASE MOD_GRILL					RETURN CMT_GRILL BREAK
		CASE MOD_BONNET					RETURN CMT_HOOD BREAK
		CASE MOD_WING_L					RETURN CMT_FENDERS_L BREAK
		CASE MOD_WING_R					RETURN CMT_FENDERS_R BREAK
		CASE MOD_ROOF					RETURN CMT_ROOF BREAK
		
		CASE MOD_ENGINE					RETURN CMT_ENGINE BREAK
		CASE MOD_BRAKES					RETURN CMT_BRAKES BREAK
		CASE MOD_GEARBOX				RETURN CMT_TRANSMISSION BREAK
		CASE MOD_HORN					RETURN CMT_HORN BREAK
		CASE MOD_SUSPENSION				RETURN CMT_SUSPENSION BREAK
		CASE MOD_ARMOUR					RETURN CMT_ARMOUR BREAK
		
		CASE MOD_TOGGLE_TURBO			RETURN CMT_TURBO BREAK
		CASE MOD_TOGGLE_TYRE_SMOKE		RETURN CMT_TYRE_SMOKE BREAK
		CASE MOD_TOGGLE_XENON_LIGHTS	RETURN CMT_HEADLIGHTS BREAK
		
		CASE MOD_WHEELS					RETURN CMT_WHEELS BREAK
		CASE MOD_REAR_WHEELS			RETURN CMT_WHEELS BREAK
		
		CASE MOD_PLTHOLDER				RETURN CMT_SUPERMOD_0 BREAK
		CASE MOD_PLTVANITY				RETURN CMT_SUPERMOD_1 BREAK
		CASE MOD_INTERIOR1				RETURN CMT_SUPERMOD_2 BREAK
		CASE MOD_INTERIOR2				RETURN CMT_SUPERMOD_3 BREAK
		CASE MOD_INTERIOR3				RETURN CMT_SUPERMOD_4 BREAK
		CASE MOD_INTERIOR4				RETURN CMT_SUPERMOD_5 BREAK
		CASE MOD_INTERIOR5				RETURN CMT_SUPERMOD_6 BREAK
		CASE MOD_SEATS					RETURN CMT_SUPERMOD_7 BREAK
		CASE MOD_STEERING				RETURN CMT_SUPERMOD_8 BREAK
		CASE MOD_KNOB					RETURN CMT_SUPERMOD_9 BREAK
		CASE MOD_PLAQUE					RETURN CMT_SUPERMOD_10 BREAK
		CASE MOD_ICE					RETURN CMT_SUPERMOD_11 BREAK
		CASE MOD_TRUNK					RETURN CMT_SUPERMOD_12 BREAK
		CASE MOD_HYDRO					RETURN CMT_SUPERMOD_13 BREAK
		CASE MOD_ENGINEBAY1				RETURN CMT_SUPERMOD_14 BREAK
		CASE MOD_ENGINEBAY2				RETURN CMT_SUPERMOD_15 BREAK
		CASE MOD_ENGINEBAY3				RETURN CMT_SUPERMOD_16 BREAK
		CASE MOD_CHASSIS2				RETURN CMT_SUPERMOD_17 BREAK
		CASE MOD_CHASSIS3				RETURN CMT_SUPERMOD_18 BREAK
		CASE MOD_CHASSIS4				RETURN CMT_SUPERMOD_19 BREAK
		CASE MOD_CHASSIS5				RETURN CMT_SUPERMOD_20 BREAK
		CASE MOD_DOOR_L					RETURN CMT_SUPERMOD_21 BREAK
		CASE MOD_DOOR_R					RETURN CMT_SUPERMOD_22 BREAK
		CASE MOD_LIVERY					RETURN CMT_SUPERMOD_23 BREAK
	ENDSWITCH
	RETURN CMT_DUMMY
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_CORONA_VEH_MOD_NAME(CORONA_VEH_MOD_TYPE sCoronaVehMod)
	SWITCH sCoronaVehMod
		CASE CVT_LIVERY				RETURN "CVT_LIVERY"
		CASE CVT_ARMOUR				RETURN "CVT_ARMOUR"	
		CASE CVT_SCOOP_RAMBAR		RETURN "CVT_SCOOP_RAMBAR"
		CASE CVT_MINE				RETURN "CVT_MINE"
	ENDSWITCH	
	RETURN ""
ENDFUNC
#ENDIF

FUNC MOD_TYPE GET_CORONA_VEH_MOD_TYPE(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod)
	IF DOES_ENTITY_EXIST(veh)
		SWITCH sCoronaVehMod
			CASE CVT_LIVERY				RETURN MOD_LIVERY
			CASE CVT_ARMOUR	
				SWITCH GET_ENTITY_MODEL(veh)
					CASE IMPERATOR		RETURN MOD_CHASSIS
					CASE ISSI4			RETURN MOD_CHASSIS
					DEFAULT				RETURN MOD_CHASSIS
				ENDSWITCH
			BREAK
			CASE CVT_SCOOP_RAMBAR		RETURN MOD_CHASSIS2
			CASE CVT_MINE				RETURN MOD_WING_R
		ENDSWITCH
	ELSE
		PRINTLN("INVALID GET_CORONA_VEH_MOD_TYPE - entity doesnt exist")
	ENDIF
	
	PRINTLN("INVALID GET_CORONA_VEH_MOD_TYPE - returning MOD_SPOILER 0")
	RETURN MOD_SPOILER
ENDFUNC

FUNC CARMOD_MENU_ENUM GET_CORONA_VEH_MOD_MENU_TYPE(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod)
	IF DOES_ENTITY_EXIST(veh)
		SWITCH sCoronaVehMod
			CASE CVT_LIVERY				RETURN CMM_SUPERMOD_LIVERY
			CASE CVT_ARMOUR		
				SWITCH GET_ENTITY_MODEL(veh)
					CASE IMPERATOR		RETURN CMM_CHASSIS
					CASE ISSI4			RETURN CMM_CHASSIS
					DEFAULT				RETURN CMM_CHASSIS
				ENDSWITCH
			BREAK
			CASE CVT_SCOOP_RAMBAR		RETURN CMM_SUPERMOD_CHASSIS2
			CASE CVT_MINE				RETURN CMM_FENDERS
		ENDSWITCH
	ELSE
		PRINTLN("INVALID GET_CORONA_VEH_MOD_MENU_TYPE - entity doesnt exist")
	ENDIF
	
	RETURN CMM_MAIN
ENDFUNC

FUNC INT GET_CORONA_VEHICLE_CURRENT_MOD(CORONA_VEH_MOD_TYPE sCoronaVehMod)
	SWITCH sCoronaVehMod
		CASE CVT_LIVERY				RETURN g_ArenaWarsCurrentLivery
		CASE CVT_ARMOUR				RETURN g_ArenaWarsCurrentArmour
		CASE CVT_SCOOP_RAMBAR		RETURN g_ArenaWarsCurrentScoopRambar
		CASE CVT_MINE				RETURN g_ArenaWarsCurrentMines
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC BOOL DOES_CORONA_VEHICLE_HAVE_THIS_MOD_EQUIPPED(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod, INT iOption)

	IF DOES_ENTITY_EXIST(veh)
		IF GET_CORONA_VEHICLE_CURRENT_MOD(sCoronaVehMod) = iOption
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_VEHICLE_MOD_OPTION_COST(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod, INT iOption, BOOL &bInstalled, BOOL bBuyNowPrice = FALSE)

	IF DOES_CORONA_VEHICLE_HAVE_THIS_MOD_EQUIPPED(veh, sCoronaVehMod, iOption)
		PRINTLN("[CORONA] - GET_VEHICLE_MOD_OPTION_COST - 0 - vehicle already has option ", iOption," for mod ", GET_CORONA_VEH_MOD_NAME(sCoronaVehMod), " installed")
		bInstalled = TRUE
		RETURN 0
	ENDIF
	
	g_ModVehicle = veh
	g_eModPriceVariation =  INT_TO_ENUM(MOD_PRICE_VARIATION_ENUM, GET_VEHICLE_MOD_PRICE_VARIATION_FOR_CATALOGUE(GET_ENTITY_MODEL(veh)))
	
	IF sCoronaVehMod = CVT_MINE
		IF iOption != -1
			RETURN GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod), TRUE), iOption+22, DEFAULT, DEFAULT, bBuyNowPrice)
		ELSE
			RETURN GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod), TRUE), 21, DEFAULT, DEFAULT, bBuyNowPrice)
		ENDIF
	ENDIF

	RETURN GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod), TRUE), iOption+1, DEFAULT, DEFAULT, bBuyNowPrice)
ENDFUNC

FUNC STRING GET_CORONA_VEHICLE_MOD_TEXT_LABEL(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod, INT iOption)
	IF iOption = -1
		RETURN "AW_NONE"
	ENDIF
	
	IF GET_NUM_MOD_KITS(veh) > 0
		SET_VEHICLE_MOD_KIT(veh, 0)
	ENDIF
	
	IF GET_NUM_VEHICLE_MODS(veh, GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod)) > 0
		IF iOption < GET_NUM_VEHICLE_MODS(veh, GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod)) 
			RETURN GET_MOD_TEXT_LABEL(veh, GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod), iOption)
		ELSE
			PRINTLN("[CORONA] - GET_CORONA_VEHICLE_MOD_TEXT_LABEL passing invalid iOption: ", iOption, " GET_NUM_VEHICLE_MODS ", GET_CORONA_VEH_MOD_NAME(sCoronaVehMod), " : ", GET_NUM_VEHICLE_MODS(veh, GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod)))
		ENDIF
	ENDIF
	
	RETURN "AW_NONE" 
ENDFUNC

FUNC BOOL IS_CORONA_VEHICLE_MOD_UNLOCKED(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod, INT iOption)
	RETURN IS_ARENA_CAREER_MOD_SHOP_ITEM_UNLOCKED(veh, GET_CORONA_VEH_MOD_MENU_TYPE(veh, sCoronaVehMod), iOption+1, 0)
ENDFUNC 

FUNC BOOL IS_ANY_CORONA_VEHICLE_MOD_OF_THIS_TYPE_UNLOCKED(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod)
	
	INT iNumMods = GET_NUM_VEHICLE_MODS(veh, GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod))
	
	IF iNumMods <= 0
		RETURN FALSE
	ENDIF
	
	INT iMod
	REPEAT iNumMods iMod
		IF IS_CORONA_VEHICLE_MOD_UNLOCKED(veh, sCoronaVehMod, iMod)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC 

FUNC INT GET_CORONA_VEH_MOD_ARRAY_INDEX(CORONA_VEH_MOD_TYPE sCoronaVehMod)
	SWITCH sCoronaVehMod
		CASE CVT_LIVERY				RETURN 0
		CASE CVT_ARMOUR				RETURN 1
		CASE CVT_SCOOP_RAMBAR		RETURN 2
		CASE CVT_MINE				RETURN 3
	ENDSWITCH	
	SCRIPT_ASSERT("INVALID GET_CORONA_VEH_MOD_ARRAY_INDEX ")
	RETURN 0
ENDFUNC

FUNC INT GET_TOTAL_COST_OF_CORONA_VEH_MODS()
	INT iTotalCost
	
	INT i
	FOR i = 0 TO MAX_NUM_CORONA_VEH_MOD - 1
		iTotalCost += g_sCoronaVehMod.iModPrice[i] 
	ENDFOR
	
	PRINTLN("GET_TOTAL_COST_OF_CORONA_VEH_MODS: ", iTotalCost)
	RETURN iTotalCost
ENDFUNC

PROC PREVIEW_AND_STORE_VEHICLE_MOD(VEHICLE_INDEX veh, CORONA_VEH_MOD_TYPE sCoronaVehMod, INT iOption)
	IF DOES_ENTITY_EXIST(veh)
	AND NOT IS_ENTITY_DEAD(veh)
		IF GET_NUM_MOD_KITS(veh) > 0
			SET_VEHICLE_MOD_KIT(veh, 0)
		ENDIF
		
		MOD_TYPE eMod = GET_CORONA_VEH_MOD_TYPE(veh, sCoronaVehMod)
		
		// Store default mod index
		g_sCoronaVehMod.iDefaultModIndex[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)] 	= GET_VEHICLE_MOD(veh, eMod)

		BOOL bInstalled
		
		g_sCoronaVehMod.iModIndex[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)] 			= iOption
		g_sCoronaVehMod.iModPrice[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)] 			= GET_VEHICLE_MOD_OPTION_COST(veh, sCoronaVehMod, iOption, bInstalled)

		IF iOption = -1
			REMOVE_VEHICLE_MOD(veh, eMod)
		ELSE
			IF GET_VEHICLE_MOD(veh, eMod) != iOption
				SET_VEHICLE_MOD(veh, eMod, iOption)
			ENDIF
		ENDIF
		
		g_sShopSettings.tlMenuItem_label[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)]		= GET_CORONA_VEHICLE_MOD_TEXT_LABEL(veh, sCoronaVehMod, iOption)
		g_sShopSettings.iMenuItem_cost[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)] 		= g_sCoronaVehMod.iModPrice[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)]
		
		PRINTLN("PREVIEW_AND_STORE_VEHICLE_MOD ", GET_CORONA_VEH_MOD_NAME(sCoronaVehMod), " iOption: ", iOption, " price: ", g_sCoronaVehMod.iModPrice[GET_CORONA_VEH_MOD_ARRAY_INDEX(sCoronaVehMod)])
	ELSE
		SCRIPT_ASSERT("PREVIEW_AND_STORE_VEHICLE_MOD ENTITY DOESN'T EXIST or DEAD!")
	ENDIF
ENDPROC

PROC CLEAR_ARENA_CORONA_VEHICLE_DATA()
	INT i
	FOR i = 0 TO MAX_NUM_CORONA_VEH_MOD - 1 
		g_sCoronaVehMod.iDefaultModIndex[i] = 0
		g_sCoronaVehMod.iModIndex[i] = 0
		g_sCoronaVehMod.iModPrice[i] = 0
		g_sShopSettings.tlMenuItem_label[i] = 0
		g_sShopSettings.iMenuItem_cost[i] = 0
	ENDFOR
	PRINTLN("CLEAR_ARENA_CORONA_VEHICLE_DATA called ")
ENDPROC

FUNC BOOL CAN_PLAYER_AFFORD_ALL_CORONA_VEH_MODS()
	INT i
	INT iCost
	FOR i = 0 TO MAX_NUM_CORONA_VEH_MOD - 1
		iCost += g_sCoronaVehMod.iModPrice[i]
	ENDFOR
	
	IF iCost > 0
	AND CAN_PLAYER_AFFORD_ITEM_COST(iCost)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_AFFORD_ANY_CORONA_VEH_MOD()
	INT i
	FOR i = 0 TO MAX_NUM_CORONA_VEH_MOD - 1
		IF CAN_PLAYER_AFFORD_ITEM_COST(g_sCoronaVehMod.iModPrice[i])
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC 

PROC SET_TEMP_SHOP_MENU_ITEM_DATA_FOR_CORONA(TEXT_LABEL_15 paramLabel, INT paramCost, INT iMenuOption, INT iOptionType = 0, INT iVariation = 0, INT iStatValue = 0, INT iCoronaMenuIntex = 0, BOOL bUseLabelForCatalogue = FALSE, INT iAltNum = -1)
	UNUSED_PARAMETER(iOptionType)
	UNUSED_PARAMETER(iVariation)
	UNUSED_PARAMETER(iStatValue)
	UNUSED_PARAMETER(bUseLabelForCatalogue)
	IF iCoronaMenuIntex >= 0 AND iCoronaMenuIntex < MAX_MENU_ROWS
		g_sShopSettings.iMenuItem_catalogurKey[iCoronaMenuIntex] = 0
		g_sShopSettings.tlMenuItem_label[iCoronaMenuIntex] = paramLabel
		g_sShopSettings.iMenuItem_cost[iCoronaMenuIntex] = paramCost

		// Store the catalogue key to help cut down on processing when we need to grab a key.
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
		AND SHOULD_ALLOW_THIS_SCRIPT_TO_GENERATE_CAT_KEYS()
			
			TEXT_LABEL_63 tlCatalogueKey
			TEXT_LABEL_15 tlModLabel
			
			
			IF bUseLabelForCatalogue
				tlModLabel = paramLabel
			ELSE
				tlModLabel = GET_CARMOD_MOD_TYPE_ENUM_NAME(INT_TO_ENUM(CARMOD_MOD_TYPE_ENUM, iOptionType))
				tlModLabel += "_"
				tlModLabel += iMenuOption
			ENDIF
			
			GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, tlModLabel, DUMMY_MODEL_FOR_SCRIPT, SHOP_TYPE_CARMOD, iOptionType, iVariation, iAltNum)
			
			PRINTLN("SET_TEMP_SHOP_MENU_ITEM_DATA - hash='", GET_HASH_KEY(tlCatalogueKey), ", key='", tlCatalogueKey, "', iCoronaMenuIntex=", iCoronaMenuIntex, "iCoronaMenuIntex= ", iCoronaMenuIntex)
			g_sShopSettings.iMenuItem_catalogurKey[iCoronaMenuIntex] = GET_HASH_KEY(tlCatalogueKey)
			g_sShopSettings.iMenuItem_cost[iCoronaMenuIntex] = paramCost
			
			
		ENDIF

	ELSE
		g_sShopSettings.tlMenuItem_label[iCoronaMenuIntex] = ""
		g_sShopSettings.iMenuItem_cost[iCoronaMenuIntex] = 0
		g_sShopSettings.iMenuItem_catalogurKey[iCoronaMenuIntex] = 0
	ENDIF
ENDPROC

CONST_INT CORONA_VEHICLE_MOD_TRANSACTION_INVALID 	0
CONST_INT CORONA_VEHICLE_MOD_TRANSACTION_PENDING 	1
CONST_INT CORONA_VEHICLE_MOD_TRANSACTION_SUCCESS 	2
CONST_INT CORONA_VEHICLE_MOD_TRANSACTION_FAILED 	3

INT iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_INVALID
INT iCoronaLastSuccessfulTransations = -1

FUNC BOOL BUY_CORONA_VEHICLE_MOD(VEHICLE_INDEX veh, INT iPVSlot = -1)

	IF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_FAILED
		PRINTLN("BUY_CORONA_VEHICLE_MOD - Transaction failed - TOP")
		RETURN FALSE
	ENDIF

	INT iCost
	
	MODEL_NAMES vehModel
	IF DOES_ENTITY_EXIST(veh)
	AND NOT IS_ENTITY_DEAD(veh)
		vehModel = GET_ENTITY_MODEL(veh)
	ENDIF
	
	BOOL bDoPayment = TRUE

	IF CAN_PLAYER_AFFORD_ANY_CORONA_VEH_MOD() 
	OR (iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_SUCCESS)

	
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			BOOL bTransactionReady
			
			// If we should use the transaction system trigger it.
			IF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_INVALID
			AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
				
				IF iPVSlot = -1 
					iPVSlot = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed
				ENDIF	
				VEHICLE_MOD_INVENTORY_DATA_STRUCT sInvData

				CPRINTLN(DEBUG_SHOPS, "BUY_CORONA_VEHICLE_MOD - Purchasing item for vehicle inventory slot ", iPVSlot)
				
				IF iPVSlot != -1
					IF g_MpSavedVehicles[iPVSlot].VehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
						CPRINTLN(DEBUG_SHOPS, "BUY_CORONA_VEHICLE_MOD - Model: ", GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iPVSlot].VehicleSetupMP.VehicleSetup.eModel))
					ELSE
						CPRINTLN(DEBUG_SHOPS, "BUY_CORONA_VEHICLE_MOD - Model: DUMMY_MODEL_FOR_SCRIPT")
					ENDIF
				ENDIF
				
				INT i

				FOR i = 0 TO MAX_NUM_CORONA_VEH_MOD - 1
					IF g_sCoronaVehMod.iModPrice[i] > 0
					AND g_sCoronaVehMod.iDefaultModIndex[i] != g_sCoronaVehMod.iModIndex[i]
						g_sShopSettings.iMenuItem_cost[i] = g_sCoronaVehMod.iModPrice[i]
						INT iMenuOption = g_sCoronaVehMod.iModIndex[i] + 1
						IF i = ENUM_TO_INT(CVT_MINE)
							// Fender names are out of whack with the stat value so build it here.
							iMenuOption += 5
							g_sShopSettings.tlMenuItem_label[i] = GET_CARMOD_MOD_TYPE_ENUM_NAME(CMT_FENDERS_R)
							g_sShopSettings.tlMenuItem_label[i] += "_"
							g_sShopSettings.tlMenuItem_label[i] += g_sCoronaVehMod.iModIndex[i]+iMenuOption
							PRINTLN("BUY_CORONA_VEHICLE_MOD CVT_MINE g_sCoronaVehMod.iModIndex[i]: ", g_sCoronaVehMod.iModIndex[i])
							PRINTLN("BUY_CORONA_VEHICLE_MOD CVT_MINE g_sShopSettings.tlMenuItem_label[i]: ", g_sShopSettings.tlMenuItem_label[i])						
						ENDIF
						
						SET_TEMP_SHOP_MENU_ITEM_DATA_FOR_CORONA(g_sShopSettings.tlMenuItem_label[i], g_sCoronaVehMod.iModPrice[i], iMenuOption, ENUM_TO_INT(GET_DEFAULT_MOD_TYPE_FOR_MOD_SLOT(GET_CORONA_VEH_MOD_TYPE(veh, INT_TO_ENUM(CORONA_VEH_MOD_TYPE,i)))), GET_VEHICLE_MOD_PRICE_VARIATION_FOR_CATALOGUE(vehModel), g_sCoronaVehMod.iModIndex[i] + 1, i)
						iCost = g_sCoronaVehMod.iModPrice[i]
						IF USE_SERVER_TRANSACTIONS()
							PRINTLN("BUY_CORONA_VEHICLE_MOD g_sShopSettings.iMenuItem_catalogurKey[", i, "]: ", g_sShopSettings.iMenuItem_catalogurKey[i])
							iCost = NET_GAMESERVER_GET_PRICE(g_sShopSettings.iMenuItem_catalogurKey[i], CATEGORY_VEHICLE_MOD, 1)
							g_sShopSettings.iMenuItem_cost[i] = iCost
						ENDIF
						
						IF CAN_PLAYER_AFFORD_ITEM_COST(g_sCoronaVehMod.iModPrice[i])
							IF GET_VEHICLE_MOD_INVENTORY_DATA_FOR_CATALOGUE(sInvData, iPVSlot, GET_CORONA_VEH_MOD_MENU_TYPE(veh, INT_TO_ENUM(CORONA_VEH_MOD_TYPE,i)), GET_CORONA_VEH_MOD_TYPE(veh, INT_TO_ENUM(CORONA_VEH_MOD_TYPE,i)), i, 0, veh)
						
								// Inventory item
								IF sInvData.iInventoryKey1 != 0
									bTransactionReady = FALSE
									IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_VEHICLE_MOD, g_sShopSettings.iMenuItem_catalogurKey[i], NET_SHOP_ACTION_BUY_VEHICLE, 1, g_sShopSettings.iMenuItem_cost[i], 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, sInvData.iInventoryKey1)
							            CPRINTLN(DEBUG_SHOPS, "BUY_CORONA_VEHICLE_MOD - Added inventory key 1: ", sInvData.iInventoryKey1)
										bTransactionReady = TRUE
							        ENDIF
								ENDIF

							ELSE
								PRINTLN("BUY_CORONA_VEHICLE_MOD - Failed to get vehicle mod inventory data index: ", i)
								SCRIPT_ASSERT("BUY_CORONA_VEHICLE_MOD - Failed to get vehicle mod inventory data")
							ENDIF
						
						ENDIF
					ENDIF
				ENDFOR
				
				// Checkout
				IF bTransactionReady
				AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_PENDING
					RETURN TRUE
				ELSE	
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
				
				// Transaction failed in some way so bail.
				IF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_INVALID
					PRINTLN("BUY_CORONA_VEHICLE_MOD - Transaction failed")
				ENDIF
			ENDIF
			
			IF iCoronaVehModTransactionState != CORONA_VEHICLE_MOD_TRANSACTION_PENDING
				IF GET_TOTAL_COST_OF_CORONA_VEH_MODS() >= 0
				
					IF USE_SERVER_TRANSACTIONS()
					AND iCoronaLastSuccessfulTransations != -1
						PRINTLN("[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
						NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(iCoronaLastSuccessfulTransations))
					ENDIF

					// Do not trigger purchase for free items.
					IF GET_TOTAL_COST_OF_CORONA_VEH_MODS() = 0
						bDoPayment = FALSE
					ENDIF

					IF bDoPayment
						TEXT_LABEL_15 tlStoredLabel
						INT iStoredCost, i
						FOR i = 0 TO MAX_NUM_CORONA_VEH_MOD - 1
							GET_TEMP_SHOP_MENU_ITEM_DATA(tlStoredLabel, iStoredCost, i)
							IF iStoredCost > 0
								IF DOES_ENTITY_EXIST(veh)
								AND IS_VEHICLE_DRIVEABLE(veh)
									STORE_CURRENT_SHOP_ITEM_DATA(tlStoredLabel, iStoredCost, ENUM_TO_INT(GET_ENTITY_MODEL(veh)), 1)
								ELSE
									STORE_CURRENT_SHOP_ITEM_DATA(tlStoredLabel, iStoredCost)
								ENDIF
								g_sCoronaVehMod.iDefaultModIndex[i] = g_sCoronaVehMod.iModIndex[i]
								PRINTLN("BUY_CORONA_VEHICLE_MOD g_sCoronaVehMod.iDefaultModIndex[", i, "]: ", g_sCoronaVehMod.iDefaultModIndex[i])
								SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP(CARMOD_SHOP_PERSONALMOD, TRUE, PURCHASE_CARMODS, ENUM_TO_INT(PERSONAL_CAR_MOD_VARIATION_ARENA_WARS))
							ENDIF	
						ENDFOR
					ENDIF
					
				ENDIF
				
				IF USE_SERVER_TRANSACTIONS()
					IF iCoronaLastSuccessfulTransations != -1
						DELETE_CASH_TRANSACTION(iCoronaLastSuccessfulTransations)
						iCoronaLastSuccessfulTransations = -1
					ENDIF
				ENDIF
			ELSE
				PRINTLN("BUY_CORONA_VEHICLE_MOD - iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_PENDING")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_CORONA_VEHICLE_MOD_SHOPPING_BASKET()
	IF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_PENDING
	
		INT iTransactionID = GET_BASKET_TRANSACTION_SCRIPT_INDEX()

		IF IS_CASH_TRANSACTION_COMPLETE(iTransactionID)
			IF GET_CASH_TRANSACTION_STATUS(iTransactionID) = CASH_TRANSACTION_STATUS_SUCCESS
				CPRINTLN(DEBUG_SHOPS, "PROCESS_CORONA_VEHICLE_MOD_SHOPPING_BASKET - Transaction finished - SUCCESS")
				iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_SUCCESS
				iCoronaLastSuccessfulTransations = iTransactionID
				IF USE_SERVER_TRANSACTIONS()
					DELETE_CASH_TRANSACTION(iTransactionID)
				ENDIF	
			ELSE
				CPRINTLN(DEBUG_SHOPS, "PROCESS_CORONA_VEHICLE_MOD_SHOPPING_BASKET - Transaction finished - FAILED")
				iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_FAILED
				DELETE_CASH_TRANSACTION(iTransactionID)
			ENDIF
		ENDIF
	ELIF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_SUCCESS
	OR iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_FAILED
		iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_INVALID
		CPRINTLN(DEBUG_SHOPS, "PROCESS_CORONA_VEHICLE_MOD_SHOPPING_BASKET - Ready to process next transaction")
	ENDIF
	
	IF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_SUCCESS
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ELIF iCoronaVehModTransactionState = CORONA_VEHICLE_MOD_TRANSACTION_FAILED
		PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
ENDPROC

FUNC INT GET_SP_CARMOD_MENU_WHEEL_COST(STRING strLabel, MOD_WHEEL_TYPE eWheelType, INT iWheel, BOOL bCar)

	TEXT_LABEL_15 tlLabel = strLabel

	IF IS_STRING_NULL_OR_EMPTY(tlLabel)
		IF iWheel <= 0 OR eWheelType = MWT_INVALID
			IF bCar
				tlLabel = "DEFAULT_C"
			ELSE
				tlLabel = "DEFAULT_B"
			ENDIF
		ELSE
		
			SWITCH eWheelType
				CASE MWT_SPORT 		tlLabel = "SPT_"			BREAK
				CASE MWT_MUSCLE 	tlLabel = "MUSC_"			BREAK
				CASE MWT_LOWRIDER 	tlLabel = "LORIDE_"			BREAK
				CASE MWT_SUV 		tlLabel = "SUV_"			BREAK
				CASE MWT_OFFROAD 	tlLabel = "OFFR_"			BREAK
				CASE MWT_TUNER 		tlLabel = "DRFT_"			BREAK
				CASE MWT_BIKE 		tlLabel = "BIKEW_"			BREAK
				CASE MWT_HIEND 		tlLabel = "HIEND_"			BREAK
			ENDSWITCH
			
			IF iWheel < 10
				tlLabel += "0"
			ENDIF
			
			tlLabel += iWheel
		ENDIF
	ENDIF
	
	INT iHash = GET_HASH_KEY(tlLabel)
	SWITCH iHash
		CASE HASH("HIEND_01")   RETURN 3280       BREAK
		CASE HASH("HIEND_02")   RETURN 2890       BREAK
		CASE HASH("HIEND_03")   RETURN 2850       BREAK
		CASE HASH("HIEND_04")   RETURN 3750       BREAK
		CASE HASH("HIEND_05")   RETURN 3100       BREAK
		CASE HASH("HIEND_06")   RETURN 3490       BREAK
		CASE HASH("HIEND_07")   RETURN 2990       BREAK
		CASE HASH("HIEND_08")   RETURN 3050       BREAK
		CASE HASH("HIEND_09")   RETURN 3480       BREAK
		CASE HASH("HIEND_10")   RETURN 2530       BREAK
		CASE HASH("HIEND_11")   RETURN 2580       BREAK
		CASE HASH("HIEND_12")   RETURN 3350       BREAK
		CASE HASH("HIEND_13")   RETURN 2880       BREAK
		CASE HASH("HIEND_14")   RETURN 2700       BREAK
		CASE HASH("HIEND_15")   RETURN 3650       BREAK
		CASE HASH("HIEND_16")   RETURN 4290       BREAK
		CASE HASH("HIEND_17")   RETURN 4150       BREAK
		CASE HASH("HIEND_18")   RETURN 3900       BREAK
		CASE HASH("HIEND_19")   RETURN 4000       BREAK
		CASE HASH("HIEND_20")   RETURN 4200       BREAK
		      
		CASE HASH("SPT_01")     RETURN 2100       BREAK
		CASE HASH("SPT_02")     RETURN 2280       BREAK
		CASE HASH("SPT_03")     RETURN 2450       BREAK
		CASE HASH("SPT_04")     RETURN 2390       BREAK
		CASE HASH("SPT_05")     RETURN 2300       BREAK
		CASE HASH("SPT_06")     RETURN 2550       BREAK
		CASE HASH("SPT_07")     RETURN 2080       BREAK
		CASE HASH("SPT_08")     RETURN 2150       BREAK
		CASE HASH("SPT_09")     RETURN 3260       BREAK
		CASE HASH("SPT_10")     RETURN 2810       BREAK
		CASE HASH("SPT_11")     RETURN 2200       BREAK
		CASE HASH("SPT_12")     RETURN 3250       BREAK
		CASE HASH("SPT_13")     RETURN 3000       BREAK
		CASE HASH("SPT_14")     RETURN 3280       BREAK
		CASE HASH("SPT_15")     RETURN 2900       BREAK
		CASE HASH("SPT_16")     RETURN 3050       BREAK
		CASE HASH("SPT_17")     RETURN 2490       BREAK
		CASE HASH("SPT_18")     RETURN 2100       BREAK
		CASE HASH("SPT_19")     RETURN 2660       BREAK
		CASE HASH("SPT_20")     RETURN 3250       BREAK
		CASE HASH("SPT_21")     RETURN 3550       BREAK
		CASE HASH("SPT_22")     RETURN 3380       BREAK
		CASE HASH("SPT_23")     RETURN 3280       BREAK
		CASE HASH("SPT_24")     RETURN 3100       BREAK
		CASE HASH("SPT_25")     RETURN 3400       BREAK
		      
		CASE HASH("MUSC_01")    RETURN 1290       BREAK
		CASE HASH("MUSC_02")    RETURN 2450       BREAK
		CASE HASH("MUSC_03")    RETURN 1500       BREAK
		CASE HASH("MUSC_04")    RETURN 1620       BREAK
		CASE HASH("MUSC_05")    RETURN 1555       BREAK
		CASE HASH("MUSC_06")    RETURN 1700       BREAK
		CASE HASH("MUSC_07")    RETURN 1780       BREAK
		CASE HASH("MUSC_08")    RETURN 1800       BREAK
		CASE HASH("MUSC_09")    RETURN 1590       BREAK
		CASE HASH("MUSC_10")    RETURN 2380       BREAK
		CASE HASH("MUSC_11")    RETURN 1540       BREAK
		CASE HASH("MUSC_12")    RETURN 1580       BREAK
		CASE HASH("MUSC_13")    RETURN 1600       BREAK
		CASE HASH("MUSC_14")    RETURN 1590       BREAK
		CASE HASH("MUSC_15")    RETURN 1775       BREAK
		CASE HASH("MUSC_16")    RETURN 1540       BREAK
		CASE HASH("MUSC_17")    RETURN 1510       BREAK
		CASE HASH("MUSC_18")    RETURN 1820       BREAK
		CASE HASH("MUSC_19")    RETURN 1550       BREAK
		CASE HASH("MUSC_20")    RETURN 1875       BREAK
		      
		CASE HASH("LORIDE_01")  RETURN 1980       BREAK
		CASE HASH("LORIDE_02")  RETURN 2150       BREAK
		CASE HASH("LORIDE_03")  RETURN 2200       BREAK
		CASE HASH("LORIDE_04")  RETURN 2050       BREAK
		CASE HASH("LORIDE_05")  RETURN 2190       BREAK
		CASE HASH("LORIDE_06")  RETURN 2220       BREAK
		CASE HASH("LORIDE_07")  RETURN 2290       BREAK
		CASE HASH("LORIDE_08")  RETURN 2600       BREAK
		CASE HASH("LORIDE_09")  RETURN 2550       BREAK
		CASE HASH("LORIDE_10")  RETURN 2330       BREAK
		CASE HASH("LORIDE_11")  RETURN 2380       BREAK
		CASE HASH("LORIDE_12")  RETURN 3000       BREAK
		CASE HASH("LORIDE_13")  RETURN 2450       BREAK
		CASE HASH("LORIDE_14")  RETURN 2500       BREAK
		CASE HASH("LORIDE_15")  RETURN 2850       BREAK
		      
		CASE HASH("SUV_01")     RETURN 3180       BREAK
		CASE HASH("SUV_02")     RETURN 3200       BREAK
		CASE HASH("SUV_03")     RETURN 3050       BREAK
		CASE HASH("SUV_04")     RETURN 3220       BREAK
		CASE HASH("SUV_05")     RETURN 3000       BREAK
		CASE HASH("SUV_06")     RETURN 3450       BREAK
		CASE HASH("SUV_07")     RETURN 3490       BREAK
		CASE HASH("SUV_08")     RETURN 3100       BREAK
		CASE HASH("SUV_09")     RETURN 3150       BREAK
		CASE HASH("SUV_10")     RETURN 3200       BREAK
		CASE HASH("SUV_11")     RETURN 3090       BREAK
		CASE HASH("SUV_12")     RETURN 3300       BREAK
		CASE HASH("SUV_13")     RETURN 3100       BREAK
		CASE HASH("SUV_14")     RETURN 3600       BREAK
		CASE HASH("SUV_15")     RETURN 3250       BREAK
		CASE HASH("SUV_16")     RETURN 3150       BREAK
		CASE HASH("SUV_17")     RETURN 3380       BREAK
		CASE HASH("SUV_18")     RETURN 3190       BREAK
		CASE HASH("SUV_19")     RETURN 3375       BREAK
		CASE HASH("SUV_20")     RETURN 3085       BREAK
		      
		CASE HASH("OFFR_01")    RETURN 1850       BREAK
		CASE HASH("OFFR_02")    RETURN 2050       BREAK
		CASE HASH("OFFR_03")    RETURN 1890       BREAK
		CASE HASH("OFFR_04")    RETURN 2100       BREAK
		CASE HASH("OFFR_05")    RETURN 2590       BREAK
		CASE HASH("OFFR_06")    RETURN 2190       BREAK
		CASE HASH("OFFR_07")    RETURN 2000       BREAK
		CASE HASH("OFFR_08")    RETURN 2200       BREAK
		CASE HASH("OFFR_09")    RETURN 1770       BREAK
		CASE HASH("OFFR_10")    RETURN 1800       BREAK
		      
		CASE HASH("DRFT_01")    RETURN 2450       BREAK
		CASE HASH("DRFT_02")    RETURN 2500       BREAK
		CASE HASH("DRFT_03")    RETURN 2490       BREAK
		CASE HASH("DRFT_04")    RETURN 2600       BREAK
		CASE HASH("DRFT_05")    RETURN 2620       BREAK
		CASE HASH("DRFT_06")    RETURN 2700       BREAK
		CASE HASH("DRFT_07")    RETURN 2550       BREAK
		CASE HASH("DRFT_08")    RETURN 2770       BREAK
		CASE HASH("DRFT_09")    RETURN 2580       BREAK
		CASE HASH("DRFT_10")    RETURN 2820       BREAK
		CASE HASH("DRFT_11")    RETURN 2855       BREAK
		CASE HASH("DRFT_12")    RETURN 2890       BREAK
		CASE HASH("DRFT_13")    RETURN 2400       BREAK
		CASE HASH("DRFT_14")    RETURN 2490       BREAK
		CASE HASH("DRFT_15")    RETURN 2520       BREAK
		CASE HASH("DRFT_16")    RETURN 2550       BREAK
		CASE HASH("DRFT_17")    RETURN 2610       BREAK
		CASE HASH("DRFT_18")    RETURN 2660       BREAK
		CASE HASH("DRFT_19")    RETURN 2720       BREAK
		CASE HASH("DRFT_20")    RETURN 2490       BREAK
		CASE HASH("DRFT_21")    RETURN 2440       BREAK
		CASE HASH("DRFT_22")    RETURN 2500       BREAK
		CASE HASH("DRFT_23")    RETURN 2585       BREAK
		CASE HASH("DRFT_24")    RETURN 2840       BREAK
		      
		CASE HASH("BIKEW_01")   RETURN 1780       BREAK
		CASE HASH("BIKEW_02")   RETURN 1800       BREAK
		CASE HASH("BIKEW_03")   RETURN 1820       BREAK
		CASE HASH("BIKEW_04")   RETURN 1850       BREAK
		CASE HASH("BIKEW_05")   RETURN 1750       BREAK
		CASE HASH("BIKEW_06")   RETURN 1690       BREAK
		CASE HASH("BIKEW_07")   RETURN 1890       BREAK
		CASE HASH("BIKEW_08")   RETURN 2180       BREAK
		CASE HASH("BIKEW_09")   RETURN 1890       BREAK
		CASE HASH("BIKEW_10")   RETURN 1950       BREAK
		CASE HASH("BIKEW_11")   RETURN 2500       BREAK
		CASE HASH("BIKEW_12")   RETURN 2600       BREAK
		CASE HASH("BIKEW_13")   RETURN 2550       BREAK
		
		CASE HASH("DEFAULT_C")	RETURN 1500		BREAK
		CASE HASH("DEFAULT_B")	RETURN 800		BREAK
		
		CASE HASH("SMOD_WHL1")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_OG_Hunnets_Chrome	BREAK // OG Hunnets (Chrome)
		CASE HASH("SMOD_WHL2")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_OG_Hunnets_Painted	BREAK // OG Hunnets (Painted)
		CASE HASH("SMOD_WHL3")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_KnockOffs_Chrome	BREAK // Knock-Offs (Chrome)
		CASE HASH("SMOD_WHL4")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_KnockOffs_Painted	BREAK // Knock-Offs (Painted)
		CASE HASH("SMOD_WHL5")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spoked_Out_Chrome	BREAK // Spoked Out (Chrome)
		CASE HASH("SMOD_WHL6")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spoked_Out_Painted	BREAK // Spoked Out (Painted)
		CASE HASH("SMOD_WHL7")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Vintage_Wire_Chrome	BREAK // Vintage Wire (Chrome)
		CASE HASH("SMOD_WHL8")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Vintage_Wire_Painted	BREAK // Vintage Wire (Painted)
		CASE HASH("SMOD_WHL9")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Smoothie_Chrome	BREAK // Smoothie (Chrome)
		CASE HASH("SMOD_WHL10")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Smoothie_Chrome_Lip	BREAK // Smoothie (Chrome Lip)
		CASE HASH("SMOD_WHL11")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Smoothie_Painted	BREAK // Smoothie (Painted)
		CASE HASH("SMOD_WHL12")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Rod_Me_Up_Chrome	BREAK // Rod Me Up(Chrome)
		CASE HASH("SMOD_WHL13")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Rod_Me_Up_Chrome_Lip	BREAK // Rod Me Up (Chome Lip)
		CASE HASH("SMOD_WHL14")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Rod_Me_Up_Painted	BREAK // Rod Me Up (Painted)
		CASE HASH("SMOD_WHL15")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Clean	BREAK // Clean
		CASE HASH("SMOD_WHL16")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Lotta_Chrome	BREAK // Lotta Chrome
		CASE HASH("SMOD_WHL17")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spindles	BREAK // Spindles
		CASE HASH("SMOD_WHL18")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Viking	BREAK // Viking 
		CASE HASH("SMOD_WHL19")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Triple_Spoke	BREAK // Triple Spoke
		CASE HASH("SMOD_WHL20")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Pharohe	BREAK // Pharohe
		CASE HASH("SMOD_WHL21")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Tiger_Style	BREAK // Tiger Style
		CASE HASH("SMOD_WHL22")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Three_Wheelin	BREAK // Three Wheelin
		CASE HASH("SMOD_WHL23")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Big_Bar	BREAK // Big Bar
		CASE HASH("SMOD_WHL24")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Biohazard	BREAK // Biohazard
		CASE HASH("SMOD_WHL25")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Waves	BREAK // Waves
		CASE HASH("SMOD_WHL26")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Lick_Lick	BREAK // Lick Lick
		CASE HASH("SMOD_WHL27")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spiralizer	BREAK // Spiralizer
		CASE HASH("SMOD_WHL28")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Hypotics	BREAK // Hypotics
		CASE HASH("SMOD_WHL29")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Psychodelic	BREAK // Psycho-Delic
		CASE HASH("SMOD_WHL30")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Half_Cut	BREAK // Half Cut
		CASE HASH("SMOD_WHL31")	RETURN g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Super_Electric	BREAK // Super Electric
	ENDSWITCH
	
	INT iHashTwo = GET_HASH_KEY(tlLabel)
	SWITCH iHashTwo
		CASE HASH("SMOD2_WHL1")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_OG_Hunnets	BREAK // Chrome OG Hunnets
		CASE HASH("SMOD2_WHL2")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_OG_Hunnets	BREAK // Gold OG Hunnets
		CASE HASH("SMOD2_WHL3")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Wires		BREAK // Chrome Wires
		CASE HASH("SMOD2_WHL4")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Wires			BREAK // Gold wires
		CASE HASH("SMOD2_WHL5")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Spoked_Out	BREAK // Chrome Spoked Out
		CASE HASH("SMOD2_WHL6")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Spoked_Out	BREAK // Gold Spoked Out
		CASE HASH("SMOD2_WHL7")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_KnockOffs	BREAK // Chrome Knock-Offs
		CASE HASH("SMOD2_WHL8")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_KnockOffs		BREAK // Gold Knock-Offs
		CASE HASH("SMOD2_WHL9")		RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Bigger_Worm	BREAK // Chrome Bigger Worm
		CASE HASH("SMOD2_WHL10")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Bigger_Worm	BREAK // Gold Bigger Worm
		CASE HASH("SMOD2_WHL11")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Vintage_Wire	BREAK // Chrome Vintage Wire
		CASE HASH("SMOD2_WHL12")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Vintage_Wire	BREAK // Gold Vintage Wire
		CASE HASH("SMOD2_WHL13")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Classic_Wire	BREAK // Chrome Classic Wire
		CASE HASH("SMOD2_WHL14")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Classic_Wire	BREAK // Gold Classic Wire
		CASE HASH("SMOD2_WHL15")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Smoothie	BREAK // Chrome Smoothie
		CASE HASH("SMOD2_WHL16")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Smoothie		BREAK // Gold Smoothie
		CASE HASH("SMOD2_WHL17")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Classic_Rod	BREAK // Chrome Classic Rod
		CASE HASH("SMOD2_WHL18")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Classic_Rod	BREAK // Gold Classic Rod 
		CASE HASH("SMOD2_WHL19")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Dollar		BREAK // Chrome Dollar
		CASE HASH("SMOD2_WHL20")	RETURN g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Dollar		BREAK // Gold Dollar
		CASE HASH("SMOD2_WHL21")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL22")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL23")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL24")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL25")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL26")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL27")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL28")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL29")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL30")	RETURN 12345	BREAK // 
		CASE HASH("SMOD2_WHL31")	RETURN 12345	BREAK // 
	ENDSWITCH
	// Chromies
	IF IS_STRING_NULL_OR_EMPTY(strLabel)
	
		SWITCH eWheelType
			CASE MWT_SPORT 		tlLabel = "RWD_SPT_"			BREAK
			CASE MWT_MUSCLE 	tlLabel = "RWD_MUSC_"			BREAK
			CASE MWT_LOWRIDER 	tlLabel = "RWD_LORIDE_"			BREAK
			CASE MWT_SUV 		tlLabel = "RWD_SUV_"			BREAK
			CASE MWT_OFFROAD 	tlLabel = "RWD_OFFR_"			BREAK
			CASE MWT_TUNER 		tlLabel = "RWD_DRFT_"			BREAK
			CASE MWT_BIKE 		tlLabel = "RWD_BIKEW_"			BREAK
			CASE MWT_HIEND 		tlLabel = "RWD_HIEND_"			BREAK
		ENDSWITCH
	
		IF eWheelType = MWT_SPORT AND iWheel > 25
			iWheel -= 25
		ELIF eWheelType = MWT_MUSCLE AND iWheel > 18
			iWheel -= 18
		ELIF eWheelType = MWT_LOWRIDER AND iWheel > 15
			iWheel -= 15
		ELIF eWheelType = MWT_SUV AND iWheel > 19
			iWheel -= 19
		ELIF eWheelType = MWT_OFFROAD AND iWheel > 10
			iWheel -= 10
		ELIF eWheelType = MWT_TUNER AND iWheel > 24
			iWheel -= 24
		ELIF eWheelType = MWT_BIKE AND iWheel > 13
			iWheel -= 13
		ELIF eWheelType = MWT_HIEND AND iWheel > 20
			iWheel -= 20
		ENDIF
		
		IF iWheel < 10
			tlLabel += "0"
		ENDIF
		
		tlLabel += iWheel
		
		iHash = GET_HASH_KEY(tlLabel)
	ENDIF
	
	SWITCH iHash
		CASE HASH("RWD_HIEND_01")	RETURN 67860	BREAK // Shadow (CHROME)
		CASE HASH("RWD_HIEND_02")	RETURN 71150	BREAK // Hypher (CHROME)
		CASE HASH("RWD_HIEND_03")	RETURN 75850	BREAK // Blade (CHROME)
		CASE HASH("RWD_HIEND_04")	RETURN 81725	BREAK // Diamond (CHROME)
		CASE HASH("RWD_HIEND_05")	RETURN 72325	BREAK // Supa Gee (CHROME)
		CASE HASH("RWD_HIEND_06")	RETURN 69975	BREAK // Chromatic Z (CHROME) 
		CASE HASH("RWD_HIEND_07")	RETURN 70210	BREAK // Mercie (CHROME)
		CASE HASH("RWD_HIEND_08")	RETURN 71855	BREAK // Obey RS (CHROME)
		CASE HASH("RWD_HIEND_09")	RETURN 74205	BREAK // GT Chrome (CHROME)
		CASE HASH("RWD_HIEND_10")	RETURN 72090	BREAK // Cheetah RR (CHROME)
		CASE HASH("RWD_HIEND_11")	RETURN 72560	BREAK // Solar (CHROME)
		CASE HASH("RWD_HIEND_12")	RETURN 76320	BREAK // Split Ten (CHROME)
		CASE HASH("RWD_HIEND_13")	RETURN 73500	BREAK // Dash VIP (CHROME)
		CASE HASH("RWD_HIEND_14")	RETURN 74910	BREAK // LozSpeed Ten (CHROME)
		CASE HASH("RWD_HIEND_15")	RETURN 82665	BREAK // Carbon Inferno (CHROME)
		CASE HASH("RWD_HIEND_16")	RETURN 81020	BREAK // Carbon Shadow (CHROME)
		CASE HASH("RWD_HIEND_17")	RETURN 82900	BREAK // Carbonic Z (CHROME)
		CASE HASH("RWD_HIEND_18")	RETURN 83135	BREAK // Carbon Solar (CHROME)
		CASE HASH("RWD_HIEND_19")	RETURN 83370	BREAK // Cheetah Carbon R (CHROME)
		CASE HASH("RWD_HIEND_20")	RETURN 83605	BREAK // Carbon S Racer (CHROME)
		CASE HASH("RWD_SPT_01")		RETURN 56580	BREAK // Inferno (CHROME)
		CASE HASH("RWD_SPT_02")		RETURN 67155	BREAK // Deep Five (CHROME)
		CASE HASH("RWD_SPT_03")		RETURN 67625	BREAK // Lozspeed Mk.V (CHROME)
		CASE HASH("RWD_SPT_04")		RETURN 67390	BREAK // Diamond Cut (CHROME)
		CASE HASH("RWD_SPT_05")		RETURN 57755	BREAK // Chrono (CHROME)
		CASE HASH("RWD_SPT_06")		RETURN 58460	BREAK // Feroci RR (CHROME)
		CASE HASH("RWD_SPT_07")		RETURN 56110	BREAK // FiftyNine (CHROME)
		CASE HASH("RWD_SPT_08")		RETURN 56815	BREAK // Mercie (CHROME)
		CASE HASH("RWD_SPT_09")		RETURN 69270	BREAK // Synthetic Z (CHROME)
		CASE HASH("RWD_SPT_10")		RETURN 68330	BREAK // Organic Type 0 (CHROME)
		CASE HASH("RWD_SPT_11")		RETURN 57050	BREAK // Endo v.1 (CHROME)
		CASE HASH("RWD_SPT_12")		RETURN 80315	BREAK // GT One (CHROME)
		CASE HASH("RWD_SPT_13")		RETURN 80080	BREAK // Duper 7 (CHROME)
		CASE HASH("RWD_SPT_14")		RETURN 69505	BREAK // Uzer (CHROME)
		CASE HASH("RWD_SPT_15")		RETURN 69035	BREAK // GroundRide (CHROME)
		CASE HASH("RWD_SPT_16")		RETURN 69740	BREAK // S Racer (CHROME)
		CASE HASH("RWD_SPT_17")		RETURN 78435	BREAK // Venum (CHROME)
		CASE HASH("RWD_SPT_18")		RETURN 66215	BREAK // Cosmo (CHROME)
		CASE HASH("RWD_SPT_19")		RETURN 68095	BREAK // Dash VIP (CHROME)
		CASE HASH("RWD_SPT_20")		RETURN 70445	BREAK // Ice Kid (CHROME)
		CASE HASH("RWD_SPT_21")		RETURN 82430	BREAK // Ruff Weld (CHROME)
		CASE HASH("RWD_SPT_22")		RETURN 71385	BREAK // Wangan Master (CHROME)
		CASE HASH("RWD_SPT_23")		RETURN 80785	BREAK // Super Five (CHROME)
		CASE HASH("RWD_SPT_24")		RETURN 79610	BREAK // Endo v.2 (CHROME)
		CASE HASH("RWD_SPT_25")		RETURN 81960	BREAK // Split Six (CHROME)
		CASE HASH("RWD_MUSC_01")	RETURN 50235	BREAK // Classic Five (CHROME)
		CASE HASH("RWD_MUSC_02")	RETURN 66450	BREAK // Dukes (CHROME)
		CASE HASH("RWD_MUSC_03")	RETURN 58930	BREAK // Muscle Freak (CHROME)
		CASE HASH("RWD_MUSC_04")	RETURN 50705	BREAK // Kracka (CHROME)
		CASE HASH("RWD_MUSC_05")	RETURN 62925	BREAK // Azreal (CHROME)
		CASE HASH("RWD_MUSC_06")	RETURN 64570	BREAK // Mecha (CHROME)
		CASE HASH("RWD_MUSC_07")	RETURN 77025	BREAK // Black Top (CHROME)
		CASE HASH("RWD_MUSC_08")	RETURN 53290	BREAK // Drag SPL (CHROME)
		CASE HASH("RWD_MUSC_09")	RETURN 62220	BREAK // Revolver (CHROME)
		CASE HASH("RWD_MUSC_10")	RETURN 77965	BREAK // Classic Rod (CHROME)
		CASE HASH("RWD_MUSC_11")	RETURN 51175	BREAK // Fairlie (CHROME)
		CASE HASH("RWD_MUSC_12")	RETURN 52585	BREAK // Spooner (CHROME)
		CASE HASH("RWD_MUSC_13")	RETURN 63630	BREAK // Five Star (CHROME)
		CASE HASH("RWD_MUSC_14")	RETURN 63865	BREAK // Old School (CHROME)
		CASE HASH("RWD_MUSC_15")	RETURN 75615	BREAK // El Jefe (CHROME)
		CASE HASH("RWD_MUSC_16")	RETURN 52115	BREAK // Dodman (CHROME)
		CASE HASH("RWD_MUSC_17")	RETURN 59635	BREAK // Six Gun (CHROME)
		CASE HASH("RWD_MUSC_18")	RETURN 62850	BREAK // Foster (CHROME)
		CASE HASH("RWD_MUSC_19")	RETURN 65395	BREAK // Split Six (CHROME)
		CASE HASH("RWD_MUSC_20")	RETURN 73735	BREAK // Mercenary (CHROME)
		CASE HASH("RWD_LORIDE_01")	RETURN 62455	BREAK // Flare (CHROME)
		CASE HASH("RWD_LORIDE_02")	RETURN 51880	BREAK // Wired (CHROME)
		CASE HASH("RWD_LORIDE_03")	RETURN 53055	BREAK // Triple Golds (CHROME)
		CASE HASH("RWD_LORIDE_04")	RETURN 52350	BREAK // Big Worm (CHROME)
		CASE HASH("RWD_LORIDE_05")	RETURN 61280	BREAK // Seven Fives (CHROME)
		CASE HASH("RWD_LORIDE_06")	RETURN 63395	BREAK // Split Six (CHROME)
		CASE HASH("RWD_LORIDE_07")	RETURN 60105	BREAK // Fresh Mesh (CHROME)
		CASE HASH("RWD_LORIDE_08")	RETURN 77260	BREAK // Lead Sled (CHROME)
		CASE HASH("RWD_LORIDE_09")	RETURN 75380	BREAK // Turbine (CHROME)
		CASE HASH("RWD_LORIDE_10")	RETURN 74675	BREAK // Super Fin (CHROME)
		CASE HASH("RWD_LORIDE_11")	RETURN 78200	BREAK // Classic Rod (CHROME)
		CASE HASH("RWD_LORIDE_12")	RETURN 79140	BREAK // Dollar (CHROME)
		CASE HASH("RWD_LORIDE_13")	RETURN 66685	BREAK // Dukes (CHROME)
		CASE HASH("RWD_LORIDE_14")	RETURN 73970	BREAK // Low Five (CHROME)
		CASE HASH("RWD_LORIDE_15")	RETURN 64100	BREAK // Gooch (CHROME)
		CASE HASH("RWD_SUV_01")		RETURN 79845	BREAK // VIP (CHROME)
		CASE HASH("RWD_SUV_02")		RETURN 56345	BREAK // Benefactor (CHROME)
		CASE HASH("RWD_SUV_03")		RETURN 65980	BREAK // Cosmo (CHROME)
		CASE HASH("RWD_SUV_04")		RETURN 68800	BREAK // Bippu (CHROME)
		CASE HASH("RWD_SUV_05")		RETURN 57520	BREAK // Royal Six (CHROME)
		CASE HASH("RWD_SUV_06")		RETURN 76790	BREAK // Fagorme (CHROME)
		CASE HASH("RWD_SUV_07")		RETURN 65745	BREAK // Deluxe (CHROME)
		CASE HASH("RWD_SUV_08")		RETURN 81490	BREAK // Iced Out (CHROME)
		CASE HASH("RWD_SUV_09")		RETURN 68565	BREAK // Cognoscenti (CHROME)
		CASE HASH("RWD_SUV_10")		RETURN 70915	BREAK // LozSpeed Ten (CHROME)
		CASE HASH("RWD_SUV_11")		RETURN 76555	BREAK // Supernova (CHROME)
		CASE HASH("RWD_SUV_12")		RETURN 58225	BREAK // Obey RS (CHROME)
		CASE HASH("RWD_SUV_13")		RETURN 77495	BREAK // LozSpeed Baller (CHROME)
		CASE HASH("RWD_SUV_14")		RETURN 82195	BREAK // Extravaganzo (CHROME)
		CASE HASH("RWD_SUV_15")		RETURN 79375	BREAK // Split Six (CHROME)
		CASE HASH("RWD_SUV_16")		RETURN 80550	BREAK // Empowered (CHROME)
		CASE HASH("RWD_SUV_17")		RETURN 81255	BREAK // Sunrise (CHROME)
		CASE HASH("RWD_SUV_18")		RETURN 70680	BREAK // Dash VIP (CHROME)
		CASE HASH("RWD_SUV_19")		RETURN 66920	BREAK // Cutter (CHROME)
		CASE HASH("RWD_SUV_20")		RETURN 79375	BREAK // Split Six (CHROME)
		CASE HASH("RWD_OFFR_01")	RETURN 55640	BREAK // Raider (CHROME)
		CASE HASH("RWD_OFFR_02")	RETURN 55405	BREAK // Mudslinger (CHROME)
		CASE HASH("RWD_OFFR_03")	RETURN 58695	BREAK // Nevis (CHROME)
		CASE HASH("RWD_OFFR_04")	RETURN 63160	BREAK // Cairngorm (CHROME)
		CASE HASH("RWD_OFFR_05")	RETURN 52820	BREAK // Amazon (CHROME)
		CASE HASH("RWD_OFFR_06")	RETURN 76085	BREAK // Challenger (CHROME)
		CASE HASH("RWD_OFFR_07")	RETURN 72795	BREAK // Dune Basher (CHROME)
		CASE HASH("RWD_OFFR_08")	RETURN 65040	BREAK // Five Star (CHROME)
		CASE HASH("RWD_OFFR_09")	RETURN 53525	BREAK // Rock Crawler (CHROME)
		CASE HASH("RWD_OFFR_10")	RETURN 61750	BREAK // Mil Spec Steelie (CHROME)
		CASE HASH("RWD_DRFT_01")	RETURN 65510	BREAK // Cosmo (CHROME)
		CASE HASH("RWD_DRFT_02")	RETURN 57990	BREAK // Super Mesh (CHROME)
		CASE HASH("RWD_DRFT_03")	RETURN 54700	BREAK // Outsider (CHROME)
		CASE HASH("RWD_DRFT_04")	RETURN 60340	BREAK // Rollas (CHROME)
		CASE HASH("RWD_DRFT_05")	RETURN 54230	BREAK // Driftmeister (CHROME)
		CASE HASH("RWD_DRFT_06")	RETURN 64335	BREAK // Slicer (CHROME)
		CASE HASH("RWD_DRFT_07")	RETURN 74440	BREAK // El Quatro (CHROME)
		CASE HASH("RWD_DRFT_08")	RETURN 78905	BREAK // Dubbed (CHROME)
		CASE HASH("RWD_DRFT_09")	RETURN 61515	BREAK // Five Star (CHROME)
		CASE HASH("RWD_DRFT_10")	RETURN 77730	BREAK // Slideways (CHROME)
		CASE HASH("RWD_DRFT_11")	RETURN 65275	BREAK // Apex (CHROME)
		CASE HASH("RWD_DRFT_12")	RETURN 55170	BREAK // Stanced EG (CHROME)
		CASE HASH("RWD_DRFT_13")	RETURN 59165	BREAK // Countersteer (CHROME)
		CASE HASH("RWD_DRFT_14")	RETURN 59870	BREAK // Endo v.1 (CHROME)
		CASE HASH("RWD_DRFT_15")	RETURN 53760	BREAK // Endo v.2 Dish (CHROME)
		CASE HASH("RWD_DRFT_16")	RETURN 60810	BREAK // Gruppe Z (CHROME)
		CASE HASH("RWD_DRFT_17")	RETURN 64805	BREAK // Choku-Dori (CHROME)
		CASE HASH("RWD_DRFT_18")	RETURN 53995	BREAK // Chicane (CHROME)
		CASE HASH("RWD_DRFT_19")	RETURN 54935	BREAK // Saisoku (CHROME)
		CASE HASH("RWD_DRFT_20")	RETURN 54465	BREAK // Dished Eight (CHROME)
		CASE HASH("RWD_DRFT_21")	RETURN 51645	BREAK // Fujiwara (CHROME)
		CASE HASH("RWD_DRFT_22")	RETURN 60575	BREAK // Zokusha (CHROME)
		CASE HASH("RWD_DRFT_23")	RETURN 75145	BREAK // Battle VIII (CHROME)
		CASE HASH("RWD_DRFT_24")	RETURN 78670	BREAK // Rally Master (CHROME)
		CASE HASH("RWD_BIKEW_01")	RETURN 16980	BREAK // Speedway (CHROME)
		CASE HASH("RWD_BIKEW_02")	RETURN 17137	BREAK // Street Special (CHROME)
		CASE HASH("RWD_BIKEW_03")	RETURN 18625	BREAK // Racer (CHROME)
		CASE HASH("RWD_BIKEW_04")	RETURN 19095	BREAK // Track Star (CHROME)
		CASE HASH("RWD_BIKEW_05")	RETURN 16823	BREAK // Overlord (CHROME)
		CASE HASH("RWD_BIKEW_06")	RETURN 16667	BREAK // Trident (CHROME)
		CASE HASH("RWD_BIKEW_07")	RETURN 19800	BREAK // Triple Threat (CHROME)
		CASE HASH("RWD_BIKEW_08")	RETURN 20897	BREAK // Stilleto (CHROME)
		CASE HASH("RWD_BIKEW_09")	RETURN 20348	BREAK // Wires (CHROME)
		CASE HASH("RWD_BIKEW_10")	RETURN 20662	BREAK // Bobber (CHROME)
		CASE HASH("RWD_BIKEW_11")	RETURN 23873	BREAK // Solidus (CHROME)
		CASE HASH("RWD_BIKEW_12")	RETURN 24422	BREAK // Ice Shield (CHROME)
		CASE HASH("RWD_BIKEW_13")	RETURN 24343	BREAK // Loops (CHROME)
		CASE HASH("BIKEW_14")		RETURN g_sMPTunables.iBiker_CarMods_Romper_Racing    	BREAK // Romper Racing
		CASE HASH("BIKEW_15")   	RETURN g_sMPTunables.iBiker_CarMods_Warp_Drive       	BREAK // Warp Drive
		CASE HASH("BIKEW_16")   	RETURN g_sMPTunables.iBiker_CarMods_Snowflake        	BREAK // Snowflake
		CASE HASH("BIKEW_17")   	RETURN g_sMPTunables.iBiker_CarMods_Holy_Spoke       	BREAK // Holy Spoke
		CASE HASH("BIKEW_18")   	RETURN g_sMPTunables.iBiker_CarMods_Old_Skool_Triple 	BREAK // Old Skool Triple
		CASE HASH("BIKEW_19")   	RETURN g_sMPTunables.iBiker_CarMods_Futura           	BREAK // Futura
		CASE HASH("BIKEW_20")   	RETURN g_sMPTunables.iBiker_CarMods_Quarter_Mile_King	BREAK // Quarter Mile King
		CASE HASH("BIKEW_21")   	RETURN g_sMPTunables.iBiker_CarMods_Cartwheel        	BREAK // Cartwheel
		CASE HASH("BIKEW_22")   	RETURN g_sMPTunables.iBiker_CarMods_Double_Five      	BREAK // Double Five
		CASE HASH("BIKEW_23")   	RETURN g_sMPTunables.iBiker_CarMods_Shuriken         	BREAK // Shuriken
		CASE HASH("BIKEW_24")   	RETURN g_sMPTunables.iBiker_CarMods_Simple_Six       	BREAK // Simple Six
		CASE HASH("BIKEW_25")   	RETURN g_sMPTunables.iBiker_CarMods_Celtic           	BREAK // Celtic
		CASE HASH("BIKEW_26")   	RETURN g_sMPTunables.iBiker_CarMods_Razer            	BREAK // Razer
		CASE HASH("BIKEW_27")   	RETURN g_sMPTunables.iBiker_CarMods_Twisted          	BREAK // Twisted
		CASE HASH("BIKEW_28")   	RETURN g_sMPTunables.iBiker_CarMods_Morning_Star     	BREAK // Morning Star
		CASE HASH("BIKEW_29")   	RETURN g_sMPTunables.iBiker_CarMods_Jagged_Spokes    	BREAK // Jagged Spokes
		CASE HASH("BIKEW_30")   	RETURN g_sMPTunables.iBiker_CarMods_Eidolon          	BREAK // Eidolon
		CASE HASH("BIKEW_31")   	RETURN g_sMPTunables.iBiker_CarMods_Enigma           	BREAK // Enigma
		CASE HASH("BIKEW_32")   	RETURN g_sMPTunables.iBiker_CarMods_Big_Spokes       	BREAK // Big Spokes
		CASE HASH("BIKEW_33")   	RETURN g_sMPTunables.iBiker_CarMods_Webs             	BREAK // Webs
		CASE HASH("BIKEW_34")		RETURN g_sMPTunables.iBiker_CarMods_Hotplate         	BREAK // Hotplate
		CASE HASH("BIKEW_35")   	RETURN g_sMPTunables.iBiker_CarMods_Bobsta           	BREAK // Bobsta
		CASE HASH("BIKEW_36")   	RETURN g_sMPTunables.iBiker_CarMods_Grouch           	BREAK // Grouch
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		PRINTLN("GET_CARMOD_MENU_WHEEL_COST - missing label: ", tlLabel)
		SCRIPT_ASSERT("GET_CARMOD_MENU_WHEEL_COST - missing label. Tell Kenneth R.")
	#ENDIF
	
	RETURN 1500
ENDFUNC

FUNC BOOL ARE_CHROME_WHEELS_FREE(INT iLabelHash)

	SWITCH iLabelHash
		CASE HASH("RWD_HIEND_01") // Shadow (CHROME)
		CASE HASH("RWD_HIEND_02") // Hypher (CHROME)
		CASE HASH("RWD_HIEND_03") // Blade (CHROME)
		CASE HASH("RWD_HIEND_04") // Diamond (CHROME)
		CASE HASH("RWD_HIEND_05") // Supa Gee (CHROME)
		CASE HASH("RWD_HIEND_06") // Chromatic Z (CHROME) 
		CASE HASH("RWD_HIEND_07") // Mercie (CHROME)
		CASE HASH("RWD_HIEND_08") // Obey RS (CHROME)
		CASE HASH("RWD_HIEND_09") // GT Chrome (CHROME)
		CASE HASH("RWD_HIEND_10") // Cheetah RR (CHROME)
		CASE HASH("RWD_HIEND_11") // Solar (CHROME)
		CASE HASH("RWD_HIEND_12") // Split Ten (CHROME)
		CASE HASH("RWD_HIEND_13") // Dash VIP (CHROME)
		CASE HASH("RWD_HIEND_14") // LozSpeed Ten (CHROME)
		CASE HASH("RWD_HIEND_15") // Carbon Inferno (CHROME)
		CASE HASH("RWD_HIEND_16") // Carbon Shadow (CHROME)
		CASE HASH("RWD_HIEND_17") // Carbonic Z (CHROME)
		CASE HASH("RWD_HIEND_18") // Carbon Solar (CHROME)
		CASE HASH("RWD_HIEND_19") // Cheetah Carbon R (CHROME)
		CASE HASH("RWD_HIEND_20") // Carbon S Racer (CHROME)
			IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FINISH_HEISTS)>=GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FINISH_HEISTS,AWARDPOSITIONS_PLATINUM,0 )
				RETURN TRUE 
			ELSE
			
				RETURN FALSE 
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_SPT_01") // Inferno (CHROME)
		CASE HASH("RWD_SPT_02") // Deep Five (CHROME)
		CASE HASH("RWD_SPT_03") // Lozspeed Mk.V (CHROME)
		CASE HASH("RWD_SPT_04") // Diamond Cut (CHROME)
		CASE HASH("RWD_SPT_05") // Chrono (CHROME)
		CASE HASH("RWD_SPT_06") // Feroci RR (CHROME)
		CASE HASH("RWD_SPT_07") // FiftyNine (CHROME)
		CASE HASH("RWD_SPT_08") // Mercie (CHROME)
		CASE HASH("RWD_SPT_09") // Synthetic Z (CHROME)
		CASE HASH("RWD_SPT_10") // Organic Type 0 (CHROME)
		CASE HASH("RWD_SPT_11") // Endo v.1 (CHROME)
		CASE HASH("RWD_SPT_12") // GT One (CHROME)
		CASE HASH("RWD_SPT_13") // Duper 7 (CHROME)
		CASE HASH("RWD_SPT_14") // Uzer (CHROME)
		CASE HASH("RWD_SPT_15") // GroundRide (CHROME)
		CASE HASH("RWD_SPT_16") // S Racer (CHROME)
		CASE HASH("RWD_SPT_17") // Venum (CHROME)
		CASE HASH("RWD_SPT_18") // Cosmo (CHROME)
		CASE HASH("RWD_SPT_19") // Dash VIP (CHROME)
		CASE HASH("RWD_SPT_20") // Ice Kid (CHROME)
		CASE HASH("RWD_SPT_21") // Ruff Weld (CHROME)
		CASE HASH("RWD_SPT_22") // Wangan Master (CHROME)
		CASE HASH("RWD_SPT_23") // Super Five (CHROME)
		CASE HASH("RWD_SPT_24") // Endo v.2 (CHROME)
		CASE HASH("RWD_SPT_25") // Split Six (CHROME)
			RETURN (IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_WIN_CAPTURES))
			IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ONLY_PLAYER_ALIVE_LTS)>=GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_ONLY_PLAYER_ALIVE_LTS,AWARDPOSITIONS_PLATINUM,0 )
				RETURN TRUE 
			ELSE
			
				RETURN FALSE 
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_MUSC_01") // Classic Five (CHROME)
		CASE HASH("RWD_MUSC_02") // Dukes (CHROME)
		CASE HASH("RWD_MUSC_03") // Muscle Freak (CHROME)
		CASE HASH("RWD_MUSC_04") // Kracka (CHROME)
		CASE HASH("RWD_MUSC_05") // Azreal (CHROME)
		CASE HASH("RWD_MUSC_06") // Mecha (CHROME)
		CASE HASH("RWD_MUSC_07") // Black Top (CHROME)
		CASE HASH("RWD_MUSC_08") // Drag SPL (CHROME)
		CASE HASH("RWD_MUSC_09") // Revolver (CHROME)
		CASE HASH("RWD_MUSC_10") // Classic Rod (CHROME)
		CASE HASH("RWD_MUSC_11") // Fairlie (CHROME)
		CASE HASH("RWD_MUSC_12") // Spooner (CHROME)
		CASE HASH("RWD_MUSC_13") // Five Star (CHROME)
		CASE HASH("RWD_MUSC_14") // Old School (CHROME)
		CASE HASH("RWD_MUSC_15") // El Jefe (CHROME)
		CASE HASH("RWD_MUSC_16") // Dodman (CHROME)
		CASE HASH("RWD_MUSC_17") // Six Gun (CHROME)
		CASE HASH("RWD_MUSC_18") // Foster (CHROME)
		CASE HASH("RWD_MUSC_19") // Split Six (CHROME)
		CASE HASH("RWD_MUSC_20") // Mercenary (CHROME)
			
			IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ONLY_PLAYER_ALIVE_LTS)>=GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_ONLY_PLAYER_ALIVE_LTS,AWARDPOSITIONS_PLATINUM,0 )
				RETURN TRUE 
			ELSE
			
				RETURN FALSE 
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_LORIDE_01") // Flare (CHROME)
		CASE HASH("RWD_LORIDE_02") // Wired (CHROME)
		CASE HASH("RWD_LORIDE_03") // Triple Golds (CHROME)
		CASE HASH("RWD_LORIDE_04") // Big Worm (CHROME)
		CASE HASH("RWD_LORIDE_05") // Seven Fives (CHROME)
		CASE HASH("RWD_LORIDE_06") // Split Six (CHROME)
		CASE HASH("RWD_LORIDE_07") // Fresh Mesh (CHROME)
		CASE HASH("RWD_LORIDE_08") // Lead Sled (CHROME)
		CASE HASH("RWD_LORIDE_09") // Turbine (CHROME)
		CASE HASH("RWD_LORIDE_10") // Super Fin (CHROME)
		CASE HASH("RWD_LORIDE_11") // Classic Rod (CHROME)
		CASE HASH("RWD_LORIDE_12") // Dollar (CHROME)
		CASE HASH("RWD_LORIDE_13") // Dukes (CHROME)
		CASE HASH("RWD_LORIDE_14") // Low Five (CHROME)
		CASE HASH("RWD_LORIDE_15") // Gooch (CHROME)
			RETURN (IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_DROPOFF_CAP_PACKAGES))
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_SUV_01") // VIP (CHROME)
		CASE HASH("RWD_SUV_02") // Benefactor (CHROME)
		CASE HASH("RWD_SUV_03") // Cosmo (CHROME)
		CASE HASH("RWD_SUV_04") // Bippu (CHROME)
		CASE HASH("RWD_SUV_05") // Royal Six (CHROME)
		CASE HASH("RWD_SUV_06") // Fagorme (CHROME)
		CASE HASH("RWD_SUV_07") // Deluxe (CHROME)
		CASE HASH("RWD_SUV_08") // Iced Out (CHROME)
		CASE HASH("RWD_SUV_09") // Cognoscenti (CHROME)
		CASE HASH("RWD_SUV_10") // LozSpeed Ten (CHROME)
		CASE HASH("RWD_SUV_11") // Supernova (CHROME)
		CASE HASH("RWD_SUV_12") // Obey RS (CHROME)
		CASE HASH("RWD_SUV_13") // LozSpeed Baller (CHROME)
		CASE HASH("RWD_SUV_14") // Extravaganzo (CHROME)
		CASE HASH("RWD_SUV_15") // Split Six (CHROME)
		CASE HASH("RWD_SUV_16") // Empowered (CHROME)
		CASE HASH("RWD_SUV_17") // Sunrise (CHROME)
		CASE HASH("RWD_SUV_18") // Dash VIP (CHROME)
		CASE HASH("RWD_SUV_19") // Cutter (CHROME)
		CASE HASH("RWD_SUV_20") // Split Six (CHROME)
			IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_LAST_TEAM_STANDINGS)>=GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_WIN_LAST_TEAM_STANDINGS,AWARDPOSITIONS_PLATINUM,0 )
				RETURN TRUE 
			ELSE
			
				RETURN FALSE 
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_OFFR_01") // Raider (CHROME)
		CASE HASH("RWD_OFFR_02") // Mudslinger (CHROME)
		CASE HASH("RWD_OFFR_03") // Nevis (CHROME)
		CASE HASH("RWD_OFFR_04") // Cairngorm (CHROME)
		CASE HASH("RWD_OFFR_05") // Amazon (CHROME)
		CASE HASH("RWD_OFFR_06") // Challenger (CHROME)
		CASE HASH("RWD_OFFR_07") // Dune Basher (CHROME)
		CASE HASH("RWD_OFFR_08") // Five Star (CHROME)
		CASE HASH("RWD_OFFR_09") // Rock Crawler (CHROME)
		CASE HASH("RWD_OFFR_10") // Mil Spec Steelie (CHROME)
		
			IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_KILL_CARRIER_CAPTURE)>=GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_KILL_CARRIER_CAPTURE,AWARDPOSITIONS_PLATINUM,0 )
				RETURN TRUE 
			ELSE
			
				RETURN FALSE 
			ENDIF
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_DRFT_01") // Cosmo (CHROME)
		CASE HASH("RWD_DRFT_02") // Super Mesh (CHROME)
		CASE HASH("RWD_DRFT_03") // Outsider (CHROME)
		CASE HASH("RWD_DRFT_04") // Rollas (CHROME)
		CASE HASH("RWD_DRFT_05") // Driftmeister (CHROME)
		CASE HASH("RWD_DRFT_06") // Slicer (CHROME)
		CASE HASH("RWD_DRFT_07") // El Quatro (CHROME)
		CASE HASH("RWD_DRFT_08") // Dubbed (CHROME)
		CASE HASH("RWD_DRFT_09") // Five Star (CHROME)
		CASE HASH("RWD_DRFT_10") // Slideways (CHROME)
		CASE HASH("RWD_DRFT_11") // Apex (CHROME)
		CASE HASH("RWD_DRFT_12") // Stanced EG (CHROME)
		CASE HASH("RWD_DRFT_13") // Countersteer (CHROME)
		CASE HASH("RWD_DRFT_14") // Endo v.1 (CHROME)
		CASE HASH("RWD_DRFT_15") // Endo v.2 Dish (CHROME)
		CASE HASH("RWD_DRFT_16") // Gruppe Z (CHROME)
		CASE HASH("RWD_DRFT_17") // Choku-Dori (CHROME)
		CASE HASH("RWD_DRFT_18") // Chicane (CHROME)
		CASE HASH("RWD_DRFT_19") // Saisoku (CHROME)
		CASE HASH("RWD_DRFT_20") // Dished Eight (CHROME)
		CASE HASH("RWD_DRFT_21") // Fujiwara (CHROME)
		CASE HASH("RWD_DRFT_22") // Zokusha (CHROME)
		CASE HASH("RWD_DRFT_23") // Battle VIII (CHROME)
		CASE HASH("RWD_DRFT_24") // Rally Master (CHROME)
			RETURN (IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FINISH_HEIST_SETUP_JOB))
		BREAK
	ENDSWITCH
	SWITCH iLabelHash
		CASE HASH("RWD_BIKEW_01") // Speedway (CHROME)
		CASE HASH("RWD_BIKEW_02") // Street Special (CHROME)
		CASE HASH("RWD_BIKEW_03") // Racer (CHROME)
		CASE HASH("RWD_BIKEW_04") // Track Star (CHROME)
		CASE HASH("RWD_BIKEW_05") // Overlord (CHROME)
		CASE HASH("RWD_BIKEW_06") // Trident (CHROME)
		CASE HASH("RWD_BIKEW_07") // Triple Threat (CHROME)
		CASE HASH("RWD_BIKEW_08") // Stilleto (CHROME)
		CASE HASH("RWD_BIKEW_09") // Wires (CHROME)
		CASE HASH("RWD_BIKEW_10") // Bobber (CHROME)
		CASE HASH("RWD_BIKEW_11") // Solidus (CHROME)
		CASE HASH("RWD_BIKEW_12") // Ice Shield (CHROME)
		CASE HASH("RWD_BIKEW_13") // Loops (CHROME)
		CASE HASH("RWD_BIKEW_14") 	// Romper Racing (CHROME)
		CASE HASH("RWD_BIKEW_15") 	// Warp Drive (CHROME)
		CASE HASH("RWD_BIKEW_16") 	// Snowflake (CHROME)
		CASE HASH("RWD_BIKEW_17") 	// Holy Spoke (CHROME)
		CASE HASH("RWD_BIKEW_18") 	// Old Skool Triple (CHROME)
		CASE HASH("RWD_BIKEW_19") 	// Futura (CHROME)
		CASE HASH("RWD_BIKEW_20") 	// Quarter Mile King (CHROME)
		CASE HASH("RWD_BIKEW_21")   // Cartwheel (CHROME)
		CASE HASH("RWD_BIKEW_22") 	// Double Five (CHROME)
		CASE HASH("RWD_BIKEW_23")	// Shuriken (CHROME)
		CASE HASH("RWD_BIKEW_24")	// Simple Six (CHROME)
		CASE HASH("RWD_BIKEW_25")	// Celtic (CHROME)
		CASE HASH("RWD_BIKEW_26")	// Razer (CHROME)
		CASE HASH("RWD_BIKEW_27")	// Twisted (CHROME)
		CASE HASH("RWD_BIKEW_28")	// Morning Star (CHROME)
		CASE HASH("RWD_BIKEW_29")	// Jagged Spokes (CHROME)
		CASE HASH("RWD_BIKEW_30")	// Eidolon (CHROME)
		CASE HASH("RWD_BIKEW_31")	// Enigma (CHROME)
		CASE HASH("RWD_BIKEW_32")	// Big Spokes (CHROME)
		CASE HASH("RWD_BIKEW_33")	// Webs (CHROME)
		CASE HASH("RWD_BIKEW_34")	// Hotplate (CHROME)
		CASE HASH("RWD_BIKEW_35")	// Bobsta (CHROME)
		CASE HASH("RWD_BIKEW_36")	// Grouch (CHROME)
			RETURN (IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_NIGHTVISION_KILLS))
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


FUNC INT GET_MP_CARMOD_MENU_WHEEL_COST(STRING strLabel, MOD_WHEEL_TYPE eWheelType, INT iWheel, BOOL bCar)

	PRINTLN("GET_MP_CARMOD_MENU_WHEEL_COST(\"", strLabel, "\", ", eWheelType, ", ", iWheel, ", ", bCar, ")")

	FLOAT out = -2.0
	BOOL bChromies = FALSE
	
	TEXT_LABEL_15 tlLabel = strLabel

	IF IS_STRING_NULL_OR_EMPTY(tlLabel)
		IF iWheel <= 0 OR eWheelType = MWT_INVALID
			IF bCar
				tlLabel = "DEFAULT_C"
			ELSE
				tlLabel = "DEFAULT_B"
			ENDIF
		ELSE
		
			SWITCH eWheelType
				CASE MWT_SPORT 		tlLabel = "SPT_"			BREAK
				CASE MWT_MUSCLE 	tlLabel = "MUSC_"			BREAK
				CASE MWT_LOWRIDER 	tlLabel = "LORIDE_"			BREAK
				CASE MWT_SUV 		tlLabel = "SUV_"			BREAK
				CASE MWT_OFFROAD 	tlLabel = "OFFR_"			BREAK
				CASE MWT_TUNER 		tlLabel = "DRFT_"			BREAK
				CASE MWT_BIKE 		tlLabel = "BIKEW_"			BREAK
				CASE MWT_HIEND 		tlLabel = "HIEND_"			BREAK
				
				CASE MWT_SUPERMOD1 tlLabel = "SMOD_WHL"			BREAK
				CASE MWT_SUPERMOD2 tlLabel = "SMOD_WHL"			BREAK
				CASE MWT_SUPERMOD3 tlLabel = "SMOD_WHL"			BREAK
				CASE MWT_SUPERMOD4 tlLabel = "SMOD_WHL"			BREAK
				CASE MWT_SUPERMOD5 tlLabel = "SMOD_WHL"			BREAK
			ENDSWITCH
			
			IF iWheel < 10
			AND eWheelType != MWT_SUPERMOD1
			AND eWheelType != MWT_SUPERMOD2
			AND eWheelType != MWT_SUPERMOD3
			AND eWheelType != MWT_SUPERMOD4
			AND eWheelType != MWT_SUPERMOD5
				tlLabel += "0"
			ENDIF
			

			tlLabel += iWheel
		ENDIF
	ENDIF
	
	IF eWheelType = MWT_BIKE
	AND (iWheel >= 27 AND iWheel <= 49)
	AND IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(g_ModVehicle))
		tlLabel = "BIKEW_"
		INT iOffset 
		iOffset = iWheel - 13
		tlLabel += iOffset
	ENDIF	
	
	INT iHash = GET_HASH_KEY(tlLabel)
	SWITCH iHash
		
		// HIEND - SET 1 - THERE IS NO STOCK WHEEL
		CASE HASH("HIEND_01")   out =  8780.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable	  BREAK
		CASE HASH("HIEND_02")   out =  9990.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable        BREAK
		CASE HASH("HIEND_03")   out =  14890.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
		CASE HASH("HIEND_04")   out =  19600.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable      BREAK
		CASE HASH("HIEND_05")   out =  10870.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
	
		// HIEND - SET 2
		CASE HASH("HIEND_06")    out =  9620.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable         BREAK
		CASE HASH("HIEND_07")    out =  9790.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable         BREAK
		CASE HASH("HIEND_08")    out =  10260.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable        BREAK
		CASE HASH("HIEND_09")    out =  13390.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable        BREAK
		CASE HASH("HIEND_10")    out =  10660.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable        BREAK
		
		// HIEND - SET 3
		CASE HASH("HIEND_11")    out =  10990.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
		CASE HASH("HIEND_12")    out =  15000.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
		CASE HASH("HIEND_13")    out =  12250.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
		CASE HASH("HIEND_14")    out =  13690.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
		CASE HASH("HIEND_15")    out =  20130.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable       BREAK
		
		// HIEND - SET 4
		CASE HASH("HIEND_16")    out =  19240.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable      BREAK
		CASE HASH("HIEND_17")    out =  21380.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable      BREAK
		CASE HASH("HIEND_18")    out =  22100.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable      BREAK
		CASE HASH("HIEND_19")    out =  23680.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable      BREAK
		CASE HASH("HIEND_20")    out =  25000.0 * g_sMptunables.fcarmod_unlock_WheelsHighEnd_expenditure_tunable      BREAK
		      
		// SPORT 
		CASE HASH("SPT_01")     out =  5530.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable BREAK
		CASE HASH("SPT_02")     out =  8120.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable BREAK
		CASE HASH("SPT_03")     out =  8620.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable BREAK
		CASE HASH("SPT_04")     out =  8430.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable BREAK
		CASE HASH("SPT_05")     out =  5980.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable BREAK
		
		CASE HASH("SPT_06")      out =  6000.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_07")      out =  5240.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_08")      out =  5750.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_09")      out =  9380.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_10")      out =  8990.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK

		CASE HASH("SPT_11")      out =  5840.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_12")      out =  18690.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable      BREAK
		CASE HASH("SPT_13")      out =  18440.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable      BREAK
		CASE HASH("SPT_14")      out =  9430.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_15")      out =  9210.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		
		CASE HASH("SPT_16")      out =  9570.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_17")      out =  16850.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable      BREAK
		CASE HASH("SPT_18")      out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_19")      out =  8850.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_20")      out =  9860.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		
		CASE HASH("SPT_21")      out =  20000.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_22")      out =  10000.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_23")      out =  19210.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_24")      out =  17600.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		CASE HASH("SPT_25")      out =  19850.0 * g_sMptunables.fcarmod_unlock_WheelsSport_expenditure_tunable       BREAK
		      
		// MUSCLE
		CASE HASH("MUSC_01")    out =  3500.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable         BREAK
		CASE HASH("MUSC_02")    out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable         BREAK
		CASE HASH("MUSC_03")    out =  6000.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable         BREAK
		CASE HASH("MUSC_04")    out =  3740.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable         BREAK
		CASE HASH("MUSC_05")    out =  7100.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable        BREAK

		CASE HASH("MUSC_06")     out =  7900.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable        BREAK
		CASE HASH("MUSC_07")     out =  15680.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable       BREAK
		CASE HASH("MUSC_08")     out =  4800.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable        BREAK
		CASE HASH("MUSC_09")     out =  6850.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable        BREAK
		CASE HASH("MUSC_10")     out =  16580.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable       BREAK

		CASE HASH("MUSC_11")     out =  3990.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable       BREAK
		CASE HASH("MUSC_12")     out =  4510.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable       BREAK
		CASE HASH("MUSC_13")     out =  7450.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable       BREAK
		CASE HASH("MUSC_14")     out =  7650.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable       BREAK
		CASE HASH("MUSC_15")     out =  14320.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable      BREAK

		CASE HASH("MUSC_16")     out =  4350.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable     BREAK
		CASE HASH("MUSC_17")     out =  6210.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable		BREAK
		CASE HASH("MUSC_18")     out =  12580.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable    BREAK
		CASE HASH("MUSC_19")     out =  15500.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable    BREAK
		CASE HASH("MUSC_20")     out =  12580.0 * g_sMptunables.fcarmod_unlock_WheelsMuscle_expenditure_tunable    BREAK
		      
		// LOWRIDER
		CASE HASH("LORIDE_01")  out =  6900.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable         BREAK
		CASE HASH("LORIDE_02")  out =  4300.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable         BREAK
		CASE HASH("LORIDE_03")  out =  4750.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable         BREAK
		CASE HASH("LORIDE_04")  out =  4480.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable         BREAK
		CASE HASH("LORIDE_05")  out =  6690.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable         BREAK

		CASE HASH("LORIDE_06")   out =  7250.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable       BREAK
		CASE HASH("LORIDE_07")   out =  6230.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable       BREAK
		CASE HASH("LORIDE_08")   out =  15800.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable      BREAK
		CASE HASH("LORIDE_09")   out =  14210.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable      BREAK
		CASE HASH("LORIDE_10")   out =  13500.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable      BREAK

		CASE HASH("LORIDE_11")   out =  16580.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable      BREAK
		CASE HASH("LORIDE_12")   out =  17000.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable      BREAK
		CASE HASH("LORIDE_13")   out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable       BREAK
		CASE HASH("LORIDE_14")   out =  12780.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable      BREAK
		CASE HASH("LORIDE_15")   out =  7650.0 * g_sMptunables.fcarmod_unlock_WheelsLowrider_expenditure_tunable       BREAK
		      
		// SUV
		CASE HASH("SUV_01")      out =  18100.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      BREAK
		CASE HASH("SUV_02")      out =  5360.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       BREAK
		CASE HASH("SUV_03")      out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       BREAK
		CASE HASH("SUV_04")      out =  9150.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      BREAK
		CASE HASH("SUV_05")      out =  5890.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       BREAK

		CASE HASH("SUV_06")      out =  15400.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      BREAK
		CASE HASH("SUV_07")      out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       BREAK
		CASE HASH("SUV_08")      out =  19600.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      BREAK
		CASE HASH("SUV_09")      out =  9130.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       BREAK
		CASE HASH("SUV_10")      out =  9980.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       BREAK

		CASE HASH("SUV_11")      out =  15230.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      	BREAK
		CASE HASH("SUV_12")      out =  6000.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable 		BREAK
		CASE HASH("SUV_13")      out =  15990.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable        BREAK
		CASE HASH("SUV_14")      out =  20000.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable        BREAK
		CASE HASH("SUV_15")      out =  17560.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable        BREAK

		CASE HASH("SUV_16")      out =  18750.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      	BREAK
		CASE HASH("SUV_17")      out =  19820.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable		BREAK
		CASE HASH("SUV_18")      out =  9890.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       	BREAK
		CASE HASH("SUV_19")      out =  8100.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable       	BREAK
		//CASE HASH("SUV_20")      out =  17560.0 * g_sMptunables.fcarmod_unlock_WheelsSUV_expenditure_tunable      	?
		 
		// OFF ROAD
		CASE HASH("OFFR_01")    out =  5000.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable         BREAK
		CASE HASH("OFFR_02")    out =  5000.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable         BREAK
		CASE HASH("OFFR_03")    out =  6000.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable         BREAK
		CASE HASH("OFFR_04")    out =  7240.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable         BREAK
		CASE HASH("OFFR_05")    out =  4690.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable         BREAK

		CASE HASH("OFFR_06")     out =  15000.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable       BREAK
		CASE HASH("OFFR_07")     out =  11200.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable       BREAK
		CASE HASH("OFFR_08")     out =  7930.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable       BREAK
		CASE HASH("OFFR_09")     out =  4810.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable        BREAK
		CASE HASH("OFFR_10")     out =  6810.0 * g_sMptunables.fcarmod_unlock_WheelsOffroad_expenditure_tunable        BREAK

    
		// TUNE
		CASE HASH("DRFT_01")     out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable        BREAK
		CASE HASH("DRFT_02")     out =  6000.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable        BREAK
		CASE HASH("DRFT_03")     out =  4990.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable        BREAK
		CASE HASH("DRFT_04")     out =  6300.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable        BREAK
		CASE HASH("DRFT_05")     out =  4930.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable        BREAK

		CASE HASH("DRFT_06")     out =  7850.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_07")     out =  13440.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable      BREAK
		CASE HASH("DRFT_08")     out =  17000.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable      BREAK
		CASE HASH("DRFT_09")     out =  6790.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_10")     out =  16480.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable      BREAK

		CASE HASH("DRFT_11")     out =  8000.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_12")     out =  5000.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_13")     out =  6100.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_14")     out =  6230.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable      BREAK
		CASE HASH("DRFT_15")     out =  4850.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK

		CASE HASH("DRFT_16")     out =  6350.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_17")     out =  7930.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_18")     out =  4920.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_19")     out =  5000.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_20")     out =  4980.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK

		CASE HASH("DRFT_21")     out =  4250.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_22")     out =  6330.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable       BREAK
		CASE HASH("DRFT_23")     out =  14100.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable      BREAK
		CASE HASH("DRFT_24")     out =  16890.0 * g_sMptunables.fcarmod_unlock_WheelsTuner_expenditure_tunable      BREAK
		  
		// BIKE
		CASE HASH("BIKEW_01")   out =  3780.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable         BREAK
		CASE HASH("BIKEW_02")   out =  4000.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable         BREAK
		CASE HASH("BIKEW_03")   out =  5120.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable         BREAK
		CASE HASH("BIKEW_04")   out =  5870.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable         BREAK
		CASE HASH("BIKEW_05")   out =  3500.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable         BREAK

		CASE HASH("BIKEW_06")    out =  3380.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       BREAK
		CASE HASH("BIKEW_07")    out =  6120.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       BREAK
		CASE HASH("BIKEW_08")    out =  7000.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       BREAK
		CASE HASH("BIKEW_09")    out =  6550.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       BREAK

		CASE HASH("BIKEW_10")    out =  6830.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       	BREAK
		CASE HASH("BIKEW_11")    out =  10250.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       	BREAK
		CASE HASH("BIKEW_12")    out =  12000.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       	BREAK
		CASE HASH("BIKEW_13")    out =  11370.0 * g_sMptunables.fcarmod_unlock_WheelsBike_expenditure_tunable       	BREAK
		
		CASE HASH("SMOD_WHL1")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_OG_Hunnets_Chrome)	BREAK // OG Hunnets (Chrome)
		CASE HASH("SMOD_WHL2")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_OG_Hunnets_Painted)	BREAK // OG Hunnets (Painted)
		CASE HASH("SMOD_WHL3")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_KnockOffs_Chrome)	BREAK // Knock-Offs (Chrome)
		CASE HASH("SMOD_WHL4")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_KnockOffs_Painted)	BREAK // Knock-Offs (Painted)
		CASE HASH("SMOD_WHL5")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spoked_Out_Chrome)	BREAK // Spoked Out (Chrome)
		CASE HASH("SMOD_WHL6")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spoked_Out_Painted)	BREAK // Spoked Out (Painted)
		CASE HASH("SMOD_WHL7")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Vintage_Wire_Chrome)	BREAK // Vintage Wire (Chrome)
		CASE HASH("SMOD_WHL8")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Vintage_Wire_Painted)	BREAK // Vintage Wire (Painted)
		CASE HASH("SMOD_WHL9")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Smoothie_Chrome)	BREAK // Smoothie (Chrome)
		CASE HASH("SMOD_WHL10")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Smoothie_Chrome_Lip)	BREAK // Smoothie (Chrome Lip)
		CASE HASH("SMOD_WHL11")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Smoothie_Painted)	BREAK // Smoothie (Painted)
		CASE HASH("SMOD_WHL12")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Rod_Me_Up_Chrome)	BREAK // Rod Me Up(Chrome)
		CASE HASH("SMOD_WHL13")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Rod_Me_Up_Chrome_Lip)	BREAK // Rod Me Up (Chome Lip)
		CASE HASH("SMOD_WHL14")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Rod_Me_Up_Painted)	BREAK // Rod Me Up (Painted)
		CASE HASH("SMOD_WHL15")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Clean)	BREAK // Clean
		CASE HASH("SMOD_WHL16")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Lotta_Chrome)	BREAK // Lotta Chrome
		CASE HASH("SMOD_WHL17")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spindles)	BREAK // Spindles
		CASE HASH("SMOD_WHL18")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Viking)	BREAK // Viking 
		CASE HASH("SMOD_WHL19")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Triple_Spoke)	BREAK // Triple Spoke
		CASE HASH("SMOD_WHL20")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Pharohe)	BREAK // Pharohe
		CASE HASH("SMOD_WHL21")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Tiger_Style)	BREAK // Tiger Style
		CASE HASH("SMOD_WHL22")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Three_Wheelin)	BREAK // Three Wheelin
		CASE HASH("SMOD_WHL23")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Big_Bar)	BREAK // Big Bar
		CASE HASH("SMOD_WHL24")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Biohazard)	BREAK // Biohazard
		CASE HASH("SMOD_WHL25")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Waves)	BREAK // Waves
		CASE HASH("SMOD_WHL26")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Lick_Lick)	BREAK // Lick Lick
		CASE HASH("SMOD_WHL27")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Spiralizer)	BREAK // Spiralizer
		CASE HASH("SMOD_WHL28")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Hypotics)	BREAK // Hypotics
		CASE HASH("SMOD_WHL29")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Psychodelic)	BREAK // Psycho-Delic
		CASE HASH("SMOD_WHL30")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Half_Cut)	BREAK // Half Cut
		CASE HASH("SMOD_WHL31")	out = TO_FLOAT(g_sMPTunables.iLowrider1_Car_Mods_Bennys_Wheels_Super_Electric)	BREAK // Super Electric

		CASE HASH("DEFAULT_C")	
		
			SWITCH eWheelType
				CASE MWT_SPORT 		out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_sport		BREAK
				CASE MWT_MUSCLE 	out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special		BREAK
				CASE MWT_LOWRIDER 	out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special		BREAK
				CASE MWT_SUV 		out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_SUV			BREAK
				CASE MWT_OFFROAD 	out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable				BREAK
				CASE MWT_TUNER 		out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable 			BREAK
				CASE MWT_BIKE 		out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_bike		BREAK
				CASE MWT_HIEND 		out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special 	BREAK
				CASE MWT_SUPERMOD1  out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special 	BREAK
				CASE MWT_SUPERMOD2  out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special 	BREAK
				CASE MWT_SUPERMOD3  out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special 	BREAK
				CASE MWT_SUPERMOD4  out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special 	BREAK
				CASE MWT_SUPERMOD5  out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_special 	BREAK
			ENDSWITCH
		BREAK

		CASE HASH("DEFAULT_B")	
			out = 3000.0 * g_sMptunables.fcarmod_unlock_StockWheels_expenditure_tunable_bike
		BREAK
	ENDSWITCH
	
	INT iHashTwo = GET_HASH_KEY(tlLabel)
	SWITCH iHashTwo
		CASE HASH("SMOD2_WHL1")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_OG_Hunnets)	BREAK // Chrome OG Hunnets
		CASE HASH("SMOD2_WHL2")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_OG_Hunnets)	BREAK // Gold OG Hunnets
		CASE HASH("SMOD2_WHL3")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Wires)		BREAK // Chrome Wires
		CASE HASH("SMOD2_WHL4")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Wires)			BREAK // Gold wires
		CASE HASH("SMOD2_WHL5")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Spoked_Out)	BREAK // Chrome Spoked Out
		CASE HASH("SMOD2_WHL6")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Spoked_Out)	BREAK // Gold Spoked Out
		CASE HASH("SMOD2_WHL7")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_KnockOffs)	BREAK // Chrome Knock-Offs
		CASE HASH("SMOD2_WHL8")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_KnockOffs)		BREAK // Gold Knock-Offs
		CASE HASH("SMOD2_WHL9")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Bigger_Worm)	BREAK // Chrome Bigger Worm
		CASE HASH("SMOD2_WHL10")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Bigger_Worm)	BREAK // Gold Bigger Worm
		CASE HASH("SMOD2_WHL11")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Vintage_Wire)	BREAK // Chrome Vintage Wire
		CASE HASH("SMOD2_WHL12")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Vintage_Wire)	BREAK // Gold Vintage Wire
		CASE HASH("SMOD2_WHL13")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Classic_Wire)	BREAK // Chrome Classic Wire
		CASE HASH("SMOD2_WHL14")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Classic_Wire)	BREAK // Gold Classic Wire
		CASE HASH("SMOD2_WHL15")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Smoothie)	BREAK // Chrome Smoothie
		CASE HASH("SMOD2_WHL16")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Smoothie)		BREAK // Gold Smoothie
		CASE HASH("SMOD2_WHL17")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Classic_Rod)	BREAK // Chrome Classic Rod
		CASE HASH("SMOD2_WHL18")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Classic_Rod)	BREAK // Gold Classic Rod 
		CASE HASH("SMOD2_WHL19")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Dollar)		BREAK // Chrome Dollar
		CASE HASH("SMOD2_WHL20")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Dollar)		BREAK // Gold Dollar
		CASE HASH("SMOD2_WHL21")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Mighty_Star)			BREAK // Chrome Mighty Star
		CASE HASH("SMOD2_WHL22")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Mighty_Star)     	 	BREAK // Gold Mighty Star
		CASE HASH("SMOD2_WHL23")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Decadent_Dish) 	 	BREAK // Chrome Decadent Dish
		CASE HASH("SMOD2_WHL24")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Decadent_Dish)   	 	BREAK // Gold Decadent Dish
		CASE HASH("SMOD2_WHL25")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Razor_Style)   	 	BREAK // Chrome Razor Style
		CASE HASH("SMOD2_WHL26")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Razor_Style)     	 	BREAK // Gold Razor Style
		CASE HASH("SMOD2_WHL27")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Celtic_Knot)   	 	BREAK // Chrome Celtic Knot
		CASE HASH("SMOD2_WHL28")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Celtic_Knot)     	 	BREAK // Gold Celtic Knot
		CASE HASH("SMOD2_WHL29")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Warrior_Dish)  	 	BREAK // Chrome Warrior Dish
		CASE HASH("SMOD2_WHL30")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Warrior_Dish)    	 	BREAK // Gold Warrior Dish
		CASE HASH("SMOD2_WHL31")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Big_Dog_Spokes)  	 	BREAK // Gold Big Dog Spokes
		
		CASE HASH("RACECAR_WHL1")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_OG_Hunnets)	BREAK 
		CASE HASH("RACECAR_WHL2")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_OG_Hunnets)	BREAK 
		CASE HASH("RACECAR_WHL3")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Wires)		BREAK 
		CASE HASH("RACECAR_WHL4")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Wires)		BREAK 
		CASE HASH("RACECAR_WHL5")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Spoked_Out)	BREAK 
		CASE HASH("RACECAR_WHL6")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Spoked_Out)	BREAK 
		CASE HASH("RACECAR_WHL7")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_KnockOffs)	BREAK 
		CASE HASH("RACECAR_WHL8")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_KnockOffs)	BREAK 
		CASE HASH("RACECAR_WHL9")		out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Bigger_Worm)BREAK 
		CASE HASH("RACECAR_WHL10")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Bigger_Worm)	BREAK 
		CASE HASH("RACECAR_WHL11")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Vintage_Wire)	BREAK
		CASE HASH("RACECAR_WHL12")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Vintage_Wire)	BREAK 
		CASE HASH("RACECAR_WHL13")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Classic_Wire)	BREAK
		CASE HASH("RACECAR_WHL14")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Classic_Wire)	BREAK 
		CASE HASH("RACECAR_WHL15")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Smoothie)	BREAK 
		CASE HASH("RACECAR_WHL16")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Smoothie)		BREAK 
		CASE HASH("RACECAR_WHL17")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Classic_Rod)	BREAK
		CASE HASH("RACECAR_WHL18")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Classic_Rod)	BREAK 
		CASE HASH("RACECAR_WHL19")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Chrome_Dollar)		BREAK 
		CASE HASH("RACECAR_WHL20")	out = TO_FLOAT(g_sMPTunables.iCar_Mods_Bennys_Bespoke_Wheels_Gold_Dollar)		BREAK 
		CASE HASH("RACECAR_WHL21")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Mighty_Star)			BREAK 
		CASE HASH("RACECAR_WHL22")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Mighty_Star)     	 	BREAK 
		CASE HASH("RACECAR_WHL23")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Chrome_Decadent_Dish) 	 	BREAK 
		CASE HASH("RACECAR_WHL24")	out = TO_FLOAT(g_sMPTunables.iBennys_Bespoke_Wheels_Gold_Decadent_Dish)   	 	BREAK 
	
		#IF FEATURE_SUMMER_2020
		CASE HASH("OFFR_11")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_RETRO_STEELIE)			 BREAK
		CASE HASH("OFFR_12")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_HEAVY_DUTY_STEELIE)	 BREAK
		CASE HASH("OFFR_13")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_CONCAVE_STEELIE)		 BREAK
		CASE HASH("OFFR_14")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_POLICE_ISSUE_STEELIE)	 BREAK
		CASE HASH("OFFR_15")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_LIGHTWEIGHT_STEELIE)	 BREAK
		CASE HASH("OFFR_16")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_DUKES)					 BREAK
		CASE HASH("OFFR_17")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_AVALANCHE)				 BREAK
		CASE HASH("OFFR_18")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_MOUNTAIN_MAN)			 BREAK
		CASE HASH("OFFR_19")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_RIDGE_CLIMBER)			 BREAK
		CASE HASH("OFFR_20")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_CONCAVE5)				 BREAK
		CASE HASH("OFFR_21")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_FLAT_SIX)				 BREAK
		CASE HASH("OFFR_22")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_ALL_TERRAIN_MONSTER)	 BREAK
		CASE HASH("OFFR_23")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_DRAG_SPL)				 BREAK
		CASE HASH("OFFR_24")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_CONCAVE_RALLY_MASTER)	 BREAK
		CASE HASH("OFFR_25")     out = TO_FLOAT(g_sMPTunables.iSUM_OFFROAD_WHEEL_RUGGED_SNOWFLAKE)		 BREAK
									   
		CASE HASH("SMOD4_WHL_1")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_RETRO_STEELIE)		 	 BREAK
		CASE HASH("SMOD4_WHL_2")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_POVERTY_SPEC_STEELIE)	 BREAK
		CASE HASH("SMOD4_WHL_3")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_CONCAVE_STEELIE)		 BREAK
		CASE HASH("SMOD4_WHL_4")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_NEBULA)				 	 BREAK
		CASE HASH("SMOD4_WHL_5")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_RACING_FIVES)			 BREAK
		CASE HASH("SMOD4_WHL_6")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_CUP_CHAMPION)			 BREAK
		CASE HASH("SMOD4_WHL_7")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_STANCED_EG_CUSTOM)		 BREAK
		CASE HASH("SMOD4_WHL_8")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_KRACKA_CUSTOM)			 BREAK
		CASE HASH("SMOD4_WHL_9")	 out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_DUKES_CUSTOM)			 BREAK
		CASE HASH("SMOD4_WHL_10") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_ENDO_V3_CUSTOM)			 BREAK
		CASE HASH("SMOD4_WHL_11") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_V8_KILLER	)			 BREAK
		CASE HASH("SMOD4_WHL_12") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_FUJIWARA_CUSTOM)		 BREAK
		CASE HASH("SMOD4_WHL_13") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_COSMO_MKII)				 BREAK
		CASE HASH("SMOD4_WHL_14") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_AERO_STAR)				 BREAK
		CASE HASH("SMOD4_WHL_15") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_HYPE_FIVE)				 BREAK
		CASE HASH("SMOD4_WHL_16") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_RUFF_WELD_MEGA_DEEP)	 BREAK
		CASE HASH("SMOD4_WHL_17") out = TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_MERCIE_CONCAVE	)		 BREAK
		CASE HASH("SMOD4_WHL_18") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_SUGOI_CONCAVE)			 BREAK
		CASE HASH("SMOD4_WHL_19") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_SYNTHETIC_Z_CONCAVE)	 BREAK
		CASE HASH("SMOD4_WHL_20") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_ENDO_V4_DISHED	)		 BREAK
		CASE HASH("SMOD4_WHL_21") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_HYPERFRESH	)		 	 BREAK
		CASE HASH("SMOD4_WHL_22") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_TRUFFADE_CONCAVE)		 BREAK
		CASE HASH("SMOD4_WHL_23") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_ORGANIC_TYPE_II)		 BREAK
		CASE HASH("SMOD4_WHL_24") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_BIG_MAMBA	)			 BREAK
		CASE HASH("SMOD4_WHL_25") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_DEEP_FLAKE	)			 BREAK
		CASE HASH("SMOD4_WHL_26") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_COSMO_MKIII)			 BREAK
		CASE HASH("SMOD4_WHL_27") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_CONCAVE_RACER	)		 BREAK
		CASE HASH("SMOD4_WHL_28") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_DEEP_FLAKE_REVERSE	)	 BREAK	
		CASE HASH("SMOD4_WHL_29") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_WILD_WAGON	)			 BREAK
		CASE HASH("SMOD4_WHL_30") out =	TO_FLOAT(g_sMPTunables.iSUM_STREET_WHEEL_CONCAVE_MEGA_MESH)	 	 BREAK
		#ENDIF
		
		CASE HASH("SMOD5_WHL_1")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_RALLY_THROWBACK)			BREAK
		CASE HASH("SMOD5_WHL_2")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_GRAVEL_TRAP)			 	BREAK
		CASE HASH("SMOD5_WHL_3")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_STOVE_TOP)			 	BREAK
		CASE HASH("SMOD5_WHL_4")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_STOVE_TOP_MESH)			BREAK
		CASE HASH("SMOD5_WHL_5")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_RETRO_3_PIECE)			BREAK
		CASE HASH("SMOD5_WHL_6")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_RALLY_MONOBLOCK)			BREAK
		CASE HASH("SMOD5_WHL_7")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_FORGED_5)			 		BREAK
		CASE HASH("SMOD5_WHL_8")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_SPLIT_STAR)				BREAK
		CASE HASH("SMOD5_WHL_9")  out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_SPEED_BOY)				BREAK
		CASE HASH("SMOD5_WHL_10") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_90S_RUNNING)			 	BREAK
		CASE HASH("SMOD5_WHL_11") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_TROPOS)			 		BREAK
		CASE HASH("SMOD5_WHL_12") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_EXOS)		 				BREAK
		CASE HASH("SMOD5_WHL_13") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_HIGH_FIVE)				BREAK
		CASE HASH("SMOD5_WHL_14") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_SUPER_LUXE)				BREAK
		CASE HASH("SMOD5_WHL_15") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_PURE_BUSINESS)			BREAK
		CASE HASH("SMOD5_WHL_16") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_PEPPER_POT)	 			BREAK
		CASE HASH("SMOD5_WHL_17") out = TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_BLACKTOP_BLENDER)		 	BREAK
		CASE HASH("SMOD5_WHL_18") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_THROWBACK)				BREAK
		CASE HASH("SMOD5_WHL_19") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_EXPRESSWAY)	 			BREAK
		CASE HASH("SMOD5_WHL_20") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_HIDDEN_SIX)		 		BREAK
		CASE HASH("SMOD5_WHL_21") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_DINKA_SPL)		 	 	BREAK
		CASE HASH("SMOD5_WHL_22") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_RETRO_TURBOFAN)			BREAK
		CASE HASH("SMOD5_WHL_23") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_CONICAL_TURBOFAN)		 	BREAK
		CASE HASH("SMOD5_WHL_24") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_ICE_STORM)			 	BREAK
		CASE HASH("SMOD5_WHL_25") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_SUPER_TURBINE)			BREAK
		CASE HASH("SMOD5_WHL_26") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_MODERN_MESH)			 	BREAK
		CASE HASH("SMOD5_WHL_27") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_FORGED_STAR)		 		BREAK
		CASE HASH("SMOD5_WHL_28") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_SHOWFLAKE)	 			BREAK	
		CASE HASH("SMOD5_WHL_29") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_GIGA_MESH)			 	BREAK
		CASE HASH("SMOD5_WHL_30") out =	TO_FLOAT(g_sMPTunables.iTUNER_MODS_WHEELS_MESH_MEISTER)	 	 		BREAK
	ENDSWITCH
	
	// Chromies
	
	IF IS_STRING_NULL_OR_EMPTY(strLabel)
	AND out = -2.0
	
		SWITCH eWheelType
			CASE MWT_SPORT 		tlLabel = "RWD_SPT_"			BREAK
			CASE MWT_MUSCLE 	tlLabel = "RWD_MUSC_"			BREAK
			CASE MWT_LOWRIDER 	tlLabel = "RWD_LORIDE_"			BREAK
			CASE MWT_SUV 		tlLabel = "RWD_SUV_"			BREAK
			CASE MWT_OFFROAD 	tlLabel = "RWD_OFFR_"			BREAK
			CASE MWT_TUNER 		tlLabel = "RWD_DRFT_"			BREAK
			CASE MWT_BIKE 		tlLabel = "RWD_BIKEW_"			BREAK
			CASE MWT_HIEND 		tlLabel = "RWD_HIEND_"			BREAK
		ENDSWITCH
	
		IF eWheelType = MWT_SPORT AND iWheel > 25
			iWheel -= 25
		ELIF eWheelType = MWT_MUSCLE AND iWheel > 18
			iWheel -= 18
		ELIF eWheelType = MWT_LOWRIDER AND iWheel > 15
			iWheel -= 15
		ELIF eWheelType = MWT_SUV AND iWheel > 19
			iWheel -= 19
		ELIF eWheelType = MWT_OFFROAD AND iWheel > 10
			iWheel -= 10
		ELIF eWheelType = MWT_TUNER AND iWheel > 24
			iWheel -= 24
		ELIF eWheelType = MWT_BIKE AND iWheel > 13
			iWheel -= 13
		ELIF eWheelType = MWT_HIEND AND iWheel > 20
			iWheel -= 20
		ENDIF
		
		IF iWheel < 10
			tlLabel += "0"
		ENDIF
		
		tlLabel += iWheel
		
		PRINTLN("kr_test: checking price of wheel with label ", tlLabel)
		
		IF eWheelType = MWT_BIKE
		AND (iWheel > 49)
		AND IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(g_ModVehicle))
			tlLabel = "RWD_BIKEW_"
			INT iOffset 
			iOffset = iWheel - 36
			tlLabel += iOffset
		ENDIF
		
		
		iHash = GET_HASH_KEY(tlLabel)
	ENDIF
	
	SWITCH iHash
		CASE HASH("RWD_HIEND_01")	out = 67860		bChromies = TRUE BREAK // Shadow (CHROME)
		CASE HASH("RWD_HIEND_02")	out = 71150		bChromies = TRUE BREAK // Hypher (CHROME)
		CASE HASH("RWD_HIEND_03")	out = 75850		bChromies = TRUE BREAK // Blade (CHROME)
		CASE HASH("RWD_HIEND_04")	out = 81725		bChromies = TRUE BREAK // Diamond (CHROME)
		CASE HASH("RWD_HIEND_05")	out = 72325		bChromies = TRUE BREAK // Supa Gee (CHROME)
		CASE HASH("RWD_HIEND_06")	out = 69975		bChromies = TRUE BREAK // Chromatic Z (CHROME) 
		CASE HASH("RWD_HIEND_07")	out = 70210		bChromies = TRUE BREAK // Mercie (CHROME)
		CASE HASH("RWD_HIEND_08")	out = 71855		bChromies = TRUE BREAK // Obey RS (CHROME)
		CASE HASH("RWD_HIEND_09")	out = 74205		bChromies = TRUE BREAK // GT Chrome (CHROME)
		CASE HASH("RWD_HIEND_10")	out = 72090		bChromies = TRUE BREAK // Cheetah RR (CHROME)
		CASE HASH("RWD_HIEND_11")	out = 72560		bChromies = TRUE BREAK // Solar (CHROME)
		CASE HASH("RWD_HIEND_12")	out = 76320		bChromies = TRUE BREAK // Split Ten (CHROME)
		CASE HASH("RWD_HIEND_13")	out = 73500		bChromies = TRUE BREAK // Dash VIP (CHROME)
		CASE HASH("RWD_HIEND_14")	out = 74910		bChromies = TRUE BREAK // LozSpeed Ten (CHROME)
		CASE HASH("RWD_HIEND_15")	out = 82665		bChromies = TRUE BREAK // Carbon Inferno (CHROME)
		CASE HASH("RWD_HIEND_16")	out = 81020		bChromies = TRUE BREAK // Carbon Shadow (CHROME)
		CASE HASH("RWD_HIEND_17")	out = 82900		bChromies = TRUE BREAK // Carbonic Z (CHROME)
		CASE HASH("RWD_HIEND_18")	out = 83135		bChromies = TRUE BREAK // Carbon Solar (CHROME)
		CASE HASH("RWD_HIEND_19")	out = 83370		bChromies = TRUE BREAK // Cheetah Carbon R (CHROME)
		CASE HASH("RWD_HIEND_20")	out = 83605		bChromies = TRUE BREAK // Carbon S Racer (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_SPT_01")		out = 56580		bChromies = TRUE BREAK // Inferno (CHROME)
		CASE HASH("RWD_SPT_02")		out = 67155		bChromies = TRUE BREAK // Deep Five (CHROME)
		CASE HASH("RWD_SPT_03")		out = 67625		bChromies = TRUE BREAK // Lozspeed Mk.V (CHROME)
		CASE HASH("RWD_SPT_04")		out = 67390		bChromies = TRUE BREAK // Diamond Cut (CHROME)
		CASE HASH("RWD_SPT_05")		out = 57755		bChromies = TRUE BREAK // Chrono (CHROME)
		CASE HASH("RWD_SPT_06")		out = 58460		bChromies = TRUE BREAK // Feroci RR (CHROME)
		CASE HASH("RWD_SPT_07")		out = 56110		bChromies = TRUE BREAK // FiftyNine (CHROME)
		CASE HASH("RWD_SPT_08")		out = 56815		bChromies = TRUE BREAK // Mercie (CHROME)
		CASE HASH("RWD_SPT_09")		out = 69270		bChromies = TRUE BREAK // Synthetic Z (CHROME)
		CASE HASH("RWD_SPT_10")		out = 68330		bChromies = TRUE BREAK // Organic Type 0 (CHROME)
		CASE HASH("RWD_SPT_11")		out = 57050		bChromies = TRUE BREAK // Endo v.1 (CHROME)
		CASE HASH("RWD_SPT_12")		out = 80315		bChromies = TRUE BREAK // GT One (CHROME)
		CASE HASH("RWD_SPT_13")		out = 80080		bChromies = TRUE BREAK // Duper 7 (CHROME)
		CASE HASH("RWD_SPT_14")		out = 69505		bChromies = TRUE BREAK // Uzer (CHROME)
		CASE HASH("RWD_SPT_15")		out = 69035		bChromies = TRUE BREAK // GroundRide (CHROME)
		CASE HASH("RWD_SPT_16")		out = 69740		bChromies = TRUE BREAK // S Racer (CHROME)
		CASE HASH("RWD_SPT_17")		out = 78435		bChromies = TRUE BREAK // Venum (CHROME)
		CASE HASH("RWD_SPT_18")		out = 66215		bChromies = TRUE BREAK // Cosmo (CHROME)
		CASE HASH("RWD_SPT_19")		out = 68095		bChromies = TRUE BREAK // Dash VIP (CHROME)
		CASE HASH("RWD_SPT_20")		out = 70445		bChromies = TRUE BREAK // Ice Kid (CHROME)
		CASE HASH("RWD_SPT_21")		out = 82430		bChromies = TRUE BREAK // Ruff Weld (CHROME)
		CASE HASH("RWD_SPT_22")		out = 71385		bChromies = TRUE BREAK // Wangan Master (CHROME)
		CASE HASH("RWD_SPT_23")		out = 80785		bChromies = TRUE BREAK // Super Five (CHROME)
		CASE HASH("RWD_SPT_24")		out = 79610		bChromies = TRUE BREAK // Endo v.2 (CHROME)
		CASE HASH("RWD_SPT_25")		out = 81960		bChromies = TRUE BREAK // Split Six (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_MUSC_01")	out = 50235		bChromies = TRUE BREAK // Classic Five (CHROME)
		CASE HASH("RWD_MUSC_02")	out = 66450		bChromies = TRUE BREAK // Dukes (CHROME)
		CASE HASH("RWD_MUSC_03")	out = 58930		bChromies = TRUE BREAK // Muscle Freak (CHROME)
		CASE HASH("RWD_MUSC_04")	out = 50705		bChromies = TRUE BREAK // Kracka (CHROME)
		CASE HASH("RWD_MUSC_05")	out = 62925		bChromies = TRUE BREAK // Azreal (CHROME)
		CASE HASH("RWD_MUSC_06")	out = 64570		bChromies = TRUE BREAK // Mecha (CHROME)
		CASE HASH("RWD_MUSC_07")	out = 77025		bChromies = TRUE BREAK // Black Top (CHROME)
		CASE HASH("RWD_MUSC_08")	out = 53290		bChromies = TRUE BREAK // Drag SPL (CHROME)
		CASE HASH("RWD_MUSC_09")	out = 62220		bChromies = TRUE BREAK // Revolver (CHROME)
		CASE HASH("RWD_MUSC_10")	out = 77965		bChromies = TRUE BREAK // Classic Rod (CHROME)
		CASE HASH("RWD_MUSC_11")	out = 51175		bChromies = TRUE BREAK // Fairlie (CHROME)
		CASE HASH("RWD_MUSC_12")	out = 52585		bChromies = TRUE BREAK // Spooner (CHROME)
		CASE HASH("RWD_MUSC_13")	out = 63630		bChromies = TRUE BREAK // Five Star (CHROME)
		CASE HASH("RWD_MUSC_14")	out = 63865		bChromies = TRUE BREAK // Old School (CHROME)
		CASE HASH("RWD_MUSC_15")	out = 75615		bChromies = TRUE BREAK // El Jefe (CHROME)
		CASE HASH("RWD_MUSC_16")	out = 52115		bChromies = TRUE BREAK // Dodman (CHROME)
		CASE HASH("RWD_MUSC_17")	out = 59635		bChromies = TRUE BREAK // Six Gun (CHROME)
		CASE HASH("RWD_MUSC_18")	out = 62850		bChromies = TRUE BREAK // Foster (CHROME)
		CASE HASH("RWD_MUSC_19")	out = 65395		bChromies = TRUE BREAK // Split Six (CHROME)
		CASE HASH("RWD_MUSC_20")	out = 73735		bChromies = TRUE BREAK // Mercenary (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_LORIDE_01")	out = 62455		bChromies = TRUE BREAK // Flare (CHROME)
		CASE HASH("RWD_LORIDE_02")	out = 51880		bChromies = TRUE BREAK // Wired (CHROME)
		CASE HASH("RWD_LORIDE_03")	out = 53055		bChromies = TRUE BREAK // Triple Golds (CHROME)
		CASE HASH("RWD_LORIDE_04")	out = 52350		bChromies = TRUE BREAK // Big Worm (CHROME)
		CASE HASH("RWD_LORIDE_05")	out = 61280		bChromies = TRUE BREAK // Seven Fives (CHROME)
		CASE HASH("RWD_LORIDE_06")	out = 63395		bChromies = TRUE BREAK // Split Six (CHROME)
		CASE HASH("RWD_LORIDE_07")	out = 60105		bChromies = TRUE BREAK // Fresh Mesh (CHROME)
		CASE HASH("RWD_LORIDE_08")	out = 77260		bChromies = TRUE BREAK // Lead Sled (CHROME)
		CASE HASH("RWD_LORIDE_09")	out = 75380		bChromies = TRUE BREAK // Turbine (CHROME)
		CASE HASH("RWD_LORIDE_10")	out = 74675		bChromies = TRUE BREAK // Super Fin (CHROME)
		CASE HASH("RWD_LORIDE_11")	out = 78200		bChromies = TRUE BREAK // Classic Rod (CHROME)
		CASE HASH("RWD_LORIDE_12")	out = 79140		bChromies = TRUE BREAK // Dollar (CHROME)
		CASE HASH("RWD_LORIDE_13")	out = 66685		bChromies = TRUE BREAK // Dukes (CHROME)
		CASE HASH("RWD_LORIDE_14")	out = 73970		bChromies = TRUE BREAK // Low Five (CHROME)
		CASE HASH("RWD_LORIDE_15")	out = 64100		bChromies = TRUE BREAK // Gooch (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_SUV_01")		out = 79845		bChromies = TRUE BREAK // VIP (CHROME)
		CASE HASH("RWD_SUV_02")		out = 56345		bChromies = TRUE BREAK // Benefactor (CHROME)
		CASE HASH("RWD_SUV_03")		out = 65980		bChromies = TRUE BREAK // Cosmo (CHROME)
		CASE HASH("RWD_SUV_04")		out = 68800		bChromies = TRUE BREAK // Bippu (CHROME)
		CASE HASH("RWD_SUV_05")		out = 57520		bChromies = TRUE BREAK // Royal Six (CHROME)
		CASE HASH("RWD_SUV_06")		out = 76790		bChromies = TRUE BREAK // Fagorme (CHROME)
		CASE HASH("RWD_SUV_07")		out = 65745		bChromies = TRUE BREAK // Deluxe (CHROME)
		CASE HASH("RWD_SUV_08")		out = 81490		bChromies = TRUE BREAK // Iced Out (CHROME)
		CASE HASH("RWD_SUV_09")		out = 68565		bChromies = TRUE BREAK // Cognoscenti (CHROME)
		CASE HASH("RWD_SUV_10")		out = 70915		bChromies = TRUE BREAK // LozSpeed Ten (CHROME)
		CASE HASH("RWD_SUV_11")		out = 76555		bChromies = TRUE BREAK // Supernova (CHROME)
		CASE HASH("RWD_SUV_12")		out = 58225		bChromies = TRUE BREAK // Obey RS (CHROME)
		CASE HASH("RWD_SUV_13")		out = 77495		bChromies = TRUE BREAK // LozSpeed Baller (CHROME)
		CASE HASH("RWD_SUV_14")		out = 82195		bChromies = TRUE BREAK // Extravaganzo (CHROME)
		CASE HASH("RWD_SUV_15")		out = 79375		bChromies = TRUE BREAK // Split Six (CHROME)
		CASE HASH("RWD_SUV_16")		out = 80550		bChromies = TRUE BREAK // Empowered (CHROME)
		CASE HASH("RWD_SUV_17")		out = 81255		bChromies = TRUE BREAK // Sunrise (CHROME)
		CASE HASH("RWD_SUV_18")		out = 70680		bChromies = TRUE BREAK // Dash VIP (CHROME)
		CASE HASH("RWD_SUV_19")		out = 66920		bChromies = TRUE BREAK // Cutter (CHROME)
		CASE HASH("RWD_SUV_20")		out = 79375		bChromies = TRUE BREAK // Split Six (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_OFFR_01")	out = 55640		bChromies = TRUE BREAK // Raider (CHROME)
		CASE HASH("RWD_OFFR_02")	out = 55405		bChromies = TRUE BREAK // Mudslinger (CHROME)
		CASE HASH("RWD_OFFR_03")	out = 58695		bChromies = TRUE BREAK // Nevis (CHROME)
		CASE HASH("RWD_OFFR_04")	out = 63160		bChromies = TRUE BREAK // Cairngorm (CHROME)
		CASE HASH("RWD_OFFR_05")	out = 52820		bChromies = TRUE BREAK // Amazon (CHROME)
		CASE HASH("RWD_OFFR_06")	out = 76085		bChromies = TRUE BREAK // Challenger (CHROME)
		CASE HASH("RWD_OFFR_07")	out = 72795		bChromies = TRUE BREAK // Dune Basher (CHROME)
		CASE HASH("RWD_OFFR_08")	out = 65040		bChromies = TRUE BREAK // Five Star (CHROME)
		CASE HASH("RWD_OFFR_09")	out = 53525		bChromies = TRUE BREAK // Rock Crawler (CHROME)
		CASE HASH("RWD_OFFR_10")	out = 61750		bChromies = TRUE BREAK // Mil Spec Steelie (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_DRFT_01")	out = 65510		bChromies = TRUE BREAK // Cosmo (CHROME)
		CASE HASH("RWD_DRFT_02")	out = 57990		bChromies = TRUE BREAK // Super Mesh (CHROME)
		CASE HASH("RWD_DRFT_03")	out = 54700		bChromies = TRUE BREAK // Outsider (CHROME)
		CASE HASH("RWD_DRFT_04")	out = 60340		bChromies = TRUE BREAK // Rollas (CHROME)
		CASE HASH("RWD_DRFT_05")	out = 54230		bChromies = TRUE BREAK // Driftmeister (CHROME)
		CASE HASH("RWD_DRFT_06")	out = 64335		bChromies = TRUE BREAK // Slicer (CHROME)
		CASE HASH("RWD_DRFT_07")	out = 74440		bChromies = TRUE BREAK // El Quatro (CHROME)
		CASE HASH("RWD_DRFT_08")	out = 78905		bChromies = TRUE BREAK // Dubbed (CHROME)
		CASE HASH("RWD_DRFT_09")	out = 61515		bChromies = TRUE BREAK // Five Star (CHROME)
		CASE HASH("RWD_DRFT_10")	out = 77730		bChromies = TRUE BREAK // Slideways (CHROME)
		CASE HASH("RWD_DRFT_11")	out = 65275		bChromies = TRUE BREAK // Apex (CHROME)
		CASE HASH("RWD_DRFT_12")	out = 55170		bChromies = TRUE BREAK // Stanced EG (CHROME)
		CASE HASH("RWD_DRFT_13")	out = 59165		bChromies = TRUE BREAK // Countersteer (CHROME)
		CASE HASH("RWD_DRFT_14")	out = 59870		bChromies = TRUE BREAK // Endo v.1 (CHROME)
		CASE HASH("RWD_DRFT_15")	out = 53760		bChromies = TRUE BREAK // Endo v.2 Dish (CHROME)
		CASE HASH("RWD_DRFT_16")	out = 60810		bChromies = TRUE BREAK // Gruppe Z (CHROME)
		CASE HASH("RWD_DRFT_17")	out = 64805		bChromies = TRUE BREAK // Choku-Dori (CHROME)
		CASE HASH("RWD_DRFT_18")	out = 53995		bChromies = TRUE BREAK // Chicane (CHROME)
		CASE HASH("RWD_DRFT_19")	out = 54935		bChromies = TRUE BREAK // Saisoku (CHROME)
		CASE HASH("RWD_DRFT_20")	out = 54465		bChromies = TRUE BREAK // Dished Eight (CHROME)
		CASE HASH("RWD_DRFT_21")	out = 51645		bChromies = TRUE BREAK // Fujiwara (CHROME)
		CASE HASH("RWD_DRFT_22")	out = 60575		bChromies = TRUE BREAK // Zokusha (CHROME)
		CASE HASH("RWD_DRFT_23")	out = 75145		bChromies = TRUE BREAK // Battle VIII (CHROME)
		CASE HASH("RWD_DRFT_24")	out = 78670		bChromies = TRUE BREAK // Rally Master (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_BIKEW_01")	out = 16980		bChromies = TRUE BREAK // Speedway (CHROME)
		CASE HASH("RWD_BIKEW_02")	out = 17137		bChromies = TRUE BREAK // Street Special (CHROME)
		CASE HASH("RWD_BIKEW_03")	out = 18625		bChromies = TRUE BREAK // Racer (CHROME)
		CASE HASH("RWD_BIKEW_04")	out = 19095		bChromies = TRUE BREAK // Track Star (CHROME)
		CASE HASH("RWD_BIKEW_05")	out = 16823		bChromies = TRUE BREAK // Overlord (CHROME)
		CASE HASH("RWD_BIKEW_06")	out = 16667		bChromies = TRUE BREAK // Trident (CHROME)
		CASE HASH("RWD_BIKEW_07")	out = 19800		bChromies = TRUE BREAK // Triple Threat (CHROME)
		CASE HASH("RWD_BIKEW_08")	out = 20897		bChromies = TRUE BREAK // Stilleto (CHROME)
		CASE HASH("RWD_BIKEW_09")	out = 20348		bChromies = TRUE BREAK // Wires (CHROME)
		CASE HASH("RWD_BIKEW_10")	out = 20662		bChromies = TRUE BREAK // Bobber (CHROME)
		CASE HASH("RWD_BIKEW_11")	out = 23873		bChromies = TRUE BREAK // Solidus (CHROME)
		CASE HASH("RWD_BIKEW_12")	out = 24422		bChromies = TRUE BREAK // Ice Shield (CHROME)
		CASE HASH("RWD_BIKEW_13")	out = 24343		bChromies = TRUE BREAK // Loops (CHROME)
	ENDSWITCH
	SWITCH iHash
		CASE HASH("BIKEW_14")	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Romper_Racing)    		bChromies = FALSE BREAK // Romper Racing
		CASE HASH("BIKEW_15") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Warp_Drive)       		bChromies = FALSE BREAK // Warp Drive
		CASE HASH("BIKEW_16") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Snowflake)        		bChromies = FALSE BREAK // Snowflake
		CASE HASH("BIKEW_17") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Holy_Spoke)       		bChromies = FALSE BREAK // Holy Spoke
		CASE HASH("BIKEW_18") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Old_Skool_Triple) 		bChromies = FALSE BREAK // Old Skool Triple
		CASE HASH("BIKEW_19") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Futura)           		bChromies = FALSE BREAK // Futura
		CASE HASH("BIKEW_20") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Quarter_Mile_King)		bChromies = FALSE BREAK // Quarter Mile King
		CASE HASH("BIKEW_21") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Cartwheel)        		bChromies = FALSE BREAK // Cartwheel
		CASE HASH("BIKEW_22") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Double_Five)      		bChromies = FALSE BREAK // Double Five
		CASE HASH("BIKEW_23") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Shuriken)         		bChromies = FALSE BREAK // Shuriken
		CASE HASH("BIKEW_24") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Simple_Six)       		bChromies = FALSE BREAK // Simple Six
		CASE HASH("BIKEW_25") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Celtic)           		bChromies = FALSE BREAK // Celtic
		CASE HASH("BIKEW_26") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Razer)            		bChromies = FALSE BREAK // Razer
		CASE HASH("BIKEW_27") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Twisted)          		bChromies = FALSE BREAK // Twisted
		CASE HASH("BIKEW_28")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Morning_Star)     		bChromies = FALSE BREAK // Morning Star
		CASE HASH("BIKEW_29")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Jagged_Spokes)    		bChromies = FALSE BREAK // Jagged Spokes
		CASE HASH("BIKEW_30")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Eidolon)          		bChromies = FALSE BREAK // Eidolon
		CASE HASH("BIKEW_31")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Enigma)           		bChromies = FALSE BREAK // Enigma
		CASE HASH("BIKEW_32")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Big_Spokes)       		bChromies = FALSE BREAK // Big Spokes
		CASE HASH("BIKEW_33") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Webs)             		bChromies = FALSE BREAK // Webs
		CASE HASH("BIKEW_34")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Hotplate)         		bChromies = FALSE BREAK // Hotplate
		CASE HASH("BIKEW_35")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Bobsta)           		bChromies = FALSE BREAK // Bobsta
		CASE HASH("BIKEW_36") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Grouch)           		bChromies = FALSE BREAK // Grouch
	ENDSWITCH
	SWITCH iHash
		CASE HASH("RWD_BIKEW_14")	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Romper_Racing_CHROME)   		bChromies = TRUE BREAK // Romper Racing
		CASE HASH("RWD_BIKEW_15") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Warp_Drive_CHROME)      		bChromies = TRUE BREAK // Warp Drive
		CASE HASH("RWD_BIKEW_16") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Snowflake_CHROME)       		bChromies = TRUE BREAK // Snowflake
		CASE HASH("RWD_BIKEW_17") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Holy_Spoke_CHROME)      		bChromies = TRUE BREAK // Holy Spoke
		CASE HASH("RWD_BIKEW_18") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Old_Skool_Triple_CHROME)		bChromies = TRUE BREAK // Old Skool Triple
		CASE HASH("RWD_BIKEW_19") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Futura_CHROME)          		bChromies = TRUE BREAK // Futura
		CASE HASH("RWD_BIKEW_20") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Quarter_Mile_King_CHROME)		bChromies = TRUE BREAK // Quarter Mile King
		CASE HASH("RWD_BIKEW_21") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Cartwheel_CHROME)       		bChromies = TRUE BREAK // Cartwheel
		CASE HASH("RWD_BIKEW_22") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Double_Five_CHROME)     		bChromies = TRUE BREAK // Double Five
		CASE HASH("RWD_BIKEW_23") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Shuriken_CHROME)        		bChromies = TRUE BREAK // Shuriken
		CASE HASH("RWD_BIKEW_24") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Simple_Six_CHROME)      		bChromies = TRUE BREAK // Simple Six
		CASE HASH("RWD_BIKEW_25") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Celtic_CHROME)          		bChromies = TRUE BREAK // Celtic
		CASE HASH("RWD_BIKEW_26") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Razer_CHROME)           		bChromies = TRUE BREAK // Razer
		CASE HASH("RWD_BIKEW_27") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Twisted_CHROME)         		bChromies = TRUE BREAK // Twisted
		CASE HASH("RWD_BIKEW_28")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Morning_Star_CHROME)    		bChromies = TRUE BREAK // Morning Star
		CASE HASH("RWD_BIKEW_29")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Jagged_Spokes_CHROME)   		bChromies = TRUE BREAK // Jagged Spokes
		CASE HASH("RWD_BIKEW_30")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Eidolon_CHROME)         		bChromies = TRUE BREAK // Eidolon
		CASE HASH("RWD_BIKEW_31")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Enigma_CHROME)          		bChromies = TRUE BREAK // Enigma
		CASE HASH("RWD_BIKEW_32")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Big_Spokes_CHROME)      		bChromies = TRUE BREAK // Big Spokes
		CASE HASH("RWD_BIKEW_33") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Webs_CHROME)            		bChromies = TRUE BREAK // Webs
		CASE HASH("RWD_BIKEW_34")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Hotplate_CHROME)        		bChromies = TRUE BREAK // Hotplate
		CASE HASH("RWD_BIKEW_35")   out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Bobsta_CHROME)          		bChromies = TRUE BREAK // Bobsta
		CASE HASH("RWD_BIKEW_36") 	out = TO_FLOAT(g_sMPTunables.iBiker_CarMods_Grouch_CHROME)          		bChromies = TRUE BREAK // Grouch
	ENDSWITCH
	
	IF bChromies								
		IF ARE_CHROME_WHEELS_FREE(iHash)		
			out = 0
		ENDIF
		SWITCH eWheelType
			CASE MWT_SPORT 		out = out * g_sMptunables.fCar_Mods_Chrome_Sport_Wheels		BREAK
			CASE MWT_MUSCLE 	out = out * g_sMptunables.fCar_Mods_Chrome_Muscle_Wheels	BREAK
			CASE MWT_LOWRIDER 	out = out * g_sMptunables.fCar_Mods_Chrome_Lowrider_Wheels	BREAK
			CASE MWT_SUV 		out = out * g_sMptunables.fCar_Mods_Chrome_SUV_Wheels		BREAK
			CASE MWT_OFFROAD 	out = out * g_sMptunables.fCar_Mods_Chrome_Offroad_Wheels	BREAK
			CASE MWT_TUNER 		out = out * g_sMptunables.fCar_Mods_Chrome_Tuner_Wheels 	BREAK
			CASE MWT_BIKE 		out = out * g_sMptunables.fCar_Mods_Chrome_Bike_Wheels		BREAK
			CASE MWT_HIEND 		out = out * g_sMptunables.fCar_Mods_Chrome_High_End_Wheels 	BREAK
			CASE MWT_SUPERMOD1  out = out * g_sMptunables.fCar_Mods_Chrome_Supermod_1_Wheels 	BREAK
			CASE MWT_SUPERMOD2  out = out * g_sMptunables.fCar_Mods_Chrome_Supermod_2_Wheels 	BREAK
			CASE MWT_SUPERMOD3  out = out * g_sMptunables.fCar_Mods_Chrome_Supermod_3_Wheels 	BREAK
			CASE MWT_SUPERMOD4  out = out * g_sMptunables.fCar_Mods_Chrome_Supermod_4_Wheels 	BREAK
			CASE MWT_SUPERMOD5  out = out * g_sMptunables.fCar_Mods_Chrome_Supermod_5_Wheels 	BREAK
		ENDSWITCH
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF out = -2.0
			PRINTLN("GET_MP_CARMOD_MENU_WHEEL_COST - missing label: ", tlLabel)
			SCRIPT_ASSERT("GET_MP_CARMOD_MENU_WHEEL_COST - missing label. Tell Kenneth R.")
		ENDIF
	#ENDIF
	
	// Donk x1.5
	IF iWheel != 0
		IF GET_VEHICLE_WHEEL_MOD_PRICE_VARIATION_FOR_CATALOGUE(GET_ENTITY_MODEL(g_ModVehicle)) = ENUM_TO_INT(MPV_FACTION3_WHEELS)
			out = out *	g_sMPTunables.fCar_Mods_Bennys_Bespoke_Wheels_Donk_Multiplier
		ENDIF
	ENDIF
	
	RETURN FLOOR(out * g_sMPTunableGroups.fwheel_mod_group_modifier)	
ENDFUNC

FUNC INT GET_CARMOD_MENU_WHEEL_COST(STRING strLabel, MOD_WHEEL_TYPE eWheelType, INT iWheel, BOOL bCar)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
		OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
		#IF FEATURE_DLC_1_2022
		OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
		#ENDIF
			RETURN 0
		ENDIF
		RETURN GET_MP_CARMOD_MENU_WHEEL_COST(strLabel, eWheelType, iWheel, bCar) // WE DIVIDE BY 2 AS ALL PRICES ARE DOUBLED LATER ON 
	ENDIF
	
	RETURN GET_SP_CARMOD_MENU_WHEEL_COST(strLabel, eWheelType, iWheel, bCar)
ENDFUNC


FUNC BOOL CAN_LIVERY_BE_CHANGED(VEHICLE_INDEX mVeh)
	IF IS_VEHICLE_DRIVEABLE(mVeh)
		IF (GET_ENTITY_MODEL(mVeh) = BATI2)
		OR (GET_ENTITY_MODEL(mVeh) = SANCHEZ)
			RETURN TRUE
		ENDIF
		IF GET_VEHICLE_LIVERY_COUNT(mVeh) > 1
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL CAN_LIVERY_2_BE_CHANGED(VEHICLE_INDEX mVeh)
	IF IS_VEHICLE_DRIVEABLE(mVeh)
		IF GET_VEHICLE_LIVERY2_COUNT(mVeh) > 1
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC COPY_SHOP_DATA_VALUES_TO_GLOBALS_FOR_VALUE_FUNCTIONS(VEHICLE_INDEX vehID, SHOP_NAME_ENUM eShopName, INT iPersonalCarModVar)
	g_ModVehicle = VehID
	g_eModShopName = eShopName
	g_iPersonalCarModVar = iPersonalCarModVar
ENDPROC

FUNC BOOL IGNORE_SELL_VALUE_FOR_MOD(VEHICLE_INDEX vehID, MOD_TYPE eModType, INT iModIndex, BOOL bBulletProofTire = FALSE)
	
	 MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehID)
	 
	IF SHOULD_APPLY_ARMOUR_WITH_ARMOR_PLATING(vehModel)
		IF eModType  = MOD_ARMOUR
			RETURN TRUE
		ENDIF	
	ENDIF
	
	SWITCH vehModel 
		CASE INSURGENT3
			SWITCH eModType
				CASE MOD_ARMOUR
					RETURN TRUE
				BREAK
				CASE MOD_CHASSIS
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
				CASE MOD_LIVERY
					IF iModIndex = 0
					OR iModIndex >= 21
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bBulletProofTire 
				RETURN TRUE
			ENDIF	
		BREAK
		CASE TECHNICAL3
			SWITCH eModType
				CASE MOD_CHASSIS
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
				CASE MOD_LIVERY
					IF iModIndex = 0
					OR iModIndex >= 21
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bBulletProofTire 
				RETURN TRUE
			ENDIF	
		BREAK
		CASE VAGNER
			SWITCH eModType
				CASE MOD_SPOILER
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE STARLING
			SWITCH eModType
				CASE MOD_GEARBOX
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE OMNIS
			SWITCH eModType
				CASE MOD_LIVERY
					IF HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS()
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE APC
		CASE HALFTRACK
		CASE NIGHTSHARK
		CASE RIOT2
		CASE THRUSTER
		CASE KHANJALI
			SWITCH eModType
				CASE MOD_LIVERY
					IF iModIndex >= 21
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bBulletProofTire 
				RETURN TRUE
			ENDIF
		BREAK
		CASE MOLOTOK
		CASE STREITER
		CASE COMET4
		CASE CHERNOBOG		
		CASE BARRAGE
		CASE AVENGER
		CASE TRAILERSMALL2
		CASE TAMPA3
		CASE OPPRESSOR
		CASE DUNE3
		CASE ARDENT
		CASE AKULA
		CASE TRAILERLARGE
		CASE SPEEDO4
		CASE PATRIOT
		CASE PATRIOT2
		CASE MULE4
		CASE POUNDER2
		CASE OPPRESSOR2
		CASE MENACER
		CASE REVOLTER
			SWITCH eModType
				CASE MOD_LIVERY
					IF iModIndex >= 21
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE SWINGER
			SWITCH eModType
				CASE MOD_LIVERY
					IF iModIndex >= 9
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE STAFFORD
			SWITCH eModType
				CASE MOD_LIVERY
					IF iModIndex = 7
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE CLIQUE
			SWITCH eModType
				CASE MOD_LIVERY
					IF iModIndex = 10
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		
		CASE CERBERUS
		CASE CERBERUS2
		CASE CERBERUS3
		CASE SCARAB
		CASE SCARAB2
		CASE SCARAB3
		CASE MONSTER3
		CASE MONSTER4
		CASE MONSTER5
			SWITCH eModType
				CASE MOD_CHASSIS2
					IF iModIndex =  3
						RETURN TRUE
					ENDIF
				BREAK	
				CASE MOD_LIVERY
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
			
			IF bBulletProofTire
				RETURN TRUE
			ENDIF
			
		BREAK
		CASE SLAMVAN4
		CASE SLAMVAN5
		CASE SLAMVAN6
		CASE BRUISER
		CASE BRUISER2
		CASE BRUISER3
			SWITCH eModType
				CASE MOD_CHASSIS2
					IF iModIndex =  3
						RETURN TRUE
					ENDIF
				BREAK	
				CASE MOD_LIVERY
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH	
		BREAK
		CASE BRUTUS
		CASE BRUTUS2
		CASE BRUTUS3
			SWITCH eModType
				CASE MOD_CHASSIS2
					IF iModIndex = 3
						RETURN TRUE
					ENDIF
				BREAK
				CASE MOD_LIVERY
					IF iModIndex =  0
						RETURN TRUE
					ENDIF
				BREAK	
			ENDSWITCH
		BREAK
		CASE ZR380
		CASE ZR3802
		CASE ZR3803
		CASE IMPERATOR
		CASE IMPERATOR2
		CASE IMPERATOR3
		CASE DOMINATOR4
		CASE DOMINATOR5
		CASE DOMINATOR6
		CASE IMPALER2
		CASE IMPALER3
		CASE IMPALER4
		CASE ISSI4
		CASE ISSI5
		CASE ISSI6
			SWITCH eModType
				CASE MOD_CHASSIS2
				CASE MOD_LIVERY
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE DEATHBIKE
		CASE DEATHBIKE2
		CASE DEATHBIKE3
			SWITCH eModType
				CASE MOD_CHASSIS3
				CASE MOD_LIVERY
					IF iModIndex = 0
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bBulletProofTire
				RETURN TRUE
			ENDIF
			
		BREAK
		CASE DUKES2
		CASE VETO
		CASE VETO2
			IF bBulletProofTire
				RETURN TRUE
			ENDIF
		BREAK
		CASE PARAGON2
			SWITCH eModType
				CASE MOD_ARMOUR
					RETURN TRUE
				BREAK
			ENDSWITCH
			
			IF bBulletProofTire
				RETURN TRUE
			ENDIF	
		BREAK
		CASE ALKONOST
			SWITCH eModType
				CASE MOD_WING_R
					IF iModIndex = 3
						RETURN TRUE
					ENDIF	
				BREAK
			ENDSWITCH
		BREAK
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE IGNUS2
			SWITCH eModType
				CASE MOD_GRILL
					IF iModIndex = 0
						RETURN TRUE
					ENDIF	
				BREAK
				CASE MOD_TOGGLE_TURBO
					// This will check if vehicle is upgraded to HSW
					IF IS_VEHICLE_A_HSW_VEHICLE(vehID)
						RETURN TRUE
					ENDIF	
				BREAK
				CASE MOD_KNOB
					IF iModIndex = 1
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		DEFAULT
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_VEHICLE_A_HSW_VEHICLE(vehID)
				SWITCH eModType
					// default mods which HSW upgrade gives for free
					CASE MOD_TOGGLE_TURBO
						RETURN TRUE
					BREAK
					CASE MOD_KNOB
						IF iModIndex = 1
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH	
			ENDIF
			#ENDIF
		BREAK
	ENDSWITCH
	
	IF eModType = MOD_LIVERY
		IF IS_XMAS_LIVERIES_FREE(vehID, GET_HASH_KEY(GET_MOD_TEXT_LABEL(vehID, eModType, iModIndex)), CMM_SUPERMOD_LIVERY #IF FEATURE_DLC_1_2022 , iModIndex #ENDIF)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_IGNORE_NON_MOD_LIVERY_PRICE(VEHICLE_INDEX vehID, BOOL IsThisFreeVeh, BOOL bIsStarterPack)
	IF GET_ENTITY_MODEL(vehID) = WINDSOR
	AND (HAS_MP_CRIMINAL_ENTERPRISE_PREMIUM_OR_STARTER_ACCESS() OR bIsStarterPack)
		RETURN TRUE
	ENDIF
	
	IF IsThisFreeVeh
		SWITCH GET_ENTITY_MODEL(vehID)
			CASE SANCHEZ
			CASE BATI
			CASE BATI2
				RETURN TRUE
			BREAK
		ENDSWITCH 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CARMOD_MOD_SELL_VALUE(VEHICLE_INDEX vehID, SHOP_NAME_ENUM eShopName = CARMOD_SHOP_01_AP, INT iPersonalCarModVar = 0, BOOL bIsStarterPack = FALSE, BOOL IsThisFreeVeh = FALSE, BOOL bIgnoreSaleModifier = FALSE)
	
	INT iModValue, iCachedModValue
	FLOAT fModValueModifier
	
	IF DOES_ENTITY_EXIST(vehID)
	AND IS_VEHICLE_DRIVEABLE(vehID)
		IF (NETWORK_IS_GAME_IN_PROGRESS() AND NETWORK_HAS_CONTROL_OF_ENTITY(vehID)) OR (NOT NETWORK_IS_GAME_IN_PROGRESS())
			COPY_SHOP_DATA_VALUES_TO_GLOBALS_FOR_VALUE_FUNCTIONS(vehID, eShopName, iPersonalCarModVar)
			
			iModValue = 0
			
			IF NOT bIgnoreSaleModifier 
				IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(vehID))
					fModValueModifier = g_sMPTunables.fpurchasedaircraftmodsalemodifier
				ELSE
					fModValueModifier = g_sMPTunables.fpurchasedcarmodsalemodifier
				ENDIF
			ELSE
				fModValueModifier = 1.0
			ENDIF	
			
			CPRINTLN(DEBUG_SHOPS, "GET_CARMOD_MOD_SELL_VALUE, mod modifier = ", fModValueModifier, " bIgnoreSaleModifier: ", GET_STRING_FROM_BOOL(bIgnoreSaleModifier))
			
			IF GET_VEHICLE_MOD_KIT(vehID) >= 0
				IF GET_VEHICLE_MOD(vehID, MOD_SPOILER) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_SPOILER, GET_VEHICLE_MOD(vehID, MOD_SPOILER))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_SPOILER, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_SPOILER) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_SPOILER = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_SKIRT) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_SKIRT, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_SKIRT) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_SKIRT = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_EXHAUST) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_EXHAUST, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_EXHAUST) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_EXHAUST = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_CHASSIS, GET_VEHICLE_MOD(vehID, MOD_CHASSIS))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_CHASSIS, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_CHASSIS) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_CHASSIS = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_GRILL) != -1
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_GRILL, GET_VEHICLE_MOD(vehID, MOD_GRILL))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_GRILL, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_GRILL) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_GRILL = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_BONNET) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_BONNET, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_BONNET) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_BONNET = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ROOF) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ROOF, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ROOF) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_ROOF = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ENGINE) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ENGINE, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ENGINE) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_ENGINE = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_BRAKES) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_BRAKES, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_BRAKES) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_BRAKES = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_GEARBOX) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_GEARBOX, GET_VEHICLE_MOD(vehID, MOD_GEARBOX))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_GEARBOX, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_GEARBOX) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_GEARBOX = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_HORN) != -1	
					// The horn menu layout has changed so we need to get the correct price.
					INT iHornMenuOption = ENUM_TO_INT(GET_CARMOD_HORN_MENU_OPTION_FROM_HASH(GET_VEHICLE_MOD_IDENTIFIER_HASH(vehID, MOD_HORN, GET_VEHICLE_MOD(vehID, MOD_HORN))))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_HORN, FALSE, DEFAULT), iHornMenuOption)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_HORN = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_SUSPENSION) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_SUSPENSION, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_SUSPENSION) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_SUSPENSION = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ARMOUR) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_ARMOUR, GET_VEHICLE_MOD(vehID, MOD_ARMOUR))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ARMOUR, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ARMOUR)+1)
					iModValue += iCachedModValue 
					CPRINTLN(DEBUG_SHOPS, "...MOD_ARMOUR = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_WHEELS) != -1	
					iCachedModValue = GET_CARMOD_MENU_WHEEL_COST(GET_MOD_TEXT_LABEL(vehID, MOD_WHEELS, GET_VEHICLE_MOD(vehID, MOD_WHEELS)), GET_VEHICLE_WHEEL_TYPE(vehID), GET_VEHICLE_MOD(vehID, MOD_WHEELS)+1, IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehID)))
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_WHEELS = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_REAR_WHEELS) != -1
				AND IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(vehID))
					iCachedModValue = GET_CARMOD_MENU_WHEEL_COST(GET_MOD_TEXT_LABEL(vehID, MOD_REAR_WHEELS, GET_VEHICLE_MOD(vehID, MOD_REAR_WHEELS)), GET_VEHICLE_WHEEL_TYPE(vehID), GET_VEHICLE_MOD(vehID, MOD_REAR_WHEELS)+1, IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehID)))
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_REAR_WHEELS = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_BUMPER_F) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_BUMPER_F, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_BUMPER_F)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_BUMPER_F = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_BUMPER_R) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_BUMPER_R, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_BUMPER_R)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_BUMPER_R = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_WING_L) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_WING_L, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_WING_L)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_WING_L = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_WING_R) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_WING_R, GET_VEHICLE_MOD(vehID, MOD_WING_R))
					INT iModIndex = GET_VEHICLE_MOD(vehID, MOD_WING_R)
					IF g_eModPriceVariation = MPV_GR_HEAVY // url:bugstar:3678474 - Is this affecting all vehicles, or just GR? Might be worth looking into 
					OR g_eModPriceVariation = MPV_GR_LIGHT
					OR g_eModPriceVariation = MPV_SMUG_HEAVY
					OR g_eModPriceVariation = MPV_SMUG_STANDARD
					OR g_eModPriceVariation = MPV_ARENA_WARS
					OR g_eModPriceVariation = MPV_RC_BANDITO
						iModIndex += 21
					ENDIF
					
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_WING_R, FALSE, DEFAULT), iModIndex + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_WING_R = $", iCachedModValue * fModValueModifier, " mode menu: ", GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_WING_R))
				ENDIF
				IF IS_TOGGLE_MOD_ON(vehID, MOD_TOGGLE_TURBO)
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_TOGGLE_TURBO, 1)
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_TOGGLE_TURBO), 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_TOGGLE_TURBO = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF IS_TOGGLE_MOD_ON(vehID, MOD_TOGGLE_XENON_LIGHTS)
					INT iModIndex
					SWITCH GET_VEHICLE_XENON_LIGHT_COLOR_INDEX(vehID)
						CASE 255	iModIndex = 1	BREAK
						CASE 0		iModIndex = 2	BREAK
						CASE 1		iModIndex = 3	BREAK
						CASE 2		iModIndex = 4	BREAK
						CASE 3		iModIndex = 5	BREAK
						CASE 4		iModIndex = 6	BREAK
						CASE 5		iModIndex = 7	BREAK
						CASE 6		iModIndex = 8	BREAK
						CASE 7		iModIndex = 9	BREAK
						CASE 8		iModIndex = 10	BREAK
						CASE 9		iModIndex = 11	BREAK
						CASE 10		iModIndex = 12	BREAK
						CASE 11		iModIndex = 13	BREAK
						CASE 12		iModIndex = 14	BREAK
						CASE 13		iModIndex = 15	BREAK
					ENDSWITCH	
					
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(CMM_LIGHTS_HEAD, iModIndex)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_TOGGLE_XENON_LIGHTS = $", iCachedModValue * fModValueModifier)
				ENDIF
				
				IF NOT GET_VEHICLE_TYRES_CAN_BURST(vehID)
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, INT_TO_ENUM(MOD_TYPE, -1), -1, TRUE)
				AND NOT IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(eShopName, iPersonalCarModVar)
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(CMM_WHEEL_ACCS, WHEEL_ACCS_OPTION_BPT)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...BPT = $", iCachedModValue * fModValueModifier)
				ENDIF
				
				IF GET_DRIFT_TYRES_SET(vehID)
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, INT_TO_ENUM(MOD_TYPE, -1), -1, TRUE)
				AND NOT IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(eShopName, iPersonalCarModVar)
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(CMM_WHEEL_ACCS, WHEEL_ACCS_OPTION_LGT)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...LGT = $", iCachedModValue * fModValueModifier)
				ENDIF
				
				IF GET_VEHICLE_MOD(vehID, MOD_PLTHOLDER) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_PLTHOLDER, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_PLTHOLDER)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_PLTHOLDER = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_PLTVANITY) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_PLTVANITY, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_PLTVANITY)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_PLTVANITY = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_INTERIOR1) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_INTERIOR1, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_INTERIOR1)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_INTERIOR1 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_INTERIOR2) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_INTERIOR2, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_INTERIOR2)+1, DEFAULT, GET_MOD_TEXT_LABEL(vehID, MOD_INTERIOR2, GET_VEHICLE_MOD(vehID, MOD_INTERIOR2)))
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_INTERIOR2 = $", iCachedModValue * fModValueModifier, ", label = ", GET_MOD_TEXT_LABEL(vehID, MOD_INTERIOR2, GET_VEHICLE_MOD(vehID, MOD_INTERIOR2)))
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_INTERIOR3) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_INTERIOR3, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_INTERIOR3)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_INTERIOR3 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_INTERIOR4) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_INTERIOR4, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_INTERIOR4)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_INTERIOR4 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_INTERIOR5) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_INTERIOR5, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_INTERIOR5)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_INTERIOR5 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_SEATS) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_SEATS, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_SEATS)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_SEATS = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_STEERING) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_STEERING, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_STEERING)+1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_STEERING = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_KNOB) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_KNOB, GET_VEHICLE_MOD(vehID, MOD_KNOB))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_KNOB, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_KNOB) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_KNOB = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_PLAQUE) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_PLAQUE, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_PLAQUE) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_PLAQUE = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ICE) != -1	
				#IF FEATURE_GEN9_EXCLUSIVE
				AND NOT IS_VEHICLE_A_HSW_VEHICLE(vehID)
				#ENDIF
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ICE, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ICE) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_ICE = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_TRUNK) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_TRUNK, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_TRUNK) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_TRUNK = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_HYDRO) > 0 // skip first option	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_HYDRO, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_HYDRO) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_HYDRO = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ENGINEBAY1) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ENGINEBAY1, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ENGINEBAY1) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_ENGINEBAY1 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ENGINEBAY2) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ENGINEBAY2, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ENGINEBAY2) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_ENGINEBAY2 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_ENGINEBAY3) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_ENGINEBAY3, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_ENGINEBAY3) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_ENGINEBAY3 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS2) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_CHASSIS2, GET_VEHICLE_MOD(vehID, MOD_CHASSIS2))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_CHASSIS2, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_CHASSIS2) + 1)
					iModValue += iCachedModValue 
					CPRINTLN(DEBUG_SHOPS, "...MOD_CHASSIS2 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS3) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_CHASSIS3, GET_VEHICLE_MOD(vehID, MOD_CHASSIS3))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_CHASSIS3, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_CHASSIS3) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_CHASSIS3 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS4) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_CHASSIS4, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_CHASSIS4) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_CHASSIS4 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_CHASSIS5) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_CHASSIS5, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_CHASSIS5) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_CHASSIS5 = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_DOOR_L) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_DOOR_L, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_DOOR_L) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_DOOR_L = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_DOOR_R) != -1	
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_DOOR_R, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_DOOR_R) + 1)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_DOOR_R = $", iCachedModValue * fModValueModifier)
				ENDIF
				IF GET_VEHICLE_MOD(vehID, MOD_LIVERY) != -1	
				AND NOT IGNORE_SELL_VALUE_FOR_MOD(vehID, MOD_LIVERY, GET_VEHICLE_MOD(vehID, MOD_LIVERY))
					iCachedModValue = GET_CARMOD_MENU_OPTION_COST(GET_CARMOD_MENU_FOR_MOD_SLOT(MOD_LIVERY, FALSE, DEFAULT), GET_VEHICLE_MOD(vehID, MOD_LIVERY) + 1, DEFAULT, DEFAULT, DEFAULT, TRUE)
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...MOD_LIVERY = $", iCachedModValue * fModValueModifier)
				ENDIF
			ENDIF
			IF CAN_LIVERY_BE_CHANGED(vehID)
				IF SHOULD_IGNORE_NON_MOD_LIVERY_PRICE(vehID, IsThisFreeVeh, bIsStarterPack)
					// Skip
					CPRINTLN(DEBUG_SHOPS, "...Livery = $0 *block*")
				ELSE
					iCachedModValue = FLOOR((TO_FLOAT(GET_VEHICLE_LIVERY_COST(vehID, GET_VEHICLE_LIVERY(vehID)))))
					iModValue += iCachedModValue
					CPRINTLN(DEBUG_SHOPS, "...Livery = $", iCachedModValue * fModValueModifier)
				ENDIF
			ENDIF
			IF CAN_LIVERY_2_BE_CHANGED(vehID)
			AND GET_VEHICLE_LIVERY2(vehID) != 0
				iCachedModValue = FLOOR((TO_FLOAT(GET_VEHICLE_LIVERY2_COST(vehID, GET_VEHICLE_LIVERY2(vehID)))))
				iModValue += iCachedModValue
				CPRINTLN(DEBUG_SHOPS, "...Livery2 = $", iCachedModValue *fModValueModifier )
			ENDIF
			
			iModValue = FLOOR((TO_FLOAT(iModValue)*fModValueModifier))
		ENDIF
	ENDIF
	
	PRINTLN("[platedupes] GET_CARMOD_MOD_SELL_VALUE = ", iModValue) 
	
	RETURN iModValue
ENDFUNC

FUNC BOOL IS_VEHICLE_IE_SUPERMOD_MODEL()
	
	IF DOES_ENTITY_EXIST(g_ModVehicle)
		IF IS_VEHICLE_DRIVEABLE(g_ModVehicle)
			IF GET_ENTITY_MODEL(g_ModVehicle) = NERO2
			OR GET_ENTITY_MODEL(g_ModVehicle) = COMET3
			OR GET_ENTITY_MODEL(g_ModVehicle) = ELEGY
			OR GET_ENTITY_MODEL(g_ModVehicle) = ITALIGTB2
			OR GET_ENTITY_MODEL(g_ModVehicle) = SPECTER2
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_JUMP_MOD()
	
	IF DOES_ENTITY_EXIST(g_ModVehicle)
		IF IS_VEHICLE_DRIVEABLE(g_ModVehicle)
			SWITCH GET_ENTITY_MODEL(g_ModVehicle)
				CASE MONSTER3
				CASE CERBERUS
				CASE BRUISER
				CASE BRUTUS
				CASE SCARAB
				CASE DOMINATOR4
				CASE IMPALER2
				CASE IMPERATOR
				CASE IMPERATOR2
				CASE IMPERATOR3
				CASE ZR380
				CASE ISSI4
				CASE SLAMVAN4
				CASE RCBANDITO
				CASE IMPALER3
				CASE BRUTUS2
				CASE BRUISER2
				CASE SLAMVAN5
				CASE ISSI5
				CASE MONSTER4
				CASE SCARAB2
				CASE CERBERUS2
				CASE DOMINATOR5
				CASE ZR3802
				CASE IMPALER4
				CASE BRUTUS3
				CASE BRUISER3
				CASE SLAMVAN6
				CASE ISSI6
				CASE MONSTER5
				CASE SCARAB3
				CASE CERBERUS3
				CASE DOMINATOR6
				CASE ZR3803
				CASE DEATHBIKE
				CASE DEATHBIKE2
				CASE DEATHBIKE3
					RETURN TRUE
				BREAK	
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_BOOST_MOD()
	
	IF DOES_ENTITY_EXIST(g_ModVehicle)
		IF IS_VEHICLE_DRIVEABLE(g_ModVehicle)
			SWITCH GET_ENTITY_MODEL(g_ModVehicle)
				CASE MONSTER3
				CASE CERBERUS
				CASE BRUISER
				CASE BRUTUS
				CASE SCARAB
				CASE DOMINATOR4
				CASE IMPALER2
				CASE IMPERATOR
				CASE IMPERATOR2
				CASE IMPERATOR3
				CASE ZR380
				CASE ISSI4
				CASE DEATHBIKE
				CASE DEATHBIKE2
				CASE DEATHBIKE3
				CASE SLAMVAN4
				CASE IMPALER3
				CASE BRUTUS2
				CASE BRUISER2
				CASE SLAMVAN5
				CASE ISSI5
				CASE MONSTER4
				CASE SCARAB2
				CASE CERBERUS2
				CASE DOMINATOR5
				CASE ZR3802
				CASE IMPALER4
				CASE BRUTUS3
				CASE BRUISER3
				CASE SLAMVAN6
				CASE ISSI6
				CASE MONSTER5
				CASE SCARAB3
				CASE CERBERUS3
				CASE DOMINATOR6
				CASE ZR3803
					RETURN TRUE
				BREAK	
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// Roof menu reordering for weapons override
FUNC BOOL IS_VEHICLE_HAVE_WEAPON_MENU()
	IF DOES_ENTITY_EXIST(g_ModVehicle)
		SWITCH GET_ENTITY_MODEL(g_ModVehicle)
			CASE APC
			CASE HALFTRACK
			CASE DUNE3
			CASE TAMPA3
			CASE TRAILERSMALL2
			CASE INSURGENT3
			CASE TECHNICAL3
			CASE OPPRESSOR
			CASE COMET4
			CASE DELUXO
			CASE REVOLTER
			CASE SAVESTRA
			CASE VISERIS
			CASE CARACARA
			CASE OPPRESSOR2
			CASE CERBERUS
			CASE MONSTER3
			CASE SLAMVAN4
			CASE ISSI4
			CASE DEATHBIKE
			CASE IMPERATOR
			CASE DEATHBIKE2
			CASE DEATHBIKE3
			CASE SLAMVAN5
			CASE ISSI5
			CASE MONSTER4
			CASE CERBERUS2
			CASE DOMINATOR5
			CASE SLAMVAN6
			CASE ISSI6
			CASE MONSTER5
			CASE CERBERUS3
			CASE DOMINATOR6
			CASE IMPERATOR2
			CASE IMPERATOR3
			CASE JB7002
			CASE MINITANK
				RETURN TRUE
		ENDSWITCH
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_HAVE_ARMOR_PLATING_MENU()
	IF DOES_ENTITY_EXIST(g_ModVehicle)
		MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(g_ModVehicle)
		SWITCH vehicleModel
			CASE APC
			CASE HALFTRACK
			CASE DUNE3
			CASE TAMPA3
			CASE TRAILERSMALL2
			CASE INSURGENT3
			CASE TECHNICAL3
			CASE POUNDER2
			CASE SPEEDO4
			CASE MULE4
			CASE IMPERATOR
			CASE DEATHBIKE
			CASE CERBERUS
			CASE BRUISER
			CASE DOMINATOR4
			CASE ZR380
			CASE ISSI4
			CASE IMPERATOR2
			CASE DEATHBIKE2
			CASE CERBERUS2
			CASE BRUISER2
			CASE DOMINATOR5
			CASE ZR3802
			CASE ISSI5
			CASE IMPERATOR3
			CASE DEATHBIKE3
			CASE CERBERUS3
			CASE BRUISER3
			CASE DOMINATOR6
			CASE ZR3803
			CASE ISSI6
			CASE BRUTUS
			CASE BRUTUS2
			CASE BRUTUS3
			#IF FEATURE_COPS_N_CROOKS
			CASE POLICET2
			#ENDIF
				RETURN TRUE
			BREAK
			DEFAULT
				IF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehicleModel)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_MINE_MENU_NEED_REORDERING()
	IF DOES_ENTITY_EXIST(g_ModVehicle)
		MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(g_ModVehicle)
		SWITCH vehicleModel
			CASE APC
			CASE DUNE3
			CASE HALFTRACK
			CASE TAMPA3
			CASE INSURGENT3
			CASE TECHNICAL3
			CASE OPPRESSOR
				RETURN TRUE
			BREAK	
			DEFAULT 
				IF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehicleModel)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CARMOD_MENU_DISPLAY_INDEX(CARMOD_MENU_ENUM eMenu)
	INT iMenuIndex = -1
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(g_ModVehicle)
	// switch around some menus for testing.
	SWITCH eMenu
	
		CASE CMM_MAINTAIN 					iMenuIndex = 0 BREAK
		
		CASE CMM_SUPERMOD					
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 58 
			ELIF IS_BASE_VEHICLE_AVAILBLE_FOR_BENNYS_AND_ARENA_SUPERMOD(vehModel)
				iMenuIndex = 2
			ELSE 
				iMenuIndex = 1
			ENDIF 
		BREAK	
		CASE CMM_SUPERMOD_PLTHOLDER			
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 61 
			ELIF IS_BASE_VEHICLE_AVAILBLE_FOR_BENNYS_AND_ARENA_SUPERMOD(vehModel)
				iMenuIndex = 99
			ELIF vehModel = GROWLER
				iMenuIndex = 55
			ELSE 
				iMenuIndex = 2
			ENDIF 
		BREAK
		CASE CMM_SUPERMOD_PLTVANITY		
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 67 
			ELIF vehModel = GAUNTLET5
				iMenuIndex = 92
			#IF FEATURE_TUNER
			ELIF vehModel = VECTRE
			OR vehModel = ZR350
			OR vehModel = WARRENER2
			OR vehModel = RT3000
			OR vehModel = PREVION
			OR vehModel = EUROS
			OR vehModel = GROWLER
			OR vehModel = COMET6
				iMenuIndex = 74	
			ELIF vehModel = JESTER4	
				iMenuIndex = 30
			ELIF vehModel = CYPHER	
				iMenuIndex = 84
			ELIF vehModel = TAILGATER2	
				iMenuIndex = 30	
			ELIF vehModel = DOMINATOR8
				iMenuIndex = 75	
			ELIF vehModel = FUTO2
				iMenuIndex = 96
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = CORSITA
			OR vehModel = LM87
				iMenuIndex = 74
			ELIF vehModel = TENF2
				iMenuIndex = 30
			ELIF vehModel = POSTLUDE
				iMenuIndex = 75
			#ENDIF
			ELSE 
				iMenuIndex = 3
			ENDIF 
		BREAK
		CASE CMM_TRIM						
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
				iMenuIndex = 33	
			ELIF IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()	
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 34
			ELSE 
				iMenuIndex = 4
			ENDIF 
		BREAK
		CASE CMM_SUPERMOD_INTERIOR2			
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 31
			ELIF vehModel = CYPHER
				iMenuIndex = 10
			ELSE 
				iMenuIndex = 5
			ENDIF 
		BREAK
		CASE CMM_SUPERMOD_INTERIOR3			
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 85
			ELIF vehModel = DOMINATOR4
				iMenuIndex = 65
			ELIF vehModel = ISSI5
				iMenuIndex = 70	
			ELSE 
				iMenuIndex = 6
			ENDIF 
		BREAK
		CASE CMM_SUPERMOD_INTERIOR4			iMenuIndex = 7 BREAK
		CASE CMM_DIALS						iMenuIndex = 8 BREAK
		CASE CMM_SUPERMOD_INTERIOR5			
			IF IS_VEHICLE_MODEL(g_ModVehicle, YOSEMITE3)
				iMenuIndex = 76 
			ELSE	
				iMenuIndex = 9 
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_SEATS	
			IF vehModel = CYPHER
				iMenuIndex = 11
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = POSTLUDE
				iMenuIndex = 11
			#ENDIF
			ELSE
				iMenuIndex = 10 
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_STEERING
			IF vehModel = CYPHER
				iMenuIndex = 5
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = POSTLUDE
				iMenuIndex = 68
			#ENDIF
			ELSE
				iMenuIndex = 11
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_KNOB				
			#IF FEATURE_GEN9_EXCLUSIVE
			IF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
				iMenuIndex = 83 
			ELSE	
			#ENDIF
				iMenuIndex = 12
			#IF FEATURE_GEN9_EXCLUSIVE
			ENDIF
			#ENDIF
		BREAK
		CASE CMM_SUPERMOD_PLAQUE			iMenuIndex = 13 BREAK
		CASE CMM_SUPERMOD_ICE				iMenuIndex = 14 BREAK
		CASE CMM_SUPERMOD_ENGINEBAY1		
			IF IS_VEHICLE_MODEL(g_ModVehicle, GAUNTLET5)
			OR IS_VEHICLE_MODEL(g_ModVehicle, YOUGA3)
				iMenuIndex = 74
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = POSTLUDE
				iMenuIndex = 33
			ELIF vehModel = KANJOSJ
				iMenuIndex = 16
			#ENDIF
			ELSE
				iMenuIndex = 15
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_ENGINEBAY2		
			IF DOES_VEHICLE_HAVE_BOOST_MOD()
				iMenuIndex = 28
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = KANJOSJ
				iMenuIndex = 15
			#ENDIF
			ELSE
				iMenuIndex = 16
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_ENGINEBAY3		
			IF DOES_VEHICLE_HAVE_JUMP_MOD()
				iMenuIndex = 83
			#IF FEATURE_TUNER
			ELIF vehModel = CYPHER
				iMenuIndex = 33
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = KANJOSJ
				iMenuIndex = 33
			ELIF vehModel = POSTLUDE
				iMenuIndex = 23
			#ENDIF
			ELSE
				iMenuIndex = 17
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_CHASSIS2			
			IF vehModel = CALICO
				iMenuIndex = 39	
			#IF FEATURE_TUNER
			ELIF vehModel = CYPHER
				iMenuIndex = 21
			#ENDIF
			ELIF vehModel = COMET6
				iMenuIndex = 47	
			ELIF vehModel = DOMINATOR7
				iMenuIndex = 61	
			ELIF vehModel = TAILGATER2
				iMenuIndex = 60
			ELSE
				iMenuIndex = 18
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_CHASSIS3
			IF vehModel = DOMINATOR7
				iMenuIndex = 18	
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = DRAUGUR
				iMenuIndex = 72
			#ENDIF
			ELSE	
				iMenuIndex = 19 
			ENDIF	
		BREAK
		CASE CMM_SUPERMOD_CHASSIS4			
			IF vehModel = YOUGA3
				iMenuIndex = 70
			ELIF vehModel = SEMINOLE2
				iMenuIndex = 65	
			ELIF vehModel = WARRENER2
				iMenuIndex = 69		
			ELIF vehModel = DOMINATOR7
				iMenuIndex = 19				
			ELIF vehModel = SULTAN3
				iMenuIndex = 70 
			ELIF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehModel)
				iMenuIndex = 53 
			ELSE	
				iMenuIndex = 20 
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_CHASSIS5			
			IF vehModel = COMET6
				iMenuIndex = 74 	
			ELIF vehModel = WARRENER2
			OR vehModel = CALICO
				iMenuIndex = 60
			#IF FEATURE_TUNER
			ELIF vehModel = REMUS
				iMenuIndex = 59
			ELIF vehModel = CYPHER
				iMenuIndex = 18
			ELIF vehModel = TAILGATER2
			OR vehModel = DOMINATOR8
				iMenuIndex = 52	
			#ENDIF	
			ELIF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehModel)
				iMenuIndex = 62
			ELSE
				iMenuIndex = 21 
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_DOOR_L			
			IF vehModel = KANJO
				iMenuIndex = 32
			ELIF vehModel = COMET6
				iMenuIndex = 89 
			ELIF vehModel = ZR350
			OR vehModel = GROWLER
				iMenuIndex = 78
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = WEEVIL2
				iMenuIndex = 40
			#ENDIF
			ELSE
				iMenuIndex = 22
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_DOOR_R			
			IF IS_VEHICLE_MODEL(g_ModVehicle, YOSEMITE3)
				iMenuIndex = 71 
			ELIF vehModel = REMUS
				iMenuIndex = 30		
			ELIF vehModel = GROWLER
				iMenuIndex = 84
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = TORERO2
				iMenuIndex = 84
			ELIF vehModel = POSTLUDE
				iMenuIndex = 15
			#ENDIF
			ELSE	
				iMenuIndex = 23
			ENDIF
		BREAK
		CASE CMM_LIGHT_COLOUR				
			IF vehModel = Z190
				iMenuIndex = 73 
			ELIF vehModel = S80
				iMenuIndex = 39
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = GREENWOOD
				iMenuIndex = 50
			#ENDIF
			ELSE
				iMenuIndex = 24
			ENDIF
		BREAK
		
		CASE CMM_ARMOUR						iMenuIndex = 25 BREAK
		CASE CMM_BODYWORK					
			IF IS_VEHICLE_HAVE_ARMOR_PLATING_MENU()
				iMenuIndex = 30 
			ELIF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			AND vehModel != STRIKEFORCE
				iMenuIndex = 42 
			ELIF vehModel = STRIKEFORCE
				iMenuIndex = 39
			ELIF vehModel = CLUB
				iMenuIndex = 73	
			ELIF vehModel = REEVER
				iMenuIndex = 75			
			ELSE
				iMenuIndex = 26 
			ENDIF
		BREAK
		CASE CMM_BRAKES						
			IF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			OR IS_VEHICLE_MODEL(g_ModVehicle, AVENGER)
				iMenuIndex = 48
			ELIF vehModel = DEITY
			#IF FEATURE_DLC_1_2022
			OR vehModel = GREENWOOD
			#ENDIF
				iMenuIndex = 28
			ELSE
				iMenuIndex = 27 
			ENDIF
		BREAK
		CASE CMM_BULLBARS					
			IF IS_VEHICLE_MODEL(g_ModVehicle, OPPRESSOR2)
				iMenuIndex = 47 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, STRIKEFORCE)
				iMenuIndex = 42
			ELIF DOES_VEHICLE_HAVE_BOOST_MOD()
				iMenuIndex = 16
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, RCBANDITO)
				iMenuIndex = 91	
			ELIF vehModel = DEITY
				iMenuIndex = 91
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = GREENWOOD
				iMenuIndex = 91
			#ENDIF
			ELSE
				iMenuIndex = 28
			ENDIF	
		BREAK
		CASE CMM_BUMPERS					
			IF (IS_THIS_PERSONAL_CAR_MOD_IN_OFFICE(g_eModShopName,g_iPersonalCarModVar) OR g_eModShopName = CARMOD_SHOP_SUPERMOD)
			AND DOES_VEHICLE_HAVE_BUMPER_ACCESSORY_MODS(g_ModVehicle)
				iMenuIndex = 95 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DELUXO)
				iMenuIndex = 43
			ELIF vehModel = SC1
			OR vehModel = FLASHGT
			OR vehModel = ZORRUSSO
			OR vehModel = LOCUST
				iMenuIndex = 38 
			ELIF vehModel = FREECRAWLER
				iMenuIndex = 43
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, TOROS)
				iMenuIndex = 75 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, BRUISER2)
				iMenuIndex = 54
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS)
			OR IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS2)
			OR IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS3)
				iMenuIndex = 33
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = POLRIATA
				iMenuIndex = 67 
			#ENDIF	
			ELIF vehModel = SEMINOLE2
				iMenuIndex = 40 
			ELIF vehModel = VETO
				iMenuIndex = 42	
//			ELIF vehModel = FUTO2
//				iMenuIndex = ?
			ELSE
				iMenuIndex =  29
			ENDIF
		BREAK
		CASE CMM_CHASSIS					
			IF vehModel = GP1
				iMenuIndex = 66
			ELIF IS_VEHICLE_HAVE_ARMOR_PLATING_MENU()
				iMenuIndex = 26 
			ELIF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			OR vehModel = KHANJALI
			OR IS_VEHICLE_MODEL(g_ModVehicle, SULTAN2)
				iMenuIndex = 70 
			ELIF vehModel = FLASHGT
				iMenuIndex = 68 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, MENACER)
				iMenuIndex = 72 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DYNASTY)	
				iMenuIndex = 39 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DRAFTER)
			OR vehModel = ZHABA
			#IF FEATURE_DLC_1_2022
			OR vehModel = DRAUGUR
			#ENDIF
				iMenuIndex = 70 	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, THRAX)	
				iMenuIndex = 81 		
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, REBLA)
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = ITALIRSX
			#ENDIF
				iMenuIndex = 77
			ELIF vehModel = VSTR
				iMenuIndex = 84
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = SURFER3
				iMenuIndex = 70
			#ENDIF
			ELIF vehModel = OPENWHEEL2
				iMenuIndex = 60
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = VETO
			OR vehModel = VETO2
				iMenuIndex = 43	
			#ENDIF	
			ELIF vehModel = REMUS
				iMenuIndex = 23	
			ELIF vehModel = JESTER4
				iMenuIndex = 74	
			ELIF vehModel = TAILGATER2
				iMenuIndex = 3
			#IF FEATURE_FIXER
			ELIF vehModel = ASTRON
				iMenuIndex = 70
			#ENDIF
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = CYCLONE2
			OR vehModel = ARBITERGT
				iMenuIndex = 76
			#ENDIF
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = IGNUS2
				iMenuIndex = 47		
			ELIF vehModel = S95
				iMenuIndex = 60			
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = VIGERO2
				iMenuIndex = 76
			ELIF vehModel = SM722
				iMenuIndex = 68
			ELIF vehModel = TORERO2
				iMenuIndex = 50
			ELIF vehModel = TENF2
				iMenuIndex = 3
			ELIF vehModel = PICADOR
				iMenuIndex = 65
			#ENDIF
			ELSE
				iMenuIndex = 30
			ENDIF
		BREAK
		CASE CMM_CHASSIS_GROUP				
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 5 
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = POLGAUNTLET
				iMenuIndex = 65 
			#ENDIF	
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = SQUADDIE
				iMenuIndex = 39
			#ENDIF
			ELIF IS_MODEL_POLICE_VEHICLE(vehModel #IF FEATURE_COPS_N_CROOKS, TRUE #ENDIF )
				iMenuIndex = 66
			ELIF vehModel = CHAMPION
				iMenuIndex = 47
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = TORERO2
				iMenuIndex = 47
			#ENDIF
			ELSE 
				iMenuIndex = 31
			ENDIF 
		BREAK
		
		CASE CMM_DEFLECTOR					
			IF IS_VEHICLE_MODEL(g_ModVehicle, FAGALOA)
				iMenuIndex = 38 
			ELIF DOES_VEHICLE_HAVE_NAME(g_ModVehicle)
				iMenuIndex = 60
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DYNASTY)
				iMenuIndex = 75 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, KANJO)
				iMenuIndex = 22
			ELIF vehModel = FURIA
				iMenuIndex = 47	
			ELSE
				iMenuIndex = 32
			ENDIF
		BREAK
		CASE CMM_ENGINE						
			
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
				iMenuIndex = 4 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, AVENGER)
				iMenuIndex = 43
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS)
			OR IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS2)
			OR IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS3)
				iMenuIndex = 34
			ELIF vehModel = BRUISER
			OR vehModel = BRUISER3
				iMenuIndex = 34
			#IF FEATURE_TUNER
			ELIF vehModel = CYPHER
				iMenuIndex = 17
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = KANJOSJ
			OR vehModel = POSTLUDE
				iMenuIndex = 17
			#ENDIF
			ELSE 
				iMenuIndex = 33
			ENDIF 
		BREAK
		
		CASE CMM_ENGINE_GROUP				
			IF IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS)
			OR IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS2)
			OR IS_VEHICLE_MODEL(g_ModVehicle, BRUTUS3)
				iMenuIndex = 29
			ELIF vehModel = BRUISER
			OR vehModel = BRUISER3
				iMenuIndex = 91	
			ELIF IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()	
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 4
			ELSE
				iMenuIndex = 34
			ENDIF
			BREAK
		
		CASE CMM_ENGINEBAY					iMenuIndex = 35 BREAK
		CASE CMM_EXHAUST					
			IF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			OR IS_VEHICLE_MODEL(g_ModVehicle, THRUSTER)
				IF vehModel != MICROLIGHT
					iMenuIndex = 77
				ELSE
					iMenuIndex = 69
				ENDIF	
			ELSE
				iMenuIndex = 36 
			ENDIF
		BREAK	
		CASE CMM_EXPLOSIVES					iMenuIndex = 37 BREAK
		CASE CMM_FAIRING					
			IF vehModel = CHEETAH2
			OR DOES_VEHICLE_MINE_MOD_CONFLICT_WITH_FENDERS(g_ModVehicle)
				iMenuIndex = 91
			ELIF  vehModel = COMET4
			OR vehModel = CLUB
			OR vehModel = MANANA2
				iMenuIndex = 40
			ELIF vehModel = SC1
			OR vehModel = FLASHGT
				iMenuIndex = 29 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, FAGALOA)
				iMenuIndex = 32
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, TOROS)
			OR vehModel = ZORRUSSO
			OR vehModel = LOCUST
				iMenuIndex = 29 
			ELIF vehModel = ASBO
			OR vehModel = GAUNTLET5
			OR vehModel = WINKY
			#IF FEATURE_DLC_1_2022
			OR vehModel = BRIOSO3
			#ENDIF
				iMenuIndex = 49	
			ELIF vehModel = ZHABA
				iMenuIndex = 73	
			ELIF vehModel = CALICO
				iMenuIndex = 40	
			#IF FEATURE_TUNER
			ELIF vehModel = COMET6
			OR vehModel = WARRENER2
			OR vehModel = REMUS
				iMenuIndex = 48 
			ELIF vehModel = VECTRE
				iMenuIndex = 39 
			ELIF vehModel = DOMINATOR8
				iMenuIndex = 49	
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = TENF2
				iMenuIndex = 47
			ELIF vehModel = POSTLUDE
			OR vehModel = PICADOR
			OR vehModel = SENTINEL4
				iMenuIndex = 39
			#ENDIF
			ELSE
				iMenuIndex = 38
			ENDIF	
		BREAK
		CASE CMM_FENDERS					
			IF vehModel = GP1
			#IF FEATURE_COPS_N_CROOKS
			OR IS_VEHICLE_MODEL(g_ModVehicle, POLGAUNTLET)
			#ENDIF	
				iMenuIndex = 47 
			ELIF IS_VEHICLE_MINE_MENU_NEED_REORDERING()
				iMenuIndex = 64
			ELIF vehModel = GB200
				iMenuIndex = 60 	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, STRIKEFORCE)
				iMenuIndex = 26
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DYNASTY)	
				iMenuIndex = 30 
			ELIF vehModel = GAUNTLET4
				iMenuIndex = 50	
			ELIF vehModel = S80
				iMenuIndex = 24 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, ZHABA)
				iMenuIndex = 75 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, JB7002)
				iMenuIndex = 70 	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, CLUB)
				iMenuIndex = 26		
			#IF FEATURE_HEIST_ISLAND
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, VETO)
			OR IS_VEHICLE_MODEL(g_ModVehicle, VETO2)
				iMenuIndex = 76	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, SQUADDIE)	
				iMenuIndex = 31
			#ENDIF	
			ELIF vehModel = CALICO
			OR vehModel = VECTRE
			#IF FEATURE_DLC_1_2022
			OR vehModel = POSTLUDE
			OR vehModel = PICADOR
			OR vehModel = SENTINEL4
			#ENDIF
				iMenuIndex = 38	
			ELSE
				iMenuIndex = 39 
			ENDIF
		BREAK
		CASE CMM_FRAME						
			IF vehModel = COMET4
			OR vehModel = CLUB
			OR vehModel = MANANA2
				iMenuIndex = 38 
			ELIF vehModel = SEMINOLE2
				iMenuIndex = 29 
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = BRIOSO2
				iMenuIndex = 75	
			#ENDIF	
			ELIF vehModel = CALICO
				iMenuIndex = 18
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = WEEVIL2
				iMenuIndex = 22
			#ENDIF
			ELSE	
				iMenuIndex = 40 
			ENDIF
		BREAK
		CASE CMM_FRONTFORKS					iMenuIndex = 41 BREAK
		CASE CMM_FRONTMUDGUARD				
			IF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			AND NOT IS_VEHICLE_MODEL(g_ModVehicle, STRIKEFORCE)
				iMenuIndex = 26
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, STRIKEFORCE)
				iMenuIndex = 28
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = VETO
			OR vehModel = VETO2
				iMenuIndex = 29	
			#ENDIF
			ELSE	
				iMenuIndex = 42 
			ENDIF
		BREAK
		CASE CMM_FRONTSEAT					
			IF IS_VEHICLE_MODEL(g_ModVehicle, AVENGER)
				iMenuIndex = 33
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DELUXO)
				iMenuIndex = 29
			ELIF vehModel = FREECRAWLER
				iMenuIndex = 29
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = VETO
			OR vehModel = VETO2
				iMenuIndex = 30	
			#ENDIF	
			ELSE	
				iMenuIndex = 43 
			ENDIF
		BREAK
		CASE CMM_FUELTANK					iMenuIndex = 44 BREAK
		CASE CMM_GOLD_PREP					iMenuIndex = 45 BREAK
		CASE CMM_GOLD_PREP2					iMenuIndex = 46 BREAK
		CASE CMM_GRILL
			IF vehModel = GP1
			#IF FEATURE_COPS_N_CROOKS
			OR IS_VEHICLE_MODEL(g_ModVehicle, POLGAUNTLET)
			#ENDIF
				iMenuIndex = 39 
			ELIF vehModel = INFERNUS2
			OR vehModel = RUSTON
			OR vehModel = PENUMBRA2
			#IF FEATURE_DLC_1_2022
			OR vehModel = PICADOR
			#ENDIF
				iMenuIndex = 74
			ELIF vehModel = OPPRESSOR2 
				iMenuIndex = 28
			ELIF vehModel = ZR3802
				iMenuIndex = 66
			ELIF vehModel = LOCUST
			OR vehModel = COMET7
			#IF FEATURE_DLC_1_2022
			OR vehModel = KANJOSJ
			#ENDIF
				iMenuIndex = 74	
			ELIF vehModel = FORMULA
				iMenuIndex = 91
			ELIF vehModel = FURIA
				iMenuIndex = 32	
			ELIF vehModel = OPENWHEEL2
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = ITALIRSX
			#ENDIF
				iMenuIndex = 64
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = VETO
			OR vehModel = VETO2
				iMenuIndex = 59	
			#ENDIF
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = WEEVIL
				iMenuIndex = 50
			#ENDIF
			ELIF vehModel = JESTER4
				iMenuIndex = 84	
			ELIF vehModel = COMET6
				iMenuIndex = 18		
			ELIF vehModel = GROWLER
				iMenuIndex = 67
			#IF FEATURE_FIXER
			ELIF vehModel = GRANGER2
				iMenuIndex = 60
			#ENDIF
			ELIF vehModel = REEVER
				iMenuIndex = 91	
			ELIF vehModel = BUFFALO4
				iMenuIndex = 52
			ELIF vehModel = CHAMPION
				iMenuIndex = 31	
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = IGNUS2
				iMenuIndex = 30		
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = SM722
				iMenuIndex = 69
			ELIF vehModel = TORERO2
				iMenuIndex = 31
			ELIF vehModel = TENF2
				iMenuIndex = 38
			#ENDIF
			ELSE
				iMenuIndex = 47
			ENDIF
		BREAK
		CASE CMM_HANDLEBARS					
			IF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			OR IS_VEHICLE_MODEL(g_ModVehicle, AVENGER)
				iMenuIndex = 27 
			#IF FEATURE_COPS_N_CROOKS
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, SURFER3)
				iMenuIndex = 53
			#ENDIF
			#IF FEATURE_TUNER
			ELIF vehModel = COMET6
			OR vehModel = WARRENER2
			OR vehModel = REMUS
				iMenuIndex = 38 
			#ENDIF	
			ELSE
				iMenuIndex = 48
			ENDIF
		BREAK
		CASE CMM_HEADLIGHTS					
			IF vehModel = ASBO
			OR vehModel = GAUNTLET5
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = WINKY
			#ENDIF
				iMenuIndex = 38	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, VSTR)
				iMenuIndex = 91 	
			ELIF vehModel = DOMINATOR8
			#IF FEATURE_DLC_1_2022
			OR vehModel = BRIOSO3
			#ENDIF
				iMenuIndex = 38
			ELSE	
				iMenuIndex = 49
			ENDIF
		BREAK
		CASE CMM_HOOD						
			IF !IS_VEHICLE_IE_SUPERMOD_MODEL()
				IF vehModel = OPPRESSOR
				OR vehModel = OPPRESSOR2
				OR vehModel = COQUETTE4
					iMenuIndex = 70
				ELIF vehModel = GAUNTLET4
					iMenuIndex = 51	
				ELIF IS_VEHICLE_MODEL(g_ModVehicle, VSTR)
					iMenuIndex = 49 	
				ELIF vehModel = GAUNTLET5
				OR vehModel = SEMINOLE2
				OR vehModel = ZR350
				OR vehModel = JESTER4
				OR vehModel = COMET6
				OR vehModel = WARRENER2
				OR vehModel = REMUS
				OR vehModel = VECTRE
				OR vehModel = CYPHER
				OR vehModel = PREVION
				OR vehModel = DOMINATOR8
				OR vehModel = EUROS
				OR vehModel = GROWLER
				OR vehModel = DOMINATOR7
				OR vehModel = TAILGATER2
				OR vehModel = CALICO
				OR vehModel = SULTAN3
				#IF FEATURE_DLC_1_2022
				OR vehModel = KANJOSJ
				OR vehModel = WEEVIL2
				OR vehModel = TENF2
				OR vehModel = SENTINEL4
				#ENDIF
					iMenuIndex = 97
				#IF FEATURE_COPS_N_CROOKS
				ELIF IS_VEHICLE_MODEL(g_ModVehicle, POLPANTO)
					iMenuIndex = 69 
				#ENDIF	
				#IF FEATURE_HEIST_ISLAND
				ELIF IS_VEHICLE_MODEL(g_ModVehicle, WEEVIL)
					iMenuIndex = 47
				#ENDIF
				#IF FEATURE_DLC_1_2022
				ELIF vehModel = GREENWOOD
					iMenuIndex = 24
				ELIF vehModel = TORERO2
					iMenuIndex = 30
				#ENDIF
				ELSE
					iMenuIndex = 50
				ENDIF
			ELSE	
				IF IS_THIS_PERSONAL_CAR_MOD_IN_OFFICE(g_eModShopName,g_iPersonalCarModVar)
				OR g_eModShopName = CARMOD_SHOP_SUPERMOD
					iMenuIndex = 97
				ELSE
					iMenuIndex = 50
				ENDIF
			ENDIF
		BREAK	
		
		
		CASE CMM_HORN						
			IF IS_VEHICLE_MODEL(g_ModVehicle, NEO)
			OR vehModel = GAUNTLET4
			OR vehModel = GAUNTLET5
				iMenuIndex = 52
			ELSE	
				iMenuIndex = 51 
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_HYDRO				
			IF IS_VEHICLE_MODEL(g_ModVehicle, BRUISER2)
				iMenuIndex = 29
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, NEO)
			OR vehModel = GAUNTLET5
				iMenuIndex = 91		
			ELIF vehModel = GAUNTLET4
				iMenuIndex = 39	
			ELIF vehModel = BUFFALO4
				iMenuIndex = 47
			#IF FEATURE_COPS_N_CROOKS
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, SURFER3)
				iMenuIndex = 74 	
			#ENDIF	
			ELIF vehModel = TAILGATER2
			OR vehModel = DOMINATOR8
				iMenuIndex = 21	
			ELSE	
				iMenuIndex = 52 
			ENDIF	
		BREAK
		CASE CMM_INTERIOR_GROUP				
			#IF FEATURE_COPS_N_CROOKS
			IF IS_VEHICLE_MODEL(g_ModVehicle, SURFER3)
				iMenuIndex = 48
			ELSE
			#ENDIF
				IF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehModel)
					iMenuIndex = 20 
				ELSE	
					iMenuIndex = 53
				ENDIF	
			#IF FEATURE_COPS_N_CROOKS
			ENDIF
			#ENDIF
		BREAK
		CASE CMM_LIGHTS						
			IF IS_VEHICLE_MODEL(g_ModVehicle, BRUISER2)
				iMenuIndex = 52
			ELSE	
				iMenuIndex = 54 
			ENDIF
		BREAK
		CASE CMM_LIGHTS_HEAD				
			IF vehModel = GROWLER
				iMenuIndex = 2
			ELSE
				iMenuIndex = 55
			ENDIF	
		BREAK
		CASE CMM_LIGHTS_NEON				iMenuIndex = 56 BREAK
		
		CASE CMM_SUPERMOD_LIVERY			iMenuIndex = 57 BREAK
		
		CASE CMM_INSURANCE					 // Now called LOSS/THEFT PREVENTION
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 1
			ELSE 
				iMenuIndex = 58
			ENDIF 
		BREAK	
		CASE CMM_MIRRORS					
			IF vehModel = TOROS
			OR vehModel = EVERON
			OR vehModel = MANANA2
				iMenuIndex = 91
			ELIF vehModel = NEBULA
				iMenuIndex = 65
			ELIF vehModel = DUKES3
			OR vehModel = YOUGA3
				iMenuIndex = 60
			#IF FEATURE_HEIST_ISLAND
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, VETO)
			OR IS_VEHICLE_MODEL(g_ModVehicle, VETO2)
				iMenuIndex = 47	
			#ENDIF	
			#IF FEATURE_TUNER
			ELIF vehModel = ZR350
			OR vehModel = JESTER4
			OR vehModel = DOMINATOR7
			OR vehModel = BUFFALO4
				iMenuIndex = 60
			#ENDIF	
			#IF FEATURE_TUNER
			ELIF vehModel = REMUS
				iMenuIndex = 21	
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = VIGERO2
			OR vehModel = GREENWOOD
				iMenuIndex = 60
			#ENDIF
			ELSE
				iMenuIndex = 59
			ENDIF
		BREAK
		CASE CMM_ORNAMENTS
			IF vehModel = SC1
			OR vehModel = DUKES3
			OR vehModel = YOUGA3
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = TOREADOR
			#ENDIF
			#IF FEATURE_TUNER
			OR vehModel = ZR350
			OR vehModel = JESTER4
			OR vehModel = DOMINATOR8
			#ENDIF
			OR vehModel = BUFFALO4
				iMenuIndex = 65
			ELIF vehModel = DOMINATOR7	
				iMenuIndex = 20
			ELIF vehModel = GB200
				iMenuIndex = 39 
			ELIF vehModel = MICHELLI
				iMenuIndex = 73 
			ELIF vehModel = DOMINATOR3
			OR vehModel = FREECRAWLER
				iMenuIndex = 91 	
			ELIF DOES_VEHICLE_HAVE_NAME(g_ModVehicle)
				iMenuIndex = 32 	
			ELIF vehModel = NEBULA
				iMenuIndex = 73	
			ELIF vehModel = KOMODA
			OR vehModel = SEMINOLE2
			OR vehModel = CINQUEMILA
			#IF FEATURE_DLC_1_2022
			OR vehModel = SENTINEL
			OR vehModel = LM87
			OR vehModel = RUINER4
			#ENDIF
				iMenuIndex = 91 
			ELIF vehModel = OPENWHEEL2
				iMenuIndex = 30	
			ELIF vehModel = WARRENER2
			OR vehModel = CALICO
				iMenuIndex = 21
			ELIF vehModel = TAILGATER2	
				iMenuIndex = 18
			#IF FEATURE_FIXER
			ELIF vehModel = GRANGER2
				iMenuIndex = 47
			#ENDIF
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = S95
				iMenuIndex = 30	
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = GREENWOOD
				iMenuIndex = 65
			#ENDIF
			ELSE	
				iMenuIndex = 60
			ENDIF
		BREAK
		CASE CMM_PLATES						
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
				iMenuIndex = 2 
			ELIF IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 92
			ELIF vehModel = DOMINATOR7
			#IF FEATURE_DLC_1_2022
			OR vehModel = CORSITA
			#ENDIF
				iMenuIndex = 65		
			ELSE 
				iMenuIndex = 61
			ENDIF 
		BREAK	
		CASE CMM_PLATEHOLDER				
			IF IS_IMANI_TECH_ALLOWED_FOR_THIS_VEHICLE(vehModel)
				iMenuIndex = 21
			ELSE
				iMenuIndex = 62
			ENDIF	
		BREAK
		CASE CMM_PLATE_GROUP				iMenuIndex = 63 BREAK
		
		CASE CMM_PUSHBAR					
			IF IS_VEHICLE_MINE_MENU_NEED_REORDERING()
				iMenuIndex = 39 
			ELIF vehModel = OPENWHEEL2
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = ITALIRSX
			#ENDIF
				iMenuIndex = 47
			ELSE
				iMenuIndex = 64
			ENDIF
		BREAK
		CASE CMM_REARMUDGUARD			
			IF vehModel = SC1
			OR vehModel = DOMINATOR8
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = TOREADOR
			#ENDIF
				iMenuIndex = 60
			ELIF vehModel = DOMINATOR4
				iMenuIndex = 6
			ELIF vehModel = NEBULA
			OR vehModel = DUKES3
			OR vehModel = YOUGA3
			#IF FEATURE_TUNER
			OR vehModel = ZR350
			OR vehModel = JESTER4
			OR vehModel = DOMINATOR7
			#ENDIF
			OR vehModel = BUFFALO4
				iMenuIndex = 59
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = POLGAUNTLET
				iMenuIndex = 66
			#ENDIF	
			ELIF vehModel = SEMINOLE2
				iMenuIndex = 20	
			ELIF vehModel = FUTO2
				iMenuIndex = 72
			#IF FEATURE_FIXER
			ELIF vehModel = ASTRON
				iMenuIndex = 76
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = VIGERO2
			OR vehModel = GREENWOOD
				iMenuIndex = 59
			ELIF vehModel = TENF
			OR vehModel = KANJOSJ 
			OR vehModel = TENF2 
				iMenuIndex = 72
			ELIF vehModel = CORSITA
				iMenuIndex = 61
			ELIF vehModel = PICADOR
				iMenuIndex = 30
			#ENDIF
			ELSE	
				iMenuIndex = 65 
			ENDIF
		BREAK
		CASE CMM_REARSEAT					
			IF vehModel = GP1
				iMenuIndex = 30
			ELIF vehModel = ZR3802
				iMenuIndex = 47
			ELIF vehModel = GAUNTLET4
			OR vehModel = GAUNTLET3
				iMenuIndex = 91	
			ELIF IS_MODEL_POLICE_VEHICLE(vehModel #IF FEATURE_COPS_N_CROOKS, TRUE #ENDIF )
				iMenuIndex = 31
			ELIF vehModel = GROWLER
				iMenuIndex = 47
			ELSE
				iMenuIndex = 66
			ENDIF
		 BREAK
		CASE CMM_PAINT_JOB					 // Now called RESPRAY
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 3 
			ELIF vehModel = GP1
			#IF FEATURE_COPS_N_CROOKS
			OR vehModel = POLCARACARA
			OR vehModel = POLRIATA
			#ENDIF
				iMenuIndex = 68 
			ELIF vehModel = GROWLER
				iMenuIndex = 66	
			ELSE 
				iMenuIndex = 67
			ENDIF 
		BREAK
		CASE CMM_ROLLCAGE 
			IF vehModel = FLASHGT
				iMenuIndex = 30 
			ELIF vehModel = PATRIOT2
				iMenuIndex = 91 
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = POLCARACARA
				iMenuIndex = 75
			ELIF vehModel = POLRIATA
				iMenuIndex = 29 
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = SM722
				iMenuIndex = 30
			ELIF vehModel = POSTLUDE
				iMenuIndex = 10
			#ENDIF
			ELSE
				iMenuIndex = 68 
			ENDIF
		BREAK
		CASE CMM_ROOF						
			IF IS_VEHICLE_IE_SUPERMOD_MODEL()
				IF IS_THIS_PERSONAL_CAR_MOD_IN_OFFICE(g_eModShopName,g_iPersonalCarModVar)
				OR g_eModShopName = CARMOD_SHOP_SUPERMOD
					iMenuIndex = 98
				ELSE
					iMenuIndex = 69
				ENDIF
			ELIF (IS_VEHICLE_HAVE_WEAPON_MENU()
			AND vehModel != TORERO)
			OR vehModel = HAVOK
			OR vehModel = MICROLIGHT
			OR vehModel = PYRO
			OR vehModel = VIGILANTE
				iMenuIndex = 84
			ELIF vehModel = FREECRAWLER
				iMenuIndex = 73
			ELIF vehModel = TIGON
			OR vehModel = CHAMPION
				iMenuIndex = 76
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = POLPANTO
				iMenuIndex = 50 
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = S95
			#ENDIF
			#ENDIF	
			ELIF vehModel = JESTER4
			OR vehModel = COMET6
			OR vehModel = REMUS
			OR vehModel = VECTRE
			OR vehModel = CYPHER
			OR vehModel = TAILGATER2
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = ASTRON2
			#ENDIF
			#IF FEATURE_DLC_1_2022
			OR vehModel = TENF
			OR vehModel = KANJOSJ
			OR vehModel = TENF2
			OR vehModel = RUINER4
			OR vehModel = TURISMOR
			OR vehModel = SCHWARZER
			#ENDIF
				iMenuIndex = 98
			ELIF vehModel = WARRENER2
				iMenuIndex = 20	
			ELIF vehModel = GRANGER2
				iMenuIndex = 70
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = SM722
				iMenuIndex = 47
			ELIF vehModel = DRAUGUR
				iMenuIndex = 74
			#ENDIF
			ELSE	
				iMenuIndex = 69
			ENDIF
		BREAK
		CASE CMM_SADDLEBAGS					
			IF vehModel = TORERO
				iMenuIndex = 91
			ELIF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			OR vehModel = KHANJALI
				iMenuIndex = 30 
			ELIF vehModel = COMET4
			OR vehModel = POUNDER2
			OR vehModel = VAGRANT
			OR vehModel = SENTINEL
				iMenuIndex = 91 
			ELIF vehModel = OPPRESSOR2
			OR vehModel = COQUETTE4
				iMenuIndex = 50
			ELIF vehModel = ISSI5
				iMenuIndex = 6	
			ELIF vehModel = DRAFTER	
			OR vehModel = ZHABA
			OR vehModel = SULTAN2
			OR vehModel = ASTRON
				iMenuIndex = 30
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = DRAUGUR
			#ENDIF
				iMenuIndex = 73 
			ELIF vehModel = JB7002
				iMenuIndex = 39 
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = SURFER3
				iMenuIndex = 30
			#ENDIF
			ELIF vehModel = YOUGA3
				iMenuIndex = 20
			ELIF vehModel = YOSEMITE3
				iMenuIndex = 23 
			ELIF vehModel = SULTAN3
				iMenuIndex = 20 	
			ELIF vehModel = GRANGER2
				iMenuIndex = 75		
			ELSE
				iMenuIndex = 70 
			ENDIF		
		BREAK
		CASE CMM_SELL						
			IF vehModel = YOSEMITE3
				iMenuIndex = 70
			ELSE	
				iMenuIndex = 71 
			ENDIF
		BREAK
		CASE CMM_SIDESTEP					
			IF vehModel = MENACER
				iMenuIndex = 30 
			ELIF vehModel = FUTO2
				iMenuIndex = 65 	
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = S95
				iMenuIndex = 91 	
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = TENF
			OR vehModel = KANJOSJ
			OR vehModel = TENF2
				iMenuIndex = 65
			ELIF vehModel = DRAUGUR
				iMenuIndex = 19
			#ENDIF
			ELSE
				iMenuIndex = 72
			ENDIF
		BREAK					
		CASE CMM_SKIRTS						
			IF IS_THIS_MODEL_A_PLANE(vehModel)
				iMenuIndex = 32 
			ELIF vehModel = Z190
				iMenuIndex = 24 
			ELIF vehModel = MICHELLI
				iMenuIndex = 60 
			ELIF vehModel = FREECRAWLER
				iMenuIndex = 69
			ELIF vehModel = NEBULA
				iMenuIndex = 60	
			ELIF vehModel = ZHABA
				iMenuIndex = 38	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, CLUB)
				iMenuIndex = 39
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = DRAUGUR
				iMenuIndex = 30
			#ENDIF
			ELSE
				iMenuIndex = 73 
			ENDIF
		BREAK
		CASE CMM_SPAREWHEEL					
			IF vehModel = INFERNUS2
			OR vehModel = RUSTON
				iMenuIndex = 47
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, SCHLAGEN)
				iMenuIndex = 91 
			ELIF vehModel = LOCUST
			OR vehModel = PENUMBRA2
			OR vehModel = COMET7
				iMenuIndex = 47	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, ZHABA)
				iMenuIndex = 39 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, GAUNTLET5)
			OR IS_VEHICLE_MODEL(g_ModVehicle, YOUGA3)
				iMenuIndex = 15
			#IF FEATURE_COPS_N_CROOKS
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, SURFER3)
				iMenuIndex = 52 
			#ENDIF
			#IF FEATURE_TUNER
			ELIF vehModel = COMET6
			OR vehModel = JESTER4
			OR vehModel = VECTRE
			OR vehModel = WARRENER2
			OR vehModel = RT3000
			OR vehModel = CYPHER
			OR vehModel = DOMINATOR8
			OR vehModel = EUROS
			OR vehModel = ZR350
			OR vehModel = PREVION
			OR vehModel = DOMINATOR7
			OR vehModel = TAILGATER2
			OR vehModel = GROWLER
			#IF FEATURE_DLC_1_2022
			OR vehModel = KANJOSJ
			OR vehModel = PICADOR
			#ENDIF
				iMenuIndex = 76
			ELIF vehModel = FUTO2
			OR vehModel = ASTRON
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = ASTRON2
			#ENDIF
				iMenuIndex = 75 
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = RHINEHART
			OR vehModel = SENTINEL
			OR vehModel = TENF2
			OR vehModel = ZENTORNO
				iMenuIndex = 75
			ELIF vehModel = CORSITA
				iMenuIndex = 77
			ELIF vehModel = KANJOSJ
				iMenuIndex = 47
			ELIF vehModel = LM87
				iMenuIndex = 3
			ELIF vehModel = DRAUGUR
				iMenuIndex = 69
			#ENDIF
			ELSE
				iMenuIndex = 74 
			ENDIF
		BREAK	
		CASE CMM_SPOILER					
			IF IS_VEHICLE_MODEL(g_ModVehicle, FAGALOA)
				iMenuIndex = 84 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, SCHLAGEN)
			#IF FEATURE_COPS_N_CROOKS
			OR IS_VEHICLE_MODEL(g_ModVehicle, SURFER3)
			#ENDIF
			OR vehModel = DOMINATOR8
			OR vehModel = FUTO2
			OR vehModel = ASTRON
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = ASTRON2
			#ENDIF
				iMenuIndex = 74 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, TOROS)
				iMenuIndex = 38 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, HELLION)
				iMenuIndex = 89 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DYNASTY)
				iMenuIndex = 32 	
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, ZHABA)
				iMenuIndex = 74 		
			#IF FEATURE_COPS_N_CROOKS
			ELIF vehModel = POLCARACARA
				iMenuIndex = 67
			#ENDIF
			#IF FEATURE_HEIST_ISLAND
			ELIF vehModel = BRIOSO2
				iMenuIndex = 40	
			#ENDIF	
			ELIF vehModel = GLENDALE2
			OR vehModel = YOSEMITE3
				iMenuIndex = 82 
			ELIF vehModel = PATRIOT3
				iMenuIndex = 81 
			ELIF vehModel = GRANGER2
				iMenuIndex = 69		
			ELIF vehModel = REEVER
				iMenuIndex = 26
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = RHINEHART
			OR vehModel = SENTINEL
			OR vehModel = TENF2
			OR vehModel = ZENTORNO
				iMenuIndex = 74
			ELIF vehModel = CORSITA
			OR vehModel = POSTLUDE
				iMenuIndex = 76
			#ENDIF
			ELSE
				iMenuIndex = 75 
			ENDIF
		BREAK
		CASE CMM_SUSPENSION				
			IF vehModel = ISSI7
			OR vehModel = IMORGON
			OR vehModel = PARAGON
			OR vehModel = PARAGON2
			OR vehModel = KANJO
			OR vehModel = CLUB
			OR vehModel = PENUMBRA2
			OR vehModel = COQUETTE4
			OR vehModel = TIGON
			OR vehModel = YOSEMITE3
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = BRIOSO2
			#ENDIF
			OR vehModel = ZR350
			OR vehModel = DOMINATOR7
			OR vehModel = WARRENER2
			OR vehModel = COMET6
			OR vehModel = VECTRE
			OR vehModel = REMUS
			OR vehModel = JESTER4
			OR vehModel = RT3000
			OR vehModel = CYPHER
			OR vehModel = PREVION
			OR vehModel = DOMINATOR8
			OR vehModel = EUROS
			OR vehModel = DOMINATOR7
			OR vehModel = TAILGATER2
			OR vehModel = GROWLER
			OR vehModel =  BALLER7
			OR vehModel =  CINQUEMILA
			OR vehModel =  CHAMPION
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = CYCLONE2
			OR vehModel = S95
			OR vehModel = ARBITERGT
			#ENDIF
				iMenuIndex = 77
			ELIF vehModel = VETO
			OR vehModel = VETO2
				iMenuIndex = 39
			ELIF vehModel = ASTRON
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = ASTRON2
			#ENDIF
			#IF FEATURE_DLC_1_2022
			OR vehModel = VIGERO2
			OR vehModel = KANJOSJ
			OR vehModel = POSTLUDE
			OR vehModel = PICADOR
			#ENDIF
				iMenuIndex = 77
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = CORSITA
			#ENDIF
				iMenuIndex = 78
			ELSE
				iMenuIndex = 76
			ENDIF
		BREAK
		CASE CMM_TAILGATE					
			IF IS_THIS_PERSONAL_CAR_MOD_IN_HANGAR(g_eModShopName,g_iPersonalCarModVar)
			OR vehModel = THRUSTER
				IF vehModel != MICROLIGHT
					iMenuIndex = 36
				ENDIF	
			ELIF vehModel = ISSI7
			OR vehModel = IMORGON
			OR vehModel = YOSEMITE2
			OR vehModel = PARAGON
			OR vehModel = PARAGON2
			OR vehModel = KANJO
			OR vehModel = PENUMBRA2
			OR vehModel = COQUETTE4
			OR vehModel = BALLER7
				iMenuIndex = 91	
			ELIF vehModel =  REBLA
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = ITALIRSX
			#ENDIF
				iMenuIndex = 30
			ELIF vehModel =  CLUB
			OR vehModel =  CINQUEMILA
				iMenuIndex = 84
			ELIF vehModel =  TIGON
			OR vehModel =  CHAMPION
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = S95
			#ENDIF
				iMenuIndex = 69
			ELIF vehModel = YOSEMITE3
				iMenuIndex = 9 
			ELIF vehModel = ZR350
			OR vehModel = DOMINATOR7
			OR vehModel = PREVION
				iMenuIndex = 21 
			ELIF vehModel = WARRENER2
			OR vehModel = VECTRE
			OR vehModel = JESTER4
			OR vehModel = RT3000
			OR vehModel = DOMINATOR8
			OR vehModel = EUROS
			OR vehModel = GROWLER
				iMenuIndex = 3
			ELIF vehModel = COMET6	
				iMenuIndex = 21
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = CYCLONE2
			OR vehModel = ARBITERGT
				iMenuIndex = 30
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = KANJOSJ
			OR vehModel = POSTLUDE
			OR vehModel = PICADOR
				iMenuIndex = 76
			#ENDIF
			ELSE
				iMenuIndex = 77 
			ENDIF
		BREAK
		CASE CMM_TRACKER					
			IF vehModel = ZR350
			OR vehModel = GROWLER
				iMenuIndex = 22	
			ELSE
				iMenuIndex = 78
			ENDIF
		BREAK
		CASE CMM_TRANSMISSION 				iMenuIndex = 79 BREAK
		CASE CMM_TRUCKBED					iMenuIndex = 80 BREAK
		CASE CMM_TRUNK						
			IF vehModel = TAMPA3
				iMenuIndex = 83 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, THRAX)	
				iMenuIndex = 70
			ELIF vehModel = GLENDALE2
			OR vehModel = YOSEMITE3
				iMenuIndex = 75 
			ELIF vehModel = PATRIOT3
				iMenuIndex = 75 	
			ELSE
				iMenuIndex = 81 
			ENDIF
		BREAK	
		CASE CMM_SUPERMOD_TRUNK				
			IF vehModel = TAMPA3
				iMenuIndex = 95 
			ELIF DOES_VEHICLE_HAVE_JUMP_MOD()
				iMenuIndex = 17 
			ELIF vehModel = GLENDALE2
			OR vehModel = YOSEMITE3
				iMenuIndex = 81 			
			ELSE
				iMenuIndex = 82 
			ENDIF	
		BREAK
		CASE CMM_TURBO						
			IF vehModel = TAMPA3
				iMenuIndex = 81 
			ELIF DOES_VEHICLE_HAVE_JUMP_MOD()
				iMenuIndex = 82 
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF IS_VEHICLE_A_HSW_VEHICLE(g_ModVehicle)
				iMenuIndex = 12 
			#ENDIF
			ELSE
				iMenuIndex = 83 
			ENDIF	
		BREAK
		CASE CMM_VIN						
			IF vehModel = XA21
			OR vehModel = DYNASTY
			OR vehModel = YOUGA3
				iMenuIndex = 91 
			ELIF IS_VEHICLE_HAVE_WEAPON_MENU()
			OR vehModel = HAVOK
			OR vehModel = MICROLIGHT
			OR vehModel = PYRO
			OR vehModel = VIGILANTE
				iMenuIndex = 69
			ELIF vehModel = BARRAGE
			OR vehModel = POUNDER2
			OR vehModel = MULE4
			OR vehModel = SPEEDO4
				iMenuIndex = 95 	
			ELIF vehModel = FAGALOA
				iMenuIndex = 75 
			ELIF vehModel = VSTR
				iMenuIndex = 30 
			ELIF vehModel = CLUB
			OR vehModel = COQUETTE4
			OR vehModel = CINQUEMILA
				iMenuIndex = 76 
			ELIF vehModel = SLAMTRUCK	
				iMenuIndex = 91 
			#IF FEATURE_TUNER
			ELIF vehModel = CYPHER
				iMenuIndex = 3
			ELIF vehModel = JESTER4
				iMenuIndex = 47	
			ELIF vehModel = GROWLER
				iMenuIndex = 23
			#ENDIF
			#IF FEATURE_DLC_1_2022
			ELIF vehModel = TORERO2
				iMenuIndex = 23
			#ENDIF
			ELSE	
				iMenuIndex = 84
			ENDIF	
		BREAK
		CASE CMM_WHEELS_MAIN			
			IF IS_THIS_PERSONAL_CAR_MOD_FOR_IMPORT_EXPORT_MISSION(g_eModShopName,g_iPersonalCarModVar)
			OR IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 6
			ELSE 
				iMenuIndex = 85
			ENDIF 
		BREAK
		CASE CMM_WHEELS						iMenuIndex = 86 BREAK
		CASE CMM_WHEEL_COL					iMenuIndex = 87 BREAK
		CASE CMM_WHEEL_ACCS					iMenuIndex = 88 BREAK
		CASE CMM_WHEELIEBAR					
			IF IS_VEHICLE_MODEL(g_ModVehicle, HELLION)
				iMenuIndex = 75 
			ELIF IS_VEHICLE_MODEL(g_ModVehicle, DRAFTER)
				iMenuIndex = 91	
			ELIF vehModel = COMET6
				iMenuIndex = 22 	
			ELSE 
				iMenuIndex = 89 
			ENDIF
		BREAK
		CASE CMM_WINDOWS					iMenuIndex = 90 BREAK
		
		CASE CMM_FENDERSTWO					
			IF vehModel = GP1
				iMenuIndex = 67 
			ELIF vehModel = XA21
			OR vehModel = DYNASTY
			OR vehModel = YOUGA3
				iMenuIndex = 84 
			ELIF vehModel = CHEETAH2
			OR DOES_VEHICLE_MINE_MOD_CONFLICT_WITH_FENDERS(g_ModVehicle)
				iMenuIndex = 38
			ELIF vehModel = TORERO
			OR vehModel = COMET4
			OR vehModel = POUNDER2
			OR vehModel = VAGRANT
				iMenuIndex = 70	
			ELIF vehModel = DOMINATOR3
			OR vehModel = FREECRAWLER
			OR vehModel = SEMINOLE2
			OR vehModel = CINQUEMILA
			#IF FEATURE_DLC_1_2022
			OR vehModel = SENTINEL
			OR vehModel = LM87
			OR vehModel = RUINER4
			#ENDIF
				iMenuIndex = 60
			ELIF vehModel = PATRIOT2
				iMenuIndex = 68 
			ELIF vehModel = TOROS
			OR vehModel = EVERON
			OR vehModel = MANANA2
				iMenuIndex = 59 	
			ELIF vehModel = SCHLAGEN
			#IF FEATURE_COPS_N_CROOKS
			OR vehModel = SURFER3
			#ENDIF
				iMenuIndex = 75 
			ELIF vehModel = RCBANDITO
				iMenuIndex = 28	
			ELIF vehModel = BRUISER
			OR vehModel = BRUISER3
				iMenuIndex = 33	
			ELIF vehModel = YOSEMITE2
				iMenuIndex = 77
			ELIF vehModel = DRAFTER
				iMenuIndex = 89	
			ELIF vehModel = NEO
			OR vehModel = GAUNTLET5
				iMenuIndex = 51		
			ELIF vehModel = GAUNTLET4
			OR vehModel = GAUNTLET3
				iMenuIndex = 66	
			ELIF vehModel = PARAGON
			OR vehModel = PARAGON2
			OR vehModel =  KANJO
			OR vehModel =  PENUMBRA2
			OR vehModel =  ISSI7
			OR vehModel =  IMORGON
			#IF FEATURE_HEIST_ISLAND
			OR vehModel = BRIOSO2
			#ENDIF
			OR vehModel =  BALLER7
				iMenuIndex = 76		
			ELIF vehModel =  VSTR
				iMenuIndex = 50 	
			ELIF vehModel =  KOMODA
				iMenuIndex = 60 			
			ELIF vehModel = FORMULA
				iMenuIndex = 47	
			ELIF vehModel =  SLAMTRUCK
				iMenuIndex = 84
			ELIF vehModel = REEVER
				iMenuIndex = 47
			ELIF vehModel = DEITY
			#IF FEATURE_DLC_1_2022
			OR vehModel = GREENWOOD
			#ENDIF
				iMenuIndex = 27
			#IF FEATURE_GEN9_EXCLUSIVE
			ELIF vehModel = S95
				iMenuIndex = 72
			#ENDIF
			ELSE
				iMenuIndex = 91
			ENDIF
		BREAK
		CASE CMM_WAITING_ON_PLAYERS		
			IF vehModel = GAUNTLET5
				iMenuIndex = 3 
			ELIF IS_PLAYER_MODDING_TUNER_CLIENT_VEHICLE()
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_MODDING_BIKER_CLIENT_VEHICLE()
			#ENDIF
				iMenuIndex = 61
			ELSE
				iMenuIndex = 92
			ENDIF
		BREAK
		CASE CMM_PRIMARY_COLOUR			iMenuIndex = 93 BREAK
		CASE CMM_SECONDARY_COLOUR		iMenuIndex = 94 BREAK
		CASE CMM_BUMPER_GROUP 			
			IF DOES_VEHICLE_HAVE_BUMPER_ACCESSORY_MODS(g_ModVehicle)
				iMenuIndex = 29 
			ELIF vehModel = TAMPA3
				iMenuIndex = 82
			ELIF vehModel = BARRAGE
			OR IS_VEHICLE_MODEL(g_ModVehicle, POUNDER2)
			OR IS_VEHICLE_MODEL(g_ModVehicle, MULE4)
			OR IS_VEHICLE_MODEL(g_ModVehicle, SPEEDO4)
				iMenuIndex = 84	
			ELSE
				iMenuIndex = 95 
			ENDIF	
		BREAK
		CASE CMM_BUMPER_SUBMENU_GROUP
			iMenuIndex = 96	
		BREAK
		CASE CMM_HOOD_GROUP
			IF IS_VEHICLE_IE_SUPERMOD_MODEL()
			OR vehModel = GAUNTLET5
			OR vehModel = SEMINOLE2
			OR vehModel = ZR350
			OR vehModel = JESTER4
			OR vehModel = COMET6
			OR vehModel = WARRENER2
			OR vehModel = REMUS
			OR vehModel = VECTRE
			OR vehModel = CYPHER
			OR vehModel = PREVION
			OR vehModel = DOMINATOR8
			OR vehModel = EUROS
			OR vehModel = GROWLER
			OR vehModel = DOMINATOR7
			OR vehModel = TAILGATER2
			OR vehModel = CALICO
			OR vehModel = SULTAN3
			#IF FEATURE_DLC_1_2022
			OR vehModel = KANJOSJ
			OR vehModel = WEEVIL2
			OR vehModel = TENF2
			OR vehModel = SENTINEL4
			#ENDIF
				iMenuIndex = 50
			ELSE	
				iMenuIndex = 97
			ENDIF	
		BREAK
		CASE CMM_ROOF_GROUP
			IF IS_VEHICLE_IE_SUPERMOD_MODEL()
			OR vehModel = JESTER4
			OR vehModel = COMET6
			OR vehModel = REMUS
			OR vehModel = VECTRE
			OR vehModel = CYPHER
			OR vehModel = TAILGATER2
			#IF FEATURE_GEN9_EXCLUSIVE
			OR vehModel = ASTRON2
			#ENDIF
			#IF FEATURE_DLC_1_2022
			OR vehModel = TENF
			OR vehModel = KANJOSJ
			OR vehModel = TENF2
			OR vehModel = RUINER4
			OR vehModel = TURISMOR
			OR vehModel = SCHWARZER
			#ENDIF
				iMenuIndex = 69
			ELSE	
				iMenuIndex = 98
			ENDIF
		BREAK
		CASE CMM_SUPERMOD_TWO
			IF IS_BASE_VEHICLE_AVAILBLE_FOR_BENNYS_AND_ARENA_SUPERMOD(vehModel)
				iMenuIndex = 1
			ELSE	
				iMenuIndex = 99
			ENDIF	
		BREAK
	ENDSWITCH
	
	RETURN iMenuIndex
ENDFUNC

PROC BLOCK_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION(VEHICLE_INDEX vehToSet, CARMOD_MENU_ENUM eMenu, BOOL bBlock)
	INT iVehicleID = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehToSet)		
	IF iVehicleID = -1
		EXIT
	ENDIF
	
	g_ModVehicle  = vehToSet
	INT iMenuOptionX, iMenuOptionY
	
	INT iMenuOption 	= GET_CARMOD_MENU_DISPLAY_INDEX(eMenu)
	
	IF iMenuOption != -1 
		iMenuOptionX 		= iMenuOption % 32
		iMenuOptionY 		= iMenuOption / 32
		
		IF bBlock
			IF NOT IS_BIT_SET(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				SET_BIT(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				PRINTLN("BLOCK_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION iMenuOption: ", iMenuOption, " TRUE")
			ENDIF	
		ELSE
			IF IS_BIT_SET(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				CLEAR_BIT(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				PRINTLN("BLOCK_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION iMenuOption: ", iMenuOption, " FALSE")
			ENDIF	
		ENDIF
	ENDIF
	
ENDPROC	

PROC RESET_CARMOD_MENU_OPTION_FOR_CREATOR_MISSION()
	INT i
	FOR i = 0 TO 3
		g_iBGMenuOptionsBlock[i] = 0
	ENDFOR	
ENDPROC

PROC BLOCK_CARMOD_MENU_OPTION(CARMOD_MENU_ENUM eMenu, BOOL bBlock)
	INT iMenuOptionX, iMenuOptionY
	
	INT iMenuOption 	= GET_CARMOD_MENU_DISPLAY_INDEX(eMenu)
	
	IF iMenuOption != -1 
		iMenuOptionX 		= iMenuOption % 32
		iMenuOptionY 		= iMenuOption / 32
		
		IF bBlock
			IF NOT IS_BIT_SET(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				SET_BIT(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				PRINTLN("BLOCK_CARMOD_MENU_OPTION iMenuOption: ", iMenuOption, " TRUE")
			ENDIF	
		ELSE
			IF IS_BIT_SET(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				CLEAR_BIT(g_iBGMenuOptionsBlock[iMenuOptionY], iMenuOptionX)
				PRINTLN("BLOCK_CARMOD_MENU_OPTION iMenuOption: ", iMenuOption, " FALSE")
			ENDIF	
		ENDIF
	ENDIF
	
ENDPROC	

PROC RESET_CARMOD_MENU_OPTION()
	INT i
	FOR i = 0 TO 3
		g_iBGMenuOptionsBlock[i] = 0
	ENDFOR	
ENDPROC
