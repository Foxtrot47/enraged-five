/* ------------------------------------------------------------------
* Name: net_betting_server.sch
* Author: James Adwick
* Date: 17/08/2012
* Purpose: Server processing of betting throughout MP.
* This should modify global broadcast data of who is taking part in
* betting and clean them up if they leave.
* Also used to update odds and issue them to participants
* ------------------------------------------------------------------*/

USING "net_betting_common.sch"

// -------------------- SERVER ONLY ACCESORS ----------------------

/// PURPOSE: (SERVER) Set the betting ID of a player who has just become available
PROC SET_PLAYER_BETTING_ID(INT index, BETTING_ID bettingID)
	GlobalServerBD_Betting.playerBettingData[index].bettingID = bettingID
ENDPROC

/// PURPOSE: (SERVER) Set whether the betting is team or not
PROC SET_PLAYER_TEAM_BETTING_VAR(INT index, BOOL bTeamBetting)
	GlobalServerBD_Betting.playerBettingData[index].bTeamBetting = bTeamBetting
ENDPROC

/// PURPOSE: (SERVER) Set the team of the player participating in betting
PROC SET_PLAYER_BETTING_TEAM(INT index, INT iTeam)
	PRINTLN("[JA@BETTING](S) SET_PLAYER_BETTING_TEAM - index ", index, " iTeam = ", iTeam, " T(", GET_CLOUD_TIME_AS_INT(), ")")
	GlobalServerBD_Betting.playerBettingData[index].iTeam = iTeam
ENDPROC

/// PURPOSE: (SERVER) Set this betting index is in tutorial mode
PROC SET_BETTING_INDEX_ON_TUTORIAL(INT index)
	SET_BIT(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_POOL_TUTORIAL)
ENDPROC

/// PURPOSE: (SERVER) Set this betting index is in tutorial mode
FUNC BOOL IS_BETTING_INDEX_ON_TUTORIAL(INT index)
	RETURN IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_POOL_TUTORIAL)
ENDFUNC

/// PURPOSE: (SERVER) Returns the stat for active betting index
FUNC INT GET_ACTIVE_BETTING_INDEX_STAT(INT iIndex)
	RETURN GlobalServerBD_Betting.activeBettingData[iIndex].iStatSum
ENDFUNC

FUNC BOOL IS_BETTING_INDEX_LOCKED(INT iIndex)
	RETURN GlobalServerBD_Betting.activeBettingData[iIndex].bLocked
ENDFUNC

/// PURPOSE: (SERVER) Returns if the betting index is set up for team betting
FUNC BOOL IS_ACTIVE_BETTING_INDEX_TEAM(INT iIndex)
	RETURN GlobalServerBD_Betting.activeBettingData[iIndex].bTeamBetting
ENDFUNC

/// PURPOSE: (SERVER) Returns if the player is involved in team betting
FUNC BOOL IS_PLAYER_IN_TEAM_BETTING(INT iIndex)
	RETURN GlobalServerBD_Betting.playerBettingData[iIndex].bTeamBetting
ENDFUNC

/// PURPOSE: (SERVER) Allow the player to begin betting in their pool
PROC SET_PLAYER_BETTING_AS_ACTIVE(INT index, BOOL bIsActive, INT iPlayerInPool)
	IF bIsActive
		SET_BIT(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_PLAYER_POOL_ACTIVE)
	ELSE
		CLEAR_BIT(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_PLAYER_POOL_ACTIVE)
	ENDIF
	
	GlobalServerBD_Betting.playerBettingData[index].iNumberPlayersInPool = iPlayerInPool
ENDPROC

/// PURPOSE: (SERVER) Update sync ID for clients to pick up changes to players
PROC SET_PLAYER_BETTING_SYNC_ID(INT index, INT iSyncID)
	GlobalServerBD_Betting.playerBettingData[index].iSyncDataID = iSyncID
ENDPROC

/// PURPOSE: (SERVER) Set the amount of cash bet on player
PROC SET_PLAYER_AMOUNT_BET_ON(INT index, INT iBetOnPlayer)
	GlobalServerBD_Betting.activeBettingData[index].iCashBetOnPlayer = iBetOnPlayer
ENDPROC

/// PURPOSE: (SERVER) Returns the total 'virtual' cash we have bet on this index in total
FUNC INT GET_CUMULATIVE_CASH_ON_BETTING_INDEX(INT index)
	RETURN GlobalServerBD_Betting.activeBettingData[index].iCumulativeCash
ENDFUNC

/// PURPOSE: (SERVER) Sets the 'virtual' cash on the betting index
PROC SET_CUMULATIVE_CASH_ON_BETTING_INDEX(INT index, INT iCash)
	GlobalServerBD_Betting.activeBettingData[index].iCumulativeCash = iCash
ENDPROC

/// PURPOSE: (SERVER) Set the odds of the player involved in betting
PROC SET_PLAYER_BETTING_ODDS(INT index, INT iOddNum, INT iOddDenom)
	GlobalServerBD_Betting.activeBettingData[index].iOddNumerator = iOddNum
	GlobalServerBD_Betting.activeBettingData[index].iOddDenominator = iOddDenom
ENDPROC

PROC SET_PLAYER_BETTING_STAT_VALUE(INT index, INT iStatVal)

	PRINTLN("[JA@BETTING](S) SET_PLAYER_BETTING_STAT_VALUE - index ", index, " iStatVal = ", iStatVal)
	GlobalServerBD_Betting.playerBettingData[index].iStatSum = iStatVal
ENDPROC

FUNC BOOL HAS_PLAYER_BEEN_BET_ON(INT index)
	RETURN GlobalServerBD_Betting.activeBettingData[index].iCashBetOnPlayer > 0
ENDFUNC

/// PURPOSE: (SERVER) Cleans up a players betting slot in server BD.
PROC RESET_PLAYER_BETTING_SLOT(INT index)
	BETTING_ID blankBettingID

	SET_PLAYER_BETTING_ID(index, blankBettingID)
	GlobalServerBD_Betting.playerBettingData[index].iSyncDataID = -1
	GlobalServerBD_Betting.playerBettingData[index].iBetSyncID = -1
	GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState = 0
	GlobalServerBD_Betting.playerBettingData[index].bTeamBetting = FALSE
	//GlobalServerBD_Betting.playerBettingData[index].iTeam = 0					// Don't want to reset this as will be used globally to get winning team
		
	// TODO: ONLY IF THIS IS NOT A TEAM
//	GlobalServerBD_Betting.activeBettingData[index].iCashBetOnPlayer = 0
//	GlobalServerBD_Betting.activeBettingData[index].iOddNumerator = 0
//	GlobalServerBD_Betting.activeBettingData[index].iOddDenominator = 1
//	GlobalServerBD_Betting.activeBettingData[index].iActiveMembers = 0
//	GlobalServerBD_Betting.activeBettingData[index].bTeamBetting = FALSE
//	GlobalServerBD_Betting.activeBettingData[index].bLocked = FALSE
//	
//	#IF IS_DEBUG_BUILD
//		GlobalServerBD_Betting.activeBettingData[index].iMorningLineN = 0
//		GlobalServerBD_Betting.activeBettingData[index].iMorningLineD = 1
//	#ENDIF
	
ENDPROC


// --------------------- PROCESS EVENTS -----------------------

/// PURPOSE:
///    Find the greatest common divisor using Euclidean Algorithm
/// PARAMS:
///    iNumA - First int (larger than B) 
///    iNumB - Second int (smaller than A)
/// RETURNS:
///    The GCD of the two ints passed.
FUNC INT GCD(INT iNumA, INT iNumB)
	INT iTemp
	
	WHILE(iNumB != 0)
	
		iTemp = iNumB
		iNumB = iNumA % iNumB
		iNumA = iTemp
		
	ENDWHILE

	RETURN iNumA
ENDFUNC

// TODO: WE NEED TO LIMIT THE ODDS PERCENTAGE TO 72% otherwise we get short odds (BAD!!)
// TODO: MAY ALSO WANT TO LIMIT ODDS TO 2% otherwise we get long odds (ALSO BAD!!)

// Based on idea we want % (points) to equal 125 so house makes a profit
// 		ODDS = (100 / winPercent) - 1
//		Set this ODDS-to-1 and convert to a simplified fraction
PROC CALC_PLAYER_BETTING_ODDS(FLOAT iDecimalOdd, INT &iOddN, INT &iOddD, BOOL bFinalOdds = FALSE)

	iOddN = ROUND(iDecimalOdd * 10) * 10
	iOddD = 100

	INT iGCD
	
	IF iOddN > iOddD
		iGCD = GCD(iOddN, iOddD)
	ELSE
		iGCD = GCD(iOddD, iOddN)
	ENDIF

	// IF gcd = 1 and number is large we should try and reduce the odds to a more recognisable scale

	iOddN = (iOddN / iGCD)
	iOddD = (iOddD / iGCD)

	PRINTLN("[JA@BETTING](S)				GCD = ", iGCD)
	PRINTLN("[JA@BETTING](S)				iOddN = ", iOddN)
	PRINTLN("[JA@BETTING](S)				iOddD = ", iOddD)
	
	//If current odds are unusual, take decimal value and set to 1.
	IF iOddD >= 5
	AND iOddN >= 20
		IF NOT bFinalOdds
	
			PRINTLN("[JA@BETTING](S)		iOddN and iOddD are too large - reduce values")
			
			iOddN = ROUND(TO_FLOAT(iOddN) / TO_FLOAT(iOddD))
			iOddD = 1
			
			PRINTLN("[JA@BETTING](S)				iOddN = ", iOddN)
			PRINTLN("[JA@BETTING](S)				iOddD = ", iOddD)		
		ENDIF
	ENDIF
		
ENDPROC

PROC CALC_MORNING_LINE_ODDS(FLOAT fPlayerPercent, INT &iOddN, INT &iOddD)

	FLOAT iDecimalOdd = (100.0 / fPlayerPercent) - 1.0
	
	PRINTLN("[JA@BETTING](S) CALC_MORNING_LINE_ODDS - ")
	PRINTLN("[JA@BETTING](S)				fPlayerPercent = ", fPlayerPercent)
	PRINTLN("[JA@BETTING](S)				iDecimalOdd = ", iDecimalOdd)
	
	// Turn our decimal odd into bookies odd (3/1 etc)
	CALC_PLAYER_BETTING_ODDS(iDecimalOdd, iOddN, iOddD)
ENDPROC

/// PURPOSE: Get the odds percentage of our specific cap
PROC CALC_PLAYER_CORRECTED_ODD_PERCENTAGE(FLOAT &fCorrectedPercent, FLOAT fPercentage)
	
	fCorrectedPercent = (100.0 / fPercentage) - 1.0
	
	IF fCorrectedPercent < 0
		PRINTLN("[JA@BETTING](S) CALC_PLAYER_CORRECTED_ODD_PERCENTAGE - fCorrectedValue = ", fCorrectedPercent)
		SCRIPT_ASSERT("Betting: Calculated corrected betting percentage is less then 0. Check short / long odd tunables")
		fCorrectedPercent = 0.05
	ENDIF
	
	PRINTLN("[JA@BETTING](S) CALC_PLAYER_CORRECTED_ODD_PERCENTAGE - fCorrectedValue = ", fCorrectedPercent)
ENDPROC

/// PURPOSE: Calculate exact odds based on money bet on player. (Shortest odds we can get is 1/10 (0.1))
FUNC BOOL CALC_EXACT_ODDS(INT iBetOnBettingIndex, INT iTotalBet, INT &iOddN, INT &iOddD, BOOL bFinalOdds = FALSE)

	FLOAT iDecimalOdd = ((iTotalBet * (1.0-(g_sMPTunables.fRake*0.01))) - iBetOnBettingIndex) / iBetOnBettingIndex		// bet on horse or total bet?
	
	PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - ")
	PRINTLN("[JA@BETTING](S)				iDecimalOdd = ", iDecimalOdd)
	PRINTLN("[JA@BETTING](S)				iBetOnBettingIndex = ", iBetOnBettingIndex)
	PRINTLN("[JA@BETTING](S)				iTotalBet = ", iTotalBet)
	PRINTLN("[JA@BETTING](S)				fRake = ", (g_sMPTunables.fRake*0.01))

	IF iDecimalOdd < 0
		
		PRINTLN("		odds are negative (award shortest odds)")
	
		PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - Odds are negative: iDecimalOdd: ", iDecimalOdd, ", g_sMPTunables.fShortOddsLimit: ", g_sMPTunables.fShortOddsLimit)
		CALC_PLAYER_CORRECTED_ODD_PERCENTAGE(iDecimalOdd, g_sMPTunables.fShortOddsLimit)
	
		CALC_PLAYER_BETTING_ODDS(iDecimalOdd, iOddN, iOddD, bFinalOdds)
		PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - New short odds (non-negative): iOddN: ", iOddN, ", iOddD: ", iOddD)
	
		RETURN TRUE
	ENDIF

	// Turn our decimal odd into bookies odd (3/1 etc)
	CALC_PLAYER_BETTING_ODDS(iDecimalOdd, iOddN, iOddD, bFinalOdds)
	
	// Cap our exact odds to the
	FLOAT iPercentOfOdds = (100.0 / (ROUND(TO_FLOAT(iOddN) / TO_FLOAT(iOddD)) + 1.0))
	IF iPercentOfOdds > g_sMPTunables.fShortOddsLimit
	
		PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - Odds are too short: iPercentOfOdds: ", iPercentOfOdds, ", g_sMPTunables.fShortOddsLimit: ", g_sMPTunables.fShortOddsLimit)
		CALC_PLAYER_CORRECTED_ODD_PERCENTAGE(iPercentOfOdds, g_sMPTunables.fShortOddsLimit)
	
		CALC_PLAYER_BETTING_ODDS(iPercentOfOdds, iOddN, iOddD, bFinalOdds)
		PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - New short odds: iOddN: ", iOddN, ", iOddD: ", iOddD)
	
	ELIF iPercentOfOdds < g_sMPTunables.fLongOddsLimit
	
		PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - Odds are too long: iPercentOfOdds: ", iPercentOfOdds, ", g_sMPTunables.fLongOddsLimit: ", g_sMPTunables.fLongOddsLimit)
		CALC_PLAYER_CORRECTED_ODD_PERCENTAGE(iPercentOfOdds, g_sMPTunables.fLongOddsLimit)
	
		CALC_PLAYER_BETTING_ODDS(iPercentOfOdds, iOddN, iOddD, bFinalOdds)
		PRINTLN("[JA@BETTING](S) CALC_EXACT_ODDS - New long odds: iOddN: ", iOddN, ", iOddD: ", iOddD)
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns the total cash placed on this player
FUNC INT GET_CASH_BET_ON_BETTING_INDEX(INT index, BETTING_ID bettingId)

	INT i
	INT iTotalCashBet
	PLAYER_INDEX playerID
	REPEAT NUM_NETWORK_PLAYERS i
		IF ARE_BETTING_IDS_EQUAL(bettingId, GlobalServerBD_Betting.playerBettingData[i].bettingID)
			
			playerID = INT_TO_PLAYERINDEX(i)
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerID)
				iTotalCashBet += GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.betsOnPlayers[index].iPlacedBet
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iTotalCashBet
ENDFUNC

/// PURPOSE: Look for all other players participating in same betting pool and update state if needed
PROC UPDATE_ODDS_AND_STATE_OF_BETTING(BETTING_ID bettingId, INT iSyncID)	
	BETTING_ID emptyBettingID
	
	// EARLY EXIT
	IF ARE_BETTING_IDS_EQUAL(emptyBettingID, bettingID)
		PRINTLN("[JA@BETTING](S) UPDATE_ODDS_AND_STATE_OF_BETTING - betting ID is 0 (null) IGNORE")
		
		EXIT
	ENDIF	
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("[JA@BETTING](S) UPDATE_ODDS_AND_STATE_OF_BETTING - called for betting ID: ") PRINT_BETTING_ID(bettingId) PRINTNL()
	#ENDIF
	
	// -----------------------------
	// 		Loop to gather info
	// -----------------------------
	
	// Used for morning line
	INT iTotalStat = 0
	// Used for exact betting
	INT iTotalBet = 0
	
	INT i
	INT iSlotsActiveInBetting
	INT iBettingIndexWithBetsOn
	BOOL bPoolIsLocked
	
	
	// Loop over our active data (General - no knowledge of teams)
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Check if the betting index is active
		IF IS_BETTING_INDEX_ACTIVE(i)
			iSlotsActiveInBetting++				// Incrememnt our count (determines if betting active)
			
			IF IS_BETTING_INDEX_LOCKED(i)		// If locked then stop doing anything
				bPoolIsLocked = TRUE
			ELSE
			
				// Update the cash bet on index, total stat and total cash bet
				SET_PLAYER_AMOUNT_BET_ON(i, GET_CASH_BET_ON_BETTING_INDEX(i, bettingID))
				
				iTotalStat += GET_ACTIVE_BETTING_INDEX_STAT(i)
				iTotalBet += GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer
				
				IF GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer > 0
					iBettingIndexWithBetsOn++
				ENDIF
			ENDIF
		ENDIF

	ENDREPEAT
	
	
	// If pool is now locked we should not update anything on the player.
	IF bPoolIsLocked
		PRINTLN("[JA@BETTING](S) UPDATE_ODDS_AND_STATE_OF_BETTING - betting pool is in lockdown for one or more players DO NOT UPDATE")
		
		EXIT
	ENDIF
	
	PRINTLN("[JA@BETTING](S) UPDATE_ODDS_AND_STATE_OF_BETTING - number of active betting indexes: ", iSlotsActiveInBetting)
	
	// --------------------------------------------
	// 	Loop to calculate odds and update players
	// --------------------------------------------
	
	INT iOddN
	INT iOddD
	INT iDeltaP
	INT iBSHasExactOdds
	FLOAT fBettingPercent
	BOOL bBettingActive = (iSlotsActiveInBetting > 1)
	INT iNewSyncID = GET_UNIQUE_BETTING_SYNC_ID(iSyncID)

	// Set the active state of pool and update with unique ID
	REPEAT NUM_NETWORK_PLAYERS i
		
		// First updates players state of this betting pool (Enough things to bet on?)
		IF ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(i), bettingId)
			
			SET_PLAYER_BETTING_AS_ACTIVE(i, bBettingActive, iSlotsActiveInBetting)
			SET_PLAYER_BETTING_SYNC_ID(i, iNewSyncID)
		ENDIF
		
		// Second update our betting index for odds if is is active
		IF IS_BETTING_INDEX_ACTIVE(i)
		
			#IF IS_DEBUG_BUILD
				PRINTSTRING("[JA@BETTING](S) Calculate odds for betting slot ") PRINTINT(i) PRINTSTRING(", ID = ") PRINT_BETTING_ID(bettingId) PRINTNL()
			#ENDIF
			
			// If we have more than one bet placed lets calculate exact odds.
			IF iBettingIndexWithBetsOn > 1
				IF HAS_PLAYER_BEEN_BET_ON(i)
					IF CALC_EXACT_ODDS(GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer, iTotalBet, iOddN, iOddD)
						SET_BIT(iBSHasExactOdds, i)
					ENDIF
				ENDIF
			ENDIF
			
			// If we don't have exact odds we need to calculate morning line
			IF NOT IS_BIT_SET(iBSHasExactOdds, i)
				fBettingPercent = ((TO_FLOAT(GET_ACTIVE_BETTING_INDEX_STAT(i)) / iTotalStat) * 100.0) * 1.25
				
				IF fBettingPercent >= g_sMPTunables.fShortOddsLimit
					iDeltaP += ROUND(fBettingPercent - g_sMPTunables.fShortOddsLimit)
					
					PRINTLN("[JA@BETTING](S) BETTING PERCENTAGE IS GREATER THAN SHORT ODD LIMIT. iDeltaP = ", iDeltaP, ", fBettingPercent = ", fBettingPercent)
					
					fBettingPercent = g_sMPTunables.fShortOddsLimit
				ENDIF
				
				IF fBettingPercent < g_sMPTunables.fLongOddsLimit
					
					PRINTLN("[JA@BETTING](S) BETTING PERCENTAGE IS LESS THAN LONG ODD LIMIT. fBettingPercent = ", fBettingPercent)
					
					fBettingPercent = g_sMPTunables.fLongOddsLimit
				ENDIF
				
				// Update odds for this player
				CALC_MORNING_LINE_ODDS(fBettingPercent, iOddN, iOddD)
				
				#IF IS_DEBUG_BUILD
					GlobalServerBD_Betting.activeBettingData[i].iMorningLineN = iOddN
					GlobalServerBD_Betting.activeBettingData[i].iMorningLineD = iOddD
				#ENDIF
				
			ENDIF
			
			SET_PLAYER_BETTING_ODDS(i, iOddN, iOddD)
		ENDIF
	ENDREPEAT
	
	// -----------------------------------
	// 	Loop to redistribute odds (if any)
	// -----------------------------------
	
	// Prevent short odds by redistributing to other players
	IF iDeltaP > 0
	AND iSlotsActiveInBetting > 1
		iDeltaP = (iDeltaP / (iSlotsActiveInBetting-1))
		
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_BETTING_INDEX_ACTIVE(i) //ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(i), bettingId)
			
				// Only change those who have morning line odds (we will be X% out but shouldn't matter as more
				// concerned about payout on those who have been bet on).
				IF NOT IS_BIT_SET(iBSHasExactOdds, i)
					GET_PLAYER_BETTING_ODDS(i, iOddN, iOddD)
					fBettingPercent = 100.0 / ((TO_FLOAT(iOddN) / TO_FLOAT(iOddD)) + 1)
									
					IF (fBettingPercent + iDeltaP) <= g_sMPTunables.fShortOddsLimit
						fBettingPercent += iDeltaP
						
						PRINTLN("[JA@BETTING](S) Adding delta left over to other players. Player = ", i)
					ENDIF
					
					// Update odds for this player
					CALC_MORNING_LINE_ODDS(fBettingPercent, iOddN, iOddD)
					
					#IF IS_DEBUG_BUILD
						GlobalServerBD_Betting.activeBettingData[i].iMorningLineN = iOddN
						GlobalServerBD_Betting.activeBettingData[i].iMorningLineD = iOddD
					#ENDIF
										
					SET_PLAYER_BETTING_ODDS(i, iOddN, iOddD)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

// HARD RESET OF ALL OUR ACTIVE BETTING DATA - Triggered by player requesting to take part in new betting pool
PROC CLEAR_BETTING_ACTIVE_DATA(BETTING_ID newBettingID)
	
	BETTING_ACTIVE_DATA emptyData
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		
		GlobalServerBD_Betting.activeBettingData[i] = emptyData
		
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PRINTSTRING("[JA@BETTING](S) CLEAR_BETTING_ACTIVE_DATA - ready for new betting pool: ") PRINT_BETTING_ID(newBettingID) PRINTNL()
	#ENDIF
	
	GlobalServerBD_Betting.g_ServerBettingPoolID = newBettingID
ENDPROC

/// PURPOSE: Loops through the updated list of players betting and collates their information (team / non-team)
PROC REFRESH_BETTING_ACTIVE_DATA(BETTING_ID bettingID)

	INT i
	
	// First clear our current data
	REPEAT NUM_NETWORK_PLAYERS i
		
		IF NOT IS_BETTING_INDEX_LOCKED(i)
			//...Stat value of betting index and number of members (always 1 if non team)
			GlobalServerBD_Betting.activeBettingData[i].iStatSum = 0
			GlobalServerBD_Betting.activeBettingData[i].iActiveMembers = 0
		ENDIF
	ENDREPEAT
	
	INT iBettingIndex
	// Collate the data from active betting players and assign data to slots used for odds etc.
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(i), bettingID)
			
			iBettingIndex = GlobalServerBD_Betting.playerBettingData[i].iTeam
					
			IF iBettingIndex != -1
				
				IF NOT IS_BETTING_INDEX_LOCKED(iBettingIndex)
					GlobalServerBD_Betting.activeBettingData[iBettingIndex].iStatSum += GlobalServerBD_Betting.playerBettingData[i].iStatSum
					GlobalServerBD_Betting.activeBettingData[iBettingIndex].iActiveMembers++
					GlobalServerBD_Betting.activeBettingData[iBettingIndex].bTeamBetting = IS_PLAYER_IN_TEAM_BETTING(i)
				ENDIF
			ENDIF
		ENDIF
	
	ENDREPEAT
		
ENDPROC


/// PURPOSE: Removes player from betting pool and updates odds / list for remaining participants
PROC CLEAR_PLAYER_FROM_BETTING(INT index)
	BETTING_ID bettingID = GET_PLAYER_BETTING_ID(index)
	INT iSyncID = GET_PLAYER_BETTING_SYNC_ID(index)

	RESET_PLAYER_BETTING_SLOT(index)
	
	// Refresh our active slot
	REFRESH_BETTING_ACTIVE_DATA(bettingID)
	
	// Only worry about updating odds / state if betting is still active (lockdown server is pretty much finished)
	UPDATE_ODDS_AND_STATE_OF_BETTING(bettingID, iSyncID)
ENDPROC


/// PURPOSE: Process received event from player who is now in a valid state for betting
PROC PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT(INT iEventID)

	PRINTLN("[JA@BETTING](S) PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT - called...")
	
	EVENT_STRUCT_PLAYER_AVAILABLE_FOR_BETTING Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex, FALSE)
				
		
			INT index = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
			
			IF NOT ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(index), Event.bettingID)
				BETTING_ID emptyBettingID
			
				// Check if they have changed betting pools without properly leaving
				IF NOT ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(index), emptyBettingID)
						
					PRINTLN("[JA@BETTING](S) PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT - player believed to be in a betting pool. Clean up first.")
											
					CLEAR_PLAYER_FROM_BETTING(index)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[JA@BETTING](S) 			Player: ", GET_PLAYER_NAME(Event.Details.FromPlayerIndex))
					PRINTSTRING("[JA@BETTING](S) 			Betting ID: ") PRINT_BETTING_ID(Event.bettingID) PRINTNL()
					PRINTLN("[JA@BETTING](S) 			iStatValue: ", Event.iStatValue)
					PRINTLN("[JA@BETTING](S) 			bTeamBetting: ", Event.bTeamBetting)
					PRINTLN("[JA@BETTING](S) 			bTutorial: ", PICK_STRING(Event.bTutorial, "TRUE", "FALSE"))
					PRINTLN("[JA@BETTING](S) 			iTeam: ", Event.iTeam)
				#ENDIF
				
				IF NOT ARE_BETTING_IDS_EQUAL(GlobalServerBD_Betting.g_ServerBettingPoolID, Event.bettingID)
					PRINTLN("[JA@BETTING](S) PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT - PLAYER REQUESTED NEW BETTING ID - CLEAN UP PREVIOUS")
					
					CLEAR_BETTING_ACTIVE_DATA(Event.bettingID)
					
				ENDIF
				
				SET_PLAYER_BETTING_ID(index, Event.bettingID)						// Store the players betting ID
				SET_PLAYER_BETTING_STAT_VALUE(index, Event.iStatValue)				// Store the stat value this player brings to betting
				SET_PLAYER_TEAM_BETTING_VAR(index, Event.bTeamBetting)				// Store if the player is in a team corona
				SET_PLAYER_BETTING_TEAM(index, Event.iTeam)							// Store the initial team the player thinks they are on
				
				
				// Check if this is the tutorial ped
				IF Event.bTutorial
					PRINTLN("[JA@BETTING] PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT - We need to handle tutorial ped")
					
					SET_BETTING_INDEX_ON_TUTORIAL(index)		
					
					// Set up the tutorial ped
					SET_PLAYER_BETTING_ID(BETTING_TUTORIAL_PED_INDEX, Event.bettingID)
					SET_PLAYER_BETTING_STAT_VALUE(BETTING_TUTORIAL_PED_INDEX, BETTING_TUTORIAL_PED_STAT)
					SET_PLAYER_TEAM_BETTING_VAR(BETTING_TUTORIAL_PED_INDEX, FALSE)
					SET_PLAYER_BETTING_TEAM(BETTING_TUTORIAL_PED_INDEX, BETTING_TUTORIAL_PED_INDEX)
					SET_BETTING_INDEX_ON_TUTORIAL(BETTING_TUTORIAL_PED_INDEX)
				ENDIF
				
				// Refresh our active slot
				REFRESH_BETTING_ACTIVE_DATA(Event.bettingID)
				
				UPDATE_ODDS_AND_STATE_OF_BETTING(Event.bettingID, GET_PLAYER_BETTING_SYNC_ID(index))
			ELSE
				PRINTLN("[JA@BETTING](S) PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT - player already part of betting pool. IGNORE")
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_PLAYER_AVAILABLE_FOR_BETTING_EVENT - could not retrieve data")
	ENDIF

ENDPROC

/// PURPOSE: Process received event from player who has left betting
PROC PROCESS_PLAYER_LEFT_BETTING(INT iEventID)
	
	EVENT_STRUCT_PLAYER_LEFT_BETTING Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF IS_NET_PLAYER_OK(Event.Details.FromPlayerIndex, FALSE)
		
			INT index = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
			BETTING_ID emptyBettingID
			
			IF NOT ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(index), emptyBettingID)				
				PRINTLN("[JA@BETTING](S) PROCESS_PLAYER_LEFT_BETTING - called for player ID ", index, "[", GET_PLAYER_NAME(Event.Details.FromPlayerIndex), "] T(", GET_CLOUD_TIME_AS_INT(), ")")
					
				CLEAR_PLAYER_FROM_BETTING(index)
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("PROCESS_PLAYER_LEFT_BETTING - could not retrieve data")
	ENDIF

ENDPROC

/// PURPOSE: Calculates final odds since the betting has finished.
PROC PROCESS_FINAL_ODDS(BETTING_ID bettingID)

	INT i
	// Used for exact betting
	INT iTotalBet = 0
	INT iBettingIndexWithBetsOn = 0
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF IS_BETTING_INDEX_ACTIVE(i)
			
			// Update the cash bet on index, total stat and total cash bet
			SET_PLAYER_AMOUNT_BET_ON(i, GET_CASH_BET_ON_BETTING_INDEX(i, bettingID))		// Update the final amount of cash bet on this slot
			
			iTotalBet += GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer				// Gather total cash bet
			
			IF GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer > 0
				iBettingIndexWithBetsOn++													// Record how many bets have been placed
			ENDIF	
			
		ENDIF

	ENDREPEAT
	
	INT iOddN
	INT iOddD
	REPEAT NUM_NETWORK_PLAYERS i
	
		// Process active betting indexes
		IF IS_BETTING_INDEX_ACTIVE(i)
		
			IF HAS_PLAYER_BEEN_BET_ON(i)
				// Keep odds as short as possible (chances are, house will lose money in this situation)
				IF NOT CALC_EXACT_ODDS(GlobalServerBD_Betting.activeBettingData[i].iCashBetOnPlayer, iTotalBet, iOddN, iOddD, TRUE)
					iOddN = 1
					iOddD = 10
				ENDIF
			
				PRINTLN("[JA@BETTING](S) PROCESS_FINAL_ODDS - final odds for player ", i, " are ", iOddN, "/", iOddD, " T(", GET_CLOUD_TIME_AS_INT(), ")")
				
				SET_PLAYER_BETTING_ODDS(i, iOddN, iOddD)
			ENDIF					
		ENDIF
		
		// Process any player updates we need to
		IF ARE_BETTING_IDS_EQUAL(bettingID, GET_PLAYER_BETTING_ID(i))
		
			IF NOT IS_BETTING_INDEX_ON_TUTORIAL(i)
		
				// If only one player has been bet on, void pool
				IF iBettingIndexWithBetsOn = 1
					PRINTLN("[JA@BETTING](S) Betting pool is void, setting bit for player ", i)
					SET_BIT(GlobalServerBD_Betting.playerBettingData[i].iBSPlayerState, BS_BETTING_POOL_VOID)
				ENDIF
			ENDIF
			
			SET_BIT(GlobalServerBD_Betting.playerBettingData[i].iBSPlayerState, BS_BETTING_FINAL_ODDS)
		ENDIF		
	ENDREPEAT
	
ENDPROC

PROC PROCESS_BETTING_POOL_LOCKED(INT iEventID)

	EVENT_STRUCT_BETTING_POOL_LOCKED Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		
		PRINTLN("[JA@BETTING](S) PROCESS_BETTING_POOL_LOCKED - setting player ", NATIVE_TO_INT(Event.Details.FromPlayerIndex), "'s pool as locked T(", GET_CLOUD_TIME_AS_INT(), ")")
		
		INT iPlayerID = NATIVE_TO_INT(Event.Details.FromPlayerIndex)
		
		// Finalise our odds if we haven't already.
		IF NOT ARE_FINAL_ODDS_CALCULATED(iPlayerID)
			PROCESS_FINAL_ODDS(Event.bettingID)
		ENDIF
		
		SET_BIT(GlobalServerBD_Betting.playerBettingData[iPlayerID].iBSPlayerState, BS_BETTING_PLAYER_POOL_LOCKED)
		
		INT i
		// Lock our active betting index
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_BETTING_INDEX_ACTIVE(i)
				GlobalServerBD_Betting.activeBettingData[i].bLocked = TRUE
			ENDIF
		ENDREPEAT
		
	ELSE
		SCRIPT_ASSERT("PROCESS_BETTING_POOL_LOCKED - could not retrieve data")
	ENDIF

ENDPROC

/// PURPOSE: Broadcasts a rejection to the player who made the bet 
PROC BROADCAST_BETTING_CASH_REJECTED(EVENT_STRUCT_BETTING_VIRTUAL_CASH_BET OldEvent)

	EVENT_STRUCT_BETTING_VIRTUAL_CASH_REJECTED Event
	Event.Details.Type = SCRIPT_EVENT_VIRTUAL_CASH_REJECTED
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iBettingIndex = OldEvent.iBettingIndex
	Event.iCashDelta = OldEvent.iCashDelta
	
	INT iSendTo = SPECIFIC_PLAYER(OldEvent.Details.FromPlayerIndex)
	IF NOT (iSendTo = 0)
	
		PRINTLN("[JA@BETTING](S) BROADCAST_BETTING_CASH_REJECTED called...(", GET_CLOUD_TIME_AS_INT(), ")")
		PRINTLN("[JA@BETTING](S) 		- iBettingIndex = ", Event.iBettingIndex)
		PRINTLN("[JA@BETTING](S) 		- iCashDelta = ", Event.iCashDelta)
		PRINTLN("[JA@BETTING](S)********************************************************")
		
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
		
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("BROADCAST_BETTING_CASH_REJECTED - playerflags = 0 so not broadcasting")
	#ENDIF
		
	ENDIF

ENDPROC

/// PURPOSE: Processes a value of cash bet from a player. Cumuulative cash is checked and rejected based on value
PROC PROCESS_BETTING_VIRTUAL_CASH_BET(INT iEventID)

	EVENT_STRUCT_BETTING_VIRTUAL_CASH_BET Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
	
		PRINTLN("[JA@BETTING](S) PROCESS_BETTING_VIRTUAL_CASH_BET - ")
		PRINTLN("[JA@BETTING](S) 				- Player = [", GET_PLAYER_NAME(Event.Details.FromPlayerIndex), "]")
		PRINTLN("[JA@BETTING](S) 				- iBettingInex = ", Event.iBettingIndex)
		PRINTLN("[JA@BETTING](S) 				- iCashDelta = ", Event.iCashDelta)
		
		INT iCurrentBet = GET_CUMULATIVE_CASH_ON_BETTING_INDEX(Event.iBettingIndex)
		
		PRINTLN("[JA@BETTING](S) 				- iCurrentBet = ", iCurrentBet)
		
		iCurrentBet += Event.iCashDelta
		
		PRINTLN("[JA@BETTING](S) 				- iCurrentBet (now) = ", iCurrentBet)
		
		IF iCurrentBet < 0
			PRINTLN("[JA@BETTING](S) 				- Cumulative cash for index = ", Event.iBettingIndex, " is now $0")
			SET_CUMULATIVE_CASH_ON_BETTING_INDEX(Event.iBettingIndex, 0)
		ELSE
			IF iCurrentBet > g_sMPTunables.fMaxbettotalamountonasingleplayer
				// SEND A REJECTION
				
				PRINTLN("[JA@BETTING](S) 				- Cumulative cash for index = ", Event.iBettingIndex, " is now $", iCurrentBet)
				PRINTLN("[JA@BETTING](S) 				- Sending REJECTION to player")
				
				BROADCAST_BETTING_CASH_REJECTED(Event)				
			ELSE
				SET_CUMULATIVE_CASH_ON_BETTING_INDEX(Event.iBettingIndex, iCurrentBet)
			ENDIF
		ENDIF
	
	ELSE
		SCRIPT_ASSERT("PROCESS_BETTING_POOL_LOCKED - could not retrieve data")
	ENDIF
	
ENDPROC

// --------------------- MAIN SERVER SYSTEM LOOP -----------------------

/// PURPOSE: Staggered loop over all players to update betting status and track players leaving
PROC MAINTAIN_SERVER_BETTING()
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_BETTING_ENABLED()
			EXIT
		ENDIF
	#ENDIF
	
	INT iCount = MPGlobals.g_iBettingPlayerLoop	// STAGGER
	PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iCount)
	
	IF IS_NET_PLAYER_OK(playerId, FALSE)
		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(playerId)

			BOOL bUpdateOdds = FALSE

			// Update players bets if they have bet
			IF GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.iBetSyncID != GlobalServerBD_Betting.playerBettingData[iCount].iBetSyncID
				
				//...Update server knowledge of sync ID
				GlobalServerBD_Betting.playerBettingData[iCount].iBetSyncID = GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.iBetSyncID
			
				//...If sync ID valid then update our odds
				IF GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.iBetSyncID != 0

					PRINTLN("[JA@BETTING](S) MAINTAIN_SERVER_BETTING - Player ", iCount, " [", GET_PLAYER_NAME(playerId), "] has placed bets, update odds and state of betting. Sync ID = ", GlobalplayerBD[NATIVE_TO_INT(playerID)].clientBetData.iBetSyncID)
					
					bUpdateOdds = TRUE
					
				ENDIF
			ENDIF
			
			// Update if the players team has changed
			IF GlobalServerBD_Betting.playerBettingData[iCount].iTeam != GET_PLAYER_TEAM_IN_CORONA(playerId)
				
				BOOL bIsBettingLocked = FALSE
				IF GlobalServerBD_Betting.playerBettingData[iCount].iTeam != -1
					bIsBettingLocked = IS_BETTING_INDEX_LOCKED(GlobalServerBD_Betting.playerBettingData[iCount].iTeam)
				ENDIF
				
				IF NOT bIsBettingLocked
					// Update our team knowledge
					PRINTLN("[JA@BETTING](S) MAINTAIN_SERVER_BETTING - Player ", iCount, " [", GET_PLAYER_NAME(playerId), "] has changed teams from: Team: ", GlobalServerBD_Betting.playerBettingData[iCount].iTeam, ", to Team: ", GET_PLAYER_TEAM_IN_CORONA(playerId), ", update odds and state of betting. T(", GET_CLOUD_TIME_AS_INT(), ")")
					GlobalServerBD_Betting.playerBettingData[iCount].iTeam = GET_PLAYER_TEAM_IN_CORONA(playerId)
				ENDIF
				
				// Refresh our data
				REFRESH_BETTING_ACTIVE_DATA(GET_PLAYER_BETTING_ID(iCount))
				
				bUpdateOdds = TRUE
			ENDIF
			
			// If we need to update the odds, then do so.
			IF bUpdateOdds
				UPDATE_ODDS_AND_STATE_OF_BETTING(GET_PLAYER_BETTING_ID(iCount), GET_PLAYER_BETTING_SYNC_ID(iCount))
			ENDIF
		ENDIF
	ELSE
	
		// Only clean up if we are not on tutorial and it's not tutorial ped index
		IF NOT IS_BETTING_INDEX_ON_TUTORIAL(iCount)
		OR iCount != BETTING_TUTORIAL_PED_INDEX
		
			// Player is not in a playing state - clean up betting
			BETTING_ID blankBettingID
		
			IF NOT ARE_BETTING_IDS_EQUAL(GET_PLAYER_BETTING_ID(iCount), blankBettingID)
			
				PRINTLN("[JA@BETTING](S) MAINTAIN_SERVER_BETTING - player not in playing state. Clean up index: ", iCount)
			
				CLEAR_PLAYER_FROM_BETTING(iCount)
			ENDIF	
		
		ENDIF
	ENDIF
	
	iCount += 1
	IF iCount >= NUM_NETWORK_PLAYERS
		MPGlobals.g_iBettingPlayerLoop = 0
	ELSE
		MPGlobals.g_iBettingPlayerLoop = iCount
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC PROCESS_BETTING_TOGGLED()
		IF GlobalServerBD.g_bDebugBettingEnabled
			GlobalServerBD.g_bDebugBettingEnabled = FALSE
			PRINTLN("[JA@BETTING](S) SCRIPT_EVENT_TOGGLE_BETTING - betting turned OFF")
		ELSE
			GlobalServerBD.g_bDebugBettingEnabled = TRUE
			PRINTLN("[JA@BETTING](S) SCRIPT_EVENT_TOGGLE_BETTING - betting turned ON")
		ENDIF
	ENDPROC
#ENDIF



