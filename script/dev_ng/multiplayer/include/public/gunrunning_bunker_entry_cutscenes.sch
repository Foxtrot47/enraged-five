//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        gunrunning_bunker_entry_cutscenes.sch														//
// Description: Implementation for all the possible cutscenes for bunker entry								//
// Written by:  Aleksej Leskin																				//
// Date:  		27/03/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_simple_cutscene.sch"
USING "mp_globals_simple_interior.sch"
USING "net_simple_interior_base.sch"
USING "Cutscene_help.sch"
USING "fmmc_camera.sch"
USING "net_realty_bunker.sch"

CONST_FLOAT BUNKER_CUTSCENE_DOOR_MOVE_SPEED		0.15
CONST_FLOAT	BUNKER_CUTSCENE_DOOR_CLOSED_ANGLE	20.000
CONST_FLOAT BUNKER_CUTSCENE_DOOR_OPENED_ANGLE	0.000
CONST_FLOAT BUNKER_CUTSCENE_DOOR_OPENNING_SPEED 0.25

ENUM BUNKER_CUTSCENE_ASSETS_ENUM
	BUNKER_CA_BUNKER_DOOR,
	BUNKER_CA_BUNKER_DOOR_FRAME,
	
	BUNKER_CA_VEHICLE_ENTRY,
	BUNKER_CA_FOOT_ENTRY,
	BUNKER_CA_HELI_ENTRY,
	BUNER_CA_HELI_INFRONT_ENTRY,
	BUNKER_CA_PLAYER_HIDE,
	BUNKER_CA_VEHICLE_STOP_AREA,
	BUNKER_CA_VEHICLE_ENTRY_FAR
ENDENUM

/// PURPOSE:
///    Prop world transform , setup for BUNKER_CUTSCENE_GET_BASE_INTERIOR
/// PARAMS:
///    eCutsceneAsset - 
/// RETURNS:
///    
FUNC TRANSFORM_STRUCT BUNKER_CUTSCENE_GET_BASE_PROP_TRANSFORM(BUNKER_CUTSCENE_ASSETS_ENUM eCutsceneAsset)
	TRANSFORM_STRUCT sOutTransform
	
	SWITCH eCutsceneAsset
		CASE BUNKER_CA_BUNKER_DOOR
			sOutTransform.Position = <<846.955, 3129.642, 40.959>>
			sOutTransform.Rotation = <<0.000, BUNKER_CUTSCENE_DOOR_CLOSED_ANGLE, -85.000>>
		BREAK
		
		CASE BUNKER_CA_BUNKER_DOOR_FRAME
			sOutTransform.Position = <<847.707, 3121.010, 40.759>>
			sOutTransform.Rotation = <<0.0, 0.0, -85.000>>
		BREAK
				
		CASE BUNKER_CA_VEHICLE_ENTRY
			sOutTransform.Position = <<849.300, 3101.211, 42.162>> 
			sOutTransform.Rotation = <<0.0, 0.0, 4.60>>
		BREAK
		
		CASE BUNKER_CA_FOOT_ENTRY
			sOutTransform.Position = <<849.4744, 3104.4358, 42.1616>>
			sOutTransform.Rotation = <<0.0, 0.0, 5.54>>
		BREAK
		CASE BUNKER_CA_HELI_ENTRY
			sOutTransform.Position = <<850.9042, 3092.861, 39.95013>>
			sOutTransform.Rotation = <<0.0, 0.0, 0.0>>
		BREAK
		CASE BUNER_CA_HELI_INFRONT_ENTRY
			sOutTransform.Position = <<848.9125, 3102.1594, 40.0891>>
			sOutTransform.Rotation = <<0.0, 0.0, 0.0>>
		BREAK
		
		CASE BUNKER_CA_PLAYER_HIDE
			sOutTransform.Position = <<844.2994, 3133.3228, 40.2004>>
			sOutTransform.Rotation = <<0.0, 0.0, 166.6310>>
		BREAK
		CASE BUNKER_CA_VEHICLE_STOP_AREA
			sOutTransform.Position = <<489.1420, 3008.6943, 39.5595>>
			sOutTransform.Rotation = <<0.0, 0.0, 0.0>>
		BREAK
		CASE BUNKER_CA_VEHICLE_ENTRY_FAR
			sOutTransform.Position = <<850.801, 3084.057, 40.292>>
			sOutTransform.Rotation = <<0.0, 0.0, 5.00>>
		BREAK
	ENDSWITCH
	
	RETURN sOutTransform
ENDFUNC

FUNC TRANSFORM_STRUCT BUNKER_CUTSCENE_GET_BASE_SCENE_TRANSFORM()
	TRANSFORM_STRUCT sBaseTransform
	
	sBaseTransform.Position = <<847.708, 3121.015, 40.761>>
	sBaseTransform.Rotation = <<0.000, 0.000, -85.000>>

	RETURN sBaseTransform
ENDFUNC

STRUCT BUNKER_DOOR_TYPE_STRUCT
	MODEL_NAMES modelDoor
	MODEL_NAMES modelFrame
ENDSTRUCT

FUNC BUNKER_DOOR_TYPE_STRUCT BUNKER_CUTSCENE_GET_DOOR_MODEL(SIMPLE_INTERIORS eInterior)
	BUNKER_DOOR_TYPE_STRUCT sOutModels
	
	sOutModels.modelDoor = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_doorpart"))
	sOutModels.modelFrame = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_basepart"))
		
	SWITCH eInterior
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		sOutModels.modelDoor = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_DoorPart_F"))
		sOutModels.modelFrame = INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_BasePart_F"))
		BREAK
	ENDSWITCH
	
	RETURN sOutModels
ENDFUNC

PROC BUNKER_CUTSCENE_DUMP_IPL_COORDS()
	#IF IS_DEBUG_BUILD
	SIMPLE_INTERIORS eBunkerInterior
	TRANSFORM_STRUCT sBunkerTransform
	TEXT_LABEL_63 tlBunkerName
	OBJECT_INDEX oBunkerDoor
	INT iBunkerNumber = 1
	INT iBunker
	
	CDEBUG1LN(DEBUG_PROPERTY, "")
	CDEBUG1LN(DEBUG_PROPERTY, "-------------- BUNKER_CUTSCENE_DUMP_IPL_COORDS --------------")
	
	FOR iBunker = SIMPLE_INTERIOR_BUNKER_1 TO SIMPLE_INTERIOR_BUNKER_12

		
		eBunkerInterior = INT_TO_ENUM(SIMPLE_INTERIORS, iBunker)
		sBunkerTransform = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eBunkerInterior)
		oBunkerDoor = GET_CLOSEST_OBJECT_OF_TYPE(sBunkerTransform.Position, 1500.0, GR_PROP_GR_BUNKEDDOOR, FALSE, FALSE, FALSE)
		
		tlBunkerName = "SIMPLE_INTERIOR_BUNKER_"
		tlBunkerName += iBunkerNumber
			
		IF DOES_ENTITY_EXIST(oBunkerDoor)
			VECTOR vCoords
			vCoords = GET_ENTITY_COORDS(oBunkerDoor)
			VECTOR vRotation
			vRotation = GET_ENTITY_ROTATION(oBunkerDoor)
			CDEBUG1LN(DEBUG_PROPERTY, "CASE ", tlBunkerName)
			CDEBUG1LN(DEBUG_PROPERTY, "sOutTransform.Position = ", vCoords)
			CDEBUG1LN(DEBUG_PROPERTY, "sOutTransform.Rotation = ", vRotation)
			CDEBUG1LN(DEBUG_PROPERTY, "BREAK")
		ELSE
			CDEBUG1LN(DEBUG_PROPERTY, "BUNKER_CUTSCENE_DUMP_IPL_COORDS - GR_PROP_GR_BUNKEDDOOR was not found at position: ", sBunkerTransform.Position, " BunkerName: ", tlBunkerName)
		ENDIF
		
		iBunkerNumber++
		IF iBunkerNumber = 8
			iBunkerNumber++
		ENDIF
	ENDFOR
	#ENDIF
ENDPROC

/// PURPOSE:
///    Get world coords of a prop for bunker entry cutscene.
/// PARAMS:
///    eInterior - which bunker location to get transform for.
///    eCutsceneAsset - which cutscene prop.
/// RETURNS:
///    
FUNC TRANSFORM_STRUCT BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(SIMPLE_INTERIORS eInterior, BUNKER_CUTSCENE_ASSETS_ENUM eCutsceneAsset, BOOL bPrintDebug = FALSE)	
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
	CDEBUG1LN(DEBUG_PROPERTY, "")
	CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD, eInterior: ", ENUM_TO_INT(eInterior), " Asset: ", ENUM_TO_INT(eCutsceneAsset))
	ENDIF
	#ENDIF
	
	TRANSFORM_STRUCT sTransformOut
	sTransformOut = _TRANSFORM_VIA_OFFSET(
		BUNKER_CUTSCENE_GET_BASE_SCENE_TRANSFORM(),
		BUNKER_CUTSCENE_GET_BASE_PROP_TRANSFORM(eCutsceneAsset),
		BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eInterior),
		bPrintDebug)
	
//	TRANSFORM_STRUCT vRot
//	vRot = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eInterior)
//	
//	ROTATE_VECTOR_FMMC(sTransformOut.Position, vRot.Rotation)	
//	sTransformOut.Rotation = sTransformOut.Rotation + vRot.Rotation
	RETURN sTransformOut
ENDFUNC

FUNC BOOL BUNKER_CUTSCENE_BUNKERDOOR_ASSETS_LOADED()
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_doorpart")))
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_basepart")))
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_DoorPart_F")))
	REQUEST_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_BasePart_F")))
	REQUEST_MODEL(prop_golf_ball)
	
	IF NOT HAS_MODEL_LOADED(prop_golf_ball)
		CDEBUG1LN(DEBUG_PROPERTY, "[INTERIOR_BUNKER][BUNKERASSETS] - HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED - NOT LOADED, prop_golf_ball")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_doorpart")))
		CDEBUG1LN(DEBUG_PROPERTY, "[INTERIOR_BUNKER][BUNKERASSETS] - HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED - NOT LOADED, gr_prop_gr_doorpart")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_DoorPart_F")))
		CDEBUG1LN(DEBUG_PROPERTY, "[INTERIOR_BUNKER][BUNKERASSETS] - HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED - NOT LOADED, gr_Prop_GR_DoorPart_F")
		RETURN FALSE
	ENDIF
	
	
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_basepart")))
		CDEBUG1LN(DEBUG_PROPERTY, "[INTERIOR_BUNKER][BUNKERASSETS] - HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED - NOT LOADED, gr_prop_gr_basepart")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_MODEL_LOADED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_GR_BasePart_F")))
		CDEBUG1LN(DEBUG_PROPERTY, "[INTERIOR_BUNKER][BUNKERASSETS] - HAVE_BUNKER_CUSTOM_CUTSCENE_ASSETS_LOADED - NOT LOADED, gr_prop_gr_basepart")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL BUNKER_CUTSCENE_BUNKERDOOR_STREAM_LOADED(INT &bitSet, INT iBit)
	IF NOT IS_BIT_SET(bitSet, iBit)
		STOP_STREAM()
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[INTERIOR_BUNKER][BUNKERASSETS] - BUNKER_CUTSCENE_BUNKERDOOR_STREAM_LOADED - existing streams stopped.")
		SET_BIT(bitSet, iBit)
	ENDIF
	
	IF NOT LOAD_STREAM("Door_Open_Long", "DLC_GR_Bunker_Door_Sounds")
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[INTERIOR_BUNKER] - BUNKER_CUTSCENE_BUNKERDOOR_STREAM_LOADED - Door_Open_Long - has not loaded yet.")
		RETURN FALSE
	ENDIF
	
	CDEBUG1LN(DEBUG_SMPL_INTERIOR, "[INTERIOR_BUNKER][BUNKERASSETS] - BUNKER_CUTSCENE_BUNKERDOOR_STREAM_LOADED - Loaded succesfully.")
	RETURN TRUE
ENDFUNC

FUNC BOOL _BUNKER_CUTSCENE_CREATE_OBJECT(OBJECT_INDEX &object, MODEL_NAMES eModel, VECTOR position, VECTOR rotation)
	IF eModel = DUMMY_MODEL_FOR_SCRIPT
		ASSERTLN("[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_CREATE_OBJECT - Invalid Model")
		RETURN FALSE
	ENDIF

	IF NOT DOES_ENTITY_EXIST(object)
		CDEBUG1LN(DEBUG_CUTSCENE, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_CREATE_OBJECT - try to create a bunker cutscene obejct, model: ", GET_MODEL_NAME_FOR_DEBUG(eModel), " Position: ", position, " Rotation: ", rotation)
		object = CREATE_OBJECT_NO_OFFSET(eModel, position, FALSE, FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(object)
		SET_ENTITY_INVINCIBLE(object, TRUE)
		SET_ENTITY_ROTATION(object, rotation)
		FREEZE_ENTITY_POSITION(object, TRUE)
		CDEBUG1LN(DEBUG_CUTSCENE, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_HAVE_ASSETS_LOADED - Created Succesfully.", GET_MODEL_NAME_FOR_DEBUG(eModel), " Position: ", position)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Create bunker entry door assets
FUNC BOOL BUNKER_CUTSCENE_CREATE_ENTRY_DOOR(SIMPLE_INTERIORS eInterior, OBJECT_INDEX &Door, OBJECT_INDEX &DoorFrame, FLOAT fDoorAngle = BUNKER_CUTSCENE_DOOR_CLOSED_ANGLE)
	IF DOES_ENTITY_EXIST(Door)
	AND DOES_ENTITY_EXIST(DoorFrame)
		RETURN TRUE
	ENDIF
	
	TRANSFORM_STRUCT sDoorTransform
	sDoorTransform = BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(eInterior, BUNKER_CA_BUNKER_DOOR)
	TRANSFORM_STRUCT sDoorFrameTransform
	sDoorFrameTransform = BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(eInterior, BUNKER_CA_BUNKER_DOOR_FRAME)
	
	BUNKER_DOOR_TYPE_STRUCT sDoorModels
	sDoorModels = BUNKER_CUTSCENE_GET_DOOR_MODEL(eInterior)
	
	IF _BUNKER_CUTSCENE_CREATE_OBJECT(Door, sDoorModels.modelDoor, sDoorTransform.Position, sDoorTransform.Rotation)
	AND _BUNKER_CUTSCENE_CREATE_OBJECT(DoorFrame, sDoorModels.modelFrame, sDoorFrameTransform.Position, sDoorFrameTransform.Rotation)				
		IF DOES_ENTITY_EXIST(Door)
			CDEBUG1LN(DEBUG_CUTSCENE, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_ENTRY_DOOR_TRANSFORM - Door and Door Exist")
			VECTOR DoorRotation = GET_ENTITY_ROTATION(Door)
			CDEBUG1LN(DEBUG_CUTSCENE, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_ENTRY_DOOR_TRANSFORM - Door CurrentRotation: ", DoorRotation)
			fDoorAngle = fDoorAngle
			SET_ENTITY_ROTATION(Door, DoorRotation)
			CDEBUG1LN(DEBUG_CUTSCENE, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_ENTRY_DOOR_TRANSFORM - Door NewRotation: ", DoorRotation)
		ENDIF
		
		IF DOES_ENTITY_EXIST(DoorFrame)
			FREEZE_ENTITY_POSITION(DoorFrame, TRUE)
			SET_ENTITY_DYNAMIC(DoorFrame, FALSE)
		ENDIF
		
		CDEBUG1LN(DEBUG_CUTSCENE, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_ENTRY_DOOR_TRANSFORM - Door and frame created succesfully.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC BUNKER_CUTSCENE_LIGHT_TRANSFORM_CUTSCENE(SIMPLE_INTERIORS eNewBunker, INT iScenesCount, SIMPLE_CUTSCENE_SHOT &sScenes[], TRANSFORM_STRUCT sReferenceTransform, BOOL bPrintDebug = FALSE)
	UNUSED_PARAMETER(bPrintDebug)
	
	OBJECT_INDEX objBase, objProp, objProp2
	TRANSFORM_STRUCT sNewTransform = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eNewBunker)
	INT iScene
	
	objBase = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 10.0>>, FALSE, FALSE, TRUE)
	IF DOES_ENTITY_EXIST(objBase)
		SET_ENTITY_COLLISION(objBase, FALSE)
		SET_ENTITY_VISIBLE(objBase, FALSE)
		SET_ENTITY_INVINCIBLE(objBase, TRUE)
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "BUNKER_CUTSCENE_TRANSFORM_CUTSCENE - objBase was not created")
		EXIT
	ENDIF
	
	objProp = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 0.0>>, FALSE, FALSE, FALSE)
	IF DOES_ENTITY_EXIST(objProp)
		SET_ENTITY_COLLISION(objProp, FALSE)
		SET_ENTITY_VISIBLE(objProp, FALSE)
		SET_ENTITY_INVINCIBLE(objProp, TRUE)
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "BUNKER_CUTSCENE_TRANSFORM_CUTSCENE - objProp was not created")
		EXIT
	ENDIF
	
	objProp2 = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 0.0>>, FALSE, FALSE, FALSE)
	IF DOES_ENTITY_EXIST(objProp2)
		SET_ENTITY_COLLISION(objProp2, FALSE)
		SET_ENTITY_VISIBLE(objProp2, FALSE)
		SET_ENTITY_INVINCIBLE(objProp2, TRUE)
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "BUNKER_CUTSCENE_TRANSFORM_CUTSCENE - objProp was not created")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
		CDEBUG1LN(DEBUG_PROPERTY, "")
		CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_TRANSFORM_CUTSCENE, eNewBunker: ", ENUM_TO_INT(eNewBunker))
	ENDIF
	#ENDIF
	
	VECTOR vRotEnd
	VECTOR vRotStart
	VECTOR vOff1, vOff2
	
	FOR iScene = 0 TO iScenesCount - 1

		SET_ENTITY_COORDS_NO_OFFSET(objBase, sReferenceTransform.Position)
		SET_ENTITY_ROTATION(objBase, sReferenceTransform.Rotation)
		
		#IF IS_DEBUG_BUILD
		IF bPrintDebug
			CDEBUG1LN(DEBUG_PROPERTY, "")
			CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_TRANSFORM_CUTSCENE, sCutscene.iScenesCount: ", iScenesCount, " iScene: ", iScene, " Before: vCamCoordsStart: ", sScenes[iScene].vCamCoordsStart, " vCamRotStart: ", sScenes[iScene].vCamRotStart,
			" vCamCoordsFinish: ", sScenes[iScene].vCamCoordsFinish, " vCamRotFinish: ", sScenes[iScene].vCamRotFinish)
		ENDIF
		#ENDIF
		
		vRotStart.x = GET_ANGULAR_DIFFERENCE(sReferenceTransform.Rotation.x, sScenes[iScene].vCamRotStart.x)
		vRotStart.y = GET_ANGULAR_DIFFERENCE(sReferenceTransform.Rotation.y, sScenes[iScene].vCamRotStart.y)
		vRotStart.z = GET_ANGULAR_DIFFERENCE(sReferenceTransform.Rotation.z, sScenes[iScene].vCamRotStart.z)
		
		vRotEnd.x = GET_ANGULAR_DIFFERENCE(sReferenceTransform.Rotation.x, sScenes[iScene].vCamRotFinish.x)
		vRotEnd.y = GET_ANGULAR_DIFFERENCE(sReferenceTransform.Rotation.y, sScenes[iScene].vCamRotFinish.y)
		vRotEnd.z = GET_ANGULAR_DIFFERENCE(sReferenceTransform.Rotation.z, sScenes[iScene].vCamRotFinish.z)
		
		
		//IF NOT bOffset
		vOff1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objBase, sScenes[iScene].vCamCoordsStart)
		vOff2 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objBase, sScenes[iScene].vCamCoordsFinish)

		SET_ENTITY_COORDS_NO_OFFSET(objBase, sNewTransform.Position)
		SET_ENTITY_ROTATION(objBase, sNewTransform.Rotation)
		ATTACH_ENTITY_TO_ENTITY(objProp, objBase, -1, vOff1, vRotStart)
		ATTACH_ENTITY_TO_ENTITY(objProp2, objBase, -1, vOff2, vRotEnd)

		#IF IS_DEBUG_BUILD
			IF bPrintDebug
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objBase), 0.2)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objBase), GET_ENTITY_COORDS(objBase) + GET_ENTITY_FORWARD_VECTOR(objBase) * 10.0)
				
				DRAW_DEBUG_SPHERE(sReferenceTransform.Position, 0.2, 90, 125, 90)
				DRAW_DEBUG_SPHERE(sNewTransform.Position, 0.3, 90, 0, 0, 100)
				
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objProp), 0.2, 255, 0, 0)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objProp), GET_ENTITY_COORDS(objProp) + GET_ENTITY_FORWARD_VECTOR(objProp) * 10.0, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objProp2), 0.2, 0, 255, 0)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objProp2), GET_ENTITY_COORDS(objProp2) + GET_ENTITY_FORWARD_VECTOR(objProp2) * 10.0, 0, 200, 0)
			ENDIF
		#ENDIF
		
		sScenes[iScene].vCamCoordsStart = GET_ENTITY_COORDS(objProp)
		sScenes[iScene].vCamRotStart	= GET_ENTITY_ROTATION(objProp)
		
		sScenes[iScene].vCamCoordsFinish = GET_ENTITY_COORDS(objProp2)
		sScenes[iScene].vCamRotFinish = GET_ENTITY_ROTATION(objProp2)
		
		IF DOES_ENTITY_EXIST(objBase)
			IF IS_ENTITY_ATTACHED(objBase)
				DETACH_ENTITY(objBase)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objProp)
			IF IS_ENTITY_ATTACHED(objProp)
				DETACH_ENTITY(objProp)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objProp2)
			IF IS_ENTITY_ATTACHED(objProp2)
				DETACH_ENTITY(objProp2)
			ENDIF
		ENDIF
	
	ENDFOR
	
	IF DOES_ENTITY_EXIST(objBase)
		IF IS_ENTITY_ATTACHED(objBase)
			DETACH_ENTITY(objBase)
		ENDIF
		DELETE_OBJECT(objBase)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objProp)
		IF IS_ENTITY_ATTACHED(objProp)
			DETACH_ENTITY(objProp)
		ENDIF
		DELETE_OBJECT(objProp)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objProp2)
		IF IS_ENTITY_ATTACHED(objProp2)
			DETACH_ENTITY(objProp2)
		ENDIF
		DELETE_OBJECT(objProp2)
	ENDIF
	
ENDPROC

PROC BUNKER_CUTSCENE_TRANSFORM_CUTSCENE(SIMPLE_INTERIORS eNewBunker, SIMPLE_CUTSCENE &sCutscene, TRANSFORM_STRUCT sReferenceTransform, BOOL bPrintDebug = FALSE)
	BUNKER_CUTSCENE_LIGHT_TRANSFORM_CUTSCENE(eNewBunker, sCutscene.iScenesCount, sCutscene.sScenes, sReferenceTransform, bPrintDebug)
ENDPROC

PROC BUNKER_CUTSCENE_MIRROR_SCENE(SIMPLE_CUTSCENE &sCutscene, INT iSceneID, VECTOR vOrigin, VECTOR vOriginDir)
	IF iSceneID >= sCutscene.iScenesCount
		ASSERTLN("BUNKER_CUTSCENE_MIRROR_SCENE - can not mirror iSceneID: ", iSceneID, " sCutscene.iScenesCount: ", sCutscene.iScenesCount)
		EXIT
	ENDIF
	
	FLOAT fAngleDifference
	CUTSCENE_HELP_MIRROR_TRANSFORM(vOrigin, vOriginDir, sCutscene.sScenes[iSceneID].vCamCoordsStart, sCutscene.sScenes[iSceneID].vCamRotStart.z, sCutscene.sScenes[iSceneID].vCamCoordsStart, sCutscene.sScenes[iSceneID].vCamRotStart.z ,fAngleDifference, FALSE)
	CDEBUG1LN(DEBUG_PROPERTY, "sCutscene.sScenes[iSceneID].vCamRotStart.z: ", sCutscene.sScenes[iSceneID].vCamRotStart.z, " ANGLE ", fAngleDifference)
	
	CUTSCENE_HELP_MIRROR_TRANSFORM(vOrigin, vOriginDir, sCutscene.sScenes[iSceneID].vCamCoordsFinish, sCutscene.sScenes[iSceneID].vCamRotFinish.z, sCutscene.sScenes[iSceneID].vCamCoordsFinish, sCutscene.sScenes[iSceneID].vCamRotFinish.z ,fAngleDifference, FALSE)
	CDEBUG1LN(DEBUG_PROPERTY, "sCutscene.sScenes[iSceneID].vCamRotFinish.z: ", sCutscene.sScenes[iSceneID].vCamRotFinish.z, " ANGLE ", fAngleDifference)
ENDPROC

PROC BUNKER_CUTSCENE_LIGHT_MIRROR_SCENE(SIMPLE_CUTSCENE_SHOT &sScenes[], INT iSceneID, VECTOR vOrigin, VECTOR vOriginDir)
	IF COUNT_OF(sScenes) < iSceneID
		ASSERTLN("BUNKER_CUTSCENE_MIRROR_SCENE - can not mirror COUNT_OF(sScenes): ", COUNT_OF(sScenes), " iSceneID: ", iSceneID)
		EXIT
	ENDIF
	
	FLOAT fAngleDifference
	CUTSCENE_HELP_MIRROR_TRANSFORM(vOrigin, vOriginDir, sScenes[iSceneID].vCamCoordsStart, sScenes[iSceneID].vCamRotStart.z, sScenes[iSceneID].vCamCoordsStart, sScenes[iSceneID].vCamRotStart.z ,fAngleDifference, FALSE)
	CDEBUG1LN(DEBUG_PROPERTY, "sCutscene.sScenes[iSceneID].vCamRotStart.z: ", sScenes[iSceneID].vCamRotStart.z, " ANGLE ", fAngleDifference)
	
	CUTSCENE_HELP_MIRROR_TRANSFORM(vOrigin, vOriginDir, sScenes[iSceneID].vCamCoordsFinish, sScenes[iSceneID].vCamRotFinish.z, sScenes[iSceneID].vCamCoordsFinish, sScenes[iSceneID].vCamRotFinish.z ,fAngleDifference, FALSE)
	CDEBUG1LN(DEBUG_PROPERTY, "sCutscene.sScenes[iSceneID].vCamRotFinish.z: ", sScenes[iSceneID].vCamRotFinish.z, " ANGLE ", fAngleDifference)
ENDPROC

/// PURPOSE:
///    Specify a vehicle that will ahve no collision with the bunker door ipl.
/// PARAMS:
///    VehRemoveCollision - 
PROC BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_ENTITY(SIMPLE_INTERIORS sBunker, ENTITY_INDEX &entity)
	IF NOT DOES_ENTITY_EXIST(entity)
		ASSERTLN("BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_ENTITY - entity doesn't exist!")
		EXIT
	ENDIF

	IF DOES_ENTITY_EXIST(MPGlobals.sBunkerLidCollisionPropData.objectBunkerLidCollision[GET_BUNKER_COUNT_FROM_SIMPLE_INTERIOR_FOR_BUNKER_LID_COLLISION(sBunker)])
		//CDEBUG1LN(DEBUG_CUTSCENE, " Disable Collsiion: BUNKER LOCATION: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(sBunker), " BUNKER: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(objBunkerDoor)), " ENTITIY: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entity)))
		SET_ENTITY_NO_COLLISION_ENTITY(entity, MPGlobals.sBunkerLidCollisionPropData.objectBunkerLidCollision[GET_BUNKER_COUNT_FROM_SIMPLE_INTERIOR_FOR_BUNKER_LID_COLLISION(sBunker)], FALSE)
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY,"BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_ENTITY - bunker door doesn't exist!")
	ENDIF
ENDPROC

PROC BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_VEH(SIMPLE_INTERIORS sBunker, VEHICLE_INDEX &inEntity)
	ENTITY_INDEX entity
	entity = inEntity
	BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_ENTITY(sBunker, entity)
ENDPROC

PROC BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_PED(SIMPLE_INTERIORS sBunker, PED_INDEX &inEntity)
	ENTITY_INDEX entity
	entity = inEntity
	BUNKER_CUTSCENE_REMOVE_BUNKER_IPL_COLLISION_ENTITY(sBunker, entity)
ENDPROC

PROC BUNKER_CUTSCENE_MAINTAIN_VEHICLE_MOVE_UNDERGROUND(VEHICLE_INDEX &vehicle, VECTOR vDoorPos, FLOAT &fSpeed, BOOL &bReachedThreshold)
	IF DOES_ENTITY_EXIST(vehicle)
	AND NOT IS_ENTITY_DEAD(vehicle)
		IF NOT bReachedThreshold
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehicle), vDoorPos) <= 11.0
				fSpeed = VMAG(GET_ENTITY_VELOCITY(vehicle))
				FREEZE_ENTITY_POSITION(vehicle, TRUE)
				SET_ENTITY_COLLISION(vehicle, FALSE)
				bReachedThreshold = TRUE
			ENDIF
		ELSE
			//Move Underground.
			VECTOR vNewPos
			vNewPos = GET_ENTITY_COORDS(vehicle) + (GET_ENTITY_FORWARD_VECTOR(vehicle) * fSpeed * GET_FRAME_TIME())
			SET_ENTITY_COORDS_NO_OFFSET(vehicle, vNewPos)
			
			VECTOR vNewRot
			vNewRot = GET_ENTITY_ROTATION(vehicle)
			IF vNewRot.x > -20
				vNewRot.x -= 35.0 * GET_FRAME_TIME()
				SET_ENTITY_ROTATION(vehicle, vNewRot)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING BUNKER_CUTSCENE_GET_BUNKER_ENTRY_RECORDING(SIMPLE_INTERIORS sBunker)
	SWITCH sBunker
		CASE SIMPLE_INTERIOR_BUNKER_1 RETURN "gr_buner_1_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_2 RETURN "gr_buner_2_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_3 RETURN "gr_buner_3_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_4 RETURN "gr_buner_4_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_5 RETURN "gr_buner_5_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_6 RETURN "gr_buner_6_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_7 RETURN "gr_buner_7_ramp"
		//CASE SIMPLE_INTERIOR_BUNKER_8 RETURN "gr_buner_3_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_9 RETURN "gr_buner_9_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_10 RETURN "gr_buner_10_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_11 RETURN "gr_buner_11_ramp"
		CASE SIMPLE_INTERIOR_BUNKER_12 RETURN "gr_buner_12_ramp"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL BUNKER_CUTSCENE_LOADED_BUNKER_ENTRY_RECORDING(SIMPLE_INTERIORS sBunker)
	STRING sVehicleRecording
	
	sVehicleRecording = BUNKER_CUTSCENE_GET_BUNKER_ENTRY_RECORDING(sBunker)
	
	IF IS_STRING_NULL_OR_EMPTY(sVehicleRecording)
		ASSERTLN("BUNKER_CUTSCENE_LOADED_BUNKER_ENTRY_RECORDING - vehicle recording does not exist for: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(sBunker))
		RETURN TRUE
	ENDIF
	
	REQUEST_WAYPOINT_RECORDING(sVehicleRecording)
	RETURN GET_IS_WAYPOINT_RECORDING_LOADED(sVehicleRecording)
ENDFUNC

PROC BUNKER_CUTSCENE_BUNKER_DOOR_SET_OPEN_RATIO(SIMPLE_INTERIORS eBunkerInterior, OBJECT_INDEX &objDoor, FLOAT fRatio = 0.0) 
	IF NOT DOES_ENTITY_EXIST(objDoor)
		ASSERTLN("BUNKER_CUTSCENE_BUNKER_DOOR_SET_OPEN_RATIO - Door does not exist, cant set ratio")
		EXIT
	ENDIF
	
	fRatio = CLAMP(fRatio, 0.0, 1.0)
	VECTOR vOpenedRot

	TRANSFORM_STRUCT sDoorClosedTransform
	sDoorClosedTransform = BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(eBunkerInterior, BUNKER_CA_BUNKER_DOOR)
	
	vOpenedRot = sDoorClosedTransform.Rotation
	vOpenedRot.y = vOpenedRot.y - 20
	
	SET_ENTITY_ROTATION(objDoor, LERP_VECTOR(sDoorClosedTransform.Rotation, vOpenedRot, fRatio))
ENDPROC

PROC BUNKER_CUTSCENE_DOOR_MAINTAIN_AUDIO(FLOAT fRatio, INT &iBitSet, INT iDoorOpenStream, INT iDoorFullyOpen)
	IF NOT IS_BIT_SET(iBitSet, iDoorOpenStream)
		CDEBUG1LN(DEBUG_SHOOTRANGE,"BUNKER_CUTSCENE_DOOR_MAINTAIN_AUDIO - Stream open door started!")
		PLAY_STREAM_FRONTEND()
		SET_BIT(iBitSet, iDoorOpenStream)
	ENDIF
	IF fRatio >= 1.0
		IF NOT IS_BIT_SET(iBitSet, iDoorFullyOpen)
			STOP_STREAM()
			PLAY_SOUND_FRONTEND(-1, "Door_Open_Limit", "DLC_GR_Bunker_Door_Sounds")
			CDEBUG1LN(DEBUG_SHOOTRANGE,"BUNKER_CUTSCENE_DOOR_MAINTAIN_AUDIO - bunker door open sound played!")
			SET_BIT(iBitSet, iDoorFullyOpen)
		ENDIF
	ENDIF
ENDPROC

ENUM BUNKER_CUTSCENE_DOOR_MOVE_RATIO
	BUNKER_CUTSCENE_DOOR_MOVE_OPEN,
	BUNKER_CUTSCENE_DOOR_MOVE_CLOSE
ENDENUM

PROC BUNKER_CUTSCENE_BUNKER_DOOR_MAINTAIN_RATIO(FLOAT &fCurrentRatio, BUNKER_CUTSCENE_DOOR_MOVE_RATIO eDesiredState)
	SWITCH eDesiredState
		CASE BUNKER_CUTSCENE_DOOR_MOVE_OPEN
			fCurrentRatio += BUNKER_CUTSCENE_DOOR_MOVE_SPEED * GET_FRAME_TIME()
		BREAK
		
		CASE BUNKER_CUTSCENE_DOOR_MOVE_CLOSE
			fCurrentRatio -= BUNKER_CUTSCENE_DOOR_MOVE_SPEED * GET_FRAME_TIME()
		BREAK
	ENDSWITCH
ENDPROC

CONST_FLOAT BC_EE_DISTANCE_HELI 38.0
CONST_FLOAT BC_EE_DISTANCE_ARMORY_TRUCK 35.000

ENUM BUNKER_CUTSCENE_ENTRY_ENTITY
	BC_EE_FOOT,
	BC_EE_VEHICLE,
	BC_EE_ARMORY_TRUCK,
	BC_EE_HELI
ENDENUM


FUNC VECTOR BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(SIMPLE_INTERIORS eInterior, BUNKER_CUTSCENE_ENTRY_ENTITY eEntity, FLOAT fOffsetManual = 0.0)
	TRANSFORM_STRUCT sEntityTransform
	TRANSFORM_STRUCT sBunkerTransform
	//Direction to entry ramp
	VECTOR vDirToPos
	sBunkerTransform = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eInterior)
	sEntityTransform = BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(eInterior, BUNKER_CA_VEHICLE_ENTRY_FAR)
	sEntityTransform.Position.z = sBunkerTransform.Position.z
	vDirToPos = NORMALISE_VECTOR(sEntityTransform.Position - sBunkerTransform.Position)
			
	

	IF fOffsetManual <> 0.0
		RETURN sBunkerTransform.Position + vDirToPos * fOffsetManual
	ENDIF
			
	SWITCH eEntity
		CASE BC_EE_ARMORY_TRUCK RETURN sBunkerTransform.Position + vDirToPos * BC_EE_DISTANCE_ARMORY_TRUCK
		CASE BC_EE_HELI RETURN sBunkerTransform.Position + vDirToPos * BC_EE_DISTANCE_HELI
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC TRANSFORM_STRUCT BUNKER_CUTSCENE_GET_ENTITY_TRANSFORM_FOR_ENTRY(SIMPLE_INTERIORS eInterior, BUNKER_CUTSCENE_ENTRY_ENTITY eEntity, FLOAT fOffsetManual = 0.0)
	TRANSFORM_STRUCT sEntityTransform
	TRANSFORM_STRUCT sBunkerTransform
	TRANSFORM_STRUCT sOutTransform
	//Direction to entry ramp
	VECTOR vDirToPos
	sBunkerTransform = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eInterior)
	sEntityTransform = BUNKER_CUTSCENE_GET_PROP_WORLD_COORD(eInterior, BUNKER_CA_VEHICLE_ENTRY_FAR)
	sEntityTransform.Position.z = sBunkerTransform.Position.z
	vDirToPos = NORMALISE_VECTOR(sEntityTransform.Position - sBunkerTransform.Position)

	sOutTransform.Rotation = sEntityTransform.Rotation
	

	IF fOffsetManual <> 0.0
		sOutTransform.Position = sBunkerTransform.Position + vDirToPos * fOffsetManual
		sOutTransform.Rotation.z = GET_HEADING_BETWEEN_VECTORS_2D(sOutTransform.Position, sBunkerTransform.Position)
		RETURN sOutTransform
	ENDIF
			
	sOutTransform.Position = BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, eEntity, fOffsetManual)	
	sOutTransform.Rotation.z = GET_HEADING_BETWEEN_VECTORS_2D(sOutTransform.Position, sBunkerTransform.Position)
	
	RETURN sOutTransform
ENDFUNC

PROC BUNKER_CUTSCENE_BUNKER_DOOR_OPEN_MAINTAIN(SIMPLE_INTERIORS eInterior, OBJECT_INDEX &BunkerDoor, FLOAT &doorOpenBias, INT &iBitSet, INT iBitDoorStream, INT iBitDoorOpen)
	IF NOT DOES_ENTITY_EXIST(BunkerDoor)
		CDEBUG1LN(DEBUG_SHOOTRANGE," BUNKER_CUTSCENE_BUNKER_DOOR_OPEN_MAINTAIN - can't manipulate the door, doesn't exist.")
		EXIT
	ENDIF
	

	BUNKER_CUTSCENE_BUNKER_DOOR_MAINTAIN_RATIO(doorOpenBias, BUNKER_CUTSCENE_DOOR_MOVE_OPEN)
	BUNKER_CUTSCENE_BUNKER_DOOR_SET_OPEN_RATIO(eInterior, BunkerDoor, doorOpenBias)
	BUNKER_CUTSCENE_DOOR_MAINTAIN_AUDIO(doorOpenBias, iBitSet, iBitDoorStream, iBitDoorOpen)
ENDPROC

//9.000 slow distance
FUNC FLOAT BUNKER_CUTSCENE_ARMORY_TRUCK_RAMP_START_OFFSET(SIMPLE_INTERIORS eInterior)
	SWITCH eInterior
		CASE SIMPLE_INTERIOR_BUNKER_1 RETURN 24.000
		CASE SIMPLE_INTERIOR_BUNKER_2 RETURN 33.000
		CASE SIMPLE_INTERIOR_BUNKER_3 RETURN 33.000
		CASE SIMPLE_INTERIOR_BUNKER_4 RETURN 31.000
		CASE SIMPLE_INTERIOR_BUNKER_5 RETURN 26.000
		CASE SIMPLE_INTERIOR_BUNKER_6 RETURN 27.000
		CASE SIMPLE_INTERIOR_BUNKER_7 RETURN 29.000
		CASE SIMPLE_INTERIOR_BUNKER_9 RETURN 32.000
		CASE SIMPLE_INTERIOR_BUNKER_10 RETURN 30.000
		CASE SIMPLE_INTERIOR_BUNKER_11 RETURN 31.000
		CASE SIMPLE_INTERIOR_BUNKER_12 RETURN 28.000
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

PROC BUNKER_CUTSCENE_ARMORY_TRUCK_MAINTAIN_RAMP(SIMPLE_INTERIORS eInterior, VEHICLE_INDEX & vehArmoryTruckCab)
	IF NOT DOES_ENTITY_EXIST(vehArmoryTruckCab)
		ASSERTLN("BUNKER_CUTSCENE_ARMORY_TRUCK_MAINTAIN_RAMP - Armory Truck does not exist.")
		EXIT
	ENDIF
	
	IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehArmoryTruckCab)
		FLOAT fTraveledDistance
		fTraveledDistance = GET_DISTANCE_BETWEEN_COORDS(BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK), GET_ENTITY_COORDS(vehArmoryTruckCab), FALSE)
		
		IF fTraveledDistance >= GET_DISTANCE_BETWEEN_COORDS(BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK), BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK, 14.000))
			//DRAW_DEBUG_SPHERE(BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK, 14.000), 3.0, 165, 137, 193, 128)
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(vehArmoryTruckCab, 6.0)
			EXIT
		ELSE
			VEHICLE_WAYPOINT_PLAYBACK_OVERRIDE_SPEED(vehArmoryTruckCab, 9.0)
		ENDIF
		
		//Ramp Start
		IF fTraveledDistance >= GET_DISTANCE_BETWEEN_COORDS(BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK), BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK, BUNKER_CUTSCENE_ARMORY_TRUCK_RAMP_START_OFFSET(eInterior)))
			IF VMAG(GET_ENTITY_VELOCITY(vehArmoryTruckCab)) < 1.0
				//DRAW_DEBUG_SPHERE(BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK, 14.000), 3.0, 252, 169, 133, 128)
				SET_VEHICLE_FORWARD_SPEED_XY(vehArmoryTruckCab, 1.0)
				#IF IS_DEBUG_BUILD
					IF GET_FRAME_COUNT() % 30 = 0
						PRINTLN("BUNKER_CUTSCENE - BUNKER_CUTSCENE_ARMORY_TRUCK_MAINTAIN_RAMP - Helping Truck move up, velocity: ", VMAG(GET_ENTITY_VELOCITY(vehArmoryTruckCab)))
					ENDIF
				#ENDIF
			ELSE
				//DRAW_DEBUG_SPHERE(BUNKER_CUTSCENE_GET_ENTITY_POSITION_FOR_ENTRY(eInterior, BC_EE_ARMORY_TRUCK, BUNKER_CUTSCENE_ARMORY_TRUCK_RAMP_START_OFFSET(eInterior)), 3.0, 133, 202, 93, 128)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_VEHICLE_TRANSFORM(VEHICLE_INDEX &entity, TRANSFORM_STRUCT sNewTransform)
	IF NOT DOES_ENTITY_EXIST(entity)
		ASSERTLN("SET_ENTITY_TRANSFORM - unable to set transform, entity does not exist.")
		EXIT
	ENDIF
	
	SET_ENTITY_COORDS(entity, sNewTransform.Position)
	SET_ENTITY_HEADING(entity, sNewTransform.Rotation.z)
ENDPROC
