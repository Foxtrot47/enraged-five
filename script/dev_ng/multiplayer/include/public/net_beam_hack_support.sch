USING "net_gangops_hacking_common.sch"

CONST_INT ciBH_MAX_LEVELS 14

CONST_INT ciBH_MAX_BEAM_SEGMENTS 30
CONST_INT ciBH_MAX_OUTER_WALLS 4
CONST_INT ciBH_MAX_NODES 20
CONST_INT ciBH_MAX_PACKETS 20
CONST_INT ciBH_MAX_INNER_WALL_RECTS 30
CONST_INT ciBH_MAX_FIREWALLS 10
CONST_INT ciBH_MAX_ROTATING_NODES 10
CONST_INT ciBH_MAX_PROXIES 5

CONST_INT ciBH_NODE_POINTS 2
CONST_INT ciBH_PACKET_POINTS 4
CONST_INT ciBH_INNER_WALL_POINTS 4
CONST_INT ciBH_FIREWALL_POINTS 4
CONST_INT ciBH_PROXY_POINTS 4

STRUCT BEAM_HACK_LEVEL_STRUCT
	VECTOR_2D vOuterWalls[ciBH_MAX_OUTER_WALLS]
	VECTOR_2D vNodes[ciBH_MAX_NODES][ciBH_NODE_POINTS]
	VECTOR_2D vPackets[ciBH_MAX_PACKETS][ciBH_PACKET_POINTS]
	VECTOR_2D vFirewalls[ciBH_MAX_FIREWALLS][ciBH_FIREWALL_POINTS]
	VECTOR_2D vInnerWalls[ciBH_MAX_INNER_WALL_RECTS][ciBH_INNER_WALL_POINTS]
	VECTOR_2D vProxies[ciBH_MAX_PROXIES][ciBH_PROXY_POINTS]
	INT iMaxNodes
	INT iMaxRotatingNodes
	INT iMaxPackets
	INT iMaxFirewalls
	INT iMaxInnerWalls
	INT iMaxProxies
ENDSTRUCT
BEAM_HACK_LEVEL_STRUCT sBeamLevel

VECTOR_2D vBeam[ciBH_MAX_BEAM_SEGMENTS]

#IF IS_DEBUG_BUILD
PROC ROTATE_BEAM_HACK_NODE_DEBUG(VECTOR_2D &v0, VECTOR_2D &v1, FLOAT fAngle)
	VECTOR_2D vCenter = DIVIDE_VECTOR_2D(ADD_VECTOR_2D(v0, v1), 2.0)
	v0 = ROTATE_VECTOR_2D_AROUND_VECTOR_2D(vCenter, v0, fAngle)
	v0.x = CLAMP(v0.x, vCenter.x - cfGRID_HALF_SIZE, vCenter.x + cfGRID_HALF_SIZE)
	v0.y = CLAMP(v0.y, vCenter.y - cfGRID_HALF_SIZE, vCenter.y + cfGRID_HALF_SIZE)
	v1 = ROTATE_VECTOR_2D_AROUND_VECTOR_2D(vCenter, v1, fAngle)
	v1.x = CLAMP(v1.x, vCenter.x - cfGRID_HALF_SIZE, vCenter.x + cfGRID_HALF_SIZE)
	v1.y = CLAMP(v1.y, vCenter.y - cfGRID_HALF_SIZE, vCenter.y + cfGRID_HALF_SIZE)
ENDPROC
#ENDIF

FUNC INT GET_BEAMHACK_TEXTURE_INDEX_FROM_LEVEL_INDEX(INT iLevel)
	SWITCH iLevel
		CASE 0 RETURN 0
		CASE 1 RETURN 1
		CASE 2 RETURN 2
		CASE 3 RETURN 3
		CASE 4 RETURN 12
		CASE 5 RETURN 5
		CASE 6 RETURN 6
		CASE 7 RETURN 7
		CASE 8 RETURN 8
		CASE 9 RETURN 9
		CASE 10 RETURN 10
		CASE 11 RETURN 11
		CASE 12 RETURN 13
		CASE 13 RETURN 14
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC TEXT_LABEL_23 GET_BEAMHACK_TEXTURE_DICT_FOR_LEVEL(INT iLevel)
	TEXT_LABEL_23 tl23Dict = "MPBeamHack_lvl"
	tl23Dict += GET_BEAMHACK_TEXTURE_INDEX_FROM_LEVEL_INDEX(iLevel)
	RETURN tl23Dict
ENDFUNC

FUNC TEXT_LABEL_23 GET_BEAMHACK_TEXTURE_BG_FOR_LEVEL(INT iLevel)
	TEXT_LABEL_23 tl23Tx = "lvl"
	tl23Tx += GET_BEAMHACK_TEXTURE_INDEX_FROM_LEVEL_INDEX(iLevel)
	tl23Tx += "_bg"
	RETURN tl23Tx
ENDFUNC

FUNC TEXT_LABEL_23 GET_BEAMHACK_TEXTURE_FG_FOR_LEVEL(INT iLevel)
	TEXT_LABEL_23 tl23Tx = "lvl"
	tl23Tx += GET_BEAMHACK_TEXTURE_INDEX_FROM_LEVEL_INDEX(iLevel)
	RETURN tl23Tx
ENDFUNC
		
/// PURPOSE:
///    Clears the level struct without making a copy to avoid big stack spikes.
PROC CLEAR_LEVEL_DATA()
	
	PRINTLN("####################################[JS][BeamHack] CLEAR_LEVEL_DATA ###########################################")
	
	//Maxes
	sBeamLevel.iMaxNodes = 0
	sBeamLevel.iMaxRotatingNodes = 0
	sBeamLevel.iMaxPackets = 0
	sBeamLevel.iMaxFirewalls = 0
	sBeamLevel.iMaxInnerWalls = 0
	sBeamLevel.iMaxProxies = 0
	
	INT i, j
	
	VECTOR_2D vZero
	
	//Outer Walls
	FOR i = 0 TO ciBH_MAX_OUTER_WALLS - 1
		sBeamLevel.vOuterWalls[i] = vZero
	ENDFOR
	
	//Inner Walls
	FOR i = 0 TO ciBH_MAX_INNER_WALL_RECTS - 1
		FOR j = 0 TO ciBH_INNER_WALL_POINTS - 1
			sBeamLevel.vInnerWalls[i][j] = vZero
		ENDFOR
	ENDFOR
	
	//Nodes
	FOR i = 0 TO ciBH_MAX_NODES - 1
		FOR j = 0 TO ciBH_NODE_POINTS - 1
			sBeamLevel.vNodes[i][j] = vZero
		ENDFOR
	ENDFOR
	
	//Packets
	FOR i = 0 TO ciBH_MAX_PACKETS - 1
		FOR j = 0 TO ciBH_PACKET_POINTS - 1
			sBeamLevel.vPackets[i][j] = vZero
		ENDFOR
	ENDFOR
	
	//Firewalls
	FOR i = 0 TO ciBH_MAX_FIREWALLS - 1
		FOR j = 0 TO ciBH_FIREWALL_POINTS - 1
			sBeamLevel.vFirewalls[i][j] = vZero
		ENDFOR
	ENDFOR
	
	//Proxies
	FOR i = 0 TO ciBH_MAX_PROXIES - 1
		FOR j = 0 TO ciBH_PROXY_POINTS - 1
			sBeamLevel.vProxies[i][j] = vZero
		ENDFOR
	ENDFOR

ENDPROC
		

PROC POPULATE_LEVEL_DATA(INT iLevel)
	
	CLEAR_LEVEL_DATA()
	
	SWITCH(iLevel)
		CASE 0
			//Maxes
			sBeamLevel.iMaxNodes = 11
			sBeamLevel.iMaxRotatingNodes = 5
			sBeamLevel.iMaxPackets = 9
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 13
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.725000, 0.732500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.987499, 0.732500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(1.100000, 0.845000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.650000, 0.845000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.650000, 0.770000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.950000, 0.770000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.950000, 0.845000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.500000, 0.770000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.500000, 0.695000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.725000, 0.695000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.725000, 0.770000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.875000, 0.695000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.875000, 0.245000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.950000, 0.245000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.950000, 0.695000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.425000, 0.320000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.500000, 0.170000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.500000, 0.320000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.575000, 0.320000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.575000, 0.245000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.875000, 0.245000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.875000, 0.320000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.725000, 0.395000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.725000, 0.620000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.800000, 0.695000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.800000, 0.320000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.875000, 0.320000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.875000, 0.695000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(-0.100000, 0.545000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(-0.100000, 0.395000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(-0.025000, 0.545000)

			sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(0.350000, 0.770000)
			sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(0.350000, 0.395000)
			sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(0.500000, 0.395000)
			sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(0.500000, 0.770000)

			sBeamLevel.vInnerWalls[10][0] = INIT_VECTOR_2D(0.200000, 0.545000)
			sBeamLevel.vInnerWalls[10][1] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vInnerWalls[10][2] = INIT_VECTOR_2D(0.350000, 0.470000)
			sBeamLevel.vInnerWalls[10][3] = INIT_VECTOR_2D(0.350000, 0.545000)

			sBeamLevel.vInnerWalls[11][0] = INIT_VECTOR_2D(0.125000, 0.620000)
			sBeamLevel.vInnerWalls[11][1] = INIT_VECTOR_2D(0.125000, 0.395000)
			sBeamLevel.vInnerWalls[11][2] = INIT_VECTOR_2D(0.200000, 0.395000)
			sBeamLevel.vInnerWalls[11][3] = INIT_VECTOR_2D(0.200000, 0.620000)

			sBeamLevel.vInnerWalls[12][0] = INIT_VECTOR_2D(0.125000, 0.245000)
			sBeamLevel.vInnerWalls[12][1] = INIT_VECTOR_2D(0.125000, 0.170000)
			sBeamLevel.vInnerWalls[12][2] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[12][3] = INIT_VECTOR_2D(0.425000, 0.245000)

			sBeamLevel.vInnerWalls[13][0] = INIT_VECTOR_2D(0.350000, 0.320000)
			sBeamLevel.vInnerWalls[13][1] = INIT_VECTOR_2D(0.350000, 0.245000)
			sBeamLevel.vInnerWalls[13][2] = INIT_VECTOR_2D(0.425000, 0.245000)
			sBeamLevel.vInnerWalls[13][3] = INIT_VECTOR_2D(0.425000, 0.320000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.960983, 0.705984)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(1.014017, 0.759017)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.510984, 0.180983)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.564016, 0.234018)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.762500, 0.320000)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.762500, 0.395000)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.414017, 0.384017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.360983, 0.330984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.060983, 0.834017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.114017, 0.780984)


			//Static
			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.950000, 0.170000)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(1.025000, 0.245000)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.575000, 0.695000)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.500000, 0.620000)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.725000, 0.695000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.800000, 0.620000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(-0.100000, 0.245000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.050000, 0.170000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.125000, 0.245000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(-0.100000, 0.320000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.960983, 0.309017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.960983, 0.255983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(1.014017, 0.255983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(1.014017, 0.309017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.660983, 0.234017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.660983, 0.180983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.714017, 0.180983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.714017, 0.234017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.735983, 0.459017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.735983, 0.405983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.789017, 0.405983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.789017, 0.459017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.623483, 0.684017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.623483, 0.630983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.676517, 0.630983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.676517, 0.684017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.510983, 0.459017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.510983, 0.405983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.564017, 0.405983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.564017, 0.459017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.285983, 0.459017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.285983, 0.405983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.339017, 0.405983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.339017, 0.459017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.585983, 0.834017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.585983, 0.780983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.639017, 0.780983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.639017, 0.834017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.210983, 0.459017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.210983, 0.405983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.264017, 0.405983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.264017, 0.459017)

			sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(-0.089017, 0.609017)
			sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(-0.089017, 0.555983)
			sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(-0.035983, 0.555983)
			sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(-0.035983, 0.609017)


			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.285983, 0.309017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.285983, 0.255983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.339017, 0.255983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.339017, 0.309017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.210983, 0.309017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.210983, 0.255983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.264017, 0.255983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.264017, 0.309017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.135983, 0.684017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.135983, 0.630983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.189017, 0.630983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.189017, 0.684017)
		BREAK
		CASE 1
			//Maxes
			sBeamLevel.iMaxNodes = 6
			sBeamLevel.iMaxRotatingNodes = 6
			sBeamLevel.iMaxPackets = 5
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 7
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.875000, 0.282500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.012501, 0.282500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(1.025000, 0.545000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(1.100000, 0.545000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.875000, 0.470000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.875000, 0.170000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.950000, 0.170000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.950000, 0.470000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.650000, 0.845000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.650000, 0.695000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.725000, 0.695000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.725000, 0.845000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(-0.100000, 0.545000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(-0.100000, 0.470000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(-0.025000, 0.470000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(-0.025000, 0.545000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.125000, 0.620000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.125000, 0.395000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.200000, 0.395000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.200000, 0.620000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.425000, 0.245000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.500000, 0.170000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.500000, 0.245000)
			
			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.575000, 0.845000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.575000, 0.770000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.650000, 0.770000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.650000, 0.845000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(-0.014017, 0.309017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.039017, 0.255984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(-0.014017, 0.834017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.039017, 0.780984)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(1.014017, 0.834017)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.960983, 0.780984)
			
			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.513383, 0.836217)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.561617, 0.778784)
 
			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.537502, 0.470001)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.537498, 0.395000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(-0.089017, 0.609017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(-0.089017, 0.555983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(-0.035983, 0.555983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(-0.035983, 0.609017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.510983, 0.234017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.510983, 0.180983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.564017, 0.180983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.564017, 0.234017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.810983, 0.384017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.810983, 0.330983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.864017, 0.330983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.864017, 0.384017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.735983, 0.834017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.735983, 0.780983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.789017, 0.780983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.789017, 0.834017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.960983, 0.234017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.960983, 0.180983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(1.014017, 0.180983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(1.014017, 0.234017)


			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.089017, 0.459017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.089017, 0.405983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(-0.035983, 0.405983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(-0.035983, 0.459017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.135983, 0.234017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.135983, 0.180983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.189017, 0.180983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.189017, 0.234017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.135983, 0.684017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.135983, 0.630983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.189017, 0.630983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.189017, 0.684017)
		BREAK
		CASE 2
			//Maxes
			sBeamLevel.iMaxNodes = 12
			sBeamLevel.iMaxRotatingNodes = 9
			sBeamLevel.iMaxPackets = 8
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 4
			sBeamLevel.iMaxProxies = 1
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.350000, 0.507500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.612500, 0.507500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.275000, 0.770000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.275000, 0.245000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.350000, 0.245000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.350000, 0.770000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.575000, 0.695000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.725000, 0.620000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.725000, 0.695000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.650000, 0.620000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.650000, 0.395000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.725000, 0.395000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.725000, 0.620000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.575000, 0.320000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.725000, 0.320000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.725000, 0.395000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.584852, 0.532835)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.640148, 0.482166)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(-0.089017, 0.234017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(-0.035983, 0.180984)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.359852, 0.307835)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.415148, 0.257166)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.364671, 0.702749)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.410329, 0.762251)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(-0.062500, 0.695001)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(-0.062500, 0.620000)
			
			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.762500, 0.545001)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.762501, 0.470000)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(1.035983, 0.534017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(1.089017, 0.480984)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.888396, 0.253774)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.936604, 0.311227)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.885983, 0.759017)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.939017, 0.705984)


			//Static
			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(-0.025000, 0.845000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(-0.100000, 0.770000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(1.100000, 0.770000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(1.100000, 0.245000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.810983, 0.609017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.810983, 0.555983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.864017, 0.555983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.864017, 0.609017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.210983, 0.309017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.210983, 0.255983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.264017, 0.255983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.264017, 0.309017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.510983, 0.834017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.510983, 0.780983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.564017, 0.780983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.564017, 0.834017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.510983, 0.234017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.510983, 0.180983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.564017, 0.180983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.564017, 0.234017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.210983, 0.684017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.210983, 0.630983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.264017, 0.630983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.264017, 0.684017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.810983, 0.459017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.810983, 0.405983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.864017, 0.405983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.864017, 0.459017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(1.035983, 0.759017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(1.035983, 0.705983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(1.089017, 0.705983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(1.089017, 0.759017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(1.035983, 0.309017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(1.035983, 0.255983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(1.089017, 0.255983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(1.089017, 0.309017)


			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.089017, 0.534017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.089017, 0.480983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(-0.035983, 0.480983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(-0.035983, 0.534017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.960983, 0.309017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.960983, 0.255983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(1.014017, 0.255983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(1.014017, 0.309017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.960983, 0.759017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.960983, 0.705983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(1.014017, 0.705983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(1.014017, 0.759017)


			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(0.135983, 0.534017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(0.135983, 0.480984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(0.189017, 0.480984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(0.189017, 0.534017)
		BREAK		
	ENDSWITCH
	SWITCH(iLevel)
		CASE 3
			//Maxes
			sBeamLevel.iMaxNodes = 14
			sBeamLevel.iMaxRotatingNodes = 9
			sBeamLevel.iMaxPackets = 9
			sBeamLevel.iMaxFirewalls = 7
			sBeamLevel.iMaxInnerWalls = 11
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(-0.100000, 0.207500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.312500, 0.207500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.100000, 0.320000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.100000, 0.245000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.125000, 0.245000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.125000, 0.320000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.125000, 0.695000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.125000, 0.245000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.200000, 0.245000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.200000, 0.695000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.200000, 0.395000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.350000, 0.395000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.350000, 0.470000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.350000, 0.470000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.350000, 0.245000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.425000, 0.245000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.425000, 0.470000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.425000, 0.470000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.425000, 0.245000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.500000, 0.245000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.500000, 0.470000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.500000, 0.470000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.500000, 0.395000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.725000, 0.395000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.725000, 0.470000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.725000, 0.470000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.725000, 0.245000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.800000, 0.245000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.800000, 0.470000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.800000, 0.320000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.800000, 0.245000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(1.025000, 0.245000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(1.025000, 0.320000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(0.500000, 0.620000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(0.500000, 0.545000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(0.875000, 0.545000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(0.875000, 0.620000)

			sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(0.500000, 0.770000)
			sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(0.500000, 0.620000)
			sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(0.575000, 0.770000)

			sBeamLevel.vInnerWalls[10][0] = INIT_VECTOR_2D(0.800000, 0.770000)
			sBeamLevel.vInnerWalls[10][1] = INIT_VECTOR_2D(0.800000, 0.620000)
			sBeamLevel.vInnerWalls[10][2] = INIT_VECTOR_2D(0.875000, 0.620000)
			sBeamLevel.vInnerWalls[10][3] = INIT_VECTOR_2D(0.875000, 0.770000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.319012, 0.244431)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.305988, 0.170570)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.719976, 0.226251)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.655024, 0.188750)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.612500, 0.845000)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.612500, 0.770001)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(-0.062500, 0.845001)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(-0.062500, 0.770000)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(-0.062500, 0.545001)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(-0.062500, 0.470000)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.285983, 0.534017)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.339017, 0.480984)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.435983, 0.759017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.489017, 0.705984)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.810983, 0.534017)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.864017, 0.480984)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.953513, 0.716652)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(1.021487, 0.748349)

			//Static
			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.500000, 0.470000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.425000, 0.545000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.875000, 0.320000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.800000, 0.395000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(0.950000, 0.320000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(1.025000, 0.395000)

			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(1.100000, 0.245000)

			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(1.100000, 0.770000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.210983, 0.384017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.210983, 0.330983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.264017, 0.330983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.264017, 0.384017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.510983, 0.384017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.510983, 0.330983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.564017, 0.330983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.564017, 0.384017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.885983, 0.384017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.885983, 0.330983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.939017, 0.330983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.939017, 0.384017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.585983, 0.684017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.585983, 0.630983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.639017, 0.630983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.639017, 0.684017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.735983, 0.684017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.735983, 0.630983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.789017, 0.630983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.789017, 0.684017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.060983, 0.384017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.060983, 0.330983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.114017, 0.330983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.114017, 0.384017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.210983, 0.684017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.210983, 0.630983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.264017, 0.630983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.264017, 0.684017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.885983, 0.609017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.885983, 0.555983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.939017, 0.555983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.939017, 0.609017)

			sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(0.885983, 0.759017)
			sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(0.885983, 0.705983)
			sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(0.939017, 0.705983)
			sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(0.939017, 0.759017)


			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.014017, 0.384017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.014017, 0.330983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.039017, 0.330983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.039017, 0.384017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.060983, 0.684017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.060983, 0.630983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.114017, 0.630983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.114017, 0.684017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(-0.014017, 0.684017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(-0.014017, 0.630983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.039017, 0.630983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.039017, 0.684017)

			sBeamLevel.vFirewalls[3][0] = INIT_VECTOR_2D(0.285983, 0.684017)
			sBeamLevel.vFirewalls[3][1] = INIT_VECTOR_2D(0.285983, 0.630983)
			sBeamLevel.vFirewalls[3][2] = INIT_VECTOR_2D(0.339017, 0.630983)
			sBeamLevel.vFirewalls[3][3] = INIT_VECTOR_2D(0.339017, 0.684017)

			sBeamLevel.vFirewalls[4][0] = INIT_VECTOR_2D(0.660983, 0.684017)
			sBeamLevel.vFirewalls[4][1] = INIT_VECTOR_2D(0.660983, 0.630983)
			sBeamLevel.vFirewalls[4][2] = INIT_VECTOR_2D(0.714017, 0.630983)
			sBeamLevel.vFirewalls[4][3] = INIT_VECTOR_2D(0.714017, 0.684017)

			sBeamLevel.vFirewalls[5][0] = INIT_VECTOR_2D(0.885983, 0.534017)
			sBeamLevel.vFirewalls[5][1] = INIT_VECTOR_2D(0.885983, 0.480983)
			sBeamLevel.vFirewalls[5][2] = INIT_VECTOR_2D(0.939017, 0.480983)
			sBeamLevel.vFirewalls[5][3] = INIT_VECTOR_2D(0.939017, 0.534017)

			sBeamLevel.vFirewalls[6][0] = INIT_VECTOR_2D(0.660983, 0.384017)
			sBeamLevel.vFirewalls[6][1] = INIT_VECTOR_2D(0.660983, 0.330983)
			sBeamLevel.vFirewalls[6][2] = INIT_VECTOR_2D(0.714017, 0.330983)
			sBeamLevel.vFirewalls[6][3] = INIT_VECTOR_2D(0.714017, 0.384017)
		BREAK
		
		CASE 4
			//Maxes
			sBeamLevel.iMaxNodes = 9
			sBeamLevel.iMaxRotatingNodes = 9
			sBeamLevel.iMaxPackets = 8
			sBeamLevel.iMaxFirewalls = 6
			sBeamLevel.iMaxInnerWalls = 8
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(-0.100000, 0.807500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(-0.025000, 0.807500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.100000, 0.545000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.100000, 0.470000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.350000, 0.470000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.350000, 0.545000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.275000, 0.620000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.275000, 0.545000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.350000, 0.545000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.350000, 0.620000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(-0.025000, 0.245000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.125000, 0.245000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.125000, 0.395000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.425000, 0.845000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.425000, 0.395000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.500000, 0.395000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.500000, 0.845000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.725000, 0.545000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.725000, 0.320000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.800000, 0.320000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.800000, 0.545000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.125000, 0.395000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.125000, 0.320000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.200000, 0.320000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.200000, 0.395000)
			
			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.500000, 0.845000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.500000, 0.770000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(1.100000, 0.770000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(1.100000, 0.845000)
			
			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.725000, 0.620000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.725000, 0.545000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.875000, 0.545000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.875000, 0.620000)
			
			//Nodes
			//Rotatable
			
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.135983, 0.834017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.189017, 0.780984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.060983, 0.609017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.114017, 0.555984)
			
			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.414017, 0.780984)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.360983, 0.834017)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(-0.089017, 0.459017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(-0.035983, 0.405984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(-0.089017, 0.234017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(-0.035983, 0.180984)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.414033, 0.181001)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.360967, 0.234000)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.510983, 0.459017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.564017, 0.405984)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.735983, 0.234017)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.789017, 0.180984)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(1.035983, 0.759017)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(1.089017, 0.705984)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.360983, 0.609017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.360983, 0.555983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.414017, 0.555983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.414017, 0.609017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.135983, 0.234017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.135983, 0.180983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.189017, 0.180983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.189017, 0.234017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.510983, 0.759017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.510983, 0.705983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.564017, 0.705983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.564017, 0.759017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.735983, 0.684017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.735983, 0.630983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.789017, 0.630983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.789017, 0.684017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.960983, 0.384017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.960983, 0.330983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(1.014017, 0.330983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(1.014017, 0.384017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.023483, 0.459017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.023483, 0.405983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.076517, 0.405983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.076517, 0.459017)
			
			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(-0.089017, 0.609017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(-0.089017, 0.555983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(-0.035983, 0.555983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(-0.035983, 0.609017)
			
			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.885983, 0.609017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.885983, 0.555983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.939017, 0.555983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.939017, 0.609017)
			
			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.014017, 0.684017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.014017, 0.630983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.039017, 0.630983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.039017, 0.684017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.135983, 0.309017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.135983, 0.255983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.189017, 0.255983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.189017, 0.309017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.510983, 0.684017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.510983, 0.630983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.564017, 0.630983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.564017, 0.684017)

			sBeamLevel.vFirewalls[3][0] = INIT_VECTOR_2D(0.810983, 0.684017)
			sBeamLevel.vFirewalls[3][1] = INIT_VECTOR_2D(0.810983, 0.630983)
			sBeamLevel.vFirewalls[3][2] = INIT_VECTOR_2D(0.864017, 0.630983)
			sBeamLevel.vFirewalls[3][3] = INIT_VECTOR_2D(0.864017, 0.684017)

			sBeamLevel.vFirewalls[4][0] = INIT_VECTOR_2D(0.435983, 0.384017)
			sBeamLevel.vFirewalls[4][1] = INIT_VECTOR_2D(0.435983, 0.330983)
			sBeamLevel.vFirewalls[4][2] = INIT_VECTOR_2D(0.489017, 0.330983)
			sBeamLevel.vFirewalls[4][3] = INIT_VECTOR_2D(0.489017, 0.384017)

			sBeamLevel.vFirewalls[5][0] = INIT_VECTOR_2D(1.035983, 0.234017)
			sBeamLevel.vFirewalls[5][1] = INIT_VECTOR_2D(1.035983, 0.180983)
			sBeamLevel.vFirewalls[5][2] = INIT_VECTOR_2D(1.089017, 0.180983)
			sBeamLevel.vFirewalls[5][3] = INIT_VECTOR_2D(1.089017, 0.234017)
		BREAK
		CASE 5
			//Maxes
			sBeamLevel.iMaxNodes = 15
			sBeamLevel.iMaxRotatingNodes = 5
			sBeamLevel.iMaxPackets = 9
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 16
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.725000, 0.732500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.987499, 0.732500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(1.100000, 0.845000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.650000, 0.845000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.650000, 0.770000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.950000, 0.770000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.950000, 0.845000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.425000, 0.770000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.425000, 0.695000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.725000, 0.695000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.725000, 0.770000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.875000, 0.695000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.875000, 0.245000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.950000, 0.245000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.950000, 0.695000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.425000, 0.470000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.425000, 0.395000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.500000, 0.395000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.500000, 0.470000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.425000, 0.320000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.500000, 0.170000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.500000, 0.320000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.575000, 0.320000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.575000, 0.245000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.875000, 0.245000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.875000, 0.320000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.575000, 0.470000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.725000, 0.395000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.725000, 0.470000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(0.800000, 0.470000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(0.800000, 0.395000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(0.875000, 0.395000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(0.875000, 0.470000)

			sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(0.800000, 0.695000)
			sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(0.800000, 0.620000)
			sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(0.875000, 0.620000)
			sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(0.875000, 0.695000)

			sBeamLevel.vInnerWalls[10][0] = INIT_VECTOR_2D(-0.100000, 0.545000)
			sBeamLevel.vInnerWalls[10][1] = INIT_VECTOR_2D(-0.100000, 0.395000)
			sBeamLevel.vInnerWalls[10][2] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vInnerWalls[10][3] = INIT_VECTOR_2D(-0.025000, 0.545000)

			sBeamLevel.vInnerWalls[11][0] = INIT_VECTOR_2D(0.350000, 0.770000)
			sBeamLevel.vInnerWalls[11][1] = INIT_VECTOR_2D(0.350000, 0.395000)
			sBeamLevel.vInnerWalls[11][2] = INIT_VECTOR_2D(0.425000, 0.395000)
			sBeamLevel.vInnerWalls[11][3] = INIT_VECTOR_2D(0.425000, 0.770000)

			sBeamLevel.vInnerWalls[12][0] = INIT_VECTOR_2D(0.200000, 0.545000)
			sBeamLevel.vInnerWalls[12][1] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vInnerWalls[12][2] = INIT_VECTOR_2D(0.350000, 0.470000)
			sBeamLevel.vInnerWalls[12][3] = INIT_VECTOR_2D(0.350000, 0.545000)

			sBeamLevel.vInnerWalls[13][0] = INIT_VECTOR_2D(0.125000, 0.620000)
			sBeamLevel.vInnerWalls[13][1] = INIT_VECTOR_2D(0.125000, 0.395000)
			sBeamLevel.vInnerWalls[13][2] = INIT_VECTOR_2D(0.200000, 0.395000)
			sBeamLevel.vInnerWalls[13][3] = INIT_VECTOR_2D(0.200000, 0.620000)

			sBeamLevel.vInnerWalls[14][0] = INIT_VECTOR_2D(0.125000, 0.245000)
			sBeamLevel.vInnerWalls[14][1] = INIT_VECTOR_2D(0.125000, 0.170000)
			sBeamLevel.vInnerWalls[14][2] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[14][3] = INIT_VECTOR_2D(0.425000, 0.245000)

			sBeamLevel.vInnerWalls[15][0] = INIT_VECTOR_2D(0.350000, 0.320000)
			sBeamLevel.vInnerWalls[15][1] = INIT_VECTOR_2D(0.350000, 0.245000)
			sBeamLevel.vInnerWalls[15][2] = INIT_VECTOR_2D(0.425000, 0.245000)
			sBeamLevel.vInnerWalls[15][3] = INIT_VECTOR_2D(0.425000, 0.320000)

			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(1.014017, 0.759017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.960983, 0.705984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.510983, 0.180984)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.564017, 0.234017)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.762500, 0.395000)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.762500, 0.320000)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.414017, 0.384017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.360983, 0.330984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.060983, 0.834017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.114017, 0.780984)

			//Static
			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.950000, 0.170000)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(1.025000, 0.245000)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.500000, 0.545000)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.500000, 0.695000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.425000, 0.620000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.725000, 0.695000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.800000, 0.620000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.800000, 0.620000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.875000, 0.545000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.800000, 0.470000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.875000, 0.545000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(0.500000, 0.470000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(0.425000, 0.545000)

			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(-0.100000, 0.245000)

			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(0.050000, 0.170000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(0.125000, 0.245000)

			sBeamLevel.vNodes[14][0] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vNodes[14][1] = INIT_VECTOR_2D(-0.100000, 0.320000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.960983, 0.309017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.960983, 0.255983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(1.014017, 0.255983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(1.014017, 0.309017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.660983, 0.234017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.660983, 0.180983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.714017, 0.180983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.714017, 0.234017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.735983, 0.609017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.735983, 0.555983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.789017, 0.555983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.789017, 0.609017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.585983, 0.684017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.585983, 0.630983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.639017, 0.630983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.639017, 0.684017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.585983, 0.534017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.585983, 0.480983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.639017, 0.480983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.639017, 0.534017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.285983, 0.459017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.285983, 0.405983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.339017, 0.405983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.339017, 0.459017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.585983, 0.834017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.585983, 0.780983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.639017, 0.780983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.639017, 0.834017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.210983, 0.459017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.210983, 0.405983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.264017, 0.405983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.264017, 0.459017)

			sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(-0.089017, 0.609017)
			sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(-0.089017, 0.555983)
			sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(-0.035983, 0.555983)
			sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(-0.035983, 0.609017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.285983, 0.309017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.285983, 0.255983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.339017, 0.255983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.339017, 0.309017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.210983, 0.309017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.210983, 0.255983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.264017, 0.255983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.264017, 0.309017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.135983, 0.684017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.135983, 0.630983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.189017, 0.630983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.189017, 0.684017)
		BREAK

		CASE 6
			//Maxes
			sBeamLevel.iMaxNodes = 12
			sBeamLevel.iMaxRotatingNodes = 4
			sBeamLevel.iMaxPackets = 8
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 6
			sBeamLevel.iMaxProxies = 1
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.425000, 0.207500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.687500, 0.207500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.725000, 0.620000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.725000, 0.170000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.800000, 0.170000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.800000, 0.620000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.575000, 0.545000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.725000, 0.545000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.725000, 0.620000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.800000, 0.320000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.800000, 0.245000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(1.025000, 0.245000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(1.025000, 0.320000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.350000, 0.695000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.425000, 0.695000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.200000, 0.695000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.200000, 0.620000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.350000, 0.620000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.350000, 0.695000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(-0.100000, 0.695000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(-0.100000, 0.620000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.050000, 0.620000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.050000, 0.695000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.694011, 0.244431)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.680989, 0.170570)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.853093, 0.398396)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.821907, 0.466605)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.113945, 0.780913)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.061055, 0.834088)
			
			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(1.036055, 0.834088)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(1.088945, 0.780913)

			//Static
			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.650000, 0.545000)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.725000, 0.470000)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.500000, 0.470000)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.425000, 0.545000)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.500000, 0.695000)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.425000, 0.620000)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.800000, 0.695000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.875000, 0.620000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(1.025000, 0.470000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(1.100000, 0.395000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(1.100000, 0.245000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.275000, 0.470000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.350000, 0.395000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(0.050000, 0.470000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(-0.025000, 0.395000)

			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(1.100000, 0.770000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(-0.014017, 0.609017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(-0.014017, 0.555983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.039017, 0.555983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.039017, 0.609017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(-0.089017, 0.234017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(-0.089017, 0.180983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(-0.035983, 0.180983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(-0.035983, 0.234017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.285983, 0.609017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.285983, 0.555983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.339017, 0.555983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.339017, 0.609017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.135983, 0.234017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.135983, 0.180983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.189017, 0.180983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.189017, 0.234017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.435983, 0.384017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.435983, 0.330983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.489017, 0.330983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.489017, 0.384017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.660983, 0.459017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.660983, 0.405983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.714017, 0.405983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.714017, 0.459017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(1.035983, 0.684017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(1.035983, 0.630983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(1.089017, 0.630983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(1.089017, 0.684017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.885983, 0.234017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.885983, 0.180983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.939017, 0.180983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.939017, 0.234017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.810983, 0.234017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.810983, 0.180983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.864017, 0.180983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.864017, 0.234017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.285983, 0.309017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.285983, 0.255983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.339017, 0.255983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.339017, 0.309017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.435983, 0.459017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.435983, 0.405983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.489017, 0.405983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.489017, 0.459017)

			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(0.135983, 0.459017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(0.135983, 0.405984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(0.189017, 0.405984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(0.189017, 0.459017)
		BREAK
		
		CASE 7
			//Maxes
			sBeamLevel.iMaxNodes = 16
			sBeamLevel.iMaxRotatingNodes = 8
			sBeamLevel.iMaxPackets = 6
			sBeamLevel.iMaxFirewalls = 5
			sBeamLevel.iMaxInnerWalls = 10
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(-0.100000, 0.207500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(-0.025000, 0.207500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.025000, 0.470000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.125000, 0.395000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.125000, 0.470000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(-0.100000, 0.620000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(-0.100000, 0.545000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.125000, 0.545000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.125000, 0.620000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.200000, 0.620000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.200000, 0.545000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.425000, 0.545000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.425000, 0.620000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.200000, 0.245000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.275000, 0.245000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.275000, 0.470000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.350000, 0.545000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.425000, 0.545000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.350000, 0.845000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.350000, 0.770000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.575000, 0.770000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.575000, 0.845000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.425000, 0.545000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.425000, 0.470000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.500000, 0.470000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.500000, 0.545000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.650000, 0.695000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.650000, 0.320000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.725000, 0.320000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.725000, 0.695000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(0.800000, 0.695000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(0.800000, 0.620000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(0.950000, 0.620000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(0.950000, 0.695000)
			
			sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(0.575000, 0.695000)
			sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(0.575000, 0.545000)
			sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(0.650000, 0.545000)
			sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(0.650000, 0.695000)
			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(-0.035983, 0.480984)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(-0.089017, 0.534017)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.435983, 0.630984)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.489017, 0.684017)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(1.099990, 0.283375)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(1.025010, 0.283375)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(1.035983, 0.759017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(1.089017, 0.705984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.060983, 0.759017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.114017, 0.705984)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.486722, 0.253873)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.438278, 0.311128)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.575173, 0.436107)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.649827, 0.436107)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.790780, 0.182874)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.734220, 0.232127)
			
			//Static
			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(-0.025000, 0.245000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(-0.100000, 0.320000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.125000, 0.245000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.200000, 0.320000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.050000, 0.620000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.125000, 0.695000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(-0.025000, 0.845000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(-0.100000, 0.770000)

			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(0.125000, 0.845000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(0.200000, 0.770000)

			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(0.275000, 0.545000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(0.350000, 0.470000)

			sBeamLevel.vNodes[14][0] = INIT_VECTOR_2D(-0.025000, 0.620000)
			sBeamLevel.vNodes[14][1] = INIT_VECTOR_2D(-0.100000, 0.695000)

			sBeamLevel.vNodes[15][0] = INIT_VECTOR_2D(0.275000, 0.170000)
			sBeamLevel.vNodes[15][1] = INIT_VECTOR_2D(0.350000, 0.245000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.023483, 0.309017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.023483, 0.255983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.076517, 0.255983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.076517, 0.309017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.435983, 0.459017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.435983, 0.405983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.489017, 0.405983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.489017, 0.459017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(1.035983, 0.459017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(1.035983, 0.405983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(1.089017, 0.405983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(1.089017, 0.459017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.735983, 0.609017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.735983, 0.555983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.789017, 0.555983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.789017, 0.609017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.735983, 0.834017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.735983, 0.780983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.789017, 0.780983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.789017, 0.834017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.585983, 0.834017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.585983, 0.780983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.639017, 0.780983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.639017, 0.834017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.285983, 0.684017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.285983, 0.630983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.339017, 0.630983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.339017, 0.684017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.285983, 0.834017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.285983, 0.780983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.339017, 0.780983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.339017, 0.834017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.585983, 0.384017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.585983, 0.330983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.639017, 0.330983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.639017, 0.384017)

			sBeamLevel.vFirewalls[3][0] = INIT_VECTOR_2D(0.660983, 0.834017)
			sBeamLevel.vFirewalls[3][1] = INIT_VECTOR_2D(0.660983, 0.780983)
			sBeamLevel.vFirewalls[3][2] = INIT_VECTOR_2D(0.714017, 0.780983)
			sBeamLevel.vFirewalls[3][3] = INIT_VECTOR_2D(0.714017, 0.834017)

			sBeamLevel.vFirewalls[4][0] = INIT_VECTOR_2D(0.960983, 0.684017)
			sBeamLevel.vFirewalls[4][1] = INIT_VECTOR_2D(0.960983, 0.630983)
			sBeamLevel.vFirewalls[4][2] = INIT_VECTOR_2D(1.014017, 0.630983)
			sBeamLevel.vFirewalls[4][3] = INIT_VECTOR_2D(1.014017, 0.684017)
		BREAK
		CASE 8
			//Maxes
			sBeamLevel.iMaxNodes = 10
			sBeamLevel.iMaxRotatingNodes = 5
			sBeamLevel.iMaxPackets = 6
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 6
			sBeamLevel.iMaxProxies = 1
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.012500, 0.845000)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.012500, 0.432500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.125000, 0.395000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.125000, 0.170000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.200000, 0.170000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.200000, 0.395000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.350000, 0.845000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.350000, 0.545000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.425000, 0.545000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.425000, 0.845000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.500000, 0.845000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.500000, 0.620000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.575000, 0.845000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(1.025000, 0.620000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(1.100000, 0.620000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(1.100000, 0.845000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.950000, 0.545000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.950000, 0.170000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(1.025000, 0.545000)
			
			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.575000, 0.845000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.575000, 0.770000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.875000, 0.770000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.875000, 0.845000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.012500, 0.470001)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.012500, 0.395001)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.285983, 0.309017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.339017, 0.255984)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.885983, 0.309017)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.939017, 0.255984)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.285983, 0.834017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.339017, 0.780984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.462500, 0.620001)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.462500, 0.545000)

			//Static
			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(1.025000, 0.620000)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(1.100000, 0.545000)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.500000, 0.170000)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.425000, 0.245000)
			
			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.950000, 0.845000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.875000, 0.770000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.950000, 0.845000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(1.025000, 0.770000)
			
			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.950000, 0.845000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(1.025000, 0.770000)

			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(-0.014017, 0.234017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(-0.014017, 0.180983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.039017, 0.180983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.039017, 0.234017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(-0.089017, 0.834017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(-0.089017, 0.780983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(-0.035983, 0.780983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(-0.035983, 0.834017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.510983, 0.234017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.510983, 0.180983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.564017, 0.180983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.564017, 0.234017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.885983, 0.234017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.885983, 0.180983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.939017, 0.180983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.939017, 0.234017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(1.035983, 0.234017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(1.035983, 0.180983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(1.089017, 0.180983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(1.089017, 0.234017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.960983, 0.684017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.960983, 0.630983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(1.014017, 0.630983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(1.014017, 0.684017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.060983, 0.234017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.060983, 0.180983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.114017, 0.180983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.114017, 0.234017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.585983, 0.759017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.585983, 0.705983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.639017, 0.705983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.639017, 0.759017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.435983, 0.759017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.435983, 0.705983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.489017, 0.705983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.489017, 0.759017)


			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(0.435983, 0.309017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(0.435983, 0.255984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(0.489017, 0.255984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(0.489017, 0.309017)
		BREAK
		CASE 9
			//Maxes
			sBeamLevel.iMaxNodes = 11
			sBeamLevel.iMaxRotatingNodes = 6
			sBeamLevel.iMaxPackets = 6
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 8
			sBeamLevel.iMaxProxies = 2
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.087500, 0.545000)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.087500, 0.807500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.275000, 0.845000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.275000, 0.695000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.350000, 0.695000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.350000, 0.845000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(-0.025000, 0.620000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(-0.025000, 0.545000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.050000, 0.545000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.050000, 0.620000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.050000, 0.545000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.050000, 0.470000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.125000, 0.470000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.125000, 0.545000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(-0.100000, 0.470000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(-0.100000, 0.395000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(-0.025000, 0.470000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.800000, 0.470000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.800000, 0.395000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(1.025000, 0.395000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(1.025000, 0.470000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.800000, 0.770000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.800000, 0.620000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.875000, 0.620000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.875000, 0.770000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.725000, 0.770000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.725000, 0.470000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.800000, 0.470000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.800000, 0.770000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.500000, 0.545000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.500000, 0.245000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.650000, 0.245000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.650000, 0.545000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.060983, 0.834017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.114017, 0.780984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.285983, 0.571517)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.339017, 0.518484)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.060983, 0.384017)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.114017, 0.330984)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(1.035983, 0.759017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(1.089017, 0.705984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.135983, 0.459017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.189017, 0.405984)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.210983, 0.234017)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.264017, 0.180984)

			//Static
			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(-0.025000, 0.545000)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.050000, 0.470000)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(-0.025000, 0.845000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(-0.100000, 0.770000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.275000, 0.245000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(1.100000, 0.245000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(1.100000, 0.770000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.210983, 0.834017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.210983, 0.780983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.264017, 0.780983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.264017, 0.834017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.060983, 0.459017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.060983, 0.405983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.114017, 0.405983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.114017, 0.459017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(-0.014017, 0.234017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(-0.014017, 0.180983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.039017, 0.180983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.039017, 0.234017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.885983, 0.534017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.885983, 0.480983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.939017, 0.480983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.939017, 0.534017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(1.035983, 0.384017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(1.035983, 0.330983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(1.089017, 0.330983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(1.089017, 0.384017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.885983, 0.759017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.885983, 0.705983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.939017, 0.705983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.939017, 0.759017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.089017, 0.384017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.089017, 0.330983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(-0.035983, 0.330983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(-0.035983, 0.384017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.810983, 0.534017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.810983, 0.480983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.864017, 0.480983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.864017, 0.534017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.360983, 0.834017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.360983, 0.780983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.414017, 0.780983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.414017, 0.834017)


			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(0.660983, 0.534017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(0.660983, 0.480984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(0.714017, 0.480984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(0.714017, 0.534017)

			sBeamLevel.vProxies[1][0] = INIT_VECTOR_2D(-0.089017, 0.534017)
			sBeamLevel.vProxies[1][1] = INIT_VECTOR_2D(-0.089017, 0.480984)
			sBeamLevel.vProxies[1][2] = INIT_VECTOR_2D(-0.035983, 0.480984)
			sBeamLevel.vProxies[1][3] = INIT_VECTOR_2D(-0.035983, 0.534017)
		BREAK
		CASE 10
			//Maxes
			sBeamLevel.iMaxNodes = 13
			sBeamLevel.iMaxRotatingNodes = 8
			sBeamLevel.iMaxPackets = 10
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 8
			sBeamLevel.iMaxProxies = 1
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.012500, 0.245000)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.012500, 0.807500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.025000, 0.245000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.050000, 0.170000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.050000, 0.245000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.125000, 0.470000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.125000, 0.170000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.200000, 0.170000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.200000, 0.470000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.275000, 0.395000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.275000, 0.170000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.350000, 0.395000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.500000, 0.320000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.500000, 0.245000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.725000, 0.245000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.725000, 0.320000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.650000, 0.245000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.650000, 0.170000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.725000, 0.170000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.725000, 0.245000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.800000, 0.395000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.800000, 0.245000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.875000, 0.245000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.875000, 0.395000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.200000, 0.845000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.200000, 0.770000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.425000, 0.770000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.425000, 0.845000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.350000, 0.770000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.350000, 0.695000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.575000, 0.695000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.575000, 0.770000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(-0.025000, 0.807481)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.050000, 0.807520)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.275000, 0.507500)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.350000, 0.507501)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.575000, 0.507500)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.650000, 0.507500)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.425000, 0.507500)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.500000, 0.507501)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.086072, 0.319973)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.086072, 0.245028)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(1.035983, 0.234017)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(1.089017, 0.180984)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.210983, 0.759017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.264017, 0.705984)
			
			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.725000, 0.507500)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.800000, 0.507501)

			//Static
			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.125000, 0.845000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.200000, 0.770000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.125000, 0.545000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.350000, 0.245000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(1.100000, 0.770000)
			
			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(-0.025000, 0.845000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(-0.100000, 0.770000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(-0.089017, 0.234017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(-0.089017, 0.180983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(-0.035983, 0.180983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(-0.035983, 0.234017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.210983, 0.384017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.210983, 0.330983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.264017, 0.330983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.264017, 0.384017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.210983, 0.234017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.210983, 0.180983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.264017, 0.180983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.264017, 0.234017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.585983, 0.234017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.585983, 0.180983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.639017, 0.180983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.639017, 0.234017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.735983, 0.234017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.735983, 0.180983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.789017, 0.180983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.789017, 0.234017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.960983, 0.684017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.960983, 0.630983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(1.014017, 0.630983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(1.014017, 0.684017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.435983, 0.834017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.435983, 0.780983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.489017, 0.780983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.489017, 0.834017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.135983, 0.759017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.135983, 0.705983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.189017, 0.705983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.189017, 0.759017)

			sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(0.885983, 0.459017)
			sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(0.885983, 0.405983)
			sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(0.939017, 0.405983)
			sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(0.939017, 0.459017)

			sBeamLevel.vPackets[9][0] = INIT_VECTOR_2D(0.360983, 0.684017)
			sBeamLevel.vPackets[9][1] = INIT_VECTOR_2D(0.360983, 0.630983)
			sBeamLevel.vPackets[9][2] = INIT_VECTOR_2D(0.414017, 0.630983)
			sBeamLevel.vPackets[9][3] = INIT_VECTOR_2D(0.414017, 0.684017)


			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.735983, 0.309017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.735983, 0.255983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.789017, 0.255983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.789017, 0.309017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.060983, 0.234017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.060983, 0.180983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.114017, 0.180983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.114017, 0.234017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.960983, 0.309017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.960983, 0.255983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(1.014017, 0.255983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(1.014017, 0.309017)

			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(1.035983, 0.534017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(1.035983, 0.480984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(1.089017, 0.480984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(1.089017, 0.534017)
		BREAK
		CASE 11
			//Maxes
			sBeamLevel.iMaxNodes = 14
			sBeamLevel.iMaxRotatingNodes = 7
			sBeamLevel.iMaxPackets = 8
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 8
			sBeamLevel.iMaxProxies = 1
			
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.462500, 0.245000)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.462500, 2.000000)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.275000, 0.845000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.275000, 0.620000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.350000, 0.620000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.350000, 0.845000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.575000, 0.845000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.575000, 0.620000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.650000, 0.620000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.650000, 0.845000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(1.025000, 0.845000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(1.025000, 0.770000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(1.100000, 0.770000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(1.100000, 0.845000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.200000, 0.545000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.200000, 0.395000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.275000, 0.395000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.275000, 0.545000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.800000, 0.395000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.800000, 0.170000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.875000, 0.170000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.875000, 0.395000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.800000, 0.695000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.800000, 0.620000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.875000, 0.620000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.875000, 0.695000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.650000, 0.395000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.650000, 0.170000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.725000, 0.170000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.725000, 0.395000)
			
			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.425000, 0.245000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.425000, 0.170000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.500000, 0.170000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.500000, 0.245000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.435983, 0.834017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.489017, 0.780984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.210983, 0.234017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.264017, 0.180984)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.585983, 0.234017)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.639017, 0.180984)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.762500, 0.320000)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.762500, 0.245001)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(1.035983, 0.609017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(1.089017, 0.555984)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.885983, 0.684017)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.939017, 0.630984)
			
			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(-0.035983, 0.684017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(-0.089017, 0.630984)

			//Static
			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.275000, 0.620000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.350000, 0.545000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.650000, 0.620000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.575000, 0.545000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(1.025000, 0.770000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(1.100000, 0.695000)
			
			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(-0.025000, 0.845000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(-0.100000, 0.770000)
			
			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(-0.100000, 0.245000)
			
			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(0.500000, 0.845000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(0.575000, 0.770000)

			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(0.575000, 0.170000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(0.500000, 0.245000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.210983, 0.834017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.210983, 0.780983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.264017, 0.780983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.264017, 0.834017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.210983, 0.759017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.210983, 0.705983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.264017, 0.705983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.264017, 0.759017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.135983, 0.534017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.135983, 0.480983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.189017, 0.480983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.189017, 0.534017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(-0.089017, 0.459017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(-0.089017, 0.405983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(-0.035983, 0.405983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(-0.035983, 0.459017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(-0.089017, 0.309017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(-0.089017, 0.255983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(-0.035983, 0.255983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(-0.035983, 0.309017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.660983, 0.759017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.660983, 0.705983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.714017, 0.705983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.714017, 0.759017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.660983, 0.834017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.660983, 0.780983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.714017, 0.780983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.714017, 0.834017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.885983, 0.309017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.885983, 0.255983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.939017, 0.255983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.939017, 0.309017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.210983, 0.609017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.210983, 0.555983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.264017, 0.555983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.264017, 0.609017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.735983, 0.234017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.735983, 0.180983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.789017, 0.180983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.789017, 0.234017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.960983, 0.309017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.960983, 0.255983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(1.014017, 0.255983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(1.014017, 0.309017)

			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(0.735983, 0.759017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(0.735983, 0.705984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(0.789017, 0.705984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(0.789017, 0.759017)
		BREAK
	ENDSWITCH
	
	SWITCH(iLevel)
		CASE 12
			//Maxes
			sBeamLevel.iMaxNodes = 14
			sBeamLevel.iMaxRotatingNodes = 7
			sBeamLevel.iMaxPackets = 6
			sBeamLevel.iMaxFirewalls = 5
			sBeamLevel.iMaxInnerWalls = 11
			sBeamLevel.iMaxProxies = 1
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.087500, 0.320000)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.087500, 2.000000)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.025000, 0.770000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.025000, 0.620000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.050000, 0.620000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.050000, 0.770000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(-0.025000, 0.545000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(-0.025000, 0.395000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.050000, 0.395000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.050000, 0.545000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(-0.025000, 0.320000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.200000, 0.170000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.200000, 0.320000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.275000, 0.320000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.275000, 0.170000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.350000, 0.320000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.350000, 0.245000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.575000, 0.170000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.575000, 0.245000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.650000, 0.470000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.650000, 0.245000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.875000, 0.245000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.875000, 0.470000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.350000, 0.845000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.350000, 0.770000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.500000, 0.770000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.500000, 0.845000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.950000, 0.845000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.950000, 0.770000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(1.100000, 0.770000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(1.100000, 0.845000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(0.125000, 0.695000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(0.125000, 0.470000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(0.200000, 0.695000)
			
			sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(0.350000, 0.545000)
			sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(0.350000, 0.470000)
			sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(0.875000, 0.470000)
			sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(0.875000, 0.545000)

			sBeamLevel.vInnerWalls[10][0] = INIT_VECTOR_2D(0.350000, 0.470000)
			sBeamLevel.vInnerWalls[10][1] = INIT_VECTOR_2D(0.350000, 0.395000)
			sBeamLevel.vInnerWalls[10][2] = INIT_VECTOR_2D(0.425000, 0.395000)
			sBeamLevel.vInnerWalls[10][3] = INIT_VECTOR_2D(0.425000, 0.470000)

			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(0.060983, 0.759017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(0.114017, 0.705984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.350000, 0.732501)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.425000, 0.732501)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.575000, 0.732501)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.650000, 0.732501)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.800004, 0.732501)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.874996, 0.732501)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(1.089017, 0.534017)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(1.035983, 0.480984)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.885983, 0.234017)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.939017, 0.180984)			
			
			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.462500, 0.395000)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.462500, 0.320000)

			//Static
			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(-0.025000, 0.845000)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(-0.100000, 0.770000)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.275000, 0.845000)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.350000, 0.770000)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(1.025000, 0.770000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(1.100000, 0.695000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.350000, 0.320000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.275000, 0.395000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(0.650000, 0.320000)

			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(0.650000, 0.170000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(0.575000, 0.245000)

			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(1.100000, 0.245000)

			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(-0.089017, 0.234017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(-0.089017, 0.180983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(-0.035983, 0.180983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(-0.035983, 0.234017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(0.360983, 0.309017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(0.360983, 0.255983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(0.414017, 0.255983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(0.414017, 0.309017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.510983, 0.309017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.510983, 0.255983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.564017, 0.255983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.564017, 0.309017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.660983, 0.834017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.660983, 0.780983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.714017, 0.780983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.714017, 0.834017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(1.035983, 0.609017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(1.035983, 0.555983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(1.089017, 0.555983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(1.089017, 0.609017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(1.035983, 0.459017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(1.035983, 0.405983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(1.089017, 0.405983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(1.089017, 0.459017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.014017, 0.609017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.014017, 0.555983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.039017, 0.555983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.039017, 0.609017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.435983, 0.309017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.435983, 0.255983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.489017, 0.255983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.489017, 0.309017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.435983, 0.459017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.435983, 0.405983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.489017, 0.405983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.489017, 0.459017)

			sBeamLevel.vFirewalls[3][0] = INIT_VECTOR_2D(0.510983, 0.834017)
			sBeamLevel.vFirewalls[3][1] = INIT_VECTOR_2D(0.510983, 0.780983)
			sBeamLevel.vFirewalls[3][2] = INIT_VECTOR_2D(0.564017, 0.780983)
			sBeamLevel.vFirewalls[3][3] = INIT_VECTOR_2D(0.564017, 0.834017)

			sBeamLevel.vFirewalls[4][0] = INIT_VECTOR_2D(0.885983, 0.534017)
			sBeamLevel.vFirewalls[4][1] = INIT_VECTOR_2D(0.885983, 0.480983)
			sBeamLevel.vFirewalls[4][2] = INIT_VECTOR_2D(0.939017, 0.480983)
			sBeamLevel.vFirewalls[4][3] = INIT_VECTOR_2D(0.939017, 0.534017)

			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(-0.089017, 0.609017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(-0.089017, 0.555984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(-0.035983, 0.555984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(-0.035983, 0.609017)	
		BREAK
		CASE 13
			//Maxes
			sBeamLevel.iMaxNodes = 15
			sBeamLevel.iMaxRotatingNodes = 9
			sBeamLevel.iMaxPackets = 10
			sBeamLevel.iMaxFirewalls = 7
			sBeamLevel.iMaxInnerWalls = 9
			sBeamLevel.iMaxProxies = 1
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.050000, 0.732500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.125000, 0.732500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.100000, 0.620000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.050000, 0.620000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.050000, 0.845000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.050000, 0.395000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.050000, 0.320000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.125000, 0.320000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.125000, 0.395000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.125000, 0.320000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.125000, 0.170000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.275000, 0.170000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.275000, 0.320000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.350000, 0.545000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.350000, 0.395000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.425000, 0.395000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.425000, 0.545000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.500000, 0.620000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.500000, 0.470000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.650000, 0.470000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.650000, 0.620000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.725000, 0.620000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.725000, 0.395000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.800000, 0.395000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.800000, 0.620000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.875000, 0.695000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.875000, 0.545000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.950000, 0.545000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.950000, 0.695000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.875000, 0.545000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.875000, 0.320000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(1.025000, 0.320000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(1.025000, 0.545000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(0.875000, 0.845000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(0.875000, 0.770000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(1.025000, 0.770000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(1.025000, 0.845000)

			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(1.035983, 0.759017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(1.089017, 0.705984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.339017, 0.834017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.285983, 0.780984)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(-0.035983, 0.609017)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(-0.089017, 0.555984)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(0.275000, 0.582500)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(0.350000, 0.582501)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.687500, 0.545001)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.687500, 0.470001)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.800000, 0.507500)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.875000, 0.507501)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.264017, 0.384017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.210983, 0.330984)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.650000, 0.207500)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.725000, 0.207501)
			
			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.500000, 0.807500)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.575000, 0.807501)

			//Static
			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.350000, 0.170000)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.275000, 0.245000)

			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.800000, 0.845000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.875000, 0.770000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(1.025000, 0.170000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(1.100000, 0.245000)
			
			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(0.050000, 0.320000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(0.125000, 0.245000)
			
			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(-0.100000, 0.245000)

			sBeamLevel.vNodes[14][0] = INIT_VECTOR_2D(-0.025000, 0.170000)
			sBeamLevel.vNodes[14][1] = INIT_VECTOR_2D(0.050000, 0.245000)

			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(0.060983, 0.234017)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(0.060983, 0.180983)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(0.114017, 0.180983)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(0.114017, 0.234017)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(-0.089017, 0.309017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(-0.089017, 0.255983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(-0.035983, 0.255983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(-0.035983, 0.309017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(0.060983, 0.684017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(0.060983, 0.630983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.114017, 0.630983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.114017, 0.684017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(0.060983, 0.834017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(0.060983, 0.780983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.114017, 0.780983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.114017, 0.834017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.135983, 0.834017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.135983, 0.780983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.189017, 0.780983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.189017, 0.834017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(0.510983, 0.684017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(0.510983, 0.630983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(0.564017, 0.630983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(0.564017, 0.684017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.435983, 0.384017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.435983, 0.330983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.489017, 0.330983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.489017, 0.384017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.660983, 0.459017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.660983, 0.405983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.714017, 0.405983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.714017, 0.459017)

			sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(0.810983, 0.459017)
			sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(0.810983, 0.405983)
			sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(0.864017, 0.405983)
			sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(0.864017, 0.459017)

			sBeamLevel.vPackets[9][0] = INIT_VECTOR_2D(1.035983, 0.834017)
			sBeamLevel.vPackets[9][1] = INIT_VECTOR_2D(1.035983, 0.780983)
			sBeamLevel.vPackets[9][2] = INIT_VECTOR_2D(1.089017, 0.780983)
			sBeamLevel.vPackets[9][3] = INIT_VECTOR_2D(1.089017, 0.834017)

			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(0.135983, 0.684017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(0.135983, 0.630983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.189017, 0.630983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.189017, 0.684017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.435983, 0.609017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.435983, 0.555983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.489017, 0.555983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.489017, 0.609017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.660983, 0.609017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.660983, 0.555983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.714017, 0.555983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.714017, 0.609017)

			sBeamLevel.vFirewalls[3][0] = INIT_VECTOR_2D(0.735983, 0.384017)
			sBeamLevel.vFirewalls[3][1] = INIT_VECTOR_2D(0.735983, 0.330983)
			sBeamLevel.vFirewalls[3][2] = INIT_VECTOR_2D(0.789017, 0.330983)
			sBeamLevel.vFirewalls[3][3] = INIT_VECTOR_2D(0.789017, 0.384017)
			
			sBeamLevel.vFirewalls[4][0] = INIT_VECTOR_2D(-0.014017, 0.459017)
			sBeamLevel.vFirewalls[4][1] = INIT_VECTOR_2D(-0.014017, 0.405983)
			sBeamLevel.vFirewalls[4][2] = INIT_VECTOR_2D(0.039017, 0.405983)
			sBeamLevel.vFirewalls[4][3] = INIT_VECTOR_2D(0.039017, 0.459017)
			
			sBeamLevel.vFirewalls[5][0] = INIT_VECTOR_2D(-0.014017, 0.534017)
			sBeamLevel.vFirewalls[5][1] = INIT_VECTOR_2D(-0.014017, 0.480983)
			sBeamLevel.vFirewalls[5][2] = INIT_VECTOR_2D(0.039017, 0.480983)
			sBeamLevel.vFirewalls[5][3] = INIT_VECTOR_2D(0.039017, 0.534017)
			
			sBeamLevel.vFirewalls[6][0] = INIT_VECTOR_2D(0.585983, 0.459017)
			sBeamLevel.vFirewalls[6][1] = INIT_VECTOR_2D(0.585983, 0.405983)
			sBeamLevel.vFirewalls[6][2] = INIT_VECTOR_2D(0.639017, 0.405983)
			sBeamLevel.vFirewalls[6][3] = INIT_VECTOR_2D(0.639017, 0.459017)

			//Proxies
			sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(-0.014017, 0.309017)
			sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(-0.014017, 0.255984)
			sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(0.039017, 0.255984)
			sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(0.039017, 0.309017)
		BREAK
		#IF IS_DEBUG_BUILD
		CASE 999 //OLD LEVEL
			//Maxes
			sBeamLevel.iMaxNodes = 14
			sBeamLevel.iMaxRotatingNodes = 10
			sBeamLevel.iMaxPackets = 11
			sBeamLevel.iMaxFirewalls = 3
			sBeamLevel.iMaxInnerWalls = 13
			sBeamLevel.iMaxProxies = 0
			//Beam
			//Origin
			vBeam[0] = INIT_VECTOR_2D(0.200000, 0.357500)
			//First Direction
			vBeam[1] = INIT_VECTOR_2D(0.612500, 0.357500)

			//Outer Walls
			sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
			sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
			sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
			sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

			//Inner Walls
			sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(0.200000, 0.545000)
			sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(0.200000, 0.470000)
			sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(0.275000, 0.470000)
			sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(0.275000, 0.545000)

			sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(0.350000, 0.320000)
			sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(0.350000, 0.245000)
			sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(0.500000, 0.245000)
			sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(0.500000, 0.320000)

			sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(0.350000, 0.770000)
			sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(0.350000, 0.695000)
			sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(0.575000, 0.695000)
			sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(0.575000, 0.770000)

			sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(0.500000, 0.695000)
			sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(0.500000, 0.395000)
			sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(0.575000, 0.695000)

			sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(0.575000, 0.470000)
			sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(0.800000, 0.395000)
			sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(0.800000, 0.470000)

			sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(0.725000, 0.395000)
			sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(0.725000, 0.245000)
			sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(0.800000, 0.245000)
			sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(0.800000, 0.395000)

			sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(0.650000, 0.695000)
			sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(0.650000, 0.545000)
			sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(0.725000, 0.545000)
			sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(0.725000, 0.695000)

			sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(0.800000, 0.845000)
			sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(0.800000, 0.695000)
			sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(0.875000, 0.695000)
			sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(0.875000, 0.845000)

			sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(0.875000, 0.770000)
			sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(0.875000, 0.695000)
			sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(1.100000, 0.695000)
			sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(1.100000, 0.770000)

			sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(0.950000, 0.545000)
			sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(0.950000, 0.470000)
			sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(1.100000, 0.470000)
			sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(1.100000, 0.545000)

			sBeamLevel.vInnerWalls[10][0] = INIT_VECTOR_2D(0.950000, 0.395000)
			sBeamLevel.vInnerWalls[10][1] = INIT_VECTOR_2D(0.950000, 0.245000)
			sBeamLevel.vInnerWalls[10][2] = INIT_VECTOR_2D(1.025000, 0.245000)
			sBeamLevel.vInnerWalls[10][3] = INIT_VECTOR_2D(1.025000, 0.395000)

			sBeamLevel.vInnerWalls[11][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
			sBeamLevel.vInnerWalls[11][1] = INIT_VECTOR_2D(-0.100000, 0.320000)
			sBeamLevel.vInnerWalls[11][2] = INIT_VECTOR_2D(0.200000, 0.320000)
			sBeamLevel.vInnerWalls[11][3] = INIT_VECTOR_2D(0.200000, 0.395000)

			sBeamLevel.vInnerWalls[12][0] = INIT_VECTOR_2D(0.050000, 0.770000)
			sBeamLevel.vInnerWalls[12][1] = INIT_VECTOR_2D(0.050000, 0.545000)
			sBeamLevel.vInnerWalls[12][2] = INIT_VECTOR_2D(0.125000, 0.545000)
			sBeamLevel.vInnerWalls[12][3] = INIT_VECTOR_2D(0.125000, 0.770000)


			//Nodes
			//Rotatable
			sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(1.035983, 0.234017)
			sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(1.089017, 0.180984)

			sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(0.114017, 0.459017)
			sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(0.060983, 0.405984)

			sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(0.864017, 0.459017)
			sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(0.810984, 0.405984)

			sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(1.089017, 0.459017)
			sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(1.035983, 0.405984)

			sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(0.189016, 0.534016)
			sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(0.135983, 0.480983)

			sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(0.189017, 0.684017)
			sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(0.135983, 0.630984)

			sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(0.789017, 0.684017)
			sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(0.735984, 0.630984)

			sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(0.714016, 0.759016)
			sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(0.660984, 0.705983)

			sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(0.339016, 0.834017)
			sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(0.285984, 0.780984)

			sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(0.639016, 0.834017)
			sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(0.585983, 0.780984)


			//Static
			sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(0.275000, 0.170000)
			sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(0.350000, 0.245000)

			sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(0.650000, 0.170000)
			sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(0.575000, 0.245000)

			sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(0.575000, 0.395000)
			sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(0.650000, 0.320000)

			sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(0.725000, 0.770000)
			sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(0.800000, 0.695000)


			//Packets
			sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(1.035983, 0.346517)
			sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(1.035983, 0.293483)
			sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(1.089017, 0.293483)
			sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(1.089017, 0.346517)

			sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(-0.089017, 0.459017)
			sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(-0.089017, 0.405983)
			sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(-0.035983, 0.405983)
			sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(-0.035983, 0.459017)

			sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(-0.014017, 0.459017)
			sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(-0.014017, 0.405983)
			sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(0.039017, 0.405983)
			sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(0.039017, 0.459017)

			sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(-0.014017, 0.534017)
			sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(-0.014017, 0.480983)
			sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(0.039017, 0.480983)
			sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(0.039017, 0.534017)

			sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(0.885983, 0.534017)
			sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(0.885983, 0.480983)
			sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(0.939017, 0.480983)
			sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(0.939017, 0.534017)

			sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(-0.089017, 0.609017)
			sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(-0.089017, 0.555983)
			sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(-0.035983, 0.555983)
			sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(-0.035983, 0.609017)

			sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(0.285983, 0.609017)
			sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(0.285983, 0.555983)
			sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(0.339017, 0.555983)
			sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(0.339017, 0.609017)

			sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(0.585983, 0.609017)
			sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(0.585983, 0.555983)
			sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(0.639017, 0.555983)
			sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(0.639017, 0.609017)

			sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(0.360983, 0.684017)
			sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(0.360983, 0.630983)
			sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(0.414017, 0.630983)
			sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(0.414017, 0.684017)

			sBeamLevel.vPackets[9][0] = INIT_VECTOR_2D(0.435983, 0.684017)
			sBeamLevel.vPackets[9][1] = INIT_VECTOR_2D(0.435983, 0.630983)
			sBeamLevel.vPackets[9][2] = INIT_VECTOR_2D(0.489017, 0.630983)
			sBeamLevel.vPackets[9][3] = INIT_VECTOR_2D(0.489017, 0.684017)

			sBeamLevel.vPackets[10][0] = INIT_VECTOR_2D(0.810983, 0.684017)
			sBeamLevel.vPackets[10][1] = INIT_VECTOR_2D(0.810983, 0.630983)
			sBeamLevel.vPackets[10][2] = INIT_VECTOR_2D(0.864017, 0.630983)
			sBeamLevel.vPackets[10][3] = INIT_VECTOR_2D(0.864017, 0.684017)


			//Firewalls
			sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.014017, 0.609017)
			sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.014017, 0.555983)
			sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(0.039017, 0.555983)
			sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(0.039017, 0.609017)

			sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(0.660983, 0.534017)
			sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(0.660983, 0.480983)
			sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(0.714017, 0.480983)
			sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(0.714017, 0.534017)

			sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(0.735983, 0.834017)
			sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(0.735983, 0.780983)
			sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(0.789017, 0.780983)
			sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(0.789017, 0.834017)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

#IF IS_DEBUG_BUILD
PROC LOAD_BLANK_LEVEL()
	//Maxes
	sBeamLevel.iMaxNodes = 20
	sBeamLevel.iMaxRotatingNodes = 10
	sBeamLevel.iMaxPackets = 20
	sBeamLevel.iMaxFirewalls = 10
	sBeamLevel.iMaxInnerWalls = 30
	sBeamLevel.iMaxProxies = 5
	//Beam
	//Origin
	vBeam[0] = INIT_VECTOR_2D(-0.137500, 0.507500)
	//First Direction
	vBeam[1] = INIT_VECTOR_2D(-0.062500, 0.507500)

	//Outer Walls
	sBeamLevel.vOuterWalls[0] = INIT_VECTOR_2D(-0.100000, 0.845000)
	sBeamLevel.vOuterWalls[1] = INIT_VECTOR_2D(-0.100000, 0.170000)
	sBeamLevel.vOuterWalls[2] = INIT_VECTOR_2D(1.100000, 0.170000)
	sBeamLevel.vOuterWalls[3] = INIT_VECTOR_2D(1.100000, 0.845000)

	//Inner Walls
	sBeamLevel.vInnerWalls[0][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[0][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[0][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[0][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[1][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[1][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[1][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[1][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[2][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[2][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[2][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[2][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[3][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[3][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[3][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[3][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[4][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[4][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[4][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[4][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[5][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[5][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[5][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[5][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[6][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[6][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[6][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[6][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[7][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[7][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[7][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[7][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[8][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[8][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[8][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[8][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[9][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[9][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[9][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[9][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[10][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[10][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[10][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[10][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[11][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[11][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[11][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[11][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[12][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[12][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[12][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[12][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[13][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[13][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[13][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[13][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[14][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[14][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[14][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[14][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[15][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[15][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[15][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[15][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[16][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[16][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[16][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[16][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[17][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[17][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[17][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[17][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[18][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[18][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[18][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[18][3] = INIT_VECTOR_2D(-0.100000, 0.845000)

	sBeamLevel.vInnerWalls[19][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[19][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[19][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[19][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[20][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[20][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[20][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[20][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[21][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[21][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[21][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[21][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[22][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[22][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[22][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[22][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[23][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[23][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[23][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[23][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[24][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[24][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[24][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[24][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[25][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[25][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[25][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[25][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[26][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[26][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[26][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[26][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[27][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[27][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[27][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[27][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[28][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[28][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[28][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[28][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	
	sBeamLevel.vInnerWalls[29][0] = INIT_VECTOR_2D(-0.175000, 0.845000)
	sBeamLevel.vInnerWalls[29][1] = INIT_VECTOR_2D(-0.175000, 0.770000)
	sBeamLevel.vInnerWalls[29][2] = INIT_VECTOR_2D(-0.100000, 0.770000)
	sBeamLevel.vInnerWalls[29][3] = INIT_VECTOR_2D(-0.100000, 0.845000)
	


	//Nodes
	//Rotatable
	sBeamLevel.vNodes[0][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[0][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[1][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[1][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[2][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[2][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[3][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[3][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[4][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[4][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[5][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[5][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[6][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[6][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[7][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[7][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[8][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[8][1] = INIT_VECTOR_2D(-0.110983, 0.555984)

	sBeamLevel.vNodes[9][0] = INIT_VECTOR_2D(-0.164017, 0.609017)
	sBeamLevel.vNodes[9][1] = INIT_VECTOR_2D(-0.110983, 0.555984)


	//Static
	sBeamLevel.vNodes[10][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[10][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[11][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[11][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[12][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[12][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[13][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[13][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[14][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[14][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[15][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[15][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[16][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[16][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[17][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[17][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[18][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[18][1] = INIT_VECTOR_2D(-0.175000, 0.320000)

	sBeamLevel.vNodes[19][0] = INIT_VECTOR_2D(-0.100000, 0.395000)
	sBeamLevel.vNodes[19][1] = INIT_VECTOR_2D(-0.175000, 0.320000)


	//Packets
	sBeamLevel.vPackets[0][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[0][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[0][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[0][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[1][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[1][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[1][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[1][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[2][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[2][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[2][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[2][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[3][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[3][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[3][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[3][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[4][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[4][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[4][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[4][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[5][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[5][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[5][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[5][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[6][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[6][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[6][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[6][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[7][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[7][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[7][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[7][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[8][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[8][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[8][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[8][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[9][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[9][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[9][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[9][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[10][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[10][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[10][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[10][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[11][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[11][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[11][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[11][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[12][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[12][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[12][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[12][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[13][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[13][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[13][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[13][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[14][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[14][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[14][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[14][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[15][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[15][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[15][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[15][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[16][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[16][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[16][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[16][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[17][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[17][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[17][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[17][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[18][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[18][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[18][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[18][3] = INIT_VECTOR_2D(-0.110983, 0.759017)

	sBeamLevel.vPackets[19][0] = INIT_VECTOR_2D(-0.164017, 0.759017)
	sBeamLevel.vPackets[19][1] = INIT_VECTOR_2D(-0.164017, 0.705983)
	sBeamLevel.vPackets[19][2] = INIT_VECTOR_2D(-0.110983, 0.705983)
	sBeamLevel.vPackets[19][3] = INIT_VECTOR_2D(-0.110983, 0.759017)


	//Firewalls
	sBeamLevel.vFirewalls[0][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[0][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[0][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[0][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[1][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[1][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[1][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[1][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[2][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[2][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[2][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[2][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[3][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[3][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[3][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[3][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[4][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[4][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[4][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[4][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[5][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[5][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[5][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[5][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[6][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[6][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[6][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[6][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[7][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[7][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[7][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[7][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[8][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[8][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[8][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[8][3] = INIT_VECTOR_2D(-0.110983, 0.684017)

	sBeamLevel.vFirewalls[9][0] = INIT_VECTOR_2D(-0.164017, 0.684017)
	sBeamLevel.vFirewalls[9][1] = INIT_VECTOR_2D(-0.164017, 0.630983)
	sBeamLevel.vFirewalls[9][2] = INIT_VECTOR_2D(-0.110983, 0.630983)
	sBeamLevel.vFirewalls[9][3] = INIT_VECTOR_2D(-0.110983, 0.684017)


	//Proxies
	sBeamLevel.vProxies[0][0] = INIT_VECTOR_2D(-0.164017, 0.459017)
	sBeamLevel.vProxies[0][1] = INIT_VECTOR_2D(-0.164017, 0.405984)
	sBeamLevel.vProxies[0][2] = INIT_VECTOR_2D(-0.110983, 0.405984)
	sBeamLevel.vProxies[0][3] = INIT_VECTOR_2D(-0.110983, 0.459017)

	sBeamLevel.vProxies[1][0] = INIT_VECTOR_2D(-0.164017, 0.459017)
	sBeamLevel.vProxies[1][1] = INIT_VECTOR_2D(-0.164017, 0.405984)
	sBeamLevel.vProxies[1][2] = INIT_VECTOR_2D(-0.110983, 0.405984)
	sBeamLevel.vProxies[1][3] = INIT_VECTOR_2D(-0.110983, 0.459017)

	sBeamLevel.vProxies[2][0] = INIT_VECTOR_2D(-0.164017, 0.459017)
	sBeamLevel.vProxies[2][1] = INIT_VECTOR_2D(-0.164017, 0.405984)
	sBeamLevel.vProxies[2][2] = INIT_VECTOR_2D(-0.110983, 0.405984)
	sBeamLevel.vProxies[2][3] = INIT_VECTOR_2D(-0.110983, 0.459017)

	sBeamLevel.vProxies[3][0] = INIT_VECTOR_2D(-0.164017, 0.459017)
	sBeamLevel.vProxies[3][1] = INIT_VECTOR_2D(-0.164017, 0.405984)
	sBeamLevel.vProxies[3][2] = INIT_VECTOR_2D(-0.110983, 0.405984)
	sBeamLevel.vProxies[3][3] = INIT_VECTOR_2D(-0.110983, 0.459017)

	sBeamLevel.vProxies[4][0] = INIT_VECTOR_2D(-0.164017, 0.459017)
	sBeamLevel.vProxies[4][1] = INIT_VECTOR_2D(-0.164017, 0.405984)
	sBeamLevel.vProxies[4][2] = INIT_VECTOR_2D(-0.110983, 0.405984)
	sBeamLevel.vProxies[4][3] = INIT_VECTOR_2D(-0.110983, 0.459017)

ENDPROC

PROC DUMP_LEVEL_TO_LOGS()
	INT i, j
	PRINTLN("")
	PRINTLN("")
	PRINTLN("")
	PRINTLN("##################################################################################################################################")
	PRINTLN("####################################[JS][BeamHack] - DUMP_LEVEL_TO_LOGS - Dumping level coords####################################")
	PRINTLN("##################################################################################################################################")
	PRINTLN("")
	PRINTLN("")
	PRINTLN("")
	
	PRINTLN("//Maxes")
	PRINTLN("sBeamLevel.iMaxNodes = ", sBeamLevel.iMaxNodes)
	PRINTLN("sBeamLevel.iMaxRotatingNodes = ", sBeamLevel.iMaxRotatingNodes)
	PRINTLN("sBeamLevel.iMaxPackets = ", sBeamLevel.iMaxPackets)
	PRINTLN("sBeamLevel.iMaxFirewalls = ", sBeamLevel.iMaxFirewalls)
	PRINTLN("sBeamLevel.iMaxInnerWalls = ", sBeamLevel.iMaxInnerWalls)
	PRINTLN("sBeamLevel.iMaxProxies = ", sBeamLevel.iMaxProxies)
	
	PRINTLN("//Beam")
	PRINTLN("//Origin")
	PRINTLN("vBeam[0] = INIT_VECTOR_2D(",vBeam[0].x, ", ",vBeam[0].y,")")
	PRINTLN("//First Direction")
	PRINTLN("vBeam[1] = INIT_VECTOR_2D(",vBeam[1].x, ", ",vBeam[1].y,")")
	PRINTLN("")
	PRINTLN("//Outer Walls")
	FOR i = 0 TO ciBH_MAX_OUTER_WALLS - 1
		PRINTLN("sBeamLevel.vOuterWalls[",i,"] = INIT_VECTOR_2D(",sBeamLevel.vOuterWalls[i].x, ", ",sBeamLevel.vOuterWalls[i].y,")")
	ENDFOR
	PRINTLN("")
	PRINTLN("//Inner Walls")
	FOR i = 0 TO sBeamLevel.iMaxInnerWalls - 1
		//IF sBeamLevel.vInnerWalls[i][0].x > -0.175
			FOR j = 0 TO ciBH_INNER_WALL_POINTS - 1
				PRINTLN("sBeamLevel.vInnerWalls[",i,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vInnerWalls[i][j].x,", ",sBeamLevel.vInnerWalls[i][j].y,")")
			ENDFOR
		//ENDIF
		PRINTLN("")
	ENDFOR	
	PRINTLN("")
	PRINTLN("//Nodes")
	PRINTLN("//Rotatable")
	FOR i = 0 TO sBeamLevel.iMaxRotatingNodes- 1
		//IF sBeamLevel.vNodes[i][0].x > (-0.164017)
			FOR j = 0 TO ciBH_NODE_POINTS - 1
				PRINTLN("sBeamLevel.vNodes[",i,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vNodes[i][j].x,", ",sBeamLevel.vNodes[i][j].y,")")
			ENDFOR
		//ENDIF
		PRINTLN("")
	ENDFOR	
	PRINTLN("")
	PRINTLN("//Static")
	FOR i = sBeamLevel.iMaxRotatingNodes TO sBeamLevel.iMaxNodes - 1
		//IF sBeamLevel.vNodes[i][0].x > -0.100000
			FOR j = 0 TO ciBH_NODE_POINTS - 1
				PRINTLN("sBeamLevel.vNodes[",i,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vNodes[i][j].x,", ",sBeamLevel.vNodes[i][j].y,")")
			ENDFOR
		//ENDIF
		PRINTLN("")
	ENDFOR	
	PRINTLN("")
	PRINTLN("//Packets")
	FOR i = 0 TO sBeamLevel.iMaxPackets - 1
		//IF sBeamLevel.vPackets[i][0].x > -0.175000
			FOR j = 0 TO ciBH_PACKET_POINTS - 1
				PRINTLN("sBeamLevel.vPackets[",i,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vPackets[i][j].x,", ",sBeamLevel.vPackets[i][j].y,")")
			ENDFOR
		//ENDIF
		PRINTLN("")
	ENDFOR	
	PRINTLN("")
	PRINTLN("//Firewalls")
	FOR i = 0 TO sBeamLevel.iMaxFirewalls - 1
		//IF sBeamLevel.vFirewalls[i][0].x > -0.175000
			FOR j = 0 TO ciBH_FIREWALL_POINTS - 1
				PRINTLN("sBeamLevel.vFirewalls[",i,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vFirewalls[i][j].x,", ",sBeamLevel.vFirewalls[i][j].y,")")
			ENDFOR
		//ENDIF
		PRINTLN("")
	ENDFOR	
	PRINTLN("")
	PRINTLN("//Proxies")
	FOR i = 0 TO sBeamLevel.iMaxProxies - 1
		//IF sBeamLevel.vProxies[i][0].x > (-0.164017)
			FOR j = 0 TO ciBH_PROXY_POINTS - 1
				PRINTLN("sBeamLevel.vProxies[",i,"][",j,"] = INIT_VECTOR_2D(",sBeamLevel.vProxies[i][j].x,", ",sBeamLevel.vProxies[i][j].y,")")
			ENDFOR
		//ENDIF
		PRINTLN("")
	ENDFOR	
	PRINTLN("")
	PRINTLN("")
	PRINTLN("")
	PRINTLN("##################################################################################################################################")
	PRINTLN("#####################################[JS][BeamHack] - DUMP_LEVEL_TO_LOGS - End of level dump######################################")
	PRINTLN("##################################################################################################################################")
	PRINTLN("")
	PRINTLN("")
	PRINTLN("")
ENDPROC
#ENDIF
