USING "globals.sch"
USING "transition_controller.sch"
USING "net_pv_rate_limiter.sch"
USING "net_ticker_logic.sch"


STRUCT BG_SCRIPT_KICK_DETAILS
	
	TIME_DATATYPE UpdateTime
	INT iSecsToKickTime
	INT iMinsToKickTime
	
	BOOL bDoneWarning1
	BOOL bDoneWarning2
	BOOL bDoneWarning3
	
	#IF IS_DEBUG_BUILD
		BOOL bDoKickNow
		
		INT iPosix
		
		INT iYear
		INT iMonth
		INT iDay
		INT iHour
		INT iMinute
		INT iSecond
		
		BOOL bDateToPosix
		BOOL bPosixToDate
		BOOL bGetCurrentTimeAsPosix
		
		INT iUpdateInterval = -1

	#ENDIF
	
ENDSTRUCT

#IF IS_DEBUG_BUILD
PROC BG_SCRIPT_CREATE_WIDGETS(BG_SCRIPT_KICK_DETAILS &Data)

	START_WIDGET_GROUP("BG Script Kick")
			
		START_WIDGET_GROUP("Tunables")
			ADD_WIDGET_BOOL("bdisableBGSK", g_sMPTunables.bdisableBGSK)
			ADD_WIDGET_BOOL("bdisablewhenonmissionBGSK", g_sMPTunables.bdisablewhenonmissionBGSK)
			ADD_WIDGET_INT_SLIDER("iexpectedBGSNumberBGSK", g_sMPTunables.iexpectedBGSNumberBGSK, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iposixtimeBGSK", g_sMPTunables.iposixtimeBGSK, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("itimes1minBGSK", g_sMPTunables.itimes1minBGSK, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("itimes2minBGSK", g_sMPTunables.itimes2minBGSK, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("itimes13minBGSK", g_sMPTunables.itimes13minBGSK, LOWEST_INT, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("BG_SCRIPT_KICK_DETAILS")
			
			ADD_WIDGET_INT_SLIDER("iSecsToKickTime", Data.iSecsToKickTime, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iMinsToKickTime", Data.iMinsToKickTime, LOWEST_INT, HIGHEST_INT, 1)
			
			ADD_WIDGET_BOOL("bDoneWarning1", Data.bDoneWarning1)
			ADD_WIDGET_BOOL("bDoneWarning2", Data.bDoneWarning2)
			ADD_WIDGET_BOOL("bDoneWarning3", Data.bDoneWarning3)
		
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("bDoKickNow", Data.bDoKickNow)
				ADD_WIDGET_INT_SLIDER("iUpdateInterval", Data.iUpdateInterval, LOWEST_INT, HIGHEST_INT, 1)
				
				//ADD_WIDGET_INT_SLIDER("g_iBGScriptVersion", g_iBGScriptVersion, LOWEST_INT, HIGHEST_INT, 1)
				
				START_WIDGET_GROUP("Posix tool")					
					ADD_WIDGET_BOOL("bDateToPosix", Data.bDateToPosix)
					ADD_WIDGET_BOOL("bPosixToDate", Data.bPosixToDate)	
					ADD_WIDGET_BOOL("bGetCurrentTimeAsPosix", Data.bGetCurrentTimeAsPosix)
					ADD_WIDGET_INT_SLIDER("iPosix", Data.iPosix, LOWEST_INT, HIGHEST_INT, 1)					
					ADD_WIDGET_INT_SLIDER("iYear", Data.iYear, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iMonth", Data.iMonth, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iDay", Data.iDay, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iHour", Data.iHour, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iMinute", Data.iMinute, LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iSecond", Data.iSecond, LOWEST_INT, HIGHEST_INT, 1)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
		
		STOP_WIDGET_GROUP()
		

	STOP_WIDGET_GROUP()

ENDPROC

#ENDIF

FUNC INT CONVERT_DATE_TO_POSIX(INT iYear, INT iMonth, INT iDay, INT iHour, INT iMinute, INT iSecond)
	
	INT iThisYear = iYear - 1970
	INT iTotalSeconds = 0
	INT i
	
	// add previous years
	IF (iThisYear > 0)
		REPEAT iThisYear i
			IF IS_LEAP_YEAR(i + 1970)
				iTotalSeconds += (60*24*366*60)
			ELSE
				iTotalSeconds += (60*24*365*60)	
			ENDIF
		ENDREPEAT
	ENDIF
	
	// add previous months of this year 
	IF (iMonth > 0)
		REPEAT (iMonth-1) i
			iTotalSeconds += GET_NUMBER_OF_DAYS_IN_MONTH(INT_TO_ENUM(MONTH_OF_YEAR, i), iYear) * 60 * 24 * 60
		ENDREPEAT
	ENDIF
	
	// add the previous days
	IF (iDay > 0)
		iTotalSeconds += (iDay-1) * 60 *24 * 60
	ENDIF
	
	// add the current hours
	iTotalSeconds += iHour * 60 * 60

	// add the current mins
	iTotalSeconds += iMinute * 60
	
	// add the current seconds
	iTotalSeconds += iSecond

	RETURN(iTotalSeconds)

ENDFUNC

FUNC INT SECONDS_TO_MINUTES(INT iSeconds)	
	FLOAT fMins = TO_FLOAT(iSeconds) / 60.0	
	RETURN FLOOR(fMins)	
ENDFUNC

#IF IS_DEBUG_BUILD
PROC UPDATE_BG_SCRIPT_KICK_DEBUG(BG_SCRIPT_KICK_DETAILS &Data)
	
	IF (Data.bDoKickNow)
		CPRINTLN(DEBUG_BGSCRIPT_KICK, "UPDATE_BG_SCRIPT_KICK_DEBUG - bDoKickNow = TRUE.")	
		
		SET_PLAYER_KICKED_TO_GO_OFFLINE_UNTIL_REBOOT(TRUE) 
		
		Data.bDoKickNow = FALSE
	ENDIF
		
	IF (Data.bDateToPosix)
		
		CPRINTLN(DEBUG_BGSCRIPT_KICK, "UPDATE_BG_SCRIPT_KICK_DEBUG - bDateToPosix = TRUE.")
	
		Data.iPosix = CONVERT_DATE_TO_POSIX(Data.iYear, Data.iMonth, Data.iDay, Data.iHour, Data.iMinute, Data.iSecond)
	
		Data.bDateToPosix = FALSE
	ENDIF
		
	IF (Data.bPosixToDate)
		CPRINTLN(DEBUG_BGSCRIPT_KICK, "UPDATE_BG_SCRIPT_KICK_DEBUG - bDateToPosix = TRUE.")
		
		UGC_DATE sDate
		CONVERT_POSIX_TIME(Data.iPosix, sDate)	
		
		Data.iYear = sDate.nYear
		Data.iMonth = sDate.nMonth 
		Data.iDay = sDate.nDay //- 1 // the day returned by CONVERT_POSIX_TIME appears 1 too much.
		Data.iHour = sDate.nHour
		Data.iMinute = sDate.nMinute
		Data.iSecond = sDate.nSecond
		
		Data.bPosixToDate = FALSE
	ENDIF
	
	IF (Data.bGetCurrentTimeAsPosix)
	
		CPRINTLN(DEBUG_BGSCRIPT_KICK, "UPDATE_BG_SCRIPT_KICK_DEBUG - bGetCurrentTimeAsPosix = TRUE.")
		
		INT iYear, iMonth, iDay, iHour, iMinute, iSecond
		GET_UTC_TIME(iYear, iMonth, iDay, iHour, iMinute, iSecond)	
		
		CPRINTLN(DEBUG_BGSCRIPT_KICK, "GET_UTC_TIME : iYear=", iYear, " iMonth=", iMonth, " iDay=", iDay, " iHour=", iHour, " iMinute=", iMinute, " iSecond=", iSecond)
		Data.iPosix = CONVERT_DATE_TO_POSIX(iYear, iMonth, iDay, iHour, iMinute, iSecond)
	
		Data.bGetCurrentTimeAsPosix = FALSE
	ENDIF
		
ENDPROC
#ENDIF

PROC PRINT_BG_SCRIPT_KICK_WARNING_TICKER(INT iMins)
	IF (iMins = 1)
		PRINT_TICKER("BGSK_WARNIN1")
	ELSE
		PRINT_TICKER_WITH_INT("BGSK_WARNING", iMins)
	ENDIF
ENDPROC

FUNC BOOL ShouldAllowBGKickAtThisTime()
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR IS_THIS_IS_A_STRAND_MISSION()
	OR IS_A_STRAND_MISSION_BEING_INITIALISED()
	OR FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	OR IS_PLAYER_IN_CORONA()
	OR IS_ON_HEIST_CORONA()
	OR IS_TRANSITION_SESSION_LAUNCHING()
	OR IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	OR SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
	OR GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID()) 
		
		#IF IS_DEBUG_BUILD
			IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_PLAYER_ON_ANY_FM_MISSION")
			ENDIF
			IF IS_THIS_IS_A_STRAND_MISSION()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_THIS_IS_A_STRAND_MISSION")
			ENDIF
			IF  IS_A_STRAND_MISSION_BEING_INITIALISED()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_A_STRAND_MISSION_BEING_INITIALISED")
			ENDIF
			IF FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT")
			ENDIF
			IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())		
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION")
			ENDIF
			IF IS_PLAYER_IN_CORONA()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_PLAYER_IN_CORONA")
			ENDIF
			IF IS_ON_HEIST_CORONA()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_ON_HEIST_CORONA")
			ENDIF
			IF IS_TRANSITION_SESSION_LAUNCHING()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_TRANSITION_SESSION_LAUNCHING")
			ENDIF
			IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - IS_THIS_A_ROUNDS_MISSION_FOR_CORONA")
			ENDIF
			IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION")
			ENDIF
			IF GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - ShouldAllowBGKickAtThisTime - GB_IS_PLAYER_ON_CONTRABAND_MISSION")
			ENDIF
		#ENDIF
	
		RETURN(FALSE)
	ENDIF
	RETURN(TRUE)
ENDFUNC

PROC RUN_BG_SCRIPT_KICK_CHECK(BG_SCRIPT_KICK_DETAILS &Data)

	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS ("sc_ignoreBGScriptKick")
	#ENDIF

		// is system disabled?
		IF NOT (g_sMPTunables.bdisableBGSK)

			INT iUpdateIntervalTime = 60000 // default
	
			#IF IS_DEBUG_BUILD
				IF (Data.iUpdateInterval > -1) 
					iUpdateIntervalTime = Data.iUpdateInterval
				ENDIF
			#ENDIF			
			

			// is it time to run an update check?
			IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), Data.UpdateTime)) > iUpdateIntervalTime)		
						
				CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - running check.")	
			
				// does my stat differ from the tunable?
				IF (g_sMPTunables.iexpectedBGSNumberBGSK != g_iBGScriptVersion)
				AND (g_sMPTunables.iexpectedBGSNumberBGSK != 0)
				
					CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - version mismatch. g_sMPTunables.iexpectedBGSNumberBGSK = ", g_sMPTunables.iexpectedBGSNumberBGSK, ", g_iBGScriptVersion = ", g_iBGScriptVersion)	
				
					// check if we are off mission
					IF ((g_sMPTunables.bdisablewhenonmissionBGSK = TRUE) AND ShouldAllowBGKickAtThisTime())
					OR (g_sMPTunables.bdisablewhenonmissionBGSK = FALSE)

					
						// find time (in minutes) until tunable posix time
					
						// current time
						INT iCurrentPosix
						INT iYear, iMonth, iDay, iHour, iMinute, iSecond
						GET_UTC_TIME(iYear, iMonth, iDay, iHour, iMinute, iSecond)	
						iCurrentPosix = CONVERT_DATE_TO_POSIX(iYear, iMonth, iDay, iHour, iMinute, iSecond)
				
						CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - iCurrentPosix = ", iCurrentPosix, ", g_sMPTunables.iposixtimeBGSK = ", g_sMPTunables.iposixtimeBGSK )
				
						// tunable time
						Data.iSecsToKickTime = g_sMPTunables.iposixtimeBGSK - iCurrentPosix
						Data.iMinsToKickTime = SECONDS_TO_MINUTES(Data.iSecsToKickTime)
						
						CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - secs until kick time = ", Data.iSecsToKickTime)
											
						IF (Data.iSecsToKickTime > 0)					
							IF Data.iSecsToKickTime < ((g_sMPTunables.itimes13minBGSK)* 60)							
								IF Data.iSecsToKickTime < ((g_sMPTunables.itimes2minBGSK) * 60)										
									IF Data.iSecsToKickTime < ((g_sMPTunables.itimes1minBGSK) * 60)	
										// do 5 min warning
										IF NOT (Data.bDoneWarning1)
											CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - do warning 1 ")
											PRINT_BG_SCRIPT_KICK_WARNING_TICKER(Data.iMinsToKickTime+1)
											PLAYSTATS_BACKGROUND_SCRIPT_ACTION("BG_SCRIPT_KICK", 1)
											Data.bDoneWarning1 = TRUE
										ENDIF
									ELSE
										// do 15 min warning
										IF NOT (Data.bDoneWarning2)
											CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - do warning 2 ")
											PRINT_BG_SCRIPT_KICK_WARNING_TICKER(Data.iMinsToKickTime+1)
											PLAYSTATS_BACKGROUND_SCRIPT_ACTION("BG_SCRIPT_KICK", 2)
											Data.bDoneWarning2 = TRUE											
										ENDIF
									ENDIF						
								ELSE								
									// do 30 min warning
									IF NOT (Data.bDoneWarning3)
										CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - do warning 3 ")
										PRINT_BG_SCRIPT_KICK_WARNING_TICKER(Data.iMinsToKickTime+1)
										PLAYSTATS_BACKGROUND_SCRIPT_ACTION("BG_SCRIPT_KICK", 3)
										Data.bDoneWarning3 = TRUE
									ENDIF
								ENDIF												
							ENDIF					
						ELSE
							CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - time expired! Kick!.")	
							SET_PLAYER_KICKED_TO_GO_OFFLINE_UNTIL_REBOOT(TRUE) 
							PLAYSTATS_BACKGROUND_SCRIPT_ACTION("BG_SCRIPT_KICK", 4)
						ENDIF
						
					
					ELSE
						CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - player is on mission or event.")				
					ENDIF
					
				ELSE
					CPRINTLN(DEBUG_BGSCRIPT_KICK, "RUN_BG_SCRIPT_KICK_CHECK - version numbers match.")				
				ENDIF
			
				Data.UpdateTime = GET_NETWORK_TIME()
			ENDIF
		
		ENDIF
		
	#IF IS_DEBUG_BUILD	
	ENDIF
	#ENDIF

ENDPROC


