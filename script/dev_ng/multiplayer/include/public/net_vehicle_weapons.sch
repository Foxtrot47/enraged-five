USING "net_include.sch"
USING "net_ambience.sch"

CONST_INT VEHICLE_MINE_LONG_PRESS							300

CONST_INT AIR_FLARE_BS_FIRED								0
CONST_INT AIR_COUNTERMEASURE_BS_MODEL_REQUESTED				1
CONST_INT AIR_COUNTERMEASURE_BS_TRIGGERED					2
CONST_INT AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED		3
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED				4
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED				5
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED			6
CONST_INT AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED			7
CONST_INT AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED		8
CONST_INT AIR_FLARE_BS_HELP_TEXT_SHOWN						9

CONST_INT AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED				0
CONST_INT AIR_BOMBS_BS_MODEL_REQUESTED						1
CONST_INT AIR_BOMBS_BS_CAMERA_CREATED						2
CONST_INT AIR_BOMBS_BS_DOORS_OPEN							3
CONST_INT AIR_BOMBS_BS_HELP_TEXT_SHOWN_1					4
CONST_INT AIR_BOMBS_BS_HELP_TEXT_SHOWN_2					5
CONST_INT AIR_BOMBS_BS_BOMB_FIRED							6
CONST_INT AIR_BOMBS_BS_CAMERA_CREATED_2						7
CONST_INT AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON				8

STRUCT VEHICLE_WEAPONS_DATA
	BOOL bMineHeld = FALSE
	BOOL bMineRequested = FALSE
	BOOL bFollowAircraftHeading = TRUE
	BOOL bBombDoorButtonReleased = FALSE
	BOOL bCounterMeasureButtonReleased = FALSE
	
	CAMERA_INDEX cBombCamera[2]
	
	ENTITY_INDEX entLastBomb
	
	INT iAirFlareBS
	INT iAirBombsBS
	INT iBombsFired = 0
	
	MODEL_NAMES eBombModel
	MODEL_NAMES eMineModel
	MODEL_NAMES eFlareModel
	
	PTFX_ID ptfxID
	
	SCRIPT_CONTROL_HOLD_TIMER vehicleMineControl
	
	SCRIPT_TIMER sFlareTimer
	SCRIPT_TIMER sMinePressDelay
	SCRIPT_TIMER sBombPressDelay
	SCRIPT_TIMER sCarpetBombDelay
	SCRIPT_TIMER sBombDoorPressDelay
	SCRIPT_TIMER sFlareCooldownSoundTimer
	SCRIPT_TIMER sWaterBombRefillTimer
	
	STRING strSmokeFX
	STRING strPTFXAsset
	
	VECTOR vGoToPos
	
	VEHICLE_INDEX vehUsedForCountermeasure
	
	WEAPON_TYPE eCachedBombWeapon = WEAPONTYPE_UNARMED
ENDSTRUCT

PROC CLEANUP_PLANE_FX_SMOKE(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWeaponsData.ptfxID)
		STOP_PARTICLE_FX_LOOPED(sVehicleWeaponsData.ptfxID)
		
		PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - STOP_PARTICLE_FX_LOOPED")
	ENDIF
	
	IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
		
		PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - AIR_FLARE_BS_FIRED FALSE")
	ENDIF
	
	IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		
		PRINTLN("[AIR_COUNTERMEASURES] CLEANUP_PLANE_FX_SMOKE - AIR_COUNTERMEASURE_BS_TRIGGERED FALSE")
	ENDIF
	
	sVehicleWeaponsData.vehUsedForCountermeasure = NULL
	
	SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(FALSE)
	
	RESET_NET_TIMER(g_sCountermeasurePressDelay)
ENDPROC

PROC CLEANUP_VEHICLE_WEAPONS(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	sVehicleWeaponsData.iAirFlareBS = 0
	
	CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
ENDPROC

PROC INITIALIZE_VEHICLE_WEAPONS(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	sVehicleWeaponsData.eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_01"))
	sVehicleWeaponsData.eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_ex_vehclemine"))
	sVehicleWeaponsData.eFlareModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_pi_flaregun_shell"))
	
	sVehicleWeaponsData.strSmokeFX = "scr_ar_trail_smoke"
	sVehicleWeaponsData.strPTFXAsset = "scr_ar_planes"
ENDPROC

FUNC WEAPON_TYPE GET_VEHICLE_MINE_TYPE(VEHICLE_INDEX vehTemp)
	IF IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
		SWITCH GET_VEHICLE_MOD(vehTemp, MOD_WING_R)
			CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_KINETIC_RC
			CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_EMP_RC
		ENDSWITCH
	ELIF SHOULD_USE_DPADR_FOR_MINE(vehTemp)
		IF IS_VEHICLE_MODEL(vehTemp, JB7002)
			SWITCH GET_VEHICLE_MOD(vehTemp, MOD_WING_R)
				CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SPIKE
				CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SLICK
			ENDSWITCH
		ELIF NOT IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
			SWITCH GET_VEHICLE_MOD(vehTemp, MOD_WING_R)
				CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_KINETIC
				CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SPIKE
				CASE 2	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_EMP
				CASE 3	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SLICK
				CASE 4	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_TAR
			ENDSWITCH
		ENDIF
	ELIF g_bEnablePowerUpMines
		SWITCH g_iPowerUpMineType
			CASE 0	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_KINETIC
			CASE 1	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SPIKE
			CASE 2	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_EMP
			CASE 3	RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SLICK
			CASE 4	RETURN WEAPONTYPE_DLC_VEHICLE_MINE
		ENDSWITCH
	#IF FEATURE_FIXER
	ELIF GET_FIXER_VEH_HELP_BIT(GET_ENTITY_MODEL(vehTemp)) >= 0
		RETURN WEAPONTYPE_DLC_VEHICLE_MINE_SLICK
	#ENDIF
	ENDIF
	
	RETURN WEAPONTYPE_DLC_VEHICLE_MINE
ENDFUNC

FUNC BOOL IS_PLAYER_VEHICLE_MINE_CONTROL_PRESSED(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX pedVehicle, CONTROL_TYPE control, CONTROL_ACTION action)
	IF IS_VEHICLE_DRIVEABLE(pedVehicle)
		IF IS_VEHICLE_MODEL(pedVehicle, DOMINATOR4)
			sVehicleWeaponsData.vehicleMineControl.action = action
			sVehicleWeaponsData.vehicleMineControl.control = control
			
			RETURN IS_CONTROL_HELD(sVehicleWeaponsData.vehicleMineControl)
		ELSE
			RETURN IS_DISABLED_CONTROL_JUST_PRESSED(control, action)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_SAW_BLADE(VEHICLE_INDEX vehTemp)
	IF DOES_ENTITY_EXIST(vehTemp)
		MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(vehTemp)
		
		IF eVehicleModel = ZR380
		OR eVehicleModel = ZR3802
		OR eVehicleModel = ZR3803
		OR eVehicleModel = DEATHBIKE
		OR eVehicleModel = DEATHBIKE2
		OR eVehicleModel = DEATHBIKE3
		OR eVehicleModel = IMPERATOR
		OR eVehicleModel = IMPERATOR2
		OR eVehicleModel = IMPERATOR3
		OR eVehicleModel = SLAMVAN4
		OR eVehicleModel = SLAMVAN5
		OR eVehicleModel = SLAMVAN6
			IF GET_VEHICLE_MOD(vehTemp, MOD_CHASSIS4) > -1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_VEHICLE_MINES(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	BOOL bUnloadMineModel = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sMinePressDelay)
		AND HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sMinePressDelay, 750)
			RESET_NET_TIMER(sVehicleWeaponsData.sMinePressDelay)
		ENDIF
		
		IF MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex) 
			
			IF DOES_ENTITY_EXIST(vehTemp)
			AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				IF CAN_VEHICLE_USE_MINES(vehTemp)
					IF NOT SHOULD_USE_DPADR_FOR_MINE(vehTemp)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
					ENDIF
					
					IF MPGlobals.sFreemodeCache.PlayerVehicleModel = RCBANDITO
						sVehicleWeaponsData.eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_Arena_Landmine_01b"))
					ELSE
						sVehicleWeaponsData.eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_VEHICLEMINE"))
					ENDIF
					
					IF IS_MODEL_VALID(sVehicleWeaponsData.eMineModel)
						IF NOT sVehicleWeaponsData.bMineRequested
							PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Requesting mine model")
							
							REQUEST_MODEL(sVehicleWeaponsData.eMineModel)
							
							sVehicleWeaponsData.bMineRequested = TRUE
						ELSE
							IF HAS_MODEL_LOADED(sVehicleWeaponsData.eMineModel)
								CONTROL_ACTION mineControl = INPUT_VEH_HORN
								
								IF SHOULD_USE_DPADR_FOR_MINE(vehTemp)
									mineControl = INPUT_CONTEXT
									
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
										mineControl = INPUT_FRONTEND_X
									ENDIF
								ENDIF
								
								sVehicleWeaponsData.vehicleMineControl.action = mineControl
								sVehicleWeaponsData.vehicleMineControl.control = PLAYER_CONTROL
								
								IF NOT sVehicleWeaponsData.bMineHeld
									IF IS_CONTROL_HELD(sVehicleWeaponsData.vehicleMineControl)
										sVehicleWeaponsData.bMineHeld = TRUE
									ENDIF
								ELSE
									IF NOT IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, mineControl)
									AND NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, mineControl)
									AND NOT IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, mineControl)
										sVehicleWeaponsData.bMineHeld = FALSE
									ENDIF
								ENDIF
								
								IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, mineControl)
								AND NOT sVehicleWeaponsData.bMineHeld
								AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
								AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
								AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
								AND NOT IS_PLAYER_IN_A_MOD_SHOP(PLAYER_ID())
								AND NOT IS_PAUSE_MENU_ACTIVE()
								AND NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sMinePressDelay)
									IF !DOES_VEHICLE_HAVE_MINE_AMO(vehTemp)
										PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - out of ammo")
										
										PLAY_SOUND_FRONTEND(-1, "rc_mines_empty", "DLC_AW_RCBandito_Mine_Sounds")
										
										START_NET_TIMER(sVehicleWeaponsData.sMinePressDelay)
										
										RETURN TRUE
									ENDIF
									
									VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
									
									DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehTemp, GET_ENTITY_MODEL(vehTemp), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
									
									VECTOR vB1 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vFrontBottomLeft, vFrontBottomRight, 0.0, 1.0, 0.5)
									VECTOR vB2 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vBackBottomLeft, vBackBottomRight, 0.0, 1.0, 0.5)
									
									vB1.z += 0.2
									vB2.z += 0.2
									
									FLOAT fOffset = 0.7
									
									IF GET_ENTITY_MODEL(vehTemp) = SPEEDO4
										fOffset = 1.0
									ENDIF
									
									IF DOES_VEHICLE_HAVE_SAW_BLADE(vehTemp)
										fOffset = 1.0
									ENDIF
									
									IF IS_VEHICLE_MODEL_A_GO_KART(GET_ENTITY_MODEL(vehTemp))
										fOffset = 1.1
									ENDIF
									
									VECTOR vB3 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, fOffset)
									
									vB1.z -= 0.2
									vB2.z -= 0.2
									
									VECTOR vB4 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, fOffset + 0.1)
									
									IF SHOULD_USE_DPADR_FOR_MINE(vehTemp)
									AND NOT IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
									AND (NETWORK_IS_ACTIVITY_SESSION() AND CONTENT_IS_USING_ARENA())
										SET_VEHICLE_BOMB_AMMO(vehTemp, GET_VEHICLE_BOMB_AMMO(vehTemp) - 1)
									ENDIF
									
									SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB3, vB4, 0, TRUE, GET_VEHICLE_MINE_TYPE(vehTemp), PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
									
									IF IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
										g_iRCVehicleMineNum++
									ENDIF
									
									IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
									AND NETWORK_IS_ACTIVITY_SESSION()
										g_bPowerUpMineUsed = TRUE
										PRINTLN("[VEHICLE_MINES][Power Ups] MAINTAIN_VEHICLE_MINES - g_bPowerUpMineUsed = TRUE")
									ENDIF
									
									PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Firing a mine")
									
									START_NET_TIMER(sVehicleWeaponsData.sMinePressDelay)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Model is invalid")
						
						IF IS_VEHICLE_MODEL(vehTemp, RCBANDITO)
							sVehicleWeaponsData.eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_Arena_Landmine_01b"))
						ELSE
							sVehicleWeaponsData.eMineModel = INT_TO_ENUM(MODEL_NAMES, HASH("W_EX_VEHICLEMINE"))
						ENDIF
						
						bUnloadMineModel = TRUE
					ENDIF
				ELSE
					bUnloadMineModel = TRUE
				ENDIF
			ELSE
				bUnloadMineModel = TRUE
			ENDIF
		ELSE
			bUnloadMineModel = TRUE
		ENDIF
	ELSE
		bUnloadMineModel = TRUE
	ENDIF
	
	IF bUnloadMineModel
		IF  sVehicleWeaponsData.bMineRequested
			PRINTLN("[VEHICLE_MINES] MAINTAIN_VEHICLE_MINES - Unloading mine model")
			
			IF IS_MODEL_VALID(sVehicleWeaponsData.eMineModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(sVehicleWeaponsData.eMineModel)
			ENDIF
			
			sVehicleWeaponsData.bMineRequested = FALSE
		ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC FIRE_CHAFF(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR g_bAllowCountermeasureAmmo
		IF GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) <= 0
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_CHAFF - out of ammo")
			
			PLAY_SOUND_FRONTEND(-1, "chaff_empty", "DLC_SM_Countermeasures_Sounds")
			
			START_NET_TIMER(g_sCountermeasurePressDelay)
			
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vPlaneRot = GET_ENTITY_ROTATION(vehPlane)
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	
	DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehPlane, GET_ENTITY_MODEL(vehPlane), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	FIRE_SPECIFIED_CHAFF(vehPlane, 0, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Mid Left
	FIRE_SPECIFIED_CHAFF(vehPlane, 1, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Mid Right
	FIRE_SPECIFIED_CHAFF(vehPlane, 2, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Far Left
	FIRE_SPECIFIED_CHAFF(vehPlane, 3, vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight, vPlaneRot) // Far Right
	
	PLAY_SOUND_FROM_ENTITY(-1, "chaff_released", vehPlane, "DLC_SM_Countermeasures_Sounds", TRUE)
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
		SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehPlane, FALSE, TRUE)
	ELSE
		BROADCAST_SET_CHAFF_LOCK_ON(ALL_PLAYERS_ON_SCRIPT(), TRUE)
	ENDIF
	
	REINIT_NET_TIMER(g_sLockOnBlock)
	
	SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR g_bAllowCountermeasureAmmo
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
			SET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane, GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) - 1)
		ELSE
			BROADCAST_DECREMENT_AIRCRAFT_AMMO(ALL_PLAYERS_ON_SCRIPT(), FALSE, TRUE)
		ENDIF
	ENDIF
	
	IF NOT MPGlobalsAmbience.bDisplayedChaffHelp
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		PRINT_HELP("CHAFF_FIRE")
		
		MPGlobalsAmbience.bDisplayedChaffHelp = TRUE
	ENDIF
	
	START_NET_TIMER(g_sCountermeasurePressDelay)
	
	PRINTLN("[AIR_COUNTERMEASURES] FIRE_CHAFF - Firing chaff")
ENDPROC

PROC MAINTAIN_USING_CHAFF(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
	AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, 1500)
		IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
		ENDIF
		
		IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		ENDIF
		
		RESET_NET_TIMER(g_sCountermeasurePressDelay)
	ENDIF
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
			PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_CHAFF - Requesting chaff vfx")
			
			REQUEST_NAMED_PTFX_ASSET("scr_sm_counter")
			
			SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
		ELSE
			IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_counter")
				IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
				AND NOT HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
					IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
					AND NOT IS_PAUSE_MENU_ACTIVE_EX()
					AND NOT IS_INTERACTION_MENU_OPEN()
					AND NOT IS_PHONE_ONSCREEN()
					AND NOT IS_BROWSER_OPEN()
					AND sVehicleWeaponsData.bCounterMeasureButtonReleased
						IF NOT HAS_NET_TIMER_STARTED(g_sLockOnBlock)
							SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
							
							PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_CHAFF - AIR_COUNTERMEASURE_BS_TRIGGERED TRUE")
						ELSE
							PLAY_SOUND_FRONTEND(-1, "chaff_cooldown", "DLC_SM_Countermeasures_Sounds")
							
							START_NET_TIMER(g_sCountermeasurePressDelay)
							
							IF NOT MPGlobalsAmbience.bDisplayedChaffDelayHelp
							AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("CHAFF_DELAY")
								
								MPGlobalsAmbience.bDisplayedChaffDelayHelp = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
			FIRE_CHAFF(sVehicleWeaponsData, vehPlane)
		ENDIF
	ENDIF
ENDPROC

PROC FIRE_AIR_FLARE(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	IF GET_VEHICLE_MOD(vehPlane, MOD_BUMPER_F) != 1
	AND !IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - no flare available")
		
		EXIT
	ENDIF
	
	IF GET_VEHICLE_MOD(vehPlane, MOD_GRILL) != 1
	AND IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - no flare available")
		
		EXIT
	ENDIF
	
	BOOL bOnlyShootTwo = FALSE
	
	IF IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		bOnlyShootTwo = TRUE
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR g_bAllowCountermeasureAmmo
		IF GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) <= 0
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - out of ammo")
			
			PLAY_SOUND_FRONTEND(-1, "flares_empty", "DLC_SM_Countermeasures_Sounds")
			
			SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
			
			START_NET_TIMER(g_sCountermeasurePressDelay)
			
			RESET_NET_TIMER(sVehicleWeaponsData.sFlareTimer)
			
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
			
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - Firing flares")
			
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	
	DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehPlane, GET_ENTITY_MODEL(vehPlane), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	VECTOR vRotation
	
	vRotation = GET_ENTITY_ROTATION(vehPlane, EULER_XYZ)
	vRotation.z = 0.0
	
	VECTOR vB1
	VECTOR vB2
	
	vB1 = <<GET_FLARE_X_POS(vehPlane, 1), GET_FLARE_Y_POS(vehPlane), GET_FLARE_Z_POS(vehPlane)>>
	vB2 = <<-6.0, -4.0, -0.2>>
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
	AND HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sFlareTimer, 250)
	AND IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		IF NOT bOnlyShootTwo
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
		
		SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
	ENDIF
	
	vB1 = <<GET_FLARE_X_POS(vehPlane, 2), GET_FLARE_Y_POS(vehPlane), GET_FLARE_Z_POS(vehPlane)>>
	
	IF IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		vB2 = <<-1.0, -4.0, -0.2>>
	ELSE
		vB2 = <<-3.0, -4.0, -0.2>>
	ENDIF
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2 , 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		
		START_NET_TIMER(sVehicleWeaponsData.sFlareTimer)
		
		SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		
		PLAY_SOUND_FROM_ENTITY(-1, "flares_released", vehPlane, "DLC_SM_Countermeasures_Sounds", TRUE)
	ENDIF
	
	vB1 = <<GET_FLARE_X_POS(vehPlane, 3), GET_FLARE_Y_POS(vehPlane), GET_FLARE_Z_POS(vehPlane)>>
	
	IF IS_VEHICLE_MODEL(vehPlane, OPPRESSOR2)
		vB2 = <<1.0, -4.0, -0.2>>
	ELSE
		vB2 = <<3.0, -4.0, -0.2>>
	ENDIF
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
		SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		
		SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
	ENDIF
	
	vB1 = <<GET_FLARE_X_POS(vehPlane, 4), GET_FLARE_Y_POS(vehPlane), GET_FLARE_Z_POS(vehPlane)>>
	vB2 = <<6.0, -4.0, -0.2>>
	
	ROTATE_VECTOR_FMMC(vB1, vRotation)
	ROTATE_VECTOR_FMMC(vB2, vRotation)
	
	vB1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB1)
	vB2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlane), GET_ENTITY_HEADING(vehPlane), vB2)
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
	AND IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
	AND HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sFlareTimer, 250)
		IF NOT bOnlyShootTwo
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB1, vB2, 0, TRUE, WEAPONTYPE_DLC_FLAREGUN, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
		
		SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
	ENDIF
	
	IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
		IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
		OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
		OR g_bAllowCountermeasureAmmo
			IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
				SET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane, GET_VEHICLE_COUNTERMEASURE_AMMO(vehPlane) - 1)
			ELSE
				BROADCAST_DECREMENT_AIRCRAFT_AMMO(ALL_PLAYERS_ON_SCRIPT(), FALSE, TRUE)
			ENDIF
		ENDIF
		
		SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
		
		START_NET_TIMER(g_sCountermeasurePressDelay)
		
		RESET_NET_TIMER(sVehicleWeaponsData.sFlareTimer)
		
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_ONE_FIRED)
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_TWO_FIRED)
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_THREE_FIRED)
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_FLARE_FOUR_FIRED)
		
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_AIR_FLARE - Firing flares")
	ENDIF
ENDPROC

PROC MAINTAIN_USING_AIR_FLARES(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	INT iFlareCooldown = 2500
	
	IF DOES_ENTITY_EXIST(vehPlane)
	AND GET_ENTITY_MODEL(vehPlane) = OPPRESSOR2
		iFlareCooldown = g_sMPTunables.iOPPRESSOR2_FLARE_COOLDOWN
	ENDIF
	
	IF g_bOverrideFlareCooldown
		iFlareCooldown = g_iFlareCooldown
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
	AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, iFlareCooldown)
		IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
		ENDIF
		
		IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
			CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		ENDIF
		
		RESET_NET_TIMER(g_sCountermeasurePressDelay)
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sFlareCooldownSoundTimer)
	AND HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sFlareCooldownSoundTimer, 500)
		RESET_NET_TIMER(sVehicleWeaponsData.sFlareCooldownSoundTimer)
	ENDIF
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		IF IS_MODEL_VALID(sVehicleWeaponsData.eFlareModel)
			IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
				PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_AIR_FLARES - Requesting flare model")
				
				REQUEST_MODEL(sVehicleWeaponsData.eFlareModel)
				
				SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
			ELSE
				IF HAS_MODEL_LOADED(sVehicleWeaponsData.eFlareModel)
					IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
						IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND sVehicleWeaponsData.bCounterMeasureButtonReleased
							RESET_NET_TIMER(sVehicleWeaponsData.sFlareTimer)
							
							SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
							
							PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_AIR_FLARES - AIR_COUNTERMEASURE_BS_TRIGGERED TRUE")		
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
			FIRE_AIR_FLARE(sVehicleWeaponsData,vehPlane)
		ELSE
			IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
			AND sVehicleWeaponsData.bCounterMeasureButtonReleased
				IF NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sFlareCooldownSoundTimer)
					START_NET_TIMER(sVehicleWeaponsData.sFlareCooldownSoundTimer)
					
					PLAY_SOUND_FRONTEND(-1, "flares_empty", "DLC_SM_Countermeasures_Sounds")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_SMOKE_OFFSET_VECTOR(VEHICLE_INDEX vehPlane)
	VECTOR vSmokeOffset = <<0.0, 0.0, 0.0>>
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800	vSmokeOffset = <<0.0, -3.0, -0.3>>	BREAK
		CASE MOGUL		vSmokeOffset = <<0.0, -5.0, 0.7>>	BREAK
		CASE ROGUE		vSmokeOffset = <<0.0, -7.0, 0.6>>	BREAK
		CASE STARLING	vSmokeOffset = <<0.0, -3.0, 0.6>>	BREAK
		CASE SEABREEZE	vSmokeOffset = <<0.0, -3.0, 0.2>>	BREAK
		CASE TULA		vSmokeOffset = <<0.0, -5.0, 0.7>>	BREAK
		CASE BOMBUSHKA	vSmokeOffset = <<0.0, -21.0, 4.5>>	BREAK
		CASE HUNTER		vSmokeOffset = <<0.0, -6.0, 0.0>>	BREAK
		CASE NOKOTA		vSmokeOffset = <<0.0, -4.0, 0.0>>	BREAK
		CASE PYRO		vSmokeOffset = <<0.0, -3.0, 0.3>>	BREAK
		CASE MOLOTOK	vSmokeOffset = <<0.0, -5.0, 0.3>>	BREAK
		CASE HAVOK		vSmokeOffset = <<0.0, -4.0, 0.3>>	BREAK
		CASE ALPHAZ1	vSmokeOffset = <<0.0, -2.5, -0.2>>	BREAK
		CASE MICROLIGHT	vSmokeOffset = <<0.0, -2.0, 0.5>>	BREAK
		CASE HOWARD		vSmokeOffset = <<0.0, -3.5, 0.5>>	BREAK
		CASE AVENGER	vSmokeOffset = <<0.0, -10.0, 1.0>>	BREAK
		CASE AKULA		vSmokeOffset = <<0.0, -6.0, 0.0>>	BREAK
		CASE THRUSTER	vSmokeOffset = <<0.0, -0.5, 0.0>>	BREAK
		CASE OPPRESSOR2 vSmokeOffset = <<0.0, -1.2, -0.1>>	BREAK
		CASE VOLATOL	vSmokeOffset = <<0.0, -20.0, 1.0>>	BREAK
		CASE ALKONOST	vSmokeOffset = <<0.0, -26.0, 0.3>>	BREAK
	ENDSWITCH
	
	RETURN vSmokeOffset
ENDFUNC

FUNC FLOAT GET_SMOKE_SCALE(VEHICLE_INDEX vehPlane)
	FLOAT vScale = 1.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE ALPHAZ1	vScale = 0.82	BREAK
		CASE BOMBUSHKA  vScale = 0.7	BREAK
		CASE AVENGER  	vScale = 0.7	BREAK
		CASE THRUSTER	vScale = 0.7	BREAK
		CASE VOLATOL	vScale = 0.7	BREAK
		CASE ALKONOST	vScale = 0.7	BREAK
		DEFAULT 		vScale = 1.0	BREAK
	ENDSWITCH
	
	RETURN vScale
ENDFUNC

PROC FIRE_SMOKE_FX(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWeaponsData.ptfxID)
		IF NOT VEHICLE_SUITABLE_FOR_AIR_FLARES(vehPlane)
			CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
			
			PRINTLN("[AIR_COUNTERMEASURES] FIRE_SMOKE_FX - VEHICLE_SUITABLE_FOR_AIR_FLARES - FALSE")
			
			EXIT
		ENDIF
		
		INT iR, iG, iB
		
		FLOAT fR, fG, fB
		
		GET_VEHICLE_TYRE_SMOKE_COLOR(vehPlane, iR,iG, iB)
		
		SWITCH GET_TYRE_SMOKE_COLOUR_ENUM_FROM_RGB(iR,iG, iB)
			CASE TYRE_SMOKE_WHITE
				fR = 255
				fG = 255
				fB = 255
			BREAK
			
			CASE TYRE_SMOKE_ORANGE
				fR = 255
				fG = 165
				fB = 0
			BREAK
			
			CASE TYRE_SMOKE_YELLOW
				fR = 255
				fG = 255
				fB = 0
			BREAK
			
			CASE TYRE_SMOKE_BLUE
				fR = 0
				fG = 0
				fB = 255
			BREAK
			
			CASE TYRE_SMOKE_RED
				fR = 255
				fG = 0
				fB = 0
			BREAK
			
			CASE TYRE_SMOKE_BLACK
		 		fR = 0
				fG = 0
				fB = 0
			BREAK
			
			CASE TYRE_SMOKE_BUSINESS_PURPLE
				fR = 128
				fG = 0
				fB = 128
			BREAK
			
			CASE TYRE_SMOKE_BUSINESS_GREEN
				fR = 0
				fG = 255
				fB = 0
			BREAK
			
			CASE TYRE_SMOKE_HIPSTER_PINK
				fR = 255
				fG = 0
				fB = 255
			BREAK
			
			CASE TYRE_SMOKE_HIPSTER_BROWN
				fR = 0
				fG = 255
				fB = 255
			BREAK
		ENDSWITCH
		
		USE_PARTICLE_FX_ASSET(sVehicleWeaponsData.strPTFXAsset)
		
		sVehicleWeaponsData.ptfxID = START_NETWORKED_PARTICLE_FX_LOOPED_ON_ENTITY_BONE(sVehicleWeaponsData.strSmokeFX, vehPlane, GET_SMOKE_OFFSET_VECTOR(vehPlane), <<0.0,0.0,0.0>>, DEFAULT, GET_SMOKE_SCALE(vehPlane), DEFAULT, DEFAULT, DEFAULT, fR/255, fG/255, fB/255, TRUE)
		
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_SMOKE_FX - Starting START_PARTICLE_FX_LOOPED_ON_ENTITY ID: ", NATIVE_TO_INT(sVehicleWeaponsData.ptfxID))
		
		PRINTLN("[AIR_COUNTERMEASURES] FIRE_SMOKE_FX - iR: ",TO_FLOAT(iR) , " iG: ", TO_FLOAT(iG), " iB: ", TO_FLOAT(iB) )
		
		SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
		
		SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(TRUE)
		
		START_NET_TIMER(g_sCountermeasurePressDelay)
	ENDIF
ENDPROC

PROC MAINTAIN_USING_SMOKE(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	REQUEST_NAMED_PTFX_ASSET(sVehicleWeaponsData.strPTFXAsset)
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
		IF HAS_NAMED_PTFX_ASSET_LOADED(sVehicleWeaponsData.strPTFXAsset)
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWeaponsData.ptfxID)
				IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
					IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
					AND NOT IS_PAUSE_MENU_ACTIVE_EX()
					AND NOT IS_INTERACTION_MENU_OPEN()
					AND NOT IS_PHONE_ONSCREEN()
					AND NOT IS_BROWSER_OPEN()
					AND sVehicleWeaponsData.bCounterMeasureButtonReleased
						SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
						
						PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_SMOKE - AIR_COUNTERMEASURE_BS_TRIGGERED TRUE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
			FIRE_SMOKE_FX(sVehicleWeaponsData, vehPlane)
		ELSE
			IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWeaponsData.ptfxID)
				IF IS_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
				AND NOT IS_PAUSE_MENU_ACTIVE_EX()
				AND NOT IS_INTERACTION_MENU_OPEN()
				AND NOT IS_PHONE_ONSCREEN()
				AND NOT IS_BROWSER_OPEN()
				AND sVehicleWeaponsData.bCounterMeasureButtonReleased
					CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
					PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_USING_SMOKE - CLEANUP_PLANE_FX_SMOKE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_COUNTERMEASURE_HELP_TEXT(VEHICLE_INDEX vVehicle)
	STRING sHelpText = ""
	
	IF CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vVehicle)
		IF VEHICLE_SUITABLE_FOR_AIR_FLARES(vVehicle)
			IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) = 1
				RETURN "FLARE_HELP"
			ENDIF
			
			IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
				IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) = 1
					RETURN "FLARE_HELP"
				ENDIF
			ENDIF
		ENDIF
		
		IF VEHICLE_SUITABLE_FOR_CHAFF(vVehicle)
			IF GET_VEHICLE_MOD(vVehicle, MOD_BUMPER_F) = 0
				RETURN "CHAFF_HELP"
			ENDIF
			
			IF IS_VEHICLE_MODEL(vVehicle, OPPRESSOR2)
				IF GET_VEHICLE_MOD(vVehicle, MOD_GRILL) = 0
					RETURN "CHAFF_HELP"
				ENDIF
			ENDIF
		ENDIF
		
		IF VEHICLE_SUITABLE_FOR_SMOKE(vVehicle)
			IF IS_TOGGLE_MOD_ON(vVehicle, MOD_TOGGLE_TYRE_SMOKE)
				RETURN "SMOKE_HELP"
			ENDIF
		ENDIF
	ENDIF
	
	RETURN sHelpText
ENDFUNC

PROC MAINTAIN_AIR_COUNTERMEASURES(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	BOOL bUnloadBombModel = FALSE
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF NOT SHOULD_VEHICLE_FIRE_SMOKE()
			INT iTimer
			
			IF SHOULD_VEHICLE_FIRE_FLARE()
				iTimer = 2500
				
				IF MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
					VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex) 
					
					IF DOES_ENTITY_EXIST(vehTemp)
					AND GET_ENTITY_MODEL(vehTemp) = OPPRESSOR2
						iTimer = g_sMPTunables.iOPPRESSOR2_FLARE_COOLDOWN
					ENDIF
				ENDIF
				
				IF g_bOverrideFlareCooldown
					iTimer = g_iFlareCooldown
				ENDIF
			ELSE
				iTimer = 1500
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
			AND HAS_NET_TIMER_EXPIRED(g_sCountermeasurePressDelay, iTimer)
				RESET_NET_TIMER(g_sCountermeasurePressDelay)
				
				IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
					CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_FIRED)
				ENDIF
				
				IF IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
					CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_TRIGGERED)
				ENDIF
			ENDIF
		ENDIF
		
		IF MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex) 
			
			IF DOES_ENTITY_EXIST(vehTemp)
				sVehicleWeaponsData.vehUsedForCountermeasure = vehTemp
				
				MAINTAIN_VEHICLE_COUNTERMEASURE_HELP_TEXT(vehTemp)
				
				IF SHOULD_VEHICLE_FIRE_CHAFF(vehTemp)
					INT iDuration = g_sMPTunables.iSMUGGLER_CHAFF_DURATION
					
					IF GET_ENTITY_MODEL(vehTemp) = OPPRESSOR2
						iDuration = g_sMPTunables.iOPPRESSOR2_CHAFF_DURATION
					ENDIF
					
					IF HAS_NET_TIMER_STARTED(g_sLockOnBlock)
					AND HAS_NET_TIMER_EXPIRED(g_sLockOnBlock, iDuration)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
							SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON_SYNCED(vehTemp, TRUE, TRUE)
						ELSE
							IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED)
								SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED)
								
								BROADCAST_SET_CHAFF_LOCK_ON(ALL_PLAYERS_ON_SCRIPT(), FALSE)
							ENDIF
						ENDIF
						
						INT iChaffCooldown = g_sMPTunables.iSMUGGLER_CHAFF_DURATION + g_sMPTunables.iSMUGGLER_CHAFF_COOLDOWN
						
						IF GET_ENTITY_MODEL(vehTemp) = OPPRESSOR2
							iChaffCooldown = g_sMPTunables.iOPPRESSOR2_CHAFF_DURATION + g_sMPTunables.iOPPRESSOR2_CHAFF_COOLDOWN
						ENDIF
						
						IF g_bOverrideChaffCooldown
							iChaffCooldown = g_sMPTunables.iSMUGGLER_CHAFF_DURATION + g_iChaffCooldown
						ENDIF
						
						IF HAS_NET_TIMER_STARTED(g_sLockOnBlock)
						AND HAS_NET_TIMER_EXPIRED(g_sLockOnBlock, iChaffCooldown)
							RESET_NET_TIMER(g_sLockOnBlock)
							
							CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_LOCKON_STOPPED)
						ENDIF
						
						PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - Resetting vehicle lock-on")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
			AND CAN_LOCAL_PLAYER_USE_COUNTERMEASURE(vehTemp)
			AND CAN_VEHICLE_HAVE_AIR_COUNTERMEASURES(vehTemp)
				IF SHOULD_VEHICLE_FIRE_FLARE()
				OR SHOULD_VEHICLE_FIRE_CHAFF(vehTemp)
					IF CAN_VEHICLE_USE_AIR_COUNTERMEASURES(vehTemp)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
						
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
						OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehTemp)
						OR SHOULD_ENABLE_COUNTERMEASURE_FOR_THIS_VEHICLE_MODEL(GET_ENTITY_MODEL(vehTemp))
							IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP(GET_COUNTERMEASURE_HELP_TEXT(vehTemp))
								
								SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							ENDIF
						ENDIF
						
						IF SHOULD_VEHICLE_FIRE_FLARE()
							MAINTAIN_USING_AIR_FLARES(sVehicleWeaponsData, vehTemp)
						ELIF SHOULD_VEHICLE_FIRE_CHAFF(vehTemp)
							MAINTAIN_USING_CHAFF(sVehicleWeaponsData, vehTemp)
						ENDIF
					ELSE
						IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
							RESET_NET_TIMER(g_sCountermeasurePressDelay)
						ENDIF
						
						bUnloadBombModel = TRUE
					ENDIF
				ELIF SHOULD_VEHICLE_FIRE_SMOKE()
					IF CAN_VEHICLE_USE_AIR_SMOKES(vehTemp)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
						
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehTemp)
						OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehTemp)
							IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP(GET_COUNTERMEASURE_HELP_TEXT(vehTemp))
								
								SET_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_FLARE_BS_HELP_TEXT_SHOWN)
							ENDIF
						ENDIF
						
						MAINTAIN_USING_SMOKE(sVehicleWeaponsData, vehTemp)
					ELSE
						IF HAS_NET_TIMER_STARTED(g_sCountermeasurePressDelay)
							RESET_NET_TIMER(g_sCountermeasurePressDelay)
						ENDIF
						
						CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
						
						bUnloadBombModel = TRUE
					ENDIF
				ENDIF
			ELSE
				CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
				
				bUnloadBombModel = TRUE
			ENDIF
		ELSE
		  	SET_PLAYER_AIRCRAFT_SMOKE_ACTIVE(FALSE)
			
			sVehicleWeaponsData.iAirFlareBS = 0
			
			CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
			
			bUnloadBombModel = TRUE
		ENDIF
	ELSE
		sVehicleWeaponsData.iAirFlareBS = 0
		
		CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
		
		bUnloadBombModel = TRUE
	ENDIF
	
	IF !IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
	OR NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWeaponsData.ptfxID)
			CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
			
			PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - player left the session cleanup")
		ENDIF	
	ENDIF
	
	IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	OR (DOES_ENTITY_EXIST(sVehicleWeaponsData.vehUsedForCountermeasure) AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), sVehicleWeaponsData.vehUsedForCountermeasure))
	OR !DOES_ENTITY_EXIST(sVehicleWeaponsData.vehUsedForCountermeasure)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sVehicleWeaponsData.ptfxID)
			CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
			
			PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - player not in vehicle")
		ENDIF
	ENDIF
	
	IF bUnloadBombModel
	AND IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
		PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - Unloading bomb model")
		
		CLEANUP_PLANE_FX_SMOKE(sVehicleWeaponsData)
		
		IF IS_MODEL_VALID(sVehicleWeaponsData.eFlareModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(sVehicleWeaponsData.eFlareModel)
		ENDIF
		
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_MODEL_REQUESTED)
	ENDIF
	
	IF bUnloadBombModel
	AND IS_BIT_SET(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
		PRINTLN("[AIR_COUNTERMEASURES] MAINTAIN_AIR_COUNTERMEASURES - Unloading chaff vfx")
		
		IF HAS_NAMED_PTFX_ASSET_LOADED("scr_sm_counter")
			REMOVE_NAMED_PTFX_ASSET("scr_sm_counter")
		ENDIF
		
		CLEAR_BIT(sVehicleWeaponsData.iAirFlareBS, AIR_COUNTERMEASURE_BS_CHAFF_PTFX_REQUESTED)
	ENDIF
	
	IF IS_CONTROL_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_COUNTER)
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_INTERACTION_MENU_OPEN()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_BROWSER_OPEN()
		sVehicleWeaponsData.bCounterMeasureButtonReleased = TRUE
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_USING_WATER_BOMBS(VEHICLE_INDEX vehPlane, INT &iVehicleIndex)
	
	//Water bombs are currently only available in missions.
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		RETURN FALSE
	ENDIF
	
	//Water bombs are only available on the Tula
	IF GET_ENTITY_MODEL(vehPlane) != TULA
		RETURN FALSE
	ENDIF
	
	iVehicleIndex = IS_ENTITY_A_MISSION_CREATOR_ENTITY(vehPlane)
	IF iVehicleIndex < 0
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVehicleIndex].iVehBitsetNine, ciFMMC_VEHICLE9_WaterBombs)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC FLOAT GET_BOMB_OFFSET(VEHICLE_INDEX vehPlane)
	FLOAT fBombOffset = 0.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
			fBombOffset = 0.5
		BREAK
		
		CASE MOGUL
			fBombOffset = 0.45
		BREAK
		
		CASE ROGUE
			fBombOffset = 0.46
		BREAK
		
		CASE STARLING
			fBombOffset = 0.55
		BREAK
		
		CASE SEABREEZE
			fBombOffset = 0.5
		BREAK
		
		CASE TULA
			fBombOffset = 0.6
		BREAK
		
		CASE BOMBUSHKA
			fBombOffset = 0.43
		BREAK
		
		CASE HUNTER
			fBombOffset = 0.5
		BREAK
		
		CASE AVENGER
			fBombOffset = 0.36
		BREAK
		
		CASE AKULA
			fBombOffset = 0.4
		BREAK
		
		CASE VOLATOL
			fBombOffset = 0.54
		BREAK
		
		CASE STRIKEFORCE
			fBombOffset = 0.7
		BREAK
		
		CASE ALKONOST
			fBombOffset = 0.4
		BREAK
	ENDSWITCH
	
	RETURN fBombOffset
ENDFUNC

PROC FIRE_AIR_BOMB(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane, BOOL bLastBomb)
	IF GET_VEHICLE_MOD(vehPlane, MOD_WING_R) < 0
		PRINTLN("[AIR_BOMBS] FIRE_AIR_BOMB - no bombs available")
		
		EXIT
	ENDIF
	
	INT iCreatorIndex
	BOOL bUsingWaterBombs = IS_VEHICLE_USING_WATER_BOMBS(vehPlane, iCreatorIndex)
	BOOL bUsingWaterBombAmmo = FALSE
	IF bUsingWaterBombs
		bUsingWaterBombAmmo = (g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCreatorIndex].iWaterBombCapacity != 0)
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR bUsingWaterBombAmmo
		IF GET_VEHICLE_BOMB_AMMO(vehPlane) <= 0
			PRINTLN("[AIR_BOMBS] FIRE_AIR_BOMB - out of ammo")
			
			IF bLastBomb
				PLAY_SOUND_FRONTEND(-1, "bombs_empty", "DLC_SM_Bomb_Bay_Bombs_Sounds")
			ENDIF
			
			START_NET_TIMER(sVehicleWeaponsData.sBombPressDelay)
			
			EXIT
		ENDIF
	ENDIF
	
	VECTOR vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight
	
	DETERMINE_4_BOTTOM_VECTORS_FOR_VEHICLE_WEAPON(vehPlane, GET_ENTITY_MODEL(vehPlane), vFrontBottomLeft, vFrontBottomRight, vBackBottomLeft, vBackBottomRight)
	
	VECTOR vB1 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vFrontBottomLeft, vFrontBottomRight, 0.0, 1.0, 0.5)
	VECTOR vB2 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vBackBottomLeft, vBackBottomRight, 0.0, 1.0, 0.5)
	
	vB1.z += 0.4
	vB2.z += 0.4
	
	VECTOR vB3 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, GET_BOMB_OFFSET(vehPlane))
	
	vB1.z -= 0.2
	vB2.z -= 0.2
	
	VECTOR vB4 = GET_INTERP_POINT_VECTOR_FOR_VEHICLE_WEAPON(vB1, vB2, 0.0, 1.0, GET_BOMB_OFFSET(vehPlane) - 0.0001)
	
	WEAPON_TYPE bombType
	
	SWITCH GET_VEHICLE_MOD(vehPlane, MOD_WING_R)
		CASE 0
			IF bUsingWaterBombs
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_WATER
			ELIF IS_VEHICLE_MODEL(vehPlane, VOLATOL)
				IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SPYPLANE)
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_STANDARD_WIDE
				ELSE
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
				ENDIF
			ELSE
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
			ENDIF
		BREAK
		
		CASE 1
			bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY
		BREAK
		
		CASE 2
			bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_GAS
		BREAK
		
		CASE 3
			bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_CLUSTER
		BREAK
	ENDSWITCH
	
	SHOOT_SINGLE_BULLET_BETWEEN_COORDS_IGNORE_ENTITY_NEW(vB3, vB4, 0, DEFAULT, bombType, PLAYER_PED_ID(), DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPlane)
	OR IS_VEHICLE_A_PERSONAL_ARMORY_AIRCRAFT(vehPlane)
	OR bUsingWaterBombAmmo
		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
			SET_VEHICLE_BOMB_AMMO(vehPlane, GET_VEHICLE_BOMB_AMMO(vehPlane) - 1)
		ELSE
			BROADCAST_DECREMENT_AIRCRAFT_AMMO(ALL_PLAYERS_ON_SCRIPT(), TRUE, FALSE)
		ENDIF
	ENDIF
	
	IF bLastBomb
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_BOMB_FIRED)
	ENDIF
	
	PLAY_SOUND_FRONTEND(-1, "bomb_deployed", "DLC_SM_Bomb_Bay_Bombs_Sounds")
	
	PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - Firing a bomb")
	
	START_NET_TIMER(sVehicleWeaponsData.sBombPressDelay)
ENDPROC

FUNC FLOAT GET_BOMB_Y_POS(VEHICLE_INDEX vehPlane)
	FLOAT fYPos = 0.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
			fYPos = 1.3
		BREAK
		
		CASE MOGUL
			fYPos = 1.25
		BREAK
		
		CASE ROGUE
			fYPos = -0.5
		BREAK
		
		CASE STARLING
			fYPos = 3.0
		BREAK
		
		CASE SEABREEZE
			fYPos = 1.0
		BREAK
		
		CASE TULA
			fYPos = 0.5
		BREAK
		
		CASE BOMBUSHKA
			fYPos = -2.0
		BREAK
		
		CASE HUNTER
			fYPos = 4.0
		BREAK
		
		CASE AVENGER
			fYPos = 0.9
		BREAK
		
		CASE AKULA
			fYPos = 6.5
		BREAK
		
		CASE VOLATOL
		CASE ALKONOST
			fYPos = -9.0
		BREAK
		
		CASE STRIKEFORCE
			fYPos = -1.5
		BREAK
	ENDSWITCH
	
	RETURN fYPos
ENDFUNC

FUNC FLOAT GET_BOMB_CAM_Z(VEHICLE_INDEX vehPlane)
	FLOAT fZPos = 0.0
	
	SWITCH GET_ENTITY_MODEL(vehPlane)
		CASE CUBAN800
		CASE MOGUL
		CASE ROGUE
		CASE STARLING
		CASE SEABREEZE
		CASE TULA
		CASE BOMBUSHKA
		CASE HUNTER
		CASE AKULA
		CASE STRIKEFORCE
			fZPos = -1.5
		BREAK
		
		CASE AVENGER
			fZPos = -3.0
		BREAK
		
		CASE VOLATOL
		CASE ALKONOST
			fZPos = -2.0
		BREAK
	ENDSWITCH
	
	RETURN fZPos
ENDFUNC

PROC CLEANUP_BOMB_CAMERA(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
		
		IF DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[0])
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			DESTROY_CAM(sVehicleWeaponsData.cBombCamera[0])
		ENDIF
		
		IF DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[1])
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			DESTROY_CAM(sVehicleWeaponsData.cBombCamera[1])
			
			CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("DLC_SM_Bomb_Bay_View_Scene")
			STOP_AUDIO_SCENE("DLC_SM_Bomb_Bay_View_Scene")
		ENDIF
		
		sVehicleWeaponsData.bFollowAircraftHeading = TRUE
		
		CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
		
		PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - g_bBombCamActive = FALSE")
		
		g_bBombCamActive = FALSE
	ENDIF
ENDPROC

FUNC INT GET_CARPET_BOMB_COUNT(VEHICLE_INDEX vehPlane)
	IF IS_VEHICLE_MODEL(vehPlane, VOLATOL)
	AND IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SPYPLANE)
		RETURN 10
	ENDIF
	
	INT iCreatorId
	IF IS_VEHICLE_USING_WATER_BOMBS(vehPlane, iCreatorId)
		RETURN ciVEHICLE_WATER_BOMB_MAX_CAPACITY
	ENDIF
	
	RETURN 5
ENDFUNC

PROC PROCESS_WATER_BOMB_HUD(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, INT iMaxWaterBombs, INT iWaterBombs, INT iRefillRate)

	IF g_bEndOfMissionCleanUpHUD
		EXIT
	ENDIF

	IF iMaxWaterBombs = 0 //Unlimited
		EXIT
	ENDIF
	
	INT i
	HUD_COLOURS eHudColours[ciVEHICLE_WATER_BOMB_MAX_CAPACITY]
	FOR i = 0 TO ciVEHICLE_WATER_BOMB_MAX_CAPACITY - 1
		IF i < iWaterBombs
			eHudColours[i] = HUD_COLOUR_BLUELIGHT
			RELOOP
		ENDIF
		eHudColours[i] = HUD_COLOUR_BLACK
	ENDFOR
	
	DRAW_ONE_PACKAGES_EIGHT_HUD(iMaxWaterBombs, "VMOD_WB_WR", FALSE, 
				eHudColours[0], eHudColours[1], eHudColours[2], eHudColours[3], eHudColours[4], 
				eHudColours[5], eHudColours[6], eHudColours[7], PICK_INT(iWaterBombs = 0, HIGHEST_INT, -1), 
				DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDORDER_TENTHBOTTOM)
	
	IF HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sWaterBombRefillTimer)
		DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sVehicleWeaponsData.sWaterBombRefillTimer), iRefillRate,
				"VMOD_WB_RE", HUD_COLOUR_BLUELIGHT, DEFAULT, HUDORDER_TOP)
	ENDIF
	
ENDPROC

FUNC BOOL CAN_WATER_BOMBS_BE_REFILLED(VEHICLE_INDEX vehPlane, INT iMaxWaterBombs, INT iWaterBombs)
	
	IF iMaxWaterBombs = 0 //Unlimited
		RETURN FALSE
	ENDIF
	
	IF iMaxWaterBombs = iWaterBombs //Full
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_IN_WATER(vehPlane)
		RETURN FALSE
	ENDIF
	
	IF GET_ENTITY_SPEED(vehPlane) > 20.0
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_UPRIGHT(vehPlane)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_WATER_BOMB_REFILLING(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane, INT iMaxWaterBombs, INT iWaterBombs, INT iRefillRate)
	
	IF NOT CAN_WATER_BOMBS_BE_REFILLED(vehPlane, iMaxWaterBombs, iWaterBombs)
		IF HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sWaterBombRefillTimer)
			RESET_NET_TIMER(sVehicleWeaponsData.sWaterBombRefillTimer)
		ENDIF
		
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sWaterBombRefillTimer, iRefillRate)
		SET_VEHICLE_BOMB_AMMO(vehPlane, iWaterBombs + 1)
		PLAY_SOUND_FRONTEND(-1, "collect_water", "dlc_sum20_yacht_missions_ah_sounds")
		RESET_NET_TIMER(sVehicleWeaponsData.sWaterBombRefillTimer)
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sWaterBombRefillTimer)
		REINIT_NET_TIMER(sVehicleWeaponsData.sWaterBombRefillTimer)
	ENDIF
	
ENDPROC

PROC PROCESS_WATER_BOMBS(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane, INT iFMMCVehID)
	
	INT iMaxWaterBombs = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iFMMCVehID].iWaterBombCapacity
	INT iWaterBombs = GET_VEHICLE_BOMB_AMMO(vehPlane)
	INT iRefillRate = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iFMMCVehID].iWaterBombRefillRate

	PROCESS_WATER_BOMB_REFILLING(sVehicleWeaponsData, vehPlane, iMaxWaterBombs, iWaterBombs, iRefillRate)
	PROCESS_WATER_BOMB_HUD(sVehicleWeaponsData, iMaxWaterBombs, iWaterBombs, iRefillRate)
	
ENDPROC

PROC MAINTAIN_USING_AIR_BOMBS(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData, VEHICLE_INDEX vehPlane)
	VECTOR vBombPos
	VECTOR vPlaneCoords = GET_ENTITY_COORDS(vehPlane)
	FLOAT fHeading = GET_ENTITY_HEADING(vehPlane)
	INT iCreatorId
	BOOL bUsingWaterBombs = IS_VEHICLE_USING_WATER_BOMBS(vehPlane, iCreatorId)
	IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_BOMB_FIRED)
	AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
		WEAPON_TYPE bombType
		
		SWITCH GET_VEHICLE_MOD(vehPlane, MOD_WING_R)
			CASE 0
				IF bUsingWaterBombs
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_WATER
				ELiF IS_VEHICLE_MODEL(vehPlane, VOLATOL)
					IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SPYPLANE)
						bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_STANDARD_WIDE
					ELSE
						bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
					ENDIF
				ELSE
					bombType = WEAPONTYPE_DLC_VEHICLE_BOMB
				ENDIF
			BREAK
			
			CASE 1
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_INCENDIARY
			BREAK
			
			CASE 2
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_GAS
			BREAK
			
			CASE 3
				bombType = WEAPONTYPE_DLC_VEHICLE_BOMB_CLUSTER
			BREAK
		ENDSWITCH
		
		IF GET_PROJECTILE_OF_PROJECTILE_TYPE_WITHIN_DISTANCE(PLAYER_PED_ID(), bombType, 25.0, vBombPos, sVehicleWeaponsData.entLastBomb)
			IF DOES_ENTITY_EXIST(sVehicleWeaponsData.entLastBomb)
			AND DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[0])
				IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
					IF DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[1])
						DESTROY_CAM(sVehicleWeaponsData.cBombCamera[1])
					ENDIF
					
					sVehicleWeaponsData.cBombCamera[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					SET_CAM_FOV(sVehicleWeaponsData.cBombCamera[1], 65.0)
					ATTACH_CAM_TO_ENTITY(sVehicleWeaponsData.cBombCamera[1], vehPlane, <<0.0, GET_BOMB_Y_POS(vehPlane), GET_BOMB_CAM_Z(vehPlane)>>)
					SET_CAM_ROT(sVehicleWeaponsData.cBombCamera[1], <<-105.0, 0.0, NORMALIZE_ANGLE(fHeading)>>)
					SET_CAM_ACTIVE_WITH_INTERP(sVehicleWeaponsData.cBombCamera[1], sVehicleWeaponsData.cBombCamera[0], 3000)
					
					SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
					
					sVehicleWeaponsData.bFollowAircraftHeading = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_BOMB_FIRED)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlane)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
		IF GET_ARE_BOMB_BAY_DOORS_OPEN(vehPlane)
			SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
		IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_2)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			PRINT_HELP(PICK_STRING(bUsingWaterBombs, "WB_CAM_HELP", "BOMB_CAM_HELP"))
			
			SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_2)
		ENDIF
		
		IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY)
		OR (IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AutomaticBombBayDoors)))
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND NOT IS_INTERACTION_MENU_OPEN()
		AND NOT IS_PHONE_ONSCREEN()
		AND NOT IS_BROWSER_OPEN()
		AND sVehicleWeaponsData.bBombDoorButtonReleased
			IF NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sBombDoorPressDelay)
				START_NET_TIMER(sVehicleWeaponsData.sBombDoorPressDelay)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sBombDoorPressDelay, 500)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(PICK_STRING(bUsingWaterBombs, "WB_CAM_HELP", "BOMB_CAM_HELP"))
						CLEAR_HELP()
					ENDIF
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
						OPEN_BOMB_BAY_DOORS(vehPlane)
					ELSE
						BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), TRUE)
					ENDIF
					
					REINIT_NET_TIMER(sVehicleWeaponsData.sBombPressDelay)
					RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
					RESET_NET_TIMER(sVehicleWeaponsData.sBombDoorPressDelay)
					
					SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					
					sVehicleWeaponsData.bBombDoorButtonReleased = FALSE
					sVehicleWeaponsData.bCounterMeasureButtonReleased = FALSE
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(sVehicleWeaponsData.sBombDoorPressDelay)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_1)
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
			PRINT_HELP(PICK_STRING(bUsingWaterBombs, "WB_HELP", "BOMB_HELP"))
			SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_HELP_TEXT_SHOWN_1)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		IF IS_CINEMATIC_CAM_RENDERING()
			SET_CINEMATIC_MODE_ACTIVE(FALSE)
		ENDIF
		
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY)
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND NOT IS_INTERACTION_MENU_OPEN()
		AND NOT IS_PHONE_ONSCREEN()
		AND NOT IS_BROWSER_OPEN()
		AND NOT (IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()) AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentySeven, ciOptionsBS27_AutomaticBombBayDoors))
		AND sVehicleWeaponsData.bBombDoorButtonReleased
			IF NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sBombDoorPressDelay)
				START_NET_TIMER(sVehicleWeaponsData.sBombDoorPressDelay)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sBombDoorPressDelay, 500)
					IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
							CLOSE_BOMB_BAY_DOORS(vehPlane)
						ELSE
							BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), FALSE)
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlane)
							IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
							AND IS_ENTITY_A_MISSION_ENTITY(vehPlane)
							AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehPlane))
								SET_PLANE_TURBULENCE_MULTIPLIER(vehPlane, 1.0)
							ENDIF
						ENDIF
						
						SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
						
						CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					ENDIF
					
					CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
					
					REINIT_NET_TIMER(sVehicleWeaponsData.sBombPressDelay)
					RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
					RESET_NET_TIMER(sVehicleWeaponsData.sBombDoorPressDelay)
					
					sVehicleWeaponsData.bBombDoorButtonReleased = FALSE
					sVehicleWeaponsData.bCounterMeasureButtonReleased = FALSE
				ENDIF
			ENDIF
		ELSE
			RESET_NET_TIMER(sVehicleWeaponsData.sBombDoorPressDelay)
		ENDIF
		
		IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			AND NOT IS_VEHICLE_MODEL(vehPlane, HUNTER)
			AND NOT IS_VEHICLE_MODEL(vehPlane, AKULA)
				IF GET_DISTANCE_BETWEEN_COORDS(vPlaneCoords, sVehicleWeaponsData.vGoToPos, FALSE) < 25.0
					VECTOR vPlaneForward = GET_ENTITY_FORWARD_VECTOR(vehPlane)
					
					vPlaneForward = NORMALISE_VECTOR(vPlaneForward)
					
					vPlaneForward.X *= 1000.0
					vPlaneForward.Y *= 1000.0
					
					sVehicleWeaponsData.vGoToPos = vPlaneCoords + vPlaneForward
					
					IF (IS_VEHICLE_MODEL(vehPlane, TULA)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
					OR (IS_VEHICLE_MODEL(vehPlane, AVENGER)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<sVehicleWeaponsData.vGoToPos.X, sVehicleWeaponsData.vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20, FALSE)
					ELIF NOT IS_VEHICLE_MODEL(vehPlane, TULA)
					AND NOT IS_VEHICLE_MODEL(vehPlane, AVENGER)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<sVehicleWeaponsData.vGoToPos.X, sVehicleWeaponsData.vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
					ENDIF
				ENDIF
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
			
			IF NOT DOES_ENTITY_EXIST(sVehicleWeaponsData.entLastBomb)
				IF DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[1])
					IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
						IF DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[0])
							DESTROY_CAM(sVehicleWeaponsData.cBombCamera[0])
						ENDIF
						
						sVehicleWeaponsData.cBombCamera[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						SET_CAM_FOV(sVehicleWeaponsData.cBombCamera[0], 65.0)
						ATTACH_CAM_TO_ENTITY(sVehicleWeaponsData.cBombCamera[0], vehPlane, <<0.0, GET_BOMB_Y_POS(vehPlane), GET_BOMB_CAM_Z(vehPlane)>>)
						SET_CAM_ROT(sVehicleWeaponsData.cBombCamera[0], <<-90.0, 0.0, NORMALIZE_ANGLE(fHeading)>>)
						SET_CAM_ACTIVE_WITH_INTERP(sVehicleWeaponsData.cBombCamera[0], sVehicleWeaponsData.cBombCamera[1], 2000)
						CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
						sVehicleWeaponsData.bFollowAircraftHeading = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_CAM_EXIST(sVehicleWeaponsData.cBombCamera[1])
			AND NOT DOES_ENTITY_EXIST(sVehicleWeaponsData.entLastBomb)
			AND NOT IS_CAM_INTERPOLATING(sVehicleWeaponsData.cBombCamera[1])
			AND IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
				DESTROY_CAM(sVehicleWeaponsData.cBombCamera[1])
				CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED_2)
				sVehicleWeaponsData.bFollowAircraftHeading = TRUE
			ENDIF
			
			IF sVehicleWeaponsData.bFollowAircraftHeading
			AND NOT IS_CAM_INTERPOLATING(sVehicleWeaponsData.cBombCamera[0])
				SET_CAM_ROT(sVehicleWeaponsData.cBombCamera[0], <<-90.0, 0.0, NORMALIZE_ANGLE(fHeading)>>)
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
				CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
			ENDIF
		ELSE
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_BROWSER_OPEN()
				IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
				AND NOT IS_VEHICLE_MODEL(vehPlane, HUNTER)
				AND NOT IS_VEHICLE_MODEL(vehPlane, AKULA)
					VECTOR vPlaneForward = GET_ENTITY_FORWARD_VECTOR(vehPlane)
					
					vPlaneForward = NORMALISE_VECTOR(vPlaneForward)
					
					vPlaneForward.X *= 1000.0
					vPlaneForward.Y *= 1000.0
					
					sVehicleWeaponsData.vGoToPos = vPlaneCoords + vPlaneForward
					
					IF (IS_VEHICLE_MODEL(vehPlane, TULA)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
					OR (IS_VEHICLE_MODEL(vehPlane, AVENGER)
					AND GET_VEHICLE_FLIGHT_NOZZLE_POSITION(vehPlane) = 0)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<sVehicleWeaponsData.vGoToPos.X, sVehicleWeaponsData.vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20, FALSE)
					ELIF NOT IS_VEHICLE_MODEL(vehPlane, TULA)
					AND NOT IS_VEHICLE_MODEL(vehPlane, AVENGER)
						TASK_PLANE_MISSION(PLAYER_PED_ID(), vehPlane, NULL, NULL, <<sVehicleWeaponsData.vGoToPos.X, sVehicleWeaponsData.vGoToPos.Y, vPlaneCoords.Z>>, MISSION_GOTO, 50.0, 0.1, -1, 30, 20)
					ENDIF
				ENDIF
				
				START_AUDIO_SCENE("DLC_SM_Bomb_Bay_View_Scene")
				
				sVehicleWeaponsData.cBombCamera[0] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0.0, 0.0, 0.0>>, <<-90.0, 0.0, NORMALIZE_ANGLE(fHeading)>>, 65, TRUE)
				ATTACH_CAM_TO_ENTITY(sVehicleWeaponsData.cBombCamera[0], vehPlane, <<0.0, GET_BOMB_Y_POS(vehPlane), GET_BOMB_CAM_Z(vehPlane)>>)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CAMERA_CREATED)
				
				PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - g_bBombCamActive = TRUE")
				
				g_bBombCamActive = TRUE
			ENDIF
		ENDIF
		
		SWITCH GET_VEHICLE_MOD(vehPlane, MOD_WING_R)
			CASE 0
				sVehicleWeaponsData.eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_01"))
			BREAK
			CASE 1 
				sVehicleWeaponsData.eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_04"))
			BREAK
			CASE 2
				sVehicleWeaponsData.eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_03"))
			BREAK
			CASE 3
				sVehicleWeaponsData.eBombModel = INT_TO_ENUM(MODEL_NAMES, HASH("w_smug_bomb_02"))
			BREAK
		ENDSWITCH
		
		IF IS_MODEL_VALID(sVehicleWeaponsData.eBombModel)
			IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
				PRINTLN("[AIR_BOMBS] MAINTAIN_USING_AIR_BOMBS - Requesting bomb model")
				
				REQUEST_MODEL(sVehicleWeaponsData.eBombModel)
				
				SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
			ELSE
				IF HAS_MODEL_LOADED(sVehicleWeaponsData.eBombModel)
					IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
						IF sVehicleWeaponsData.iBombsFired < GET_CARPET_BOMB_COUNT(vehPlane)
							IF NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sCarpetBombDelay)
								IF sVehicleWeaponsData.iBombsFired = (GET_CARPET_BOMB_COUNT(vehPlane) - 1)
									FIRE_AIR_BOMB(sVehicleWeaponsData, vehPlane, TRUE)
								ELSE
									FIRE_AIR_BOMB(sVehicleWeaponsData, vehPlane, FALSE)
								ENDIF
								
								sVehicleWeaponsData.iBombsFired++
								
								START_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sCarpetBombDelay, 200)
									RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
								ENDIF
							ENDIF
						ELSE
							CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
							
							sVehicleWeaponsData.iBombsFired = 0
							
							RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
						ENDIF
					ELSE
						IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sBombPressDelay)
							FIRE_AIR_BOMB(sVehicleWeaponsData, vehPlane, TRUE)
							
							RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
						ENDIF
						
						IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
						AND NOT IS_PAUSE_MENU_ACTIVE_EX()
						AND NOT IS_INTERACTION_MENU_OPEN()
						AND NOT IS_PHONE_ONSCREEN()
						AND NOT IS_BROWSER_OPEN()
						AND NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sBombPressDelay)
							IF NOT HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sCarpetBombDelay)
								START_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
							ELSE
								IF HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sCarpetBombDelay, 500)
									SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
								ENDIF
							ENDIF
						ELSE
							RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_CONTROL_RELEASED(PLAYER_CONTROL, INPUT_VEH_FLY_BOMB_BAY)
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND NOT IS_INTERACTION_MENU_OPEN()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_BROWSER_OPEN()
		sVehicleWeaponsData.bBombDoorButtonReleased = TRUE
	ENDIF
	
ENDPROC

FUNC BOOL  MAINTAIN_AIR_BOMBS(VEHICLE_WEAPONS_DATA &sVehicleWeaponsData)
	BOOL bUnloadBombModel = FALSE
	BOOL bReturn = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
		IF HAS_NET_TIMER_STARTED(sVehicleWeaponsData.sBombPressDelay)
		AND HAS_NET_TIMER_EXPIRED(sVehicleWeaponsData.sBombPressDelay, 750)
			RESET_NET_TIMER(sVehicleWeaponsData.sBombPressDelay)
		ENDIF
		
		IF MPGlobals.sFreemodeCache.bIsPlayerInAnyVehicle
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(MPGlobals.sFreemodeCache.PlayerPedIndex) 

			INT iCreatorId = -1
			BOOL bUsingWaterBombs = IS_VEHICLE_USING_WATER_BOMBS(vehTemp, iCreatorId)
			
			MAINTAIN_VEHICLE_AIR_BOMBS_HELP_TEXT(vehTemp, bUsingWaterBombs)
			
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
			AND CAN_LOCAL_PLAYER_USE_AIR_BOMBS(vehTemp)
			AND CAN_VEHICLE_HAVE_AIR_BOMBS(vehTemp)
				bReturn = TRUE
				
				IF bUsingWaterBombs
					PROCESS_WATER_BOMBS(sVehicleWeaponsData, vehTemp, iCreatorId)
				ENDIF
			
				IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
					
					IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
					AND DOES_VEHICLE_HAVE_WEAPONS(vehTemp)
						IF NOT IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
							GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), sVehicleWeaponsData.eCachedBombWeapon)
							
							PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Disabling vehicle weapon")
							
							DISABLE_VEHICLE_WEAPON(TRUE, sVehicleWeaponsData.eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
							
							SET_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
						ENDIF
					ENDIF
				ELSE
					IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
						DISABLE_VEHICLE_WEAPON(FALSE, sVehicleWeaponsData.eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
						
						PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Enabling vehicle weapon")
						
						CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
					ENDIF
				ENDIF
				
				IF CAN_VEHICLE_USE_AIR_BOMBS(vehTemp)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
						IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
						AND IS_ENTITY_A_MISSION_ENTITY(vehTemp)
						AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
							IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
								SET_PLANE_TURBULENCE_MULTIPLIER(vehTemp, 0.0)
							ENDIF
						ENDIF
					ENDIF
					
					MAINTAIN_USING_AIR_BOMBS(sVehicleWeaponsData, vehTemp)
				ELSE
					RESET_NET_TIMER(sVehicleWeaponsData.sBombPressDelay)
					RESET_NET_TIMER(sVehicleWeaponsData.sCarpetBombDelay)
					
					CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_CARPET_BOMB_TRIGGERED)
					
					bUnloadBombModel = TRUE
				ENDIF
				
				IF GET_ENTITY_HEIGHT_ABOVE_GROUND(vehTemp) < 1.5
				OR NOT IS_ENTITY_IN_AIR(vehTemp)
					IF NOT IS_VEHICLE_MODEL(vehTemp, VOLATOL)
					OR IS_VEHICLE_ON_ALL_WHEELS(vehTemp)
						IF NOT bUsingWaterBombs
						OR NOT IS_ENTITY_IN_WATER(vehTemp)
							IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
								IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
									CLOSE_BOMB_BAY_DOORS(vehTemp)
									
									IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
									AND IS_ENTITY_A_MISSION_ENTITY(vehTemp)
									AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
										SET_PLANE_TURBULENCE_MULTIPLIER(vehTemp, 1.0)
									ENDIF
								ELSE
									BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), FALSE)
								ENDIF
								
								SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
								
								CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
							DISABLE_VEHICLE_WEAPON(FALSE, sVehicleWeaponsData.eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
							
							PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Enabling vehicle weapon")
							
							CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
						ENDIF
					ENDIF
					
					CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
				ENDIF
			ELSE
				IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(vehTemp)
						CLOSE_BOMB_BAY_DOORS(vehTemp)
						
						IF NOT GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
						AND IS_ENTITY_A_MISSION_ENTITY(vehTemp)
						AND IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehTemp))
							SET_PLANE_TURBULENCE_MULTIPLIER(vehTemp, 1.0)
						ENDIF
					ELSE
						BROADCAST_OPEN_BOMB_BAY_DOORS(ALL_PLAYERS_ON_SCRIPT(), FALSE)
					ENDIF
					
					SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
					
					CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
				ENDIF
				
				CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DOORS_OPEN)
				
				IF IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
					DISABLE_VEHICLE_WEAPON(FALSE, sVehicleWeaponsData.eCachedBombWeapon, vehTemp, PLAYER_PED_ID())
					
					PRINTLN("[AIR_BOMBS] - MAINTAIN_AIR_BOMBS - Enabling vehicle weapon")
					
					CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_DISABLE_VEHICLE_WEAPON)
				ENDIF
				
				IF bUsingWaterBombs
				AND IS_VEHICLE_DRIVEABLE(vehTemp)
					PROCESS_WATER_BOMB_HUD(sVehicleWeaponsData, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCreatorId].iWaterBombCapacity, GET_VEHICLE_BOMB_AMMO(vehTemp), g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iCreatorId].iWaterBombRefillRate)
				ENDIF
				
				CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
				
				bUnloadBombModel = TRUE
			ENDIF
		ELSE
			CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
			
			sVehicleWeaponsData.iAirBombsBS = 0
			bUnloadBombModel = TRUE
		ENDIF
	ELSE
		CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
		
		sVehicleWeaponsData.iAirBombsBS = 0
		bUnloadBombModel = TRUE
	ENDIF
	
	IF bUnloadBombModel
	AND IS_BIT_SET(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
		PRINTLN("[AIR_BOMBS] MAINTAIN_AIR_BOMBS - Unloading bomb model")
		
		IF IS_MODEL_VALID(sVehicleWeaponsData.eBombModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(sVehicleWeaponsData.eBombModel)
		ENDIF
		
		CLEAR_BIT(sVehicleWeaponsData.iAirBombsBS, AIR_BOMBS_BS_MODEL_REQUESTED)
	ENDIF
	
	IF bUnloadBombModel
		CLEANUP_BOMB_CAMERA(sVehicleWeaponsData)
	ENDIF
	RETURN bReturn
ENDFUNC
