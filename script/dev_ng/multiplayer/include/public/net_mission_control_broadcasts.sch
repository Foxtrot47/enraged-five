USING "rage_builtins.sch"
USING "globals.sch"

USING "net_prints.sch"
USING "net_events.sch"

#IF IS_DEBUG_BUILD
	USING "net_mission_control_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Control_Broadcasts.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all the Mission Control Broadcast functions and Broadcast STRUCTS.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission Control BITFLAGS to pass to Mission Triggerer
// ===========================================================================================================

CONST_INT	MC_BITS_TRIGGERING_USE_COMMS				0
CONST_INT	MC_BITS_TRIGGERING_SHOW_CUTSCENE			1
CONST_INT	MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE	2




// ===========================================================================================================
//      Mission Control ENUMS Required Buy Broadcast Functions
// ===========================================================================================================

// List of 'Failed To Launch Mission' reasons
ENUM m_eFailedToLaunchReasons
	FTLR_ARRAY_FULL,
	FTLR_TEAM_REQUIREMENTS_NOT_MET,
	FTLR_PRISON_MISSION_ALREADY_EXISTS,
	FTLR_MISSION_INSTANCE_ACTIVE,
	FTLR_MISSION_VARIATION_INSTANCE_ACTIVE,
	FTLR_DATA_SETUP_ERROR
ENDENUM




// ===========================================================================================================
//      Mission Control Known Host Functions
// ===========================================================================================================

// PURPOSE: Return the PlayerBits of the Mission Controller Host
//
// RETURN VALUE:		INT				The current host as player Bit (or ALL_PLAYERS() if host is unknown)
//
// NOTES:	This gets updated by the new Host when the Host migrates
//			This routine would be better placed in a higher level function available to the whole game.
//			Returns 'SPECIFIC_PLAYER' if the host is valid, or 'ALL_PLAYERS' if the host is not valid
FUNC INT Get_MC_Host_As_PlayerBit()

	IF GlobalServerBD_BlockB.missionControlData.mcHost != INVALID_PLAYER_INDEX()
		IF (IS_NET_PLAYER_OK(GlobalServerBD_BlockB.missionControlData.mcHost, FALSE))
			RETURN (SPECIFIC_PLAYER(GlobalServerBD_BlockB.missionControlData.mcHost))
		ENDIF
	ENDIF
	
	RETURN (ALL_PLAYERS())
	
ENDFUNC




// ===========================================================================================================
//      Mission Control Resync Broadcast Storage Functions
//		(Used for when the Host migrates to try to recover from when events sent to the old host get lost)
// ===========================================================================================================

// PURPOSE:	Clears out the External Resync Storage array
PROC Clear_All_MC_Broadcast_Resync_Storage()

	g_structMissionControlResyncBroadcasts emptyStruct

	INT tempLoop = 0
	REPEAT MAX_MC_RESYNC_BROADCASTS tempLoop
		g_sMCResyncComms[tempLoop] = emptyStruct
	ENDREPEAT
	
	g_numMCResyncBroadcasts = 0

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear out any resync data where the timer has expired
//
// INPUT PARAMS:		paramClearAll			[DEFAULT = TRUE] True means clear all entries, FALSE means leave entries marked as being resynced due to recent host migration
//
// NOTES:	Any entries marked as being part of an ongoing resync check will have a time of 0
//			Automatically leaves all entries alone if a resync is in progress
PROC Clear_Old_MC_Broadcasts_From_Resync_Storage(BOOL paramClearAll = TRUE)

	IF (g_numMCResyncBroadcasts = 0)
		EXIT
	ENDIF
	
	// Don't clear the existing data if a Host Migration Resync is in progress
	IF (g_sMCResync.mccrExternalTimeoutInitialised)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl][Resync]: External Host Migration Resync in progress so not clearing out the old data at the moment.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check for timeout on the existing data on the array and reshuffle the data up the array
	TIME_DATATYPE netTimer = GET_NETWORK_TIME()
	
	INT theFrom	= 0
	INT theTo	= 0
	
	// ...if requested, leave any entries being checked by the existing resync checks
	IF NOT (paramClearAll)
		BOOL continueChecking = TRUE
		WHILE (continueChecking)
			continueChecking = FALSE
			IF (theFrom < g_numMCResyncBroadcasts)
				IF (g_sMCResyncComms[theFrom].mcrbInUseByActiveResync)
					// ...part of an active resync check, so ignore this one
					theFrom++
					theTo++
					
					continueChecking = TRUE
				ENDIF
			ENDIF
		ENDWHILE
	ENDIF
	
	WHILE (theFrom < g_numMCResyncBroadcasts)
		IF (IS_TIME_LESS_THAN(netTimer, g_sMCResyncComms[theFrom].mcrbTimeout))
			// The data hasn't timed out, so shuffle this piece of data up the array
			IF NOT (theFrom = theTo)
				g_sMCResyncComms[theTo] = g_sMCResyncComms[theFrom]
			ENDIF
			
			// Next storage position in the array
			theTo++
		ENDIF
		
		// Next data to check
		theFrom++
	ENDWHILE
	
	// Update the counter
	g_numMCResyncBroadcasts = theTo
	
	// Clear out any old data still in the array
	g_structMissionControlResyncBroadcasts emptyStruct
	WHILE (theTo < theFrom)
		g_sMCResyncComms[theTo] = emptyStruct
		theTo++
	ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To mark all existing items in the array so that new items received after this can be ignored during the resync
PROC Mark_All_MC_Broadcasts_In_Resync_Storage_As_Being_Checked()

	INT tempLoop = 0
	REPEAT g_numMCResyncBroadcasts tempLoop
		g_sMCResyncComms[tempLoop].mcrbInUseByActiveResync = TRUE
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert an Event ID to a Resync Comms Type ID
//
// INPUT PARAMS:		paramEventID				The Broadcast EventID being sent to the Mission Controller
// RETURN VALUE:		g_eMCCResyncCommsTypeID		The TypeID representing this EventID
FUNC g_eMCCResyncCommsTypeID Convert_EventID_To_Resync_TypeID(SCRIPTED_EVENT_TYPES paramEventID)

	SWITCH (paramEventID)
		CASE SCRIPT_EVENT_REQUEST_NEW_SPECIFIC_MISSION
			RETURN MCCRCH_REQUEST_SPECIFIC_MISSION
			
		CASE SCRIPT_EVENT_REQUEST_NEW_MISSION_OF_TYPE
			RETURN MCCRCH_REQUEST_MISSION_OF_TYPE
			
		CASE SCRIPT_EVENT_REQUEST_JOIN_EXISTING_MISSION
			RETURN MCCRCH_REQUEST_JOIN_MISSION
			
		CASE SCRIPT_EVENT_RESERVE_MISSION_BY_PLAYER
			RETURN MCCRCH_RESERVE_MISSION_BY_PLAYER
			
		CASE SCRIPT_EVENT_RESERVE_MISSION_BY_LEADER
			RETURN MCCRCH_RESERVE_MISSION_BY_LEADER
			
		CASE SCRIPT_EVENT_CANCEL_MISSION_RESERVATION_BY_PLAYER
			RETURN MCCRCH_CANCEL_RESERVATION_BY_PLAYER
			
		CASE SCRIPT_EVENT_CANCEL_MISSION_RESERVED_BY_LEADER
			RETURN MCCRCH_CANCEL_RESERVED_BY_LEADER
			
		CASE SCRIPT_EVENT_START_RESERVED_MISSION
			RETURN MCCRCH_START_RESERVED_MISSION
			
		CASE SCRIPT_EVENT_CHANGE_RESERVED_MISSION_DETAILS
			RETURN MCCRCH_CHANGE_RESERVED_MISSION
			
		CASE SCRIPT_EVENT_MISSION_NOT_JOINABLE
			RETURN MCCRCH_MISSION_NOT_JOINABLE
			
		CASE SCRIPT_EVENT_MISSION_JOINABLE_AGAIN_FOR_PLAYERS
			RETURN MCCRCH_MISSION_JOINABLE_FOR_PLAYERS
			
		CASE SCRIPT_EVENT_MISSION_READY_FOR_SECONDARY_TEAMS
			RETURN MCCRCH_READY_FOR_SECONDARY_TEAMS
	ENDSWITCH
	
	// Failed to find the eventID
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl][Resync]: Convert_EventID_To_Resync_TypeID. Unknown EventID. Add to SWITCH statement.") NET_NL()
		SCRIPT_ASSERT("Convert_EventID_To_Resync_TypeID(). Unknown EventID. Add to SWITCH statement. Tell Keith")
	#ENDIF
	
	RETURN MCCRCH_NO_COMMS

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Generate a timeout time for when this broadcast even will become obsolete on the resync array
//
// RETURN VALUE:		INT				The timeout
FUNC TIME_DATATYPE Calculate_Resync_Storage_Timeout()
	
	TIME_DATATYPE timeoutTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), MCRESYNC_ENTRY_ON_ARRAY_TIMEOUT_msec)
	RETURN (timeoutTime)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Generate a shorter timeout time for when this broadcast even will become obsolete on the resync array
//
// RETURN VALUE:		INT				The timeout
//
// NOTES:	Set it to the resync timer delay time
FUNC TIME_DATATYPE Calculate_Resync_Storage_Short_Timeout()
	
	TIME_DATATYPE timeoutTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), MCRESYNC_TIMER_DELAY_msec)
	RETURN (timeoutTime)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check for an entry with duplicate details and, if found, update the timeout time
//
// INPUT PARAMS:		paramTypeID					The typeID of the broadcast event being sent to the Mission Controller
//						paramUniqueID				The UniqueID of this request
//						paramMissionID				The MissionID if required by the event
//						paramVariation				The variation of the mission if required by the event
// RETURN VALUE:		BOOL						TRUE if an existing duplicate entry was found and the timeout was restarted, otherwise FALSE
FUNC BOOL Update_Existing_Entry_On_Resync_Storage_Array(g_eMCCResyncCommsTypeID paramTypeID, INT paramUniqueID, MP_MISSION paramMissionID, INT paramVariation)

	// Check for a duplicate entry
	BOOL foundMatch = FALSE
	INT tempLoop = 0
	REPEAT g_numMCResyncBroadcasts tempLoop
		foundMatch = FALSE
		
		IF (g_sMCResyncComms[tempLoop].mcrbCommsTypeID	= paramTypeID)
			// Found an entry with a duplicate type
			// Some TypeIDs also need to match unique ID, but requests for new missions don't need to match uniqueIDs because each request would be for a different ID
			foundMatch = TRUE
			
			SWITCH (paramTypeID)
				// These entries require the uniqueIDs to match because the UniqueID would be known when the broadcast was sent
				CASE MCCRCH_REQUEST_JOIN_MISSION
				CASE MCCRCH_CANCEL_RESERVATION_BY_PLAYER
				CASE MCCRCH_CANCEL_RESERVED_BY_LEADER
				CASE MCCRCH_MISSION_NOT_JOINABLE
				CASE MCCRCH_MISSION_JOINABLE_FOR_PLAYERS
				CASE MCCRCH_READY_FOR_SECONDARY_TEAMS
				CASE MCCRCH_CHANGE_RESERVED_MISSION
					// ...check for matching unique IDs
					IF NOT (g_sMCResyncComms[tempLoop].mcrbUniqueID	= paramUniqueID)
						// ...not a match
						foundMatch = FALSE
					ENDIF
					BREAK
					
				// These don't require the uniqueIDs to match
				CASE MCCRCH_REQUEST_MISSION_OF_TYPE
				CASE MCCRCH_START_RESERVED_MISSION
					BREAK
					
				// These don't require the uniqueIDs to match but do need the Mission ID and Variation to match
				CASE MCCRCH_REQUEST_SPECIFIC_MISSION
				CASE MCCRCH_RESERVE_MISSION_BY_PLAYER
				CASE MCCRCH_RESERVE_MISSION_BY_LEADER
					IF NOT (g_sMCResyncComms[tempLoop].mcrbMissionID	= paramMissionID)
					OR NOT (g_sMCResyncComms[tempLoop].mcrbVariation	= paramVariation)
						// ...not a match
						foundMatch = FALSE
					ENDIF
					BREAK
					
				DEFAULT
					SCRIPT_ASSERT("Update_Existing_Entry_On_Resync_Storage_Array(): A new TypeID needs to be been added to the switch statement. Tell Keith.")
					foundMatch = FALSE
					BREAK
			ENDSWITCH
			
			// If this is a match then update the timestamp and the uniqueID, then reshuffle this entry to the end of the array to be in timestamp order
			IF (foundMatch)
				// Updating timestamp and uniqueID
				g_sMCResyncComms[tempLoop].mcrbTimeout				= Calculate_Resync_Storage_Timeout()
				g_sMCResyncComms[tempLoop].mcrbInUseByActiveResync	= FALSE
				g_sMCResyncComms[tempLoop].mcrbUniqueID				= paramUniqueID
		
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MControl][Resync]: Update_Existing_Entry_On_Resync_Storage_Array() - Found Duplicate Details On Resync Array.") NET_NL()
					NET_PRINT("   Updating uniqueID and timeout:") NET_NL()
				#ENDIF
				
				// Updating MissionID and Mission Variation if necessary
				SWITCH (paramTypeID)
					CASE MCCRCH_CHANGE_RESERVED_MISSION
						g_sMCResyncComms[tempLoop].mcrbMissionID	= paramMissionID
						g_sMCResyncComms[tempLoop].mcrbVariation	= paramVariation

						#IF IS_DEBUG_BUILD
							NET_PRINT("   Updating missionID and variation:") NET_NL()
						#ENDIF
						BREAK
				ENDSWITCH
				
				#IF IS_DEBUG_BUILD
					Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(tempLoop)
				#ENDIF
				
				// Shuffling this entry to the end of the array
				// ...grab the updated entry
				g_structMissionControlResyncBroadcasts holdUpdatedEntry = g_sMCResyncComms[tempLoop]
				
				// ...move remaining entries up one array position
				INT theTo = tempLoop
				INT theFrom = theTo + 1
				WHILE (theFrom  < g_numMCResyncBroadcasts)
					g_sMCResyncComms[theTo] = g_sMCResyncComms[theFrom]
					
					theTo++
					theFrom++
				ENDWHILE
				
				// Stick the updated entry at the end of hte valid entries
				g_sMCResyncComms[theTo] = holdUpdatedEntry
				
				#IF IS_DEBUG_BUILD
					NET_PRINT("   Shuffling this entry to the end of the array - displaying new array:") NET_NL()
					Debug_Output_Mission_Control_Resync_Array()
				#ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Didn't find an existing duplicate entry
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of a broadcast on the resync array
//
// INPUT PARAMS:		paramEventID				The Broadcast EventID being sent to the Mission Controller
//						paramUniqueID				The UniqueID of this request
//						paramMissionID				[DEFAULT = eNULL_MISSION] The MissionID if required by the event
//						paramVariation				[DEFAULT = NO_MISSION_VARIATION] The variation of the mission if required by the event
PROC Store_MC_Broadcast_On_Resync_Storage(SCRIPTED_EVENT_TYPES paramEventID, INT paramUniqueID, MP_MISSION paramMissionID = eNULL_MISSION, INT paramVariation = NO_MISSION_VARIATION)

	// Clear out broadcasts on the Resync array where the timer has expired
	Clear_Old_MC_Broadcasts_From_Resync_Storage()
	
	// Store the data
	g_eMCCResyncCommsTypeID	typeID = Convert_EventID_To_Resync_TypeID(paramEventID)
	IF (typeID = MCCRCH_NO_COMMS)
		EXIT
	ENDIF
	
	// Check for a duplicate entry and, if found, update the timeout time
	IF (Update_Existing_Entry_On_Resync_Storage_Array(typeID, paramUniqueID, paramMissionID, paramVariation))
		EXIT
	ENDIF
	
	// Ensure there is space on the array for another piece of data
	IF (g_numMCResyncBroadcasts >= MAX_MC_RESYNC_BROADCASTS)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl][Resync]: Store_MC_Broadcast_On_Resync_Storage() - RESYNC STORAGE ARRAY IS FULL. Increase MAX_MC_RESYNC_BROADCASTS.") NET_NL()
			NET_PRINT("      Current Array Contents:") NET_NL()
			Debug_Output_Mission_Control_Resync_Array()
			SCRIPT_ASSERT("Store_MC_Broadcast_On_Resync_Storage(). Array Full. Increase MAX_MC_RESYNC_BROADCASTS. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	g_sMCResyncComms[g_numMCResyncBroadcasts].mcrbCommsTypeID			= typeID
	g_sMCResyncComms[g_numMCResyncBroadcasts].mcrbUniqueID				= paramUniqueID
	g_sMCResyncComms[g_numMCResyncBroadcasts].mcrbMissionID				= paramMissionID
	g_sMCResyncComms[g_numMCResyncBroadcasts].mcrbVariation				= paramVariation
	g_sMCResyncComms[g_numMCResyncBroadcasts].mcrbTimeout				= Calculate_Resync_Storage_Timeout()
	g_sMCResyncComms[g_numMCResyncBroadcasts].mcrbInUseByActiveResync	= FALSE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl][Resync]: Store_MC_Broadcast_On_Resync_Storage() - New Details Stored On Resync Array:") NET_NL()
		Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(g_numMCResyncBroadcasts)
	#ENDIF
	
	g_numMCResyncBroadcasts++
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Updated Resync Array Contents:") NET_NL()
		Debug_Output_Mission_Control_Resync_Array()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allows entries to be removed from the Resync Storage array when a reply from the host is received.
//
// INPUT PARAMS:		paramReplyType				The Broadcast ReplyTypeID being sent back to the player from the Mision Controller
//						paramUniqueID				The UniqueID passed with the reply
PROC Update_Resync_Storage_Based_On_Replies(g_eMCBroadcastType paramReplyType, INT paramUniqueID)

	// Clear any old entries before updating
	Clear_Old_MC_Broadcasts_From_Resync_Storage()

	// Ensure the Broadcast Type is valid
	SWITCH (paramReplyType)
		CASE MCCBT_NONE
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl][Resync]: Update_Resync_Storage_Based_On_Replies() - Reply received: ")
				NET_PRINT(Convert_MC_Broadcast_Type_To_String(paramReplyType))
				NET_PRINT(" - INVALID REPLY, so IGNORE")
				NET_NL()
			#ENDIF
			EXIT
	ENDSWITCH

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl][Resync]: Update_Resync_Storage_Based_On_Replies() - Reply received: ")
		NET_PRINT(Convert_MC_Broadcast_Type_To_String(paramReplyType))
		NET_NL()
		NET_PRINT("      Current Array Contents:") NET_NL()
		Debug_Output_Mission_Control_Resync_Array()
	#ENDIF
	
	// Nothing to do if there are no entries on the array
	IF (g_numMCResyncBroadcasts = 0)
		EXIT
	ENDIF
	
	// Go through the array looking for entries with this uniqueID that should be removed because of this reply type
	INT theStorePos = 0
	INT theCheckPos = 0
	BOOL keepThisEntry = FALSE
	
	g_eMCCResyncCommsTypeID	thisResyncCommsType = MCCRCH_NO_COMMS
	
	REPEAT g_numMCResyncBroadcasts theCheckPos
		keepThisEntry		= TRUE
		thisResyncCommsType	= g_sMCResyncComms[theCheckPos].mcrbCommsTypeID
		
		// Mainly entries with the correct uniqueID are likely to be removed by replies
		IF (g_sMCResyncComms[theCheckPos].mcrbUniqueID = paramUniqueID)
			// ...uniqueIDs are the same, so check if this broadcast type should be removed
			SWITCH (thisResyncCommsType)
				CASE MCCRCH_REQUEST_SPECIFIC_MISSION
				CASE MCCRCH_REQUEST_MISSION_OF_TYPE
				CASE MCCRCH_REQUEST_JOIN_MISSION
				CASE MCCRCH_RESERVE_MISSION_BY_PLAYER
				CASE MCCRCH_RESERVE_MISSION_BY_LEADER
				CASE MCCRCH_CANCEL_RESERVATION_BY_PLAYER
				CASE MCCRCH_CANCEL_RESERVED_BY_LEADER
				CASE MCCRCH_START_RESERVED_MISSION
				CASE MCCRCH_CHANGE_RESERVED_MISSION
					// React to Specific Responses
					SWITCH (paramReplyType)
						// All of these entries will be removed by most of the Basic Responses: FULL, FINISHED, ON MISSION
						CASE MCCBT_MISSION_FULL
						CASE MCCBT_MISSION_FINISHED
						CASE MCCBT_PLAYER_ON_MISSION
							keepThisEntry = FALSE
							BREAK
							
						// [BUG 2023214] Most of these entries will also be removed by FAIL, but CANCEL_RESERVATION should swap to a shorter expiry time
						CASE MCCBT_REQUEST_FAILED
							IF (thisResyncCommsType = MCCRCH_CANCEL_RESERVATION_BY_PLAYER)
								// Keep the entry, but use a shortened timeout
								g_sMCResyncComms[theCheckPos].mcrbTimeout = Calculate_Resync_Storage_Short_Timeout()
								
								#IF IS_DEBUG_BUILD
									NET_PRINT("   Array Entry ")
									NET_PRINT_INT(theCheckPos)
									NET_PRINT(" - HAVING TIMEOUT SET SHORT BY REPLY: ")
									NET_PRINT(Convert_MC_Broadcast_Type_To_String(paramReplyType))
									IF (g_sMCResyncComms[theCheckPos].mcrbInUseByActiveResync)
										NET_PRINT(" [ENTRY WAS MARKED AS PART OF THE RESYNC CHECK]")
									ENDIF
									NET_NL()
									Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(theCheckPos)
								#ENDIF
							ELSE
								keepThisEntry = FALSE
							ENDIF
							BREAK
							
						// Some entries will be removed by the uniqueID being changed
						CASE MCCBT_CHANGE_UNIQUE_ID
							IF (thisResyncCommsType = MCCRCH_REQUEST_SPECIFIC_MISSION)
								keepThisEntry = FALSE
							ENDIF
							BREAK
							
						// Some entries should remain for a short time longer by the reservation being confirmed
						CASE MCCBT_CONFIRMED_RESERVED
							IF (thisResyncCommsType = MCCRCH_RESERVE_MISSION_BY_PLAYER)
							OR (thisResyncCommsType = MCCRCH_RESERVE_MISSION_BY_LEADER)
								// Keep the entry, but use a shortened timeout
								g_sMCResyncComms[theCheckPos].mcrbTimeout = Calculate_Resync_Storage_Short_Timeout()
								
								#IF IS_DEBUG_BUILD
									NET_PRINT("   Array Entry ")
									NET_PRINT_INT(theCheckPos)
									NET_PRINT(" - HAVING TIMEOUT SET SHORT BY REPLY: ")
									NET_PRINT(Convert_MC_Broadcast_Type_To_String(paramReplyType))
									IF (g_sMCResyncComms[theCheckPos].mcrbInUseByActiveResync)
										NET_PRINT(" [ENTRY WAS MARKED AS PART OF THE RESYNC CHECK]")
									ENDIF
									NET_NL()
									Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(theCheckPos)
								#ENDIF
							ENDIF
							BREAK
					ENDSWITCH
					BREAK
					
				// Ignore these - there's no confirmation
				CASE MCCRCH_MISSION_NOT_JOINABLE
				CASE MCCRCH_MISSION_JOINABLE_FOR_PLAYERS
				CASE MCCRCH_READY_FOR_SECONDARY_TEAMS
					BREAK
					
				DEFAULT
					NET_PRINT("...KGM MP [MControl][Resync]: Update_Resync_Storage_Based_On_Replies() - Unknown Resync Comms Type - Add to SWITCH. Tell Keith: ")
					NET_PRINT(Convert_Resync_CommsTypeID_To_String(g_sMCResyncComms[theCheckPos].mcrbCommsTypeID))
					NET_NL()
					SCRIPT_ASSERT("Update_Resync_Storage_Based_On_Replies(): ERROR - Unknown Resync Comms Type. See COnsole Log. Tell Keith.")
					BREAK
			ENDSWITCH
		ENDIF
		
		// Check for exceptions where an entry will be removed by a reply even if the unique ID differs
		IF (keepThisEntry)
			SWITCH (thisResyncCommsType)
				// A 'cancel reservation' entry will be removed by any new 'reservation confirmed' reply.
				// A player can cancel reservation then immediately reserve the mission again. Without this,
				//		when the 'confirmed reserved' arrives it removes the 'request reservation' but leaves
				//		the old 'cancel reservation' on the array. If the host migrates then the 'cancel
				//		reservation' will get sent again because the player is detected to be still on the
				//		mission - but this would be legimitate. However the resync would have forced the
				//		player off the mission again.
				CASE MCCRCH_CANCEL_RESERVATION_BY_PLAYER
					SWITCH (paramReplyType)
						CASE MCCBT_CONFIRMED_RESERVED
							keepThisEntry = FALSE
							BREAK
					ENDSWITCH
					BREAK
			ENDSWITCH			
		ENDIF
		
		// Display some debug output if an entry is being removed
		// NOTE: I'm currently also deleting entries that are part of a host migration resync check - I think this should be ok
		#IF IS_DEBUG_BUILD
			IF NOT (keepThisEntry)
				NET_PRINT("   Array Entry ")
				NET_PRINT_INT(theCheckPos)
				NET_PRINT(" - BEING REMOVED BY REPLY: ")
				NET_PRINT(Convert_MC_Broadcast_Type_To_String(paramReplyType))
				IF (g_sMCResyncComms[theCheckPos].mcrbInUseByActiveResync)
					NET_PRINT(" [ENTRY WAS MARKED AS PART OF THE RESYNC CHECK]")
				ENDIF
				NET_NL()
				Debug_Output_One_Set_Of_Mission_Control_Resync_Array_Details(theCheckPos)
			ENDIF
		#ENDIF
		
		// Move this entry if it is being kept
		IF (keepThisEntry)
			IF NOT (theStorePos = theCheckPos)
				g_sMCResyncComms[theStorePos] = g_sMCResyncComms[theCheckPos]
			ENDIF
			
			theStorePos++
		ENDIF
	ENDREPEAT
	
	// Modify the max entries value
	g_numMCResyncBroadcasts = theStorePos

	#IF IS_DEBUG_BUILD
		NET_PRINT("      Updated Array Contents:") NET_NL()
		Debug_Output_Mission_Control_Resync_Array()
	#ENDIF

ENDPROC



// ===========================================================================================================
//      Mission Control Helper Functions
// ===========================================================================================================

// PURPOSE:	Check if the Slot is available for new data
//
// INPUT PARAMS:		paramArrayPos			Array Position of data to be checked
// RETURN VALUE:		BOOL					TRUE if the slot has no data is available for new data, otherwise FALSE
FUNC BOOL Is_MP_Mission_Request_Slot_Available_For_New_Data(INT paramArrayPos)
	RETURN (GlobalServerBD_MissionRequest.missionRequests[paramArrayPos].mrStatusID = NO_MISSION_REQUEST_RECEIVED)
ENDFUNC




// ===========================================================================================================
//      Unique Mission Request ID routines
// ===========================================================================================================

// PURPOSE:	Generates a unique ID for a Mission Trigger request
//
// RETURN VALUE:		INT			Unique ID
FUNC INT Generate_Unique_Mission_Request_ID()

	INT theID = GET_RANDOM_INT_IN_RANGE(MIN_UNIQUE_ID, MAX_UNIQUE_ID)
	
	// There's a slim possibility that the Random ID may not be unique, so change it if a duplicate is found
	BOOL foundDuplicate = FALSE
	BOOL keepChecking = FALSE
	INT tempLoop = 0
	
	WHILE (keepChecking)
		tempLoop = 0
		foundDuplicate = FALSE
		
		REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
			IF (keepChecking)
				IF NOT (Is_MP_Mission_Request_Slot_Available_For_New_Data(tempLoop))
					IF (GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrMissionData.mdUniqueID = theID)
						keepChecking = FALSE
						foundDuplicate = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Looped though all mission requests, so quit unless a duplicate found
		keepChecking = FALSE
		
		// If found duplicate, then increase the ID
		IF (foundDuplicate)
			theID += 1
			keepChecking = TRUE
		ENDIF
	ENDWHILE
	
	RETURN (theID)

ENDFUNC




// ===========================================================================================================
//      Mission Control Event Structs and Broadcasts
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Event: Reply Player Joined Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventReplyPlayerJoinedMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission that the player joined
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a reply to server that a player joined a mission
//
// INPUT PARAMS:		paramMissionData				// The Mission Data for the mission the player joined
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
PROC Broadcast_Reply_Player_Joined_Mission(MP_MISSION_DATA paramMissionData)

	// Store the Broadcast data
	m_structEventReplyPlayerJoinedMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_REPLY_PLAYER_JOINED_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Broadcasting SCRIPT_EVENT_REPLY_PLAYER_JOINED_MISSION: to server, issued by ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Inst=")
		NET_PRINT_INT(paramMissionData.iInstanceId)
		NET_PRINT(") (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reply Player Failed To Join Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventReplyPlayerFailedToJoinMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission that the player failed to join
	BOOL						rejectedByPlayer		// TRUE if the player should be classed as having rejected this mission offer
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a reply to server that a player failed to join a mission
//
// INPUT PARAMS:		paramMissionData				// The Mission Data for the mission the player failed to join
//						paramRejectedByPlayer			// [DEFAULT = FALSE] TRUE if the player actively rejected the mission, otherwise FALSE
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
PROC Broadcast_Reply_Player_Failed_To_Join_Mission(MP_MISSION_DATA paramMissionData, BOOL paramRejectedByPlayer = FALSE)

	// Store the Broadcast data
	m_structEventReplyPlayerFailedToJoinMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_REPLY_PLAYER_FAILED_TO_JOIN_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	Event.rejectedByPlayer			= paramRejectedByPlayer
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Broadcasting SCRIPT_EVENT_REPLY_PLAYER_FAILED_TO_JOIN_MISSION: to server, issued by ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Inst=")
		NET_PRINT_INT(paramMissionData.iInstanceId)
		NET_PRINT(") (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]  [Rejected By Player = ")
		IF (paramRejectedByPlayer)
			NET_PRINT(" TRUE")
		ELSE
			NET_PRINT(" FALSE")
		ENDIF
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reply Player Ended Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventReplyPlayerEndedMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission that the player ended
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a reply to server that a player ended a mission
//
// INPUT PARAMS:		paramMissionData				// The Mission Data for the mission that the player ended
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
PROC Broadcast_Reply_Player_Ended_Mission(MP_MISSION_DATA paramMissionData)

	// Store the Broadcast data
	m_structEventReplyPlayerEndedMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_REPLY_PLAYER_ENDED_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Broadcasting SCRIPT_EVENT_REPLY_PLAYER_ENDED_MISSION: to server, issued by ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Inst=")
		NET_PRINT_INT(paramMissionData.iInstanceId)
		NET_PRINT(") (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Check If Still Ok To Join Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventCheckIfStillOkToJoinMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission that the player failed to join
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to server for confirmation that it is still ok to join mission
//
// INPUT PARAMS:		paramMissionData				// The Mission Data for the mission the player is trying to join
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
PROC Broadcast_Check_If_Still_Ok_To_Join_Mission(MP_MISSION_DATA paramMissionData)

	// Store the Broadcast data
	m_structEventCheckIfStillOkToJoinMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_CHECK_IF_STILL_OK_TO_JOIN_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Broadcasting SCRIPT_EVENT_CHECK_IF_STILL_OK_TO_JOIN_MISSION: to server, issued by ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Inst=")
		NET_PRINT_INT(paramMissionData.iInstanceId)
		NET_PRINT(") (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Invite Player Onto Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventInvitePlayerOntoMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data (to ensure it is available when mission triggering needs it - in case the slot data is a bit slow getting broadcast)
	INT							bitsOptions				// BITFIELD of options to tailor the Mission Triggering based on flags supplied by Mission Controller
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast an invitation to a player to optionally join a mission
//
// INPUT PARAMS:			paramPlayerID			// The Player Index the request is being sent to
//							paramSlotID				// The slot containing the mission data
//
// NOTES:	This request is always intended for a specific player
PROC Broadcast_Invite_Player_Onto_Mission(PLAYER_INDEX paramPlayerID, INT paramSlotID)

	// Store the Broadcast data
	m_structEventInvitePlayerOntoMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_INVITE_PLAYER_ONTO_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData
	
	// ...mission triggering options
	Event.bitsOptions				= ALL_MISSION_CONTROL_BITS_CLEAR
	
	// ......set the 'use secondary objective' flag if the 'first crook gang joining' flag is set
	// NOTE: The first crook gang joining flag only gets set if secondary objectives are required for the mission and the first Crook Gang has already been issued with join requests
	IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsOptions, MPMR_OPTION_BITFLAG_FIRST_CROOK_GANG_JOINING))
		SET_BIT(Event.bitsOptions, MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE)
	ENDIF
	
	// Broadcast the event to a SPECIFIC PLAYER
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........(SERVER) Broadcasting SCRIPT_EVENT_INVITE_PLAYER_ONTO_MISSION") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]  [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [Use Secondary Objective = ")
		IF (IS_BIT_SET(Event.bitsOptions, MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE))
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT("]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Force Player Onto Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventForcePlayerOntoMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data (to ensure it is available when mission triggering needs it - in case the slot data is a bit slow getting broadcast)
	INT							bitsOptions				// BITFIELD of options to tailor the Mission Triggering based on flags supplied by Mission Controller
	INT 						UniqueId
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast force a player to join a mission
//
// INPUT PARAMS:			paramPlayerID			// The Player Index the request is being sent to
//							paramSlotID				// The slot containing the mission data
//							paramUseComms			// TRUE if the player should use a Communication, otherwise FALSE
//							paramShowCutscene		// [DEFAULT = FALSE] TRUE if the mission should show a pre-mission cutscene, otherwise FALSE
//
// NOTES:	This request is always intended for a specific player
PROC Broadcast_Force_Player_Onto_Mission(PLAYER_INDEX paramPlayerID, INT paramSlotID, BOOL paramUseComms, BOOL paramShowCutscene = FALSE)

	// Store the Broadcast data
	m_structEventForcePlayerOntoMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_FORCE_PLAYER_ONTO_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	Event.UniqueId	= GET_THIS_PLAYER_UNIQUE_EVENT_ID(paramPlayerID)
	PRINTLN("SCRIPT_EVENT_FORCE_PLAYER_ONTO_MISSION Event.UniqueId	= GET_THIS_PLAYER_UNIQUE_EVENT_ID(paramPlayerID)")
	
	Event.missionData				= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData
	
	// ...mission triggering options
	Event.bitsOptions				= ALL_MISSION_CONTROL_BITS_CLEAR
	
	// ......set the 'Use Comms' flag based on the parameter
	IF (paramUseComms)
		SET_BIT(Event.bitsOptions, MC_BITS_TRIGGERING_USE_COMMS)
	ENDIF
	
	// ......set the 'Show Cutscene' flag based on the parameter
	IF (paramShowCutscene)
		SET_BIT(Event.bitsOptions, MC_BITS_TRIGGERING_SHOW_CUTSCENE)
	ENDIF
	
	// ......set the 'use secondary objective' flag if the 'first crook gang joining' flag is set
	// NOTE: The first crook gang joining flag only gets set if secondary objectives are required for the mission and the first Crook Gang has already been issued with join requests
	IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsOptions, MPMR_OPTION_BITFLAG_FIRST_CROOK_GANG_JOINING))
		SET_BIT(Event.bitsOptions, MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE)
	ENDIF
	
	// Broadcast the event to a SPECIFIC PLAYER
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........(SERVER) Broadcasting SCRIPT_EVENT_FORCE_PLAYER_ONTO_MISSION") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]  [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UseComms=")
		IF (paramUseComms)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
			IF (IS_BIT_SET(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsReservedPlayersByPlayer, NATIVE_TO_INT(paramPlayerID)))
				NET_PRINT(" (Player Choice Reserved for mission)")
			ENDIF
		ENDIF
		NET_PRINT("]  [Show Cutscene = ")
		IF (paramShowCutscene)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT("]  [Use Secondary Objective = ")
		IF (IS_BIT_SET(Event.bitsOptions, MC_BITS_TRIGGERING_USE_SECONDARY_OBJECTIVE))
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT("]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Finished
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionRequestFinished
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The unique Mission Request ID for a mission that has Finished
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission Request Finished to a specific player
//
// INPUT PARAMS:			paramSlotID					// The slot containing the mission data
//							paramPlayerID				// PLAYER_INDEX of the player to send the broadcast to
//
// NOTES:	This request is intended for a specific player
PROC Broadcast_Mission_Request_Finished_To_Specific_Player(INT paramSlotID, PLAYER_INDEX paramPlayerID)

	// Store the Broadcast data
	m_structEventMissionRequestFinished Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_FINISHED
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Finished_To_Specific_Player() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlotID)
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Finished_To_Specific_Player() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Broadcast the event to ALL PLAYERS
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_FINISHED to specific player") NET_NL()
		IF (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
			NET_PRINT("           [PlayerID = ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_PRINT("]")
		ENDIF
		NET_PRINT("           [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that Mission Request Finished
//
// INPUT PARAMS:			paramSlotID				// The slot containing the mission data
//
// NOTES:	This request is always intended for all players so that the requester and any players find out
PROC Broadcast_Mission_Request_Finished(INT paramSlotID)

	// Store the Broadcast data
	m_structEventMissionRequestFinished Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_FINISHED
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Finished() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlotID)
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Finished() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Broadcast the event to ALL PLAYERS
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_FINISHED") NET_NL()
		NET_PRINT("           [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Failed
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionRequestFailed
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The unique Mission Request ID for a mission request that failed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission Request Failed (accepting a unique ID as parameter)
//
// INPUT PARAMS:			paramUniqueID				// The uniqueID containing the mission data
//
// NOTES:	This request is always intended for all players because the requester may be a script host
PROC Broadcast_Mission_Request_Failed_For_UniqueID(INT paramUniqueID)

	// Store the Broadcast data
	m_structEventMissionRequestFailed Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_FAILED
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Failed_For_UniqueID() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Failed_For_UniqueID() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Broadcast the event to ALL PLAYERS
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_FAILED (cutdown details)") NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission Request Failed to a specific player (accepting a unique ID as parameter)
//
// INPUT PARAMS:			paramUniqueID				// The uniqueID containing the mission data
//							paramPlayerID				// PLAYER_INDEX of the player to send the broadcast to
//
// NOTES:	This request is intended for a specific player
PROC Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(INT paramUniqueID, PLAYER_INDEX paramPlayerID)

	// Store the Broadcast data
	m_structEventMissionRequestFailed Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_FAILED
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_PLAYER_ACTIVE(paramPlayerID)
		PRINTLN("...KGM MP [MControl]: (SERVER) Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID() - ERROR: NETWORK_IS_PLAYER_ACTIVE(paramPlayerID)")
		EXIT
	ENDIF
	
	// Broadcast the event to ALL PLAYERS
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_FAILED to specific player (cutdown details)") NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		IF (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
			NET_PRINT("  [PlayerID = ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_PRINT("]")
		ENDIF
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission Request Failed
//
// INPUT PARAMS:			paramSlotID				// The slot containing the mission data
//
// NOTES:	This now forwards the request to the version of this command that accepts a uniqueID.
PROC Broadcast_Mission_Request_Failed(INT paramSlotID)
	
	INT uniqueID = GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID
	
	// Error Checking
	IF (uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Failed() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlotID)
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Failed() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Pass the event for broadcast
	Broadcast_Mission_Request_Failed_For_UniqueID(uniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........(SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_FAILED (additional details for an already stored request)") NET_NL()
		NET_PRINT("           [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Mission Full
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionRequestMissionFull
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The unique Mission Request ID for a mission request that failed
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission Request Mission Is Full
//
// INPUT PARAMS:			paramSlotID				The slot containing the mission data
//							paramPlayerID			The Player the broadcast is intended for
//							paramSpecificUniqueID	[DEFAULT = NO_UNIQUE_ID] Used if a UniqueID other than the slot's uniqueID should be used when sending the message
//
// NOTES:	This request is always intended for an individual player
//			KGM 13/1/15 [BUG 2143257]: The uniqueID is used to inform excess players standing in the Heist Leader's planning room when a heist is setup that there's no room for the player
PROC Broadcast_Mission_Request_Mission_Full(PLAYER_INDEX paramPlayerID, INT paramSlotID, INT paramSpecificUniqueID = NO_UNIQUE_ID)

	// Store the Broadcast data
	m_structEventMissionRequestMissionFull Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_MISSION_FULL
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID
	
	// KGM 13/1/15 [BUG 2143257]: Use specific UniqueID for the broadcast?
	IF (paramSpecificUniqueID != NO_UNIQUE_ID)
		Event.uniqueID = paramSpecificUniqueID
	ENDIF
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_MissionFull() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlotID)
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_MissionFull() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Broadcast the event to a SPECIFIC PLAYER
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_MISSION_FULL") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]  [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		IF (paramSpecificUniqueID != NO_UNIQUE_ID)
			NET_PRINT(" - But Using Specific UniqueID: ")
			NET_PRINT_INT(paramSpecificUniqueID)
		ENDIF
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Change UniqueID
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionRequestChangeUniqueID
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							oldUniqueID				// The original unique Mission Request ID for a mission request
	INT							newUniqueID				// The replaced unique Mission Request ID for a mission request
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission Request UniqueID has changed
//
// INPUT PARAMS:			paramPlayerID			// The Player the broadcast is intended for
//							paramOldUniqueID		// The original uniqueID
//							paramNewUniqueID		// The replacement uniqueID
//
// NOTES:	This request is always intended for an individual player
//			A uniqueID can change for a few reasons, but mainly because a player has been put onto an existing instance of a similar mission.
PROC Broadcast_Mission_Request_Change_UniqueID(PLAYER_INDEX paramPlayerID, INT paramOldUniqueID, INT paramNewUniqueID)

	// Store the Broadcast data
	m_structEventMissionRequestChangeUniqueID Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_CHANGE_UNIQUEID
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.oldUniqueID				= paramOldUniqueID
	Event.newUniqueID				= paramNewUniqueID
	
	// Error Checking
	IF (Event.oldUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Change_UniqueID() - ERROR: Old Unique ID is NO_UNIQUE_ID") NET_NL()
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Change_UniqueID() - ERROR: Old Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	IF (Event.newUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Change_UniqueID() - ERROR: New Unique ID is NO_UNIQUE_ID") NET_NL()
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Change_UniqueID() - ERROR: New Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF

	// Broadcast the event to a SPECIFIC PLAYER
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_CHANGE_UNIQUEID") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]  [OldUniqueID = ") NET_PRINT_INT(paramOldUniqueID)
		NET_PRINT("]  [NewUniqueID = ") NET_PRINT_INT(paramNewUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Player On Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionRequestPlayerOnMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The unique Mission Request ID for a mission that the mission controller knows that the player has joined
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that Mission Request Player On Mission
//
// INPUT PARAMS:			paramSlotID				// The slot containing the mission data
//							paramPlayerID			// The Player being told the mission controller knows he has joined the mission
//
// NOTES:	This request is always intended for a specific player
PROC Broadcast_Mission_Request_Player_On_Mission(INT paramSlotID, PLAYER_INDEX paramPlayerID)

	// Store the Broadcast data
	m_structEventMissionRequestPlayerOnMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_REQUEST_PLAYER_ON_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcast_Mission_Request_Player_On_Mission() - ERROR: Unique ID is NO_UNIQUE_ID") NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(paramSlotID)
		
			SCRIPT_ASSERT("Broadcast_Mission_Request_Player_On_Mission() - ERROR: Unique ID is NO_UNIQUE_ID. See Console Log. Tell Keith.")
		#ENDIF
			
		EXIT
	ENDIF
	
	// Broadcast the event to ALL PLAYERS
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_MISSION_REQUEST_PLAYER_ON_MISSION") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC





// -----------------------------------------------------------------------------------------------------------
//      Event: Confirmation Player Should Launch Mission
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventConfirmationLaunchMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data (to ensure it is available when mission triggering needs it - in case the slot data is a bit slow getting broadcast)
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast Confirmation Player SHould Join Mission
//
// INPUT PARAMS:			paramPlayerID			// The Player Index the request is being sent to
//							paramSlotID				// The slot containing the mission data
//
// NOTES:	This request is always intended for a specific player
PROC Broadcast_Confirmation_Player_Should_Launch_Mission(PLAYER_INDEX paramPlayerID, INT paramSlotID)

	// Store the Broadcast data
	m_structEventConfirmationLaunchMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_CONFIRMATION_LAUNCH_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData
	
	// Broadcast the event to a SPECIFIC PLAYER
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........(SERVER) Broadcasting SCRIPT_EVENT_CONFIRMATION_LAUNCH_MISSION") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]  [MissionSlot = ")
		NET_PRINT_INT(paramSlotID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Reason For Mission Request Failure
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventReasonForMissionRequestFailure
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	m_eFailedToLaunchReasons	reason					// Reason for the mission failing to launch
	MP_MISSION					mission					// The mission that failed to launch
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the reason for failure is valid from this source
//
// INPUT PARAMS:			paramSourceID				The sourceID for the mission request
//							paramReasonID				The reasonID to explain why the mission failed to launch
// RETURN VALUE:			BOOL						TRUE if the source/reason combo is valid, otherwise FALSE
FUNC BOOL Is_Reason_For_Failure_Valid_From_This_Source(g_eMPMissionSource paramSourceID, m_eFailedToLaunchReasons paramReasonID)

	SWITCH (paramSourceID)
		// Valid Sources
		CASE MP_MISSION_SOURCE_DEBUG_MENU
		CASE MP_MISSION_SOURCE_MISSION_FLOW_BLIP
			BREAK
			
		// These sources are not valid
		CASE MP_MISSION_SOURCE_GROUPED
		CASE MP_MISSION_SOURCE_JOBLIST
		CASE MP_MISSION_SOURCE_INVITE
		CASE MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN
		CASE MP_MISSION_SOURCE_DEBUG_GROUPED
		CASE MP_MISSION_SOURCE_MISSION_FLOW_AUTO
		CASE MP_MISSION_SOURCE_AMBIENT
		CASE MP_MISSION_SOURCE_HEIST
		CASE UNKNOWN_MISSION_SOURCE
			RETURN FALSE
			
		// UNKNOWN SOURCE
		DEFAULT
			SCRIPT_ASSERT("Is_Reason_For_Failure_Valid_From_This_Source(): Need to add additional SourceID to SWITCH. Tell Keith.")
			RETURN FALSE
	ENDSWITCH
	
	// From Valid Sources, do specific Reason checks
	IF (paramSourceID = MP_MISSION_SOURCE_DEBUG_MENU)
	OR (paramSourceID = MP_MISSION_SOURCE_MISSION_FLOW_BLIP)
		// All reasons are valid for the Debug Menu (and mission flow?)
		SWITCH (paramReasonID)
			CASE FTLR_ARRAY_FULL
			CASE FTLR_DATA_SETUP_ERROR
			CASE FTLR_TEAM_REQUIREMENTS_NOT_MET
			CASE FTLR_PRISON_MISSION_ALREADY_EXISTS
			CASE FTLR_MISSION_INSTANCE_ACTIVE
			CASE FTLR_MISSION_VARIATION_INSTANCE_ACTIVE
				RETURN TRUE
				
			DEFAULT
				SCRIPT_ASSERT("Is_Reason_For_Failure_Valid_From_This_Source(): Need to add additional ReaonIDs to DEBUG MENU SWITCH. Tell Keith.")
				RETURN FALSE
		ENDSWITCH
		
		// Catch All - unnecessary
		RETURN FALSE
	ENDIF
	
	// Shouldn't reach here
	SCRIPT_ASSERT("Is_Reason_For_Failure_Valid_From_This_Source(): A source has been classed as VALID but contains no reason checks. Tell Keith.")
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast the reason that a mission failed to launch
//
// INPUT PARAMS:			paramIsHost					TRUE if the requester is a script host (rather than an individual player)
//							paramPlayerID				The player to send the broadcast to
//							paramSourceID				The sourceID for the mission request
//							paramReasonID				The reasonID to explain why the mission failed to launch
//							paramMissionID				The missionID that failed to launch
//
// NOTES:	This request is intended for a specific player
PROC Broadcast_Reason_For_Mission_Request_Failure(BOOL paramIsHost, PLAYER_INDEX paramPlayerID, g_eMPMissionSource paramSourceID, m_eFailedToLaunchReasons paramReasonID, MP_MISSION paramMissionID)

	// Ignore if the requester is a HOST (only send to specific requesters)
	IF (paramIsHost)
		EXIT
	ENDIF
	
	// Only send if the request came from a source that should receive a reason for failure
	// Also some sources should only send some reasons for failure (eg: The Joblist may only send reasons that can be displayed in the released game)
	IF NOT (Is_Reason_For_Failure_Valid_From_This_Source(paramSourceID, paramReasonID))
		EXIT
	ENDIF

	// Store the Broadcast data
	m_structEventReasonForMissionRequestFailure Event
	
	Event.Details.Type				= SCRIPT_EVENT_REASON_FOR_MISSION_REQUEST_FAILURE
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.reason					= paramReasonID
	Event.mission					= paramMissionID
	
	// Broadcast the event to a SPECIFIC PLAYER
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_REASON_FOR_MISSION_REQUEST_FAILURE") NET_NL()
		NET_PRINT("           [ToPlayer = ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT("]  [Mission = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Request New Mission Of Type
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRequestNewMissionOfType
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	BOOL						requesterIsHost			// TRUE if the requesting player was acting as script host, FALSE if acting as an individual player
	INT							requestFromTeam			// The Team ID if a specific team should be a priority for going on the mission
	g_eMPMissionSource			requestSourceID			// The Source of the Mission Request
	MP_MISSION_TYPE				missionTypeID			// The type of mission required
	INT							uniqueID				// A unique ID for this mission request
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to launch any mission of a specific mission type
//
// INPUT PARAMS:		paramIsHost				// TRUE if the request came from a host, FALSE if from an individual player
//						paramSourceID			// Request Source ID
//						paramMissionTypeID		// The Type of Mission requested
//						paramTeam				// [DEFAULT=TEAM_INVALID] A team that should be a priority for joining the mission
// RETURN VALUE:		INT						// The unique ID generated for this mission request
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
FUNC INT Broadcast_Request_New_Mission_Of_Type(BOOL paramIsHost, g_eMPMissionSource paramSourceID, MP_MISSION_TYPE paramMissionTypeID, INT paramTeam = TEAM_INVALID)
	
	// Generate unique Mission Request ID
	INT uniqueID = Generate_Unique_Mission_Request_ID()

	// Store the Broadcast data
	m_structEventRequestNewMissionOfType Event
	
	Event.Details.Type				= SCRIPT_EVENT_REQUEST_NEW_MISSION_OF_TYPE
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.requesterIsHost			= paramIsHost
	Event.requestFromTeam			= paramTeam
	Event.requestSourceID			= paramSourceID
	Event.missionTypeID				= paramMissionTypeID
	Event.uniqueID					= uniqueID
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_REQUEST_NEW_MISSION_OF_TYPE, uniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_REQUEST_NEW_MISSION_OF_TYPE: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [IsHost = ")
		IF (paramIsHost)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT("]  [SourceID = ")
		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID))
		NET_PRINT("]  [MissionTypeID = ")
		NET_PRINT(Convert_Mission_Type_To_String(paramMissionTypeID))
		NET_PRINT("]  [Team = ")
		NET_PRINT(Convert_Team_To_String(paramTeam))
		NET_PRINT("]  [UniqueID = ")
		NET_PRINT_INT(uniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Return the generated UniqueID
	RETURN (uniqueID)
	
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Event: Request New Specific Mission
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRequestNewSpecificMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	BOOL						requesterIsHost			// TRUE if the requesting player was acting as script host, FALSE if acting as an individual player
	INT							requestFromTeam			// The Team ID if a specific team should be a priority for going on the mission
	g_eMPMissionSource			requestSourceID			// The Source of the Mission Request
	MP_MISSION_ID_DATA			missionIdData			// Any pre-determined mission Id data
	INT							uniqueID				// The uniqueID for the mission
	INT							instanceID				// The InstanceID for the mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to launch a specific mission
//
// INPUT PARAMS:		paramIsHost				// TRUE if the request came from a host, FALSE if from an individual player
//						paramSourceID			// Request Source ID
//						paramMissionData		// The pre-determined Mission Data
//						paramTeam				// [DEFAULT=TEAM_INVALID] A team that should be a priority for joining the mission
// RETURN VALUE:		INT						// The unique ID generated for this mission request
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
FUNC INT Broadcast_Request_New_Specific_Mission(BOOL paramIsHost, g_eMPMissionSource paramSourceID, MP_MISSION_DATA paramMissionData, INT paramTeam = TEAM_INVALID)

	// The Mission Data must include a valid missionID
	IF (paramMissionData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Request_New_Specific_Mission - ERROR: MissionID is eNULL_MISSION. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Request_New_Specific_Mission() - ERROR MissionID is eNULL_MISSION. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// If the requester is not the host, then any team passed as parameter should match the requesting player's team or should be set to be the player's team if not specified
	INT storeThisTeam = GET_PLAYER_TEAM(PLAYER_ID())
	IF NOT (paramIsHost)
		IF NOT (paramTeam = TEAM_INVALID)
			// A specific team has been requested, so ensure it is the player's team
			IF NOT (storeThisTeam = paramTeam)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MControl]: Broadcast_Request_New_Specific_Mission - ERROR: Required Team is set, but does not match the Requesting Player's Team. Requested by: ")
					NET_PRINT(GET_THIS_SCRIPT_NAME())
					NET_NL()
					
					NET_PRINT("       Required Team: ")
					NET_PRINT(Convert_Team_To_String(paramTeam))
					NET_NL()
					
					NET_PRINT("       Player Team: ")
					NET_PRINT(Convert_Team_To_String(storeThisTeam))
					NET_PRINT("  [")
					NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
					NET_PRINT("]")
					NET_NL()
					
					SCRIPT_ASSERT("Broadcast_Request_New_Specific_Mission() - ERROR Inconsistency between specified team and player's Team. Ignoring Request. Check console log. Tell Keith.")
				#ENDIF
				
				RETURN NO_UNIQUE_ID
			ENDIF
		ENDIF
	ELSE
		// The requester is the host, so the team to store as required should be set to be the team passed as parameter (which is ok to be TEAM_INVALID)
		storeThisTeam = paramTeam
	ENDIF
	
	// Generate unique Mission Request ID
	INT uniqueID = Generate_Unique_Mission_Request_ID()

	// Store the Broadcast data
	m_structEventRequestNewSpecificMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_REQUEST_NEW_SPECIFIC_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.requesterIsHost			= paramIsHost
	Event.requestFromTeam			= storeThisTeam
	Event.requestSourceID			= paramSourceID
	Event.missionIdData				= paramMissionData.mdID
	Event.uniqueID					= uniqueID
	Event.instanceID				= paramMissionData.iInstanceId
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_REQUEST_NEW_SPECIFIC_MISSION, uniqueID, paramMissionData.mdID.idMission, paramMissionData.mdID.idVariation)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_REQUEST_NEW_SPECIFIC_MISSION: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [IsHost = ")
		IF (paramIsHost)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT("]  [SourceID = ")
		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID))
		NET_PRINT("]  [Team = ")
		NET_PRINT(Convert_Team_To_String(storeThisTeam))
		NET_PRINT("]  [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ")
		NET_PRINT_INT(uniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
	// Return the unique ID
	RETURN uniqueID
	
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Event: Request Join Existing Mission
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRequestJoinExistingMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// UniqueID of the mission request being joined
	g_eMPMissionSource			sourceID				// The sourceID for the request
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to join an existing specific mission (probably from JobList)
//
// INPUT PARAMS:		paramUniqueID			// The uniqueID for the mission being joined
//						paramSourceID			// The sourceID for this request
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in
PROC Broadcast_Request_Join_Existing_Mission(INT paramUniqueID, g_eMPMissionSource paramSourceID)

	// There must be a valid uniqueID
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Request_Join_Existing_Mission - ERROR: UniqueID is NO_UNIQUE_ID. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Request_Join_Existing_Mission() - ERROR UniqueID is NO_UNIQUE_ID. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Store the Broadcast data
	m_structEventRequestJoinExistingMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_REQUEST_JOIN_EXISTING_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	Event.sourceID					= paramSourceID
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_REQUEST_JOIN_EXISTING_MISSION, paramUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_REQUEST_JOIN_EXISTING_MISSION: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]  [SourceID = ")
		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID))
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Confirm Reserved For Mission
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventConfirmReservedForMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							bitsReservedPlayers		// Bitfield containing all players reserved for the mission (including this player)
	INT							numReservedPlayers		// The number of reserved players (including this player)
	INT							oldUniqueID				// The original UniqueID for the reservation request
	INT							newUniqueID				// The replacement UniqueID for the existing reserved mission request this player was added to
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast confirm reserved for mission
//
// INPUT PARAMS:		paramPlayerID			// The player this confirmation is being broadcast to
//						paramNewUniqueID		// The new uniqueID this reservation request is changing to
//						paramOldUniqueID		// The original uniqueID passed with the reservation request
//						paramBitsPlayers		// A bitfield containing all the other player currently required for this mission
//
// NOTES:	Assumes the player is still OK and that the mission request slot is valid
PROC Broadcast_Confirm_Reserved_For_Mission(PLAYER_INDEX paramPlayerID, INT paramNewUniqueID, INT paramOldUniqueID, INT paramBitsPlayers)

	// Ensure the old UniqueID is valid - this should be checked prior to this call, but assert just in case
	IF (paramOldUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Broadcast_Confirm_Reserved_For_Mission - ERROR: paramOldUniqueID is NO_UNIQUE_ID. Check console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Store the Broadcast data
	m_structEventConfirmReservedForMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_CONFIRM_RESERVED_FOR_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.bitsReservedPlayers		= paramBitsPlayers
	
	// Count number of reserved players
	INT tempCount = 0
	INT tempLoop = 0
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(Event.bitsReservedPlayers, tempLoop))
			tempCount++
		ENDIF
	ENDREPEAT
	
	Event.numReservedPlayers		= tempCount
	
	Event.oldUniqueID				= paramOldUniqueID
	Event.newUniqueID				= paramNewUniqueID
	
	#IF IS_DEBUG_BUILD
		NET_NL()
	#ENDIF
	
	// Broadcast the event to the specified player
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(paramPlayerID))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Broadcasting SCRIPT_EVENT_CONFIRM_RESERVED_FOR_MISSION to ") NET_PRINT(GET_PLAYER_NAME(paramPlayerID)) NET_NL()
		NET_PRINT("    Sending Num Players Reserved For Mission: ")
		NET_PRINT_INT(Event.numReservedPlayers)
		NET_NL()
		NET_PRINT("    Sending Player Bits Reserved For Mission:")
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(Event.bitsReservedPlayers)
		NET_PRINT("    Original Unique ID: ")
		NET_PRINT_INT(Event.oldUniqueID)
		NET_NL()
		NET_PRINT("    Replace With Mission Unique ID: ")
		NET_PRINT_INT(Event.newUniqueID)
		NET_NL()
		NET_PRINT("    (Unique IDs should be the same if this is the first reserved player, but different if there are already reserved players)")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reserve Specific Mission By Player
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventReserveMissionByPlayer
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							requestFromTeam			// The Team ID if a specific team should be a priority for going on the mission
	g_eMPMissionSource			requestSourceID			// The Source of the Mission Request
	MP_MISSION_ID_DATA			missionIdData			// Any pre-determined missionID data
	BOOL						hasCutscene				// TRUE if there is a pre-mission cutscene, otherwise FALSE
	BOOL						forceOwnInstance		// TRUE if the player wants to force their own instance of the mission (ie: host it), FALSE if ok to join existing instance in same session
	INT							tutorialSessionID		// The player's existing tutorial session ID [TUTORIAL_SESSION_NOT_ACTIVE means main session]
	PLAYER_INDEX				invitingPlayer			// The Inviting Player (or INVALID_PLAYER_INDEX() if not being invited onto the mission by a particular player)
	BOOL						inInvitingPlayerTS		// TRUE if already in the Inviting Player's Transition Session, otherwise FALSE
	INT							uniqueID				// The UniqueID for the mission request
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to reserve a specific mission by a specific player
//
// INPUT PARAMS:		paramSourceID				// Request Source ID
//						paramMissionData			// The pre-determined Mission Data
//						paramHasCutscene			// TRUE if the mission should show a pre-mission cutscene, otherwise FALSE
//						paramOwnInstance			// [DEFAULT = FALSE] TRUE if the player should start a new instance of the mission in this session, FALSE if ok to join an existing instance
//						paramTeam					// [DEFAULT = TEAM_INVALID] A team that should be a priority for joining the mission
//						paramTutorialSessionID		// [DEFAULT = TUTORIAL_SESSION_NOT_ACTIVE] The players existing tutorial session ID that restricts which missions can be joined
//						paramInvitorAsInt			// [DEFAULT = -1] The Inviting Player as an INT (to allow an appropriate default value)
//						paramInInvitorsTransition	// [DEFAULT = FALSE] TRUE if there is a known inviting player and the joining player is already in the inviting player's transition session, otherwise FALSE
// RETURN VALUE:		INT							// The unique ID generated for this mission request
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in.
//			This request can only come from an individual, not from a script host.
FUNC INT Broadcast_Reserve_Mission_By_Player(g_eMPMissionSource paramSourceID, MP_MISSION_DATA paramMissionData, BOOL paramHasCutscene, BOOL paramOwnInstance = FALSE, INT paramTeam = TEAM_INVALID, INT paramTutorialSessionID = TUTORIAL_SESSION_NOT_ACTIVE, INT paramInvitorAsInt = -1, BOOL paramInInvitorsTransition = FALSE)

	// Ignore if this player is SCTV
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Reserve_Mission_By_Player - ERROR: Player is SCTV. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Reserve_Mission_By_Player() - ERROR: Player is SCTV. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN NO_UNIQUE_ID
	ENDIF

	// The Mission Data must include a valid missionID
	IF (paramMissionData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Reserve_Mission_By_Player - ERROR: MissionID is eNULL_MISSION. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Reserve_Mission_By_Player() - ERROR MissionID is eNULL_MISSION. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Any team passed as parameter should match the requesting player's team or should be set to be the player's team if not specified
	INT storeThisTeam = GET_PLAYER_TEAM(PLAYER_ID())
	IF NOT (paramTeam = TEAM_INVALID)
		// A specific team has been requested, so ensure it is the player's team
		IF NOT (storeThisTeam = paramTeam)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Broadcast_Reserve_Mission_By_Player - ERROR: Required Team is set, but does not match the Requesting Player's Team. Requested by: ")
				NET_PRINT(GET_THIS_SCRIPT_NAME())
				NET_NL()
				
				NET_PRINT("       Required Team: ")
				NET_PRINT(Convert_Team_To_String(paramTeam))
				NET_NL()
				
				NET_PRINT("       Player Team: ")
				NET_PRINT(Convert_Team_To_String(storeThisTeam))
				NET_PRINT("  [")
				NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
				NET_PRINT("]")
				NET_NL()
				
				SCRIPT_ASSERT("Broadcast_Reserve_Mission_By_Player() - ERROR Inconsistency between specified team and player's Team. Ignoring Request. Check console log. Tell Keith.")
			#ENDIF
			
			RETURN NO_UNIQUE_ID
		ENDIF
	ENDIF
	
	// Is there an optional inviting player whose mission should be joined?
	PLAYER_INDEX theInvitingPlayer = INVALID_PLAYER_INDEX()
	IF (paramInvitorAsInt != -1)
		theInvitingPlayer = INT_TO_PLAYERINDEX(paramInvitorAsInt)
	ENDIF
	
	// Generate unique Mission Request ID
	INT uniqueID = Generate_Unique_Mission_Request_ID()

	// Store the Broadcast data
	m_structEventReserveMissionByPlayer Event
	
	Event.Details.Type				= SCRIPT_EVENT_RESERVE_MISSION_BY_PLAYER
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.requestFromTeam			= storeThisTeam
	Event.requestSourceID			= paramSourceID
	Event.missionIdData				= paramMissionData.mdID
	Event.hasCutscene				= paramHasCutscene
	Event.forceOwnInstance			= paramOwnInstance
	Event.tutorialSessionID			= paramTutorialSessionID
	Event.invitingPlayer			= theInvitingPlayer
	Event.inInvitingPlayerTS		= paramInInvitorsTransition
	Event.uniqueID					= uniqueID
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_RESERVE_MISSION_BY_PLAYER, uniqueID, paramMissionData.mdID.idMission, paramMissionData.mdID.idVariation)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_RESERVE_MISSION_BY_PLAYER: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [SourceID = ")
		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID))
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("           [Team = ")
		NET_PRINT(Convert_Team_To_String(storeThisTeam))
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("           [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]")
		NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(uniqueID)
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("           [TutSessionID = ")
		IF (paramTutorialSessionID = TUTORIAL_SESSION_NOT_ACTIVE)
			NET_PRINT("Main Session")
		ELSE
			NET_PRINT_INT(paramTutorialSessionID)
		ENDIF
		NET_PRINT("]")
		NET_NL()
		IF (paramOwnInstance)
			NET_PRINT("           [FORCE OWN INSTANCE OF MISSION]")
			NET_NL()
		ENDIF
		IF (theInvitingPlayer != INVALID_PLAYER_INDEX())
			NET_PRINT("           [INVITED BY: ") NET_PRINT(GET_PLAYER_NAME(theInvitingPlayer))
			IF (paramInInvitorsTransition)
				NET_PRINT(" (In Inviting Player's Transition Session)")
			ENDIF
			NET_PRINT("]")
			NET_NL()
		ENDIF
		IF (paramHasCutscene)
			NET_PRINT("           [Has Pre-Mission Cutscene]")
			NET_NL()
		ENDIF
	#ENDIF
	
	// Return the unique ID
	RETURN uniqueID
	
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reserve Specific Mission By Leader
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventReserveMissionByLeader
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							requestFromTeam			// The Team ID if a specific team should be a priority for going on the mission
	g_eMPMissionSource			requestSourceID			// The Source of the Mission Request
	MP_MISSION_ID_DATA			missionIdData			// Any pre-determined missionID data
	BOOL						hasCutscene				// TRUE if there is a pre-mission cutscene, otherwise FALSE
	INT							bitsPlayersToReserve	// Bitfield of players to be 'Reserved by Leader' - this can include Leader but Leader will automatically be 'Reserved By Player'
	INT							uniqueID				// The uniqueID for the mission request
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to reserve a specific mission by a Leader
//
// INPUT PARAMS:		paramSourceID			// Request Source ID
//						paramMissionData		// The pre-determined Mission Data
//						paramHasCutscene		// TRUE if the mission should show a pre-mission cutscene, otherwise FALSE
//						paramBitsPlayers		// A bitfield containing all players that should be reserved for the mission
//						paramTeam				// [DEFAULT=TEAM_INVALID] A team that should be a priority for joining the mission
// RETURN VALUE:		INT						// The unique ID generated for this mission request
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need target player bits passed in.
//			This request can only come from an individual, not from a script host.
FUNC INT Broadcast_Reserve_Mission_By_Leader(g_eMPMissionSource paramSourceID, MP_MISSION_DATA paramMissionData, BOOL paramHasCutscene, INT paramBitsPlayers, INT paramTeam = TEAM_INVALID)

	// The Mission Data must include a valid missionID
	IF (paramMissionData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Reserve_Mission_By_Leader - ERROR: MissionID is eNULL_MISSION. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Reserve_Mission_By_Leader() - ERROR MissionID is eNULL_MISSION. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN NO_UNIQUE_ID
	ENDIF
	
	// Any team passed as parameter should match the Leader's team or should be set to be the Leader's team if not specified
	INT storeThisTeam = GET_PLAYER_TEAM(PLAYER_ID())
	IF NOT (paramTeam = TEAM_INVALID)
		// A specific team has been requested, so ensure it is the Leader's team
		IF NOT (storeThisTeam = paramTeam)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MControl]: Broadcast_Reserve_Mission_By_Leader - ERROR: Required Team is set, but does not match the Leader's Team. Requested by: ")
				NET_PRINT(GET_THIS_SCRIPT_NAME())
				NET_NL()
				
				NET_PRINT("       Required Team: ")
				NET_PRINT(Convert_Team_To_String(paramTeam))
				NET_NL()
				
				NET_PRINT("       Leader Team: ")
				NET_PRINT(Convert_Team_To_String(storeThisTeam))
				NET_PRINT("  [")
				NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
				NET_PRINT("]")
				NET_NL()
				
				SCRIPT_ASSERT("Broadcast_Reserve_Mission_By_Leader() - ERROR Inconsistency between specified team and Leader's Team. Ignoring Request. Check console log. Tell Keith.")
			#ENDIF
			
			RETURN NO_UNIQUE_ID
		ENDIF
	ENDIF
	
	// Ensure the player bits contain data, otherwise set the Leader's bit and let processing continue
	IF (paramBitsPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Reserve_Mission_By_Leader - ERROR: No Player Bits set. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_PRINT(" - Setting the Leader's bit and continuing")
			NET_NL()
				
			SCRIPT_ASSERT("Broadcast_Reserve_Mission_By_Leader() - ERROR Request contains no player bits. Setting Leader bit and continuing. Tell Keith.")
		#ENDIF
		
		INT leaderPlayer = NATIVE_TO_INT(PLAYER_ID())
		SET_BIT(paramBitsPlayers, leaderPlayer)
	ENDIF
	
	// Generate unique Mission Request ID
	INT uniqueID = Generate_Unique_Mission_Request_ID()

	// Store the Broadcast data
	m_structEventReserveMissionByLeader Event
	
	Event.Details.Type				= SCRIPT_EVENT_RESERVE_MISSION_BY_LEADER
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.requestFromTeam			= storeThisTeam
	Event.requestSourceID			= paramSourceID
	Event.missionIdData				= paramMissionData.mdID
	Event.hasCutscene				= paramHasCutscene
	Event.bitsPlayersToReserve		= paramBitsPlayers
	Event.uniqueID					= uniqueID
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_RESERVE_MISSION_BY_LEADER, uniqueID, paramMissionData.mdID.idMission, paramMissionData.mdID.idVariation)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_RESERVE_MISSION_BY_LEADER: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [SourceID = ")
		NET_PRINT(Convert_Mission_Source_To_String(paramSourceID))
		NET_PRINT("]  [Team = ")
		NET_PRINT(Convert_Team_To_String(storeThisTeam))
		NET_PRINT("]  [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Var=")
		NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ")
		NET_PRINT_INT(uniqueID)
		NET_PRINT("]")
		IF (paramHasCutscene)
			NET_PRINT(" - Has Pre-Mission Cutscene")
		ELSE
			NET_PRINT(" - no pre-mission cutscene")
		ENDIF
		NET_NL()
		NET_PRINT("          All Players Being Reserved For Mission:")
		Debug_Output_Player_Names_From_Bitfield(paramBitsPlayers)
		NET_NL()
	#ENDIF
	
	// Return the unique ID
	RETURN uniqueID
	
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Event: Start Reserved Mission
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventStartReservedMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							bitsStartingPlayers		// A bitfield with all the players that are going to start the mission
	INT							uniqueID				// The UniqueID of the reserved mission that should now start
	
	#IF IS_DEBUG_BUILD
	BOOL						debugAllowWithOnePlayer	// TRUE if the mission can ignore minimum player requirements, FALSE to obey minimum player requirements
	#ENDIF
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to start a reserved mission
//
// INPUT PARAMS:		paramUniqueID			Unique ID of the reserved mission
//						paramBitsPlayers		A bitfield containing the legal starting players
// RETURN VALUE:		BOOL					TRUE if the broadcast has been sent, otherwise FALSE
//
// NOTES:	This request is always intended for the Mission Control server.
//			This request also indicates all the legal starting players - all of whom should have previously reserved the mission and be 'required players' on it.
FUNC BOOL Broadcast_Start_Reserved_Mission(INT paramUniqueID, INT paramBitsPlayers)

	// The UniqueID needs to be valid
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Start_Reserved_Mission - ERROR: UniqueID is NO_UNIQUE_ID. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Start_Reserved_Mission() - ERROR UniqueID is NO_UNIQUE_ID. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// There must be player bits passed in
	IF (paramBitsPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Start_Reserved_Mission - ERROR: There are no player bits set. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Start_Reserved_Mission() - ERROR There are no player bits set. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// The player starting the mission should be part of the player bits
	PLAYER_INDEX thisPlayerIndex = PLAYER_ID()
	INT thisPlayerAsInt = NATIVE_TO_INT(thisPlayerIndex)
	IF NOT (IS_BIT_SET(paramBitsPlayers, thisPlayerAsInt))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Start_Reserved_Mission - ERROR: The requesting player's bit is not set. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			NET_PRINT("      Requesting Player: ")
			NET_PRINT(GET_PLAYER_NAME(thisPlayerIndex))
			NET_NL()
			NET_PRINT("      Player Bits Set:")
			NET_NL()
			Debug_Output_Player_Names_From_Bitfield(paramBitsPlayers)
			
			SCRIPT_ASSERT("Broadcast_Start_Reserved_Mission() - ERROR This player's bit is not set. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Store the Broadcast data
	m_structEventStartReservedMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_START_RESERVED_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.bitsStartingPlayers		= paramBitsPlayers
	Event.uniqueID					= paramUniqueID
	
	#IF IS_DEBUG_BUILD
		Event.debugAllowWithOnePlayer	= g_missionControlAllowLaunchWithOnePlayer
	#ENDIF
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_START_RESERVED_MISSION, paramUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_START_RESERVED_MISSION: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("      UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_NL()
		NET_PRINT("      Starting Players")
		IF (g_missionControlAllowLaunchWithOnePlayer)
			NET_PRINT(" [DEBUG: Allowing Launch With One Player]")
		ENDIF
		NET_PRINT(":")
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(paramBitsPlayers)
	#ENDIF
	
	// Everything OK
	RETURN TRUE
	
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Event: Cancel Mission Reservation By Player
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventCancelMissionReservationByPlayer
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID of the mission from which the player is removing his reservation
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to cancel a mission reservation by a player
//
// INPUT PARAMS:		paramUniqueID			// The Unique ID of the mission
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in.
//			This request can only come from an individual, not from a script host.
PROC Broadcast_Cancel_Mission_Reservation_By_Player(INT paramUniqueID)

	// The Mission Data must include a valid uniqueID
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Cancel_Mission_Reservation_By_Player() - ERROR: UniqueID is NO_UNIQUE_ID. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Cancel_Mission_Reservation_By_Player() - ERROR: UniqueID is NO_UNIQUE_ID. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Store the Broadcast data
	m_structEventCancelMissionReservationByPlayer Event
	
	Event.Details.Type				= SCRIPT_EVENT_CANCEL_MISSION_RESERVATION_BY_PLAYER
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_CANCEL_MISSION_RESERVATION_BY_PLAYER, paramUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_CANCEL_MISSION_RESERVATION_BY_PLAYER: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Cancel Mission Reserved By Leader
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventCancelMissionReservedByLeader
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID of the mission reservation that is being cancelled
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to cancel a mission that was reserved by a leader
//
// INPUT PARAMS:		paramUniqueID			// The Unique ID of the mission
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in.
//			This request can only come from an individual, not from a script host.
PROC Broadcast_Cancel_Mission_Reserved_By_Leader(INT paramUniqueID)

	// The Mission Data must include a valid uniqueID
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Cancel_Mission_Reserved_By_Leader() - ERROR: UniqueID is NO_UNIQUE_ID. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Cancel_Mission_Reserved_By_Leader() - ERROR: UniqueID is NO_UNIQUE_ID. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Store the Broadcast data
	m_structEventCancelMissionReservedByLeader Event
	
	Event.Details.Type				= SCRIPT_EVENT_CANCEL_MISSION_RESERVED_BY_LEADER
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_CANCEL_MISSION_RESERVED_BY_LEADER, paramUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_CANCEL_MISSION_RESERVED_BY_LEADER: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Change Reserved Mission Details
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventChangeReservedMissionDetails
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID of the mission from which the player is removing his reservation
	MP_MISSION_ID_DATA			newMissionIdData		// The modified missionID data
	BOOL						isHost					// TRUE if this came from the host of the calling script, FALSE if it came from an individual on the mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast a request to change the mission details
//
// INPUT PARAMS:		paramUniqueID				The Unique ID of the mission
//						paramIsHost					TRUE if this change is from a script host, FALSE if from an individual player on the mission
// RETURN PARAMS:		paramMissionIdData			The new Mission Id Details
//
// NOTES:	This request is always intended for the Mission Control server, so it doesn't need player bits passed in.
PROC Broadcast_Change_Reserved_Mission_Details(INT paramUniqueID, MP_MISSION_ID_DATA &paramMissionIdData, BOOL paramIsHost)

	// The UniqueID must be valid
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Change_Reserved_Mission_Details() - ERROR: UniqueID is NO_UNIQUE_ID. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Change_Reserved_Mission_Details() - ERROR: UniqueID is NO_UNIQUE_ID. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// The MissionID must be valid
	IF (paramMissionIdData.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Broadcast_Change_Reserved_Mission_Details() - ERROR: MissionID is eNULL_MISSION. Requested by: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			
			SCRIPT_ASSERT("Broadcast_Change_Reserved_Mission_Details() - ERROR: MissionID is eNULL_MISSION. Ignoring Request. Check console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Store the Broadcast data
	m_structEventChangeReservedMissionDetails Event
	
	Event.Details.Type				= SCRIPT_EVENT_CHANGE_RESERVED_MISSION_DETAILS
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	Event.newMissionIdData			= paramMissionIdData
	Event.isHost					= paramIsHost
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_CHANGE_RESERVED_MISSION_DETAILS, paramUniqueID, paramMissionIdData.idMission, paramMissionIdData.idVariation)
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: Broadcasting SCRIPT_EVENT_CHANGE_RESERVED_MISSION_DETAILS: requested by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [UniqueID = ")
		NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]  [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionIdData.idMission))
		NET_PRINT(" (Var=")
		NET_PRINT_INT(paramMissionIdData.idVariation)
		NET_PRINT(")]  [CreatorID = ")
		NET_PRINT_INT(paramMissionIdData.idCreator)
		NET_PRINT("]  [Cloud Filename = ")
		NET_PRINT(paramMissionIdData.idCloudFilename)
		NET_PRINT("]  [SharedRegID = ")
		NET_PRINT_INT(paramMissionIdData.idSharedRegID)
		NET_PRINT("]")
		IF (paramIsHost)
			NET_PRINT(" - HOST OF CALLING SCRIPT")
		ENDIF
		NET_NL()
	#ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission Control Events Broadcast By Mission
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Not Joinable
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionNotJoinable
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission is no longer joinable
//
// INPUT PARAMS:			paramMissionData			// The mission data
//
// NOTES:	This request is always intended for the Mission Control Host
PROC Broadcast_Mission_Not_Joinable(MP_MISSION_DATA paramMissionData)

	// Store the Broadcast data
	m_structEventMissionNotJoinable Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_NOT_JOINABLE
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	
	// Broadcast the event to a MISSION CONTROL HOST
	// NOTE: Send this to ALL_PLAYERS instead of only the script host so other players can store the details in case the event is lost due to Host Migration
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_MISSION_NOT_JOINABLE, paramMissionData.mdUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Broadcasting SCRIPT_EVENT_MISSION_NOT_JOINABLE") NET_NL()
		NET_PRINT("           ") NET_PRINT_TIME() NET_PRINT(" [To ALL_PLAYERS]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(paramMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Joinable Again For Players
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionJoinableAgainForPlayers
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission is joinable again for players (not for teams - this would still need code added)
//
// INPUT PARAMS:			paramMissionData			// The mission data
//
// NOTES:	This request is always intended for the Mission Control Host
PROC Broadcast_Mission_Joinable_Again_For_Players(MP_MISSION_DATA paramMissionData)

	// Store the Broadcast data
	m_structEventMissionJoinableAgainForPlayers Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_JOINABLE_AGAIN_FOR_PLAYERS
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	
	// Broadcast the event to a MISSION CONTROL HOST
	// NOTE: Send this to ALL_PLAYERS instead of only the script host so other players can store the details in case the event is lost due to Host Migration
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_MISSION_JOINABLE_AGAIN_FOR_PLAYERS, paramMissionData.mdUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Broadcasting SCRIPT_EVENT_MISSION_JOINABLE_AGAIN_FOR_PLAYERS") NET_NL()
		NET_PRINT("           ") NET_PRINT_TIME() NET_PRINT(" [To ALL_PLAYERS]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Ready For Secondary Teams
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionReadyForSecondaryTeams
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_DATA				missionData				// The mission data
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that a Mission is ready for the Joining Teams to join it
//
// INPUT PARAMS:			paramMissionData			// The mission data
//
// NOTES:	This request is always intended for the Mission Control Host
PROC Broadcast_Mission_Ready_For_Secondary_Teams(MP_MISSION_DATA paramMissionData)

	// Store the Broadcast data
	m_structEventMissionNotJoinable Event
	
	Event.Details.Type				= SCRIPT_EVENT_MISSION_READY_FOR_SECONDARY_TEAMS
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.missionData				= paramMissionData
	
	// Broadcast the event to a MISSION CONTROL HOST
	// NOTE: Send this to ALL_PLAYERS instead of only the script host so other players can store the details in case the event is lost due to Host Migration
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	
	// Store the details on the Resync Storage Array (to help recover should this event get lost when the Host migrates)
	Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_MISSION_READY_FOR_SECONDARY_TEAMS, paramMissionData.mdUniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Broadcasting SCRIPT_EVENT_MISSION_READY_FOR_SECONDARY_TEAMS") NET_NL()
		NET_PRINT("           ") NET_PRINT_TIME() NET_PRINT(" [To ALL_PLAYERS]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionData.mdID.idMission))
		NET_PRINT(" (Inst=") NET_PRINT_INT(paramMissionData.iInstanceId)
		NET_PRINT(") (Var=") NET_PRINT_INT(paramMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(paramMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Force Player From Mission Request
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventForcePlayerFromMissionRequest
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID for the mission request the player is being cleaned up from
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that this player should be forced off of a specific mission he is allocated for
//
// INPUT PARAMS:			paramUniqueID				// The UniqueID for the Mission Request the player is being forced off
//
// NOTES:	This request is always intended for the Mission Control Host
//			This was originally added to force a cleanup if the data goes out of sync after a host migration
PROC Broadcast_Force_Player_From_Mission_Request(INT paramUniqueID)

	// Store the Broadcast data
	m_structEventForcePlayerFromMissionRequest Event
	
	Event.Details.Type				= SCRIPT_EVENT_FORCE_PLAYER_FROM_MISSION_REQUEST
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Broadcast the event to a MISSION CONTROL HOST
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........(SERVER) Broadcasting SCRIPT_EVENT_FORCE_PLAYER_FROM_MISSION_REQUEST") NET_NL()
		NET_PRINT("           ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT("UniqueID = ") NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Force Player Quit PreMission
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventForcePlayerQuitPreMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID for the mission request the player is being cleaned up from
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that this player should cancel teh mission he is triggering if pre-mission
//
// INPUT PARAMS:			paramUniqueID				// The UniqueID for the Mission the player is being forced to quit pre-mission
//
// NOTES:	This was originally added to force a cleanup if the data goes out of sync after a mission controller host migration
PROC Broadcast_Force_Player_Quit_PreMission(INT paramUniqueID)

	// Store the Broadcast data
	m_structEventForcePlayerQuitPreMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_FORCE_PLAYER_QUIT_PREMISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Broadcast the event to a MISSION CONTROL HOST
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), SPECIFIC_PLAYER(PLAYER_ID()))
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........(SERVER) Broadcasting SCRIPT_EVENT_FORCE_PLAYER_QUIT_PREMISSION") NET_NL()
		NET_PRINT("           ") NET_PRINT_TIME() NET_PRINT(" [To Player: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_PRINT("]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT("UniqueID = ") NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Corona Matchmaking Closed
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionMatchmakingClosed
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID for the mission request for the mission instance being closed to Walk-Ins
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that this mission is closed to Walk-Ins (but still accepts invited players)
//
// INPUT PARAMS:			paramUniqueID				// The UniqueID for the Mission that is being closed to walk-ins
PROC Broadcast_Corona_Matchmaking_Closed(INT paramUniqueID)

	// Store the Broadcast data
	m_structEventMissionMatchmakingClosed Event
	
	Event.Details.Type				= SCRIPT_EVENT_CORONA_MATCHMAKING_CLOSED
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Broadcast the event to a MISSION CONTROL HOST
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Broadcasting SCRIPT_EVENT_CORONA_MATCHMAKING_CLOSED") NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT("UniqueID = ") NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Corona Matchmaking Open
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMissionMatchmakingOpen
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID for the mission request for the mission instance being opened to Walk-Ins
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast that this mission is open to Walk-Ins
//
// INPUT PARAMS:			paramUniqueID				// The UniqueID for the Mission that is being opened to walk-ins
PROC Broadcast_Corona_Matchmaking_Open(INT paramUniqueID)

	// Store the Broadcast data
	m_structEventMissionMatchmakingOpen Event
	
	Event.Details.Type				= SCRIPT_EVENT_CORONA_MATCHMAKING_OPEN
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Broadcast the event to a MISSION CONTROL HOST
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Broadcasting SCRIPT_EVENT_CORONA_MATCHMAKING_OPEN") NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT("UniqueID = ") NET_PRINT_INT(paramUniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC




// ===========================================================================================================
//      Mission Control Client - Public Access Functions
// ===========================================================================================================

// PURPOSE:	Check if this uniqueID is in the local 'Failed Mission Requests' list
//
// RETURN VALUE:		BOOL			TRUE if the Mission Request failed, otherwise FALSE
FUNC BOOL Has_MP_Mission_Request_Failed(INT paramUniqueID)

	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Has_MP_Mission_Request_Failed() - Checked with NO_UNIQUE_ID by: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			
			SCRIPT_ASSERT("Has_MP_Mission_Request_Failed() - called with NO_UNIQUE_ID")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		IF (g_sMCBroadcasts[tempLoop].MCB_Type = MCCBT_REQUEST_FAILED)
			IF (g_sMCBroadcasts[tempLoop].uniqueID = paramUniqueID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not Found
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this uniqueID is in the local 'Finished Mission Requests' list
//
// RETURN VALUE:		BOOL			TRUE if the Mission Request has Finished, otherwise FALSE
FUNC BOOL Has_MP_Mission_Request_Finished(INT paramUniqueID)

	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Has_MP_Mission_Request_Finished() - IGNORING: Checked with NO_UNIQUE_ID by: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			
			SCRIPT_ASSERT("Has_MP_Mission_Request_Finished() - called with NO_UNIQUE_ID")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		IF (g_sMCBroadcasts[tempLoop].MCB_Type = MCCBT_MISSION_FINISHED)
			IF (g_sMCBroadcasts[tempLoop].uniqueID = paramUniqueID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not Found
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this uniqueID is in the local 'Mission Full Mission Requests' list
//
// RETURN VALUE:		BOOL			TRUE if the Mission Request has Just Been Broadcast as Mission Full, otherwise FALSE
FUNC BOOL Has_MP_Mission_Request_Just_Broadcast_Mission_Full(INT paramUniqueID)

	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP: Has_MP_Mission_Request_Just_Broadcast_Mission_Full() - IGNORING: Checked with NO_UNIQUE_ID by: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			
			SCRIPT_ASSERT("Has_MP_Mission_Request_Just_Broadcast_Mission_Full() - called with NO_UNIQUE_ID")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	INT tempLoop = 0
	REPEAT g_numMCBroadcasts tempLoop
		IF (g_sMCBroadcasts[tempLoop].MCB_Type = MCCBT_MISSION_FULL)
			IF (g_sMCBroadcasts[tempLoop].uniqueID = paramUniqueID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not Found
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      Mission Control Info Event Structs and Broadcasts
//		(These are broadcasts to provide transitional information to other parts of the game if needed)
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Event: MRInfo Mission Just Started
//		(The Broadcast routine only - The Process routine is in net_process_events for general access)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventMRInfoMissionJustStarted
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The uniqueID of the mission just started
	INT							bitsReservedTeams		// Teams reserved for the mission
	INT							bitsJoinableTeams		// Teams joining the mission at launch
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast an Info Event that this Mission Request has just started
//
// INPUT PARAMS:		paramSlotID						// The Mission Request slot for the mission being joined
//
// NOTES:	This request is always intended for all players
PROC Broadcast_Mission_Request_Info_Mission_Just_Started(INT paramSlotID)

	// Store the Broadcast data
	m_structEventMRInfoMissionJustStarted Event
	
	Event.Details.Type				= SCRIPT_EVENT_MRINFO_MISSION_JUST_STARTED
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID
	Event.bitsReservedTeams			= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsReservedTeams
	Event.bitsJoinableTeams			= GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsJoinableTeams
	
	// Broadcast the event to all players
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Broadcasting SCRIPT_EVENT_MRINFO_MISSION_JUST_STARTED: to all players, issued by server (") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_PRINT(")") NET_NL()
		NET_PRINT("           [MissionID = ")
		NET_PRINT(GET_MP_MISSION_NAME(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idMission))
		NET_PRINT(" (Inst=")
		NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.iInstanceId)
		NET_PRINT(") (Var=")
		NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdID.idVariation)
		NET_PRINT(")]  [UniqueID = ") NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrMissionData.mdUniqueID)
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("           [BITFIELDS: (Reserved Teams = ")
		NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsReservedTeams)
		NET_PRINT(") (Launch Joinable Teams = ")
		NET_PRINT_INT(GlobalServerBD_MissionRequest.missionRequests[paramSlotID].mrBitsJoinableTeams)
		NET_PRINT(")]")
		NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Exclude Player from Mission - this is added so that the host can kick players and prevent walk-in
//		(The Broadcast routine only - The Process routine in in the mission control public header file)
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventExcludePlayerFromCurrentMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							uniqueID				// The unique Mission Request ID for the mission the player is being excluded from
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Broadcast Mission Request Exclude Player from the current mission
//
// INPUT PARAMS:		paramUniqueID			The Unique of the player's mission
//
// NOTES:	This request is always intended for the host
PROC Broadcast_Exclude_Player_From_Current_Mission(INT paramUniqueID)

	// Store the Broadcast data
	m_structEventExcludePlayerFromCurrentMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_EXCLUDE_PLAYER_FROM_CURRENT_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.uniqueID					= paramUniqueID
	
	// Error Checking
	IF (Event.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" Broadcast_Exclude_Player_From_Current_Mission() - IGNORE: Current Mission Unique ID is NO_UNIQUE_ID") NET_NL()
		#ENDIF
			
		EXIT
	ENDIF
	
	// Broadcast the event to ALL PLAYERS
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), Get_MC_Host_As_PlayerBit())
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" Broadcasting SCRIPT_EVENT_EXCLUDE_PLAYER_FROM_CURRENT_MISSION") NET_NL()
		NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [To MC Host: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		NET_PRINT("           [")
		NET_PRINT("UniqueID = ") NET_PRINT_INT(Event.uniqueID)
		NET_PRINT("]")
		NET_NL()
	#ENDIF
	
ENDPROC






