
USING "net_include.sch"
USING "Net_XP_Func.sch"


PROC RANKUP_REMOVE()
	IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_MP_RANK_BAR)
	
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("RANKUP_SET_RANK_SCORES INITIALISE called ")
		#ENDIF
		
		BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "REMOVE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC

PROC RANKUP_SET_COLOUR(HUD_COLOURS aMainColour, HUD_COLOURS aDarkColour)
	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "SET_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(aMainColour))
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(aDarkColour))
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC RANKUP_OVERRIDE_ANIMATION_SPEED(INT AnimSpeedms)
	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "OVERRIDE_ANIMATION_SPEED")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(AnimSpeedms)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC RANKUP_OVERRIDE_ONSCREEN_DURATION(INT Durationms)
	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "OVERRIDE_ONSCREEN_DURATION")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(Durationms)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC RANKUP_SET_BAR_TEXT(STRING BarTextStr, INT AlphaValue)
	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "SET_BAR_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(BarTextStr)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(AlphaValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC RANKUP_RESET_BAR_TEXT()
	IF HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_MP_RANK_BAR)
		BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "RESET_BAR_TEXT")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC RANKUP_FADE_BAR_OUT(BOOL bInstantly = FALSE)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT(" RANKUP_FADE_BAR_OUT ")
	#ENDIF
	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "FADE_BAR_OUT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bInstantly)
	END_SCALEFORM_MOVIE_METHOD()
	
	
	
ENDPROC

PROC RANKUP_SET_RANK_SCORES(INT CurrentRankLimit, INT NextRankLimit, INT PlayersPreviousXP, INT playersCurrentXP, INT rankCurrent, INT alphaValue)
	BEGIN_SCALEFORM_SCRIPT_HUD_MOVIE_METHOD(HUD_MP_RANK_BAR, "SET_RANK_SCORES")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(CurrentRankLimit)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(NextRankLimit)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(PlayersPreviousXP)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(playersCurrentXP)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(rankCurrent)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(alphaValue)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


FUNC BOOL IS_SCALEFORMXML_RANKBAR_RUNNING()
	RETURN HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_MP_RANK_BAR)
ENDFUNC

PROC NEXT_RP_ADDITION_SHOW_RANKBAR()
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[RP][RANKBAR] <<NEXT_RP_ADDITION_SHOW_RANKBAR>> ")
	#ENDIF
	g_b_NextRPAdditionShowRankBar = TRUE
ENDPROC

FUNC BOOL RUN_SCALEFORMXML_RANKBAR(BOOL bStayOnNoMovement, INT StartLevel, INT TargetLevel, INT HowManyRankChanges, INT iTeam, INT TimeDisplay, BOOL wrappedRoundXPChange )
		
		
		IF IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)
		OR  IS_SCRIPTED_HUD_COMPONENT_HIDDEN_THIS_FRAME(HUD_MP_RANK_BAR) 
			IF IS_SCRIPTED_HUD_COMPONENT_HIDDEN_THIS_FRAME(HUD_MP_RANK_BAR) 
				NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES IS_SCRIPTED_HUD_COMPONENT_HIDDEN_THIS_FRAME(HUD_MP_RANK_BAR)  ")
			ENDIF
			IF IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)
				NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES IS_SCRIPT_HUD_DISABLED(HUDPART_RANKBAR)  ")
			ENDIF
			RETURN TRUE
		ENDIF
		
		INT PlayersStartRank = GET_RANK_FROM_XP_VALUE(StartLevel)
		
		INT PlayersCurrent_Rank_Limit_XP = GET_XP_NEEDED_FOR_RANK(PlayersStartRank, FALSE)
		INT PlayersNext_Rank_Limit_XP = GET_XP_NEEDED_FOR_RANK(PlayersStartRank+1, FALSE)
		INT PlayersCurrent_XP = StartLevel
		INT PlayersNewXP 
		INT RankLowerNumber
		INT RankHigherNumber
		
		
		INT currentRankLimit = PlayersCurrent_Rank_Limit_XP
		INT nextRankLimit = PlayersNext_Rank_Limit_XP
		INT playersCurrentXP = PlayersCurrent_XP

		INT difBetweenRanks = nextRankLimit - currentRankLimit
		INT howFarIntoThisRankNow = playersCurrentXP - currentRankLimit
		INT newXPPercentage = FLOOR((TO_FLOAT(howFarIntoThisRankNow) / TO_FLOAT(difBetweenRanks)) * 100)
		
		

		
		#IF IS_DEBUG_BUILD
		
		INT howFarIntoThisRankNew = PlayersNewXP - currentRankLimit
		INT newXPPercentageNew = FLOOR((TO_FLOAT(howFarIntoThisRankNew) / TO_FLOAT(difBetweenRanks)) * 100)
		
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES difBetweenRanks = ")NET_PRINT_INT(difBetweenRanks)
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES howFarIntoThisRankNow = ")NET_PRINT_INT(howFarIntoThisRankNow)
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES newXPPercentage = ")NET_PRINT_INT(newXPPercentage)
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES howFarIntoThisRankNew = ")NET_PRINT_INT(howFarIntoThisRankNew)
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES UNUSED newXPPercentageNew = ")NET_PRINT_INT(newXPPercentageNew)
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES UNUSED newXPPercentageNew%newXPPercentage = ")NET_PRINT_INT(newXPPercentageNew%newXPPercentage)
		NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES g_sMPTunables.iRankbarMinimumSegment = ")NET_PRINT_INT(g_sMPTunables.iRankbarMinimumSegment)

		#ENDIF
//		IF 
//		
//		ENDIF
		
		INT HiddenPercentageLevel = g_sMPTunables.iRankbarMinimumSegment
		IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(PLAYER_ID())
			NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES On a fm event, change percentage check to 0 ")

			HiddenPercentageLevel = 0
		ENDIF
		
//		IF newXPPercentageNew%newXPPercentage >= 10
		IF newXPPercentage < HiddenPercentageLevel
		AND bStayOnNoMovement = FALSE
		AND wrappedRoundXPChange = FALSE
		AND g_b_NextRPAdditionShowRankBar = FALSE
			//show rank bar
			NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES Not rolling into a segment - Exitting.  ")
			IF IS_SCALEFORMXML_RANKBAR_RUNNING()
				NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES Not rolling into a segment - Exitting. Rank bar is running so calling REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_MP_RANK_BAR)  ")
				REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_MP_RANK_BAR)
			ELSE
				NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES Not rolling into a segment - Exitting. Rank bar is NOT running but calling REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_MP_RANK_BAR) anyway ")
				REMOVE_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_MP_RANK_BAR)
			ENDIF
			g_b_NextRPAdditionShowRankBar = FALSE
			RETURN TRUE
		ENDIF
		
		
		
			
		IF NOT IS_SCALEFORMXML_RANKBAR_RUNNING()
			NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES: HAS_SCALEFORM_SCRIPT_HUD_MOVIE_LOADED(HUD_MP_RANK_BAR) = FALSE ")
			REQUEST_SCALEFORM_SCRIPT_HUD_MOVIE(HUD_MP_RANK_BAR)	
		ELSE
			
			g_b_NextRPAdditionShowRankBar = FALSE
			
			INT AlphaValue = 100
//			IF NETWORK_IS_ACTIVITY_SESSION()
//				AlphaValue = 50
//			ENDIF

			IF bStayOnNoMovement
			
				SWITCH iTeam
					CASE TEAM_FREEMODE
						RANKUP_SET_COLOUR(HUD_COLOUR_FREEMODE,HUD_COLOUR_FREEMODE_DARK)
					BREAK
				ENDSWITCH
				
				IF GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE
					NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES GET_DPADDOWN_ACTIVATION_STATE() = DPADDOWN_NONE - Cleanup ")
				
					RANKUP_FADE_BAR_OUT(TRUE)
					RETURN TRUE
				ENDIF
				
				RANKUP_OVERRIDE_ONSCREEN_DURATION(TimeDisplay)
//				RANKUP_SET_RANK_SCORES(PlayersCurrent_XP , PlayersCurrent_XP,PlayersCurrent_Rank_Limit_XP, PlayersCurrent_Rank_Limit_XP, PlayersStartRank)

				NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES: JUST DISPLAY ")

				RANKUP_SET_RANK_SCORES(PlayersCurrent_Rank_Limit_XP, PlayersNext_Rank_Limit_XP, PlayersCurrent_XP , PlayersCurrent_XP, PlayersStartRank, AlphaValue)
					
	

			ELSE
		

				NET_NL()NET_PRINT("[RP][RANKBAR]  RANKUP_SET_RANK_SCORES INITIALISE called ")
				NET_NL()NET_PRINT("[RP][RANKBAR]   -HowManyRankChanges ")NET_PRINT_INT(HowManyRankChanges)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -PlayersStartRank ")NET_PRINT_INT(PlayersStartRank)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -StartLevel ")NET_PRINT_INT(StartLevel)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -TargetLevel ")NET_PRINT_INT(TargetLevel)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -PlayersCurrent_Rank_Limit_XP ")NET_PRINT_INT(PlayersCurrent_Rank_Limit_XP)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -PlayersNext_Rank_Limit_XP ")NET_PRINT_INT(PlayersNext_Rank_Limit_XP)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -PlayersCurrent_XP ")NET_PRINT_INT(PlayersCurrent_XP)
				NET_NL()NET_PRINT("[RP][RANKBAR]   -PlayersNewXP ")NET_PRINT_INT(PlayersNewXP)
				
				
				SWITCH iTeam
					CASE TEAM_FREEMODE
						RANKUP_SET_COLOUR(HUD_COLOUR_FREEMODE,HUD_COLOUR_FREEMODE_DARK)
					BREAK
				ENDSWITCH
				RANKUP_OVERRIDE_ANIMATION_SPEED(2000)
				RANKUP_OVERRIDE_ONSCREEN_DURATION(TimeDisplay)
				
				INT I
				FOR I = 0 TO HowManyRankChanges
				
					IF HowManyRankChanges > 0

						IF TargetLevel < StartLevel
							RankHigherNumber = ((PlayersStartRank-(I))+1)
							RankLowerNumber = (PlayersStartRank-(I))
						ELSE
							RankHigherNumber = ((PlayersStartRank+(I))+1)
							RankLowerNumber = (PlayersStartRank+(I))
						ENDIF
					ELSE
						RankHigherNumber = ((PlayersStartRank)+1)
						RankLowerNumber = (PlayersStartRank)
					ENDIF
					
					
					PlayersCurrent_Rank_Limit_XP = GET_XP_NEEDED_FOR_RANK(RankLowerNumber)		
					PlayersNext_Rank_Limit_XP = GET_XP_NEEDED_FOR_RANK(RankHigherNumber)
					
					IF I = 0 //First
					AND HowManyRankChanges > 0
						NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES IN LOOP called - First ")
						PlayersCurrent_XP = StartLevel
						IF TargetLevel < StartLevel
							PlayersNewXP = PlayersCurrent_Rank_Limit_XP
						ELSE
							PlayersNewXP = PlayersNext_Rank_Limit_XP
						ENDIF
						//grankbarchangedlevelactivatebigmessage = TRUE
						irankbarchangedlevelactivatebigmessagetimer = 0
						NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES grankbarchangedlevelactivatebigmessage = TRUE ")
					
					ELIF HowManyRankChanges = 0 
						NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES IN LOOP called - Stay in Level ")
						PlayersCurrent_XP = StartLevel
						PlayersNewXP = TargetLevel

					ELIF I = HowManyRankChanges //Last 
						NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES IN LOOP called - Last ")
						IF TargetLevel < StartLevel
							PlayersCurrent_XP = PlayersNext_Rank_Limit_XP
							PlayersNewXP = TargetLevel
						ELSE
							PlayersCurrent_XP = PlayersCurrent_Rank_Limit_XP
							PlayersNewXP = TargetLevel
						ENDIF

					ELSE //Changes inbetween
						NET_NL()NET_PRINT("[RANKBAR] RANKUP_SET_RANK_SCORES IN LOOP called - inbetween ")
						IF TargetLevel < StartLevel
							PlayersCurrent_XP = PlayersNext_Rank_Limit_XP
							PlayersNewXP = PlayersCurrent_Rank_Limit_XP
						ELSE
							PlayersCurrent_XP = PlayersCurrent_Rank_Limit_XP
							PlayersNewXP = PlayersNext_Rank_Limit_XP
						ENDIF
					ENDIF
						
					

					NET_NL()NET_PRINT("[RANKBAR] 		 -I ")NET_PRINT_INT(I)
					NET_NL()NET_PRINT("[RANKBAR] 		 -StartLevel ")NET_PRINT_INT(StartLevel)
					NET_NL()NET_PRINT("[RANKBAR] 		 -TargetLevel ")NET_PRINT_INT(TargetLevel)
					NET_NL()NET_PRINT("[RANKBAR] 		 -RankHigherNumber ")NET_PRINT_INT(RankHigherNumber)
					NET_NL()NET_PRINT("[RANKBAR] 		 -RankLowerNumber ")NET_PRINT_INT(RankLowerNumber)
					NET_NL()NET_PRINT("[RANKBAR] 		 -PlayersCurrent_Rank_Limit_XP ")NET_PRINT_INT(PlayersCurrent_Rank_Limit_XP)
					NET_NL()NET_PRINT("[RANKBAR] 		 -PlayersNext_Rank_Limit_XP ")NET_PRINT_INT(PlayersNext_Rank_Limit_XP)
					NET_NL()NET_PRINT("[RANKBAR] 		 -PlayersCurrent_XP ")NET_PRINT_INT(PlayersCurrent_XP)
					NET_NL()NET_PRINT("[RANKBAR] 		 -PlayersNewXP ")NET_PRINT_INT(PlayersNewXP)


//					RANKUP_SET_RANK_SCORES(PlayersCurrent_Rank_Limit_XP, PlayersNext_Rank_Limit_XP, PlayersCurrent_XP , PlayersNewXP, RankLowerNumber)

					RANKUP_SET_RANK_SCORES(PlayersCurrent_Rank_Limit_XP, PlayersNext_Rank_Limit_XP, PlayersCurrent_XP ,PlayersNewXP , RankLowerNumber, AlphaValue)


			ENDFOR
				

				
				
			ENDIF	

			RETURN TRUE

		ENDIF
		
		RETURN FALSE
		
ENDFUNC






