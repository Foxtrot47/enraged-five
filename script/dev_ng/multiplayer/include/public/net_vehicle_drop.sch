//////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_VEHICLE_DROP.sch													//
// Description: Player calls for an Vehicle drop to their current location.				//
// Written by:  Ryan Baker																//
// Date: 08/01/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"
//USING "commands_path.sch"
//USING "building_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"
USING "net_taxi.sch"
USING "net_ambience.sch"
USING "net_contact_requests.sch"
#IF FEATURE_HEIST_ISLAND
USING "mp_submarine_coords.sch"
#ENDIF

CONST_INT VEHICLE_DROP_MAX_VEHICLE_MODELS 10

CONST_INT VEHICLE_DROP_PLANE	0
CONST_INT VEHICLE_DROP_DRIVE	1

CONST_FLOAT VD_LEAVE_RANGE						250.0
CONST_FLOAT VD_VEHICLE_MIN_SPAWN_RANGE			25.0		//50.0
CONST_FLOAT VD_VEHICLE_MAX_SPAWN_RANGE			100.0		//200.0
CONST_FLOAT VD_VEHICLE_SPEED					20.0
CONST_FLOAT VD_VEHICLE_REACHED_PLAYER_RANGE		10.0
CONST_FLOAT VD_VEHICLE_BLIP_RANGE				100.0
CONST_FLOAT VD_DIALOGUE_RANGE					50.0
CONST_FLOAT VD_STOP_AT_PLAYER_RANGE				7.5
//CONST_FLOAT VD_PERSONAL_RANGE					200.0	//NOW IN mp_globals_common.sch
CONST_INT VD_END_CALL_DELAY						30000
CONST_INT VD_DELIVERY_DELAY						5000

CONST_FLOAT VD_MIN_NODE_RANGE					50.0
CONST_FLOAT VD_CLEAR_AREA_RANGE					3.0

//CONST_INT VD_STANDARD_PRICE						2500
//CONST_INT VD_PERSONAL_PRICE						500
//CONST_INT VD_IMPOUND_PRICE                      450

CONST_INT VD_MAX_NUM_PERSONAL_VEHICLES			10

//VEHICLE DROP PERSONAL VEHICLE VARIATION DATA
CONST_INT VDP_SETUP			0
CONST_INT VDP_CREATION		1
CONST_INT VDP_POST_CREATION	2
CONST_INT VDP_DELIVERY		3
CONST_INT VDP_END_CALL		4
CONST_INT VDP_CLEANUP		5

STRUCT VEHICLE_DROP_PERSONAL_DATA
	INT iVehicleDropPersonalStage
	NETWORK_INDEX niVehicle
	NETWORK_INDEX niDriver
	INT iBitSet
	INT iVehicleSlot
	VECTOR vDropLocation
	structPedsForConversation speechStruct
	INT iNode
	SCRIPT_TIMER iEndCallTimer
	SCRIPT_TIMER iDeliveryDelayTimer
	structPedsForConversation sSpeech
	VECTOR vCreatePos
	FLOAT fCreateHeading
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT VEHICLE_DROP_PERSONAL_DEBUG_DATA
	INT iDebugVehicleGrab
	
	BOOL bBuyHauler2, bBuyPhantom3
	TEXT_WIDGET_ID twTruckModelName
	
	VECTOR vVehicleCoord
	FLOAT fVehicleHeading
	INT iTruckColours[4]
	BOOL bVehicleExtras[8]
	
	VECTOR vTrailerOffset
	FLOAT fTrailerHeadingOffset
	INT iTrailerColours[4]
	BOOL bTrailerExtras[8]
//	INT iTrailerMods[MAX_VEHICLE_MOD_SLOTS]
	
	BOOL bDrawDebug, bMoveTruck, bMoveTrailer, bAttachTrailer, bDetachTrailer
	INT iTrailerDoor
	BOOL bOpenTrailerDoor, bCloseTrailerDoor
	FLOAT fAttachTrailer, fOverride_extenable_side_ratio = -1.0, fExtenable_side_target_ratio = -1.0
	BOOL bOverride_side_ratio
ENDSTRUCT
#ENDIF

CONST_INT VDP_DialogueDone		0
CONST_INT VDP_PathsLoaded		1
CONST_INT VDP_PhonecallReceived	2
CONST_INT VDP_PhonecallDone		3
CONST_INT VDP_EndPhonecallSetup	4
CONST_INT VDP_ClearAreaDone		5
CONST_INT VDP_StopAtPlayerDone	6
CONST_INT VDP_VehAtCreationPos	7
CONST_INT VDP_PlayerDamagedVehicle	8 // B*1299605
CONST_INT VDP_CreationCoordsFound	9
CONST_INT VDP_CustomCarNodesAdded	10


FUNC MODEL_NAMES VD_GET_PERSONAL_VEHICLE_MODEL(INT iIndex)
	IF (iIndex > -1)
    	RETURN g_MpSavedVehicles[iIndex].VehicleSetupMP.VehicleSetup.eModel
	ENDIF
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Used to decide if call to vehicle drop should be successful response
///    or position is ok to spawn a taxi
/// PARAMS:
///    vPosition - position to test
/// RETURNS:
///    TRUE if position is in a taxi restricted area 
FUNC BOOL IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA(VECTOR vPosition, BOOL bBypassChecksForPA = FALSE)
	
	IF WILL_POINT_USE_CUSTOM_VEHICLE_NODES(vPosition)
	OR bBypassChecksForPA
		NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return FALSE : WILL_POINT_USE_CUSTOM_VEHICLE_NODES")  NET_NL()
		RETURN FALSE
	ENDIF
	
	// only use this if it covers the green - still want to call cabs from the carpark
	//IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_GOLF_COURSE, vPosition)	// too big a radius check
	IF VDIST2(vPosition, << -1144.49731, 43.01712, 51.94447 >>) <= (325.0*325.0)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_GOLF_COURSE)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_GOLF_COURSE check")  NET_NL()
			RETURN TRUE
		ENDIF	
		// ac golf checks are too loose so need extra area checks unfortunately
		// Goes from here along the clubhouse heading clockwise
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1319.768555,29.526155,49.566158>>, <<-1336.526611,121.035141,75.618881>>, 18.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 1")  NET_NL()
			RETURN TRUE
		ENDIF
		// Caddy carpark - leading to main carpark
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1360.019653,110.130661,52.634132>>, <<-1365.276001,172.062439,72.013123>>, 52.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 2")  NET_NL()
			RETURN TRUE
		ENDIF
		// North East corner
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1363.006592,170.533035,50.008129>>, <<-1296.903320,178.813324,73.371040>>, 35.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 3")  NET_NL()
			RETURN TRUE
		ENDIF
		// Along North wall
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1197.521240,199.864288,57.044708>>, <<-1298.039551,176.138412,73.332520>>, 34.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 4")  NET_NL()
			RETURN TRUE
		ENDIF
		// Along North East corner
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1103.744873,221.136673,55.348141>>, <<-1208.393799,191.090927,79.137085>>, 45.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 5")  NET_NL()
			RETURN TRUE
		ENDIF
		// East border
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-989.032837,-89.393761,32.488010>>, <<-1086.210938,-115.707649,50.650505>>, 70.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 6")  NET_NL()
			RETURN TRUE
		ENDIF
		// South border
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-958.548096,-111.545547,30.764324>>, <<-1132.685303,190.784119,73.903656>>, 70.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 7")  NET_NL()
			RETURN TRUE
		ENDIF
		// south boarder heading from southern most point up towards the club house
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1077.028198,-139.733704,38.733883>>, <<-1299.555908,-15.168939,63.437103>>, 20.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 8")  NET_NL()
			RETURN TRUE
		ENDIF
		// south boarder West corner covering on foot entrance to green
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1319.768555,29.526155,47.566158>>, <<-1287.619263,-24.888639,67.527298>>, 20.000000)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : golf course extra check 9")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_AIRPORT_AIRSIDE, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_AIRPORT_AIRSIDE)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_AIRPORT_AIRSIDE check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MILITARY_BASE, vPosition)
	AND NOT DOES_PLAYER_OWN_AN_AB_HANGAR(PLAYER_ID())
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_MILITARY_BASE)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_MILITARY_BASE check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_PRISON, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_PRISON)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_PRISON check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_BIOTECH, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_BIOTECH)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_BIOTECH check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MILITARY_DOCKS, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_MILITARY_DOCKS)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_MILITARY_DOCKS check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_MOVIE_STUDIO, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_MOVIE_STUDIO)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_MOVIE_STUDIO check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_AREACHECK_IN_ACTIVE_RANGE(AC_DOWNTOWN_POLICE, vPosition)
		IF IS_COORD_IN_SPECIFIED_AREA(vPosition, AC_DOWNTOWN_POLICE)
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : AC_DOWNTOWN_POLICE check")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	// road leading up to gov facility
	// B*1395206 - 
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<2591.626465,-268.893951,111.885857>>, <<2591.331055,-614.435547,55.369205>>, 70.000000)
		NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : gov facility road check")  NET_NL()
		RETURN TRUE
	ENDIF
	
	// B*1392187 - mountainous area leading to base jump in Raton Canyon
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-804.050049,4216.411621,204.487198>>, <<-509.067871,4135.190430,123.250168>>, 250.000000)
		NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : mountainous area Raton Canyon base jump")  NET_NL()
		RETURN TRUE
	ENDIF
	
	// B*1533798 - West side Mount Chiliad
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-148.877655,4862.203613,305.644196>>, <<454.627441,5573.104004,804.096985>>, 250.000000)	// majority West from top of cable cart building down
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-482.893066,4990.255371,155.160110>>, <<7.830751,5009.370605,430.760376>>, 250.000000)		// minor West following down from previous check to lower ground
		NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : Mount Chiliad West")  NET_NL()
		RETURN TRUE
	ENDIF
	
	// B*1533798 - East side Mount Chiliad
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<441.399902,5579.990234,802.513794>>, <<965.777588,5675.920898,601.264648>>, 250.000000)		// East from top of cable cart building down
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<954.113892,5641.050781,646.505432>>, <<2140.375000,5377.753418,149.122086>>, 250.000000)	// majority East following down from previous check to lower ground
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<2117.347656,5377.259277,173.329727>>, <<2439.933838,5297.444824,62.686623>>, 100.000000)	// minor East following down from previous check to lower ground	
	OR IS_POINT_IN_ANGLED_AREA(vPosition, <<2393.347412,5321.579590,107.062424>>, <<2523.945557,5124.745605,41.683842>>, 70.000000)		// minor East following down from previous check to base		
		NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : Mount Chiliad East")  NET_NL()
		RETURN TRUE
	ENDIF
	
	//B*1485093 - Port of LS heist Setup area - restricted by gates
	IF VDIST2(vPosition, << -99.68751, -2448.89111, 5.01731 >>) <= (230.0 * 230.0)	// ensure checks only happen if player is within range of the area
		IF IS_POINT_IN_ANGLED_AREA(vPosition, <<85.054482,-2511.883789,-2.996267>>, <<-57.599766,-2412.716309,15.000947>>, 75.000000)	// East section heading down the side of the gates		
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<7.516524,-2546.740967,1.331557>>, <<-177.326843,-2417.046631,19.160444>>, 80.000000)	// Central area heading from South East gates into area
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-260.035370,-2419.978271,1.399635>>, <<-27.263750,-2423.848389,25.000641>>, 80.000000)	// Waters edge
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-187.425598,-2516.085693,-6.849975>>, <<-186.751801,-2438.148682,25.001602>>, 40.00000)	// South gates
		OR IS_POINT_IN_ANGLED_AREA(vPosition, <<-73.608131,-2538.563721,-6.989857>>, <<-183.255585,-2465.145020,25.020298>>, 70.000000)	// South area
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : Port of LS heist Setup area")  NET_NL()
			RETURN TRUE
		ENDIF
	ENDIF
	
	//B*1622780 - Entrance to army base
	IF IS_POINT_IN_ANGLED_AREA(vPosition, <<-1579.821777,2785.389160,9.915375>>, <<-1685.904663,2918.024170,76.249123>>, 45.875000)
		NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA return TRUE : Entrance to army base")  NET_NL()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the specified position is in a zone classed as safe for the taxi to use deadend nodes in it's path finding
///    basically anything in urban areas aren't safe because NF_IGNORE_SWITCHED_OFF_DEADENDS ae used for driveways
/// PARAMS:
///    vPos - specific position
/// RETURNS:
///    TRUE if the position is safe to allow NF_IGNORE_SWITCHED_OFF_DEADENDS
FUNC BOOL IS_POSITION_IN_ZONE_SAFE_FOR_DEADEND_NODES_VD(VECTOR vPos)
	
	STRING tempZone
	tempZone = GET_NAME_OF_ZONE(vPos)
	
	IF ARE_STRINGS_EQUAL("SanAnd", tempZone) // San Andreas
	OR ARE_STRINGS_EQUAL("Alamo", tempZone) // Alamo Sea
	
	//OR ARE_STRINGS_EQUAL("Alta", tempZone) // Alta
	//OR ARE_STRINGS_EQUAL("Airp", tempZone) // Los Santos International Airport
	
	OR ARE_STRINGS_EQUAL("ArmyB", tempZone) // Fort Zancudo
	OR ARE_STRINGS_EQUAL("BhamCa", tempZone) // Banham Canyon
	
	//OR ARE_STRINGS_EQUAL("Banning", tempZone) // Banning
	
	OR ARE_STRINGS_EQUAL("Baytre", tempZone) // Baytree Canyon
	
	//OR ARE_STRINGS_EQUAL("Beach", tempZone) // Vespucci Beach
	
	OR ARE_STRINGS_EQUAL("BradT", tempZone) // Braddock Tunnel
	
	OR ARE_STRINGS_EQUAL("BradP", tempZone) // Braddock Pass

	//OR ARE_STRINGS_EQUAL("Burton", tempZone) // Burton

	OR ARE_STRINGS_EQUAL("CANNY", tempZone) // Raton Canyon
	OR ARE_STRINGS_EQUAL("CCreak", tempZone) // Cassidy Creek

	//OR ARE_STRINGS_EQUAL("CalafB", tempZone) // Calafia Bridge
	
	OR ARE_STRINGS_EQUAL("ChamH", tempZone) // Chamberlain Hills
	OR ARE_STRINGS_EQUAL("CHU", tempZone) // Chumash
	
	//OR ARE_STRINGS_EQUAL("CHIL", tempZone) // Vinewood Hills

	OR ARE_STRINGS_EQUAL("COSI", tempZone) // Countryside

	OR ARE_STRINGS_EQUAL("CMSW", tempZone) // Chiliad Mountain State Wilderness
	OR ARE_STRINGS_EQUAL("Cypre", tempZone) // Cypress Flats
	
	//OR ARE_STRINGS_EQUAL("Davis", tempZone) // Davis

	OR ARE_STRINGS_EQUAL("Desrt", tempZone) // Grand Senora Desert

	//OR ARE_STRINGS_EQUAL("DelBe", tempZone) // Del Perro Beach
	//OR ARE_STRINGS_EQUAL("DelPe", tempZone) // Del Perro
	//OR ARE_STRINGS_EQUAL("DelSol", tempZone) // La Puerta
	//OR ARE_STRINGS_EQUAL("Downt", tempZone) // Downtown
	//OR ARE_STRINGS_EQUAL("DTVine", tempZone) // Downtown Vinewood
	//OR ARE_STRINGS_EQUAL("Eclips", tempZone) // Eclipse
	//OR ARE_STRINGS_EQUAL("ELSant", tempZone) // East Los Santos
	//OR ARE_STRINGS_EQUAL("EBuro", tempZone) // El Burro Heights
	
	OR ARE_STRINGS_EQUAL("ELGorl", tempZone) // El Gordo Lighthouse

	//OR ARE_STRINGS_EQUAL("Elysian", tempZone) // Elysian Island

	OR ARE_STRINGS_EQUAL("Galli", tempZone) // Galileo Park
	OR ARE_STRINGS_EQUAL("Galfish", tempZone) // Galilee

	//OR ARE_STRINGS_EQUAL("Greatc", tempZone) // Great Chaparral
	//OR ARE_STRINGS_EQUAL("Golf", tempZone) // GWC and Golfing Society
	//OR ARE_STRINGS_EQUAL("GrapeS", tempZone) // Grapeseed
	//OR ARE_STRINGS_EQUAL("Hawick", tempZone) // Hawick

	OR ARE_STRINGS_EQUAL("Harmo", tempZone) // Harmony

	//OR ARE_STRINGS_EQUAL("Heart", tempZone) // Heart Attacks Beach

	OR ARE_STRINGS_EQUAL("HumLab", tempZone) // Humane Labs and Research

	//OR ARE_STRINGS_EQUAL("HORS", tempZone) // Vinewood Racetrack
	//OR ARE_STRINGS_EQUAL("Koreat", tempZone) // Little Seoul

	OR ARE_STRINGS_EQUAL("Jail", tempZone) // Bolingbroke Penitentiary
	OR ARE_STRINGS_EQUAL("LAct", tempZone) // Land Act Reservoir
	OR ARE_STRINGS_EQUAL("LDam", tempZone) // Land Act Dam
	OR ARE_STRINGS_EQUAL("Lago", tempZone) // Lago Zancudo

	//OR ARE_STRINGS_EQUAL("LegSqu", tempZone) // Legion Square	
	//OR ARE_STRINGS_EQUAL("LosSF", tempZone) // Los Santos Freeway
	//OR ARE_STRINGS_EQUAL("LMesa", tempZone) // La Mesa
	//OR ARE_STRINGS_EQUAL("LosPuer", tempZone) // La Puerta 
	//OR ARE_STRINGS_EQUAL("LosPFy", tempZone) // La Puerta Fwy
	//OR ARE_STRINGS_EQUAL("LOSTMC", tempZone) // Lost MC
	//OR ARE_STRINGS_EQUAL("Mirr", tempZone) // Mirror Park
	//OR ARE_STRINGS_EQUAL("Morn", tempZone) // Morningwood
	//OR ARE_STRINGS_EQUAL("Murri", tempZone) // Murrieta Heights

	OR ARE_STRINGS_EQUAL("MTChil", tempZone) // Mount Chiliad
	OR ARE_STRINGS_EQUAL("MTJose", tempZone) // Mount Josiah
	OR ARE_STRINGS_EQUAL("MTGordo", tempZone) // Mount Gordo	- B*1334556

	//OR ARE_STRINGS_EQUAL("Movie", tempZone) // Richards Majestic

	OR ARE_STRINGS_EQUAL("NCHU", tempZone) // North Chumash

	//OR ARE_STRINGS_EQUAL("Noose", tempZone) // N.O.O.S.E

	OR ARE_STRINGS_EQUAL("Oceana", tempZone) // Pacific Ocean

	//OR ARE_STRINGS_EQUAL("Observ", tempZone) // Galileo Observatory

	OR ARE_STRINGS_EQUAL("Palmpow", tempZone) // Palmer-Taylor Power Station

	//OR ARE_STRINGS_EQUAL("PBOX", tempZone) // Pillbox Hill

	OR ARE_STRINGS_EQUAL("PBluff", tempZone) // Pacific Bluffs
	OR ARE_STRINGS_EQUAL("Paleto", tempZone) // Paleto Bay	
	OR ARE_STRINGS_EQUAL("PalCov", tempZone) // Paleto Cove	- B*1332906
	OR ARE_STRINGS_EQUAL("PalFor", tempZone) // Paleto Forest
	OR ARE_STRINGS_EQUAL("PalHigh", tempZone) // Palomino Highlands

	//OR ARE_STRINGS_EQUAL("ProcoB", tempZone) // Procopio Beach
	//OR ARE_STRINGS_EQUAL("Prol", tempZone) // North Yankton

	OR ARE_STRINGS_EQUAL("RTRAK", tempZone) // Redwood Lights Track
	OR ARE_STRINGS_EQUAL("Rancho", tempZone) // Rancho

	//OR ARE_STRINGS_EQUAL("RGLEN", tempZone) // Richman Glen
	//OR ARE_STRINGS_EQUAL("Richm", tempZone) // Richman
	//OR ARE_STRINGS_EQUAL("Rockf", tempZone) // Rockford Hills

	OR ARE_STRINGS_EQUAL("SANDY", tempZone) // Sandy Shores
	OR ARE_STRINGS_EQUAL("TongvaH", tempZone) // Tongva Hills
	OR ARE_STRINGS_EQUAL("TongvaV", tempZone) // Tongva Valley
	//OR ARE_STRINGS_EQUAL("East_V", tempZone) // East Vinewood

	OR ARE_STRINGS_EQUAL("Zenora", tempZone) // Senora Freeway
	OR ARE_STRINGS_EQUAL("Slab", tempZone) // Stab City

	//OR ARE_STRINGS_EQUAL("SKID", tempZone) // Mission Row
	//OR ARE_STRINGS_EQUAL("SLSant", tempZone) // South Los Santos
	//OR ARE_STRINGS_EQUAL("Stad", tempZone) // Maze Bank Arena
	//OR ARE_STRINGS_EQUAL("Tatamo", tempZone) // Tataviam Mountains
	//OR ARE_STRINGS_EQUAL("Termina", tempZone) // Terminal
	//OR ARE_STRINGS_EQUAL("TEXTI", tempZone) // Textile City
	//OR ARE_STRINGS_EQUAL("WVine", tempZone) // West Vinewood
	//OR ARE_STRINGS_EQUAL("UtopiaG", tempZone) // Utopia Gardens
	//OR ARE_STRINGS_EQUAL("Vesp", tempZone) // Vespucci
	//OR ARE_STRINGS_EQUAL("VCana", tempZone) // Vespucci Canals
	//OR ARE_STRINGS_EQUAL("Vine", tempZone) // Vinewood
	//OR ARE_STRINGS_EQUAL("WMirror", tempZone) // W Mirror Drive

	OR ARE_STRINGS_EQUAL("WindF", tempZone) // Ron Alternates Wind Farm
	OR ARE_STRINGS_EQUAL("Zancudo", tempZone) // Zancudo River
	OR ARE_STRINGS_EQUAL("SanChia", tempZone) // San Chianski Mountain Range

	//OR ARE_STRINGS_EQUAL("STRAW", tempZone) // Strawberry
	
	OR ARE_STRINGS_EQUAL("zQ_UAR", tempZone) // Davis Quartz

	//OR ARE_STRINGS_EQUAL("ZP_ORT", tempZone) // Port of South Los Santos

		NET_PRINT("     ---------->     VEHICLE DROP -  : IS_POSITION_IN_ZONE_SAFE_FOR_DEADEND_NODES return TRUE for vPos = ") NET_PRINT_VECTOR(vPos) NET_PRINT(" zone = ") NET_PRINT(tempZone) NET_NL()
	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    check if the position is going to be close enough to a valid vehicle node for the vehicle drop to take place
/// RETURNS:
///    TRUE if node is within range
FUNC BOOL IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP(VECTOR vPosition)
	
	PRINTLN("IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP - called with ", vPosition)
	
	// if passing in same coords as last time then return stored result (for optimising)
	IF (VDIST(g_SpawnData.vLastVector_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP, vPosition) < g_SpawnData.fDistFromLastCall_2)
		PRINTLN("IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP - same as last call, returning ", g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP)	
		RETURN g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP 
	ENDIF

	// store input
	g_SpawnData.vLastVector_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = vPosition	
	
	
	VECTOR vNode
	
	INT iYacht = LOCAL_PLAYER_PRIVATE_YACHT_ID()
	IF NOT (iYacht = -1)
		IF IS_PLAYER_NEAR_YACHT(PLAYER_ID(), iYacht, PRIVATE_YACHT_CAPTAIN_SERVICE_DISTANCE)
			NET_PRINT("     ---------->     VEHICLE DROP - IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP() near yacht. returning TRUE") NET_NL()
			g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF WILL_POINT_USE_CUSTOM_VEHICLE_NODES(vPosition)
		g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = TRUE
		RETURN TRUE
	ENDIF

	// always search switched off nodes, but if we are in the city ensure they aren't dead ends (stop taxi using driveways).
	NODE_FLAGS nodeFlags = NF_IGNORE_SLIPLANES|NF_INCLUDE_SWITCHED_OFF_NODES	
	
	IF NOT IS_POSITION_IN_ZONE_SAFE_FOR_DEADEND_NODES_VD(vPosition)
		nodeFlags = NF_IGNORE_SLIPLANES|NF_INCLUDE_SWITCHED_OFF_NODES|NF_IGNORE_SWITCHED_OFF_DEADENDS
		NET_PRINT("     ---------->     VEHICLE DROP - IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP() using node flags NF_INCLUDE_SWITCHED_OFF_NODES | NF_IGNORE_SWITCHED_OFF_DEADENDS") NET_NL()
	ENDIF	
	
	IF NOT GET_CLOSEST_VEHICLE_NODE(vPosition, vNode, nodeFlags, 100.0, 2.5)
	AND NOT (MPGlobalsAmbience.bRequestedImpoundByPA) // ignore dist check if being requested by PA. fix for 2858321
	
		NET_PRINT("     ---------->     VEHICLE DROP - : IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP() return FALSE  - failed to find closest valid node nearby :") NET_PRINT_VECTOR(vPosition) NET_NL()
		g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = FALSE
		RETURN FALSE
	ENDIF
	
	
	IF NOT (VDIST2(vPosition, <<-237.3, 196.9, 81.6>>) < (100.0 * 100) ) // ignore an awkward spot outside one of the warehouses, where the road uses slip road nodes. 2816550
	
		IF VDIST2(vPosition, vNode) > (VD_MIN_NODE_RANGE * VD_MIN_NODE_RANGE)
		AND NOT (MPGlobalsAmbience.bRequestedImpoundByPA) // ignore dist check if being requested by PA. fix for 2858321
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP () return FALSE  - vPosition more than ") NET_PRINT_FLOAT(VD_MIN_NODE_RANGE) NET_PRINT(" away from closest valid node") NET_NL()
			g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = FALSE
			RETURN FALSE
		ENDIF
		
		// z cap check to 20.0
		FLOAT fZ = vPosition.Z - vNode.Z
		IF ABSF(fZ) > 20.0
			NET_PRINT("     ---------->     VEHICLE DROP : IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP () return FALSE  - Z height diff to big ") NET_NL()
			g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = FALSE
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	g_SpawnData.bLastResult_IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP = TRUE
	RETURN TRUE
ENDFUNC

//PURPOSE: Creates the delivery sequence and tasks the passed in ped with it
PROC CREATE_AND_GIVE_VEHICLE_DROP_DRIVE_SEQUENCE(PED_INDEX thisPed, NETWORK_INDEX niVehicle, VECTOR vDropLocation)

	// match taxi spawning and drive to player
	FLOAT fTargetReached = 45.0
	DRIVINGMODE eDrivingMode = DF_SteerAroundStationaryCars|DF_StopForPeds|DF_StopAtLights|DF_ChangeLanesAroundObstructions|DF_StopForCars|DF_ForceJoinInRoadDirection|DF_SteerAroundObjects|DF_UseSwitchedOffNodes|DF_AvoidRestrictedAreas|DF_UseWanderFallbackInsteadOfStraightLine
	
	SEQUENCE_INDEX seqVehicleDriver
	OPEN_SEQUENCE_TASK(seqVehicleDriver)
		// shouldn't need longe range as spawn dist is capped at 200m
		TASK_VEHICLE_DRIVE_TO_COORD(NULL, NET_TO_VEH(niVehicle), vDropLocation, VD_VEHICLE_SPEED, DRIVINGSTYLE_NORMAL, GET_ENTITY_MODEL(NET_TO_ENT(niVehicle)), eDrivingMode, fTargetReached, 5.0)
		TASK_VEHICLE_PARK(NULL, NET_TO_VEH(niVehicle), vDropLocation, 0, PARK_TYPE_PULL_OVER, 60, TRUE)
		TASK_LEAVE_ANY_VEHICLE(NULL)//, 0, ECF_DONT_CLOSE_DOOR) // B*1598767 - request to close door
		TASK_WANDER_STANDARD(NULL)
	CLOSE_SEQUENCE_TASK(seqVehicleDriver)
	TASK_PERFORM_SEQUENCE(thisPed, seqVehicleDriver)
	CLEAR_SEQUENCE_TASK(seqVehicleDriver)
	
	NET_PRINT("     ---------->     VEHICLE DROP - CREATE_AND_GIVE_VEHICLE_DROP_DRIVE_SEQUENCE tasked vDropLocation = ") NET_PRINT_VECTOR(vDropLocation) NET_NL()
ENDPROC

//PURPOSE: Creates the Stop sequence if driver is at the player
PROC CREATE_AND_GIVE_VEHICLE_STOP_AT_PLAYER_SEQUENCE(PED_INDEX thisPed, VEHICLE_INDEX niVehicle)
	DRIVINGMODE eDrivingMode = DF_SteerAroundStationaryCars|DF_StopForPeds|DF_StopAtLights|DF_ChangeLanesAroundObstructions|DF_StopForCars|DF_ForceJoinInRoadDirection|DF_SteerAroundObjects|DF_UseSwitchedOffNodes|DF_AvoidRestrictedAreas|DF_UseWanderFallbackInsteadOfStraightLine
	
	CLEAR_PED_TASKS(thisPed)
	SEQUENCE_INDEX seqVehicleDriver
	OPEN_SEQUENCE_TASK(seqVehicleDriver)
		//TASK_VEHICLE_PARK(NULL, NET_TO_VEH(niVehicle), GET_ENTITY_COORDS(thisPed), 0, PARK_TYPE_PULL_OVER, 60, TRUE)
		//TASK_VEHICLE_TEMP_ACTION(NULL, niVehicle, TEMPACT_BRAKE, 500)
		TASK_VEHICLE_MISSION(NULL, niVehicle, NULL, MISSION_STOP, VD_VEHICLE_SPEED, eDrivingMode, 1.0, 1.0)
		TASK_LEAVE_ANY_VEHICLE(NULL)//, 0, ECF_DONT_CLOSE_DOOR)	// B*1598767 - request to close door
		TASK_WANDER_STANDARD(NULL)
	CLOSE_SEQUENCE_TASK(seqVehicleDriver)
	TASK_PERFORM_SEQUENCE(thisPed, seqVehicleDriver)
	CLEAR_SEQUENCE_TASK(seqVehicleDriver)
	
	NET_PRINT("     ---------->     VEHICLE DROP - CREATE_AND_GIVE_VEHICLE_STOP_AT_PLAYER_SEQUENCE tasked vDropLocation = ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(thisPed, FALSE)) NET_NL()
ENDPROC

//PURPOSE: Controls the Drivers dialogue line when he delievrs the Car
PROC CONTROL_DRIVER_DIALOGUE(INT &thisBit, NETWORK_INDEX &niDriver, NETWORK_INDEX &niVehicle, structPedsForConversation &thisSpeech)//, VECTOR vDropLocation)//, NETWORK_INDEX niVehicle)
	IF NOT IS_BIT_SET(thisBit, VDP_DialogueDone)
		IF NOT IS_NET_PED_INJURED(niDriver)
			IF NOT IS_PED_IN_ANY_VEHICLE(NET_TO_PED(niDriver))
				RESET_PED_LAST_VEHICLE(NET_TO_PED(niDriver))
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_PED(niDriver), <<VD_DIALOGUE_RANGE, VD_DIALOGUE_RANGE, VD_DIALOGUE_RANGE>>)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
				AND NOT IS_BIT_SET(thisBit, VDP_PlayerDamagedVehicle) // B*1299605
					TASK_LOOK_AT_ENTITY(NET_TO_PED(niDriver), PLAYER_PED_ID(), 5000)
					ADD_PED_FOR_DIALOGUE(thisSpeech, 3, NET_TO_PED(niDriver), "MECHANIC")
					CREATE_CONVERSATION(thisSpeech, "CT_AUD", "MPCT_MCarr", CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT, DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, TRUE)
					NET_PRINT_TIME() NET_PRINT("     ---------->    VEHICLE DROP - CONTROL_DRIVER_DIALOGUE - DONE") NET_NL()
					SET_BIT(thisBit, VDP_DialogueDone)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->    VEHICLE DROP - CONTROL_DRIVER_DIALOGUE - FAIL") NET_NL()
					SET_BIT(thisBit, VDP_DialogueDone)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(thisBit, VDP_StopAtPlayerDone)
					IF IS_ENTITY_AT_ENTITY(NET_TO_PED(niDriver), PLAYER_PED_ID(), <<VD_STOP_AT_PLAYER_RANGE, VD_STOP_AT_PLAYER_RANGE, VD_STOP_AT_PLAYER_RANGE>>, FALSE, FALSE) // Don't check 3D to fix instances of elevated roads, see B*1613213
					//AND NOT IS_ENTITY_AT_COORD(NET_TO_PED(niDriver), vDropLocation, <<VD_STOP_AT_PLAYER_RANGE*2, VD_STOP_AT_PLAYER_RANGE*2, VD_STOP_AT_PLAYER_RANGE*2>>) // Commented out to prevent the mechanic endlessly driving when near the player
						CREATE_AND_GIVE_VEHICLE_STOP_AT_PLAYER_SEQUENCE(NET_TO_PED(niDriver), NET_TO_VEH(niVehicle))
						SET_BIT(thisBit, VDP_StopAtPlayerDone)
						NET_PRINT_TIME() NET_PRINT("     ---------->    VEHICLE DROP - CONTROL_DRIVER_DIALOGUE - VDP_StopAtPlayerDone SET") NET_NL()
					ENDIF
				ENDIF
				IF NOT IS_BIT_SET(thisBit, VDP_PlayerDamagedVehicle)
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(NET_TO_VEH(PERSONAL_VEHICLE_NET_ID(TRUE)), PLAYER_PED_ID())
					SET_BIT(thisBit, VDP_PlayerDamagedVehicle)
					NET_PRINT_TIME() NET_PRINT("     ---------->    VEHICLE DROP - CONTROL_DRIVER_DIALOGUE - VDP_PlayerDamagedVehicle SET") NET_NL()
				ENDIF
			ENDIF
		//ELSE
		//	NET_PRINT_TIME() NET_PRINT("     ---------->    VEHICLE DROP - CONTROL_DRIVER_DIALOGUE - FAIL - MECHANIC INJURED") NET_NL()
		ENDIF
	//ELSE
	//	NET_PRINT_TIME() NET_PRINT("     ---------->    VEHICLE DROP - CONTROL_DRIVER_DIALOGUE - FAIL - thisBit") NET_NL()
	ENDIF
ENDPROC


///////////////////////////////////////////////
///    		VEHICLE AND DRIVER    			///
///////////////////////////////////////////////

//PURPOSE: Create the Vehicle on the Ground
FUNC BOOL CREATE_VEHICLE_ON_GROUND( NETWORK_INDEX &niVehicle, NETWORK_INDEX &niDriver, VECTOR pos, FLOAT heading, MODEL_NAMES VehicleModel, INT &thisBitSet)
	
	IF REQUEST_LOAD_MODEL(VehicleModel)
	//AND REQUEST_LOAD_MODEL(DriverModel)
			
		// Create Vehicle
		IF MPGlobalsAmbience.bLaunchVehicleDropPersonal
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_NL()
			ENDIF
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				IF CREATE_MP_SAVED_VEHICLE(pos, heading, TRUE, MPGlobalsAmbience.iVDPersonalVehicleSlot, TRUE, TRUE)	// don't override position which has already been generated in CREATE_VEHICLE_AND_DRIVER
					niVehicle = PERSONAL_VEHICLE_NET_ID(TRUE)
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PERSONAL VEHICLE COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
					MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niVehicle), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
					SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
				ENDIF
			ENDIF
				
			BOOL bIncludeTrailer = FALSE
			IF IS_MODEL_A_PERSONAL_TRAILER(VehicleModel)
				bIncludeTrailer = TRUE
				IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
					MODEL_NAMES eVehModel = SADLER
					VECTOR trailerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos, heading, -<<0,-7.5,1>>)
					
					IF IS_TOW_VEHICLE_BEING_CLEANED_UP()
						
						// waiting for saddler to be cleaned up
						PRINTLN("     ---------->     VEHICLE DROP - SADDLER - waiting to cleanup previous")
						
					ELSE
						IF REQUEST_LOAD_MODEL(eVehModel)
						AND CREATE_NET_VEHICLE(niDriver, eVehModel, trailerPos, heading, FALSE, DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
						//	niDriver = PERSONAL_VEHICLE_NET_ID(TRUE)
		//					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
							GlobalPlayerBd[NATIVE_TO_INT(PLAYER_ID())].netID_PVTowVeh = niDriver
							MPGlobalsAmbience.TowVehicleID = NET_TO_VEH(niDriver)
							#IF IS_DEBUG_BUILD
								NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PERSONAL VEHICLE TRAILER COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
								VEHICLE_INDEX vehTemp = NET_TO_VEH(niDriver)
								IF IS_VEHICLE_DRIVEABLE(vehTemp)	
									NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
								ENDIF						
							#ENDIF	
							
							IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
								INT iDecoratorValue
								IF DECOR_EXIST_ON(MPGlobalsAmbience.TowVehicleID, "MPBitset")
									iDecoratorValue = DECOR_GET_INT(MPGlobalsAmbience.TowVehicleID, "MPBitset")
								ENDIF
								SET_BIT(iDecoratorValue, MP_DECORATOR_BS_PV_TOW_VEHICLE)
								DECOR_SET_INT(MPGlobalsAmbience.TowVehicleID, "MPBitset", iDecoratorValue)
							ENDIF
							
						//	MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niDriver), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			IF bIncludeTrailer
			AND NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
			AND (NOT bIncludeTrailer OR TAKE_CONTROL_OF_NET_ID(niDriver))
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				VEHICLE_INDEX truckTemp
				IF bIncludeTrailer
					truckTemp = NET_TO_VEH(niDriver)
				ENDIF
				
				IF NOT IS_ENTITY_AT_COORD(NET_TO_VEH(niVehicle), pos, <<10, 10, 10>>)
					SET_ENTITY_HEADING(NET_TO_VEH(niVehicle), heading)
					SET_ENTITY_COORDS(NET_TO_VEH(niVehicle), pos)
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PERSONAL VEHICLE COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
				ENDIF
				
				IF GET_VEHICLE_HAS_LANDING_GEAR(vehTemp)
				AND NOT IS_VEHICLE_MODEL(vehTemp, AVENGER)
					CONTROL_LANDING_GEAR(vehTemp, LGC_DEPLOY_INSTANT)
				ENDIF
				
				IF IS_VEHICLE_MODEL(vehTemp, VALKYRIE)
					SET_VEHICLE_EXTRA(vehTemp, 12, TRUE)
					SET_VEHICLE_EXTRA(vehTemp, 13, TRUE)
					SET_VEHICLE_EXTRA(vehTemp, 14, TRUE)
				ENDIF
				
				IF bIncludeTrailer
				//	SET_VEHICLE_AS_CURRENT__XX""XX__VEHICLE(vehTemp, truckTemp, DEFAULT, DEFAULT)
				//	SET_SAVED__XX""XX__FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
						
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
					AND IS_VEHICLE_DRIVEABLE(truckTemp)
						VEHICLE_SETUP_STRUCT_MP sData
						sData.VehicleSetup.eModel = SADLER
						//sData.VehicleSetup.tlPlateText = "V0MF3U3R"
						sData.VehicleSetup.iPlateIndex = 1
						sData.VehicleSetup.iColour1 = 154
						sData.VehicleSetup.iColour2 = 154
						sData.VehicleSetup.iColourExtra1 = 154
						sData.VehicleSetup.iColourExtra2 = 154
						sData.iColour5 = 1
						sData.iColour6 = 132
						sData.iLivery2 = 0
						sData.VehicleSetup.iWindowTintColour = 1
						sData.VehicleSetup.iWheelType = 4
						sData.VehicleSetup.iTyreR = 255
						sData.VehicleSetup.iTyreG = 255
						sData.VehicleSetup.iTyreB = 255
						sData.VehicleSetup.iNeonR = 255
						sData.VehicleSetup.iNeonB = 255
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_4)
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
						SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
						sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
						sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
						sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
						sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
						sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
						sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
						SET_VEHICLE_SETUP_MP(truckTemp, sData)
						
						IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
							IF NOT DECOR_EXIST_ON(truckTemp,"Not_Allow_As_Saved_Veh")
								DECOR_SET_INT(truckTemp,"Not_Allow_As_Saved_Veh",1)
							ENDIF
						ENDIF
						IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
							INT iDecoratorValue
							IF DECOR_EXIST_ON(truckTemp, "MPBitset")
								iDecoratorValue = DECOR_GET_INT(truckTemp, "MPBitset")
							ENDIF
							SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
							DECOR_SET_INT(truckTemp, "MPBitset", iDecoratorValue)
						ENDIF
						
						SET_VEH_RADIO_STATION(truckTemp, GET_RADIO_STATION_NAME(ENUM_TO_INT(RADIO_GENRE_COUNTRY)))
						
						ATTACH_VEHICLE_TO_TRAILER(truckTemp, vehTemp, DEFAULT)
						SET_TRAILER_LEGS_RAISED(vehTemp)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PERSONAL VEHICLE TRUCK TRAILER ATTACHED ") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PERSONAL VEHICLE TRUCK TRAILER CANNOT BE ATTACHED ") NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(vehTemp), "vehTemp is drivable, ", "vehTemp is NOT drivable, ")) NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(truckTemp), "truckTemp is drivable", "truckTemp is NOT drivable")) NET_NL()
					ENDIF
				ENDIF
				
				SET_BIT(thisBitSet, VDP_VehAtCreationPos)
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			//SET_MODEL_AS_NO_LONGER_NEEDED(DriverModel)
			RETURN TRUE
		ENDIF
		
		// Create Vehicle
		IF MPGlobalsAmbience.bLaunchVehicleDropTruck
			VECTOR trailerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos, heading, <<0,-7.5,1>>)
			
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				CLEAR_AREA(trailerPos, VD_CLEAR_AREA_RANGE+2.0, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_PRINT(" AND ") NET_PRINT_VECTOR(trailerPos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID())
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, pos, heading, FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				//	niVehicle = PERSONAL_VEHICLE_NET_ID(TRUE)
				//	SET_BIT(thisBitSet, VDP_VehAtCreationPos)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GUNRUNNING TRUCK COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
					MPGlobalsAmbience.bJustCreatedTruck = TRUE
				//	MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niVehicle), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
				MODEL_NAMES eVehModel = TRAILERLARGE
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niDriver, eVehModel, trailerPos, heading, FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				//	niDriver = PERSONAL_VEHICLE_NET_ID(TRUE)
//					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GUNRUNNING TRAILER COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niDriver)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
					MPGlobalsAmbience.bJustCreatedTrailer = TRUE
				//	MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niDriver), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
			OR NOT NETWORK_DOES_NETWORK_ID_EXIST(niDriver)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
			AND TAKE_CONTROL_OF_NET_ID(niDriver)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				VEHICLE_INDEX truckTemp = NET_TO_VEH(niDriver)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF NOT IS_ENTITY_AT_COORD(vehTemp, pos, <<10, 10, 10>>)
						IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehTemp)
							DETACH_VEHICLE_FROM_TRAILER(vehTemp)
						ENDIF
						
						SET_ENTITY_HEADING(vehTemp, heading)
						SET_ENTITY_COORDS(vehTemp, pos)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GUNRUNNING TRUCK COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
					ENDIF
					SET_ARMORY_TRUCK_IS_IN_BUNKER(FALSE)
					SET_ARMORY_TRUCK_UNDER_MAP(FALSE)
					SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
					SET_VEHICLE_AS_CURRENT_TRUCK_VEHICLE(vehTemp, truckTemp, DEFAULT, DEFAULT)
					SET_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_PLAYER_TRUCK_IS_IN_FREEMODE(TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
					AND IS_VEHICLE_DRIVEABLE(truckTemp)
						SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE(niVehicle, niDriver)
						
						ATTACH_VEHICLE_TO_TRAILER(vehTemp, truckTemp, DEFAULT)
						SET_TRAILER_LEGS_RAISED(truckTemp)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GUNRUNNING TRUCK TRAILER ATTACHED ") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GUNRUNNING TRUCK TRAILER CANNOT BE ATTACHED ") NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(vehTemp), "vehTemp is drivable, ", "vehTemp is NOT drivable, ")) NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(truckTemp), "truckTemp is drivable", "truckTemp is NOT drivable")) NET_NL()
					ENDIF
					
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GUNRUNNING TRUCK is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			//SET_MODEL_AS_NO_LONGER_NEEDED(DriverModel)
			RETURN TRUE
		ENDIF
		// Create Vehicle
		IF MPGlobalsAmbience.bLaunchVehicleDropAvenger 
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_ARMORY_AIRCRAFT_MODEL()
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, pos, heading, FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				//	niVehicle = PERSONAL_VEHICLE_NET_ID(TRUE)
				//	SET_BIT(thisBitSet, VDP_VehAtCreationPos)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GANG_OPS PLANE COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
					MPGlobalsAmbience.bJustCreatedTruck = TRUE
				//	MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niVehicle), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF NOT IS_ENTITY_AT_COORD(vehTemp, pos, <<10, 10, 10>>)
						IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehTemp)
							DETACH_VEHICLE_FROM_TRAILER(vehTemp)
						ENDIF
						
						SET_ENTITY_HEADING(vehTemp, heading)
						SET_ENTITY_COORDS(vehTemp, pos)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GANG_OPS PLANE COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
					ENDIF
					SET_ARMORY_AIRCRAFT_IS_IN_DEFUNCT_BASE(FALSE)
					SET_ARMORY_AIRCRAFT_UNDER_MAP(FALSE)
					SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
					SET_VEHICLE_AS_CURRENT_AVENGER_VEHICLE(vehTemp, DEFAULT, DEFAULT)
					SET_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_PLAYER_PLANE_IS_IN_FREEMODE(TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
						SET_COMMON_PROPERTIES_FOR_AVENGER_VEHICLE(niVehicle)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GANG_OPS PLANE TRAILER ATTACHED ") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GANG_OPS PLANE TRAILER CANNOT BE ATTACHED ") NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(vehTemp), "vehTemp is drivable, ", "vehTemp is NOT drivable, ")) NET_NL()
					ENDIF
					
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GANG_OPS PLANE is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			//SET_MODEL_AS_NO_LONGER_NEEDED(DriverModel)
			RETURN TRUE
		ENDIF
		// Create Vehicle
		IF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
			VECTOR trailerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos, heading, <<0,-7.5,1>>)
			
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				CLEAR_AREA(trailerPos, VD_CLEAR_AREA_RANGE+2.0, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_PRINT(" AND ") NET_PRINT_VECTOR(trailerPos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_HACKER_TRUCK_MODEL()
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, pos, heading, FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				//	niVehicle = PERSONAL_VEHICLE_NET_ID(TRUE)
				//	SET_BIT(thisBitSet, VDP_VehAtCreationPos)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - HACKER TRUCK COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
				//	MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niVehicle), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF NOT IS_ENTITY_AT_COORD(vehTemp, pos, <<10, 10, 10>>)
						IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehTemp)
							DETACH_VEHICLE_FROM_TRAILER(vehTemp)
						ENDIF
							
						SET_ENTITY_HEADING(vehTemp, heading)
						SET_ENTITY_COORDS(vehTemp, pos)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - HACKER TRUCK COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
					ENDIF
					SET_HACKER_TRUCK_IS_IN_BUSINESS_HUB(FALSE)
					SET_HACKER_TRUCK_UNDER_MAP(FALSE)
					SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
					SET_VEHICLE_AS_CURRENT_HACKER_TRUCK_VEHICLE(vehTemp, DEFAULT, DEFAULT)
					SET_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_PLAYER_HACKERTRUCK_IS_IN_FREEMODE(TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
						SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE(niVehicle)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - HACKER TRUCK TRAILER ATTACHED ") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - HACKER TRUCK TRAILER CANNOT BE ATTACHED ") NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(vehTemp), "vehTemp is drivable, ", "vehTemp is NOT drivable, ")) NET_NL()
					ENDIF
					
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - HACKER TRUCK is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			RETURN TRUE
		ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF MPGlobalsAmbience.bLaunchVehicleDropAcidLab
			VECTOR trailerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos, heading, <<0,-7.5,1>>)
			
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				CLEAR_AREA(trailerPos, VD_CLEAR_AREA_RANGE+2.0, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_PRINT(" AND ") NET_PRINT_VECTOR(trailerPos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_ACID_LAB_MODEL()
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, pos, heading, FALSE,DEFAULT,FALSE,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
				//	niVehicle = PERSONAL_VEHICLE_NET_ID(TRUE)
				//	SET_BIT(thisBitSet, VDP_VehAtCreationPos)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - ACID LAB COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
				//	MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(NET_TO_VEH(niVehicle), MPGlobalsAmbience.iVDPersonalVehicleSlot, FALSE, FALSE, TRUE, FALSE)
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF NOT IS_ENTITY_AT_COORD(vehTemp, pos, <<10, 10, 10>>)
						IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehTemp)
							DETACH_VEHICLE_FROM_TRAILER(vehTemp)
						ENDIF
							
						SET_ENTITY_HEADING(vehTemp, heading)
						SET_ENTITY_COORDS(vehTemp, pos)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - ACID LAB COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
					ENDIF
					SET_ACID_LAB_IN_JUGGALO_HIDEOUT(FALSE)
					SET_ACID_LAB_UNDER_MAP(FALSE)
					SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
					SET_VEHICLE_AS_CURRENT_ACID_LAB_VEHICLE(vehTemp, DEFAULT, DEFAULT)
					SET_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_PLAYER_ACIDLAB_IS_IN_FREEMODE(TRUE)
					
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
						SET_COMMON_PROPERTIES_FOR_ACID_LAB_VEHICLE(niVehicle)
						
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - ACID LAB TRAILER ATTACHED ") NET_NL()
					ELSE
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - ACID LAB TRAILER CANNOT BE ATTACHED ") NET_PRINT(PICK_STRING(IS_VEHICLE_DRIVEABLE(vehTemp), "vehTemp is drivable, ", "vehTemp is NOT drivable, ")) NET_NL()
					ENDIF
					
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - ACID LAB is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			RETURN TRUE
		ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
			VECTOR trailerPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos, heading, <<0,-7.5,1>>)
			
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				CLEAR_AREA(trailerPos, VD_CLEAR_AREA_RANGE+2.0, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_PRINT(" AND ") NET_PRINT_VECTOR(trailerPos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_SUBMARINE_MODEL()

				// the create_net_vehicle applies an offset for the model, so take it away -8
				FLOAT zOffset = -8.0			
				
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, <<pos.x, pos.y, (pos.z + zOffset)>>, heading, FALSE, DEFAULT, FALSE, DEFAULT, FALSE, DEFAULT, DEFAULT, TRUE, TRUE)															
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUBMARINE COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					
					IF NOT IS_TRANSITION_ACTIVE()
						FREEZE_ENTITY_POSITION(vehTemp, TRUE)
						REQUEST_WARP_SUBMARINE(pos, heading)					
					ENDIF

					SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
					SET_VEHICLE_AS_CURRENT_SUBMARINE_VEHICLE(vehTemp, DEFAULT, DEFAULT)
					SET_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_PLAYER_SUBMARINE_IN_FREEMODE(TRUE)
					
					SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE(niVehicle)
				
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUBMARINE is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			RETURN TRUE
		ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
			
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_SUBMARINE_DINGHY_MODEL()
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, pos, heading, FALSE, DEFAULT, FALSE, DEFAULT, FALSE, DEFAULT, DEFAULT, TRUE)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUBMARINE DINGHY COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF NOT IS_ENTITY_AT_COORD(vehTemp, pos, <<10, 10, 10>>)
						SET_ENTITY_HEADING(vehTemp, heading)
						SET_ENTITY_COORDS(vehTemp, pos)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUBMARINE DINGHY COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
					ENDIF

					SET_VEHICLE_AS_CURRENT_SUBMARINE_DINGHY_VEHICLE(vehTemp, DEFAULT, DEFAULT)
					SET_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					
					SET_COMMON_PROPERTIES_FOR_SUBMARINE_DINGHY_VEHICLE(niVehicle)
				
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUBMARINE is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			RETURN TRUE
		ENDIF
		#ENDIF
		
		#IF FEATURE_DLC_2_2022
		IF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
			
			IF NOT IS_BIT_SET(thisBitSet, VDP_ClearAreaDone)
				CLEAR_AREA(pos, VD_CLEAR_AREA_RANGE, FALSE, FALSE, FALSE, TRUE)
				
				SET_BIT(thisBitSet, VDP_ClearAreaDone)
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - A - CLEAR AREA AT ") NET_PRINT_VECTOR(pos) NET_NL()
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				MODEL_NAMES eVehModel = GET_SUPPORT_BIKE_MODEL()
				IF REQUEST_LOAD_MODEL(eVehModel)
				AND CREATE_NET_VEHICLE(niVehicle, eVehModel, pos, heading, FALSE, DEFAULT, FALSE, DEFAULT, FALSE, DEFAULT, DEFAULT, TRUE)
					#IF IS_DEBUG_BUILD
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUPPORT BIKE COORDS SET - CREATE_MP_SAVED_VEHICLE - spawn pos = ")
						VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
						IF IS_VEHICLE_DRIVEABLE(vehTemp)	
							NET_PRINT_VECTOR(GET_ENTITY_COORDS(vehTemp)) NET_PRINT(" heading = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(vehTemp)) NET_NL()							
						ENDIF						
					#ENDIF	
				ENDIF
			ENDIF
			
			IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
				RETURN FALSE
			ENDIF
			
			IF TAKE_CONTROL_OF_NET_ID(niVehicle)
				VEHICLE_INDEX vehTemp = NET_TO_VEH(niVehicle)
				
				IF NOT IS_ENTITY_DEAD(vehTemp)
					IF NOT IS_ENTITY_AT_COORD(vehTemp, pos, <<10, 10, 10>>)
						SET_ENTITY_HEADING(vehTemp, heading)
						SET_ENTITY_COORDS(vehTemp, pos)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUPPORT BIKE COORDS SET - AT ") NET_PRINT_VECTOR(pos) NET_NL()
					ENDIF
					
					SET_VEHICLE_AS_CURRENT_SUPPORT_BIKE_VEHICLE(vehTemp, DEFAULT, DEFAULT)
					SET_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_COMMON_PROPERTIES_FOR_SUPPORT_BIKE_VEHICLE(niVehicle)
					
					SET_BIT(thisBitSet, VDP_VehAtCreationPos)
				ELSE
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - SUPPORT BIKE is dead ") NET_NL()
				ENDIF
			ENDIF
			
			SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
			RETURN TRUE
		ENDIF
		#ENDIF
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(niVehicle)
		RETURN FALSE
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(VehicleModel)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    perform checks on vSpawnPosition to make sure it's safe for spawning taxi
/// PARAMS:
///    vPlayerCoords - player's position
///    vSpawnPosition - place to check is safe
///    fMaxSpawnDist - max dist vSpawnPosition is allowed to be from vPlayerCoords (jMart said 300m is path loading size
///    fMinSpawnDist - min dist vSpawnPosition has to be from vPlayerCoords
/// RETURNS:
///    TRUE if spawn position is safe
FUNC BOOL IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP(VECTOR vPlayerCoords, VECTOR vSpawnPosition, FLOAT fMaxSpawnDist = VD_VEHICLE_MAX_SPAWN_RANGE, FLOAT fMinSpawnDist = VD_VEHICLE_MIN_SPAWN_RANGE, BOOL bIgnoreDistanceCheck = FALSE, BOOL bIgnoreSightChecks = FALSE)

	RETURN TRUE // this function is no longer required. Neil F. 3243131

	// cheaper
	FLOAT fDistSquared =	VDIST(vPlayerCoords, vSpawnPosition) 	//VDIST2(vPlayerCoords, vSpawnPosition)
	PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : vPlayerCoords = ", vPlayerCoords)
	PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : vSpawnPosition = ", vSpawnPosition)
	PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : fDistSquared = ", fDistSquared)
		
	// check the spawn position isn't too far away
	IF NOT bIgnoreDistanceCheck
		IF fDistSquared >= fMaxSpawnDist	//(fMaxSpawnDist * fMaxSpawnDist)
			PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : return FALSE spawn pos too far away!")
			PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : fDistSquared = ", fDistSquared, " fMaxSpawnDist = ", fMaxSpawnDist)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// check the spawn position isn't too close to the playe
	IF fDistSquared <= fMinSpawnDist	//(fMinSpawnDist * fMinSpawnDist)
		//CPRINTLN(DEBUG_AMBIENT, GET_THIS_SCRIPT_NAME(), " : IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP() : return FALSE spawn pos to close to player!")
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : return FALSE spawn pos to close to player!  fDistSquared = ", fDistSquared, " fMinSpawnDist = ", fMinSpawnDist)
		RETURN FALSE
	ENDIF
	
		IF (MPGlobalsAmbience.bYachtPersonalVehicleRequest)	
			// don't do visible check if spawning from yacht
		ELSE
		
	// check player won't see it spawn in
	IF NOT bIgnoreSightChecks
		IF IS_SPHERE_VISIBLE(vSpawnPosition, 2.5)
			PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : return FALSE sphere at position visible")
			RETURN FALSE
		ENDIF
	ENDIF
	
		ENDIF
	
	//check the position isn't obsured
	IF IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vSpawnPosition, <<3,3,3>>)
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : return FALSE point obsured by mission entity")
		RETURN FALSE
	ENDIF
	
	IF IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA(vSpawnPosition)
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP : return FALSE point in taxi restricted area")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

/// PURPOSE:
///    perform checks on vSpawnPosition to make sure it's safe for spawning truck
/// PARAMS:
///    vPlayerCoords - player's position
///    vSpawnPosition - place to check is safe
///    fMaxSpawnDist - max dist vSpawnPosition is allowed to be from vPlayerCoords (jMart said 300m is path loading size
///    fMinSpawnDist - min dist vSpawnPosition has to be from vPlayerCoords
/// RETURNS:
///    TRUE if spawn position is safe
FUNC BOOL IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP(VECTOR vSpawnPosition, FLOAT fSpawnHeading,
		FLOAT fMaxSpawnDist = VD_VEHICLE_MAX_SPAWN_RANGE,
		FLOAT fMinSpawnDist = VD_VEHICLE_MIN_SPAWN_RANGE,
		BOOL bIgnoreDistanceCheck = FALSE,
		BOOL bIgnoreSightChecks = FALSE)
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	VECTOR vTrailerCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPosition, fSpawnHeading, <<0,-7.5,1>>)
	
	// cheaper
	FLOAT fDist =	VDIST(vPlayerCoords, vSpawnPosition)
	
	// check the spawn position isn't too far away
	IF NOT bIgnoreDistanceCheck
		IF fDist >= fMaxSpawnDist	//(fMaxSpawnDist * fMaxSpawnDist)
			PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE spawn pos ", vSpawnPosition, " too far away from player ", vPlayerCoords, " !  fDist = ", fDist, " fMaxSpawnDist = ", fMaxSpawnDist)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// check the spawn position isn't too close to the playe
	IF fDist <= fMinSpawnDist	//(fMinSpawnDist * fMinSpawnDist)
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE spawn pos ", vSpawnPosition, " to close to player ", vPlayerCoords, " !  fDist = ", fDist, " fMinSpawnDist = ", fMinSpawnDist)
		RETURN FALSE
	ENDIF
	
//	#IF FEATURE_APARTMENT_CONTENT
//		IF (MPGlobalsAmbience.bYachtPersonalVehicleRequest)	
//			// don't do visible check if spawning from yacht
//		ELSE
//	#ENDIF
		
	// check player won't see it spawn in
	IF NOT bIgnoreSightChecks
		IF fDist <= 150.0		//300.0
			IF IS_SPHERE_VISIBLE(vSpawnPosition, 2.0)
				PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE sphere at spawn position ", vSpawnPosition, " visible!  fDist = ", fDist, " vPlayerCoords = ", vPlayerCoords)
				RETURN FALSE
			ENDIF
			IF IS_SPHERE_VISIBLE(vTrailerCoords, 4.0)
				PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE sphere at trailer position ", vTrailerCoords, " visible!  fDist = ", fDist, " vPlayerCoords = ", vPlayerCoords)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
//	#IF FEATURE_APARTMENT_CONTENT
//		ENDIF
//	#ENDIF
	
	//check the position isn't obsured
	IF IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vSpawnPosition, <<4,4,4>>)
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE spawn point ", vSpawnPosition, " obsured by mission entity")
		RETURN FALSE
	ENDIF
	IF IS_POINT_OBSCURED_BY_A_MISSION_ENTITY(vTrailerCoords, <<8,8,4>>)
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE trailer point ", vTrailerCoords, " obsured by mission entity")
		RETURN FALSE
	ENDIF
	
	IF IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA(vSpawnPosition)
		PRINTLN(GET_THIS_SCRIPT_NAME(), " : VEHICLE DROP - IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP : return FALSE point ", vSpawnPosition, " in taxi restricted area")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC


CONST_INT NUMBER_OF_TRUCK_SPAWNS 60

FUNC BOOL GET_TRUCK_VEHICLE_SPAWN_LOCATIONS(INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	vCoords = <<0,0,0>>		fHeading = 0.0	
	SWITCH iLocation
		CASE 0	vCoords = <<1284.7395, -545.5959, 68.6026>>		fHeading = 80.5526	BREAK	// Mirror Park	(car gens may be an issue)
		CASE 1	vCoords = <<-1467.9331, -795.3086, 22.7123>>	fHeading = 319.5706	BREAK	// Del Perro Pier
		CASE 2	vCoords = <<-1096.3696, -1715.5807, 3.2635>>	fHeading = 305.2817	BREAK	// Vespucci Beach
		CASE 3	vCoords = <<-770.2704, -1303.4984, 4.0004>>		fHeading = 320.4224	BREAK	// La Puerta
		CASE 4	vCoords = <<-224.9072, -2042.7854, 26.6204>>	fHeading = 323.4220	BREAK	// Maze Bank Arena
		CASE 5	vCoords = <<-928.1251, -2078.0291, 8.2990>>		fHeading = 134.4157	BREAK	// LSIA
		CASE 6	vCoords = <<968.5782, -3232.8459, 4.8981>>		fHeading = 89.7001	BREAK	// Terminal
		CASE 7	vCoords = <<1255.7095, -2358.0815, 48.9347>>	fHeading = 353.8768	BREAK	// El Burro Heights
		CASE 8	vCoords = <<55.2290, -1724.0416, 28.3030>>		fHeading = 319.5799	BREAK	// Davis	(car gens may be an issue)
		CASE 9	vCoords = <<1150.1780, -1483.1593, 33.6926>>	fHeading = 359.6766	BREAK	// El Burro Heights	(car drive scenario going through spot)
		CASE 10	vCoords = <<37.8095, 52.0435, 71.6005>>			fHeading = 159.7696	BREAK	// West Vinewood
		CASE 11	vCoords = <<-33.9780, -98.8644, 56.3819>>		fHeading = 159.6577	BREAK	// Hawick	(Alley - could be too tight for expanded and has vehicle gens)
		CASE 12	vCoords = <<-306.8060, -979.8259, 30.0806>>		fHeading = 250.0748	BREAK	// Pillbox Hill
		CASE 13	vCoords = <<315.2949, -698.7788, 28.3404>>		fHeading = 250.2768	BREAK	// Textile City	(drop off for Fake ID business)
		CASE 14	vCoords = <<512.2047, -1148.3099, 28.3127>>		fHeading = 1.7660	BREAK	// La Mesa	(ALT - close to the above so can be freed up for a new spot if needed + car scenarios here)
		CASE 15	vCoords = <<-562.7316, 327.9078, 83.4115>>		fHeading = 265.3254	BREAK	// West Vinewood
		CASE 16	vCoords = <<-951.5113, 171.5849, 64.5215>>		fHeading = 94.4913	BREAK	// Rockford Hills	(car drive scenario going through spot)
		CASE 17	vCoords = <<-836.8151, 362.5360, 86.1082>>		fHeading = 89.2988	BREAK	// Rockford Hills
		CASE 18	vCoords = <<-2076.5588, -229.3211, 20.0404>>	fHeading = 26.5634	BREAK	// Pacific Bluffs
		CASE 19	vCoords = <<-2191.7969, -378.2889, 12.2412>>	fHeading = 49.1378	BREAK	// Pacific Bluffs	(car drive scenario going through spot)
		CASE 20	vCoords = <<-3017.1223, 357.6781, 13.5654>>		fHeading = 170.0936	BREAK	// Banham Canyon	(car gens + scenario may be an issue)
		CASE 21	vCoords = <<-3092.7588, 1155.5844, 19.3590>>	fHeading = 356.6831	BREAK	// Chumash
		CASE 22	vCoords = <<-1902.0404, 764.0038, 139.8360>>	fHeading = 133.0039	BREAK	// Richmen Glen
		CASE 23	vCoords = <<-405.0357, 1175.4813, 324.6416>>	fHeading = 253.2484	BREAK	// Galileo Obseravtory
		CASE 24	vCoords = <<709.5561, 648.7717, 127.9112>>		fHeading = 250.8219	BREAK	// Vinewood Hills
		CASE 25	vCoords = <<960.2486, 156.6699, 79.8307>>		fHeading = 140.1893	BREAK	// Vinewood Racetrack	(car gens + scenario may be an issue)
		CASE 26	vCoords = <<893.7540, -66.7553, 77.7643>>		fHeading = 148.1264	BREAK	// East Vinewood
		CASE 27	vCoords = <<1700.5562, -1444.7173, 112.1386>>	fHeading = 340.5691	BREAK	// El Burro Heights	(scenarios may be an issue)
		CASE 28	vCoords = <<2569.2188, 401.6441, 107.4593>>		fHeading = 358.9408	BREAK	// Tatavium Mountains
		CASE 29	vCoords = <<1609.1687, 1179.6801, 83.2313>>		fHeading = 181.0576	BREAK	// Vinewood Hills
		
		CASE 30	vCoords = <<2056.3582, 5188.7412, 51.3049>>		fHeading = 39.8946	BREAK	// Grapeseed	(Placed near Bunker)
		CASE 31	vCoords = <<274.6361, 3129.0549, 40.9197>>		fHeading = 88.0160	BREAK	// Grand Senora Desert	(Placed near Bunker)
		CASE 32	vCoords = <<922.5869, 3084.8281, 40.4309>>		fHeading = 277.2226	BREAK	// Grand Senora Desert	(Placed near Bunker)
		CASE 33	vCoords = <<1466.5073, 2243.2249, 75.0900>>		fHeading = 206.6338	BREAK	// Grand Senora Desert	(Placed near Bunker)
		CASE 34	vCoords = <<529.8820, 2609.0593, 41.2793>>		fHeading = 8.4184	BREAK	// Harmony	(Placed near Bunker)
		CASE 35	vCoords = <<-209.9119, 4217.9932, 43.6867>>		fHeading = 255.8171	BREAK	// Calafia Bridge	(Placed near Bunker + car drive scenario going through spot)
		CASE 36	vCoords = <<2186.9873, 3368.1038, 44.4263>>		fHeading = 297.6278	BREAK	// Sandy Shores	(Placed near Bunker)
		CASE 37	vCoords = <<2552.0815, 3250.3733, 51.7113>>		fHeading = 312.1330	BREAK	// Grand Senora Desert	(Placed near Bunker)
		CASE 38	vCoords = <<487.0461, 6589.9878, 25.2470>>		fHeading = 216.8444	BREAK	// Paleto Bay	(Placed near Bunker)
		CASE 39	vCoords = <<-647.7258, 5907.3638, 16.0567>>		fHeading = 23.2491	BREAK	// Paleto Forest	(Placed near Bunker)
		CASE 40	vCoords = <<1996.3438, 2602.3235, 53.3127>>		fHeading = 133.0891	BREAK	// Grand Senora Desert
		CASE 41	vCoords = <<2168.1475, 2869.9507, 45.7404>>		fHeading = 241.2353	BREAK	// Grand Senora Desert
		CASE 42	vCoords = <<2523.6399, 1603.3480, 29.0631>>		fHeading = 299.8963	BREAK	// Ron's Alternate Windfarm
		CASE 43	vCoords = <<2204.4773, 3709.9663, 36.2608>>		fHeading = 117.0373	BREAK	// Sandy Shores
		CASE 44	vCoords = <<2656.4993, 4307.1104, 43.5602>>		fHeading = 35.6329	BREAK	// Grapeseed
		CASE 45	vCoords = <<1418.7610, 3731.9033, 31.8520>>		fHeading = 199.6500	BREAK	// Sandy Shores
		CASE 46	vCoords = <<1803.8041, 3277.6375, 41.7935>>		fHeading = 213.5281	BREAK	// Sandy Shores Airfield
		CASE 47	vCoords = <<-192.9088, 6206.8257, 30.5037>>		fHeading = 224.0903	BREAK	// Paleto Bay
		CASE 48	vCoords = <<-824.0980, 5441.8135, 32.7164>>		fHeading = 50.8771	BREAK	// Paleto Forest	(car scenarios may be an issue)
		CASE 49	vCoords = <<-959.2971, 5420.9688, 37.9608>>		fHeading = 110.5969	BREAK	// Paleto Forest
		CASE 50	vCoords = <<-1527.4692, 4995.7827, 61.4537>>	fHeading = 138.5709	BREAK	// Great Ocean Highway
		CASE 51	vCoords = <<-2234.4683, 4313.8672, 47.1469>>	fHeading = 151.7738	BREAK	// North Chumash
		CASE 52	vCoords = <<-2510.7588, 3588.1436, 13.5909>>	fHeading = 169.5942	BREAK	// North Chumash
		CASE 53	vCoords = <<-2734.8918, 2301.7727, 17.2203>>	fHeading = 156.3156	BREAK	// Lago Zancudo
		CASE 54	vCoords = <<-2210.6809, 2309.3918, 32.0001>>	fHeading = 108.1975	BREAK	// Tongva Hills
		CASE 55	vCoords = <<-1546.5492, 2145.4927, 54.4770>>	fHeading = 300.3906	BREAK	// Tongva Valley
		CASE 56	vCoords = <<-78.0324, 2007.6090, 179.0079>>		fHeading = 135.3284	BREAK	// Grand Senora Desert	(car scenarios may be an issue)
		CASE 57	vCoords = <<-406.9017, 2813.6167, 44.5846>>		fHeading = 323.1792	BREAK	// Zancudo River
		CASE 58	vCoords = <<1074.6450, 2056.9958, 51.6412>>		fHeading = 94.7879	BREAK	// Redwood Lights Track	(car scenarios may be an issue)
		CASE 59	vCoords = <<352.5322, 4455.8276, 61.6691>>		fHeading = 20.8798	BREAK	// Mount Chiliad
		CASE NUMBER_OF_TRUCK_SPAWNS								RETURN FALSE
	ENDSWITCH
	RETURN NOT IS_VECTOR_ZERO(vCoords)
ENDFUNC

PROC ADD_CUSTOM_NODES_FOR_ALL_TRUCK_SPAWNS()
	
	PRINTLN("ADD_CUSTOM_NODES_FOR_ALL_TRUCK_SPAWNS - called")
	DEBUG_PRINTCALLSTACK()
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	INT iLocation
	VECTOR vCoords
	FLOAT fHeading
	
	REPEAT NUMBER_OF_TRUCK_SPAWNS iLocation
		IF GET_TRUCK_VEHICLE_SPAWN_LOCATIONS(iLocation, vCoords, fHeading)
			ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
		ENDIF
	ENDREPEAT
	
ENDPROC

//PROC GET_PRIVATE_TRUCK_PERSONAL_VEHICLE_SPAWN_LOCATION(INT &iClosestLocation, VECTOR &vCoords, FLOAT &fHeading)
//	BOOL bIgnoreDistanceCheck = FALSE
//	BOOL bIgnoreSightChecks = FALSE
//	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
//	
//	SIMPLE_INTERIORS eSimpleInteriors = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
//	IF (eSimpleInteriors != SIMPLE_INTERIOR_INVALID)
//		vPlayerCoords = GET_BLIP_COORD_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
//		bIgnoreDistanceCheck = TRUE
//		bIgnoreSightChecks = TRUE
//	ENDIF
//	
//	iClosestLocation = -1
//	INT iLocation = 0
//	VECTOR vLocCoords
//	FLOAT fLocHeading, fClosestDistance = 999999.0
//	WHILE GET_TRUCK_VEHICLE_SPAWN_LOCATIONS(iLocation, vLocCoords, fLocHeading)
//	//	FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, vLocCoords, FALSE)
//		FLOAT fTravelDistance = CALCULATE_TRAVEL_DISTANCE_BETWEEN_POINTS(vPlayerCoords, vLocCoords)
//		IF fTravelDistance < fClosestDistance
//			IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP(vLocCoords, fLocHeading,
//					10000.0,	//FLOAT fMaxSpawnDist = VD_VEHICLE_MAX_SPAWN_RANGE,
//					DEFAULT,	//FLOAT fMinSpawnDist = VD_VEHICLE_MIN_SPAWN_RANGE,
//					bIgnoreDistanceCheck,
//					bIgnoreSightChecks)
//				fClosestDistance = fTravelDistance
//				iClosestLocation = iLocation
//			ELSE
//				CDEBUG1LN(DEBUG_AMBIENT, "GET_PRIVATE_TRUCK_PERSONAL_VEHICLE_SPAWN_LOCATION - location ", iLocation, " not safe")
//			ENDIF
//		ENDIF
//		
//		iLocation++
//	ENDWHILE
//	
//	GET_TRUCK_VEHICLE_SPAWN_LOCATIONS(iClosestLocation, vCoords, fHeading)
//ENDPROC

CONST_INT NUMBER_OF_HANGARS										5

CONST_INT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_STANDARD			15
CONST_INT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_LARGE				33
CONST_INT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_ALL				33

CONST_INT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_HANGAR_STANDARD	9
CONST_INT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_HANGAR_LARGE		30

FUNC BOOL CAN_VEHICLE_MODEL_USE_STANDARD_SPAWN(MODEL_NAMES eVehicleModel)
	SWITCH eVehicleModel
		CASE TITAN
		CASE SKYLIFT
		CASE SHAMAL
		CASE MILJET
		CASE LUXOR
		CASE LUXOR2
		CASE NIMBUS
		CASE BOMBUSHKA
		CASE AVENGER
		CASE VOLATOL
		CASE ALKONOST
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_PERSONAL_AIRCRAFT_SPAWN_LOCATIONS_STANDARD(INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	vCoords = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
	
	SWITCH iLocation
		CASE 0	vCoords = <<1180.9164, 302.2191, 80.9909>>		fHeading = 148.3989	BREAK
		CASE 1	vCoords = <<1275.9928, 209.0666, 80.8549>>		fHeading = 148.3989	BREAK
		CASE 2	vCoords = <<616.9166, -712.7324, 11.5367>>		fHeading = 178.1982	BREAK
		CASE 3	vCoords = <<570.2534, -712.5157, 11.5039>>		fHeading = 179.9983	BREAK
		CASE 4	vCoords = <<614.4362, -1273.8783, 8.7479>>		fHeading = 196.3985	BREAK
		CASE 5	vCoords = <<570.6973, -1285.6173, 8.7397>>		fHeading = 134.4157	BREAK
		CASE 6	vCoords = <<-318.9105, -2553.1855, 5.0007>>		fHeading = 48.9986	BREAK
		CASE 7	vCoords = <<-373.5407, -2630.2053, 5.0003>>		fHeading = 135.7982	BREAK
		CASE 8	vCoords = <<297.4395, -3108.8081, 4.8849>>		fHeading = 358.7974	BREAK
		CASE 9	vCoords = <<181.5659, -3282.0044, 4.6287>>		fHeading = 359.9974	BREAK
		CASE 10	vCoords = <<1008.1997, -2918.9448, 4.9006>>		fHeading = 89.9964	BREAK
		CASE 11	vCoords = <<1030.6255, -2918.8599, 4.9006>>		fHeading = 269.1960	BREAK
		CASE 12	vCoords = <<1496.3076, -2297.1604, 73.5446>>	fHeading = 151.3958	BREAK
		CASE 13	vCoords = <<1808.4520, -1288.0942, 95.0310>>	fHeading = 113.1954	BREAK
		CASE 14	vCoords = <<-1130.4027, -1694.8329, 3.3732>>	fHeading = 118.9948	BREAK
		CASE NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_STANDARD		RETURN FALSE
	ENDSWITCH
	
	RETURN NOT IS_VECTOR_ZERO(vCoords)
ENDFUNC

FUNC BOOL GET_PERSONAL_AIRCRAFT_SPAWN_LOCATIONS_LARGE(INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	
	vCoords = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
	
	SWITCH iLocation
		CASE 0		vCoords = <<1063.0157, 3074.6736, 40.2213>>		fHeading = 284.7990	BREAK
		CASE 1		vCoords = <<1078.6023, 3017.7776, 40.2675>>		fHeading = 285.5990	BREAK
		CASE 2		vCoords = <<1402.6498, 3001.2241, 39.5500>>		fHeading = 313.9985	BREAK
		CASE 3		vCoords = <<1794.3689, 3271.5613, 41.3554>>		fHeading = 99.3988	BREAK
		CASE 4		vCoords = <<1927.4818, 4710.6841, 40.1854>>		fHeading = 295.5985	BREAK
		CASE 5		vCoords = <<-2732.5339, 2929.5127, 1.2234>>		fHeading = 175.5980	BREAK
		CASE 6		vCoords = <<1542.3007, 3176.7744, 40.0060>>		fHeading = 300.7964	BREAK
		CASE 7		vCoords = <<824.6652, 6624.6675, 1.0573>>		fHeading = 89.9993	BREAK
		CASE 8		vCoords = <<316.3836, 6907.3242, 2.6752>>		fHeading = 223.3988	BREAK
		CASE 9		vCoords = <<-617.3377, 6321.4253, 2.3934>>		fHeading = 308.1984	BREAK
		CASE 10		vCoords = <<-261.3058, 6575.1196, 1.5169>>		fHeading = 135.5977	BREAK
		CASE 11		vCoords = <<-2256.2988, 4491.5430, 1.7095>>		fHeading = 137.7965	BREAK
		CASE 12		vCoords = <<-2597.5154, 3515.2341, 10.5145>>	fHeading = 339.5955	BREAK
		CASE 13		vCoords = <<-2270.3621, 4475.9517, 1.6268>>		fHeading = 136.5958	BREAK
		CASE 14		vCoords = <<-2470.5657, 4193.1543, 3.6440>>		fHeading = 172.5953	BREAK
		CASE 15		vCoords = <<2821.8313, 4703.8193, 45.3946>>		fHeading = 19.7947	BREAK
		CASE 16		vCoords = <<3248.6128, 5062.4663, 20.2962>>		fHeading = 264.1931	BREAK
		CASE 17		vCoords = <<2124.8259, 2797.1843, 49.2823>>		fHeading = 70.1927	BREAK
		CASE 18		vCoords = <<1623.7146, 2064.1597, 85.9994>>		fHeading = 333.1922	BREAK
		CASE 19		vCoords = <<1791.2021, 1744.1121, 71.7541>>		fHeading = 336.5922	BREAK
		CASE 20		vCoords = <<2499.5400, 1071.9027, 75.0344>>		fHeading = 302.7912	BREAK
		CASE 21		vCoords = <<2904.8059, 403.0110, 1.4181>>		fHeading = 175.7903	BREAK
		CASE 22		vCoords = <<1477.9835, 1558.7197, 109.1304>>	fHeading = 209.7909	BREAK
		CASE 23		vCoords = <<342.9406, 2711.4727, 54.9858>>		fHeading = 302.9901	BREAK
		CASE 24		vCoords = <<-5.9789, 2601.9780, 83.8922>>		fHeading = 2.3895	BREAK
		CASE 25		vCoords = <<-2776.4878, 2338.9163, 1.3561>>		fHeading = 355.5891	BREAK
		CASE 26		vCoords = <<-3283.1919, 1082.1276, 1.1924>>		fHeading = 349.1890	BREAK
		CASE 27		vCoords = <<946.7811, -2623.2400, 3.8948>>		fHeading = 253.5885	BREAK
		CASE 28		vCoords = <<-2085.1997, -548.4119, 2.8008>>		fHeading = 222.5882	BREAK
		CASE 29		vCoords = <<-1707.7856, -826.0955, 8.2391>>		fHeading = 69.3881	BREAK
		CASE 30		vCoords = <<-1744.3962, -852.9501, 7.3410>>		fHeading = 54.9880	BREAK
		CASE 31		vCoords = <<-2079.0583, -494.0884, 6.4533>>		fHeading = 210.9880	BREAK
		CASE 32		vCoords = <<-1451.1891, -1121.3497, 2.1671>>	fHeading = 170.7995	BREAK
		CASE NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_LARGE				RETURN FALSE
	ENDSWITCH
	
	RETURN NOT IS_VECTOR_ZERO(vCoords)
ENDFUNC

FUNC BOOL GET_PERSONAL_AIRCRAFT_SPAWN_LOCATIONS_ALL(INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	vCoords = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
	
	SWITCH iLocation
		CASE 0	vCoords = <<1063.0157, 3074.6736, 40.2213>>		fHeading = 284.7990	BREAK
		CASE 1	vCoords = <<1078.6023, 3017.7776, 40.2675>>		fHeading = 285.5990	BREAK
		CASE 2	vCoords = <<1402.6498, 3001.2241, 39.5500>>		fHeading = 313.9985	BREAK
		CASE 3	vCoords = <<1794.3689, 3271.5613, 41.3554>>		fHeading = 99.3988	BREAK
		CASE 4	vCoords = <<1927.4818, 4710.6841, 40.1854>>		fHeading = 295.5985	BREAK
		CASE 5	vCoords = <<-2732.5339, 2929.5127, 1.2234>>		fHeading = 175.5980	BREAK
		CASE 6	vCoords = <<1542.3007, 3176.7744, 40.0060>>		fHeading = 300.7964	BREAK
		CASE 7	vCoords = <<824.6652, 6624.6675, 1.0573>>		fHeading = 89.9993	BREAK
		CASE 8	vCoords = <<316.3836, 6907.3242, 2.6752>>		fHeading = 223.3988	BREAK
		CASE 9	vCoords = <<-617.3377, 6321.4253, 2.3934>>		fHeading = 308.1984	BREAK
		CASE 10	vCoords = <<-261.3058, 6575.1196, 1.5169>>		fHeading = 135.5977	BREAK
		CASE 11	vCoords = <<-2256.2988, 4491.5430, 1.7095>>		fHeading = 137.7965	BREAK
		CASE 12	vCoords = <<-2597.5154, 3515.2341, 10.5145>>	fHeading = 339.5955	BREAK
		CASE 13	vCoords = <<-2270.3621, 4475.9517, 1.6268>>		fHeading = 136.5958	BREAK
		CASE 14	vCoords = <<-2470.5657, 4193.1543, 3.6440>>		fHeading = 172.5953	BREAK
		CASE 15	vCoords = <<2821.8313, 4703.8193, 45.3946>>		fHeading = 19.7947	BREAK
		CASE 16	vCoords = <<3248.6128, 5062.4663, 20.2962>>		fHeading = 264.1931	BREAK
		CASE 17	vCoords = <<2124.8259, 2797.1843, 49.2823>>		fHeading = 70.1927	BREAK
		CASE 18	vCoords = <<1623.7146, 2064.1597, 85.9994>>		fHeading = 333.1922	BREAK
		CASE 19	vCoords = <<1791.2021, 1744.1121, 71.7541>>		fHeading = 336.5922	BREAK
		CASE 20	vCoords = <<2499.5400, 1071.9027, 75.0344>>		fHeading = 302.7912	BREAK
		CASE 21	vCoords = <<2904.8059, 403.0110, 1.4181>>		fHeading = 175.7903	BREAK
		CASE 22	vCoords = <<1477.9835, 1558.7197, 109.1304>>	fHeading = 209.7909	BREAK
		CASE 23	vCoords = <<342.9406, 2711.4727, 54.9858>>		fHeading = 302.9901	BREAK
		CASE 24	vCoords = <<-5.9789, 2601.9780, 83.8922>>		fHeading = 2.3895	BREAK
		CASE 25	vCoords = <<-2776.4878, 2338.9163, 1.3561>>		fHeading = 355.5891	BREAK
		CASE 26	vCoords = <<-3283.1919, 1082.1276, 1.1924>>		fHeading = 349.1890	BREAK
		CASE 27	vCoords = <<946.7811, -2623.2400, 3.8948>>		fHeading = 253.5885	BREAK
		CASE 28	vCoords = <<-2085.1997, -548.4119, 2.8008>>		fHeading = 222.5882	BREAK
		CASE 29	vCoords = <<-1707.7856, -826.0955, 8.2391>>		fHeading = 69.3881	BREAK
		CASE 30	vCoords = <<-1744.3962, -852.9501, 7.3410>>		fHeading = 54.9880	BREAK
		CASE 31	vCoords = <<-2079.0583, -494.0884, 6.4533>>		fHeading = 210.9880	BREAK
		CASE 32	vCoords = <<-1451.1891, -1121.3497, 2.1671>>	fHeading = 170.7995	BREAK
		CASE NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_ALL				RETURN FALSE
	ENDSWITCH
	
	RETURN NOT IS_VECTOR_ZERO(vCoords)
ENDFUNC

FUNC SIMPLE_INTERIORS GET_HANGAR_ID(INT i)
	SIMPLE_INTERIORS eHangarID
	
	SWITCH i
		CASE 0	eHangarID = SIMPLE_INTERIOR_HANGAR_1	BREAK
		CASE 1	eHangarID = SIMPLE_INTERIOR_HANGAR_2	BREAK
		CASE 2	eHangarID = SIMPLE_INTERIOR_HANGAR_3	BREAK
		CASE 3	eHangarID = SIMPLE_INTERIOR_HANGAR_4	BREAK
		CASE 4	eHangarID = SIMPLE_INTERIOR_HANGAR_5	BREAK
	ENDSWITCH
	
	RETURN eHangarID
ENDFUNC

PROC ADD_CUSTOM_NODES_FOR_PERSONAL_AIRCRAFT_SPAWNS(MODEL_NAMES eVehicleModel)
	PRINTLN("ADD_CUSTOM_NODES_FOR_PERSONAL_AIRCRAFT_SPAWNS - called")
	DEBUG_PRINTCALLSTACK()
	
	INT i
	INT iLocation
	VECTOR vCoords
	FLOAT fHeading
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	
	REPEAT NUMBER_OF_HANGARS i
		SIMPLE_INTERIORS eHangarID = GET_HANGAR_ID(i)
		
		IF CanLocalPlayerSpawnAtHangar(eHangarID)
		AND (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<g_SimpleInteriorData.vMidPoints[eHangarID].X, g_SimpleInteriorData.vMidPoints[eHangarID].Y, 0.0>>) < 300.0)
			iLocation = 0
			
			IF CAN_VEHICLE_MODEL_USE_STANDARD_SPAWN(eVehicleModel)
				REPEAT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_HANGAR_STANDARD iLocation
					IF GET_HANGAR_PA_OUTSIDE_SPAWN_POINT(eHangarID, iLocation, vCoords, fHeading)
						ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
					ENDIF
				ENDREPEAT
			ELSE
				REPEAT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_HANGAR_LARGE iLocation
					IF GET_HANGAR_LARGE_PA_OUTSIDE_SPAWN_POINT(eHangarID, iLocation, vCoords, fHeading)
						ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDREPEAT
	
	iLocation = 0
	
	IF CAN_VEHICLE_MODEL_USE_STANDARD_SPAWN(eVehicleModel)
		REPEAT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_STANDARD iLocation
			IF GET_PERSONAL_AIRCRAFT_SPAWN_LOCATIONS_STANDARD(iLocation, vCoords, fHeading)
				ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
			ENDIF
		ENDREPEAT
	ELSE
		IF IS_AIRCRAFT_MODEL_A_LARGE_AIRCRAFT(eVehicleModel)
			REPEAT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_LARGE iLocation
				IF GET_PERSONAL_AIRCRAFT_SPAWN_LOCATIONS_LARGE(iLocation, vCoords, fHeading)
					ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	iLocation = 0
	
	REPEAT NUMBER_OF_PERSONAL_AIRCRAFT_SPAWNS_ALL iLocation
		IF GET_PERSONAL_AIRCRAFT_SPAWN_LOCATIONS_ALL(iLocation, vCoords, fHeading)
			ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
		ENDIF
	ENDREPEAT
ENDPROC

CONST_INT NUMBER_OF_AVENGER_SPAWNS				100

FUNC BOOL GET_AVENGER_VEHICLE_SPAWN_LOCATIONS(INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	vCoords = <<0.0, 0.0, 0.0>>
	fHeading = 0.0
	
	SWITCH iLocation
		CASE 0		vCoords = <<1434.0710, 2627.0769, 46.7690>>		fHeading = 279.4000	BREAK
		CASE 1		vCoords = <<1359.4120, 2891.0029, 40.5910>>		fHeading = 140.8000	BREAK
		CASE 2		vCoords = <<995.8070, 2993.0681, 39.9910>>		fHeading = 142.9980	BREAK
		CASE 3		vCoords = <<1015.1100, 2727.4800, 38.8570>>		fHeading = 358.9980	BREAK
		CASE 4		vCoords = <<117.3480, 2820.7109, 49.9540>>		fHeading = 307.3990	BREAK
		CASE 5		vCoords = <<248.8420, 2674.8650, 42.8900>>		fHeading = 167.5980	BREAK
		CASE 6		vCoords = <<-544.2230, 2707.9050, 41.5760>>		fHeading = 46.9980	BREAK
		CASE 7		vCoords = <<335.5350, 2126.1450, 102.2960>>		fHeading = 2.1980	BREAK
		CASE 8		vCoords = <<2829.6440, 3651.2051, 49.5040>>		fHeading = 167.3980	BREAK
		CASE 9		vCoords = <<2556.4250, 4252.5078, 40.2350>>		fHeading = 53.3980	BREAK
		CASE 10		vCoords = <<2276.1179, 3805.7739, 34.4970>>		fHeading = 298.5970	BREAK
		CASE 11		vCoords = <<2962.7251, 4066.3960, 54.8480>>		fHeading = 105.7970	BREAK
		CASE 12		vCoords = <<3271.7991, 5023.8242, 21.4060>>		fHeading = 319.5960	BREAK
		CASE 13		vCoords = <<2826.8440, 4787.4668, 47.8710>>		fHeading = 111.3960	BREAK
		CASE 14		vCoords = <<2504.2639, 5641.0371, 45.8170>>		fHeading = 280.1950	BREAK
		CASE 15		vCoords = <<2409.7949, 4810.9922, 35.0490>>		fHeading = 124.7950	BREAK
		CASE 16		vCoords = <<-658.0960, 5858.4912, 17.3540>>		fHeading = 207.7950	BREAK
		CASE 17		vCoords = <<-453.9640, 6106.8169, 29.6010>>		fHeading = 57.3930	BREAK
		CASE 18		vCoords = <<-552.3390, 6252.3242, 8.0730>>		fHeading = 187.9930	BREAK
		CASE 19		vCoords = <<-886.1380, 5615.8071, 3.0790>>		fHeading = 62.7920	BREAK
		CASE 20		vCoords = <<104.5530, 7026.4102, 10.2560>>		fHeading = 4.9920	BREAK
		CASE 21		vCoords = <<483.7740, 6614.2891, 23.3150>>		fHeading = 3.3910	BREAK
		CASE 22		vCoords = <<-149.7590, 6695.1318, 0.2130>>		fHeading = 357.7920	BREAK
		CASE 23		vCoords = <<1532.3979, 6613.3291, 1.3160>>		fHeading = 24.9910	BREAK
		CASE 24		vCoords = <<-2779.5210, 2359.1121, 1.5620>>		fHeading = 42.9900	BREAK
		CASE 25		vCoords = <<-2515.1609, 3688.6311, 11.6350>>	fHeading = 14.9890	BREAK
		CASE 26		vCoords = <<-1500.8260, 2014.9771, 65.0110>>	fHeading = 297.3890	BREAK
		CASE 27		vCoords = <<-1951.4780, 2650.7891, 1.8840>>		fHeading = 158.9880	BREAK
		CASE 28		vCoords = <<-84.0840, 3633.8030, 44.7800>>		fHeading = 350.5880	BREAK
		CASE 29		vCoords = <<525.9930, 3553.8379, 31.8490>>		fHeading = 352.5880	BREAK
		CASE 30		vCoords = <<283.7420, 3113.0959, 41.4230>>		fHeading = 182.7880	BREAK
		CASE 31		vCoords = <<1305.0540, 3591.2690, 33.2220>>		fHeading = 69.5880	BREAK
		CASE 32		vCoords = <<2518.2090, 1516.8101, 30.7230>>		fHeading = 268.6000	BREAK
		CASE 33		vCoords = <<2161.6880, 2645.7009, 52.4830>>		fHeading = 76.1990	BREAK
		CASE 34		vCoords = <<2060.5181, 1542.2629, 74.7900>>		fHeading = 293.3980	BREAK
		CASE 35		vCoords = <<1524.9740, 2007.4640, 98.4400>>		fHeading = 260.5970	BREAK
		CASE 36		vCoords = <<1054.0890, -186.7010, 69.0690>>		fHeading = 330.9960	BREAK
		CASE 37		vCoords = <<2561.3870, 836.9340, 87.0020>>		fHeading = 353.9960	BREAK
		CASE 38		vCoords = <<2294.3950, -662.1430, 74.5860>>		fHeading = 183.7960	BREAK
		CASE 39		vCoords = <<1384.5260, 736.0110, 78.3320>>		fHeading = 10.1960	BREAK
		CASE 40		vCoords = <<1060.7190, 4257.3120, 36.7010>>		fHeading = 206.5960	BREAK
		CASE 41		vCoords = <<-1604.1270, 4793.1870, 51.8220>>	fHeading = 123.1950	BREAK
		CASE 42		vCoords = <<1704.9399, 4548.2158, 38.6180>>		fHeading = 179.9940	BREAK
		CASE 43		vCoords = <<1953.5870, 4935.4990, 43.9130>>		fHeading = 313.7930	BREAK
		CASE 44		vCoords = <<2121.8069, 3241.7429, 45.4640>>		fHeading = 170.1930	BREAK
		CASE 45		vCoords = <<931.5170, 2147.0110, 51.0470>>		fHeading = 303.1930	BREAK
		CASE 46		vCoords = <<-1600.5160, 2955.2329, 32.0520>>	fHeading = 230.3930	BREAK
		CASE 47		vCoords = <<1927.8130, 3670.2510, 32.1420>>		fHeading = 204.5930	BREAK
		CASE 48		vCoords = <<-3010.6350, 2095.5530, 39.0650>>	fHeading = 140.7930	BREAK
		CASE 49		vCoords = <<-3141.2800, 1187.6429, 19.1520>>	fHeading = 86.1920	BREAK
		CASE 50		vCoords = <<-2432.6130, -318.9950, 2.9390>>		fHeading = 227.3910	BREAK
		CASE 51		vCoords = <<-457.6170, 1146.9380, 324.9040>>	fHeading = 129.5910	BREAK
		CASE 52		vCoords = <<335.0160, 842.1930, 193.3170>>		fHeading = 69.7910	BREAK
		CASE 53		vCoords = <<-1245.4730, 840.5900, 191.5630>>	fHeading = 245.7900	BREAK
		CASE 54		vCoords = <<469.2660, 1073.2321, 232.0310>>		fHeading = 174.7880	BREAK
		CASE 55		vCoords = <<-961.3770, 298.2630, 68.8910>>		fHeading = 230.5880	BREAK
		CASE 56		vCoords = <<-1735.0350, 137.3630, 63.3710>>		fHeading = 170.5880	BREAK
		CASE 57		vCoords = <<-1755.0070, -765.2180, 8.6350>>		fHeading = 140.7870	BREAK
		CASE 58		vCoords = <<-1453.0551, -1118.5930, 2.1670>>	fHeading = 51.1870	BREAK
		CASE 59		vCoords = <<-1143.9821, -1695.3290, 3.3600>>	fHeading = 301.3870	BREAK
		CASE 60		vCoords = <<-1005.3620, -2378.0229, 12.9450>>	fHeading = 269.7860	BREAK
		CASE 61		vCoords = <<-1754.8409, -2775.9080, 12.9450>>	fHeading = 287.9860	BREAK
		CASE 62		vCoords = <<-1059.2030, -2915.9109, 12.9500>>	fHeading = 287.9860	BREAK
		CASE 63		vCoords = <<819.2090, 233.2960, 82.6330>>		fHeading = 31.3860	BREAK
		CASE 64		vCoords = <<-502.9020, -2757.6990, 5.0000>>		fHeading = 135.3990	BREAK
		CASE 65		vCoords = <<287.8600, -2436.8589, 7.0420>>		fHeading = 353.7990	BREAK
		CASE 66		vCoords = <<264.1010, -2829.1780, 5.0210>>		fHeading = 0.3990	BREAK
		CASE 67		vCoords = <<806.8470, -3039.0830, 4.7420>>		fHeading = 93.3970	BREAK
		CASE 68		vCoords = <<1482.0580, -2435.8669, 65.2090>>	fHeading = 262.7970	BREAK
		CASE 69		vCoords = <<1676.3101, -1559.4320, 111.5830>>	fHeading = 280.7980	BREAK
		CASE 70		vCoords = <<-709.5500, -1653.6980, 23.5580>>	fHeading = 336.1970	BREAK
		CASE 71		vCoords = <<-1667.7650, -859.1910, 8.0130>>		fHeading = 50.3970	BREAK
		CASE 72		vCoords = <<-279.0400, -1655.3030, 30.8490>>	fHeading = 57.9950	BREAK
		CASE 73		vCoords = <<195.9210, -2097.3230, 16.6910>>		fHeading = 121.5960	BREAK
		//CASE 74		vCoords = <<129.0830, -143.5910, 65.1270>>		fHeading = 69.1960	BREAK // unaccessible rooftop. 4183088
		CASE 74		vCoords = <<11.0130, -1080.4700, 37.1520>>		fHeading = 249.7930	BREAK
		CASE 75		vCoords = <<1486.3090, -1967.5970, 69.8360>>	fHeading = 48.9970	BREAK
		CASE 76		vCoords = <<-1312.2810, 36.4680, 52.0970>>		fHeading = 274.5960	BREAK
		CASE 77		vCoords = <<-603.9720, -2333.4580, 12.8280>>	fHeading = 229.1960	BREAK
		CASE 78		vCoords = <<296.4430, -1450.7020, 45.5100>>		fHeading = 229.7950	BREAK
		CASE 79		vCoords = <<-763.4630, -1452.8210, 4.0010>>		fHeading = 51.1950	BREAK
		CASE 80		vCoords = <<1012.7690, -2333.6951, 29.5100>>	fHeading = 173.9940	BREAK
		CASE 81		vCoords = <<404.7730, -643.8460, 27.5000>>		fHeading = 90.5930	BREAK
		CASE 82		vCoords = <<486.0400, -40.6510, 87.8570>>		fHeading = 330.1930	BREAK
		CASE 83		vCoords = <<-394.9750, -78.4260, 53.4280>>		fHeading = 125.7930	BREAK
		CASE 84		vCoords = <<661.2150, -1586.6949, 8.7090>>		fHeading = 195.9920	BREAK
		CASE 85		vCoords = <<-1057.5200, -686.8160, 21.5840>>	fHeading = 95.7920	BREAK
		CASE 86		vCoords = <<120.0360, -1070.0270, 28.1920>>		fHeading = 270.1390	BREAK
		CASE 87		vCoords = <<1105.7939, -875.0780, 49.3010>>		fHeading = 147.3380	BREAK
		CASE 88		vCoords = <<-914.5863, -789.9854, 15.5407>>		fHeading = 185.9363	BREAK
		CASE 89		vCoords = <<-2050.6211, -449.3880, 10.4090>>	fHeading = 49.9370	BREAK
		CASE 90		vCoords = <<612.8830, -959.1300, 9.6010>>		fHeading = 180.1370	BREAK
		CASE 91		vCoords = <<-1225.9600, -377.9650, 58.2870>>	fHeading = 118.1370	BREAK
		CASE 92		vCoords = <<-95.3570, -458.1720, 34.3350>>		fHeading = 101.5370	BREAK
		CASE 93		vCoords = <<939.6270, -551.7190, 58.4720>>		fHeading = 117.1360	BREAK
		CASE 94		vCoords = <<-587.3490, -32.8900, 42.4690>>		fHeading = 127.4850	BREAK
		CASE 95		vCoords = <<1910.2755, 426.4446, 161.9082>>		fHeading = 292.5952	BREAK
		CASE 96		vCoords = <<-1357.7791, -3088.1350, 13.3050>>	fHeading = 330.8000	BREAK
		CASE 97		vCoords = <<-1616.6331, -3023.4441, 13.1470>>	fHeading = 287.5990	BREAK
		CASE 98		vCoords = <<-934.0860, -3442.6111, 13.1460>>	fHeading = 8.7990	BREAK
		CASE 99		vCoords = <<-1323.3643, -2834.5898, 12.9449>>	fHeading = 329.2000	BREAK
		CASE NUMBER_OF_AVENGER_SPAWNS							RETURN FALSE
	ENDSWITCH
	
	RETURN NOT IS_VECTOR_ZERO(vCoords)
ENDFUNC

PROC ADD_CUSTOM_NODES_FOR_ALL_AVENGER_SPAWNS()
	PRINTLN("ADD_CUSTOM_NODES_FOR_ALL_AVENGER_SPAWNS - called")
	DEBUG_PRINTCALLSTACK()
	
	INT iLocation
	VECTOR vCoords
	FLOAT fHeading
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	
	iLocation = 0
	
	REPEAT NUMBER_OF_AVENGER_SPAWNS iLocation
		IF GET_AVENGER_VEHICLE_SPAWN_LOCATIONS(iLocation, vCoords, fHeading)
			ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
		ENDIF
	ENDREPEAT
ENDPROC

CONST_INT NUMBER_OF_HACKERTRUCK_SPAWNS 128

FUNC BOOL GET_HACKERTRUCK_VEHICLE_SPAWN_LOCATIONS(INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	vCoords = <<0,0,0>>		fHeading = 0.0	
	SWITCH iLocation
		CASE 0	vCoords = <<771.3560, -1404.8390, 25.5550>>		fHeading = 269.9990	BREAK	// 
		CASE 1	vCoords = <<415.4690, -1561.1770, 28.1480>>		fHeading = 139.4000	BREAK	// 
		CASE 2	vCoords = <<511.8010, -1124.3790, 28.2920>>		fHeading = 179.7990	BREAK	// 
		CASE 3	vCoords = <<862.5207, -1659.0712, 28.7015>>		fHeading = 86.3980  BREAK	// 
		CASE 4	vCoords = <<104.7510, -1070.0240, 28.1920>>		fHeading = 67.9980	BREAK	// 
		CASE 5	vCoords = <<429.1920, -646.4200, 27.5000>>		fHeading = 180.3970	BREAK	// 
		CASE 6	vCoords = <<976.2440, -1036.8890, 39.8770>>		fHeading = 269.9970	BREAK	// 
		CASE 7	vCoords = <<44.9340, -605.1610, 30.6290>>		fHeading = 250.3970	BREAK	// 
		CASE 8	vCoords = <<-180.2660, -1506.2050, 31.6360>>	fHeading = 319.1960	BREAK	// 
		CASE 9	vCoords = <<30.1810, -1714.6990, 28.3030>>		fHeading = 293.1950	BREAK	// 
		CASE 10	vCoords = <<-319.7010, -1102.7280, 22.0260>>	fHeading = 249.7940	BREAK	// 
		CASE 11	vCoords = <<105.5288, -1412.5883, 28.2321>>		fHeading = 228.9940 BREAK	// 
		CASE 12	vCoords = <<-169.2030, -56.4500, 51.8150>>		fHeading = 160.5920	BREAK	// 
		CASE 13	vCoords = <<-517.0560, 273.6090, 82.0810>>		fHeading = 174.1920	BREAK	// 
		CASE 14	vCoords = <<202.7911, 383.8754, 106.5120>>		fHeading = 259.3918 BREAK	// 
		CASE 15	vCoords = <<-529.2789, -322.2184, 34.2076>>		fHeading = 29.9920  BREAK	// 
		CASE 16	vCoords = <<738.7450, -2124.1990, 28.2870>>		fHeading = 266.1920	BREAK	// 
		CASE 17	vCoords = <<1294.1110, -1980.7190, 43.0550>>	fHeading = 133.3910	BREAK	// 
		CASE 18	vCoords = <<983.1700, -2450.6650, 27.5400>>		fHeading = 174.7910	BREAK	// 
		CASE 19	vCoords = <<1296.5590, -1728.0870, 52.7980>>	fHeading = 115.7910	BREAK	// 
		CASE 20	vCoords = <<-626.6560, -2319.0620, 12.8280>>	fHeading = 138.9900	BREAK	// 
		CASE 21	vCoords = <<-988.8550, -2698.4600, 12.8310>>	fHeading = 60.5900	BREAK	// 
		CASE 22	vCoords = <<-986.1970, -2090.6680, 9.8200>>		fHeading = 134.5900	BREAK	// 
		CASE 23	vCoords = <<-905.8060, -2398.6111, 12.9440>>	fHeading = 148.9900	BREAK	// 
		CASE 24	vCoords = <<275.9260, -3148.3850, 4.7900>>		fHeading = 0.7890	BREAK	// 
		CASE 25	vCoords = <<233.0590, -2964.0559, 4.8470>>		fHeading = 90.7890	BREAK	// 
		CASE 26	vCoords = <<276.2840, -2640.0161, 5.0010>>		fHeading = 88.1890	BREAK	// 
		CASE 27	vCoords = <<-19.7070, -2662.1431, 5.0040>>		fHeading = 358.3890	BREAK	// 
		CASE 28	vCoords = <<-387.0840, -2798.6311, 5.0000>>		fHeading = 355.7890	BREAK	// 
		CASE 29	vCoords = <<-124.8910, -2419.0430, 5.0000>>		fHeading = 180.3890	BREAK	// 
		CASE 30	vCoords = <<593.0050, -2714.5610, 5.0570>>		fHeading = 360.1880	BREAK	// 
		CASE 31	vCoords = <<529.2400, -2934.1121, 5.0440>>		fHeading = 269.7880	BREAK	// 
		CASE 32	vCoords = <<808.0260, -3126.9761, 4.8980>>		fHeading = 1.4000	BREAK	// 
		CASE 33	vCoords = <<1107.4600, -3229.1399, 4.8970>>		fHeading = 0.9990	BREAK	// 
		CASE 34	vCoords = <<1115.5090, -2943.7720, 4.9010>>		fHeading = 89.3990	BREAK	// 
		CASE 35	vCoords = <<628.7070, 74.6910, 88.7470>>		fHeading = 159.5990	BREAK	// 
		CASE 36	vCoords = <<917.3540, -263.9650, 67.3530>>		fHeading = 54.3980	BREAK	// 
		CASE 37	vCoords = <<383.9740, -81.0480, 66.5090>>		fHeading = 250.3980	BREAK	// 
		CASE 38	vCoords = <<978.7190, 175.6850, 79.8310>>		fHeading = 140.1980	BREAK	// 
		CASE 39	vCoords = <<-1461.3149, -583.9260, 29.9390>>	fHeading = 215.3980	BREAK	// 
		CASE 40	vCoords = <<-1123.8870, -317.9200, 36.6730>>	fHeading = 85.3970	BREAK	// 
		CASE 41	vCoords = <<-841.1420, -752.7510, 21.7450>>		fHeading = 89.5970	BREAK	// 
		CASE 42	vCoords = <<-1396.2220, -1025.7460, 3.3950>>	fHeading = 304.9960	BREAK	// 
		CASE 43	vCoords = <<-1111.8459, -1727.0310, 3.1800>>	fHeading = 304.7960	BREAK	// 
		CASE 44	vCoords = <<-780.0440, -1286.9380, 4.0000>>		fHeading = 260.9950	BREAK	// 
		CASE 45	vCoords = <<-1020.8240, -1127.5660, 1.0970>>	fHeading = 299.9940	BREAK	// 
		CASE 46	vCoords = <<-1265.7140, -1359.0690, 3.2080>>	fHeading = 199.9950	BREAK	// 
		CASE 47	vCoords = <<-510.2010, -970.4140, 22.5780>>		fHeading = 87.9940	BREAK	// 
		CASE 48	vCoords = <<-648.8180, -1723.6490, 23.5720>>	fHeading = 2.7940	BREAK	// 
		CASE 49	vCoords = <<-112.2180, -2012.8710, 17.0170>>	fHeading = 81.9930	BREAK	// 
		CASE 50	vCoords = <<461.8700, -1948.9130, 23.6280>>		fHeading = 25.1930	BREAK	// 
		CASE 51	vCoords = <<1133.7380, -1459.1610, 33.6800>>	fHeading = 270.5920	BREAK	// 
		CASE 52	vCoords = <<1459.0830, -2593.9651, 47.5150>>	fHeading = 344.9910	BREAK	// 
		CASE 53	vCoords = <<1682.0750, -1554.2700, 111.6410>>	fHeading = 71.1910	BREAK	// 
		CASE 54	vCoords = <<1301.8510, -714.4310, 63.7620>>		fHeading = 63.1910	BREAK	// 
		CASE 55	vCoords = <<784.1310, -728.8300, 26.8690>>		fHeading = 0.9910	BREAK	// 
		CASE 56	vCoords = <<-1766.5145, -507.1015, 37.8289>>	fHeading = 296.9910 BREAK	// 
		CASE 57	vCoords = <<-1388.8199, 7.4140, 52.2720>>		fHeading = 11.5900	BREAK	// 
		CASE 58	vCoords = <<1899.6980, -1014.4830, 77.8420>>	fHeading = 325.7900	BREAK	// 
		CASE 59	vCoords = <<763.9120, 605.5650, 124.7810>>		fHeading = 249.5890	BREAK	// 
		CASE 60	vCoords = <<1541.3270, 860.5400, 76.4520>>		fHeading = 331.5880	BREAK	// 
		CASE 61	vCoords = <<2569.2490, 334.9490, 107.4550>>		fHeading = 90.7880	BREAK	// 
		CASE 62	vCoords = <<-1612.2450, -968.4910, 12.0170>>	fHeading = 319.7880	BREAK	// 
		CASE 63	vCoords = <<-1917.1801, 332.9550, 88.0370>>		fHeading = 97.7870	BREAK	// 
		CASE 64	vCoords = <<599.6830, 775.6720, 201.9950>>		fHeading = 255.8000	BREAK	// 
		CASE 65	vCoords = <<239.7780, 1160.1230, 224.4600>>		fHeading = 13.9990	BREAK	// 
		CASE 66	vCoords = <<-413.7300, 1196.9080, 324.6420>>	fHeading = 164.9980	BREAK	// 
		CASE 67	vCoords = <<-1439.3220, 454.2680, 110.3880>>	fHeading = 336.3970	BREAK	// 
		CASE 68	vCoords = <<-3003.6560, 86.1790, 10.6080>>		fHeading = 59.7970	BREAK	// 
		CASE 69	vCoords = <<-3140.8877, 1146.6362, 19.8277>>	fHeading = 246.5959 BREAK	// 
		CASE 70	vCoords = <<-2579.1660, 2319.4250, 32.0590>>	fHeading = 97.3960	BREAK	// 
		CASE 71	vCoords = <<-1886.2690, 2017.6130, 139.8590>>	fHeading = 167.7950	BREAK	// 
		CASE 72	vCoords = <<1299.3080, 1419.4860, 99.6010>>		fHeading = 356.9440	BREAK	// 
		CASE 73	vCoords = <<2677.5520, 1525.8040, 23.5070>>		fHeading = 359.9440	BREAK	// 
		CASE 74	vCoords = <<-70.7290, 2780.2700, 55.6560>>		fHeading = 272.7440	BREAK	// 
		CASE 75	vCoords = <<797.9260, 2202.5471, 50.6720>>		fHeading = 66.5430	BREAK	// 
		CASE 76	vCoords = <<2550.1750, 2636.6890, 36.9470>>		fHeading = 269.3430	BREAK	// 
		CASE 77	vCoords = <<1234.8199, 2700.9080, 37.0060>>		fHeading = 269.5430	BREAK	// 
		CASE 78	vCoords = <<2003.4250, 3123.2461, 45.8190>>		fHeading = 142.5420	BREAK	// 
		CASE 79	vCoords = <<-1555.8510, 1385.6219, 125.3030>>	fHeading = 314.1420	BREAK	// 
		CASE 80	vCoords = <<-1478.3860, 2786.3450, 19.7090>>	fHeading = 286.5410	BREAK	// 
		CASE 81	vCoords = <<2065.1470, 3845.7151, 32.8690>>		fHeading = 119.1410	BREAK	// 
		CASE 82	vCoords = <<1569.1060, 3700.6299, 33.3680>>		fHeading = 294.7400	BREAK	// 
		CASE 83	vCoords = <<2750.9180, 3444.1321, 55.1020>>		fHeading = 157.3400	BREAK	// 
		CASE 84	vCoords = <<341.0390, 3579.4500, 32.3440>>		fHeading = 159.1308	BREAK	// 
		CASE 85	vCoords = <<1106.5380, 3389.1211, 33.4290>>		fHeading = 6.5380	BREAK	// 
		CASE 86	vCoords = <<416.7040, 3090.2830, 41.9130>>		fHeading = 87.1380	BREAK	// 
		CASE 87	vCoords = <<2713.7810, 4366.7461, 46.0200>>		fHeading = 339.9380	BREAK	// 
		CASE 88	vCoords = <<2372.2451, 4920.4048, 41.0210>>		fHeading = 135.3380	BREAK	// 
		CASE 89	vCoords = <<1702.0970, 4798.6938, 40.8310>>		fHeading = 90.1360	BREAK	// 
		CASE 90	vCoords = <<1377.9611, 4376.1060, 43.0790>>		fHeading = 272.3360	BREAK	// 
		CASE 91	vCoords = <<346.4620, 4468.4609, 61.5590>>		fHeading = 6.9360	BREAK	// 
		CASE 92	vCoords = <<3261.1550, 5163.9429, 18.6870>>		fHeading = 172.9350	BREAK	// 
		CASE 93	vCoords = <<-2229.1909, 4319.0791, 47.4170>>	fHeading = 152.3350	BREAK	// 
		CASE 94	vCoords = <<-66.3900, 1886.0870, 195.5870>>		fHeading = 174.1350	BREAK	// 
		CASE 95	vCoords = <<-271.1700, 3948.2549, 41.6160>>		fHeading = 218.3350	BREAK	// 
		CASE 96	vCoords = <<-1069.9659, 4367.1152, 12.0670>>	fHeading = 256.2000	BREAK	// 
		CASE 97	vCoords = <<2989.1240, 3507.5171, 70.3820>>		fHeading = 298.3990	BREAK	// 
		CASE 98	vCoords = <<2598.1060, 5304.6201, 43.5430>>		fHeading = 14.9990	BREAK	// 
		CASE 99	vCoords = <<1678.1570, 6404.1782, 30.0640>>		fHeading = 75.1980	BREAK	// 
		CASE 100 vCoords = <<1343.9850, 6515.7568, 18.5500>>	fHeading = 272.5980	BREAK	// 
		CASE 101 vCoords = <<420.3680, 6585.7998, 26.0940>>		fHeading = 177.5980	BREAK	// 
		CASE 102 vCoords = <<22.8070, 6658.2881, 30.5160>>		fHeading = 182.5980	BREAK	// 
		CASE 103 vCoords = <<-252.5000, 6220.0811, 30.4890>>	fHeading = 135.3970	BREAK	// 
		CASE 104 vCoords = <<-709.2730, 5773.9072, 16.3310>>	fHeading = 64.5970	BREAK	// 
		CASE 105 vCoords = <<-817.9080, 5411.1670, 33.1060>>	fHeading = 86.5960	BREAK	// 
		CASE 106 vCoords = <<-1507.2469, 5020.7671, 61.5480>>	fHeading = 316.7960	BREAK	// 
		CASE 107 vCoords = <<-2529.1860, 3638.1201, 11.6560>>	fHeading = 335.3950	BREAK	// 
		CASE 108 vCoords = <<3702.4399, 4444.5630, 21.2124>>	fHeading = 26.7994  BREAK	// 
		CASE 109 vCoords = <<3512.2344, 3766.3777, 28.9057>>	fHeading = 80.9993  BREAK	// 
		CASE 110 vCoords = <<549.4282, 2623.5127, 41.8136>>		fHeading = 9.1992   BREAK	// 
		CASE 111 vCoords = <<1509.9009, 2017.7638, 97.9527>>	fHeading = 177.9990 BREAK	// 
		CASE 112 vCoords = <<2368.9138, 2417.8459, 61.2132>>	fHeading = 233.1991 BREAK	// 
		CASE 113 vCoords = <<-738.6634, 2290.9121, 73.1389>>	fHeading = 38.9988  BREAK	// 
		CASE 114 vCoords = <<-2468.2351, 1049.1121, 190.7591>>	fHeading = 78.3987  BREAK	// 
		CASE 115 vCoords = <<-477.9336, 2999.4419, 25.7750>>	fHeading = 150.9983 BREAK	// 
		CASE 116 vCoords = <<-1170.6553, 927.5237, 195.8715>>	fHeading = 86.1980  BREAK	// 
		CASE 117 vCoords = <<718.3862, 1720.6819, 182.1247>>	fHeading = 69.9977  BREAK	// 
		CASE 118 vCoords = <<-2362.2700, 283.4155, 167.1771>>	fHeading = 24.1976  BREAK	// 
		CASE 119 vCoords = <<160.8658, 774.8909, 208.5414>>		fHeading = 315.5970 BREAK	// 
		CASE 120 vCoords = <<-1013.2085, 200.9232, 64.7565>>	fHeading = 268.5967 BREAK	// 
		CASE 121 vCoords = <<1676.8130, -1952.3281, 111.4110>>	fHeading = 82.5966  BREAK	// 
		CASE 122 vCoords = <<-324.6476, -697.6640, 31.9862>>	fHeading = 0.5964   BREAK	// 
		CASE 123 vCoords = <<191.2062, 60.2289, 82.6153>>		fHeading = 69.5963  BREAK	// 
		CASE 124 vCoords = <<290.9829, -341.0181, 43.9199>>		fHeading = 160.7961 BREAK	// 
		CASE 125 vCoords = <<1936.2963, 598.1614, 174.9315>>	fHeading = 328.9958 BREAK	// 
		CASE 126 vCoords = <<-986.8098, 2742.7090, 24.0853>>	fHeading = 288.3954 BREAK	// 
		CASE 127 vCoords = <<1090.1060, 3590.0398, 32.3631>>	fHeading = 360.1952 BREAK	// 
		
		CASE NUMBER_OF_HACKERTRUCK_SPAWNS						RETURN FALSE
	ENDSWITCH
	RETURN NOT IS_VECTOR_ZERO(vCoords)
ENDFUNC

PROC ADD_CUSTOM_NODES_FOR_ALL_HACKERTRUCK_SPAWNS()
	
	PRINTLN("ADD_CUSTOM_NODES_FOR_ALL_HACKERTRUCK_SPAWNS - called")
	DEBUG_PRINTCALLSTACK()
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	INT iLocation
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vCoords, vCoords1
	FLOAT fHeading, fHeading1
	
	INT iOrderedList[NUMBER_OF_HACKERTRUCK_SPAWNS]
	REPEAT NUMBER_OF_HACKERTRUCK_SPAWNS iLocation
		iOrderedList[iLocation] = iLocation
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_NET_AMBIENT, "ADD_CUSTOM_NODES_FOR_ALL_HACKERTRUCK_SPAWNS begin ordering list")
	BOOL bDone = FALSE
	WHILE (!bDone) 
		bDone = TRUE
		FOR iLocation = 0 TO (NUMBER_OF_HACKERTRUCK_SPAWNS-2)
			IF GET_HACKERTRUCK_VEHICLE_SPAWN_LOCATIONS(iOrderedList[iLocation], vCoords, fHeading)
			AND GET_HACKERTRUCK_VEHICLE_SPAWN_LOCATIONS(iOrderedList[iLocation+1], vCoords1, fHeading1)
				IF (VDIST2(vPlayerPos, vCoords) > VDIST2(vPlayerPos, vCoords1))
					CDEBUG1LN(DEBUG_NET_AMBIENT, "    ADD_CUSTOM_NODES_FOR_ALL_HACKERTRUCK_SPAWNS iOrderedList[", iLocation, "] ", VDIST(vPlayerPos, vCoords), " with iOrderedList[", iLocation+1, "] ", VDIST(vPlayerPos, vCoords1))
					
					INT iTempLoc = iOrderedList[iLocation]
					iOrderedList[iLocation] = iOrderedList[iLocation+1]
					iOrderedList[iLocation+1] = iTempLoc
					
					bDone = FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDWHILE
	CDEBUG1LN(DEBUG_NET_AMBIENT, "ADD_CUSTOM_NODES_FOR_ALL_HACKERTRUCK_SPAWNS finished ordering list")

	REPEAT MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES iLocation
		IF GET_HACKERTRUCK_VEHICLE_SPAWN_LOCATIONS(iOrderedList[iLocation], vCoords, fHeading)
			ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
		ENDIF
	ENDREPEAT
	
ENDPROC

#IF FEATURE_HEIST_ISLAND


FUNC BOOL GET_SUBMARINE_VEHICLE_SPAWN_CLUSTER_LOCATION(INT iCluster, INT iLocation, VECTOR &vCoords, FLOAT &fHeading)
	ASSERTLN(iCluster < NUMBER_OF_SUBMARINE_SPAWNS / SUBMARINE_SPAWN_CLUSTER_SIZE)
	ASSERTLN(iLocation < NUMBER_OF_SUBMARINE_SPAWNS)
	INT iActualLocation = (iCluster * SUBMARINE_SPAWN_CLUSTER_SIZE) + iLocation
	RETURN GET_SUBMARINE_VEHICLE_SPAWN_LOCATIONS(iActualLocation, vCoords, fHeading)
ENDFUNC

PROC ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_SPAWNS()
	
	PRINTLN("ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_SPAWNS - called")
	DEBUG_PRINTCALLSTACK()
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	
	INT iLocation
	FLOAT fHeading
	VECTOR vCoords
	// we dont need to oreder the vehicle nodes, this gets done in net_nodes.sch
	REPEAT NUMBER_OF_SUBMARINE_SPAWNS iLocation
		IF GET_SUBMARINE_VEHICLE_SPAWN_LOCATIONS(iLocation, vCoords, fHeading)
			//Lower spawn point under the water
			vCoords.z = SUBMARINE_SPAWN_DEPTH
			ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
		ENDIF
	ENDREPEAT
	
ENDPROC



//Try to add iCount water positions fRadius from the player
PROC TRY_ADD_CUSTOM_NODES_FOR_WATER_SPAWNS_NEARBY(INT iCount, FLOAT fRadius)

	PRINTLN("[TRY_ADD_CUSTOM_NODES_FOR_WATER_SPAWNS_NEARBY] - called (iCount=", iCount, ", fRadius=", fRadius)
	DEBUG_PRINTCALLSTACK()

	//Add random locations nearby
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	FLOAT fThetaStep = (360) / TO_FLOAT(iCount)
	FLOAT fTheta = GET_RANDOM_FLOAT_IN_RANGE(0, 360)
	INT iLocation = 0
	REPEAT iCount iLocation
		
		fTheta += fThetaStep
		VECTOR NearbyLocation = vPlayerPos + <<fRadius*COS(fTheta), fRadius*SIN(fTheta), 9999.9.0>>
		
		PRINTLN("[TRY_ADD_CUSTOM_NODES_FOR_WATER_SPAWNS_NEARBY] Testing fTheta=", fTheta)
		
		//Validate if location is on the water
		IF GET_WATER_HEIGHT(NearbyLocation, NearbyLocation.Z)
			PRINTLN("[TRY_ADD_CUSTOM_NODES_FOR_WATER_SPAWNS_NEARBY] Found valid location: ", NearbyLocation)
			ADD_CUSTOM_VEHICLE_NODE(NearbyLocation, GET_RANDOM_FLOAT_IN_RANGE(0,360))
		ENDIF
		
	ENDREPEAT
ENDPROC

PROC ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS()
	
	PRINTLN("[ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS] - called")
	DEBUG_PRINTCALLSTACK()
	
	CLEAR_CUSTOM_VEHICLE_NODES()
	
	TRY_ADD_CUSTOM_NODES_FOR_WATER_SPAWNS_NEARBY(8, 20)
	TRY_ADD_CUSTOM_NODES_FOR_WATER_SPAWNS_NEARBY(8, 40)
	
	INT iLocation
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	VECTOR vCoords, vCoords1
	FLOAT fHeading, fHeading1
	
	//Build fixed locations list
	INT iOrderedList[NUMBER_OF_SUBMARINE_DINGHY_SPAWNS]
	REPEAT NUMBER_OF_SUBMARINE_DINGHY_SPAWNS iLocation
		iOrderedList[iLocation] = iLocation
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_NET_AMBIENT, "[ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS] begin ordering list")
	BOOL bDone = FALSE
	WHILE (!bDone) 
		bDone = TRUE
		FOR iLocation = 0 TO (NUMBER_OF_SUBMARINE_DINGHY_SPAWNS-2)
			IF GET_SUBMARINE_DINGHY_VEHICLE_SPAWN_LOCATIONS(iOrderedList[iLocation], vCoords, fHeading)
			AND GET_SUBMARINE_DINGHY_VEHICLE_SPAWN_LOCATIONS(iOrderedList[iLocation+1], vCoords1, fHeading1)
				IF (VDIST2(vPlayerPos, vCoords) > VDIST2(vPlayerPos, vCoords1))
					CDEBUG1LN(DEBUG_NET_AMBIENT, "    [ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS] iOrderedList[", iLocation, "] ", VDIST(vPlayerPos, vCoords), " with iOrderedList[", iLocation+1, "] ", VDIST(vPlayerPos, vCoords1))
					
					INT iTempLoc = iOrderedList[iLocation]
					iOrderedList[iLocation] = iOrderedList[iLocation+1]
					iOrderedList[iLocation+1] = iTempLoc
					
					bDone = FALSE
				ENDIF
			ENDIF
		ENDFOR
	ENDWHILE
	CDEBUG1LN(DEBUG_NET_AMBIENT, "[ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS] finished ordering list")
	
	INT iRemainingSpawnLocations = MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES - g_SpawnData.iNumberOfCustomVehicleNodes
	REPEAT iRemainingSpawnLocations iLocation
		IF GET_SUBMARINE_DINGHY_VEHICLE_SPAWN_LOCATIONS(iOrderedList[iLocation], vCoords, fHeading)
			ADD_CUSTOM_VEHICLE_NODE(vCoords, fHeading)
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

/// PURPOSE:
///    Finds coords for the near the player who launched the Joyrider.
/// RETURNS:
///    BOOL - TRUE if found cords, FALSE if not. 
FUNC BOOL GET_CLOSE_VEHICLE_NODE(VECTOR vSearchPos, VECTOR &vLocation, INT &iNode)
	
		IF MPGlobalsAmbience.bYachtPersonalVehicleRequest
			INT iYacht = LOCAL_PLAYER_PRIVATE_YACHT_ID()
			IF NOT (iYacht = -1)
				FLOAT fHead
				GET_PRIVATE_YACHT_PERSONAL_VEHICLE_SPAWN_LOCATION(iYacht, vLocation, fHead)
				NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE returning yacht vehicl spawn coords. ") NET_PRINT_VECTOR(vLocation) NET_NL()
				RETURN TRUE	
			ENDIF
		ENDIF
	
		IF MPGlobalsAmbience.bLaunchVehicleDropTruck
			
			RETURN TRUE
		
//			INT iLocation
//			FLOAT fHead
//			GET_PRIVATE_TRUCK_PERSONAL_VEHICLE_SPAWN_LOCATION(iLocation, vLocation, fHead)
//			
//			IF IS_VECTOR_ZERO(vLocation)
//				iNode++
//				NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE FAILED - iNode = ") NET_PRINT_INT(iNode) NET_PRINT(", vSearchPos:") NET_PRINT_VECTOR(vSearchPos) NET_NL()
//				RETURN FALSE
//			ELSE
//				NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE returning truck spawn coords. ") NET_PRINT_VECTOR(vLocation) NET_PRINT(", location:") NET_PRINT_INT(iLocation) NET_PRINT(", vSearchPos:") NET_PRINT_VECTOR(vSearchPos) NET_PRINT(", fTravelDistance:") NET_PRINT_FLOAT(CALCULATE_TRAVEL_DISTANCE_BETWEEN_POINTS(vSearchPos, vLocation)) NET_NL()
//				RETURN TRUE
//			ENDIF
		ENDIF
	
	IF MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsAmbience.bLaunchVehicleDropAvenger 
		RETURN TRUE
	ENDIF
	
	IF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
	#IF FEATURE_DLC_2_2022
	OR MPGlobalsAmbience.bLaunchVehicleDropAcidLab
	#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	IF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_LEAVING_SIMPLE_INTERIOR_USING_THE_VALET()
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_TUNER
	IF HAS_PLAYER_STARTED_WARP_INTO_CAR_MEET_CAR_PARK()
	OR HAS_PLAYER_STARTED_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF WILL_POINT_USE_CUSTOM_VEHICLE_NODES(vSearchPos)		
		vLocation = vSearchPos
		PRINTLN("GET_CLOSE_VEHICLE_NODE - this search pos will use custom vehicle nodes, using ", vSearchPos)
		RETURN TRUE
	ENDIF	
	
	// mirror SP taxi creation where you get the closest node to the player for using GENERATE_VEHICLE_CREATION_POS_FROM_PATHS
	FLOAT fZ_MeasureMulti = 100.0	// increased as per B*1183689
	FLOAT fZ_Tolerance = 2.5		// B*1430520
	
	// always search switched off nodes, but if we are in the city ensure they aren't dead ends (stop taxi using driveways).
	NODE_FLAGS nodeFlags = NF_IGNORE_SLIPLANES|NF_INCLUDE_SWITCHED_OFF_NODES
	IF NOT IS_POSITION_IN_ZONE_SAFE_FOR_DEADEND_NODES_VD(vSearchPos)
		nodeFlags = NF_IGNORE_SLIPLANES|NF_INCLUDE_SWITCHED_OFF_NODES|NF_IGNORE_SWITCHED_OFF_DEADENDS
		NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE using node flags NF_IGNORE_SLIPLANES|NF_INCLUDE_SWITCHED_OFF_NODES|NF_IGNORE_SWITCHED_OFF_DEADENDS")
	ENDIF
	
	IF GET_NTH_CLOSEST_VEHICLE_NODE(vSearchPos, iNode, vLocation, nodeFlags, fZ_MeasureMulti, fZ_Tolerance)
		IF VDIST2(vSearchPos, vLocation) < (VD_MIN_NODE_RANGE * VD_MIN_NODE_RANGE)
		OR (MPGlobalsAmbience.bRequestedImpoundByPA) // ignore dist check if being requested by PA. fix for 2858321
		OR (VDIST2(vSearchPos, <<-237.3, 196.9, 81.6>>) < (100.0 * 100) )   // ignore an awkward spot outside one of the warehouses, where the road uses slip road nodes. 2816550
		
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE - return TRUE - vLocation = ") NET_PRINT_VECTOR(vLocation) NET_PRINT(" vSearchPos = ") NET_PRINT_VECTOR(vSearchPos) NET_PRINT(" iNode = ") NET_PRINT_INT(iNode)NET_NL()
			RETURN TRUE
		ELSE
			iNode++
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE - FAILED - vSearchPos : ") NET_PRINT_VECTOR(vSearchPos) NET_PRINT(" not close enough to valid node pos! vLocation = ") NET_PRINT_VECTOR(vLocation) NET_PRINT(" iNode = ") NET_PRINT_INT(iNode) NET_NL()
		ENDIF
	ELSE
		iNode++
		NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - GET_CLOSE_VEHICLE_NODE - FAILED - iNode = ") NET_PRINT_INT(iNode) NET_NL()
	ENDIF
		
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(VEHICLE_SPAWN_QUEUE_ENUM eVehicleSpawnQueue)
	SWITCH eVehicleSpawnQueue
		CASE eVeh_spawn_0_NONE			RETURN "0_NONE" BREAK
		CASE eVeh_spawn_1_PERSONAL		RETURN "1_PERSONAL" BREAK
		CASE eVeh_spawn_2_PEGASUS		RETURN "2_PEGASUS" BREAK
		CASE eVeh_spawn_3_TRUCK			RETURN "3_TRUCK" BREAK
		CASE eVeh_spawn_4_AIRCRAFT		RETURN "4_AIRCRAFT" BREAK
		CASE eVeh_spawn_5_HACKERTRUCK	RETURN "5_HACKERTRUCK" BREAK
		CASE eVeh_spawn_6_SUBMARINE	RETURN "6_SUBMARINE" BREAK
		CASE eVeh_spawn_7_SUBMARINE_DINGHY	RETURN "7_SUBMARINE_DINGHY" BREAK
		#IF FEATURE_DLC_2_2022
		CASE eVeh_spawn_8_ACIDLAB	RETURN "8_ACIDLAB" BREAK
		CASE eVeh_spawn_9_SUPPORT_BIKE 	RETURN "9_SUPPORT_BIKE" BREAK
		#ENDIF
	ENDSWITCH
	
	CASSERTLN(DEBUG_NET_AMBIENT, "<VEH_SPAWN_QUEUE> GET_STRING_FROM_VEHICLE_SPAWN_QUEUE - unknown eVehicleSpawnQueue", eVehicleSpawnQueue)
	TEXT_LABEL_63 str = ENUM_TO_INT(eVehicleSpawnQueue)
	str += "_UNKNOWN"
	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
ENDFUNC
#ENDIF

PROC SET_SPAWNING_BOOKED_VEHICLE(VEHICLE_SPAWN_QUEUE_ENUM eVehicleSpawnQueue)
	IF (g_eCachedVehicleSpawnQueue = eVeh_spawn_0_NONE)
		CPRINTLN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " VEH_SPAWN_QUEUE> set spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - safe to set")
		g_eCachedVehicleSpawnQueue = eVehicleSpawnQueue
	ELIF (g_eCachedVehicleSpawnQueue = eVehicleSpawnQueue)
	//	CDEBUG3LN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " veh_spawn_queue> set spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - already set to ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue))
	ELSE
		CASSERTLN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " VEH_SPAWN_QUEUE> set spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - but its set to ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(g_eCachedVehicleSpawnQueue))
	ENDIF
ENDPROC
PROC CLEAR_SPAWNING_BOOKED_VEHICLE(VEHICLE_SPAWN_QUEUE_ENUM eVehicleSpawnQueue)
	IF (g_eCachedVehicleSpawnQueue = eVehicleSpawnQueue)
		CPRINTLN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " VEH_SPAWN_QUEUE> clear spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - safe to clear")
		g_eCachedVehicleSpawnQueue = eVeh_spawn_0_NONE
	ELIF (g_eCachedVehicleSpawnQueue = eVeh_spawn_0_NONE)
		CDEBUG1LN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " veh_spawn_queue> clear spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - already cleared")
	ELSE
		CASSERTLN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " VEH_SPAWN_QUEUE> clear spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - but its set to ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(g_eCachedVehicleSpawnQueue))
	ENDIF
ENDPROC
FUNC BOOL CAN_SPAWN_BOOKED_VEHICLE(VEHICLE_SPAWN_QUEUE_ENUM eVehicleSpawnQueue)

	#IF FEATURE_CASINO
	IF (GB_GET_GB_CASINO_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = CSV_RECOVER_LUXURY_CAR )
		PRINTLN("CAN_SPAWN_BOOKED_VEHICLE - disabled during CSV_RECOVER_LUXURY_CAR")
		RETURN FALSE
	ENDIF
	#ENDIF

	IF (g_eCachedVehicleSpawnQueue = eVeh_spawn_0_NONE)
	OR (g_eCachedVehicleSpawnQueue = eVehicleSpawnQueue)
	//	CDEBUG3LN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " veh_spawn_queue> spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - TRUE")
		RETURN TRUE
	ENDIF
	
//	CDEBUG3LN(DEBUG_NET_AMBIENT, "<", GET_THIS_SCRIPT_NAME(), " veh_spawn_queue> spawning veh ", GET_STRING_FROM_VEHICLE_SPAWN_QUEUE(eVehicleSpawnQueue), " - FALSE")
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_HELI_VEHICLE_SPAWN()

	// don't spawn helis on MBV_COLLECT_DJ_HOOKED #4755833
	IF GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = MBV_COLLECT_DJ_HOOKED
	#IF FEATURE_HEIST_ISLAND
	OR GB_GET_ISLAND_HEIST_PREP_PLAYER_IS_ON(PLAYER_ID()) = IHV_STEALTH_HELICOPTER_2
	#ENDIF
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SIMPLE_INTERIOR_VALET_SERVICE_TEMP_SPAWN_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_CASINO
		CASE SIMPLE_INTERIOR_CASINO_APT
		#IF FEATURE_TUNER
		CASE SIMPLE_INTERIOR_CAR_MEET
		#ENDIF
			SWITCH iPlayerID
				CASE 0	vSpawnPoint 	= <<884.0570, 46.3511, 57.6129>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 1	vSpawnPoint 	= <<891.7275, 39.9353, 57.7330>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 2	vSpawnPoint 	= <<876.3865, 52.7669, 57.4716>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 3	vSpawnPoint 	= <<899.3981, 33.5194, 58.6127>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 4	vSpawnPoint 	= <<868.7159, 59.1828, 55.7184>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 5	vSpawnPoint 	= <<890.4728, 54.0216, 57.8319>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 6	vSpawnPoint 	= <<898.1434, 47.6058, 57.9758>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 7	vSpawnPoint 	= <<882.8023, 60.4375, 57.6432>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 8	vSpawnPoint 	= <<905.8139, 41.1900, 58.6127>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 9	vSpawnPoint 	= <<875.1318, 66.8533, 55.6020>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 10	vSpawnPoint 	= <<896.8887, 61.6922, 57.9627>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 11	vSpawnPoint 	= <<904.5592, 55.2763, 58.0874>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 12	vSpawnPoint 	= <<889.2181, 68.1080, 57.7462>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 13	vSpawnPoint 	= <<912.2297, 48.8605, 58.6127>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 14	vSpawnPoint 	= <<881.5476, 74.5238, 55.4267>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 15	vSpawnPoint 	= <<903.3045, 69.3627, 58.0086>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 16	vSpawnPoint 	= <<910.9750, 62.9469, 58.1549>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 17	vSpawnPoint 	= <<895.6340, 75.7785, 57.7366>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 18	vSpawnPoint 	= <<918.6456, 56.5310, 58.6127>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 19	vSpawnPoint 	= <<887.9634, 82.1944, 56.1261>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 20	vSpawnPoint 	= <<909.7203, 77.0332, 58.0837>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 21	vSpawnPoint 	= <<917.3909, 70.6174, 58.2345>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 22	vSpawnPoint 	= <<902.0498, 83.4491, 57.8257>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 23	vSpawnPoint 	= <<925.0614, 64.2016, 58.6127>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 24	vSpawnPoint 	= <<894.3793, 89.8649, 56.1375>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 25	vSpawnPoint 	= <<916.1362, 84.7038, 58.1581>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 26	vSpawnPoint 	= <<923.8067, 78.2879, 57.9697>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 27	vSpawnPoint 	= <<908.4656, 91.1196, 57.9333>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 28	vSpawnPoint 	= <<931.4772, 71.8721, 57.7652>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
				CASE 29	vSpawnPoint 	= <<900.7951, 97.5354, 56.2400>>	fSpawnHeading 	= 320.0900	RETURN TRUE	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Spawns the player's requested personal vehicle nearby, and a driver to drive it to the player
// NOTE: vDropLocation	// ensure the position we are navigating to is the stored position, as player could walk to inaccessible area during this point
FUNC BOOL CREATE_VEHICLE_AND_DRIVER(NETWORK_INDEX &niVehicle, NETWORK_INDEX &niDriver, MODEL_NAMES VehicleModel, INT &thisBit, INT iBitIndex, VECTOR &vDropLocation, INT &iNode, VECTOR &pos2, FLOAT &fHeading)
	
	//VECTOR pos2	//, pos3, vec, vec2
	//FLOAT vTrailerCoords
	//INT iNumLanes
	//VECTOR resultLinkDir
	
	//VIEWPORT_INDEX GameViewport
	
	//IF NOT IS_BIT_SET(thisBit, iBitIndex)
		//IF LOAD_ALL_PATH_NODES(TRUE)
	//		SET_BIT(thisBit, iBitIndex)
	//		NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - biPathsLoaded SET ") NET_NL()
		//ENDIF
		
	IF (ABSF(VMAG(vDropLocation)) <= 0.001)
		vDropLocation = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
		PRINTLN("CREATE_VEHICLE_AND_DRIVER, setting vDropLocation to ", vDropLocation)
	ENDIF
		
	VECTOR vPlayerCoords = vDropLocation
	FLOAT fTemp
	MODEL_NAMES ActualModel	
	COMBINED_MODELS CombinedModel = GET_COMBINED_MODEL_ID_FROM_MODEL_NAME(VehicleModel)
	IF (CM_NULL = CombinedModel)
		ActualModel = VehicleModel
	ELSE
		FillCombinedModelStruct(CombinedModel, g_SafeCombinedModel_Data)
		ActualModel = g_SafeCombinedModel_Data.Model[0]
	ENDIF
	
		
	IF REQUEST_LOAD_MODEL(ActualModel)
	//AND REQUEST_LOAD_MODEL(DriverModel)
	
		IF IS_PLAYER_LEAVING_SIMPLE_INTERIOR_USING_THE_VALET()
		#IF FEATURE_TUNER
		OR HAS_PLAYER_STARTED_WARP_INTO_CAR_MEET_CAR_PARK()
		OR HAS_PLAYER_STARTED_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK()
		#ENDIF
			VECTOR vSpawnLocation
			GET_SIMPLE_INTERIOR_VALET_SERVICE_TEMP_SPAWN_LOCATION(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), vSpawnLocation, fHeading)
			
			IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP(vPlayerCoords, vSpawnLocation, 1.0, 0.0, TRUE, TRUE)
				IF CREATE_VEHICLE_ON_GROUND(niVehicle, niDriver, vSpawnLocation, fHeading, ActualModel, thisBit)					
					CLEAR_BIT(thisBit, iBitIndex)
					CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
					NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - VDP_CreationCoordsFound CLEARED - B") NET_NL()
					NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_ON_GROUND - NORMAL - return TRUE : pos2 = ") NET_PRINT_VECTOR(vSpawnLocation) NET_PRINT(" heading = ") NET_PRINT_FLOAT(0.0) NET_NL()
					RETURN TRUE
				ENDIF
			ENDIF
			
		ELIF NOT ARE_COORDS_IN_SPECIAL_CREATION_AREA(vDropLocation)
		OR MPGlobalsAmbience.bLaunchVehicleDropTruck
		OR MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
		OR MPGlobalsAmbience.bLaunchVehicleDropAvenger 
		OR MPGlobalsAmbience.bLaunchVehicleDropHackerTruck 
		OR MPGlobalsAmbience.bLaunchVehicleDropSubmarine
		OR MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
		#IF FEATURE_DLC_2_2022
		OR MPGlobalsAmbience.bLaunchVehicleDropAcidLab
		OR MPGlobalsAmbience.bLaunchVehicleDropSupportBike
		#ENDIF
			// dont add custom vehicle nodes if another query is currently running, as they will get cleared when that finishes
			IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)
			
				// only adde custom nodes if HAS_GOT_SPAWN_LOCATION isnt currently running
				IF IS_SAFE_TO_START_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS()
				
					IF NOT IS_HAS_GOT_SPAWN_LOCATION_RUNNING()
					
						IF NOT IS_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS_RUNNING()
					
							// if we are dropping a personal vehicle then make sure we dont have custom nodes added by
							// some other system. see url:bugstar:7070827
							IF (MPGlobalsAmbience.bLaunchVehicleDropPersonal)
							AND NOT (MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft) // url:bugstar:7201447
							
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)	
									PRINTLN("     ---------->     VEHICLE DROP - bLaunchVehicleDropPersonal - not adding custom nodes.")
								ENDIF
								
							// if coords are on a personal yacht then add a custom car node.
							ELIF MPGlobalsAmbience.bYachtPersonalVehicleRequest
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									INT iYacht = LOCAL_PLAYER_PRIVATE_YACHT_ID()
									IF NOT (iYacht = -1)
										VECTOR vCoords
										FLOAT fHead
										CLEAR_CUSTOM_VEHICLE_NODES()
										GET_PRIVATE_YACHT_PERSONAL_VEHICLE_SPAWN_LOCATION(iYacht, vCoords, fHead)
										ADD_CUSTOM_VEHICLE_NODE(vCoords, fHead)						
										SET_BIT(thisBit, VDP_CustomCarNodesAdded)	
									ENDIF	
								ENDIF
							ELIF MPGlobalsAmbience.bLaunchVehicleDropTruck
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									ADD_CUSTOM_NODES_FOR_ALL_TRUCK_SPAWNS()
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)
								ENDIF
							ELIF MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)
									ADD_CUSTOM_NODES_FOR_PERSONAL_AIRCRAFT_SPAWNS(VehicleModel)
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)
								ENDIF
							ELIF MPGlobalsAmbience.bLaunchVehicleDropAvenger 
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)
									ADD_CUSTOM_NODES_FOR_ALL_AVENGER_SPAWNS()
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)
								ENDIF
							ELIF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
							#IF FEATURE_DLC_2_2022
							OR MPGlobalsAmbience.bLaunchVehicleDropAcidLab
							#ENDIF
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									ADD_CUSTOM_NODES_FOR_ALL_HACKERTRUCK_SPAWNS()
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)
								ENDIF
								
							
							#IF FEATURE_HEIST_ISLAND
							ELIF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_SPAWNS()
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)
								ENDIF
							ELIF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									ADD_CUSTOM_NODES_FOR_ALL_SUBMARINE_DINGHY_SPAWNS()
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)
								ENDIF
							#ENDIF	
//							// bobby y - else statement already handles it
//							ELIF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
//								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
//									SET_BIT(thisBit, VDP_CustomCarNodesAdded)	
//									PRINTLN("     ---------->     VEHICLE DROP - bLaunchVehicleDropSupportBike - not adding custom nodes.")
//								ENDIF
							ELSE
								
								
								// if we are not using a dropoff that uses custom nodes, just set the bit.
								IF NOT IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
									SET_BIT(thisBit, VDP_CustomCarNodesAdded)	
									PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - not using custom nodes, setting bit.")
								ENDIF
								
							ENDIF

							
						
						ELSE
							PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - waiting for other query to finish before adding custom nodes.")
						ENDIF
					ELSE
						PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - waiting for IS_HAS_GOT_SPAWN_LOCATION_RUNNING to finish before adding custom nodes.")
					ENDIF
				ELSE
					PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - waiting for IS_SAFE_TO_START_HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS before adding custom nodes.")
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)
				#IF FEATURE_HEIST_ISLAND
				IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
				AND g_SpawnData.bSpawningInSimpleInterior						
					
					// if this is the boot spawn, choose a random point, else go with the last know position in freemode
					IF NOT (g_bHasSpawnedFirstSub)
					
						IF (g_iFirstSubSpawnLocation = -1)
							g_iFirstSubSpawnLocation = GET_RANDOM_INT_IN_RANGE(0, NUMBER_OF_SUBMARINE_SPAWNS)
							PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - ON BOOT - first sub spawn, picked random location = ", g_iFirstSubSpawnLocation)
						ENDIF
											
						GET_SUBMARINE_VEHICLE_SPAWN_LOCATIONS(g_iFirstSubSpawnLocation, vPlayerCoords, fTemp)
						PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - ON BOOT - first sub spawn, using position = ", vPlayerCoords)
						
					ELSE
					
						// if we are spawning in the submarine, then we are in the interior BEFORE the submarine exist,
						// so use the last stored entry coords as a basis of where to spawn the sub - see url:bugstar:6815740 
						vPlayerCoords = GET_MP_VECTOR_CHARACTER_STAT(MP_STAT_FM_SPAWN_POSITION)						
						PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - spawning in sub, use last saved spawn position = ", vPlayerCoords)
						
						IF (VMAG(vPlayerCoords) = 0.0)
							vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
						ENDIF
						
					ENDIF
					
					
				ELSE
				#ENDIF
					vPlayerCoords = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID())
				#IF FEATURE_HEIST_ISLAND
				ENDIF
				#ENDIF
				
				NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - using player coords ") NET_PRINT_VECTOR(vPlayerCoords) NET_NL()
			ENDIF			
		
			// add check for model type being boat - if not don't use water nodes?
			//IF GENERATE_VEHICLE_CREATION_POS_FROM_PATHS(vDropLocation, pos2, resultLinkDir, 0, 180, VD_VEHICLE_MIN_SPAWN_RANGE, TRUE, TRUE, FALSE)
			
				VEHICLE_SPAWN_LOCATION_PARAMS Params
				Params.fMinDistFromCoords= VD_VEHICLE_MIN_SPAWN_RANGE
				Params.fMaxDistance= VD_VEHICLE_MAX_SPAWN_RANGE

				IF IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded)	
					Params.fMaxDistance= 9999.0	
				ENDIF
				
				IF MPGlobalsAmbience.bLaunchVehicleDropTruck
					Params.fHigherZLimit = 9999.0
					Params.bIgnoreCustomNodesForArea = TRUE
					Params.bCheckEntityArea = TRUE
				ENDIF
				
				IF MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
					Params.fMinDistFromCoords = 5.0
					Params.fHigherZLimit = 9999.0
					Params.bIgnoreCustomNodesForArea = TRUE
					Params.bAvoidSpawningInExclusionZones = FALSE
					Params.bUseExactCoordsIfPossible = TRUE
					Params.bConsiderOnlyActiveNodes = FALSE
					Params.bStartOfMissionPVSpawn = TRUE
					Params.bCheckEntityArea = TRUE
				ENDIF
				
				IF MPGlobalsAmbience.bLaunchVehicleDropAvenger 
					Params.fMinDistFromCoords = 5.0
					Params.fHigherZLimit = 9999.0
					Params.bIgnoreCustomNodesForArea = TRUE
					Params.bAvoidSpawningInExclusionZones = FALSE
					Params.bUseExactCoordsIfPossible = TRUE
					Params.bConsiderOnlyActiveNodes = FALSE
					Params.bStartOfMissionPVSpawn = TRUE
					Params.bCheckEntityArea = TRUE
					Params.fVisibleDistance = 250.0
				ENDIF
				
				IF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
				#IF FEATURE_DLC_2_2022
				OR MPGlobalsAmbience.bLaunchVehicleDropAcidLab
				#ENDIF
					Params.fHigherZLimit = 9999.0
					Params.bIgnoreCustomNodesForArea = TRUE
					Params.bCheckEntityArea = TRUE
				ENDIF
				
				#IF FEATURE_HEIST_ISLAND
				IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine			
					params.bIgnoreCustomNodesForMissionLaunch = TRUE
					params.bIgnoreCustomNodesForArea = TRUE
					params.bAvoidSpawningInExclusionZones = FALSE
					params.bCheckOwnVisibility = FALSE
					params.bAllowFallbackToInactiveNodes = FALSE
					Params.fHigherZLimit = 9999.0
					Params.bIgnoreCustomNodesForArea = TRUE
					Params.bCheckEntityArea = TRUE
					Params.bUseCustomNodesOnly = TRUE
				ENDIF
				#ENDIF
				
				#IF FEATURE_HEIST_ISLAND
				IF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
					params.bIgnoreCustomNodesForMissionLaunch = TRUE
					params.bIgnoreCustomNodesForArea = TRUE
					params.bAvoidSpawningInExclusionZones = FALSE
					params.bCheckThisPlayerVehicle = FALSE
					params.bAllowFallbackToInactiveNodes = FALSE
					Params.fHigherZLimit = 9999.0
					Params.bCheckEntityArea = TRUE
					Params.bUseCustomNodesOnly = TRUE
				ENDIF
				#ENDIF
				
				#IF FEATURE_DLC_2_2022
				IF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
					params.bIgnoreCustomNodesForMissionLaunch = TRUE
					params.bIgnoreCustomNodesForArea = TRUE
					params.bAvoidSpawningInExclusionZones = FALSE
					params.bConsiderOnlyActiveNodes = FALSE
					params.bUseExactCoordsIfPossible = TRUE
					Params.fHigherZLimit = 9999.0
					Params.bCheckEntityArea = TRUE
					Params.bUseCustomNodesOnly = FALSE
				ENDIF
				#ENDIF

				Params.bCheckThisPlayerVehicle = FALSE
	
				
				IF (Params.bUseExactCoordsIfPossible = TRUE)
					IF VDIST2(vPlayerCoords, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 10.0
						PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - bUseExactCoordsIfPossible setting FALSE")
						Params.bUseExactCoordsIfPossible = FALSE	
					ENDIF
				ENDIF
	
	
				IF IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded) 
					IF IS_BIT_SET(thisBit, VDP_CreationCoordsFound)
					OR HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(vPlayerCoords, <<0.0, 0.0, 0.0>>, VehicleModel, TRUE, pos2, fHeading, Params)
					
						IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_DROP(vPlayerCoords, pos2, Params.fMaxDistance, Params.fMinDistFromCoords)
						
							//GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(pos2, 1, pos3, fHeading, iNumLanes, NF_INCLUDE_SWITCHED_OFF_NODES)
							
							IF MPGlobalsAmbience.bLaunchVehicleDropTruck
								IF IS_BIT_SET(thisBit, VDP_CreationCoordsFound)
									IF NOT (MPGlobalsAmbience.bJustCreatedTruck)
									AND NOT (MPGlobalsAmbience.bJustCreatedTrailer)
										IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP(pos2, fHeading,
												10000.0,	//FLOAT fMaxSpawnDist = VD_VEHICLE_MAX_SPAWN_RANGE,
												DEFAULT,	//FLOAT fMinSpawnDist = VD_VEHICLE_MIN_SPAWN_RANGE,
												DEFAULT,	//BOOL bIgnoreDistanceCheck = FALSE,
												DEFAULT)	//BOOL bIgnoreSightChecks = FALSE)
											//
										ELSE
											NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - truck pos2:")
											NET_PRINT_VECTOR(pos2)
											NET_PRINT(" no longer safe, get new truck coords")
											NET_PRINT_VECTOR(vPlayerCoords)
											NET_NL()
											
											CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
											CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
											
											RETURN FALSE
										ENDIF
									ELSE
										
									ENDIF
								ENDIF
								
								IF MPGlobalsAmbience.iBSPerformVehicleDropTruckChecks != 0
									MPGlobalsAmbience.vPerformVehicleDropTruckChecks = pos2
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - truck pos2:")
									NET_PRINT_VECTOR(pos2)
									NET_PRINT(" performing vehicle drop checks ")
									NET_PRINT_INT(MPGlobalsAmbience.iBSPerformVehicleDropTruckChecks)
									NET_NL()
	//								
	//								CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
	//								CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
									
									RETURN FALSE
								ENDIF
							ENDIF
							
							IF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
								IF IS_BIT_SET(thisBit, VDP_CreationCoordsFound)
									IF NOT (MPGlobalsAmbience.bJustCreatedTruck)
									AND NOT (MPGlobalsAmbience.bJustCreatedTrailer)
										IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP(pos2, fHeading,
												10000.0,	//FLOAT fMaxSpawnDist = VD_VEHICLE_MAX_SPAWN_RANGE,
												DEFAULT,	//FLOAT fMinSpawnDist = VD_VEHICLE_MIN_SPAWN_RANGE,
												DEFAULT,	//BOOL bIgnoreDistanceCheck = FALSE,
												DEFAULT)	//BOOL bIgnoreSightChecks = FALSE)
											//
										ELSE
											NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - HACKER TRUCK pos2:")
											NET_PRINT_VECTOR(pos2)
											NET_PRINT(" no longer safe, get new hacker truck coords")
											NET_PRINT_VECTOR(vPlayerCoords)
											NET_NL()
											
											CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
											CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
											
											RETURN FALSE
										ENDIF
									ELSE
										
									ENDIF
								ENDIF
								
								IF MPGlobalsAmbience.iBSPerformVehicleDropHackerTruckChecks != 0
									MPGlobalsAmbience.vPerformVehicleDropHackerTruckChecks = pos2
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - hacker truck pos2:")
									NET_PRINT_VECTOR(pos2)
									NET_PRINT(" performing vehicle drop checks ")
									NET_PRINT_INT(MPGlobalsAmbience.iBSPerformVehicleDropHackerTruckChecks)
									NET_NL()
	//								
	//								CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
	//								CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
									
									RETURN FALSE
								ENDIF
							ENDIF
							
							#IF FEATURE_HEIST_ISLAND
							IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
								IF MPGlobalsAmbience.iBSPerformVehicleDropSubmarineChecks != 0
									MPGlobalsAmbience.vPerformVehicleDropSubmarineChecks = pos2
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - Submarine pos2:")
									NET_PRINT_VECTOR(pos2)
									NET_PRINT(" performing vehicle drop checks ")
									NET_PRINT_INT(MPGlobalsAmbience.iBSPerformVehicleDropSubmarineChecks)
									NET_NL()

									RETURN FALSE
								ENDIF
							ENDIF
							#ENDIF
							
							#IF FEATURE_HEIST_ISLAND
							IF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
								IF MPGlobalsAmbience.iBSPerformVehicleDropSubmarineDinghyChecks != 0
									MPGlobalsAmbience.vPerformVehicleDropSubmarineDinghyChecks = pos2
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - Submarine dinghy pos2:")
									NET_PRINT_VECTOR(pos2)
									NET_PRINT(" performing vehicle drop checks ")
									NET_PRINT_INT(MPGlobalsAmbience.iBSPerformVehicleDropSubmarineDinghyChecks)
									NET_NL()

									RETURN FALSE
								ENDIF
							ENDIF
							#ENDIF
							
							#IF FEATURE_DLC_2_2022
							IF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
								IF MPGlobalsAmbience.iBSPerformVehicleDropSupportBikeChecks != 0
									MPGlobalsAmbience.vPerformVehicleDropSupportBikeChecks = pos2
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - Support bike pos2:")
									NET_PRINT_VECTOR(pos2)
									NET_PRINT(" performing vehicle drop checks ")
									NET_PRINT_INT(MPGlobalsAmbience.iBSPerformVehicleDropSupportBikeChecks)
									NET_NL()
									RETURN FALSE
								ENDIF
							ENDIF
							
							IF MPGlobalsAmbience.bLaunchVehicleDropAcidLab
								IF IS_BIT_SET(thisBit, VDP_CreationCoordsFound)
									IF NOT (MPGlobalsAmbience.bJustCreatedTruck)
									AND NOT (MPGlobalsAmbience.bJustCreatedTrailer)
										IF IS_SPAWN_POSITION_SAFE_FOR_NET_VEHICLE_TRUCK_DROP(pos2, fHeading,
												10000.0,	//FLOAT fMaxSpawnDist = VD_VEHICLE_MAX_SPAWN_RANGE,
												DEFAULT,	//FLOAT fMinSpawnDist = VD_VEHICLE_MIN_SPAWN_RANGE,
												DEFAULT,	//BOOL bIgnoreDistanceCheck = FALSE,
												DEFAULT)	//BOOL bIgnoreSightChecks = FALSE)
											//
										ELSE
											NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - ACID LAB pos2:")
											NET_PRINT_VECTOR(pos2)
											NET_PRINT(" no longer safe, get new hacker truck coords")
											NET_PRINT_VECTOR(vPlayerCoords)
											NET_NL()
											
											CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
											CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
											
											RETURN FALSE
										ENDIF
									ELSE
										
									ENDIF
								ENDIF
								
								IF MPGlobalsAmbience.iBSPerformVehicleDropAcidLabChecks != 0
									MPGlobalsAmbience.vPerformVehicleDropAcidLabChecks = pos2
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER FAILED - acid lab pos2:")
									NET_PRINT_VECTOR(pos2)
									NET_PRINT(" performing vehicle drop checks ")
									NET_PRINT_INT(MPGlobalsAmbience.iBSPerformVehicleDropAcidLabChecks)
									NET_NL()
	//								
	//								CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
	//								CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
									
									RETURN FALSE
								ENDIF
							ENDIF
							#ENDIF
							
							IF NOT IS_BIT_SET(thisBit, VDP_CreationCoordsFound)
								MPGlobalsAmbience.bJustCreatedTruck = FALSE
								MPGlobalsAmbience.bJustCreatedTrailer = FALSE
								#IF FEATURE_HEIST_ISLAND
								MPGlobalsAmbience.bJustCreatedSubmarine = FALSE
								MPGlobalsAmbience.bJustCreatedSubmarineDinghy = FALSE
								#ENDIF
								#IF FEATURE_DLC_2_2022
								MPGlobalsAmbience.bJustCreatedSupportBike = FALSE
								#ENDIF
								SET_BIT(thisBit, VDP_CreationCoordsFound)
								NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - VDP_CreationCoordsFound SET") NET_NL()
							ENDIF
														
							CLEAR_AREA_OF_VEHICLES(pos2, 5.0)//, TRUE, FALSE, FALSE, TRUE)
							
							NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - spawn pos2 generated = ") NET_PRINT_VECTOR(pos2) NET_PRINT(" heading = ") NET_PRINT_FLOAT(fHeading) NET_PRINT(" vDropLocation (place navigating to = ") NET_PRINT_VECTOR(vDropLocation) NET_NL()
							
							/*// Invert the heading if taxi is create facing away from the player
							pos3 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(pos2, fHeading, <<0.0, 1.0, 0.0>>)
							// Vehicle Vector
							vec = pos3 - pos2
							// Vector to Player
							vec2 = vDropLocation - pos2
							IF (GET_ANGLE_BETWEEN_2D_VECTORS(vec.x, vec.y, vec2.x, vec2.y) > 60.0)
								fHeading += 180
								fHeading = WRAP(fHeading, 0, 360)
								printDebugStringAndFloat("\n VEHICLE DROP - revised fHeading = ", fHeading)
							ENDIF*/
							
							//IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(pos2,5,2,1,6,TRUE,TRUE,TRUE,120,FALSE,-1,TRUE,0)
								IF CREATE_VEHICLE_ON_GROUND(niVehicle, niDriver, pos2, fHeading, ActualModel, /*DriverModel, vDropLocation,*/ thisBit)
									//RELEASE_PATH_NODES()
									
									CLEAR_BIT(thisBit, VDP_CustomCarNodesAdded)
									CLEAR_CUSTOM_VEHICLE_NODES()
									
									CLEAR_BIT(thisBit, iBitIndex)
									CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - VDP_CreationCoordsFound CLEARED - B") NET_NL()
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - NORMAL - return TRUE : pos2 = ") NET_PRINT_VECTOR(pos2) NET_PRINT(" heading = ") NET_PRINT_FLOAT(fHeading) NET_NL()
									
									#IF FEATURE_HEIST_ISLAND
									IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
									AND g_SpawnData.bSpawningInSimpleInterior
										IF NOT (g_bHasSpawnedFirstSub)
											g_bHasSpawnedFirstSub = TRUE
											PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - ON BOOT - setting g_bHasSpawnedFirstSub = TRUE")
										ENDIF
									ENDIF
									#ENDIF
									
									RETURN TRUE
								ENDIF
								
								IF MPGlobalsAmbience.bYachtPersonalVehicleRequest
									NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - resetting MPGlobalsAmbience.bYachtPersonalVehicleRequest") NET_NL()
									MPGlobalsAmbience.bYachtPersonalVehicleRequest = FALSE
								ENDIF
								
							//ELSE
							//	NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - IS_POINT_OK_FOR_NET_ENTITY_CREATION = FALSE ") NET_NL()
							//ENDIF
						ELSE
							AddPointToServerRefusalList(pos2) // so we dont get same result returned. fix for 3163054
							CLEAR_BIT(thisBit, VDP_CreationCoordsFound)
							NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - VDP_CreationCoordsFound CLEARED - A") NET_NL()
							GET_CLOSE_VEHICLE_NODE(GET_PLAYER_COORDS(PLAYER_ID()), vDropLocation, iNode)
							NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - OVER MAX VEHICLE_SPAWN_RANGE LIMIT") NET_PRINT_FLOAT(VD_VEHICLE_MAX_SPAWN_RANGE) NET_NL()
						ENDIF
					ELSE
						NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS = FALSE") NET_NL()
						PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - IS_BIT_SET(thisBit, VDP_CreationCoordsFound) = ", IS_BIT_SET(thisBit, VDP_CreationCoordsFound))
					ENDIF
				ELSE					
					PRINTLN("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded) = ", IS_BIT_SET(thisBit, VDP_CustomCarNodesAdded))					
				ENDIF
			//ENDIF
		
		//SCRIPTED CREATION POINT
		ELSE
			IF GET_SCRIPTED_TAXI_CREATION_POINT(vDropLocation, pos2, fHeading)
				IF CREATE_VEHICLE_ON_GROUND(niVehicle, niDriver, pos2, fHeading, VehicleModel, /*DriverModel, vDropLocation,*/ thisBit)
					//RELEASE_PATH_NODES()
					CLEAR_BIT(thisBit, iBitIndex)
					NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - SCRIPTED - PATHS RELEASED ") NET_NL()
					RETURN TRUE
				ENDIF
			ELSE
				NET_PRINT("     ---------->     VEHICLE DROP - CREATE_VEHICLE_AND_DRIVER - GET_SCRIPTED_TAXI_CREATION_POINT = FALSE ") NET_NL()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns 0 if it is okay to use the Personal Vehicle. Else returns a number based on reason.

ENUM BYPASS_PERSONAL_VEHICLE_CHECKS_ENUM
	 BPVC_default = 0
	,BPVC_TRUCK
	,BPVC_AVENGER
	,BPVC_HACKERTRUCK
	,BPVC_SUBMARINE
	,BPVC_SUBMARINE_DINGHY
	#IF FEATURE_DLC_2_2022
	,BPVC_ACIDLAB
	,BPVC_SUPPORT_BIKE
	#ENDIF
ENDENUM


FUNC INT SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT(INT iVehicleNumber = -1, BOOL bDoLimitedChecks = FALSE, BOOL bDoVehicleNumberCheck = FALSE, BOOL bDoVehicleDriveableCheck = FALSE, BOOL bareweonspecialvehicleselection = FALSE,
		BYPASS_PERSONAL_VEHICLE_CHECKS_ENUM eBypassPVChecksForSpecialVeh = BPVC_default, BOOL bBypassPVChecksroad = FALSE, BOOL bBypassChecksforPA = FALSE)
	
	BOOL bBypassPVChecksForTruck = (eBypassPVChecksForSpecialVeh = BPVC_TRUCK)
	BOOL bBypassPVChecksForAvenger = (eBypassPVChecksForSpecialVeh = BPVC_AVENGER)
	BOOL bBypassPVChecksForHackerTruck = (eBypassPVChecksForSpecialVeh = BPVC_HACKERTRUCK)
	BOOL bBypassPVChecksForSubmarine = (eBypassPVChecksForSpecialVeh = BPVC_SUBMARINE)
	BOOL bBypassPVChecksForSubmarineDinghy = (eBypassPVChecksForSpecialVeh = BPVC_SUBMARINE_DINGHY)
	#IF FEATURE_DLC_2_2022
	BOOL bBypassPVChecksForAcidLab = (eBypassPVChecksForSpecialVeh = BPVC_ACIDLAB)
	#ENDIF
	BOOL bBypassChecksForAATrailer = FALSE
	
	#IF FEATURE_DLC_2_2022
	BOOL bBypassPVChecksForSupportBike = (eBypassPVChecksForSpecialVeh = BPVC_SUPPORT_BIKE)
	#ENDIF
	
	IF iVehicleNumber = DISPLAY_SLOT_AA_TRAILER
		bBypassChecksForAATrailer = TRUE
	ENDIF
	
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
	AND g_piOwnerOfTestDrivePV = PLAYER_ID()
		PRINTLN("PERSONAL VEHICLE SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - Test driving PV")
		
		RETURN 98
	ENDIF
	
	IF NOT bBypassPVChecksForTruck
	AND NOT bBypassPVChecksForAvenger
	AND NOT bBypassPVChecksForHackerTruck
	AND NOT bBypassPVChecksForSubmarine
	AND NOT bBypassPVChecksForSubmarineDinghy
	#IF FEATURE_DLC_2_2022
	AND NOT bBypassPVChecksForAcidLab
	AND NOT bBypassPVChecksForSupportBike
	#ENDIF
		IF iVehicleNumber = -1
			IF CURRENT_SAVED_VEHICLE_SLOT() >	-1 					//MPGlobalsAmbience.iVDPersonalVehicleSlot > -1
				iVehicleNumber = CURRENT_SAVED_VEHICLE_SLOT()		//MPGlobalsAmbience.iVDPersonalVehicleSlot
				if IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel)
				AND bareweonspecialvehicleselection = FALSE
					PRINTLN("PERSONAL VEHICLE SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT This is a special vehicle ")
					RETURN 1
				ELIF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel)
				AND bareweonspecialvehicleselection = FALSE
					PRINTLN("PERSONAL VEHICLE SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT This is a hangar vehicle ")
					RETURN 1
				else
					//MPGlobalsAmbience.iVDPersonalVehicleSlot = iVehicleNumber
					NET_PRINT("     ---------->     VEHICLE DROP - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT -A- iVehicleNumber ") NET_PRINT_INT(iVehicleNumber) NET_NL()
				ENDIF
			ELSE
				IF bDoVehicleNumberCheck = FALSE
					iVehicleNumber = 0
				ELSE
					//NET_PRINT("     ---------->     VEHICLE DROP - return 1 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - NO VALID VEHICLE NUM - iVDPersonalVehicleSlot = ") NET_PRINT_INT(MPGlobalsAmbience.iVDPersonalVehicleSlot) NET_NL()
					RETURN 1
				ENDIF
			ENDIF
		ENDIF
		
		IF iVehicleNumber <= -1
			NET_PRINT("     ---------->     VEHICLE DROP - return 1 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - NO VALID VEHICLE NUM -B- iVehicleNumber = ") NET_PRINT_INT(iVehicleNumber) NET_NL()
			RETURN 1
		ENDIF
		
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW() 
			IF NOT IS_GUNRUNNING_WEAPONISED_VEHICLE_ALLOWED(TRUE, iVehicleNumber)
				NET_PRINT("     ---------->     VEHICLE DROP - return 6 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - IS_GUNRUNNING_WEAPONISED_VEHICLE_ALLOWED is FALSE - iVehicleNumber = ") NET_PRINT_INT(iVehicleNumber) NET_NL()
				RETURN 6
			ENDIF
		ENDIF
	
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_OnlyAllowLobbyClassRestrictedVehiclesFromPhone)					
			PRINTLN("    ---------->     VEHICLE DROP - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - ciOptionsBS28_OnlyAllowLobbyClassRestrictedVehiclesFromPhone set - Checking for Restricted vehicles, Model Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel))
			IF NOT IS_PERSONAL_VEHICLE_MODEL_ALLOWED_IN_GENERIC_JOB_BY_ANY_LIBRARY(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel)
				PRINTLN("    ---------->     VEHICLE DROP - return 25 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - iVehicle: ", iVehicleNumber, ", Name: ", FMMC_GET_MODEL_NAME_FOR_DEBUG(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel), " is a restricted class - bClassRestricted")
				RETURN 25
			ENDIF
		ENDIF
		
		//NET_PRINT("     ---------->     VEHICLE DROP - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT -B- iVehicleNumber ") NET_PRINT_INT(iVehicleNumber) NET_NL()
	
		IF IS_BIT_SET(g_MpSavedVehicles[iVehicleNumber].iVehicleBS, MP_SAVED_VEHICLE_DESTROYED)
			IF IS_BIT_SET(g_MpSavedVehicles[iVehicleNumber].iVehicleBS, MP_SAVED_VEHICLE_INSURED)
				NET_PRINT("     ---------->     VEHICLE DROP - return 12 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER PERSONAL VEHICLE IS DESTROYED - INSURED") NET_NL()
				RETURN 12
			ELSE
				NET_PRINT("     ---------->     VEHICLE DROP - return 13 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER PERSONAL VEHICLE IS DESTROYED - NOT INSURED") NET_NL()
				RETURN 13
			ENDIF
		ENDIF
	ENDIF
	
	IF bDoVehicleDriveableCheck = TRUE
		IF NOT IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 6 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - VEHCILE IS NOT DRIVEABLE ") NET_NL()
			RETURN 6
		ENDIF
	ENDIF
	
	IF bDoLimitedChecks = FALSE
		IF MPGlobalsAmbience.bLaunchVehicleDropPersonal			//VDPersonalData.iVehicleDropPersonalStage = VDP_SETUP
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS PERSONAL VEHICLE DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropTruck
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS TRUCK DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS PERSONAL AIRCRAFT DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropAvenger 
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS PLANE DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS HACKER TRUCK DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS SUBMARINE DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS SUBMARINE DINGHY DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		#IF FEATURE_DLC_2_2022
		IF MPGlobalsAmbience.bLaunchVehicleDropAcidLab
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS ACID LAB DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		IF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
			NET_PRINT("     ---------->     VEHICLE DROP - return 7 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER STILL ON PREVIOUS SUPPORT BIKE DELIVERY ") NET_NL()
			RETURN 7
		ENDIF
		#ENDIF
	ENDIF
	
//	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED)
//		NET_PRINT("     ---------->     VEHICLE DROP - return 8 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PERSONAL VEHICLE IS IMPOUNDED ") NET_NL()
//		RETURN 8
//	ENDIF
	
	IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS, MP_SAVE_GENERAL_BS_NOT_PAID_MECHANIC)
	AND NOT bBypassPVChecksForAvenger
	AND NOT bBypassPVChecksForTruck
	AND NOT bBypassPVChecksForHackerTruck
	AND NOT bBypassPVChecksForSubmarine
	AND NOT bBypassPVChecksForSubmarineDinghy
	#IF FEATURE_DLC_2_2022
	AND NOT bBypassPVChecksForAcidLab
	AND NOT bBypassPVChecksForSupportBike
	#ENDIF
		NET_PRINT("     ---------->     VEHICLE DROP - return 10 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - MECHANIC HASN'T BEEN PAID ") NET_NL()
		RETURN 10
	ENDIF
	
	IF NOT bBypassPVChecksForTruck
	AND NOT bBypassPVChecksForAvenger
	AND NOT bBypassPVChecksForHackerTruck
	AND NOT bBypassPVChecksForSubmarine
	AND NOT bBypassPVChecksForSubmarineDinghy
	#IF FEATURE_DLC_2_2022
	AND NOT bBypassPVChecksForAcidLab
	AND NOT bBypassPVChecksForSupportBike
	#ENDIF
		IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(iVehicleNumber)
		//IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_PLYVEH_MODEL) = 0
			NET_PRINT("     ---------->     VEHICLE DROP - return 8 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PERSONAL VEHICLE IS IMPOUNDED ") NET_NL()
			RETURN 8
		ENDIF
	ENDIF
	
	IF NOT bBypassPVChecksForTruck
	AND NOT bBypassPVChecksForAvenger
	AND NOT bBypassPVChecksForHackerTruck
	AND NOT bBypassPVChecksForSubmarine
	AND NOT bBypassPVChecksForSubmarineDinghy
	#IF FEATURE_DLC_2_2022
	AND NOT bBypassPVChecksForAcidLab
	AND NOT bBypassPVChecksForSupportBike
	#ENDIF
		IF IS_THIS_MODEL_A_SPECIAL_VEHICLE(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel)
			IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
			AND NOT IS_ENTITY_DEAD(PERSONAL_VEHICLE_ID())
			AND IS_THIS_MODEL_A_SPECIAL_VEHICLE(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()))
			AND GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()) != TRAILERSMALL2
				NET_PRINT("     ---------->     VEHICLE DROP - return 14 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER HAS SPECIAL VEHICLE ACTIVE IN WORLD ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
				RETURN 14		
			ENDIF
		ENDIF
	ENDIF
	
	IF bBypassPVChecksForTruck
		IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_TRUCK(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING TRUCK ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF TRUCK ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
//		IF IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(PLAYER_ID())
//		OR IS_PLAYER_TRUCK_IN_FREEMODE(PLAYER_ID())
//			NET_PRINT("     ---------->     VEHICLE DROP - return 19 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - TRUCK WHILE PLAYERS TRUCK IS IN FREEMODE ") NET_NL()
//			RETURN 19
//		ENDIF
		IF IS_PLAYER_REQUESTED_AVENGER_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_PLANE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 20 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - TRUCK WHILE PLAYERS AVENGER IS IN FREEMODE ") NET_NL()
			RETURN 20
		ENDIF
		IF IS_PLAYER_REQUESTED_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - TRUCK WHILE PLAYERS HACKER TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 21
		ENDIF
		#IF FEATURE_HEIST_ISLAND
		IF HAS_PLAYER_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_SUBMARINE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 22 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - TRUCK WHILE PLAYERS SUBMARINE IS IN FREEMODE ") NET_NL()
			RETURN 22
		ENDIF
		#ENDIF
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_ACIDLAB_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - TRUCK WHILE PLAYERS ACID LAB IS IN FREEMODE ") NET_NL()
			RETURN 23
		ENDIF
		#ENDIF
		
	ELIF bBypassPVChecksForAvenger
		IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_AVENGER(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING AVENGER ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF AVENGER ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
		IF IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_TRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 19 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - AVENGER WHILE PLAYERS TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 19
		ENDIF
		IF IS_PLAYER_REQUESTED_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - AVENGER WHILE PLAYERS HACKER TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 21
		ENDIF
		#IF FEATURE_HEIST_ISLAND
		IF HAS_PLAYER_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_SUBMARINE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 22 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - AVENGER WHILE PLAYERS SUBMARINE IS IN FREEMODE ") NET_NL()
			RETURN 22
		ENDIF
		#ENDIF
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_ACIDLAB_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - AVENGER WHILE PLAYERS ACID LAB IS IN FREEMODE ") NET_NL()
			RETURN 23
		ENDIF
		#ENDIF
		
	ELIF bBypassPVChecksForHackerTruck
		IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_HACKERTRUCK(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING HACKER TRUCK ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF HACKER TRUCK ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
		IF IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_TRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 19 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - HACKER TRUCK WHILE PLAYERS TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 19
		ENDIF
		IF IS_PLAYER_REQUESTED_AVENGER_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_PLANE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 20 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - HACKER TRUCK WHILE PLAYERS AVENGER IS IN FREEMODE ") NET_NL()
			RETURN 20
		ENDIF
		#IF FEATURE_HEIST_ISLAND
		IF HAS_PLAYER_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_SUBMARINE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 22 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - HACKER TRUCK WHILE PLAYERS SUBMARINE IS IN FREEMODE ") NET_NL()
			RETURN 22
		ENDIF
		#ENDIF
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_ACIDLAB_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - HACKER TRUCK WHILE PLAYERS ACID LAB IS IN FREEMODE ") NET_NL()
			RETURN 23
		ENDIF
		#ENDIF
		
	ELIF bBypassPVChecksForSubmarine
		IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_SUBMARINE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING SUBMARINE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF SUBMARINE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
		IF IS_PLAYER_IN_SUBMARINE_THEY_OWN(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - IS_PLAYER_IN_SUBMARINE_THEY_OWN ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
		IF IS_ENTITY_ALIVE(GET_SUBMARINE_VEHICLE(PLAYER_ID()))
		AND IS_PLAYER_SUBMARINE_IN_FREEMODE(PLAYER_ID())
			RETURN 14 //Already active
		ENDIF
		IF IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_TRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 19 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - SUBMARINE WHILE PLAYERS TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 19
		ENDIF
		IF IS_PLAYER_REQUESTED_AVENGER_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_PLANE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 20 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - SUBMARINE WHILE PLAYERS AVENGER IS IN FREEMODE ") NET_NL()
			RETURN 20
		ENDIF
		IF IS_PLAYER_REQUESTED_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - SUBMARINE WHILE PLAYERS HACKER TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 21
		ENDIF
		#IF FEATURE_DLC_2_2022
		IF IS_PLAYER_REQUESTED_ACID_LAB_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_ACIDLAB_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - SUBMARINE WHILE PLAYERS ACID LAB IS IN FREEMODE ") NET_NL()
			RETURN 23
		ENDIF
		#ENDIF
	ELIF bBypassPVChecksForSubmarineDinghy
		IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_SUBMARINE_DINGHY(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING DINGHY ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF DINGHY ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
	#IF FEATURE_DLC_2_2022
	ELIF bBypassPVChecksForAcidLab
		IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_ACIDLAB(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING ACID LAB ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF ACID LAB ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
		IF IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_TRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 19 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - ACID LAB WHILE PLAYERS TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 19
		ENDIF
		IF IS_PLAYER_REQUESTED_AVENGER_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_PLANE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 20 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - ACID LAB WHILE PLAYERS AVENGER IS IN FREEMODE ") NET_NL()
			RETURN 20
		ENDIF
		IF IS_PLAYER_REQUESTED_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_HACKERTRUCK_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 21 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - ACID LAB WHILE PLAYERS HACKER TRUCK IS IN FREEMODE ") NET_NL()
			RETURN 21
		ENDIF
		IF HAS_PLAYER_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(PLAYER_ID())
		OR IS_PLAYER_SUBMARINE_IN_FREEMODE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP - return 22 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - ACID LAB WHILE PLAYERS SUBMARINE IS IN FREEMODE ") NET_NL()
			RETURN 22
		ENDIF
	#ENDIF
	#IF FEATURE_DLC_2_2022
	ELIF bBypassPVChecksForSupportBike
		IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_SUPPORT_BIKE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING SUPPORT BIKE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF SUPPORT BIKE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			RETURN 2
		ENDIF
	#ENDIF
	ELSE
		IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_MY_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
					NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING PERSONAL VEHICLE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
					RETURN 9
				ENDIF
			ENDIF
			
			IF NOT bBypassChecksforPA
				NET_PRINT("     ---------->     VEHICLE DROP - return 2 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS WITHIN RANGE OF PERSONAL VEHICLE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
				
				#IF FEATURE_TUNER
				IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
					RETURN 98
				ENDIF
				#ENDIF
				
				RETURN 2
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_MY_OLD_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID()))
				NET_PRINT("     ---------->     VEHICLE DROP - return 9 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS USING OLD PERSONAL VEHICLE ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
				RETURN 9
			ENDIF
		ENDIF
	ENDIF
	
	IF bBypassChecksforPA
		IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
		AND NOT IS_ENTITY_DEAD(PERSONAL_VEHICLE_ID())
		AND IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()))
			NET_PRINT("     ---------->     VEHICLE DROP - return 15 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IS ALREADY USING PERSONAL AIRCRAFT ") NET_PRINT_FLOAT(VD_PERSONAL_RANGE) NET_NL()
			
			RETURN 15
		ENDIF
	ENDIF
	
	// Checks for possession of personal vehicle
	IF NOT bBypassPVChecksForTruck
	AND NOT bBypassPVChecksForAvenger
	AND NOT bBypassPVChecksForHackerTruck
	AND NOT bBypassPVChecksForSubmarine
	AND NOT bBypassPVChecksForSubmarineDinghy
	#IF FEATURE_DLC_2_2022
	AND NOT bBypassPVChecksForAcidLab
	AND NOT bBypassPVChecksForSupportBike
	#ENDIF
		IF g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
		//IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_PLYVEH_MODEL) = 0
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("     ---------->     VEHICLE DROP - return 1 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER HAS NO PERSONAL VEHICLE SET ") NET_NL()
			RETURN 1
		ENDIF
				
		//Check player has unlocked a garage
		IF NOT bBypassChecksForAATrailer
			IF NOT DOES_PLAYER_OWN_ANY_VEHICLE_STORAGE()
				IF g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
					NET_PRINT("     ---------->     VEHICLE DROP - return 5 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER DOESN'T HAVE GARAGE BUT DOES HAVE PERSONAL VEHICLE ") NET_NL()
					RETURN 5
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Check player is not in a tutorial session
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT IS_PLAYER_ON_MP_INTRO()
	#ENDIF
		NET_PRINT("     ---------->     VEHICLE DROP - return 6 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PLAYER IN TUTORIAL SESSION ") NET_NL()
		RETURN 6
	ENDIF
	
	IF bBypassPVChecksForTruck
		IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - TRUCK IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	ELIF bBypassPVChecksForAvenger
		IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - AVENGER IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	ELIF bBypassPVChecksForHackerTruck
		IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - HACKER TRUCK IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	ELIF bBypassPVChecksForSubmarine
		IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - SUBMARINE IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	ELIF bBypassPVChecksForSubmarineDinghy
		IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - DINGHY IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	#IF FEATURE_DLC_2_2022
	ELIF bBypassPVChecksForAcidLab
		IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - ACID LAB IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	#ENDIF
	#IF FEATURE_DLC_2_2022
	ELIF bBypassPVChecksForSupportBike
		IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
			NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - SUPPORT BIKE IS NOT EMPTY ") NET_NL()
			RETURN 3
		ENDIF
	#ENDIF
	ELSE
		IF NOT bBypassChecksforPA
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NOT_EMPTY)
				NET_PRINT("     ---------->     VEHICLE DROP - : return 3 - SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT - PERSONAL VEHICLE IS NOT EMPTY ") NET_NL()
				RETURN 3
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	// Check player isn't in a property
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		NET_PRINT("     ---------->     VEHICLE DROP : return 4  - IS_PLAYER_IN_PROPERTY returned TRUE ") NET_NL()
		RETURN 4
	ENDIF
	IF bBypassPVChecksForAvenger
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP : return 4  - IS_PLAYER_IN_BUNKER returned TRUE ") NET_NL()
			RETURN 4
		ENDIF
		IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
			NET_PRINT("     ---------->     VEHICLE DROP : return 4  - IS_PLAYER_IN_IE_GARAGE returned TRUE ") NET_NL()
			RETURN 4
		ENDIF
	ENDIF
	
	// check player is close to the road network
	
	IF NOT IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP(vPlayerPosition)
	AND NOT bBypassPVChecksroad
	AND NOT bBypassChecksforPA
	AND NOT bBypassPVChecksForAvenger
	#IF FEATURE_HEIST_ISLAND
	AND NOT bBypassPVChecksForSubmarine
	AND NOT bBypassPVChecksForSubmarineDinghy
	#ENDIF
	AND NOT IS_PLAYER_LEAVING_SIMPLE_INTERIOR_USING_THE_VALET()
	#IF FEATURE_TUNER
	AND NOT IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
	#ENDIF
		NET_PRINT("     ---------->     VEHICLE DROP : return 4  - IS_POSITION_CLOSE_TO_VEHICLE_NODE_FOR_VEHICLE_DROP returned FALSE ") NET_NL()
		RETURN 4
		
	ENDIF
	
	#IF FEATURE_TUNER
	IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		IF iVehicleNumber >= 0
			IF NOT CAN_THIS_VEHICLE_MODEL_ENTER_CAR_MEET(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel)
			OR NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(g_MpSavedVehicles[iVehicleNumber].vehicleSetupMP.VehicleSetup.eModel, PROPERTY_OWNED_SLOT_CASINO_GARAGE)
				RETURN 99
			ENDIF
		ELIF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
		AND NOT IS_ENTITY_DEAD(PERSONAL_VEHICLE_ID())
			IF NOT CAN_THIS_VEHICLE_MODEL_ENTER_CAR_MEET(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()))
			OR NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()), PROPERTY_OWNED_SLOT_CASINO_GARAGE)
				RETURN 99
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	// make sure he's not calling from an inaccessible area
	IF IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA(vPlayerPosition, bBypassChecksforPA OR bBypassPVChecksForAvenger #IF FEATURE_HEIST_ISLAND OR bBypassPVChecksForSubmarine OR bBypassPVChecksForSubmarineDinghy #ENDIF)
	AND NOT bBypassChecksforPA
		NET_PRINT("     ---------->     VEHICLE DROP : return 4 - IS_POSITION_IN_VEHICLE_DROP_RESTRICTED_AREA check") NET_NL()
		RETURN 4
	ENDIF	
		
	NET_PRINT("     ---------->     VEHICLE DROP : return 0 ") NET_NL()
	RETURN 0
ENDFUNC

//PURPOSE: Controls an end phonecall from the contact
PROC PROCESS_VEHICLE_DROP_END_PHONECALL(VEHICLE_DROP_PERSONAL_DATA &VDPersonalData)
	//Move on once the phonecall is done
	IF IS_BIT_SET(VDPersonalData.iBitSet, VDP_PhonecallReceived)
	AND NOT IS_BIT_SET(VDPersonalData.iBitSet, VDP_PhonecallDone)
		IF NOT IS_PHONE_ONSCREEN()
			CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_PERSONAL_VEHICLE)
			//GIVE_LOCAL_PLAYER_FM_CASH(MPGlobalsAmbience.iVehicleDropCost, 1, TRUE, 0)
			//NETWORK_BUY_ITEM(MPGlobalsAmbience.iVehicleDropCost, GET_HASH_KEY(PVSpace), PURCHASE_CARDROPOFF)
			SET_BIT(VDPersonalData.iBitSet, VDP_PhonecallDone)
			VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_END_PHONECALL - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
		ENDIF
	ENDIF
	
	//Check the phonecall has been received
	IF IS_BIT_SET(VDPersonalData.iBitSet, VDP_EndPhonecallSetup)
	AND NOT IS_BIT_SET(VDPersonalData.iBitSet, VDP_PhonecallReceived)
		IF IS_PHONE_ONSCREEN()
			SET_BIT(VDPersonalData.iBitSet, VDP_PhonecallReceived)
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - END PHONECALL RECEIVED   <-----     ") NET_NL()
		ENDIF
	ENDIF
	
	//Do the phonecall
	IF NOT IS_BIT_SET(VDPersonalData.iBitSet, VDP_EndPhonecallSetup)
		ADD_PED_FOR_DIALOGUE(VDPersonalData.sSpeech, 3, NULL, "MECHANIC")
		IF Request_MP_Comms_Message(VDPersonalData.sSpeech, CHAR_MP_MECHANIC, "CT_AUD", "MPCT_11")	
			SET_BIT(VDPersonalData.iBitSet, VDP_EndPhonecallSetup)
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - END PHONECALL TRIGGERED   <-----     ") NET_NL()
		ENDIF
	ENDIF
ENDPROC

PROC DoPostCreation(VEHICLE_DROP_PERSONAL_DATA &VDPersonalData, BOOL bBypassPVChecksForTruck = FALSE, BOOL bBypassPVChecksForAvenger = FALSE, BOOL bBypassPVChecksForHackerTruck = FALSE 
					, BOOL bBypassPVChecksForSubmarine = FALSE, BOOL bBypassPVChecksForSubmarineDinghy = FALSE #IF FEATURE_DLC_2_2022 , BOOL bBypassPVChecksForSupportBike = FALSE, BOOL bBypassPVChecksForAcidLab = FALSE #ENDIF )
	#IF IS_DEBUG_BUILD
	PRINTLN("DoPostCreation called", 
			", bBypassPVChecksForTruck: ", GET_STRING_FROM_BOOL(bBypassPVChecksForTruck),
			", bBypassPVChecksForAvenger: ", GET_STRING_FROM_BOOL(bBypassPVChecksForAvenger),
			", bBypassPVChecksForHackerTruck: ", GET_STRING_FROM_BOOL(bBypassPVChecksForHackerTruck) 
			#IF FEATURE_HEIST_ISLAND , ", bBypassPVChecksForSubmarine: ", 		GET_STRING_FROM_BOOL(bBypassPVChecksForSubmarine) #ENDIF
			#IF FEATURE_HEIST_ISLAND , ", bBypassPVChecksForSubmarineDinghy: ", GET_STRING_FROM_BOOL(bBypassPVChecksForSubmarineDinghy) #ENDIF 
			#IF FEATURE_DLC_2_2022 ,   ", bBypassPVChecksForSupportBike = ",	GET_STRING_FROM_BOOL(bBypassPVChecksForSupportBike) #ENDIF )
	
	#IF FEATURE_DLC_2_2022
	PRINTLN("DoPostCreation called, bBypassPVChecksForAcidLab = ", bBypassPVChecksForAcidLab)
	#ENDIF
	#ENDIF
	
	IF bBypassPVChecksForTruck
		BOOL bRepairedTruckAndTrailerLarge
		bRepairedTruckAndTrailerLarge = TRUE
		
		//Repair Vehicle
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			SET_VEHICLE_FIXED(NET_TO_VEH(VDPersonalData.niVehicle)) 
			SET_ENTITY_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_DIRT_LEVEL(NET_TO_VEH(VDPersonalData.niVehicle), 0) 
			REMOVE_DECALS_FROM_VEHICLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_VEH(VDPersonalData.niVehicle))
		ELSE
			bRepairedTruckAndTrailerLarge = FALSE
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(VDPersonalData.niDriver))
			SET_VEHICLE_FIXED(NET_TO_VEH(VDPersonalData.niDriver))
			SET_TRAILER_LEGS_RAISED(NET_TO_VEH(VDPersonalData.niDriver))	//url:bugstar:3570795 - Truck Property -  Requesting the MOC in freemode and then driving it around will have the trailer scraping off the ground. Makes it difficult to move.
			SET_ENTITY_HEALTH(NET_TO_VEH(VDPersonalData.niDriver), 1000) 
			SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(VDPersonalData.niDriver), 1000) 
			SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(VDPersonalData.niDriver), 1000) 
			SET_VEHICLE_DIRT_LEVEL(NET_TO_VEH(VDPersonalData.niDriver), 0) 
			REMOVE_DECALS_FROM_VEHICLE(NET_TO_VEH(VDPersonalData.niDriver)) 
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_VEH(VDPersonalData.niDriver))
		ELSE
			bRepairedTruckAndTrailerLarge = FALSE
		ENDIF
		
		IF bRepairedTruckAndTrailerLarge
			VDPersonalData.iVehicleDropPersonalStage = VDP_DELIVERY
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - USE NEW PERSONAL Truck_0 - iVehicleDropPersonalStage = VDP_DELIVERY") NET_NL()
		ENDIF
	ELIF bBypassPVChecksForAvenger
		BOOL bRepairedAvengerLarge
		bRepairedAvengerLarge = TRUE
		
		SET_VEHICLE_AS_CURRENT_AVENGER_VEHICLE(NET_TO_VEH(VDPersonalData.niVehicle))
		//Repair Vehicle
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			SET_VEHICLE_FIXED(NET_TO_VEH(VDPersonalData.niVehicle)) 
			CONTROL_LANDING_GEAR(NET_TO_VEH(VDPersonalData.niVehicle), LGC_DEPLOY_INSTANT)
			SET_ENTITY_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_DIRT_LEVEL(NET_TO_VEH(VDPersonalData.niVehicle), 0) 
			REMOVE_DECALS_FROM_VEHICLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_VEH(VDPersonalData.niVehicle))
			SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(VDPersonalData.niVehicle))
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(VDPersonalData.niVehicle), TRUE)
		ELSE
			bRepairedAvengerLarge = FALSE
		ENDIF
		
		IF bRepairedAvengerLarge
			VDPersonalData.iVehicleDropPersonalStage = VDP_DELIVERY
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - USE NEW PERSONAL PLANE - iVehicleDropPersonalStage = VDP_DELIVERY") NET_NL()
		ENDIF
		
	ELIF bBypassPVChecksForHackerTruck
	OR bBypassPVChecksForSubmarine
	OR bBypassPVChecksForSubmarineDinghy
	#IF FEATURE_DLC_2_2022
	OR bBypassPVChecksForAcidLab
	OR bBypassPVChecksForSupportBike
	#ENDIF
		BOOL bRepairedHackerTruckLarge
		bRepairedHackerTruckLarge = TRUE
		
		//Repair Vehicle
		IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			SET_VEHICLE_FIXED(NET_TO_VEH(VDPersonalData.niVehicle)) 
			SET_ENTITY_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_DIRT_LEVEL(NET_TO_VEH(VDPersonalData.niVehicle), 0) 
			REMOVE_DECALS_FROM_VEHICLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_VEH(VDPersonalData.niVehicle))
		ELSE
			bRepairedHackerTruckLarge = FALSE
		ENDIF
		
		IF bRepairedHackerTruckLarge
			VDPersonalData.iVehicleDropPersonalStage = VDP_DELIVERY
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - USE NEW HACKER TRUCK - iVehicleDropPersonalStage = VDP_DELIVERY") NET_NL()
		ENDIF
	ELSE
		SET_LAST_USED_VEHICLE_SLOT(MPGlobalsAmbience.iVDPersonalVehicleSlot)
		
		IF SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE(NET_TO_VEH(VDPersonalData.niVehicle))
			//Repair Vehicle
			SET_VEHICLE_FIXED(NET_TO_VEH(VDPersonalData.niVehicle)) 
			SET_ENTITY_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_ENGINE_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_PETROL_TANK_HEALTH(NET_TO_VEH(VDPersonalData.niVehicle), 1000) 
			SET_VEHICLE_DIRT_LEVEL(NET_TO_VEH(VDPersonalData.niVehicle), 0) 
			REMOVE_DECALS_FROM_VEHICLE(NET_TO_VEH(VDPersonalData.niVehicle)) 
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_VEH(VDPersonalData.niVehicle))
			SET_LAST_USED_VEHICLE_SLOT(MPGlobalsAmbience.iVDPersonalVehicleSlot)
			MPGlobalsAmbience.VDPersonalVehicleModel = g_MpSavedVehicles[MPGlobalsAmbience.iVDPersonalVehicleSlot].vehicleSetupMP.VehicleSetup.eModel
			MP_SAVE_VEHICLE_CLEAR_CAR_IMPOUNDED(MPGlobalsAmbience.iVDPersonalVehicleSlot)
			SET_BIT(g_MpSavedVehicles[MPGlobalsAmbience.iVDPersonalVehicleSlot].iVehicleBS, MP_SAVED_VEHICLE_OUT_GARAGE)
			MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(MPGlobalsAmbience.iVDPersonalVehicleSlot,g_MpSavedVehicles[MPGlobalsAmbience.iVDPersonalVehicleSlot] ,TRUE)
			
			IF g_sMPTunables.bAllowMechanicSave
				REQUEST_SAVE(SSR_REASON_PV_MECHANIC_DELIVERY, STAT_SAVETYPE_IMMEDIATE_FLUSH)
			ENDIF
			
			VDPersonalData.iVehicleDropPersonalStage = VDP_DELIVERY
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - USE NEW PERSONAL VEHICLE - iVehicleDropPersonalStage = VDP_DELIVERY") NET_NL()
		ELSE
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - SET_VEHICLE_AS_CURRENT_PERSONAL_VEHICLE - IN PROGRESS") NET_NL()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BYPASS_CHECKS_FOR_SUBMARINE(BOOL &bBypassPVChecksForSubmarine)
	IF (g_SpawnData.bSpawningInSimpleInterior AND bBypassPVChecksForSubmarine)
		RETURN TRUE
	ENDIF
	#IF FEATURE_GEN9_EXCLUSIVE
	IF MPGlobalsAmbience.bSubSpawnForHeistDeeplink
		RETURN TRUE
	ENDIF
	IF g_sHeistDeeplinkStruct.eHeistType = eHEISTTYPE_ISLAND_HEIST
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the Personal Vehicle being delivered to the Player
PROC PROCESS_VEHICLE_DROP_PERSONAL(VEHICLE_DROP_PERSONAL_DATA &VDPersonalData,
		BOOL bBypassPVChecksForTruck = FALSE,
		BOOL bBypassChecksforPA = FALSE,
		BOOL bBypassPVChecksForAvenger = FALSE,
		BOOL bBypassPVChecksForHackerTruck = FALSE
		, BOOL bBypassPVChecksForSubmarine = FALSE
		, BOOL bBypassPVChecksForSubmarineDinghy = FALSE
		#IF FEATURE_DLC_2_2022
		, BOOL bBypassPVChecksForSupportBike = FALSE
		, BOOL bBypassPVChecksForAcidLab = FALSE
		#ENDIF )
	
	BOOL bCheckPlayerDead = TRUE
	BOOL bCheckPlayerInGameMode = TRUE
	
	//Cleanup if player enters tutorial session
	//IF NETWORK_IS_IN_TUTORIAL_SESSION()
	//	VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
	//	NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER IN TUTORIAL SESSION - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
	//ENDIF
	
	//Process Stages
	SWITCH VDPersonalData.iVehicleDropPersonalStage
		CASE VDP_SETUP
			INT iResult
			
			VECTOR vPlayerPos
			vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
			
			// ensure there is a valid node close by to deliver
			IF GET_CLOSE_VEHICLE_NODE(vPlayerPos, VDPersonalData.vDropLocation, VDPersonalData.iNode)
				PRINTLN(" VEHICLE DROP PROCESS_VEHICLE_DROP_PERSONAL MPGlobalsAmbience.iVDPersonalVehicleSlot ", MPGlobalsAmbience.iVDPersonalVehicleSlot)
				
				BYPASS_PERSONAL_VEHICLE_CHECKS_ENUM eBypassPVChecksForSpecialVeh
				eBypassPVChecksForSpecialVeh = BPVC_default
				IF bBypassPVChecksForTruck
					eBypassPVChecksForSpecialVeh = BPVC_TRUCK
				ENDIF
				IF bBypassPVChecksForAvenger
					eBypassPVChecksForSpecialVeh = BPVC_AVENGER
				ENDIF
				IF bBypassPVChecksForHackerTruck
					eBypassPVChecksForSpecialVeh = BPVC_HACKERTRUCK
				ENDIF
				IF  bBypassPVChecksForSubmarine
					eBypassPVChecksForSpecialVeh = BPVC_SUBMARINE
				ENDIF
				IF  bBypassPVChecksForSubmarineDinghy
					eBypassPVChecksForSpecialVeh = BPVC_SUBMARINE_DINGHY
				ENDIF
				#IF FEATURE_DLC_2_2022
				IF bBypassPVChecksForAcidLab
					eBypassPVChecksForSpecialVeh = BPVC_ACIDLAB
				ENDIF
				IF bBypassPVChecksForSupportBike
					eBypassPVChecksForSpecialVeh = BPVC_SUPPORT_BIKE
				ENDIF
				#ENDIF
				BOOL bBypassPVChecksroad
				bBypassPVChecksroad = bBypassPVChecksForTruck OR bBypassPVChecksForAvenger OR bBypassPVChecksForHackerTruck #IF FEATURE_DLC_2_2022 OR bBypassPVChecksForAcidLab #ENDIF
				#IF FEATURE_HEIST_ISLAND
				bBypassPVChecksroad = bBypassPVChecksroad OR bBypassPVChecksForSubmarine OR bBypassPVChecksForSubmarineDinghy
				#ENDIF
				
				iResult = SAFE_TO_USE_PERSONAL_VEHICLE_WITH_INT(MPGlobalsAmbience.iVDPersonalVehicleSlot, TRUE, DEFAULT, DEFAULT, DEFAULT, 
																eBypassPVChecksForSpecialVeh, bBypassPVChecksroad, bBypassChecksforPA)
				
				IF iResult = 0
				//OR iResult = 8
					//IF MPGlobalsAmbience.VDPersonalVehicleModel != g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel
						/*IF NETWORK_DOES_NETWORK_ID_EXIST(VDPersonalData.niVehicle)
							CLEANUP_NET_ID(VDPersonalData.niVehicle)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - CLEANUP_NET_ID - VDPersonalData.niVehicle") NET_NL()
						ENDIF*/
						
						//TWH - RLB - Rowan - made it so if you call a new car and your old one is not impounded it is put back into the garage (PT 1434710)
						
						IF NOT bBypassPVChecksForTruck
						AND NOT bBypassPVChecksForAvenger
						AND NOT bBypassPVChecksForHackerTruck
						AND NOT bBypassPVChecksForSubmarine
						AND NOT bBypassPVChecksForSubmarineDinghy
						#IF FEATURE_DLC_2_2022
						AND NOT bBypassPVChecksForAcidLab
						AND NOT bBypassPVChecksForSupportBike
						#ENDIF
							IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
								IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
									SET_SAVE_VEHICLE_AS_DESTROYED(CURRENT_SAVED_VEHICLE_SLOT())
								ELSE
									IF CURRENT_SAVED_VEHICLE_SLOT() > -1 
										CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE -12cleared on #",CURRENT_SAVED_VEHICLE_SLOT())
										IF IS_MODEL_A_PERSONAL_TRAILER(g_MpSavedVehicles[MPGlobalsAmbience.iVDPersonalVehicleSlot].vehicleSetupMP.VehicleSetup.eModel)
											IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].vehicleSetupMP.VehicleSetup.eModel)
												PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE: Cleaning up requested parking to hangar, player no longer valid")
												SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS,REMOTE_STORED_VEHICLE_BS_RETURNED_TO_HANGAR)
											ELSE
												PRINTLN("[personal_vehicle] MP_SAVED_VEHICLE_OUT_GARAGE: Cleaning up requested parking to garage, player no longer valid")
												SET_BIT(mpPropMaintain.remoteStoredPersVeh.iLocalFlagsBS,REMOTE_STORED_VEHICLE_BS_RETURNED_TO_GARAGE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							CLEANUP_MP_SAVED_VEHICLE(FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
							CLEAR_BIT(g_TransitionSessionNonResetVars.MissionPVBitset, ciSPAWN_PV_IN_ORIGINAL_LOC)
							
							IF NETWORK_DOES_NETWORK_ID_EXIST(VDPersonalData.niDriver)
							AND (GET_ENTITY_MODEL(NET_TO_ENT(VDPersonalData.niDriver)) = SADLER)
								NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - delete old sadler") NET_NL()
								DELETE_NET_ID(VDPersonalData.niDriver)
							ENDIF
							
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - CLEANUP_MP_SAVED_VEHICLE called") NET_NL()
						ENDIF
						
						IF bBypassPVChecksForTruck
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
						ENDIF
						IF bBypassPVChecksForAvenger
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
						ENDIF
						IF bBypassPVChecksForHackerTruck
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
						ENDIF
						IF bBypassPVChecksForSubmarine
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_6_SUBMARINE)
						ENDIF
						IF bBypassPVChecksForSubmarineDinghy
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_7_SUBMARINE_DINGHY)
						ENDIF
						#IF FEATURE_DLC_2_2022
						IF bBypassPVChecksForAcidLab
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
						ENDIF
						IF  bBypassPVChecksForSupportBike
							SET_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_9_SUPPORT_BIKE)
						ENDIF
						#ENDIF 
						SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_PHONE_MECH_DROP_CAR, TRUE)
						VDPersonalData.iVehicleDropPersonalStage = VDP_CREATION
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - iVehicleDropPersonalStage = VDP_CREATION") NET_NL()
					//ENDIF				
				ELSE
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - NOT SAFE TO USE PERSONAL VEHICLE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELSE
				// 3 node grab attempts
				IF VDPersonalData.iNode > 3
					iResult = 4	// cancel saying too far from road network
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					PRINT_HELP("VD_FAIL4")
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - NOT SAFE TO USE PERSONAL VEHICLE - GET_CLOSE_VEHICLE_NODE falled iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF				
			ENDIF			
		BREAK
		
		CASE VDP_CREATION
			IF SHOULD_BYPASS_CHECKS_FOR_SUBMARINE(bBypassPVChecksForSubmarine)
				bCheckPlayerDead = FALSE
				bCheckPlayerInGameMode = FALSE
			ENDIF
			
			IF IS_NET_PLAYER_OK(PLAYER_ID(), bCheckPlayerDead, bCheckPlayerInGameMode)
				IF (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID <= 0
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty <= 0
				AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID()))
				OR SHOULD_BYPASS_CHECKS_FOR_SUBMARINE(bBypassPVChecksForSubmarine)
					IF HAS_NET_TIMER_EXPIRED(VDPersonalData.iDeliveryDelayTimer, VD_DELIVERY_DELAY)
					OR IS_PLAYER_LEAVING_SIMPLE_INTERIOR_USING_THE_VALET()
					OR (bBypassPVChecksForSubmarineDinghy)
					OR SHOULD_BYPASS_CHECKS_FOR_SUBMARINE(bBypassPVChecksForSubmarine)
					#IF FEATURE_TUNER
					OR (HAS_PLAYER_STARTED_WARP_INTO_CAR_MEET_CAR_PARK() AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP())
					OR (HAS_PLAYER_STARTED_WARP_INTO_PRIVATE_CAR_MEET_CAR_PARK() AND NOT IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP())
					#ENDIF
					#IF IS_DEBUG_BUILD OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) #ENDIF
						IF (NOT bBypassPVChecksForTruck 
							AND NOT bBypassPVChecksForAvenger 
							AND NOT bBypassPVChecksForHackerTruck 
							#IF FEATURE_HEIST_ISLAND 
							AND NOT bBypassPVChecksForSubmarine 
							AND NOT bBypassPVChecksForSubmarineDinghy 
							#ENDIF 
							#IF FEATURE_DLC_2_2022
							AND NOT bBypassPVChecksForSupportBike
							AND NOT bBypassPVChecksForAcidLab
							#ENDIF
							AND MPGlobalsAmbience.iVDPersonalVehicleSlot > -1 
							AND CREATE_VEHICLE_AND_DRIVER( VDPersonalData.niVehicle, VDPersonalData.niDriver, g_MpSavedVehicles[MPGlobalsAmbience.iVDPersonalVehicleSlot].vehicleSetupMP.VehicleSetup.eModel, VDPersonalData.iBitSet, VDP_PathsLoaded,  VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading)
							)
						OR (bBypassPVChecksForTruck AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_PLAYERS_COMBINED_GUNRUNNING_TRUCK_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						OR (bBypassPVChecksForAvenger AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_ARMORY_AIRCRAFT_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						OR (bBypassPVChecksForHackerTruck AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_HACKER_TRUCK_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						OR (bBypassPVChecksForSubmarine AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_SUBMARINE_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						OR (bBypassPVChecksForSubmarineDinghy AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_SUBMARINE_DINGHY_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						#IF FEATURE_DLC_2_2022
						OR (bBypassPVChecksForAcidLab AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_ACID_LAB_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						OR (bBypassPVChecksForSupportBike AND CREATE_VEHICLE_AND_DRIVER(VDPersonalData.niVehicle, VDPersonalData.niDriver, GET_SUPPORT_BIKE_MODEL(), VDPersonalData.iBitSet, VDP_PathsLoaded, VDPersonalData.vDropLocation, VDPersonalData.iNode, VDPersonalData.vCreatePos, VDPersonalData.fCreateHeading))
						#ENDIF
							IF TAKE_CONTROL_OF_NET_ID(VDPersonalData.niVehicle)	
								DoPostCreation(VDPersonalData,
												bBypassPVChecksForTruck,
												bBypassPVChecksForAvenger,
												bBypassPVChecksForHackerTruck
												#IF FEATURE_HEIST_ISLAND ,
												bBypassPVChecksForSubmarine,
												bBypassPVChecksForSubmarineDinghy
												#ENDIF
												#IF FEATURE_DLC_2_2022, 
												bBypassPVChecksForSupportBike,
												bBypassPVChecksForAcidLab
												#ENDIF
												)
							ELSE
								VDPersonalData.iVehicleDropPersonalStage = VDP_POST_CREATION
							ENDIF
						ELSE
							IF HAS_NET_TIMER_EXPIRED(VDPersonalData.iEndCallTimer, VD_END_CALL_DELAY)
								VDPersonalData.iVehicleDropPersonalStage = VDP_END_CALL
								NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - TOO LONG TO CREATE - iVehicleDropPersonalStage = VDP_END_CALL") NET_NL()
							ELSE
								NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - CREATE_VEHICLE_AND_DRIVER - PERSONAL - IN PROGRESS") NET_NL()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF bBypassPVChecksForTruck
						CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
						SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
					ENDIF
					IF bBypassPVChecksForAvenger
						CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
						SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ARMORY_AIRCRAFT)
					ENDIF
					IF bBypassPVChecksForHackerTruck
						CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
						SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
					ENDIF
					
					IF bBypassPVChecksForSubmarine
						CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
						SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
					ENDIF
					IF bBypassPVChecksForSubmarineDinghy
						CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
					ENDIF
					#IF FEATURE_DLC_2_2022
					IF bBypassPVChecksForAcidLab
						CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
						SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
					ENDIF
					IF bBypassPVChecksForSupportBike
						CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
						CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
					ENDIF
					#ENDIF
					
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER IN OR ENTERING PROPERTY - A ")
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID > 0
						NET_PRINT(" iEnteringPropertyID = ")
						NET_PRINT_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID)
					ENDIF
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
						NET_PRINT(" iCurrentlyInsideProperty = ")
						NET_PRINT_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					ENDIF
					IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
						NET_PRINT(" walking into simple interior ")
					ENDIF
					NET_PRINT(" - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELSE
				IF bBypassPVChecksForTruck
					CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
				ENDIF
				IF bBypassPVChecksForAvenger
					CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ARMORY_AIRCRAFT)
				ENDIF
				IF bBypassPVChecksForHackerTruck
					CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
				ENDIF
				IF bBypassPVChecksForSubmarine
					CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
				ENDIF
				IF bBypassPVChecksForSubmarineDinghy
					CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
				ENDIF
				#IF FEATURE_DLC_2_2022
				IF bBypassPVChecksForAcidLab
					CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
				ENDIF
				IF bBypassPVChecksForSupportBike
					CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
				ENDIF
				#ENDIF
				
				VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER NOT OKAY - A - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
			ENDIF
		BREAK
		
		CASE VDP_POST_CREATION
			IF SHOULD_BYPASS_CHECKS_FOR_SUBMARINE(bBypassPVChecksForSubmarine)
				bCheckPlayerDead = FALSE
				bCheckPlayerInGameMode = FALSE
			ENDIF
			
			IF IS_NET_PLAYER_OK(PLAYER_ID(), bCheckPlayerDead, bCheckPlayerInGameMode)
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID <= 0
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty <= 0
				AND NOT IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		
					IF TAKE_CONTROL_OF_NET_ID(VDPersonalData.niVehicle)
						DoPostCreation(VDPersonalData
										, bBypassPVChecksForTruck
										, bBypassPVChecksForAvenger
										, bBypassPVChecksForHackerTruck
										#IF FEATURE_HEIST_ISLAND
										, bBypassPVChecksForSubmarine
										, bBypassPVChecksForSubmarineDinghy
										#ENDIF
										#IF FEATURE_DLC_2_2022
										, bBypassPVChecksForSupportBike
										, bBypassPVChecksForAcidLab
										#ENDIF
										)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(VDPersonalData.iEndCallTimer, VD_END_CALL_DELAY)
							VDPersonalData.iVehicleDropPersonalStage = VDP_END_CALL
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - VDP_POST_CREATION - PROCESS_VEHICLE_DROP_PERSONAL - TOO LONG TO CREATE - iVehicleDropPersonalStage = VDP_END_CALL") NET_NL()
						ELSE
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - VDP_POST_CREATION - PROCESS_VEHICLE_DROP_PERSONAL - CREATE_VEHICLE_AND_DRIVER - PERSONAL - IN PROGRESS") NET_NL()
						ENDIF			
					ENDIF
				ELSE
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - VDP_POST_CREATION - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER IN OR ENTERING PROPERTY - A - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELSE
				IF bBypassPVChecksForTruck
					CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
				ENDIF
				IF bBypassPVChecksForAvenger
					CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ARMORY_AIRCRAFT)
				ENDIF
				IF bBypassPVChecksForHackerTruck
					CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
				ENDIF
				IF bBypassPVChecksForSubmarine
					CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
				ENDIF
				IF bBypassPVChecksForSubmarineDinghy
					CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
				ENDIF
				#IF FEATURE_DLC_2_2022
				IF bBypassPVChecksForAcidLab
					CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
				ENDIF
				IF bBypassPVChecksForSupportBike
					CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
				ENDIF
				#ENDIF
				
				VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - VDP_POST_CREATION - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER NOT OKAY - A - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
			ENDIF
		BREAK
		
		CASE VDP_DELIVERY
			IF SHOULD_BYPASS_CHECKS_FOR_SUBMARINE(bBypassPVChecksForSubmarine)
				bCheckPlayerDead = FALSE
				bCheckPlayerInGameMode = FALSE
			ENDIF
			
			IF bBypassPVChecksForAvenger
				IF IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_AVENGER_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELIF bBypassPVChecksForTruck
				IF IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_TRUCK_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELIF bBypassPVChecksForHackerTruck
				IF IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELIF bBypassPVChecksForSubmarine
				IF IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELIF bBypassPVChecksForSubmarineDinghy
				IF IS_MP_SAVED_SUBMARINE_DINGHY_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_SUBMARINE_DINGHY_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			#IF FEATURE_DLC_2_2022
			ELIF bBypassPVChecksForAcidLab
				IF IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			ELIF bBypassPVChecksForSupportBike
				IF IS_MP_SAVED_SUPPORT_BIKE_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_SUPPORT_BIKE_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF
			#ENDIF
			ELSE
				IF IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP()
					VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
					NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_MP_SAVED_VEHICLE_BEING_CLEANED_UP = TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
				ENDIF	
			ENDIF
			
			IF IS_NET_PLAYER_OK(PLAYER_ID(), bCheckPlayerDead, bCheckPlayerInGameMode)
				NETWORK_INDEX niPersonalVeh
				VEHICLE_INDEX personalVehIndex
				
				IF bBypassPVChecksForTruck
					personalVehIndex = MPGlobalsAmbience.vehTruckVehicle[0]
				ELIF bBypassPVChecksForAvenger
					personalVehIndex = MPGlobalsAmbience.vehAvengerVehicle
				ELIF bBypassPVChecksForHackerTruck
					personalVehIndex = MPGlobalsAmbience.vehHackerTruck
				ELIF bBypassPVChecksForSubmarine
					personalVehIndex = MPGlobalsAmbience.vehSubmarine
				ELIF bBypassPVChecksForSubmarineDinghy
					personalVehIndex = MPGlobalsAmbience.vehSubmarineDinghy
				#IF FEATURE_DLC_2_2022
				ELIF bBypassPVChecksForAcidLab
					personalVehIndex = MPGlobalsAmbience.vehAcidLab
				ELIF bBypassPVChecksForSupportBike
					personalVehIndex = MPGlobalsAmbience.vehSupportBike
				#ENDIF
				ELSE
					niPersonalVeh = PERSONAL_VEHICLE_NET_ID(TRUE)
				ENDIF
				
				IF bBypassPVChecksForAvenger
				OR bBypassPVChecksForTruck
				OR bBypassPVChecksForHackerTruck
				OR bBypassPVChecksForSubmarine
				OR bBypassPVChecksForSubmarineDinghy
				#IF FEATURE_DLC_2_2022
				OR bBypassPVChecksForAcidLab
				OR bBypassPVChecksForSupportBike
				#ENDIF
					IF IS_VEHICLE_DRIVEABLE(personalVehIndex)
						//CONTROL_DRIVER_DIALOGUE(VDPersonalData.iBitSet, VDPersonalData.niDriver, VDPersonalData.niVehicle, VDPersonalData.speechStruct)//, VDPersonalData.vDropLocation)//, VDPersonalData.niVehicle)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), personalVehIndex)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER IN VEHICLE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), personalVehIndex, <<VD_VEHICLE_MAX_SPAWN_RANGE*2, VD_VEHICLE_MAX_SPAWN_RANGE*2, VD_VEHICLE_MAX_SPAWN_RANGE*2>>)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER FAR FROM VEHICLE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF IS_VEHICLE_EMPTY(personalVehIndex, TRUE)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_VEHICLE_EMPTY TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						
					ELSE
						IF bBypassPVChecksForAvenger
							CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ARMORY_AIRCRAFT)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF bBypassPVChecksForHackerTruck
							CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF bBypassPVChecksForSubmarine
							CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF bBypassPVChecksForSubmarineDinghy
							CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						#IF FEATURE_DLC_2_2022
						ELIF bBypassPVChecksForAcidLab
							CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF bBypassPVChecksForSupportBike
							CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						#ENDIF
						ELSE
							CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
					ENDIF	
				ELSE  
					IF IS_NET_VEHICLE_DRIVEABLE(niPersonalVeh)
						//CONTROL_DRIVER_DIALOGUE(VDPersonalData.iBitSet, VDPersonalData.niDriver, VDPersonalData.niVehicle, VDPersonalData.speechStruct)//, VDPersonalData.vDropLocation)//, VDPersonalData.niVehicle)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(niPersonalVeh))
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER IN VEHICLE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), NET_TO_VEH(niPersonalVeh), <<VD_VEHICLE_MAX_SPAWN_RANGE*2, VD_VEHICLE_MAX_SPAWN_RANGE*2, VD_VEHICLE_MAX_SPAWN_RANGE*2>>)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER FAR FROM VEHICLE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ELIF IS_VEHICLE_EMPTY(NET_TO_VEH(niPersonalVeh), TRUE)
							VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_VEHICLE_EMPTY TRUE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
					ELSE 
						IF bBypassPVChecksForTruck
							CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						IF bBypassPVChecksForHackerTruck
							CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						IF bBypassPVChecksForSubmarine
							CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						IF bBypassPVChecksForSubmarineDinghy
							CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						#IF FEATURE_DLC_2_2022
						IF bBypassPVChecksForAcidLab
							CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
							SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						IF bBypassPVChecksForSupportBike
							CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
							CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
							NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - IS_NET_VEHICLE_DRIVEABLE(") NET_PRINT_INT(NATIVE_TO_INT(niPersonalVeh)) NET_PRINT(") FALSE - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
						ENDIF
						#ENDIF
						
						VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - vehicle is not driveable - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()

						
					ENDIF
				ENDIF
			ELSE
				IF bBypassPVChecksForTruck
					CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
				ENDIF
				IF bBypassPVChecksForAvenger
					CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ARMORY_AIRCRAFT)
				ENDIF
				IF bBypassPVChecksForHackerTruck
					CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
				ENDIF
				IF bBypassPVChecksForSubmarine
					CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
				ENDIF
				IF bBypassPVChecksForSubmarineDinghy
					CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
				ENDIF
				#IF FEATURE_DLC_2_2022
				IF bBypassPVChecksForAcidLab
					CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
				ENDIF
				IF bBypassPVChecksForSupportBike
					CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
				ENDIF
				#ENDIF
				VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
				NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - PLAYER NOT OKAY - B - iVehicleDropPersonalStage = VDP_CLEANUP") NET_NL()
			ENDIF
		BREAK
		
		CASE VDP_END_CALL
			PROCESS_VEHICLE_DROP_END_PHONECALL(VDPersonalData)
			
			IF VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
			AND bBypassPVChecksForTruck
				CLEANUP_MP_SAVED_TRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
				SET_REQUESTED_TRUCK_IN_FREEMODE(FALSE)
				CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_GUNRUNNING_TRUCK)
			ENDIF
			IF VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
			AND bBypassPVChecksForAvenger
				CLEANUP_MP_SAVED_AVENGER(FALSE, FALSE, TRUE, FALSE, TRUE)
				SET_REQUESTED_AVENGER_IN_FREEMODE(FALSE)
				CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ARMORY_AIRCRAFT)
			ENDIF
			IF VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
			AND bBypassPVChecksForHackerTruck
				CLEANUP_MP_SAVED_HACKERTRUCK(FALSE, FALSE, TRUE, FALSE, TRUE)
				SET_REQUESTED_HACKERTRUCK_IN_FREEMODE(FALSE)
				CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_HACKER_TRUCK)
			ENDIF
			IF bBypassPVChecksForSubmarine
				CLEANUP_MP_SAVED_SUBMARINE(FALSE, FALSE, TRUE, FALSE, TRUE)
				SET_REQUESTED_SMPL_INT_SUBMARINE_IN_FREEMODE(FALSE)
				CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE)
			ENDIF
			IF bBypassPVChecksForSubmarineDinghy
				CLEANUP_MP_SAVED_SUBMARINE_DINGHY(FALSE, FALSE, TRUE, FALSE, TRUE)
				CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUBMARINE_DINGHY)
			ENDIF
			#IF FEATURE_DLC_2_2022
			IF VDPersonalData.iVehicleDropPersonalStage = VDP_CLEANUP
				IF bBypassPVChecksForAcidLab
					CLEANUP_MP_SAVED_ACIDLAB(FALSE, FALSE, TRUE, FALSE, TRUE)
					SET_REQUESTED_ACID_LAB_IN_FREEMODE(FALSE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_ACID_LAB)
				ENDIF
				IF bBypassPVChecksForSupportBike
					CLEANUP_MP_SAVED_SUPPORT_BIKE(FALSE, FALSE, TRUE, FALSE, TRUE)
					CONTACT_REQUEST_CLEAR_LOCAL_CD_TIMER(REQUEST_SUPPORT_BIKE)
				ENDIF
			ENDIF
			#ENDIF
		BREAK
		
		CASE VDP_CLEANUP
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(VDPersonalData.niDriver)
				IF IS_MODEL_A_PED(GET_ENTITY_MODEL(NET_TO_ENT(VDPersonalData.niDriver)))
					IF NOT IS_NET_PED_INJURED(VDPersonalData.niDriver)
						NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - Setting driver to keep tasks and then releasing him") NET_NL()
						IF IS_PED_IN_ANY_VEHICLE(NET_TO_PED(VDPersonalData.niDriver))
							TASK_LEAVE_ANY_VEHICLE(NET_TO_PED(VDPersonalData.niDriver), 0, ECF_RESUME_IF_INTERRUPTED)
						ELSE
							RESET_PED_LAST_VEHICLE(NET_TO_PED(VDPersonalData.niDriver))		// 1636215 - Stop Mechanic considering the player's car as their last vehicle
						ENDIF
						SET_PED_COMBAT_ATTRIBUTES(NET_TO_PED(VDPersonalData.niDriver), CA_USE_VEHICLE, FALSE) 	// B*1612005 - Ensure mechanic doesn't get back in the player's car if in combat
						SET_PED_FLEE_ATTRIBUTES(NET_TO_PED(VDPersonalData.niDriver), FA_USE_VEHICLE, FALSE) 	// B*1612005 - Ensure mechanic doesn't get back in the player's car if fleeing
						SET_PED_KEEP_TASK(NET_TO_PED(VDPersonalData.niDriver), TRUE) // B*1561482
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NET_TO_PED(VDPersonalData.niDriver), FALSE)
					ENDIF
					CLEANUP_NET_ID(VDPersonalData.niDriver)
				ELIF (GET_ENTITY_MODEL(NET_TO_ENT(VDPersonalData.niDriver)) = SADLER)
					CLEANUP_NET_ID(VDPersonalData.niDriver)
				ENDIF
			ENDIF
			//CLEAR_BIT(VDPersonalData.iBitSet, VDP_DialogueDone)
			//CLEAR_BIT(VDPersonalData.iBitSet, VDP_ClearAreaDone)
			VDPersonalData.iBitSet = 0
			VDPersonalData.vCreatePos = <<0, 0, 0>>
			VDPersonalData.fCreateHeading = 0
			MPGlobalsAmbience.bLaunchVehicleDropPersonal = FALSE
			IF bBypassPVChecksForTruck
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_3_TRUCK)
			ENDIF
			MPGlobalsAmbience.bLaunchVehicleDropTruck = FALSE
			MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft = FALSE
			IF bBypassPVChecksForAvenger
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_4_AIRCRAFT)
			ENDIF
			MPGlobalsAmbience.bLaunchVehicleDropAvenger  = FALSE
			IF bBypassPVChecksForHackerTruck
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_5_HACKERTRUCK)
			ENDIF
			IF bBypassPVChecksForSubmarine
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_6_SUBMARINE)
			ENDIF
			IF bBypassPVChecksForSubmarineDinghy AND CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_7_SUBMARINE_DINGHY)
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_7_SUBMARINE_DINGHY)
				VDPersonalData.niVehicle = NULL
			ENDIF
			MPGlobalsAmbience.bLaunchVehicleDropSubmarine = FALSE
			MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy = FALSE
			#IF FEATURE_DLC_2_2022
			IF bBypassPVChecksForAcidLab
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_8_ACIDLAB)
			ENDIF
			
			MPGlobalsAmbience.bLaunchVehicleDropAcidLab = FALSE

			IF bBypassPVChecksForSupportBike AND CAN_SPAWN_BOOKED_VEHICLE(eVeh_spawn_9_SUPPORT_BIKE)
				CLEAR_SPAWNING_BOOKED_VEHICLE(eVeh_spawn_9_SUPPORT_BIKE)
				VDPersonalData.niVehicle = NULL
			ENDIF
			MPGlobalsAmbience.bLaunchVehicleDropSupportBike = FALSE
			#ENDIF
			
			#IF FEATURE_GEN9_EXCLUSIVE
			MPGlobalsAmbience.bSubSpawnForHeistDeeplink = FALSE
			#ENDIF
			MPGlobalsAmbience.bLaunchVehicleDropHackerTruck = FALSE
			MPGlobalsAmbience.bRequestingVehicleDropPersonal = FALSE
			MPGlobalsAmbience.VDPersonalVehicleModel = DUMMY_MODEL_FOR_SCRIPT
			IF (MPGlobalsAmbience.bRequestedImpoundByPA)
				BROADCAST_TELL_SERVER_CONTACT_REQUEST_COMPLETE(ALL_PLAYERS(),REQUEST_PERSONAL_ASSISTANT_IMPOUND_RECOVERY,INVALID_PLAYER_INDEX())
			ENDIF
			MPGlobalsAmbience.bRequestedImpoundByPA = FALSE
			VDPersonalData.iVehicleDropPersonalStage = VDP_SETUP
			RESET_NET_TIMER(VDPersonalData.iEndCallTimer)
			RESET_NET_TIMER(VDPersonalData.iDeliveryDelayTimer)
			MPGlobalsAmbience.iVDPersonalVehicleSlot = -1							
			NET_PRINT_TIME() NET_PRINT("     ---------->     VEHICLE DROP - PROCESS_VEHICLE_DROP_PERSONAL - VDP_CLEANUP - DONE") NET_NL()
		BREAK
	ENDSWITCH

ENDPROC

PROC PROCESS_TRUCK_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = " Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "removed"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempvehTrailer 
	
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF DOES_ENTITY_EXIST(g_viGunRunTailerInBunker)
			tempvehTrailer = g_viGunRunTailerInBunker
		ELSE
			PRINTLN("[PROCESS_TRUCK_DROP_BLIP] - g_viGunRunTailerInBunker doesn't exist ")
			EXIT
		ENDIF
	ELSE	
		tempvehTrailer = MPGlobalsAmbience.vehTruckVehicle[1]
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(tempvehTrailer)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
		tempvehTrailer = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
	ENDIF
	IF NOT DOES_ENTITY_EXIST(tempvehTrailer)
	OR IS_ENTITY_DEAD(tempvehTrailer)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempvehTrailer)
				PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as vehicle does not exist")
			ELIF IS_ENTITY_DEAD(tempvehTrailer)
				PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as vehicle is dead")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		EXIT
	ENDIF
	
	ENTITY_INDEX tempEntity = GET_ENTITY_ATTACHED_TO(tempvehTrailer)
	
	IF DOES_ENTITY_EXIST(tempEntity)
	AND IS_ENTITY_A_VEHICLE(tempEntity)
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempEntity))

		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(tempEntity)))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_TRUCK(PLAYER_ID(), tl63Reason)
		OR IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
		OR IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempvehTrailer)

			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_TRUCK(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as player in armory Truck_0")
				ELIF IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as mp saved Truck_0 being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempvehTrailer)
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				blipID = ADD_BLIP_FOR_ENTITY(tempvehTrailer)
				
				SET_BLIP_SPRITE(blipID, RADAR_TRACE_GR_COVERT_OPS)
				
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"GRTRUCK")
				
				VECTOR vBlipCoords = GET_ENTITY_COORDS(tempvehTrailer)
				PRINTLN("[PROCESS_TRUCK_DROP_BLIP]  Adding vehicle blip ", vBlipCoords)
			
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempvehTrailer = GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_TRUCK_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as is wrong type, removing.")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempvehTrailer)))	
				ENDIF
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_BLIP_FLASHES(blipID, TRUE)
					SET_BLIP_FLASH_TIMER(blipID, 7000)
					CLEAR_SAVED_TRUCK_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] started flashing pv blip")
					
					MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_TRUCK_BLIP = TRUE
				ENDIF
				
				BOOL bDisplayedGR_TRUCK_BLIP = FALSE
				IF MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_TRUCK_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_TRUCK_BLIP = FALSE
					bDisplayedGR_TRUCK_BLIP = TRUE
				ENDIF
				
				IF NOT bDisplayedGR_TRUCK_BLIP
				AND NOT MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_TRUCK_BLIP_G
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF VDIST(vPlayerPos, vBlipPos) <= 30.0
						PRINT_HELP("GR_TRUCK_BLIP_G")
					ENDIF
					MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_TRUCK_BLIP_G = TRUE
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerTruckBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_TRUCK_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerTruckBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerTruckBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_TRUCK_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerTruckBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerTruckBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_TRUCK_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF (tempvehTrailer = g_viGunRunTailerInBunker)
					display = DISPLAY_RADAR_ONLY
				ENDIF
				
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_TRUCK_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF

		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_AVENGER_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = " Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "removed"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempveh
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		IF DOES_ENTITY_EXIST(g_viGangOpsAvengerInDefunctBase)
			tempveh = g_viGangOpsAvengerInDefunctBase
		ELSE
			//PRINTLN("[PROCESS_AVENGER_DROP_BLIP] - g_viGangOpsAvengerInDefunctBase doesn't exist ")
			EXIT
		ENDIF
	ELSE	
		tempveh = MPGlobalsAmbience.vehAvengerVehicle
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(tempveh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
		tempveh= NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(tempveh)
	OR IS_ENTITY_DEAD(tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempveh)
				PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as vehicle does not exist")
			ELIF IS_ENTITY_DEAD(tempveh)
				PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as vehicle is dead")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(tempveh)
	AND NOT IS_ENTITY_DEAD(tempveh)
	AND IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_AVENGER(PLAYER_ID(), tl63Reason)
		OR IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
		OR IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempveh)
			
			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_AVENGER(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as player in armory aircraft")
				ELIF IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as mp saved aircraft being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempveh)
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				blipID = ADD_BLIP_FOR_ENTITY(tempveh)
				
				SET_BLIP_SPRITE(blipID, GET_CORRECT_PED_BLIP_SPRITE_FOR_VEHICLE_MODEL(AVENGER))
				SET_BLIP_SCALE(blipID, GET_BASE_SCALE_FOR_BLIP_SPRITE(GET_BLIP_SPRITE(blipID)))
				
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"GRPLANE")
				
				VECTOR vBlipCoords = GET_ENTITY_COORDS(tempveh)
				
				BOOL bDebugInfo = FALSE
				IF (tempveh = g_viGangOpsAvengerInDefunctBase)
					IF NOT bDebugInfo
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords, " for g_viGangOpsAvengerInDefunctBase")
						bDebugInfo = TRUE
					ELSE
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords, " for g_viGangOpsAvengerInDefunctBase as well...")
					ENDIF
				ENDIF
				IF (tempveh = MPGlobalsAmbience.vehAvengerVehicle)
					IF NOT bDebugInfo
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords, " for MPGlobalsAmbience.vehAvengerVehicle")
						bDebugInfo = TRUE
					ELSE
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords, " for MPGlobalsAmbience.vehAvengerVehicle as well...")
					ENDIF
				ENDIF
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)
				AND (tempveh = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
					IF NOT bDebugInfo
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords, " for GlobalplayerBD[", get_player_name(PLAYER_ID()), "].netID_AvengerV")
						bDebugInfo = TRUE
					ELSE
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords, " for GlobalplayerBD[", get_player_name(PLAYER_ID()), "].netID_AvengerV as well...")
					ENDIF
				ENDIF
				IF NOT bDebugInfo
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  Adding vehicle blip ", vBlipCoords)
					bDebugInfo = TRUE
				ENDIF
				UNUSED_PARAMETER(vBlipCoords)
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempveh= GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as is wrong type, removing.")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
				ENDIF
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_AVENGER_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_BLIP_FLASHES(blipID, TRUE)
					SET_BLIP_FLASH_TIMER(blipID, 7000)
					CLEAR_SAVED_AVENGER_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] started flashing pv blip")
					
					MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_PLANE_BLIP = TRUE
				ENDIF
				
				BOOL bDisplayedGR_PLANE_BLIP = FALSE
				IF MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_PLANE_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					PRINT_HELP("GR_PLANE_BLIP")
					MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_PLANE_BLIP = FALSE
					bDisplayedGR_PLANE_BLIP = TRUE
				ENDIF
				
				IF NOT bDisplayedGR_PLANE_BLIP
				AND NOT MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_PLANE_BLIP_G
				AND SAFE_TO_PRINT_PV_HELP()
					IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					OR IS_VEHICLE_ALLOWED_IN_ARMORY_WORKSHOP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
						IF VDIST(vPlayerPos, vBlipPos) <= 30.0
							PRINT_HELP("GR_PLANE_BLIP_G")
						ENDIF
						MPGlobalsAmbience.bPrintedTruckSpawnHelp_GR_PLANE_BLIP_G = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerTruckBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerTruckBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerTruckBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerTruckBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerTruckBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_AVENGER_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF (tempveh= g_viGangOpsAvengerInDefunctBase)
					display = DISPLAY_RADAR_ONLY
				ENDIF
				
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_AVENGER_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_HACKER_TRUCK_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = " Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "removed"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempveh
	
	IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
		IF DOES_ENTITY_EXIST(g_viHackerTruckInBusinessHub)
			tempveh = g_viHackerTruckInBusinessHub
		ELSE
			PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] - g_viHackerTruckInBusinessHub doesn't exist ")
			EXIT
		ENDIF
	ELSE	
		tempveh = MPGlobalsAmbience.vehHackerTruck
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(tempveh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
		tempveh= NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(tempveh)
	OR IS_ENTITY_DEAD(tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempveh)
				PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as vehicle does not exist")
			ELIF IS_ENTITY_DEAD(tempveh)
				PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as vehicle is dead")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(tempveh)
	AND NOT IS_ENTITY_DEAD(tempveh)
	AND IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_HACKER_TRUCK(PLAYER_ID(), tl63Reason)
		OR (IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID()) AND NOT IS_LOCAL_PLAYER_USING_DRONE())
		OR IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempveh)

			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_HACKER_TRUCK(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF (IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID()) AND NOT IS_LOCAL_PLAYER_USING_DRONE())
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as player in hacker truck")
				ELIF IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as mp saved hacker truck being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempveh)
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				blipID = ADD_BLIP_FOR_ENTITY(tempveh)
				
				SET_BLIP_SPRITE(blipID, RADAR_TRACE_BAT_WP1)
				
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"BBTRUCK")

				IF g_bHackerTruckBlipBlue
					MPGlobalsAmbience.bPlayerHackertruckBlipHudColour = HUD_COLOUR_BLUE
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipID, MPGlobalsAmbience.bPlayerHackertruckBlipHudColour)	
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP]  Setting vehicle blip to ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour))
				ENDIF
				
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempveh = GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as is wrong type, removing.")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
				ENDIF
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_HACKER_TRUCK_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
						SET_BLIP_FLASHES(blipID, TRUE)
						SET_BLIP_FLASH_TIMER(blipID, 7000)
						CLEAR_SAVED_HACKER_TRUCK_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] started flashing pv blip")
						
						MPGlobalsAmbience.bPrintedHackerTruckSpawnHelp_BB_TRUCK_BLIP = TRUE
					ELSE
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] dont flash pv blip - using drone")
					ENDIF
				ENDIF
				
				BOOL bDisplayedBB_TRUCK_BLIP = FALSE
				IF MPGlobalsAmbience.bPrintedHackerTruckSpawnHelp_BB_TRUCK_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] print help, but blip is at origin? ", vBlipPos)
					ELIF (GET_PLAYERS_LAST_VEHICLE() = tempveh)
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] print help, but player was last in this hacker truck? ")
						MPGlobalsAmbience.bPrintedHackerTruckSpawnHelp_BB_TRUCK_BLIP = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						#ENDIF
						#IF IS_DEBUG_BUILD
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] print help BB_TRUCK_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
						#ENDIF
						IF !IS_BIT_SET(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_HAKCER_TRUCK_SPAWN_HELP)
							PRINT_HELP("BB_TRUCK_BLIP")
							SET_BIT(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_HAKCER_TRUCK_SPAWN_HELP)
						ENDIF	
						MPGlobalsAmbience.bPrintedHackerTruckSpawnHelp_BB_TRUCK_BLIP = FALSE
						bDisplayedBB_TRUCK_BLIP = TRUE
					ENDIF
				ENDIF
				
				IF NOT bDisplayedBB_TRUCK_BLIP
				AND NOT MPGlobalsAmbience.bPrintedHackerTruckSpawnHelp_BB_TRUCK_BLIP_G
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						//
					ELSE
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF VDIST(vPlayerPos, vBlipPos) <= 30.0
							PRINT_HELP("BB_TRUCK_BLIP_G")
						ENDIF
						MPGlobalsAmbience.bPrintedHackerTruckSpawnHelp_BB_TRUCK_BLIP_G = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF g_bHackerTruckBlipBlue
						MPGlobalsAmbience.bPlayerHackertruckBlipHudColour = HUD_COLOUR_BLUE
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour) )
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour), " - on mission")
					ELIF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerHackertruckBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF g_bHackerTruckBlipBlue
						MPGlobalsAmbience.bPlayerHackertruckBlipHudColour = HUD_COLOUR_BLUE
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour) )
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerHackertruckBlipHudColour), " - on mission")
					ELIF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerHackertruckBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerHackertruckBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
				AND (tempveh = g_viHackerTruckInBusinessHub)
				AND NOT IS_LOCAL_PLAYER_USING_DRONE()
					display = DISPLAY_RADAR_ONLY
				ENDIF
				
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_HACKER_TRUCK_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_DLC_2_2022
PROC PROCESS_ACID_LAB_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = " Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "removed"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempveh
	
	IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
		IF DOES_ENTITY_EXIST(g_viAcidLabInJuggaloWarehouse)
			tempveh = g_viAcidLabInJuggaloWarehouse
		ELSE
			PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] - g_viAcidLabInJuggaloWarehouse doesn't exist ")
			EXIT
		ENDIF
	ELSE	
		tempveh = MPGlobalsAmbience.vehAcidLab
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(tempveh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
		tempveh= NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(tempveh)
	OR IS_ENTITY_DEAD(tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempveh)
				PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as vehicle does not exist")
			ELIF IS_ENTITY_DEAD(tempveh)
				PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as vehicle is dead")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(tempveh)
	AND NOT IS_ENTITY_DEAD(tempveh)
	AND IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_ACID_LAB(PLAYER_ID(), tl63Reason)
		OR IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
		OR IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempveh)

			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_ACID_LAB(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF IS_PLAYER_IN_ACID_LAB(PLAYER_ID())
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as player in acid lab")
				ELIF IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as mp saved acid lab being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempveh)
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				blipID = ADD_BLIP_FOR_ENTITY(tempveh)
				
				SET_BLIP_SPRITE(blipID, RADAR_TRACE_BAT_WP1)
				
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"BBACIDLAB")

				IF g_bAcidLabBlipBlue
					MPGlobalsAmbience.bPlayerAcidLabBlipHudColour = HUD_COLOUR_BLUE
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipID, MPGlobalsAmbience.bPlayerAcidLabBlipHudColour)	
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP]  Setting vehicle blip to ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour))
				ENDIF
				
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempveh = GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as is wrong type, removing.")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
				ENDIF
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_ACID_LAB_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					SET_BLIP_FLASHES(blipID, TRUE)
					SET_BLIP_FLASH_TIMER(blipID, 7000)
					CLEAR_SAVED_ACID_LAB_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] started flashing pv blip")
					
					MPGlobalsAmbience.bPrintedAcidLabSpawnHelp_BB_LAB_BLIP = TRUE
				ENDIF
				
				BOOL bDisplayedBB_LAB_BLIP = FALSE
				IF MPGlobalsAmbience.bPrintedAcidLabSpawnHelp_BB_LAB_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] print help, but blip is at origin? ", vBlipPos)
					ELIF (GET_PLAYERS_LAST_VEHICLE() = tempveh)
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] print help, but player was last in this acid lab? ")
						MPGlobalsAmbience.bPrintedAcidLabSpawnHelp_BB_LAB_BLIP = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						#ENDIF
						#IF IS_DEBUG_BUILD
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] print help BB_LAB_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
						#ENDIF
						IF !IS_BIT_SET(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_ACID_LAB_SPAWN_HELP)
							PRINT_HELP("BB_LAB_BLIP")
							SET_BIT(g_iVehWeaponBS, VEH_WEAPON_GLOBAL_BIT_ACID_LAB_SPAWN_HELP)
						ENDIF	
						MPGlobalsAmbience.bPrintedAcidLabSpawnHelp_BB_LAB_BLIP = FALSE
						bDisplayedBB_LAB_BLIP = TRUE
					ENDIF
				ENDIF
				
				IF NOT bDisplayedBB_LAB_BLIP
				AND NOT MPGlobalsAmbience.bPrintedAcidLabSpawnHelp_BB_LAB_BLIP_G
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						//
					ELSE
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF VDIST(vPlayerPos, vBlipPos) <= 30.0
							PRINT_HELP("BB_LAB_BLIP_G")
						ENDIF
						MPGlobalsAmbience.bPrintedAcidLabSpawnHelp_BB_LAB_BLIP_G = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF g_bAcidLabBlipBlue
						MPGlobalsAmbience.bPlayerAcidLabBlipHudColour = HUD_COLOUR_BLUE
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour) )
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour), " - on mission")
					ELIF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerAcidLabBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF g_bAcidLabBlipBlue
						MPGlobalsAmbience.bPlayerAcidLabBlipHudColour = HUD_COLOUR_BLUE
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour) )
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerAcidLabBlipHudColour), " - on mission")
					ELIF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerAcidLabBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerAcidLabBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
				AND (tempveh = g_viAcidLabInJuggaloWarehouse)
					display = DISPLAY_RADAR_ONLY
				ENDIF
				
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_ACID_LAB_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC PROCESS_SUBMARINE_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = "Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "Blocking"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempveh = GET_SUBMARINE_VEHICLE(PLAYER_ID())
	
	//Is Vehicle valid?
	IF NOT DOES_ENTITY_EXIST(tempveh)
	OR IS_ENTITY_DEAD(tempveh)
	OR NOT IS_ENTITY_A_VEHICLE(tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempveh)
				PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as entity does not exist")
			ELIF IS_ENTITY_DEAD(tempveh)
				PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as entity is dead")
			ELIF NOT IS_ENTITY_A_VEHICLE(tempveh)
				PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as entity is not a vehicle")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
	
	ELIF IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE(PLAYER_ID(), tl63Reason)
		OR IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempveh)
		OR IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_STANDING_ON_SUBMARINE)
			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as mp saved hacker truck being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempveh)
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELIF IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_STANDING_ON_SUBMARINE)
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as player is on hull")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				blipID = ADD_BLIP_FOR_ENTITY(tempveh)
				
				SET_BLIP_SPRITE(blipID, RADAR_TRACE_SUB2)
				
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGHEST)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"BB_SUBMARINE")
				
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempveh = GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] removing blip as is wrong type")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
				ENDIF
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_SUBMARINE_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
						SET_BLIP_FLASHES(blipID, TRUE)
						SET_BLIP_FLASH_TIMER(blipID, 7000)
						CLEAR_SAVED_SUBMARINE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] started flashing pv blip")
						
						MPGlobalsAmbience.bPrintedSubmarineSpawnHelp_BB_BLIP = TRUE
					ELSE
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] dont flash pv blip - using drone")
					ENDIF
				ENDIF
				
				BOOL bDisplayedBB_SUB_BLIP = FALSE
				IF MPGlobalsAmbience.bPrintedSubmarineSpawnHelp_BB_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] print help, but blip is at origin? ", vBlipPos)
					ELIF (GET_PLAYERS_LAST_VEHICLE() = tempveh)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] print help, but player was last in this hacker truck? ")
						MPGlobalsAmbience.bPrintedSubmarineSpawnHelp_BB_BLIP = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						#ENDIF
						#IF IS_DEBUG_BUILD
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] print help BB_SUB_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
						#ENDIF
						IF NOT MPGlobalsAmbience.bSkipSubSpawnHelp
							PRINT_HELP_WITH_COLOURED_STRING("BB_SUB_BLIP", "H_BLIP_SUB2", GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
							MPGlobalsAmbience.bPrintSubmarineDinghyReqTip = TRUE
						ELSE
							PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] bypassing print for initial purchase spawn")
						ENDIF
						MPGlobalsAmbience.bSkipSubSpawnHelp = FALSE
						MPGlobalsAmbience.bPrintedSubmarineSpawnHelp_BB_BLIP = FALSE
						bDisplayedBB_SUB_BLIP = TRUE
						
					ENDIF
				ENDIF
				
				IF NOT bDisplayedBB_SUB_BLIP
				AND NOT MPGlobalsAmbience.bPrintedSubmarineSpawnHelp_BB_BLIP_G
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						//
					ELSE
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						IF VDIST(vPlayerPos, vBlipPos) <= 30.0
							PRINT_HELP_WITH_COLOURED_STRING("BB_SUB_BLIP_G", "H_BLIP_SUB2", GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
						ENDIF
						MPGlobalsAmbience.bPrintedSubmarineSpawnHelp_BB_BLIP_G = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerSubmarineBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerSubmarineBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerSubmarineBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerSubmarineBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerSubmarineBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
				AND (tempveh = g_viHackerTruckInBusinessHub)
				AND NOT IS_LOCAL_PLAYER_USING_DRONE()
					display = DISPLAY_RADAR_ONLY
				ENDIF
				
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SUBMARINE_DINGHY_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = "Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "Blocking"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempveh = MPGlobalsAmbience.vehSubmarineDinghy
	
	IF NOT DOES_ENTITY_EXIST(tempveh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
		tempveh= NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(tempveh)
	OR IS_ENTITY_DEAD(tempveh)
	OR NOT IS_ENTITY_A_VEHICLE(tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempveh)
				PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as entity does not exist")
			ELIF IS_ENTITY_DEAD(tempveh)
				PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as entity is dead")
			ELIF NOT IS_ENTITY_A_VEHICLE(tempveh)
				PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as entity is not a vehicle")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		EXIT
	
	ELIF IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE_DINGHY(PLAYER_ID(), tl63Reason)
		OR IS_MP_SAVED_SUBMARINE_DINGHY_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempveh)

			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUBMARINE_DINGHY(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF IS_MP_SAVED_SUBMARINE_DINGHY_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as mp saved hacker truck being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempveh)
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				blipID = ADD_BLIP_FOR_ENTITY(tempveh)
				
				SET_BLIP_SPRITE(blipID, RADAR_TRACE_PLAYER_BOAT)
				
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"BB_DINGHY")
				
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempveh = GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] removing blip as is wrong type")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
				ENDIF
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_SUBMARINE_DINGHY_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
						SET_BLIP_FLASHES(blipID, TRUE)
						SET_BLIP_FLASH_TIMER(blipID, 7000)
						CLEAR_SAVED_SUBMARINE_DINGHY_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
						PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] started flashing pv blip")
						
						MPGlobalsAmbience.bPrintedSubmarineDinghySpawnHelp_BB_BLIP = TRUE
					ELSE
						PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] dont flash pv blip - using drone")
					ENDIF
				ENDIF
				
				IF MPGlobalsAmbience.bPrintedSubmarineDinghySpawnHelp_BB_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] print help, but blip is at origin? ", vBlipPos)
					ELIF (GET_PLAYERS_LAST_VEHICLE() = tempveh)
						PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] print help, but player was last in this vehicle? ")
						MPGlobalsAmbience.bPrintedSubmarineDinghySpawnHelp_BB_BLIP = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						#ENDIF
						#IF IS_DEBUG_BUILD
						PRINTLN("[PROCESS_SUBMARINE_DINGHY_DROP_BLIP] print help BB_DINGHY_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
						#ENDIF
						PRINT_HELP_WITH_COLOURED_STRING("BB_DINGHY_BLIP", "H_BLIP_DINGHY", GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))	
						MPGlobalsAmbience.bPrintedSubmarineDinghySpawnHelp_BB_BLIP = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerSubmarineDinghyBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerSubmarineDinghyBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerSubmarineDinghyBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerSubmarineDinghyBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerSubmarineDinghyBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
				AND (tempveh = g_viHackerTruckInBusinessHub)
				AND NOT IS_LOCAL_PLAYER_USING_DRONE()
					display = DISPLAY_RADAR_ONLY
				ENDIF
				
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_SUBMARINE_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_DLC_2_2022
PROC PROCESS_SUPPORT_BIKE_DROP_BLIP(BLIP_INDEX &blipID)
	#IF IS_DEBUG_BUILD
	STRING sRemoving = ""
	IF DOES_BLIP_EXIST(blipID)
		sRemoving = "Removing"
	ELIF (GET_FRAME_COUNT() % 60 = 0)
		sRemoving = "Blocking"
	ENDIF
	#ENDIF

	VEHICLE_INDEX tempveh = MPGlobalsAmbience.vehSupportBike
	
	IF NOT DOES_ENTITY_EXIST(tempveh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
		tempveh= NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
	ENDIF

	IF NOT DOES_ENTITY_EXIST(tempveh)
	OR IS_ENTITY_DEAD(tempveh)
	OR NOT IS_ENTITY_A_VEHICLE(tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			IF NOT DOES_ENTITY_EXIST(tempveh)
				PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as entity does not exist")
			ELIF IS_ENTITY_DEAD(tempveh)
				PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as entity is dead")
			ELIF NOT IS_ENTITY_A_VEHICLE(tempveh)
				PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as entity is not a vehicle")
			ENDIF
		ENDIF
		#ENDIF
		
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		EXIT
	
	ELIF IS_NET_PLAYER_OK(PLAYER_ID()) 
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempveh)
		#IF IS_DEBUG_BUILD
		IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
			PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as player is in ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(tempveh))
		ENDIF
		#ENDIF
		IF DOES_BLIP_EXIST(blipID)
			REMOVE_BLIP(blipID)
		ENDIF
		
	// Out vehicle	
	ELSE
		TEXT_LABEL_63 tl63Reason
		
		// Add/remove blip
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		OR IS_PLAYER_SCTV(PLAYER_ID())
		OR IS_A_SPECTATOR_CAM_RUNNING()
		OR Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		OR IS_ON_RACE_GLOBAL_SET()
		OR NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUPPORT_BIKE(PLAYER_ID(), tl63Reason)
		OR IS_MP_SAVED_SUPPORT_BIKE_BEING_CLEANED_UP()
		OR NOT IS_VEHICLE_DRIVEABLE(tempveh)
			#IF IS_DEBUG_BUILD
			IF NOT IS_STRING_NULL_OR_EMPTY(sRemoving)
				IF NETWORK_IS_IN_TUTORIAL_SESSION()
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as player is in tutorial session")
				ELIF IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as player is SCTV")
				ELIF IS_A_SPECTATOR_CAM_RUNNING()
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as spectator cam is running")
				ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as player is on LTS mission")
				ELIF IS_ON_RACE_GLOBAL_SET()
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as player is on a race")
				ELIF NOT SHOULD_ADD_BLIP_FOR_OWNERS_SUPPORT_BIKE(PLAYER_ID(), tl63Reason)
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as not safe to add [", tl63Reason, "]")
				ELIF IS_MP_SAVED_SUPPORT_BIKE_BEING_CLEANED_UP()
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as mp saved support bike being cleaned up")
				ELIF NOT IS_VEHICLE_DRIVEABLE(tempveh)
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as vehicle not driveable")
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip - uncommented reason???")
				ENDIF
			ENDIF
			#ENDIF
		
			IF DOES_BLIP_EXIST(blipID)
				REMOVE_BLIP(blipID)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(blipID)
				// @bty : todo - BLIP add correct blip sprite
				blipID = ADD_BLIP_FOR_ENTITY(tempveh)
				SET_BLIP_SPRITE(blipID, RADAR_TRACE_GANG_BIKE)
				SET_BLIP_PRIORITY(blipID, BLIPPRIORITY_HIGH)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipID,"BB_ALPV")
			ELSE
				// if the blip does exist but is somehow not on this vehicle then remove it. 3161065
				IF (GET_BLIP_INFO_ID_TYPE(blipID) = BLIPTYPE_VEHICLE)
					IF NOT (tempveh = GET_BLIP_INFO_ID_ENTITY_INDEX(blipID))
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP]  existing blip is not on correct entity, removing.")
						REMOVE_BLIP(blipID)
					ENDIF
				ELSE
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] removing blip as is wrong type")
					REMOVE_BLIP(blipID)
				ENDIF
			ENDIF
			
			// does the blip need rotated?
			IF DOES_BLIP_EXIST(blipID)
				IF SHOULD_BLIP_BE_MANUALLY_ROTATED(blipID)
					SET_BLIP_ROTATION(blipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(tempveh)))	
				ENDIF
			ENDIF
			
			// set personal vehicle blip colour
			IF DOES_BLIP_EXIST(blipID)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())))
						MPGlobalsAmbience.bPlayerSupportBikeBlipHudColour = GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobalsAmbience.bPlayerSupportBikeBlipHudColour), " - players gang is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobalsAmbience.bPlayerSupportBikeBlipHudColour) )
					ENDIF
				ELIF IS_THREAD_ACTIVE(MPGlobals.VehicleData.threadIDCustomColour)
					IF NOT (GET_BLIP_COLOUR(blipID) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour))
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP]  set blip colour blip ", HUD_COLOUR_AS_STRING(MPGlobals.VehicleData.blipCustomColour), " - threadIDCustomColour is active")
						SET_BLIP_COLOUR(blipID, GET_BLIP_COLOUR_FROM_HUD_COLOUR(MPGlobals.VehicleData.blipCustomColour) )
					ENDIF
				ELSE
					IF NOT (MPGlobalsAmbience.bPlayerSupportBikeBlipHudColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as players gang no longer active")
						REMOVE_BLIP(blipID)
						MPGlobalsAmbience.bPlayerSupportBikeBlipHudColour = HUD_COLOUR_PURE_WHITE
					ENDIF
					IF NOT (MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE)
					AND DOES_BLIP_EXIST(blipID)
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] ", sRemoving, " blip as threadIDCustomColour no longer active")
						REMOVE_BLIP(blipID)
						MPGlobals.VehicleData.blipCustomColour = HUD_COLOUR_PURE_WHITE
					ENDIF
				ENDIF	
			ENDIF
			
			// flash the pv if requested.
			IF DOES_BLIP_EXIST(blipID)
				IF IS_SAVED_SUPPORT_BIKE_FLAG_SET(MP_SAVED_VEH_FLAG_FLASH_BLIP)
					IF NOT IS_LOCAL_PLAYER_USING_DRONE()
						SET_BLIP_FLASHES(blipID, TRUE)
						SET_BLIP_FLASH_TIMER(blipID, 7000)
						CLEAR_SAVED_SUPPORT_BIKE_FLAG(MP_SAVED_VEH_FLAG_FLASH_BLIP)
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] started flashing pv blip")
						
						MPGlobalsAmbience.bPrintedSupportBikeSpawnHelp_BB_BLIP = TRUE
					ELSE
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] dont flash pv blip - using drone")
					ENDIF
				ENDIF
				
				IF MPGlobalsAmbience.bPrintedSupportBikeSpawnHelp_BB_BLIP
				AND SAFE_TO_PRINT_PV_HELP()
					VECTOR vBlipPos = GET_BLIP_COORDS(blipID)
					IF IS_VECTOR_ZERO(vBlipPos)
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] print help, but blip is at origin? ", vBlipPos)
					ELIF (GET_PLAYERS_LAST_VEHICLE() = tempveh)
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] print help, but player was last in this vehicle? ")
						MPGlobalsAmbience.bPrintedSupportBikeSpawnHelp_BB_BLIP = FALSE
					ELSE
						#IF IS_DEBUG_BUILD
						VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						#ENDIF
						#IF IS_DEBUG_BUILD
						PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] print help BB_ALPV_BLIP, dist (", vPlayerPos, ", ", vBlipPos, ") ", VDIST(vPlayerPos, vBlipPos))
						#ENDIF
						PRINT_HELP_WITH_COLOURED_STRING("BB_ALPV_BLIP", "H_BLIP_ALPV", GET_HUD_COLOUR_FROM_BLIP_COLOUR(GET_BLIP_COLOUR(blipID)))/* GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())*/
						MPGlobalsAmbience.bPrintedSupportBikeSpawnHelp_BB_BLIP = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(blipID)
				BLIP_DISPLAY display = DISPLAY_BOTH
				IF display != GET_BLIP_INFO_ID_DISPLAY(blipID)
					PRINTLN("[PROCESS_SUPPORT_BIKE_DROP_BLIP] set blip display as ", BLIP_DISPLAY_AS_STRING(display))
					SET_BLIP_DISPLAY(blipID, display)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF


//PURPOSE: Controls the launching of Vehicle Drop
PROC MAINTAIN_VEHICLE_DROP_CLIENT(VEHICLE_DROP_PERSONAL_DATA &VDPersonalData, VEHICLE_DROP_PERSONAL_DATA &VDPersonalTruckData, BLIP_INDEX &TruckBlipID, BLIP_INDEX &avengerBlipID
								, BLIP_INDEX &HackerTruckBlipID, BLIP_INDEX &SubmarineBlipID, BLIP_INDEX &SubmarineDinghyBlipID #IF FEATURE_DLC_2_2022 , BLIP_INDEX &SupportBikeBlipID, BLIP_INDEX &AcidLabBlipID #ENDIF )
	
	//Personal Vehicle Delivery
	IF MPGlobalsAmbience.bLaunchVehicleDropPersonal
		IF MPGlobalsAmbience.bLaunchVehicleDropPersonalAircraft
			PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalData, FALSE, TRUE)
		ELSE
			PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalData, FALSE)
		ENDIF
	ENDIF
	
	IF MPGlobalsAmbience.bLaunchVehicleDropTruck
	AND (NOT IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_3_TRUCK))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, TRUE)
	ENDIF
	PROCESS_TRUCK_DROP_BLIP(TruckBlipID)
	
	IF MPGlobalsAmbience.bLaunchVehicleDropAvenger
	AND (NOT IS_MP_SAVED_AVENGER_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_4_AIRCRAFT))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, FALSE, FALSE, TRUE)
	ENDIF
	PROCESS_AVENGER_DROP_BLIP(avengerBlipID)
	
	IF MPGlobalsAmbience.bLaunchVehicleDropHackerTruck
	AND (NOT IS_MP_SAVED_HACKERTRUCK_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_5_HACKERTRUCK))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	PROCESS_HACKER_TRUCK_DROP_BLIP(HackerTruckBlipID)
	
	IF MPGlobalsAmbience.bLaunchVehicleDropSubmarine
	AND (NOT IS_MP_SAVED_SUBMARINE_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_6_SUBMARINE))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	PROCESS_SUBMARINE_DROP_BLIP(SubmarineBlipID)
	
	IF MPGlobalsAmbience.bLaunchVehicleDropSubmarineDinghy
	AND (NOT IS_MP_SAVED_SUBMARINE_DINGHY_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_7_SUBMARINE_DINGHY))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	PROCESS_SUBMARINE_DINGHY_DROP_BLIP(SubmarineDinghyBlipID)
	
	#IF FEATURE_DLC_2_2022
	IF MPGlobalsAmbience.bLaunchVehicleDropAcidLab
	AND (NOT IS_MP_SAVED_ACIDLAB_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_8_ACIDLAB))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	PROCESS_ACID_LAB_DROP_BLIP(AcidLabBlipID)
	
	IF MPGlobalsAmbience.bLaunchVehicleDropSupportBike
	AND (NOT IS_MP_SAVED_SUPPORT_BIKE_BEING_CLEANED_UP()
	OR (VDPersonalTruckData.iVehicleDropPersonalStage = VDP_CLEANUP AND g_eCachedVehicleSpawnQueue = eVeh_spawn_9_SUPPORT_BIKE))
		PROCESS_VEHICLE_DROP_PERSONAL(VDPersonalTruckData, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	PROCESS_SUPPORT_BIKE_DROP_BLIP(SupportBikeBlipID)
	#ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_VEHICLE_DROP_PERSONAL_WIDGET(VEHICLE_DROP_PERSONAL_DATA &VDPersonalTruckData, VEHICLE_DROP_PERSONAL_DEBUG_DATA &VDPersonalTruckDebugData)
	INT iExtra
	TEXT_LABEL_23 str
	
	START_WIDGET_GROUP("Gunrunning Truck Drop")
		ADD_WIDGET_BOOL("bLaunchVehicleDropTruck", MPGlobalsAmbience.bLaunchVehicleDropTruck)
		
		#IF IS_DEBUG_BUILD
		IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS("gunrunning_truck", TRUE)
			IF !VDPersonalTruckDebugData.bDrawDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				VDPersonalTruckDebugData.bDrawDebug = TRUE
			ENDIF	
		ENDIF
		#ENDIF
		
		ADD_WIDGET_BOOL("bDrawDebug", VDPersonalTruckDebugData.bDrawDebug)
		
		VDPersonalTruckDebugData.iDebugVehicleGrab = 1
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("local data")
			ADD_TO_WIDGET_COMBO("global data")
			ADD_TO_WIDGET_COMBO("players broadcast data")
		STOP_WIDGET_COMBO("iDebugVehicleGrab", VDPersonalTruckDebugData.iDebugVehicleGrab)
		
		ADD_WIDGET_BOOL("bBuyHauler2", VDPersonalTruckDebugData.bBuyHauler2)
		ADD_WIDGET_BOOL("bBuyPhantom3", VDPersonalTruckDebugData.bBuyPhantom3)
		VDPersonalTruckDebugData.twTruckModelName = ADD_TEXT_WIDGET("twTruckModelName")
		
		START_WIDGET_GROUP("Vehicle Drop Personal Data")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("VDP_SETUP")		//0
				ADD_TO_WIDGET_COMBO("VDP_CREATION")		//1
				ADD_TO_WIDGET_COMBO("VDP_DELIVERY")		//2
				ADD_TO_WIDGET_COMBO("VDP_END_CALL")		//3
				ADD_TO_WIDGET_COMBO("VDP_CLEANUP")		//4
			STOP_WIDGET_COMBO("iVehicleDropPersonalStage", VDPersonalTruckData.iVehicleDropPersonalStage)
			
		//	ADD_WIDGET_INT_SLIDER("niVehicle", VDPersonalTruckData.niVehicle)
		//	ADD_WIDGET_INT_SLIDER("niDriver", VDPersonalTruckData.niDriver)
			ADD_WIDGET_INT_SLIDER("iBitSet", VDPersonalTruckData.iBitSet, 0, 100, 1)
			ADD_WIDGET_INT_SLIDER("iVehicleSlot", VDPersonalTruckData.iVehicleSlot, -100, 100, 1)
			ADD_WIDGET_VECTOR_SLIDER("vDropLocation", VDPersonalTruckData.vDropLocation, -7500.0, 7500.0, 0.1)
		//	ADD_WIDGET_INT_SLIDER("speechStruct", VDPersonalTruckData.speechStruct)
			ADD_WIDGET_INT_SLIDER("iNode", VDPersonalTruckData.iNode, 0, 100, 1)
		//	ADD_WIDGET_INT_SLIDER("iEndCallTimer", VDPersonalTruckData.iEndCallTimer, 0, 100, 1)
		//	ADD_WIDGET_INT_SLIDER("iDeliveryDelayTimer", VDPersonalTruckData.iDeliveryDelayTimer, 0, 100, 1)
		//	ADD_WIDGET_INT_SLIDER("sSpeech", VDPersonalTruckData.sSpeech)
			ADD_WIDGET_VECTOR_SLIDER("vCreatePos", VDPersonalTruckData.vCreatePos, 0, 100, 1)
			ADD_WIDGET_FLOAT_SLIDER("fCreateHeading", VDPersonalTruckData.fCreateHeading, 0, 100, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Truck")
			ADD_WIDGET_VECTOR_SLIDER("vVehicleCoord", VDPersonalTruckDebugData.vVehicleCoord, -7500.0, 7500.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fVehicleHeading", VDPersonalTruckDebugData.fVehicleHeading, 0.0, 360.0, 0.1)
			ADD_WIDGET_INT_SLIDER("iTruckColours[0]", VDPersonalTruckDebugData.iTruckColours[0], 0, 250, 1)
			ADD_WIDGET_INT_SLIDER("iTruckColours[1]", VDPersonalTruckDebugData.iTruckColours[1], 0, 250, 1)
			ADD_WIDGET_INT_SLIDER("iTruckColours[2]", VDPersonalTruckDebugData.iTruckColours[2], 0, 250, 1)
			ADD_WIDGET_INT_SLIDER("iTruckColours[3]", VDPersonalTruckDebugData.iTruckColours[3], 0, 250, 1)
			REPEAT COUNT_OF(VDPersonalTruckDebugData.bVehicleExtras) iExtra
				str  = "bVehicleExtras["
				str += iExtra
				str += "]"
				ADD_WIDGET_BOOL(str, VDPersonalTruckDebugData.bVehicleExtras[iExtra])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Trailer")
			ADD_WIDGET_BOOL("bMoveTrailer", VDPersonalTruckDebugData.bMoveTrailer)
			ADD_WIDGET_VECTOR_SLIDER("vTrailerOffset", VDPersonalTruckDebugData.vTrailerOffset, -7500.0, 7500.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fTrailerHeadingOffset", VDPersonalTruckDebugData.fTrailerHeadingOffset, 0.0, 360.0, 0.1)
			ADD_WIDGET_INT_SLIDER("iTrailerColours[0]", VDPersonalTruckDebugData.iTrailerColours[0], 0, 250, 1)
			ADD_WIDGET_INT_SLIDER("iTrailerColours[1]", VDPersonalTruckDebugData.iTrailerColours[1], 0, 250, 1)
			ADD_WIDGET_INT_SLIDER("iTrailerColours[2]", VDPersonalTruckDebugData.iTrailerColours[2], 0, 250, 1)
			ADD_WIDGET_INT_SLIDER("iTrailerColours[3]", VDPersonalTruckDebugData.iTrailerColours[3], 0, 250, 1)
			REPEAT COUNT_OF(VDPersonalTruckDebugData.bTrailerExtras) iExtra
				str  = "bTrailerExtras["
				str += iExtra
				str += "]"
				ADD_WIDGET_BOOL(str, VDPersonalTruckDebugData.bTrailerExtras[iExtra])
			ENDREPEAT
//			REPEAT COUNT_OF(VDPersonalTruckDebugData.iTrailerMods) iExtra
//				str  = "iTrailerMods["
//				str += debug_GET_MOD_TYPE_NAME(INT_TO_ENUM(MOD_TYPE, iExtra))
//				str += "]"
//				ADD_WIDGET_INT_SLIDER(str, VDPersonalTruckDebugData.iTrailerMods[iExtra], -1, 10, 1)
//			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("Toggles")
		ADD_WIDGET_BOOL("bMoveTruck", VDPersonalTruckDebugData.bMoveTruck)
		ADD_WIDGET_BOOL("bAttachTrailer", VDPersonalTruckDebugData.bAttachTrailer)
		ADD_WIDGET_FLOAT_SLIDER("fAttachTrailer", VDPersonalTruckDebugData.fAttachTrailer, 0.0, 360.0, 0.1)
		ADD_WIDGET_BOOL("bDetachTrailer", VDPersonalTruckDebugData.bDetachTrailer)
		
		VDPersonalTruckDebugData.iTrailerDoor = ENUM_TO_INT(SC_DOOR_BOOT)
		START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("FRONT_LEFT")
			ADD_TO_WIDGET_COMBO("FRONT_RIGHT")
			ADD_TO_WIDGET_COMBO("REAR_LEFT")
			ADD_TO_WIDGET_COMBO("REAR_RIGHT")
			ADD_TO_WIDGET_COMBO("BONNET")
			ADD_TO_WIDGET_COMBO("BOOT")
		STOP_WIDGET_COMBO("iTrailerDoor", VDPersonalTruckDebugData.iTrailerDoor)
		
		ADD_WIDGET_BOOL("bOpenTrailerDoor", VDPersonalTruckDebugData.bOpenTrailerDoor)
		ADD_WIDGET_BOOL("bCloseTrailerDoor", VDPersonalTruckDebugData.bCloseTrailerDoor)
		
		ADD_WIDGET_FLOAT_SLIDER("fOverride_extenable_side_ratio", VDPersonalTruckDebugData.fOverride_extenable_side_ratio, -1.0,1.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("fExtenable_side_target_ratio", VDPersonalTruckDebugData.fExtenable_side_target_ratio, -1.0,1.0,0.01)
	//	ADD_WIDGET_BOOL("bOverride_side_ratio", VDPersonalTruckDebugData.bOverride_side_ratio)
	STOP_WIDGET_GROUP()
ENDPROC
PROC GET_PROPERTY_SECTION_TYPE_COLOUR(ARMORY_TRUCK_SECTION_TYPE_ENUM ePS, INT& red, INT& green, INT& blue)
	HUD_COLOURS HudColour
	SWITCH ePS
		CASE AT_ST_COMMAND_CENTER		HudColour = HUD_COLOUR_BLUE BREAK
		CASE AT_ST_GUNMOD				HudColour = HUD_COLOUR_YELLOW BREAK
		CASE AT_ST_CARMOD				HudColour = HUD_COLOUR_GREEN BREAK
	//	CASE PST_BARBER				HudColour = HUD_COLOUR_unknown BREAK
		CASE AT_ST_LIVING_ROOM 		HudColour = HUD_COLOUR_PURE_WHITE BREAK
		CASE AT_ST_EMPTY_SINGLE		HudColour = HUD_COLOUR_PURPLE BREAK
		CASE AT_ST_EMPTY_DOUBLE		HudColour = HUD_COLOUR_PURPLE BREAK
		CASE AT_ST_VEHICLE_STORAGE	HudColour = HUD_COLOUR_ORANGE BREAK
		
		DEFAULT
			HudColour = HUD_COLOUR_BLACK	BREAK
		BREAK
	ENDSWITCH
	
	INT alpha_param
	GET_HUD_COLOUR(HudColour, red, green, blue, alpha_param)
	red		= (255-red)
	green	= (255-green)
	blue	= (255-blue)
ENDPROC
PROC UPDATE_VEHICLE_DROP_PERSONAL_WIDGET(VEHICLE_DROP_PERSONAL_DATA &VDPersonalTruckData, VEHICLE_DROP_PERSONAL_DEBUG_DATA &VDPersonalTruckDebugData)
	VEHICLE_INDEX viTruck, viTrailer
	
	INT iRed, iGreen, iBlue, ialpha_param
	SWITCH VDPersonalTruckDebugData.iDebugVehicleGrab
		CASE 0
			IF NETWORK_DOES_NETWORK_ID_EXIST(VDPersonalTruckData.niVehicle)
				viTruck = NET_TO_VEH(VDPersonalTruckData.niVehicle)
			ENDIF
			IF NETWORK_DOES_NETWORK_ID_EXIST(VDPersonalTruckData.niDriver)
				viTrailer = NET_TO_VEH(VDPersonalTruckData.niDriver)
			ENDIF
			
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, ialpha_param)
		BREAK
		CASE 1
			viTruck = MPGlobalsAmbience.vehTruckVehicle[0]
			viTrailer = MPGlobalsAmbience.vehTruckVehicle[1]
			
			GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, ialpha_param)
		BREAK
		CASE 2
			viTruck = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])
			viTrailer = NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])
			
			GET_HUD_COLOUR(HUD_COLOUR_RED, iRed, iGreen, iBlue, ialpha_param)
		BREAK
		
		DEFAULT
			viTruck = NULL
			viTrailer = NULL
		BREAK
	ENDSWITCH
	
	IF VDPersonalTruckDebugData.bBuyHauler2
		SET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0, ENUM_TO_INT(HAULER2))
		VDPersonalTruckDebugData.bBuyHauler2 = FALSE
	ENDIF
	IF VDPersonalTruckDebugData.bBuyPhantom3
		SET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0, ENUM_TO_INT(PHANTOM3))
		VDPersonalTruckDebugData.bBuyPhantom3 = FALSE
	ENDIF
	STRING NewWidgetContents = GET_SAFE_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_GUNRUNNING_TRUCK_MODEL())
	SET_CONTENTS_OF_TEXT_WIDGET(VDPersonalTruckDebugData.twTruckModelName, NewWidgetContents)
	
	IF VDPersonalTruckDebugData.bDrawDebug
		BOOL bCanDrawDebug = TRUE
		
		INT iStrOffset = 0, iColumn = 0
		TEXT_LABEL_63 tl63Str
		
		ARMORY_TRUCK_SECTION_TYPE_ENUM eType
		ARMORY_TRUCK_SECTION_TINT_ENUM eTint
		tl63Str  = "model: "
		tl63Str += GET_SAFE_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_GUNRUNNING_TRUCK_MODEL())
		DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.2,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)
		
		INT iRed2, iGreen2, iBlue2
		eType = GET_ARMORY_TRUCK_SECTION_TYPE_FROM_POS(ATS_FIRST_SECTION)
		eTint = GET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_FIRST_SECTION)
		GET_PROPERTY_SECTION_TYPE_COLOUR(eType, iRed2, iGreen2, iBlue2)
		tl63Str  = GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eType)
		DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset+0))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.5)), iRed2, iGreen2, iBlue2, ialpha_param)
		tl63Str  = GET_ARMORY_TRUCK_SECTION_TINT_NAME(eTint)
		DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset+1))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.5)), iRed2, iGreen2, iBlue2, ialpha_param)
		
		eType = GET_ARMORY_TRUCK_SECTION_TYPE_FROM_POS(ATS_SECOND_SECTION)
		eTint = GET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_SECOND_SECTION)
		GET_PROPERTY_SECTION_TYPE_COLOUR(eType, iRed2, iGreen2, iBlue2)
		tl63Str  = GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eType)
		DRAW_DEBUG_TEXT_2D(tl63Str,			<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset+0))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+1.0)), iRed2, iGreen2, iBlue2, ialpha_param)
		IF eTint != GET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_FIRST_SECTION)
			tl63Str = GET_ARMORY_TRUCK_SECTION_TINT_NAME(eTint)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset+1))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+1.0)), iRed2, iGreen2, iBlue2, ialpha_param)
		ENDIF
		
		eType = GET_ARMORY_TRUCK_SECTION_TYPE_FROM_POS(ATS_THIRD_SECTION)
		eTint = GET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_THIRD_SECTION)
		GET_PROPERTY_SECTION_TYPE_COLOUR(eType, iRed2, iGreen2, iBlue2)
		tl63Str  = GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eType)
		DRAW_DEBUG_TEXT_2D(tl63Str,			<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset+0))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+1.5)), iRed2, iGreen2, iBlue2, ialpha_param)
		IF eTint != GET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_FIRST_SECTION)
			tl63Str = GET_ARMORY_TRUCK_SECTION_TINT_NAME(eTint)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset+1))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+1.5)), iRed2, iGreen2, iBlue2, ialpha_param)
		ENDIF
		iStrOffset++
		
		BOOL bDisplay = FALSE
//		tl63Str = "local data "
//		IF NETWORK_DOES_NETWORK_ID_EXIST(VDPersonalTruckData.niVehicle)
//			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(VDPersonalTruckData.niVehicle))
//				tl63Str += "drivable"
//			ELSE
//				tl63Str += "undrivable"
//			ENDIF
//			bDisplay = TRUE
//		ELSE
//			tl63Str += "NULL"
//		ENDIF
//		IF NETWORK_DOES_NETWORK_ID_EXIST(VDPersonalTruckData.niDriver)
//			IF IS_VEHICLE_DRIVEABLE(NET_TO_VEH(VDPersonalTruckData.niDriver))
//				tl63Str += ", drivable"
//			ELSE
//				tl63Str += ", undrivable"
//			ENDIF
//			bDisplay = TRUE
//		ELSE
//			tl63Str += ", NULL"
//		ENDIF
//		IF bDisplay
//			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
//		ENDIF
		
//		bDisplay = FALSE
//		tl63Str = "global data "
//		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
//			IF IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehTruckVehicle[0])
//				tl63Str += GET_ENTITY_HEALTH(MPGlobalsAmbience.vehTruckVehicle[0])
//			ELSE
//				tl63Str += "undrivable"
//			ENDIF
//			bDisplay = TRUE
//		ELSE
//			tl63Str += "NULL"
//		ENDIF
//		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[1])
//			IF IS_VEHICLE_DRIVEABLE(MPGlobalsAmbience.vehTruckVehicle[1])
//				tl63Str += ", "
//				tl63Str += GET_ENTITY_HEALTH(MPGlobalsAmbience.vehTruckVehicle[1])
//			ELSE
//				tl63Str += ", undrivable"
//			ENDIF
//			bDisplay = TRUE
//		ELSE
//			tl63Str += ", NULL"
//		ENDIF
//		IF bDisplay
//			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
//		ENDIF
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			bDisplay = FALSE
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i))
				tl63Str  = GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i))
			ELSE
				tl63Str  = "remote "
				tl63Str += i
			ENDIF
			tl63Str += " "
			
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[i][0])
				IF IS_VEHICLE_DRIVEABLE(MPGlobals.RemoteTruckV[i][0])
					tl63Str += GET_ENTITY_HEALTH(MPGlobals.RemoteTruckV[i][0])
				ELSE
					tl63Str += "undrivable"
				ENDIF
				
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(MPGlobals.RemoteTruckV[i][0])
					tl63Str += " (attached)"
				ENDIF
				
				bDisplay = TRUE
			ELSE
				tl63Str += "NULL"
			ENDIF
			IF DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[i][1])
				IF IS_VEHICLE_DRIVEABLE(MPGlobals.RemoteTruckV[i][1])
					tl63Str += ", "
					tl63Str += GET_ENTITY_HEALTH(MPGlobals.RemoteTruckV[i][1])
				ELSE
					tl63Str += ", undrivable"
				ENDIF
				bDisplay = TRUE
			ELSE
				tl63Str += ", NULL"
			ENDIF
			IF bDisplay
				DRAW_DEBUG_TEXT_2D(tl63Str,	<<0.2,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			ENDIF
		ENDREPEAT
		
		INT iFlag
		tl63Str = "flags:"
		REPEAT (32*2) iFlag
			IF IS_SAVED_TRUCK_FLAG_SET(INT_TO_ENUM(SAVED_VEHICLE_FLAG, iFlag))
				STRING sFlag = GET_STRING_FROM_SAVED_VEHICLE_FLAG(INT_TO_ENUM(SAVED_VEHICLE_FLAG, iFlag))
				
				IF (GET_LENGTH_OF_LITERAL_STRING(tl63Str)+GET_LENGTH_OF_LITERAL_STRING(sFlag)) >= 64
					DRAW_DEBUG_TEXT_2D(tl63Str,	<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), 255-iRed, 255-iGreen, 255-iBlue, ialpha_param)	iStrOffset++
					tl63Str = "    "
				ENDIF
				
				tl63Str += sFlag
				tl63Str += " "
			ENDIF
		ENDREPEAT
		DRAW_DEBUG_TEXT_2D(tl63Str,	<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), 255-iRed, 255-iGreen, 255-iBlue, ialpha_param)	iStrOffset++
		iStrOffset++
		
		IF NOT IS_BROWSER_OPEN()
			//
//			GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iRed, iGreen, iBlue, ialpha_param)
//			tl63Str  = "unused_1:"
//			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD__unused_1)
//			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
//			
//			tl63Str  = "unused_2:"
//			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD__unused_2)
//			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
//			
//			tl63Str  = "unused_3:"
//			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD__unused_3)
//			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
//			
//			tl63Str  = "unused_4:"
//			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD__unused_4)
//			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			//
			GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iRed, iGreen, iBlue, ialpha_param)
			tl63Str  = "iColour1:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_1)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "iColour2:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_2)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "iColourExtra1:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_EXTRA_1)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "iColourExtra2:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_EXTRA_2)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "iColourCrew:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1)
			IF IS_BIT_SET(GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1), COL_CREW_1_SET)
				tl63Str += " primary"
			ENDIF
			IF IS_BIT_SET(GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1), COL_CREW_2_SET)
				tl63Str += " secondary"
			ENDIF
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "modRoof:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "modLivery:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_LIVERY)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			
			tl63Str  = "modPlate:"
			tl63Str += GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_PLATE_INDEX)
			DRAW_DEBUG_TEXT_2D(tl63Str,		<<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*(TO_FLOAT(iColumn)+0.0)), iRed, iGreen, iBlue, ialpha_param)	iStrOffset++
			iStrOffset++
		ENDIF
		
		INT iExtra
		IF IS_VEHICLE_DRIVEABLE(viTruck)
			IF NOT VDPersonalTruckDebugData.bMoveTruck
				VDPersonalTruckDebugData.vVehicleCoord = GET_ENTITY_COORDS(viTruck)
				VDPersonalTruckDebugData.fVehicleHeading = GET_ENTITY_HEADING(viTruck)
				
				GET_VEHICLE_COLOURS(viTruck, VDPersonalTruckDebugData.iTruckColours[0], VDPersonalTruckDebugData.iTruckColours[1])
				GET_VEHICLE_EXTRA_COLOURS(viTruck, VDPersonalTruckDebugData.iTruckColours[2], VDPersonalTruckDebugData.iTruckColours[3])
				
				REPEAT COUNT_OF(VDPersonalTruckDebugData.bVehicleExtras) iExtra
					VDPersonalTruckDebugData.bVehicleExtras[iExtra] = IS_VEHICLE_EXTRA_TURNED_ON(viTruck, iExtra+1)
				ENDREPEAT
			ELSE
				SET_ENTITY_COORDS(viTruck, VDPersonalTruckDebugData.vVehicleCoord)
				SET_ENTITY_HEADING(viTruck, VDPersonalTruckDebugData.fVehicleHeading)
				
				SET_VEHICLE_COLOURS(viTruck, VDPersonalTruckDebugData.iTruckColours[0], VDPersonalTruckDebugData.iTruckColours[1])
				SET_VEHICLE_EXTRA_COLOURS(viTruck, VDPersonalTruckDebugData.iTruckColours[2], VDPersonalTruckDebugData.iTruckColours[3])
				
				REPEAT COUNT_OF(VDPersonalTruckDebugData.bVehicleExtras) iExtra
					SET_VEHICLE_EXTRA(viTruck, iExtra+1, NOT VDPersonalTruckDebugData.bVehicleExtras[iExtra])
				ENDREPEAT
			ENDIF
		ELSE
			bCanDrawDebug = FALSE
			VDPersonalTruckDebugData.vVehicleCoord = <<0,0,0>>
			VDPersonalTruckDebugData.fVehicleHeading = 0
			VDPersonalTruckDebugData.iTruckColours[0] = 0
			VDPersonalTruckDebugData.iTruckColours[1] = 0
			VDPersonalTruckDebugData.iTruckColours[2] = 0
			VDPersonalTruckDebugData.iTruckColours[3] = 0
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(viTrailer)
			IF NOT VDPersonalTruckDebugData.bMoveTruck
			AND NOT VDPersonalTruckDebugData.bMoveTrailer
				VDPersonalTruckDebugData.vTrailerOffset = -GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viTrailer, VDPersonalTruckDebugData.vVehicleCoord)
				VDPersonalTruckDebugData.fTrailerHeadingOffset = GET_ENTITY_HEADING(viTrailer)-VDPersonalTruckDebugData.fVehicleHeading
				
				GET_VEHICLE_COLOURS(viTrailer, VDPersonalTruckDebugData.iTrailerColours[0], VDPersonalTruckDebugData.iTrailerColours[1])
				GET_VEHICLE_EXTRA_COLOURS(viTrailer, VDPersonalTruckDebugData.iTrailerColours[2], VDPersonalTruckDebugData.iTrailerColours[3])
				
				REPEAT COUNT_OF(VDPersonalTruckDebugData.bTrailerExtras) iExtra
					VDPersonalTruckDebugData.bTrailerExtras[iExtra] = IS_VEHICLE_EXTRA_TURNED_ON(viTrailer, iExtra+1)
				ENDREPEAT
//				REPEAT COUNT_OF(VDPersonalTruckDebugData.iTrailerMods) iExtra
//					VDPersonalTruckDebugData.iTrailerMods[iExtra] = GET_VEHICLE_MOD(viTrailer, INT_TO_ENUM(MOD_TYPE, iExtra))
//				ENDREPEAT
			ELSE
				VECTOR pos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VDPersonalTruckDebugData.vVehicleCoord, VDPersonalTruckDebugData.fTrailerHeadingOffset, VDPersonalTruckDebugData.vTrailerOffset)
				SET_ENTITY_COORDS(viTrailer, pos)
				SET_ENTITY_HEADING(viTrailer, VDPersonalTruckDebugData.fVehicleHeading+VDPersonalTruckDebugData.fTrailerHeadingOffset)
				
				SET_VEHICLE_COLOURS(viTrailer, VDPersonalTruckDebugData.iTrailerColours[0], VDPersonalTruckDebugData.iTrailerColours[1])
				SET_VEHICLE_EXTRA_COLOURS(viTrailer, VDPersonalTruckDebugData.iTrailerColours[2], VDPersonalTruckDebugData.iTrailerColours[3])
				
				REPEAT COUNT_OF(VDPersonalTruckDebugData.bTrailerExtras) iExtra
					SET_VEHICLE_EXTRA(viTrailer, iExtra+1, NOT VDPersonalTruckDebugData.bTrailerExtras[iExtra])
				ENDREPEAT
//				REPEAT COUNT_OF(VDPersonalTruckDebugData.iTrailerMods) iExtra
//					SET_VEHICLE_MOD(viTrailer, INT_TO_ENUM(MOD_TYPE, iExtra), VDPersonalTruckDebugData.iTrailerMods[iExtra])
//				ENDREPEAT
			ENDIF
		ELSE
			VDPersonalTruckDebugData.bMoveTrailer = FALSE
			bCanDrawDebug = FALSE
			VDPersonalTruckDebugData.vTrailerOffset = <<0,0,0>>
			VDPersonalTruckDebugData.iTrailerColours[0] = 0
			VDPersonalTruckDebugData.iTrailerColours[1] = 0
			VDPersonalTruckDebugData.iTrailerColours[2] = 0
			VDPersonalTruckDebugData.iTrailerColours[3] = 0
		ENDIF
		
		IF NOT bCanDrawDebug
			VDPersonalTruckDebugData.bMoveTruck = FALSE
		ENDIF
	ENDIF
	
	IF VDPersonalTruckDebugData.bAttachTrailer
		ar_ATTACH_VEHICLE_TO_TRAILER(viTruck, viTrailer, VDPersonalTruckDebugData.fAttachTrailer, DEFAULT)
		
		SET_TRAILER_LEGS_RAISED(viTrailer)
	//	VDPersonalTruckDebugData.bAttachTrailer = FALSE
	ELSE
		VDPersonalTruckDebugData.fAttachTrailer = VDPersonalTruckDebugData.fTrailerHeadingOffset
	ENDIF
	IF VDPersonalTruckDebugData.bDetachTrailer
		DETACH_VEHICLE_FROM_TRAILER(viTruck)
		VDPersonalTruckDebugData.bDetachTrailer = FALSE
		VDPersonalTruckDebugData.bAttachTrailer = FALSE
	ENDIF
	
	IF VDPersonalTruckDebugData.bOpenTrailerDoor
		SET_VEHICLE_DOOR_OPEN(viTrailer, INT_TO_ENUM(SC_DOOR_LIST, VDPersonalTruckDebugData.iTrailerDoor), DEFAULT, FALSE)
		VDPersonalTruckDebugData.bOpenTrailerDoor = FALSE
	ENDIF
	IF VDPersonalTruckDebugData.bCloseTrailerDoor
		SET_VEHICLE_DOOR_SHUT(viTrailer, INT_TO_ENUM(SC_DOOR_LIST, VDPersonalTruckDebugData.iTrailerDoor), FALSE)
		VDPersonalTruckDebugData.bCloseTrailerDoor = FALSE
	ENDIF
	
	BOOL bOverridingRatio = FALSE
	IF VDPersonalTruckDebugData.fOverride_extenable_side_ratio != -1.0
		bOverridingRatio = TRUE
		IF NOT VDPersonalTruckDebugData.bOverride_side_ratio
			VEHICLE_SET_OVERRIDE_EXTENABLE_SIDE_RATIO(viTrailer, TRUE)
			VDPersonalTruckDebugData.bOverride_side_ratio = TRUE
		ENDIF
		VEHICLE_SET_OVERRIDE_SIDE_RATIO(viTrailer, VDPersonalTruckDebugData.fOverride_extenable_side_ratio)
	ENDIF
	IF VDPersonalTruckDebugData.fExtenable_side_target_ratio != -1.0
		bOverridingRatio = TRUE
		IF NOT VDPersonalTruckDebugData.bOverride_side_ratio
			VEHICLE_SET_OVERRIDE_EXTENABLE_SIDE_RATIO(viTrailer, TRUE)
			VDPersonalTruckDebugData.bOverride_side_ratio = TRUE
		ENDIF
		VEHICLE_SET_EXTENABLE_SIDE_TARGET_RATIO(viTrailer, VDPersonalTruckDebugData.fExtenable_side_target_ratio)
	ENDIF
	IF NOT bOverridingRatio
	AND VDPersonalTruckDebugData.bOverride_side_ratio
		VEHICLE_SET_OVERRIDE_EXTENABLE_SIDE_RATIO(viTrailer, FALSE)
		VDPersonalTruckDebugData.bOverride_side_ratio = FALSE
	ENDIF
ENDPROC
#ENDIF


