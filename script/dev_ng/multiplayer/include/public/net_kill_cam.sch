

// ====================================================================================
// ====================================================================================
//
// Name:        net_kill_cam.sch
// Description: Kill cam functions called in freemode.sc and death events
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================


// =================
// 	Using headers
// =================

USING "script_player.sch"
USING "net_spectator_cam_common.sch"

USING "net_gang_boss_common.sch"

CONST_FLOAT MAX_KILL_CAM_DISTANCE_SQR				10000.0//100sqr
CONST_FLOAT MAX_KILL_CAM_HINT_DISTANCE_SQR			2500.0//50sqr
CONST_INT KILL_CAM_HINT_TIME						2000//1sec

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Returns a Kill Cam stage ID as a string
/// PARAMS:
///    thisStage - Kill Cam stage ID to convert
/// RETURNS:
///    KILL_CAM_STAGE as a string
FUNC STRING GET_KILL_CAM_STAGE_NAME(KILL_CAM_STAGE thisStage)
	SWITCH thisStage
		CASE KILL_CAM_STAGE_OFF			RETURN "KILL_CAM_STAGE_OFF"			BREAK
		CASE KILL_CAM_STAGE_CORPSE		RETURN "KILL_CAM_STAGE_CORPSE"		BREAK
		CASE KILL_CAM_STAGE_HINT		RETURN "KILL_CAM_STAGE_HINT"		BREAK
		CASE KILL_CAM_STAGE_FOLLOW		RETURN "KILL_CAM_STAGE_FOLLOW"		BREAK
	ENDSWITCH
	RETURN "INVALID KILL CAM STAGE"
ENDFUNC

#ENDIF

/// PURPOSE:
///    Cleans up the current stage and initialises the requested stage. Sets the requested stage as a current stage. Prints debug for this process
/// PARAMS:
///    thisStage - requested current stage
PROC SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE thisStage)
	
	//Cleanup old stage
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === Cleaning up ", GET_KILL_CAM_STAGE_NAME(g_KillCamStruct.eCurrentStage))
	#ENDIF
	SWITCH g_KillCamStruct.eCurrentStage
		
		CASE KILL_CAM_STAGE_OFF
			
		BREAK
		
		CASE KILL_CAM_STAGE_CORPSE
			
		BREAK
		
		CASE KILL_CAM_STAGE_HINT
			STOP_GAMEPLAY_HINT(TRUE)
		BREAK
		
		CASE KILL_CAM_STAGE_FOLLOW
			SET_IN_SPECTATOR_MODE(FALSE, NULL, TRUE)
			//CLEANUP_CAM_VIEW_MODES_CINEMATIC(g_KillCamStruct.camViewMode, TRUE)
			IF NOT g_sMPTunables.bDISABLE_FIX_FOR_3162657
				IF thisStage = KILL_CAM_STAGE_OFF
					SET_SPEC_CAM_MODES(FALSE)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Setup new stage
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === Setting up ", GET_KILL_CAM_STAGE_NAME(thisStage))
	#ENDIF
	SWITCH thisStage
		
		CASE KILL_CAM_STAGE_OFF
			PED_INDEX pedNull
			g_KillCamStruct.pedCurrentFocus = pedNull
		BREAK
		
		CASE KILL_CAM_STAGE_CORPSE	//look at the player corpse (default view)
			
		BREAK
		
		CASE KILL_CAM_STAGE_HINT	//hint the cam towards a nearby killer
			IF IS_PED_IN_ANY_VEHICLE(g_KillCamStruct.pedCurrentFocus)
			AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(g_KillCamStruct.pedCurrentFocus, TRUE))
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(g_KillCamStruct.pedCurrentFocus, TRUE))
				ENDIF
				SET_GAMEPLAY_ENTITY_HINT(	GET_VEHICLE_PED_IS_IN(g_KillCamStruct.pedCurrentFocus, TRUE), <<0.0,0.0,0.0>>, TRUE, -1, 3000, 
											DEFAULT_INTERP_OUT_TIME, HINTTYPE_KILLER_CAM)
			ELSE
				SET_GAMEPLAY_ENTITY_HINT(g_KillCamStruct.pedCurrentFocus, <<0.0,0.0,0.0>>, TRUE, -1, 3000, DEFAULT_INTERP_OUT_TIME, HINTTYPE_KILLER_CAM)
			ENDIF
		BREAK
		
		CASE KILL_CAM_STAGE_FOLLOW	//switch the camera to spectate the killer
			//SETUP_CAM_VIEW_MODES_CINEMATIC(g_KillCamStruct.camViewMode, TRUE)
			SET_IN_SPECTATOR_MODE(TRUE, g_KillCamStruct.pedCurrentFocus, TRUE)
			SET_SPEC_CAM_MODES(TRUE)
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() //For Bug 2031300
		BREAK
		
	ENDSWITCH
	
	//Store new stage
	g_KillCamStruct.eCurrentStage = thisStage
		
ENDPROC

/// PURPOSE:
///    Sets up the kill cam using the ped index passed, selects a kill cam stage to begin with
/// PARAMS:
///    thisTarget - ped index of the killer ped
PROC SETUP_KILL_CAM(PED_INDEX thisTarget)

	IF DOES_ENTITY_EXIST(thisTarget)
		
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		ENDIF
		
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		IF IS_PED_A_PLAYER(thisTarget)
		AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisTarget))
			CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === SETUP_KILL_CAM ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisTarget)))
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === SETUP_KILL_CAM with invalid player")
		ENDIF
		#ENDIF
		
		g_KillCamStruct.pedCurrentFocus = thisTarget
		
		g_KillCamStruct.iTime = GET_GAME_TIMER()
		
		
		// If the target is a player, check we are not in hunt the boss mode (2584223)
		IF IS_PED_A_PLAYER(thisTarget)
			PLAYER_INDEX targetPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisTarget)
			IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_HUNT_THE_BOSS
				IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = targetPlayer
					CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === SETUP_KILL_CAM with player who is the boss in HUNT THE BOSS")
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
		/*FLOAT fDistance = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_KillCamStruct.pedCurrentFocus))	//commented out so will always go to
		IF fDistance < MAX_KILL_CAM_DISTANCE_SQR																			//hint cam straight away
			IF IS_PED_IN_ANY_VEHICLE(g_KillCamStruct.pedCurrentFocus, TRUE)
				SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_FOLLOW)
			ELSE
				IF fDistance < MAX_KILL_CAM_HINT_DISTANCE_SQR*/
					SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_HINT)
				/*ELSE
					SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_FOLLOW)
				ENDIF
			ENDIF
		ELSE
			SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_CORPSE)
		ENDIF*/
		
	ENDIf
	
ENDPROC

/// PURPOSE:
///    Cleans up the kill cam, returning the camera to observing the local player ped
PROC CLEANUP_KILL_CAM()

	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === CLEANUP_KILL_CAM")
	#ENDIF
	
	SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_OFF)
	
ENDPROC

/// PURPOSE:
///    Maintains the kill cam that controls the camera when the local player is dead
PROC MAINTAIN_KILL_CAM()
	FLOAT fDistance
	
	IF g_KillCamStruct.eCurrentStage <> KILL_CAM_STAGE_OFF	//if the kill cam is running
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === LOCAL PLAYER NOT DEAD")
			#ENDIF
			CLEANUP_KILL_CAM()
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_GAME_TIMER() % 1500 < 50
	            CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === Disabled player + camera control as g_KillCamStruct.eCurrentStage = ", GET_KILL_CAM_STAGE_NAME(g_KillCamStruct.eCurrentStage))
            ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	SWITCH g_KillCamStruct.eCurrentStage
		
		CASE KILL_CAM_STAGE_OFF
			
		BREAK
		
		CASE KILL_CAM_STAGE_CORPSE	//look at the player corpse (default view)
			IF NOT IS_PAUSE_MENU_ACTIVE()
				DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
				DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	            CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === KILL_CAM_STAGE_CORPSE - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RESPAWN_FASTER)
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					// B*2240084 Allow chat when waiting to respawn
					ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_ALL)
					ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_TEAM)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE KILL_CAM_STAGE_HINT	//hint the cam towards a nearby killer
			IF NOT IS_PAUSE_MENU_ACTIVE()
				DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
				DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	            CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === KILL_CAM_STAGE_HINT - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RESPAWN_FASTER)
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					// B*2240084 Allow chat when waiting to respawn
					ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_ALL)
					ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_TEAM)
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() - g_KillCamStruct.iTime > KILL_CAM_HINT_TIME	//if we've been hint camming for a minimum amount of time
				IF NOT DOES_ENTITY_EXIST(g_KillCamStruct.pedCurrentFocus)
				OR IS_ENTITY_DEAD(g_KillCamStruct.pedCurrentFocus)				//if killer ped is dead or not valid
					SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_CORPSE)		//camera back to local corpse
				ELSE
					IF IS_PED_IN_ANY_VEHICLE(g_KillCamStruct.pedCurrentFocus, TRUE)	//if killer ped gets in a vehicle
						SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_FOLLOW)		//spec cam
					ELSE
						fDistance = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_KillCamStruct.pedCurrentFocus))
						IF fDistance < MAX_KILL_CAM_DISTANCE_SQR
							IF fDistance > MAX_KILL_CAM_HINT_DISTANCE_SQR
							OR IS_ENTITY_OCCLUDED(g_KillCamStruct.pedCurrentFocus)		//if killer is far away, or something is blocking the view
								IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != NULL
								OR GET_INTERIOR_FROM_ENTITY(g_KillCamStruct.pedCurrentFocus) != NULL
									IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = GET_INTERIOR_FROM_ENTITY(g_KillCamStruct.pedCurrentFocus)
										SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_FOLLOW)	//spec cam
									ENDIF
								ELSE
									SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_FOLLOW)	//spec cam
								ENDIF
							ENDIF
						ELSE															//if ped is nearby
							SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_CORPSE)		//return camera to local corpse
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE KILL_CAM_STAGE_FOLLOW	//switch the camera to spectate the killer
			
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() //For Bug 2031300
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
				DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
				DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	            CPRINTLN(DEBUG_SPECTATOR, "=== KILL CAM === KILL_CAM_STAGE_FOLLOW - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE) //FOR BUG 1844204
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RESPAWN_FASTER)
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					// B*2240084 Allow chat when waiting to respawn
					ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_ALL)
					ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MP_TEXT_CHAT_TEAM)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(g_KillCamStruct.pedCurrentFocus)		//if killer is dead
			OR IS_ENTITY_DEAD(g_KillCamStruct.pedCurrentFocus)
				SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_CORPSE)		//return cam to local corpse
			ELSE
				IF IS_PED_A_PLAYER(g_KillCamStruct.pedCurrentFocus)
				AND IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(NETWORK_GET_PLAYER_INDEX_FROM_PED(g_KillCamStruct.pedCurrentFocus))
				AND (GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(NETWORK_GET_PLAYER_INDEX_FROM_PED(g_KillCamStruct.pedCurrentFocus)) = SIMPLE_INTERIOR_ARMORY_TRUCK_1 
				OR GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(NETWORK_GET_PLAYER_INDEX_FROM_PED(g_KillCamStruct.pedCurrentFocus))) = SIMPLE_INTERIOR_TYPE_BUNKER
				OR GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(NETWORK_GET_PLAYER_INDEX_FROM_PED(g_KillCamStruct.pedCurrentFocus))) = SIMPLE_INTERIOR_TYPE_HANGAR
				OR GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(NETWORK_GET_PLAYER_INDEX_FROM_PED(g_KillCamStruct.pedCurrentFocus))) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE)
					SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_CORPSE)	// if the killer is entering MOC or bunker, return cam to local corpse
				ENDIF
				
				fDistance = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(g_KillCamStruct.pedCurrentFocus))
				IF fDistance > MAX_KILL_CAM_DISTANCE_SQR					//if killer is far away
					SWITCH_CURRENT_KILL_CAM_STAGE(KILL_CAM_STAGE_CORPSE)	//return cam to local corpse
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
		
ENDPROC


