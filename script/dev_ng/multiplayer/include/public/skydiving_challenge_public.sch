//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        luxury_showroom_public.sch																			//
// Description: Contains public functions for maintaining the luxury showroom located opposite the music studio.	//
// 				url:bugstar:7447898 																				//
// Written by:  Joe Davis																							//
// Date:  		17/02/2022																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_GameDir.sch"
USING "net_gang_boss.sch"

#IF FEATURE_DLC_1_2022

/// PURPOSE:
///    Deletes the freemode skydiving challenge props and scenario block.
PROC CLEANUP_SKYDIVING_PROPS(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	IF DOES_ENTITY_EXIST(sData.objParachuteCabin)
		DELETE_OBJECT(sData.objParachuteCabin)
	ENDIF
	IF DOES_ENTITY_EXIST(sData.objSign)
		DELETE_OBJECT(sData.objSign)
	ENDIF
	IF sData.sbiScenarioBlocker != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sData.sbiScenarioBlocker)
		sData.sbiScenarioBlocker = NULL
	ENDIF
ENDPROC

FUNC SKYDIVING_LOCATION_STRUCT GET_SKYDIVING_CHALLENGE_LOCATION_DATA(INT iSkydivingLocation)
	SKYDIVING_LOCATION_STRUCT sOutput
	SWITCH iSkydivingLocation
		CASE 0
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<-121.2990, -962.7370, 26.5430>>
			sOutput.fHeading	= 339.8000
			sOutput.vRotation	= <<0.0000, 0.0000, -20.2000>>
			
			sOutput.model2		= reh_prop_reh_sign_jk_01a
			sOutput.vCoords2	= <<-121.3620, -962.9480, 26.9770>>
			sOutput.fHeading2	= 160.2000
			sOutput.vRotation2	= <<0.0000, 0.0000, 160.2000>>

		BREAK
		CASE 1
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<153.5065, -721.2749, 46.3450>>
			sOutput.fHeading	= 338.1990
			sOutput.vRotation	= <<0.0000, 0.0000, -21.8010>>
			
			sOutput.model2		= reh_prop_reh_sign_jk_01a
			sOutput.vCoords2	= <<153.4370, -721.4560, 46.8340>>
			sOutput.fHeading2	= 156.7990
			sOutput.vRotation2	= <<0.0000, 0.0000, 156.7990>>

		BREAK
		CASE 2
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<-812.6889, 300.0814, 85.4293>>
			sOutput.fHeading	= 270.5990
			sOutput.vRotation	= <<0.0000, 0.0000, -89.4010>>
			
			sOutput.model2		= reh_prop_reh_sign_jk_01a
			sOutput.vCoords2	= <<-812.8720, 299.7560, 85.5420>>
			sOutput.fHeading2	= 89.9980
			sOutput.vRotation2	= <<0.0000, 0.0000, 89.9980>>
		BREAK
		CASE 3
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<-1223.9670, 3854.4700, 488.6330>>
			sOutput.fHeading	= 261.4010
			sOutput.vRotation	= <<0.0000, 0.0000, -98.5990>>
			
			sOutput.model2		= INT_TO_ENUM(MODEL_NAMES, hash("prop_rock_4_c_2"))
			sOutput.vCoords2	= <<-1224.9281, 3854.4878, 488.0164>>
			sOutput.fHeading2	= 67.5994
			sOutput.vRotation2	= <<0.0000, 0.0000, 67.5994>>
		BREAK
		CASE 4
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<426.3270, 5612.6611, 765.6420>>
			sOutput.fHeading	= 9.1990
			sOutput.vRotation	= <<-0.0130, 5.8900, 9.2480>>
			
			sOutput.model2		= reh_prop_reh_sign_jk_01a
			sOutput.vCoords2	= <<426.3870, 5612.5681, 766.1630>>
			sOutput.fHeading2	= 189.0270
			sOutput.vRotation2	= <<2.9410, -5.0500, -170.6970>>
		BREAK
		CASE 5
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<503.8620, 5506.4380, 773.4060>>
			sOutput.fHeading	= 311.4000
			sOutput.vRotation	= <<0.0000, 0.0000, -48.6000>>
		BREAK
		CASE 6
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<804.9200, 5716.2471, 694.7060>>
			sOutput.fHeading	= 349.5450
			sOutput.vRotation	= <<0.0000, -4.5440, -10.4870>>
		BREAK
		CASE 7
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<-859.6970, 4725.7729, 275.6470>>
			sOutput.fHeading	= 30.2000
			sOutput.vRotation	= <<0.0000, 0.0000, 30.2000>>
		BREAK
		CASE 8
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<1717.6105, 3295.6414, 40.4830>>
			sOutput.fHeading	= 197.1990
			sOutput.vRotation	= <<0.0000, -0.0000, -162.8010>>
			
			sOutput.model2		= reh_prop_reh_sign_jk_01a
			sOutput.vCoords2	= <<1717.5520, 3295.8235, 40.9418>>
			sOutput.fHeading2	= 15.5990
			sOutput.vRotation2	= <<0.0000, 0.0000, 15.5990>>
		BREAK
		CASE 9
			sOutput.model		= reh_prop_reh_bag_para_01a
			sOutput.vCoords		= <<2033.5729, 4733.2383, 40.8810>>
			sOutput.fHeading	= 25.7990
			sOutput.vRotation	= <<0.0000, 0.0000, 25.7990>>
			
			sOutput.model2		= reh_prop_reh_sign_jk_01a
			sOutput.vCoords2	= <<2033.6775, 4733.0479, 41.3508>>
			sOutput.fHeading2	= 204.7989
			sOutput.vRotation2	= <<0.0000, -0.0000, -155.2010>>
		BREAK
		CASE 10
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-1167.2120, -2494.6211, 12.9560>>
			sOutput.fHeading	= 330.4490
			sOutput.vRotation	= <<0.0000, 0.0000, -29.5510>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<-1171.3610, -2494.1509, 12.9450>>
			sOutput.fHeading2	= 177.2480
		BREAK
		CASE 11
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<2790.3999, 1465.6350, 23.5180>>
			sOutput.fHeading	= 254.6480
			sOutput.vRotation	= <<0.0000, 0.0000, -105.3520>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<2787.8000, 1462.2430, 23.5040>>
			sOutput.fHeading2	= 54.8480
		BREAK
		CASE 12
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-782.1660, -1452.2850, 4.0130>>
			sOutput.fHeading	= 50.0480
			sOutput.vRotation	= <<0.0000, 0.0000, 50.0480>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<-778.7060, -1449.1630, 4.0010>>
			sOutput.fHeading2	= 101.0480
		BREAK
		CASE 13
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-559.4300, -909.0310, 22.8630>>
			sOutput.fHeading	= 179.4470
			sOutput.vRotation	= <<0.0000, 0.0000, 179.4470>>

			sOutput.model2		= SANCHEZ2
			sOutput.vCoords2	= <<-563.0560, -907.1520, 22.8630>>
			sOutput.fHeading2	= 145.2470
		BREAK
		CASE 14
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-136.5510, 6356.9668, 30.4920>>
			sOutput.fHeading	= 314.8460
			sOutput.vRotation	= <<0.0000, 0.0000, -45.1540>>

			sOutput.model2		= SANCHEZ2
			sOutput.vCoords2	= <<-139.0860, 6359.8838, 30.4910>>
			sOutput.fHeading2	= 330.6460
		BREAK
		CASE 15
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<742.9500, 2535.9351, 72.1560>>
			sOutput.fHeading	= 0.4460
			sOutput.vRotation	= <<0.0000, 0.0000, 0.4460>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<747.2520, 2534.1870, 72.1470>>
			sOutput.fHeading2	= 336.4460
		BREAK
		CASE 16
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-2952.7900, 441.3630, 14.2510>>
			sOutput.fHeading	= 267.8460
			sOutput.vRotation	= <<0.0000, 0.0000, -92.1540>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<-2956.5791, 438.6640, 14.2790>>
			sOutput.fHeading2	= 66.0450
		BREAK
		CASE 17
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-1522.1130, 1491.6420, 110.5950>>
			sOutput.fHeading	= 165.0450
			sOutput.vRotation	= <<0.0000, 0.0000, 165.0450>>
		BREAK
		CASE 18
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<261.5550, -209.2910, 60.5660>>
			sOutput.fHeading	= 158.8440
			sOutput.vRotation	= <<0.0000, 0.0000, 158.8440>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<265.8970, -210.4780, 60.5710>>
			sOutput.fHeading2	= 116.8430
		BREAK
		CASE 19
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<739.4191, -1223.1754, 23.7705>>
			sOutput.fHeading	= 74.9998
			sOutput.vRotation	= <<0.0000, 0.0000, 74.9998>>

			sOutput.model2		= SANCHEZ2
			sOutput.vCoords2	= <<742.6091, -1219.9139, 23.7677>>
			sOutput.fHeading2	= 56.7998
		BREAK
		CASE 20
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-1724.4279, -1129.7800, 12.0438>>
			sOutput.fHeading	= 139.7993
			sOutput.vRotation	= <<0.0000, -0.0000, 139.7993>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<-1726.9906, -1126.3635, 12.0294>>
			sOutput.fHeading2	= 305.3989
		BREAK		
		CASE 21		
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<735.9623, 1303.1774, 359.2930>>
			sOutput.fHeading	= 6.3988
			sOutput.vRotation	= <<0.0000, 0.0000, 6.3988>>

			sOutput.model2		= SANCHEZ2
			sOutput.vCoords2	= <<740.3937, 1304.3322, 359.2967>>
			sOutput.fHeading2	= 342.3988
		BREAK		
		CASE 22		
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<2555.3403, 301.0995, 107.4623>>
			sOutput.fHeading	= 270.1980
			sOutput.vRotation	= <<0.0000, 0.0000, -89.8020>>

			sOutput.model2		= BLAZER
			sOutput.vCoords2	= <<2555.6204, 296.0918, 107.4606>>
			sOutput.fHeading2	= 234.1978
		BREAK		
		CASE 23		
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-1143.5713, 2683.3020, 17.0937>>
			sOutput.fHeading	= 6.7976
			sOutput.vRotation	= <<0.0000, 0.0000, 6.7976>>

			sOutput.model2		= SANCHEZ2
			sOutput.vCoords2	= <<-1146.5444, 2680.5107, 17.0939>>
			sOutput.fHeading2	= 57.1975
		BREAK
		CASE 24
			sOutput.model		= reh_prop_reh_cabine_01a
			sOutput.vCoords		= <<-917.5775, -1155.1293, 3.7723>>
			sOutput.fHeading	= 29.7957
			sOutput.vRotation	= <<0.0000, 0.0000, 29.7957>>

			sOutput.model2		= SANCHEZ2
			sOutput.vCoords2	= <<-913.9274, -1157.9376, 3.7922>>
			sOutput.fHeading2	= 12.3957
		BREAK
	ENDSWITCH
	RETURN sOutput
ENDFUNC

FUNC FLOAT GET_SKYDIVING_TRIGGER_DISTANCE(INT iSkydivingLocation)
	SWITCH iSkydivingLocation
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
		CASE 8
		CASE 9
			RETURN 1.0
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
		CASE 16
		CASE 17
		CASE 18
		CASE 19
		CASE 20
		CASE 21
		CASE 22
		CASE 23
		CASE 24
		CASE 25
			RETURN 4.5
	ENDSWITCH
	RETURN 3.0
ENDFUNC

FUNC BOOL GET_SKYDIVING_USES_HELI(INT iSkydivingLocation)
	SWITCH iSkydivingLocation
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
		CASE 8
		CASE 9
		CASE 10
			RETURN FALSE
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
		CASE 16
		CASE 17
		CASE 18
		CASE 19
		CASE 20
		CASE 21
		CASE 22
		CASE 23
		CASE 24
		CASE 25
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SKYDIVING_PLACE_OBJECT_MANUALLY(INT iSkydivingLocation)
	SWITCH iSkydivingLocation
		CASE 5	RETURN TRUE
		CASE 7	RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_SKYDIVING_PARACHUTE_CABIN(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	IF DOES_ENTITY_EXIST(sData.objParachuteCabin)
		DELETE_OBJECT(sData.objParachuteCabin)
	ELSE
		INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
		IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
			RETURN FALSE
		ENDIF
		SKYDIVING_LOCATION_STRUCT sLocation = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)

		sData.objParachuteCabin = CREATE_OBJECT(sLocation.model,sLocation.vCoords,FALSE)
		IF NOT SKYDIVING_PLACE_OBJECT_MANUALLY(iSkydivingLocation)
			PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(sData.objParachuteCabin)
		ENDIF
		SET_ENTITY_ROTATION(sData.objParachuteCabin,sLocation.vRotation)
		SET_ENTITY_INVINCIBLE(sData.objParachuteCabin, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(sData.objParachuteCabin, FALSE)
		FREEZE_ENTITY_POSITION(sData.objParachuteCabin, TRUE)
		SET_ENTITY_LOD_DIST(sData.objParachuteCabin, 150)
		SET_MODEL_AS_NO_LONGER_NEEDED(sLocation.model)
		
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_SKYDIVING_VEHICLE_MODS(MODEL_NAMES mnVehicleModel, VEHICLE_SETUP_STRUCT_MP& sData)
	SWITCH mnVehicleModel
		CASE sanchez2	
			sData.VehicleSetup.eModel = sanchez2 // SANCHEZ02
			sData.VehicleSetup.tlPlateText = "JUNK"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 54
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 6
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK	

		CASE BLAZER	
			sData.VehicleSetup.eModel = BLAZER // BLAZER
			sData.VehicleSetup.tlPlateText = "JUNK"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 54
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
		BREAK	

		CASE MAVERICK	
			sData.VehicleSetup.eModel = MAVERICK // MAVERICK
			sData.VehicleSetup.tlPlateText = "24BUE439"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 54
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 155
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
		BREAK	
	ENDSWITCH
ENDPROC

FUNC BOOL GET_SKYDIVING_USES_SIGN(INT iSkydivingLocation)
	SWITCH iSkydivingLocation
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
		CASE 8
		CASE 9
			RETURN TRUE
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
		CASE 16
		CASE 17
		CASE 18
		CASE 19
		CASE 20
		CASE 21
		CASE 22
		CASE 23
		CASE 24
		CASE 25
			RETURN FALSE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SET_SKYDIVING_VEHICLE_DECORATORS(VEHICLE_INDEX vehId)

	SET_VEHICLE_CANNOT_BE_STORED_IN_PROPERTIES(vehId)
	
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		INT iDecoratorValue
		IF DECOR_EXIST_ON(vehId, "MPBitset")
			iDecoratorValue = DECOR_GET_INT(vehId, "MPBitset")
		ENDIF
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_PASSIVE_MODE_VEHICLE)
		SET_BIT(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)
		DECOR_SET_INT(vehId, "MPBitset", iDecoratorValue)
		
		PRINTLN("[fm_content_skydive] - SET_MISSION_VEHICLE_COMMON_DECORATORS") 
	ENDIF

ENDPROC

PROC CREATE_SKYDIVING_CABIN_SIDE_VEHICLE(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	IF NOT IS_BIT_SET(sData.iBitset, ciSKYDIVING_CHALLENGE_VEHICLE_CREATED)
		INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
		IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
			EXIT
		ENDIF
		SKYDIVING_LOCATION_STRUCT sLocation = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
		IF NOT GET_SKYDIVING_USES_SIGN(iSkydivingLocation)
			IF sLocation.model2 != DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(sLocation.model2)
				IF HAS_MODEL_LOADED(sLocation.model2)
					IF CREATE_NET_VEHICLE(sData.netCabinVeh,sLocation.model2,sLocation.vCoords2,sLocation.fHeading2,FALSE)
						IF DOES_ENTITY_EXIST(NET_TO_ENT(sData.netCabinVeh))
							SET_ENTITY_INVINCIBLE(NET_TO_ENT(sData.netCabinVeh), TRUE)
							SET_ENTITY_CAN_BE_DAMAGED(NET_TO_ENT(sData.netCabinVeh), FALSE)
							FREEZE_ENTITY_POSITION(NET_TO_ENT(sData.netCabinVeh), FALSE)
							SET_ENTITY_LOD_DIST(NET_TO_ENT(sData.netCabinVeh), 150)
							SET_MODEL_AS_NO_LONGER_NEEDED(sLocation.model2)
							VEHICLE_SETUP_STRUCT_MP sVehicelModsData
							SET_SKYDIVING_VEHICLE_MODS(sLocation.model2,sVehicelModsData)
							SET_VEHICLE_SETUP_MP(NET_TO_VEH(sData.netCabinVeh), sVehicelModsData, FALSE, DEFAULT, TRUE)
							SET_SKYDIVING_VEHICLE_DECORATORS(NET_TO_VEH(sData.netCabinVeh))
							SET_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_VEHICLE_CREATED)
							PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_CABIN_SIDE_VEHICLE - Entity created. Location: ",iLocation)
							CLEANUP_NET_ID(sData.netCabinVeh)
						ELSE
							PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_CABIN_SIDE_VEHICLE - Entity doesn't exist after trying to create. Location: ",iLocation)
						ENDIF
					ELSE
						PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_CABIN_SIDE_VEHICLE - Model hasn't loaded yet. Location: ",iSkydivingLocation)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CREATE_SKYDIVING_SIGN(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
	IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
		EXIT
	ENDIF
	SKYDIVING_LOCATION_STRUCT sLocation = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
	IF GET_SKYDIVING_USES_SIGN(iSkydivingLocation)
		PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_SIGN - GET_SKYDIVING_USES_SIGN ")
		IF DOES_ENTITY_EXIST(sData.objSign)
			DELETE_OBJECT(sData.objSign)
		ELSE
			PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_SIGN - NOT DOES_ENTITY_EXIST ")
			IF sLocation.model2 != DUMMY_MODEL_FOR_SCRIPT
				PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_SIGN - sLocation.model2 != DUMMY_MODEL_FOR_SCRIPT ")
				sData.objSign = CREATE_OBJECT(sLocation.model2,sLocation.vCoords2,FALSE)
				SET_ENTITY_ROTATION(sData.objSign,sLocation.vRotation2)
				SET_ENTITY_INVINCIBLE(sData.objSign, TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(sData.objSign, FALSE)
				FREEZE_ENTITY_POSITION(sData.objSign, TRUE)
				SET_ENTITY_LOD_DIST(sData.objSign, 150)
				SET_MODEL_AS_NO_LONGER_NEEDED(sLocation.model2)
			ENDIF
		ENDIF
	ELSE
	ENDIF
ENDPROC

PROC CLEANUP_SKYDIVING_SCENARIO_BLOCKER(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	IF sData.sbiScenarioBlocker != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sData.sbiScenarioBlocker)
		sData.sbiScenarioBlocker = NULL
	ENDIF
	PRINTLN("[fm_content_skydive] CLEANUP_SKYDIVING_SCENARIO_BLOCKER ")
ENDPROC

PROC CREATE_SKYDIVING_SCENARIO_BLOCKER(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	CLEANUP_SKYDIVING_SCENARIO_BLOCKER(sData)
	INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
	IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
		EXIT
	ENDIF
	SKYDIVING_LOCATION_STRUCT sLocation = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
	VECTOR vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sLocation.vCoords,sLocation.fHeading,<<-10.0,-10.0,-5.0>>)
	VECTOR vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sLocation.vCoords,sLocation.fHeading,<<10.0,10.0,10.0>>)
	sData.sbiScenarioBlocker = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
	CLEAR_AREA_OF_PEDS(sLocation.vCoords,20)
	PRINTLN("[fm_content_skydive] CREATE_SKYDIVING_SCENARIO_BLOCKER - Scenario blocker created for location ",iLocation)
ENDPROC

FUNC INT GET_CLOSEST_SKYDIVING_CHALLENGE_LOCATION()
	INT iSkydivingLocation,iOrder,iClosest
	FLOAT fClosestDistance = -1
	FLOAT fDist
	SKYDIVING_LOCATION_STRUCT sLocation
	REPEAT ciSKYDIVING_CHALLENGE_MAX_LOCATIONS iOrder
		iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iOrder))
		IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
			RELOOP
		ENDIF
		sLocation = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
		fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), sLocation.vCoords)
		IF fDist < fClosestDistance
		OR fClosestDistance = -1
			iClosest = iOrder
			fClosestDistance = fDist
		ENDIF
	ENDREPEAT
	RETURN iClosest
ENDFUNC

FUNC BOOL DOES_SKYDIVING_CHALLENGE_PARACHUTE_CABIN_EXIST(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT sData)
	IF NOT DOES_ENTITY_EXIST(sData.objParachuteCabin)
		RETURN FALSE
	ENDIF
	INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
	IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
		RETURN FALSE
	ENDIF
	SKYDIVING_LOCATION_STRUCT sLocationData = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sData.objParachuteCabin,sLocationData.vCoords) < 1.0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SKYDIVING_PARACHUTE_CABIN_EXIST(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT sData)
	IF NOT g_sMPTunables.bCollectables_Skydives
		RETURN FALSE
	ENDIF
	INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
	IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
		RETURN FALSE
	ENDIF
	SKYDIVING_LOCATION_STRUCT sLocation = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
	IF sLocation.model = reh_prop_reh_bag_para_01a
		IF sData.bChallengeRunning
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_SKYDIVING_CHALLENGE_SIGN_EXIST(INT iLocation, SKYDIVING_CHALLENGE_FLOW_STRUCT sData)
	INT iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iLocation))
	IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
		RETURN FALSE
	ENDIF
	SKYDIVING_LOCATION_STRUCT sLocationData = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
	IF GET_SKYDIVING_USES_SIGN(iSkydivingLocation)
		IF NOT DOES_ENTITY_EXIST(sData.objSign)
			RETURN FALSE
		ENDIF
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sData.objSign,sLocationData.vCoords2) < 1.0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PROCESS_SKYDIVING_CHALLENGE_FLOW
	IF NOT g_sMPTunables.bCollectables_Skydives
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_PLAYER_IN_CORONA()
	OR GB_IS_LOCAL_PLAYER_PERMANENT_TO_GANG_BOSS_MISSION()
	OR FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
	OR IS_PLAYER_AN_ANIMAL(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	OR IS_PLAYER_ON_MP_INTRO()
	#ENDIF
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC SKYDIVING_CREATE_WIDGET(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	IF NOT IS_BIT_SET(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_WIDGET_CREATED)
		START_WIDGET_GROUP("Camera settings")
			ADD_WIDGET_BOOL("STOP AUTO-JUMP",sData.bStopAutojump)
			ADD_WIDGET_BOOL("Override Helicopter Cam (local coords to heli)",sData.bOverrideHeliCam)
			ADD_WIDGET_VECTOR_SLIDER("Start Coords",sData.vInitCoords,-50.0,50.0,0.1)
			ADD_WIDGET_VECTOR_SLIDER("End Coords",sData.vEndCoords,-50.0,50.0,0.1)
			ADD_WIDGET_VECTOR_SLIDER("Point at Start Coords",sData.vInitPointAtCoords,-50.0,50.0,0.1)
			ADD_WIDGET_VECTOR_SLIDER("Point at End Coords",sData.vEndPointAtCoords,-50.0,50.0,0.1)
			ADD_WIDGET_BOOL("Override Shoulder cam (local coords to player ped)",sData.bOverrideShoulderCam)
			ADD_WIDGET_VECTOR_SLIDER("Shoulder Coords offset",sData.vShoulderCoords,-50.0,50.0,0.1)
			ADD_WIDGET_VECTOR_SLIDER("Shoulder PointAt Coords",sData.vShoulderPointAtCoords,-360.0,360.0,0.1)
			ADD_WIDGET_FLOAT_SLIDER("Shoulder FOV",sData.fShoulderFOV,0.0,360.0,0.1)
			ADD_WIDGET_BOOL("Draw Shoulder PointAt Coords",sData.bDrawShoulderPointAt)
		STOP_WIDGET_GROUP()

		SET_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_WIDGET_CREATED)
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IS_LOCAL_PLAYER_FACING_SKYDIVING_COORD(VECTOR vCoord, VECTOR vCoordForward, FLOAT fAngleRange)
	
	VECTOR vPlayerForward = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
	VECTOR vPlayerToCoordDirection = NORMALISE_VECTOR(vCoord - GET_ENTITY_COORDS(PLAYER_PED_ID()))
	FLOAT fDot
	
	//If vCoordForward is non-zero, check the player is facing the front of the co-ordinate.
	IF NOT IS_VECTOR_ZERO(vCoordForward)
		//Check the player's position is in front of the co-ordinate.
		fDot = DOT_PRODUCT(vCoordForward, vPlayerToCoordDirection)
		
		IF NOT (fDot < 0.0)
			RETURN FALSE
		ENDIF
		
		//Get the dot product of the player's forward vector and the inverted co-ordinate's forward vector.
		fDot = DOT_PRODUCT(vPlayerForward, -vCoordForward)
	ELSE
		//Get the dot product of the player's forward vector and the direction vector from the player to the co-ordinate.
		fDot = DOT_PRODUCT(vPlayerForward, vPlayerToCoordDirection)
	ENDIF
	
	//Check the player is facing the co-ordinate within fAngleRange.
	IF ACOS(CLAMP(fDot, -1.0, 1.0)) > fAngleRange
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR SKYDIVING_GET_LOCATION_FORWARD_VECTOR(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData, INT iSkydivingLocation)

	IF GET_SKYDIVING_USES_SIGN(iSkydivingLocation)
		RETURN GET_ENTITY_FORWARD_VECTOR(sData.objParachuteCabin)
	ENDIF
	
	RETURN -GET_ENTITY_FORWARD_VECTOR(sData.objParachuteCabin)

ENDFUNC

FUNC FLOAT SKYDIVING_GET_LOCATION_HEADING(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData, INT iSkydivingLocation)

	IF GET_SKYDIVING_USES_SIGN(iSkydivingLocation)
		RETURN GET_ENTITY_HEADING(sData.objParachuteCabin)
	ENDIF
	
	RETURN -GET_ENTITY_HEADING(sData.objParachuteCabin)

ENDFUNC

FUNC BOOL IS_SKYDIVING_CHALLENGE_REACHABLE(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData, INT iSkydivingLocation)

	FLOAT fDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sData.objParachuteCabin)
	
	VECTOR vForward = SKYDIVING_GET_LOCATION_FORWARD_VECTOR(sData, iSkydivingLocation)
	
	IF fDist <= GET_SKYDIVING_TRIGGER_DISTANCE(iSkydivingLocation)
	AND IS_LOCAL_PLAYER_FACING_SKYDIVING_COORD(GET_ENTITY_COORDS(sData.objParachuteCabin), vForward, 90.0)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_SAME_Z_AS_SKYDIVING_LOCATION(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData,FLOAT fThreshold)
	VECTOR vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vLocationCoord = GET_ENTITY_COORDS(sData.objParachuteCabin)
	
	RETURN ABSF(vPlayerCoord.z - vLocationCoord.z) < fThreshold
ENDFUNC

FUNC BOOL IS_PLAYER_IN_FRONT_CABIN_PARACHUTE(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData, INT iSkydivingLocation)	
	
	VECTOR vCabinCoord = GET_ENTITY_COORDS(sData.objParachuteCabin, FALSE)
	FLOAT fCabinHeading = SKYDIVING_GET_LOCATION_HEADING(sData,iSkydivingLocation)
	
	VECTOR vMin
	VECTOR vMax
	FLOAT fWidth
	
	IF GET_SKYDIVING_USES_SIGN(iSkydivingLocation)
		// Parachute location
		vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCabinCoord, fCabinHeading, <<0.0, 0.0, 0.0>>)
		vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCabinCoord, fCabinHeading, <<0.0, 2.0, 1.0>>)
		fWidth = 2.0
	ELSE
		// Cabin location
		vMin = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCabinCoord, fCabinHeading, <<0.0, 2.0, 0.0>>)
		vMax = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCabinCoord, fCabinHeading, <<0.0, 2.0, 1.0>>)
		fWidth = 2.0
	ENDIF	
	
	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vMin, vMax, fWidth, FALSE, FALSE)
	
ENDFUNC

FUNC BOOL CAN_TRIGGER_SKYDIVING_CHALLENGE(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData, INT iSkydivingLocation, BOOL &bReturn)

	IF IS_BIT_SET(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DPAD_PRESSED) // IF DPAD HAS BEEN PRESSED, WE DON'T NEED TO CHECK THIS AS THE MISSION SHOULD BE LAUNCHING
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - DPAD was already pressed")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_SKYDIVING_CHALLENGE_PARACHUTE_CABIN_EXIST(sData.iClosestSkydiving,sData)
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Cabin doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYDIVING_CHALLENGE_REACHABLE(sData,iSkydivingLocation)
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Not reacheable")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), GET_ENTITY_COORDS(sData.objParachuteCabin, FALSE), 90.0)
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player not facing cabin/bag")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_SAME_Z_AS_SKYDIVING_LOCATION(sData,3.0) // 3.0 because the player z on the floor is at 1.5ish from the cabin z, on top is at 4.0ish
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player is not on the same z")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_IN_FRONT_CABIN_PARACHUTE(sData, iSkydivingLocation)
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - No clear LOS between player and cabin/parachute.")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
	OR IS_WEB_BROWSER_OPEN()
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player's phone is active")
		RETURN FALSE
	ENDIF
	
	bReturn = TRUE
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player in a vehicle")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BEAST(PLAYER_ID())
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player is Beast")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player is test driving a vehicle")
		RETURN FALSE
	ENDIF

	IF IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player is an animal")
		RETURN FALSE
	ENDIF
	
	RESET_NET_TIMER(sData.stCheckTimer)							// WHILE INSIDE THE INTERACT DISTANCE THERE'S NO NEED TO CHECK EVERY 5s THE CLOSEST

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG(FALSE)
			PRINT_HELP("SD_HT_NO_MC")
		ELSE
			IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
				PRINT_HELP("SD_HT_NO_ASS")
			ELSE
				PRINT_HELP("SD_HT_NO_BOD")
			ENDIF
		ENDIF
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player is member of a gang")
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
		PRINTLN("[fm_content_skydive] CAN_TRIGGER_SKYDIVING_CHALLENGE - Player ")
		PRINT_HELP("PM_INF_QMF1")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
		PRINT_HELP("SD_HT_INT")
		RETURN FALSE
	ENDIF
		
	RETURN TRUE

ENDFUNC

PROC TRIGGER_SKYDIVING_CHALLENGE(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData, INT iSkydivingLocation)

	SET_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DPAD_PRESSED)
	SKYDIVING_DEBUG_PRINT_PACKED_STATS()
	REQUEST_LAUNCH_GB_MISSION(FMMC_TYPE_SKYDIVING_CHALLENGE, 0, iSkydivingLocation)
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SD_HT_INT")
		CLEAR_HELP()
	ENDIF

ENDPROC

PROC MAINTAIN_DISABLE_MISSIONS_AT_COORDS(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)
	IF DOES_ENTITY_EXIST(sData.objParachuteCabin)
	AND IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(sData.objParachuteCabin, FALSE), 10.0)
		IF !g_matcBlockAndHideAllMissions
			PRINTLN("[fm_content_skydive] MAINTAIN_DISABLE_MISSIONS_AT_COORDS - Within 10m of nearest skydive start location, calling Block_All_MissionsAtCoords_Missions with TRUE")
			Block_All_MissionsAtCoords_Missions(TRUE)
			SET_BIT(sData.iBitset, ciSKYDIVING_CHALLENGE_BS_SET_MATC_DISABLED_AREA)
		ENDIF
	ELSE
		IF IS_BIT_SET(sData.iBitset, ciSKYDIVING_CHALLENGE_BS_SET_MATC_DISABLED_AREA)
			PRINTLN("[fm_content_skydive] MAINTAIN_DISABLE_MISSIONS_AT_COORDS - Not within 10m of nearest skydive start location, calling Block_All_MissionsAtCoords_Missions with FALSE")
			Block_All_MissionsAtCoords_Missions(FALSE)
			CLEAR_BIT(sData.iBitset, ciSKYDIVING_CHALLENGE_BS_SET_MATC_DISABLED_AREA)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL MAINTAIN_SKYDIVING_CHALLENGE_FLOW(SKYDIVING_CHALLENGE_FLOW_STRUCT& sData)

	INT iOrder
	BOOL bReturn = FALSE
	
	IF NOT SHOULD_SKYDIVING_PARACHUTE_CABIN_EXIST(sData.iClosestSkydiving, sData)
		PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - NOT SHOULD_SKYDIVING_PARACHUTE_CABIN_EXIST(sData.iClosestSkydiving, sData)")
		IF DOES_ENTITY_EXIST(sData.objParachuteCabin)
			DELETE_OBJECT(sData.objParachuteCabin)
		ENDIF
	ENDIF
	IF NOT SHOULD_PROCESS_SKYDIVING_CHALLENGE_FLOW()
		IF IS_BIT_SET(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DAILY_DATA_INIT)
			REPEAT ciSKYDIVING_CHALLENGE_MAX_LOCATIONS iOrder
				IF DOES_BLIP_EXIST(sData.sBlipData[iOrder].BlipId)
					REMOVE_BLIP(sData.sBlipData[iOrder].BlipId)
				ENDIF
			ENDREPEAT
			RESET_NET_TIMER(sData.stCheckTimer)
			CLEAR_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DAILY_DATA_INIT)
		ENDIF
		RETURN bReturn
	ENDIF
	
	IF sData.bFlashBips
		REPEAT ciSKYDIVING_CHALLENGE_MAX_LOCATIONS iOrder
			IF DOES_BLIP_EXIST(sData.sBlipData[iOrder].BlipId)
				SET_BLIP_FLASHES(sData.sBlipData[iOrder].BlipId,TRUE)
				SET_BLIP_FLASH_TIMER(sData.sBlipData[iOrder].BlipId,7000)
			ENDIF
		ENDREPEAT
		sData.bFlashBips = FALSE
	ENDIF
	
	IF sData.bShowExtraHelp
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet13, BI_FM_NMH13_JUNK_ENERGY_SKYDIVES_EXTRA_HELP_DONE)
			IF HAS_NET_TIMER_EXPIRED(sData.stHelpTextTimer, ciSKYDIVING_CHALLENGE_EXTRA_HELP_TEXT_TIMER)
				PRINT_HELP("SKYDIVE_BAG_EXTRA_HELP")
				SET_BIT(MPGlobalsAmbience.iFmNmhBitSet13,BI_FM_NMH13_JUNK_ENERGY_SKYDIVES_EXTRA_HELP_DONE)
				sData.bShowExtraHelp = FALSE
			ENDIF
		ELSE
			sData.bShowExtraHelp = FALSE
		ENDIF
	ENDIF

	INT iSkydivingLocation
	SKYDIVING_LOCATION_STRUCT sSkydivingData
//	IF IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet13, BI_FM_NMH13_JUNK_ENERGY_SKYDIVES_EMAIL_DONE)    // commented out as per Chris B.'s comment url:bugstar:7648209
	/// BLIPS ///
		IF NOT SHOULD_HIDE_AMBIENT_BLIP(ciPI_HIDE_MENU_ITEM_AMBIENT_ACTIVITIES_JUNK_ENERGY_SKYDIVES)
		AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		AND NOT SHOULD_HIDE_ALL_ACTIVITY_BLIPS()
		
			IF NOT IS_BIT_SET(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DAILY_DATA_INIT)
				// Clear all blips, so they are reset when the day passes
				REPEAT ciSKYDIVING_CHALLENGE_MAX_LOCATIONS iOrder
					IF DOES_BLIP_EXIST(sData.sBlipData[iOrder].BlipId)
						REMOVE_BLIP(sData.sBlipData[iOrder].BlipId)
						sData.sBlipData[iOrder].iBitset = 0
					ENDIF
				ENDREPEAT

				// Create all blips
				REPEAT ciSKYDIVING_CHALLENGE_MAX_LOCATIONS iOrder
					iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+iOrder))
					IF iSkydivingLocation < 0 OR iSkydivingLocation > 24
						RELOOP
					ENDIF
					sSkydivingData = GET_SKYDIVING_CHALLENGE_LOCATION_DATA(iSkydivingLocation)
					// Add blip

					IF NOT DOES_BLIP_EXIST(sData.sBlipData[iOrder].BlipId)
						sData.sBlipData[iOrder].BlipId = ADD_BLIP_FOR_COORD(sSkydivingData.vCoords)
						SET_BLIP_SPRITE(sData.sBlipData[iOrder].BlipId, RADAR_TRACE_JUNK_SKYDIVE)
						SET_BLIP_AS_SHORT_RANGE(sData.sBlipData[iOrder].BlipId, TRUE)
						SET_BLIP_NAME_FROM_TEXT_FILE(sData.sBlipData[iOrder].BlipId, "SD_BLIP")
						SET_BLIP_PRIORITY(sData.sBlipData[iOrder].BlipId,BLIPPRIORITY_LOW_LOWEST)
						IF GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_GOLD0)+iOrder*4)) != 255
							SHOW_GOLD_TICK_ON_BLIP(sData.sBlipData[iOrder].BlipId,TRUE)
							PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - Blip tick set to gold - ",iOrder," - Challenge Gold ")
						ELIF GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_ALLCHECKP0)+iOrder*4)) != 255
						OR GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_PARTIME0)+iOrder*4)) != 255
						OR GET_PACKED_STAT_INT(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_INT_JUNKENGYSDIVES_ACCURATLN0)+iOrder*4)) != 255
							SHOW_TICK_ON_BLIP(sData.sBlipData[iOrder].BlipId,TRUE)
							PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - Blip tick set to green - ",iOrder," - Challenge Complete ")
						ENDIF
						PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - Added blip ",iOrder," at ",sSkydivingData.vCoords)
						sData.bBlipJustCreated = TRUE
					ELSE
						IF IS_BIT_SET(sData.sBlipData[iOrder].iBitset, ciSKYDIVING_CHALLENGE_BS_FLASH_BLIP)
							PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - Flashing blip")
							SET_BLIP_FLASHES(sData.sBlipData[iOrder].BlipId, TRUE)
							SET_BLIP_FLASH_INTERVAL(sData.sBlipData[iOrder].BlipId, BLIP_FLASHING_TIME)
							SET_BLIP_FLASH_TIMER(sData.sBlipData[iOrder].BlipId, FLASH_CONTRABAND_BLIP_DEFAULT_TIME)
							CLEAR_BIT(sData.sBlipData[iOrder].iBitset, ciSKYDIVING_CHALLENGE_BS_FLASH_BLIP)
						ENDIF
						IF IS_BIT_SET(sData.sBlipData[iOrder].iBitset, ciSKYDIVING_CHALLENGE_BS_MAKE_BLIP_LONG_RANGE)
							IF IS_BLIP_SHORT_RANGE(sData.sBlipData[iOrder].BlipId)
								SET_BLIP_AS_SHORT_RANGE(sData.sBlipData[iOrder].BlipId, FALSE)
								PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - Made blip long range")
								CLEAR_BIT(sData.sBlipData[iOrder].iBitset, ciSKYDIVING_CHALLENGE_BS_MAKE_BLIP_LONG_RANGE)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				SET_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DAILY_DATA_INIT)
			ENDIF
		ELSE
			REPEAT ciSKYDIVING_CHALLENGE_MAX_LOCATIONS iOrder
				IF DOES_BLIP_EXIST(sData.sBlipData[iOrder].BlipId)
					REMOVE_BLIP(sData.sBlipData[iOrder].BlipId)
					sData.sBlipData[iOrder].iBitset = 0
				ENDIF
			ENDREPEAT
			CLEAR_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DAILY_DATA_INIT)
		ENDIF
		
		
	/// PROPS ///
		IF HAS_NET_TIMER_EXPIRED(sData.stCheckTimer, 5000)
		OR sData.iClosestSkydiving = -1
		OR sData.bBlipJustCreated
			sData.bBlipJustCreated = FALSE

			CLEAR_BIT(sData.iBitset,ciSKYDIVING_CHALLENGE_BS_DPAD_PRESSED)	// DPAD PRESSED WILL BE RESET AFTER 5s IF SOMETHING GOES WRONG

			sData.iClosestSkydiving = GET_CLOSEST_SKYDIVING_CHALLENGE_LOCATION()
			PRINTLN("[fm_content_skydive] MAINTAIN_SKYDIVING_CHALLENGE_FLOW - sData.iClosestSkydiving ",sData.iClosestSkydiving)
			IF NOT DOES_SKYDIVING_CHALLENGE_PARACHUTE_CABIN_EXIST(sData.iClosestSkydiving,sData)
				IF CREATE_SKYDIVING_PARACHUTE_CABIN(sData.iClosestSkydiving, sData)
					CREATE_SKYDIVING_SCENARIO_BLOCKER(sData.iClosestSkydiving, sData)
					CLEAR_BIT(sData.iBitset, ciSKYDIVING_CHALLENGE_VEHICLE_CREATED)
				ENDIF
			ENDIF
			IF NOT DOES_SKYDIVING_CHALLENGE_SIGN_EXIST(sData.iClosestSkydiving,sData)
				CREATE_SKYDIVING_SIGN(sData.iClosestSkydiving, sData)
			ENDIF
			CREATE_SKYDIVING_CABIN_SIDE_VEHICLE(sData.iClosestSkydiving, sData)

			
			RESET_NET_TIMER(sData.stCheckTimer)
		ENDIF
		
		MAINTAIN_DISABLE_MISSIONS_AT_COORDS(sData)
		
	/// LAUNCH ///
		iSkydivingLocation = GET_MP_INT_CHARACTER_STAT(INT_TO_ENUM(MP_INT_STATS, ENUM_TO_INT(MP_STAT_DAILYCOLLECT_Skydives0)+sData.iClosestSkydiving))
		IF CAN_TRIGGER_SKYDIVING_CHALLENGE(sData, iSkydivingLocation,bReturn)
			TRIGGER_SKYDIVING_CHALLENGE(sData, iSkydivingLocation)
		ENDIF

//	ENDIF
	
	RETURN bReturn
ENDFUNC
#ENDIF // #IF FEATURE_DLC_1_2022
