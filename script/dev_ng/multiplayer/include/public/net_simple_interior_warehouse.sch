//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_warehouse.sch															//
// Description: This is implementation of warehouse as simple interior. As such this does not expose any	//
//				public functions. All functions to manipulate simple interiors are in net_simple_interior	//
// Written by:  Tymon																						//
// Date:  		2/2/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_gang_boss.sch"
USING "MP_globals_simple_interior.sch"
USING "net_simple_interior_base.sch"
USING "net_realty_warehouse.sch"
USING "net_simple_interior_common.sch"
USING "net_simple_interior_cs_header_include.sch"

CONST_INT SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_BOSS 	0 // We entered warehouse as boss (also owner)
CONST_INT SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_GOON 	1 // We entered warehouse as goon
CONST_INT SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_OWNER 2 // We entered warehouse as owner (but not boss)

CONST_INT BS_SIMPLE_INTERIOR_WAREHOUSE_WANTED_MODIFIER_SET 			0
CONST_INT BS_SIMPLE_INTERIOR_WAREHOUSE_WANTED_LEVEL_CLEARED 		1
CONST_INT BS_SIMPLE_INTERIOR_WAREHOUSE_DOOR_AROUND_OPENED			2
CONST_INT BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED	3

//Blip data
CONST_INT MAX_WAREHOUSE_BLIPS	1

FUNC BOOL DOES_WAREHOUSE_USE_EXTERIOR_SCRIPT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_WAREHOUSE(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID)
	UNUSED_PARAMETER(iEntranceID)
	
	IF g_sMPTunables.bexec_disable_warehouse_entry  
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(playerID) // Only when player is in executive gang they can enter warehouses
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
		RETURN DOES_PLAYER_OWN_WAREHOUSE(GB_GET_THIS_PLAYER_GANG_BOSS(playerID), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	ELSE
		IF DOES_PLAYER_OWN_WAREHOUSE(playerID, GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			RETURN TRUE
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_WAREHOUSE_IN_VEHICLE(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eModel)
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_WAREHOUSE_ENTRY_IN_CURRENT_VEHICLE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
	UNUSED_PARAMETER(bReturnSetExitCoords)
	UNUSED_PARAMETER(data)
	UNUSED_PARAMETER(details)
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_WAREHOUSE_REASON_FOR_BLOCKED_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)
	UNUSED_PARAMETER(iEntranceUsed)
	UNUSED_PARAMETER(txtExtraString)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO_MISSION()
		RETURN "SCW_CLOSED"
	ENDIF
	#ENDIF
	
	IF SHOULD_BLOCK_SIMPLE_INTERIOR_ENTRY_FOR_ON_CALL()
		RETURN "SI_ENTR_BLCK16A"
	ENDIF
	
	IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
				RETURN "WHOUSE_MC_BLOCK_P" // Your Warehouse is unavailable when being an MC President.
			ELSE
				txtExtraString = GB_GET_PLAYER_BIKER_ROLE_TEXT_LABEL(PLAYER_ID())
				RETURN "WHOUSE_MC_BLOCK_M" // Your Warehouse is unavailable when being [ROLE].
			ENDIF
		ELSE
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
				PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
				
				IF NOT DOES_PLAYER_OWN_WAREHOUSE(playerBoss, GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
						RETURN "WHOUSE_GOONHLP1C" // Your Warehouse is unavailable when working as a Bodyguard for a CEO.
					ELSE
						RETURN "WHOUSE_GOON_HLP1" // Your Warehouse is unavailable when working as a Bodyguard for a VIP.
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF (DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	OR DOES_PLAYER_OWN_WAREHOUSE(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)))
		IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
			RETURN "JUG_BLOCK_WARE"
		ENDIF
		IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
			// You cannot enter whilst delivering a bounty.
			RETURN "BOUNTY_PROP_BLCK"
		ENDIF
	ENDIF
	
	IF IS_NPC_IN_VEHICLE()
		// You cannot enter whilst an NPC is in the vehicle.
		RETURN "NPC_BLOCK"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_WAREHOUSE_REASON_FOR_BLOCKED_EXIT(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iExitUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iExitUsed)
	
	IF SHOULD_JOB_ENTRY_TYPE_BLOCK_SIMPLE_INTERIOR_TRANSITION()
		RETURN "SI_EXIT_BLCK16A"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL CAN_PLAYER_INVITE_OTHERS_TO_WAREHOUSE(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(playerID)
	RETURN FALSE // Warehouses don't use invitations
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_INVITED_TO_WAREHOUSE(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(playerID)
	RETURN FALSE // Warehouses don't use invitations
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_BE_INVITED_TO_WAREHOUSE_BY_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(pInvitingPlayer)
	RETURN FALSE
ENDFUNC

PROC GET_WAREHOUSE_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtTitle)
	UNUSED_PARAMETER(strOptions)
	UNUSED_PARAMETER(eventOptions)
	UNUSED_PARAMETER(iOptionsCount)
ENDPROC

PROC PROCESS_WAREHOUSE_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(menu)
ENDPROC

FUNC PLAYER_INDEX GET_WAREHOUSE_OWNER(INT iWarehouse)
	// VIPs warehouse
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
        IF IS_NET_PLAYER_OK(playerBoss, FALSE)
	    	IF DOES_PLAYER_OWN_WAREHOUSE(playerBoss, iWarehouse)
	        	RETURN playerBoss
	    	ENDIF
     	ENDIF
	ELSE
		// Owned warehouse
		IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
			RETURN PLAYER_ID()
		ENDIF
    ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC BOOL IS_WAREHOUSE_OWNER_ID_VALID(INT iOwnerID)
	IF iOwnerID != -1
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_WAREHOUSE_INSIDE_BOUNDING_BOX(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bbox  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH GET_WAREHOUSE_SIZE(GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		CASE eWarehouseSmall
			bbox.vInsideBBoxMin = <<1086.8363, -3103.8796, -41.0000>>
			bbox.vInsideBBoxMax = <<1105.8070, -3094.8030, -36.0099>>
		BREAK
		CASE eWarehouseMedium
			bbox.vInsideBBoxMin = <<1047.3560, -3112.1750, -41.0000>>
			bbox.vInsideBBoxMax = <<1074.3080, -3092.8257, -33.7074>>
		BREAK
		CASE eWarehouseLarge
			bbox.vInsideBBoxMin = <<990.5693, -3113.3821, -41.0000>>
			bbox.vInsideBBoxMax = <<1030.2635, -3088.4788, -28.7216>>
		BREAK
	ENDSWITCH
ENDPROC

PROC INITIALISE_GENERIC_SMALL_WAREHOUSE(SIMPLE_INTERIOR_DETAILS &details)
	details.exitData.vLocateCoords1[0] = <<-7.911, 4.559, 0.0>>
	details.exitData.vLocateCoords2[0] = <<-7.911, 6.989, 2.200>>
	details.exitData.fLocateWidths[0] = 3.860
	details.exitData.fHeadings[0] = 90.000
ENDPROC

PROC INITIALISE_GENERIC_MEDIUM_WAREHOUSE(SIMPLE_INTERIOR_DETAILS &details)
	details.exitData.vLocateCoords1[0] = <<-8.780, 6.917, 0.000>>
	details.exitData.vLocateCoords2[0] = <<-8.780, 10.000, 2.200>>
	details.exitData.fLocateWidths[0] = 3.860
	details.exitData.fHeadings[0] = 90.000
ENDPROC

PROC INITIALISE_GENERIC_LARGE_WAREHOUSE(SIMPLE_INTERIOR_DETAILS &details)
	details.exitData.vLocateCoords1[0] = <<-15.091, 2.575, 0.000>>
	details.exitData.vLocateCoords2[0] = <<-15.091, 5.875, 2.200>>
	details.exitData.fLocateWidths[0] = 3.860
	details.exitData.fHeadings[0] = 90.000
ENDPROC

PROC GET_WAREHOUSE_INTERIOR_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH GET_WAREHOUSE_SIZE(GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		CASE eWarehouseSmall
			vPosition = <<1094.9884, -3104.7756, -40.0000>>
			fHeading = 0.0
			txtType = "ex_int_warehouse_s_dlc"
		BREAK
		CASE eWarehouseMedium
			vPosition = <<1056.4877, -3105.7239, -40.0000>>
			fHeading = 0.0
			txtType = "ex_int_warehouse_m_dlc"
		BREAK
		CASE eWarehouseLarge
			vPosition = <<1007.1347, -3102.0793, -40.0000>>
			fHeading = 0.0
			txtType = "ex_int_warehouse_l_dlc"
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_WAREHOUSE_LOAD_SECONDARY_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
		SET_INTERIOR_FLOOR_INDEX(GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER())
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Spawn points for USE_CUSTOM_SPAWN_POINTS generated with 
///    bGenerateRandomCustomSpawnPoints_Grid from Spawning / Warping widget in freemode
/// PARAMS:
///    eSimpleInteriorID - 
///    iSpawnPoint - 
///    vSpawnPoint - 
///    fSpawnHeading - 
FUNC BOOL GET_WAREHOUSE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle  , BOOL bUseSecondaryInteriorDetails = FALSE )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSpawnInVehicle)
	
	SWITCH GET_WAREHOUSE_SIZE(GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		//════════════════════════════════════════════════════════════════════
		CASE eWarehouseSmall
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-6.3318, 5.2695, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-6.3390, 4.1196, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-6.3246, 6.4194, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-5.1818, 5.2622, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-5.1890, 4.1123, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-5.1746, 6.4121, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-4.0317, 5.2549, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-4.0389, 4.1050, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-4.0245, 6.4048, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-2.8817, 5.2476, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-2.8889, 4.0977, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-2.8745, 6.3975, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-1.7317, 5.2402, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-1.7389, 4.0903, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-1.7245, 6.3901, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-0.5817, 5.2329, 0.0001>>
				    fSpawnHeading = 269.6400
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE eWarehouseMedium
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-7.2566, 8.0942, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-6.8567, 6.5940, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-6.5566, 5.0940, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-6.9567, 0.8938, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-6.9567, -0.6062, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-5.7566, 8.0942, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-5.7566, 6.5942, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-4.8567, 5.2939, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-4.5566, 0.6238, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-4.5566, -1.8062, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-4.2566, 8.0942, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-0.6566, 6.5942, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-2.7567, 5.3938, 0.0000>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-2.7567, 0.6238, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-2.4567, -1.8062, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-2.7566, 8.0942, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE eWarehouseLarge
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-13.3667, 4.2373, 0.0001>>
				    fSpawnHeading = 180.0000
					RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-13.6667, 2.2373, 0.0001>>
				    fSpawnHeading = 180.0000
					RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-13.1667, -0.2207, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-10.7667, -0.2207, 0.0000>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-9.3663, -0.1626, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-8.6663, 6.2373, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-8.3663, 4.6375, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-9.2663, 1.5374, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-10.1663, -2.4626, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-8.3663, -2.2629, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-6.7667, 2.9373, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-6.7667, 1.6375, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-8.5663, -4.1626, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-9.4663, -1.2629, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-7.0663, -3.1626, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-9.9667, -4.1626, 0.0001>>
				    fSpawnHeading = 270.0000
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS GET_WAREHOUSE_CUSTOM_INTERIOR_WARP_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS eParams
	RETURN eParams
ENDFUNC

/// PURPOSE:
///    Spawn points for USE_CUSTOM_SPAWN_POINTS on the outside of warehouse, generated with 
///    bGenerateRandomCustomSpawnPoints_Grid from Spawning / Warping widget in freemode
/// PARAMS:
///    eSimpleInteriorID - 
///    iSpawnPoint - 
///    vSpawnPoint - 
///    fSpawnHeading - 
/// RETURNS:
///    
FUNC BOOL GET_WAREHOUSE_OUTSIDE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	UNUSED_PARAMETER(bSpawnInVehicle)

	SWITCH eSimpleInteriorID
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_1
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<53.5655, -2569.2639, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<53.3032, -2566.7639, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<53.2278, -2570.4629, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<53.3409, -2564.7642, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<51.2658, -2568.7261, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<51.3035, -2566.7263, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<51.8281, -2569.9260, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<51.3412, -2564.7266, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<49.2662, -2568.6885, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<49.3039, -2566.6887, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<54.6285, -2565.2881, 5.2087>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<49.3416, -2564.6890, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<47.2666, -2568.6509, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<47.3042, -2566.6511, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<52.3289, -2563.1509, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<47.3419, -2564.6514, 5.0046>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_2
			SWITCH iSpawnPoint
				CASE 0
			    vSpawnPoint = <<-1082.3696, -1261.3761, 4.6112>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 1
			    vSpawnPoint = <<-1080.8263, -1263.9487, 4.6936>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 2
			    vSpawnPoint = <<-1083.9130, -1258.8035, 4.4974>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 3
			    vSpawnPoint = <<-1079.2830, -1266.5214, 4.7761>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 4
			    vSpawnPoint = <<-1085.4563, -1256.2308, 4.4452>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 5
			    vSpawnPoint = <<-1079.7970, -1259.8328, 4.6507>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 6
			    vSpawnPoint = <<-1078.2537, -1262.4054, 4.7587>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 7
			    vSpawnPoint = <<-1081.3403, -1257.2601, 4.5154>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 8
			    vSpawnPoint = <<-1076.7103, -1264.9780, 4.8411>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 9
			    vSpawnPoint = <<-1082.8837, -1254.6875, 4.4188>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 10
			    vSpawnPoint = <<-1077.2244, -1258.2894, 4.6669>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 11
			    vSpawnPoint = <<-1075.6810, -1260.8621, 4.7941>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 12
			    vSpawnPoint = <<-1078.7677, -1255.7168, 4.5341>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 13
			    vSpawnPoint = <<-1074.1377, -1263.4347, 4.8505>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 14
			    vSpawnPoint = <<-1080.3110, -1253.1442, 4.4313>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			CASE 15
			    vSpawnPoint = <<-1074.6517, -1256.7461, 4.6728>>
			    fSpawnHeading = 300.9600
			    RETURN TRUE
			BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_3
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<896.2726, -1035.3372, 34.1073>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<898.7726, -1035.3372, 34.1073>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<901.2726, -1035.3372, 34.1073>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<903.7726, -1035.3372, 34.1073>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<896.2726, -1032.8372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<898.7726, -1032.8372, 33.9662>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<901.2726, -1032.8372, 33.9661>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<903.7726, -1032.8372, 33.9661>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<896.2726, -1030.3372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<898.7726, -1030.3372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<901.2726, -1030.3372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<903.7726, -1030.3372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<896.2726, -1027.8372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<898.7726, -1027.8372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<901.2726, -1027.8372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<903.7726, -1027.8372, 33.9666>>
				    fSpawnHeading = 360.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_4
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<247.8323, -1956.5043, 22.1788>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<249.5188, -1958.0681, 22.1585>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<246.1458, -1954.9404, 22.2111>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<251.2053, -1959.6320, 22.1202>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<244.4594, -1953.3766, 22.2510>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<249.3962, -1954.8177, 22.1503>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<251.0827, -1956.3816, 22.1042>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<247.7097, -1953.2539, 22.1978>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<252.7692, -1957.9454, 22.0476>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<246.0232, -1951.6901, 22.2508>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<250.9601, -1953.1312, 22.0426>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<252.6466, -1954.6951, 21.9945>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<249.2736, -1951.5674, 22.0914>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<254.3331, -1956.2589, 21.9397>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<247.5871, -1950.0035, 22.1449>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<243.8240, -1951.4447, 22.2600>>
				    fSpawnHeading = 317.1600
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_5
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-424.8701, 185.5529, 79.7918>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-421.9708, 184.8951, 79.7919>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-420.2715, 184.8373, 79.7612>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-417.9722, 185.4795, 79.7288>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-415.6729, 185.4217, 79.6811>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-413.3736, 185.3639, 79.6331>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-424.9279, 183.2536, 79.6270>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-422.6286, 183.1958, 79.5531>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-420.3293, 183.1380, 79.4768>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-418.0300, 183.4802, 79.4155>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-415.7307, 183.3224, 79.3917>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-413.4314, 183.2646, 79.3209>>
				    fSpawnHeading = 178.5600
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-424.3857, 181.2543, 79.6170>>
				    fSpawnHeading = 265.5600
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-426.886, 185.0965, 79.5167>>
				    fSpawnHeading = 88.5600
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-417.0871, 181.4387, 79.4749>>
				    fSpawnHeading = 270.5600
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-414.4878, 181.3809, 79.4060>>
				    fSpawnHeading = 271.5600
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_6
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-1042.9740, -2022.8810, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-1044.9407, -2024.7659, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-1046.6074, -2026.3508, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-1048.2742, -2027.9358, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-1044.8589, -2021.5142, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-1046.5256, -2023.0991, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-1048.1924, -2024.6841, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-1049.8591, -2026.2690, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-1046.4438, -2019.8474, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-1048.1106, -2021.4324, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-1049.7773, -2023.0173, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-1051.4441, -2024.6023, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-1048.0288, -2018.1807, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-1049.6956, -2019.7656, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-1051.3623, -2021.3506, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-1053.0291, -2022.9355, 12.1616>>
				    fSpawnHeading = 43.5600
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_7
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-1268.4657, -812.4496, 16.1077>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-1269.9979, -810.4742, 16.1168>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-1266.9335, -814.4250, 16.0992>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-1271.5302, -808.4988, 16.1253>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-1265.4012, -816.4004, 16.0992>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-1270.4410, -813.9819, 16.1078>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-1271.9733, -812.0065, 16.1169>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-1268.9088, -815.9573, 16.0992>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-1273.5055, -810.0311, 16.1253>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-1267.3766, -817.9327, 16.0992>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-1272.4164, -815.5142, 16.1078>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-1273.9486, -813.5388, 16.1169>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-1270.8842, -817.4896, 16.0992>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-1275.4808, -811.5634, 16.1253>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-1269.3519, -819.4650, 16.0992>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-1274.3917, -817.0464, 16.1222>>
				    fSpawnHeading = 127.8000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_8
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-874.0634, -2734.7161, 12.9095>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-872.7773, -2732.5723, 12.9109>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-875.3495, -2736.8599, 12.9053>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-871.4913, -2730.4285, 12.9083>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-876.6356, -2739.0037, 12.9029>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-876.2072, -2733.4299, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-874.9211, -2731.2861, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-877.4933, -2735.5737, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-873.6351, -2729.1423, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-878.7794, -2737.7175, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-878.3510, -2732.1438, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-877.0649, -2730.0000, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-879.6371, -2734.2876, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-875.7789, -2727.8562, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-880.9232, -2736.4314, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-880.4948, -2730.8577, 12.8285>>
				    fSpawnHeading = 59.0400
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_9
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<274.3696, -3015.4106, 4.6999>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<274.4167, -3012.9111, 4.6995>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<274.3225, -3017.9102, 4.7001>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<274.4639, -3010.4116, 4.6994>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<274.2754, -3020.4097, 4.7003>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<271.8701, -3015.3635, 4.7094>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<271.9172, -3012.8640, 4.7092>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<271.8230, -3017.8630, 4.7096>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<271.9643, -3010.3645, 4.7090>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<271.7758, -3020.3625, 4.7097>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<269.3705, -3015.3164, 4.7188>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<269.4177, -3012.8169, 4.7196>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<269.3234, -3017.8159, 4.7189>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<269.4648, -3010.3174, 4.7188>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<269.2763, -3020.3154, 4.7191>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<266.8710, -3015.2693, 4.7284>>
				    fSpawnHeading = 88.9200
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_10
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<1569.8593, -2130.3018, 77.3351>>
				    fSpawnHeading = 105.8400
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<1568.4163, -2130.7112, 77.3351>>
				    fSpawnHeading = 105.8400
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<1566.9733, -2131.1206, 76.8952>>
				    fSpawnHeading = 105.8400
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<1566.0922, -2133.6050, 76.6096>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<1563.5922, -2133.6050, 76.5872>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<1568.5922, -2133.6050, 76.6408>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<1566.0922, -2136.1050, 76.5637>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<1563.5922, -2136.1050, 76.5312>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<1568.5922, -2136.1050, 76.6043>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<1566.0922, -2138.6050, 76.6349>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<1563.5922, -2138.6050, 76.6020>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<1568.5922, -2138.6050, 76.6595>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<1566.0922, -2141.1050, 76.6486>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<1563.5922, -2141.1050, 76.6264>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<1568.5922, -2141.1050, 76.6633>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<1566.0922, -2143.6050, 76.6380>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_11
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-315.0531, -2698.2361, 6.5502>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-314.0059, -2697.1621, 6.3438>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-312.9586, -2696.0881, 5.9510>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-311.9114, -2695.0142, 5.5625>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-310.8642, -2693.9402, 5.1789>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-309.8169, -2692.8662, 5.1532>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-308.7697, -2691.7922, 5.0003>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-307.7224, -2690.7183, 5.0003>>
				    fSpawnHeading = 315.7200
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-314.0204, -2699.4949, 6.5486>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-312.9597, -2700.5554, 6.5465>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-311.8990, -2701.6160, 6.0171>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-310.8384, -2702.6765, 5.3512>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-309.7777, -2703.7371, 5.1532>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-308.7170, -2704.7976, 5.1532>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-307.6564, -2705.8582, 5.1532>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-306.5957, -2706.9187, 5.1532>>
				    fSpawnHeading = 225.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_12
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<500.5130, -652.0776, 23.9102>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<500.1997, -654.5579, 23.9046>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<500.8264, -649.5973, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<499.8864, -657.0381, 23.8990>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<501.1397, -647.1170, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<502.9933, -652.3909, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<502.6800, -654.8712, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<503.3066, -649.9106, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<502.3667, -657.3515, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<503.6200, -647.4304, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<505.4736, -652.7043, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<505.1603, -655.1846, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<505.7869, -650.2240, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<504.8470, -657.6649, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<506.1003, -647.7437, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<507.9539, -653.0176, 23.7512>>
				    fSpawnHeading = 262.8000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_13
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-527.8182, -1783.6528, 20.5241>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-530.0165, -1782.4623, 20.5136>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-532.2148, -1781.2717, 20.4936>>
				    fSpawnHeading = 322.5600
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-526.6276, -1781.4546, 20.3953>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-528.8259, -1780.2640, 20.3978>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-531.0242, -1779.0735, 20.4048>>
				    fSpawnHeading = 325.5600
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-525.4370, -1779.5560, 20.3393>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-527.6353, -1778.3660, 20.3620>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-531.9336, -1776.8750, 20.3854>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-525.7464, -1777.6580, 20.3332>>
				    fSpawnHeading = 325.5600
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-527.9447, -1776.4680, 20.3559>>
				    fSpawnHeading = 325.5600
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-529.5430, -1777.0770, 20.3792>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-522.1558, -1779.3600, 20.1963>>
				    fSpawnHeading = 322.5600
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-520.1541, -1780.8690, 20.2250>>
				    fSpawnHeading = 316.5600
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-523.2524, -1780.8790, 20.2515>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-521.8652, -1782.5620, 20.3421>>
				    fSpawnHeading = 331.5600
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_14
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-294.8197, -1353.2212, 30.3182>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-294.8982, -1355.7200, 30.3161>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-294.7412, -1350.7224, 30.3066>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-293.1767, -1357.0190, 30.3098>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-294.6627, -1348.2236, 30.3058>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-295.0553, -1360.7175, 30.3097>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-294.5841, -1345.7249, 30.3098>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-295.1338, -1363.2163, 30.3097>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-292.9209, -1351.8000, 30.3170>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-292.9994, -1354.5980, 30.3113>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-293.1424, -1349.6010, 30.3154>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-292.7780, -1358.8970, 30.3111>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-293.0639, -1347.1021, 30.3070>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-293.1565, -1360.7960, 30.3111>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-292.9854, -1344.3030, 30.3044>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-293.2350, -1363.2948, 30.3111>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_15
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<349.8802, 328.4255, 103.3104>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<352.3017, 327.8038, 103.1931>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<354.7231, 327.1820, 103.0763>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<357.1446, 326.5603, 102.9681>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<359.5660, 325.9386, 102.8674>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<361.9875, 325.3168, 102.7710>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<364.4089, 324.6951, 102.7018>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<366.8304, 324.0734, 102.6326>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<348.0585, 327.5041, 103.3091>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<350.7800, 326.2823, 103.1918>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<353.5014, 325.6606, 103.0750>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<356.2229, 324.7389, 102.9793>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<358.6443, 324.1171, 102.8786>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<361.0658, 323.4954, 102.7693>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<363.1872, 322.8737, 102.7001>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<365.9086, 322.2519, 102.6309>>
				    fSpawnHeading = 165.6000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_16
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<925.6405, -1560.2426, 29.7406>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<925.6606, -1557.0426, 29.8954>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<925.6204, -1563.4425, 29.8634>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<925.6807, -1553.8427, 29.8012>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<925.6003, -1566.6425, 29.6487>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<922.4406, -1560.2224, 29.7553>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<922.4606, -1557.0225, 29.7750>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<922.4205, -1563.4224, 29.7366>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<922.4807, -1553.8225, 29.7882>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<922.4004, -1566.6223, 29.7040>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<919.2406, -1560.2023, 29.7592>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<919.2607, -1557.0023, 29.7694>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<919.2205, -1563.4022, 29.7600>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<919.2808, -1553.8024, 29.7790>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<919.2004, -1566.6022, 29.7440>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<916.0406, -1560.1821, 29.7456>>
				    fSpawnHeading = 89.6400
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_17
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<760.1638, -909.4731, 24.2546>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<760.1839, -906.2732, 24.2490>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<760.2040, -903.0732, 24.2297>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<760.2241, -899.8733, 24.2069>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<760.2441, -896.6733, 24.1849>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<760.2642, -893.4734, 24.1641>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<760.2843, -890.2734, 24.1432>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<760.6044, -911.6735, 24.1212>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<762.1638, -909.4932, 24.2505>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<762.1839, -906.2933, 24.2306>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<762.2039, -903.0933, 24.1944>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<762.2240, -899.8934, 24.1655>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<762.2441, -896.6934, 24.1417>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<762.2642, -893.4935, 24.1227>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<762.2843, -890.2935, 24.1055>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<762.3043, -911.9936, 24.0937>>
				    fSpawnHeading = 269.6400
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_18
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<1038.5519, -2173.2070, 30.5259>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<1038.3108, -2176.3979, 30.5213>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<1038.7930, -2170.0161, 30.5306>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<1038.0697, -2179.5889, 30.5189>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<1039.0341, -2166.8252, 30.5330>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<1037.8286, -2182.7798, 30.5083>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<1041.7428, -2173.4480, 30.4763>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<1041.5017, -2176.6389, 30.4677>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<1041.9839, -2170.2571, 30.5075>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<1041.2606, -2179.8298, 30.4313>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<1042.2250, -2167.0662, 30.5287>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<1041.0195, -2183.0208, 30.4238>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<1044.9337, -2173.6890, 30.4530>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<1044.6926, -2176.8799, 30.4220>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<1045.1748, -2170.4980, 30.4845>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<1044.4515, -2180.0708, 30.3856>>
				    fSpawnHeading = 265.6800
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_19
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<1018.3384, -2511.6499, 27.4777>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<1017.9365, -2516.6685, 27.3020>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<1018.1375, -2514.3750, 27.3020>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<1017.7356, -2519.8621, 27.3020>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK

				CASE 4
				    vSpawnPoint = <<1014.5419, -2519.6611, 27.3058>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<1014.7429, -2516.4675, 27.3049>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<1014.9438, -2513.2739, 27.3037>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<1015.1447, -2510.0803, 27.3024>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<1011.3483, -2519.4602, 27.3077>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<1011.5492, -2516.2666, 27.3062>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<1011.7501, -2513.0730, 27.3048>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<1011.9510, -2509.8794, 27.3034>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<1008.1546, -2519.2593, 27.3043>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<1008.3555, -2516.0657, 27.3020>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<1008.5565, -2512.8721, 27.3020>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<1008.7574, -2509.6785, 27.3020>>
				    fSpawnHeading = 86.4000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_20
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-245.4970, 202.4499, 82.7567>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-247.9962, 202.3870, 83.0061>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-250.4954, 202.3242, 83.2191>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-252.9946, 202.2614, 83.5141>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-245.4342, 199.9507, 82.2888>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-247.9334, 199.8878, 82.6439>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-250.4326, 199.8250, 82.9599>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-252.9318, 199.7622, 83.3373>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-245.3714, 197.4514, 81.7634>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-247.8706, 197.3886, 82.2527>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-250.3698, 197.3258, 82.6763>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-252.8690, 197.2630, 83.1381>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-245.3085, 194.9522, 81.3477>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<-247.8077, 194.8894, 81.9302>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<-250.3069, 194.8266, 82.4314>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<-252.8062, 194.7638, 82.9539>>
				    fSpawnHeading = 181.4400
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_21
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<540.8308, -1944.6714, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<542.1703, -1946.7822, 23.9845>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<539.4913, -1942.5605, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<543.5099, -1948.8931, 23.9824>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<538.1517, -1940.4497, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<544.8494, -1951.0039, 23.9803>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<542.9417, -1943.3318, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<544.2812, -1945.4426, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<541.6021, -1941.2209, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<545.6207, -1947.5535, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<540.2626, -1939.1101, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<546.9603, -1949.6643, 23.9838>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<545.0525, -1941.9922, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<546.3920, -1944.1030, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<543.7130, -1939.8813, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<547.7316, -1946.2139, 23.9851>>
				    fSpawnHeading = 302.4000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_22
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<95.6630, -2216.3105, 5.1712>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<95.6473, -2213.8105, 5.1787>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<95.6787, -2218.8105, 5.1712>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<95.6316, -2211.3105, 5.0431>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<95.6945, -2221.3105, 5.1712>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<95.6159, -2208.8105, 5.0431>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<93.1631, -2216.3262, 5.0333>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<93.1474, -2213.8262, 5.0368>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<93.1788, -2218.8262, 5.0333>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<93.1317, -2211.3262, 5.0431>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<93.1945, -2221.3262, 5.0333>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<93.1160, -2208.8262, 5.0431>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<90.6631, -2216.3418, 5.0373>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 13
				    vSpawnPoint = <<90.6474, -2213.8418, 5.0409>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 14
				    vSpawnPoint = <<90.6788, -2218.8418, 5.0371>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
				CASE 15
				    vSpawnPoint = <<90.6317, -2211.3418, 5.0468>>
				    fSpawnHeading = 90.3600
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC INITIALISE_WAREHOUSE_SOUND_NAMES_AND_SETS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details)
	INT iWarehouseID = GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	
	IF iWarehouseID = ciW_LSIA_2 OR iWarehouseID = ciW_Rancho_2
		// These are warehouses with garage type door
		details.entryAnim.strOnEnterFadeOutFinishSoundSet = "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS"
		details.entryAnim.strOnEnterFadeOutFinishSoundName = "Door_Open"
		details.entryAnim.strOnLeaveFadeInStartSoundSet = "GTAO_EXEC_WH_GARAGE_DOOR_SOUNDS"
		details.entryAnim.strOnLeaveFadeInStartSoundName = "Door_Close"
	ELSE
		// These are normal door warehouses
		//details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
		//details.entryAnim.strOnEnterPhaseSoundName = "Push"
		details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
		details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
		details.entryAnim.fOnEnterSoundPhases[0] = 0.271
		details.entryAnim.fOnEnterSoundPhases[1] = 0.411
		details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
		
		details.entryAnim.strOnLeaveFadeInStartSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
		details.entryAnim.strOnLeaveFadeInStartSoundName = "Closed"
	ENDIF
ENDPROC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_WAREHOUSE_ASYNC_SPAWN_GRID_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_WAREHOUSE_ASYNC_SPAWN_GRID_INSIDE_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC VECTOR GET_WAREHOUSE_TELEPORT_IN_SVM_COORD(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC _SET_CAM_COORDS_SAME_AS_GARAGE_EXIT(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	VECTOR vGarageSyncedScenePos = ROTATE_VECTOR_ABOUT_Z(<< 231.848, -1006.707, -99.992 >>, 63.360)
	VECTOR vGarageSyncSceneRot = << 0.000, 0.000, -63.360 >>
	VECTOR vGarageCamPos = ROTATE_VECTOR_ABOUT_Z(<<233.2975, -1005.8097, -98.5457>>, 63.360)
	VECTOR vGarageCamRot = <<-26.8902, -0.0380, 129.5137>> //- <<0.0, 0.0, -63.360>>
	FLOAT fGarageCameraFOV = 30.3982
	
	VECTOR vRelativeCamPos = vGarageCamPos - vGarageSyncedScenePos 
	FLOAT fRelativeRotZ = vGarageCamRot.Z - vGarageSyncSceneRot.Z
	
	entryAnim.cameraPos = ROTATE_VECTOR_ABOUT_Z(ROTATE_VECTOR_ABOUT_Z(entryAnim.syncScenePos, -1.0 * entryAnim.syncSceneRot.Z) + vRelativeCamPos, entryAnim.syncSceneRot.Z)
	entryAnim.cameraRot = vGarageCamRot
	entryAnim.cameraRot.Z = entryAnim.syncSceneRot.Z + fRelativeRotZ
	entryAnim.cameraFov = fGarageCameraFOV
ENDPROC

FUNC SIMPLE_INTERIOR_DETAILS_BS GET_WAREHOUSE_PROPERTIES_BS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_DETAILS_BS structReturn
	
	//BS1
	
	//BS2
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_PARENT_INT_SCRIPT_DOES_LOAD_SCENE)
	
	RETURN structReturn
ENDFUNC

PROC GET_WAREHOUSE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSMPLIntPreCache)
		
	INT iWarehouseID = GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	details.sProperties = GET_WAREHOUSE_PROPERTIES_BS(eSimpleInteriorID)
	
	SWITCH GET_WAREHOUSE_SIZE(iWarehouseID)
		CASE eWarehouseSmall
			INITIALISE_GENERIC_SMALL_WAREHOUSE(details)
		BREAK
		CASE eWarehouseMedium
			INITIALISE_GENERIC_MEDIUM_WAREHOUSE(details)
		BREAK
		CASE eWarehouseLarge
			INITIALISE_GENERIC_LARGE_WAREHOUSE(details)
		BREAK
	ENDSWITCH
	
	GET_WAREHOUSE_INSIDE_BOUNDING_BOX(eSimpleInteriorID, details.insideBBox  , FALSE )
	
	INITIALISE_WAREHOUSE_SOUND_NAMES_AND_SETS(eSimpleInteriorID, details)
	
	details.strInteriorChildScript = "AM_MP_WAREHOUSE"
	
	SWITCH eSimpleInteriorID
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_1 // ciW_Elysian_Island_2 // PASS 1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<55.6698, -2562.5171, 9.7216>>
			details.entryAnim.cameraRot = <<-26.9934, -0.0000, 158.2394>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<55.173, -2569.180, 5.005>>
			details.entryAnim.syncSceneRot = <<0, 0, 88.920>>
			details.entryAnim.startingPhase = 0.185
			details.entryAnim.endingPhase = 0.450
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<47.4620, -2556.3516, 9.3575>>
			details.entryAnim.establishingCameraRot = <<-3.9405, 0.0000, -159.8518>>
			details.entryAnim.fEstablishingCameraFov = 60.9384
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_2 // ciW_La_Puerta_1 // PASS 1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-1085.3030, -1258.3400, 6.2175>>
			details.entryAnim.cameraRot = <<-8.7714, -0.0000, -149.6619>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-1083.550, -1262.168, 4.584>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 120.0>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-1076.2229, -1243.3074, 10.6771>>
			details.entryAnim.establishingCameraRot = <<-9.2423, 0.0000, 147.7254>>
			details.entryAnim.fEstablishingCameraFov = 51.5371
			
			details.entryAnim.modelFakeDoor = PROP_DAMDOOR_01
			details.entryAnim.vFakeDoorPosition = <<-1083.314, -1262.911, 6.048>>
			details.entryAnim.vFakeDoorRotation = <<0.0, 0.0, 120.0>>
			
			details.entryAnim.modelFakeFrame = PROP_DAMDOOR_01
			details.entryAnim.vFakeFramePosition = <<-1083.289, -1262.854, 3.742>>
			details.entryAnim.vFakeFrameRotation = <<0.0, 0.0, 120.0>>
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_3 // ciW_La_Mesa_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<894.9501, -1036.6302, 35.7994>>
			details.entryAnim.cameraRot = <<-6.6207, -0.0000, -74.3905>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<896.377, -1036.943, 34.114>>
			details.entryAnim.syncSceneRot = <<0, 0, -176.040>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<905.3550, -1022.5312, 39.7356>>
			details.entryAnim.establishingCameraRot = <<-11.3208, 0.0000, 143.5729>>
			details.entryAnim.fEstablishingCameraFov = 57.3700
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_4 // ciW_Rancho_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<249.9270, -1959.5112, 23.9722>>
			details.entryAnim.cameraRot = <<0.0683, -0.0000, 47.4904>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<246.915, -1957.426, 22.234>>
			details.entryAnim.syncSceneRot = <<0, 0, -41.400>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<245.9380, -1938.4198, 27.6721>>
			details.entryAnim.establishingCameraRot = <<-7.5442, -0.0000, 159.4763>>
			details.entryAnim.fEstablishingCameraFov = 59.7297
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_5 // ciW_West_Vinewood_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-422.9159, 186.2770, 81.3179>>
			details.entryAnim.cameraRot = <<0.2632, -0.0000, 96.0908>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-424.770, 186.591, 79.806>>
			details.entryAnim.syncSceneRot = <<0, 0, 6.840>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-408.0694, 174.6422, 86.9432>>
			details.entryAnim.establishingCameraRot = <<-13.2642, 0.0000, 28.3935>>
			details.entryAnim.fEstablishingCameraFov = 52.4348
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_6 // ciW_LSIA_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-1040.9694, -2022.8397, 13.8865>>
			details.entryAnim.cameraRot = <<-4.3062, -0.0000, 128.7958>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-1041.771, -2024.093, 12.262>>
			details.entryAnim.syncSceneRot = <<0, 0, 48.600>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-1085.9432, -2028.4543, 24.7947>>
			details.entryAnim.establishingCameraRot = <<-1.2216, -0.0000, -90.0273>>
			details.entryAnim.fEstablishingCameraFov = 55.8325
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_7 // ciW_Del_Perro_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-1269.0490, -810.1869, 17.6841>>
			details.entryAnim.cameraRot = <<-5.2284, -0.0000, -154.3061>> 
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-1267.408, -811.694, 16.108>>
			details.entryAnim.syncSceneRot = <<0, 0, 129.240>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-1275.5771, -830.3143, 21.4835>>
			details.entryAnim.establishingCameraRot = <<-1.8344, -0.0000, -30.0049>>
			details.entryAnim.fEstablishingCameraFov = 54.6851
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_8 // ciW_LSIA_2
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_left"
			//details.entryAnim.cameraPos = <<-872.8038, -2734.1841, 14.4381>>
			//details.entryAnim.cameraRot = <<-21.9497, -0.0314, -176.8129>>
			//details.entryAnim.cameraFov = 30.3982
			details.entryAnim.syncScenePos = <<-873.020, -2736.370, 12.960>>
			details.entryAnim.syncSceneRot = <<0, 0, -1.44>>
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			
			details.entryAnim.establishingCameraPos = <<-900.0779, -2719.9211, 22.4450>>
			details.entryAnim.establishingCameraRot = <<2.7476, -0.0000, -120.3092>>
			details.entryAnim.fEstablishingCameraFov = 74.8420
			
			_SET_CAM_COORDS_SAME_AS_GARAGE_EXIT(details.entryAnim)
			
			////details.entryAnim.fPhaseForEnterSound = 0.4
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_9 // ciW_Elysian_Island_1 // PASS 1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<275.3322, -3009.5398, 10.6753>>
			details.entryAnim.cameraRot = <<-38.6490, -0.0000, 167.1707>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<276.130, -3015.219, 5.299>>
			details.entryAnim.syncSceneRot = <<0, 0, 89.640>>
			details.entryAnim.startingPhase = 0.220
			details.entryAnim.endingPhase = 0.480
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<258.2233, -3009.1833, 8.9970>>
			details.entryAnim.establishingCameraRot = <<-1.9814, -0.0000, -115.9320>>
			details.entryAnim.fEstablishingCameraFov = 51.7418
			
			details.entryAnim.modelFakeDoor = PROP_DAMDOOR_01
			details.entryAnim.vFakeDoorPosition = <<276.161, -3016.038, 6.4456>>
			details.entryAnim.vFakeDoorRotation = <<0.0, 0.0, 90.0>>
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_10 // ciW_El_Burro_Heights_1 // PASS 1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1575.4513, -2129.3647, 80.8606>>
			details.entryAnim.cameraRot = <<-16.8649, 0.0000, 103.5603>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<1569.734, -2129.194, 77.335>>
			details.entryAnim.syncSceneRot = <<0, 0, 15.480>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<1581.3960, -2133.4521, 81.8751>>
			details.entryAnim.establishingCameraRot = <<-6.7888, -0.0000, 56.6227>>
			details.entryAnim.fEstablishingCameraFov = 62.3286
			
			details.entryAnim.modelFakeDoor = PROP_DAMDOOR_01
			details.entryAnim.vFakeDoorPosition = <<1568.861, -2129.332, 78.573>>
			details.entryAnim.vFakeDoorRotation = <<0.0, 0.0, 15.300>>
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_11 // ciW_Elysian_Island_3 // PASS 1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-307.5915, -2705.9937, 11.3516>>
			details.entryAnim.cameraRot = <<-13.9349, -0.0000, 44.8217>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-316.077, -2698.804, 6.550>>
			details.entryAnim.syncSceneRot = <<0, 0, 134.280>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-291.8181, -2701.5024, 11.8306>>
			details.entryAnim.establishingCameraRot = <<-7.1218, -0.0000, 90.1946>>
			details.entryAnim.fEstablishingCameraFov = 51.9229
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_12 // ciW_La_Mesa_2
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<501.5328, -645.4450, 28.0574>>
			details.entryAnim.cameraRot = <<-22.2649, -0.0000, 160.9995>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<499.179, -651.811, 23.909>>
			details.entryAnim.syncSceneRot = <<0, 0, 84.240>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<512.1525, -636.3975, 28.4480>>
			details.entryAnim.establishingCameraRot = <<-1.2414, -0.0000, 122.1720>>
			details.entryAnim.fEstablishingCameraFov = 53.7499
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_13 // ciW_Maze_Bank_Arena_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-539.1638, -1778.4342, 23.8737>>
			details.entryAnim.cameraRot = <<-9.2253, 0.0000, -119.9646>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-528.827, -1784.887, 20.598>>
			details.entryAnim.syncSceneRot = <<0, 0, 149.040>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-539.6097, -1759.1420, 27.4026>>
			details.entryAnim.establishingCameraRot = <<-5.2881, -0.0000, -171.1322>>
			details.entryAnim.fEstablishingCameraFov = 53.8715
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_14 // ciW_Strawberry_1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-294.1060, -1359.9198, 32.7505>>
			details.entryAnim.cameraRot = <<-4.7499, -0.0000, -0.9732>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-296.605, -1353.023, 30.308>>
			details.entryAnim.syncSceneRot = <<0, 0, -89.640>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-277.6619, -1368.5438, 35.1506>>
			details.entryAnim.establishingCameraRot = <<-1.3521, -0.0000, 55.7123>>
			details.entryAnim.fEstablishingCameraFov = 50.9305
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_15 // ciW_Downtown_Vinewood_1 // PASS 1
			// Entry animation
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<356.1015, 326.3331, 107.5898>>
			details.entryAnim.cameraRot = <<-18.8627, -0.0000, 59.5920>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<350.351, 330.055, 103.445>>
			details.entryAnim.syncSceneRot = <<0, 0, -13.680>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<360.9772, 310.1750, 109.4587>>
			details.entryAnim.establishingCameraRot = <<-2.4531, 0.0000, 12.6192>>
			details.entryAnim.fEstablishingCameraFov = 51.9772
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_16 // ciW_La_Mesa_3
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<925.8218, -1564.6162, 34.3633>>
			details.entryAnim.cameraRot = <<-32.8521, -0.0000, -19.6456>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<927.600, -1560.443, 29.943>>
			details.entryAnim.syncSceneRot = <<0, 0, -90.720>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<913.1396, -1575.7090, 34.2933>>
			details.entryAnim.establishingCameraRot = <<3.1582, -0.0000, -58.1463>>
			details.entryAnim.fEstablishingCameraFov = 53.8629
			
			details.entryAnim.modelFakeDoor = PROP_DAMDOOR_01
			details.entryAnim.vFakeDoorPosition = <<927.711, -1559.567, 31.094>>
			details.entryAnim.vFakeDoorRotation = <<0.0, 0.0, -90.000>>
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_17 // ciW_La_Mesa_4
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<759.1849, -906.9100, 26.0423>>
			details.entryAnim.cameraRot = <<-1.1809, -0.0000, -179.4534>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<758.631, -909.267, 24.438>>
			details.entryAnim.syncSceneRot = <<0, 0, 91.440>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<777.9869, -877.4920, 33.0619>>
			details.entryAnim.establishingCameraRot = <<-7.5130, -0.0000, 129.5847>>
			details.entryAnim.fEstablishingCameraFov = 50.8783
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_18 // ciW_Cypress_Flats_2
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1038.0001, -2170.0530, 32.9912>>
			details.entryAnim.cameraRot = <<-22.5347, -0.0000, 175.7987>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<1037.014, -2172.944, 30.550>>
			details.entryAnim.syncSceneRot = <<0, 0, 84.960>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<1061.9108, -2157.0208, 35.0165>>
			details.entryAnim.establishingCameraRot = <<-2.5720, 0.0000, 111.9210>>
			details.entryAnim.fEstablishingCameraFov = 58.6139
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_19 // ciW_Cypress_Flats_3
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1017.9374, -2517.5828, 30.4418>>
			details.entryAnim.cameraRot = <<-18.4157, -0.0000, -15.4654>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<1019.739, -2511.918, 27.445>>
			details.entryAnim.syncSceneRot = <<0, 0, -96.120>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<1000.5519, -2529.7949, 32.7126>>
			details.entryAnim.establishingCameraRot = <<-5.7162, -0.0000, -73.9334>>
			details.entryAnim.fEstablishingCameraFov = 55.0288
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_20 // ciW_Vinewood_1
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-248.4653, 203.9834, 86.8847>>
			details.entryAnim.cameraRot = <<-36.3135, -0.0000, -102.8081>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<-245.054, 204.085, 83.000>>
			details.entryAnim.syncSceneRot = <<0, 0, -3.600>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<-270.0336, 183.7648, 90.1035>>
			details.entryAnim.establishingCameraRot = <<-1.7739, -0.0000, -22.3539>>
			details.entryAnim.fEstablishingCameraFov = 62.3599
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_21 // ciW_Rancho_2
			details.entryAnim.dictionary = "anim@apt_trans@garage"
			details.entryAnim.clip = "gar_open_1_right"
			//details.entryAnim.cameraPos = <<537.7051, -1940.6688, 28.6519>>
			//details.entryAnim.cameraRot = <<-28.3917, -0.0000, -172.0205>>
			//details.entryAnim.cameraFov = 30.3982
			details.entryAnim.syncScenePos = <<539.003, -1945.851, 23.984>>
			details.entryAnim.syncSceneRot = <<0, 0, -131.040>> // take the right angle heading and add 26.64 to get the same one as in garages
			details.entryAnim.startingPhase = 0.0
			details.entryAnim.endingPhase = 0.45
			////details.entryAnim.fPhaseForEnterSound = 0.4
			details.entryAnim.establishingCameraPos = <<545.4655, -1926.3098, 30.7424>>
			details.entryAnim.establishingCameraRot = <<-6.6888, -0.0000, 152.7569>>
			details.entryAnim.fEstablishingCameraFov = 52.6655
			
			_SET_CAM_COORDS_SAME_AS_GARAGE_EXIT(details.entryAnim)
			
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_WAREHOUSE_22 // ciW_Banning_1
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<97.0925, -2218.3169, 6.9921>>
			details.entryAnim.cameraRot = <<-1.0588, -0.0000, -3.5294>>
			details.entryAnim.cameraFov = 40.0
			details.entryAnim.syncScenePos = <<97.293, -2215.945, 5.502>>
			details.entryAnim.syncSceneRot = <<0, 0, -88.200>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			//details.entryAnim.fPhaseForEnterSound = 0.283
			details.entryAnim.establishingCameraPos = <<82.9859, -2232.3552, 9.8449>>
			details.entryAnim.establishingCameraRot = <<-1.3436, -0.0000, -57.1714>>
			details.entryAnim.fEstablishingCameraFov = 55.5776
		BREAK
		//════════════════════════════════════════════════════════════════════
	ENDSWITCH
	
ENDPROC

PROC GET_WAREHOUSE_EXIT_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iCurrentExit)
	IF COUNT_OF(strOptions) != SIMPLE_INTERIOR_MENU_MAX_OPTIONS
		CASSERTLN(DEBUG_PROPERTY, "GET_WAREHOUSE_EXIT_MENU_TITLE_AND_OPTIONS - options array has wrong size.")
		EXIT
	ENDIF
	
	txtTitle = "MP_WHOUSE_EXITT"
	iOptionsCount = 2
	strOptions[0] = "MP_WHOUSE_EXITa"
	strOptions[1] = "MP_WHOUSE_EXITb"
ENDPROC

FUNC STRING GET_SIMPLE_INTERIOR_WAREHOUSE_NAME(SIMPLE_INTERIORS eSimpleInteriorID)
	RETURN GET_WAREHOUSE_NAME(GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
ENDFUNC

/// PURPOSE:
///    We can only trigger exit all if we are the owner of the warehouse
/// PARAMS:
///    eSimpleInteriorID - 
/// RETURNS:
///    
FUNC BOOL CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_WAREHOUSE(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitId)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iExitId)
	RETURN IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_OWNER) OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_BOSS)
ENDFUNC

/// PURPOSE:
///    Sets access bits which hold information on how player entered given warehouse - as a goon? as boss? as owner?
///    Also determines which players (who already might be in a warehouse) we should join.
/// PARAMS:
///    eSimpleInteriorID - 
///    iInvitingPlayer - 
PROC SET_WAREHOUSE_ACCESS_BS(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS = 0
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_BOSS)
		ELSE
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_GOON)
		ENDIF
		
		// Create flags for everyone who potentially we would like to join in the warehouse
		PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
		PLAYER_INDEX playerGoon
		
		IF playerBoss != INVALID_PLAYER_INDEX() 
		AND playerBoss != PLAYER_ID() 
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerBoss))
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] SET_WAREHOUSE_ACCESS_BIT - We want to join boss: ", GET_PLAYER_NAME(playerBoss), " (", NATIVE_TO_INT(playerBoss), ")")
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] SET_WAREHOUSE_ACCESS_BIT - Check if there are any goons to join, how many of them are: ", GB_GET_NUM_GOONS_IN_LOCAL_GANG())
		#ENDIF
		
		INT i
		REPEAT GB_GET_NUM_GOONS_IN_LOCAL_GANG() i
			playerGoon = GB_GET_GANG_GOON_AT_INDEX(playerBoss, i)
		
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
				SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerGoon))
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] SET_WAREHOUSE_ACCESS_BIT - We want to join goon: ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ")")
				#ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] SET_WAREHOUSE_ACCESS_BIT - Players we should join BS: ", globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS)
		#ENDIF
	ELSE
		IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] SET_WAREHOUSE_ACCESS_BIT - We are entering this warehouse as in owner.")
			#ENDIF
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_OWNER)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_WAREHOUSE(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	IF g_SimpleInteriorData.iAccessBS = 0
		// How did we get here? Probably script relaunched somehow, kick us out
		RETURN TRUE
	ENDIF
	
		// No access at all if you're a member or boss of MC
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			RETURN TRUE
		ENDIF
	
	// Kick out players who entered as goons and stopped being goons while inside.
	// Even if they own a warehouse they need to reenter their own after they stopped being a goon.
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_GOON)
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// If we walked in our own warehouse as boss or owner but quit and got hired as a goon - kick us out
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// For warehouses, since there are no invites, not being able to walk in is equivalent to not being supposed to be inside
	RETURN NOT CAN_PLAYER_ENTER_WAREHOUSE(PLAYER_ID(), eSimpleInteriorID, 0)
ENDFUNC

FUNC STRING GET_WAREHOUSE_KICK_OUT_REASON(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	UNUSED_PARAMETER(txtPlayerNameString)
	//UNUSED_PARAMETER(txtExtraString)
	
	IF g_SimpleInteriorData.bExitAllTriggeredByOwner
		g_SimpleInteriorData.bExitAllTriggeredByOwner = FALSE
		RETURN "MP_WHOUSE_KICKe"
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS = 0
		RETURN "MP_WHOUSE_KICKc" // you don't have access to this warehouse
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_GOON)
			// We were a goon and now we are part of MC
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
					RETURN "MP_WHOUSE_KICKf" // You no longer have access to the Warehouse as you became an MC President.
				ELSE
					txtExtraString = GB_GET_PLAYER_BIKER_ROLE_TEXT_LABEL(PLAYER_ID())
					RETURN "MP_WHOUSE_KICKg" // You no longer have access to the Warehouse as you became [ROLE].
				ENDIF
			ENDIF
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			RETURN "MP_WHOUSE_KICKb" // you're not longer a goon
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_WAREHOUSE_ACCESS_BS_OWNER)
			// We were a boss or no function and now we are part of MC
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
					RETURN "MP_WHOUSE_KICKf" // You no longer have access to the Warehouse as you became an MC President.
				ELSE
					txtExtraString = GB_GET_PLAYER_BIKER_ROLE_TEXT_LABEL(PLAYER_ID())
					RETURN "MP_WHOUSE_KICKg" // You no longer have access to the Warehouse as you became [ROLE].
				ENDIF
			ENDIF
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			RETURN "MP_WHOUSE_KICKd" // Your Warehouse is unavailable when working as a Bodyguard for a VIP.
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

PROC GET_WAREHOUSE_ENTRY_LOCATE_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT &iR, INT &iG, INT &iB, INT &iA)
	UNUSED_PARAMETER(eSimpleInteriorID)
	GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
ENDPROC

/// PURPOSE:
///    This sets some data in stagger to determine if any given warehouse is owned by any
///    friend in the session. Both players and warehouses are staggered, final data is copied
///    to a bitset at the end of full player/warehouse cycle.
PROC MAINTAIN_WAREHOUSES_FRIEND_OWNERSHIP()

	IF IS_SIMPLE_INTERIOR_VALID(INT_TO_ENUM(SIMPLE_INTERIORS, g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger))
	
		// Show blip if any of our friends owns it
		INT iArray, iBit 
		GetBitsetArrayIndexAndBitForSimpleInterior(INT_TO_ENUM(SIMPLE_INTERIORS, g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger), iArray, iBit)
		
		PLAYER_INDEX playerFriend = INT_TO_PLAYERINDEX(g_SimpleInteriorData.iFriendOwnerStagger)
	
		IF NOT IS_BIT_SET(g_SimpleInteriorData.iOwnedByFriendBitSetTemp[iArray], iBit)
			IF playerFriend != INVALID_PLAYER_INDEX() AND playerFriend != PLAYER_ID() AND IS_NET_PLAYER_OK(playerFriend, FALSE)
				
				BOOL bThisIsFriend = FALSE
				
				g_ClanDescription = GET_PLAYER_CREW(PLAYER_ID())
				g_otherClanDescription = GET_PLAYER_CREW(playerFriend)
				IF g_ClanDescription.Id = g_otherClanDescription.Id
					bThisIsFriend = TRUE
				ELSE
					g_GamerHandle = GET_GAMER_HANDLE_PLAYER(playerFriend)
					IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
						IF NETWORK_IS_FRIEND(g_GamerHandle)	
							bThisIsFriend = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				SWITCH GET_SIMPLE_INTERIOR_TYPE(INT_TO_ENUM(SIMPLE_INTERIORS, g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger))
				
					CASE SIMPLE_INTERIOR_TYPE_WAREHOUSE
						IF bThisIsFriend AND DOES_PLAYER_OWN_WAREHOUSE(playerFriend, GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(INT_TO_ENUM(SIMPLE_INTERIORS, g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger)))
							SET_BIT(g_SimpleInteriorData.iOwnedByFriendBitSetTemp[iArray], iBit)
						ENDIF
					BREAK
					CASE SIMPLE_INTERIOR_TYPE_IE_GARAGE
//						IF bThisIsFriend AND DOES_PLAYER_OWN_WAREHOUSE(playerFriend, GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(INT_TO_ENUM(SIMPLE_INTERIOR, g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger)))
//							SET_BIT(g_SimpleInteriorData.iOwnedByFriendBitSetTemp[iArray], iBit)
//						ENDIF
					BREAK
					CASE SIMPLE_INTERIOR_TYPE_FACTORY
						IF bThisIsFriend AND DOES_PLAYER_OWN_FACTORY(playerFriend, GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(INT_TO_ENUM(SIMPLE_INTERIORS, g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger)))
							SET_BIT(g_SimpleInteriorData.iOwnedByFriendBitSetTemp[iArray], iBit)
						ENDIF					
					BREAK					
				ENDSWITCH
			

			ENDIF
		ENDIF
		

		g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger = (g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger + 1) % ENUM_TO_INT(SIMPLE_INTERIOR_END)
		
		IF g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger = ENUM_TO_INT(SIMPLE_INTERIOR_INVALID) + 1
			g_SimpleInteriorData.iFriendOwnerStagger = (g_SimpleInteriorData.iFriendOwnerStagger + 1) % NUM_NETWORK_PLAYERS
		
			IF g_SimpleInteriorData.iFriendOwnerStagger = 0
				// We went the full cycle, can copy visibility data to actual bitset
				REPEAT COUNT_OF(g_SimpleInteriorData.iOwnedByFriendBitSet) iArray
					g_SimpleInteriorData.iOwnedByFriendBitSet[iArray] = g_SimpleInteriorData.iOwnedByFriendBitSetTemp[iArray]
					g_SimpleInteriorData.iOwnedByFriendBitSetTemp[iArray] = 0
				ENDREPEAT
			ENDIF
		ENDIF
	
	ELSE
		g_SimpleInteriorData.iOwnedByFriendSimpleInteriorStagger = ENUM_TO_INT(SIMPLE_INTERIOR_INVALID) + 1
	ENDIF
		
ENDPROC

FUNC INT GET_WAREHOUSE_MAX_BLIPS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN MAX_WAREHOUSE_BLIPS
ENDFUNC

FUNC BOOL SHOULD_WAREHOUSE_BE_BLIPPED_THIS_FRAME(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	INT iWarehouseID = GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), iWarehouseID)
				RETURN TRUE
			ENDIF
		ELSE
			IF DOES_PLAYER_OWN_WAREHOUSE(GB_GET_LOCAL_PLAYER_GANG_BOSS(), iWarehouseID)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), iWarehouseID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WAREHOUSE_BLIP_SHORT_RANGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BLIP_SPRITE GET_WAREHOUSE_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN RADAR_TRACE_WAREHOUSE
	/*
	IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		RETURN RADAR_TRACE_WAREHOUSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		RETURN RADAR_TRACE_WAREHOUSE
	ENDIF
	
	RETURN RADAR_TRACE_WAREHOUSE_FOR_SALE
	*/
ENDFUNC

FUNC BLIP_PRIORITY GET_WAREHOUSE_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIPPRIORITY_MED
ENDFUNC

PROC MAINTAIN_WAREHOUSE_BLIP_EXTRA_FUNCTIONALITY(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)
	UNUSED_PARAMETER(OverlayBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	INT iWarehouseID = GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	BOOL bShouldUpdate = FALSE
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT IS_PLAYER_ON_MP_INTRO_MISSION()
	#ENDIF
		IF iBlipType != 1
			bShouldUpdate = TRUE
		ENDIF
		
		TEXT_LABEL_63 strBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID())
		
		IF NOT bShouldUpdate
			IF NOT IS_STRING_NULL_OR_EMPTY(strBlipName)
			AND GET_HASH_KEY(strBlipName) != iBlipNameHash
				bShouldUpdate = TRUE
			ENDIF
		ENDIF
		
		IF bShouldUpdate
			BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_WHOUSE_BLP2") // ~a~ Warehouse
			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(strBlipName)
			END_TEXT_COMMAND_SET_BLIP_NAME(blipIndex)
			iBlipNameHash = GET_HASH_KEY(strBlipName)
			iBlipType = 1
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_BLIP_NAME - Updated organization name (", strBlipName ,") on warehouse blip (simple interior id: ", eSimpleInteriorID, ")")
			#ENDIF
			EXIT
		ENDIF
	ELSE
		IF iBlipType != 2
			bShouldUpdate = TRUE
		ENDIF
		
		IF bShouldUpdate
			IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), iWarehouseID)
				SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "MP_WHOUSE_BLP0") // Owned warehouse
				iBlipType = 2
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_BLIP_NAME - Updated name to owned warehouse (simple interior id: ", eSimpleInteriorID, ")")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF iBlipType = 0 // This happens if no other names were selected
		bShouldUpdate = TRUE
	ENDIF
	
	IF bShouldUpdate
		SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "MP_WHOUSE_BLP1") // Warehouse
		iBlipType = 3
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_BLIP_NAME - Using generic warehouse blip name, no other was selected (simple interior id: ", eSimpleInteriorID, ")")
		#ENDIF
	ENDIF
ENDPROC

FUNC INT GET_WAREHOUSE_BLIP_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	
	//url:bugstar:2817398 - Exec1 - Colour offices and warehouse blips the Org colour when you are a Bodyguard and change the access to your own offices (warehouse access is already blocked)
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_GANG_TYPE(GT_VIP)
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
	ELIF GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(PLAYER_ID(), FALSE, GT_VIP)
		PLAYER_INDEX GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
	ENDIF
	
	RETURN BLIP_COLOUR_WHITE
ENDFUNC

FUNC VECTOR GET_WAREHOUSE_MAP_MIDPOINT(SIMPLE_INTERIORS eSimpleInteriorID)
	RETURN GET_WAREHOUSE_BLIP_COORDS(eSimpleInteriorID)
ENDFUNC

FUNC BOOL DOES_WAREHOUSE_BLOCK_WEAPONS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC	

FUNC BOOL DOES_WAREHOUSE_RESTRICT_PLAYER_MOVEMENT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC _REMOVE_WAREHOUSE_GATE_MODEL_HIDES(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	IF IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
		IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_19 // ciW_Cypress_Flats_3
			/* Disabled for B*4051170
			REMOVE_MODEL_HIDE(<<996.4036, -2481.7568, 28.8007>>, 4.0, PROP_FACGATE_04_L)
			REMOVE_MODEL_HIDE(<<985.8288, -2480.6804, 28.8007>>, 4.0, PROP_FACGATE_04_R)
			REMOVE_MODEL_HIDE(<<996.5616, -2481.7729, 28.8807>>, 2.0, PROP_FACGATE_03POST)
			REMOVE_MODEL_HIDE(<<985.6758, -2480.6670, 28.8807>>, 2.0, PROP_FACGATE_03POST)
			*/
			CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] _REMOVE_WAREHOUSE_GATE_MODEL_HIDES - Cleared model hides for door at ciW_Cypress_Flats_3")
			#ENDIF
		ENDIF
		
		IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_13 // ciW_La_Mesa_2
			/* Disabled for B*4051170
			REMOVE_MODEL_HIDE(<<-550.8629, -1772.7957, 22.3438>>, 4.0, PROP_FACGATE_03B_R, FALSE)
			REMOVE_MODEL_HIDE(<<-542.2010, -1777.7977, 22.1771>>, 4.0, PROP_FACGATE_03B_L, FALSE)
			*/
			
			CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] _REMOVE_WAREHOUSE_GATE_MODEL_HIDES - Cleared model hides for door at ciW_La_Mesa_2")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eState)
	UNUSED_PARAMETER(bExtScriptMustRun)
	
	IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_21 // ciW_Rancho_2
		VECTOR vDoorPos = <<550.9767, -1896.7406, 24.1463>>
		INT iDoorHash
		
		IF IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_DOOR_AROUND_OPENED)
			IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vDoorPos) > 70.0
				CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_DOOR_AROUND_OPENED)
			ENDIF
		ELSE
			IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vDoorPos) <= 70.0
				
				// Left side
				IF NOT DOOR_SYSTEM_FIND_EXISTING_DOOR(vDoorPos, PROP_FACGATE_01, iDoorHash)
					iDoorHash = GET_HASH_KEY("WHOUSE_DOOR_RANCHO_2A")
					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
						ADD_DOOR_TO_SYSTEM(iDoorHash, PROP_FACGATE_01, vDoorPos, FALSE, FALSE)
					ENDIF
				ENDIF
				
				DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, 1.0, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_UNLOCKED, FALSE)
				
				// Right side
				vDoorPos = <<539.9537, -1901.9557, 24.2126>>
				IF NOT DOOR_SYSTEM_FIND_EXISTING_DOOR(vDoorPos, PROP_FACGATE_01B, iDoorHash)
					iDoorHash = GET_HASH_KEY("WHOUSE_DOOR_RANCHO_2B")
					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
						ADD_DOOR_TO_SYSTEM(iDoorHash, PROP_FACGATE_01B, vDoorPos, FALSE, FALSE)
					ENDIF
				ENDIF
				
				DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, -1.0, FALSE)
				DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_UNLOCKED, FALSE)
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS - Opened door for ciW_Rancho_2")
				#ENDIF
			
				SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_DOOR_AROUND_OPENED)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("DisableModelHides")
		IF (GET_FRAME_COUNT() % 30) = 0	
			PRINTLN("MAINTAIN_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS - Command line: 'DisableModelHides' is active, preventing model hides from being created.")
		ENDIF
	ELSE
	#ENDIF
	
		IF NOT IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
			IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_19 // ciW_Cypress_Flats_3
			AND NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
				/* Disabled for B*4051170
				CREATE_MODEL_HIDE(<<996.4036, -2481.7568, 28.8007>>, 4.0, PROP_FACGATE_04_L, FALSE)
				CREATE_MODEL_HIDE(<<985.8288, -2480.6804, 28.8007>>, 4.0, PROP_FACGATE_04_R, FALSE)
				CREATE_MODEL_HIDE(<<996.5616, -2481.7729, 28.8807>>, 2.0, PROP_FACGATE_03POST, FALSE)
				CREATE_MODEL_HIDE(<<985.6758, -2480.6670, 28.8807>>, 2.0, PROP_FACGATE_03POST, FALSE)
				*/
				
				SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS - Removed door for ciW_Cypress_Flats_3")
				#ENDIF
			ENDIF
			
			IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_13 // ciW_La_Mesa_2
			AND NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
				/* Disabled for B*4051170
				CREATE_MODEL_HIDE(<<-550.8629, -1772.7957, 22.3438>>, 4.0, PROP_FACGATE_03B_R, FALSE)
				CREATE_MODEL_HIDE(<<-542.2010, -1777.7977, 22.1771>>, 4.0, PROP_FACGATE_03B_L, FALSE)
				*/
				
				SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS - Removed door for ciW_La_Mesa_2")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_PROBLEMATIC_GATES_REMOVED)
				_REMOVE_WAREHOUSE_GATE_MODEL_HIDES(details, eSimpleInteriorID)
			ENDIF
		ENDIF
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

PROC RESET_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_21
	
	ENDIF
	
	_REMOVE_WAREHOUSE_GATE_MODEL_HIDES(details, eSimpleInteriorID)
	
	/*
	CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_WANTED_MODIFIER_SET)
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	*/
ENDPROC

PROC MAINTAIN_WAREHOUSE_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_WANTED_LEVEL_CLEARED)
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][WAREHOUSE] MAINTAIN_WAREHOUSE_INSIDE_GAMEPLAY_MODIFIERS - Cleared wanted level")
			#ENDIF
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		ENDIF
		SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_WANTED_LEVEL_CLEARED)
	ENDIF
ENDPROC

PROC RESET_WAREHOUSE_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_WAREHOUSE_WANTED_LEVEL_CLEARED)
ENDPROC

FUNC BOOL SHOULD_THIS_WAREHOUSE_CORONA_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD, BOOL bPrintReason #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF g_sMPTunables.bexec_disable_warehouse_entry 
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, disabled by tunable bexec_disable_warehouse_entry")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT CAN_PLAYER_USE_PROPERTY(TRUE #IF IS_DEBUG_BUILD , bPrintReason #ENDIF)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, CAN_PLAYER_USE_PROPERTY is FALSE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF (NOT GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID()) AND IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = ", IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY(), " and player is not on contraband mission")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_SELL
		AND NOT IS_BIT_SET(g_GB_WORK_VARS.iEventBitSet, ciGB_BOSS_SELL_MISSION_FAILED)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, we are on sell contraband mission.")
				ENDIF
			#ENDIF
			RETURN TRUE
			
		ELIF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_BUY
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				#IF IS_DEBUG_BUILD
					IF bPrintReason
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, player is on buy mission and has wanted level.")
					ENDIF
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, NETWORK_IS_ACTIVITY_SESSION() is TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	BOOL bTempPrintReason = FALSE
	#IF IS_DEBUG_BUILD
	bTempPrintReason = bPrintReason
	#ENDIF
	
	IF IS_PLAYER_BLOCKED_FROM_PROPERTY(bTempPrintReason, PROPERTY_WAREHOUSE)
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR] SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN - Returning TRUE, IS_PLAYER_BLOCKED_FROM_PROPERTY = FALSE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
	OR HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR]SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN TRUE player is entering avenger from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_HACKER_TRUCK_PILOT_ENTERING()
	OR IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR]SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN TRUE player is entering hacker truck from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_WAREHOUSE_CAR_GENS_BLOCK_AREA(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)
	IF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_4
		vMin = <<238.6076, -1980.3732, 19.7774>>
		vMax = <<262.0809, -1954.5331, 28.2874>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_WAREHOUSE_5
		vMin = <<-416.9828, 175.1209, 78.2398>>
		vMax = <<-404.4813, 188.3270, 85.6049>>
		bDoBlockCarGen = TRUE
		EXIT
	ENDIF
	
	bDoBlockCarGen = FALSE
ENDPROC

FUNC BOOL GET_WAREHOUSE_OUTSIDE_MODEL_HIDE(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iIndex)
	UNUSED_PARAMETER(vPos)
	UNUSED_PARAMETER(fRadius)
	UNUSED_PARAMETER(fActivationRadius)
	UNUSED_PARAMETER(eModel)
	UNUSED_PARAMETER(bSurviveMapReload)
	RETURN FALSE
ENDFUNC

PROC GET_WAREHOUSE_SCENARIO_PED_REMOVAL_SPHERE(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vCoords)
	UNUSED_PARAMETER(fRadius)
	bDoRemoveScenarioPeds = FALSE
ENDPROC

FUNC BOOL SHOULD_TRIGGER_EXIT_IN_CAR_FOR_WAREHOUSE(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(interiorOwner)
	UNUSED_PARAMETER(bOwnerDrivingOut)
	RETURN FALSE
ENDFUNC

FUNC INT GET_WAREHOUSE_ENTRANCES_COUNT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 1
ENDFUNC

FUNC INT GET_WAREHOUSE_INVITE_ENTRY_POINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN -1
ENDFUNC

PROC GET_WAREHOUSE_ENTRANCE_DATA(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)
	IF iEntranceID = 0
		GET_WAREHOUSE_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition)
		
		data.vLocatePos1 = <<1, 1, 2>>
		
		SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
		SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
		
		GET_WAREHOUSE_ENTRY_LOCATE_COLOUR(eSimpleInteriorID, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
	ENDIF
ENDPROC

PROC GET_WAREHOUSE_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_WAREHOUSE_ACCESS(SIMPLE_INTERIOR_DETAILS &details)
	UNUSED_PARAMETER(details)
ENDPROC

FUNC SIMPLE_INTERIORS GET_OWNED_SMPL_INT_OF_TYPE_WAREHOUSE(PLAYER_INDEX playerID, INT iPropertySlot, INT iFactoryType)
	UNUSED_PARAMETER(iFactoryType)
	UNUSED_PARAMETER(iPropertySlot)
	RETURN GET_SIMPLE_INTERIOR_ID_FROM_WAREHOUSE_ID(GET_PLAYER_OWNED_WAREHOUSE_IN_SLOT(playerID, iPropertySlot))
ENDFUNC

PROC WAREHOUSE_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(veh)
	UNUSED_PARAMETER(bSetOnGround)
ENDPROC

//////////////////////////////////////

PROC BUILD_WAREHOUSE_LOOK_UP_TABLE(SIMPLE_INTERIOR_INTERFACE &interface, SIMPLE_INTERIOR_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_WAREHOUSE_USE_EXTERIOR_SCRIPT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION
			interface.getSimpleInteriorInteriorTypeAndPosition = &GET_WAREHOUSE_INTERIOR_TYPE_AND_POSITION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS
			interface.getSimpleInteriorDetails = &GET_WAREHOUSE_DETAILS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS_BS
			interface.getSimpleInteriorDetailsPropertiesBS = &GET_WAREHOUSE_PROPERTIES_BS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_NAME
			interface.simpleInteriorFP_ReturnS_ParamID = &GET_SIMPLE_INTERIOR_WAREHOUSE_NAME
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
			interface.getSimpleInteriorInsideBoundingBox = &GET_WAREHOUSE_INSIDE_BOUNDING_BOX
		BREAK
		CASE E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
			interface.getOwnedSimpleInteriorOfType = &GET_OWNED_SMPL_INT_OF_TYPE_WAREHOUSE
		BREAK
		CASE E_SIMPLE_INTERIOR_LOAD_SECONDARY_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_WAREHOUSE_LOAD_SECONDARY_INTERIOR
		BREAK
		//════════════════════════════════════════════════════════════════════
		
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCES_COUNT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_WAREHOUSE_ENTRANCES_COUNT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_DATA
			interface.getSimpleInteriorEntranceData = &GET_WAREHOUSE_ENTRANCE_DATA
		BREAK	
		CASE E_GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_WAREHOUSE_INVITE_ENTRY_POINT
		BREAK			
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS
			interface.getSimpleInteriorEntranceSceneDetails = &GET_WAREHOUSE_ENTRANCE_SCENE_DETAILS
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_CORONA_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_WAREHOUSE_CORONA_BE_HIDDEN
		BREAK
		
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_LOCATE_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_WAREHOUSE_LOCATE_BE_HIDDEN
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR_IN_VEHICLE
			interface.simpleInteriorFP_ReturnB_ParamPIDAndModel = &CAN_PLAYER_ENTER_WAREHOUSE_IN_VEHICLE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPID_ID_AndInt = &CAN_PLAYER_ENTER_WAREHOUSE
		BREAK
		CASE E_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE
			interface.canPlayerUseEntryInCurrentVeh = &CAN_PLAYER_USE_WAREHOUSE_ENTRY_IN_CURRENT_VEHICLE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_ENTRY
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_WAREHOUSE_REASON_FOR_BLOCKED_ENTRY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_EXIT
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_WAREHOUSE_REASON_FOR_BLOCKED_EXIT
		BREAK
		CASE E_CAN_PLAYER_INVITE_OTHERS_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_INVITE_OTHERS_TO_WAREHOUSE
		BREAK
		CASE E_CAN_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_BE_INVITED_TO_WAREHOUSE
		BREAK
		CASE E_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER
			interface.intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer = &CAN_LOCAL_PLAYER_BE_INVITED_TO_WAREHOUSE_BY_PLAYER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_WAREHOUSE_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDAndEMenu = &PROCESS_WAREHOUSE_ENTRANCE_MENU
		BREAK
		CASE E_SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_WAREHOUSE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_KICK_OUT_REASON
			interface.getSimpleInteriorKickOutReason = &GET_WAREHOUSE_KICK_OUT_REASON
		BREAK
		CASE E_SET_SIMPLE_INTERIOR_ACCESS_BS
			interface.setSimpleInteriorAccessBS = &SET_WAREHOUSE_ACCESS_BS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_SPAWN_POINT
			interface.getSimpleInteriorSpawnPoint = &GET_WAREHOUSE_SPAWN_POINT
		BREAK
		CASE E_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS
			interface.getSimpleInteriorCustomInteriorWarpParams = &GET_WAREHOUSE_CUSTOM_INTERIOR_WARP_PARAMS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT
			interface.getSimpleInteriorOutsideSpawnPoint = &GET_WAREHOUSE_OUTSIDE_SPAWN_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_WAREHOUSE_ASYNC_SPAWN_GRID_LOCATION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_INSIDE_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_WAREHOUSE_ASYNC_SPAWN_GRID_INSIDE_LOCATION
		BREAK
		CASE E_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS
			interface.maintainLeaveentryVehIfNoAccess = &MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_WAREHOUSE_ACCESS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_TELE_IN_SVM_COORD
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_WAREHOUSE_TELEPORT_IN_SVM_COORD
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS
			interface.getSimpleInteriorExitMenuTitleAndOptions = &GET_WAREHOUSE_EXIT_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_WAREHOUSE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_MAX_BLIPS
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_WAREHOUSE_MAX_BLIPS
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_BE_BLIPPED_THIS_FRAME
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_WAREHOUSE_BE_BLIPPED_THIS_FRAME
		BREAK
		CASE E_IS_SIMPLE_INTERIOR_BLIP_SHORT_RANGE
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &IS_WAREHOUSE_BLIP_SHORT_RANGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_SPRITE
			interface.simpleInteriorFP_ReturnEBS_ParamIDAndInt = &GET_WAREHOUSE_BLIP_SPRITE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COORDS
			interface.simpleInteriorFP_ReturnV_ParamIDAndInt = &GET_WAREHOUSE_BLIP_COORDS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY
			interface.maintainSimpleInteriorBlipExtraFunctionality = &MAINTAIN_WAREHOUSE_BLIP_EXTRA_FUNCTIONALITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_PRIORITY
			interface.getSimpleInteriorBlipPriority	= &GET_WAREHOUSE_BLIP_PRIORITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COLOUR
			interface.simpleInteriorFP_ReturnI_ParamIDAndInt = &GET_WAREHOUSE_BLIP_COLOUR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_MAP_MIDPOINT
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_WAREHOUSE_MAP_MIDPOINT
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.maintainSimpleInteriorOutsideGameplayModifiers = &MAINTAIN_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_WAREHOUSE_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &MAINTAIN_WAREHOUSE_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_WAREHOUSE_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_BLOCK_WEAPONS
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_WAREHOUSE_BLOCK_WEAPONS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_RESTRICT_PLAYER_MOVEMENT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_WAREHOUSE_RESTRICT_PLAYER_MOVEMENT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA
			interface.getSimpleInteriorCarGensBlockArea = &GET_WAREHOUSE_CAR_GENS_BLOCK_AREA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE
			interface.getSimpleInteriorOutsideModelHide = &GET_WAREHOUSE_OUTSIDE_MODEL_HIDE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE
			interface.getSimpleInteriorScenarioPedRemovalSphere = &GET_WAREHOUSE_SCENARIO_PED_REMOVAL_SPHERE
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_TRIGGER_EXIT_IN_CAR
			interface.simpleInteriorFP_ReturnB_ParamID_PID_AndB = &SHOULD_TRIGGER_EXIT_IN_CAR_FOR_WAREHOUSE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
			interface.DoCustomActionForStateWarpToSpawnPoint = &WAREHOUSE_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
		BREAK
	ENDSWITCH
ENDPROC
