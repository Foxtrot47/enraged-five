// ====================================================================================
// ====================================================================================
//
// Name:        net_orbital_cannon.sch
// Description: Header for Orbital Cannon functionality for simple interiors
// Written By:  Alex Murphy
//
// ====================================================================================
// ====================================================================================

USING "freemode_header.sch"
USING "context_control_public.sch"

/// PURPOSE: Number of initial menu options
CONST_INT MAX_INITIAL_MENU_OPTIONS							3

/// PURPOSE: Initial menu options
CONST_INT INITIAL_MENU_FREE_CAM								0
CONST_INT INITIAL_MENU_ACQUIRE_TARGET						1
CONST_INT INITIAL_MENU_TARGET_LOCK_ON						2

/// PURPOSE: Orbital Cannon local bitset
CONST_INT ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE			0
CONST_INT ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION		1
CONST_INT ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP		2
CONST_INT ORBITAL_CANNON_LOCAL_BS_FOCUS_SET					3
CONST_INT ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW		4
CONST_INT ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS				5
CONST_INT ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN				6
CONST_INT ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT				7
CONST_INT ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS				8
CONST_INT ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1				9
CONST_INT ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2				10
CONST_INT ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU		11
CONST_INT ORBITAL_CANNON_LOCAL_BS_CLOSE_MENU				12
CONST_INT ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS			13
CONST_INT ORBITAL_CANNON_LOCAL_BS_SWITCHING_CAMERA			14
CONST_INT ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING				15
CONST_INT ORBITAL_CANNON_LOCAL_BS_GO_TO_TASK				16
CONST_INT ORBITAL_CANNON_LOCAL_BS_ANIM_RUNNING				17
CONST_INT ORBITAL_CANNON_LOCAL_BS_QUIT_INITIAL_MENU_WARNING	18
CONST_INT ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE			19

/// PURPOSE: Orbital Cannon scaleform bitset
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL			0
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_INACTIVE				1
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0			2
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1			3
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2			4
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3			5
CONST_INT ORBITAL_CANNON_SCALEFORM_BS_STATE_SET				6

/// PURPOSE: Orbital Cannon switch bitset
CONST_INT ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED		0
CONST_INT ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING		1
CONST_INT ORBITAL_CANNON_SWITCH_BS_ACTIVATING				2
CONST_INT ORBITAL_CANNON_SWITCH_BS_DEACTIVATING				3

/// PURPOSE: Orbital Cannon control bitset
CONST_INT ORBITAL_CANNON_CONTROL_BS_SET_WEATHER				0
CONST_INT ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS				1
CONST_INT ORBITAL_CANNON_CONTROL_BS_SET_RADAR				2
CONST_INT ORBITAL_CANNON_CONTROL_BS_SET_CONTROLS			3

CONST_INT ORBITAL_CANNON_PURCHASE_BS_MANUAL					0
CONST_INT ORBITAL_CANNON_PURCHASE_BS_AUTO					1
CONST_INT ORBITAL_CANNON_PURCHASE_BS_BOTH					2

ENUM ORBITAL_CANNON_TRANSACTION_STATE
	ORBITAL_CANNON_TRANSACTION_STATE_MENU,
	ORBITAL_CANNON_TRANSACTION_STATE_PENDING,
	ORBITAL_CANNON_TRANSACTION_STATE_SUCCESS,
	ORBITAL_CANNON_TRANSACTION_STATE_FAILED
ENDENUM

/// PURPOSE: Stages of interacting with the Orbital Cannon
ENUM ORBITAL_CANNON_CLIENT_STAGE
	ORBITAL_CANNON_CLIENT_STAGE_INIT,
	ORBITAL_CANNON_CLIENT_STAGE_WALKING,
	ORBITAL_CANNON_CLIENT_STAGE_ANIM,
	ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED
ENDENUM

/// PURPOSE: Stages of using the Orbital Cannon
ENUM ORBITAL_CANNON_WEAPON_STAGE
	ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU,
	ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU,
	ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM,
	ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET,
	ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
ENDENUM

/// PURPOSE: States to manage activating/deactivating the Orbital Cannon
ENUM ORBITAL_CANNON_SWITCH_STATE
	ORBITAL_CANNON_SWITCH_STATE_FADE_OUT,
	ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FADE_OUT,
	ORBITAL_CANNON_SWITCH_STATE_CLEANUP_CURRENT,
	ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED,
	ORBITAL_CANNON_SWITCH_STATE_SETUP_LOAD_SCENE,
	ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_LOAD_SCENE,
	ORBITAL_CANNON_SWITCH_STATE_CLEANUP_LOAD_SCENE,
	ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FILTER_TIME,
	ORBITAL_CANNON_SWITCH_STATE_FADE_IN,
	ORBITAL_CANNON_SWITCH_STATE_CLEANUP_PROCESS,
	ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
ENDENUM

/// PURPOSE: Menu input control values
STRUCT MENU_INPUT
	BOOL bAccept
	BOOL bBack
	BOOL bPrevious
	BOOL bNext
	
	INT iLeftX
	INT iLeftY
	INT iRightX
	INT iRightY
	INT iMoveUp
	INT iCursorValue
ENDSTRUCT

/// PURPOSE: Orbital Cannon local data
STRUCT ORBITAL_CANNON_LOCAL_DATA_STRUCT
	BOOL bFired = FALSE
	BOOL bRefund = FALSE
	BOOL bActivated = FALSE
	BOOL bAvailable = TRUE
	BOOL bRebuildMenu = FALSE
	BOOL bBlockFiring = FALSE
	BOOL bFiringOnVehicle = FALSE
	BOOL bAudioBankLoaded = FALSE
	BOOL bReaquiringTarget = FALSE
	BOOL bConfirmMenuOption = FALSE
	BOOL bDefaultCameraMovementLastFrame = FALSE
	
	CAMERA_INDEX camOrbitalCannon
	
	INT iBitset = 0
	INT iSoundID = -1
	INT iSceneID = -1
	INT iControlBS = 0
	INT iZoomLevel = 0
	INT iPurchaseBS = 0
	INT iKillCounter = 0
	INT iMenuStagger = 0
	INT iScaleformBS = 0
	INT iPlayerCount = 0
	INT iSwitchBitset = 0
	INT iCamSwitchTimer = 0
	INT iMenuCurrentItem = 0
	INT iSoundPanSoundID = -1
	INT iChargingSoundID = -1
	INT iTransactionFlags = 0
	INT iBackgroundSoundID = -1
	INT iTransactionAmount = 0
	INT iCurrentTransaction = 0
	INT iSoundZoomInSoundID = -1
	INT iSoundZoomOutSoundID = -1
	INT iCurrentScaleformState = 0
	INT iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
	
	MENU_INPUT sMenuInput
	
	ORBITAL_CANNON_SWITCH_STATE eSwitchState = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
	
	ORBITAL_CANNON_TRANSACTION_STATE eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_MENU
	
	ORBITAL_CANNON_WEAPON_STAGE eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	ORBITAL_CANNON_WEAPON_STAGE eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	
	PLAYER_INDEX pTargetPlayers[NUM_NETWORK_PLAYERS]
	
	SCALEFORM_INDEX sfButton
	SCALEFORM_INDEX sfOrbitalCannon
	
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformInstructionalButtons
	
	SCRIPT_TIMER sFeedTimer
	SCRIPT_TIMER sFiringTimer
	SCRIPT_TIMER sCooldownTimer
	SCRIPT_TIMER sSwitchDelay
	SCRIPT_TIMER sInitialDelay
	SCRIPT_TIMER sAudioBankTimer
	SCRIPT_TIMER sKillTracker
	SCRIPT_TIMER sSyncDelay
	
	TIME_DATATYPE scrollDelay
	
	TRANSACTION_SERVICES eTransactionService
	
	VECTOR vCurrentCamPos
	VECTOR vLoadSceneLocation
	VECTOR vLoadSceneRot
	
	VEHICLE_INDEX vTarget
ENDSTRUCT

/// PURPOSE: Orbital Cannon client data
STRUCT ORBITAL_CANNON_CLIENT_DATA_STRUCT
	ORBITAL_CANNON_CLIENT_STAGE eStage = ORBITAL_CANNON_CLIENT_STAGE_INIT
ENDSTRUCT

/// PURPOSE: Orbital Cannon server data
STRUCT ORBITAL_CANNON_SERVER_DATA_STRUCT
	BOOL bFired = FALSE
	BOOL bFiring = FALSE
	BOOL bJustFired = FALSE
	BOOL bReadyToBeObserved = FALSE
	
	INT iZoomLevel = 0
	
	ORBITAL_CANNON_WEAPON_STAGE eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	
	PLAYER_INDEX pUser
	PLAYER_INDEX pOwner
	PLAYER_INDEX pTargetPlayer
	
	VECTOR vCamPos
ENDSTRUCT

/// PURPOSE: Stores the Orbital Cannon camera Position, Rotation and FOV
STRUCT ORBITAL_CANNON_CAMERA_DETAILS
	FLOAT fFOV
	
	VECTOR vPos
	VECTOR vRot
ENDSTRUCT

FUNC BOOL DOES_PLAYER_HAVE_FREE_SHOT()
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_XMAS2017_CANNON_GIFT)
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PURCHASE_XMAS2017_CANNON_GIFT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Initializes the Orbital Cannon server data
/// PARAMS:
///    OrbitalCannonServer - Server data struct
PROC SERVER_INIT_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, PLAYER_INDEX pOwner)
	PRINTLN("[ORBITAL_CANNON] SERVER_INIT_ORBITAL_CANNON")
	
	OrbitalCannonServer.pUser = INVALID_PLAYER_INDEX()
	OrbitalCannonServer.pOwner = pOwner
ENDPROC

/// PURPOSE:
///    Checks if the local player is controlling the Orbital Cannon or just observing
/// PARAMS:
///    OrbitalCannonServer - Server data struct
/// RETURNS:
///    TRUE if the local player is the one controlling the Orbital Cannon
FUNC BOOL IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer)
	IF PLAYER_ID() = OrbitalCannonServer.pUser
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets new Orbital Cannon switch state
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
///    eState - New state to switch to
PROC SET_ORBITAL_CANNON_SWITCH_STATE(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE eState)
	PRINTLN("[ORBITAL_CANNON] SET_ORBITAL_CANNON_SWITCH_STATE - ", ENUM_TO_INT(eState))
	
	OrbitalCannonLocal.eSwitchState = eState
ENDPROC

/// PURPOSE:
///    Gets the current Orbital Cannon switch state
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
/// RETURNS:
///    ORBITAL_CANNON_SWITCH_STATE - Current Orbital Cannon switch state
FUNC ORBITAL_CANNON_SWITCH_STATE GET_ORBITAL_CANNON_SWITCH_STATE(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	RETURN OrbitalCannonLocal.eSwitchState
ENDFUNC

/// PURPOSE:
///    Sets new Orbital Cannon client stage
/// PARAMS:
///    OrbitalCannonClient - Client data struct
///    eStage - New stage to switch to
PROC SET_ORBITAL_CANNON_CLIENT_STAGE(ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE eStage)
	PRINTLN("[ORBITAL_CANNON] SET_ORBITAL_CANNON_CLIENT_STAGE - ", ENUM_TO_INT(eStage))
	
	OrbitalCannonClient.eStage = eStage
ENDPROC

PROC SET_FRIENDLY_FIRE(BOOL bSet)
	PLAYER_INDEX playerID
	
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		playerID = INT_TO_PLAYERINDEX(iPlayer)
		
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
		AND playerID != PLAYER_ID()
			IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(playerID, GB_GET_LOCAL_PLAYER_GANG_BOSS())
			OR GB_IS_PLAYER_ALLIED_WITH_THIS_PLAYER(playerID, PLAYER_ID())
				SET_PLAYER_CAN_DAMAGE_PLAYER(PLAYER_ID(), playerID, bSet)
				SET_PLAYER_CAN_DAMAGE_PLAYER(playerID, PLAYER_ID(), bSet)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Sets up activating/deactivating the Orbital Cannon
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC CLIENT_SETUP_SWITCH(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	PRINTLN("[ORBITAL_CANNON] CLIENT_SETUP_SWITCH")
	
	OrbitalCannonLocal.iCamSwitchTimer = 0
	
	OrbitalCannonLocal.bDefaultCameraMovementLastFrame = FALSE
	
	SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_FADE_OUT)
	
	CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
	CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
	
	SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
ENDPROC

PROC START_ORBITAL_CANNON_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundID = -1
		OrbitalCannonLocal.iSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundID, "cannon_charge_fire_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_ORBITAL_CANNON_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundID)
		OrbitalCannonLocal.iSoundID = -1
	ENDIF
ENDPROC

PROC START_ORBITAL_CANNON_BACKGROUND_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iBackgroundSoundID = -1
		OrbitalCannonLocal.iBackgroundSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iBackgroundSoundID, "background_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_ORBITAL_CANNON_BACKGROUND_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iBackgroundSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iBackgroundSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iBackgroundSoundID)
		OrbitalCannonLocal.iBackgroundSoundID = -1
	ENDIF
ENDPROC

PROC START_ORBITAL_CANNON_CHARGING_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iChargingSoundID = -1
		OrbitalCannonLocal.iChargingSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iChargingSoundID, "cannon_activating_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_ORBITAL_CANNON_CHARGING_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iChargingSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iChargingSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iChargingSoundID)
		OrbitalCannonLocal.iChargingSoundID = -1
	ENDIF
ENDPROC

PROC START_ORBITAL_CANNON_PAN_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundPanSoundID = -1
		OrbitalCannonLocal.iSoundPanSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundPanSoundID, "pan_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_ORBITAL_CANNON_PAN_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundPanSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundPanSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundPanSoundID)
		OrbitalCannonLocal.iSoundPanSoundID = -1
	ENDIF
ENDPROC

PROC START_ORBITAL_CANNON_ZOOM_IN_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomInSoundID = -1
		OrbitalCannonLocal.iSoundZoomInSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundZoomInSoundID, "zoom_out_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomInSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundZoomInSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundZoomInSoundID)
		OrbitalCannonLocal.iSoundZoomInSoundID = -1
	ENDIF
ENDPROC

PROC START_ORBITAL_CANNON_ZOOM_OUT_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomOutSoundID = -1
		OrbitalCannonLocal.iSoundZoomOutSoundID = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(OrbitalCannonLocal.iSoundZoomOutSoundID, "zoom_out_loop", "dlc_xm_orbital_cannon_sounds", TRUE)
	ENDIF
ENDPROC

PROC STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF OrbitalCannonLocal.iSoundZoomOutSoundID != -1
		STOP_SOUND(OrbitalCannonLocal.iSoundZoomOutSoundID)
		RELEASE_SOUND_ID(OrbitalCannonLocal.iSoundZoomOutSoundID)
		OrbitalCannonLocal.iSoundZoomOutSoundID = -1
	ENDIF
ENDPROC

FUNC INT GET_ORBITAL_CANNON_MANUAL_PRICE()
	IF g_sMPTunables.bOrbitalCannonManualSale
		RETURN g_sMPTunables.IH2_ORBITAL_CANNON_MANUAL_SALE_PRICE
	ENDIF
	
	RETURN g_sMPTunables.IH2_ORBITAL_CANNON_MANUAL_PRICE
ENDFUNC

FUNC INT GET_ORBITAL_CANNON_AUTOMATIC_PRICE(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bBypassSale = FALSE)
	IF g_sMPTunables.bOrbitalCannonAutomaticSale
	AND NOT bBypassSale
		IF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
			RETURN 0
		ELIF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
			RETURN g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_SALE_PRICE - GET_ORBITAL_CANNON_MANUAL_PRICE()
		ELSE
			RETURN g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_SALE_PRICE
		ENDIF
	ELSE
		IF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
			RETURN 0
		ELIF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
			RETURN g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_PRICE - GET_ORBITAL_CANNON_MANUAL_PRICE()
		ELSE
			RETURN g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_PRICE
		ENDIF
	ENDIF
	
	RETURN g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_PRICE
ENDFUNC

/// PURPOSE:
///    Non-full clean up for the Orbital Cannon
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
///    bReturnControl - Flag to return player control
PROC TIDYUP_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bReturnControl, BOOL bProcessRefund)
	PRINTLN("[ORBITAL_CANNON] TIDYUP_ORBITAL_CANNON")
	
	IF NETWORK_IS_SIGNED_ONLINE()
		IF bProcessRefund
			IF NOT DOES_PLAYER_HAVE_FREE_SHOT()
				IF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
					INT iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_PRICE
					
					IF g_sMPTunables.bOrbitalCannonAutomaticSale
						iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_SALE_PRICE
					ENDIF
					
					IF USE_SERVER_TRANSACTIONS()
						NETWORK_REFUND_CASH_TYPE(iRefundPrice, MP_REFUND_TYPE_ORBITAL_AUTO, MP_REFUND_REASON_NOT_USED, TRUE)
					ELSE
						NETWORK_EARN_TARGET_REFUND(iRefundPrice, 1)
					ENDIF
				ELIF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
					IF USE_SERVER_TRANSACTIONS()
						NETWORK_REFUND_CASH_TYPE(GET_ORBITAL_CANNON_MANUAL_PRICE(), MP_REFUND_TYPE_ORBITAL_MANUAL, MP_REFUND_REASON_NOT_USED, TRUE)
					ELSE
						NETWORK_EARN_TARGET_REFUND(GET_ORBITAL_CANNON_MANUAL_PRICE(), 0)
					ENDIF
				ENDIF
			ENDIF
			
			g_iOrbitalCannonRefundBS = 0
		ELSE
			PRINTLN("[ORBITAL_CANNON] TIDYUP_ORBITAL_CANNON - Skipping refund")
		ENDIF
	ELSE
		g_iOrbitalCannonRefundBS = 0
		
		PRINTLN("[ORBITAL_CANNON] TIDYUP_ORBITAL_CANNON - Not connected, resetting g_iOrbitalCannonRefundBS")
	ENDIF
	
	CLEANUP_MENU_ASSETS()
	
	IF NOT GB_IS_BOSS_FF_TOGGLED_ON()
		SET_FRIENDLY_FIRE(FALSE)
	ENDIF
	
	CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("helicopterhud")
	
	STOP_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
	STOP_ORBITAL_CANNON_BACKGROUND_SOUND(OrbitalCannonLocal)
	STOP_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
	STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
	STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
	STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
	
	STOP_AUDIO_SCENE("dlc_xm_orbital_cannon_camera_active_scene")
	
	RESET_NET_TIMER(OrbitalCannonLocal.sAudioBankTimer)
	
	IF ANIMPOSTFX_IS_RUNNING("MP_OrbitalCannon")
		ANIMPOSTFX_STOP("MP_OrbitalCannon")
	ENDIF
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
	ENDIF
	
	IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		OrbitalCannonServer.bReadyToBeObserved = FALSE
		OrbitalCannonServer.bFired = FALSE
		OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
		OrbitalCannonServer.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	ELSE
		OrbitalCannonLocal.bBlockFiring = FALSE
	ENDIF
	
	g_SimpleInteriorData.bSuppressConcealCheck = FALSE
	FORCE_SIMPLE_INTERIOR_CONCEAL_PLAYERS_CHECK()
	
	OrbitalCannonLocal.bFired = FALSE
	OrbitalCannonLocal.bActivated = FALSE
	OrbitalCannonLocal.bRebuildMenu = FALSE
	OrbitalCannonLocal.iScaleformBS = 0
	OrbitalCannonLocal.iPurchaseBS = 0
	OrbitalCannonLocal.iMenuCurrentItem = 0
	OrbitalCannonLocal.iCurrentScaleformState = 0
	OrbitalCannonLocal.bConfirmMenuOption = FALSE
	OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_MENU
	OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
	CLEAR_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_GO_TO_TASK)
	CLEAR_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_ANIM_RUNNING)
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
	
	RESET_NET_TIMER(OrbitalCannonLocal.sSwitchDelay)
	
	ENABLE_WEAPON_SWAP()
	Enable_MP_Comms()
	
	IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
		CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
	ENDIF
	
	IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
		CLEAR_OVERRIDE_WEATHER()
	ENDIF
	
	IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_RADAR)
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	
	IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		DESTROY_CAM(OrbitalCannonLocal.camOrbitalCannon)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(OrbitalCannonLocal.sfOrbitalCannon)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
	ENDIF
	
	IF bReturnControl
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_CONTROLS)
			PRINTLN("[ORBITAL_CANNON] TIDYUP_ORBITAL_CANNON - Returning player control")
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_VISIBILITY_CHANGES)
		ENDIF
	ENDIF
	
	OrbitalCannonLocal.iControlBS = 0
ENDPROC

/// PURPOSE:
///    Fully cleans up the Orbital Cannon client
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonClient - Client data struct
///    OrbitalCannonLocal - Local data struct
///    bReturnControl - Flag to return player control
PROC CLEANUP_ORBITAL_CANNON_CLIENT(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bReturnControl, BOOL bProcessRefund)
	PRINTLN("[ORBITAL_CANNON] CLEANUP_ORBITAL_CANNON_CLIENT")
	
	TIDYUP_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, bReturnControl, bProcessRefund)
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
		PRINTLN("[ORBITAL_CANNON] CLEANUP_ORBITAL_CANNON_CLIENT - Enabling kill yourself option")
		
		ENABLE_KILL_YOURSELF_OPTION()
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
	ENDIF
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
		PRINTLN("[ORBITAL_CANNON] CLEANUP_ORBITAL_CANNON_CLIENT - Enabling interaction menu")
		
		ENABLE_INTERACTION_MENU()
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
	ENDIF
	
	IF OrbitalCannonLocal.iOrbitalCannonContextIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
	ENDIF
	
	SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_USING_CANNON)
	
	OrbitalCannonLocal.iBitset = 0
	OrbitalCannonLocal.iSwitchBitset = 0
	
	SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_INIT)
ENDPROC

/// PURPOSE:
///    Gets the heading for offscreen target arrow
/// PARAMS:
///    vOldCoords - Local players coords
///    vNewCoords - Target players coords
/// RETURNS:
///    fHeading - Heading of target arrow
FUNC FLOAT GET_HUD_HEADING_FROM_COORDS(VECTOR vOldCoords, VECTOR vNewCoords)
	FLOAT fHeading
	FLOAT fX = vNewCoords.x - vOldCoords.x
	FLOAT fY = vNewCoords.y - vOldCoords.y
	
	IF fY != 0
		fHeading = ATAN2(fX, fY)
	ELSE
		IF fX < 0.0
			fHeading = -90.0
		ELSE
			fHeading = 90.0
		ENDIF
	ENDIF
	
	RETURN fHeading
ENDFUNC

/// PURPOSE:
///    Gets the hud element alpha, based on distance from the local player
/// PARAMS:
///    fDistance - Distance away from local player
/// RETURNS:
///    iAlpha - Target alpha value
FUNC INT GET_TARGET_ALPHA(FLOAT fDistance)
	INT iAlpha = 255
	
	// Override for Missions.
	FLOAT fDistanceMax = 3500.0	
	IF g_bMissionPlacedOrbitalCannon
		fDistanceMax = 10000.0
	ENDIF
	
	IF fDistance >= fDistanceMax
		iAlpha = 0
	ELIF fDistance <= 1000.0
		iAlpha = 255
	ELSE
		FLOAT fPercentage
		
		fPercentage = 1.0 - ((fDistance - 1000.0) / fDistanceMax)
		
		iAlpha = ROUND(iAlpha * fPercentage)
	ENDIF
	
	RETURN iAlpha
ENDFUNC

FUNC STRING GET_LETTER_SPRITE_FROM_NUMBER(INT i)
	SWITCH i
		CASE 1 		RETURN "orb_target_a"
		CASE 2 		RETURN "orb_target_b"
		CASE 3 		RETURN "orb_target_c"
		CASE 4 		RETURN "orb_target_d"
		CASE 5		RETURN "orb_target_e"
		CASE 6 		RETURN "orb_target_f"
		CASE 7		RETURN "orb_target_g"
	ENDSWITCH
	
	RETURN "invalid"
ENDFUNC

/// PURPOSE:
///    Draws arrow to point to ped offscreen
/// PARAMS:
///    pedPlayer - Specified target ped
PROC DRAW_OFFSCREEN_ARROW_FOR_COORD(PED_INDEX pedPlayer)
	FLOAT fpos
	
	VECTOR vPedPos = GET_ENTITY_COORDS(pedPlayer, FALSE)
	
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPedPos, fpos, fpos)
			INT r, g, b, a
			
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
			
			VECTOR vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
			VECTOR vCamRot = <<-90.0, 0.0, 0.0>>
			
			FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(<<vCamCoord.x, vCamCoord.y, 0.0>>, <<vPedPos.x, vPedPos.y, 0.0>>)
			FLOAT fHeight = vPedPos.z - vCamCoord.z
			FLOAT fPitchToPed
			
			IF fDist > 0
				fPitchToPed = ATAN(fHeight / fDist)
			ELSE
				fPitchToPed = 0
			ENDIF
			
			FLOAT fHeadingToPed = GET_HUD_HEADING_FROM_COORDS(vCamCoord, vPedPos)
			FLOAT fAngleToPoint = ATAN2(COS(vCamRot.x) * SIN(fPitchToPed) - SIN(vCamRot.x) * COS(fPitchToPed) * COS((fHeadingToPed * -1) - vCamRot.z), SIN((fHeadingToPed * -1) - vCamRot.z) * COS(fPitchToPed))
			FLOAT fX = 0.5 - (cos(fAngleToPoint) * 0.19)
			FLOAT fY = 0.5 - (sin(fAngleToPoint) * 0.19)
			
			FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vPedPos, vCamCoord, FALSE)
			
			DRAW_SPRITE("helicopterhud", "hudArrow", fX, fY, 0.02, 0.04, fAngleToPoint - 90.0, r, g, b, GET_TARGET_ALPHA(fDistance), TRUE)
			
			SET_TEXT_CENTRE(TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws arrow to point to ped offscreen
/// PARAMS:
///    pedPlayer - Specified target ped
PROC DRAW_OFFSCREEN_ARROW_FOR_COORD_VEHICLE(VEHICLE_INDEX vehIndex)
	FLOAT fpos
	
	VECTOR vVehPos = GET_ENTITY_COORDS(vehIndex, FALSE)
	
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vVehPos, fpos, fpos)
			INT r, g, b, a
			
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
			
			VECTOR vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
			VECTOR vCamRot = <<-90.0, 0.0, 0.0>>
			
			FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(<<vCamCoord.x, vCamCoord.y, 0.0>>, <<vVehPos.x, vVehPos.y, 0.0>>)
			FLOAT fHeight = vVehPos.z - vCamCoord.z
			FLOAT fPitchToPed
			
			IF fDist > 0
				fPitchToPed = ATAN(fHeight / fDist)
			ELSE
				fPitchToPed = 0
			ENDIF
			
			FLOAT fHeadingToPed = GET_HUD_HEADING_FROM_COORDS(vCamCoord, vVehPos)
			FLOAT fAngleToPoint = ATAN2(COS(vCamRot.x) * SIN(fPitchToPed) - SIN(vCamRot.x) * COS(fPitchToPed) * COS((fHeadingToPed * -1) - vCamRot.z), SIN((fHeadingToPed * -1) - vCamRot.z) * COS(fPitchToPed))
			FLOAT fX = 0.5 - (cos(fAngleToPoint) * 0.19)
			FLOAT fY = 0.5 - (sin(fAngleToPoint) * 0.19)
			
			FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vVehPos, vCamCoord, FALSE)
			
			DRAW_SPRITE("helicopterhud", "hudArrow", fX, fY, 0.02, 0.04, fAngleToPoint - 90.0, r, g, b, GET_TARGET_ALPHA(fDistance), TRUE)
						
			SET_TEXT_CENTRE(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_TARGET_LETTER_FOR_COORD(PED_INDEX pedPlayer, INT iLetter)
	FLOAT  fBoxDrawSize
	
	INT r, g, b, a
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	SET_DRAW_ORIGIN(GET_ENTITY_COORDS(pedPlayer), FALSE)
	
	fBoxDrawSize = 0.015
	
	GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
	
	DRAW_SPRITE("helicopterhud", GET_LETTER_SPRITE_FROM_NUMBER(iLetter), -fBoxDrawSize * 0.9, -fBoxDrawSize * 1.35, fBoxDrawSize * 1.5, fBoxDrawSize * 2.75, 0, r, g, b, a, TRUE)
	
	CLEAR_DRAW_ORIGIN()
	
	FLOAT fpos
	
	VECTOR vPedPos = GET_ENTITY_COORDS(pedPlayer, FALSE)
	
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vPedPos, fpos, fpos)
			
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
			
			VECTOR vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
			VECTOR vCamRot = <<-90.0, 0.0, 0.0>>
			
			FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(<<vCamCoord.x, vCamCoord.y, 0.0>>, <<vPedPos.x, vPedPos.y, 0.0>>)
			FLOAT fHeight = vPedPos.z - vCamCoord.z
			FLOAT fPitchToPed
			
			IF fDist > 0
				fPitchToPed = ATAN(fHeight / fDist)
			ELSE
				fPitchToPed = 0
			ENDIF
			
			FLOAT fHeadingToPed = GET_HUD_HEADING_FROM_COORDS(vCamCoord, vPedPos)
			FLOAT fAngleToPoint = ATAN2(COS(vCamRot.x) * SIN(fPitchToPed) - SIN(vCamRot.x) * COS(fPitchToPed) * COS((fHeadingToPed * -1) - vCamRot.z), SIN((fHeadingToPed * -1) - vCamRot.z) * COS(fPitchToPed))
			FLOAT fX = 0.5 - (cos(fAngleToPoint) * 0.165)
			FLOAT fY = 0.5 - (sin(fAngleToPoint) * 0.15)
			
			FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vPedPos, vCamCoord, FALSE)
			
			DRAW_SPRITE("helicopterhud", GET_LETTER_SPRITE_FROM_NUMBER(iLetter), fX, fY, 0.02, 0.04, 0, r, g, b, GET_TARGET_ALPHA(fDistance), TRUE)
			
			SET_TEXT_CENTRE(TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_TARGET_LETTER_FOR_COORD_VEHICLE(VEHICLE_INDEX vehIndex, INT iLetter)
	FLOAT  fBoxDrawSize
	
	INT r, g, b, a
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	SET_DRAW_ORIGIN(GET_ENTITY_COORDS(vehIndex), FALSE)
	
	fBoxDrawSize = 0.017
	
	GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
	
	DRAW_SPRITE("helicopterhud", GET_LETTER_SPRITE_FROM_NUMBER(iLetter), -fBoxDrawSize * 0.9, -fBoxDrawSize * 1.35, fBoxDrawSize * 1.5, fBoxDrawSize * 2.75, 0, r, g, b, a, TRUE)
	
	CLEAR_DRAW_ORIGIN()
	
	FLOAT fpos
	VECTOR vVehPos = GET_ENTITY_COORDS(vehIndex, FALSE)
	
	IF DOES_CAM_EXIST(GET_RENDERING_CAM())
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vVehPos, fpos, fpos)
			
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, r, g, b, a)
			
			VECTOR vCamCoord = GET_CAM_COORD(GET_RENDERING_CAM())
			VECTOR vCamRot = <<-90.0, 0.0, 0.0>>
			
			FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(<<vCamCoord.x, vCamCoord.y, 0.0>>, <<vVehPos.x, vVehPos.y, 0.0>>)
			FLOAT fHeight = vVehPos.z - vCamCoord.z
			FLOAT fPitchToPed
			
			IF fDist > 0
				fPitchToPed = ATAN(fHeight / fDist)
			ELSE
				fPitchToPed = 0
			ENDIF
			
			FLOAT fHeadingToPed = GET_HUD_HEADING_FROM_COORDS(vCamCoord, vVehPos)
			FLOAT fAngleToPoint = ATAN2(COS(vCamRot.x) * SIN(fPitchToPed) - SIN(vCamRot.x) * COS(fPitchToPed) * COS((fHeadingToPed * -1) - vCamRot.z), SIN((fHeadingToPed * -1) - vCamRot.z) * COS(fPitchToPed))
			FLOAT fX = 0.5 - (cos(fAngleToPoint) * 0.165)
			FLOAT fY = 0.5 - (sin(fAngleToPoint) * 0.15)
			
			FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vVehPos, vCamCoord, FALSE)
			
			DRAW_SPRITE("helicopterhud", GET_LETTER_SPRITE_FROM_NUMBER(iLetter), fX, fY, 0.02, 0.04, 0, r, g, b, GET_TARGET_ALPHA(fDistance), TRUE)
						
			SET_TEXT_CENTRE(TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Draws box around specified ped
/// PARAMS:
///    pedPlayer - Specified target ped
PROC DRAW_BOX_AROUND_TARGET(PED_INDEX pedPlayer, BOOL bHasLetter = FALSE, HUD_COLOURS hudColour = HUD_COLOUR_GREEN)
	FLOAT  fBoxDrawSize
	
	INT r, g, b, a
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	SET_DRAW_ORIGIN(GET_ENTITY_COORDS(pedPlayer), FALSE)
	
	fBoxDrawSize = 0.015
	
	GET_HUD_COLOUR(hudColour, r, g, b, a)
	
	IF NOT bHasLetter
		DRAW_SPRITE("helicopterhud", "hud_corner", -fBoxDrawSize * 0.6, -fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 0, r, g, b, a, TRUE)
	ENDIF
	DRAW_SPRITE("helicopterhud", "hud_corner", fBoxDrawSize * 0.6, -fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 90, r, g, b, a, TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner", -fBoxDrawSize * 0.6, fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 270, r, g, b, a, TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner", fBoxDrawSize * 0.6, fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 180, r, g, b, a, TRUE)
	
	CLEAR_DRAW_ORIGIN()
ENDPROC

/// PURPOSE:
///    Draws box around specified ped
/// PARAMS:
///    pedPlayer - Specified target ped
PROC DRAW_BOX_AROUND_TARGET_VEHICLE(VEHICLE_INDEX vehIndex, BOOL bHasLetter = FALSE, HUD_COLOURS hudColour = HUD_COLOUR_GREEN)
	FLOAT  fBoxDrawSize
	
	INT r, g, b, a
	
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	SET_DRAW_ORIGIN(GET_ENTITY_COORDS(vehIndex), FALSE)
	
	fBoxDrawSize = 0.017
	
	GET_HUD_COLOUR(hudColour, r, g, b, a)
	
	IF NOT bHasLetter
		DRAW_SPRITE("helicopterhud", "hud_corner", -fBoxDrawSize * 0.6, -fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 0, r, g, b, a, TRUE)
	ENDIF
	DRAW_SPRITE("helicopterhud", "hud_corner", fBoxDrawSize * 0.6, -fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 90, r, g, b, a, TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner", -fBoxDrawSize * 0.6, fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 270, r, g, b, a, TRUE)
	DRAW_SPRITE("helicopterhud", "hud_corner", fBoxDrawSize * 0.6, fBoxDrawSize, fBoxDrawSize * 1.5, fBoxDrawSize * 1.5, 180, r, g, b, a, TRUE)
	
	CLEAR_DRAW_ORIGIN()
ENDPROC

FUNC BOOL IS_LOCAL_PLAYER_ORBITAL_CANNON_ANGLED_AREA(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer)
	IF OrbitalCannonServer.pOwner = PLAYER_ID()
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<330.898621,4829.519531,-60.401714>>, <<330.059662,4830.686035,-58.401714>>, 2.5)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<329.805420,4826.680664,-60.401714>>, <<330.830322,4827.978516,-58.401714>>, 2.5)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.721771,4826.797363,-60.401714>>, <<328.319092,4826.187012,-58.401714>>, 2.5)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.145538,4829.780762,-60.401714>>, <<326.071350,4828.163086,-58.401714>>, 2.5)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<328.755920,4831.455078,-60.401714>>, <<327.050903,4830.950195,-58.401714>>, 2.5)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.376831,4832.011719,-60.401714>>, <<330.323883,4826.256348,-58.401714>>, 5.5)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_ORBITAL_CANNON_GO_TO_LOCATION(BOOL bScene = FALSE)
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<330.898621,4829.519531,-60.401714>>, <<330.059662,4830.686035,-58.401714>>, 2.5)
		IF bScene
			RETURN <<328.4866, 4828.8472, -59.5579>>
		ELSE
			RETURN <<331.0342, 4830.4980, -59.3721>>
		ENDIF
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<329.805420,4826.680664,-60.401714>>, <<330.830322,4827.978516,-58.401714>>, 2.5)
		IF bScene
			RETURN <<328.4866, 4828.8472, -59.5579>>
		ELSE
			RETURN <<330.8769, 4826.9761, -59.3721>>
		ENDIF
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.721771,4826.797363,-60.401714>>, <<328.319092,4826.187012,-58.401714>>, 2.5)
		IF bScene
			RETURN <<328.4866, 4828.8472, -59.5579>>
		ELSE
			RETURN <<327.3960, 4826.0142, -59.3721>>
		ENDIF
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.145538,4829.780762,-60.401714>>, <<326.071350,4828.163086,-58.401714>>, 2.5)
		IF bScene
			RETURN <<328.4866, 4828.8472, -59.5579>>
		ELSE
			RETURN <<325.4554, 4829.0093, -59.3721>>
		ENDIF
	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<328.755920,4831.455078,-60.401714>>, <<327.050903,4830.950195,-58.401714>>, 2.5)
		IF bScene
			RETURN <<328.4866, 4828.8472, -59.5579>>
		ELSE
			RETURN <<327.6529, 4831.7661, -59.3721>>
		ENDIF
	ENDIF
	
	IF bScene
		RETURN <<328.4866, 4828.8472, -59.5579>>
	ENDIF
	
	RETURN <<331.0342, 4830.4980, -59.3721>>
ENDFUNC

FUNC FLOAT GET_ORBITAL_CANNON_GO_TO_HEADING(BOOL bScene = FALSE)
	IF NOT bScene
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<330.898621,4829.519531,-60.401714>>, <<330.059662,4830.686035,-58.401714>>, 2.5)
			RETURN 124.95
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<329.805420,4826.680664,-60.401714>>, <<330.830322,4827.978516,-58.401714>>, 2.5)
			RETURN 53.95
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.721771,4826.797363,-60.401714>>, <<328.319092,4826.187012,-58.401714>>, 2.5)
			RETURN -19.05
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.145538,4829.780762,-60.401714>>, <<326.071350,4828.163086,-58.401714>>, 2.5)
			RETURN -91.05
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<328.755920,4831.455078,-60.401714>>, <<327.050903,4830.950195,-58.401714>>, 2.5)
			RETURN -162.05
		ENDIF
	ELSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<330.898621,4829.519531,-60.401714>>, <<330.059662,4830.686035,-58.401714>>, 2.5)
			RETURN 159.0
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<329.805420,4826.680664,-60.401714>>, <<330.830322,4827.978516,-58.401714>>, 2.5)
			RETURN 88.0
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.721771,4826.797363,-60.401714>>, <<328.319092,4826.187012,-58.401714>>, 2.5)
			RETURN 15.0
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<326.145538,4829.780762,-60.401714>>, <<326.071350,4828.163086,-58.401714>>, 2.5)
			RETURN -57.0
		ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<328.755920,4831.455078,-60.401714>>, <<327.050903,4830.950195,-58.401714>>, 2.5)
			RETURN -128.0
		ENDIF
	ENDIF
	
	RETURN 122.0
ENDFUNC

PROC START_ORBITAL_CANNON_SYNC_SCENE(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, STRING sAnim, BOOL bLoopAnim = FALSE)
	VECTOR vAAScenePos = GET_ORBITAL_CANNON_GO_TO_LOCATION(TRUE)
	VECTOR vAASceneRot = <<0.0, 0.0, GET_ORBITAL_CANNON_GO_TO_HEADING(TRUE)>>
	
	OrbitalCannonLocal.iSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vAAScenePos, vAASceneRot, DEFAULT, DEFAULT, bLoopAnim)
										
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), OrbitalCannonLocal.iSceneID, "anim@AMB@FACILITY@MISSLE_CONTROLROOM@", sAnim, REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT)
	
	NETWORK_START_SYNCHRONISED_SCENE(OrbitalCannonLocal.iSceneID)
ENDPROC

/// PURPOSE:
///    Prints context intention for the Orbital Cannon and maintains the activation
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
/// RETURNS:
///    TRUE when the local player activates the Orbital Cannon
FUNC BOOL CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	BOOL bReadyForContext = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
	AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
	AND NOT IS_PED_WALKING(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND (OrbitalCannonServer.pOwner = PLAYER_ID()
	OR OrbitalCannonServer.pUser != INVALID_PLAYER_INDEX())
	AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(OrbitalCannonLocal.iSceneID))
	AND NOT g_bDisableOrbitalCannon
	AND NOT g_bDisableOrbitalCannonBG
	AND STAT_IS_STATS_TRACKING_ENABLED()
		IF IS_LOCAL_PLAYER_ORBITAL_CANNON_ANGLED_AREA(OrbitalCannonServer)
		AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(PLAYER_PED_ID(), <<328.6464, 4828.9414, -59.5541>>, 50.0)
		AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
			#IF IS_DEBUG_BUILD
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - bReadyForContext - TRUE")
			#ENDIF
			
			bReadyForContext = TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - IS_NET_PLAYER_OK")
		ENDIF
		
		IF IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - biKY_AnimStarted")
		ENDIF
		
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - GLOBAL_SPEC_BS_USING_MP_RADIO")
		ENDIF
		
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - IS_PLAYER_CONTROL_ON")
		ENDIF
		
		IF IS_ANY_INTERACTION_ANIM_PLAYING()
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - IS_ANY_INTERACTION_ANIM_PLAYING")
		ENDIF
		
		IF IS_PAUSE_MENU_ACTIVE_EX()
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - IS_PAUSE_MENU_ACTIVE_EX")
		ENDIF
		
		IF OrbitalCannonServer.pUser = INVALID_PLAYER_INDEX()
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - OrbitalCannonServer.pUser is invalid")
		ENDIF
		
		IF g_bDisableOrbitalCannon
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - g_bDisableOrbitalCannon is TRUE")
		ENDIF
		
		IF g_bDisableOrbitalCannonBG
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - g_bDisableOrbitalCannonBG is TRUE")
		ENDIF
		
		IF NOT STAT_IS_STATS_TRACKING_ENABLED()
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - STAT_IS_STATS_TRACKING_ENABLED is FALSE")
		ENDIF
		#ENDIF
	ENDIF
	
	IF OrbitalCannonServer.pOwner != PLAYER_ID()
	AND OrbitalCannonServer.bReadyToBeObserved = FALSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - bReadyForContext - FALSE")
		#ENDIF
		
		bReadyForContext = FALSE
	ELSE
		#IF IS_DEBUG_BUILD
		IF OrbitalCannonServer.pOwner = PLAYER_ID()
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - I'm the owner")
		ENDIF
		
		IF OrbitalCannonServer.bReadyToBeObserved = FALSE
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - OrbitalCannonServer.bReadyToBeObserved is FALSE")
		ENDIF
		#ENDIF
	ENDIF
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
		OrbitalCannonLocal.iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF bReadyForContext
	AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
		IF bInPropertyOwnerNotPaidLastUtilityBill
			IF OrbitalCannonLocal.iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention, CP_HIGH_PRIORITY, "ORB_CAN_BILL")
			ENDIF
		ELSE
			IF OrbitalCannonLocal.iOrbitalCannonContextIntention = NEW_CONTEXT_INTENTION
				IF OrbitalCannonServer.pUser = INVALID_PLAYER_INDEX()
				AND OrbitalCannonServer.pOwner = PLAYER_ID()
					REGISTER_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention, CP_HIGH_PRIORITY, "ORB_CAN_START")
				ELSE
					REGISTER_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention, CP_HIGH_PRIORITY, "ORB_CAN_OBSRV")
				ENDIF
			ELSE
				IF HAS_CONTEXT_BUTTON_TRIGGERED(OrbitalCannonLocal.iOrbitalCannonContextIntention)
					PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON - Triggering Orbital Cannon activation")
					
					CLEAR_ALL_BIG_MESSAGES()
					CLEANUP_BIG_MESSAGE()
					
					RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RELEASE_CONTEXT_INTENTION(OrbitalCannonLocal.iOrbitalCannonContextIntention)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_MOVEMENT_SPEED(INT iZoomLevel)
	FLOAT fMovementSpeed = 100.0
	
	SWITCH iZoomLevel
		CASE 0	fMovementSpeed = 90.0	BREAK
		CASE 1	fMovementSpeed = 60.0	BREAK
		CASE 2	fMovementSpeed = 50.0	BREAK
		CASE 3	fMovementSpeed = 25.0	BREAK
		CASE 4	fMovementSpeed = 10.0	BREAK
	ENDSWITCH
	
	RETURN fMovementSpeed
ENDFUNC

/// PURPOSE:
///    Maintains the Orbital Cannon zooming in/out
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
///    fNewFOV - Orbital Cannon camera FOV
///    bOwner - TRUE if the local player is the owner
PROC MAINTAIN_CAMERA_ZOOM(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, FLOAT &fNewFOV, BOOL bOwner = FALSE)
	FLOAT fStep = 125.0
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
		fNewFOV = fNewFOV +@ fStep
		
		SWITCH OrbitalCannonLocal.iZoomLevel
			CASE 1
				IF fNewFOV > 100.0
					fNewFOV = 100.0
					OrbitalCannonLocal.iZoomLevel = 0
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF fNewFOV > 80.0
					fNewFOV = 80.0
					OrbitalCannonLocal.iZoomLevel = 1
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF fNewFOV > 60.0
					fNewFOV = 60.0
					OrbitalCannonLocal.iZoomLevel = 2
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF fNewFOV > 40.0
					fNewFOV = 40.0
					OrbitalCannonLocal.iZoomLevel = 3
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ELIF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
		fNewFOV = fNewFOV -@ fStep
		
		SWITCH OrbitalCannonLocal.iZoomLevel
			CASE 0
				IF fNewFOV < 80.0
					fNewFOV = 80.0
					OrbitalCannonLocal.iZoomLevel = 1
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF fNewFOV < 60.0
					fNewFOV = 60.0
					OrbitalCannonLocal.iZoomLevel = 2
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF fNewFOV < 40.0
					fNewFOV = 40.0
					OrbitalCannonLocal.iZoomLevel = 3
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF fNewFOV < 20.0
					fNewFOV = 20.0
					OrbitalCannonLocal.iZoomLevel = 4
					
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					
					IF NOT bOwner
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Get the Z pos for the point in world, to avoid mountains
/// PARAMS:
///    vCamPos - Current camera position in world
/// RETURNS:
///    Z position to avoid mountains
FUNC FLOAT GET_Z_POS(VECTOR vCamPos)
	IF vCamPos.y >= 1000.0
	AND vCamPos.y <= 1700.0
	AND vCamPos.x >= -700.0
	AND vCamPos.x <= 1100.0
		RETURN 500.0
	ENDIF
	
	IF vCamPos.y >= 3600.0
	AND vCamPos.y <= 4100.0
	AND vCamPos.x >= -1700.0
	AND vCamPos.x <= -700.0
		RETURN 550.0
	ENDIF
	
	IF vCamPos.y >= 4880.0
	AND vCamPos.y <= 6150.0
	AND vCamPos.x >= -500.0
	AND vCamPos.x <= 1900.0
		RETURN 850.0
	ENDIF
	
	RETURN 400.0
ENDFUNC

FUNC FLOAT GET_ZOOM_LEVEL_FLOAT(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bDecrease = FALSE, BOOL bInitial = FALSE)
	IF NOT bInitial
		IF NOT bDecrease
			SWITCH OrbitalCannonLocal.iZoomLevel
				CASE 0	RETURN 0.25	BREAK
				CASE 1	RETURN 0.5	BREAK
				CASE 2	RETURN 0.75	BREAK
				CASE 3	RETURN 1.0	BREAK
			ENDSWITCH
		ELSE
			SWITCH OrbitalCannonLocal.iZoomLevel
				CASE 1	RETURN 0.0	BREAK
				CASE 2	RETURN 0.25	BREAK
				CASE 3	RETURN 0.5	BREAK
				CASE 4	RETURN 0.75	BREAK
			ENDSWITCH
		ENDIF
	ELSE
		SWITCH OrbitalCannonServer.iZoomLevel
			CASE 0	RETURN 0.0	BREAK
			CASE 1	RETURN 0.25	BREAK
			CASE 2	RETURN 0.5	BREAK
			CASE 3	RETURN 0.75	BREAK
			CASE 4	RETURN 1.0	BREAK
		ENDSWITCH
	ENDIF
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Maintains the Orbital Cannon moving up/down, left/right and zooming in/out
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
PROC MAINTAIN_CAMERA_MOVEMENT(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		FLOAT fRightY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		
		IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				IF fRightY = 0.0
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
			BOOL bUpdate = FALSE
			BOOL bUpdateZoom = FALSE
			BOOL bTrackingPed = FALSE
			
			FLOAT fNewFOV = GET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon)
			FLOAT fStep = 35.0 + GET_MOVEMENT_SPEED(OrbitalCannonLocal.iZoomLevel)
			FLOAT fLeftX = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X)
			FLOAT fLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
			
			VECTOR vPedCoords
			VECTOR vNewCamCoords = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
			
			IF OrbitalCannonServer.bFiring
				fStep = 50.0
			ENDIF
			
			IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
			AND NOT OrbitalCannonLocal.bFired
				IF OrbitalCannonServer.pTargetPlayer != INVALID_PLAYER_INDEX()
					PED_INDEX pedTemp = GET_PLAYER_PED(OrbitalCannonServer.pTargetPlayer)
					
					IF DOES_ENTITY_EXIST(pedTemp)
					AND NOT IS_PED_INJURED(pedTemp)
						PLAYER_INDEX pVehicleOwner
						
						IF IS_PLAYER_IN_ARMORY_TRUCK(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1], FALSE)
							ENDIF
						ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_ARMORY_AIRCRAFT_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
							ENDIF
						ELIF IS_PLAYER_IN_HACKER_TRUCK(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_HACKER_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
							ENDIF
						#IF FEATURE_HEIST_ISLAND
						ELIF IS_PLAYER_IN_SUBMARINE(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
							ENDIF
						#ENDIF
						ELSE
							vPedCoords = GET_ENTITY_COORDS(pedTemp, FALSE)
						ENDIF
						
						vNewCamCoords.x = vPedCoords.x
						vNewCamCoords.y = vPedCoords.y
						
						bTrackingPed = TRUE
						bUpdate = TRUE
					ENDIF
				ENDIF
			ELSE
				IF fLeftX > 0.1
					IF (vNewCamCoords.x +@ ABSF(fStep * fLeftX)) <= 4000.0
						bUpdate = TRUE
						
						vNewCamCoords.x = vNewCamCoords.x +@ ABSF(fStep * fLeftX)
					ENDIF
				ELIF fLeftX < -0.1
					IF (vNewCamCoords.x -@ ABSF(fStep * fLeftX)) >= -4000.0
						bUpdate = TRUE
						
						vNewCamCoords.x = vNewCamCoords.x -@ ABSF(fStep * fLeftX)
					ENDIF
				ENDIF
				
				IF fLeftY > 0.1
					IF (vNewCamCoords.y -@ ABSF(fStep * fLeftY)) >= -4000.0
						bUpdate = TRUE
						
						vNewCamCoords.y = vNewCamCoords.y -@ ABSF(fStep * fLeftY)
					ENDIF
				ELIF fLeftY < -0.1
					IF (vNewCamCoords.y +@ ABSF(fStep * fLeftY)) <= 8000.0
						bUpdate = TRUE
						
						vNewCamCoords.y = vNewCamCoords.y +@ ABSF(fStep * fLeftY)
					ENDIF
				ENDIF
			ENDIF
			
			FLOAT fZPos = GET_Z_POS(vNewCamCoords)
			
			IF bTrackingPed
				IF vPedCoords.z > (GET_Z_POS(vPedCoords) - 100.0)
					fZPos = vPedCoords.z + 100.0
				ELSE
					fZPos = GET_Z_POS(vPedCoords)
				ENDIF
			ENDIF
			
			IF vNewCamCoords.z != fZPos
				IF bTrackingPed
					fStep *= 2
				ENDIF
				
				IF vNewCamCoords.z < fZPos
					vNewCamCoords.z = vNewCamCoords.z +@ ABSF(fStep)
					
					IF vNewCamCoords.z > fZPos
						vNewCamCoords.z = fZPos
					ENDIF
				ELIF vNewCamCoords.z > fZPos
					vNewCamCoords.z = vNewCamCoords.z -@ ABSF(fStep)
					
					IF vNewCamCoords.z < fZPos
						vNewCamCoords.z = fZPos
					ENDIF
				ENDIF
				
				bUpdate = TRUE
			ENDIF
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					IF fRightY > 0.3
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						AND OrbitalCannonLocal.iZoomLevel > 0
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, TRUE))
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
						ENDIF
					ELIF fRightY < -0.3
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						AND OrbitalCannonLocal.iZoomLevel < 4
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, FALSE))
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
						ENDIF
					ENDIF
				ELSE
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						AND OrbitalCannonLocal.iZoomLevel > 0
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, TRUE))
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
						ENDIF
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						AND OrbitalCannonLocal.iZoomLevel < 4
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, FALSE))
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
			OR IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				bUpdateZoom = TRUE
			ENDIF
			
			FLOAT fXIncrease = 0.0
			FLOAT fYIncrease = 0.0
			
			VECTOR vTemp = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
			
			IF vTemp.x < vNewCamCoords.x
				fXIncrease = 50.0
			ELIF vTemp.x > vNewCamCoords.x
				fXIncrease = -50.0
			ENDIF
			
			IF vTemp.y < vNewCamCoords.y
				fYIncrease = 50.0
			ELIF vTemp.y > vNewCamCoords.y
				fYIncrease = -50.0
			ENDIF
			
			IF bUpdate
			OR bUpdateZoom
				IF GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
				AND OrbitalCannonLocal.eWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
					IF bUpdate
						IF bTrackingPed
							IF NOT ARE_VECTORS_ALMOST_EQUAL(vTemp, vNewCamCoords, 0.085, TRUE)
								START_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
							ELSE
								STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
								STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
								STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
							ENDIF
						ELSE
							START_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
						ENDIF
					ELSE
						STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
					ENDIF
					
					IF bUpdateZoom
						IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
							START_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
						ELIF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
							START_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
						ENDIF
					ELSE
						STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
					ENDIF
				ELSE
					STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
					STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
					STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
				ENDIF
				
				MAINTAIN_CAMERA_ZOOM(OrbitalCannonLocal, fNewFOV, TRUE)
				
				SET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon, fNewFOV)
				
				SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, vNewCamCoords)
			ELSE
				STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
				STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
				STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
			ENDIF
			
			FLOAT fTempZPos
			GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vNewCamCoords, fTempZPos, TRUE)
			
			SET_FOCUS_POS_AND_VEL(<<vNewCamCoords.x + fXIncrease, vNewCamCoords.y + fYIncrease, fTempZPos + 50.0>>, <<-90.0, 0.0, 0.0>>)
		ENDIF
	ELSE
		IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
			BOOL bUpdateZoom = FALSE
			
			FLOAT fNewFOV = GET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon)
			
			VECTOR vCamCoords = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
				IF OrbitalCannonServer.iZoomLevel < OrbitalCannonLocal.iZoomLevel
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					AND OrbitalCannonLocal.iZoomLevel > 0
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, TRUE))
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ELIF OrbitalCannonServer.iZoomLevel > OrbitalCannonLocal.iZoomLevel
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
					AND OrbitalCannonLocal.iZoomLevel < 4
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, FALSE))
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
			OR IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				bUpdateZoom = TRUE
			ENDIF
			
			IF bUpdateZoom
				IF GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
				AND OrbitalCannonServer.eWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
					IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
						START_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
					ELIF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
						START_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
					ENDIF
				ELSE
					STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
					STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
				ENDIF
				
				MAINTAIN_CAMERA_ZOOM(OrbitalCannonLocal, fNewFOV)
				
				SET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon, fNewFOV)
			ELSE
				STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
				STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
			ENDIF
			
			FLOAT fTempZPos
			GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vCamCoords, fTempZPos, TRUE)
			
			SET_FOCUS_POS_AND_VEL(<<vCamCoords.x, vCamCoords.y, fTempZPos + 50.0>>, <<-90.0, 0.0, 0.0>>)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the description of the current menu selection
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC UPDATE_MENU_DESCRIPTION(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	BOOL bTimer = FALSE
	
	STRING sDescription = ""
	
	IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
		SWITCH OrbitalCannonLocal.iMenuCurrentItem
			CASE 0
				sDescription = "ORB_CAN_MENU_1a"
			BREAK
			
			CASE 1
				IF (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)) < (g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000)
				AND NOT (IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
				OR IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO))
					bTimer = TRUE
				ELIF g_sMPTunables.bdisablekillswitchOrbital
					sDescription = "ORB_CAN_MENU_NA"
				ELIF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
					sDescription = "ORB_CAN_MENU_2c"
				ELIF NOT NETWORK_CAN_SPEND_MONEY(GET_ORBITAL_CANNON_MANUAL_PRICE(), FALSE, TRUE, FALSE)
				AND NOT DOES_PLAYER_HAVE_FREE_SHOT()
					sDescription = "ORB_CAN_NO_CASH"
				ELIF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
				AND NOT DOES_PLAYER_HAVE_FREE_SHOT()
					sDescription = "ORB_CAN_MAN_NA"
				ELIF OrbitalCannonLocal.bConfirmMenuOption
					sDescription = "ORB_CAN_MENU_2b"
				ELIF DOES_PLAYER_HAVE_FREE_SHOT()
					sDescription = "ORB_CAN_MENU_2d"
				ELSE
					sDescription = "ORB_CAN_MENU_2a"
				ENDIF
			BREAK
			
			CASE 2
				IF (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)) < (g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000)
				AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
					bTimer = TRUE
				ELIF g_sMPTunables.bdisablekillswitchOrbital
					sDescription = "ORB_CAN_MENU_NA"
				ELIF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
					sDescription = "ORB_CAN_MENU_3d"
				ELIF OrbitalCannonLocal.iPlayerCount <= 0
					sDescription = "ORB_CAN_MENU_3c"
				ELIF NOT NETWORK_CAN_SPEND_MONEY(GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal), FALSE, TRUE, FALSE)
					sDescription = "ORB_CAN_NO_CASH"
				ELIF OrbitalCannonLocal.bConfirmMenuOption
					sDescription = "ORB_CAN_MENU_3b"
				ELIF DOES_PLAYER_HAVE_FREE_SHOT()
					sDescription = "ORB_CAN_MENU_3e"
				ELSE
					sDescription = "ORB_CAN_MENU_3a"
				ENDIF
			BREAK
		ENDSWITCH
	ELIF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
		sDescription = "ORB_CAN_MENU_3a"
	ENDIF
	
	IF bTimer
		TEXT_LABEL_15 tl_15MenuDescriptionTime
		tl_15MenuDescriptionTime = GET_TEXT_OF_TIME(((g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000) - ((GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)))) * 1000)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_MENU_HELP_TEXT")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CONT_REQ_CD")
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tl_15MenuDescriptionTime)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		END_SCALEFORM_MOVIE_METHOD()
	ELSE
		BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_MENU_HELP_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sDescription)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the availability of the current menu selection
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC MAINTAIN_MENU_AVAILABILITY(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	OrbitalCannonLocal.bAvailable = TRUE
	
	IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
		SWITCH OrbitalCannonLocal.iMenuCurrentItem
			CASE 0
				OrbitalCannonLocal.bAvailable = TRUE
			BREAK
			
			CASE 1
				IF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
				OR g_sMPTunables.bdisablekillswitchOrbital
				OR (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)) < (g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000)
				OR (NOT NETWORK_CAN_SPEND_MONEY(GET_ORBITAL_CANNON_MANUAL_PRICE(), FALSE, TRUE, FALSE) AND NOT (IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL) OR IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)) AND NOT DOES_PLAYER_HAVE_FREE_SHOT())
				OR (IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO) AND NOT DOES_PLAYER_HAVE_FREE_SHOT())
					OrbitalCannonLocal.bAvailable = FALSE
				ENDIF
			BREAK
			
			CASE 2
				IF OrbitalCannonLocal.iPlayerCount <= 0
				OR g_sMPTunables.bdisablekillswitchOrbital
				OR IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
				OR (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)) < (g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000)
				OR (NOT NETWORK_CAN_SPEND_MONEY(GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal), FALSE, TRUE, FALSE) AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO))
					OrbitalCannonLocal.bAvailable = FALSE
					
					PRINTLN("[ORBITAL_CANNON] MAINTAIN_MENU_AVAILABILITY - Target player is unavailable")
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEAR_ORBITAL_CANNON_TRANSACTION(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	OrbitalCannonLocal.eTransactionService = SERVICE_INVALID
	OrbitalCannonLocal.iTransactionAmount = -1
	OrbitalCannonLocal.iTransactionFlags = 0
	
	IF OrbitalCannonLocal.iCurrentTransaction != -1
		DELETE_CASH_TRANSACTION(OrbitalCannonLocal.iCurrentTransaction)
		OrbitalCannonLocal.iCurrentTransaction = -1
	ENDIF
	
	PRINTLN("[ORBITAL_CANNON] CLEAR_ORBITAL_CANNON_TRANSACTION called")	
ENDPROC

PROC START_ORBITAL_CANNON_CASH_TRANSACTION(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, TRANSACTION_SERVICES eService, INT iAmount, INT iFlags = 0)
	IF NOT (OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_MENU)
		IF NOT IS_CASH_TRANSACTION_COMPLETE(OrbitalCannonLocal.iCurrentTransaction)
			ASSERTLN("[ORBITAL_CANNON] START_ORBITAL_CANNON_CASH_TRANSACTION - another transaction is ongoing.")
			
			EXIT
		ENDIF
	ENDIF
	
	OrbitalCannonLocal.eTransactionService = eService
	OrbitalCannonLocal.iTransactionAmount = iAmount
	OrbitalCannonLocal.iTransactionFlags = iFlags
	
	IF USE_SERVER_TRANSACTIONS()
		IF NOT NETWORK_REQUEST_CASH_TRANSACTION(OrbitalCannonLocal.iCurrentTransaction, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, OrbitalCannonLocal.eTransactionService, OrbitalCannonLocal.iTransactionAmount, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
			CLEAR_ORBITAL_CANNON_TRANSACTION(OrbitalCannonLocal)
			
			ASSERTLN("[ORBITAL_CANNON] START_ORBITAL_CANNON_CASH_TRANSACTION - NETWORK_REQUEST_CASH_TRANSACTION failed!")
			
			EXIT
		ENDIF
	ENDIF
	
	OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_PENDING
	
	PRINTLN("[ORBITAL_CANNON] START_ORBITAL_CANNON_CASH_TRANSACTION called with ", GET_CASH_TRANSACTION_SERVICE_NAME(OrbitalCannonLocal.eTransactionService), " $", iAmount)
ENDPROC

/// PURPOSE:
///    Updates the current menu
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonClient - Client data struct
///    OrbitalCannonLocal - Local data struct
PROC UPDATE_MENU(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	INT i
	
	IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_INITIAL_MENU_WARNING)
		SWITCH OrbitalCannonLocal.eTransactionState
			CASE ORBITAL_CANNON_TRANSACTION_STATE_MENU
				IF GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
					MAINTAIN_MENU_AVAILABILITY(OrbitalCannonLocal)
					
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					OR OrbitalCannonLocal.sMenuInput.bAccept
						IF OrbitalCannonLocal.bAvailable
							PLAY_SOUND_FRONTEND(-1, "menu_select", "dlc_xm_orbital_cannon_sounds", TRUE)
							
							IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
								SWITCH OrbitalCannonLocal.iMenuCurrentItem
									CASE 0
										PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Selecting free cam")
										
										REPEAT MAX_INITIAL_MENU_OPTIONS i
											BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
												SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
											END_SCALEFORM_MOVIE_METHOD()
										ENDREPEAT
										
										OrbitalCannonLocal.iMenuCurrentItem = 0
										OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
										OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
										OrbitalCannonLocal.bConfirmMenuOption = FALSE
										
										OrbitalCannonServer.bFired = FALSE
										OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
										
										SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
										SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_CLOSE_MENU)
									BREAK
									
									CASE 1
										IF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
										OR IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Confirming paid acquire target")
											
											REPEAT MAX_INITIAL_MENU_OPTIONS i
												BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
													SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
												END_SCALEFORM_MOVIE_METHOD()
											ENDREPEAT
											
											OrbitalCannonLocal.iMenuCurrentItem = 0
											OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
											OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
											OrbitalCannonLocal.bConfirmMenuOption = FALSE
											
											OrbitalCannonServer.bFired = FALSE
											OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
											
											IF (OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
											OR OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON)
											AND NOT OrbitalCannonLocal.bFired
												IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
													START_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
													
													START_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
												ENDIF
											ENDIF
											
											SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
											SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_CLOSE_MENU)
										ELIF OrbitalCannonLocal.bConfirmMenuOption
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Confirming acquire target")
											
											IF DOES_PLAYER_HAVE_FREE_SHOT()
												START_ORBITAL_CANNON_CASH_TRANSACTION(OrbitalCannonLocal, SERVICE_SPEND_ORBITAL_MANUAL, 0)
											ELSE
												START_ORBITAL_CANNON_CASH_TRANSACTION(OrbitalCannonLocal, SERVICE_SPEND_ORBITAL_MANUAL, GET_ORBITAL_CANNON_MANUAL_PRICE())
											ENDIF
										ELSE
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Selecting acquire target")
											
											OrbitalCannonLocal.bConfirmMenuOption = TRUE
										ENDIF
									BREAK
									
									CASE 2
										IF IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Confirming paid target player")
											
											REPEAT MAX_INITIAL_MENU_OPTIONS i
												BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
													SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
												END_SCALEFORM_MOVIE_METHOD()
											ENDREPEAT
											
											OrbitalCannonLocal.bRebuildMenu = FALSE
											
											OrbitalCannonLocal.iMenuCurrentItem = 0
											OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
											OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
											OrbitalCannonLocal.bConfirmMenuOption = FALSE
											
											OrbitalCannonServer.bFired = FALSE
											OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
											
											SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
										ELIF OrbitalCannonLocal.bConfirmMenuOption
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Confirming target player")
											
											START_ORBITAL_CANNON_CASH_TRANSACTION(OrbitalCannonLocal, SERVICE_SPEND_ORBITAL_AUTO, GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal))
										ELSE
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Selecting target player")
											
											OrbitalCannonLocal.bConfirmMenuOption = TRUE
										ENDIF
									BREAK
								ENDSWITCH
							ELIF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
								PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Selecting target player: ", OrbitalCannonLocal.iMenuCurrentItem)
								
								IF OrbitalCannonLocal.iMenuCurrentItem >= 0
								AND OrbitalCannonLocal.iMenuCurrentItem < NUM_NETWORK_PLAYERS
									OrbitalCannonServer.pTargetPlayer = OrbitalCannonLocal.pTargetPlayers[OrbitalCannonLocal.iMenuCurrentItem]
								ENDIF
								
								OrbitalCannonLocal.iMenuCurrentItem = 0
								
								OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
								
								OrbitalCannonServer.bFired = FALSE
								
								CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
								
								SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_CLOSE_MENU)
								SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
								
								SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
								
								SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
							ENDIF
						ELSE
							PLAY_SOUND_FRONTEND(-1, "menu_select", "dlc_xm_orbital_cannon_sounds", TRUE)
						ENDIF
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					OR OrbitalCannonLocal.sMenuInput.bBack
						STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
						
						IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
							IF OrbitalCannonLocal.bConfirmMenuOption
								OrbitalCannonLocal.bConfirmMenuOption = FALSE
							ELSE
								IF (IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
								OR IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL))
								AND NOT DOES_PLAYER_HAVE_FREE_SHOT()
									SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_INITIAL_MENU_WARNING)
								ELSE
									SWITCH OrbitalCannonLocal.iMenuCurrentItem
										CASE 0
											PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Backing out of initial menu")
											
											SET_GAMEPLAY_CAM_RELATIVE_HEADING()
											SET_GAMEPLAY_CAM_RELATIVE_PITCH()
											
											SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU)
										BREAK
										
										CASE 1
											IF OrbitalCannonLocal.bConfirmMenuOption
												OrbitalCannonLocal.bConfirmMenuOption = FALSE
											ELSE
												PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Backing out of initial menu")
												
												SET_GAMEPLAY_CAM_RELATIVE_HEADING()
												SET_GAMEPLAY_CAM_RELATIVE_PITCH()
												
												SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU)
											ENDIF
										BREAK
										
										CASE 2
											IF OrbitalCannonLocal.bConfirmMenuOption
												OrbitalCannonLocal.bConfirmMenuOption = FALSE
											ELSE
												PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Backing out of initial menu")
												
												SET_GAMEPLAY_CAM_RELATIVE_HEADING()
												SET_GAMEPLAY_CAM_RELATIVE_PITCH()
												
												SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU)
											ENDIF
										BREAK
									ENDSWITCH
								ENDIF
							ENDIF
						ELIF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
							PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Backing out of player menu")
							
							REPEAT NUM_NETWORK_PLAYERS i
								BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
								END_SCALEFORM_MOVIE_METHOD()
							ENDREPEAT
							
							OrbitalCannonLocal.bRebuildMenu = FALSE
							OrbitalCannonLocal.iMenuCurrentItem = 0
							OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
							OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "menu_back", "dlc_xm_orbital_cannon_sounds", TRUE)
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) AND ALLOW_ANALOGUE_MOVEMENT(OrbitalCannonLocal.scrollDelay, OrbitalCannonLocal.sMenuInput.iMoveUp)
					OR OrbitalCannonLocal.sMenuInput.bPrevious AND ALLOW_ANALOGUE_MOVEMENT(OrbitalCannonLocal.scrollDelay, OrbitalCannonLocal.sMenuInput.iMoveUp)
						IF OrbitalCannonLocal.sMenuInput.iMoveUp > 0
							IF NOT OrbitalCannonLocal.bConfirmMenuOption
								IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
									IF OrbitalCannonLocal.iMenuCurrentItem > 0
										OrbitalCannonLocal.iMenuCurrentItem--
										
										CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(OrbitalCannonLocal.sfOrbitalCannon, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
										
										PLAY_SOUND_FRONTEND(-1, "menu_up_down", "dlc_xm_orbital_cannon_sounds", TRUE)
									ENDIF
								ELIF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
									IF OrbitalCannonLocal.iMenuCurrentItem > 0
										OrbitalCannonLocal.iMenuCurrentItem--
										
										CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(OrbitalCannonLocal.sfOrbitalCannon, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
										
										PLAY_SOUND_FRONTEND(-1, "menu_up_down", "dlc_xm_orbital_cannon_sounds", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) AND ALLOW_ANALOGUE_MOVEMENT(OrbitalCannonLocal.scrollDelay, OrbitalCannonLocal.sMenuInput.iMoveUp)
					OR OrbitalCannonLocal.sMenuInput.bNext AND ALLOW_ANALOGUE_MOVEMENT(OrbitalCannonLocal.scrollDelay, OrbitalCannonLocal.sMenuInput.iMoveUp)
						IF OrbitalCannonLocal.sMenuInput.iMoveUp < 0
							IF NOT OrbitalCannonLocal.bConfirmMenuOption
								IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
									IF OrbitalCannonLocal.iMenuCurrentItem < MAX_INITIAL_MENU_OPTIONS - 1
										OrbitalCannonLocal.iMenuCurrentItem++
										
										CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(OrbitalCannonLocal.sfOrbitalCannon, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
										
										PLAY_SOUND_FRONTEND(-1, "menu_up_down", "dlc_xm_orbital_cannon_sounds", TRUE)
									ENDIF
								ELIF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
									IF OrbitalCannonLocal.iMenuCurrentItem < OrbitalCannonLocal.iPlayerCount - 1
										OrbitalCannonLocal.iMenuCurrentItem++
										
										CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(OrbitalCannonLocal.sfOrbitalCannon, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
										
										PLAY_SOUND_FRONTEND(-1, "menu_up_down", "dlc_xm_orbital_cannon_sounds", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ORBITAL_CANNON_TRANSACTION_STATE_PENDING
				IF USE_SERVER_TRANSACTIONS()
					IF IS_CASH_TRANSACTION_COMPLETE(OrbitalCannonLocal.iCurrentTransaction)
						IF GET_CASH_TRANSACTION_STATUS(OrbitalCannonLocal.iCurrentTransaction) = CASH_TRANSACTION_STATUS_SUCCESS
							PRINTLN("[ORBITAL_CANNON] UPDATE_MENU transaction complete - ", OrbitalCannonLocal.iCurrentTransaction, " ", GET_CASH_TRANSACTION_SERVICE_NAME(OrbitalCannonLocal.eTransactionService), " $", OrbitalCannonLocal.iTransactionAmount, ": Success!")
							NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(OrbitalCannonLocal.iCurrentTransaction))
							
							IF OrbitalCannonLocal.eTransactionService = SERVICE_SPEND_ORBITAL_MANUAL
								NETWORK_SPEND_GANGOPS_CANNON(OrbitalCannonLocal.iTransactionAmount, FALSE, TRUE, 0)
								
								SET_BIT(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_MANUAL_PURCHASED)
							ELIF OrbitalCannonLocal.eTransactionService = SERVICE_SPEND_ORBITAL_AUTO
								IF OrbitalCannonLocal.iTransactionAmount = GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal)
									NETWORK_SPEND_GANGOPS_CANNON(OrbitalCannonLocal.iTransactionAmount, FALSE, TRUE, 1)
								ELSE
									NETWORK_SPEND_GANGOPS_CANNON(OrbitalCannonLocal.iTransactionAmount, FALSE, TRUE, 2)
								ENDIF
								
								SET_BIT(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_AUTOMATIC_PURCHASED)
							ENDIF
							
							IF OrbitalCannonLocal.iCurrentTransaction != -1
								DELETE_CASH_TRANSACTION(OrbitalCannonLocal.iCurrentTransaction)
								OrbitalCannonLocal.iCurrentTransaction = -1
							ENDIF
							
							OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_SUCCESS
						ELSE
							PRINTLN("[ORBITAL_CANNON] UPDATE_MENU transaction complete - ", OrbitalCannonLocal.iCurrentTransaction, " ", GET_CASH_TRANSACTION_SERVICE_NAME(OrbitalCannonLocal.eTransactionService), " $", OrbitalCannonLocal.iTransactionAmount, ": Fail!")
							
							IF OrbitalCannonLocal.iCurrentTransaction != -1
								DELETE_CASH_TRANSACTION(OrbitalCannonLocal.iCurrentTransaction)
								OrbitalCannonLocal.iCurrentTransaction = -1
							ENDIF
							
							OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_FAILED
						ENDIF
					ELSE
						PRINTLN("[ORBITAL_CANNON] UPDATE_MENU transaction waiting on IS_CASH_TRANSACTION_COMPLETE")
					ENDIF
				ELSE
					IF OrbitalCannonLocal.eTransactionService = SERVICE_SPEND_ORBITAL_MANUAL
						NETWORK_SPEND_GANGOPS_CANNON(OrbitalCannonLocal.iTransactionAmount, FALSE, TRUE, 0)
						
						SET_BIT(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_MANUAL_PURCHASED)
					ELIF OrbitalCannonLocal.eTransactionService = SERVICE_SPEND_ORBITAL_AUTO
						IF OrbitalCannonLocal.iTransactionAmount = GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal)
							NETWORK_SPEND_GANGOPS_CANNON(OrbitalCannonLocal.iTransactionAmount, FALSE, TRUE, 1)
						ELSE
							NETWORK_SPEND_GANGOPS_CANNON(OrbitalCannonLocal.iTransactionAmount, FALSE, TRUE, 2)
						ENDIF
						
						SET_BIT(g_iOrbitalCannonRefundBS, ciH2_ORBITAL_CANNON_AUTOMATIC_PURCHASED)
					ENDIF
					
					OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_SUCCESS
				ENDIF
			BREAK
			
			CASE ORBITAL_CANNON_TRANSACTION_STATE_SUCCESS
				IF OrbitalCannonLocal.eTransactionService = SERVICE_SPEND_ORBITAL_MANUAL
					PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Confirming paid acquire target")
					
					REPEAT MAX_INITIAL_MENU_OPTIONS i
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
						END_SCALEFORM_MOVIE_METHOD()
					ENDREPEAT
					
					OrbitalCannonLocal.iMenuCurrentItem = 0
					OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
					OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
					OrbitalCannonLocal.bConfirmMenuOption = FALSE
					
					OrbitalCannonServer.bFired = FALSE
					OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
					
					IF (OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
					OR OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON)
					AND NOT OrbitalCannonLocal.bFired
						IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
							START_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
							
							START_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
						ENDIF
					ENDIF
					
					SET_BIT(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
					
					SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_CLOSE_MENU)
					
					OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_MENU
				ELIF OrbitalCannonLocal.eTransactionService = SERVICE_SPEND_ORBITAL_AUTO
					PRINTLN("[ORBITAL_CANNON] UPDATE_MENU - Confirming paid target player")
					
					REPEAT MAX_INITIAL_MENU_OPTIONS i
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
						END_SCALEFORM_MOVIE_METHOD()
					ENDREPEAT
					
					OrbitalCannonLocal.bRebuildMenu = FALSE
					
					OrbitalCannonLocal.iMenuCurrentItem = 0
					OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
					OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
					OrbitalCannonLocal.bConfirmMenuOption = FALSE
					
					OrbitalCannonServer.bFired = FALSE
					OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
					
					SET_BIT(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
					
					OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_MENU
				ENDIF
			BREAK
			
			CASE ORBITAL_CANNON_TRANSACTION_STATE_FAILED
				CLEAR_ORBITAL_CANNON_TRANSACTION(OrbitalCannonLocal)
				OrbitalCannonLocal.eTransactionState = ORBITAL_CANNON_TRANSACTION_STATE_MENU
			BREAK
		ENDSWITCH
		
		UPDATE_MENU_DESCRIPTION(OrbitalCannonLocal)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER("IB_QUIT", "ORB_CAN_QUIT1", FE_WARNING_NO | FE_WARNING_YES, "ORB_CAN_QUIT2")
		
		IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU)
			CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_INITIAL_MENU_WARNING)
		ENDIF
		
		IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
			STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
			STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
			STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
			
			PLAY_SOUND_FRONTEND(-1, "menu_back", "dlc_xm_orbital_cannon_sounds", TRUE)
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
			
			CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_INITIAL_MENU_WARNING)
		ENDIF
	ENDIF
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
ENDPROC

/// PURPOSE:
///    Builds the initial menu
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC BUILD_INITIAL_MENU(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	PRINTLN("[ORBITAL_CANNON] BUILD_INITIAL_MENU - Building initial menu")
	
	BOOL bAvailable = TRUE
	
	BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_MENU_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ORB_CAN_MENU_T")
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "ADD_MENU_ITEM")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ORB_CAN_MENU_1T")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
	END_SCALEFORM_MOVIE_METHOD()
	
	bAvailable = TRUE
	
	IF IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
	OR g_sMPTunables.bdisablekillswitchOrbital
	OR (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)) < (g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000)
	OR (NOT NETWORK_CAN_SPEND_MONEY(GET_ORBITAL_CANNON_MANUAL_PRICE(), FALSE, TRUE, FALSE) AND NOT (IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL) OR IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)) AND NOT DOES_PLAYER_HAVE_FREE_SHOT())
	OR (IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO) AND NOT DOES_PLAYER_HAVE_FREE_SHOT())
		bAvailable = FALSE
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "ADD_MENU_ITEM")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ORB_CAN_MENU_2T")
		
		IF DOES_PLAYER_HAVE_FREE_SHOT()
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STORE_FREE_PRICE")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ITEM_COST")
					ADD_TEXT_COMPONENT_INTEGER(GET_ORBITAL_CANNON_MANUAL_PRICE())
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
		
		IF g_sMPTunables.bOrbitalCannonManualSale
			IF NOT DOES_PLAYER_HAVE_FREE_SHOT()
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ITEM_COST")
					ADD_TEXT_COMPONENT_INTEGER(g_sMPTunables.IH2_ORBITAL_CANNON_MANUAL_PRICE)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	bAvailable = TRUE
	
	IF OrbitalCannonLocal.iPlayerCount <= 0
	OR g_sMPTunables.bdisablekillswitchOrbital
	OR IS_PLAYER_IN_PASSIVE_MODE(PLAYER_ID())
	OR (GET_CLOUD_TIME_AS_INT() - GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN)) < (g_sMPTunables.IH2_ORBITAL_CANON_COOLDOWN_TIMER / 1000)
	OR (NOT NETWORK_CAN_SPEND_MONEY(GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal), FALSE, TRUE, FALSE) AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO))
		bAvailable = FALSE
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "ADD_MENU_ITEM")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ORB_CAN_MENU_3T")
		
		IF DOES_PLAYER_HAVE_FREE_SHOT()
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STORE_FREE_PRICE")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ITEM_COST")
					ADD_TEXT_COMPONENT_INTEGER(GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal))
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
		
		IF g_sMPTunables.bOrbitalCannonAutomaticSale
			IF NOT DOES_PLAYER_HAVE_FREE_SHOT()
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ITEM_COST")
					ADD_TEXT_COMPONENT_INTEGER(GET_ORBITAL_CANNON_AUTOMATIC_PRICE(OrbitalCannonLocal, TRUE))
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Builds the player menu
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC BUILD_PLAYER_MENU(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	PRINTLN("[ORBITAL_CANNON] BUILD_PLAYER_MENU - Building player menu")
	
	INT iPlayerCount = 0
	
	PLAYER_INDEX pPlayer
	
	BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_MENU_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ORB_CAN_MENU_3T")
	END_SCALEFORM_MOVIE_METHOD()
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		pPlayer = INT_TO_PLAYERINDEX(i)
		
		IF pPlayer != PLAYER_ID()
		AND pPlayer != INVALID_PLAYER_INDEX()
			PED_INDEX pedTemp = GET_PLAYER_PED(pPlayer)
			
			IF DOES_ENTITY_EXIST(pedTemp)
			AND NOT IS_PED_INJURED(pedTemp)
				IF CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(pPlayer)
					BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "ADD_MENU_ITEM")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerCount)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME_STRING(GET_PLAYER_NAME(pPlayer))
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
					END_SCALEFORM_MOVIE_METHOD()
					
					OrbitalCannonLocal.pTargetPlayers[iPlayerCount] = pPlayer
					
					iPlayerCount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	INT j
	REPEAT (NUM_NETWORK_PLAYERS - iPlayerCount) j
		j += iPlayerCount
		
		BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(j)
		END_SCALEFORM_MOVIE_METHOD()
	ENDREPEAT
	
	IF iPlayerCount > 0
		IF OrbitalCannonLocal.iMenuCurrentItem >= iPlayerCount
			OrbitalCannonLocal.iMenuCurrentItem = iPlayerCount - 1
		ENDIF
	ELSE
		OrbitalCannonLocal.iMenuCurrentItem = 0
	ENDIF
	
	PRINTLN("[ORBITAL_CANNON] BUILD_PLAYER_MENU - Number of players in menu: ", iPlayerCount)
ENDPROC

/// PURPOSE:
///    Builds the current initial/player menu
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC BUILD_MENU(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	INT iPlayerCount = 0
	
	PLAYER_INDEX pPlayer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		pPlayer = INT_TO_PLAYERINDEX(i)
		
		IF pPlayer != PLAYER_ID()
		AND pPlayer != INVALID_PLAYER_INDEX()
			PED_INDEX pedTemp = GET_PLAYER_PED(pPlayer)
			
			IF DOES_ENTITY_EXIST(pedTemp)
			AND NOT IS_PED_INJURED(pedTemp)
				IF CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(pPlayer)
					iPlayerCount++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF OrbitalCannonLocal.iPlayerCount != iPlayerCount
		OrbitalCannonLocal.iPlayerCount = iPlayerCount
		
		OrbitalCannonLocal.bRebuildMenu = FALSE
	ENDIF
	
	OrbitalCannonLocal.iMenuStagger++
	
	IF OrbitalCannonLocal.iMenuStagger >= 10
		OrbitalCannonLocal.iMenuStagger = 0
		
		OrbitalCannonLocal.bRebuildMenu = FALSE
	ENDIF
	
	IF iPlayerCount = 0
		OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
		OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
	ENDIF
	
	IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
		BUILD_INITIAL_MENU(OrbitalCannonLocal)
	ELIF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
		IF NOT OrbitalCannonLocal.bRebuildMenu
			BUILD_PLAYER_MENU(OrbitalCannonLocal)
			
			OrbitalCannonLocal.bRebuildMenu = TRUE
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles shooting range menu controls for keyboard & mouse
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
PROC SETUP_MENU_CONTROLS(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(OrbitalCannonLocal.sMenuInput.iLeftX, OrbitalCannonLocal.sMenuInput.iLeftY, OrbitalCannonLocal.sMenuInput.iRightX, OrbitalCannonLocal.sMenuInput.iRightY)
	
	// Set the input flag states
	OrbitalCannonLocal.sMenuInput.bAccept = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	OrbitalCannonLocal.sMenuInput.bBack = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	OrbitalCannonLocal.sMenuInput.bPrevious = ((OrbitalCannonLocal.sMenuInput.iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
	OrbitalCannonLocal.sMenuInput.bNext = ((OrbitalCannonLocal.sMenuInput.iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
ENDPROC

/// PURPOSE:
///    Clean up everything related to firing the Orbital Cannon
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
///    bServer - TRUE if the player is the host
PROC CLEANUP_FIRING_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, BOOL bServer)
	IF bServer
	AND OrbitalCannonServer.bFiring
		OrbitalCannonServer.bFiring = FALSE
	ENDIF
	
	IF bServer
	AND OrbitalCannonServer.bJustFired
		OrbitalCannonServer.bJustFired = FALSE
	ENDIF
	
	IF IS_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
		STOP_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
	ENDIF
	
	STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
	CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
	
	RESET_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
ENDPROC

/// PURPOSE:
///    Get the initial position of the Orbital Cannon camera
/// PARAMS:
///    propertyID - Simple interior property ID
/// RETURNS:
///    VECTOR - Camera position
FUNC VECTOR GET_INITIAL_CAMERA_POS(SIMPLE_INTERIORS propertyID)
	SWITCH propertyID
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1		RETURN <<1277.8008, 2826.2588, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2		RETURN <<17.4709, 2620.2334, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3		RETURN <<2759.7202, 3900.2961, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4		RETURN <<3405.1191, 5500.5479, 400.0>>	BREAK
//		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_5		RETURN <<-818.6610, 5673.8794, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6		RETURN <<-8.8511, 6835.0034, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7		RETURN <<-2305.1941, 2477.9417, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8		RETURN <<44.7481, 3329.0310, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9		RETURN <<2085.6975, 1740.2380, 400.0>>	BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10	RETURN <<1878.3208, 281.3556, 400.0>>	BREAK
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Processes the player Orbital Cannon input
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonClient - Client broadcast data
///    OrbitalCannonLocal - Local data struct
/// RETURNS:
///    True if player declares they no longer want to interact with the Orbital Cannon
FUNC BOOL CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, SIMPLE_INTERIORS propertyID)
	IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sCooldownTimer)
	AND HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sCooldownTimer, 5000)
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
		
		RESET_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
		RESET_NET_TIMER(OrbitalCannonLocal.sCooldownTimer)
		
		IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
			OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		AND (GET_FRAME_COUNT() % 10) = 0
			IF OrbitalCannonServer.iZoomLevel != OrbitalCannonLocal.iZoomLevel
				OrbitalCannonServer.iZoomLevel = OrbitalCannonLocal.iZoomLevel
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(OrbitalCannonServer.vCamPos, GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon))
				OrbitalCannonServer.vCamPos = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
			ENDIF
		ENDIF
	ELSE
		IF NOT ARE_VECTORS_EQUAL(OrbitalCannonLocal.vCurrentCamPos, OrbitalCannonServer.vCamPos)
			OrbitalCannonLocal.vCurrentCamPos = OrbitalCannonServer.vCamPos
		ENDIF
		
		VECTOR vTemp
		
		IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
			BOOL bDefaultCameraMovement = TRUE
			
			IF OrbitalCannonServer.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
				IF OrbitalCannonServer.pTargetPlayer != INVALID_PLAYER_INDEX()
					PED_INDEX pedTemp = GET_PLAYER_PED(OrbitalCannonServer.pTargetPlayer)
					
					IF DOES_ENTITY_EXIST(pedTemp)
					AND NOT IS_PED_INJURED(pedTemp)
						PLAYER_INDEX pVehicleOwner
						
						VECTOR vPedCoords
						
						IF IS_PLAYER_IN_ARMORY_TRUCK(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1], FALSE)
							ENDIF
						ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_ARMORY_AIRCRAFT_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
							ENDIF
						ELIF IS_PLAYER_IN_HACKER_TRUCK(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_HACKER_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
							ENDIF
						#IF FEATURE_HEIST_ISLAND
						ELIF IS_PLAYER_IN_SUBMARINE(OrbitalCannonServer.pTargetPlayer)
							pVehicleOwner = GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(OrbitalCannonServer.pTargetPlayer)
							
							IF pVehicleOwner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(pVehicleOwner)
							AND DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)])
								vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
							ENDIF
						#ENDIF
						ELSE
							vPedCoords = GET_ENTITY_COORDS(pedTemp, FALSE)
						ENDIF
						
						VECTOR vCamCoords = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
						
						vTemp.x = vPedCoords.x
						vTemp.y = vPedCoords.y
						
						vTemp.z = vCamCoords.z
						
						FLOAT fStep = 125.0
						FLOAT fZPos = GET_Z_POS(vTemp)
						
						IF vPedCoords.z > (GET_Z_POS(vPedCoords) - 100.0)
							vTemp.z = vPedCoords.z + 100.0
						ELSE
							vTemp.z = GET_Z_POS(vPedCoords)
						ENDIF
						
						IF vTemp.z != fZPos
							IF vTemp.z < fZPos
								vTemp.z = vTemp.z +@ ABSF(fStep)
								
								IF vTemp.z > fZPos
									vTemp.z = fZPos
								ENDIF
							ELIF vTemp.z > fZPos
								vTemp.z = vTemp.z -@ ABSF(fStep)
								
								IF vTemp.z < fZPos
									vTemp.z = fZPos
								ENDIF
							ENDIF
						ENDIF
						
						bDefaultCameraMovement = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IF bDefaultCameraMovement
				IF ARE_VECTORS_ALMOST_EQUAL(GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon), OrbitalCannonLocal.vCurrentCamPos, 2.0)
					vTemp = OrbitalCannonLocal.vCurrentCamPos
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon), OrbitalCannonLocal.vCurrentCamPos, FALSE) >= 250.0
					AND (GET_DISTANCE_BETWEEN_COORDS(OrbitalCannonLocal.vCurrentCamPos, GET_INITIAL_CAMERA_POS(propertyID), FALSE) < 250.0
					OR OrbitalCannonServer.pTargetPlayer != INVALID_PLAYER_INDEX())
						CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, TRUE)
						
						CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
						
						SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
						
						SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
						
						RETURN FALSE
					ELSE
						vTemp = INTERP_VECTOR(GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon), OrbitalCannonLocal.vCurrentCamPos, 0.075, INTERPTYPE_LINEAR)
					ENDIF
				ENDIF
				
				OrbitalCannonLocal.bDefaultCameraMovementLastFrame = TRUE
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon), vTemp, FALSE) >= 30.0
				AND OrbitalCannonLocal.bDefaultCameraMovementLastFrame
					CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, TRUE)
					
					CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
					
					SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
					
					SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
					
					RETURN FALSE
				ENDIF
				
				OrbitalCannonLocal.bDefaultCameraMovementLastFrame = FALSE
			ENDIF
			
			IF NOT ARE_VECTORS_ALMOST_EQUAL(vTemp, OrbitalCannonLocal.vCurrentCamPos, 0.085, TRUE)
				START_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
			ELSE
				STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
			ENDIF
			
			SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, vTemp)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
		IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sInitialDelay, 3000)
			STOP_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
			
			PLAY_SOUND_FRONTEND(-1, "cannon_active", "dlc_xm_orbital_cannon_sounds", TRUE)
			
			RESET_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sSyncDelay)
		IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sSyncDelay, 1500)
			RESET_NET_TIMER(OrbitalCannonLocal.sSyncDelay)
		ENDIF
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_BROWSER_OPEN()
	AND NOT IS_TRANSITION_ACTIVE()
	AND GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
			IF OrbitalCannonLocal.eWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
				IF IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
				AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
				AND OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
				AND NOT OrbitalCannonLocal.bFired
				AND NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
				AND NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sSyncDelay)
				AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
				AND NOT IS_WARNING_MESSAGE_ACTIVE()
				AND DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
					OrbitalCannonServer.bFiring = TRUE
					
					IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFiringTimer)
						START_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
						
						START_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
						
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
							SET_VARIABLE_ON_SOUND(OrbitalCannonLocal.iSoundID, "Firing", 0)
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
						ENDIF
						
						SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 3000)
							IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
								BOOL bSetTimer = TRUE
								
								VECTOR vCamPos = GET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon)
								
								PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT - Firing Orbital Cannon")
								
								THEFEED_FLUSH_QUEUE()
								
								START_NET_TIMER(OrbitalCannonLocal.sFeedTimer)
								
								SET_VARIABLE_ON_SOUND(OrbitalCannonLocal.iSoundID, "Firing", 1)
								
								IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
								AND OrbitalCannonServer.pTargetPlayer != INVALID_PLAYER_INDEX()
									BOOL bTrackingPed = TRUE
									PED_INDEX pedTemp = GET_PLAYER_PED(OrbitalCannonServer.pTargetPlayer)
									PLAYER_INDEX pVehicleOwner
									VECTOR vPedPos
									
									IF IS_PLAYER_IN_ARMORY_TRUCK(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1])
											vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1], FALSE)
											bTrackingPed = FALSE
											OrbitalCannonLocal.vTarget = MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1]
											OrbitalCannonLocal.bFiringOnVehicle = TRUE
										ENDIF
									ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_ARMORY_AIRCRAFT_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)])
											vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
											bTrackingPed = FALSE
											OrbitalCannonLocal.vTarget = MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)]
											OrbitalCannonLocal.bFiringOnVehicle = TRUE
										ENDIF
									ELIF IS_PLAYER_IN_HACKER_TRUCK(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_HACKER_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)])
											vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
											bTrackingPed = FALSE
											OrbitalCannonLocal.vTarget = MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)]
											OrbitalCannonLocal.bFiringOnVehicle = TRUE
										ENDIF
									#IF FEATURE_HEIST_ISLAND
									ELIF IS_PLAYER_IN_SUBMARINE(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)])
											vPedPos = GET_ENTITY_COORDS(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
											bTrackingPed = FALSE
											OrbitalCannonLocal.vTarget = MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)]
											OrbitalCannonLocal.bFiringOnVehicle = TRUE
										ENDIF
									#ENDIF
									ENDIF
									
									IF DOES_ENTITY_EXIST(pedTemp)
									OR NOT bTrackingPed
										IF bTrackingPed
											vPedPos = GET_ENTITY_COORDS(pedTemp, FALSE)
											OrbitalCannonLocal.bFiringOnVehicle = FALSE
										ENDIF
										
										FLOAT fTempZ
										
										VECTOR vNormal
										VECTOR vTempPedPos = vPedPos
										VECTOR vPedForwardVector
										
										IF bTrackingPed
											IF NOT IS_PED_INJURED(pedTemp)
												vPedForwardVector = GET_ENTITY_FORWARD_VECTOR(pedTemp)
											ENDIF
										ELSE
											IF IS_VEHICLE_DRIVEABLE(OrbitalCannonLocal.vTarget)
												vPedForwardVector = GET_ENTITY_FORWARD_VECTOR(OrbitalCannonLocal.vTarget)
											ENDIF
										ENDIF
										
										GET_GROUND_Z_FOR_3D_COORD(vCamPos, vCamPos.z)
										GET_GROUND_Z_AND_NORMAL_FOR_3D_COORD(vCamPos, fTempZ, vNormal)
										
										IF CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(OrbitalCannonServer.pTargetPlayer)
											IF vPedPos.z >= (vCamPos.z - 15.0)
												USE_PARTICLE_FX_ASSET("scr_xm_orbital")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xm_orbital_blast", vCamPos, vNormal, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
												
												IF NOT g_bDisableOrbitalCannonSound
													PLAY_SOUND_FROM_COORD(-1, "DLC_XM_Explosions_Orbital_Cannon", vCamPos, DEFAULT, TRUE)
												ENDIF
												
												vPedForwardVector = NORMALISE_VECTOR(vPedForwardVector)
												
												vPedForwardVector.X *= 5.0
												vPedForwardVector.Y *= 5.0
												
												vTempPedPos += vPedForwardVector
												
												IF bTrackingPed
													IF IS_PED_IN_ANY_VEHICLE(pedTemp)
														ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), <<vTempPedPos.x, vTempPedPos.y, vPedPos.z>>, EXP_TAG_ORBITAL_CANNON, 1.0)
													ENDIF
												ENDIF
												
												ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), vPedPos, EXP_TAG_ORBITAL_CANNON, 1.0)
												
												vTempPedPos = vPedPos - vPedForwardVector
												
												IF bTrackingPed
													IF IS_PED_IN_ANY_VEHICLE(pedTemp)
														ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), <<vTempPedPos.x, vTempPedPos.y, vPedPos.z>>, EXP_TAG_ORBITAL_CANNON, 1.0)
													ENDIF
												ENDIF
												
												g_sFM_HEIST_PREP_Telemetry_data.m_OrbitalCannonKills++ 
												
												PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT - TARGET EXPLOSION X: ", vPedPos.x)
												PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT - TARGET EXPLOSION Y: ", vPedPos.y)
												PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT - TARGET EXPLOSION Z: ", vPedPos.z)
												
												PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT - g_sFM_HEIST_PREP_Telemetry_data.m_OrbitalCannonKills = ", g_sFM_HEIST_PREP_Telemetry_data.m_OrbitalCannonKills)
											ELSE
												USE_PARTICLE_FX_ASSET("scr_xm_orbital")
												START_NETWORKED_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_xm_orbital_blast", vCamPos, vNormal, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
												
												IF NOT g_bDisableOrbitalCannonSound
													PLAY_SOUND_FROM_COORD(-1, "DLC_XM_Explosions_Orbital_Cannon", vCamPos, DEFAULT, TRUE)
												ENDIF
												
												ADD_OWNED_EXPLOSION(PLAYER_PED_ID(), vCamPos, EXP_TAG_ORBITAL_CANNON, 1.0)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									FIRE_ORBITAL_CANNON(vCamPos)
								ENDIF
								
								OrbitalCannonServer.bFired = TRUE
								OrbitalCannonServer.bJustFired = TRUE
								
								OrbitalCannonLocal.iPurchaseBS = 0
								
								g_iOrbitalCannonRefundBS = 0
								
								#IF IS_DEBUG_BUILD
								IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipOrbitalCannonCooldown")
									bSetTimer = FALSE
								ENDIF
								#ENDIF
								
								IF bSetTimer
									SET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN, GET_CLOUD_TIME_AS_INT())
								ENDIF
								
								IF OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
									IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_XMAS2017_CANNON_GIFT)
									AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PURCHASE_XMAS2017_CANNON_GIFT)
										SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PURCHASE_XMAS2017_CANNON_GIFT, TRUE)
									ENDIF
								ENDIF
								
								IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
									REINIT_NET_TIMER(OrbitalCannonLocal.sKillTracker)
									
									OrbitalCannonLocal.iKillCounter = GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_KILLS)
								ENDIF
								
								REQUEST_SAVE(SSR_REASON_ORBITAL_CANNON, STAT_SAVETYPE_IMMEDIATE_FLUSH)
								
								SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
								
								SHAKE_CAM(OrbitalCannonLocal.camOrbitalCannon, "GAMEPLAY_EXPLOSION_SHAKE", 1.5)
								
								START_NET_TIMER(OrbitalCannonLocal.sCooldownTimer)
								
								SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
								SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
								
								IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
									OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
									OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
								ENDIF
								
								OrbitalCannonLocal.bFired = TRUE
								
								g_sFM_HEIST_PREP_Telemetry_data.m_OrbitalCannonShots++
								PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT - g_sFM_HEIST_PREP_Telemetry_data.m_OrbitalCannonShots = ", g_sFM_HEIST_PREP_Telemetry_data.m_OrbitalCannonShots)
							ENDIF
						ELIF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 2000)
							IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
								SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
								
								SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
							ENDIF
						ELIF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 1000)
							IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
								SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
								
								SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					BOOL bSkip = FALSE
					
					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_ATTACK)
					AND OrbitalCannonLocal.eWeaponStage > ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						PLAY_SOUND_FRONTEND(-1, "inactive_fire_fail",  "dlc_xm_orbital_cannon_sounds")
					ENDIF
					
					IF NOT OrbitalCannonServer.bJustFired
						IF OrbitalCannonServer.bFiring
							OrbitalCannonServer.bFiring = FALSE
							
							REINIT_NET_TIMER(OrbitalCannonLocal.sSyncDelay)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
						CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, TRUE)
					ENDIF
					
					STOP_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
					
					IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
					ENDIF
					
					IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
						SET_WARNING_MESSAGE_WITH_HEADER("ORB_CAN_WARN1", "ORB_CAN_WARN2", FE_WARNING_NO | FE_WARNING_YES, "ORB_CAN_WARN3")
						
						IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
							PLAY_SOUND_FRONTEND(-1, "menu_select", "dlc_xm_orbital_cannon_sounds", TRUE)
							
							CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, TRUE)
							
							OrbitalCannonLocal.bFired = FALSE
							OrbitalCannonLocal.bRebuildMenu = FALSE
							OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
							OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
							
							CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
						ENDIF
						
						IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
							STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
							STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
							STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
							
							OrbitalCannonServer.bFiring = FALSE
							
							PLAY_SOUND_FRONTEND(-1, "menu_back", "dlc_xm_orbital_cannon_sounds", TRUE)
							
							CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
							
							bSkip = TRUE
						ENDIF
					ENDIF
					
					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
					AND NOT bSkip
					AND NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sKillTracker)
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_NO_TARG")
					AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_DEAD")
						STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
						STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
						
						OrbitalCannonServer.bFiring = FALSE
						
						PLAY_SOUND_FRONTEND(-1, "menu_back", "dlc_xm_orbital_cannon_sounds", TRUE)
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
						
						IF OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
							OrbitalCannonLocal.bFired = FALSE
							OrbitalCannonLocal.bRebuildMenu = FALSE
							OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
							OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
						ELSE
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
						ENDIF
					ENDIF
					
					IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					AND OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						PLAY_SOUND_FRONTEND(-1, "menu_reset", "dlc_xm_orbital_cannon_sounds", true)
						
						CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, TRUE)
						
						CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
						
						SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
						
						SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
					ENDIF
				ENDIF
				
				IF NOT IS_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
					MAINTAIN_CAMERA_MOVEMENT(OrbitalCannonServer, OrbitalCannonLocal)
				ENDIF
			ELSE
				IF IS_BIT_SET(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU)
					CLEAR_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_QUTTING_INITIAL_MENU)
					
					SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF OrbitalCannonServer.bFiring
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
			AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
			AND NOT OrbitalCannonLocal.bBlockFiring
				IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFiringTimer)
					START_NET_TIMER(OrbitalCannonLocal.sFiringTimer)
					
					START_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
					
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
					
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
						SET_VARIABLE_ON_SOUND(OrbitalCannonLocal.iSoundID, "Firing", 0)
						
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
					ENDIF
				ELSE
					IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 3000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 256)
							
							SHAKE_CAM(OrbitalCannonLocal.camOrbitalCannon, "GAMEPLAY_EXPLOSION_SHAKE", 1.5)
							
							SET_VARIABLE_ON_SOUND(OrbitalCannonLocal.iSoundID, "Firing", 1)
							
							OrbitalCannonLocal.bFired = TRUE
							
							START_NET_TIMER(OrbitalCannonLocal.sCooldownTimer)
							
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
						ENDIF
					ELIF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 2000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_2)
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
						ENDIF
					ELIF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 1000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
							SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_STAGE_1)
							
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 1000, 50)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				STOP_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
				
				IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
				ENDIF
				
				IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
					CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, FALSE)
				ENDIF
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				STOP_ORBITAL_CANNON_SOUND(OrbitalCannonLocal)
				
				IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SOUND_VARIABLE)
				ENDIF
				
				IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FIRE_PRESS)
					CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, FALSE)
				ENDIF
				
				STOP_ORBITAL_CANNON_PAN_SOUND(OrbitalCannonLocal)
				STOP_ORBITAL_CANNON_ZOOM_IN_SOUND(OrbitalCannonLocal)
				STOP_ORBITAL_CANNON_ZOOM_OUT_SOUND(OrbitalCannonLocal)
				
				PLAY_SOUND_FRONTEND(-1, "menu_back", "dlc_xm_orbital_cannon_sounds", TRUE)
				
				CLEANUP_FIRING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, FALSE)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
				
				RETURN TRUE
			ENDIF
			
			IF NOT IS_CAM_SHAKING(OrbitalCannonLocal.camOrbitalCannon)
				MAINTAIN_CAMERA_MOVEMENT(OrbitalCannonServer, OrbitalCannonLocal)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Orbital Cannon camera cleanup
/// PARAMS:
///    OrbitalCannonLocal - Local data struct
/// RETURNS:
///    TRUE when the camera has cleanedup
FUNC BOOL CLIENT_CLEANUP_ORBITAL_CANNON_CAMERA(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	PRINTLN("[ORBITAL_CANNON] CLIENT_CLEANUP_ORBITAL_CANNON_CAMERA - Cleaning up Orbital Cannon camera")
	
	IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
	ENDIF
	
	SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the starting position for the Orbital Cannon camera
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
///    propertyID - Simple interior property ID
/// RETURNS:
///    ORBITAL_CANNON_CAMERA_DETAILS - Camera Position, Rotation and FOV above current simple interior
FUNC ORBITAL_CANNON_CAMERA_DETAILS GET_ORBITAL_CANNON_CAMERA_DETAILS(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, SIMPLE_INTERIORS propertyID)
	ORBITAL_CANNON_CAMERA_DETAILS tempCamDetails
	
	IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		SWITCH propertyID
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
				tempCamDetails.vPos = <<1272.9401, 2834.7305, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
				tempCamDetails.vPos = <<34.7610, 2621.1882, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
				tempCamDetails.vPos = <<2755.7183, 3907.1672, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
				tempCamDetails.vPos = <<3389.3667, 5509.0469, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
//			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_5
//				tempCamDetails.vPos = <<-818.6610, 5673.8794, 400.0>>
//				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
//				tempCamDetails.fFOV = 100.0
//			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
				tempCamDetails.vPos = <<19.8769, 6825.1323, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
				tempCamDetails.vPos = <<-2229.3433, 2399.0884, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
				tempCamDetails.vPos = <<-2.5912, 3344.7856, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
				tempCamDetails.vPos = <<2086.2700, 1761.5515, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
				tempCamDetails.vPos = <<1864.7178, 269.0547, 400.0>>
				tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
				tempCamDetails.fFOV = 100.0
			BREAK
		ENDSWITCH
	ELSE
		tempCamDetails.vPos = OrbitalCannonServer.vCamPos
		tempCamDetails.vRot = <<-90.0, 0.0, 0.0>>
		
		SWITCH OrbitalCannonServer.iZoomLevel
			CASE 0
				OrbitalCannonLocal.iZoomLevel = 0
				tempCamDetails.fFOV = 100.0
			BREAK
			
			CASE 1
				OrbitalCannonLocal.iZoomLevel = 1
				tempCamDetails.fFOV = 80.0
			BREAK
			
			CASE 2
				OrbitalCannonLocal.iZoomLevel = 2
				tempCamDetails.fFOV = 60.0
			BREAK
			
			CASE 3
				OrbitalCannonLocal.iZoomLevel = 3
				tempCamDetails.fFOV = 40.0
			BREAK
			
			CASE 4
				OrbitalCannonLocal.iZoomLevel = 4
				tempCamDetails.fFOV = 20.0
			BREAK
		ENDSWITCH
	ENDIF
	
	tempCamDetails.vPos.z = GET_Z_POS(tempCamDetails.vPos)
	
	RETURN tempCamDetails
ENDFUNC

/// PURPOSE:
///    Handles drawing boxes around players in session, and arrows for players offscreen
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
PROC DRAW_PLAYER_TRACKERS(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer)
	PLAYER_INDEX pPlayer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		pPlayer = INT_TO_PLAYERINDEX(i)
		
		IF pPlayer != PLAYER_ID()
		AND pPlayer != INVALID_PLAYER_INDEX()
		AND pPlayer != OrbitalCannonServer.pTargetPlayer
			PED_INDEX pedTemp = GET_PLAYER_PED(pPlayer)
			
			IF DOES_ENTITY_EXIST(pedTemp)
			AND NOT IS_PED_INJURED(pedTemp)
				IF CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(pPlayer)
				AND NOT IS_PLAYER_IN_ARMORY_TRUCK(pPlayer)
				AND NOT IS_PLAYER_IN_ARMORY_AIRCRAFT(pPlayer)
				AND NOT IS_PLAYER_IN_HACKER_TRUCK(pPlayer)
				#IF FEATURE_HEIST_ISLAND
				AND NOT IS_PLAYER_IN_SUBMARINE(pPlayer)
				#ENDIF
					IF HAS_STREAMED_TEXTURE_DICT_LOADED("helicopterhud")
						DRAW_BOX_AROUND_TARGET(pedTemp)
						DRAW_OFFSCREEN_ARROW_FOR_COORD(pedTemp)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Checks that the current targeted player can still be targeted
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
PROC MAINTAIN_TARGET_PLAYER(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	BOOL bBackToMenu = FALSE
	
	IF OrbitalCannonLocal.eWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
		IF NOT OrbitalCannonLocal.bFired
			IF IS_PLAYER_DEAD(OrbitalCannonServer.pTargetPlayer)
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_DEAD")
					OrbitalCannonLocal.bReaquiringTarget = TRUE
					
					PRINT_HELP_FOREVER("ORB_CAN_DEAD")
				ENDIF
			ELSE
				IF NOT CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(OrbitalCannonServer.pTargetPlayer)
					OrbitalCannonLocal.bFired = TRUE
					
					PRINT_HELP("ORB_CAN_NO_TARG", 5000)
				ELSE
					IF OrbitalCannonLocal.bReaquiringTarget
						OrbitalCannonLocal.iMenuCurrentItem = 0
						
						OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
						
						CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
						
						SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_CLOSE_MENU)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
						
						SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
						
						SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
						
						OrbitalCannonLocal.bReaquiringTarget = FALSE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				bBackToMenu = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF OrbitalCannonLocal.bRefund
		IF NOT DOES_PLAYER_HAVE_FREE_SHOT()
			PRINT_HELP("ORB_CAN_REFUND1", 5000)
			
			INT iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_PRICE
			
			IF g_sMPTunables.bOrbitalCannonAutomaticSale
				iRefundPrice = g_sMPTunables.IH2_ORBITAL_CANNON_AUTOMATIC_SALE_PRICE
			ENDIF
			
			IF USE_SERVER_TRANSACTIONS()
				NETWORK_REFUND_CASH_TYPE(iRefundPrice, MP_REFUND_TYPE_ORBITAL_AUTO, MP_REFUND_REASON_NOT_USED, TRUE)
			ELSE
				NETWORK_EARN_TARGET_REFUND(iRefundPrice, 1)
			ENDIF
		ELSE
			PRINT_HELP("ORB_CAN_REFUND2", 5000)
		ENDIF
		
		OrbitalCannonLocal.bRefund = FALSE
	ENDIF
	
	IF bBackToMenu
		INT iPlayerCount = 0
		
		PLAYER_INDEX pPlayer
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			pPlayer = INT_TO_PLAYERINDEX(i)
			
			IF pPlayer != PLAYER_ID()
			AND pPlayer != INVALID_PLAYER_INDEX()
				PED_INDEX pedTemp = GET_PLAYER_PED(pPlayer)
				
				IF DOES_ENTITY_EXIST(pedTemp)
				AND NOT IS_PED_INJURED(pedTemp)
					IF CAN_PLAYER_BE_TARGETTED_BY_ORBITAL_CANNON(pPlayer)
						iPlayerCount++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
		
		OrbitalCannonLocal.bFired = FALSE
		
		SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
		
		IF iPlayerCount <= 0
			OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU
		ELSE
			OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_CURRENT_CANNON_SCALEFORM_STATE(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		SWITCH OrbitalCannonLocal.eWeaponStage
			CASE ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU		RETURN 1	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU		RETURN 1	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM			RETURN 2	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET		RETURN 3	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON		RETURN 4	BREAK
		ENDSWITCH
	ELSE
		SWITCH OrbitalCannonServer.eWeaponStage
			CASE ORBITAL_CANNON_WEAPON_STAGE_INITIAL_MENU		RETURN 2	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU		RETURN 2	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM			RETURN 2	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET		RETURN 3	BREAK
			CASE ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON		RETURN 4	BREAK
		ENDSWITCH
	ENDIF
	
	RETURN 1
ENDFUNC

/// PURPOSE:
///    Processes activating/deactivating the Orbital Cannon
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonLocal - Local data struct
///    propertyID - Simple interior property ID
/// RETURNS:
///    TRUE if the Orbital Cannon is activated/deactivated
FUNC BOOL CLIENT_MAINTAIN_ACTIVATED_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, SIMPLE_INTERIORS propertyID)
	BOOL bFinished = FALSE
	BOOL bReturnTrue = FALSE
	
	CONTROL_ACTION eZoomInControl = INPUT_SCRIPT_RIGHT_AXIS_Y
	CONTROL_ACTION eZoomOutControl = INPUT_SCRIPT_RIGHT_AXIS_Y
	
	ORBITAL_CANNON_CAMERA_DETAILS thisCamera
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		eZoomInControl = INPUT_CURSOR_SCROLL_UP
		eZoomOutControl = INPUT_CURSOR_SCROLL_DOWN
	ENDIF
	
	IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		OrbitalCannonServer.eWeaponStage = OrbitalCannonLocal.eWeaponStage
	ENDIF
	
	SWITCH GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal)
		CASE ORBITAL_CANNON_SWITCH_STATE_FADE_OUT
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(500)
				ENDIF
			ELSE
				OrbitalCannonLocal.iCamSwitchTimer = GET_GAME_TIMER()
				
				IF NOT IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
					OrbitalCannonLocal.bBlockFiring = TRUE
				ENDIF
				
				SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FADE_OUT)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FADE_OUT
			IF IS_SCREEN_FADED_OUT()
				IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
					CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				ENDIF
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SWITCHING_CAMERA)
				
				ENABLE_NIGHTVISION(VISUALAID_OFF)
				
				SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_CLEANUP_CURRENT)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_CLEANUP_CURRENT
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				ANIMPOSTFX_PLAY("MP_OrbitalCannon", 0, TRUE)
				
				OrbitalCannonLocal.iZoomLevel = 0
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_IN)
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOMING_OUT)
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_ZOOM_PRESS)
				
				CLEAR_HELP()
				SET_OVERRIDE_WEATHER("Clear")
				CLEAR_TIMECYCLE_MODIFIER()
				CASCADE_SHADOWS_SET_AIRCRAFT_MODE(TRUE)
				
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
				
				IF (OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
				AND OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET)
				OR (OrbitalCannonLocal.eNewWeaponStage = ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
				AND OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON)
					IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
					AND NOT OrbitalCannonLocal.bFired
						START_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
						
						START_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
					ENDIF
				ENDIF
				
				IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
				AND OrbitalCannonLocal.bActivated
					INT i
					REPEAT NUM_NETWORK_PLAYERS i
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "REMOVE_MENU_ITEM")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
						END_SCALEFORM_MOVIE_METHOD()
					ENDREPEAT
				ENDIF
				
				OrbitalCannonLocal.eWeaponStage = OrbitalCannonLocal.eNewWeaponStage
			ELSE
				ANIMPOSTFX_STOP("MP_OrbitalCannon")
				
				IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
					CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
					CLEAR_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_SHADOWS)
				ENDIF
				
				IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
					OrbitalCannonServer.bReadyToBeObserved = FALSE
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[ORBITAL_CANNON] - CLIENT_MAINTAIN_ACTIVATED_ORBITAL_CANNON - Ready to be observed - FALSE")
					#ENDIF
				ENDIF
				
				g_SimpleInteriorData.bSuppressConcealCheck = FALSE
				FORCE_SIMPLE_INTERIOR_CONCEAL_PLAYERS_CHECK()
				
				CLEAR_HELP()
				
				IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
					CLEAR_OVERRIDE_WEATHER()
					CLEAR_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
				ENDIF
			ENDIF
			
			IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
			AND OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
				
				OrbitalCannonServer.pTargetPlayer = INVALID_PLAYER_INDEX()
			ENDIF
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
				DESTROY_CAM(OrbitalCannonLocal.camOrbitalCannon)
			ENDIF
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
				NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
			ENDIF
			
			SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED
			IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(OrbitalCannonLocal.sfOrbitalCannon)
				
				OrbitalCannonLocal.vLoadSceneLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
				OrbitalCannonLocal.vLoadSceneRot = GET_GAMEPLAY_CAM_ROT()
				bFinished = TRUE
			ELSE
				IF NOT OrbitalCannonLocal.bActivated
					START_AUDIO_SCENE("dlc_xm_orbital_cannon_camera_active_scene")
				ENDIF
				
				IF DOES_PLAYER_HAVE_FREE_SHOT()
					SET_BIT(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_BOTH)
					SET_BIT(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_MANUAL)
					SET_BIT(OrbitalCannonLocal.iPurchaseBS, ORBITAL_CANNON_PURCHASE_BS_AUTO)
				ENDIF
				
				OrbitalCannonLocal.bActivated = TRUE
				OrbitalCannonLocal.sfOrbitalCannon = REQUEST_SCALEFORM_MOVIE("ORBITAL_CANNON_CAM")
				
				IF HAS_SCALEFORM_MOVIE_LOADED(OrbitalCannonLocal.sfOrbitalCannon)
					thisCamera = GET_ORBITAL_CANNON_CAMERA_DETAILS(OrbitalCannonServer, OrbitalCannonLocal, propertyID)
					
					IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
						OrbitalCannonServer.iZoomLevel = 0
					ENDIF
					
					BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_ZOOM_LEVEL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_ZOOM_LEVEL_FLOAT(OrbitalCannonServer, OrbitalCannonLocal, DEFAULT, TRUE))
					END_SCALEFORM_MOVIE_METHOD()
					
					IF NOT (thisCamera.vPos.x = 0.0
					AND thisCamera.vPos.y = 0.0
					AND thisCamera.vPos.z = 0.0)
					AND thisCamera.fFOV <> 0.0
						IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
							DESTROY_CAM(OrbitalCannonLocal.camOrbitalCannon)
						ENDIF
						
						SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
						SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
						
						OrbitalCannonLocal.camOrbitalCannon = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						
						BOOL bDefaultCamPos
						
						bDefaultCamPos = FALSE
						
						IF OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
						AND IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
							bDefaultCamPos = TRUE
						ELSE
							IF OrbitalCannonServer.pTargetPlayer != INVALID_PLAYER_INDEX()
								PED_INDEX pedTemp
								pedTemp = GET_PLAYER_PED(OrbitalCannonServer.pTargetPlayer)
								
								IF DOES_ENTITY_EXIST(pedTemp)
								AND NOT IS_PED_INJURED(pedTemp)
									PLAYER_INDEX pVehicleOwner
									
									VECTOR vPedCoords
									
									IF IS_PLAYER_IN_ARMORY_TRUCK(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_ARMORY_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1])
											vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteTruckV[NATIVE_TO_INT(pVehicleOwner)][1], FALSE)
										ENDIF
									ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_ARMORY_AIRCRAFT_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)])
											vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
										ENDIF
									ELIF IS_PLAYER_IN_HACKER_TRUCK(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_HACKER_TRUCK_THAT_PLAYER_IS_INSIDE(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)])
											vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
										ENDIF
									#IF FEATURE_HEIST_ISLAND
									ELIF IS_PLAYER_IN_SUBMARINE(OrbitalCannonServer.pTargetPlayer)
										pVehicleOwner = GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(OrbitalCannonServer.pTargetPlayer)
										
										IF pVehicleOwner != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(pVehicleOwner)
										AND DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)])
											vPedCoords = GET_ENTITY_COORDS(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(pVehicleOwner)], FALSE)
										ENDIF
									#ENDIF
									ELSE
										vPedCoords = GET_ENTITY_COORDS(pedTemp, FALSE)
									ENDIF
									
									IF vPedCoords.z > (GET_Z_POS(vPedCoords) - 100.0)
										vPedCoords.z = vPedCoords.z + 100.0
									ELSE
										vPedCoords.z = GET_Z_POS(vPedCoords)
									ENDIF
									
									OrbitalCannonLocal.vLoadSceneLocation = vPedCoords
									
									SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, vPedCoords)
								ELSE
									bDefaultCamPos = TRUE
								ENDIF
							ELSE
								bDefaultCamPos = TRUE
							ENDIF
						ENDIF
						
						IF bDefaultCamPos
							IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
								SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, thisCamera.vPos)
								
								OrbitalCannonLocal.vLoadSceneLocation = thisCamera.vPos
							ELSE
								OrbitalCannonLocal.vCurrentCamPos = OrbitalCannonServer.vCamPos
								
								OrbitalCannonLocal.vLoadSceneLocation = OrbitalCannonLocal.vCurrentCamPos
								
								SET_CAM_COORD(OrbitalCannonLocal.camOrbitalCannon, OrbitalCannonServer.vCamPos)
							ENDIF
						ENDIF
						
						SET_CAM_ROT(OrbitalCannonLocal.camOrbitalCannon, thisCamera.vRot)
						SET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon, thisCamera.fFOV)
						
						g_SimpleInteriorData.bSuppressConcealCheck = TRUE
						FORCE_SIMPLE_INTERIOR_CONCEAL_PLAYERS_CHECK()
						
						SET_CAM_ACTIVE(OrbitalCannonLocal.camOrbitalCannon, TRUE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						bFinished = TRUE
					ELSE
						PRINTLN("[ORBITAL_CANNON] - ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED - Camera is at 0")
					ENDIF
				ELSE
					PRINTLN("[ORBITAL_CANNON] - ORBITAL_CANNON_SWITCH_STATE_SETUP_DESIRED - Scaleform loading")
				ENDIF
			ENDIF
			
			IF bFinished
				SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FILTER_TIME)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_FILTER_TIME
			IF GET_GAME_TIMER() - OrbitalCannonLocal.iCamSwitchTimer > SPEC_HUD_FILTER_CHANGE_TIME
				SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_SETUP_LOAD_SCENE)
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_SETUP_LOAD_SCENE
			IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			IF OrbitalCannonLocal.vLoadSceneRot.x = 0.0
			AND OrbitalCannonLocal.vLoadSceneRot.y = 0.0
			AND OrbitalCannonLocal.vLoadSceneRot.z = 0.0
				NEW_LOAD_SCENE_START_SPHERE(OrbitalCannonLocal.vLoadSceneLocation, 300.0)
			ELSE
				NEW_LOAD_SCENE_START(OrbitalCannonLocal.vLoadSceneLocation, OrbitalCannonLocal.vLoadSceneRot, 300)
			ENDIF
			
			SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
			
			SET_FOCUS_POS_AND_VEL(OrbitalCannonLocal.vLoadSceneLocation, <<-90.0, 0.0, 0.0>>)
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
					OrbitalCannonServer.vCamPos = OrbitalCannonLocal.vLoadSceneLocation
				ENDIF
			ENDIF
			
			NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FOCUS_SET)
			
			OrbitalCannonLocal.iCamSwitchTimer = GET_GAME_TIMER()
			
			SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_LOAD_SCENE)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_WAIT_FOR_LOAD_SCENE
			IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sSwitchDelay)
				IF IS_NEW_LOAD_SCENE_LOADED()
				OR GET_GAME_TIMER() - OrbitalCannonLocal.iCamSwitchTimer > 10000
					IF IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
						NEW_LOAD_SCENE_STOP()
						CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_RUNNING)
					ENDIF
					
					START_NET_TIMER(OrbitalCannonLocal.sSwitchDelay)
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sSwitchDelay, 2000)
					RESET_NET_TIMER(OrbitalCannonLocal.sSwitchDelay)
					
					SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_CLEANUP_LOAD_SCENE)
				ENDIF
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_CLEANUP_LOAD_SCENE
			CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
			CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SWITCHING_CAMERA)
			
			OrbitalCannonLocal.vLoadSceneLocation = <<0.0, 0.0, 0.0>>
			OrbitalCannonLocal.vLoadSceneRot = <<0.0, 0.0, 0.0>>
			
			RESET_ADAPTATION()
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				START_ORBITAL_CANNON_BACKGROUND_SOUND(OrbitalCannonLocal)
			ENDIF
			
			SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_FADE_IN)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_FADE_IN
			IF g_iSimpleInteriorState = SIMPLE_INT_STATE_IDLE
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						TOGGLE_RENDERPHASES(TRUE)
						
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ELSE
					SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_CLEANUP_PROCESS)
				ENDIF
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_CLEANUP_PROCESS
			CLEAR_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_LOAD_SCENE_REQUIRED)
			
			OrbitalCannonLocal.vLoadSceneLocation = <<0.0, 0.0, 0.0>>
			OrbitalCannonLocal.vLoadSceneRot = <<0.0, 0.0, 0.0>>
			
			SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
			
			SET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal, ORBITAL_CANNON_SWITCH_STATE_USING_CANNON)
		BREAK
		
		CASE ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
			IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
				MAINTAIN_TARGET_PLAYER(OrbitalCannonServer, OrbitalCannonClient, OrbitalCannonLocal)
			ENDIF
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_NO_TARG")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_DEAD")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_REFUND1")
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ORB_CAN_REFUND2")
				AND IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				
				IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
				AND IS_SCREEN_FADED_IN()
				AND NOT OrbitalCannonServer.bReadyToBeObserved
					OrbitalCannonServer.bReadyToBeObserved = TRUE
					
					#IF IS_DEBUG_BUILD
					PRINTLN("[ORBITAL_CANNON] - CLIENT_MAINTAIN_ACTIVATED_ORBITAL_CANNON - Ready to be observed")
					#ENDIF
				ENDIF
				
				FORCE_ALLOW_TIME_BASED_FADING_THIS_FRAME()
				
				IF NOT IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
				AND NOT OrbitalCannonServer.bFiring
					OrbitalCannonLocal.bBlockFiring = FALSE
				ENDIF
				
				IF OrbitalCannonLocal.eWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
				OR NOT IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
					DRAW_PLAYER_TRACKERS(OrbitalCannonServer)
				ENDIF
			ENDIF
			
			bReturnTrue = TRUE
		BREAK
	ENDSWITCH
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
		AND NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_QUIT_WARNING)
			IF HAS_SCALEFORM_MOVIE_LOADED(OrbitalCannonLocal.sfOrbitalCannon)
				BOOL bSwitchScaleform = FALSE
				
				IF OrbitalCannonLocal.iCurrentScaleformState != GET_CURRENT_CANNON_SCALEFORM_STATE(OrbitalCannonServer, OrbitalCannonLocal)
					IF GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) >= ORBITAL_CANNON_SWITCH_STATE_CLEANUP_CURRENT
						bSwitchScaleform = TRUE
						
						OrbitalCannonLocal.iCurrentScaleformState = GET_CURRENT_CANNON_SCALEFORM_STATE(OrbitalCannonServer, OrbitalCannonLocal)
						
						IF NOT IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
							IF (OrbitalCannonLocal.iCurrentScaleformState = 3
							OR OrbitalCannonLocal.iCurrentScaleformState = 4)
							AND NOT OrbitalCannonLocal.bFired
								IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
									START_NET_TIMER(OrbitalCannonLocal.sInitialDelay)
									
									START_ORBITAL_CANNON_CHARGING_SOUND(OrbitalCannonLocal)
								ENDIF
							ENDIF
							
							IF OrbitalCannonLocal.iCurrentScaleformState = 0
							OR OrbitalCannonLocal.iCurrentScaleformState = 1
							OR OrbitalCannonLocal.iCurrentScaleformState = 2
								OrbitalCannonLocal.bFired = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bSwitchScaleform
					BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_STATE")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CURRENT_CANNON_SCALEFORM_STATE(OrbitalCannonServer, OrbitalCannonLocal))
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFiringTimer)
					IF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 1000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(3)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ELIF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 2000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ELIF NOT HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFiringTimer, 3000)
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							END_SCALEFORM_MOVIE_METHOD()
							
							SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COUNTDOWN")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
						END_SCALEFORM_MOVIE_METHOD()
						
						SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_0)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_1)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_2)
						CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_COUNTDOWN_3)
					ENDIF
				ENDIF
				
				IF OrbitalCannonLocal.iCurrentScaleformState = 3
				OR OrbitalCannonLocal.iCurrentScaleformState = 4
					IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
					AND NOT OrbitalCannonServer.bFired
						BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_CHARGING_LEVEL")
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(OrbitalCannonLocal.sInitialDelay) / 3000.0)
						END_SCALEFORM_MOVIE_METHOD()
						
						IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
						ENDIF
						
						IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
							CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
						ENDIF
					ELSE
						IF NOT OrbitalCannonLocal.bFired
						AND NOT OrbitalCannonServer.bFired
							IF GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) = ORBITAL_CANNON_SWITCH_STATE_USING_CANNON
								IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
									BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_CHARGING_LEVEL")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1.0)
									END_SCALEFORM_MOVIE_METHOD()
									
									SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
									
									IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
										CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
								CLEAR_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_CHARGING_FULL)
							ENDIF
							
							IF NOT IS_BIT_SET(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
								BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SHOW_COOLDOWN_METER")
								END_SCALEFORM_MOVIE_METHOD()
								
								BEGIN_SCALEFORM_MOVIE_METHOD(OrbitalCannonLocal.sfOrbitalCannon, "SET_COOLDOWN_LEVEL")
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0.0)
								END_SCALEFORM_MOVIE_METHOD()
								
								SET_BIT(OrbitalCannonLocal.iScaleformBS, ORBITAL_CANNON_SCALEFORM_BS_INACTIVE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
				DRAW_SCALEFORM_MOVIE_FULLSCREEN(OrbitalCannonLocal.sfOrbitalCannon, 255, 255, 255, 0, 1)
				RESET_SCRIPT_GFX_ALIGN()
				
				IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_SWITCHING_CAMERA)
					IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
					OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
						REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(OrbitalCannonLocal.scaleformInstructionalButtons)
						
						IF (OrbitalCannonLocal.eWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_FREE_CAM
						OR NOT IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer))
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL), "HUD_INPUT3", OrbitalCannonLocal.scaleformInstructionalButtons)
							
							IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
								IF OrbitalCannonLocal.eWeaponStage != ORBITAL_CANNON_WEAPON_STAGE_TARGET_LOCK_ON
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_FRONTEND_X), "ORB_CAN_RE", OrbitalCannonLocal.scaleformInstructionalButtons)
								ENDIF
								
								IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eZoomOutControl), "ORB_CAN_ZOOMO", OrbitalCannonLocal.scaleformInstructionalButtons)
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eZoomInControl), "ORB_CAN_ZOOMI", OrbitalCannonLocal.scaleformInstructionalButtons)
								ELSE
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, eZoomInControl), "ORB_CAN_ZOOM", OrbitalCannonLocal.scaleformInstructionalButtons)
								ENDIF
								
								IF OrbitalCannonLocal.eWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
								AND OrbitalCannonLocal.eNewWeaponStage >= ORBITAL_CANNON_WEAPON_STAGE_ACQUIRE_TARGET
								AND NOT OrbitalCannonLocal.bFired
								AND NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sInitialDelay)
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_ATTACK), "ORB_CAN_FIRE", OrbitalCannonLocal.scaleformInstructionalButtons)
								ENDIF
							ENDIF
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "ITEM_SELECT", OrbitalCannonLocal.scaleformInstructionalButtons)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "ITEM_BACK", OrbitalCannonLocal.scaleformInstructionalButtons)
						ENDIF
						
						CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_UPDATE_BUTTONS)
					ENDIF
					
					SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
					SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(OrbitalCannonLocal.scaleformInstructionalButtons, 1.0)
					RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(OrbitalCannonLocal.sfButton, thisSpritePlacement, OrbitalCannonLocal.scaleformInstructionalButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(OrbitalCannonLocal.scaleformInstructionalButtons))
					SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturnTrue
ENDFUNC

/// PURPOSE:
///    Processes player in interaction mode with the Orbital Cannon
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonClient - Client data struct
///    OrbitalCannonLocal - Local data struct
///    propertyID - Simple interior property ID
/// RETURNS:
///    TRUE when player wants to stop interacting with the Orbital Cannon
FUNC BOOL CLIENT_MAINTAIN_USING_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, SIMPLE_INTERIORS propertyID)
	DISABLE_DPADDOWN_THIS_FRAME()
	DISPLAY_AMMO_THIS_FRAME(FALSE)
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF NOT IS_RADAR_HIDDEN()
		DISPLAY_RADAR(FALSE)
		
		SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_RADAR)
	ENDIF
	
	IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP)
		IF NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
		AND GET_ORBITAL_CANNON_SWITCH_STATE(OrbitalCannonLocal) >= ORBITAL_CANNON_SWITCH_STATE_CLEANUP_LOAD_SCENE
		AND IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_FORCE_SCALEFORM_DRAW)
		AND HAS_SCALEFORM_MOVIE_LOADED(OrbitalCannonLocal.sfOrbitalCannon)
		AND NOT IS_BIT_SET(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
		AND OrbitalCannonLocal.eWeaponStage <= ORBITAL_CANNON_WEAPON_STAGE_PLAYER_MENU
			SETUP_MENU_CONTROLS(OrbitalCannonLocal)
			BUILD_MENU(OrbitalCannonLocal)
			UPDATE_MENU(OrbitalCannonServer, OrbitalCannonClient, OrbitalCannonLocal)
		ENDIF
		
		IF CLIENT_MAINTAIN_ACTIVATED_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonClient, OrbitalCannonLocal, propertyID)
			IF CLIENT_MAINTAIN_ORBITAL_CANNON_INPUT(OrbitalCannonServer, OrbitalCannonClient, OrbitalCannonLocal, propertyID)
			OR bInPropertyOwnerNotPaidLastUtilityBill
			OR g_bCelebrationScreenIsActive
			OR OrbitalCannonServer.pUser = INVALID_PLAYER_INDEX()
			OR g_bDisableOrbitalCannon
			OR g_bDisableOrbitalCannonBG
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Closing the Orbital Cannon")
				
				IF IS_BIT_SET(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
					CLEAR_OVERRIDE_WEATHER()
					CLEAR_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_WEATHER)
				ENDIF
				
				CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
				
				SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_DEACTIVATING)
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP)
			ENDIF
		ENDIF
	ELSE
		IF CLIENT_MAINTAIN_ACTIVATED_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonClient, OrbitalCannonLocal, propertyID)
			CLIENT_CLEANUP_ORBITAL_CANNON_CAMERA(OrbitalCannonLocal)
			
			CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_USE_CANNON_CLEANUP)
			
			IF IS_LOCAL_PLAYER_CONTROLLING_ORBITAL_CANNON(OrbitalCannonServer)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(OrbitalCannonLocal.iSceneID))
					DETACH_SYNCHRONIZED_SCENE(NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(OrbitalCannonLocal.iSceneID))
					OrbitalCannonLocal.iSceneID = -1
				ENDIF
				
				START_ORBITAL_CANNON_SYNC_SCENE(OrbitalCannonLocal, "Exit")
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_LOD_SCALE_VALUE(ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal)
	FLOAT fFOV = 100.0
	
	IF DOES_CAM_EXIST(OrbitalCannonLocal.camOrbitalCannon)
		fFOV = GET_CAM_FOV(OrbitalCannonLocal.camOrbitalCannon)
		
		IF fFOV < 30.0
			RETURN 1.6
		ELIF fFOV < 50.0
			RETURN 1.45
		ELIF fFOV < 70.0
			RETURN 1.3
		ELIF fFOV < 90.0
			RETURN 1.15
		ELSE
			RETURN 1.0
		ENDIF
	ENDIF
	
	RETURN 1.0
ENDFUNC

/// PURPOSE:
///    Client every frame update to be called by all players in the simple interior to handle displaying and interacting with the Orbital Cannon
/// PARAMS:
///    OrbitalCannonServer - Server data struct
///    OrbitalCannonClient - Client data struct
///    OrbitalCannonLocal - Local data struct
///    propertyID - Simple interior property ID
PROC CLIENT_MAINTAIN_ORBITAL_CANNON(ORBITAL_CANNON_SERVER_DATA_STRUCT &OrbitalCannonServer, ORBITAL_CANNON_CLIENT_DATA_STRUCT &OrbitalCannonClient, ORBITAL_CANNON_LOCAL_DATA_STRUCT &OrbitalCannonLocal, SIMPLE_INTERIORS propertyID)
	SWITCH OrbitalCannonClient.eStage
		CASE ORBITAL_CANNON_CLIENT_STAGE_INIT
			PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Initialising Orbital Cannon")
			
			SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_WALKING)
		BREAK
		
		CASE ORBITAL_CANNON_CLIENT_STAGE_WALKING
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Enabling kill yourself option")
				
				ENABLE_KILL_YOURSELF_OPTION()
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF IS_BIT_SET(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Enabling interaction menu")
				
				ENABLE_INTERACTION_MENU()
				
				CLEAR_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			IF CLIENT_MAINTAIN_ACTIVATING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal)
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Player is activating the Orbital Cannon")
				
				IF OrbitalCannonServer.pOwner = PLAYER_ID()
					SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ANIM)
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipOrbitalCannonCooldown")
						SET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN, 0)
					ENDIF
					#ENDIF
				ELSE
					PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Player is activating the Orbital Cannon")
					
					CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
					
					SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
					
					REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
					
					SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
					
					SET_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
				ENDIF
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_CLIENT_STAGE_ANIM
			SET_IDLE_KICK_DISABLED_THIS_FRAME()
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Disabling player control")
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
				
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_CONTROLS)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Disabling kill yourself option")
				
				DISABLE_KILL_YOURSELF_OPTION()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF NOT IS_INTERACTION_MENU_DISABLED()
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Disabling interaction menu")
				
				DISABLE_INTERACTION_MENU()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_ANIM_RUNNING)
				IF NOT IS_BIT_SET(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_GO_TO_TASK)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ORBITAL_CANNON_GO_TO_LOCATION(), PEDMOVEBLENDRATIO_WALK, 5000, GET_ORBITAL_CANNON_GO_TO_HEADING(), 0.01)
						SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_GO_TO_TASK)
						
						REQUEST_ANIM_DICT("anim@AMB@FACILITY@MISSLE_CONTROLROOM@")
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
						IF HAS_ANIM_DICT_LOADED("anim@AMB@FACILITY@MISSLE_CONTROLROOM@")
							START_ORBITAL_CANNON_SYNC_SCENE(OrbitalCannonLocal, "Enter")
							
							SET_BIT(OrbitalCannonLocal.iBitSet, ORBITAL_CANNON_LOCAL_BS_ANIM_RUNNING)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				INT iLocalSceneID
				
				iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(OrbitalCannonLocal.iSceneID)
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
				OR GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= 0.96
					IF IS_SYNCHRONIZED_SCENE_RUNNING(OrbitalCannonLocal.iSceneID)
						DETACH_SYNCHRONIZED_SCENE(OrbitalCannonLocal.iSceneID)
						OrbitalCannonLocal.iSceneID = -1
					ENDIF
					
					START_ORBITAL_CANNON_SYNC_SCENE(OrbitalCannonLocal, "IDLE", TRUE)
					
					IF NOT GB_IS_BOSS_FF_TOGGLED_ON()
						SET_FRIENDLY_FIRE(TRUE)
					ENDIF
					
					CLIENT_SETUP_SWITCH(OrbitalCannonLocal)
					
					SET_BIT(OrbitalCannonLocal.iSwitchBitset, ORBITAL_CANNON_SWITCH_BS_ACTIVATING)
					
					REQUEST_STREAMED_TEXTURE_DICT("helicopterhud")
					
					SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED)
					
					SET_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
				ENDIF
			ENDIF
		BREAK
		
		CASE ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED
			IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sKillTracker)
			AND HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sKillTracker, 15000)
				BOOL bRefund
				bRefund = FALSE
				
				IF OrbitalCannonLocal.bFiringOnVehicle
					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_KILLS) <= OrbitalCannonLocal.iKillCounter
					AND DOES_ENTITY_EXIST(OrbitalCannonLocal.vTarget)
					AND IS_VEHICLE_DRIVEABLE(OrbitalCannonLocal.vTarget)
						bRefund = TRUE
					ENDIF
				ELSE
					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_KILLS) <= OrbitalCannonLocal.iKillCounter
						bRefund = TRUE
					ENDIF
				ENDIF
				
				IF bRefund
				AND NOT g_bDisableOrbitalCannonRefund
					OrbitalCannonLocal.bRefund = TRUE
					
					SET_MP_INT_CHARACTER_STAT(MP_STAT_ORBITAL_CANNON_COOLDOWN, 0)
				ELSE
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_XMAS2017_CANNON_GIFT)
					AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PURCHASE_XMAS2017_CANNON_GIFT)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PURCHASE_XMAS2017_CANNON_GIFT, TRUE)
					ENDIF
				ENDIF
				
				RESET_NET_TIMER(OrbitalCannonLocal.sKillTracker)
				
				REQUEST_SAVE(SSR_REASON_ORBITAL_CANNON, STAT_SAVETYPE_IMMEDIATE_FLUSH)
			ENDIF
			
			SET_IDLE_KICK_DISABLED_THIS_FRAME()
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Disabling player control")
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
				
				SET_BIT(OrbitalCannonLocal.iControlBS, ORBITAL_CANNON_CONTROL_BS_SET_CONTROLS)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
				ENDIF
			ENDIF
			
			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Disabling kill yourself option")
				
				DISABLE_KILL_YOURSELF_OPTION()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			
			IF NOT IS_INTERACTION_MENU_DISABLED()
				PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Disabling interaction menu")
				
				DISABLE_INTERACTION_MENU()
				
				SET_BIT(OrbitalCannonLocal.iBitset, ORBITAL_CANNON_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			
			IF HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFeedTimer)
				IF HAS_NET_TIMER_EXPIRED(OrbitalCannonLocal.sFeedTimer, 6000)
					RESET_NET_TIMER(OrbitalCannonLocal.sFeedTimer)
				ENDIF
			ENDIF
			
			IF NOT HAS_NET_TIMER_STARTED(OrbitalCannonLocal.sFeedTimer)
				THEFEED_HIDE_THIS_FRAME()
			ENDIF
			
			IF CLEAN_UP_ON_CALL_WITH_OUT_WAIT()
				IF CLIENT_MAINTAIN_USING_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonClient, OrbitalCannonLocal, propertyID)
					PRINTLN("[ORBITAL_CANNON] CLIENT_MAINTAIN_ORBITAL_CANNON - Player chose to exit the Orbital Cannon")
					
					REMOVE_ANIM_DICT("anim@AMB@FACILITY@MISSLE_CONTROLROOM@")
					
					TIDYUP_ORBITAL_CANNON(OrbitalCannonServer, OrbitalCannonLocal, TRUE, TRUE)
					
					SET_ORBITAL_CANNON_CLIENT_STAGE(OrbitalCannonClient, ORBITAL_CANNON_CLIENT_STAGE_WALKING)
				ENDIF
			ENDIF
			
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		BREAK
	ENDSWITCH
	
	IF OrbitalCannonClient.eStage = ORBITAL_CANNON_CLIENT_STAGE_ACTIVATED
		SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()
		OVERRIDE_LODSCALE_THIS_FRAME(GET_LOD_SCALE_VALUE(OrbitalCannonLocal))
		USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
		SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(GET_FINAL_RENDERED_CAM_COORD())
		
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	ENDIF
ENDPROC
