//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        warp_transition.sch																				
/// Description: Header for launching the a warp transition
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		22/01/2020																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "warp_transition_struct.sch"
USING "rc_helper_functions.sch"
USING "warp_transition_globals.sch"
USING "freemode_header.sch"

/// PURPOSE: 
///    Time to wait after warp (fake load delay)
///    TODO: make this configurable
CONST_INT ciWARP_TRANSITION__LOAD_DELAY 3000

/// PURPOSE: 
///    Enum state of launching a warp transition cutscene
ENUM _WT__CUTSCENE_SCRIPT_LAUNCH_STATE
	_WT_CSLS__NOT_ASSIGNED
	,_WT_CSLS__SUCCESS
	,_WT_CSLS__LOADING
	,_WT_CSLS__FAILED
ENDENUM

///-----------------------
///    HELPERS
///-----------------------

/// PURPOSE:
///    Loads and launches the leave cinematic and stores its thread id as the leave cutscene script thread ID.
FUNC _WT__CUTSCENE_SCRIPT_LAUNCH_STATE _WT__TRY_LAUNCH_LEAVE_CUTSCENE_SCRIPT(WARP_TRANSITION_STRUCT &sInst)
	// The script is already running so don't try again
	IF IS_THREAD_ACTIVE(WARP_TRANSITION__GET_LEAVE_CUTSCENE_SCRIPT_THREAD_ID(sInst))
		RETURN _WT_CSLS__SUCCESS
	ENDIF
	
	INT iScriptNameHash = WARP_TRANSITION_CONFIG__GET_LEAVE_CUTSCENE_SCRIPT_HASH(sInst.sConfigData)
	
	// A script cinematic hasn't been assigned
	IF iScriptNameHash = -1
		RETURN _WT_CSLS__NOT_ASSIGNED
	ENDIF
	
	// Make sure the script exists and throw an assert if it doesn't
	IF NOT DOES_SCRIPT_WITH_NAME_HASH_EXIST(iScriptNameHash)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_LEAVE_CUTSCENE_SCRIPT - Script does not exist, script hash name: ", iScriptNameHash)
		ASSERTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_LEAVE_CUTSCENE_SCRIPT - Script does not exist, script hash name: ", iScriptNameHash)
		RETURN _WT_CSLS__FAILED
	ENDIF
	
	REQUEST_SCRIPT_WITH_NAME_HASH(iScriptNameHash)
	
	IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(iScriptNameHash)
		RETURN _WT_CSLS__LOADING
	ENDIF
	
	DISABLE_INTERACTION_MENU()
	
	// Launch script and store the thread ID so we can check that it finishes later
	THREADID tiThreadId
	INT iStackSize = WARP_TRANSITION_CONFIG__GET_LEAVE_CUTSCENE_SCRIPT_STACK_SIZE(sInst.sConfigData)
	
	IF sInst.sConfigData.bLeaveCutsceneHasArgs // requires config args
		WARP_TRANSITION_CUTSCENE_ARGS sArgs
		sArgs.eSections = sInst.sConfigData.eLeaveSections
		sArgs.vArriveLocation = sInst.sConfigData.vArriveLocation
		sArgs.fArriveHeading = sInst.sConfigData.fArriveHeading
		sArgs.eArriveOutfit = sInst.sConfigData.eArriveOutfit
		sArgs.eArriveBossOutfit = sInst.sConfigData.eArriveBossOutfit
				
		tiThreadId = START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(iScriptNameHash, sArgs, SIZE_OF(sArgs), iStackSize)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_LEAVE_CUTSCENE_SCRIPT - Launched script with args and name hash: ", iScriptNameHash)
	ELSE
		tiThreadId = START_NEW_SCRIPT_WITH_NAME_HASH(iScriptNameHash, iStackSize)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_LEAVE_CUTSCENE_SCRIPT - Launched script with name hash: ", iScriptNameHash)
	ENDIF
	
	_WT__SET_LEAVE_CUTSCENE_SCRIPT_THREAD_ID(sInst, tiThreadId)	
	SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(iScriptNameHash)
	
	RETURN _WT_CSLS__SUCCESS
ENDFUNC

PROC _WT__KILL_LEAVE_CUTSCENE_SCRIPT(WARP_TRANSITION_STRUCT &sInst)
	THREADID tiScriptNameHash = WARP_TRANSITION__GET_LEAVE_CUTSCENE_SCRIPT_THREAD_ID(sInst)
	IF IS_THREAD_ACTIVE(tiScriptNameHash)
		TERMINATE_THREAD(tiScriptNameHash)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] KILL_LEAVE_CUTSCENE_SCRIPT - Killed script")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the leave cutscene has finished 
/// RETURNS:
///    TRUE if the cutscene thread is no longer active or if the thread ID is null
///    FALSE if the thread is active
FUNC BOOL _WT__HAS_LEAVE_CUTSCENE_FINISHED(WARP_TRANSITION_STRUCT &sInst)
	THREADID tiLeaveCutscene = WARP_TRANSITION__GET_LEAVE_CUTSCENE_SCRIPT_THREAD_ID(sInst)
	RETURN tiLeaveCutscene = NULL OR NOT IS_THREAD_ACTIVE(tiLeaveCutscene)
ENDFUNC

/// PURPOSE:
///    Checks if warp transitions are blocked globaly by a tuneable  
FUNC BOOL _WT__ARE_ALL_WARP_TRANSITIONS_BLOCKED() 
	RETURN  g_sMPTunables.bBlockAllWarpTransitions
ENDFUNC

/// PURPOSE:
///    Loads and launches the arrival cinematic and stores its thread id as the leave cutscene script thread ID.
FUNC _WT__CUTSCENE_SCRIPT_LAUNCH_STATE _WT__TRY_LAUNCH_ARRIVE_CUTSCENE_SCRIPT(WARP_TRANSITION_STRUCT &sInst)
	// The script is already running so don't try again
	IF IS_THREAD_ACTIVE(WARP_TRANSITION__GET_ARRIVE_CUTSCENE_SCRIPT_THREAD_ID(sInst))
		RETURN _WT_CSLS__SUCCESS
	ENDIF
	
	INT iScriptNameHash = WARP_TRANSITION_CONFIG__GET_ARRIVE_CUTSCENE_SCRIPT_HASH(sInst.sConfigData)
	
	// A script cinematic hasn't been assigned
	IF iScriptNameHash = -1
		RETURN _WT_CSLS__NOT_ASSIGNED
	ENDIF
	
	// Make sure the script exists and throw an assert if it doesn't
	IF NOT DOES_SCRIPT_WITH_NAME_HASH_EXIST(iScriptNameHash)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_ARRIVE_CUTSCENE_SCRIPT - Script does not exist, script hash name: ", iScriptNameHash)
		ASSERTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_ARRIVE_CUTSCENE_SCRIPT - Script does not exist, script hash name: ", iScriptNameHash)
		RETURN _WT_CSLS__FAILED
	ENDIF
	
	REQUEST_SCRIPT_WITH_NAME_HASH(iScriptNameHash)
	
	IF NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(iScriptNameHash)
		RETURN _WT_CSLS__LOADING
	ENDIF
	
	DISABLE_INTERACTION_MENU()
	
	// Launch script and store the thread ID so we can check that it finishes later
	THREADID tiThreadId
	INT iStackSize = WARP_TRANSITION_CONFIG__GET_ARRIVE_CUTSCENE_SCRIPT_STACK_SIZE(sInst.sConfigData)
	
	IF sInst.sConfigData.bArriveCutsceneHasArgs // requires config args
		WARP_TRANSITION_CUTSCENE_ARGS sArgs
		sArgs.eSections = sInst.sConfigData.eArriveSections
		sArgs.vArriveLocation = sInst.sConfigData.vArriveLocation
		sArgs.fArriveHeading = sInst.sConfigData.fArriveHeading
		sArgs.eArriveOutfit = sInst.sConfigData.eArriveOutfit
		sArgs.eArriveBossOutfit = sInst.sConfigData.eArriveBossOutfit
		
		tiThreadId = START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(iScriptNameHash, sArgs, SIZE_OF(sArgs), iStackSize)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_ARRIVE_CUTSCENE_SCRIPT - Launched script with args and name hash: ", iScriptNameHash)
	ELSE
		tiThreadId = START_NEW_SCRIPT_WITH_NAME_HASH(iScriptNameHash, iStackSize)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] TRY_LAUNCH_ARRIVE_CUTSCENE_SCRIPT - Launched script with name hash: ", iScriptNameHash)
	ENDIF
	
	_WT__SET_ARRIVE_CUTSCENE_SCRIPT_THREAD_ID(sInst, tiThreadId)
	SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(iScriptNameHash)
	
	RETURN _WT_CSLS__SUCCESS
ENDFUNC

PROC _WT__KILL_ARRIVE_CUTSCENE_SCRIPT(WARP_TRANSITION_STRUCT &sInst)
	THREADID tiScriptNameHash = WARP_TRANSITION__GET_ARRIVE_CUTSCENE_SCRIPT_THREAD_ID(sInst)
	IF IS_THREAD_ACTIVE(tiScriptNameHash)
		TERMINATE_THREAD(tiScriptNameHash)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] KILL_ARRIVE_CUTSCENE_SCRIPT - Killed script")
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the arrive cutscene has finished 
/// RETURNS:
///    TRUE if the cutscene thread is no longer active or if the thread ID is null
///    FALSE if the thread is active
FUNC BOOL _WT__HAS_ARRIVE_CUTSCENE_FINISHED(WARP_TRANSITION_STRUCT &sInst)
	THREADID tiArriveCutscene = WARP_TRANSITION__GET_ARRIVE_CUTSCENE_SCRIPT_THREAD_ID(sInst)
	RETURN tiArriveCutscene = NULL OR NOT IS_THREAD_ACTIVE(tiArriveCutscene)
ENDFUNC

PROC _WT__KILL_ALL_CUTSCENE_SCRIPTS(WARP_TRANSITION_STRUCT &sInst)
	_WT__KILL_ARRIVE_CUTSCENE_SCRIPT(sInst)
	_WT__KILL_LEAVE_CUTSCENE_SCRIPT(sInst)
ENDPROC

/// PURPOSE:
///    Moves the player to the arrive location if it has been set
PROC _WT__MOVE_PLAYER_TO_ARRIVE_LOCATION(WARP_TRANSITION_STRUCT &sInst)
	VECTOR vWarpLocation = WARP_TRANSITION_CONFIG__GET_ARRIVE_LOCATION(sInst.sConfigData)
	
	// Move the player if arrive location is set
	IF ARE_VECTORS_ALMOST_EQUAL(<<0,0,0>>, vWarpLocation)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] MOVE_PLAYER_TO_ARRIVE_LOCATION - No arrive location set not warping player")
		EXIT
	ENDIF
	
	PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] MOVE_PLAYER_TO_ARRIVE_LOCATION - Moving player to ", vWarpLocation)
	
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
	SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpLocation)
	REQUEST_COLLISION_AT_COORD(vWarpLocation)
    LOAD_ALL_OBJECTS_NOW()
ENDPROC

/// PURPOSE:
///    Sets the players rotation to the arrive heading if it has been set
PROC _WT__ROTATE_PLAYER_TO_ARRIVE_HEADING(WARP_TRANSITION_STRUCT &sInst)
	FLOAT fHeading 
	IF fHeading != _cfWARP_TRANSITION_INVALID_HEADING
		SET_ENTITY_HEADING(PLAYER_PED_ID(), WARP_TRANSITION_CONFIG__GET_ARRIVE_HEADING(sInst.sConfigData))
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0) 
	ELSE
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] ROTATE_PLAYER_TO_ARRIVE_HEADING - No heading set not rotating player")
	ENDIF
ENDPROC

FUNC BOOL _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER(VECTOR vConflictedLocation, PLAYER_INDEX &piRemotePlayer)
	CONST_FLOAT cfWARP_TRANSITION_ARRIVAL_CONFLICT_RANGE 1.0
	PED_INDEX piPed = GET_PLAYER_PED(piRemotePlayer)	
	
	// If the player is already there we should concede the spot
	IF IS_ENTITY_AT_COORD(piPed, vConflictedLocation, <<cfWARP_TRANSITION_ARRIVAL_CONFLICT_RANGE, cfWARP_TRANSITION_ARRIVAL_CONFLICT_RANGE, cfWARP_TRANSITION_ARRIVAL_CONFLICT_RANGE>>, FALSE, FALSE)
		PRINTLN("[WARP_TRANSITION] _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER - Player ", NATIVE_TO_INT(piRemotePlayer), " is already at location ", vConflictedLocation, "! Returning TRUE")
		RETURN TRUE
	ENDIF
	
	// If the player isn't in a warp transition they don't need the spot
	IF NOT IS_PLAYER_IN_WARP_TRANSITION(piRemotePlayer)
		PRINTLN("[WARP_TRANSITION] _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER - Player ", NATIVE_TO_INT(piRemotePlayer), " is not in warp transition. Returning FALSE")
		RETURN FALSE
	ENDIF
	
	TRANSFORM_STRUCT sRemotePlayersArriveLocation = GET_PLAYERS_CURRENT_WARP_TRANSITION_ARRIVE_LOCATION(piRemotePlayer)
	
	// If the player is in a transition but they aren't attempting to arrive at this location there is no conflict
	IF NOT ARE_VECTORS_ALMOST_EQUAL(vConflictedLocation, sRemotePlayersArriveLocation.Position, cfWARP_TRANSITION_ARRIVAL_CONFLICT_RANGE)
		PRINTLN("[WARP_TRANSITION] _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER - Player ", NATIVE_TO_INT(piRemotePlayer), " is not set to arrive at location ", vConflictedLocation, ". Returning FALSE")
		RETURN FALSE
	ENDIF
	
	/// If there is a conflict the player with the highest id gets the spot, unless the other player has already locked it in as
	/// their valid arrive location
	IF NATIVE_TO_INT(PLAYER_ID()) > NATIVE_TO_INT(piRemotePlayer) AND NOT IS_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_LOCKED_IN(piRemotePlayer)
		PRINTLN("[WARP_TRANSITION] _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER - Player ", NATIVE_TO_INT(piRemotePlayer), " has lower priority, and is not locked in. Returning FALSE")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[WARP_TRANSITION] _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER - Player ", NATIVE_TO_INT(piRemotePlayer), " is already set to arrive at ", vConflictedLocation, "! Returning TRUE")
	RETURN TRUE
ENDFUNC

FUNC BOOL _WT__IS_ARRIVE_COORD_VALID(VECTOR vCoord)
	INT iMaxParticipant = NETWORK_GET_NUM_PARTICIPANTS()
	INT iParticipant = 0
	PLAYER_INDEX piLocalPlayer = PLAYER_ID()
	PARTICIPANT_INDEX piParticipantIndex
	PLAYER_INDEX piRemotePlayer	
	
	REPEAT iMaxParticipant iParticipant	
		piParticipantIndex = INT_TO_PARTICIPANTINDEX(iParticipant)	
		
		// Ignore inactive
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piParticipantIndex)
			RELOOP
		ENDIF
		
		IF piRemotePlayer = piLocalPlayer
			RELOOP
		ENDIF
		
		// Only include valid players
		IF NOT IS_NET_PLAYER_OK(piRemotePlayer)
			RELOOP
		ENDIF
		
		piRemotePlayer = NETWORK_GET_PLAYER_INDEX(piParticipantIndex)
		
		// If we need to concede break the loop and get a better position
		IF _WT__SHOULD_CONCEDE_WARP_ARRIVE_LOCATION_TO_PLAYER(vCoord, piRemotePlayer)
			RETURN FALSE
		ENDIF
		
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC _WT__VALIDATE_ARRIVE_LOCATION(WARP_TRANSITION_STRUCT &sInst)
	// We may use this again during the transition so make sure we only validate once
	IF IS_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_LOCKED_IN(PLAYER_ID())
		PRINTLN("[WARP_TRANSITION] VALIDATE_ARRIVE_LOCATION - already validated position")
		EXIT
	ENDIF
	
	// There are no backup positions so whatever happens happens
	IF NOT sInst.sConfigData.bHasBackupPositions
		PRINTLN("[WARP_TRANSITION] VALIDATE_ARRIVE_LOCATION - no backup positions assigned")
		_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(TRUE)
		EXIT
	ENDIF
	
	VECTOR vOrginalArriveLocation = WARP_TRANSITION_CONFIG__GET_ARRIVE_LOCATION(sInst.sConfigData)
	
	// The assigned position is valid so go with that
	IF _WT__IS_ARRIVE_COORD_VALID(vOrginalArriveLocation)
		PRINTLN("[WARP_TRANSITION] VALIDATE_ARRIVE_LOCATION - Original position is valid")
		_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(TRUE)
		EXIT
	ENDIF
	
	INT iBackupPosition
	INT iMaxBackupPositions = COUNT_OF(sInst.sConfigData.sBackupPositions)
	TRANSFORM_STRUCT sNewTransform
	INT iSelectedPosition = -1
	
	REPEAT iMaxBackupPositions iBackupPosition
		sNewTransform = sInst.sConfigData.sBackupPositions[iBackupPosition]
		
		IF _WT__IS_ARRIVE_COORD_VALID(sNewTransform.Position)
			iSelectedPosition = iBackupPosition
			PRINTLN("[WARP_TRANSITION] VALIDATE_ARRIVE_LOCATION - Found valid backup position")
			BREAKLOOP
		ENDIF
		
	ENDREPEAT
	
	// No backup positions are available so just let them use the original
	IF iSelectedPosition = -1
		PRINTLN("[WARP_TRANSITION] VALIDATE_ARRIVE_LOCATION - Failed to find free backup position, defaulting to original")
		_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(TRUE)
		EXIT
	ENDIF
	
	// We have a better position so use that instead and lock it in so no one else tries to take it
	WARP_TRANSITION_CONFIG__SET_ARRIVE_LOCATION(sInst.sConfigData, sNewTransform.Position)
	WARP_TRANSITION_CONFIG__SET_ARRIVE_HEADING(sInst.sConfigData, sNewTransform.Rotation.z)
	sInst.sConfigData.iBackupPositionIndexUsed = iSelectedPosition
	_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(iSelectedPosition)
	_SET_LOCAL_PLAYERS_CURRENT__WARP_TRANSITION_ARRIVE_LOCATION(sNewTransform)
	_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(TRUE)
	PRINTLN("[WARP_TRANSITION] VALIDATE_ARRIVE_LOCATION - Using backup position: ", iSelectedPosition)
ENDPROC

/// PURPOSE:
///    Warps the player to the arrive coords and heading if they are still alive
PROC _WT__WARP_PLAYER_TO_ARRIVE_LOCATION(WARP_TRANSITION_STRUCT &sInst)
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] WARP_PLAYER_TO_ARRIVE_LOCATION - Failed to warp player!")
		EXIT
	ENDIF
	
	IF NOT IS_SCREEN_FADED_OUT()
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] WARP_PLAYER_TO_ARRIVE_LOCATION - Failed to warp player!   SCREEN IS NOT FADED OUT")
		EXIT
	ENDIF
	
	_WT__VALIDATE_ARRIVE_LOCATION(sInst)
	_WT__MOVE_PLAYER_TO_ARRIVE_LOCATION(sInst)
	_WT__ROTATE_PLAYER_TO_ARRIVE_HEADING(sInst)
ENDPROC

PROC _WT__SET_PLAYERS_ARRIVE_OUTFIT(WARP_TRANSITION_STRUCT &sInst)
	IF sInst.sConfigData.eArriveOutfit != OUTFIT_MP_DEFAULT
		MP_OUTFITS_APPLY_DATA 	sApplyData
		sApplyData.pedID 		= PLAYER_PED_ID()
		sApplyData.eOutfit 		= sInst.sConfigData.eArriveOutfit
		sApplyData.eApplyStage 	= AOS_SET 		// Force stage to setting stage
		sApplyData.iPlayerIndex = -1			// This will only ever by the local player
		SET_PED_MP_OUTFIT(sApplyData)
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] SET_PLAYERS_ARRIVE_OUTFIT - Outfit equipped for arrive cutscene: ", GET_MP_OUTFIT_NAME(sInst.sConfigData.eArriveOutfit))
	ENDIF
ENDPROC

PROC _WT__SAFE_CLEAR_LOCAL_PLAYER_BLOOD_DAMAGE()
	PED_INDEX pedPlayer = PLAYER_PED_ID()
	
	IF IS_ENTITY_ALIVE(pedPlayer)
		CLEAR_PED_BLOOD_DAMAGE(pedPlayer)
		PRINTLN("[WARP_TRANSITION] SAFE_CLEAR_LOCAL_PLAYER_BLOOD_DAMAGE - Clearing blood")
	ENDIF
ENDPROC

///-----------------------
///    ON STATE ENTERS
///-----------------------

/// PURPOSE:
///    To be called on entering the pre leave cutscene state, it disables the player controls
PROC _WT__ON_ENTER_PRE_LEAVE_CUTSCENE()
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	// url:bugstar:6821263
	g_bEndScreenSuppressFadeIn = TRUE
ENDPROC

/// PURPOSE:
///    To be called on entering the warp player state, it resets the warp timer, moves the player,
///    and loads everything at the players new position
PROC _WT__ON_ENTER_WARP_PLAYER_STATE(WARP_TRANSITION_STRUCT &sInst)
	RESET_NET_TIMER(sInst.sWarpTimer)
ENDPROC

/// PURPOSE:
///    To be called on entering the arrive cutscene, it fades in the screen and gives back
///    player control
PROC _WT__ON_ENTER_ARRIVE_CUTSCENE(WARP_TRANSITION_STRUCT &sInst)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	_WT__SET_PLAYERS_ARRIVE_OUTFIT(sInst)
ENDPROC


///-----------------------
///    STATE UPDATES
///-----------------------

/// PURPOSE:
///    Waits for the assigned pre-leave event to finish before going on to the leave cinematic
PROC _WT__UPDATE_PRE_LEAVE_CUTSCENE_STATE(WARP_TRANSITION_STRUCT &sInst)
	// Wait for assigned event to complete
	
	WARP_TRANSITION_BOOL_EVENT fpPreLeaveEvent = WARP_TRANSITION_CONFIG__GET_PRE_LEAVE_CINEMATIC_EVENT(sInst.sConfigData)
	IF fpPreLeaveEvent != NULL
		#IF IS_DEBUG_BUILD
			STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
		#ENDIF
		
		IF NOT CALL fpPreLeaveEvent(sInst.sConfigData)
			IF NOT IS_BIT_SET_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
				SET_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
			ENDIF
			
			PRINTLN("[WARP_TRANSITION][", strWarpName, "]UPDATE_PRE_LEAVE_CUTSCENE_STATE - waiting for pre leave cinematic event to finish")
			EXIT
		ENDIF
		
		PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_PRE_LEAVE_CUTSCENE_STATE - event finished")
	ENDIF
	
	// Wait for cinematic script to launch, continue even if a script is not assigned or it fails
	IF _WT__TRY_LAUNCH_LEAVE_CUTSCENE_SCRIPT(sInst) = _WT_CSLS__LOADING
		EXIT
	ENDIF
	
	IF sInst.sConfigData.eBloodClear = WTCB_CLEAR_BEFORE_FIRST_CINEMATIC
		_WT__SAFE_CLEAR_LOCAL_PLAYER_BLOOD_DAMAGE()
	ENDIF
	
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__LEAVE_CUTSCENE)
ENDPROC

/// PURPOSE:
///    Updates the cutscene state waiting for the cutscene script to finish,
///    it also processes a registered event during the cutscene
PROC _WT__UPDATE_LEAVE_CUTSCENE_STATE(WARP_TRANSITION_STRUCT &sInst)
	// Process during cutscene event and wait until finished
	WARP_TRANSITION_BOOL_EVENT fpDuringLeaveEvent = WARP_TRANSITION_CONFIG__GET_DURING_LEAVE_CINEMATIC_EVENT(sInst.sConfigData)
	
	IF fpDuringLeaveEvent != NULL
		#IF IS_DEBUG_BUILD
			STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
		#ENDIF
		
		IF NOT CALL fpDuringLeaveEvent(sInst.sConfigData)
			IF NOT IS_BIT_SET_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
				SET_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
			ENDIF
			
			PRINTLN("[WARP_TRANSITION][", strWarpName, "] _WT__UPDATE_LEAVE_CUTSCENE_STATE - waiting for during leave cinematic event to finish")
			EXIT
		ENDIF
		
		PRINTLN("[WARP_TRANSITION][", strWarpName, "] _WT__UPDATE_LEAVE_CUTSCENE_STATE - event finished")
	ENDIF
	
	// Wait for cutscene to end before progressing
	IF NOT _WT__HAS_LEAVE_CUTSCENE_FINISHED(sInst)
		EXIT
	ENDIF
	
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__POST_LEAVE_CUTSCENE)
ENDPROC

/// PURPOSE:
///    Processes any registered post leave cutscene events and transitions to the warp state
PROC _WT__UPDATE_POST_LEAVE_CUTSCENE_STATE(WARP_TRANSITION_STRUCT &sInst)
	// Wait for assigned event to complete
	WARP_TRANSITION_BOOL_EVENT fpPostLeaveEvent = WARP_TRANSITION_CONFIG__GET_POST_LEAVE_CINEMATIC_EVENT(sInst.sConfigData)
	
	IF fpPostLeaveEvent != NULL
		#IF IS_DEBUG_BUILD
			STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
		#ENDIF
		
		IF NOT CALL fpPostLeaveEvent(sInst.sConfigData)
			IF NOT IS_BIT_SET_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
				SET_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
			ENDIF
			
			PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_POST_LEAVE_CUTSCENE_STATE - waiting for post leave cinematic event to finish")
			EXIT
		ENDIF
		
		PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_POST_LEAVE_CUTSCENE_STATE - event finished")
	ENDIF
	
	_WT__ON_ENTER_WARP_PLAYER_STATE(sInst)
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__WARP_PLAYER)
ENDPROC

/// PURPOSE:
///    Waits for a duration after the player has warp to give everything time to load
PROC _WT__UPDATE_WARP_PLAYER_STATE(WARP_TRANSITION_STRUCT &sInst)
	IF HAS_NET_TIMER_EXPIRED(sInst.sWarpTimer, ciWARP_TRANSITION__LOAD_DELAY + (NATIVE_TO_INT(PLAYER_ID()) * 100))
		_WT__WARP_PLAYER_TO_ARRIVE_LOCATION(sInst)
		_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__PRE_ARRIVE_CUTSCENE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates any registered pre arrive cutscene events
PROC _WT__UPDATE_PRE_ARRIVE_CUTSCENE(WARP_TRANSITION_STRUCT &sInst)
	WARP_TRANSITION_BOOL_EVENT fpPreArriveEvent = WARP_TRANSITION_CONFIG__GET_PRE_ARRIVE_CINEMATIC_EVENT(sInst.sConfigData)
	IF fpPreArriveEvent != NULL
		#IF IS_DEBUG_BUILD
			STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
		#ENDIF
		
		IF NOT CALL fpPreArriveEvent(sInst.sConfigData)
			IF NOT IS_BIT_SET_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
				SET_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
			ENDIF
			
			PRINTLN("[WARP_TRANSITION][", strWarpName, "]  UPDATE_PRE_ARRIVE_CUTSCENE - waiting for pre arrive cinematic event to finish")
			EXIT
		ENDIF
		
		PRINTLN("[WARP_TRANSITION][", strWarpName, "]  UPDATE_PRE_ARRIVE_CUTSCENE - event finished")
	ENDIF
	
	// Wait for cinematic script to launch, continue even if a script is not assigned or it fails
	IF _WT__TRY_LAUNCH_ARRIVE_CUTSCENE_SCRIPT(sInst) = _WT_CSLS__LOADING
		EXIT
	ENDIF
	
	IF sInst.sConfigData.eBloodClear = WTCB_CLEAR_BEFORE_SECOND_CINEMATIC
		_WT__SAFE_CLEAR_LOCAL_PLAYER_BLOOD_DAMAGE()
	ENDIF
	
	_WT__ON_ENTER_ARRIVE_CUTSCENE(sInst)
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__ARRIVE_CUTSCENE)
ENDPROC

/// PURPOSE:
///    Updates the arrive cutscene waiting for it to finish before progressing
PROC _WT__UPDATE_ARRIVE_CUTSCENE_STATE(WARP_TRANSITION_STRUCT &sInst)	
	WARP_TRANSITION_BOOL_EVENT fpDuringArriveEvent = WARP_TRANSITION_CONFIG__GET_DURING_ARRIVE_CINEMATIC_EVENT(sInst.sConfigData)
	
	IF fpDuringArriveEvent != NULL	
		#IF IS_DEBUG_BUILD
			STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
		#ENDIF
		
		IF NOT CALL fpDuringArriveEvent(sInst.sConfigData)
			IF NOT IS_BIT_SET_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
				SET_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
			ENDIF
			
			PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_ARRIVE_CUTSCENE_STATE - waiting for during arrive cinematic event to finish")
			EXIT
		ENDIF
		
		PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_ARRIVE_CUTSCENE_STATE - event finished")
	ENDIF
	
	// Wait for cutscene to end before progressing
	IF NOT _WT__HAS_ARRIVE_CUTSCENE_FINISHED(sInst)
		EXIT
	ENDIF
	
	IF sInst.sConfigData.eBloodClear = WTCB_CLEAR_ON_ARRIVAL
		_WT__SAFE_CLEAR_LOCAL_PLAYER_BLOOD_DAMAGE()
	ENDIF
	
	// Cutscenes may move the player, replace the player at the arrive location
	_WT__WARP_PLAYER_TO_ARRIVE_LOCATION(sInst)
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__POST_ARRIVE_CUTSCENE)
ENDPROC

/// PURPOSE:
///    Updates any registered post arrive cutscene events
///    NOTE: Players will have control at this point
PROC _WT__UPDATE_POST_ARRIVE_CUTSCENE_STATE(WARP_TRANSITION_STRUCT &sInst)
	WARP_TRANSITION_BOOL_EVENT fpPostArriveEvent = WARP_TRANSITION_CONFIG__GET_POST_ARRIVE_CINEMATIC_EVENT(sInst.sConfigData)
	
	IF fpPostArriveEvent != NULL
		#IF IS_DEBUG_BUILD
			STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
		#ENDIF
		
		IF NOT CALL fpPostArriveEvent(sInst.sConfigData)
			IF NOT IS_BIT_SET_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
				SET_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
			ENDIF
			
			PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_POST_ARRIVE_CUTSCENE_STATE - waiting for post arrive cinematic event to finish")
			EXIT
		ENDIF
		
		PRINTLN("[WARP_TRANSITION][", strWarpName, "] UPDATE_POST_ARRIVE_CUTSCENE_STATE - event finished")
	ENDIF
	
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__CLEANUP)
ENDPROC

/// PURPOSE:
///    Ends the script by calling cleanup
PROC _WT__UPDATE_CLEANUP_STATE(WARP_TRANSITION_STRUCT &sInst)
	CLEAR_LOCAL_PLAYERS_WARP_TRANSITION_GLOBAL_DATA()
	
	sInst.sConfigData.iBS = 0 // Clear temporary bits on transition config
	
	// url:bugstar:6821263
	g_bEndScreenSuppressFadeIn = FALSE
	
	ENABLE_INTERACTION_MENU()
	
	PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData),"][GLOBAL] UPDATE_CLEANUP_STATE - Set player in transition global to false")
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__IDLE)
ENDPROC

///-----------------------
///    PUBLIC API
///-----------------------    

PROC INIT_WARP_TRANSITION(WARP_TRANSITION_STRUCT &sInst, WARP_TRANSITION_CONFIG_STRUCT &sWarpConfig)
	WARP_TRANSITION__SET_CONFIG_DATA(sInst, sWarpConfig)
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__IDLE)
	_WT__SET_INITIALIZED(sInst, TRUE)
ENDPROC

/// PURPOSE:
///    Checks if the warp transition is being blocked by anything
/// RETURNS:
///    TRUE if the warp transition is being blocked by anything
FUNC BOOL IS_WARP_TRANSITION_BLOCKED(WARP_TRANSITION_STRUCT &sInst)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableWarpTransitionBlocks")
		RETURN FALSE
	ENDIF
	#ENDIF

	WARP_TRANSITION_BOOL_EVENT_NO_STRUCT fpBlockers = WARP_TRANSITION_CONFIG__GET_WARP_BLOCKERS(sInst.sConfigData)
	IF fpBlockers != NULL
		IF CALL fpBlockers()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the local players current vehicle and turns off the engine
PROC _WARP_TRANSITION_DISABLE_VEHICLE_ENGINE()
	PED_INDEX pedPlayer = GET_PLAYER_PED(PLAYER_ID())
	
	IF NOT IS_ENTITY_ALIVE(pedPlayer)
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(pedPlayer, TRUE)
		EXIT
	ENDIF
	
	VEHICLE_INDEX viVehicle = GET_VEHICLE_PED_IS_USING(pedPlayer)
	
	IF NOT IS_ENTITY_ALIVE(viVehicle)
	OR (NETWORK_GET_ENTITY_IS_NETWORKED(viVehicle)
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(viVehicle))
		EXIT
	ENDIF
	
	SET_VEHICLE_ENGINE_ON(viVehicle, FALSE, TRUE)
ENDPROC

/// PURPOSE:
///    Starts the configured warp transition so long as it is not already warping
///    It will fail if the warp transitions defined blockers return true
///    or warp transitions are blocked globaly by it's tuneable
FUNC BOOL START_WARP_TRANSITION(WARP_TRANSITION_STRUCT &sInst)
	#IF IS_DEBUG_BUILD
		STRING strWarpName = WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData)
	#ENDIF
	
	IF _WT__ARE_ALL_WARP_TRANSITIONS_BLOCKED()
		PRINTLN("[WARP_TRANSITION][", strWarpName,"] START_WARP_TRANSITION - All warps are blocked globaly by tuneable!")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
		PRINTLN("[WARP_TRANSITION][", strWarpName,"] START_WARP_TRANSITION - Another warp transition has set itself in progress, we can't start another until its finished!")
		RETURN FALSE
	ENDIF
	
	IF IS_WARP_TRANSITION_BLOCKED(sInst)
		PRINTLN("[WARP_TRANSITION][", strWarpName,"] START_WARP_TRANSITION - Defined warp blockers returned true")
		RETURN FALSE
	ENDIF
		
	IF WARP_TRANSITION__GET_STATE(sInst) != WARP_TRANSITION_STATE__IDLE
		PRINTLN("[WARP_TRANSITION][", strWarpName,"] START_WARP_TRANSITION - Warp all ready in progress")
		RETURN TRUE
	ENDIF
	
	FORCE_SHOP_CLEANUP_ALL()
	
	_SET_LOCAL_PLAYER_IN_WARP_TRANSITION(TRUE)
	TRANSFORM_STRUCT sArriveLocation
	sArriveLocation.Position = sInst.sConfigData.vArriveLocation
	sArriveLocation.Rotation = <<0.0, 0.0, sInst.sConfigData.fArriveHeading>>
	_SET_LOCAL_PLAYERS_CURRENT__WARP_TRANSITION_ARRIVE_LOCATION(sArriveLocation)
	PRINTLN("[WARP_TRANSITION][", strWarpName,"][GLOBAL] START_WARP_TRANSITION - Set player in transition global to true")
	
	// Turn off any vehicle engines so we don't get the engine sound
	_WARP_TRANSITION_DISABLE_VEHICLE_ENGINE()
	DISABLE_INTERACTION_MENU()
	
	_WT__ON_ENTER_PRE_LEAVE_CUTSCENE()
	_WT__SET_STATE(sInst, WARP_TRANSITION_STATE__PRE_LEAVE_CUTSCENE)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the given warp transition is in the process of warping the player
/// RETURNS:
///    TRUE if a warp transition is in progress
FUNC BOOL IS_WARP_TRANSITION_IN_PROGRESS(WARP_TRANSITION_STRUCT &sInst)
	RETURN WARP_TRANSITION__GET_STATE(sInst) != WARP_TRANSITION_STATE__IDLE
ENDFUNC

#IF IS_DEBUG_BUILD
DEBUGONLY PROC _WARP_TRANSITION_DEBUG__PRINT_NOT_INITIALIZED(WARP_TRANSITION_STRUCT &sInst, STRING strCallingFuncName)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_WarpTransitionExtraDebug")
		IF GET_FRAME_COUNT() % 180 = 0
			PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData), "] ",strCallingFuncName," - Transition not initialized with config data!")
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC _WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
	IF g_bKillWarpTransitionInvincibility
		EXIT
	ENDIF

	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Runs the configured warp transition, this will not do anything if INIT_WARP_TRANSITION hasn't been called
///    for the transition to start you must also call START_WARP_TRANSITION
FUNC BOOL MAINTAIN_WARP_TRANSITION(WARP_TRANSITION_STRUCT &sInst)	
	IF NOT WARP_TRANSITION__IS_INITIALIZED(sInst)
		#IF IS_DEBUG_BUILD
		_WARP_TRANSITION_DEBUG__PRINT_NOT_INITIALIZED(sInst, "MAINTAIN_WARP_TRANSITION")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	SWITCH WARP_TRANSITION__GET_STATE(sInst)
		CASE WARP_TRANSITION_STATE__IDLE 
			RETURN FALSE// Waiting for warp to trigger
		CASE WARP_TRANSITION_STATE__PRE_LEAVE_CUTSCENE
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_PRE_LEAVE_CUTSCENE_STATE(sInst)
		BREAK
		CASE WARP_TRANSITION_STATE__LEAVE_CUTSCENE
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_LEAVE_CUTSCENE_STATE(sInst)
		BREAK
		CASE WARP_TRANSITION_STATE__POST_LEAVE_CUTSCENE
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_POST_LEAVE_CUTSCENE_STATE(sInst)
		BREAK	
		CASE WARP_TRANSITION_STATE__WARP_PLAYER
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_WARP_PLAYER_STATE(sInst)
		BREAK
		CASE WARP_TRANSITION_STATE__PRE_ARRIVE_CUTSCENE
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_PRE_ARRIVE_CUTSCENE(sInst)
		BREAK
		CASE WARP_TRANSITION_STATE__ARRIVE_CUTSCENE	
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_ARRIVE_CUTSCENE_STATE(sInst)
		BREAK
		CASE WARP_TRANSITION_STATE__POST_ARRIVE_CUTSCENE
			_WARP_TRANSITION__MAINTAIN_INVINCIBILITY()
			_WT__UPDATE_POST_ARRIVE_CUTSCENE_STATE(sInst)
		BREAK		
		CASE WARP_TRANSITION_STATE__CLEANUP	
			_WT__UPDATE_CLEANUP_STATE(sInst)
			RETURN TRUE// Warp has finished
	ENDSWITCH	
	
	RETURN FALSE
ENDFUNC

PROC RESET_WARP_TRANSITION(WARP_TRANSITION_STRUCT &sInst, BOOL bKillCutsceneScripts = FALSE)
	IF bKillCutsceneScripts
		_WT__KILL_ALL_CUTSCENE_SCRIPTS(sInst)
	ENDIF
	
	// If this was the transition that was in progress clear the global flag
	IF WARP_TRANSITION__GET_STATE(sInst) > WARP_TRANSITION_STATE__IDLE
		CLEAR_LOCAL_PLAYERS_WARP_TRANSITION_GLOBAL_DATA()
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData),"][GLOBAL] CLEANUP_WARP_TRANSITION - Set player in transition global to false")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MUTES_RADIO_SCENE")
		STOP_AUDIO_SCENE("MUTES_RADIO_SCENE")
	ENDIF
	
	// Store last config and clear the transition struct the re-init it with the saved config
	WARP_TRANSITION_CONFIG_STRUCT sLastConfig = WARP_TRANSITION__GET_CONFIG_DATA(sInst)
	sLastConfig.iBS = 0
	WARP_TRANSITION_STRUCT sEmpty
	COPY_SCRIPT_STRUCT(sInst, sEmpty, SIZE_OF(sInst))

	INIT_WARP_TRANSITION(sInst, sLastConfig)
ENDPROC

PROC CLEANUP_WARP_TRANSITION(WARP_TRANSITION_STRUCT &sInst, BOOL bKillCutsceneScripts = FALSE)
	IF bKillCutsceneScripts
		_WT__KILL_ALL_CUTSCENE_SCRIPTS(sInst)
	ENDIF
	
	// If this was the transition that was in progress clear the global flag
	IF WARP_TRANSITION__GET_STATE(sInst) > WARP_TRANSITION_STATE__IDLE
		CLEAR_LOCAL_PLAYERS_WARP_TRANSITION_GLOBAL_DATA()
		PRINTLN("[WARP_TRANSITION][", WARP_TRANSITION_CONFIG__GET_DEBUG_NAME(sInst.sConfigData),"][GLOBAL] CLEANUP_WARP_TRANSITION - Set player in transition global to false")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("MUTES_RADIO_SCENE")
		STOP_AUDIO_SCENE("MUTES_RADIO_SCENE")
	ENDIF
	
	// Clear the struct
	WARP_TRANSITION_STRUCT sEmpty
	sInst = sEmpty
ENDPROC
