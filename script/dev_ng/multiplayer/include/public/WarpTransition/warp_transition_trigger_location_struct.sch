USING "context_control_public.sch"
USING "warp_transition.sch"
USING "warp_transition_trigger_location_state_enum.sch"

ENUM WARP_TRANSITION_TRIGGER_LOCATION_STATE
	WTTLS__LOCATE
	,WTTLS__PROMPT
	,WTTLS__CONFIGURE_WALK_IN_CAMERA
	,WTTLS__WALK_IN
	,WTTLS__CONFIGURE_MENU_CAMERA
	,WTTLS__WAIT_FOR_DESTINATION_SELECTION
	,WTTLS__WAIT_FOR_TRAVEL_OPTIONS
	,WTTLS__TRANSITIONING
	,WTTLS__CONFIGURE_WALK_OUT_CAMERA
	,WTTLS__WALK_OUT
	,WTTLS__CONFIGURE_POST_WALK_OUT_CAMERA
	,WTTLS__INTERP_TO_GAME
	,WTTLS__WAIT_FOR_FADE_IN
ENDENUM

DEBUGONLY FUNC STRING DEBUG_GET_WARP_TRANSITION_TRIGGER_LOCATION_STATE_AS_STRING(WARP_TRANSITION_TRIGGER_LOCATION_STATE eEnum)
	SWITCH eEnum
		CASE WTTLS__LOCATE							RETURN	"WTTLS__LOCATE"
		CASE WTTLS__PROMPT							RETURN	"WTTLS__PROMPT"
		CASE WTTLS__CONFIGURE_WALK_IN_CAMERA		RETURN	"WTTLS__CONFIGURE_WALK_IN_CAMERA"
		CASE WTTLS__WALK_IN							RETURN	"WTTLS__WALK_IN"
		CASE WTTLS__CONFIGURE_MENU_CAMERA			RETURN	"WTTLS__CONFIGURE_MENU_CAMERA"
		CASE WTTLS__WAIT_FOR_DESTINATION_SELECTION	RETURN	"WTTLS__WAIT_FOR_DESTINATION_SELECTION"
		CASE WTTLS__WAIT_FOR_TRAVEL_OPTIONS			RETURN	"WTTLS__WAIT_FOR_TRAVEL_OPTIONS"
		CASE WTTLS__TRANSITIONING					RETURN	"WTTLS__TRANSITIONING"
		CASE WTTLS__CONFIGURE_WALK_OUT_CAMERA		RETURN	"WTTLS__CONFIGURE_WALK_OUT_CAMERA"
		CASE WTTLS__WALK_OUT						RETURN	"WTTLS__WALK_OUT"
		CASE WTTLS__CONFIGURE_POST_WALK_OUT_CAMERA	RETURN	"WTTLS__CONFIGURE_POST_WALK_OUT_CAMERA"
		CASE WTTLS__INTERP_TO_GAME					RETURN	"WTTLS__INTERP_TO_GAME"
		CASE WTTLS__WAIT_FOR_FADE_IN				RETURN	"WTTLS__WAIT_FOR_FADE_IN"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_WARP_TRANSITION_TRIGGER_LOCATION_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

ENUM WARP_TRANSITION_MENU_OPTIONS
	WARP_TRANSITION_MENU_OPTIONS__WARP_ALONE
	,WARP_TRANSITION_MENU_OPTIONS__WARP_WITH_NEARBY_ORGANIZATION
	,WARP_TRANSITION_MENU_OPTIONS__WARP_WITH_NEARBY_FRIENDS_AND_CREW
ENDENUM

TYPEDEF FUNC BOOL T_IS_BLOCKED()
TYPEDEF FUNC BOOL T_GET_IN_TRIGGER_VOLUME()
TYPEDEF FUNC BOOL T_GET_SPECIFIC_PLAYER_IN_TRIGGER_VOLUME(PLAYER_INDEX piPlayer)
TYPEDEF FUNC BOOL T_CONFIGURE_CAMERA(CAMERA_INDEX ciCamera)
TYPEDEF PROC T_TRANSITION_EVENT(PLAYER_INDEX piPlayerToSendEventTo)
TYPEDEF FUNC STRING T_MENU_OPTION_LABEL_LOOKUP(WARP_TRANSITION_MENU_OPTIONS eOption)
TYPEDEF PROC T_CONFIGURE_BLIP(BLIP_INDEX &biBlip)
TYPEDEF PROC T_BAIL_EVENT()

ENUM WARP_TRANSITION_TRIGGER_LOCATION_BITSET
	WARP_TRANSITION_TRIGGER_LOCATION_BS__INITIALIZED
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__SKIP_PROMPT
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__ALLOW_TAG_ALONG_WARP
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__TRAVEL_MENU_CREATED
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_IN_ANIM
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_WALK_IN_ANIM
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_OUT_ANIM
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_WALK_OUT_ANIM
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_MENU_BANNER
	,WARP_TRANSITION_TRIGGER_LOCATION_BS__FORCE_BAIL_SCREEN_FADE_IN
ENDENUM

ENUM WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS
	WTTLC__INVALID = -1
	,WTTLC__WALK_IN_CAM
	,WTTLC__WALK_OUT_CAM
	,WTTLC__MENU_CAM
	,WTTLC__POST_WALK_OUT_CAM
ENDENUM

STRUCT WARP_TRANSITION_TRIGGER_LOCATION_STRUCT
	WARP_TRANSITION_STRUCT sWarpTransition	
	WARP_TRANSITION_TRIGGER_LOCATION_STATE eState
	
	// Prompt
	INT iContextIntention = NEW_CONTEXT_INTENTION
	STRING strContextLabel
	
	// Blip
	T_CONFIGURE_BLIP fpConfigureBlip
	BLIP_INDEX biBlip
	
	// Trigger
	VECTOR vLeaveMarkerLocation
	BOOL bIsMarkerVisible = TRUE
	T_GET_IN_TRIGGER_VOLUME fpIsPlayerInTriggerVolume
	T_IS_BLOCKED fpIsTriggerBlocked
	T_BAIL_EVENT fpBail
	
	INT iBitSet // ENUM WARP_TRANSITION_TRIGGER_LOCATION_BITSET
	INT iSyncSceneID = -1
	
	INT iWalkInAnimDoorSoundStage = 0
	
	// Walk in anim
	VECTOR vWalkInSceneCoord
	VECTOR vWalkInSceneRot
	STRING strWalkInAnim
	STRING strWalkInAnimDict
	FLOAT fWalkInStartPhase
	FLOAT fWalkInEndPhase = 0.9
	
	// Walk out anim
	VECTOR vWalkOutSceneCoord
	VECTOR vWalkOutSceneRot
	STRING strWalkOutAnim
	STRING strWalkOutAnimDict
	FLOAT fWalkOutStartPhase
	FLOAT fWalkOutEndPhase = 0.9
	
	// Tag along warp menu
	STRING strMenuTitle
	T_TRANSITION_EVENT fpTransitionEvent
	T_MENU_OPTION_LABEL_LOOKUP fpMenuOptionLabelLookup
	T_GET_SPECIFIC_PLAYER_IN_TRIGGER_VOLUME fpIsPlayerInTagAlongVolume
	
	// Entry menu
	STRING strDestinationMenuTitle
	STRING strDestinationName
	
	// Cameras
	CAMERA_INDEX ciMainCam
	CAMERA_INDEX ciBuffer
	T_CONFIGURE_CAMERA fpConfigureCamera[COUNT_OF(WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS)]
	CAMERA_TYPE eCamType[COUNT_OF(WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS)]
	CAMERA_TYPE eCurrentCamType
	
	STRING strBannerTexDict
	STRING strBannerTex
	
	#IF IS_DEBUG_BUILD
	STRING strDebugName
	#ENDIF
ENDSTRUCT

///---------------------
///    GETTERS
///---------------------

FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_DESTINATION_MENU_TITLE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strDestinationMenuTitle
ENDFUNC

FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_DESTINATION_NAME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strDestinationName
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Gets the debug name of the transition location, this is used for prints in the system
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The debug name of the transition location
DEBUGONLY FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_DEBUG_NAME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	IF IS_STRING_NULL_OR_EMPTY(sInst.strDebugName)
		sInst.strDebugName = "UNDEFINED"
	ENDIF
	
	RETURN sInst.strDebugName
ENDFUNC
#ENDIF

FUNC INT WARP_TRANSITION_TRIGGER_LOCATION__GET_SYNC_SCENE_ID(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.iSyncSceneID
ENDFUNC

/// PURPOSE:
///    Gets if this trigger location should show a prompt and wait for input to start the transition
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    TRUE if this trigger location should show a prompt
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_SKIP_PROMPT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__SKIP_PROMPT)
ENDFUNC

/// PURPOSE:
///    Gets if the warp trigger has a walk in scene where the players ped walks to a location
///    or into a building
/// PARAMS:
///    sInst - the warp trigger location instance 
/// RETURNS:
///    TRUE if the warp trigger has a walk in scene
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_HAS_WALK_IN(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_IN_ANIM)
ENDFUNC

/// PURPOSE:
///    Gets the location the player should walk to when starting the warp trigger
/// PARAMS:
///    sInst - the warp trigger location instance  
/// RETURNS:
///    The target location the player will walk to in world space
FUNC VECTOR WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_IN_SCENE_LOCATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.vWalkInSceneCoord
ENDFUNC

/// PURPOSE:
///    Gets the rotation the player should walk to when starting the warp trigger
/// PARAMS:
///    sInst - the warp trigger rotation instance  
/// RETURNS:
///    The target rotation the player will walk to in world space
FUNC VECTOR WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_IN_SCENE_ROTATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.vWalkInSceneRot
ENDFUNC

/// PURPOSE:
///    Gets the walk in animation clip name used for when the player enters the trigger
/// PARAMS:
///    sInst - the warp trigger location instance  
/// RETURNS:
///    The walk in animation clip name
FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_IN_ANIM_CLIP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strWalkInAnim
ENDFUNC

/// PURPOSE:
///    Gets the walk in animation dictionary used for when the player enters the trigger
/// PARAMS:
///    sInst - the warp trigger location instance  
/// RETURNS:
///    The walk in animation dictionary
FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_IN_ANIM_DICT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strWalkInAnimDict
ENDFUNC

FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_HAS_WALK_IN_ANIM_LOADED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_WALK_IN_ANIM)
ENDFUNC

/// PURPOSE:
///    Gets if the warp trigger has a walk out scene where the players ped walks to a location
///    or into a building
/// PARAMS:
///    sInst - the warp trigger location instance 
/// RETURNS:
///    TRUE if the warp trigger has a walk out scene
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_HAS_WALK_OUT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_OUT_ANIM)
ENDFUNC

/// PURPOSE:
///    Gets the location the player should walk to when starting the warp trigger
/// PARAMS:
///    sInst - the warp trigger location instance  
/// RETURNS:
///    The target location the player will walk to in world space
FUNC VECTOR WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_OUT_SCENE_LOCATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.vWalkOutSceneCoord
ENDFUNC

/// PURPOSE:
///    Gets the rotation the player should walk to when starting the warp trigger
/// PARAMS:
///    sInst - the warp trigger rotation instance  
/// RETURNS:
///    The target rotation the player will walk to in world space
FUNC VECTOR WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_OUT_SCENE_ROTATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.vWalkOutSceneRot
ENDFUNC

/// PURPOSE:
///    Gets the walk out animation clip name used for when the player enters the trigger
/// PARAMS:
///    sInst - the warp trigger location instance  
/// RETURNS:
///    The walk out animation clip name
FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_OUT_ANIM_CLIP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strWalkOutAnim
ENDFUNC

/// PURPOSE:
///    Gets the walk out animation dictionary used for when the player enters the trigger
/// PARAMS:
///    sInst - the warp trigger location instance  
/// RETURNS:
///    The walk out animation dictionary
FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_OUT_ANIM_DICT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strWalkOutAnimDict
ENDFUNC

FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_HAS_WALK_OUT_ANIM_LOADED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_WALK_OUT_ANIM)
ENDFUNC

FUNC BOOL _WARP_TRANSITION_TRIGGER_LOCATION__GET_HAS_CAM_ASSIGNED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS eCam)
	IF eCam = WTTLC__INVALID
		RETURN FALSE
	ENDIF
	
	RETURN sInst.fpConfigureCamera[eCam] != NULL
ENDFUNC

/// PURPOSE:
///    Uses the assigned camera configuration to configure the given camera
/// PARAMS:
///    sInst - the warp trigger location instance
///    ciMainCam - the camera to configure
/// RETURNS:
///    TRUE configured
FUNC BOOL _WARP_TRANSITION_TRIGGER_LOCATION__CONFIGURE_CAM(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, CAMERA_INDEX ciMainCam, WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS eCam)
	IF eCam = WTTLC__INVALID
		RETURN TRUE
	ENDIF
	
	IF NOT _WARP_TRANSITION_TRIGGER_LOCATION__GET_HAS_CAM_ASSIGNED(sInst, eCam)
		RETURN TRUE
	ENDIF
	
	RETURN CALL sInst.fpConfigureCamera[eCam](ciMainCam)
ENDFUNC

FUNC CAMERA_TYPE _WARP_TRANSITION_TRIGGER_LOCATION__GET_CAM_TYPE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst,  WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS eCam)
	IF eCam = WTTLC__INVALID
		RETURN CAMTYPE_SCRIPTED
	ENDIF
	
	RETURN sInst.eCamType[eCam]
ENDFUNC

/// PURPOSE:
///    Gets if the warp trigger location has been initialized, for it to run the
///    trigger location must be initialized with a warp transition
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    TRUE if the warp trigger location has been initialized
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_INITIALIZED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__INITIALIZED)
ENDFUNC

/// PURPOSE:
///    Gets the warp triggers marker location this is where the blue corona will be placed
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The warp triggers marker location
FUNC VECTOR WARP_TRANSITION_TRIGGER_LOCATION__GET_LEAVE_MARKER_LOCATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.vLeaveMarkerLocation
ENDFUNC

/// PURPOSE:
///    Checks if the marker location should be visible to the player
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    TRUE if the marker should be visible to the player
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__IS_MARKER_LOCATION_VISIBLE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.bIsMarkerVisible
ENDFUNC

/// PURPOSE:
///    Checks if the local player is in the defined trigger volume for starting the transition, this uses an assigned function pointer and may
///    check multiple trigger areas according to the assigned function
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    Checks if the local player is in the defined trigger volume for starting the transition
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_PLAYER_IN_TRIGGER_VOLUME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	IF sInst.fpIsPlayerInTriggerVolume = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL sInst.fpIsPlayerInTriggerVolume()
ENDFUNC

/// PURPOSE:
///    Checks if the given player is in the tag along volume for bringing extra players a long in the warp
///    this is according to an assigned function pointer and may check multiple areas
/// PARAMS:
///    sInst - the warp trigger location instance
///    piPlayerIndex - the player to check
/// RETURNS:
///    TRUE if in the tag along area, FALSE if not or a trigger volume has not been assigned
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__IS_PLAYER_IN_ASSIGNED_TAG_ALONG_VOLUME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, PLAYER_INDEX piPlayerIndex)
	IF sInst.fpIsPlayerInTagAlongVolume = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL sInst.fpIsPlayerInTagAlongVolume(piPlayerIndex)
ENDFUNC

/// PURPOSE:
///    Gets if the trigger location allows bringing other players along in the warp according
///    to a menu selection
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    TRUE if other players can be brought along in the waro
FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_ALLOW_TAG_ALONG_WARP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__ALLOW_TAG_ALONG_WARP)
ENDFUNC

/// PURPOSE:
///    Gets the current state of the trigger location used for its internal state machine
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The current state of the trigger location
FUNC WARP_TRANSITION_TRIGGER_LOCATION_STATE WARP_TRANSITION_TRIGGER_LOCATION__GET_STATE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.eState
ENDFUNC

/// PURPOSE:
///    Gets the assigned context label that shows as a prompt to start the warp transition
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    Gets the assigned context label
FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_CONTEXT_LABEL(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.strContextLabel
ENDFUNC

/// PURPOSE:
///    Gets the context intention id that is used for the warp activation prompt
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The context intention id that is used for the warp activation prompt
FUNC INT WARP_TRANSITION_TRIGGER_LOCATION__GET_CONTEXT_INTENTION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.iContextIntention
ENDFUNC

/// PURPOSE:
///    Gets the blip index that is shown on the minimap for the trigger location
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The blip index for the warp trigger location
FUNC BLIP_INDEX WARP_TRANSITION_TRIGGER_LOCATION__GET_BLIP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.biBlip
ENDFUNC

/// PURPOSE:
///    Gets the main camera index used to show the player entering and exiting the trigger location
///    similar to property enters and exits
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The main camera index used to show the player entering and exiting the trigger location
FUNC CAMERA_INDEX WARP_TRANSITION_TRIGGER_LOCATION__GET_MAIN_CAMERA(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.ciMainCam
ENDFUNC

/// PURPOSE:
///    Gets the title of the warp menu that shows options for bringing players along in the warp
/// PARAMS:
///    sInst - the warp trigger location instance
/// RETURNS:
///    The title of the warp menu
FUNC STRING WARP_TRANSITION_TRIGGER_LOCATION__GET_WARP_MENU_TITLE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	IF IS_STRING_NULL(sInst.strMenuTitle)
		RETURN "WARP_TRAVEL"
	ENDIF
	
	RETURN sInst.strMenuTitle
ENDFUNC

FUNC FLOAT WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_IN_START_PHASE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.fWalkInStartPhase
ENDFUNC

FUNC FLOAT WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_IN_END_PHASE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.fWalkInEndPhase
ENDFUNC

FUNC FLOAT WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_OUT_START_PHASE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.fWalkOutStartPhase
ENDFUNC

FUNC FLOAT WARP_TRANSITION_TRIGGER_LOCATION__GET_WALK_OUT_END_PHASE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN sInst.fWalkOutEndPhase
ENDFUNC

FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_MENU_CREATED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__TRAVEL_MENU_CREATED)
ENDFUNC

FUNC BOOL WARP_TRANSITION_TRIGGER_LOCATION__GET_IS_TRIGGER_BLOCKED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	IF sInst.fpIsTriggerBlocked = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL sInst.fpIsTriggerBlocked()
ENDFUNC

///--------------------------------
///    SETTERS
///--------------------------------  

PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_TRIGGER_BLOCKS(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_IS_BLOCKED fpIsTriggerBlocked)
	sInst.fpIsTriggerBlocked = fpIsTriggerBlocked
ENDPROC

PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_DESTINATION_MENU_TITLE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strTitle)
	sInst.strDestinationMenuTitle = strTitle
ENDPROC

PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_DESTINATION_NAME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strName)
	sInst.strDestinationName = strName
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Sets the debug name for the trigger location that will be used in the prints
/// PARAMS:
///    sInst - the warp trigger location instance
///    strDebugName - the debug name to use
DEBUGONLY PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_DEBUG_NAME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strDebugName)
	sInst.strDebugName = strDebugName
ENDPROC
#ENDIF

/// PURPOSE:
///    Sets if the warp trigger location should start the warp transition without showing a prompt and waiting for input
/// PARAMS:
///    sInst - the warp trigger location instance
///    bUsePrompt - 
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_SKIP_PROMPT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bSkipPrompt)
	ENABLE_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__SKIP_PROMPT, bSkipPrompt)	
ENDPROC

/// PURPOSE:
///    Sets the sync scene id for the walk in and walk out animations
/// PARAMS:
///    sInst - the warp trigger location instance
///    iSyncSceneID - the sync scene id running the walk in or walk out animation
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_SYNC_SCENE_ID(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, INT iSyncSceneID)
	sInst.iSyncSceneID = iSyncSceneID
ENDPROC

/// PURPOSE:
///    Sets the walk in anim sound stage.
/// PARAMS:
///    sInst - Warp trigger location instance.
///    iWalkInAnimDoorSoundStage - Walk in anim sound stage.
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_WALK_IN_SOUND_STAGE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, INT iWalkInAnimDoorSoundStage)
	sInst.iWalkInAnimDoorSoundStage = iWalkInAnimDoorSoundStage
ENDPROC

/// PURPOSE:
///    Sets the walk in animation and scene location used when the player enters the trigger volume
///    this allows you to do door openings
/// PARAMS:
///    sInst -  the warp trigger location instance
///    strAnimName - the animation clip name to use
///    strAnimDict - the dictionary the anim clip belongs to
///    vSceneLocation - the location the animation should play at in world space
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_WALK_IN_ANIM(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strAnimName, STRING strAnimDict, 
VECTOR vSceneLocation, VECTOR vSceneRot, FLOAT fStartPhase = 0.0, FLOAT fEndPhase = 0.9)
	SET_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_IN_ANIM)
	sInst.vWalkInSceneCoord = vSceneLocation
	sInst.vWalkInSceneRot = vSceneRot
	sInst.strWalkInAnim = strAnimName
	sInst.strWalkInAnimDict = strAnimDict
	sInst.fWalkInStartPhase = fStartPhase
	sInst.fWalkInEndPhase = fEndPhase
ENDPROC

/// PURPOSE:
///    Removes any assigned walk in animation on the warp transition trigger
/// PARAMS:
///    sInst - the warp trigger location instance
PROC WARP_TRANSITION_TRIGGER_LOCATION__REMOVE_WALK_IN(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	CLEAR_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_IN_ANIM)
	sInst.vWalkInSceneCoord = <<0.0, 0.0, 0.0>>
	sInst.vWalkInSceneRot = <<0.0, 0.0, 0.0>>
	sInst.strWalkInAnim = ""
	sInst.strWalkInAnimDict = ""
ENDPROC

PROC _WARP_TRANSITION_TRIGGER_LOCATION__SET_WALK_IN_ANIM_HAS_LOADED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bLoaded)
	ENABLE_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_WALK_IN_ANIM, bLoaded)
ENDPROC

/// PURPOSE:
///    Sets the walk out animation and scene location used when the player enters the trigger volume
///    this allows you to do door openings
/// PARAMS:
///    sInst -  the warp trigger location instance
///    strAnimName - the animation clip name to use
///    strAnimDict - the dictionary the anim clip belongs to
///    vSceneLocation - the location the animation should play at in world space
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_WALK_OUT_ANIM(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strAnimName, STRING strAnimDict,
VECTOR vSceneLocation, VECTOR vSceneRot, FLOAT fStartPhase = 0.0, FLOAT fEndPhase = 0.9)
	IF NOT IS_STRING_NULL_OR_EMPTY(strAnimName)
	AND NOT IS_STRING_NULL_OR_EMPTY(strAnimDict)
		SET_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_OUT_ANIM)
	ENDIF
	sInst.vWalkOutSceneCoord = vSceneLocation
	sInst.vWalkOutSceneRot = vSceneRot
	sInst.strWalkOutAnim = strAnimName
	sInst.strWalkOutAnimDict = strAnimDict
	sInst.fWalkOutStartPhase = fStartPhase
	sInst.fWalkOutEndPhase = fEndPhase
ENDPROC

/// PURPOSE:
///    Removes any assigned walk out animation on the warp transition trigger
/// PARAMS:
///    sInst - the warp trigger location instance
PROC WARP_TRANSITION_TRIGGER_LOCATION__REMOVE_WALK_OUT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst)
	CLEAR_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_WALK_OUT_ANIM)
	sInst.vWalkOutSceneCoord = <<0.0, 0.0, 0.0>>
	sInst.vWalkOutSceneRot = <<0.0, 0.0, 0.0>>
	sInst.strWalkOutAnim = ""
	sInst.strWalkOutAnimDict = ""
ENDPROC

PROC _WARP_TRANSITION_TRIGGER_LOCATION__SET_WALK_OUT_ANIM_HAS_LOADED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bLoaded)
	ENABLE_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__HAS_LOADED_WALK_OUT_ANIM, bLoaded)
ENDPROC

/// PURPOSE:
///    Sets the title that will apper of the warp menu that shows options for bringing other players along in the warp
/// PARAMS:
///    sInst - the warp trigger location instance
///    strMenuTitle - the menu title to use at the top of the warp menu
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_WARP_MENU_TITLE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strMenuTitle)
	sInst.strMenuTitle = strMenuTitle
ENDPROC

/// PURPOSE:
///    Sets a function that configures the given camera
/// PARAMS:
///    sInst - the warp trigger location instance
///    fpConfigureWalkInCamera - the camera configuration function
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_CAMERA_CONFIGURATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_CONFIGURE_CAMERA fpConfigureCamera, WARP_TRANSITION_TRIGGER_LOCATION_CAMERAS eCam, CAMERA_TYPE eCamType = CAMTYPE_SCRIPTED)
	IF eCam = WTTLC__INVALID
		EXIT
	ENDIF
	
	sInst.fpConfigureCamera[eCam] = fpConfigureCamera
	sInst.eCamType[eCam] = eCamType
ENDPROC

PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_MENU_BANNER(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strBannerTexDict, STRING strBannerTex)
	sInst.strBannerTexDict = strBannerTexDict
	sInst.strBannerTex = strBannerTex
ENDPROC

/// PURPOSE:
///    Sets if the warp trigger is initialized, this is private and should only be used internally
///    the system has to be initialized for it to update
/// PARAMS:
///    sInst - the warp trigger location instance
///    bInitialized - if the trigger location is initialized
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_INITIALIZED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bInitialized)
	ENABLE_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__INITIALIZED, bInitialized)
ENDPROC

PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_BLIP_SETUP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_CONFIGURE_BLIP fpConfigureBlip)
	sInst.fpConfigureBlip = fpConfigureBlip
ENDPROC

/// PURPOSE:
///    Sets a lookup that will be used for for grabbing text labels for the various options in the menu, if
///    not assigned default options will be used. This lookup should return a text label for every option in the passed enum
/// PARAMS:
///    sInst - the warp trigger location instance
///    fpMenuOptionLabelLookup - the lookup of menu option text labels
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_MENU_OPTIONS_TEXT_LABEL_LOOKUP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_MENU_OPTION_LABEL_LOOKUP fpMenuOptionLabelLookup)
	sInst.fpMenuOptionLabelLookup = fpMenuOptionLabelLookup
ENDPROC

/// PURPOSE:
///    Set the camera index that the main camera will use for showing the player enter and exit the trigger
/// PARAMS:
///    sInst - the warp trigger location instance
///    ciMainCam - the camera index to use
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_MAIN_CAMERA(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, CAMERA_INDEX ciMainCam)
	sInst.ciMainCam = ciMainCam
ENDPROC

/// PURPOSE:
///    Sets the warp transition that the trigger location will use when the player triggers the warp
/// PARAMS:
///    sInst - the warp trigger location instance
///    sWarpTransition - the warp transition to use
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_WARP_TRANSITION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, WARP_TRANSITION_STRUCT &sWarpTransition)
	sInst.sWarpTransition = sWarpTransition
ENDPROC

/// PURPOSE:
///    Sets the leave marker location, this is where the corona marker will appear in world
/// PARAMS:
///    sInst - the warp trigger location instance
///    vLeaveMarkerLocation - the location for the marker
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_LEAVE_MARKER_LOCATION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, VECTOR vLeaveMarkerLocation)
	sInst.vLeaveMarkerLocation = vLeaveMarkerLocation
ENDPROC

/// PURPOSE:
///    Sets if the leave marker location should be visible to the player
/// PARAMS:
///    sInst - the warp trigger location instance
///    bIsMarkerVisible - if the location for the marker should be visible
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_LEAVE_MARKER_LOCATION_IS_VISIBLE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bIsMarkerVisible)
	sInst.bIsMarkerVisible = bIsMarkerVisible
ENDPROC

/// PURPOSE:
///    Sets the function that checks if the player is in the trigger location for starting the warp. The assigned functions should
///    return true when the player is in the volume, multiple volumes can be used if neccessary
/// PARAMS:
///    sInst - the warp trigger location instance
///    fpIsPlayerInTriggerVolume - the function for checking if the player is in the trigger volume
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_IS_PLAYER_IN_TRIGGER_VOLUME(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_GET_IN_TRIGGER_VOLUME fpIsPlayerInTriggerVolume)
	sInst.fpIsPlayerInTriggerVolume = fpIsPlayerInTriggerVolume
ENDPROC

/// PURPOSE:
///    Sets if the warp trigger location allows friends, crew members, and gang members to be optionally brought along in the warp.
///    This will appear as a set of menu options to the player depending on other players in the area
/// PARAMS:
///    sInst - the warp trigger location instance
///    bAllowTagAlongWarp - if other players can be brought along in the warp
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_ALLOW_TAG_ALONG_WARP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bAllowTagAlongWarp)
	ENABLE_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__ALLOW_TAG_ALONG_WARP, bAllowTagAlongWarp)
ENDPROC

/// PURPOSE:
///    Sets the network warp transition event that will be sent to other players that need to be brought along in the warp. 
///    An external event process will need to be setup to process theses invite events.
/// PARAMS:
///    sInst - the warp trigger location instance
///    fpTransitionEvent - the network event for making other players warp
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_NETWORK_TRANSITION_EVENT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_TRANSITION_EVENT fpTransitionEvent)
	sInst.fpTransitionEvent = fpTransitionEvent
ENDPROC

/// PURPOSE:
///    Sets the state of the warp trigger location used by the internal FSM
/// PARAMS:
///    sInst - the warp trigger location instance
///    eState - the new state
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_STATE(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, WARP_TRANSITION_TRIGGER_LOCATION_STATE eState)
	PRINTLN("[WARP_TRANSITION_TRIGGER_LOCATION] SET_STATE - transition: ",
	DEBUG_GET_WARP_TRANSITION_TRIGGER_LOCATION_STATE_AS_STRING(WARP_TRANSITION_TRIGGER_LOCATION__GET_STATE(sInst)), " -> ",
	DEBUG_GET_WARP_TRANSITION_TRIGGER_LOCATION_STATE_AS_STRING(eState))
	sInst.eState = eState
ENDPROC

/// PURPOSE:
///    Sets the label that will be used for the trigger prompt, this should include a right dpad indicator
/// PARAMS:
///    sInst - the warp trigger location instance
///    strContextLabel - the text label to use for the prompt
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_CONTEXT_LABEL(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, STRING strContextLabel)
	sInst.strContextLabel = strContextLabel
ENDPROC

/// PURPOSE:
///    Sets the id of the context intention being used for the trigger prompt
/// PARAMS:
///    sInst - the warp trigger location instance
///    iContextIntention - the context intention id
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_CONTEXT_INTENTION(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, INT iContextIntention)
	PRINTLN("[WARP_TRANSITION_TRIGGER_LOCATION] SET_CONTEXT_INTENTION - iContextIntention: ", iContextIntention) 
	sInst.iContextIntention = iContextIntention
ENDPROC

/// PURPOSE:
///    Sets the blip index being used by the trigger location
/// PARAMS:
///    sInst - the warp trigger location instance
///    biBlip - the blip index to use
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_BLIP(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BLIP_INDEX biBlip)
	sInst.biBlip = biBlip
ENDPROC

PROC _WARP_TRANSITION_TRIGGER_LOCATION__SET_MENU_CREATED(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, BOOL bCreated)
	ENABLE_BIT_ENUM(sInst.iBitSet, WARP_TRANSITION_TRIGGER_LOCATION_BS__TRAVEL_MENU_CREATED, bCreated)
ENDPROC

/// PURPOSE:
///    Sets an event that is called when bailing from this warp trigger, you should use this to add any additional cleanup that needs to 
///    happen when we bail from the trigger/transition.
/// PARAMS:
///    sInst - the warp trigger location instance
///    fpBailEvent - the proc that performs the bail cleanup
PROC WARP_TRANSITION_TRIGGER_LOCATION__SET_BAIL_EVENT(WARP_TRANSITION_TRIGGER_LOCATION_STRUCT &sInst, T_BAIL_EVENT fpBailEvent)
	sInst.fpBail = fpBailEvent
ENDPROC

