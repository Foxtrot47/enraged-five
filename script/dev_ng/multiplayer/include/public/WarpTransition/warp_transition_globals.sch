USING "mp_globals.sch"

/// PURPOSE:
///    Checks if the player is in the middle of a warp transition, including its cutscenes
/// PARAMS:
///    piPlayer - the player to check
/// RETURNS:
///    TRUE if the player is in the middle of a warp transition
FUNC BOOL IS_PLAYER_IN_WARP_TRANSITION(PLAYER_INDEX piPlayer)
	RETURN GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.bInWarpTransition
ENDFUNC

/// PURPOSE:
///    Checks if the local player is in the middle of a warp transition, including its cutscenes
/// RETURNS:
///    TRUE if the local player is in the middle of a warp transition
FUNC BOOL IS_LOCAL_PLAYER_IN_WARP_TRANSITION()
	RETURN IS_PLAYER_IN_WARP_TRANSITION(PLAYER_ID())
ENDFUNC

/// PURPOSE:
///    Sets if the player is in the middle of a warp transition
/// PARAMS:
///    piPlayer - the player to set
///    bInWarpTransition - if in the middle of a warp transition
PROC SET_PLAYER_IN_WARP_TRANSITION(PLAYER_INDEX piPlayer, BOOL bInWarpTransition)
	GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.bInWarpTransition = bInWarpTransition
	PRINTLN("[WARP_TRANSITION] SET_PLAYER_IN_WARP_TRANSITION - bIsInWarpTransition: ", bInWarpTransition)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets if the local player is in the middle of a warp transition
/// PARAMS:
///    bInWarpTransition - if in the middle of a warp transition
PROC _SET_LOCAL_PLAYER_IN_WARP_TRANSITION(BOOL bInWarpTransition)
	SET_PLAYER_IN_WARP_TRANSITION(PLAYER_ID(), bInWarpTransition)
ENDPROC

/// PURPOSE:
///    Checks if the player is in the middle of a warp trigger
/// PARAMS:
///    piPlayer - the player to check
/// RETURNS:
///    TRUE if the player is in the middle of a warp trigger
FUNC BOOL IS_PLAYER_IN_WARP_TRANSITION_TRIGGER(PLAYER_INDEX piPlayer)
	RETURN GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.bInWarpTrigger
ENDFUNC

/// PURPOSE:
///    Checks if the local player is in the middle of a warp trigger
/// RETURNS:
///    TRUE if the local player is in the middle of a warp trigger
FUNC BOOL IS_LOCAL_PLAYER_IN_WARP_TRANSITION_TRIGGER()
	RETURN IS_PLAYER_IN_WARP_TRANSITION_TRIGGER(PLAYER_ID())
ENDFUNC

/// PURPOSE:
///    Sets if the player is in the middle of the trigger process for a warp transition trigger
/// PARAMS:
///    piPlayer - the player to set
///    bIsInWarpTrigger - if in a warp trigger
PROC _SET_PLAYER_IN_WARP_TRANSITION_TRIGGER(PLAYER_INDEX piPlayer, BOOL bIsInWarpTrigger)
	GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.bInWarpTrigger = bIsInWarpTrigger
	PRINTLN("[WARP_TRANSITION] _SET_PLAYER_IN_WARP_TRANSITION_TRIGGER - bIsInWarpTrigger: ", bIsInWarpTrigger)
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Sets if the local player is in the middle of the trigger process for a warp transition trigger
/// PARAMS:
///    bIsInWarpTrigger - if in a warp trigger
PROC _SET_LOCAL_PLAYER_IN_WARP_TRANSITION_TRIGGER(BOOL bIsInWarpTrigger)
	_SET_PLAYER_IN_WARP_TRANSITION_TRIGGER(PLAYER_ID(), bIsInWarpTrigger)
ENDPROC

FUNC TRANSFORM_STRUCT GET_PLAYERS_CURRENT_WARP_TRANSITION_ARRIVE_LOCATION(PLAYER_INDEX piPlayer)
	RETURN GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.sArriveLocation
ENDFUNC

FUNC TRANSFORM_STRUCT GET_LOCAL_PLAYERS_CURRENT_WARP_TRANSITION_ARRIVE_LOCATION()
	RETURN GET_PLAYERS_CURRENT_WARP_TRANSITION_ARRIVE_LOCATION(PLAYER_ID())
ENDFUNC

PROC _SET_PLAYERS_CURRENT_WARP_TRANSITION_ARRIVE_LOCATION(PLAYER_INDEX piPlayer, TRANSFORM_STRUCT &sArriveLocation)
	GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.sArriveLocation = sArriveLocation
ENDPROC

PROC _SET_LOCAL_PLAYERS_CURRENT__WARP_TRANSITION_ARRIVE_LOCATION(TRANSFORM_STRUCT &sArriveLocation)
	_SET_PLAYERS_CURRENT_WARP_TRANSITION_ARRIVE_LOCATION(PLAYER_ID(), sArriveLocation)
ENDPROC

FUNC BOOL IS_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_LOCKED_IN(PLAYER_INDEX piPlayer)
	RETURN GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.bArriveLocationLocked
ENDFUNC

PROC _SET_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(PLAYER_INDEX piPlayer, BOOL bLockedIn)
	GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.bArriveLocationLocked = bLockedIn
ENDPROC

PROC _SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(BOOL bLockedIn)
	_SET_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(PLAYER_ID(), bLockedIn)
ENDPROC

PROC _SET_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(PLAYER_INDEX piPlayer, INT iArriveLocationIndex)
	GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.iArriveLocationIndex = iArriveLocationIndex
ENDPROC

PROC _SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(INT iArriveLocationIndex)
	_SET_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(PLAYER_ID(), iArriveLocationIndex)
ENDPROC

FUNC INT GET_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(PLAYER_INDEX piPlayer)
	RETURN GlobalplayerBD[NATIVE_TO_INT(piPlayer)].sWarpTransition.iArriveLocationIndex 
ENDFUNC

FUNC INT GET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX()
	RETURN GET_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(PLAYER_ID())
ENDFUNC

PROC CLEAR_LOCAL_PLAYERS_WARP_TRANSITION_GLOBAL_DATA()
	_SET_LOCAL_PLAYER_IN_WARP_TRANSITION(FALSE)
	TRANSFORM_STRUCT sArriveLocation
	sArriveLocation.Position = <<0.0, 0.0, 0.0>>
	sArriveLocation.Rotation = <<0.0, 0.0, 0.0>>
	_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_INDEX(-1)
	_SET_LOCAL_PLAYERS_CURRENT__WARP_TRANSITION_ARRIVE_LOCATION(sArriveLocation)
	_SET_LOCAL_PLAYERS_WARP_TRANSITION_ARRIVE_LOCATION_AS_LOCKED_IN(FALSE)
ENDPROC
