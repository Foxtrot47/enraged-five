//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        warp_transition_struct.sch																	 
/// Description: Struct for a warp transition
///																											
/// Written by:  Online Technical Team: Tom Turner,															
/// Date:  		22/01/2020																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "warp_transition_config_struct.sch"
USING "warp_transition_state_enum.sch"
USING "script_maths.sch"

STRUCT WARP_TRANSITION_STRUCT
	BOOL bInitialized
	THREADID tiLeaveCutsceneThreadID
	THREADID tiArriveCutsceneThreadID	
	
	SCRIPT_TIMER sWarpTimer
	WARP_TRANSITION_STATE eState
	WARP_TRANSITION_CONFIG_STRUCT sConfigData
ENDSTRUCT

///--------------------------------
///    GETTERS
///--------------------------------    

FUNC WARP_TRANSITION_CONFIG_STRUCT WARP_TRANSITION__GET_CONFIG_DATA(WARP_TRANSITION_STRUCT &sInst)
	RETURN sInst.sConfigData
ENDFUNC

FUNC WARP_TRANSITION_STATE WARP_TRANSITION__GET_STATE(WARP_TRANSITION_STRUCT &sInst)
	RETURN sInst.eState
ENDFUNC

FUNC THREADID WARP_TRANSITION__GET_LEAVE_CUTSCENE_SCRIPT_THREAD_ID(WARP_TRANSITION_STRUCT &sInst)
	RETURN sInst.tiLeaveCutsceneThreadID
ENDFUNC

FUNC THREADID WARP_TRANSITION__GET_ARRIVE_CUTSCENE_SCRIPT_THREAD_ID(WARP_TRANSITION_STRUCT &sInst)
	RETURN sInst.tiArriveCutsceneThreadID
ENDFUNC

FUNC BOOL WARP_TRANSITION__IS_INITIALIZED(WARP_TRANSITION_STRUCT &sInst)
	RETURN sInst.bInitialized
ENDFUNC

///--------------------------------
///    SETTERS
///--------------------------------   

PROC WARP_TRANSITION__SET_CONFIG_DATA(WARP_TRANSITION_STRUCT &sInst, WARP_TRANSITION_CONFIG_STRUCT &sConfigData)
	COPY_SCRIPT_STRUCT(sInst.sConfigData, sConfigData, SIZE_OF(sConfigData))	
ENDPROC

PROC _WT__SET_STATE(WARP_TRANSITION_STRUCT &sInst, WARP_TRANSITION_STATE eState)
	PRINTLN("[WARP_TRANSITION] SET_STATE - transition: ",
	DEBUG_GET_WARP_TRANSITION_STATE_AS_STRING(WARP_TRANSITION__GET_STATE(sInst)), " -> ",
	DEBUG_GET_WARP_TRANSITION_STATE_AS_STRING(eState))
	sInst.eState = eState
	
	CLEAR_BIT_ENUM(sInst.sConfigData.iBS, WTCB_EVENT_NOT_FIRST_CALL)
	sInst.sConfigData.sBailTimer.Timer = NULL
	sInst.sConfigData.sBailTimer.bInitialisedTimer = FALSE
ENDPROC

PROC _WT__SET_LEAVE_CUTSCENE_SCRIPT_THREAD_ID(WARP_TRANSITION_STRUCT &sInst, THREADID tiCutsceneThread)
	sInst.tiLeaveCutsceneThreadID = tiCutsceneThread
ENDPROC

PROC _WT__SET_ARRIVE_CUTSCENE_SCRIPT_THREAD_ID(WARP_TRANSITION_STRUCT &sInst, THREADID tiCutsceneThread)
	sInst.tiArriveCutsceneThreadID = tiCutsceneThread
ENDPROC

PROC _WT__SET_INITIALIZED(WARP_TRANSITION_STRUCT &sInst, BOOL bInitialized)
	sInst.bInitialized = bInitialized
ENDPROC
