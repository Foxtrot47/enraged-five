//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CAR_MEET_PRIZE_VEHICLE_VARS.sch																		//
// Description: Header file containing constants, structs and enums for the Car Meet prize vehicle.						//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		02/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_TUNER
USING "net_include.sch"
USING "net_realty_new.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CONSTANTS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

CONST_INT MAX_NUM_CAR_MEET_PRIZE_VEHICLE_DISPLAY_CHALLENGE_HELP_TEXT	3
CONST_INT PRIZE_VEHICLE_CHALLENGE_ONE_PARAM_ID							7
CONST_INT MAX_PRIZE_VEHICLE_DAY_STREAK_CHALLENGE_ID						3

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ BIT SETS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

// CAR_MEET_PRIZE_VEHICLE_STRUCT.iBS
CONST_INT BS_PRIZE_VEHICLE_DATA_SLAMTRUCK_CREATED						0
CONST_INT BS_PRIZE_VEHICLE_DATA_PRIZE_VEHICLE_CREATED					1
CONST_INT BS_PRIZE_VEHICLE_DATA_ATTACH_PRIZE_VEHICLE_TO_SLAMTRUCK		2

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM CAR_MEET_PRIZE_VEHICLE_CHALLENGE_RACE_TYPE
	CM_PVC_RACE_TYPE_INVALID = -1,
	CM_PVC_RACE_TYPE_LS_CAR_MEET_SERIES = 0,
	CM_PVC_RACE_TYPE_STREET,
	CM_PVC_RACE_TYPE_PURSUIT,
	CM_PVC_RACE_TYPE_SPRINT,
	CM_PVC_RACE_TYPE_MAX
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT
	REPLACE_MP_VEH_OR_PROP_MENU mpsvReplaceVehicle
	INT iResultSlot						= 0
	INT iDisplaySlot					= 0
	INT iControlState					= 0
	INT iTransactionResult				= 0
ENDSTRUCT

STRUCT CAR_MEET_PRIZE_VEHICLE_STRUCT
	INT iBS
	BOOL bDisplayedEntranceHelpText		= FALSE
	VEHICLE_INDEX vehSlamtruck
	VEHICLE_INDEX vehPrizeVehicle
	BLIP_INDEX blipPrizeVehicle
	CAR_MEET_PRIZE_VEHICLE_REWARD_STRUCT RewardVehicleData
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT CAR_MEET_VEHICLE_DEBUG_DATA
	BOOL bFreezeVehiclePosition	= TRUE
	VECTOR vCoords
	VECTOR vCachedCoords
	VECTOR vRotation
	VECTOR vCachedRotation
ENDSTRUCT

STRUCT CAR_MEET_PRIZE_VEHICLE_CUSTOMISATION_DEBUG_DATA
	BOOL bBlockPrizeVehicle				= FALSE
	INT iModelHash						= 0
	INT iColour1						= 0
	INT iColour2						= 0
	INT iExtraColour1					= 0
	INT iExtraColour2					= 0
	INT iColour5						= 0
	INT iColour6						= 0
	INT iLivery							= -1
	INT iWheelType						= 0
	INT iWheelID						= 0
	INT iWindowTintColour				= 0
	INT iLightsColour					= 0
ENDSTRUCT

STRUCT CAR_MEET_PRIZE_VEHICLE_DEBUG_STRUCT
	BOOL bEnableDebug 					= FALSE
	BOOL bWarpToPrizeVehicle			= FALSE
	BOOL bSetChallengeAsComplete		= FALSE
	BOOL bSetChallengeAsIncomplete		= FALSE
	BOOL bResetIterationHelpText		= FALSE
	BOOL bUpdateChallengePIM			= FALSE
	BOOL bOverridePosixTime				= FALSE
	INT iChallengeID					= 0
	INT iChallengeParamOne				= 0
	INT iChallengeParamTwo				= 0
	INT iChallengeProgress				= 0
	INT iChallengePosixTime				= 0
	VECTOR vAttachmentOffsetCoords
	VECTOR vCachedAttachmentOffsetCoords
	VECTOR vAttachmentOffsetRotation
	VECTOR vCachedAttachmentOffsetRotation
	CAR_MEET_VEHICLE_DEBUG_DATA SlamtruckData
	CAR_MEET_VEHICLE_DEBUG_DATA PrizeVehicleData
	CAR_MEET_PRIZE_VEHICLE_CUSTOMISATION_DEBUG_DATA PrizeVehicleCustomisation
	CAR_MEET_PRIZE_VEHICLE_CUSTOMISATION_DEBUG_DATA CachedPrizeVehicleCustomisation
ENDSTRUCT
#ENDIF  // IS_DEBUG_BUILD
#ENDIF	// FEATURE_TUNER
