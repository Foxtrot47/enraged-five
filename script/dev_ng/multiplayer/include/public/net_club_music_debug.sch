//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_MUSIC_DEBUG.sch																				//
// Description: Header file containing debug functionality for club music.				 								//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		30/03/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
#IF IS_DEBUG_BUILD
USING "net_club_music_common.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG FUNCTIONS ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Debug: Gets the DJs name.
/// PARAMS:
///    eClubDJ - The DJ to get the name of.
/// RETURNS: The DJs name as a string.
FUNC STRING GET_CLUB_DJ_NAME(CLUB_DJS eClubDJ)
	STRING sDJName = ""
	SWITCH eClubDJ
		CASE CLUB_DJ_SOLOMUN					sDJName = "CLUB_DJ_SOLOMUN"						BREAK
		CASE CLUB_DJ_TALE_OF_US					sDJName = "CLUB_DJ_TALE_OF_US"					BREAK
		CASE CLUB_DJ_DIXON						sDJName = "CLUB_DJ_DIXON"						BREAK
		CASE CLUB_DJ_BLACK_MADONNA				sDJName = "CLUB_DJ_BLACK_MADONNA"				BREAK
		CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB		sDJName = "CLUB_DJ_KEINEMUSIK_NIGHTCLUB"		BREAK
		CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY		sDJName = "CLUB_DJ_KEINEMUSIK_BEACH_PARTY"		BREAK
		CASE CLUB_DJ_PALMS_TRAX					sDJName = "CLUB_DJ_PALMS_TRAX"					BREAK
		CASE CLUB_DJ_MOODYMANN					sDJName = "CLUB_DJ_MOODYMANN"					BREAK
		CASE CLUB_DJ_DR_DRE_ABSENT				sDJName = "CLUB_DJ_DR_DRE_ABSENT"				BREAK
		CASE CLUB_DJ_DR_DRE_MIX					sDJName = "CLUB_DJ_DR_DRE_MIX"					BREAK
		CASE CLUB_DJ_DR_DRE_MIX_VOCALS			sDJName = "CLUB_DJ_DR_DRE_MIX_VOCALS"			BREAK
		CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS		sDJName = "CLUB_DJ_DR_DRE_MUSICIAN_DRUMS"		BREAK
		CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS		sDJName = "CLUB_DJ_DR_DRE_MUSICIAN_VOCALS"		BREAK
		CASE CLUB_DJ_DR_DRE_MIX_2				sDJName = "CLUB_DJ_DR_DRE_MIX_2"				BREAK
	ENDSWITCH	
	RETURN sDJName
ENDFUNC

/// PURPOSE:
///    Debug: Gets the DJs station name.
/// PARAMS:
///    CLUB_RADIO_STATIONS - The Station to get the name of.
/// RETURNS: The DJ stations name as a string.
FUNC STRING GET_CLUB_DJ_STATION_NAME(CLUB_RADIO_STATIONS eClubDJ)
	STRING sDJStationName = ""
	SWITCH eClubDJ
		CASE CLUB_RADIO_STATION_SOLOMUN					sDJStationName = "CLUB_RADIO_STATION_SOLOMUN"					BREAK
		CASE CLUB_RADIO_STATION_TALE_OF_US				sDJStationName = "CLUB_RADIO_STATION_TALE_OF_US"				BREAK
		CASE CLUB_RADIO_STATION_DIXON					sDJStationName = "CLUB_RADIO_STATION_DIXON"						BREAK
		CASE CLUB_RADIO_STATION_BLACK_MADONNA			sDJStationName = "CLUB_RADIO_STATION_BLACK_MADONNA"				BREAK
		CASE CLUB_RADIO_STATION_SOLOMUN_PRIVATE			sDJStationName = "CLUB_RADIO_STATION_SOLOMUN_PRIVATE"			BREAK
		CASE CLUB_RADIO_STATION_TALE_OF_US_PRIVATE		sDJStationName = "CLUB_RADIO_STATION_TALE_OF_US_PRIVATE"		BREAK
		CASE CLUB_RADIO_STATION_DIXON_PRIVATE			sDJStationName = "CLUB_RADIO_STATION_DIXON_PRIVATE"				BREAK
		CASE CLUB_RADIO_STATION_BLACK_MADONNA_PRIVATE	sDJStationName = "CLUB_RADIO_STATION_BLACK_MADONNA_PRIVATE"		BREAK
		CASE CLUB_RADIO_STATION_KEINEMUSIK_NIGHTCLUB	sDJStationName = "CLUB_RADIO_STATION_KEINEMUSIK_NIGHTCLUB"		BREAK
		CASE CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY	sDJStationName = "CLUB_RADIO_STATION_KEINEMUSIK_BEACH_PARTY"	BREAK
		CASE CLUB_RADIO_STATION_PALMS_TRAX				sDJStationName = "CLUB_RADIO_STATION_PALMS_TRAX"				BREAK
		CASE CLUB_RADIO_STATION_MOODYMANN				sDJStationName = "CLUB_RADIO_STATION_MOODYMANN"					BREAK
		CASE CLUB_RADIO_STATION_DR_DRE_ABSENT			sDJStationName = "CLUB_RADIO_STATION_DR_DRE_ABSENT"				BREAK
		CASE CLUB_RADIO_STATION_DR_DRE_MIX				sDJStationName = "CLUB_RADIO_STATION_DR_DRE_MIX"				BREAK
		CASE CLUB_RADIO_STATION_DR_DRE_MIX_VOCALS		sDJStationName = "CLUB_RADIO_STATION_DR_DRE_MIX_VOCALS"			BREAK
		CASE CLUB_RADIO_STATION_DR_DRE_MUSICIAN_DRUMS	sDJStationName = "CLUB_RADIO_STATION_DR_DRE_MUSICIAN_DRUMS"		BREAK
		CASE CLUB_RADIO_STATION_DR_DRE_MUSICIAN_VOCALS	sDJStationName = "CLUB_RADIO_STATION_DR_DRE_MUSICIAN_VOCALS"	BREAK
		CASE CLUB_RADIO_STATION_DR_DRE_MIX_2			sDJStationName = "CLUB_RADIO_STATION_DR_DRE_MIX_2"				BREAK
		
	ENDSWITCH	
	RETURN sDJStationName
ENDFUNC

/// PURPOSE:
///    Converts milliseconds into minutes.
/// PARAMS:
///    iMilliseconds - milliseconds to convert.
/// RETURNS: Converted milliseconds as FLOAT minutes.
FUNC FLOAT CONVERT_MILLISECONDS_INTO_MINUTES(INT iMilliseconds)
	RETURN (TO_FLOAT(iMilliseconds)/1000.0)/60.0
ENDFUNC

/// PURPOSE:
///    Converts milliseconds into minutes. Uses clock time - remainder will be out of 0-60 instead of 0-100.
/// PARAMS:
///    iMilliseconds - Milliseconds to convert into minutes.
/// RETURNS: Converted minutes as clock time. Returned as a FLOAT.
FUNC FLOAT CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(INT iMilliseconds)
	
	// Get the minutes with remainder going from 0-100
	FLOAT fMinutes = CONVERT_MILLISECONDS_INTO_MINUTES(iMilliseconds)
	
	// Get the remainder value on its own
	INT iFlooredMinutes = FLOOR(fMinutes)
	FLOAT fRemainderSeconds = fMinutes-TO_FLOAT(iFlooredMinutes)
	
	// Convert the remainder from 0-100 into clock time 0-60 seconds
	FLOAT fSeconds = (fRemainderSeconds*60.0)
	fSeconds /= 100.0
	fSeconds += TO_FLOAT(iFlooredMinutes)
	
	RETURN fSeconds
ENDFUNC

/// PURPOSE:
///    Output debug info about the club music data.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    bRemotePlayerData - Whether to output host or remote player data.
PROC PRINT_CLUB_MUSIC_DATA(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA DJServerData, BOOL bRemotePlayerData = FALSE)
	PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - DJ: ", GET_CLUB_DJ_NAME(DJServerData.eDJ))
	PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Track: ", DJServerData.iMusicTrack)
	PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Radio Station: ", GET_CLUB_RADIO_STATION_NAME(eClubLocation, DJServerData.eRadioStation))
	PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Music Track: ", DJServerData.iMusicTrack)
	PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Intensity: ", GET_CLUB_MUSIC_INTENSITY_NAME(DJServerData.eMusicIntensity))
	IF (bRemotePlayerData)
		INT iCurrntMixTimeMS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DJServerData.stMusicTimer, DEFAULT, TRUE)
		PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Starting radio station mix time MS: ", iCurrntMixTimeMS)
		PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Starting radio station track time MS: ", GET_CLUB_RADIO_STATION_TRACK_TIME_FROM_MIX_TIME_MS(eClubLocation, DJServerData, iCurrntMixTimeMS))
	ELSE
		PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Starting Track Time MS: ", GET_CLUB_RADIO_STATION_TRACK_TIME_FROM_MIX_TIME_MS(eClubLocation, DJServerData, DJServerData.iStartingMixTimeMS))
		PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Starting Mix Time MS: ", DJServerData.iStartingMixTimeMS, " Mins: ", CONVERT_MILLISECONDS_INTO_MINUTES(DJServerData.iStartingMixTimeMS), " Mins Clock Time: ", CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(DJServerData.iStartingMixTimeMS))
		PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Intensity ID: ", DJServerData.iTrackIntensityID)
		PRINTLN("[CLUB_MUSIC] PRINT_CLUB_MUSIC_DATA - Next Intensity Time MS: ", DJServerData.iNextTrackIntensityTimeMS, " Mins: ", CONVERT_MILLISECONDS_INTO_MINUTES(DJServerData.iNextTrackIntensityTimeMS), " Mins Clock Time: ", CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(DJServerData.iNextTrackIntensityTimeMS))
	ENDIF
	PRINTLN("[CLUB_MUSIC] ")
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════╡ DEBUG WIDGETS - ON-SCREEN DEBUG ╞═══════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Renders the on-screen debug to the screen.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    DJLocalData - Local data to query..
PROC DISPLAY_CLUB_MUSIC_DEBUG(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA DJServerData, DJ_LOCAL_DATA &DJLocalData)
	
	// Aspect Ratio
	FLOAT fAspectRatio = GET_SCREEN_ASPECT_RATIO()
	FLOAT fStartingAspectRatio = 1.333333
	FLOAT fAspectRatioDiff = (fStartingAspectRatio/fAspectRatio)+0.065
	
	// Background
	DRAW_RECT(0.364, 0.165, 0.694, 0.304, 255, 255, 255, 170)
	
	// DJ
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.025, "STRING", "DJ: ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.044*fAspectRatioDiff, 0.025, "STRING", GET_CLUB_DJ_NAME(DJLocalData.eDJ))
	
	// Radio Station
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.050, "STRING", "Radio Station: ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.102*fAspectRatioDiff, 0.050, "STRING", GET_CLUB_RADIO_STATION_NAME(eClubLocation, DJServerData.eRadioStation))
	
	// Music Track
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.075, "STRING", "Music Track: ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.094*fAspectRatioDiff, 0.075, "STRING", GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
	
	// Music Track ID
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.100, "STRING", "Music Track ID: ")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.110*fAspectRatioDiff, 0.100, "NUMBER", DJServerData.iMusicTrack)
	
	// Track Progression in Minutes
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.125, "STRING", "Track Progression (Mins): ")
	
	INT iTrackDurationMS 		= GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)
	FLOAT fCurrentTrackTimeMins = CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(g_clubMusicData.iCurrentTrackTimeMS)
	FLOAT fTrackDurationMins	= CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(iTrackDurationMS)
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.170*fAspectRatioDiff, 0.125, "NUMBER", fCurrentTrackTimeMins, 2)
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.200*fAspectRatioDiff, 0.125, "STRING", "/")
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(0, 0, 0, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.210*fAspectRatioDiff, 0.125, "NUMBER", fTrackDurationMins, 2)
	
	IF DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
		// Track Intensity ID
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 50, 50, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.150, "STRING", "Track Intensity ID: ")
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(0, 0, 0, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.124*fAspectRatioDiff, 0.150, "NUMBER", DJServerData.iTrackIntensityID)
		
		// Track Intensity
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 50, 50, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.175, "STRING", "Track Intensity: ")
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(0, 0, 0, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.108*fAspectRatioDiff, 0.175, "STRING", GET_CLUB_MUSIC_INTENSITY_NAME(DJLocalData.eMusicIntensity))
		
		// Track Intensity About To Change
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(255, 50, 50, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.200, "STRING", "Seconds Until Intensity Change: ")
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(0, 0, 0, 255)
		DISPLAY_TEXT_WITH_NUMBER(0.205*fAspectRatioDiff, 0.200, "NUMBER", g_iTimeUntilPedDanceTransitionSecs)
		
		IF (DJLocalData.eNextMusicIntensity != CLUB_MUSIC_INTENSITY_NULL)
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.250*fAspectRatioDiff, 0.200, "STRING", "Next Intensity: ")
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.328*fAspectRatioDiff, 0.200, "STRING", GET_CLUB_MUSIC_INTENSITY_NAME(DJLocalData.eNextMusicIntensity))
		ENDIF
	ENDIF
	INT iAnimDuration 	
	FLOAT fCurrentAnimTimeMins 
	FLOAT fAnimDurationMins
				
	SWITCH DJServerData.eDJ
		CASE CLUB_DJ_DR_DRE_ABSENT
		CASE CLUB_DJ_DR_DRE_MIX
		CASE CLUB_DJ_DR_DRE_MIX_VOCALS
		CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
		CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
		CASE CLUB_DJ_DR_DRE_MIX_2
			SWITCH  DJServerData.eDJ
				CASE CLUB_DJ_DR_DRE_ABSENT
					g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_INVALID)
					g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT)
					g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_INVALID)
				BREAK
				CASE CLUB_DJ_DR_DRE_MIX
				CASE CLUB_DJ_DR_DRE_MIX_VOCALS
					g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING)
					g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING)
					g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_INVALID)
				BREAK
				CASE CLUB_DJ_DR_DRE_MIX_2	
					g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2)
					g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2)
					g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_INVALID)
				BREAK	
				CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
					g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS)
					g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS)
					g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS)
				BREAK	
				CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
					g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS)
					g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS)
					g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS)
				BREAK
			ENDSWITCH		
			
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.250*fAspectRatioDiff, 0.175, "STRING", "Dre Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.330*fAspectRatioDiff, 0.175, "NUMBER", g_DebugAnimSeqDetail.iClip[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.370*fAspectRatioDiff, 0.175, "STRING", g_DebugAnimSeqDetail.sClipName[0])			
						
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.175, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.175, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[0])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[0] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[0])
				// Anim Progression in Minutes
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.175, "STRING", "Anim Progression(Mins): ")
				
				iAnimDuration 		= FLOOR(GET_ANIM_DURATION(g_DebugAnimSeqDetail.sDictName[0], g_DebugAnimSeqDetail.sClipName[0]) * 1000)
				fCurrentAnimTimeMins = CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(FLOOR(GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[0]) * iAnimDuration))
				fAnimDurationMins	= CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(iAnimDuration)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.170*fAspectRatioDiff, 0.175, "NUMBER", fCurrentAnimTimeMins, 2)
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.200*fAspectRatioDiff, 0.175, "STRING", "/")
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.210*fAspectRatioDiff, 0.175, "NUMBER", fAnimDurationMins, 2)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.175, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.175, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.175, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.175, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 4)
			ENDIF
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.250*fAspectRatioDiff, 0.200, "STRING", "Lola Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.330*fAspectRatioDiff, 0.200, "NUMBER", g_DebugAnimSeqDetail.iClip[1])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.370*fAspectRatioDiff, 0.200, "STRING", g_DebugAnimSeqDetail.sClipName[1])			
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.200, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.200, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[1])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[1] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[1])
				// Anim Progression in Minutes
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.200, "STRING", "Anim Progression(Mins): ")
				
				iAnimDuration 		= FLOOR(GET_ANIM_DURATION(g_DebugAnimSeqDetail.sDictName[1], g_DebugAnimSeqDetail.sClipName[1]) * 1000)
				fCurrentAnimTimeMins = CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(FLOOR(GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[1]) * iAnimDuration))
				fAnimDurationMins	= CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(iAnimDuration)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.170*fAspectRatioDiff, 0.200, "NUMBER", fCurrentAnimTimeMins, 2)
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.200*fAspectRatioDiff, 0.200, "STRING", "/")
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.210*fAspectRatioDiff, 0.200, "NUMBER", fAnimDurationMins, 2)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.200, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.200, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[1]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.200, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.200, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[1]), 4)
			ENDIF
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.250*fAspectRatioDiff, 0.225, "STRING", "AP Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.330*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iClip[2])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.370*fAspectRatioDiff, 0.225, "STRING", g_DebugAnimSeqDetail.sClipName[2])			
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.225, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[2])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[2] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[2])
				// Anim Progression in Minutes
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.225, "STRING", "Anim Progression(Mins): ")
				
				iAnimDuration 		= FLOOR(GET_ANIM_DURATION(g_DebugAnimSeqDetail.sDictName[2], g_DebugAnimSeqDetail.sClipName[2]) * 1000)
				fCurrentAnimTimeMins = CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(FLOOR(GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[2]) * iAnimDuration))
				fAnimDurationMins	= CONVERT_MILLISECONDS_INTO_MINUTES_CLOCK_TIME(iAnimDuration)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.170*fAspectRatioDiff, 0.225, "NUMBER", fCurrentAnimTimeMins, 2)
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.200*fAspectRatioDiff, 0.225, "STRING", "/")
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.210*fAspectRatioDiff, 0.225, "NUMBER", fAnimDurationMins, 2)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.225, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[2]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.225, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[2]), 4)
			ENDIF
		BREAK
		CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
		CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
			g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ADAM)
			g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ME)
			g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_RAMPA)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.225, "STRING", "Adam Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iClip[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.225, "STRING", g_DebugAnimSeqDetail.sClipName[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.225, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[0])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[0] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[0])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.225, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.225, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 4)
			ENDIF
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.250, "STRING", "Me Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.250, "NUMBER", g_DebugAnimSeqDetail.iClip[1])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.250, "STRING", g_DebugAnimSeqDetail.sClipName[1])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.250, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.250, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[1])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[1] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[1])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.250, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.250, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[1]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.250, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.250, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[1]), 4)
			ENDIF
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.275, "STRING", "Rampa Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.275, "NUMBER", g_DebugAnimSeqDetail.iClip[2])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.275, "STRING", g_DebugAnimSeqDetail.sClipName[2])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.275, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.275, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[2])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[2] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[2])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.275, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.275, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[2]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.275, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.275, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[2]), 4)
			ENDIF
		BREAK
		CASE CLUB_DJ_MOODYMANN
			g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_DJ_MOODYMANN)
			g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_01)
			g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_02)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.225, "STRING", "MM Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iClip[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.225, "STRING", g_DebugAnimSeqDetail.sClipName[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.225, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[0])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[0] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[0])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.225, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.225, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 4)
			ENDIF
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.250, "STRING", "Dancer 1 Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.250, "NUMBER", g_DebugAnimSeqDetail.iClip[1])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.250, "STRING", g_DebugAnimSeqDetail.sClipName[1])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.250, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.250, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[1])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[1] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[1])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.250, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.250, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[1]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.250, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.250, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[1]), 4)
			ENDIF
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.275, "STRING", "Dancer 2 Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.275, "NUMBER", g_DebugAnimSeqDetail.iClip[2])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.275, "STRING", g_DebugAnimSeqDetail.sClipName[2])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.275, "STRING", "Sync Scene: ")

			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.275, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[2])

			IF (g_DebugAnimSeqDetail.iSyncSceneID[2] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[2])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.275, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.275, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[2]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.275, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.275, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[2]), 4)
			ENDIF
		BREAK
		CASE CLUB_DJ_PALMS_TRAX
			g_DebugAnimSeqDetail.pedActivity[0] = ENUM_TO_INT(PED_ACT_DJ_PALMS_TRAX)
			g_DebugAnimSeqDetail.pedActivity[1] = ENUM_TO_INT(PED_ACT_INVALID)
			g_DebugAnimSeqDetail.pedActivity[2] = ENUM_TO_INT(PED_ACT_INVALID)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.225, "STRING", "PT Clip: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.100*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iClip[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.140*fAspectRatioDiff, 0.225, "STRING", g_DebugAnimSeqDetail.sClipName[0])
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.600*fAspectRatioDiff, 0.225, "STRING", "Sync Scene: ")
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(0, 0, 0, 255)
			DISPLAY_TEXT_WITH_NUMBER(0.670*fAspectRatioDiff, 0.225, "NUMBER", g_DebugAnimSeqDetail.iSyncSceneID[0])
			
			IF (g_DebugAnimSeqDetail.iSyncSceneID[0] != -1)
			AND IS_SYNCHRONIZED_SCENE_RUNNING(g_DebugAnimSeqDetail.iSyncSceneID[0])
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.710*fAspectRatioDiff, 0.225, "STRING", "Phase: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.750*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_PHASE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 3)
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(255, 50, 50, 255)
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.790*fAspectRatioDiff, 0.225, "STRING", "Rate: ")
				
				SET_TEXT_SCALE(0.3, 0.3)
				SET_TEXT_COLOUR(0, 0, 0, 255)
				DISPLAY_TEXT_WITH_FLOAT(0.825*fAspectRatioDiff, 0.225, "NUMBER", GET_SYNCHRONIZED_SCENE_RATE(g_DebugAnimSeqDetail.iSyncSceneID[0]), 4)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Renders the on-screen debug to the screen.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    DJLocalData - Local data to query..
PROC DISPLAY_SLIMMER_CLUB_MUSIC_DEBUG(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA DJServerData, DJ_LOCAL_DATA &DJLocalData)
	
	// Aspect Ratio
	FLOAT fAspectRatio = GET_SCREEN_ASPECT_RATIO()
	FLOAT fStartingAspectRatio = 1.333333
	FLOAT fAspectRatioDiff = (fStartingAspectRatio/fAspectRatio)+0.065

	// Music Track
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.025, "STRING", "Music Track: ")
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.074*fAspectRatioDiff, 0.025, "STRING", GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
	
	// Track Progression in Minutes
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.035, "STRING", "Track Progression (Mins): ")
	
	INT iTrackDurationMS 		= GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)
	FLOAT fCurrentTrackTimeMins = CONVERT_MILLISECONDS_INTO_MINUTES(g_clubMusicData.iCurrentTrackTimeMS)
	FLOAT fTrackDurationMins	= CONVERT_MILLISECONDS_INTO_MINUTES(iTrackDurationMS)
	
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.120*fAspectRatioDiff, 0.035, "NUMBER", fCurrentTrackTimeMins, 2)
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.145*fAspectRatioDiff, 0.035, "STRING", "/")
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.150*fAspectRatioDiff, 0.035, "NUMBER", fTrackDurationMins, 2)

	// Set Progression in Minutes
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.045, "STRING", "Set Progression (Mins): ")

	FLOAT fCurrentSetTimeMins = CONVERT_MILLISECONDS_INTO_MINUTES(g_clubMusicData.iCurrentMixTimeMS)
	FLOAT fTrackSetDurationMins	= CONVERT_MILLISECONDS_INTO_MINUTES(GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData, TRUE))
	
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.120*fAspectRatioDiff, 0.045, "NUMBER", fCurrentSetTimeMins, 2)
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.145*fAspectRatioDiff, 0.045, "STRING", "/")
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_FLOAT(0.150*fAspectRatioDiff, 0.045, "NUMBER", fTrackSetDurationMins, 2)

	// Track Intensity
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.055, "STRING", "Track Intensity: ")
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.078*fAspectRatioDiff, 0.055, "STRING", GET_CLUB_MUSIC_INTENSITY_NAME(DJLocalData.eMusicIntensity))
	
	// Track Intensity About To Change
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 50, 50, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.065, "STRING", "Seconds Until Intensity Change: ")
	
	SET_TEXT_SCALE(0.2, 0.2)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.140*fAspectRatioDiff, 0.065, "NUMBER", g_iTimeUntilPedDanceTransitionSecs)
	
	IF (DJLocalData.eNextMusicIntensity != CLUB_MUSIC_INTENSITY_NULL)
		SET_TEXT_SCALE(0.2, 0.2)
		SET_TEXT_COLOUR(255, 50, 50, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, 0.075, "STRING", "Next Intensity: ")
		SET_TEXT_SCALE(0.2, 0.2)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.078*fAspectRatioDiff, 0.075, "STRING", GET_CLUB_MUSIC_INTENSITY_NAME(DJLocalData.eNextMusicIntensity))
	ENDIF
	
ENDPROC

FUNC BOOL DEBUG_HAS_DJ_DATAFILE_LOADED(CLUB_LOCATIONS eClubLocation, CLUB_DJS eClubDJ)
	TEXT_LABEL_63 sDataFileName
	IF DOES_CLUB_LOCATION_USE_DATAFILE(eClubLocation, eClubDJ, sDataFileName)
		CLEANUP_ADDITIONAL_LOCAL_DATA_FILE()
		RETURN DATAFILE_LOAD_OFFLINE_UGC_FOR_ADDITIONAL_DATA_FILE(0, sDataFileName)
	ENDIF
	RETURN TRUE
ENDFUNC 

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ UPDATE WIDGETS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Updates all the club music debug widgets.
/// PARAMS:
///    eClubLocation - Club location.
///    DJDebugData - Debug data to populate.
///    DJServerData - Server data to query.
///    DJLocalData - Local data to query.
PROC UPDATE_CLUB_MUSIC_WIDGETS(CLUB_LOCATIONS eClubLocation, DJ_DEBUG_DATA &DJDebugData, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	
	IF (DJDebugData.bDisplayDebug)
		DISPLAY_CLUB_MUSIC_DEBUG(eClubLocation, DJServerData, DJLocalData)
	ENDIF
	
	IF (DJDebugData.bDisplaySlimmedDebug)	
		DISPLAY_SLIMMER_CLUB_MUSIC_DEBUG(eClubLocation, DJServerData, DJLocalData)
	ENDIF
	
	// Host has changed debug settings of music track
	IF (NETWORK_IS_HOST_OF_THIS_SCRIPT() AND DJDebugData.bResetMusicTrack)
		g_clubMusicData.eDebugNextDJ = INT_TO_ENUM(CLUB_DJS, DJDebugData.iDJ)
		
		// Start timer to give server BD a chance to update and then send debug event for remote players to update
		IF NOT HAS_NET_TIMER_STARTED(DJDebugData.stHostUpdateMusicData)
			START_NET_TIMER(DJDebugData.stHostUpdateMusicData)
			
			// Update music data to widget data
			DJServerData.eRadioStation 				= GET_CLUB_RADIO_STATION(eClubLocation, DJServerData.eDJ)
			
			IF DJDebugData.iMusicTrack > _GET_CLUB_DJ_RADIO_STATION_MAX_TRACKS(DJServerData.eRadioStation)
				DJDebugData.iMusicTrack = _GET_CLUB_DJ_RADIO_STATION_MAX_TRACKS(DJServerData.eRadioStation)
			ENDIF
			
			DJServerData.iMusicTrack 				= DJDebugData.iMusicTrack
			DJServerData.iMusicTrackNameHash 		= GET_HASH_KEY(GET_CLUB_RADIO_STATION_TRACK_NAME(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack))
			DJServerData.iTrackDurationMixTimeMS 	= GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)
			
			// Validate track starting min from widget
			INT iWidgetTrackStartingTimeMS = FLOOR((DJDebugData.fTrackStartingMin*60.0)*1000.0)
			INT iTrackDuration = GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)
			IF (iWidgetTrackStartingTimeMS >= iTrackDuration)
				iWidgetTrackStartingTimeMS = (iTrackDuration-10000)
				DJDebugData.fTrackStartingMin = CONVERT_MILLISECONDS_INTO_MINUTES(iWidgetTrackStartingTimeMS)
			ENDIF
			
			DJServerData.iStartingMixTimeMS 		= GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(eClubLocation, DJServerData, iWidgetTrackStartingTimeMS)
			REINIT_NET_TIMER(DJServerData.stMusicTimer)
			DJServerData.stMusicTimer.Timer 		= GET_TIME_OFFSET(DJServerData.stMusicTimer.Timer, -DJServerData.iStartingMixTimeMS)
			
			IF DOES_CLUB_USE_MUSIC_INTENSITY(eClubLocation)
				DJServerData.iTrackIntensityID 			= GET_CLUB_RADIO_STATION_TRACK_INTENSITY_ID(eClubLocation, DJServerData)
				DJServerData.iNextTrackIntensityTimeMS  = GET_CLUB_RADIO_STATION_TRACK_INTENSITY_TIME_MS(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
				DJServerData.eMusicIntensity 			= GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID)
				DJServerData.eNextMusicIntensity 		= GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(eClubLocation, DJServerData, DJServerData.iTrackIntensityID+1)
			ENDIF
			
			DJLocalData.bMusicPlaying				= FALSE
			
		ELIF HAS_NET_TIMER_STARTED_AND_EXPIRED(DJDebugData.stHostUpdateMusicData, DEBUG_MAX_SERVER_BD_UPDATE_TIME_MS)
			RESET_NET_TIMER(DJDebugData.stHostUpdateMusicData)
			DEBUG_BROADCAST_CLUB_MUSIC_RESET_TRACK(DJServerData.eDJ)
			DJDebugData.bResetMusicTrack = FALSE
		ENDIF
	ENDIF
	
	// DJ Widget Text
	IF DOES_TEXT_WIDGET_EXIST(DJDebugData.twDJ)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_CONTENTS_OF_TEXT_WIDGET(DJDebugData.twDJ, GET_CLUB_DJ_NAME(DJServerData.eDJ))
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DJDebugData.twDJ, "-")
		ENDIF
	ENDIF
	
	// Next DJ
	IF DOES_TEXT_WIDGET_EXIST(DJDebugData.twNextDJ)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_CONTENTS_OF_TEXT_WIDGET(DJDebugData.twNextDJ, GET_CLUB_DJ_NAME(INT_TO_ENUM(CLUB_DJS, DJDebugData.iDJ)))
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DJDebugData.twNextDJ, "-")
		ENDIF
	ENDIF
	
	// Host status message
	IF DOES_TEXT_WIDGET_EXIST(DJDebugData.twHostMessage)
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			SET_CONTENTS_OF_TEXT_WIDGET(DJDebugData.twHostMessage, "You are host")
		ELSE
			SET_CONTENTS_OF_TEXT_WIDGET(DJDebugData.twHostMessage, "You are not host - Do not use these widgets! - Locking values")
		ENDIF
	ENDIF

	// Lock values if not host
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		DJDebugData.iDJ 						= 0
		DJDebugData.iMusicTrack 				= 0
		DJDebugData.fTrackStartingMin 			= 0.0
		DJDebugData.bResetMusicTrack 			= FALSE
	ENDIF
	
	// Next Intensity
	IF (DJLocalData.eNextMusicIntensity = DJLocalData.eMusicIntensity)
		DJLocalData.eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL
	ENDIF
	
	IF DJDebugData.bMusicPlaying
		DJLocalData.bMusicPlaying = FALSE
		DJDebugData.bMusicPlaying = FALSE
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ CREATE WIDGETS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Creates the club music debug widgets..
/// PARAMS:
///    DJDebugData - Debug data to populate.
PROC CREATE_CLUB_MUSIC_WIDGETS(DJ_DEBUG_DATA &DJDebugData)
	START_WIDGET_GROUP("Club Music")
		ADD_WIDGET_BOOL("Display Debug", DJDebugData.bDisplayDebug)
		ADD_WIDGET_BOOL("Display Slimmed Debug", DJDebugData.bDisplaySlimmedDebug)
		ADD_WIDGET_BOOL("Toggle music playing var", DJDebugData.bMusicPlaying)
		START_WIDGET_GROUP("Host Widgets")
			DJDebugData.twHostMessage = ADD_TEXT_WIDGET("Host Status:")
			ADD_WIDGET_STRING("")
			DJDebugData.twDJ = ADD_TEXT_WIDGET("Current DJ:")
			ADD_WIDGET_INT_SLIDER("Next DJ", DJDebugData.iDJ, -1, (ENUM_TO_INT(CLUB_DJ_TOTAL)-1), 1)
			DJDebugData.twNextDJ = ADD_TEXT_WIDGET("Next DJ:")
			ADD_WIDGET_INT_SLIDER("Music Track", DJDebugData.iMusicTrack, 1, 4, 1)
			ADD_WIDGET_FLOAT_SLIDER("Track Starting Minute", DJDebugData.fTrackStartingMin, 0.0, TO_FLOAT(DEBUG_MAX_CLUB_RADIO_STATION_TRACK_DURATION_MINS), 0.1)
			ADD_WIDGET_BOOL("Reset Music Track", DJDebugData.bResetMusicTrack)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF //IS_DEBUG_BUILD
#ENDIF //FEATURE_HEIST_ISLAND
