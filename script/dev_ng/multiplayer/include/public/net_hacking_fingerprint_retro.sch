USING "net_hacking_fingerprint_common.sch"

CONST_INT ciFR_SELECTED			0
CONST_INT ciFR_REQUIRED			1

CONST_INT ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS 8
CONST_INT ciFR_TARGET_NUMBER_OF_ELEMENTS 4
CONST_INT ciFR_NUMBER_OF_PRINTS_INGAME 8
CONST_INT ciFR_FINGEPRINT_VARAIATIONS 8

// Bit flags
CONST_INT ciFR_FINGERPRINTS_SELECTED 0
CONST_INT ciFR_CHECKING_MATCH 1
CONST_INT ciFR_SHOWING_LOADING 2
CONST_INT ciFR_PASSING_FINGERPRINT 3
CONST_INT ciFR_FAILED_FINGERPRINT 4
CONST_INT ciFR_SCRAMBLING 5
CONST_INT ciFR_SCRAMBLED 6
CONST_INT ciFR_FULL_MATCH 7
CONST_INT ciFR_SHOWN_HELP 8

CONST_INT ciFR_UI_ROW_LENGTH 1
CONST_INT ciFR_UI_COLUMN_LENGTH 8

CONST_INT ciFR_MAX_LIVES  6
CONST_INT ciFR_STARTUP_TIMER 8000
CONST_INT ciFR_HACK_TIME  300000
CONST_INT ciFR_HACK_TIME_FULL  300000
CONST_INT ciFR_CHECKING_TIME 4000
CONST_INT ciFR_PRE_CHECK_TIME 2000
CONST_INT ciFR_SCRAMBLE_TIME 90000
CONST_INT ciFR_SCRAMBLE_TIME_FULL 90000
CONST_INT ciFR_END_DELAY 8000

TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_COLUMN1 0.182
TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_ROW1 0.358
TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_SPACING 0.07037

TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_WIDTH 468.0
TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_HEIGHT 72.0
TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_SELECTOR_WIDTH 468.0
TWEAK_FLOAT cfFR_FINGERPRINT_ELEMENT_SELECTOR_HEIGHT 72.0

TWEAK_FLOAT cfFR_FINGERPRINT_LARGE_POSITIONX 0.75
TWEAK_FLOAT cfFR_FINGERPRINT_LARGE_POSITIONY 0.6
TWEAK_FLOAT cfFR_FINGERPRINT_LARGE_WIDTH 512.0
TWEAK_FLOAT cfFR_FINGERPRINT_LARGE_HEIGHT 648.0

CONST_FLOAT cfFR_CIRCLEDECORATION_POSITIONX 0.983
CONST_FLOAT cfFR_CIRCLEDECORATION_POSITIONY 0.679
CONST_FLOAT cfFR_CIRCLEDECORATION_WIDTH 100.0
CONST_FLOAT cfFR_CIRCLEDECORATION_HEIGHT 100.0
CONST_INT ciFR_CIRCLEDECORATION_FRAMES 3

CONST_FLOAT cfFR_FINGERPRINT_TRACKER_WIDTH 100.0
CONST_FLOAT cfFR_FINGERPRINT_TRACKER_HEIGHT 128.0
TWEAK_FLOAT cfFR_FINGERPRINT_TRACKER_POSITIONY 0.182
TWEAK_FLOAT cfFR_FINGERPRINT_TRACKER1_POSITIONX 0.581
TWEAK_FLOAT cfFR_FINGERPRINT_TRACKER2_POSITIONX 0.707
TWEAK_FLOAT cfFR_FINGERPRINT_TRACKER3_POSITIONX 0.833
TWEAK_FLOAT cfFR_FINGERPRINT_TRACKER4_POSITIONX 0.959
CONST_FLOAT cfFR_FINGERPRINT_TRACKER_SELECTOR_WIDTH 128.0
CONST_FLOAT cfFR_FINGERPRINT_TRACKER_SELECTOR_HEIGHT 128.0

CONST_FLOAT cfFR_TIMER_POSITIONX 0.1325
CONST_FLOAT cfFR_TIMER_POSITIONY 0.144
CONST_FLOAT cfFR_TIMER_DIGIT_DIFFERENCE 0.031

TWEAK_FLOAT cfFR_LIVES_POSITIONX 0.964
TWEAK_FLOAT cfFR_LIVES_POSITIONY 0.340
TWEAK_FLOAT	cfFR_LIVES_OFFSET 0.075

//TWEAK_FLOAT cfFR_SCRAMBLE_POSITIONX 0.184
//TWEAK_FLOAT cfFR_SCRAMBLE_POSITIONY 0.235
//TWEAK_FLOAT cfFR_SCRAMBLE_LEFTPOINT 0.097
//
//TWEAK_FLOAT cfFR_SCRAMBLE_WIDTH 500.0
//TWEAK_FLOAT cfFR_SCRAMBLE_HEIGHT 28.0
//TWEAK_FLOAT cfFR_SCRAMBLE_ELEMENT_WIDTH 9.0
//TWEAK_FLOAT cfFR_SCRAMBLE_ELEMENT_HEIGHT 18.0
//TWEAK_FLOAT cfFR_SCRAMBLE_DIFFBETWEENSEGMENT 0.0084
//TWEAK_FLOAT cfFR_SCRAMBLE_Y_OFFSET 0.0

TWEAK_FLOAT cfFR_SCRAMBLE_POSITIONX 0.184
TWEAK_FLOAT cfFR_SCRAMBLE_POSITIONY 0.235
TWEAK_FLOAT cfFR_SCRAMBLE_LEFTPOINT 0.23337
TWEAK_FLOAT cfFR_SCRAMBLE_WIDTH 500.0
TWEAK_FLOAT cfFR_SCRAMBLE_HEIGHT 28.0
TWEAK_FLOAT cfFR_SCRAMBLE_ELEMENT_WIDTH 8.0
TWEAK_FLOAT cfFR_SCRAMBLE_ELEMENT_HEIGHT 18.0
TWEAK_FLOAT cfFR_SCRAMBLE_DIFFBETWEENSEGMENT 0.01114
TWEAK_FLOAT cfFR_SCRAMBLE_Y_OFFSET 0.0

TWEAK_FLOAT cfFR_RESULTS_LOADING_POSITIONY 0.516
TWEAK_FLOAT cfFR_RESULTS_LOADING_INITIALX 0.279
TWEAK_FLOAT cfFR_RESULTS_LOADING_XDIFF 0.0245
TWEAK_FLOAT cfFR_RESULTS_LOADING_HEIGHT 37.0
TWEAK_FLOAT cfFR_RESULTS_LOADING_WIDTH 40.0

TWEAK_INT ciFR_RESULTS_LOADING_MAX_SEGMENTS 17

CONST_FLOAT cfFR_TIMER_WIDTH 30.0
CONST_FLOAT cfFR_TIMER_HEIGHT 42.0

CONST_INT ciFR_SCRAMBLE_MAX_SEGMENTS 41

HG_RGBA_COLOUR_STRUCT rgbaHighlighted

FLOAT fHelpTimer
FLOAT fAbortTimer_R

#IF IS_DEBUG_BUILD
BOOL bFullMatch
BOOL bFullRestart
#ENDIF

// DATA
FUNC BOOL fr_IS_FINGERPRINT_REQUIRED(INT iFingerPrint, INT i)
	SWITCH iFingerPrint
		CASE 0
			SWITCH i
				CASE 0
				CASE 3
				CASE 5
				CASE 6
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH i
				CASE 2
				CASE 4
				CASE 5
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH i
				CASE 2
				CASE 5
				CASE 6
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH i
				CASE 3
				CASE 4
				CASE 6
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH i
				CASE 1
				CASE 2
				CASE 5
				CASE 6
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			SWITCH i
				CASE 0
				CASE 3
				CASE 6
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH i
				CASE 0
				CASE 2
				CASE 4
				CASE 5
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH i
				CASE 3
				CASE 4
				CASE 6
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL FC_IS_PLAYER_ABLE_TO_INTERACT_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_PLAY
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_VISUAL_TEST
		//CDEBUG1LN(DEBUG_MINIGAME, "[MC][FingerPrintClone_RETRO] No interact - Wrong state")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
		//CDEBUG1LN(DEBUG_MINIGAME, "[MC][FingerPrintClone_RETRO] No interact - checking match")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
		//CDEBUG1LN(DEBUG_MINIGAME, "[MC][FingerPrintClone_RETRO] No interact - passing fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
		//CDEBUG1LN(DEBUG_MINIGAME, "[MC][FingerPrintClone_RETRO] No interact - failed fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
		//CDEBUG1LN(DEBUG_MINIGAME, "[MC][FingerPrintClone_RETRO] No interact - scrambling")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC GET_POSITION_FOR_fr_ELEMENT(INT iElement, FLOAT &fElementX, FLOAT &fElementY)
	
	fElementX = cfFR_FINGERPRINT_ELEMENT_COLUMN1
	// Even spacing
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW2 = (cfFR_FINGERPRINT_ELEMENT_ROW1 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW3 = (cfFR_FINGERPRINT_ELEMENT_ROW2 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW4 = (cfFR_FINGERPRINT_ELEMENT_ROW3 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW5 = (cfFR_FINGERPRINT_ELEMENT_ROW4 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW6 = (cfFR_FINGERPRINT_ELEMENT_ROW5 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW7 = (cfFR_FINGERPRINT_ELEMENT_ROW6 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	FLOAT cfFR_FINGERPRINT_ELEMENT_ROW8 = (cfFR_FINGERPRINT_ELEMENT_ROW7 + cfFR_FINGERPRINT_ELEMENT_SPACING)
	
	SWITCH iElement
		CASE 0 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW1
		BREAK
		CASE 1 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW2
		BREAK
		CASE 2 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW3
		BREAK
		CASE 3
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW4
		BREAK
		CASE 4 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW5
		BREAK
		CASE 5 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW6
		BREAK
		CASE 6 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW7
		BREAK
		CASE 7 
			fElementY = cfFR_FINGERPRINT_ELEMENT_ROW8
		BREAK
	ENDSWITCH
ENDPROC

// GAME PLAY FUNCTIONS
PROC SET_ELEMENT_SELECTED_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	
	INT i
	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning = iFingerPrint
			IF NOT IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
				IF sGameplayData.iSelectedElementCount >= ciFR_TARGET_NUMBER_OF_ELEMENTS
					PRINTLN("[MC][FingerPrintClone] Too many elements selected")
					EXIT
				ENDIF
				//PLAY_SOUND_FRONTEND(-1, "Select_Print_Tile", sGameplayData.sBaseStruct.sAudioSet)
				sGameplayData.iSelectedElementCount++
				SET_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
				PRINTLN("[MC][FingerPrintClone] Selecting element ", iFingerPrint)
				
				IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
					PLAY_SOUND_FRONTEND(-1, "Tile_select", sGameplayData.sBaseStruct.sAudioSet)
				ENDIF
			ELSE
				//PLAY_SOUND_FRONTEND(-1, "Deselect_Print_Tile", sGameplayData.sBaseStruct.sAudioSet)
				sGameplayData.iSelectedElementCount--
				PRINTLN("[MC][FingerPrintClone] Deselecting element ", iFingerPrint)
				CLEAR_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
				
				IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
					PLAY_SOUND_FRONTEND(-1, "Tile_select", sGameplayData.sBaseStruct.sAudioSet)
				ENDIF
			ENDIF
	
			PRINTLN("[MC][FingerPrintClone] Element Location ", sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning)
		ENDIF
	ENDFOR
ENDPROC

FUNC BOOL CHECK_FINGERPRINT_SELECTED_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	RETURN IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iFingerPrint].iBitset, ciFR_SELECTED)
ENDFUNC

FUNC BOOL CHECK_FINGERPRINT_REQUIRED_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	RETURN IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iFingerPrint].iBitset, ciFR_REQUIRED)
ENDFUNC

FUNC BOOL CHECK_FINGERPRINT_MATCHED_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	RETURN CHECK_FINGERPRINT_REQUIRED_R(sGameplayData, iFingerPrint) AND CHECK_FINGERPRINT_SELECTED_R(sGameplayData, iFingerPrint)
ENDFUNC

FUNC BOOL fr_IS_PLAYER_ABLE_TO_INTERACT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_PLAY
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_VISUAL_TEST
		//PRINTLN("[MC][FingerPrintClone] No interact - Wrong state")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
		PRINTLN("[MC][FingerPrintClone] No interact - checking match")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
		PRINTLN("[MC][FingerPrintClone] No interact - passing fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
		PRINTLN("[MC][FingerPrintClone] No interact - failed fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
		PRINTLN("[MC][FingerPrintClone] No interact - scrambling")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC START_ELEMENT_CHECK_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
	AND sGameplayData.iSelectedElementCount >= ciFR_TARGET_NUMBER_OF_ELEMENTS
		SET_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
		SET_BIT(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
		// DO OTHER STUFF HERE
		// Audio/Visual effect
	ENDIF
ENDPROC

PROC RETRO_FINGERPRINT_DEBUG_PRINT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)

	INT i
	FOR i = 0 TO ciFR_NUMBER_OF_PRINTS_INGAME - 1
		IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
			CDEBUG1LN(DEBUG_MINIGAME, "[BAZ]ADJUST_SELECTOR_R - INPUT_FRONTEND_UP - Numbers matching. iPositioning = ", 
				sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning ,
				" and sprite_Id: ", sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id,
				"and selected element", sGameplayData.iSelectedElement)
		ENDIF
	ENDFOR

ENDPROC

PROC ADJUST_SELECTOR_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, CONTROL_ACTION eControlAction)
	//INT iTempElement = sGameplayData.iSelectedElement
	PRINTLN("[MC][FingerPrintClone] Old Selector Position ", sGameplayData.iSelectedElement)
	//PRINTLN("[MC][FingerPrintClone] Action ", eControlAction)
	// Elements are set pressing up and down while in FULL MATCH version of the game.
	SWITCH eControlAction
		CASE INPUT_FRONTEND_UP
		PRINTLN("[MC][FingerPrintClone] Action Up")
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
				SET_ELEMENT_SELECTED_R(sGameplayData, sGameplayData.iSelectedElement)
			ENDIF
			sGameplayData.iSelectedElement -= ciFR_UI_ROW_LENGTH
			
			IF sGameplayData.iSelectedElement < 0
				IF sGameplayData.iSelectedElement = -1
					sGameplayData.iSelectedElement = ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS - 1
				ELSE
					sGameplayData.iSelectedElement = ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS - ciFR_UI_ROW_LENGTH
				ENDIF
			ENDIF
			
			sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
			
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)

				SET_ELEMENT_SELECTED_R(sGameplayData, sGameplayData.iSelectedElement)
				RETRO_FINGERPRINT_DEBUG_PRINT(sGameplayData)
			
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "Cursor_Move", sGameplayData.sBaseStruct.sAudioSet)
		BREAK
		CASE INPUT_FRONTEND_DOWN
		PRINTLN("[MC][FingerPrintClone] Action Down")
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
				SET_ELEMENT_SELECTED_R(sGameplayData, sGameplayData.iSelectedElement)
			ENDIF
			sGameplayData.iSelectedElement += ciFR_UI_ROW_LENGTH
			
			IF sGameplayData.iSelectedElement >= ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS
				IF sGameplayData.iSelectedElement = ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS
					sGameplayData.iSelectedElement = 0
				ELSE
					sGameplayData.iSelectedElement = 0 + (ciFR_UI_ROW_LENGTH/ciFR_UI_ROW_LENGTH)
				ENDIF
			ENDIF
			
			sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
				SET_ELEMENT_SELECTED_R(sGameplayData, sGameplayData.iSelectedElement)
				RETRO_FINGERPRINT_DEBUG_PRINT(sGameplayData)
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "Cursor_Move", sGameplayData.sBaseStruct.sAudioSet)
		BREAK
		CASE INPUT_FRONTEND_LEFT
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
				CDEBUG1LN(DEBUG_MINIGAME,"[BAZ][FingerPrintClone] Action Left")
				INT i
				
				FOR i=0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
					IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
						sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id -= 1
						
						IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id < 0
							sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id = (ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS - 1)
						ENDIF
					ENDIF
				ENDFOR
				
				RETRO_FINGERPRINT_DEBUG_PRINT(sGameplayData)
				PLAY_SOUND_FRONTEND(-1, "Tile_select", sGameplayData.sBaseStruct.sAudioSet)
			ENDIF
		BREAK
		CASE INPUT_FRONTEND_RIGHT
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
				CDEBUG1LN(DEBUG_MINIGAME,"[BAZ][FingerPrintClone] Action Right")
				INT i
				
				FOR i=0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
					IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
						sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id += 1

						IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id >= ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS
							sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id = 0
						ENDIF
					ENDIF
				ENDFOR
				
				RETRO_FINGERPRINT_DEBUG_PRINT(sGameplayData)
				PLAY_SOUND_FRONTEND(-1, "Tile_Select", sGameplayData.sBaseStruct.sAudioSet)
			ENDIF
		BREAK
	
	ENDSWITCH
	
	PRINTLN("[MC][FingerPrintClone] New Selector Position ", sGameplayData.iSelectedElement)
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_INPUT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	CONTROL_ACTION eLeftStick = HG_GET_LEFT_STICK_ACTION(sGameplayData.sBaseStruct)
	
	// Move around the selection
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR eLeftStick = INPUT_FRONTEND_UP
		ADJUST_SELECTOR_R(sGameplayData, INPUT_FRONTEND_UP)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR eLeftStick = INPUT_FRONTEND_DOWN
		ADJUST_SELECTOR_R(sGameplayData, INPUT_FRONTEND_DOWN)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	OR eLeftStick = INPUT_FRONTEND_LEFT
		ADJUST_SELECTOR_R(sGameplayData, INPUT_FRONTEND_LEFT)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR eLeftStick = INPUT_FRONTEND_RIGHT
		ADJUST_SELECTOR_R(sGameplayData, INPUT_FRONTEND_RIGHT)
	ENDIF
	
//	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
//			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdRetroTimer)
//			SET_BIT(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
//		ENDIF
//	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
	AND FC_IS_PLAYER_ABLE_TO_INTERACT_R(sGameplayData)
	
		INT i
		INT j = 0
		FOR i = 0 TO ciFR_NUMBER_OF_PRINTS_INGAME - 1
		
//			IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning = sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id
//				CDEBUG1LN(DEBUG_MINIGAME, "[BAZ]PROCESS_FINGERPRINT_RETRO_INPUT - Numbers matching. iPositioning = ", sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning ," and sprite_Id: ", sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id)
//			ENDIF
			
			// Loop here until all sprites line up
			IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning = sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iSprite_Id
				j++
				//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ]PROCESS_FINGERPRINT_RETRO_INPUT - Match count J:", j)
			ENDIF
			
			IF j >= ciFR_NUMBER_OF_PRINTS_INGAME
				//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ]PROCESS_FINGERPRINT_RETRO_INPUT - Starting element check")
				PLAY_SOUND_FRONTEND(-1, "Print_Appears", sGameplayData.sBaseStruct.sAudioSet)
				SET_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
				SET_BIT(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
			ENDIF
			
		ENDFOR
		
		EXIT
	ENDIF
	
	// Don't do anything below if we are in full match version.
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
		 sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
		 SET_ELEMENT_SELECTED_R(sGameplayData, sGameplayData.iSelectedElement)
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
		sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
		START_ELEMENT_CHECK_R(sGameplayData)
	ENDIF
ENDPROC

PROC RUN_SCRAMBLE_PROCESS_R_OLD(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	INT newPositions[ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS]
	INT i
	INT j
	INT iRandomValue
	
	BOOL bWipeSelection
	
	iRandomValue = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF iRandomValue < sGameplayData.sBaseStruct.iScrambleWipeChance 
		sGameplayData.iSelectedElementCount = 0
		bWipeSelection = TRUE
		
	ENDIF
	
	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		newPositions[i] = -1
	ENDFOR

	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		newPositions[i] = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(newPositions, ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS,0,ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS )
	ENDFOR
	
	FOR i=0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		IF newPositions[i] = -1
			PRINTLN("[MC][FingerPrintClone] Scrambling - Element ", i, " is invalid - manually filling")
		
			FOR j = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
				IF NOT ARRAY_CONTAINS_INT(newPositions, j, ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS)
					newPositions[i] = j
					PRINTLN("[MC][FingerPrintClone] Scrambling - Element ", i, " manually filled with ", j)
					BREAKLOOP
				ENDIF
			ENDFOR
		ENDIF
	ENDFOR
	
	// Apply positions
	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		PRINTLN("[MC][FingerPrintClone] Scrambling - Element ", i)
		PRINTLN("[MC][FingerPrintClone] Scrambling - Old Position ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning)
		PRINTLN("[MC][FingerPrintClone] Scrambling - New Position ", newPositions[i])
	
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning = newPositions[i]
		
		IF bWipeSelection
			CLEAR_BIT(sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset, ciFR_SELECTED)
		ENDIF
	ENDFOR
	
ENDPROC

PROC RUN_SCRAMBLE_PROCESS_R_FULL(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	INT iNewValues[ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS]
	INT i

	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		iNewValues[i] = -1
	ENDFOR

	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		iNewValues[i] = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(iNewValues, ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS,0,ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS )
	ENDFOR
	
	FOR i=0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iSprite_Id = iNewValues[i]
	ENDFOR
	
	// 

ENDPROC

PROC RUN_SCRAMBLE_PROCESS_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)

	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH) 
		RUN_SCRAMBLE_PROCESS_R_FULL(sGameplayData, iFingerPrint)
	ELSE
		RUN_SCRAMBLE_PROCESS_R_OLD(sGameplayData, iFingerPrint)
	ENDIF

ENDPROC

// url:bugstar:6800993 - [Script] Fingerprint Minigame - Could you tweak the difficulty to progressively get more difficult with each print
PROC SET_INITIAL_FINGERPRINT_SETUP(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)

	sGameplayData.sFingerPrintElements[0][4].iSprite_Id = 4
	sGameplayData.sFingerPrintElements[0][5].iSprite_Id = 5
	sGameplayData.sFingerPrintElements[0][6].iSprite_Id = 6
	sGameplayData.sFingerPrintElements[0][7].iSprite_Id = 7

	sGameplayData.sFingerPrintElements[1][5].iSprite_Id = 5
	sGameplayData.sFingerPrintElements[1][6].iSprite_Id = 6
	sGameplayData.sFingerPrintElements[1][7].iSprite_Id = 7
	
	sGameplayData.sFingerPrintElements[2][6].iSprite_Id = 6
	sGameplayData.sFingerPrintElements[2][7].iSprite_Id = 7

ENDPROC

PROC SCRAMBLE_TILE_LOCATIONS_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)

	RUN_SCRAMBLE_PROCESS_R(sGameplayData, sGameplayData.iCurrentFingerprintIndex)
	
//	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH) 
//		WHILE sGameplayData.iSelectedElement = sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][sGameplayData.iSelectedElement].iPositioning
//			IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][sGameplayData.iSelectedElement].iBitset, ciFR_SELECTED)
//
//				sGameplayData.iSelectedElement++
//				
//				IF sGameplayData.iSelectedElement >= ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS
//					sGameplayData.iSelectedElement = 0
//				ENDIF
//			ELSE
//				BREAKLOOP
//			ENDIF
//		ENDWHILE
//	ELSE
//		sGameplayData.iSelectedElement = 0
//	ENDIF
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH) 
		sGameplayData.iSelectedElement = 0
	ENDIF
	
	SET_BIT(sGameplayData.iF_Bs, ciFR_SCRAMBLED)
ENDPROC

PROC MAINTAIN_FINGERPRINT_RETRO_HELP(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
//	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HG_fr_HELP_04a")
//	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		EXIT
//	ENDIF
	
	// scramble
//	IF IS_BIT_SET(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
//		IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_TEAM_HAS_AGGRO)
////			IF NOT bShowFinalQuitWarning
////				PRINT_HELP_NO_SOUND("HG_fr_HELP_04a", ciHG_FIRST_ABORT_TIME)
////			ELSE
//			IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
//				PRINT_HELP_NO_SOUND("HG_fr_HELP_04") // Warning for aborting hack is not relevant in this version.
//			ENDIF
////			ENDIF
//		ENDIF
//			
//		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdRetroTimer, ciHG_ABORT_WARNING_TIME)
//			CLEAR_BIT(sGameplayData.iF_Bs, ciHG_ABORT_WARNING)
//		ENDIF
//
//		EXIT
//	ENDIF
	
	// scramble
//	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SHOWN_HELP)
//		PRINT_HELP_NO_SOUND("HG_fr_HELP_03")
//		EXIT
//	ENDIF
	
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SHOWN_HELP)
		fHelpTimer += (0+@1000)
		SWITCH (FLOOR(fHelpTimer / 12000))
			CASE 0
				PRINT_HELP_NO_SOUND("HG_fr_HELP_01")
			BREAK
			
			CASE 1
				PRINT_HELP_NO_SOUND("HG_fr_HELP_02")
			BREAK
			
			CASE 2
				PRINT_HELP_NO_SOUND("HG_fr_HELP_03")
			BREAK
			
			CASE 3
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					PRINT_HELP_NO_SOUND("HG_FR_HELP_04_PC")
				ELSE
					PRINT_HELP_NO_SOUND("HG_fr_HELP_04")
				ENDIF
			BREAK
			
			CASE 4
				SET_BIT(sGameplayData.iF_Bs, ciFR_SHOWN_HELP)
			BREAK
		ENDSWITCH
	ELSE
		fHelpTimer = 0
	ENDIF
	
	// check
//	IF sGameplayData.iSelectedElementCount >= ciFR_TARGET_NUMBER_OF_ELEMENTS
//	AND fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
//		PRINT_HELP_NO_SOUND("HG_fr_HELP_02")
//		EXIT
//	ENDIF
	
	// Select
//	IF fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
//		PRINT_HELP_NO_SOUND("HG_fr_HELP_01")
//	ENDIF
ENDPROC

PROC MAINTAIN_FINGERPRINT_RETRO_CONTEXT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
//		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//			sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
//			PRINTLN("[MC][FingerprintClone] Drawing cancel prompt")
//		ENDIF
		
		IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
				PRINTLN("[MC][FingerprintClone] Drawing cancel prompt")
			ENDIF
		ELSE //IS_USING_KEYBOARD_AND_MOUSE
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
			OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
				sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
				PRINTLN("[MC][FingerprintClone] Drawing cancel prompt PC")
			ENDIF
		ENDIF
	
		SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		
		IF sGameplayData.sBaseStruct.bUpdatePrompts
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			
			// Exit
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					fAbortTimer_R += (0+@1000) // Add to the timer this frame
					DRAW_GENERIC_METER(ROUND(fAbortTimer_R), ci_HG_QUIT_TIME, "HG_INT_04a")
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
				ELSE
					fAbortTimer_R = 0.0
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
				ENDIF

			ELSE //IS_USING_KEYBOARD_AND_MOUSE
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE)
					fAbortTimer_R += (0+@1000) // Add to the timer this frame
					DRAW_GENERIC_METER(ROUND(fAbortTimer_R), ci_HG_QUIT_TIME, "HG_INT_04a")
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
				ELSE
					fAbortTimer_R = 0.0

					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_DELETE, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
				ENDIF
			ENDIF
			
			// Exit
//			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//				fAbortTimer_R += (0+@1000) // Add to the timer this frame
//				DRAW_GENERIC_METER(ROUND(fAbortTimer_R), ci_HG_QUIT_TIME, "HG_INT_04a")
//				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
//			ELSE
//				fAbortTimer_R = 0.0
//
//				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
//			ENDIF
			// Select
//			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HG_INT_01", sGameplayData.sBaseStruct.scaleformInstructionalButtons)
//			
//			IF sGameplayData.iSelectedElementCount >= ciFR_TARGET_NUMBER_OF_ELEMENTS
//			// Check
//			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "HG_INT_02", sGameplayData.sBaseStruct.scaleformInstructionalButtons)
//			ENDIF
			
			// Change Selector
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_ALL), "HG_INT_03", sGameplayData.sBaseStruct.scaleformInstructionalButtons)			
			
			sGameplayData.sBaseStruct.bUpdatePrompts = FALSE
		ENDIF
		
		//SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameplayData.sBaseStruct.scaleformInstructionalButtonsIndex, thisSpritePlacement, sGameplayData.sBaseStruct.scaleformInstructionalButtons)
	ELSE
		RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sGameplayData.sBaseStruct.scaleformInstructionalButtons)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sGameplayData.sBaseStruct.scaleformInstructionalButtonsIndex)
	ENDIF
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_GAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)

#IF IS_DEBUG_BUILD
	IF bFullMatch
		IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
			//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] PROCESS_FINGERPRINT_RETRO_GAME - ciFR_FULL_MATCH - Set FULL MATCH version")
			SET_BIT(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		ENDIF
	ELSE
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		ENDIF
	ENDIF
	
	// Reinitialise the whole game
	IF bFullRestart
		 UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_INIT)
		 bFullRestart = FALSE // Clear the bool
	ENDIF
#ENDIF

	PROCESS_COMMON_HACKING_EVERY_FRAME_MISC(sGameplayData.sBaseStruct, sControllerStruct)
	
	// TODO: Disabling for debug purposes.
//	IF IS_PC_VERSION()
//		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
//	ENDIF
//	DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
		AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
		AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
		// Do the check procedure here
			INT iCurrentlyMatched = 0
			INT i
			PRINTLN("[MC][FingerPrintClone] Fingerprint Check Started")
			FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
				IF CHECK_FINGERPRINT_SELECTED_R(sGameplayData, i)
					PRINTLN("[MC][FingerPrintClone] Fingerprint Selected: ", i)
				ENDIF
			
				IF CHECK_FINGERPRINT_MATCHED_R(sGameplayData, i)
					iCurrentlyMatched += 1
					PRINTLN("[MC][FingerPrintClone] Fingerprint Matched: ", i)
				ENDIF
			ENDFOR
			
			IF iCurrentlyMatched >= ciFR_TARGET_NUMBER_OF_ELEMENTS
				//ASSERTLN("[BAZ][FingerPrintClone_Retro] Print_Match")
				SET_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
//				STOP_SOUND(sGameplayData.iProcessingSoundID)
//				PLAY_SOUND_FRONTEND(-1, "Print_Match", sGameplayData.sBaseStruct.sAudioSet)
			ELSE
				//ASSERTLN("[BAZ][FingerPrintClone_Retro] Print_Not_Match")
				SET_BIT(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT )
//				STOP_SOUND(sGameplayData.iProcessingSoundID)
//				PLAY_SOUND_FRONTEND(-1, "Print_Not_Match", sGameplayData.sBaseStruct.sAudioSet)
				sGameplayData.iNumberOfLastMatchedElements = iCurrentlyMatched
			ENDIF
			
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		ENDIF
	ELSE // FULL MATCH version
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
		AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
			SET_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
	AND IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
		IF HAS_SOUND_FINISHED(sGameplayData.iProcessingSoundID)
			PLAY_SOUND_FRONTEND(sGameplayData.iProcessingSoundID, "Processing", sGameplayData.sBaseStruct.sAudioSet)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFR_PRE_CHECK_TIME)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
			STOP_SOUND(sGameplayData.iProcessingSoundID)
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
				//ASSERTLN("[BAZ] Print_Match playing")
				PLAY_SOUND_FRONTEND(-1, "Print_Match", sGameplayData.sBaseStruct.sAudioSet)
			ELSE
				//ASSERTLN("[BAZ] Print_Not_Match playing")
				PLAY_SOUND_FRONTEND(-1, "Print_Not_Match", sGameplayData.sBaseStruct.sAudioSet)
			ENDIF
		ENDIF
	ENDIF
	
	// Help Text
	MAINTAIN_FINGERPRINT_RETRO_HELP(sGameplayData)
	
	IF NOT IS_BIT_SET(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
		STOP_SOUND(-1)
		PLAY_SOUND_FRONTEND(-1, "Pattern_Shift", sGameplayData.sBaseStruct.sAudioSet)
		SET_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
	ENDIF
	

//	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH) 
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdScrambleTimer, sGameplayData.sBaseStruct.iScrambleTime)
		AND fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
			SET_BIT(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_SCRAMBLED)
			PRINTLN("[MC][FingerPrintClone] Scrambling")
			PLAY_SOUND_FRONTEND(-1, "Pattern_Scramble", sGameplayData.sBaseStruct.sAudioSet)
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
			IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLED)
				SCRAMBLE_TILE_LOCATIONS_R(sGameplayData)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFR_CHECKING_TIME)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
				PRINTLN("[MC][FingerPrintClone] Finishing Scrambling")
			ENDIF
		ENDIF
//	ENDIF
	
	// RESET ANIMATION
	IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFR_CHECKING_TIME)
	AND NOT fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
	PRINTLN("[MC][FingerPrintClone] Timer Expired")
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			PRINTLN("[MC][FingerPrintClone] Checking Match")
			IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
				STOP_SOUND(-1)
				PLAY_SOUND_FRONTEND(-1, "Window_Clear", sGameplayData.sBaseStruct.sAudioSet)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
				sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
				sGameplayData.sBaseStruct.iCurrentLives--
				sGameplayData.iSelectedElement = 0
				IF sGameplayData.sBaseStruct.iCurrentLives <= 0
					PRINTLN("[MC][FingerPrintClone] Out of Lives")
					UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_FAIL)
				ELSE
					IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
						SET_BIT(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
					ENDIF
				ENDIF
				PRINTLN("[MC][FingerPrintClone] Finishing Failed")
			ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
				
				CLEAR_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
				CLEAR_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
				
				// Make sure first item is selected
				IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
					// Deselect all elements beforehand
					INT i
					FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
						IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
							sGameplayData.iSelectedElementCount--
							CLEAR_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
						ENDIF
					ENDFOR
				ENDIF
				
				sGameplayData.iCurrentFingerprintIndex++
				sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
				sGameplayData.iSelectedElement = 0
				sGameplayData.iSelectedElementCount = 0
				
				IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
					INT i
					FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
						IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning = 0 // Auto select first
							IF NOT IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
								sGameplayData.iSelectedElementCount++
								SET_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
				
				PRINTLN("[MC][FingerPrintClone] Finishing Passed")
				IF sGameplayData.iCurrentFingerprintIndex >= sGameplayData.sBaseStruct.iNumberOfStages
					PRINTLN("[MC][FingerPrintClone] Passing Game")
					sGameplayData.iCurrentFingerprintIndex--
					UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PASS)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "Window_Clear", sGameplayData.sBaseStruct.sAudioSet)
					
					IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
						SET_BIT(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
					ENDIF
					
					PRINTLN("[MC][FingerPrintClone] New Fingerprint is ", sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex])
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DRAW_FINGERPRINT_ELEMENT_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, TEXT_LABEL_63 tFingerPrint,INT iElement, TEXT_LABEL_31 tlDictionary)
	//PRINTLN("[MC][FingerprintClone] Drawing element ", iElement)
	// draw basic fingerprint
	FLOAT elementX
	FLOAT elementY
	
	GET_POSITION_FOR_fr_ELEMENT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iPositioning, elementX, elementY)
	
	INT j = (sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex] + 1)
	
	// Take off one if we are using the full match version.
//	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
//		j -= 1
//	ENDIF
	
	tFingerPrint += j
	
	//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] DRAW_FINGERPRINT_ELEMENT_R: ", tFingerPrint)
	
	TEXT_LABEL_63 tlElementSprite = tFingerPrint
	tlElementSprite += "_SLICE_0"
//	j = (sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iSprite_Id + 1)
//	tlElementSprite += j
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
		IF sGameplayData.sBaseStruct.iDrawCounter <= 0
			sGameplayData.iScrambledValues[iElement] = 0
		ELIF sGameplayData.sBaseStruct.iDrawCounter+iElement = 10
			sGameplayData.iScrambledValues[iElement] = GET_RANDOM_INT_IN_RANGE(1, ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS)
		ENDIF
		IF sGameplayData.iScrambledValues[iElement] = 0
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro", "comp_blank", elementX, elementY, cfFR_FINGERPRINT_ELEMENT_WIDTH, cfFR_FINGERPRINT_ELEMENT_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA) 
		ELSE
			tlElementSprite += sGameplayData.iScrambledValues[iElement]
			//PRINTLN("[MC][FingerprintClone] Drawing scramble sprite ", tlElementSprite)
			DRAW_HACKING_SPRITE_CORRECTED(tlDictionary, tlElementSprite, elementX, elementY, cfFR_FINGERPRINT_ELEMENT_WIDTH, cfFR_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA) 
		ENDIF
	ELSE
		j = (sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iSprite_Id + 1)
		
		tlElementSprite += j
		//tlElementSprite += sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iSprite_Id + 1
		//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FingerprintClone] Drawing sprite: ", tlElementSprite)
		
		IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iBitset, ciFR_SELECTED)
			// Draw selected
			DRAW_HACKING_SPRITE_CORRECTED(tlDictionary, tlElementSprite, elementX, elementY, cfFR_FINGERPRINT_ELEMENT_WIDTH, cfFR_FINGERPRINT_ELEMENT_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA) 
		ELSE
			DRAW_HACKING_SPRITE_CORRECTED(tlDictionary, tlElementSprite, elementX, elementY, cfFR_FINGERPRINT_ELEMENT_WIDTH, cfFR_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA) 
		ENDIF
		
		// Solution Debug - Uncomment to show the solution pieces in red highlight
//		#IF IS_DEBUG_BUILD
//			IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iBitset, ciFR_REQUIRED)
//				DRAW_HACKING_SPRITE_CORRECTED(tlDictionary, tlElementSprite, elementX, elementY, cfFR_FINGERPRINT_ELEMENT_WIDTH, cfFR_FINGERPRINT_ELEMENT_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR + 100, sGameplayData.rgbaTintColour.iG - 100, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
//			ENDIF
//		#ENDIF
			
		IF sGameplayData.iSelectedElement = sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iPositioning
			// Draw selector
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro", "selectorFrame", elementX, elementY, cfFR_FINGERPRINT_ELEMENT_SELECTOR_WIDTH, cfFR_FINGERPRINT_ELEMENT_SELECTOR_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA) 
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF sGameplayData.sBaseStruct.bDebugShowGuide
				IF IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][iElement].iBitset, ciFR_REQUIRED)
					DRAW_HACKING_SPRITE_CORRECTED("mphackinggame", "Comp_Blank", elementX, elementY, cfFR_FINGERPRINT_ELEMENT_WIDTH / 4, cfFR_FINGERPRINT_ELEMENT_HEIGHT / 4, 0.0, sGameplayData.rgbaTintColour.iR, 0, 0, sGameplayData.rgbaTintColour.iA / 4)
				ENDIF
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC DRAW_LARGE_FINGERPRINT_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, TEXT_LABEL_63 tFingerPrint, TEXT_LABEL_31 tlDictionary)
	TEXT_LABEL_63 tlElementSprite 
	
	//PRINTLN("[MC][FingerprintClone] Drawing Dictionary ", tlDictionary)

	tlElementSprite = tFingerPrint
	INT i = (sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex]+1)
	
	// Take off one if we are using the full match version.
//	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
//		i -= 1
//	ENDIF
	
	tlElementSprite += i
		
	//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FingerprintClone_Retro] Drawing big sprite ", tlElementSprite)
		
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)

		DRAW_HACKING_SPRITE_CORRECTED(tlDictionary, tlElementSprite, cfFR_FINGERPRINT_LARGE_POSITIONX, cfFR_FINGERPRINT_LARGE_POSITIONY, cfFR_FINGERPRINT_LARGE_WIDTH, cfFR_FINGERPRINT_LARGE_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
	ELSE
		DRAW_HACKING_SPRITE_CORRECTED(tlDictionary, tlElementSprite, cfFR_FINGERPRINT_LARGE_POSITIONX, cfFR_FINGERPRINT_LARGE_POSITIONY, cfFR_FINGERPRINT_LARGE_WIDTH, cfFR_FINGERPRINT_LARGE_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	ENDIF
ENDPROC

FUNC FLOAT GET_TRACKER_POSITION_R(INT iElement)
	FLOAT position
	
	SWITCH iElement
		CASE 1
			position = cfFR_FINGERPRINT_TRACKER1_POSITIONX
		BREAK
		CASE 2
			position = cfFR_FINGERPRINT_TRACKER2_POSITIONX
		BREAK
		CASE 3
			position = cfFR_FINGERPRINT_TRACKER3_POSITIONX
		BREAK
		CASE 4
			position = cfFR_FINGERPRINT_TRACKER4_POSITIONX
		BREAK
	ENDSWITCH
	  
	RETURN position
ENDFUNC
	
PROC DRAW_FINGERPRINT_TRACKER_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iElement)
	TEXT_LABEL_63 tlElementSprite = "FINGERPRINT_HACKING_MINIGAME_FINGERPRINTS_0"
	TEXT_LABEL_31 tlElementDict = "MPFClone_Retro_Print"
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		tlElementDict = "MPFClone_Retro_PrintFull"
		tlElementSprite += (iElement+1)
		tlElementDict += (iElement+1)
	ELSE // Original gameplay
		tlElementSprite += (iElement+1)
		tlElementDict += (iElement)
	ENDIF
	
	FLOAT positionX = GET_TRACKER_POSITION_R(iElement)
	
	IF iElement <= sGameplayData.sBaseStruct.iNumberOfStages
		IF iElement = sGameplayData.iCurrentFingerprintIndex+1
			DRAW_HACKING_SPRITE_CORRECTED(tlElementDict, tlElementSprite, positionX, cfFR_FINGERPRINT_TRACKER_POSITIONY, cfFR_FINGERPRINT_TRACKER_WIDTH, cfFR_FINGERPRINT_TRACKER_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro", "Decyphered_Selector", positionX, cfFR_FINGERPRINT_TRACKER_POSITIONY, cfFR_FINGERPRINT_TRACKER_SELECTOR_WIDTH, cfFR_FINGERPRINT_TRACKER_SELECTOR_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
		ELSE
			DRAW_HACKING_SPRITE_CORRECTED(tlElementDict, tlElementSprite, positionX, cfFR_FINGERPRINT_TRACKER_POSITIONY, cfFR_FINGERPRINT_TRACKER_WIDTH, cfFR_FINGERPRINT_TRACKER_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro", "Decyphered_Selector", positionX, cfFR_FINGERPRINT_TRACKER_POSITIONY, cfFR_FINGERPRINT_TRACKER_SELECTOR_WIDTH, cfFR_FINGERPRINT_TRACKER_SELECTOR_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
		ENDIF
//	ELSE
//		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro", "Disabled_Signal", positionX, cfFR_FINGERPRINT_TRACKER_POSITIONY, cfFR_FINGERPRINT_TRACKER_WIDTH, cfFR_FINGERPRINT_TRACKER_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA/2)
	ENDIF
ENDPROC


PROC FILL_FINGERPRINT_STRUCT_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, INT iFingerPrint)
	INT i
	
	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning = i
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iSprite_Id = i
		sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset = 0
		
		IF fr_IS_FINGERPRINT_REQUIRED(sGameplayData.iFingerPrintIds[iFingerPrint], i)
			SET_BIT(sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset, ciFR_REQUIRED)
		ENDIF
		
		PRINTLN("[MC][FingerprintClone][SETUP] Fingerprint Element ", i)
		PRINTLN("[MC][FingerprintClone][SETUP] Element Position ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iPositioning)
		PRINTLN("[MC][FingerprintClone][SETUP] Element Sprite ID ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iSprite_Id)
		PRINTLN("[MC][FingerprintClone][SETUP] Element Bitset ", sGameplayData.sFingerPrintElements[iFingerPrint][i].iBitset)
		PRINTLN("[MC][FingerprintClone][SETUP] Element End")
	ENDFOR
	
	
ENDPROC

// Request to draw a darkened rect behind the SUCCESS image
// url:bugstar:6748049 - [Script] Fingerprint Minigame - Darken the elements while Processing message is on the screen
PROC DRAW_FINGERPRINT_R_RECT_SCREEN_DARKEN()

	HG_RGBA_COLOUR_STRUCT rgbaDarkened
	
	// Darkened rect 
	rgbaDarkened.iR = 0
	rgbaDarkened.iG = 0
	rgbaDarkened.iB = 0
	rgbaDarkened.iA = 215
	
	DRAW_RECT(0.5, 0.5, 1.0, 1.0, rgbaDarkened.iR, rgbaDarkened.iG, rgbaDarkened.iB, rgbaDarkened.iA)
ENDPROC

PROC SETUP_FINGERPRINTS_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
UNUSED_PARAMETER(sControllerStruct)
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FINGERPRINTS_SELECTED)
		EXIT
	ENDIF
	
	// Set the finger print IDs
	INT i
	
	FOR i = 0 TO ciFR_NUMBER_OF_PRINTS_INGAME - 1
		sGameplayData.iFingerPrintIds[i] = -1
	ENDFOR
	
	FOR i = 0 TO ciFR_NUMBER_OF_PRINTS_INGAME - 1
		//PRINTLN("[MC][FingerprintClone][SETUP] Setting up Fingerprint ", i)
		
		sGameplayData.iFingerPrintIds[i] = GET_RANDOM_INT_IN_RANGE_NOT_IN_ARRAY(sGameplayData.iFingerPrintIds, ciFR_NUMBER_OF_PRINTS_INGAME, 0, ciFR_FINGEPRINT_VARAIATIONS)
		
		// Fill in the data
		FILL_FINGERPRINT_STRUCT_R(sGameplayData, i)
		RUN_SCRAMBLE_PROCESS_R(sGameplayData, i)
		SET_INITIAL_FINGERPRINT_SETUP(sGameplayData)
		//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FingerprintClone][SETUP] Finishing setup Fingerprint ", i)
		//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FingerprintClone][SETUP] ID is ", sGameplayData.iFingerPrintIds[i])
	ENDFOR
	
	SET_BIT(sGameplayData.iF_Bs, ciFR_FINGERPRINTS_SELECTED)
ENDPROC

PROC DRAW_FINGERPRINT_RETRO_GAME(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)

	// show normal game
	TEXT_LABEL_31 tlFingerprintDictionary = "MPFClone_Retro_Print"
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)	
		tlFingerprintDictionary = "MPFClone_Retro_PrintFull"
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_K, KEYBOARD_MODIFIER_NONE, "")
		
		sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex]++
		
		IF sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex] >= 8
			sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex] = 0
		ENDIF
		
		RUN_SCRAMBLE_PROCESS_R(sGameplayData, sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex])
	ENDIF
#ENDIF
	
	INT i = sGameplayData.iFingerPrintIds[sGameplayData.iCurrentFingerprintIndex]
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)	
		i += 1
	ENDIF
	
	tlFingerprintDictionary += i
	
	TEXT_LABEL_63 tlFingerPrint = "FINGERPRINT_HACKING_MINIGAME_FINGERPRINTS_0"

	// Large FingerPrint
	DRAW_LARGE_FINGERPRINT_R(sGameplayData, tlFingerPrint, tlFingerprintDictionary )
	
	i = 0 // Reset iterator
	FOR i=0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		DRAW_FINGERPRINT_ELEMENT_R(sGameplayData, tlFingerPrint, i, tlFingerprintDictionary )
	ENDFOR
		
	// DRAW TRACKER
	FOR i = 1 TO ciFR_NUMBER_OF_PRINTS_INGAME
		DRAW_FINGERPRINT_TRACKER_R(sGameplayData, i)
	ENDFOR

	// Warning Messages
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH )
		DRAW_FINGERPRINT_R_RECT_SCREEN_DARKEN() // url:bugstar:6748049
		// show common
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
		// show checking in progress
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_LOADING", "FINGERPRINT_HACKING_MINIGAME_LOADING_WINDOW", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_LOADING_WIDTH + 32, cfHG_STATUS_MESSAGE_LOADING_HEIGHT - 8, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
			
			// DRAW LOADING BAR
			int progress = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdAnimationTimer)
			
			IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
				progress = ciFR_PRE_CHECK_TIME
			ENDIF
			
			FLOAT percentageOfSegments = TO_FLOAT(progress) / TO_FLOAT(ciFR_PRE_CHECK_TIME)
			percentageOfSegments = percentageOfSegments * 100
			//PRINTLN("[HACKINGGAME] Percentage of segments is ", percentageOfSegments)
			
			FLOAT numberOfSegments = (TO_FLOAT(ciFR_RESULTS_LOADING_MAX_SEGMENTS) / 100) * percentageOfSegments
			
			//PRINTLN("[HACKINGGAME] Number of segments is ", numberOfSegments)
			FLOAT posX
			FOR i = 1 TO FLOOR(numberOfSegments)
				posX = cfFR_RESULTS_LOADING_INITIALX + cfFR_RESULTS_LOADING_XDIFF*i
				DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_LOADING", "FINGERPRINT_HACKING_MINIGAME_LOADING_BAR_SEGMENT", posX, cfFR_RESULTS_LOADING_POSITIONY, cfFR_RESULTS_LOADING_WIDTH, cfFR_RESULTS_LOADING_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
			ENDFOR
		
		ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT )
			// Show Failure w/ number
			DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iResultCounter, ciHG_RESULT_FRAME_COUNT, HG_ANIMATION_SLOW, "MPFClone_Retro_MESSAGES", "FINGERPRINT_HACKING_MINIGAME_MESSAGES_INCORRECT_0", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_WIDTH, cfHG_STATUS_MESSAGE_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
		ELIF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT )
			// Show Passing Fingerprint
			DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iResultCounter, ciHG_RESULT_FRAME_COUNT, HG_ANIMATION_SLOW,"MPFClone_Retro_CORRECT", "FINGERPRINT_HACKING_MINIGAME_MESSAGES_CORRECT_0", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_WIDTH, cfHG_STATUS_MESSAGE_HEIGHT, 0.0, rgbaHighlighted.iR, rgbaHighlighted.iG, rgbaHighlighted.iB, rgbaHighlighted.iA)
		ENDIF
		
	ENDIF
	
	
ENDPROC
// END FUNCTION

FUNC TEXT_LABEL_23 GET_DICTIONARY_FOR_FINGERPRINT_ID_R(INT i)
	TEXT_LABEL_23 t123
	
	t123 = "mpfclone_Retro_print"
	t123 += i
	
	return t123
ENDFUNC

FUNC TEXT_LABEL_31 GET_DICTIONARY_FOR_FINGERPRINT_ID_R_FULL(INT i)
	TEXT_LABEL_31 t131
	
	t131 = "MPFClone_Retro_PrintFull"
	t131 += (i+1)
	
	return t131
ENDFUNC

FUNC BOOL LOAD_FINGERPRINTS_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
UNUSED_PARAMETER(sControllerStruct)
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FINGERPRINTS_SELECTED)
		// Complain loudly to the game's manager
		RETURN FALSE
	ENDIF
	INT i
	TEXT_LABEL_31 tl31 // Enlarged for longer strings
	
	FOR i = 0 TO ciFR_NUMBER_OF_PRINTS_INGAME - 1
		
		// Two variations of the fingerprint gameplay.
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
			tl31 = GET_DICTIONARY_FOR_FINGERPRINT_ID_R_FULL(sGameplayData.iFingerPrintIds[i])
		ELSE
			tl31 = GET_DICTIONARY_FOR_FINGERPRINT_ID_R(sGameplayData.iFingerPrintIds[i])
		ENDIF
		CDEBUG1LN(DEBUG_MINIGAME, "[MC][BAZ][FingerprintClone_retro] Check if Loaded: ", tl31)
		// Load
		REQUEST_STREAMED_TEXTURE_DICT(tl31)
		IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl31)
			CDEBUG1LN(DEBUG_MINIGAME, "[MC][BAZ][FingerprintClone_retro] Not Loaded ", tl31)
			RETURN FALSE
		ENDIF
	
	ENDFOR

	RETURN TRUE
ENDFUNC

PROC fr_RESET_GAMEPLAY_DATA(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	sGameplayData.iF_Bs = 0
	sGameplayData.sBaseStruct.iCurrentLives = ciFR_MAX_LIVES
	sGameplayData.iCurrentFingerprintIndex = 0
	
	sGameplayData.rgbaTintColour.iR = 41
	sGameplayData.rgbaTintColour.iG = 194
	sGameplayData.rgbaTintColour.iB = 82
	sGameplayData.rgbaTintColour.iA = 255
	
	rgbaHighlighted.iR = 255
	rgbaHighlighted.iB = 255
	rgbaHighlighted.iG = 255
	rgbaHighlighted.iA = 255
	
	INT i
	
	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.iScrambledValues[i] = 1
	ENDFOR
	
	FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
		sGameplayData.iCheckingAlphaValue[i] = 255
	ENDFOR
	
	sGameplayData.sBaseStruct.iMaxTime = ciFR_HACK_TIME
	sGameplayData.sBaseStruct.iScrambleTime = ciFR_SCRAMBLE_TIME
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		sGameplayData.sBaseStruct.iMaxTime = ciFR_HACK_TIME_FULL
		sGameplayData.sBaseStruct.iScrambleTime = ciFR_SCRAMBLE_TIME_FULL
	ENDIF
	
	sGameplayData.sBaseStruct.iScrambleWipeChance = 0
	
	sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
	
	#IF IS_DEBUG_BUILD
		sGameplayData.sBaseStruct.fDebugTimeToSet = TO_FLOAT(ciFR_HACK_TIME/60000)
		sGameplayData.sBaseStruct.iDebugLivesToSet = ciFR_MAX_LIVES
		sGameplayData.sBaseStruct.fDebugScrambleTimeToSet = TO_FLOAT(ciFR_SCRAMBLE_TIME/1000)
		sGameplayData.sBaseStruct.iScrambleWipeChanceToSet = 0
	#ENDIF
ENDPROC

PROC SETUP_RETRO_GAME_TIMER_DIGIT(TEXT_LABEL_63 &tTextElement, INT iDigt)
	tTextElement = "FINGERPRINT_HACKING_MINIGAME_NUMBERS_0"
	
	IF iDigt > 9
		tTextElement += 9
	ELIF iDigt < 0
		tTextElement += 0
	ELSE
		tTextElement += iDigt
	ENDIF
	
ENDPROC

PROC DRAW_RETRO_GAME_TIMER(HACKING_GAME_STRUCT &sGameStruct, int iTotalTime, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct, FLOAT fDigitDifference, BOOL bUpdate = TRUE)

	IF HAS_NET_TIMER_STARTED(sGameStruct.tdRetroTimer)
		INT iTimeLeft = iTotalTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameStruct.tdRetroTimer)

		IF sGameStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		OR NOT bUpdate
			iTimeLeft = iTotalTime
		ENDIF

		IF iTimeLeft < 0
			PRINTLN("[HACKINGGAME] Game Timer less than 0")
		ENDIF
		
		INT iMinutes        = (iTimeLeft/1000)/60
    	INT iSeconds        = (iTimeLeft-(iMinutes*60*1000))/1000
    	INT iMiliseconds    =  iTimeLeft-((iSeconds+(iMinutes*60))*1000)
		
		TEXT_LABEL_63 tElementName


		// Draw the individual digits using the central position as a starting point
		// Minutes
		IF iMinutes <= 0
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", "FINGERPRINT_HACKING_MINIGAME_NUMBERS_00", fPositionX - (fDigitDifference*2), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", "FINGERPRINT_HACKING_MINIGAME_NUMBERS_00", fPositionX - fDigitDifference, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		ELSE
			//first Digit
			
			SETUP_RETRO_GAME_TIMER_DIGIT(tElementName, iMinutes % 10 )
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", tElementName, fPositionX - (fDigitDifference), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
			
			iMinutes /= 10
			SETUP_RETRO_GAME_TIMER_DIGIT(tElementName, iMinutes)

			// second Digit
			DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", tElementName, fPositionX - (fDigitDifference*2), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		ENDIF

		// Colon
		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", "FINGERPRINT_HACKING_MINIGAME_NUMBERS_COLON", fPositionX, fPositionY, 32.0, 32.0, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )

		// Seconds
		//first Digit
		SETUP_RETRO_GAME_TIMER_DIGIT(tElementName, iSeconds % 10)
		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", tElementName, fPositionX + (fDigitDifference * 2), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		
		iSeconds /= 10
		
		SETUP_RETRO_GAME_TIMER_DIGIT(tElementName, iSeconds)

		// second Digit
		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", tElementName, fPositionX + fDigitDifference, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
	
		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", "FINGERPRINT_HACKING_MINIGAME_NUMBERS_COLON", fPositionX + (fDigitDifference * 3), fPositionY, 32.0, 32.0, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		
		// Miliseconds
		
		SETUP_RETRO_GAME_TIMER_DIGIT(tElementName, iMiliseconds % 10)

		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", tElementName, fPositionX + (fDigitDifference * 4), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
		
		iMiliseconds /= 10
		
		iMiliseconds = iMiliseconds % 10
		
		SETUP_RETRO_GAME_TIMER_DIGIT(tElementName, iMiliseconds)
		// second Digit
		DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_NUMBERS", tElementName, fPositionX + (fDigitDifference * 5), fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )

	ENDIF
ENDPROC

PROC DRAW_RETRO_GAME_LIVES(HACKING_GAME_STRUCT &sGameStruct, BOOL IsVertical, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct, FLOAT fOffset )
	INT i
	
	FLOAT fCustomPositionX
	FLOAT fCustomPositionY
	
	IF (sGameStruct.iMaxLives - sGameStruct.iCurrentLives) > 0
		FOR i = 1 TO (sGameStruct.iMaxLives - sGameStruct.iCurrentLives)
		  IF IsVertical
		  	fCustomPositionX = fPositionX
			fCustomPositionY = fPositionY + (fOffset * i)
		  ELSE
		  	fCustomPositionX = fPositionX + (fOffset * i)
			fCustomPositionY = fPositionY
		  ENDIF
		  
		  DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_LIFE", "FINGERPRINT_HACKING_MINIGAME_LIFE", fCustomPositionX, fCustomPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA / 2 )
		ENDFOR
	ENDIF
ENDPROC

PROC DRAW_HACKING_GAME_SCRAMBLER_R(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, int iTotalTime, FLOAT fPositionX, FLOAT fPositionY, FLOAT fWidth, FLOAT fHeight, HG_RGBA_COLOUR_STRUCT sColourStruct, FLOAT fLeftMost, FLOAT fDiff, FlOAT fElementWidth, FLOAT fElementHeight, BOOL bShouldAnimate, FLOAT fElementOffsetY, BOOL bIsScrambling)
	
	// Don't run the scrambler if we aren't in gameplay
	IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_PLAY
		EXIT
	ENDIF
	
	INT timeLeft
	
	IF (HAS_NET_TIMER_STARTED(sGameplayData.sBaseStruct.tdScrambleTimer)
	AND sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY)
	AND bShouldAnimate
	//AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH) // Dont's count down unless playing the full version.
		timeLeft = iTotalTime - GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdScrambleTimer)
		PRINTLN("[HACKINGGAME] Scramble Time left is ", timeLeft)
		
		IF timeLeft < 0
			PRINTLN("[HACKINGGAME] Scramble Timer less than 0")
		ENDIF
	ELSE
		timeLeft = iTotalTime
	ENDIF
	
	DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_SCRAMBLER", "FINGERPRINT_HACKING_MINIGAME_SCRAMBLER_BACKGROUND", fPositionX, fPositionY, fWidth, fHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA / 2)
	
	// Draw however many segements are needed
	FLOAT percentageOfSegments = TO_FLOAT(timeLeft) / TO_FLOAT(iTotalTime)
	percentageOfSegments = percentageOfSegments * 100
	PRINTLN("[HACKINGGAME] [DRAW_SCRAMBLE] Percentage of segments is ", percentageOfSegments)
	
	FLOAT numberOfSegments = (TO_FLOAT(ciFR_SCRAMBLE_MAX_SEGMENTS) / 100)
	numberOfSegments = numberOfSegments * percentageOfSegments
	
	sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount = FLOOR(numberOfSegments)
	
	IF sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount != sGameplayData.sBaseStruct.iPreviousScrambleSegmentCount
	AND NOT bIsScrambling
		//PLAY_SCRAMBLER_AUDIO(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount, ciFR_SCRAMBLE_MAX_SEGMENTS)
		// Determine sfx playback
		IF sGameplayData.sBaseStruct.iPreviousScrambleSegmentCount != sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount
		AND NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLING) // Do not play SFX if the scramble is in action
			SWITCH CEIL(sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount/ 10.0)
				CASE 0
				CASE 1
					PLAY_SOUND_FRONTEND(-1, "Scramble_Countdown_High", sGameplayData.sBaseStruct.sAudioSet)
				BREAK
			
				CASE 2
					PLAY_SOUND_FRONTEND(-1, "Scramble_Countdown_MED", sGameplayData.sBaseStruct.sAudioSet)
				BREAK
			ENDSWITCH

			// High countdown if at 3 or higher
			IF CEIL(sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount/ 10.0) >= 3	
				PLAY_SOUND_FRONTEND(-1, "Scramble_Countdown_Low", sGameplayData.sBaseStruct.sAudioSet)
			ENDIF
			
			sGameplayData.sBaseStruct.iPreviousScrambleSegmentCount = sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount
		ENDIF
	ENDIF
	
	PRINTLN("[HACKINGGAME] Number of segments is ", numberOfSegments)
	INT i
	FLOAT posX
	FOR i = 1 TO sGameplayData.sBaseStruct.iCurrentScrambleSegmentCount
		posX = (fPositionX - fLeftMost) + fDiff*i
		//DRAW_HACKING_PIXELSPACE_RECT(INIT_HG_VECTOR(posX, fPositionY+fElementOffsetY), INIT_HG_VECTOR(fElementWidth,fElementHeight), sColourStruct)
		//DRAW_SPRITE(pTextureDictionaryName, pTextureName, HG_APPLY_ASPECT_MOD_TO_X(CentreX), CentreY, GET_OUTPUT_WIDTH_FOR_IMAGE(Width) * fHG_AspectRatioMod , GET_OUTPUT_HEIGHT_FOR_IMAGE(Height), Rotation, R, G, B, A, DoStereorize)
		DRAW_RECT(HG_APPLY_ASPECT_MOD_TO_X(posX), fPositionY+fElementOffsetY, (fElementWidth/1920.0)* fHG_AspectRatioMod, fElementHeight/1080.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA)
		//DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_SCRAMBLER", "FINGERPRINT_HACKING_MINIGAME_SCRAMBLER_FILL_SEGMENT", posX, fPositionY+fElementOffsetY, fElementWidth, fElementHeight, 0.0, sColourStruct.iR, sColourStruct.iG, sColourStruct.iB, sColourStruct.iA )
	ENDFOR

ENDPROC

PROC DRAW_FINGERPRINT_RETRO_PLAY_AREA(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	//DRAW_PLAY_AREA_COMMON(sGameplayData.sBaseStruct)
	
	// Draw Mode Specific play area here
	// Background frames
	DRAW_HACKING_SPRITE_CORRECTED("MPFClone_Retro_BACKGROUND", "FINGERPRINT_HACKING_MINIGAME_BACKGROUND", cfHG_BG_XPOS, cfHG_BG_YPOS, 1980.0, 1080.0, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	
	// Timer
	DRAW_RETRO_GAME_TIMER(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iMaxTime, cfFR_TIMER_POSITIONX, cfFR_TIMER_POSITIONY, cfFR_TIMER_WIDTH, cfFR_TIMER_HEIGHT, rgbaHighlighted, cfFR_TIMER_DIGIT_DIFFERENCE)
	
	// Draw Lives
	// TODO: May need to be restored if we opt for restoring the old gameplay at any point.
	//DRAW_RETRO_GAME_LIVES(sGameplayData.sBaseStruct, TRUE, cfFR_LIVES_POSITIONX, cfFR_LIVES_POSITIONY, cfHG_LIVES_WIDTH, cfHG_LIVES_HEIGHT, sGameplayData.rgbaTintColour, cfFR_LIVES_OFFSET)
	
	// Draw scramble timer
	DRAW_HACKING_GAME_SCRAMBLER_R(sGameplayData, sGameplayData.sBaseStruct.iScrambleTime, cfFR_SCRAMBLE_POSITIONX, cfFR_SCRAMBLE_POSITIONY, cfFR_SCRAMBLE_WIDTH, cfFR_SCRAMBLE_HEIGHT, sGameplayData.rgbaTintColour, cfFR_SCRAMBLE_LEFTPOINT, cfFR_SCRAMBLE_DIFFBETWEENSEGMENT, cfFR_SCRAMBLE_ELEMENT_WIDTH, cfFR_SCRAMBLE_ELEMENT_HEIGHT, FC_IS_PLAYER_ABLE_TO_INTERACT_R(sGameplayData), cfFR_SCRAMBLE_Y_OFFSET, IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SCRAMBLING))

//	DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.iCircleDecoration[0], ciFR_CIRCLEDECORATION_FRAMES, HG_ANIMATION_DEFAULT, "MPFClone_Retro_LIFE", "disc_A", cfFR_CIRCLEDECORATION_POSITIONX, cfFR_CIRCLEDECORATION_POSITIONY, cfFR_CIRCLEDECORATION_WIDTH, cfFR_CIRCLEDECORATION_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
//	DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.iCircleDecoration[1], ciFR_CIRCLEDECORATION_FRAMES, HG_ANIMATION_DEFAULT, "MPFClone_Retro_LIFE", "disc_B", cfFR_CIRCLEDECORATION_POSITIONX, cfFR_CIRCLEDECORATION_POSITIONY, cfFR_CIRCLEDECORATION_WIDTH, cfFR_CIRCLEDECORATION_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
//	DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.iCircleDecoration[2], ciFR_CIRCLEDECORATION_FRAMES, HG_ANIMATION_DEFAULT, "MPFClone_Retro_LIFE", "disc_C", cfFR_CIRCLEDECORATION_POSITIONX, cfFR_CIRCLEDECORATION_POSITIONY, cfFR_CIRCLEDECORATION_WIDTH, cfFR_CIRCLEDECORATION_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	
	// Draw Techoration
	//DRAW_HACKING_GAME_DECOR(sGameplayData.sBaseStruct, "MPFClone_Decor", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_TECHARATION_WIDTH, cfHG_TECHARATION_HEIGHT, sGameplayData.rgbaTintColour )
ENDPROC

PROC CLEAN_UP_FINGERPRINT_RETRO(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, BOOL bFinalCleanup = FALSE, INT iType = HASH("practice"), INT iPlaythroughId = 0)
	UNUSED_PARAMETER(iType)
	IF NOT bFinalCleanup
		iPlaythroughId = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST4_SESSION_ID_POSTIME)
		//sControllerStruct.iTimeElapsed = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdTimer) // Set elapsed time for telemetry
		PRINTLN("CLEAN_UP_FINGERPRINT_RETRO, iPlaythroughId = ", iPlaythroughId)
		HACKING_GAME_UPDATE_TELEMETRY_H4(sControllerStruct, sGameplayData.sBaseStruct, iPlaythroughId, HASH("fingerprint clone"), sGameplayData.iCurrentFingerprintIndex+1, GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdRetroTimer))
	ENDIF
	
	HACKING_GAME_CLOSE_LOOPING_SOUNDS(sGameplayData.sBaseStruct, "DLC_H4_Fingerprint_Hack_Scene")
	
	CLEAR_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
	HACKING_COMMON_CLEANUP(sControllerStruct, sGameplayData.sBaseStruct, bFinalCleanup)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_LIFE")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_BACKGROUND")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_LOADING")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_MESSAGES")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_NUMBERS")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPFClone_Retro_SCRAMBLER")
	
	INT i
	TEXT_LABEL_31 tl31
	FOR i = 0 TO ciFR_NUMBER_OF_PRINTS_INGAME -1
	
		// bugstar://7240212
		IF i < 0
			i = 0
		ENDIF
	
		tl31 = "MPFClone_Retro_Print"
		tl31 += i
		
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
			tl31 = "MPFClone_Retro_PrintFull"
			tl31 += (i+1)
		ENDIF
		
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(tl31)
	ENDFOR

	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_FH_MG")
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_EVERY_FRAME_MISC(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	HACKING_GAME_HANDLE_LOOPING_SOUNDS(sGameplayData.sBaseStruct, "DLC_H4_Fingerprint_Hack_Scene")
	
	PROCESS_COMMON_HACKING_EVERY_FRAME_MISC(sGameplayData.sBaseStruct, sControllerStruct)
ENDPROC

// Copy of the Init process
FUNC BOOL PROCESS_COMMON_GAME_INIT()
	PRINTLN("[MC][HackingGame] Running Init")
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		DISABLE_INTERACTION_MENU() 
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	TEXT_LABEL_23 tl23 = "mphackinggameoverlay"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF
	
	tl23 = "mphackinggameoverlay1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][HackingGame] Not Loaded ", tl23)
		PRINTLN("[MC][HackingGame] Not Loaded ", tl23)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

// PROCESS STATE FUNCTIONS
PROC PROCESS_FINGERPRINT_RETRO_INIT(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FAILED) // Don't re-enter Init when the game is cleaning up
		EXIT
	ENDIF
	
	FINGERPRINT_MINIGAME_GAMEPLAY_DATA sEmpty
	COPY_SCRIPT_STRUCT(sGameplayData, sEmpty, SIZE_OF(FINGERPRINT_MINIGAME_GAMEPLAY_DATA))
	
	// setup common data
	IF NOT PROCESS_COMMON_GAME_INIT()
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FingerPrintRetro] Not Loaded MPFClone_Retro")
		PRINTLN("[BAZ][FPR][HackingGame] General init returning false")
		EXIT
	ENDIF
	
	fr_RESET_GAMEPLAY_DATA(sGameplayData)
	// Load Common textures for fingerprint
	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro")
		PRINTLN("[BAZ][FPR][HackingGame] Not Loaded MPFClone_Retro")
		EXIT
	ENDIF

	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_BACKGROUND")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_BACKGROUND")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_BACKGROUND")
		PRINTLN("[BAZ][FPR][HackingGame] Not Loaded MPFClone_Retro_BACKGROUND")
		EXIT
	ENDIF

	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_LIFE")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_LIFE")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_LIFE")
		PRINTLN("[BAZ][FPR][HackingGame] Not Loaded MPFClone_Retro_LIFE")
		EXIT
	ENDIF

	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_LOADING")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_LOADING")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_LOADING")
		PRINTLN("[BAZ][FPR][HackingGame] Not Loaded MPFClone_Retro_LOADING")
		EXIT
	ENDIF

	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_MESSAGES")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_MESSAGES")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_MESSAGES")
		PRINTLN("[BAZ][FPR][HackingGame] Not Loaded MPFClone_Retro_MESSAGES")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_CORRECT")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_CORRECT")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_CORRECT")
		PRINTLN("[BAZ][FPR][HackingGame] Not Loaded MPFClone_Retro_CORRECT")
		EXIT
	ENDIF

	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_NUMBERS")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_NUMBERS")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_NUMBERS")
		EXIT
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPFClone_Retro_SCRAMBLER")
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED("MPFClone_Retro_SCRAMBLER")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_SCRAMBLER")
		PRINTLN("[BAZ][FP][FingerPrintRetro] Not Loaded MPFClone_Retro_SCRAMBLER")
		EXIT
	ENDIF
	 
	// Audio
	sGameplayData.sBaseStruct.sAudioSet = "DLC_H4_Fingerprint_Hack_Minigame_Sounds"
	
	// We only need to grab and assign a sound id if we do not currently have one.
	IF sGameplayData.iProcessingSoundID = -1
		sGameplayData.iProcessingSoundID = GET_SOUND_ID()
	ENDIF
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEI4/DLC_HEI4_FH_MG")
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ][FP][FingerPrintRetro] Not Loaded audio bank DLC_HEI4/DLC_HEI4_FH_MG")
		PRINTLN("[BAZ][FP][FingerPrintRetro] Not Loaded audio bank DLC_HEI4/DLC_HEI4_FH_MG")
		EXIT
	ENDIF
	
	// Force set full match on,
	IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
	
	// Force debug on
#IF IS_DEBUG_BUILD
		bFullMatch = TRUE
#ENDIF
		SET_BIT(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
	ENDIF
	
	// Initialise any timers
	fHelpTimer = 0.0
	
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_SHOWN_HELP)
		CLEAR_BIT(sGameplayData.iF_Bs, ciFR_SHOWN_HELP)
	ENDIF
	
	//Turn on blinders for multihead displays.
	SET_MULTIHEAD_SAFE(TRUE, TRUE)
	g_FMMC_STRUCT.bDisablePhoneInstructions = TRUE
	
	//Prevent notifications from appearing during the minigame.
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].g_bIsHacking = TRUE
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bIsHacking)
	
	sGameplayData.sBaseStruct.sQuitTimer.action = INPUT_FRONTEND_CANCEL
	sGameplayData.sBaseStruct.sQuitTimer.control = FRONTEND_CONTROL
	
	sGameplayData.sBaseStruct.sQuitTimerPC.control = FRONTEND_CONTROL
	sGameplayData.sBaseStruct.sQuitTimerPC.action = INPUT_FRONTEND_DELETE
	
	sGameplayData.sBaseStruct.iHGBackgroundLoopSndId = GET_SOUND_ID()
	
	CLEAR_BIT(sControllerStruct.iBS, ciHGBS_QUIT)
	
	// Restart / start the new timer if it hasn't been already
	START_NET_TIMER(sGameplayData.sBaseStruct.tdRetroTimer) // Safe to use here as it doesnt run if already initialised.
	REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdRetroTimer)
	
	UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_LOAD_LEVEL)
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_LOAD_LEVEL(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages)
	HACKING_GAME_SET_NUMBER_OF_PATTERNS(sGameplayData.sBaseStruct, iNumberOfStages)

	SETUP_FINGERPRINTS_R(sGameplayData, sControllerStruct)
	
#IF IS_DEBUG_BUILD
	IF bFullMatch
		IF NOT IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
			//CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] PROCESS_FINGERPRINT_RETRO_INIT - ciFR_FULL_MATCH - Set FULL MATCH version")
			SET_BIT(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		ENDIF
	ELSE
		IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
		ENDIF
	ENDIF
#ENDIF
	
	// Load textures for finger prints
	IF NOT LOAD_FINGERPRINTS_R(sGameplayData, sControllerStruct)
		EXIT
	ENDIF
	
	LOAD_BINK_MOVIE(sGameplayData.sBaseStruct, "FingerPrint_Intro")
	
	// Start animation time
	REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
	
	//FINGERPRINT_RETRO_SETUP_HACKER_DETAILS(sGameplayData)
	
	UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_STARTUP)
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_STARTUP(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	// Run the startup process
	PROCESS_HACKING_GAME_STARTUP(sControllerStruct, sGameplayData.sBaseStruct)
	
	IF MAINTAIN_BINK_MOVIE(sGameplayData.sBaseStruct)
	OR HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciFR_STARTUP_TIMER)
		RELEASE_BINK_MOVIE(sGameplayData.sBaseStruct.movieId)
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdEndDelay) // Reset for next movie
		UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PLAY)
		//UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_VISUAL_TEST)
	ENDIF
	
	// Auto set the first element selected in FUll Match mode
	IF IS_BIT_SET(sGameplayData.iF_Bs, ciFR_FULL_MATCH)
	
		INT i
		FOR i = 0 TO ciFR_NUMBER_OF_FINGERPRINT_ELEMENTS-1
			IF sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iPositioning = 0 // Auto select first
				IF NOT IS_BIT_SET(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
					sGameplayData.iSelectedElementCount++
					SET_BIT(sGameplayData.sFingerPrintElements[sGameplayData.iCurrentFingerprintIndex][i].iBitset, ciFR_SELECTED)
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_PLAY(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfPlays)
	BOOL bCanInteract = fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		
	PROCESS_HACKING_GAME_PLAY(sControllerStruct, sGameplayData.sBaseStruct, iNumberOfPlays, bCanInteract)
	
	IF bCanInteract
		PROCESS_FINGERPRINT_RETRO_INPUT(sGameplayData)
	ENDIF
	
	PROCESS_FINGERPRINT_RETRO_GAME(sGameplayData, sControllerStruct)
	
	DRAW_FINGERPRINT_RETRO_GAME(sGameplayData)
	
	MAINTAIN_FINGERPRINT_RETRO_CONTEXT(sGameplayData)
ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_PASS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_PASS(sControllerStruct, sGameplayData.sBaseStruct, ciFR_END_DELAY, "FingerPrint_Success")

ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_FAIL(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_FAIL(sControllerStruct, sGameplayData.sBaseStruct, ciFR_END_DELAY, "FingerPrint_Fail")

ENDPROC

PROC PROCESS_FINGERPRINT_RETRO_VISUAL_TEST(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_VISUAL_TEST(sControllerStruct, sGameplayData.sBaseStruct)
	
	IF fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		PROCESS_FINGERPRINT_RETRO_INPUT(sGameplayData)
	ENDIF
	
	DRAW_FINGERPRINT_RETRO_GAME(sGameplayData)
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_FINGERPRINT_RETRO_WIDGETS(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	IF NOT sGameplayData.sBaseStruct.bDebugCreatedWidgets
		PRINTLN("[MC][FingerprintCloneDebug] - Widgets not made ")
		
		
		sGameplayData.sBaseStruct.widgetFingerprintClone = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
		SET_CURRENT_WIDGET_GROUP(sGameplayData.sBaseStruct.widgetFingerprintClone)
		START_WIDGET_GROUP("FingerPrintClone_Retro")
			
			// Game Values
			ADD_WIDGET_STRING("Game Values")
			
			ADD_WIDGET_INT_SLIDER("Lives:", sGameplayData.sBaseStruct.iDebugLivesToSet, 1, 6, 1 )
			
			ADD_WIDGET_FLOAT_SLIDER("Time:", sGameplayData.sBaseStruct.fDebugTimeToSet, 1.0, 10.0, 0.5 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Time:", sGameplayData.sBaseStruct.fDebugScrambleTimeToSet, 10.0, 500.0, 1.0 )
			ADD_WIDGET_INT_SLIDER("Scramble Wipe Chance:", sGameplayData.sBaseStruct.iScrambleWipeChanceToSet, 0, 100, 1 )
			
			ADD_WIDGET_BOOL("RESET GAME", sGameplayData.sBaseStruct.bDebugReset)
			ADD_WIDGET_BOOL("ALT FULL MATCH VERSION", bFullMatch)
			ADD_WIDGET_BOOL("ALT FULL RESTART", bFullRestart)
			// Positioning Values
//			ADD_WIDGET_STRING("Debug Values")
//			ADD_WIDGET_FLOAT_SLIDER("Background X", cfHG_BG_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Y", cfHG_BG_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Width", cfHG_BG_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Height", cfHG_BG_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Play Area Width", cfHG_PLAYAREA_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Play Area Height", cfHG_PLAYAREA_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Techaration Width", cfHG_TECHARATION_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Techaration Height", cfHG_TECHARATION_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 X", cfHG_tech1_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Y", cfHG_tech1_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Width", cfHG_tech1_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Height", cfHG_tech1_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 X", cfHG_tech2_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Y", cfHG_tech2_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Width", cfHG_tech2_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Height", cfHG_tech2_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 X", cfHG_tech3_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Y", cfHG_tech3_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Width", cfHG_tech3_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Height", cfHG_tech3_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 X", cfHG_tech4_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Y", cfHG_tech4_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Width", cfHG_tech4_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Height", cfHG_tech4_HEIGHT, 0.0, 2000.0, 10.0 )
//			
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintColumn 1", cfFR_FINGERPRINT_ELEMENT_COLUMN1, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 1", cfFR_FINGERPRINT_ELEMENT_ROW1, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 2", cfFR_FINGERPRINT_ELEMENT_ROW2, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 3", cfFR_FINGERPRINT_ELEMENT_ROW3, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 4", cfFR_FINGERPRINT_ELEMENT_ROW4, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 5", cfFR_FINGERPRINT_ELEMENT_ROW5, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 6", cfFR_FINGERPRINT_ELEMENT_ROW6, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 7", cfFR_FINGERPRINT_ELEMENT_ROW7, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 8", cfFR_FINGERPRINT_ELEMENT_ROW8, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Width", cfFR_FINGERPRINT_ELEMENT_WIDTH, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Height", cfFR_FINGERPRINT_ELEMENT_HEIGHT, 0.0, 2000.0, 10.0 )
			
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Spacing", cfFR_FINGERPRINT_ELEMENT_SPACING, 0.0, 0.1, 0.001 )
			
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge X", cfFR_FINGERPRINT_LARGE_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Y", cfFR_FINGERPRINT_LARGE_POSITIONY, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Width", cfFR_FINGERPRINT_LARGE_WIDTH, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Height", cfFR_FINGERPRINT_LARGE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Left X", cfFR_GRID_DETAILS_LEFTX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Right X", cfFR_GRID_DETAILS_RIGHTX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Width", cfFR_GRID_DETAILS_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Height", cfFR_GRID_DETAILS_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise X", cfFR_GRID_NOISE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Y", cfFR_GRID_NOISE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Width", cfFR_GRID_NOISE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Height", cfFR_GRID_NOISE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage X", cfHG_STATUS_MESSAGE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Y", cfHG_STATUS_MESSAGE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Width", cfHG_STATUS_MESSAGE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Height", cfHG_STATUS_MESSAGE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
			ADD_WIDGET_FLOAT_SLIDER("Results Loading Initial X", cfFR_RESULTS_LOADING_INITIALX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Results Loading Diff X", cfFR_RESULTS_LOADING_XDIFF, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Results Loading Y", cfFR_RESULTS_LOADING_POSITIONY, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Results Loading Height", cfFR_RESULTS_LOADING_HEIGHT, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Results Loading Width", cfFR_RESULTS_LOADING_WIDTH, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_INT_SLIDER("Results Loading Max Segments", ciFR_RESULTS_LOADING_MAX_SEGMENTS, 0, 100, 1)
//			
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Width", cfFR_FINGERPRINT_TRACKER_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Height", cfFR_FINGERPRINT_TRACKER_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Selector Width", cfFR_FINGERPRINT_TRACKER_SELECTOR_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Selector Height", cfFR_FINGERPRINT_TRACKER_SELECTOR_HEIGHT, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Y", cfFR_FINGERPRINT_TRACKER_POSITIONY, 0.0, 2000.0, 0.01 )
			
			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker1 X", cfFR_FINGERPRINT_TRACKER1_POSITIONX, 0.0, 2000.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker2 X", cfFR_FINGERPRINT_TRACKER2_POSITIONX, 0.0, 2000.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker3 X", cfFR_FINGERPRINT_TRACKER3_POSITIONX, 0.0, 2000.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker4 X", cfFR_FINGERPRINT_TRACKER4_POSITIONX, 0.0, 2000.0, 0.01 )
			
//			ADD_WIDGET_FLOAT_SLIDER("Timer X", cfFR_TIMER_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Y", cfFR_TIMER_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Width", cfFR_TIMER_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Height", cfFR_TIMER_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Digit Difference", cfFR_TIMER_DIGIT_DIFFERENCE, 0.0, 2000.0, 0.01 )
//			
			ADD_WIDGET_FLOAT_SLIDER("Lives X", cfFR_LIVES_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Lives Y", cfFR_LIVES_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Width", cfHG_LIVES_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Height", cfHG_LIVES_HEIGHT, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Lives Offset", cfFR_LIVES_OFFSET, 0.0, 2000.0, 0.01 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration X", cfFR_CIRCLEDECORATION_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Y", cfFR_CIRCLEDECORATION_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Width", cfFR_CIRCLEDECORATION_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Height", cfFR_CIRCLEDECORATION_HEIGHT, 0.0, 2000.0, 10.0 )
//			
			ADD_WIDGET_FLOAT_SLIDER("Scramble X", cfFR_SCRAMBLE_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Y", cfFR_SCRAMBLE_POSITIONY, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble BG Width", cfFR_SCRAMBLE_WIDTH, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble BG Height", cfFR_SCRAMBLE_HEIGHT, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Width", cfFR_SCRAMBLE_ELEMENT_WIDTH, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Height", cfFR_SCRAMBLE_ELEMENT_HEIGHT, 0.0, 2000.0, 10.0 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Y Offset", cfFR_SCRAMBLE_Y_OFFSET, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Left Most", cfFR_SCRAMBLE_LEFTPOINT, -1.0, 1.0, 0.0001 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Difference", cfFR_SCRAMBLE_DIFFBETWEENSEGMENT, 0.0, 0.02, 0.0001 )
			
//			ADD_WIDGET_FLOAT_SLIDER("Default Anim Speed", cfHG_DEFAULT_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
//			ADD_WIDGET_FLOAT_SLIDER("Slow Anim Speed", cfHG_SLOW_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
//			ADD_WIDGET_FLOAT_SLIDER("Fast Anim Speed", cfHG_FAST_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )

//			ADD_WIDGET_INT_SLIDER("Overlay Alpha", ciHG_OVERLAY_ALPHA, 0, 255, 10)
//			ADD_WIDGET_INT_SLIDER("Overlay R", ciHG_OVERLAY_R, 0, 255, 10)
//			ADD_WIDGET_INT_SLIDER("Overlay G", ciHG_OVERLAY_G, 0, 255, 10)
//			ADD_WIDGET_INT_SLIDER("Overlay B", ciHG_OVERLAY_B, 0, 255, 10)
//			
//			ADD_WIDGET_INT_SLIDER("Shadow Alpha", ciHG_SHADOW_ALPHA, 0, 255, 10)
//			
//			ADD_WIDGET_BOOL("Hide Overlays", sGameplayData.sBaseStruct.bDebugHideOverlay)
//			ADD_WIDGET_BOOL("Hide Shadow Layer", sGameplayData.sBaseStruct.bDebugHideShadow)
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(sGameplayData.sBaseStruct.widgetFingerprintClone)
		sGameplayData.sBaseStruct.bDebugCreatedWidgets = TRUE
	ENDIF
ENDPROC


/// DEBUG
PROC PROCESS_FINGERPRINT_RETRO_DEBUG(FINGERPRINT_MINIGAME_GAMEPLAY_DATA &sGameplayData)
	
	IF NOT sGameplayData.sBaseStruct.bDebugCreatedWidgets
		PROCESS_FINGERPRINT_RETRO_WIDGETS(sGameplayData)
	ENDIF
	
	IF NOT fr_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		DRAW_DEBUG_TEXT_2D("INTERACTION LOCKED", NULL_VECTOR(), 255, 0, 0)
	ENDIF
	
	IF sGameplayData.sBaseStruct.bDebugReset
		sGameplayData.sBaseStruct.iMaxTime = FLOOR(sGameplayData.sBaseStruct.fDebugTimeToSet * 60000)
		sGameplayData.sBaseStruct.iScrambleTime = FLOOR(sGameplayData.sBaseStruct.fDebugScrambleTimeToSet * 1000)
		sGameplayData.sBaseStruct.iCurrentLives = sGameplayData.sBaseStruct.iDebugLivesToSet
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdRetroTimer)
		sGameplayData.sBaseStruct.iScrambleWipeChance = sGameplayData.sBaseStruct.iScrambleWipeChanceToSet
		sGameplayData.sBaseStruct.bDebugReset = FALSE
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "")
		PRINTLN("[MC][FingerprintClone] - PROCESS_FINGERPRINT_RETRO_DEBUG - J SKIP - Going to HACKING_GAME_PASS")
		UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PASS)
	ENDIF
	
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			PRINTLN("[MC][FingerprintClone] - PROCESS_FINGERPRINT_RETRO_DEBUG - J SKIP - Going to HACKING_GAME_VISUAL_TEST")
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
			CLEAR_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
			UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_VISUAL_TEST)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			sGameplayData.sBaseStruct.bDebugShowGuide = !sGameplayData.sBaseStruct.bDebugShowGuide
		ENDIF
	ELIF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			PRINTLN("[MC][FingerprintClone] - PROCESS_FINGERPRINT_RETRO_DEBUG - J SKIP - Going to HACKING_GAME_PLAY")
			sGameplayData.iCurrentFingerprintIndex = 0
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_SCRAMBLING)
			UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PLAY)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL, "")
			SET_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			SET_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
		ENDIF

		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_SHIFT, "")
			SET_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			SET_BIT(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_ALT, "")
			SET_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			SET_BIT(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
		ENDIF

		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_FAILED_FINGERPRINT)
			CLEAR_BIT(sGameplayData.iF_Bs, ciFR_PASSING_FINGERPRINT)
			SET_BIT(sGameplayData.iF_Bs, ciFR_SHOWING_LOADING)
		ENDIF
	ENDIF
ENDPROC
#ENDIF
///
