/* ===================================================================================== *\
		MISSION NAME	:	net_gang_ops_planning.sch
		AUTHOR			:	Alastair Costley - 2017/07/03
		DESCRIPTION		:	Header for the Gang Ops planning board.
\* ===================================================================================== */


// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "net_FPS_cam.sch"
USING "net_gang_ops_helpers.sch"
USING "net_gang_ops_scaleform.sch"
USING "MP_Scaleform_Functions.sch"
USING "hud_drawing.sch"
USING "SVM_MISSION_FLOW.sch"

/* ===================================================================================== *\
			VARIABLES + INITIAL DECLARATIONS + TYPES
\* ===================================================================================== */


/* ===================================================================================== *\
			DEBUG ONLY
\* ===================================================================================== */
#IF IS_DEBUG_BUILD

PROC PRIVATE_PROCESS_GANG_OPS_PLANNING_WIDGET_COMMANDS(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	IF sClientData.sDebugData.bStart
	
		IF sClientData.sStateData.eState != GOBS_IDLE
			sClientData.sStateData.eState = GOBS_IDLE
		ENDIF
		
		sClientData.sStateData.eState = GOBS_INIT
		sClientData.sDebugData.bStart = FALSE
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - Debug Starting Planning")
	ENDIF

	sClientData.sCameraData.ePositionId = INT_TO_ENUM(GANG_OPS_CAMERA_LOCATION, sClientData.sCameraData.iPositionId)
	
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_WIDGET_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	TEXT_LABEL_63 sTemp = ""

	IF NOT sClientData.sDebugData.bCreatedWidgets
		IF DOES_WIDGET_GROUP_EXIST(sClientData.sDebugData.wgParentGroup)
			SET_CURRENT_WIDGET_GROUP(sClientData.sDebugData.wgParentGroup)
			
			sClientData.sDebugData.wgActiveGroup = START_WIDGET_GROUP("  Gang Ops (Planning)") 
			
				ADD_WIDGET_STRING(" - DEBUG SETTINGS - ")
				ADD_WIDGET_BOOL("Start", sClientData.sDebugData.bStart)
				ADD_WIDGET_BOOL("Reset", sClientData.sDebugData.bReset)
				ADD_WIDGET_BOOL("Player Visibility", sClientData.sDebugData.bPlayerVisible)
				ADD_WIDGET_BOOL("Lock Rendering to Player", sClientData.sDebugData.bLockToPlayer)
				
				ADD_WIDGET_STRING(" - DEBUG ACTIONS - ")
				ADD_WIDGET_BOOL("Interact", sClientData.sStateData.bInteract)
				
				
				START_WIDGET_GROUP("Component Toggle")
					ADD_WIDGET_BOOL("Component Toggle: Scaleform", sClientData.sScaleformData.bEnableScaleform)
					ADD_WIDGET_BOOL("Component Toggle: Camera", sClientData.sCameraData.bEnableCamera)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Scaleform")
					ADD_WIDGET_BOOL("Verbose Scaleform Logging", sClientData.sScaleformData.bVerboseDebugging)
					
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Pos", sClientData.sScaleformData.vMainBoardPosition, -9999.9, 9999.9, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Rot", sClientData.sScaleformData.vMainBoardRotation, -1000.0, 1000.0, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - Size", sClientData.sScaleformData.vMainBoardSize, 0.0, 100.0, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Size", sClientData.sScaleformData.vMainBoardWorldSize, 0.0, 100.0, 1.0)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Cameras")
				
					ADD_WIDGET_INT_SLIDER("Cam Position", sClientData.sCameraData.iPositionId, 0, 10, 1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Pos Offset", sClientData.sCameraData.vCamInitialOffset, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Map Pos Offset", sClientData.sCameraData.vCamMapOffset, -100.0, 100.0, 0.1)
					
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Rot Offset", sClientData.sCameraData.vCamInitialOffsetRot, -180.0, 180.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Map Rot Offset", sClientData.sCameraData.vCamMapOffsetRot, -180.0, 180.0, 0.1)
				
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Planning Data")
				
					sTemp = "Strand Name: " 
					sTemp += sClientData.sStrandData.tlStrandName
					ADD_WIDGET_STRING(sTemp)
										
					ADD_WIDGET_INT_SLIDER("Strand Cost: ", sClientData.sStrandData.iStrandCost, 0, 1000000, 1000)
					ADD_WIDGET_INT_SLIDER("Total Missions: ", sClientData.sStrandData.iTotalMissions, 0, 7, 1)
				
					INT index
				
					FOR index = 0 TO (sClientData.sStrandData.iTotalMissions-1)
					
						sTemp = "Mission Num: " 
						sTemp += index
						ADD_WIDGET_STRING(sTemp)
						
						sTemp = "Title: " 
						sTemp += sClientData.sStrandData.sMissionData[index].sMissionTitle
						ADD_WIDGET_STRING(sTemp)
						
						sTemp = "Desc: " 
						sTemp += sClientData.sStrandData.sMissionData[index].iMissionDescriptionHash
						ADD_WIDGET_STRING(sTemp)

//						ADD_WIDGET_BOOL("Set Unavailable", sClientData.sStrandData.sMissionData[index].bNotAvailable)
					
					ENDFOR
				
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Highlight / Cursor Data") 
					//DEBUG_eFocusPoint = ENUM_TO_INT(			g_HeistPrePlanningClient.sBoardState.eFocusPoint)
					//ADD_WIDGET_INT_SLIDER("eFocusPoint: ", 		DEBUG_eFocusPoint, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iHighlight: ", 		sClientData.sCursorData.iHighlight, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iSubItemSelection: ",sClientData.sCursorData.iSubItemSelection, -100, 100, 1)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
			
			CLEAR_CURRENT_WIDGET_GROUP(sClientData.sDebugData.wgParentGroup)
			
			sClientData.sDebugData.bCreatedWidgets = TRUE
		ENDIF
	ENDIF

	PRIVATE_PROCESS_GANG_OPS_PLANNING_WIDGET_COMMANDS(sClientData)

ENDPROC

#ENDIF

/* ===================================================================================== *\
			HELPERS AND MISC WRAPPERS
\* ===================================================================================== */

/// PURPOSE:
///    Sets the mission unavailable value
FUNC BOOL CAN_PLAYER_LAUNCH_MISSION(GANG_OPS_MISSION_ENUM eMission)
		
	IF IS_GANG_OPS_MISSION_A_FREEMODE_PREP_MISSION(eMission)
		IF GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_FM_GANGOPS) != GB_MU_REASON_AVAILABLE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] CAN_PLAYER_LAUNCH_MISSION - FALSE - GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), FMMC_TYPE_FM_GANGOPS) != GB_MU_REASON_AVAILABLE")
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_PURCHASE_MISSION_EQUIPMENT(GANG_OPS_MISSION_ENUM eMission #IF IS_DEBUG_BUILD, BOOL bBlockPrints = TRUE #ENDIF)
	IF NOT IS_GANG_OPS_MISSION_A_FREEMODE_PREP_MISSION(eMission)
		
		#IF IS_DEBUG_BUILD
		IF !bBlockPrints
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but selected mission is not a prep mission")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_GANG_OPS_MISSION_STATE(PLAYER_ID(), eMission) = GANG_OPS_MISSION_STATE_COMPLETED
		
		#IF IS_DEBUG_BUILD
		IF !bBlockPrints
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but selected mission has already been completed")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_GANG_OPS_MISSION_STATE(PLAYER_ID(), eMission) = GANG_OPS_MISSION_STATE_STOLEN_FROM_RIVALS
		
		#IF IS_DEBUG_BUILD
		IF !bBlockPrints
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but selected mission equipment has been stolen from rivals")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_GANG_OPS_MISSION_STATE(PLAYER_ID(), eMission) = GANG_OPS_MISSION_STATE_PURCHASED_EQUIPMENT
		
		#IF IS_DEBUG_BUILD
		IF !bBlockPrints
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but selected mission equipment has already been purchased")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF eMission = GANG_OPS_MISSION_FREEMODE_FLIGHT_RECORDER
		IF NOT IS_GANG_OPS_MISSION_AVAILABLE(PLAYER_ID(), GANG_OPS_MISSION_FREEMODE_FLIGHT_RECORDER)
			
			#IF IS_DEBUG_BUILD
			IF !bBlockPrints
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but selected mission is flight recorder and the player has not completed all other missions yet")
			ENDIF
			#ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PRIVATE_REQUEST_GANG_OPS_PLANNING_SCALEFORM(GANG_OPS_SCALEFORM_DATA &sScaleformData)
	sScaleformData.siMainBoard = REQUEST_SCALEFORM_MOVIE("IAA_HEIST_BOARD")
	sScaleformData.siButtons = REQUEST_SCALEFORM_MOVIE("INSTRUCTIONAL_BUTTONS")
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Requested scaleform assets.")
ENDPROC

/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of it's own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sScaleformStruct - 
PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_INSTR_BTNS(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't render the buttons if the player is typing in network chat.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	// Don't render the buttons if the player is running the tutorial
	IF sClientData.sTutorialData.bTutorialInProgress
		EXIT
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF sClientData.sCursorData.bUsingKeyboardAndMouse = FALSE
			sClientData.sScaleformData.bUpdateInstructions = TRUE
			sClientData.sCursorData.bUsingKeyboardAndMouse = TRUE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Buttons - player is now using keyboard and mouse, need to update instructional buttons")
		ENDIF
	ELSE
		IF sClientData.sCursorData.bUsingKeyboardAndMouse = TRUE
			sClientData.sScaleformData.bUpdateInstructions = TRUE
			sClientData.sCursorData.bUsingKeyboardAndMouse = FALSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Buttons - player is no longer using keyboard and mouse, need to update instructional buttons")
		ENDIF
	ENDIF
	
	IF NOT sClientData.sScaleformData.bUpdateInstructions
	
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.siButtons,
											spPlacementLocation,
											sClientData.sScaleformData.sButtons,
											FALSE)
	
		EXIT
	ENDIF
	
	IF NOT GET_IS_WIDESCREEN()
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sScaleformData.sButtons, 0.5)
	ELSE
	      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sScaleformData.sButtons, 0.7)
	ENDIF
	
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.sButtons)
	
	GANG_OPS_MISSION_ENUM eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandId)
	
	SWITCH sClientData.sCameraData.ePositionId
	
		CASE GOCAM_MAIN
		
			IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
			AND CAN_PLAYER_LAUNCH_MISSION(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_LAUNCH", sClientData.sScaleformData.sButtons, TRUE)
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
				
			IF CAN_PLAYER_PURCHASE_MISSION_EQUIPMENT(eMission)
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, 
				"HPPAY_SETUP", sClientData.sScaleformData.sButtons, TRUE)	
			ENDIF
			
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
				"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map.
				
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
				"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
			
		BREAK
			
		CASE GOCAM_OVERVIEW
		
			IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
			AND CAN_PLAYER_LAUNCH_MISSION(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_LAUNCH", sClientData.sScaleformData.sButtons, TRUE)
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
				
			IF CAN_PLAYER_PURCHASE_MISSION_EQUIPMENT(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, 
				"HPPAY_SETUP", sClientData.sScaleformData.sButtons, TRUE)	
			ENDIF			
		
			SWITCH sClientData.sCameraData.eCachedPositionId
				CASE GOCAM_MAIN
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
					"HP_MISSION", sClientData.sScaleformData.sButtons, TRUE) // View missions
				BREAK
				
				CASE GOCAM_MAP
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
					"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map
				BREAK
				
				CASE GOCAM_IMAGES
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
					"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance
				BREAK
			ENDSWITCH
				
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RT, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look 
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
				"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
			
		BREAK
		
		CASE GOCAM_MAP
		
			IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
			AND CAN_PLAYER_LAUNCH_MISSION(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_LAUNCH", sClientData.sScaleformData.sButtons, TRUE)
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE)
					
			IF CAN_PLAYER_PURCHASE_MISSION_EQUIPMENT(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, 
				"HPPAY_SETUP", sClientData.sScaleformData.sButtons, TRUE)	
			ENDIF
					
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
			
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"HP_MISSION", sClientData.sScaleformData.sButtons, TRUE) // View missions.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
				"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance.
												
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
				"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
				
		BREAK
			
		CASE GOCAM_IMAGES
		
			IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
			AND CAN_PLAYER_LAUNCH_MISSION(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HEIST_IB_LAUNCH", sClientData.sScaleformData.sButtons, TRUE)
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE)
					
			IF CAN_PLAYER_PURCHASE_MISSION_EQUIPMENT(eMission)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, 
				"HPPAY_SETUP", sClientData.sScaleformData.sButtons, TRUE)	
			ENDIF
					
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
			
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
				"HP_MISSION", sClientData.sScaleformData.sButtons, TRUE) // View missions.
			
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
			ENDIF
												
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
				"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.

		BREAK
	
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Buttons - Updated instructional buttons for position: ", ENUM_TO_INT(sClientData.sCameraData.ePositionId))
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.siButtons,
										spPlacementLocation,
										sClientData.sScaleformData.sButtons,
										sClientData.sScaleformData.bUpdateInstructions)
											
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, FALSE)
								
ENDPROC

PROC PRIVATE_RENDER_GANG_OPS_PLANNING_SCALEFORM(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	IF sClientData.sScaleformData.bDrawBoard
	AND g_bPlayingGangOpsCutscene = FALSE
		IF HAS_SCALEFORM_MOVIE_LOADED(sClientData.sScaleformData.siMainBoard)

			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_TEXT_RENDER_ID(sClientData.sScaleformData.iRenderTargetID)
			SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sClientData.sScaleformData.siMainBoard, TRUE)
			DRAW_SCALEFORM_MOVIE(sClientData.sScaleformData.siMainBoard, cfHEIST_SCREEN_CENTRE_X, cfHEIST_SCREEN_CENTRE_Y, cfHEIST_SCREEN_WIDTH, cfHEIST_SCREEN_HEIGHT, 255, 255, 255, 255)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
				
			IF NOT sClientData.sAudioData.bStartedLoopingBackgroundSound
				sClientData.sAudioData.iLoopingBackgroundSoundId = GET_SOUND_ID()
				IF sClientData.sAudioData.iLoopingBackgroundSoundId != -1
					PLAY_SOUND_FROM_COORD(sClientData.sAudioData.iLoopingBackgroundSoundId, "Background", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Render - Started looping background sound as we've started drawing the screen. Sound ID: ", sClientData.sAudioData.iLoopingBackgroundSoundId)
					sClientData.sAudioData.bStartedLoopingBackgroundSound = TRUE
				ENDIF
			ENDIF
									
			IF sClientData.sScaleformData.bVerboseDebugging
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Render - Index: ", NATIVE_TO_INT(sClientData.sScaleformData.siMainBoard))
			ENDIF
		ENDIF
	ENDIF
	
	IF sClientData.sScaleformData.bDrawInstructions	
		PRIVATE_UPDATE_GANG_OPS_PLANNING_INSTR_BTNS(sClientData)
	ENDIF

ENDPROC

PROC PRIVATE_INIT_GANG_OPS_PLANNING_CAMERA(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	sClientData.sCameraData.vBaseRotation.X = 0.0
	sClientData.sCameraData.vBaseRotation.Y = 0.0
	sClientData.sCameraData.vBaseRotation.Z = -180.0
	
	VECTOR vPosOffset = PUBLIC_GET_GANG_OPS_POSITION_OFFSET(sClientData.sCameraData, GOVEC_CAM_INITIAL)
	VECTOR vCamPos =  PUBLIC_GET_GANG_OPS_CAMERA_POSITION_FOR_POSITION_ID(sClientData.sCameraData)
	
	VECTOR vRotOffset = PUBLIC_GET_GANG_OPS_ROTATION_OFFSET(sClientData.sCameraData, GOVEC_CAM_INITIAL)
	VECTOR vCamRot = PUBLIC_GET_GANG_OPS_CAMERA_ROTATION_FOR_POSITION_ID(sClientData.sCameraData)
	
	INIT_FIRST_PERSON_CAMERA(	sClientData.sCameraData.sFPSCam, 
								vCamPos,
								vCamRot, // TODO: Re-add for rotation.
								60, // TODO: This needs to be done properly 
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, 0, -1, FALSE)

	PUBLIC_SET_GANG_OPS_FPS_CAM_FAR(sClientData.sCameraData)

	SET_WIDESCREEN_BORDERS(TRUE,0)	
	
	GANG_OPS_VECTORS eNewLoc = PUBLIC_GET_GANG_OPS_POSITION_FOR_CAM_STATE(sClientData.sCameraData.ePositionId)

	vPosOffset = PUBLIC_GET_GANG_OPS_POSITION_OFFSET(sClientData.sCameraData, eNewLoc)
	vRotOffset = PUBLIC_GET_GANG_OPS_ROTATION_OFFSET(sClientData.sCameraData, eNewLoc)

	sClientData.sCameraData.ePositionId = GOCAM_MAIN
//	sClientData.sCameraData.vCamCurrentPos = <<349.1823, 4870.0718, -58.7310>> //PUBLIC_GET_GANG_OPS_POSITION_FROM_BOARD(sClientData.sPositionData, vPosOffset)
//	sClientData.sCameraData.vCamCurrentRot = <<-0.8655, -0.0000, -34.8697>> //PUBLIC_GET_GANG_OPS_ROTATION_FROM_BOARD(sClientData.sCameraData, vRotOffset)
	
	sClientData.sCameraData.vCamCurrentPos = PUBLIC_GET_GANG_OPS_CAMERA_POSITION_FOR_POSITION_ID(sClientData.sCameraData)
	sClientData.sCameraData.vCamCurrentRot = PUBLIC_GET_GANG_OPS_CAMERA_ROTATION_FOR_POSITION_ID(sClientData.sCameraData)
	
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Camera - Started FPS camera.")
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Pos: ", vCamPos)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Rot: ", vCamRot)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Current Pos: ", sClientData.sCameraData.vCamCurrentPos)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Current Rot: ", sClientData.sCameraData.vCamCurrentRot)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Pos Offset: ", vPosOffset)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Rot Offset: ", vRotOffset)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Base Rot: ", sClientData.sCameraData.vBaseRotation)

ENDPROC

PROC PRIVATE_CLEAR_GANG_OPS_PLANNING_CAMERA(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	CLEAR_FIRST_PERSON_CAMERA(sClientData.sCameraData.sFPSCam)
	SET_WIDESCREEN_BORDERS(FALSE,0)	

	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Camera - Cleared FPS camera.")
	
ENDPROC


/// PURPOSE:
///    Manage and maintain the UP/DOWN selection indexes driven from by front-end controls.
/// PARAMS:
///    eHighlightType - ENUM - Direction of travel for the highlighter.
PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_CONTROL_SELECTIONS(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, GANG_OPS_CONTROL_TYPE eControl)

	SWITCH eControl
	
		CASE GOCT_DOWN
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_DOWN)

			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - DOWN.")
			
			SWITCH sClientData.sStrandData.iStrandId
				CASE ciGANGOPS_MISSION_STRAND_IAA
					IF sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_1
					OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_2
					OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_FINAL_MISSION
						PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				BREAK
				
				CASE ciGANGOPS_MISSION_STRAND_SUBMARINE
					IF sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_1
					OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_2
					OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_FINAL_MISSION
						PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				BREAK
				
				CASE ciGANGOPS_MISSION_STRAND_MISSILE_SILO
					IF sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_1
					OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_2
					OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_FINAL_MISSION
						PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				BREAK
			ENDSWITCH

		BREAK
			
		CASE GOCT_UP
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_UP)
			
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - UP.")
			
			IF sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_2
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_FINAL_MISSION
				PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
		BREAK
			
		CASE GOCT_LEFT
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_LEFT)
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - LEFT.")
			
			IF sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_PREP_MISSION_1
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_1
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_1
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_1
				PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "Nav_Left_Right_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
		BREAK
			
			
		CASE GOCT_RIGHT
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_RIGHT)
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - RIGHT.")
			
			IF sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_FINAL_MISSION
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_FINAL_MISSION
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_FINAL_MISSION
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_FINAL_MISSION
			OR sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_FINAL_MISSION
				PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "Nav_Left_Right_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
		BREAK
			
	ENDSWITCH

ENDPROC

PROC PRIVATE_UPDATE_CAMERA_AND_FOCUS_FOR_INPUT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, GANG_OPS_CONTROL_TYPE eControl)

	// TODO: Do we want to cancel transitions midway?
	IF sClientData.sCameraData.bTransitioning
		EXIT
	ENDIF

	SWITCH eControl
	
		CASE GOCT_LB

			IF sClientData.sCameraData.ePositionId = GOCAM_MAIN
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAP")
				sClientData.sCameraData.ePositionId = GOCAM_MAP
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAP)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_MAP
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_IMAGES")
				sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_IMAGES)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.ePositionId = GOCAM_MAIN
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
		BREAK
		
		CASE GOCT_RB
		
			IF sClientData.sCameraData.ePositionId = GOCAM_MAIN
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_IMAGES")
				sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_IMAGES)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_MAP
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.ePositionId = GOCAM_MAIN
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAP")
				sClientData.sCameraData.ePositionId = GOCAM_MAP
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAP)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
		BREAK
			
		CASE GOCT_Y
		
			IF sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.eCachedPositionId = sClientData.sCameraData.ePositionId
				sClientData.sCameraData.iCachedPositionId = sClientData.sCameraData.iPositionId
				sClientData.sCameraData.ePositionId = GOCAM_OVERVIEW
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_OVERVIEW)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_OVERVIEW
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_OVERVIEW")
				sClientData.sCameraData.ePositionId = sClientData.sCameraData.eCachedPositionId
				sClientData.sCameraData.iPositionId = sClientData.sCameraData.iCachedPositionId
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC PRIVATE_TOGGLE_GANG_OPS_INTERACTION(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, BOOL bEnableInteract)
	sClientData.sStateData.bInteract = bEnableInteract
	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_TOGGLE_GANG_OPS_INTERACTION - Toggle interact: ", sClientData.sStateData.bInteract)
ENDPROC

FUNC INT PRIVATE_GET_GANG_OPS_PLANNING_MISSION_STATE_FROM_SELECTION_ID(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, INT iSelectionID)

	SWITCH iSelectionID
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_1].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_2		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_1].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_FINAL_MISSION			RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_1].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_PREP_MISSION_1		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_2].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_PREP_MISSION_2		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_2].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_FINAL_MISSION			RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_2].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_1		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_3].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_2		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_3].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_FINAL_MISSION			RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_3].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_1		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_4].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_2		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_4].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_FINAL_MISSION			RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_4].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_1		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_5].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_2		RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_5].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_FINAL_MISSION			RETURN sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_5].iMissionState
	ENDSWITCH

	// Dont show, if any are blank this is probably why
	RETURN -1
ENDFUNC

PROC PRIVATE_SET_GANG_OPS_PLANNING_MISSION_STATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, INT iSelectionID, INT iState)
	SWITCH iSelectionID
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_1].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_2		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_1].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_FINAL_MISSION			sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_1].iMissionState = iState												BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_PREP_MISSION_1		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_2].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_PREP_MISSION_2		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_2].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_2_FINAL_MISSION			sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_2].iMissionState = iState												BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_1		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_3].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_PREP_MISSION_2		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_3].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_3_FINAL_MISSION			sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_3].iMissionState = iState												BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_1		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_4].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_PREP_MISSION_2		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_4].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_4_FINAL_MISSION			sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_4].iMissionState = iState												BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_1		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_5].sPrepMission[GANG_OPS_PREP_MISSION_1].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_2		sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_5].sPrepMission[GANG_OPS_PREP_MISSION_2].iMissionState = iState		BREAK
		CASE GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_FINAL_MISSION			sClientData.sStrandData.sMissionData[GANG_OPS_MISSION_5].iMissionState = iState												BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL PRIVATE_GANG_OPS_IS_SELECTION_ID_VALID_FOR_STRAND(INT iSelectionID, INT iStrand)
	IF GET_GANG_OPS_MISSION_FROM_SELECTION_ID(iSelectionID, iStrand) != GANG_OPS_MISSION_INVALID
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	IF sClientData.sStrandData.bUpdateMissionStates
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE - Processing")
		INT iLoop, iTempState
		GANG_OPS_MISSION_ENUM eMission
		REPEAT (GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_FINAL_MISSION+1) iLoop
			IF PRIVATE_GANG_OPS_IS_SELECTION_ID_VALID_FOR_STRAND(iLoop, sClientData.sStrandData.iStrandId)
				eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(iLoop, sClientData.sStrandData.iStrandId)
				iTempState = GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission)
				IF PRIVATE_GET_GANG_OPS_PLANNING_MISSION_STATE_FROM_SELECTION_ID(sClientData, iLoop) != iTempState
					PRIVATE_SET_GANG_OPS_PLANNING_MISSION_STATE(sClientData, iLoop, iTempState)
					PUBLIC_SET_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE - State of mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " has been updated to ", PRIVATE_GET_GANG_OPS_PLANNING_MISSION_STATE_FROM_SELECTION_ID(sClientData, iLoop))
				ELSE
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE - State of mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " is already ", GET_GANG_OPS_MISSION_STATE_NAME_FOR_DEBUG(iTempState))
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE - selection ID ", iLoop, " is not valid for strand")
			ENDIF
		ENDREPEAT
		IF sClientData.sStrandData.piLeader = PLAYER_ID()
			GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId++
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE - GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId++ = ", GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId)
		ENDIF
		sClientData.sStrandData.bUpdateMissionStates = FALSE
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_NET_DATA(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	IF sClientData.sStrandData.piLeader != PLAYER_ID()
	AND sClientData.sStrandData.piLeader != INVALID_PLAYER_INDEX()
		IF GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sStrandData.piLeader)].GangOps.iSyncId != GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_NET_DATA - My sync ID is out of sync with the leader, updating")
		
			sClientData.sStrandData.bUpdateMissionStates = TRUE
			GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId = GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sStrandData.piLeader)].GangOps.iSyncId
		ENDIF
		
		IF sClientData.sCursorData.iHighlight != GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sStrandData.piLeader)].GangOps.iHighlight
			sClientData.sCursorData.iHighlight = GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sStrandData.piLeader)].GangOps.iHighlight 
			PUBLIC_GANG_OPS_SET_CURRENT_SELECTION(sClientData.sScaleformData.siMainBoard, sClientData.sCursorData.iHighlight)
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_NET_DATA - leader highlight has changed, new highlight: ", sClientData.sCursorData.iHighlight)
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, BOOL bEnable)
	IF sClientData.sStrandData.piLeader = PLAYER_ID()
		IF bEnable
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - bEnable")
			CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "ENABLE_NAVIGATION")
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - !bEnable")
			CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "DISABLE_NAVIGATION")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - Not leader, disable")
		CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "DISABLE_NAVIGATION")
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	IF sClientData.sStrandData.piLeader = PLAYER_ID()
	AND NOT HAS_PLAYER_COMPLETED_GANG_OPS_TUTORIAL(PLAYER_ID())
		STRING sGangType
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
			sGangType = "HP_GANG_MC"
		ELSE
			sGangType = "HP_GANG_ORG"
		ENDIF
		SWITCH sClientData.sTutorialData.iTutorialStage
			CASE 0
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					// This is the Planning Screen. Here you can view the Freemode Preps and Setups that must be completed in order to progress to the Finale.
					PRINT_HELP("HP_TUT_1", 10000)
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = TRUE
					sClientData.sTutorialData.bTutorialInProgress = TRUE
					PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, FALSE)
					PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - HP_PLAN_TUT1 done, advancing tutorial stage")
				ENDIF
			BREAK
			
			CASE 1
				// In order to unlock a Setup you must obtain the necessary Heist Prep Equipment from Freemode Preps.
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_1")
					PRINT_HELP("HP_TUT_2", 10000)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - HP_PLAN_TUT2 done, advancing tutorial stage")
				ENDIF
			BREAK
			
			CASE 2
				// There are three ways to acquire Heist Prep Equipment. By completing Freemode Preps, stealing it from rival Heist Crews or by purchasing the equipment directly.
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_2")
					PRINT_HELP_WITH_STRING("HP_TUT_3", sGangType, 10000)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - HP_PLAN_TUT4 done, advancing tutorial stage")
				ENDIF
			BREAK
			
			CASE 3
				// Freemode Preps can be completed in any order. Setups can also be completed in any order once unlocked.
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_3")
					PRINT_HELP_WITH_STRING("HP_TUT_4", sGangType, 10000)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - HP_PLAN_TUT4 done, advancing tutorial stage")
				ENDIF
			BREAK
			
			CASE 4
				// Complete all Freemode Preps and Setups to progress to the Heist Finale Screen.
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_TUT_4")
					PRINT_HELP_WITH_STRING("HP_TUT_5", sGangType, 10000)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - HP_PLAN_TUT4 done, advancing tutorial stage")
				ENDIF
			BREAK
			
			CASE 5
				IF NOT IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("HP_TUT_5", sGangType)
					SET_PLAYER_HAS_COMPLETED_GANG_OPS_TUTORIAL()
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - Tutorial finished!")
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF sClientData.sTutorialData.bTutorialInProgress
			sClientData.sTutorialData.bTutorialInProgress = FALSE
			PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, TRUE)
			sClientData.sScaleformData.bDrawInstructions = TRUE
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
		ENDIF
		
		// ====HEIST ALREADY ACTIVE====
		//		If you have a Heist already active you can do one of the following:
		//		-	Call Lester to Cancel Heist and clear the planning screen â€“ Replay planning screen will appear
		//		-	Finish the current active Heist as normal â€“ Replay planning screen will appear
		//		When accessing the planning screen with an active Heist we display this help text:
		//		-	â€œYou have completed all Heists; cancel or complete your current Heist to access the Replay Planning Screen. You can cancel your current Heist by calling Lester.â€
		IF HAS_PLAYER_COMPLETED_ALL_GANG_OPS_HEIST_STRANDS(PLAYER_ID())
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iHeistStatusBitSet, GANG_OPS_BD_HEIST_STATUS_BITSET_USED_REPLAY_BOARD_TO_START_HEIST)
				IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_GANG_OPS_HEIST_REPLAY_AVAILABLE_HELP_2_DONE)
					PRINT_HELP("HPREPLAY_AV2")
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL - Replay available help 2 done - ", GET_FILENAME_FOR_AUDIO_CONVERSATION("HPREPLAY_AV2"))
					SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_GANG_OPS_HEIST_REPLAY_AVAILABLE_HELP_2_DONE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_LAUNCH_GANG_OPS_MISSION(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
//	IF sClientData.sCameraData.ePositionId != GOCAM_MAIN
//	AND sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
//		EXIT
//	ENDIF

	IF sClientData.sCursorData.bUpdateHighlight
	OR sClientData.sCursorData.bGetSelectionRequested
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_LAUNCH_GANG_OPS_MISSION - EXIT - sClientData.sCursorData.bUpdateHighlight OR sClientData.sCursorData.bGetSelectionRequested")
			
		EXIT
	ENDIF

	GANG_OPS_MISSION_ENUM eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandId)

	IF CAN_PLAYER_LAUNCH_MISSION(eMission)
		IF NOT IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
				
			IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
		
				PLAY_SOUND_FROM_COORD(-1,"Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_LAUNCH_GANG_OPS_MISSION - Selected to launch ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " which is available, go go warning screen")
				
				IF IS_GANG_OPS_MISSION_A_FREEMODE_PREP_MISSION(eMission)
					IF NOT PROCESS_GANGOPS_PREP_MISSION_WARNING_SCREENS(GET_GANGOPS_FLOW_AWARD_MISSION_FROM_GANG_OPS_MISSION_ENUM(eMission),sClientData.sStrandData.iWarningScreenBS,sClientData.sStrandData.iWarningScreen,TRUE)
						SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
					ENDIF
				ELSE
					SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
				ENDIF
				
			ELIF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_LOCKED
			
				PLAY_SOUND_FROM_COORD(-1,"Error", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_LAUNCH_GANG_OPS_MISSION - Selected to launch ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " which is locked. Dun dun duuun.")
				IF eMission = GANG_OPS_MISSION_FREEMODE_FLIGHT_RECORDER
				AND NOT IS_GANG_OPS_MISSION_AVAILABLE(PLAYER_ID(), GANG_OPS_MISSION_FREEMODE_FLIGHT_RECORDER)
					IF NOT IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_FLIGHT_RECORDER_ERROR)
						PRINT_HELP("HPLAUNCH_LOCK2")
						SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_FLIGHT_RECORDER_ERROR)
					ENDIF
				ELIF IS_GANG_OPS_MISSION_A_FREEMODE_PREP_MISSION(eMission)
					IF NOT IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_PREP_ERROR)
						PRINT_HELP("HPLAUNCH_LOCK3")
						SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_PREP_ERROR)
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_SETUP_ERROR)
						PRINT_HELP("HPLAUNCH_LOCK")
						SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_SETUP_ERROR)
					ENDIF
				ENDIF
			
			ELIF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_COMPLETED
			
				PLAY_SOUND_FROM_COORD(-1,"Error", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_LAUNCH_GANG_OPS_MISSION - Selected to launch ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " which has already been completed. Dun dun duuun.")
				IF NOT IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_COMPLETE_ERROR)
					PRINT_HELP("HPLAUNCH_COMP")
					SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SHOWN_LAUNCH_COMPLETE_ERROR)
				ENDIF
			
			ELIF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_DONT_SHOW
				// Do nothing
			ENDIF
		ENDIF
	ELSE
		PLAY_SOUND_FROM_COORD(-1,"Error", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MISSION_LAUNCH] PRIVATE_LAUNCH_GANG_OPS_MISSION - Selected to launch ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " But player cannot launch the mission as it is unavailable.")
		PRINT_HELP("HPLAUNCH_UNAV")
	ENDIF
ENDPROC

PROC PRIVATE_SET_GANG_OPS_PLANNING_HIGHLIGHT_UPDATE_REQUIRED(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, BOOL bUpdate)
	sClientData.sCursorData.bUpdateHighlight = bUpdate
	CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_PLANNING_HIGHLIGHT_UPDATE_REQUIRED - bUpdate = ", GET_STRING_FROM_BOOL(bUpdate))
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	IF sClientData.sCursorData.bUpdateHighlight
	AND sClientData.sCursorData.bGetSelectionRequested
		CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT - New input, scrap the current get selection query and grab the new one")
		sClientData.sCursorData.bGetSelectionRequested = FALSE
	ENDIF

	IF NOT sClientData.sCursorData.bGetSelectionRequested
		IF sClientData.sCursorData.bUpdateHighlight
			BEGIN_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "GET_CURRENT_SELECTION")
			sClientData.sCursorData.currentSelectionReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			sClientData.sCursorData.bUpdateHighlight = FALSE
			sClientData.sCursorData.bGetSelectionRequested = TRUE
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT - Started to grab current selection, return index: ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex))
		ENDIF
	ELSE
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sClientData.sCursorData.currentSelectionReturnIndex)
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Scaleform movie method return ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex)," ready!")
			INT iNewHighlight = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sClientData.sCursorData.currentSelectionReturnIndex)
			IF sClientData.sCursorData.iHighlight != iNewHighlight
				sClientData.sCursorData.iHighlight = iNewHighlight
				GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iHighlight = iNewHighlight
				DO_INTERACT_ANIM()
//				DO_LOOK_AT_COORD(iNewHighlight)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
				PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
				PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
				CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT - new highlight: ", sClientData.sCursorData.iHighlight, ", selected member: ", sClientData.sCursorData.iSelectedMember)
			ELSE
				CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT - Selection/highlight hasnt updated")
			ENDIF
			sClientData.sCursorData.bGetSelectionRequested = FALSE
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT - Waiting for scaleform movie method return ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex)," to be ready...")
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_GangOpsHeistDebugPad")
		GANG_OPS_MISSION_ENUM eMission, ePrepMission1, ePrepMission2
		
		eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandId)
		
		IF eMission != GANG_OPS_MISSION_INVALID
		
			IF IS_GANG_OPS_MISSION_A_FREEMODE_PREP_MISSION(eMission)
			
				IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
			
					SET_GANG_OPS_MISSION_COMPLETED(eMission)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION - [1] Complete prep mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission))
					
				ELIF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_COMPLETED
				OR GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_STOLEN_FROM_RIVALS
				OR GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_PURCHASED_EQUIPMENT
				
					SET_GANG_OPS_MISSION_COMPLETED(eMission, FALSE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION - Set prep mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " available")
				
				ENDIF
			
			ELIF IS_GANG_OPS_MISSION_AN_INSTANCED_MISSION(eMission)
			
				IF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_AVAILABLE
			
					SET_GANG_OPS_MISSION_COMPLETED(eMission)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION - Complete final mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission))
					
					ePrepMission1 = GET_GANG_OPS_PREP_MISSION_FROM_INSTANCED_MISSION(eMission, GANG_OPS_PREP_MISSION_1)
					IF ePrepMission1 != GANG_OPS_MISSION_INVALID
					AND GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, ePrepMission1) != GANG_OPS_MISSION_STATE_COMPLETED
						SET_GANG_OPS_MISSION_COMPLETED(ePrepMission1)
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION - [2] Complete prep mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(ePrepMission1))
					ENDIF
					
					ePrepMission2 = GET_GANG_OPS_PREP_MISSION_FROM_INSTANCED_MISSION(eMission, GANG_OPS_PREP_MISSION_2)
					IF ePrepMission2 != GANG_OPS_MISSION_INVALID
					AND GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, ePrepMission2) != GANG_OPS_MISSION_STATE_COMPLETED
						SET_GANG_OPS_MISSION_COMPLETED(ePrepMission2)
						CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION - [3] Complete prep mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(ePrepMission2))
					ENDIF
					
				ELIF GET_PLAYER_GANG_OPS_MISSION_STATE(sClientData.sStrandData.piLeader, eMission) = GANG_OPS_MISSION_STATE_COMPLETED
				
					SET_GANG_OPS_MISSION_COMPLETED(eMission, FALSE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION - Set final mission ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission), " available")
				
				ENDIF
			
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_DEBUG_GANG_OPS_COMPLETE_PREPS(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	UNUSED_PARAMETER(sClientData)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_GangOpsHeistDebugPad")
	
	ENDIF
ENDPROC

PROC PRIVATE_DEBUG_GANG_OPS_COMPLETE_STRAND(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	UNUSED_PARAMETER(sClientData)
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_GangOpsHeistDebugPad")
	
	ENDIF
ENDPROC
#ENDIF

PROC PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, BOOL &bCanSkip)
//	IF sClientData.sCameraData.ePositionId != GOCAM_MAIN
//	AND sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
//		EXIT
//	ENDIF
	
	GANG_OPS_MISSION_ENUM eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandId)
	
	IF NOT CAN_PLAYER_PURCHASE_MISSION_EQUIPMENT(eMission #IF IS_DEBUG_BUILD, FALSE #ENDIF)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but is unable to purchase the equipment")
		EXIT
	ENDIF
	
	bCanSkip = TRUE
	
	INT iCashNeeded
	IF NETWORK_CAN_SPEND_MONEY2(GET_GANG_OPS_FREEMODE_MISSION_EQUIPMENT_COST(), FALSE, TRUE, FALSE, iCashNeeded)
		IF sClientData.sStrandData.iWarningScreen = -1
			PLAY_SOUND_FROM_COORD(-1, "Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			sClientData.sStrandData.iWarningScreen = GANG_OPS_WARNING_SCREEN_PURCHASE_EQUIPMENT
			PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, FALSE)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, throw a warning screen and wait for response")
		ENDIF
	ELSE
		IF sClientData.sStrandData.iWarningScreen = -1
			PLAY_SOUND_FROM_COORD(-1, "Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			sClientData.sStrandData.iWarningScreen = GANG_OPS_WARNING_SCREEN_NOT_ENOUGH_CASH_FOR_EQUIPMENT
			PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, FALSE)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT - Player has opted to pay for gang ops equipment, but doesnt have enough money to pay for it")
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_INPUTS(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	IF sClientData.sStrandData.iWarningScreen != -1
	OR sClientData.sStrandData.iWarningScreenBuffer != -1
	OR sClientData.sTutorialData.bTutorialInProgress
		IF HAS_NET_TIMER_STARTED(sClientData.sCursorData.stInputTimer)
			REINIT_NET_TIMER(sClientData.sCursorData.stInputTimer)
		ENDIF
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
		EXIT
	ENDIF
	
	IF !sClientData.sStateData.bInteract
		EXIT
	ENDIF
	
	GANG_OPS_CONTROL_TYPE eControlType = PUBLIC_GET_GANG_OPS_INPUTS_THIS_FRAME_ONLY(sClientData.sInputData)
	
	BOOL bBlockDirectionalInputs
	
	IF HAS_NET_TIMER_STARTED(sClientData.sCursorData.stInputTimer)
		IF HAS_NET_TIMER_EXPIRED(sClientData.sCursorData.stInputTimer, 1000)
			RESET_NET_TIMER(sClientData.sCursorData.stInputTimer)
		ENDIF
		EXIT
	ENDIF
	
	SWITCH eControlType
		CASE GOCT_UP
		CASE GOCT_DOWN
		CASE GOCT_LEFT
		CASE GOCT_RIGHT
		
			PRIVATE_UPDATE_GANG_OPS_PLANNING_CONTROL_SELECTIONS(sClientData, eControlType)
			PRIVATE_SET_GANG_OPS_PLANNING_HIGHLIGHT_UPDATE_REQUIRED(sClientData, TRUE)
	
			BREAK
			
		// Camera Pan
		CASE GOCT_LB
		CASE GOCT_RB
		CASE GOCT_Y
		
			PRIVATE_UPDATE_CAMERA_AND_FOCUS_FOR_INPUT(sClientData, eControlType)
		
			BREAK
			
		CASE GOCT_B
		
			PRIVATE_TOGGLE_GANG_OPS_INTERACTION(sClientData, FALSE)
			PLAY_SOUND_FROM_COORD(-1, "Back", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			DO_INTERACT_ANIM()
			
			BREAK
			
		CASE GOCT_A
		
			PRIVATE_LAUNCH_GANG_OPS_MISSION(sClientData)

			BREAK
			
		CASE GOCT_X
		
			PRIVATE_PAY_FOR_GANG_OPS_EQUIPMENT(sClientData, bBlockDirectionalInputs)
		
			BREAK
			
			
		#IF IS_DEBUG_BUILD
		CASE GOCT_LT_RT_A
		
			PRIVATE_DEBUG_GANG_OPS_COMPLETE_SELECTION(sClientData)
		
			BREAK
		
		CASE GOCT_LT_RT_X
		
			PRIVATE_DEBUG_GANG_OPS_COMPLETE_PREPS(sClientData)
		
			BREAK
		
		CASE GOCT_LT_RT_Y
		
			PRIVATE_DEBUG_GANG_OPS_COMPLETE_STRAND(sClientData)
		
			BREAK
		
		#ENDIF
	ENDSWITCH
	
	IF bBlockDirectionalInputs
		REINIT_NET_TIMER(sClientData.sCursorData.stInputTimer)
	ENDIF

ENDPROC

PROC START_GANGOPS_PLANNING_MISSION(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_LAUNCH_MISSION)
//	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	
	IF GB_IS_LOCAL_GANGOPS_CALL_BIT_SET(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION)
		GB_CLEAR_LOCAL_GANGOPS_CALL_BIT(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GTA$] START_GANGOPS_PLANNING_MISSION - cleared eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION")
	ENDIF
		
	IF GB_IS_LOCAL_GANGOPS_CALL_BIT_SET(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_CASH_DROP_PREP_MISSION)
		GB_SET_LOCAL_GANGOPS_CALL_BIT(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_CASH_DROP_PREP_MISSION)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GTA$] START_GANGOPS_PLANNING_MISSION - cleared eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_CASH_DROP_PREP_MISSION")
	ENDIF
	
	PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, TRUE)
	sClientData.sStrandData.iWarningScreen = -1
	sClientData.sStrandData.iWarningScreenBuffer = 0
	sClientData.sStrandData.iWarningScreenBS = 0

ENDPROC

PROC SKIP_GANGOPS_PLANNING_MISSION(GANG_OPS_CLIENT_PLANNING_DATA &sClientData, GANG_OPS_MISSION_ENUM eMission)

	IF USE_SERVER_TRANSACTIONS()
		INT iTransaction
		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_GANGOPS_SKIP_MISSION, GET_GANG_OPS_FREEMODE_MISSION_EQUIPMENT_COST(), iTransaction, FALSE, TRUE)
		g_cashTransactionData[iTransaction].cashInfo.iNumCrates = ENUM_TO_INT(eMission)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GTA$] SKIP_GANGOPS_PLANNING_MISSION - Equipment purchased - SERVICE_SPEND_GANGOPS_SKIP_MISSION, $", GET_GANG_OPS_FREEMODE_MISSION_EQUIPMENT_COST())
	ELSE
		NETWORK_SPEND_GANGOPS_SKIP_MISSION(ENUM_TO_INT(eMission), GET_GANG_OPS_FREEMODE_MISSION_EQUIPMENT_COST(), FALSE, TRUE)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GTA$] SKIP_GANGOPS_PLANNING_MISSION - Equipment purchased - NETWORK_SPEND_GANGOPS_SKIP_MISSION, $", GET_GANG_OPS_FREEMODE_MISSION_EQUIPMENT_COST())
	ENDIF
	
	PLAY_SOUND_FROM_COORD(-1, "Pay", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
	
	INCREMENT_MP_INT_CHARACHTER_STAT(1, MP_STAT_GANGOPS_PREP_SKIP)
					
	SET_GANG_OPS_MISSION_COMPLETED(eMission, TRUE, FALSE, TRUE)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	RUN_DOOMSDAY_UDS_MISSION_COMPLETION_CHECKS(eMission)
	#ENDIF
	
	// url:bugstar:5795862 - Daily Objectives - "Complete a Doomsday Heist Prep." did not complete when the player paid for set up.
	SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_COMPLETE_DOOMSDAY_PREP)
	
	// Cash drop gets a unique phonecall
	IF eMission = GANG_OPS_MISSION_FREEMODE_DEAD_DROP
		// Make sure we clear doing the normal call if we chose to skip a different mission before this
		IF GB_IS_LOCAL_GANGOPS_CALL_BIT_SET(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION)
			GB_CLEAR_LOCAL_GANGOPS_CALL_BIT(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION)
		ENDIF
		GB_SET_LOCAL_GANGOPS_CALL_BIT(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_CASH_DROP_PREP_MISSION)
	ELSE
		IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_PREP_SKIP_CALL_DONE)
			IF NOT GB_IS_LOCAL_GANGOPS_CALL_BIT_SET(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_CASH_DROP_PREP_MISSION)
				GB_SET_LOCAL_GANGOPS_CALL_BIT(eGB_LOCAL_GANGOPS_CALL_BITSET_NOTIFICATION_SKIPPED_PREP_MISSION)
			ENDIF
			SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_PREP_SKIP_CALL_DONE)
		ENDIF
	ENDIF
	
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
	PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, TRUE)
	sClientData.sStrandData.iWarningScreen = -1
	sClientData.sStrandData.iWarningScreenBuffer = 0
	sClientData.sStrandData.iWarningScreenBS = 0

ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	IF sClientData.sStrandData.iWarningScreen = -1
		EXIT
	ENDIF
	
	GANG_OPS_MISSION_ENUM eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandId)
	
	BOOL bConfirm
	IF PUBLIC_GANG_OPS_MAINTAIN_WARNING_SCREEN(sClientData.sStrandData.iWarningScreen, bConfirm, eMission, DEFAULT, sClientData.sStrandData.iWarningScreenBS)
		IF bConfirm
			SWITCH sClientData.sStrandData.iWarningScreen
				CASE GANG_OPS_WARNING_SCREEN_PURCHASE_EQUIPMENT
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_PURCHASE_EQUIPMENT - Equipment purchased")
					
					IF NOT IS_ANY_GANGOPS_FLOW_AWARD_IN_PROGRESS()
						SKIP_GANGOPS_PLANNING_MISSION(sClientData, eMission)
					ELSE
						sClientData.sStrandData.iWarningScreen = GANG_OPS_WARNING_SCREEN_SKIP_CHALLENGE
						sClientData.sStrandData.iWarningScreenBuffer = 0
					ENDIF
				BREAK
				
				CASE GANG_OPS_WARNING_SCREEN_START_PREP_MISSION
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_START_PREP_MISSION - Prep mission launched")
					START_GANGOPS_PLANNING_MISSION(sClientData)
				BREAK
				
				CASE GANG_OPS_WARNING_SCREEN_START_INSTANCED_MISSION
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_START_INSTANCED_MISSION - Instanced mission launched")
					START_GANGOPS_PLANNING_MISSION(sClientData)
				BREAK
				
				CASE GANG_OPS_WARNING_SCREEN_ORDER_CHALLENGE
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_ORDER_CHALLENGE - Mission launched")
					START_GANGOPS_PLANNING_MISSION(sClientData)
				BREAK
								
				CASE GANG_OPS_WARNING_SCREEN_SKIP_CHALLENGE
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_SKIP_CHALLENGE - Mission skipped")
					SKIP_GANGOPS_PLANNING_MISSION(sClientData, eMission)
					RESET_ALL_GANGOPS_FLOW_AWARD_PROGRESS()
				BREAK
				
				CASE GANG_OPS_WARNING_SCREEN_TEAM_CHALLENGE
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_TEAM_CHALLENGE - Mission launched")
					START_GANGOPS_PLANNING_MISSION(sClientData)
				BREAK
				
				CASE GANG_OPS_WARNING_SCREEN_NOT_ENOUGH_CASH_FOR_EQUIPMENT
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - GANG_OPS_WARNING_SCREEN_NOT_ENOUGH_CASH - Confirmed")
					PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, TRUE)
					sClientData.sStrandData.iWarningScreen = -1
					sClientData.sStrandData.iWarningScreenBuffer = 0
					sClientData.sStrandData.iWarningScreenBS = 0
				BREAK
			ENDSWITCH
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS - Cancel")
			PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, TRUE)
			sClientData.sStrandData.iWarningScreen = -1
			sClientData.sStrandData.iWarningScreenBuffer = 0
			sClientData.sStrandData.iWarningScreenBS = 0
		ENDIF
	ENDIF
ENDPROC

//FUNC BOOL PRIVATE_REQUEST_GANG_OPS_DESCRIPTION_HASHES(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
//
//	BOOL bAllLoaded = TRUE
//
//	IF NOT sClientData.sStrandData.sDescLoadVars.bSucess
//		REQUEST_AND_LOAD_CACHED_DESCRIPTION(sClientData.sStrandData.iStrandDesc, sClientData.sStrandData.sDescLoadVars)
//		bAllLoaded = FALSE
//	ENDIF
//	
//	INT index
//	FOR index = 0 TO (sClientData.sStrandData.iTotalMissions-1)
//	
//		IF sClientData.sStrandData.sMissionData[index].sMissionDescLoadVars.bSucess
//			RELOOP
//		ENDIF
//	
//		IF (sClientData.sStrandData.sMissionData[index].iMissionDescriptionHash != 0)
//		
//			IF REQUEST_AND_LOAD_CACHED_DESCRIPTION(sClientData.sStrandData.sMissionData[index].iMissionDescriptionHash,
//				sClientData.sStrandData.sMissionData[index].sMissionDescLoadVars)
//				
//				IF NOT sClientData.sStrandData.sMissionData[index].sMissionDescLoadVars.bSucess
//					bAllLoaded = FALSE
//				ENDIF
//				
//			ENDIF
//		
//		ENDIF
//		
//	ENDFOR
//	
//	IF NOT bAllLoaded
//		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Desc Hashes - Still loading hashes")
//	ENDIF
//	
//	RETURN bAllLoaded
//	
//ENDFUNC

PROC PRIVATE_PROCESS_GANG_OPS_DESCRIPTION_HASHES(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	STRING sText
	INT iStartCharacterIndex, iStringLengthInCharacters, iArrayIndex, index
	BOOL bFinishedCopying

	FOR index = 0 TO (sClientData.sStrandData.iTotalMissions-1)

		IF sClientData.sStrandData.sMissionData[index].bResolvedHashes
			RELOOP
		ENDIF

		FOR iArrayIndex = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION-1)
			sClientData.sStrandData.sMissionData[index].tl63MissionDescription[iArrayIndex] = ""
		ENDFOR
		
		iStartCharacterIndex = 0
		iArrayIndex = 0
		bFinishedCopying = FALSE
		
		//Get the cached description
		sText = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(
			sClientData.sStrandData.sMissionData[index].iMissionDescriptionHash, INVITE_MAX_DESCRIPTION_LENGTH)	
			
		iStringLengthInCharacters = GET_LENGTH_OF_LITERAL_STRING(sText)
	
		//Copy this string into chunks using a byte limit
		WHILE NOT bFinishedCopying
			//If we've got to the end of the string then we're done
			IF iStartCharacterIndex >= iStringLengthInCharacters
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Proc Hashes - Finished processing description strings for mission index ", index)
				bFinishedCopying = TRUE
				sClientData.sStrandData.sMissionData[index].bResolvedHashes = TRUE
			ELSE
				//If we're over the array size then we done
				IF iArrayIndex >= FMMC_NUM_LABELS_FOR_DESCRIPTION
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Proc Hashes - Reached maximum labels")
					bFinishedCopying = TRUE
					sClientData.sStrandData.sMissionData[index].bResolvedHashes = TRUE
				ELSE
					//Get the next chunk
					sClientData.sStrandData.sMissionData[index].tl63MissionDescription[iArrayIndex] = 
						GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(
							sText, 
							iStartCharacterIndex, 
							iStringLengthInCharacters, 
							63)
					iStartCharacterIndex += GET_LENGTH_OF_LITERAL_STRING(
						sClientData.sStrandData.sMissionData[index].tl63MissionDescription[iArrayIndex])
					
					//Move to the next array
					iArrayIndex++
				ENDIF
			ENDIF				
		ENDWHILE
	ENDFOR
	
ENDPROC

FUNC BOOL PRIVATE_ARE_GANG_OPS_DESCRIPTION_HASHES_RESOLVED(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	INT index

	FOR index = 0 TO (sClientData.sStrandData.iTotalMissions-1)
		IF NOT sClientData.sStrandData.sMissionData[index].bResolvedHashes
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC

PROC PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	INT index, iMarker
	FLOAT fZoomScale
	VECTOR vMarkerCoords, vZoomCoords
	GANG_OPS_MISSION_ENUM eMission
//	TEXT_LABEL_63 temp

	IF PUBLIC_IS_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData)
		eMission = GET_GANG_OPS_MISSION_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandId)
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD - Setting up map zooming/scaling for ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission))
		
		// Zoom the map if necessary
		
		vZoomCoords = GET_GANG_OPS_MISSION_MAP_ZOOM_COORDS(eMission)
		fZoomScale = GET_GANG_OPS_MISSION_MAP_ZOOM_SCALE(eMission)
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD - Zoom Coords: ", vZoomCoords, "; Zoom scale: ", fZoomScale)
		
		PUBLIC_GANG_OPS_SET_MAP_DISPLAY(sClientData.sScaleformData.siMainBoard,
										CEIL(vZoomCoords.x),
										CEIL(vZoomCoords.y),
										fZoomScale,
										FALSE)
										
		IF NOT ARE_VECTORS_EQUAL(vZoomCoords, sClientData.sCameraData.vLastMapZoomCoords)
			IF sClientData.sAudioData.bStartedLoopingBackgroundSound
				IF sClientData.sAudioData.iLoopingBackgroundSoundId != -1
					SET_VARIABLE_ON_SOUND(sClientData.sAudioData.iLoopingBackgroundSoundId, "x", vZoomCoords.x)
					SET_VARIABLE_ON_SOUND(sClientData.sAudioData.iLoopingBackgroundSoundId, "y", vZoomCoords.y)
					SET_VARIABLE_ON_SOUND(sClientData.sAudioData.iLoopingBackgroundSoundId, "scale", fZoomScale)
				ENDIF
			ENDIF
			sClientData.sCameraData.vLastMapZoomCoords = vZoomCoords
		ENDIF
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD - Setting up markers for ", GET_GANG_OPS_MISSION_NAME_FOR_DEBUG(eMission))
		
		PUBLIC_GANG_OPS_REMOVE_ALL_MAP_MARKERS(sClientData.sScaleformData.siMainBoard)
		
		FOR iMarker = 0 TO (MAX_GANG_OPS_MAP_MARKERS-1)
			vMarkerCoords = GET_GANG_OPS_MISSION_MAP_MARKER_COORDS(eMission, iMarker)
			IF NOT IS_VECTOR_ZERO(vMarkerCoords)
				PUBLIC_GANG_OPS_ADD_MAP_MARKER(sClientData.sScaleformData.siMainBoard, iMarker, vMarkerCoords, iMarker)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD - Adding marker ", iMarker, " at coords: ", vMarkerCoords)
			ENDIF
		ENDFOR
		
		
		PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF

	IF PUBLIC_IS_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData)
		PUBLIC_GANG_OPS_UPDATE_FLAVOUR_IMAGES(	sClientData.sScaleformData.siMainBoard,
												GET_TEXTURE_DICT(sClientData.sStrandData.iStrandID),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_TOP, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_1, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_2, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_3, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_4, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_5, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_1, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_2, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_3, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_BOTTOM, sClientData.sCursorData.iHighlight, sClientData.sStrandData.iStrandID, GANG_OPS_FLAVOUR_IMAGE_TYPE_PLANNING)		)
												
		PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF
	
	IF PUBLIC_IS_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData)
		FOR index = 0 TO (sClientData.sStrandData.iTotalMissions-1)
			
				PUBLIC_GANG_OPS_UPDATE_PLANNING_SLOT(	sClientData.sScaleformData.siMainBoard,
														sClientData.sStrandData.iStrandID,
														index,
														
														sClientData.sStrandData.sMissionData[index].sPrepMission[0].sMissionTitle,
														sClientData.sStrandData.sMissionData[index].sPrepMission[0].sMissionDescription,
														sClientData.sStrandData.sMissionData[index].sPrepMission[0].iMissionState,
														sClientData.sStrandData.sMissionData[index].sPrepMission[0].tlPicture,
														
														sClientData.sStrandData.sMissionData[index].sPrepMission[1].sMissionTitle,
														sClientData.sStrandData.sMissionData[index].sPrepMission[1].sMissionDescription,
														sClientData.sStrandData.sMissionData[index].sPrepMission[1].iMissionState,
														sClientData.sStrandData.sMissionData[index].sPrepMission[1].tlPicture,
														
														sClientData.sStrandData.sMissionData[index].sMissionTitle,
														sClientData.sStrandData.sMissionData[index].sMissionDescription,
														sClientData.sStrandData.sMissionData[index].iMissionState,
														sClientData.sStrandData.sMissionData[index].tlPicture,
														sClientData.sStrandData.sMissionData[index].sPrepMission[0].tlEquipmentAcquired,
														sClientData.sStrandData.sMissionData[index].sPrepMission[1].tlEquipmentAcquired)
			
			
		ENDFOR
		
		PUBLIC_SET_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF

	IF PUBLIC_IS_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData)
		
		STRING sOrgName = GET_DEFAULT_GANG_NAME(sClientData.sStrandData.piLeader)
		IF sClientData.sStrandData.piLeader != INVALID_PLAYER_INDEX()
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(sClientData.sStrandData.piLeader)
				sOrgName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sClientData.sStrandData.piLeader #IF IS_DEBUG_BUILD , TRUE #ENDIF )
			ENDIF
		ENDIF
		
		PUBLIC_GANG_OPS_UPDATE_PLANNING_SCREEN(	sClientData.sScaleformData.siMainBoard, 
												sClientData.sScaleformData.bSkipLoading, 
												sClientData.sStrandData.tlStrandName, 
												sOrgName,
												sClientData.sStrandData.tlStrandDesc)
												
		IF sClientData.sScaleformData.bSkipLoading
			PLAY_SOUND_FROM_COORD(-1, "Draw_Board", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		ELSE
			PLAY_SOUND_FROM_COORD(-1, "Bootup", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		ENDIF
		
		PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF
	
ENDPROC

PROC PRIVATE_UNLOAD_GANG_OPS_PLANNING_SCRIPT_RESOURCES(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)

	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sClientData.sScaleformData.siMainBoard)
		
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Cleanup - Released all script assets and finished cleanup.")

ENDPROC


/* ===================================================================================== *\
			STATE MACHINE - STATE METHODS AND MAIN UPDATE
\* ===================================================================================== */

PROC PRIVATE_GANG_OPS_PLANNING_IDLE_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC



PROC PRIVATE_GANG_OPS_PLANNING_INIT_ENTRY(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	UNUSED_PARAMETER(sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INIT ENTRY")
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_INIT_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_INIT_EXIT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INIT EXIT")
//	PUBLIC_SET_GANG_OPS_HIGHLIGHTER_STATE(sClientData.sCursorData)
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
ENDPROC



PROC PRIVATE_GANG_OPS_PLANNING_LOADING_ENTRY(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - LOADING ENTRY")
	PRIVATE_REQUEST_GANG_OPS_PLANNING_SCALEFORM(sClientData.sScaleformData)
	///PRIVATE_REQUEST_GANG_OPS_DESCRIPTION_HASHES(sClientData)
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_LOADING_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	BOOL bReady = TRUE
	
	IF NOT PUBLIC_HAVE_GANG_OPS_ASSETS_LOADED(sClientData.sScaleformData)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Waiting for assets to load.")
		bReady = FALSE
	ENDIF
		
	IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Waiting for UGC initial data to load.")
		bReady = FALSE
	ENDIF
	
	IF NOT PUBLIC_HAS_GANG_OPS_AUDIO_LOADED()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Waiting for audio assets to load.")
		bReady = FALSE
	ENDIF
	
	IF bReady
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_LOADING_EXIT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - LOADING EXIT")
	PRIVATE_RENDER_GANG_OPS_PLANNING_SCALEFORM(sClientData)
	PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_SET_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1
	GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iHighlight = sClientData.sCursorData.iHighlight
ENDPROC



PROC PRIVATE_GANG_OPS_PLANNING_DISPLAYING_ENTRY(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - DISPLAYING ENTRY")
	UNUSED_PARAMETER(sClientData)
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_GANG_OPS_PLANNING_DISPLAYING_ENTRY - SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)")
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_DISPLAYING_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_NET_DATA(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD(sClientData)
	PRIVATE_RENDER_GANG_OPS_PLANNING_SCALEFORM(sClientData)
	
	IF sClientData.sStateData.bInteract
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_DISPLAYING_EXIT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - DISPLAYING EXIT")
	UNUSED_PARAMETER(sClientData)
ENDPROC



PROC PRIVATE_GANG_OPS_PLANNING_INUSE_ENTRY(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INUSE ENTRY")
	
	PRIVATE_INIT_GANG_OPS_PLANNING_CAMERA(sClientData)
	
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] INUSE ENTRY - Setting camera focus to: GOCAM_MAIN")
	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
	
	IF NOT HAS_PLAYER_COMPLETED_GANG_OPS_TUTORIAL(PLAYER_ID())
		sClientData.sTutorialData.bTutorialInProgress = TRUE
	ENDIF
	
	sClientData.sScaleformData.bDrawInstructions = TRUE
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
	CLEAR_ALL_BIG_MESSAGES()
	PUBLIC_GANG_OPS_SET_CURRENT_SELECTION(sClientData.sScaleformData.siMainBoard, GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1)
	PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, TRUE)
	PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_GANG_OPS_FLASH_ACTIVE_ELEMENT(sClientData.sScaleformData.siMainBoard)
	
	IF sClientData.sStrandData.iStrandId = ciGANGOPS_MISSION_STRAND_MISSILE_SILO
		IF IS_GANG_OPS_MISSION_AVAILABLE(sClientData.sStrandData.piLeader, GANG_OPS_MISSION_FREEMODE_FLIGHT_RECORDER)
			PUBLIC_GANG_OPS_PULSE_SELECTION(sClientData.sScaleformData.siMainBoard, GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_PREP_MISSION_1)
		ENDIF
		IF IS_GANG_OPS_MISSION_AVAILABLE(sClientData.sStrandData.piLeader, GANG_OPS_MISSION_INSTANCED_SPYPLANE)
			PUBLIC_GANG_OPS_PULSE_SELECTION(sClientData.sScaleformData.siMainBoard, GANG_OPS_PLANNING_SELECTION_ID_MISSION_5_FINAL_MISSION)
		ENDIF
	ENDIF

ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_INUSE_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
		PRIVATE_TOGGLE_GANG_OPS_INTERACTION(g_GangOpsPlanning, FALSE)
	ENDIF
	
	PUBLIC_MAINTAIN_EXCLUSIVE_INPUT()
	PRIVATE_UPDATE_GANG_OPS_PLANNING_TUTORIAL(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_MISSION_STATE(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_NET_DATA(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_HIGHLIGHT(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_BOARD(sClientData)
	PRIVATE_RENDER_GANG_OPS_PLANNING_SCALEFORM(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_WARNING_SCREENS(sClientData)
	PRIVATE_UPDATE_GANG_OPS_PLANNING_INPUTS(sClientData)
	PUBLIC_UPDATE_GANG_OPS_CAMERA(sClientData.sCameraData)//, sClientData.sPositionData)
	PUBLIC_GANG_OPS_HIDE_HUD()
//	MAINTAIN_INTERACT_ANIM(sClientData.sInputData)
	sClientData.sStrandData.iWarningScreenBuffer = -1
	
	IF NOT sClientData.sStateData.bInteract
		SET_BIT(sClientData.iPlanningBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		PRIVATE_CLEAR_GANG_OPS_PLANNING_CAMERA(sClientData)
		PUBLIC_REGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_INUSE_EXIT(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INUSE EXIT")
	
	sClientData.sScaleformData.bDrawInstructions = FALSE
	PRIVATE_SET_GANG_OPS_PLANNING_NAVIGATION(sClientData, FALSE)
	
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_CLEANUP_ENTRY(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - CLEANUP ENTRY")
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
	
	PUBLIC_GANG_OPS_DRAW_RECTANGLE(sClientData.sScaleformData)
ENDPROC

PROC PRIVATE_GANG_OPS_PLANNING_CLEANUP_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	IF NOT IS_BIT_SET(sClientData.iPlanningBitSet, GANG_OPS_BITSET_DO_TOGGLE_RENDERPHASES)
		PUBLIC_GANG_OPS_DRAW_RECTANGLE(sClientData.sScaleformData)
		PUBLIC_CLEAN_GANG_OPS_SCALEFORM_CACHE()
		PRIVATE_CLEAR_GANG_OPS_PLANNING_CAMERA(sClientData)
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
ENDPROC
PROC PRIVATE_GANG_OPS_PLANNING_UPDATE(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	SWITCH (sClientData.sStateData.eState)
	
		CASE GOBS_IDLE
		
			PRIVATE_GANG_OPS_PLANNING_IDLE_UPDATE(sClientData)
		
		BREAK
		
		CASE GOBS_INIT
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_INIT_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_PLANNING_INIT_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_INIT_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
			
		BREAK
		
		CASE GOBS_LOADING
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_LOADING_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_PLANNING_LOADING_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_LOADING_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_DISPLAYING
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_DISPLAYING_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_PLANNING_DISPLAYING_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_DISPLAYING_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_INUSE
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_INUSE_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_PLANNING_INUSE_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_INUSE_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_CLEANUP
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_PLANNING_CLEANUP_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_PLANNING_CLEANUP_UPDATE(sClientData)
			
		BREAK
		
		CASE GOBS_FINISHED
			PRIVATE_UNLOAD_GANG_OPS_PLANNING_SCRIPT_RESOURCES(sClientData)
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_GANG_OPS_PLANNING(GANG_OPS_CLIENT_PLANNING_DATA &sClientData)
	
	#IF IS_DEBUG_BUILD
	PRIVATE_GANG_OPS_PLANNING_WIDGET_UPDATE(sClientData)
	#ENDIF
	
	PRIVATE_GANG_OPS_PLANNING_UPDATE(sClientData)
	
ENDPROC


//EOF
