// Philip O'Duffy
USING "net_activity_creator_activities.sch"
USING "net_activity_creator_components.sch"
// Mission Script -----------------------------------------//
SCRIPT(INT iCurrentProperty)	
	iCurrentProperty = iCurrentProperty
	#IF IS_DEBUG_BUILD 
//	CREATE_LUXE_ACT_WIDGETS(serverBD, luxeActState)
	
	
	ACTIVITY_CREATOR_USER_INTERFACE_STRUCT activityCreatorUI
	
	PROCESS_PRE_GAME_ACT_CREATOR_UI()
	
	ACTIVITY_CREATOR_USER_INTERFACE_WIDGETS(activityCreatorUI, iCurrentProperty)
	
	WHILE TRUE
		WAIT(0)
		ACTIVITY_CREATOR_USER_INTERFACE_LOGIC(activityCreatorUI, iCurrentProperty)
	ENDWHILE
	#ENDIF
ENDSCRIPT