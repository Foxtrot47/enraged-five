

USING "net_quick_fix_func.sch"













PROC RESET_ALL_QUICK_FIX_BUYABLE_VEHICLE_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_BUYABLE_VEHICLE_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_3, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_4, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_5, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_6, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_7, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_8, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_9, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_10, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_11, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_12, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_13, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_14, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_15, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_16, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_17, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_18, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_19, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_BS_20, 0, I)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_3, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_4, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_5, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_6, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_7, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_8, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_9, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_10, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_11, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_12, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_13, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_14, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_15, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_16, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_17, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_18, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_19, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_BS_20, 0, I)
	ENDFOR
	

ENDPROC



PROC RESET_ALL_QUICK_FIX_NON_BUYABLE_VEHICLE_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_NON_BUYABLE_VEHICLE_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1

		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_NONBY_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_NONBY_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_NONBY_3, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_NONBY_4, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_NONBY_5, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_RM_NONBY_6, 0, I)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_NONBY_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_NONBY_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_NONBY_3, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_NONBY_4, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_NONBY_5, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_VEH_GV_NONBY_6, 0, I)

	ENDFOR
	

ENDPROC





FUNC BOOL IS_VEHICLE_LARGE(SITE_BUYABLE_VEHICLE aVeh)

	SWITCH aVeh
		CASE BV_A_RHINO
		CASE BV_A_TITAN
		CASE BV_A_CARGOBOB
		CASE BV_A_BUZZARD
		CASE BV_A_CRUSADER
		CASE BV_A_BARRACKS
		
				//Planes
		CASE BV_P_DODO
		CASE BV_P_SHAMAL
		CASE BV_P_STUNT
		CASE BV_P_CUBAN
		CASE BV_P_DUSTER
		CASE BV_P_LUXOR
		CASE BV_P_FROGGER
		CASE BV_P_MAVERICK
	
		//Boat
		CASE BV_B_MARINA
		CASE BV_B_MARQUIS
		CASE BV_B_JETMAX
		CASE BV_B_SPEEDER
		CASE BV_B_SQUALO
		CASE BV_B_SUNDECK
		CASE BV_B_TROPIC
		CASE BV_B_SEASHARK
		CASE BV_B_SUBMERSIBLE
		CASE BV_B_SUNTRAP
	
		//Elista
		CASE BV_MP_ANNIHILATOR
		CASE BV_MP_MAMMATUS
		CASE BV_MP_VELUM
		
		//Warstock
		CASE BV_MP_DUMP
		CASE BV_MP_AIRBUS
		CASE BV_MP_BUS
		CASE BV_MP_COACH
		CASE BV_MP_JOURNEY
		CASE BV_MP_MULE
		CASE BV_MP_RENTALBUS
		CASE BV_MP_STRETCH
		
		//DLC
		CASE BV_DLC_SPEEDER
		CASE BV_EX_CARGOBOB2
		CASE BV_EX_BRICKADE
			RETURN TRUE
		BREAK
 
	ENDSWITCH
	
	IF IS_BUYABLE_VEHICLE_SUITABLE_FOR_GARAGE(aVeh) = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC







PROC SET_VEHICLE_QUICKFIX_DEFAULT_COLOUR(SITE_BUYABLE_VEHICLE AVeh)

	SWITCH AVeh
		CASE BV_BI_SHOTARO
			g_eLastBuyableVehicleColourSelected = BCV_GREEN_COLOUR
		BREAK
	ENDSWITCH
	
ENDPROC





FUNC BOOL RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE(SITE_BUYABLE_VEHICLE AVeh, INT iCharacterSlot)


   	PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE: - called. ")

	SET_VEHICLE_QUICKFIX_DEFAULT_COLOUR(AVeh)
	
	VECTOR vtemp
    VEHICLE_INDEX vi
	INT iSlotToUse = -1
	INT iDisplaySlotToUse = -1
	
	VEHICLE_SETUP_STRUCT_MP VehSetupStruct
	
	VehSetupStruct.VehicleSetup.eModel = GET_MODEL_FOR_BUYABLE_VEHICLE(AVeh)

    REQUEST_MODEL(VehSetupStruct.VehicleSetup.eModel)
	
	IF HAS_MODEL_LOADED(VehSetupStruct.VehicleSetup.eModel)

		vtemp = GET_PLAYER_COORDS(GET_PLAYER_INDEX())
	    vi = CREATE_VEHICLE(VehSetupStruct.VehicleSetup.eModel,<<vtemp.x,vtemp.y,vtemp.z + 200.0>>,0.0,FALSE,FALSE) // Doesn't need to be a network object
	   
		GET_VEHICLE_SETUP_MP(vi, VehSetupStruct)
				
		CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(AVeh, VehSetupStruct, TRUE)

		// Apply setup to vehicle
		SET_VEHICLE_SETUP_MP( vi, VehSetupStruct )
		
		iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT_FROM_STATS(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,VehSetupStruct.VehicleSetup.eModel,0, iCharacterSlot)
		iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,VehSetupStruct.VehicleSetup.eModel,0,iCharacterSlot,TRUE)
		IF g_sMPTunables.bDisableDisplaySlots
			iDisplaySlotToUse = iSlotToUse 
			PRINTLN("g_sMPTunables.bDisableDisplaySlots setting iDisplaySlotToUse = iSlotToUse ")
		ENDIF
		
		IF iSlotToUse > -1
		AND iDisplaySlotToUse > -1
		
			STAT_SET_CURRENT_POSIX_TIME(GET_ADMIN_VEH_POSIX_ENUM(iCharacterSlot))

			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_BUY_INSURANCE, TRUE,iCharacterSlot)
			MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT_QUICK_FIX(vi,QuickFixSavedVehicles,iSlotToUse,TRUE,TRUE,FALSE,TRUE,TRUE, iCharacterSlot)
			MPSV_SET_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse,iCharacterSlot)
			
			IF GET_ACTIVE_CHARACTER_SLOT() = (iCharacterSlot)
//		        IF NOT IS_VEHICLE_A_CYCLE(VehSetupStruct.VehicleSetup.eModel)
//		            INSURE_MP_SAVED_VEHICLE_PRIVATE(iSlotToUse)
//		        ENDIF
//			    QuickFixSavedVehicles[iSlotToUse].vehicleSetupMP.VehicleSetup.iColour1 = VehSetupStruct.vehicleSetup.iColour1 
//		        SET_VEHICLE_AS_JUST_PURCHASED(iSlotToUse)
				SET_DO_END_OF_TRANSITION_SAVE(TRUE)
				PLAYSTATS_QUICKFIX_TOOL(QF_VEHICLE_GIVE, GET_LABEL_BUYABLE_VEHICLE(AVeh))
				ADD_QUICKFIXFEED_ELEMENT(QF_VEHICLE_GIVE,"QFE_VEHICLE_GV", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_MODEL_FOR_BUYABLE_VEHICLE(AVeh)), iCharacterSlot)
      	  		PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE: Placing vehicle in Garage. ")
			ELSE
				PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE: Not Placing vehicle in Garage as giving not to the active character. ")
			ENDIF

	  	ELSE
      	  PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE: Couldn't find a free slot to save vehicle")
		ENDIF

		IF DOES_ENTITY_EXIST(vi)
			DELETE_VEHICLE(vi)
		ENDIF

		RETURN TRUE	
	ELSE
       PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE: HAS_MODEL_LOADED = FALSE for ",GET_LABEL_BUYABLE_VEHICLE(AVeh))
	ENDIF		
			
	RETURN FALSE

ENDFUNC



PROC RUN_QUICK_FIX_GIVE_LARGE_VEHICLES(SITE_BUYABLE_VEHICLE AVeh, INT iSlot)


	STAT_SET_CURRENT_POSIX_TIME(GET_ADMIN_VEH_POSIX_ENUM(iSlot))
	SET_DO_END_OF_TRANSITION_SAVE(TRUE)
	
	//Do other slot
	IF GET_ACTIVE_CHARACTER_SLOT() = iSlot
		
		SET_BIG_ASS_VEHICLE_IN_STATS_AND_BS_ADMIN(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(AVeh), iSlot)
		SET_TANK_VEHICLE_HAS_CHANGED_ADMIN(TRUE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MPBAV_TIME, 0, iSlot)

		PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_LARGE_VEHICLES: Giving player Vehicle for Active slot ", iSlot)
		
		
		SET_DO_END_OF_TRANSITION_SAVE(TRUE)
		
	ELSE

		SET_BIG_ASS_VEHICLE_IN_STATS_AND_BS_ADMIN(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(AVeh), iSlot)
		SET_TANK_VEHICLE_HAS_CHANGED_ADMIN(TRUE)
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MPBAV_TIME, GET_CLOUD_TIME_AS_INT(), iSlot)
		#IF IS_DEBUG_BUILD
		PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_LARGE_VEHICLES: Giving player Vehicle for Non Active slot  ", iSlot)
		#ENDIF
		SET_DO_END_OF_TRANSITION_SAVE(TRUE)
		
		
	ENDIF	

	PLAYSTATS_QUICKFIX_TOOL(QF_VEHICLE_GIVE, GET_LABEL_BUYABLE_VEHICLE(AVeh))

	ADD_QUICKFIXFEED_ELEMENT(QF_VEHICLE_GIVE,"QFE_VEHICLE_GV", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_MODEL_FOR_BUYABLE_VEHICLE(AVeh)),iSlot )
	


ENDPROC

PROC QUICK_FIX_CLEAR_VEHICLE_DATA()
	INT j
	PRINTLN("QUICK_FIX_CLEAR_VEHICLE_DATA - called - setting QuickFixVehicleSplitStages = 0")
	REPEAT MAX_MP_SAVED_VEHICLES j
		RESET_MP_SAVED_VEHICLE_STRUCT(QuickFixSavedVehicles[j])	
	ENDREPEAT
	QuickFixVehicleSplitStages = 0
	
//	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_VEHICLE_TIMESTAMP) > 0
//		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarsModifiedTimeStamp = 0
//		PRINTLN("QUICK_FIX_CLEAR_VEHICLE_DATA - Run") 
//	ENDIF
ENDPROC

FUNC BOOL QUICK_FIX_IS_PLAYER_VEHICLE_DATA_OLD(INT iStatTimeStamp, INT iCharacterSlot)
	
	BOOL FoundAVehicle = FALSE
	INT I
	FOR I = 0 TO MAX_MP_SAVED_VEHICLES-1
		IF GET_MP_INT_CHARACTER_STAT(GET_INT_STAT_FOR_VEHICLE_SLOT(MPSV_STAT_ELEMENT_MODEL, I),iCharacterSlot)  > 0
			FoundAVehicle = TRUE
			I = MAX_MP_SAVED_VEHICLES
		ENDIF	
	ENDFOR
	IF GET_ACTIVE_CHARACTER_SLOT() = iCharacterSlot
		FOR I = 0 TO MAX_MP_SAVED_VEHICLES-1
			IF g_MpSavedVehicles[i].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
				FoundAVehicle = TRUE
				I = MAX_MP_SAVED_VEHICLES
			ENDIF	
		ENDFOR
	ENDIF
		
	IF iStatTimeStamp = 0
	AND FoundAVehicle = TRUE
		PRINTLN("[QUICKFIX] CHARACTER DATA IS OLD!! ")
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC


FUNC BOOL QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS(INT iCharacterSlot, BOOL &bAbortInvalidData)
	
	PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS Filling struct with data. iCharacterSlot: ",iCharacterSlot)
	INT iNumSplits = 19 
	INT iSplit = 20//MAX_MP_SAVED_VEHICLES/iSplit = 18.15
	INT iLimit
	INT i
	INT iSaveTimeStamp = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCarsModifiedTimeStamp
	INT iStatTimeStamp = GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_VEHICLE_TIMESTAMP,iCharacterSlot)

	PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS: iSaveTimeStamp = ", iSaveTimeStamp)
	PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS: iStatTimeStamp = ", iStatTimeStamp)


	//profile stats have not been setup yet
	IF iStatTimeStamp = 0
		IF QUICK_FIX_IS_PLAYER_VEHICLE_DATA_OLD(iStatTimeStamp, iCharacterSlot) = FALSE
			IF GET_ACTIVE_CHARACTER_SLOT() = iCharacterSlot
				PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS: Car details: profile stats have never been filled no data available")
			ELSE
				bAbortInvalidData = TRUE
				QuickFixVehicleSplitStages = 0
				RETURN TRUE
			ENDIF
		ELIF GET_ACTIVE_CHARACTER_SLOT() = iCharacterSlot
			REPEAT MAX_MP_SAVED_VEHICLES i
				QuickFixSavedVehicles[i] = g_MpSavedVehicles[i]
			ENDREPEAT
			PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS: assigned quickfix struct to be equal to save game")
		ELSE
			bAbortInvalidData = TRUE
			QuickFixVehicleSplitStages = 0
			RETURN TRUE
		ENDIF
	ELSE
	
		//IF iSaveTimeStamp != iStatTimeStamp
			//IF iStatTimeStamp > iSaveTimeStamp
				PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS: Car details: profile stats have data replacing save game data. QuickFixVehicleSplitStages = ", QuickFixVehicleSplitStages)
				//profile stats are newer replace the save game data

				
				SWITCH QuickFixVehicleSplitStages
					CASE 0
					CASE 1
					CASE 2
					CASE 3
					CASE 4
					CASE 5
					CASE 6
					CASE 7
					CASE 8
					CASE 9
					CASE 10
					CASE 11
					CASE 12
					CASE 13
					CASE 14
					CASE 15
					CASE 16
					CASE 17
					CASE 18
						iLimit = iSplit*(QuickFixVehicleSplitStages+1)
						IF iLimit >= MAX_MP_SAVED_VEHICLES
							iLimit = MAX_MP_SAVED_VEHICLES-1
						ENDIF
						FOR i = iSplit*QuickFixVehicleSplitStages TO iLimit
							IF NOT MP_SAVE_VEHICLE_SLOT_SAVEGAME_FROM_STATS(i,QuickFixSavedVehicles[i], iCharacterSlot,FALSE,TRUE)
								RETURN FALSE
							ENDIF
						ENDFOR
						QuickFixVehicleSplitStages++
						IF QuickFixVehicleSplitStages = iNumSplits
							RETURN TRUE
						ENDIF
						RETURN FALSE
					BREAK
//					CASE 1
//						FOR i = Sixth TO Sixth*2
//							IF NOT MP_SAVE_VEHICLE_SLOT_SAVEGAME_FROM_STATS(i,QuickFixSavedVehicles[i], iCharacterSlot)
//								RETURN FALSE
//							ENDIF
//						ENDFOR
//						QuickFixVehicleSplitStages++
//						RETURN FALSE
//					BREAK
//					CASE 2
//						FOR i = Sixth*2 TO Sixth
//							IF NOT MP_SAVE_VEHICLE_SLOT_SAVEGAME_FROM_STATS(i,QuickFixSavedVehicles[i], iCharacterSlot)
//								RETURN FALSE
//							ENDIF
//						ENDFOR
//						QuickFixVehicleSplitStages = 0
//						RETURN TRUE
//					BREAK
				ENDSWITCH

				
			//ENDIF

		//ENDIF
	ENDIF

	PRINTLN("[QUICKFIX] QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS = TRUE ")
	QuickFixVehicleSplitStages = 0
RETURN TRUE
ENDFUNC

PROC RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE(SITE_BUYABLE_VEHICLE AVeh, INT iCharacterSlot)


	MODEL_NAMES aModel = GET_MODEL_FOR_BUYABLE_VEHICLE(AVeh)
	INT iVehicleSlot
	
	
	
	REPEAT MAX_MP_SAVED_VEHICLES iVehicleSlot
		IF aModel = QuickFixSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.eModel
			IF NOT IS_BIT_SET(QuickFixSavedVehicles[iVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
			OR (IS_BIT_SET(QuickFixSavedVehicles[iVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
	        AND IS_BIT_SET(QuickFixSavedVehicles[iVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_INSURED))
	           
				STAT_SET_CURRENT_POSIX_TIME(GET_ADMIN_VEH_POSIX_ENUM(iCharacterSlot))

				CLEAR_MP_SAVED_VEHICLE_SLOT(iVehicleSlot, iCharacterSlot)
				#IF IS_DEBUG_BUILD
				IF iCharacterSlot = GET_ACTIVE_CHARACTER_SLOT()
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: Removing player Vehicle for Active slot ", iCharacterSlot)
				ELSE
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: Removing player Vehicle for Non Active slot ", iCharacterSlot)
				ENDIF
				#ENDIF
				SET_DO_END_OF_TRANSITION_SAVE(TRUE)
				PLAYSTATS_QUICKFIX_TOOL(QF_VEHICLE_REMOVE, GET_LABEL_BUYABLE_VEHICLE(AVeh))
				ADD_QUICKFIXFEED_ELEMENT(QF_VEHICLE_REMOVE,"QFE_VEHICLE_RM", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_MODEL_FOR_BUYABLE_VEHICLE(AVeh)), iCharacterSlot)

				//iVehicleSlot = MAX_MP_SAVED_VEHICLES //Put this back in if you only want to delete the first instance. 
			ELSE
				IF iCharacterSlot = GET_ACTIVE_CHARACTER_SLOT()
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: Trying to Remove player Vehicle for Active slot ", iCharacterSlot, " but the vehicle isn't setup right or has already been lost. ")
				ELSE
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: Trying to Remove player Vehicle for Non Active slot ", iCharacterSlot, " but the vehicle isn't setup right or has already been lost. ")
				ENDIF
	        ENDIF
		ELSE
		
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: aModel = ", aModel)
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: iVehicleSlot = ", iVehicleSlot)
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE: MpSavedVehicles = ", QuickFixSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.eModel)
	    ENDIF
	ENDREPEAT

	


ENDPROC



PROC RUN_QUICK_FIX_REMOVE_LARGE_VEHICLES_BUYABLE(SITE_BUYABLE_VEHICLE AVeh, INT iSlot)

		IF GET_ACTIVE_CHARACTER_SLOT() = iSlot
				
			CLEAR_BIG_ASS_VEHICLE_IN_STATS_AND_BS_ADMIN(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(AVeh), iSlot)
			SET_TANK_VEHICLE_HAS_CHANGED_ADMIN(TRUE)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MPBAV_TIME, 0, iSlot)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_LARGE_VEHICLES_BUYABLE: Removing player Vehicle for Active slot ", iSlot)
			#ENDIF
			SET_DO_END_OF_TRANSITION_SAVE(TRUE)
				
							
		ELSE
		
			CLEAR_BIG_ASS_VEHICLE_IN_STATS_AND_BS_ADMIN(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(AVeh), iSlot)
			SET_TANK_VEHICLE_HAS_CHANGED_ADMIN(TRUE)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PEGASUS_VEHICLE_COLLECTED, FALSE)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MPBAV_TIME, GET_CLOUD_TIME_AS_INT(), iSlot)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_LARGE_VEHICLES_BUYABLE: Remove player Vehicle for Non Active slot  ", iSlot)
			#ENDIF
			SET_DO_END_OF_TRANSITION_SAVE(TRUE)
			
		ENDIF
	
		PLAYSTATS_QUICKFIX_TOOL(QF_VEHICLE_REMOVE, GET_LABEL_BUYABLE_VEHICLE(AVeh))
		ADD_QUICKFIXFEED_ELEMENT(QF_VEHICLE_REMOVE,"QFE_VEHICLE_RM", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_MODEL_FOR_BUYABLE_VEHICLE(AVeh)), iSlot)

ENDPROC



FUNC BOOL SHOULD_GIVE_COLLECTORS_EDITION_VEHICLE(SITE_BUYABLE_VEHICLE WhichVehicle)
	
	IF WhichVehicle = BV_CE_KHAMELION
	OR WhichVehicle = BV_CE_HOTKNIFE
	OR WhichVehicle = BV_CE_CARBONRS
		IF IS_COLLECTORS_EDITION_GAME() = FALSE
			
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[QUICKFIX] SHOULD_GIVE_COLLECTORS_EDITION_VEHICLE = FALSE running check for vehicle ")
			NET_PRINT(GET_LABEL_BUYABLE_VEHICLE(WhichVehicle))NET_NL()
			#ENDIF

			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC




FUNC BOOL RUN_QUICK_FIX_CHECK_GIVE_VEHICLES_BUYABLE(INT CharacterSlot, INT& Stage, INT& VehicleNum)

	INT iSET_VEH_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a vehicle. 
	
	SITE_BUYABLE_VEHICLE WhichVehicle = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, VehicleNum)
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = VehicleNum
			WHILE I < ENUM_TO_INT(TOTAL_BUYABLE_VEHICLES)
				VehicleNum = I
				iSET_VEH_CHANGE_VALUE_ADMIN = 0
				WhichVehicle = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, VehicleNum)
			
				WhichStat = GET_ADMIN_VEH_GV_BS_BITSET_ENUM(WhichVehicle, CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_VEH_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = VehicleNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						NET_NL()NET_PRINT("[QUICKFIX] Check Give Vehicle ")NET_PRINT_INT(VehicleNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_LABEL_BUYABLE_VEHICLE(WhichVehicle))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_VEH_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_VEH_GV_BS_BITSET_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					
					IF iSET_VEH_CHANGE_VALUE_ADMIN = -1
						NET_NL()NET_PRINT("[QUICKFIX] iSET_VEH_CHANGE_VALUE_ADMIN = -1 !!! CHEATER !!! ")
					
					ELSE
			
						
						IF IS_BIT_SET(iSET_VEH_CHANGE_VALUE_ADMIN, (VehicleNum%32))
						AND SHOULD_GIVE_COLLECTORS_EDITION_VEHICLE(WhichVehicle) = TRUE
							
						
						
							VERIFY_MP_PROPERTY_STAT(CharacterSlot)
						
							#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Give vehicle ")NET_PRINT_INT(VehicleNum)
							NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_LABEL_BUYABLE_VEHICLE(WhichVehicle))
							NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
							#ENDIF
						
							IF IS_VEHICLE_LARGE(WhichVehicle)
								RUN_QUICK_FIX_GIVE_LARGE_VEHICLES(WhichVehicle, CharacterSlot)
							ELSE
								Stage = 1
								I = ENUM_TO_INT(TOTAL_BUYABLE_VEHICLES)
							ENDIF

						ENDIF
					ENDIF
					
				ELSE
					Stage = 2
					I = ENUM_TO_INT(TOTAL_BUYABLE_VEHICLES)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				VehicleNum = 0
				Stage = 2
			ENDIF

		BREAK
		
		CASE 1
			WhichVehicle = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, VehicleNum)
			
			IF RUN_QUICK_FIX_GIVE_VEHICLES_BUYABLE(WhichVehicle, CharacterSlot)
				Stage = 0
				VehicleNum++
			ENDIF
		
		BREAK
	
	
		
		CASE 2
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVE_VEHICLES_BUYABLE finished checking vehicles for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			VehicleNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC


FUNC BOOL RUN_QUICK_FIX_CHECK_REMOVING_VEHICLES_BUYABLE(INT CharacterSlot, INT& Stage, INT& VehicleNum)



INT iSET_VEH_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a vehicle. 
	
	SITE_BUYABLE_VEHICLE WhichVehicle = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, VehicleNum)
	
	SWITCH Stage
	
		CASE 0
			
			
			I = VehicleNum
			WHILE I < ENUM_TO_INT(TOTAL_BUYABLE_VEHICLES)
				VehicleNum = I
				iSET_VEH_CHANGE_VALUE_ADMIN = 0
				WhichVehicle = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, VehicleNum)
			
				WhichStat = GET_ADMIN_VEH_RM_BS_BITSET_ENUM(WhichVehicle, CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_VEH_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = VehicleNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						NET_NL()NET_PRINT("[QUICKFIX] Check Remove Vehicle ")NET_PRINT_INT(VehicleNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_LABEL_BUYABLE_VEHICLE(WhichVehicle))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_VEH_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_VEH_RM_BS_BITSET_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					IF IS_BIT_SET(iSET_VEH_CHANGE_VALUE_ADMIN, (VehicleNum%32))
					
						VERIFY_MP_PROPERTY_STAT(CharacterSlot)
					
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[QUICKFIX] Bit is set for REMOVE vehicle ")NET_PRINT_INT(VehicleNum)
						NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_LABEL_BUYABLE_VEHICLE(WhichVehicle))
						NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
						#ENDIF
					
						IF IS_VEHICLE_LARGE(WhichVehicle)
							RUN_QUICK_FIX_REMOVE_LARGE_VEHICLES_BUYABLE(WhichVehicle, CharacterSlot)
						ELSE
							Stage = 1
							I = ENUM_TO_INT(TOTAL_BUYABLE_VEHICLES)
						ENDIF

					ENDIF
					
				ELSE
					Stage = 2
					I = ENUM_TO_INT(TOTAL_BUYABLE_VEHICLES)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				VehicleNum = 0
				Stage = 2
			ENDIF

		BREAK
		
		CASE 1
			WhichVehicle = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, VehicleNum)

			RUN_QUICK_FIX_REMOVE_VEHICLES_BUYABLE(WhichVehicle, CharacterSlot)
			Stage = 0
			VehicleNum++
			
		
		BREAK
	
	
		
		CASE 2
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_REMOVING_VEHICLES_BUYABLE finished checking vehicles for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			VehicleNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE



ENDFUNC






FUNC BOOL RUN_QUICK_FIX_BUYABLE_VEHICLES(INT& Stages, INT& CheckStage, INT& VehicleNum, INT& CharacterSlot)

//	STATSENUM WhichStatPosix
	BOOL bAbortQuickFix
	SWITCH Stages
	
		CASE 0
			IF QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS(CharacterSlot,bAbortQuickFix)
				IF bAbortQuickFix
					PRINTLN("RUN_QUICK_FIX_BUYABLE_VEHICLES: invalid data-1 character #",CharacterSlot)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages = 2
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
					ENDIF
				ELSE
					NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_BUYABLE_VEHICLES Moving to case 1 ")
					Stages++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_CHECK_REMOVING_VEHICLES_BUYABLE(CharacterSlot, CheckStage, VehicleNum)
					CharacterSlot++
					
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages++
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						Stages--
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					CheckStage = 0
					VehicleNum = 0
					CharacterSlot = 0
					Stages++
				ELSE
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					Stages--
				ENDIF
			ENDIF
			
			
		BREAK
		
		CASE 2
			IF QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS(CharacterSlot,bAbortQuickFix)
				IF bAbortQuickFix
					PRINTLN("RUN_QUICK_FIX_BUYABLE_VEHICLES: invalid data-2 character #",CharacterSlot)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages = 4
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
					ENDIF
				ELSE
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3

			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_CHECK_GIVE_VEHICLES_BUYABLE(CharacterSlot, CheckStage, VehicleNum)
					CharacterSlot++
					
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						
						NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_BUYABLE_VEHICLES Moving to case 2 active char ")

						Stages++
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_BUYABLE_VEHICLES Moving back to case 1 active char ")
						
						Stages--
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from giving checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS	
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					CheckStage = 0
					VehicleNum = 0
					CharacterSlot = 0
					
					NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_BUYABLE_VEHICLES Moving to case 2 non active char ")
					
					Stages++
				ELSE
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_BUYABLE_VEHICLES Moving back to case 1, non active char ")
					Stages--
				ENDIF
			ENDIF
			
		BREAK
		
		
		
		CASE 4
			QUICK_FIX_CLEAR_VEHICLE_DATA()
			RESET_ALL_QUICK_FIX_BUYABLE_VEHICLE_STATS()
			Stages = 0
			RETURN TRUE
		BREAK
		
		
	
		
	ENDSWITCH

	

	RETURN FALSE
	

ENDFUNC




































PROC RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE(QUICK_FIX_NON_BUYABLE_VEHICLES AVeh, INT iCharacterSlot)


	MODEL_NAMES aModel = GET_MODEL_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(AVeh)
	INT iVehicleSlot
	
	
	
	REPEAT MAX_MP_SAVED_VEHICLES iVehicleSlot
		IF aModel = QuickFixSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.eModel
			IF NOT IS_BIT_SET(QuickFixSavedVehicles[iVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
			OR (IS_BIT_SET(QuickFixSavedVehicles[iVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
	        AND IS_BIT_SET(QuickFixSavedVehicles[iVehicleSlot].iVehicleBS,MP_SAVED_VEHICLE_INSURED))
	           
				STAT_SET_CURRENT_POSIX_TIME(GET_ADMIN_VEH_RM_NONBY_POSIX_ENUM(iCharacterSlot))

				CLEAR_MP_SAVED_VEHICLE_SLOT(iVehicleSlot, iCharacterSlot)
				#IF IS_DEBUG_BUILD
				IF iCharacterSlot = GET_ACTIVE_CHARACTER_SLOT()
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: Removing player Vehicle for Active slot ", iCharacterSlot)
				ELSE
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: Removing player Vehicle for Non Active slot ", iCharacterSlot)
				ENDIF
				#ENDIF

				PLAYSTATS_QUICKFIX_TOOL(QF_VEHICLE_REMOVE, GET_STRING_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(AVeh))
				ADD_QUICKFIXFEED_ELEMENT(QF_VEHICLE_REMOVE, "QFE_VEHICLE_RM",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL((aModel)), iCharacterSlot)


				SET_DO_END_OF_TRANSITION_SAVE(TRUE)
				iVehicleSlot = MAX_MP_SAVED_VEHICLES
			ELSE
				IF iCharacterSlot = GET_ACTIVE_CHARACTER_SLOT()
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: Trying to Remove player Vehicle for Active slot ", iCharacterSlot, " but the vehicle isn't setup right or has already been lost. ")
				ELSE
					PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: Trying to Remove player Vehicle for Non Active slot ", iCharacterSlot, " but the vehicle isn't setup right or has already been lost. ")
				ENDIF
	        ENDIF
		ELSE
		
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: aModel = ", aModel)
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: iVehicleSlot = ", iVehicleSlot)
			PRINTLN("[QUICKFIX] RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE: MpSavedVehicles = ", QuickFixSavedVehicles[iVehicleSlot].vehicleSetupMP.VehicleSetup.eModel)
	    ENDIF
	ENDREPEAT

	


ENDPROC









FUNC BOOL RUN_QUICK_FIX_CHECK_REMOVING_VEHICLES_NON_BUYABLE(INT CharacterSlot, INT& Stage, INT& VehicleNum)



INT iSET_VEH_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a vehicle. 
	
	QUICK_FIX_NON_BUYABLE_VEHICLES WhichVehicle = INT_TO_ENUM(QUICK_FIX_NON_BUYABLE_VEHICLES, VehicleNum)
	
	SWITCH Stage
	
		CASE 0
			
			
			I = VehicleNum
			WHILE I < ENUM_TO_INT(QF_NONBUY_VEH_TOTAL_NUMBER)
				VehicleNum = I
				iSET_VEH_CHANGE_VALUE_ADMIN = 0
				WhichVehicle = INT_TO_ENUM(QUICK_FIX_NON_BUYABLE_VEHICLES, VehicleNum)
			
				WhichStat = GET_ADMIN_VEH_RM_NONBY_ENUM(WhichVehicle, CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_VEH_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
					ModNum = VehicleNum%32
					BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
					NET_NL()NET_PRINT("[QUICKFIX] NON BUYABLE Remove  ")NET_PRINT_INT(VehicleNum)
					NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
					NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_STRING_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(WhichVehicle))
					NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_VEH_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_VEH_RM_NONBY_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
			
					IF IS_BIT_SET(iSET_VEH_CHANGE_VALUE_ADMIN, (VehicleNum%32))
					
						VERIFY_MP_PROPERTY_STAT(CharacterSlot)
						
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[QUICKFIX] NON BUYABLE Bit is set for REMOVE vehicle ")NET_PRINT_INT(VehicleNum)
						NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_STRING_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(WhichVehicle))
						NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
						#ENDIF
						
						Stage = 1
						I = ENUM_TO_INT(QF_NONBUY_VEH_TOTAL_NUMBER)
						

					ENDIF
					
				ELSE
					Stage = 2
					I = ENUM_TO_INT(QF_NONBUY_VEH_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				VehicleNum = 0
				Stage = 2
			ENDIF

		BREAK
		
		CASE 1
			WhichVehicle = INT_TO_ENUM(QUICK_FIX_NON_BUYABLE_VEHICLES, VehicleNum)

			RUN_QUICK_FIX_REMOVE_VEHICLES_NON_BUYABLE(WhichVehicle, CharacterSlot)
			Stage = 0
			VehicleNum++
			
		
		BREAK
	
	
		
		CASE 2
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_REMOVING_VEHICLES_NON_BUYABLE finished checking vehicles for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			VehicleNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE



ENDFUNC





FUNC BOOL RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE(QUICK_FIX_NON_BUYABLE_VEHICLES AVehBV, INT iCharacterSlot)


   	PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE: - called. ")

//	VEHICLE_SETUP_STRUCT_MP VehSetupStruct 

//	CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(AVeh, VehSetupStruct.VehicleSetup, TRUE)
	
	MODEL_NAMES AVeh = GET_MODEL_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(AVehBV)
	
	VECTOR vtemp
    VEHICLE_INDEX vi
	INT iSlotToUse = -1
	INT iDisplaySlotToUse = -1
    REQUEST_MODEL(AVeh)
	
	IF HAS_MODEL_LOADED(AVeh)

		vtemp = GET_PLAYER_COORDS(GET_PLAYER_INDEX())
	    vi = CREATE_VEHICLE(AVeh,<<vtemp.x,vtemp.y,vtemp.z + 200.0>>,0.0,FALSE,FALSE) // Doesn't need to be a network object
		
		iSlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT_FROM_STATS(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,AVeh,0, iCharacterSlot)
		iDisplaySlotToUse = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,AVeh,0,iCharacterSlot,TRUE)
		IF g_sMPTunables.bDisableDisplaySlots
			iDisplaySlotToUse = iSlotToUse 
			PRINTLN("g_sMPTunables.bDisableDisplaySlots setting iDisplaySlotToUse = iSlotToUse ")
		ENDIF
		IF iSlotToUse > -1
		AND iDisplaySlotToUse > -1
		
			STAT_SET_CURRENT_POSIX_TIME(GET_ADMIN_VEH_POSIX_ENUM(iCharacterSlot))

			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_BUY_INSURANCE, TRUE,iCharacterSlot)
			MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT_QUICK_FIX(vi,QuickFixSavedVehicles,iSlotToUse,TRUE,TRUE,FALSE,TRUE,TRUE, iCharacterSlot)
			MPSV_SET_DISPLAY_SLOT(iDisplaySlotToUse,iSlotToUse,iCharacterSlot)
			
			IF GET_ACTIVE_CHARACTER_SLOT() = (iCharacterSlot)

				PLAYSTATS_QUICKFIX_TOOL(QF_VEHICLE_GIVE, GET_STRING_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(AVehBV))
				ADD_QUICKFIXFEED_ELEMENT(QF_VEHICLE_GIVE,"QFE_VEHICLE_GV", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(AVeh), iCharacterSlot)

				
			
				SET_DO_END_OF_TRANSITION_SAVE(TRUE)

			
      	  		PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE: Placing vehicle in Garage. ")
			ELSE
				PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE: Not Placing vehicle in Garage as giving not to the active character. ")
			ENDIF

	  	ELSE
      	  PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE: Couldn't find a free slot to save vehicle")
		ENDIF

		IF DOES_ENTITY_EXIST(vi)
			DELETE_VEHICLE(vi)
		ENDIF

		RETURN TRUE	
	ELSE
		#IF IS_DEBUG_BUILD
       PRINTLN("[QUICKFIX] RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE_MODEL_NAMES: HAS_MODEL_LOADED = FALSE for ", GET_MODEL_NAME_FOR_DEBUG(AVeh))
		#ENDIF
	ENDIF		
			
	RETURN FALSE

ENDFUNC









FUNC BOOL RUN_QUICK_FIX_CHECK_GIVING_VEHICLES_NON_BUYABLE(INT CharacterSlot, INT& Stage, INT& VehicleNum)



INT iSET_VEH_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a vehicle. 
	
	QUICK_FIX_NON_BUYABLE_VEHICLES WhichVehicle = INT_TO_ENUM(QUICK_FIX_NON_BUYABLE_VEHICLES, VehicleNum)
	
	SWITCH Stage
	
		CASE 0
			
			
			I = VehicleNum
			WHILE I < ENUM_TO_INT(QF_NONBUY_VEH_TOTAL_NUMBER)
				VehicleNum = I
				iSET_VEH_CHANGE_VALUE_ADMIN = 0
				WhichVehicle = INT_TO_ENUM(QUICK_FIX_NON_BUYABLE_VEHICLES, VehicleNum)
			
				WhichStat = GET_ADMIN_VEH_GV_NONBY_ENUM(WhichVehicle, CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_VEH_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
					ModNum = VehicleNum%32
					BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
					NET_NL()NET_PRINT("[QUICKFIX] NON BUYABLE GIVE  ")NET_PRINT_INT(VehicleNum)
					NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
					NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_STRING_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(WhichVehicle))
					NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_VEH_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_VEH_GV_NONBY_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
			
					IF IS_BIT_SET(iSET_VEH_CHANGE_VALUE_ADMIN, (VehicleNum%32))
					
						VERIFY_MP_PROPERTY_STAT(CharacterSlot)
						
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[QUICKFIX] NON BUYABLE Bit is set for GIVE vehicle ")NET_PRINT_INT(VehicleNum)
						NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_STRING_FROM_QUICK_FIX_NON_BUYABLE_VEHICLE(WhichVehicle))
						NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
						#ENDIF
						
						Stage = 1
						I = ENUM_TO_INT(QF_NONBUY_VEH_TOTAL_NUMBER)
						

					ENDIF
					
				ELSE
					Stage = 2
					I = ENUM_TO_INT(QF_NONBUY_VEH_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				VehicleNum = 0
				Stage = 2
			ENDIF

		BREAK
		
		CASE 1
			WhichVehicle = INT_TO_ENUM(QUICK_FIX_NON_BUYABLE_VEHICLES, VehicleNum)

			IF RUN_QUICK_FIX_GIVE_VEHICLES_NON_BUYABLE(WhichVehicle, CharacterSlot)
				Stage = 0
				VehicleNum++
			ENDIF
			
		
		BREAK
	
	
		
		CASE 2
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVING_VEHICLES_NON_BUYABLE finished checking vehicles for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			VehicleNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE



ENDFUNC







FUNC BOOL RUN_QUICK_FIX_NON_BUYABLE_VEHICLES(INT& Stages, INT& CheckStage, INT& VehicleNum, INT& CharacterSlot)

	BOOL bAbortQuickFix
	SWITCH Stages
	

		CASE 0
			IF QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS(CharacterSlot,bAbortQuickFix)
				IF bAbortQuickFix
					PRINTLN("RUN_QUICK_FIX_NON_BUYABLE_VEHICLES: invalid data-1 character #",CharacterSlot)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages = 2
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
					ENDIF
				ELSE
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_CHECK_REMOVING_VEHICLES_NON_BUYABLE(CharacterSlot, CheckStage, VehicleNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages++
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						Stages--
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] NON BUY Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					CheckStage = 0
					VehicleNum = 0
					CharacterSlot = 0
					Stages++
				ELSE
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					Stages--
				ENDIF
			ENDIF
			
			
		BREAK
		
		
		
		CASE 2
			IF QUICK_FIX_SETUP_VEHICLE_DATA_STRUCTS(CharacterSlot,bAbortQuickFix)
				IF bAbortQuickFix
					PRINTLN("RUN_QUICK_FIX_NON_BUYABLE_VEHICLES: invalid data-2 character #",CharacterSlot)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages = 4
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
					ENDIF
				ELSE
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_CHECK_GIVING_VEHICLES_NON_BUYABLE(CharacterSlot, CheckStage, VehicleNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						CheckStage = 0
						VehicleNum = 0
						CharacterSlot = 0
						Stages++
					ELSE
						QUICK_FIX_CLEAR_VEHICLE_DATA()
						Stages--
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] NON BUY Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from GIVE checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					CheckStage = 0
					VehicleNum = 0
					CharacterSlot = 0
					Stages++
				ELSE
					QUICK_FIX_CLEAR_VEHICLE_DATA()
					Stages--
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 4
			RESET_ALL_QUICK_FIX_NON_BUYABLE_VEHICLE_STATS()
			QUICK_FIX_CLEAR_VEHICLE_DATA()
			Stages = 0
			RETURN TRUE
			
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC









FUNC BOOL RUN_QUICK_FIX_VEHICLES(QUICKFIX_DETAILS& Details)
	
	SWITCH Details.iQuickFixMainStages
	
		CASE 0
		
			IF RUN_QUICK_FIX_BUYABLE_VEHICLES(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				QUICK_FIX_CLEAR_VEHICLE_DATA()
				Details.iQuickFixMainStages++
			ENDIF
		BREAK
		
		
		CASE 1
			IF RUN_QUICK_FIX_NON_BUYABLE_VEHICLES(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				Details.iQuickFixMainStages++
			
			ENDIF
		
		BREAK
		
		CASE 2
			Details.iQuickFixStages = 0
			Details.iQuickFixMainStages = 0		
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC





























PROC RESET_ALL_QUICK_FIX_WEAPON_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_WEAPON_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_GV_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_GV_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_GV_BS_3, 0, I)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_RM_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_RM_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_RM_BS_3, 0, I)
	ENDFOR
	

ENDPROC

/// PURPOSE:
///    Sets any specific weapon unlock progression stats.
/// PARAMS:
///    eWeapon - Weapon to query.
PROC SET_WEAPON_UNLOCK_PROGRESSION_STATS(WEAPON_TYPE eWeapon, INT iSlot)
	SWITCH eWeapon
		CASE WEAPONTYPE_DLC_DOUBLEACTION
			SET_PACKED_STAT_INT(PACKED_MP_REVOLVER_TREASURE_HUNT_PROGRESS, ciMAX_REVOLVER_TREASURE_HUNT_PROGRESSION, iSlot)
		BREAK
		CASE WEAPONTYPE_DLC_STONE_HATCHET
			SET_PACKED_STAT_INT(PACKED_MP_RDR_BOUNTY_HUNTER_NUMBER_OF_BOUNTIES_COMPLETED, ciMAX_STONE_HATCHET_BOUNTIES_PROGRESSION, iSlot)
		BREAK
		CASE WEAPONTYPE_DLC_NAVYREVOLVER
			SET_PACKED_STAT_BOOL(PACKED_MP_SERIAL_KILLER_NAVY_REVOLVER_FOUND, TRUE, iSlot)
		BREAK
		CASE WEAPONTYPE_DLC_GADGETPISTOL
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_WEAPON_GADGETPISTOL_UNLOCKED, TRUE, iSlot)
		BREAK
		CASE WEAPONTYPE_DLC_COMBATSHOTGUN
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_WEAPON_COMBATSHOTGUN_UNLOCKED, TRUE, iSlot)
		BREAK
	ENDSWITCH
ENDPROC

PROC GIVE_CHARACTER_WEAPONS(WEAPON_TYPE aWeapon, INT iSlot)

	INT ActiveSlot = GET_ACTIVE_CHARACTER_SLOT()

	NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_WEAPONS - called with weapon ")NET_PRINT(GET_WEAPON_NAME(aWeapon))
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(ActiveSlot)NET_NL()
	
	IF ActiveSlot = iSlot
		SET_MP_WEAPON_EQUIPPED(aWeapon, TRUE, iSlot)
		SET_MP_WEAPON_PURCHASED(aWeapon, TRUE, iSlot)
		SET_MP_WEAPON_UNLOCKED(aWeapon, TRUE, FALSE)
		SET_WEAPON_UNLOCK_PROGRESSION_STATS(aWeapon, iSlot)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			INT StartingAmmo = GET_WEAPON_CLIP_SIZE(aWeapon)
			StartingAmmo *= 4
			IF StartingAmmo = 0
				StartingAmmo = 1
			ENDIF
			IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), aWeapon)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), aWeapon, StartingAmmo, FALSE) //GET_PLAYERS_CURRENT_STORED_AMMO(WEAPONTYPE_PISTOL), FALSE)
				NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_WEAPONS - GIVE_WEAPON_TO_PED ")NET_PRINT(GET_WEAPON_NAME(aWeapon))
				NET_PRINT(" StartingAmmo ")NET_PRINT_INT(StartingAmmo)NET_NL()
			ELSE
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), aWeapon, StartingAmmo)
				NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_WEAPONS - ADD_AMMO_TO_PED ")NET_PRINT(GET_WEAPON_NAME(aWeapon))
				NET_PRINT(" StartingAmmo ")NET_PRINT_INT(StartingAmmo)NET_NL()
			ENDIF
			SET_MP_INT_CHARACTER_STAT(GET_AMMO_CURRENT_FOR_WEAPON(aWeapon), StartingAmmo, iSlot)
			SET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(aWeapon), StartingAmmo, iSlot)
			SET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_POSSESSION_STAT(aWeapon), TRUE, iSlot)		
			PLAYSTATS_QUICKFIX_TOOL(QF_WEAPON_GIVE, GET_WEAPON_NAME(aWeapon))
			ADD_QUICKFIXFEED_ELEMENT(QF_WEAPON_GIVE, "QFE_WEAPONS_GV", GET_WEAPON_NAME(aWeapon), iSlot)
		ENDIF
	ELSE
		INT StartingAmmo = GET_WEAPON_CLIP_SIZE(aWeapon)
		StartingAmmo *= 4
		IF StartingAmmo = 0
			StartingAmmo = 1
		ENDIF
		NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_WEAPONS to NON ACTIVE PED - GIVE_WEAPON_TO_PED ")NET_PRINT(GET_WEAPON_NAME(aWeapon))
		NET_PRINT(" StartingAmmo ")NET_PRINT_INT(StartingAmmo)NET_NL()

		SET_MP_WEAPON_EQUIPPED(aWeapon, TRUE, iSlot)
		SET_MP_WEAPON_PURCHASED(aWeapon, TRUE, iSlot)
		SET_MP_WEAPON_UNLOCKED(aWeapon, TRUE, FALSE)
		SET_WEAPON_UNLOCK_PROGRESSION_STATS(aWeapon, iSlot)
		SET_MP_INT_CHARACTER_STAT(GET_AMMO_CURRENT_FOR_WEAPON(aWeapon), StartingAmmo, iSlot)
		SET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(aWeapon), StartingAmmo, iSlot)
		SET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_POSSESSION_STAT(aWeapon), TRUE, iSlot)
		PLAYSTATS_QUICKFIX_TOOL(QF_WEAPON_GIVE, GET_WEAPON_NAME(aWeapon))
		ADD_QUICKFIXFEED_ELEMENT(QF_WEAPON_GIVE,"QFE_WEAPONS_GV", GET_WEAPON_NAME(aWeapon), iSlot)
	ENDIF

ENDPROC

PROC REMOVE_CHARACTER_WEAPONS(WEAPON_TYPE aWeapon, INT iSlot)

	INT ActiveSlot = GET_ACTIVE_CHARACTER_SLOT()
	
	NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_WEAPONS - called with weapon ")NET_PRINT(GET_WEAPON_NAME(aWeapon))
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(ActiveSlot)NET_NL()
	
	IF ActiveSlot = iSlot
		SET_MP_WEAPON_EQUIPPED(aWeapon, FALSE, iSlot)
		SET_MP_WEAPON_PURCHASED(aWeapon, FALSE, iSlot)
		SET_MP_INT_CHARACTER_STAT(GET_AMMO_CURRENT_FOR_WEAPON(aWeapon), 0, iSlot)
		SET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(aWeapon), 0, iSlot)
		SET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_POSSESSION_STAT(aWeapon), FALSE, iSlot)
		SET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_AS_GIFT_STAT(aWeapon),FALSE,iSlot )
		PLAYSTATS_QUICKFIX_TOOL(QF_WEAPON_REMOVE, GET_WEAPON_NAME(aWeapon))
		ADD_QUICKFIXFEED_ELEMENT(QF_WEAPON_REMOVE, "QFE_WEAPONS_RM", GET_WEAPON_NAME(aWeapon), iSlot)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), aWeapon)
				REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), aWeapon)				
			ENDIF
		ENDIF
	ELSE
		SET_MP_WEAPON_EQUIPPED(aWeapon, FALSE, iSlot)
		SET_MP_WEAPON_PURCHASED(aWeapon, FALSE, iSlot)
		SET_MP_INT_CHARACTER_STAT(GET_AMMO_CURRENT_FOR_WEAPON(aWeapon), 0, iSlot)
		SET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(aWeapon), 0, iSlot)
		SET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_POSSESSION_STAT(aWeapon), FALSE, iSlot)
		PLAYSTATS_QUICKFIX_TOOL(QF_WEAPON_REMOVE, GET_WEAPON_NAME(aWeapon))
		ADD_QUICKFIXFEED_ELEMENT(QF_WEAPON_REMOVE, "QFE_WEAPONS_RM", GET_WEAPON_NAME(aWeapon), iSlot)
	ENDIF

ENDPROC



FUNC BOOL RUN_QUICK_FIX_GIVE_WEAPON(INT CharacterSlot, INT& Stage, INT& WeaponNum)

	INT iSET_WEAPON_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a weapon. 
	
	WEAPON_TYPE WhichWeapon = GET_WEAPONTYPE_FROM_QUICK_FIX_WEAPONS(INT_TO_ENUM(QUICK_FIX_WEAPONS, WeaponNum))
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = WeaponNum
			WHILE I < ENUM_TO_INT(QF_WEAP_TOTAL_NUMBER)
				WeaponNum = I
				iSET_WEAPON_CHANGE_VALUE_ADMIN = 0
				WhichWeapon = GET_WEAPONTYPE_FROM_QUICK_FIX_WEAPONS(INT_TO_ENUM(QUICK_FIX_WEAPONS, WeaponNum))
			
				WhichStat = GET_ADMIN_WEAPON_GV_ENUM(INT_TO_ENUM(QUICK_FIX_WEAPONS, WeaponNum), CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_WEAPON_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = WeaponNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						NET_NL()NET_PRINT("[QUICKFIX] Check Give Weapon ")NET_PRINT_INT(WeaponNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_WEAPON_NAME(WhichWeapon)))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_WEAPON_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_WEAP_GV_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
			
					IF iSET_WEAPON_CHANGE_VALUE_ADMIN = -1
						NET_NL()NET_PRINT("[QUICKFIX] iSET_WEAPON_CHANGE_VALUE_ADMIN = -1 !!! CHEATER !!! ")
					
					ELSE
			
						IF IS_BIT_SET(iSET_WEAPON_CHANGE_VALUE_ADMIN, (WeaponNum%32))
							#IF IS_DEBUG_BUILD			
							NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Give Weapon ")NET_PRINT_INT(WeaponNum)
							NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_WEAPON_NAME(WhichWeapon))
							NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
							#ENDIF
							
							GIVE_CHARACTER_WEAPONS(WhichWeapon, CharacterSlot)

						ENDIF
					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_WEAP_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				WeaponNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVE_WEAPON finished checking Weapon for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			WeaponNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC



FUNC BOOL RUN_QUICK_FIX_REMOVE_WEAPON(INT CharacterSlot, INT& Stage, INT& WeaponNum)

	INT iSET_WEAPON_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a weapon. 
	
	WEAPON_TYPE WhichWeapon = GET_WEAPONTYPE_FROM_QUICK_FIX_WEAPONS(INT_TO_ENUM(QUICK_FIX_WEAPONS, WeaponNum))
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = WeaponNum
			WHILE I < ENUM_TO_INT(QF_WEAP_TOTAL_NUMBER)
				WeaponNum = I
				iSET_WEAPON_CHANGE_VALUE_ADMIN = 0
				WhichWeapon = GET_WEAPONTYPE_FROM_QUICK_FIX_WEAPONS(INT_TO_ENUM(QUICK_FIX_WEAPONS, WeaponNum))
			
				WhichStat = GET_ADMIN_WEAPON_RM_ENUM(INT_TO_ENUM(QUICK_FIX_WEAPONS, WeaponNum), CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_WEAPON_CHANGE_VALUE_ADMIN)
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = WeaponNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						NET_NL()NET_PRINT("[QUICKFIX] Check Remove Weapon ")NET_PRINT_INT(WeaponNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_WEAPON_NAME(WhichWeapon)))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_WEAPON_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_WEAP_RM_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					IF IS_BIT_SET(iSET_WEAPON_CHANGE_VALUE_ADMIN, (WeaponNum%32))
						#IF IS_DEBUG_BUILD				
						NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Remove Weapon ")NET_PRINT_INT(WeaponNum)
						NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_WEAPON_NAME(WhichWeapon))
						NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
						#ENDIF
						
						REMOVE_CHARACTER_WEAPONS(WhichWeapon, CharacterSlot)

					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_WEAP_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				WeaponNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_REMOVE_WEAPON finished checking Weapon for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			WeaponNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_WEAPONS(INT& Stages, INT& CheckStage, INT& WeaponNum, INT& CharacterSlot)


	SWITCH Stages

		CASE 0
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_REMOVE_WEAPON(CharacterSlot, CheckStage, WeaponNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						WeaponNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Remove Weapon Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					WeaponNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
			
		BREAK
		
		
		CASE 1
			
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_GIVE_WEAPON(CharacterSlot, CheckStage, WeaponNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						WeaponNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Give Weapon Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					WeaponNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE 2
			RESET_ALL_QUICK_FIX_WEAPON_STATS()
			RETURN TRUE
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_WEAPONS(QUICKFIX_DETAILS& Details)
	
	SWITCH Details.iQuickFixMainStages
	
		CASE 0
		
			IF RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_WEAPONS(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				Details.iQuickFixMainStages++
			ENDIF
		BREAK
		

		
		CASE 1
			Details.iQuickFixStages = 0
			Details.iQuickFixMainStages = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC























PROC RESET_ALL_QUICK_FIX_APARTM_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_APARTM_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_GV_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_GV_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_GV_BS_3, 0, I)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_RM_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_RM_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_RM_BS_3, 0, I)

		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_APARTM_SLOT, 0, I)

	ENDFOR
	

ENDPROC




PROC GIVE_CHARACTER_APARTMS(INT aAPARTM, INT iSlot, BOOL RankBoost = FALSE)

	INT ActiveSlot = GET_ACTIVE_CHARACTER_SLOT()

	TEXT_LABEL_15 ApartName =GET_PROPERTY_NAME(aAPARTM) 
	
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_APARTMS - called with APARTM ")NET_PRINT_INT(aAPARTM)
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(ActiveSlot)
	NET_PRINT(" name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(ApartName))NET_NL()
	#ENDIF
	
	IF GetHeavilyAccessed_MP_STAT(MP_STAT_PROPERTY_HOUSE, iSlot) = aAPARTM
	OR GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_1, iSlot) = aAPARTM
	OR GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_2, iSlot) = aAPARTM
		PRINTLN("[QUICKFIX] GIVE_CHARACTER_APARTMS - Player has already has apartment #",aAPARTM," aborting. ")
		EXIT
	ENDIF
	
	IF ActiveSlot = iSlot
		
		IF GetHeavilyAccessed_MP_STAT(MP_STAT_PROPERTY_HOUSE, iSlot) = 0
		
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,aAPARTM, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,GET_CLOUD_TIME_AS_INT(), iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,aAPARTM, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,GET_CLOUD_TIME_AS_INT(), iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(aAPARTM))*g_sMPTunables.fPropertyMultiplier)
			IF RankBoost
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[0] = aAPARTM
			SET_LAST_USED_PROPERTY(aAPARTM)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_GIVE, ApartName)
			IF RankBoost = FALSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"QFE_PROPERTY_GV", ApartName, iSlot, RankBoost)
			ELSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"RP_BOOSTDONE", ApartName, iSlot, RankBoost)
			ENDIF
			IF RankBoost
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FREE_PROPERTY_SLOT_0, TRUE, iSlot)	// player was rewarded free apartment in this property slot
			ENDIF
			
		ELIF GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_1, iSlot) = 0
		
		
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_1,aAPARTM, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(aAPARTM))*g_sMPTunables.fPropertyMultiplier)
			IF RankBoost
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_1,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[1] = aAPARTM
			SET_LAST_USED_PROPERTY(aAPARTM)
			
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_GIVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE, "QFE_PROPERTY_GV",ApartName, iSlot)
			IF RankBoost
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FREE_PROPERTY_SLOT_1, TRUE, iSlot)	// player was rewarded free apartment in this property slot
			ENDIF
			
		ELIF GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_2, iSlot) = 0

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_2,aAPARTM, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0] =CEIL(TO_FLOAT(GET_MP_PROPERTY_VALUE(aAPARTM))*g_sMPTunables.fPropertyMultiplier)
			IF RankBoost
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0] = 0
			ENDIF
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_2,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0], iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[2] = aAPARTM
			SET_LAST_USED_PROPERTY(aAPARTM)
			
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_GIVE, ApartName)
			IF RankBoost = FALSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"QFE_PROPERTY_GV", ApartName, iSlot, RankBoost)
			ELSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"RP_BOOSTDONE", ApartName, iSlot, RankBoost)
			ENDIF
			IF RankBoost
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FREE_PROPERTY_SLOT_2, TRUE, iSlot)	// player was rewarded free apartment in this property slot
			ENDIF
			
		ELSE
			NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_APARTMS - Player has no free apartment slots, can't give apartment. ")
		
		ENDIF
		
	ELSE
		

		IF GetHeavilyAccessed_MP_STAT(MP_STAT_PROPERTY_HOUSE, iSlot) = 0

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,aAPARTM, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,GET_CLOUD_TIME_AS_INT(), iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,aAPARTM, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,GET_CLOUD_TIME_AS_INT(), iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue, iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_GIVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE, "QFE_PROPERTY_GV", ApartName, iSlot)
			IF RankBoost
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FREE_PROPERTY_SLOT_0, TRUE, iSlot)	// player was rewarded free apartment in this property slot
			ENDIF
			
		ELIF GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_1, iSlot) = 0
		
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_1,aAPARTM, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_1,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value, iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_GIVE, ApartName)
			IF RankBoost = FALSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"QFE_PROPERTY_GV", ApartName, iSlot, RankBoost)
			ELSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"RP_BOOSTDONE", ApartName, iSlot, RankBoost)
			ENDIF
			IF RankBoost
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FREE_PROPERTY_SLOT_1, TRUE, iSlot)	// player was rewarded free apartment in this property slot
			ENDIF
			
		ELIF GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_2, iSlot) = 0
		
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_2,aAPARTM, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_2,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0], iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_GIVE, ApartName)
			IF RankBoost = FALSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"QFE_PROPERTY_GV", ApartName, iSlot, RankBoost)
			ELSE
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_GIVE,"RP_BOOSTDONE", ApartName, iSlot, RankBoost)
			ENDIF
			IF RankBoost
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FREE_PROPERTY_SLOT_2, TRUE, iSlot)	// player was rewarded free apartment in this property slot
			ENDIF
			
		ELSE
			NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_APARTMS - Player has no free apartment slots for non active slot, can't give apartment. ")
		
		ENDIF
	ENDIF

ENDPROC

PROC REMOVE_CHARACTER_APARTMS(INT aAPARTM, INT iSlot)

	INT ActiveSlot = GET_ACTIVE_CHARACTER_SLOT()

	IF iSlot = -1
		iSlot = ActiveSlot
	ENDIF
	TEXT_LABEL_15 ApartName =GET_PROPERTY_NAME(aAPARTM) 
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - called with APARTM ")NET_PRINT_INT(aAPARTM)
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(ActiveSlot)
	NET_PRINT(" name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(ApartName))
	NET_PRINT(" PROPERTY_HOUSE aptslot 1 stat = ")NET_PRINT_INT(GetHeavilyAccessed_MP_STAT(MP_STAT_PROPERTY_HOUSE, iSlot))
	NET_PRINT(" PROPERTY_HOUSE aptslot 2 stat = ")NET_PRINT_INT(GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_1, iSlot))
	NET_NL()
	#ENDIF


	IF ActiveSlot = iSlot


		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROPERTY_HOUSE, iSlot)
			IF GET_OWNED_PROPERTY(1) = 0
		
				NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 1, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,0, iSlot)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,0, iSlot)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,0, iSlot)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,0, iSlot)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue =0
				SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,0, iSlot)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[0] = 0
				SET_LAST_USED_PROPERTY(0)
				PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Cannot remove first property as the player has a second property GET_OWNED_PROPERTY(1) = ")NET_PRINT_INT(GET_OWNED_PROPERTY(1))NET_NL()
			ENDIF
		ENDIF
			
	
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_1, iSlot)
			IF GET_OWNED_PROPERTY(2) = 0
				NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 2, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_1,0, iSlot)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value =0
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_1,0, iSlot)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[1] = 0
				SET_LAST_USED_PROPERTY(0)
				PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Cannot remove second property as the player has a third property GET_OWNED_PROPERTY(2) = ")NET_PRINT_INT(GET_OWNED_PROPERTY(2))NET_NL()
			ENDIF
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_2, iSlot)
			IF GET_OWNED_PROPERTY(3) = 0
				NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 3, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_2,0, iSlot)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[0] =0
				SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_2,0, iSlot)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[2] = 0
				SET_LAST_USED_PROPERTY(0)
				PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
				ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Cannot remove third property as the player has a fourth property GET_OWNED_PROPERTY(2) = ")NET_PRINT_INT(GET_OWNED_PROPERTY(2))NET_NL()
			ENDIF
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_3, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 4, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_3,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[1] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_3,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[3] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_4, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 5, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_4,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_4,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[4] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_YACHT_PROPERTY, iSlot)//  PROPERTY_YACHT_APT_1_BASE
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 5, remove Yacht ")NET_PRINT_INT(aAPARTM)NET_NL()
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_PROPERTY,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_PROPERTY,0, iSlot)
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(FALSE)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_INV_YACHT_MODEL_0, 0, iSlot)
			CLEAR_BIG_ASS_VEHICLE_BS(GET_BIGASS_VEHICLE_INDICE_BIT_FROM_ENUM(BV_DLC_BIG_YACHT)) 
			MP_BIG_ASS_VEHICLES_STORE_DATA_IN_STATS()
			
		ENDIF
		
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_OFFICE, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 5, remove Office ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_0-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE,"QFE_PROPERTY_RM",  ApartName, iSlot)
			
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_CLUBHOUSE, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 6, remove Clubhouse ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CLUBHOUSE-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CLUBHOUSE,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE,"QFE_PROPERTY_RM",  ApartName, iSlot)
			
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_5, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 6, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_5,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_6-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_5,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_APT_6] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		

		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_6, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 7, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_6,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_7-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_6,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_APT_7] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_7, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 8, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_7,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_8-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_7,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_APT_8] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		
		//CDM PROPERTY STAT UPDATE //OLD PROPERTIES
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_8, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 9, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_8,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_9-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_8,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_APT_9] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_9, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 10, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_9,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_APT_10-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_9,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_APT_10] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_OFFICE_GAR1, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt office gar 1, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR1,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_OFFICE_GAR2, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt office gar 2, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR2,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_OFFICE_GAR3, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt office gar 3, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_GAR3,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_IE_WAREHOUSE, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt IE_WAREHOUSE, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_IE_WAREHOUSE,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_IE_WAREHOUSE-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_IE_WAREHOUSE,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_IE_WAREHOUSE] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_BUSINESSHUB_VALUE, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt ARENAWARS gar 1, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_BUSINESSHUB_VALUE,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_NIGHTCLUB-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_BUSINESSHUB_VALUE,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_NIGHTCLUB] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_MEGAWARE_GAR1, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt MEGAWARE gar 1, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR1,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR1,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_MEGAWARE_GAR2, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt MEGAWARE gar 2, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR2,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR2,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_MEGAWARE_GAR3, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt MEGAWARE gar 3, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR3,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_MEGAWARE_GAR3,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_ARENAWARS_GAR1, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt ARENAWARS gar 1, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARENAWARS_GAR1,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARENAWARS_GAR1,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_ARENAWARS_GAR2, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt ARENAWARS gar 2, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARENAWARS_GAR3,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL2-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARENAWARS_GAR2,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL2] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_ARENAWARS_GAR3, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt ARENAWARS gar 3, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARENAWARS_GAR3,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL3-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARENAWARS_GAR3,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL3] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_CASINO_GAR1, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt CASINO gar, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CASINO_GAR1,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_CASINO_GARAGE-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_CASINO_GAR1,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CASINO_GARAGE] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		
		#IF FEATURE_CASINO_HEIST
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_ARCADE_GAR1, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt ARCADE gar, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARCADE_GAR1,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_ARCADE_GARAGE-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_ARCADE_GAR1,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_ARCADE_GARAGE] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		#ENDIF
		
		//MAX_OWNED_PROPERTIES
		#IF FEATURE_TUNER
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_AUTO_SHOP, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This AUTO SHOP gar, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_AUTO_SHOP,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY-2] =0
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_AUTO_SHOP_VALUE,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		#ENDIF
		
		#IF FEATURE_FIXER //CDM PROPERTY STAT UPDATE
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROP_SECURITY_OFFICE_GAR, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This SECURITY OFFICE gar, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROP_SECURITY_OFFICE_GAR,0, iSlot)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR-2] =0
			//SET_MP_INT_CHARACTER_STAT(MP_STAT_SECURITY_OFFICE_VALUE,0, iSlot)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR] = 0
			SET_LAST_USED_PROPERTY(0)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		#ENDIF
	ELSE
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_PROPERTY_HOUSE, iSlot)
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 1 in non-active slot, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()

			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_TIME,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_TIME,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE_NEW_VALUE,0, iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE,"QFE_PROPERTY_RM",  ApartName, iSlot)
		ENDIF
	
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_1, iSlot)
		
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 2 in non active slot, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()
				
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_1,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_1,0, iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE,"QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_MULTI_PROPERTY_2, iSlot)
		
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 2 in non active slot, remove aAPARTM ")NET_PRINT_INT(aAPARTM)NET_NL()
				
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_2,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MULTI_PROPERTY_VALUE_2,0, iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
		ENDIF
		

		
		IF aAPARTM = GetHeavilyAccessed_MP_STAT(MP_STAT_YACHT_PROPERTY, iSlot)//  PROPERTY_YACHT_APT_1_BASE
			NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_APARTMS - Player Does Own This in apt 5, remove Yacht ")NET_PRINT_INT(aAPARTM)NET_NL()
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_PROPERTY,0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_PROPERTY,0, iSlot)
			PLAYSTATS_QUICKFIX_TOOL(QF_APARTMENT_REMOVE, ApartName)
			ADD_QUICKFIXFEED_ELEMENT(QF_APARTMENT_REMOVE, "QFE_PROPERTY_RM", ApartName, iSlot)
			SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(FALSE)
			
		ENDIF
		
		
		
	ENDIF

ENDPROC




FUNC BOOL RUN_QUICK_FIX_GIVE_APARTM(INT CharacterSlot, INT& Stage, INT& APARTMNum)

	INT iSET_APARTM_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	TEXT_LABEL_31 PropertyName 
	#ENDIF
	INT I
	//Give a APARTM. 
	
	INT WhichAPARTM = GET_APARTMTYPE_FROM_QUICK_FIX_APARTMS(INT_TO_ENUM(QUICK_FIX_APARTMS, APARTMNum))
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = APARTMNum
			WHILE I < ENUM_TO_INT(QF_APARTM_TOTAL_NUMBER)
				APARTMNum = I
				iSET_APARTM_CHANGE_VALUE_ADMIN = 0
				WhichAPARTM = GET_APARTMTYPE_FROM_QUICK_FIX_APARTMS(INT_TO_ENUM(QUICK_FIX_APARTMS, APARTMNum))
			
				WhichStat = GET_ADMIN_APARTM_GV_ENUM(INT_TO_ENUM(QUICK_FIX_APARTMS, APARTMNum), CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_APARTM_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = APARTMNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						PropertyName = GET_PROPERTY_NAME(WhichAPARTM)
						NET_NL()NET_PRINT("[QUICKFIX] Check Give APARTM ")NET_PRINT_INT(APARTMNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(PropertyName))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_APARTM_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_APARTM_GV_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
			
					IF iSET_APARTM_CHANGE_VALUE_ADMIN = -1
						NET_NL()NET_PRINT("[QUICKFIX] iSET_APARTM_CHANGE_VALUE_ADMIN = -1 !!! CHEATER !!! ")
					
					ELSE
			
						IF IS_BIT_SET(iSET_APARTM_CHANGE_VALUE_ADMIN, (APARTMNum%32))
									
							#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Give APARTM ")NET_PRINT_INT(APARTMNum)
							NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(PropertyName))
							NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
							#ENDIF
							
							GIVE_CHARACTER_APARTMS(WhichAPARTM, CharacterSlot)

							SET_DO_END_OF_TRANSITION_SAVE_CLOUD(TRUE)
						ENDIF
					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_APARTM_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				APARTMNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVE_APARTM finished checking APARTM for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			APARTMNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC



FUNC BOOL RUN_QUICK_FIX_REMOVE_APARTM(INT CharacterSlot, INT& Stage, INT& APARTMNum)

	INT iSET_APARTM_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	TEXT_LABEL_31 PropertyName 
	#ENDIF
	INT I
	//Give a APARTM. 
	
	INT WhichAPARTM = GET_APARTMTYPE_FROM_QUICK_FIX_APARTMS(INT_TO_ENUM(QUICK_FIX_APARTMS, APARTMNum))
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = APARTMNum
			WHILE I < ENUM_TO_INT(QF_APARTM_TOTAL_NUMBER)
				APARTMNum = I
				iSET_APARTM_CHANGE_VALUE_ADMIN = 0
				WhichAPARTM = GET_APARTMTYPE_FROM_QUICK_FIX_APARTMS(INT_TO_ENUM(QUICK_FIX_APARTMS, APARTMNum))

				WhichStat = GET_ADMIN_APARTM_RM_ENUM(INT_TO_ENUM(QUICK_FIX_APARTMS, APARTMNum), CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_APARTM_CHANGE_VALUE_ADMIN)
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = APARTMNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						PropertyName = GET_PROPERTY_NAME(WhichAPARTM)
						NET_NL()NET_PRINT("[QUICKFIX] Check Remove APARTM ")NET_PRINT_INT(APARTMNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(PropertyName))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_APARTM_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_APARTM_RM_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					IF IS_BIT_SET(iSET_APARTM_CHANGE_VALUE_ADMIN, (APARTMNum%32))
						
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Remove APARTM ")NET_PRINT_INT(APARTMNum)
						NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_FILENAME_FOR_AUDIO_CONVERSATION(PropertyName))
						NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
						#ENDIF
						
						REMOVE_CHARACTER_APARTMS(WhichAPARTM, CharacterSlot)

						SET_DO_END_OF_TRANSITION_SAVE_CLOUD(TRUE)

					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_APARTM_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				APARTMNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_REMOVE_APARTM finished checking APARTM for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			APARTMNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_APARTMS(INT& Stages, INT& CheckStage, INT& APARTMNum, INT& CharacterSlot)


	SWITCH Stages

		CASE 0
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_REMOVE_APARTM(CharacterSlot, CheckStage, APARTMNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						APARTMNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Remove APARTM Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					APARTMNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
					
			
		BREAK
		
		
		CASE 1

			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_GIVE_APARTM(CharacterSlot, CheckStage, APARTMNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						APARTMNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Give APARTM Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					APARTMNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
			
			
		BREAK
		
		
		
		CASE 2
			RESET_ALL_QUICK_FIX_APARTM_STATS()
			RETURN TRUE
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_APARTMS(QUICKFIX_DETAILS& Details)
	
	SWITCH Details.iQuickFixMainStages
	
		CASE 0
		
			IF RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_APARTMS(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				Details.iQuickFixMainStages++
			ENDIF
		BREAK
		

		
		CASE 1
			Details.iQuickFixStages = 0
			Details.iQuickFixMainStages = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

































PROC RESET_ALL_QUICK_FIX_CLOTHES_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_CLOTHES_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_3, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_4, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_5, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_6, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_7, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_8, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_9, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_10, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_11, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_12, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_13, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_14, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_15, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_16, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_17, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_18, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_19, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_20, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_21, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_22, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_23, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_24, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_25, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_26, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_27, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_28, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_29, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_30, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_31, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_32, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_33, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_34, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_35, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_36, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_37, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_38, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_39, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_40, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_41, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_42, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_43, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_44, 0, I)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_1, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_2, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_3, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_4, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_5, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_6, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_7, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_8, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_9, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_10, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_11, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_12, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_13, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_14, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_15, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_16, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_17, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_18, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_19, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_20, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_21, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_22, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_23, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_24, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_25, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_26, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_27, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_28, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_29, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_30, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_31, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_32, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_33, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_34, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_35, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_36, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_37, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_38, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_39, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_40, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_41, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_42, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_43, 0, I)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_RM_BS_44, 0, I)
	ENDFOR
	

ENDPROC




PROC GIVE_CHARACTER_CLOTHES(QUICK_FIX_CLOTHES aCLOTHES, INT iSlot)

	INT ActiveSlot = GET_ACTIVE_CHARACTER_SLOT()
	
	BOOL isMale = IS_CHARACTER_MALE(iSlot)
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_CLOTHES - called with CLOTHES ")NET_PRINT(GET_QUICK_FIX_CLOTHES_STRING(aCLOTHES, isMale))
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(ActiveSlot)NET_NL()
	#ENDIF
	
	IF ActiveSlot = iSlot	
		QUICK_FIX_CHANGE_CLOTHES(aCLOTHES, TRUE, iSlot,TRUE )
		PLAYSTATS_QUICKFIX_TOOL(QF_CLOTHES_GIVE, GET_QUICK_FIX_CLOTHES_STRING(aCLOTHES, isMale))
		ADD_QUICKFIXFEED_ELEMENT(QF_CLOTHES_GIVE,"QFE_CLOTHES_GV", "", iSlot)
	ELSE
		QUICK_FIX_CHANGE_CLOTHES(aCLOTHES, TRUE, iSlot,FALSE )
		PLAYSTATS_QUICKFIX_TOOL(QF_CLOTHES_GIVE, GET_QUICK_FIX_CLOTHES_STRING(aCLOTHES, isMale))
		ADD_QUICKFIXFEED_ELEMENT(QF_CLOTHES_GIVE,"QFE_CLOTHES_GV", "", iSlot)
	ENDIF

ENDPROC

PROC REMOVE_CHARACTER_CLOTHES(QUICK_FIX_CLOTHES aCLOTHES, INT iSlot)

	INT ActiveSlot = GET_ACTIVE_CHARACTER_SLOT()

	IF iSlot = -1
		iSlot = ActiveSlot
	ENDIF

	BOOL isMale = IS_CHARACTER_MALE(iSlot)
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[QUICKFIX] REMOVE_CHARACTER_CLOTHES - called with CLOTHES ")NET_PRINT(GET_QUICK_FIX_CLOTHES_STRING(aCLOTHES, isMale))
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(ActiveSlot)NET_NL()
	#ENDIF
	
	IF ActiveSlot = iSlot	
		QUICK_FIX_CHANGE_CLOTHES(aCLOTHES, FALSE, iSlot,TRUE )
		PLAYSTATS_QUICKFIX_TOOL(QF_CLOTHES_REMOVE, GET_QUICK_FIX_CLOTHES_STRING(aCLOTHES, isMale))
		ADD_QUICKFIXFEED_ELEMENT(QF_CLOTHES_REMOVE,"QFE_CLOTHES_RM", "", iSlot)
	ELSE
		QUICK_FIX_CHANGE_CLOTHES(aCLOTHES, FALSE, iSlot,FALSE )
		PLAYSTATS_QUICKFIX_TOOL(QF_CLOTHES_REMOVE, GET_QUICK_FIX_CLOTHES_STRING(aCLOTHES, isMale))
		ADD_QUICKFIXFEED_ELEMENT(QF_CLOTHES_REMOVE,"QFE_CLOTHES_RM", "", iSlot)
	ENDIF

ENDPROC




FUNC BOOL RUN_QUICK_FIX_GIVE_CLOTHES(INT CharacterSlot, INT& Stage, INT& CLOTHESNum)

	INT iSET_CLOTHES_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	TEXT_LABEL_31 ClothesName 
	BOOL isMale = IS_CHARACTER_MALE(CharacterSlot)	
	#ENDIF
	INT I
	//Give a CLOTHES. 
	
	
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = CLOTHESNum
			WHILE I < ENUM_TO_INT(QF_CLOTHES_TOTAL_NUMBER)
				CLOTHESNum = I
				iSET_CLOTHES_CHANGE_VALUE_ADMIN = 0
			
				WhichStat = GET_ADMIN_CLOTHES_GV_ENUM(INT_TO_ENUM(QUICK_FIX_CLOTHES, CLOTHESNum), CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_CLOTHES_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")

						ModNum = CLOTHESNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						ClothesName = GET_QUICK_FIX_CLOTHES_STRING(INT_TO_ENUM(QUICK_FIX_CLOTHES, CLOTHESNum), isMale)
						NET_NL()NET_PRINT("[QUICKFIX] Check Give CLOTHES ")NET_PRINT_INT(CLOTHESNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT((ClothesName))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_CLOTHES_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_CLOTHES_GV_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
			
					IF iSET_CLOTHES_CHANGE_VALUE_ADMIN = -1
						NET_NL()NET_PRINT("[QUICKFIX] iSET_CLOTHES_CHANGE_VALUE_ADMIN = -1 !!! CHEATER !!! ")
						
					ELSE
			
						IF IS_BIT_SET(iSET_CLOTHES_CHANGE_VALUE_ADMIN, (CLOTHESNum%32))
								
							#IF IS_DEBUG_BUILD			
							NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Give CLOTHES ")NET_PRINT_INT(CLOTHESNum)
							NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT((ClothesName))
							NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
							#ENDIF
							
							GIVE_CHARACTER_CLOTHES(INT_TO_ENUM(QUICK_FIX_CLOTHES, CLOTHESNum), CharacterSlot)

							SET_DO_END_OF_TRANSITION_SAVE(TRUE)
						ENDIF
					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_CLOTHES_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				CLOTHESNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVE_CLOTHES finished checking CLOTHES for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			CLOTHESNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC




FUNC BOOL RUN_QUICK_FIX_REMOVE_CLOTHES(INT CharacterSlot, INT& Stage, INT& CLOTHESNum)

	INT iSET_CLOTHES_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	TEXT_LABEL_31 ClothesName 
	BOOL isMale = IS_CHARACTER_MALE(CharacterSlot)
	#ENDIF
	INT I
	//Give a CLOTHES. 
		
	
	
	
	SWITCH Stage
	
	
		CASE 0
			
			
			I = CLOTHESNum
			WHILE I < ENUM_TO_INT(QF_CLOTHES_TOTAL_NUMBER)
				CLOTHESNum = I
				iSET_CLOTHES_CHANGE_VALUE_ADMIN = 0

				WhichStat = GET_ADMIN_CLOTHES_RM_ENUM(INT_TO_ENUM(QUICK_FIX_CLOTHES, CLOTHESNum), CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_CLOTHES_CHANGE_VALUE_ADMIN)
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = CLOTHESNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						ClothesName = GET_QUICK_FIX_CLOTHES_STRING(INT_TO_ENUM(QUICK_FIX_CLOTHES, CLOTHESNum), isMale)
						NET_NL()NET_PRINT("[QUICKFIX] Check Remove CLOTHES ")NET_PRINT_INT(CLOTHESNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT((ClothesName))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_CLOTHES_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_CLOTHES_RM_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					IF IS_BIT_SET(iSET_CLOTHES_CHANGE_VALUE_ADMIN, (CLOTHESNum%32))
						#IF IS_DEBUG_BUILD				
						NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Remove CLOTHES ")NET_PRINT_INT(CLOTHESNum)
						NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT((ClothesName))
						NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
						#ENDIF
						
						REMOVE_CHARACTER_CLOTHES(INT_TO_ENUM(QUICK_FIX_CLOTHES, CLOTHESNum), CharacterSlot)

						SET_DO_END_OF_TRANSITION_SAVE(TRUE)

					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_CLOTHES_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				CLOTHESNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_REMOVE_CLOTHES finished checking CLOTHES for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			CLOTHESNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_CLOTHES(INT& Stages, INT& CheckStage, INT& CLOTHESNum, INT& CharacterSlot)


	SWITCH Stages

		CASE 0
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_REMOVE_CLOTHES(CharacterSlot, CheckStage, CLOTHESNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						CLOTHESNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Remove CLOTHES Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					CLOTHESNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
			
		BREAK
		
		
		CASE 1
			
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_GIVE_CLOTHES(CharacterSlot, CheckStage, CLOTHESNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						CLOTHESNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Give CLOTHES Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					CLOTHESNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE 2
			RESET_ALL_QUICK_FIX_CLOTHES_STATS()
			RETURN TRUE
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_CLOTHES(QUICKFIX_DETAILS& Details)
	
	SWITCH Details.iQuickFixMainStages
	
		CASE 0
		
			IF RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_CLOTHES(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				Details.iQuickFixMainStages++
			ENDIF
		BREAK
		

		
		CASE 1
			Details.iQuickFixStages = 0
			Details.iQuickFixMainStages = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC



















FUNC STRING GET_QUICK_FIX_PROGRESS_NAME(QUICK_FIX_PROGRESS aPROGRESS)

	STRING result = ""
	
	SWITCH aPROGRESS
		CASE QF_PROG_HEIST_AWD_FLOW_ORDER		RETURN "AWT_777"		//All In Order
		CASE QF_PROG_HEIST_AWD_SAME_TEAM 		RETURN "AWT_778"		//Loyalty
		CASE QF_PROG_HEIST_AWD_ULTIMATE			RETURN "AWT_779"		//Criminal Mastermind
		CASE QF_PROG_HEIST_AWD_MEMBER			RETURN "AWT_786"		//Supporting Role 
		CASE QF_PROG_HEIST_AWD_FIRST_PERSON		RETURN "AWT_785"		//Another Perspective 
		
 
	ENDSWITCH
	

	RETURN result

ENDFUNC







PROC RESET_ALL_QUICK_FIX_PROGRESS_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_PROGRESS_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_PROG_GV_BS_1, 0, I)

	ENDFOR
	

ENDPROC



PROC GIVE_CHARACTER_PROGRESS(QUICK_FIX_PROGRESS aPROGRESS, INT iSlot)

	NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_PROGRESS - called with PROGRESS ")NET_PRINT(GET_QUICK_FIX_PROGRESS_STRING(aPROGRESS))
	NET_PRINT(" iSlot = ")NET_PRINT_INT(iSlot)
	INT total
	INT Max 
	INT Id
	EWarehouseSize size
	SWITCH aPROGRESS
	
		CASE QF_PROG_HEIST_AWD_FLOW_ORDER
			
			g_QF_bForceAwardFlowOrder = TRUE
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS))
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_PROGRESS_GV", GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS), iSlot)
		BREAK
		
		CASE QF_PROG_HEIST_AWD_SAME_TEAM
			
			g_QF_bForceAwardSameTeam = TRUE
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS))
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_PROGRESS_GV", GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS), iSlot)
		BREAK
		
		CASE QF_PROG_HEIST_AWD_ULTIMATE
			
			g_QF_bForceAwardUltimate = TRUE
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS))
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_PROGRESS_GV", GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS), iSlot)
		BREAK
		
		CASE QF_PROG_HEIST_AWD_MEMBER
			
			g_QF_bForceAwardMember = TRUE
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS))
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_PROGRESS_GV", GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS), iSlot)
		BREAK
		
		CASE QF_PROG_HEIST_AWD_FIRST_PERSON
			
			g_QF_bForceAwardFirstPerson = TRUE
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS))
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_PROGRESS_GV", GET_QUICK_FIX_PROGRESS_NAME(aPROGRESS), iSlot)
		BREAK
		
		CASE QF_PROG_YACHTNAME_RESET
						
			SET_MP_STRING_CHARACTER_STAT(MP_STAT_YACHT_NAME, "", iSlot)
			SET_MP_STRING_CHARACTER_STAT(MP_STAT_YACHT_NAME2, "", iSlot)
			
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_YACHTNAME")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_YACHTNAME", "", iSlot)
			
		BREAK
		
		CASE QF_CONTRA_REMOVE_CONTRABAND
		
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED0, 0, iSlot)
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED1, 0, iSlot)
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED2, 0, iSlot)
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED3, 0, iSlot)
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED4, 0, iSlot)
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED5, 0, iSlot)
			
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE1, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE2, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE3, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE4, 0, iSlot)
			
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE1, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE2, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE3, 0, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE4, 0, iSlot)


			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_RM")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_RM", "", iSlot)
		BREAK
		
		CASE QF_CONTRA_GIVE_ORNAMENTAL_EGG
		
			
		
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED0, 1, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 1, iSlot)
			
			
			total = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0,iSlot, TRUE)
			Id = GET_MP_INT_CHARACTER_STAT(MP_STAT_WARHOUSESLOT0, iSlot, TRUE)
			size = GET_WAREHOUSE_SIZE(Id)
			
			SWITCH  size
				CASE eWarehouseSmall
					Max = ciSMALL_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseMedium
					Max = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseLarge
					Max = ciLARGE_WAREHOUSE_CRATE_CAPACITY
				BREAK
			ENDSWITCH
			IF total < 0
				total = 0
			ENDIF
			IF total >= Max
				total = Max-1
			ENDIF
			IF total < (Max-1)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, (total+1))
			ENDIF
			SET_WAREHOUSE_CRATES_STAT_INT(0, total, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_OEGG), TRUE, iSlot)
			
			INIT_SPECIAL_ITEM_GLOBAL_DATA()
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_GV")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_GV", "", iSlot)

			
		BREAK
		CASE QF_CONTRA_GIVE_GOLDEN_MINIGUN
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED0, 2, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 1, iSlot)
			
			total = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0,iSlot, TRUE)
			Id = GET_MP_INT_CHARACTER_STAT(MP_STAT_WARHOUSESLOT0, iSlot, TRUE)
			size = GET_WAREHOUSE_SIZE(Id)
			 
			SWITCH  size
				CASE eWarehouseSmall
					Max = ciSMALL_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseMedium
					Max = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseLarge
					Max = ciLARGE_WAREHOUSE_CRATE_CAPACITY
				BREAK
			ENDSWITCH
			IF total < 0
				total = 0
			ENDIF
			IF total >= Max
				total = Max-1
			ENDIF
			IF total < (Max-1)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, (total+1))
			ENDIF
			SET_WAREHOUSE_CRATES_STAT_INT(0, total, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_MINIG), TRUE, iSlot)

			
			INIT_SPECIAL_ITEM_GLOBAL_DATA()
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_GV")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_GV", "", iSlot)
			
		BREAK
		CASE QF_CONTRA_GIVE_EXTRA_LARGE_DIAMOND
		
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED0, 3, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 1, iSlot)
			
			total = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0,iSlot, TRUE)
			Id = GET_MP_INT_CHARACTER_STAT(MP_STAT_WARHOUSESLOT0, iSlot, TRUE)
			size = GET_WAREHOUSE_SIZE(Id)
		
			
			SWITCH  size
				CASE eWarehouseSmall
					Max = ciSMALL_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseMedium
					Max = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseLarge
					Max = ciLARGE_WAREHOUSE_CRATE_CAPACITY
				BREAK
			ENDSWITCH
			IF total < 0
				total = 0
			ENDIF
			IF total >= Max
				total = Max-1
			ENDIF
			IF total < (Max-1)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, (total+1))
			ENDIF
			SET_WAREHOUSE_CRATES_STAT_INT(0, total, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_XLDIAM), TRUE, iSlot)
			
			INIT_SPECIAL_ITEM_GLOBAL_DATA()
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_GV")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_GV", "", iSlot)
			
		BREAK
		CASE QF_CONTRA_GIVE_SASQUATCH_HIDE
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED0, 4, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 1, iSlot)
			
			total = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0,iSlot, TRUE)
			Id = GET_MP_INT_CHARACTER_STAT(MP_STAT_WARHOUSESLOT0, iSlot, TRUE)
			size = GET_WAREHOUSE_SIZE(Id)
			
			SWITCH  size
				CASE eWarehouseSmall
					Max = ciSMALL_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseMedium
					Max = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseLarge
					Max = ciLARGE_WAREHOUSE_CRATE_CAPACITY
				BREAK
			ENDSWITCH
			IF total < 0
				total = 0
			ENDIF
			IF total >= Max
				total = Max-1
			ENDIF
			IF total < (Max-1)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, (total+1))
			ENDIF
			SET_WAREHOUSE_CRATES_STAT_INT(0, total, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_SHIDE), TRUE, iSlot)
			
			INIT_SPECIAL_ITEM_GLOBAL_DATA()
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_GV")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_GV", "", iSlot)
		BREAK
		CASE QF_CONTRA_GIVE_FILM_REEL
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED0, 5, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE0, 1, iSlot)
			
			total = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0,iSlot, TRUE)
			Id = GET_MP_INT_CHARACTER_STAT(MP_STAT_WARHOUSESLOT0, iSlot, TRUE)
			size = GET_WAREHOUSE_SIZE(Id)
			
			SWITCH  size
				CASE eWarehouseSmall
					Max = ciSMALL_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseMedium
					Max = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseLarge
					Max = ciLARGE_WAREHOUSE_CRATE_CAPACITY
				BREAK
			ENDSWITCH
			IF total < 0
				total = 0
			ENDIF
			IF total >= Max
				total = Max-1
			ENDIF
			IF total < (Max-1)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, (total+1))
			ENDIF
			SET_WAREHOUSE_CRATES_STAT_INT(0, total, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_FREEL), TRUE, iSlot)
			
			INIT_SPECIAL_ITEM_GLOBAL_DATA()
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_GV")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_GV", "", iSlot)
		BREAK
		CASE QF_CONTRA_GIVE_RARE_POCKET_WATCH
		
			SET_PACKED_STAT_INT(PACKED_MP_INT_SPECIALITEMSCOLLECTED1, 6, iSlot)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_SPCONTOTALFORWHOUSE1, 1, iSlot)
			
			total = GET_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0,iSlot, TRUE)
			Id = GET_MP_INT_CHARACTER_STAT(MP_STAT_WARHOUSESLOT0, iSlot, TRUE)
			size = GET_WAREHOUSE_SIZE(Id)
			
			SWITCH  size
				CASE eWarehouseSmall
					Max = ciSMALL_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseMedium
					Max = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				BREAK
				CASE eWarehouseLarge
					Max = ciLARGE_WAREHOUSE_CRATE_CAPACITY
				BREAK
			ENDSWITCH
			IF total < 0
				total = 0
			ENDIF
			IF total >= Max
				total = Max-1
			ENDIF
			IF total < (Max-1)
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_CONTOTALFORWHOUSE0, (total+1))
			ENDIF
			SET_WAREHOUSE_CRATES_STAT_INT(0, total, GET_WAREHOUSE_CRATE_MODEL_INDEX(EX_PROP_CRATE_WATCH), TRUE, iSlot)
			
			INIT_SPECIAL_ITEM_GLOBAL_DATA()
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_CONTRA_GV")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_CONTRA_GV", "", iSlot)
		BREAK


		CASE QF_CONTRA_OFFICENAME_RESET
			
			SET_MP_STRING_CHARACTER_STAT(MP_STAT_GB_GANG_NAME, "", iSlot)
			SET_MP_STRING_CHARACTER_STAT(MP_STAT_GB_GANG_NAME2, "", iSlot)
		
			SET_MP_STRING_CHARACTER_STAT(MP_STAT_GB_OFFICE_NAME, "", iSlot)
			SET_MP_STRING_CHARACTER_STAT(MP_STAT_GB_OFFICE_NAME2, "", iSlot)
			
			PLAYSTATS_QUICKFIX_TOOL(QF_PROGRESS_GIVE, "QFE_OFFICENAME")
			ADD_QUICKFIXFEED_ELEMENT(QF_PROGRESS_GIVE, "QFE_OFFICENAME", "", iSlot)
		BREAK
		
		CASE QF_HEIST3_AWD_SERIAL_KILLER_CLUES
			SET_PACKED_STAT_BOOL(PACKED_MP_SERIAL_KILLER_CLUE_1, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SERIAL_KILLER_CLUE_2, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SERIAL_KILLER_CLUE_3, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SERIAL_KILLER_CLUE_4, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SERIAL_KILLER_CLUE_5, TRUE)
		BREAK
		CASE QF_HEIST3_AWD_SIGNAL_JAMMER
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_0, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_1, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_2, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_3, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_4, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_5, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_6, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_7, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_8, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_9, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_10, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_11, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_12, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_13, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_14, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_15, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_16, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_17, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_18, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_19, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_20, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_21, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_22, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_23, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_24, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_25, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_26, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_27, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_28, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_29, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_30, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_31, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_32, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_33, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_34, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_35, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_36, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_37, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_38, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_39, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_40, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_41, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_42, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_43, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_44, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_45, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_46, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_47, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_48, TRUE)
			SET_PACKED_STAT_BOOL(PACKED_MP_SIGNAL_JAMMER_49, TRUE)
		BREAK
		
		CASE QF_FIXER_WEAPON_SKIN_APPISTOL
			SET_MP_WEAPON_ADDON_UNLOCKED(WEAPONCOMPONENT_DLC_APPISTOL_VARMOD_SECURITY, WEAPONTYPE_APPISTOL, TRUE, FALSE)
		BREAK
		CASE QF_FIXER_WEAPON_SKIN_PUMPSHOTGUN
			SET_MP_WEAPON_ADDON_UNLOCKED(WEAPONCOMPONENT_DLC_PUMPSHOTGUN_VARMOD_SECURITY, WEAPONTYPE_PUMPSHOTGUN, TRUE, FALSE)
		BREAK
		CASE QF_FIXER_WEAPON_SKIN_MICROSMG
			SET_MP_WEAPON_ADDON_UNLOCKED(WEAPONCOMPONENT_DLC_MICROSMG_VARMOD_SECURITY, WEAPONTYPE_MICROSMG, TRUE, FALSE)
		BREAK
		CASE QF_FIXER_WEAPON_SKIN_HEAVYRIFLE
			SET_MP_WEAPON_ADDON_UNLOCKED(WEAPONCOMPONENT_DLC_HEAVYRIFLE_CAMO, WEAPONTYPE_DLC_HEAVYRIFLE, TRUE, FALSE)
		BREAK
		
		#IF FEATURE_GEN9_EXCLUSIVE		
		CASE QF_GEN9_VEHICLE_LIVERY_MEMBER_DEVESTE_1
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEVESTE_MEMBER_LIVERY1, TRUE)
		BREAK
		CASE QF_GEN9_VEHICLE_LIVERY_MEMBER_DEVESTE_2
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEVESTE_MEMBER_LIVERY2, TRUE)
		BREAK
		CASE QF_GEN9_VEHICLE_LIVERY_MEMBER_BRIOSO_1
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRIOSO_MEMBER_LIVERY1, TRUE)
		BREAK
		CASE QF_GEN9_VEHICLE_LIVERY_MEMBER_BRIOSO_2
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRIOSO_MEMBER_LIVERY2, TRUE)
		BREAK
		CASE QF_GEN9_VEHICLE_LIVERY_TIME_TRIAL
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_HSW_TIME_TRIAL_LIVERY, TRUE)
		BREAK
		
		#IF FEATURE_DLC_1_2022
		CASE QF_GEN9_VEHICLE_LIVERY_MEMBER_CORSITA_1
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CORSITA_MEMBER_LIVERY1, TRUE)
		BREAK
		CASE QF_GEN9_VEHICLE_LIVERY_MEMBER_VIGERO2_1
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_VIGERO2_MEMBER_LIVERY1, TRUE)
		BREAK
		#ENDIF
		
		#ENDIF
	ENDSWITCH
		
		
	
	
ENDPROC





FUNC BOOL RUN_QUICK_FIX_GIVE_PROGRESS(INT CharacterSlot, INT& Stage, INT& ProgressNum)

	INT iSET_PROGRESS_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a PROGRES. 
	
	QUICK_FIX_PROGRESS WhichPROGRES = (INT_TO_ENUM(QUICK_FIX_PROGRESS, ProgressNum))
	
	SWITCH Stage

	
		CASE 0
			
			
			I = ProgressNum
			WHILE I < ENUM_TO_INT(QF_PROG_TOTAL_NUMBER)
				ProgressNum = I
				iSET_PROGRESS_CHANGE_VALUE_ADMIN = 0
				WhichPROGRES = (INT_TO_ENUM(QUICK_FIX_PROGRESS, ProgressNum))
			
				WhichStat = GET_ADMIN_PROGRESS_GV_ENUM(WhichPROGRES, CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_PROGRESS_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = ProgressNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						NET_NL()NET_PRINT("[QUICKFIX] Check Give PROGRESS ")NET_PRINT_INT(ProgressNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_QUICK_FIX_PROGRESS_STRING(WhichPROGRES))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_PROGRESS_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_PROGRESS_GV_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					IF iSET_PROGRESS_CHANGE_VALUE_ADMIN = -1
						NET_NL()NET_PRINT("[QUICKFIX] iSET_PROGRESS_CHANGE_VALUE_ADMIN = -1 !!! CHEATER !!! ")
						
					ELSE
			
						IF IS_BIT_SET(iSET_PROGRESS_CHANGE_VALUE_ADMIN, (ProgressNum%32))
							#IF IS_DEBUG_BUILD			
							NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Give PROGRES ")NET_PRINT_INT(ProgressNum)
							NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_QUICK_FIX_PROGRESS_NAME(WhichPROGRES))
							NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
							#ENDIF
							
							GIVE_CHARACTER_PROGRESS(WhichPROGRES, CharacterSlot)

						ENDIF
					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_PROG_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				ProgressNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVE_PROGRES finished checking PROGRESS for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			ProgressNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC






FUNC BOOL RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_PROGRESS(INT& Stages, INT& CheckStage, INT& ProgressNum, INT& CharacterSlot)


	SWITCH Stages

		CASE 0
			
//			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
//				IF RUN_QUICK_FIX_REMOVE_PROGRES(CharacterSlot, CheckStage, ProgressNum)
//					CharacterSlot++
//					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
//						CheckStage = 0
//						ProgressNum = 0
//						CharacterSlot = 0
//						Stages++
//					ENDIF
//				ENDIF
//			ELSE
//				NET_NL()NET_PRINT("[QUICKFIX] Remove PROGRES Character in slot ")NET_PRINT_INT(CharacterSlot)
//				NET_PRINT(" is not active, moving on from remove checks. ")
//				CharacterSlot++
//				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
//					CheckStage = 0
//					ProgressNum = 0
//					CharacterSlot = 0
//					Stages++
//				ENDIF
//			ENDIF
			
			Stages++
			
		BREAK
		
		
		CASE 1
			
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_GIVE_PROGRESS(CharacterSlot, CheckStage, ProgressNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						ProgressNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Give PROGRESS Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from remove checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					ProgressNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE 2
			RESET_ALL_QUICK_FIX_PROGRESS_STATS()
			RETURN TRUE
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC





FUNC BOOL RUN_QUICK_FIX_PROGRESS(QUICKFIX_DETAILS& Details)
	
	SWITCH Details.iQuickFixMainStages
	
		CASE 0
		
			IF RUN_QUICK_FIX_CHECK_GIVE_AND_REMOVE_PROGRESS(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				Details.iQuickFixMainStages++
			ENDIF
		BREAK
		

		
		CASE 1
			Details.iQuickFixStages = 0
			Details.iQuickFixMainStages = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC





PROC RESET_ALL_QUICK_FIX_DECORATION_STATS()

	NET_NL()NET_PRINT("[QUICKFIX] RESET_ALL_QUICK_FIX_DECORATION_STATS - called ")
	INT I
	FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_DECORATION_GV_BS_1, 0, I)
	ENDFOR
	

ENDPROC

PROC GIVE_CHARACTER_DECORATION(QUICK_FIX_DECORATION aDECORATION, INT iSlot)

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[QUICKFIX] GIVE_CHARACTER_DECORATION - called with DECORATION ")NET_PRINT(GET_QUICK_FIX_DECORATION_STRING(aDECORATION))
	NET_PRINT(" for slot ")NET_PRINT_INT(iSlot)NET_PRINT(" Active char is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
	#ENDIF
	
	SWITCH aDECORATION
	
		CASE QF_DECORATION_WA_GTA_LADY
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_13)
		BREAK
		CASE QF_DECORATION_WA_CAR_DECLASSE_2
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_65)
		BREAK
		CASE QF_DECORATION_WA_DIAMOND
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_22)
		BREAK
		CASE QF_DECORATION_WA_THE_WIZARDS_RUIN
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_77)
		BREAK
		CASE QF_DECORATION_WA_DEFENDER_OF_THE_FAITH
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_78)
		BREAK
		CASE QF_DECORATION_WA_MONKEYS_PARADISE
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_79)
		BREAK
		CASE QF_DECORATION_WA_PENETRATOR
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_80)
		BREAK
		CASE QF_DECORATION_WA_RACE_AND_CHASE_CROTCH_ROCKETS
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_81)
		BREAK
		CASE QF_DECORATION_WA_RACE_AND_CHASE_STREET_LEGAL
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_82)
		BREAK
		CASE QF_DECORATION_WA_RACE_AND_CHASE_GET_TRUCKIN
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_83)
		BREAK
		CASE QF_DECORATION_WA_BADLANDS_REVENGE_II
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_84)
		BREAK
		CASE QF_DECORATION_WA_SPACE_MONKEY_3
			SET_CASINO_DECORATION_AS_GIFTED(CPM_VW_AW_WV_85)
		BREAK
		
	ENDSWITCH
	STAT_SET_CURRENT_POSIX_TIME(GET_ADMIN_DECORATION_GV_POSIX_ENUM(iSlot))

ENDPROC

FUNC BOOL RUN_QUICK_FIX_GIVE_DECORATIONS(INT CharacterSlot, INT& Stage, INT& DecorationNum)

	INT iSET_DECORATION_CHANGE_VALUE_ADMIN
	STATSENUM WhichStat
	#IF IS_DEBUG_BUILD
	INT ModNum
	FLOAT BitNum
	#ENDIF
	INT I
	//Give a DECORATION. 
	
	QUICK_FIX_DECORATION  WhichDECORATION = (INT_TO_ENUM(QUICK_FIX_DECORATION, DecorationNum))
	
	SWITCH Stage

	
		CASE 0
			
			
			I = DecorationNum
			WHILE I < ENUM_TO_INT(QF_DECORATION_TOTAL_NUMBER)
				DecorationNum = I
				iSET_DECORATION_CHANGE_VALUE_ADMIN = 0
				WhichDECORATION = (INT_TO_ENUM(QUICK_FIX_DECORATION, DecorationNum))
			
				WhichStat = GET_ADMIN_DECORATION_GV_ENUM(WhichDECORATION, CharacterSlot)
				
				

				IF STAT_GET_INT(WhichStat, iSET_DECORATION_CHANGE_VALUE_ADMIN)
			
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_RunQuickFixToolPrints")
						ModNum = DecorationNum%32
						BitNum = POW(TO_FLOAT(2),TO_FLOAT(ModNum))
						NET_NL()NET_PRINT("[QUICKFIX] Check Give DECORATION ")NET_PRINT_INT(DecorationNum)
						NET_PRINT(" (mod is ")NET_PRINT_INT(ModNum)NET_PRINT(" index ")NET_PRINT_INT(FLOOR(BitNum))
						NET_PRINT(") Slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_QUICK_FIX_DECORATION_STRING(WhichDECORATION))
						NET_PRINT(" bitset has value ")NET_PRINT_INT(iSET_DECORATION_CHANGE_VALUE_ADMIN)NET_PRINT(" in stat ")NET_PRINT(GET_ADMIN_DECORATION_GV_BS_STRING(WhichStat))NET_NL()
					ENDIF
					#ENDIF
					
					IF iSET_DECORATION_CHANGE_VALUE_ADMIN = -1
						NET_NL()NET_PRINT("[QUICKFIX] iSET_DECORATION_CHANGE_VALUE_ADMIN = -1 !!! CHEATER !!! ")
						
					ELSE
			
						IF IS_BIT_SET(iSET_DECORATION_CHANGE_VALUE_ADMIN, (DecorationNum%32))
							#IF IS_DEBUG_BUILD			
							NET_NL()NET_PRINT("[QUICKFIX] Bit is set for Give DECORTION ")NET_PRINT_INT(DecorationNum)
							NET_PRINT(" slot ")NET_PRINT_INT(CharacterSlot)NET_PRINT(" Name = ")NET_PRINT(GET_QUICK_FIX_DECORATION_STRING(WhichDECORATION))
							NET_PRINT(" Active Character is ")NET_PRINT_INT(GET_ACTIVE_CHARACTER_SLOT())NET_NL()
							#ENDIF
							
							GIVE_CHARACTER_DECORATION(WhichDECORATION, CharacterSlot)

						ENDIF
					ENDIF
					
				ELSE
					Stage = 1
					I = ENUM_TO_INT(QF_DECORATION_TOTAL_NUMBER)
				ENDIF
				I++

				
			ENDWHILE
			
			IF Stage = 0
				DecorationNum = 0
				Stage = 1
			ENDIF

		BREAK

		CASE 1
			NET_NL()NET_PRINT("[QUICKFIX] RUN_QUICK_FIX_CHECK_GIVE_DECORATION finished checking DECORATION for Char ")NET_PRINT_INT(CharacterSlot)NET_PRINT(", moving onto the next char. ")
			NET_NL()NET_PRINT("[QUICKFIX]")
			DecorationNum = 0
			Stage = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	
	RETURN FALSE

ENDFUNC

FUNC BOOL RUN_QUICK_FIX_CHECK_GIVE_DECORATIONS(INT& Stages, INT& CheckStage, INT& DecorationNum, INT& CharacterSlot)


	SWITCH Stages

		CASE 0
			
			IF IS_STAT_CHARACTER_ACTIVE(CharacterSlot)
				IF RUN_QUICK_FIX_GIVE_DECORATIONS(CharacterSlot, CheckStage, DecorationNum)
					CharacterSlot++
					IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
						CheckStage = 0
						DecorationNum = 0
						CharacterSlot = 0
						Stages++
					ENDIF
				ENDIF
			ELSE
				NET_NL()NET_PRINT("[QUICKFIX] Give DECORATIONS Character in slot ")NET_PRINT_INT(CharacterSlot)
				NET_PRINT(" is not active, moving on from decoration checks. ")
				CharacterSlot++
				IF CharacterSlot = MAX_NUM_CHARACTER_SLOTS
					CheckStage = 0
					DecorationNum = 0
					CharacterSlot = 0
					Stages++
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE 1
			RESET_ALL_QUICK_FIX_DECORATION_STATS()
			RETURN TRUE
		BREAK

	ENDSWITCH

	RETURN FALSE

ENDFUNC

FUNC BOOL RUN_QUICK_FIX_DECORATIONS(QUICKFIX_DETAILS& Details)
	
	SWITCH Details.iQuickFixMainStages
	
		CASE 0
		
			IF RUN_QUICK_FIX_CHECK_GIVE_DECORATIONS(Details.iQuickFixStages,Details.iQuickCheckStages,Details.iQuickIndex, Details.iQuickCharacterSlot)
				Details.iQuickFixMainStages++
			ENDIF
		BREAK
		

		
		CASE 1
			Details.iQuickFixStages = 0
			Details.iQuickFixMainStages = 0
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC



/// PURPOSE:
///    To enable new accounts to be quickly set up with a large number of limited GTAO items
///    url:bugstar:2698166
PROC QUICKFIX_GIFT_LIMITED_ITEMS_TO_NEW_ACCOUNTS()
	NET_NL()NET_PRINT("[QUICKFIX] QUICKFIX_GIFT_LIMITED_ITEMS_TO_NEW_ACCOUNTS - called ")
	
	/*
	INT i
	REPEAT MAX_NUM_CHARACTER_SLOTS i
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_WEAPON_GV_BS_2, 12288, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_1, -4, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_2, 260096, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_3, 16383, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_5, 536879103, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_6, 16775168, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_7, 1073479680, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_8, -268419072, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_9, 255, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_11, -134217728, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_12, -32769, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_13, 2147483647, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_14, 2147483647, i)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ADMIN_CLOTHES_GV_BS_15, 134217727, i)
	ENDREPEAT
	*/
	
	STAT_SET_INT(MP0_ADMIN_WEAPON_GV_BS_2, 12288)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_1, -4)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_2, 260096)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_3, 16383)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_5, 536879103)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_6, 16775168)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_7, 1073479680)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_8, -268419072)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_9, 255)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_11, -134217728)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_12, -32769)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_13, 2147483647)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_14, 2147483647)
	STAT_SET_INT(MP0_ADMIN_CLOTHES_GV_BS_15, 134217727)
	
	STAT_SET_INT(MP1_ADMIN_WEAPON_GV_BS_2, 12288)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_1, -4)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_2, 260096)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_3, 16383)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_5, 536879103)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_6, 16775168)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_7, 1073479680)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_8, -268419072)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_9, 255)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_11, -134217728)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_12, -32769)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_13, 2147483647)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_14, 2147483647)
	STAT_SET_INT(MP1_ADMIN_CLOTHES_GV_BS_15, 134217727)
ENDPROC


