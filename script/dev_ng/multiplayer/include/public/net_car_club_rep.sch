//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CAR_CLUB_REP.sch																					//
// Description: Header file containing functionality to track a players car club reputation.							//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		16/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_TUNER
USING "net_include.sch"
USING "net_ticker_logic.sch"
USING "net_car_club_rep_rewards.sch"
USING "shared_hud_displays.sch"
USING "ped_component_public.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ TIER PROGRESSION ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Returns the number of rep required to rank up to the next tier, e.g. 3 -> 4.
/// PARAMS:
///    iTier - Tier to query.
/// RETURNS: Rep required to rank up to the next tier, as an INT.
FUNC INT GET_CAR_CLUB_REP_FOR_TIER(INT iTier)
	
	IF (iTier <= 1)
		RETURN 0
		
	ELIF (iTier = 2)
		RETURN (iTier*50)
		
	ELIF (iTier >= MAX_NUM_CAR_CLUB_FORMULA_TIERS)
		RETURN g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_1000
	ENDIF
	
	RETURN ((iTier-1)*50 - (((iTier-4)+1)*(50-g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200)))
ENDFUNC

/// PURPOSE:
///    Main function for returning the total rep required for a tier.
///    Uses a formula to calculate instead of looping through all previous tiers.
///    Differs from GET_CAR_CLUB_REP_FOR_TIER as that function only gets the rep needed between tiers.
/// PARAMS:
///    iTier - Tier to query.
/// RETURNS: The total rep required for a tier, as an INT.
FUNC INT GET_CAR_CLUB_TOTAL_REP_FOR_TIER_MAIN(INT iTier)
	
	// Note: Rep 0, 1 & 2 are exceptions to the rules as they don't follow the same sequence.
	//		 
	//		 Formula for calculating total rep for tier:
	//		 	Total Rep = (T(100.0)-100.0) + (T-3.0)((F(T-3.0))+F)
	//			
	//			T = Tier
	//			F = Incremental Factor // TO_FLOAT(g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200)/2.0
	//
	//		Whole Number 	= (T(100.0)-100.0)
	// 		Offset 			= (T-3.0)((F(T-3.0))+F)
	//		Total Rep		= Whole Number + Offset
	//
	//		We do (T-3) as 3 is the first Tier to follow the sequence.
	
	FLOAT fTier 				= TO_FLOAT(iTier)
	FLOAT fIncrementalFactor	= TO_FLOAT(g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200)/2.0
	
	RETURN ROUND((fTier*100.0)-100.0 + (fTier-3.0)*((fIncrementalFactor*(fTier-3.0))+fIncrementalFactor))
ENDFUNC

/// PURPOSE:
///    Returns the total rep required for a tier.
///    The rep needed to complete the current level increases by g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200 each tier.
/// PARAMS:
///    iTier - Tier to query.
/// RETURNS: The total rep required for a tier, as an INT.
FUNC INT GET_CAR_CLUB_REP_TOTAL_FOR_TIER(INT iTier)
	
	IF (iTier <= 1)
		RETURN 0
		
	ELIF (iTier >= MAX_NUM_CAR_CLUB_FORMULA_TIERS)
		RETURN ((iTier-(MAX_NUM_CAR_CLUB_FORMULA_TIERS-1))*g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_1000)+GET_CAR_CLUB_TOTAL_REP_FOR_TIER_MAIN(MAX_NUM_CAR_CLUB_FORMULA_TIERS-1)
	ENDIF
	
	RETURN GET_CAR_CLUB_TOTAL_REP_FOR_TIER_MAIN(iTier)
ENDFUNC

/// PURPOSE:
///    Returns the current tier based on total car club rep.
///    Reverse engineers the formula for calculating the total rep needed for a tier: GET_CAR_CLUB_REP_TOTAL_FOR_TIER
///    Then uses quadratic equations to solve what tier we are on.
/// PARAMS:
///    iCarClubRep - Total car club rep.
/// RETURNS: The current tier based on car club rep, as an INT.
FUNC INT GET_CAR_CLUB_REP_TIER(INT iCarClubRep)
	
	// Note: Rep 0, 1 & 2 are exceptions to the rules as they don't follow the same sequence.
	//		 
	//		 Formula for calculating total rep for tier:
	//		 	Total Rep = (T(100.0)-100.0) + (T-3.0)((F(T-3.0))+F)
	//			
	//			T = Tier
	//			F = Incremental Factor // TO_FLOAT(g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200)/2.0
	//
	//		 When working this formula to conclusion, we end up with three values; A, B & C.
	//		 We can then use these values in a quadratic equation to work out which tier we are on.
	//		 The below formulas are reverse engineered to work out what A, B & C should be:
	//
	//		 Offset = -(F*3)+F
	//		 A = F
	//		 B = (-(F*3)+Offset)+100.0
	//		 C = (-3*Offset)-100.0
	//		 C = rep-C
	//
	//		 We can then put these values into a quadratic equation and floor the result to get the tier.
	
	INT iMaxFormulaRep = GET_CAR_CLUB_TOTAL_REP_FOR_TIER_MAIN(MAX_NUM_CAR_CLUB_FORMULA_TIERS-1)
	
	// For tiers 0, 1 & 2
	IF (iCarClubRep < 100)
		RETURN 1
	
	// For tiers that dont use the formula
	ELIF (iCarClubRep > iMaxFormulaRep)
		FLOAT fRepDiff 	= TO_FLOAT(iCarClubRep)-TO_FLOAT(iMaxFormulaRep)
		INT iTiersPlus 	= FLOOR(fRepDiff/TO_FLOAT(g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_1000))
		RETURN (MAX_NUM_CAR_CLUB_FORMULA_TIERS-1)+iTiersPlus
	ENDIF
	
	// Calculate A, B & C for our quadratic equation
	FLOAT fIncrementalFactor 	= TO_FLOAT(g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200)/2.0
	FLOAT fOffset 				= -(fIncrementalFactor*3.0)+fIncrementalFactor
	
	FLOAT fA = fIncrementalFactor
	FLOAT fB = (-(fIncrementalFactor*3.0)+fOffset)+100.0
	FLOAT fC = (-3.0*fOffset)-100.0
	fC = iCarClubRep-fC
	
	RETURN FLOOR(QUADRATIC_EQUATION_FLOAT(-fA, -fB, fC, TRUE))
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ TELEMETRY ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_CAR_CLUB_EARN_REP_TYPE_STRING(EARN_CAR_CLUB_REP_TYPE eRepEarnType)
	STRING sEarnRepType = ""
	SWITCH eRepEarnType
		CASE EARN_REP_TYPE_INVALID								sEarnRepType = "EARN_REP_TYPE_INVALID"								BREAK
		CASE EARN_REP_TYPE_FTB_SPRINT_RACE						sEarnRepType = "EARN_REP_TYPE_FTB_SPRINT_RACE"						BREAK
		CASE EARN_REP_TYPE_FTB_STREET_RACE						sEarnRepType = "EARN_REP_TYPE_FTB_STREET_RACE"						BREAK
		CASE EARN_REP_TYPE_FTB_PURSUIT_SERIES					sEarnRepType = "EARN_REP_TYPE_FTB_PURSUIT_SERIES"					BREAK
		CASE EARN_REP_TYPE_FTB_CAR_CLUB_MOD_VEH					sEarnRepType = "EARN_REP_TYPE_FTB_CAR_CLUB_MOD_VEH"					BREAK
		CASE EARN_REP_TYPE_FTB_SANDBOX_DRIVE_TEST_CAR			sEarnRepType = "EARN_REP_TYPE_FTB_SANDBOX_DRIVE_TEST_CAR"			BREAK
		CASE EARN_REP_TYPE_FTB_TIME_TRIAL						sEarnRepType = "EARN_REP_TYPE_FTB_TIME_TRIAL"						BREAK
		CASE EARN_REP_TYPE_FTB_CHECKPOINT_DASH					sEarnRepType = "EARN_REP_TYPE_FTB_CHECKPOINT_DASH"					BREAK
		CASE EARN_REP_TYPE_FTB_HEAD_TO_HEAD						sEarnRepType = "EARN_REP_TYPE_FTB_HEAD_TO_HEAD"						BREAK
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB					sEarnRepType = "EARN_REP_TYPE_DB_VISIT_CAR_CLUB"					BREAK
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB_PERS_VEH			sEarnRepType = "EARN_REP_TYPE_DB_VISIT_CAR_CLUB_PERS_VEH"			BREAK
		CASE EARN_REP_TYPE_DB_SPRINT_RACE						sEarnRepType = "EARN_REP_TYPE_DB_SPRINT_RACE"						BREAK
		CASE EARN_REP_TYPE_DB_STREET_RACE						sEarnRepType = "EARN_REP_TYPE_DB_STREET_RACE"						BREAK
		CASE EARN_REP_TYPE_DB_PURSUIT_SERIES					sEarnRepType = "EARN_REP_TYPE_DB_PURSUIT_SERIES"					BREAK
		CASE EARN_REP_TYPE_DB_CAR_CLUB_MOD_VEH					sEarnRepType = "EARN_REP_TYPE_DB_CAR_CLUB_MOD_VEH"					BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_TEST_CAR			sEarnRepType = "EARN_REP_TYPE_DB_SANDBOX_DRIVE_TEST_CAR"			BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_PERS_VEH			sEarnRepType = "EARN_REP_TYPE_DB_SANDBOX_DRIVE_PERS_VEH"			BREAK
		CASE EARN_REP_TYPE_DB_TIME_TRIAL						sEarnRepType = "EARN_REP_TYPE_DB_TIME_TRIAL"						BREAK
		CASE EARN_REP_TYPE_DB_CHECKPOINT_DASH					sEarnRepType = "EARN_REP_TYPE_DB_CHECKPOINT_DASH"					BREAK
		CASE EARN_REP_TYPE_DB_HEAD_TO_HEAD						sEarnRepType = "EARN_REP_TYPE_DB_HEAD_TO_HEAD"						BREAK
		CASE EARN_REP_TYPE_RACE_SPRINT							sEarnRepType = "EARN_REP_TYPE_RACE_SPRINT"							BREAK
		CASE EARN_REP_TYPE_RACE_STREET							sEarnRepType = "EARN_REP_TYPE_RACE_STREET"							BREAK
		CASE EARN_REP_TYPE_RACE_PURSUIT							sEarnRepType = "EARN_REP_TYPE_RACE_PURSUIT"							BREAK
		CASE EARN_REP_TYPE_RACE_CHECKPOINT_DASH					sEarnRepType = "EARN_REP_TYPE_RACE_CHECKPOINT_DASH"					BREAK
		CASE EARN_REP_TYPE_RACE_HEAD_TO_HEAD					sEarnRepType = "EARN_REP_TYPE_RACE_HEAD_TO_HEAD"					BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS				sEarnRepType = "EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS"				BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS				sEarnRepType = "EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS"				BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS				sEarnRepType = "EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS"				BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET					sEarnRepType = "EARN_REP_TYPE_TIME_SPENT_CAR_MEET"					BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX					sEarnRepType = "EARN_REP_TYPE_TIME_SPENT_SANDBOX"					BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH			sEarnRepType = "EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH"			BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH				sEarnRepType = "EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH"				BREAK
		CASE EARN_REP_TYPE_CAR_MEET_MERCH_SHOP					sEarnRepType = "EARN_REP_TYPE_CAR_MEET_MERCH_SHOP"					BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_PREP				sEarnRepType = "EARN_REP_TYPE_AUTO_SHOP_CONTRACT_PREP"				BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE			sEarnRepType = "EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE"			BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY			sEarnRepType = "EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY"			BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY	sEarnRepType = "EARN_REP_TYPE_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY"	BREAK
		
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_FIRST_TIME	sEarnRepType = "EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_FIRST_TIME"	BREAK
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_DAILY			sEarnRepType = "EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_DAILY"			BREAK
		#ENDIF
		
		CASE EARN_REP_TYPE_MAX									sEarnRepType = "EARN_REP_TYPE_MAX"									BREAK
	ENDSWITCH
	RETURN sEarnRepType
ENDFUNC

PROC DEBUG_PRINT_ADD_CAR_CLUB_REP(EARN_CAR_CLUB_REP_TYPE eRepEarnType, INT iRepAmount)
	PRINTLN("[CAR_CLUB_REP] DEBUG_PRINT_ADD_CAR_CLUB_REP - Rep Earn Type: ", DEBUG_GET_CAR_CLUB_EARN_REP_TYPE_STRING(eRepEarnType), " Rep Amount: ", iRepAmount)
ENDPROC
#ENDIF //IS_DEBUG_BUILD

FUNC INT GET_CAR_CLUB_REP_EARN_TYPE_HASH(EARN_CAR_CLUB_REP_TYPE eRepEarnType)
	INT iRepEarnTypeHash = -1
	SWITCH eRepEarnType
		// First time bonuses
		CASE EARN_REP_TYPE_FTB_SPRINT_RACE						iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_SPRINT_RACE")						BREAK
		CASE EARN_REP_TYPE_FTB_STREET_RACE						iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_STREET_RACE")						BREAK
		CASE EARN_REP_TYPE_FTB_PURSUIT_SERIES					iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_PURSUIT_SERIES")						BREAK
		CASE EARN_REP_TYPE_FTB_CAR_CLUB_MOD_VEH					iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_CAR_CLUB_MOD_VEH")					BREAK
		CASE EARN_REP_TYPE_FTB_SANDBOX_DRIVE_TEST_CAR			iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_SANDBOX_DRIVE_TEST_CAR")				BREAK
		CASE EARN_REP_TYPE_FTB_TIME_TRIAL						iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_TIME_TRIAL")							BREAK
		CASE EARN_REP_TYPE_FTB_CHECKPOINT_DASH					iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_CHECKPOINT_DASH")					BREAK
		CASE EARN_REP_TYPE_FTB_HEAD_TO_HEAD						iRepEarnTypeHash = HASH("EARN_REP_TYPE_FTB_HEAD_TO_HEAD")						BREAK
		
		// Daily bonuses
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB					iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_VISIT_CAR_CLUB")						BREAK
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB_PERS_VEH			iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_VISIT_CAR_CLUB_PERS_VEH")				BREAK
		CASE EARN_REP_TYPE_DB_SPRINT_RACE						iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_SPRINT_RACE")							BREAK
		CASE EARN_REP_TYPE_DB_STREET_RACE						iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_STREET_RACE")							BREAK
		CASE EARN_REP_TYPE_DB_PURSUIT_SERIES					iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_PURSUIT_SERIES")						BREAK
		CASE EARN_REP_TYPE_DB_CAR_CLUB_MOD_VEH					iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_CAR_CLUB_MOD_VEH")					BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_TEST_CAR			iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_SANDBOX_DRIVE_TEST_CAR")				BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_PERS_VEH			iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_SANDBOX_DRIVE_PERS_VEH")				BREAK
		CASE EARN_REP_TYPE_DB_TIME_TRIAL						iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_TIME_TRIAL")							BREAK
		CASE EARN_REP_TYPE_DB_CHECKPOINT_DASH					iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_CHECKPOINT_DASH")						BREAK
		CASE EARN_REP_TYPE_DB_HEAD_TO_HEAD						iRepEarnTypeHash = HASH("EARN_REP_TYPE_DB_HEAD_TO_HEAD")						BREAK
		
		// Races
		CASE EARN_REP_TYPE_RACE_SPRINT							iRepEarnTypeHash = HASH("EARN_REP_TYPE_RACE_SPRINT")							BREAK
		CASE EARN_REP_TYPE_RACE_STREET							iRepEarnTypeHash = HASH("EARN_REP_TYPE_RACE_STREET")							BREAK
		CASE EARN_REP_TYPE_RACE_PURSUIT							iRepEarnTypeHash = HASH("EARN_REP_TYPE_RACE_PURSUIT")							BREAK
		CASE EARN_REP_TYPE_RACE_CHECKPOINT_DASH					iRepEarnTypeHash = HASH("EARN_REP_TYPE_RACE_CHECKPOINT_DASH")					BREAK
		CASE EARN_REP_TYPE_RACE_HEAD_TO_HEAD					iRepEarnTypeHash = HASH("EARN_REP_TYPE_RACE_HEAD_TO_HEAD")						BREAK
		
		// Visit car club streaks
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS				iRepEarnTypeHash = HASH("EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS")					BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS				iRepEarnTypeHash = HASH("EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS")					BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS				iRepEarnTypeHash = HASH("EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS")					BREAK
		
		// Misc
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET					iRepEarnTypeHash = HASH("EARN_REP_TYPE_TIME_SPENT_CAR_MEET")					BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX					iRepEarnTypeHash = HASH("EARN_REP_TYPE_TIME_SPENT_SANDBOX")						BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH			iRepEarnTypeHash = HASH("EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH")				BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH				iRepEarnTypeHash = HASH("EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH")				BREAK
		CASE EARN_REP_TYPE_CAR_MEET_MERCH_SHOP					iRepEarnTypeHash = HASH("EARN_REP_TYPE_CAR_MEET_MERCH_SHOP")					BREAK
		
		// Auto Shop
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_PREP				iRepEarnTypeHash = HASH("EARN_REP_TYPE_AUTO_SHOP_CONTRACT_PREP")				BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE			iRepEarnTypeHash = HASH("EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE")				BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY			iRepEarnTypeHash = HASH("EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY")			BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY	iRepEarnTypeHash = HASH("EARN_REP_TYPE_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY")		BREAK
		
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_FIRST_TIME	iRepEarnTypeHash = HASH("EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_FIRST_TIME")		BREAK
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_DAILY			iRepEarnTypeHash = HASH("EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_DAILY")			BREAK
		#ENDIF
	ENDSWITCH
	RETURN iRepEarnTypeHash
ENDFUNC

FUNC INT GET_CAR_CLUB_REP_MATCH_ID(EARN_CAR_CLUB_REP_TYPE eRepEarnType)
	INT iMatchID = -1
	SWITCH eRepEarnType
		CASE EARN_REP_TYPE_FTB_SPRINT_RACE
		CASE EARN_REP_TYPE_FTB_STREET_RACE
		CASE EARN_REP_TYPE_FTB_PURSUIT_SERIES
		CASE EARN_REP_TYPE_FTB_TIME_TRIAL
		CASE EARN_REP_TYPE_FTB_CHECKPOINT_DASH
		CASE EARN_REP_TYPE_FTB_HEAD_TO_HEAD
		CASE EARN_REP_TYPE_DB_SPRINT_RACE
		CASE EARN_REP_TYPE_DB_STREET_RACE
		CASE EARN_REP_TYPE_DB_PURSUIT_SERIES
		CASE EARN_REP_TYPE_DB_TIME_TRIAL
		CASE EARN_REP_TYPE_DB_CHECKPOINT_DASH
		CASE EARN_REP_TYPE_DB_HEAD_TO_HEAD
		CASE EARN_REP_TYPE_RACE_SPRINT
		CASE EARN_REP_TYPE_RACE_STREET
		CASE EARN_REP_TYPE_RACE_PURSUIT
		CASE EARN_REP_TYPE_RACE_CHECKPOINT_DASH
		CASE EARN_REP_TYPE_RACE_HEAD_TO_HEAD
			iMatchID = GB_GET_SESSION_ID_FOR_TELEMETRY()
		BREAK
	ENDSWITCH
	RETURN iMatchID
ENDFUNC

/// PURPOSE:
///    Sends the car club rep metric.
/// PARAMS:
///    iCarClubRepToAdd - Car club rep points added.
///    iCarClubRepTier - Car club rep tier after new points have been added.
///    bCarClubRepTierLevelUp - Has the car club rep tier increased.
PROC SEND_CAR_CLUB_REP_METRIC(INT iCarClubRepToAdd, INT iCarClubRepTier, BOOL bCarClubRepTierLevelUp, EARN_CAR_CLUB_REP_TYPE eRepEarnType, INT iPurchasedItemHash)

	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - iCarClubRepToAdd:", iCarClubRepToAdd, " iCarClubRepTier: ", iCarClubRepTier, " eRepEarnType: ", DEBUG_GET_CAR_CLUB_EARN_REP_TYPE_STRING(eRepEarnType), " iPurchasedItemHash: ", iPurchasedItemHash)
	
	STRUCT_CARCLUB_POINTS metricData
	
	metricData.boss_id1		= GB_GET_GANG_BOSS_UUID_INT_A(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	metricData.boss_id2 	= GB_GET_GANG_BOSS_UUID_INT_B(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	metricData.match_id 	= GET_CAR_CLUB_REP_MATCH_ID(eRepEarnType)
	metricData.method		= GET_CAR_CLUB_REP_EARN_TYPE_HASH(eRepEarnType)
	metricData.amount		= iCarClubRepToAdd
	metricData.itemBought	= iPurchasedItemHash
	metricData.memberLevel	= iCarClubRepTier
	metricData.levelUp		= bCarClubRepTierLevelUp
	
	SWITCH eRepEarnType
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS	metricData.streak = 7	BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS	metricData.streak = 14	BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS	metricData.streak = 30	BREAK
		DEFAULT										metricData.streak = -1	BREAK
	ENDSWITCH
	
	PLAYSTATS_CARCLUB_POINTS(metricData)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - Sending metric: ")
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - boss_id1: ", metricData.boss_id1)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - boss_id2: ", metricData.boss_id2)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - match_id: ", metricData.match_id)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - method: ", metricData.method)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - amount: ", metricData.amount)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - itemBought: ", metricData.itemBought)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - memberLevel: ", metricData.memberLevel)
	PRINTLN("[CAR_CLUB_REP] SEND_CAR_CLUB_REP_METRIC - levelUp: ", metricData.levelUp)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ STATS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Returns the local players car club rep. Uses the stat value.
/// RETURNS: The local players car club rep as an INT.
FUNC INT GET_LOCAL_PLAYERS_CAR_CLUB_REP()
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_CAR_CLUB_REP)
ENDFUNC

/// PURPOSE:
///    Returns the local players car club rep tier.
/// RETURNS: The local players car club rep tier as an INT.
FUNC INT GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
	RETURN GET_CAR_CLUB_REP_TIER(GET_LOCAL_PLAYERS_CAR_CLUB_REP())
ENDFUNC

/// PURPOSE:
///    Returns a players car club rep. Uses global broadcast data.
/// PARAMS:
///    playerToCheck - Player ID to check.
/// RETURNS: A players car club rep as an INT.
FUNC INT GET_PLAYERS_CAR_CLUB_REP(PLAYER_INDEX playerToCheck)
	IF (playerToCheck = INVALID_PLAYER_INDEX())
		RETURN 0
	ENDIF
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.sCarMeetPropertyData.iCarClubRep
ENDFUNC

/// PURPOSE:
///    Returns a players car club rep tier. Uses global broadcast data.
/// PARAMS:
///    playerToCheck - Player ID to check.
/// RETURNS: A players car club rep tier as an INT.
FUNC INT GET_PLAYERS_CAR_CLUB_REP_TIER(PLAYER_INDEX playerToCheck)
	IF (playerToCheck = INVALID_PLAYER_INDEX())
		RETURN 1
	ENDIF
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.sCarMeetPropertyData.iCarClubTier
ENDFUNC

/// PURPOSE:
///    Returns the maximum car club rep the player can achieve. 
///    Tier 999 (one point behind tier 1000 so the UI bar is full bar tier 999).
/// RETURNS: The maximum car club rep as an INT.
FUNC INT GET_MAX_CAR_CLUB_REP()
	RETURN GET_CAR_CLUB_REP_TOTAL_FOR_TIER(MAX_NUM_CAR_CLUB_REP_TIERS+1)-1
ENDFUNC

/// PURPOSE:
///    Gets a vehicle eligible for trade price reward via membership progression.
/// PARAMS:
///    iTunerRewardVehicle - Int usually passed in via a loop.
FUNC MODEL_NAMES PROGRESSION_REWARD_VEHICLE_TRADE_PRICE__GET_VEHICLE_MODEL(INT iTunerRewardVehicle)
	SWITCH iTunerRewardVehicle
		// Launch
		CASE 0			RETURN SULTAN3
		CASE 1			RETURN RT3000
		CASE 2			RETURN VECTRE
		CASE 3			RETURN ZR350
		CASE 4			RETURN WARRENER2
		CASE 5			RETURN CALICO
		CASE 6			RETURN REMUS
		CASE 7			RETURN CYPHER
		// Embedded
		CASE 8			RETURN DOMINATOR7	// Week 2
		CASE 9			RETURN JESTER4		// Week 3
		CASE 10			RETURN FUTO2		// Week 4
		CASE 11			RETURN DOMINATOR8	// Week 5
		CASE 12			RETURN PREVION		// Week 6
		CASE 13			RETURN GROWLER		// Week 7
		CASE 14			RETURN COMET6		// Week 8
		#IF FEATURE_DLC_1_2022
		CASE 15			RETURN POSTLUDE
		CASE 16			RETURN KANJOSJ
		#ENDIF
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC BOOL HAS_UNLOCKED_TRADE_PRICES_FOR_ALL_LAUNCH_DAY_TUNER_VEHICLES()
	MODEL_NAMES eVehicleModel
	BOOL bUnlockedVeh
	
	INT i
	REPEAT MAX_NUM_CAR_CLUB_TIER_TRADE_PRICE_UNLOCKS i
		eVehicleModel = PROGRESSION_REWARD_VEHICLE_TRADE_PRICE__GET_VEHICLE_MODEL(i)
		
		IF IS_THIS_VEHICLE_A_LAUNCH_DAY_TUNER_VEHICLE(eVehicleModel)
			bUnlockedVeh = IS_TUNER_VEHICLE_REWARD_UNLOCKED(eVehicleModel)
			
			IF NOT bUnlockedVeh
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Manages the Vehicle Trade Price awards from the membership. Unlocks a random trade price every 5 frames. Called for 1 frame.
FUNC BOOL ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE(MODEL_NAMES &eVehModel)
	
	INT i
	MODEL_NAMES eVehicleModel
	BOOL bUnlockedVeh[MAX_NUM_CAR_CLUB_TIER_TRADE_PRICE_UNLOCKS]
	BOOL bAllVehiclesUnlocked = TRUE
	
	// ----- Get all the vehicles that are already unlocked -----
	PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] **************************")
	REPEAT MAX_NUM_CAR_CLUB_TIER_TRADE_PRICE_UNLOCKS i
		eVehicleModel = PROGRESSION_REWARD_VEHICLE_TRADE_PRICE__GET_VEHICLE_MODEL(i)
		bUnlockedVeh[i] = IS_TUNER_VEHICLE_REWARD_UNLOCKED(eVehicleModel)
		PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Vehicle ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), " is already unlocked: ", GET_STRING_FROM_BOOL(bUnlockedVeh[i]))
		
		IF NOT bUnlockedVeh[i]
			bAllVehiclesUnlocked = FALSE
		ENDIF
	ENDREPEAT
	PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] **************************")
	
	// ----- Exit now if all trade prices are unlocked -----
	IF bAllVehiclesUnlocked
		PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] All tuner vehicle trade prices are unlocked. Exiting.")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Unlocking a trade price for a random tuner vehicle!")
	
	// ----- Only generate number from available unlocks pool -----
	INT iLockedVehCount
	INT iAvailableUnlocks[MAX_NUM_CAR_CLUB_TIER_TRADE_PRICE_UNLOCKS]
	BOOL bOkayToUnlockVehicle
	REPEAT MAX_NUM_CAR_CLUB_TIER_TRADE_PRICE_UNLOCKS i
	
		// Make sure all launch day vehicles are unlocked first. 
		eVehicleModel = PROGRESSION_REWARD_VEHICLE_TRADE_PRICE__GET_VEHICLE_MODEL(i)
		IF NOT IS_THIS_VEHICLE_A_LAUNCH_DAY_TUNER_VEHICLE(eVehicleModel)
			IF NOT HAS_UNLOCKED_TRADE_PRICES_FOR_ALL_LAUNCH_DAY_TUNER_VEHICLES()
				PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Player has not unlocked all launch day vehicles yet. Setting this veh to not available yet: ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel))
				bOkayToUnlockVehicle = FALSE
			ELSE
				bOkayToUnlockVehicle = TRUE
			ENDIF
		ELSE
			bOkayToUnlockVehicle = TRUE
		ENDIF
	
		IF NOT bUnlockedVeh[i]
		AND bOkayToUnlockVehicle
			iAvailableUnlocks[i] = iLockedVehCount
			iLockedVehCount++
		ELSE
			iAvailableUnlocks[i] = -1
		ENDIF
	ENDREPEAT
	
	// ----- Unlock random vehicle trade price -----
	INT iRandomNumber = GET_RANDOM_INT_IN_RANGE(0, iLockedVehCount)
	PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Randomly picking number: ", iRandomNumber, " iLockedVehCount: ", iLockedVehCount)
	
	REPEAT MAX_NUM_CAR_CLUB_TIER_TRADE_PRICE_UNLOCKS i
		IF iAvailableUnlocks[i] > -1
		AND iAvailableUnlocks[i] = iRandomNumber
			eVehicleModel = PROGRESSION_REWARD_VEHICLE_TRADE_PRICE__GET_VEHICLE_MODEL(i)
			SET_TUNER_VEHICLE_PROGRESSION_REWARD_UNLOCKED(eVehicleModel)
			PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Unlocking vehicle trade price for vehicle: ", GET_MODEL_NAME_FOR_DEBUG(eVehicleModel), "! :)")
			
			IF IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME(eVehicleModel)
				eVehModel = eVehicleModel
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CAR_CLUB_REP_FREEMODE_HELP_TEXT(BOOL bDisplayHelpText)
	
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_HELP_TEXT)
	OR NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR NOT bDisplayHelpText
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		EXIT
	ENDIF
	
	INT iRepIncreaseHelpText = GET_PACKED_STAT_INT(PACKED_MP_INT_CAR_CLUB_REP_INCREASE_HELP_TEXT)
	
	IF NOT g_bCarClubRepIncreasedDisplayHelpText						// Once per boot
	AND iRepIncreaseHelpText < MAX_NUM_CAR_CLUB_REP_INCREASE_HELP_TEXT	// Three times max
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_HELP_TEXT)
			SET_PACKED_STAT_INT(PACKED_MP_INT_CAR_CLUB_REP_INCREASE_HELP_TEXT, iRepIncreaseHelpText+1)
			g_bCarClubRepIncreasedDisplayHelpText = TRUE
			
			#IF NOT FEATURE_GEN9_EXCLUSIVE
			PRINT_HELP("CCR_INC_HT")
			#ENDIF
			
			#IF FEATURE_GEN9_EXCLUSIVE
			PRINT_HELP("CCR_INC_HT_G9")
			#ENDIF
		ENDIF
	ELSE
		// Can't display help text so clear bit
		CLEAR_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_HELP_TEXT)
	ENDIF
	
ENDPROC

PROC MAINTAIN_CAR_CLUB_REP_FREEMODE_REWARD_HELP_TEXT(BOOL bDisplayHelpText)
	
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PURSUIT_SERIES_HELP_TEXT)
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PRIVATE_TAKEOVER_HELP_TEXT)
		EXIT
	ENDIF
	
	IF NOT bDisplayHelpText
	OR NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PURSUIT_SERIES_HELP_TEXT)
		CLEAR_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PURSUIT_SERIES_HELP_TEXT)
		IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
			// Flash Race Organiser blip and display help text
			SET_BIT(g_SimpleInteriorData.iNinthBS, BS9_SIMPLE_INTERIOR_CAR_MEET_FLASH_RACE_ORGANIZER_BLIP)
			PRINT_HELP("CCR_PSCM_HT")
		ELSE
			// Flash Pursuit Race freemode blip and display help text
			CV2_SET_SERIES_CLOSEST_BLIP_SHOULD_FLASH(ciCV2_SERIES_PURSUIT)
			PRINT_HELP("CCR_PSFM_HT")
		ENDIF
		
	ELIF IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PRIVATE_TAKEOVER_HELP_TEXT)
		CLEAR_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PRIVATE_TAKEOVER_HELP_TEXT)
		IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
			// Flash Mimi blip and display help text
			SET_BIT(g_SimpleInteriorData.iNinthBS, BS9_SIMPLE_INTERIOR_CAR_MEET_FLASH_MIMI_BLIP)
			PRINT_HELP("CCR_PTCM_HT")
		ELSE
			// Flash LS Car Meet freemode blip and display help text
			SET_SIMPLE_INTERIOR_GLOBAL_BIT(SI_BS_FlashBlips, SIMPLE_INTERIOR_CAR_MEET)
			PRINT_HELP("CCR_PTFM_HT")
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_CAR_CLUB_REP_FREEMODE_TEXT_MESSAGE_BIT()
	
	IF IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PURSUIT_SERIES_TEXT_MESSAGE)
		RETURN BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PURSUIT_SERIES_TEXT_MESSAGE
	ELIF IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_FINALE_ABILITY_EMAIL)
		RETURN BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_FINALE_ABILITY_EMAIL
	ELIF IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PRIVATE_TAKEOVER_EMAIL)
		RETURN BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PRIVATE_TAKEOVER_EMAIL
	ENDIF
	
	RETURN -1
ENDFUNC

PROC GET_CAR_CLUB_REP_FREEMODE_TEXT_MESSAGE_DATA(INT iMessageBit, enumCharacterList &eCharacter, TEXT_LABEL_63 &tlMessage, BOOL &bSendEmail)
	
	SWITCH iMessageBit
		CASE BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PURSUIT_SERIES_TEXT_MESSAGE
			eCharacter 	= CHAR_HAO
			tlMessage 	= "CCR_EML_PS"
			bSendEmail 	= FALSE
		BREAK
		CASE BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_FINALE_ABILITY_EMAIL
			eCharacter 	= CHAR_LS_CAR_MEET_MPEMAIL
			tlMessage 	= "CCR_EML_FA"
			bSendEmail 	= TRUE
		BREAK
		CASE BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PRIVATE_TAKEOVER_EMAIL
			eCharacter 	= CHAR_LS_CAR_MEET_MPEMAIL
			tlMessage 	= "CCR_EML_PT"
			bSendEmail 	= TRUE
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC BOOL MAINTAIN_CAR_CLUB_REP_FREEMODE_EMAILS()
	
	IF NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PURSUIT_SERIES_TEXT_MESSAGE)
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_FINALE_ABILITY_EMAIL)
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PRIVATE_TAKEOVER_EMAIL)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	INT iMessageBit = GET_CAR_CLUB_REP_FREEMODE_TEXT_MESSAGE_BIT()
	IF (iMessageBit = -1)
		RETURN FALSE
	ENDIF
	
	BOOL bSendEmail = TRUE
	TEXT_LABEL_63 tlMessage
	enumCharacterList eCharacter
	GET_CAR_CLUB_REP_FREEMODE_TEXT_MESSAGE_DATA(iMessageBit, eCharacter, tlMessage, bSendEmail)
	
	IF (bSendEmail)
		IF SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER(eCharacter, tlMessage, EMAIL_UNLOCKED, EMAIL_NOT_CRITICAL, EMAIL_AUTO_UNLOCK_AFTER_READ, MPE_NO_REPLY_REQUIRED)
			CLEAR_BIT(g_SimpleInteriorData.iTenthBS, iMessageBit)
		ENDIF
	ELSE
		IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(eCharacter, tlMessage, TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED, DEFAULT, DEFAULT, DEFAULT, CANNOT_CALL_SENDER)
			CLEAR_BIT(g_SimpleInteriorData.iTenthBS, iMessageBit)
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PRINT_CAR_CLUB_REP_HELP_TEXT()
	
	// Rep increase help text - Only displays first three times, and only once per boot
	INT iRepIncreaseHelpText = GET_PACKED_STAT_INT(PACKED_MP_INT_CAR_CLUB_REP_INCREASE_HELP_TEXT)
	
	IF NOT g_bCarClubRepIncreasedDisplayHelpText						// Once per boot
	AND iRepIncreaseHelpText < MAX_NUM_CAR_CLUB_REP_INCREASE_HELP_TEXT	// Three times max
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			CLEAR_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_HELP_TEXT)
			SET_PACKED_STAT_INT(PACKED_MP_INT_CAR_CLUB_REP_INCREASE_HELP_TEXT, iRepIncreaseHelpText+1)
			g_bCarClubRepIncreasedDisplayHelpText = TRUE
			
			#IF NOT FEATURE_GEN9_EXCLUSIVE
			PRINT_HELP("CCR_INC_HT")
			#ENDIF
			
			#IF FEATURE_GEN9_EXCLUSIVE
			PRINT_HELP("CCR_INC_HT_G9")
			#ENDIF
		ELSE
			// Can't display the help text right now, set bit to try and display when next available in freemode
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_HELP_TEXT)
				SET_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_HELP_TEXT)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_CAR_CLUB_REP_REWARD_VALID(CAR_CLUB_REP_REWARDS eRepReward)
	RETURN (eRepReward > REP_REWARD_INVALID AND eRepReward < REP_REWARD_MAX)
ENDFUNC

FUNC BOOL IS_CAR_CLUB_REP_REWARD_TYPE_VALID(CAR_CLUB_REP_REWARD_TYPE eRepRewardType)
	RETURN (eRepRewardType > REP_REWARD_TYPE_INVALID AND eRepRewardType < REP_REWARD_TYPE_MAX)
ENDFUNC

FUNC BOOL IS_CAR_CLUB_REP_REWARD_DISABLED(CAR_CLUB_REP_REWARDS eRepReward)
	SWITCH eRepReward
		CASE REP_REWARD_RANK_10_FINALE_ABILITY_REPAIR		RETURN g_sMPTunables.bTUNER_CARCLUB_DISABLE_REPAIR
		CASE REP_REWARD_RANK_15_FINALE_ABILITY_DISTRACTION	RETURN g_sMPTunables.bTUNER_CARCLUB_DISABLE_DISTRACTION
		CASE REP_REWARD_RANK_25_FINALE_ABILITY_AMBUSH		RETURN g_sMPTunables.bTUNER_CARCLUB_DISABLE_AMBUSH
		CASE REP_REWARD_RANK_30_FINALE_ABILITY_RIDE_ALONG	RETURN g_sMPTunables.bTUNER_CARCLUB_DISABLE_RIDEALONG	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SET_CAR_CLUB_REP_REWARD_PACKED_STAT(CAR_CLUB_REP_REWARDS eRepReward)
	
	IF NOT IS_CAR_CLUB_REP_REWARD_VALID(eRepReward)
		RETURN FALSE
	ENDIF
	
	IF IS_CAR_CLUB_REP_REWARD_DISABLED(eRepReward)
		PRINTLN("[CAR_CLUB_REP] SET_CAR_CLUB_REP_REWARD_PACKED_STAT - Reward disabled")
		RETURN FALSE
	ENDIF
	
	STATS_PACKED eRewardPackedStatBool = GET_CAR_CLUB_REP_REWARD_PACKED_STAT(eRepReward)
	IF (eRewardPackedStatBool = START_MP_BOOL_PACKED)
		RETURN FALSE
	ENDIF
	
	SET_PACKED_STAT_BOOL(eRewardPackedStatBool, TRUE)
	RETURN TRUE
ENDFUNC

PROC SEND_CAR_CLUB_REP_REWARD_ITEM_TICKER(CAR_CLUB_REP_REWARDS eRepReward)
	
	CAR_CLUB_REP_REWARD_TYPE eRepRewardType = GET_CAR_CLUB_REP_REWARD_TYPE(eRepReward)
	IF NOT IS_CAR_CLUB_REP_REWARD_TYPE_VALID(eRepRewardType)
		EXIT
	ENDIF
	
	STRING sTickerTextLabel = ""
	SWITCH eRepRewardType
		CASE REP_REWARD_TYPE_CLOTHING				sTickerTextLabel = "CCR_UNL_CLTH"	BREAK
		CASE REP_REWARD_TYPE_VEH_TRADE_PRICE		sTickerTextLabel = ""				BREAK	// Send ticker for vehicle trade price inside PROCESS_CAR_CLUB_REP_REWARD_ITEM()
		CASE REP_REWARD_TYPE_PURSUIT_SERIES			sTickerTextLabel = "CCR_UNL_RC"		BREAK
		CASE REP_REWARD_TYPE_FINALE_ABILITY			sTickerTextLabel = "CCR_UNL_FA"		BREAK
		CASE REP_REWARD_TYPE_PRIVATE_TAKEOVER		sTickerTextLabel = "CCR_UNL_FT"		BREAK
		CASE REP_REWARD_TYPE_WHEEL_STYLE			sTickerTextLabel = "CCR_UNL_WS"		BREAK
		CASE REP_REWARD_TYPE_LIVERY					sTickerTextLabel = "CCR_UNL_LIV"	BREAK
		CASE REP_REWARD_TYPE_VEH_WEBSITE_DISCOUNT	sTickerTextLabel = ""				BREAK	// Send ticker for vehicle trade price inside PROCESS_CAR_CLUB_REP_REWARD_ITEM()
	ENDSWITCH
	
	BOOL bMalePed = !IS_PLAYER_FEMALE()
	STRING sTickerSubTextLabel = GET_CAR_CLUB_REP_REWARD_TEXT_LABEL(eRepReward, bMalePed)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sTickerTextLabel)
	AND NOT IS_STRING_NULL_OR_EMPTY(sTickerSubTextLabel)
		#IF IS_DEBUG_BUILD
		PRINTLN("[CAR_CLUB_REP] UNLOCK_CAR_CLUB_REP_REWARDS - Reward Unlocked: ", GET_FILENAME_FOR_AUDIO_CONVERSATION(sTickerSubTextLabel))
		#ENDIF
		PRINT_TICKER_WITH_STRING(sTickerTextLabel, sTickerSubTextLabel)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    The majority of rewards are handled in difference system, which just check the state of the unlock packed stats. However, some reward items need to be processed here.
PROC PROCESS_CAR_CLUB_REP_REWARD_ITEM(CAR_CLUB_REP_REWARDS eRepReward, BOOL bSendTicker = TRUE)
	
	CAR_CLUB_REP_REWARD_TYPE eRepRewardType = GET_CAR_CLUB_REP_REWARD_TYPE(eRepReward)
	IF NOT IS_CAR_CLUB_REP_REWARD_TYPE_VALID(eRepRewardType)
		EXIT
	ENDIF
	
	BOOL bMalePed = !IS_PLAYER_FEMALE()
	STRING sTickerSubTextLabel = ""
	
	SWITCH eRepRewardType
		CASE REP_REWARD_TYPE_VEH_WEBSITE_DISCOUNT
			COUPON_TYPE eCoupon
			INT iRewardVariant 
			iRewardVariant = GET_RANDOM_INT_IN_RANGE(0, 2)
			
			eCoupon = COUPON_CASINO_CAR_SITE		//Legendary Motorsports
			IF (iRewardVariant = 1)
				eCoupon = COUPON_CASINO_CAR_SITE2	//Southern San Andreas Super Autos
			ENDIF
			OBTAIN_COUPON(eCoupon)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[CAR_CLUB_REP] UNLOCK_CAR_CLUB_REP_REWARDS - Reward Unlocked: ", GET_FILENAME_FOR_AUDIO_CONVERSATION(sTickerSubTextLabel))
			#ENDIF
			
			IF (bSendTicker)
				sTickerSubTextLabel = GET_CAR_CLUB_REP_REWARD_TEXT_LABEL(eRepReward, bMalePed, iRewardVariant)
				IF NOT IS_STRING_NULL_OR_EMPTY(sTickerSubTextLabel)
					PRINT_TICKER_WITH_STRING("CCR_UNL_VD", sTickerSubTextLabel)
				ENDIF
			ENDIF
		BREAK
		CASE REP_REWARD_TYPE_VEH_TRADE_PRICE
			MODEL_NAMES eVehModel
			eVehModel = DUMMY_MODEL_FOR_SCRIPT
			IF ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE(eVehModel)
				
				#IF IS_DEBUG_BUILD
				PRINTLN("[CAR_CLUB_REP] UNLOCK_CAR_CLUB_REP_REWARDS - Reward Unlocked: ", GET_FILENAME_FOR_AUDIO_CONVERSATION(sTickerSubTextLabel))
				#ENDIF
				
				IF (bSendTicker)
					sTickerSubTextLabel = GET_CAR_CLUB_REP_REWARD_TEXT_LABEL(eRepReward, bMalePed, DEFAULT, eVehModel)
					IF NOT IS_STRING_NULL_OR_EMPTY(sTickerSubTextLabel)
						PRINT_TICKER_WITH_STRING("CCR_UNL_TP", sTickerSubTextLabel)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE REP_REWARD_TYPE_FINALE_ABILITY
			IF (eRepReward = REP_REWARD_RANK_10_FINALE_ABILITY_REPAIR)
				SET_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_FINALE_ABILITY_EMAIL)
				PRINT_HELP("CCR_INC_FA")
			ENDIF
		BREAK
		CASE REP_REWARD_TYPE_PURSUIT_SERIES
			IF (eRepReward = REP_REWARD_RANK_5_PURSUIT_SERIES)
				SET_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PURSUIT_SERIES_TEXT_MESSAGE)
				SET_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PURSUIT_SERIES_HELP_TEXT)
			ENDIF
		BREAK
		CASE REP_REWARD_TYPE_PRIVATE_TAKEOVER
			IF (eRepReward = REP_REWARD_RANK_20_PRIVATE_TAKEOVER)
				SET_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_SEND_PRIVATE_TAKEOVER_EMAIL)
				SET_BIT(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_CAR_MEET_REP_DISPLAY_PRIVATE_TAKEOVER_HELP_TEXT)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC INT GET_CAR_CLUB_REP_REWARD_CASH_AMOUNT(INT iRepTier)
	INT iCashAmount = -1
	
	IF (iRepTier = g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_5)
		iCashAmount = g_sMPTunables.iTUNER_CARCLUB_REWARDS_CASH_BRACKET_5
		
	ELIF (iRepTier >= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_3+1 AND iRepTier <= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_4)
		iCashAmount = g_sMPTunables.iTUNER_CARCLUB_REWARDS_CASH_BRACKET_4
	
	ELIF (iRepTier >= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_2+1 AND iRepTier <= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_3)
		iCashAmount = g_sMPTunables.iTUNER_CARCLUB_REWARDS_CASH_BRACKET_3
	
	ELIF (iRepTier >= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_1+1 AND iRepTier <= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_2)
		iCashAmount = g_sMPTunables.iTUNER_CARCLUB_REWARDS_CASH_BRACKET_2
	
	ELIF (iRepTier >= 2 AND iRepTier <= g_sMPTunables.iTUNER_CARCLUB_LEVEL_BRACKET_1)
		iCashAmount = g_sMPTunables.iTUNER_CARCLUB_REWARDS_CASH_BRACKET_1
	ENDIF
	
	RETURN iCashAmount
ENDFUNC

PROC GIVE_CAR_CLUB_REP_REWARD_CASH(INT iRepTier)
	INT iCashAmount = GET_CAR_CLUB_REP_REWARD_CASH_AMOUNT(iRepTier)
	IF (iCashAmount != -1)
		IF USE_SERVER_TRANSACTIONS()
			INT iTransactionSlot
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_TUNER_CAR_CLUB_MEMBERSHIP, iCashAmount, iTransactionSlot)
			PRINTLN("[CAR_CLUB_REP] GIVE_CAR_CLUB_REP_REWARD_CASH - PC - Giving the player cash amount: ", iCashAmount)
		ELSE
			NETWORK_EARN_CARCLUB_MEMBERSHIP(iCashAmount)
			PRINTLN("[CAR_CLUB_REP] GIVE_CAR_CLUB_REP_REWARD_CASH - Console - Giving the player cash amount: ", iCashAmount)
		ENDIF
	ENDIF
ENDPROC

PROC SET_CAR_CLUB_REP_REWARD_PLAYER_BROADCAST_DATA(CAR_CLUB_REP_REWARDS eRepReward)
	SWITCH eRepReward
		CASE REP_REWARD_RANK_10_FINALE_ABILITY_REPAIR		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_REPAIR_UNLOCKED)		BREAK
		CASE REP_REWARD_RANK_15_FINALE_ABILITY_DISTRACTION	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_DISTRACTION_UNLOCKED)	BREAK
		CASE REP_REWARD_RANK_25_FINALE_ABILITY_AMBUSH		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_AMBUSH_UNLOCKED)		BREAK
		CASE REP_REWARD_RANK_30_FINALE_ABILITY_RIDE_ALONG	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_RIDEALONG_UNLOCKED)	BREAK
	ENDSWITCH
ENDPROC

PROC UNLOCK_CAR_CLUB_REP_REWARDS(INT iCarClubRepTier, INT iNewCarClubRepTier)
	
	INT iTier
	CAR_CLUB_REP_REWARDS eRepReward
	CAR_CLUB_REP_REWARDS eSecondRepReward
	
	FOR iTier = iCarClubRepTier+1 TO iNewCarClubRepTier
		PRINTLN("[CAR_CLUB_REP] UNLOCK_CAR_CLUB_REP_REWARDS - Player has increased their rank to: ", iTier)
		GET_CAR_CLUB_REP_TIER_REWARD(eRepReward, eSecondRepReward, iTier)
		
		// Unlock Rep Reward
		IF SET_CAR_CLUB_REP_REWARD_PACKED_STAT(eRepReward)
			PRINTLN("[CAR_CLUB_REP] UNLOCK_CAR_CLUB_REP_REWARDS - Setting packed stat for main reward, tier: ", iTier)
			SEND_CAR_CLUB_REP_REWARD_ITEM_TICKER(eRepReward)
			PROCESS_CAR_CLUB_REP_REWARD_ITEM(eRepReward)
			SET_CAR_CLUB_REP_REWARD_PLAYER_BROADCAST_DATA(eRepReward)
		ENDIF
		
		// Unlock Second Rep Reward
		IF SET_CAR_CLUB_REP_REWARD_PACKED_STAT(eSecondRepReward)
			PRINTLN("[CAR_CLUB_REP] UNLOCK_CAR_CLUB_REP_REWARDS - Setting packed stat for second reward, tier: ", iTier)
			SEND_CAR_CLUB_REP_REWARD_ITEM_TICKER(eSecondRepReward)
			PROCESS_CAR_CLUB_REP_REWARD_ITEM(eSecondRepReward)
			SET_CAR_CLUB_REP_REWARD_PLAYER_BROADCAST_DATA(eSecondRepReward)
		ENDIF
		
		// Give Cash
		GIVE_CAR_CLUB_REP_REWARD_CASH(iTier)
		
		// Send Rep Tier Increase Ticker
		PRINT_TICKER_WITH_INT("CCR_INC_TCK", iTier, TRUE)
	ENDFOR
	
	PRINT_CAR_CLUB_REP_HELP_TEXT()
ENDPROC

/// PURPOSE:
///    Adds an amount to the local players car club rep.
/// PARAMS:
///    iCarClubRepToAdd - Amount to add.
///    eRepEarnType - Rep earn type.
///    iPurchasedItemHash - Hash of purchased item that earned rep (for telemetry).
PROC ADD_CAR_CLUB_REP(INT iCarClubRepToAdd, EARN_CAR_CLUB_REP_TYPE eRepEarnType, INT iPurchasedItemHash = -1, FLOAT fMultiplier = 1.0)
	
	IF (iCarClubRepToAdd <= 0) 										// Invalid rep amount
	OR (PLAYER_ID() = INVALID_PLAYER_INDEX())						// Invalid player ID
	OR g_sMPTunables.bBLOCK_CAR_CLUB_REP_CHANGE						// Tunable blocked
	OR (GET_LOCAL_PLAYERS_CAR_CLUB_REP() >= GET_MAX_CAR_CLUB_REP())	// Max rep already reached
	OR NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())	// Does not own membership
		EXIT
	ENDIF
	
	IF fMultiplier != 1.0
		PRINTLN("[CAR_CLUB_REP] ADD_CAR_CLUB_REP - fMultiplier: ", fMultiplier)
		iCarClubRepToAdd = ROUND(TO_FLOAT(iCarClubRepToAdd) * fMultiplier)
	ENDIF
	
	INT iCarClubRep 			= GET_LOCAL_PLAYERS_CAR_CLUB_REP()
	INT iCarClubRepTier 		= GET_CAR_CLUB_REP_TIER(iCarClubRep)
	INT iNewCarClubRep 			= (iCarClubRep+iCarClubRepToAdd)
	
	// Just reached max rep
	INT iMaxCarClubRep = GET_MAX_CAR_CLUB_REP()
	IF (iNewCarClubRep > iMaxCarClubRep)
		iNewCarClubRep = iMaxCarClubRep
	ENDIF
	
	BOOL bCarClubRepTierLevelUp	= FALSE
	INT iNewCarClubRepTier 		= GET_CAR_CLUB_REP_TIER(iNewCarClubRep)
	
	// Has car club rep tier increased?
	IF (iNewCarClubRepTier > iCarClubRepTier)
		bCarClubRepTierLevelUp = TRUE
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CAR_CLUB_REP, iNewCarClubRep)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubRep 	= iNewCarClubRep
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubTier	= iNewCarClubRepTier
	
	SEND_CAR_CLUB_REP_METRIC(iCarClubRepToAdd, iNewCarClubRepTier, bCarClubRepTierLevelUp, eRepEarnType, iPurchasedItemHash)
	SET_MP_INT_CHARACTER_AWARD(MP_AWARD_CAR_CLUB_MEM, iNewCarClubRepTier)
	
	IF (bCarClubRepTierLevelUp)
		UNLOCK_CAR_CLUB_REP_REWARDS(iCarClubRepTier, iNewCarClubRepTier)
	ENDIF
	
ENDPROC

FUNC BOOL IS_CAR_CLUB_REP_EARN_TYPE_VALID(EARN_CAR_CLUB_REP_TYPE eEarnRepType)
	RETURN (eEarnRepType > EARN_REP_TYPE_INVALID AND eEarnRepType < EARN_REP_TYPE_MAX)
ENDFUNC

FUNC INT GET_CAR_CLUB_REP_EARN_AMOUNT(EARN_CAR_CLUB_REP_TYPE eEarnRepType, INT iRacePosition = -1, INT iRaceParticipants = -1)
	INT iRepAmount = -1
	SWITCH eEarnRepType
		CASE EARN_REP_TYPE_FTB_SPRINT_RACE					iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_SPRINT_FIRST_TIME_BONUS_XP_MULTIPLIER)				BREAK
		CASE EARN_REP_TYPE_FTB_STREET_RACE					iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_STREET_FIRST_TIME_BONUS_XP_MULTIPLIER)				BREAK
		CASE EARN_REP_TYPE_FTB_PURSUIT_SERIES				iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_PURSUIT_FIRST_TIME_BONUS_XP_MULTIPLIER)			BREAK
		CASE EARN_REP_TYPE_FTB_CAR_CLUB_MOD_VEH				iRepAmount = ROUND(g_sMPTunables.iTUNER_MOD_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_MOD_FIRST_TIME_BONUS_XP_MULTIPLIER)					BREAK
		CASE EARN_REP_TYPE_FTB_SANDBOX_DRIVE_TEST_CAR		iRepAmount = ROUND(g_sMPTunables.iTUNER_SANDBOX_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_SANDBOX_TEST_FIRST_TIME_BONUS_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_FTB_TIME_TRIAL					iRepAmount = ROUND(g_sMPTunables.iTUNER_TIMETRIAL_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_TIMETRIAL_FIRST_TIME_BONUS_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_FTB_CHECKPOINT_DASH				iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_CHECKPOINT_FIRST_TIME_BONUS_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_FTB_HEAD_TO_HEAD					iRepAmount = ROUND(g_sMPTunables.iTUNER_HEADTOHEAD_FIRST_TIME_BONUS_XP*g_sMPTunables.fTUNER_HEADTOHEAD_FIRST_TIME_BONUS_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB				iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_CARCLUB_FIRST_TIME_DAILY_XP_MULTIPLIER)			BREAK
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB_PERS_VEH		iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_PV_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_CARCLUB_PV_FIRST_TIME_DAILY_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_DB_SPRINT_RACE					iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_SPRINT_FIRST_TIME_DAILY_XP_MULTIPLIER)				BREAK
		CASE EARN_REP_TYPE_DB_STREET_RACE					iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_STREET_FIRST_TIME_DAILY_XP_MULTIPLIER)				BREAK
		CASE EARN_REP_TYPE_DB_PURSUIT_SERIES				iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_PURSUIT_FIRST_TIME_DAILY_XP_MULTIPLIER)			BREAK
		CASE EARN_REP_TYPE_DB_CAR_CLUB_MOD_VEH				iRepAmount = ROUND(g_sMPTunables.iTUNER_MOD_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_MOD_FIRST_TIME_DAILY_XP_MULTIPLIER)					BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_TEST_CAR		iRepAmount = ROUND(g_sMPTunables.iTUNER_SANDBOX_TEST_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_SANDBOX_TEST_FIRST_TIME_DAILY_XP_MULTIPLIER)	BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_PERS_VEH		iRepAmount = ROUND(g_sMPTunables.iTUNER_SANDBOX_PV_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_SANDBOX_PV_FIRST_TIME_DAILY_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_DB_TIME_TRIAL					iRepAmount = ROUND(g_sMPTunables.iTUNER_TIMETRIAL_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_TIMETRIAL_FIRST_TIME_DAILY_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_DB_CHECKPOINT_DASH				iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_CHECKPOINT_FIRST_TIME_DAILY_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_DB_HEAD_TO_HEAD					iRepAmount = ROUND(g_sMPTunables.iTUNER_HEADTOHEAD_FIRST_TIME_DAILY_XP*g_sMPTunables.fTUNER_HEADTOHEAD_FIRST_TIME_DAILY_XP_MULTIPLIER)		BREAK
		CASE EARN_REP_TYPE_RACE_SPRINT
			SWITCH iRacePosition
				CASE 1
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_1ST_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 3)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_3P_1ST_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_2P_1ST_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 2
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_2ND_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 3)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_3P_2ND_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_2P_2ND_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 3
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_3RD_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 3)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_3P_3RD_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 4
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_SPRINT_4TH_PLACE_XP*g_sMPTunables.fTUNER_SPRINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE EARN_REP_TYPE_RACE_STREET
			SWITCH iRacePosition
				CASE 1
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_1ST_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_3P_1ST_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 1 AND iRaceParticipants <= 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_2P_1ST_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 2
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_2ND_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_3P_2ND_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 1 AND iRaceParticipants <= 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_2P_2ND_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 3
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_3RD_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_3P_3RD_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 4
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_4TH_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_3P_4TH_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 5
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_5TH_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 6
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_6TH_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 7
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_7TH_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 8
					IF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_STREET_8TH_PLACE_XP*g_sMPTunables.fTUNER_STREET_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE EARN_REP_TYPE_RACE_PURSUIT
			SWITCH iRacePosition
				CASE 1
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_1ST_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_1ST_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_3P_1ST_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 1 AND iRaceParticipants <= 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_2P_1ST_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 2
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_2ND_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_2ND_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_3P_2ND_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 1 AND iRaceParticipants <= 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_2P_2ND_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 3
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_3RD_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_3RD_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_3P_3RD_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 4
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_4TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_4TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 3 AND iRaceParticipants <= 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_3P_4TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 5
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_5TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 6
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_6TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_6TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 7
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_7TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_7TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 8
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_8TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants >= 5 AND iRaceParticipants <= 8)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_5P_8TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 9
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_9TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 10
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_10TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 11
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_11TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 12
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_12TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 13
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_13TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 14
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_14TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 15
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_15TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 16
					IF (iRaceParticipants >= 9 AND iRaceParticipants <= 16)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_PURSUIT_16TH_PLACE_XP*g_sMPTunables.fTUNER_PURSUIT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE EARN_REP_TYPE_RACE_CHECKPOINT_DASH
			SWITCH iRacePosition
				CASE 1
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_1ST_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 3)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_3P_1ST_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_2P_1ST_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 2
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_2ND_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 3)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_3P_2ND_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 2)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_2P_2ND_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 3
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_3RD_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ELIF (iRaceParticipants = 3)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_3P_3RD_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
				CASE 4
					IF (iRaceParticipants = 4)
						iRepAmount = ROUND(g_sMPTunables.iTUNER_CHECKPOINT_4TH_PLACE_XP*g_sMPTunables.fTUNER_CHECKPOINT_RACE_PLACE_XP_MULTIPLIER)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE EARN_REP_TYPE_RACE_HEAD_TO_HEAD
			SWITCH iRacePosition
				CASE 1
					iRepAmount = ROUND(g_sMPTunables.iTUNER_HEADTOHEAD_1ST_PLACE_XP*g_sMPTunables.fTUNER_HEADTOHEAD_RACE_PLACE_XP_MULTIPLIER)
				BREAK
				CASE 2
					iRepAmount = ROUND(g_sMPTunables.iTUNER_HEADTOHEAD_2ND_PLACE_XP*g_sMPTunables.fTUNER_HEADTOHEAD_RACE_PLACE_XP_MULTIPLIER)
				BREAK
			ENDSWITCH
		BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS				iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_VISITS_7_STREAK_XP*g_sMPTunables.fTUNER_CARCLUB_VISITS_STREAK_XP_MULTIPLIER)							BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS				iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_VISITS_14_STREAK_XP*g_sMPTunables.fTUNER_CARCLUB_VISITS_STREAK_XP_MULTIPLIER)							BREAK
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS				iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_VISITS_30_STREAK_XP*g_sMPTunables.fTUNER_CARCLUB_VISITS_STREAK_XP_MULTIPLIER)							BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET					iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_TIME_XP*g_sMPTunables.fTUNER_CARCLUB_TIME_XP_MULTIPLIER)												BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX					iRepAmount = ROUND(g_sMPTunables.iTUNER_SANDBOX_TIME_XP*g_sMPTunables.fTUNER_SANDBOX_TIME_XP_MULTIPLIER)												BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH			iRepAmount = ROUND(g_sMPTunables.iTUNER_CARCLUB_TIME_XP_MERCH*g_sMPTunables.fTUNER_CARCLUB_TIME_XP_MERCH_MULTIPLIER)									BREAK
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH				iRepAmount = ROUND(g_sMPTunables.iTUNER_SANDBOX_TIME_XP_MERCH*g_sMPTunables.fTUNER_SANDBOX_TIME_XP_MERCH_MULTIPLIER)									BREAK
		CASE EARN_REP_TYPE_CAR_MEET_MERCH_SHOP					iRepAmount = ROUND(g_sMPTunables.iTUNER_MERCH_PURCHASE_XP*g_sMPTunables.fTUNER_MERCH_PURCHASE_XP_MULTIPLIER)											BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_PREP				iRepAmount = ROUND(g_sMPTunables.iTUNER_AUTO_SHOP_CONTRACT_PREP_XP*g_sMPTunables.fTUNER_AUTO_SHOP_CONTRACT_PREP_XP_MULTIPLIER)							BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE			iRepAmount = ROUND(g_sMPTunables.iTUNER_AUTO_SHOP_CONTRACT_FINALE_XP*g_sMPTunables.fTUNER_AUTO_SHOP_CONTRACT_FINALE_XP_MULTIPLIER)						BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY			iRepAmount = ROUND(g_sMPTunables.iTUNER_AUTO_SHOP_CUSTOMER_DELIVERY_XP*g_sMPTunables.fTUNER_AUTO_SHOP_CUSTOMER_DELIVERY_XP_MULTIPLIER)					BREAK
		CASE EARN_REP_TYPE_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY	iRepAmount = ROUND(g_sMPTunables.iTUNER_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY_XP*g_sMPTunables.fTUNER_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY_XP_MULTIPLIER)		BREAK
		
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_FIRST_TIME 	iRepAmount = g_sMPTunables.iCAR_MEET_VEHICLE_CLONE_FIRST_TIME_XP																						BREAK
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_DAILY			iRepAmount = g_sMPTunables.iCAR_MEET_VEHICLE_CLONE_DAILY_XP																								BREAK
		#ENDIF		
	ENDSWITCH
	RETURN iRepAmount
ENDFUNC

PROC AWARD_CAR_CLUB_REP(EARN_CAR_CLUB_REP_TYPE eEarnRepType, INT iRacePosition = -1, INT iRaceParticipants = -1, INT iPurchasedItemHash = -1, FLOAT fMultiplier = 1.0)
	
	IF NOT IS_CAR_CLUB_REP_EARN_TYPE_VALID(eEarnRepType)
		EXIT
	ENDIF
	
	INT iRepAmount = GET_CAR_CLUB_REP_EARN_AMOUNT(eEarnRepType, iRacePosition, iRaceParticipants)
	IF (iRepAmount = -1)
		EXIT
	ENDIF
	
	SWITCH eEarnRepType
		// First time bonuses - Set and never cleared
		CASE EARN_REP_TYPE_FTB_SPRINT_RACE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_SPRINT_RACE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_SPRINT_RACE, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_STREET_RACE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_STREET_RACE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_STREET_RACE, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_PURSUIT_SERIES
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_PURSUIT_SERIES)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_PURSUIT_SERIES, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_CAR_CLUB_MOD_VEH
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_CAR_CLUB_MOD_VEH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_CAR_CLUB_MOD_VEH, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_SANDBOX_DRIVE_TEST_CAR
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_SANDBOX_DRIVE_TEST_CAR)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_SANDBOX_DRIVE_TEST_CAR, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_TIME_TRIAL
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_TIME_TRIAL)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_TIME_TRIAL, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_CHECKPOINT_DASH
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_CHECKPOINT_DASH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_CHECKPOINT_DASH, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_FTB_HEAD_TO_HEAD
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_HEAD_TO_HEAD)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_HEAD_TO_HEAD, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		
		// Daily bonuses - Packed stats get reset daily - First time bonus must be awarded BEFORE daily bonus
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_VISIT_CAR_CLUB)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_VISIT_CAR_CLUB, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_VISIT_CAR_CLUB_PERS_VEH
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_VISIT_CAR_CLUB_PERS_VEH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_VISIT_CAR_CLUB_PERS_VEH, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_SPRINT_RACE
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_SPRINT_RACE)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SPRINT_RACE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SPRINT_RACE, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_STREET_RACE
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_STREET_RACE)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_STREET_RACE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_STREET_RACE, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_PURSUIT_SERIES
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_PURSUIT_SERIES)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_PURSUIT_SERIES)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_PURSUIT_SERIES, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_CAR_CLUB_MOD_VEH
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_CAR_CLUB_MOD_VEH)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_CAR_CLUB_MOD_VEH)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_CAR_CLUB_MOD_VEH, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_TEST_CAR
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_SANDBOX_DRIVE_TEST_CAR)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SANDBOX_DRIVE_TEST_CAR)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SANDBOX_DRIVE_TEST_CAR, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_SANDBOX_DRIVE_PERS_VEH
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SANDBOX_DRIVE_PERS_VEH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SANDBOX_DRIVE_PERS_VEH, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_TIME_TRIAL
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_TIME_TRIAL)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_TIME_TRIAL)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_TIME_TRIAL, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_CHECKPOINT_DASH
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_CHECKPOINT_DASH)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_CHECKPOINT_DASH)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_CHECKPOINT_DASH, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE EARN_REP_TYPE_DB_HEAD_TO_HEAD
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_FTB_HEAD_TO_HEAD)
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_HEAD_TO_HEAD)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_HEAD_TO_HEAD, TRUE)
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Races
		CASE EARN_REP_TYPE_RACE_SPRINT
		CASE EARN_REP_TYPE_RACE_STREET
		CASE EARN_REP_TYPE_RACE_PURSUIT
		CASE EARN_REP_TYPE_RACE_CHECKPOINT_DASH
		CASE EARN_REP_TYPE_RACE_HEAD_TO_HEAD
			ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash, fMultiplier)
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
			#ENDIF
		BREAK
		
		// Visit car club streaks
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS
		CASE EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS
			ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
			#ENDIF
		BREAK
		
		// Misc
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX
		CASE EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH
		CASE EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH
		CASE EARN_REP_TYPE_CAR_MEET_MERCH_SHOP
			ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
			#ENDIF
		BREAK
		
		// Auto Shop
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_PREP
		CASE EARN_REP_TYPE_AUTO_SHOP_CONTRACT_FINALE
		CASE EARN_REP_TYPE_AUTO_SHOP_CUSTOMER_DELIVERY
		CASE EARN_REP_TYPE_AUTO_SHOP_EXOTIC_EXPORTS_DELIVERY
			ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType, iPurchasedItemHash)
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
			#ENDIF
		BREAK
		
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_FIRST_TIME
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIRST_CLONE_VEH_CAR_MEET_REP)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIRST_CLONE_VEH_CAR_MEET_REP, TRUE)
				ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType)
				#IF IS_DEBUG_BUILD
				DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
				#ENDIF
			ENDIF
		BREAK
		
		CASE EARN_REP_TYPE_CAR_MEET_VEHICLE_CLONE_DAILY
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIRST_CLONE_VEH_CAR_MEET_REP)
				INT iCurrentPosixTime, iCharacterUsedTime
				iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
				iCharacterUsedTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_HSW_CLONE_REP_TIMER)
				IF iCurrentPosixTime > iCharacterUsedTime
					ADD_CAR_CLUB_REP(iRepAmount, eEarnRepType)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_HSW_CLONE_REP_TIMER, iCurrentPosixTime + g_sMPTunables.iCAR_MEET_VEHICLE_CLONE_XP_TIMER_LIMIT)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINT_ADD_CAR_CLUB_REP(eEarnRepType, iRepAmount)
					#ENDIF
				ENDIF	
			ENDIF
		BREAK
		#ENDIF
		
	ENDSWITCH
	
ENDPROC

PROC RESET_CAR_CLUB_REP_DAILY_BONUSES()
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_VISIT_CAR_CLUB, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_VISIT_CAR_CLUB_PERS_VEH, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SPRINT_RACE, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_STREET_RACE, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_PURSUIT_SERIES, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_CAR_CLUB_MOD_VEH, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SANDBOX_DRIVE_TEST_CAR, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_SANDBOX_DRIVE_PERS_VEH, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_TIME_TRIAL, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_CHECKPOINT_DASH, FALSE)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_REP_EARN_DB_HEAD_TO_HEAD, FALSE)
ENDPROC

/// PURPOSE:
///    Initialises the local players car club rep. Sets global broadcast data based on stats.
PROC INITIALISE_CAR_CLUB_REP()
	IF (PLAYER_ID() = INVALID_PLAYER_INDEX())
		EXIT
	ENDIF
	
	INT iCarClubRep = GET_LOCAL_PLAYERS_CAR_CLUB_REP()
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubRep 	= iCarClubRep
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubTier = GET_CAR_CLUB_REP_TIER(iCarClubRep)
	
	// Reward player BD
	IF GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(REP_REWARD_RANK_10_FINALE_ABILITY_REPAIR))
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_REPAIR_UNLOCKED)
	ENDIF
	IF GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(REP_REWARD_RANK_15_FINALE_ABILITY_DISTRACTION))
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_DISTRACTION_UNLOCKED)
	ENDIF
	IF GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(REP_REWARD_RANK_25_FINALE_ABILITY_AMBUSH))
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_AMBUSH_UNLOCKED)
	ENDIF
	IF GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(REP_REWARD_RANK_30_FINALE_ABILITY_RIDE_ALONG))
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_CAR_CLUB_REP_FINALE_ABILITY_RIDEALONG_UNLOCKED)
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	INT iTier = GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
	
	IF iTier >= MAX_CAR_CLUB_TIER_FOR_TRADE_PRICE_UNLOCKS
		IF NOT IS_TUNER_VEHICLE_REWARD_UNLOCKED(KANJOSJ)
			PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Player already reached rank 85. Give KANJOSJ trade price. Tier = ", iTier)
			SET_TUNER_VEHICLE_PROGRESSION_REWARD_UNLOCKED(KANJOSJ)
			PRINT_TICKER_WITH_STRING("CCR_UNL_TP", GET_CAR_CLUB_REWARD_TRADE_PRICE_VEHICLE_TEXT_LABEL(KANJOSJ))
		ENDIF
		
		IF NOT IS_TUNER_VEHICLE_REWARD_UNLOCKED(POSTLUDE)
			PRINTLN("[ADD_CAR_CLUB_TIER_PROGRESSION_REWARD_VEHICLE_TRADE_PRICE] Player already reached rank 85. Give POSTLUDE trade price. Tier = ", iTier)
			SET_TUNER_VEHICLE_PROGRESSION_REWARD_UNLOCKED(POSTLUDE)
			PRINT_TICKER_WITH_STRING("CCR_UNL_TP", GET_CAR_CLUB_REWARD_TRADE_PRICE_VEHICLE_TEXT_LABEL(POSTLUDE))
		ENDIF
	ENDIF
	#ENDIF
	
ENDPROC

FUNC BOOL HAS_LOCAL_PLAYER_UNLOCKED_PURSUIT_SERIES()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BypassRepLevelRequirement")
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(REP_REWARD_RANK_5_PURSUIT_SERIES))
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_UNLOCKED_CAR_MEET_PRIVATE_TAKEOVER()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_BypassRepLevelRequirement")
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN GET_PACKED_STAT_BOOL(GET_CAR_CLUB_REP_REWARD_PACKED_STAT(REP_REWARD_RANK_20_PRIVATE_TAKEOVER))
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ UI ╞══════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Initialises the car club rep UI data.
/// PARAMS:
///    UIData - Data to set.
PROC INITIALISE_CAR_CLUB_REP_UI(CAR_CLUB_REP_UI_STRUCT &UIData)
	UIData.iCarClubRep 				= GET_LOCAL_PLAYERS_CAR_CLUB_REP()
	UIData.iCarClubRepTier 			= GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
	UIData.bHasCarClubMembership 	= HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
	UIData.iStartingRepTier			= UIData.iCarClubRepTier
	
	UIData.bSetAnimateTime			= FALSE
	UIData.bMultiTiered				= FALSE
	UIData.iStartingBarValue		= 0
	UIData.iTargetBarValue			= 0
	UIData.iBarDifference			= 0
	UIData.iAnimateTimeMS			= 0
	
	RESET_NET_TIMER(UIData.stHoldAtTargetTimer)
ENDPROC

/// PURPOSE:
///    Returns TRUE if the car club rep UI bar is animating.
/// PARAMS:
///    UIData - Data to query.
/// RETURNS: TRUE if the car club rep UI bar is animating. FALSE otherwise.
FUNC BOOL IS_CAR_CLUB_REP_UI_BAR_ANIMATING(CAR_CLUB_REP_UI_STRUCT UIData)
	RETURN UIData.bSetAnimateTime
ENDFUNC

/// PURPOSE:
///    Returns the animation time for the car club rep UI bar.
///    Animation time is based on the percentage of the bar, with min and max caps.
/// PARAMS:
///    UIData - Data to query.
///    iTargetBarValue - The target UI bar value to animate too.
/// RETURNS: The animation time in milliseconds as an INT.
FUNC INT GET_CAR_CLUB_REP_UI_BAR_ANIMATE_TIME_MS(CAR_CLUB_REP_UI_STRUCT &UIData, INT iTargetBarValue)
	
	INT iRepForNextTier 	= GET_CAR_CLUB_REP_FOR_TIER(UIData.iCarClubRepTier+1)
	INT iBarDiff 			= (iTargetBarValue-UIData.iCarClubRep)
	
	FLOAT fAnimateTimeMS	= (TO_FLOAT(iBarDiff)/TO_FLOAT(iRepForNextTier))*100.0
	fAnimateTimeMS *= (TO_FLOAT(MAX_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS)/100.0)
	
	IF NOT UIData.bMultiTiered
		IF (fAnimateTimeMS > MAX_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS)
			fAnimateTimeMS = MAX_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS	
		ELIF (fAnimateTimeMS < MIN_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS)
			fAnimateTimeMS = MIN_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS
		ENDIF
	ENDIF
	
	RETURN ROUND(fAnimateTimeMS)
ENDFUNC

/// PURPOSE:
///    Sets the car club rep UI bar animation data.
/// PARAMS:
///    UIData - Data to set.
///    iStartingBarValue - The starting UI bar value to animate from.
///    iTargetBarValue - The target UI bar value to animate too.
///    iAnimateTimeMS - Time to animate UI bar in milliseconds.
PROC SET_CAR_CLUB_REP_UI_BAR_ANIM_DATA(CAR_CLUB_REP_UI_STRUCT &UIData, INT iStartingBarValue, INT iTargetBarValue)
	
	IF (NOT UIData.bSetAnimateTime)
		UIData.bSetAnimateTime 		= TRUE
		UIData.iStartingBarValue 	= iStartingBarValue
		UIData.iTargetBarValue		= iTargetBarValue
		UIData.iBarDifference		= (UIData.iTargetBarValue-UIData.iStartingBarValue)
		UIData.iAnimateTimeMS		= GET_CAR_CLUB_REP_UI_BAR_ANIMATE_TIME_MS(UIData, iTargetBarValue)
		UIData.tdAnimateTimer 		= GET_TIME_OFFSET(GET_NETWORK_TIME(), UIData.iAnimateTimeMS)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Clears the car club rep UI bar animation data so it can animate again.
/// PARAMS:
///    UIData - Data to set.
///    iBarValue - Finalises the bar value to the stored target value.
PROC CLEAR_CAR_CLUB_REP_UI_BAR_ANIM_DATA(CAR_CLUB_REP_UI_STRUCT &UIData, INT &iBarValue, BOOL bClearMultiTieredFlag)
	
	iBarValue 						= UIData.iTargetBarValue
	UIData.iCarClubRepTier			= GET_CAR_CLUB_REP_TIER(iBarValue)
	UIData.iStartingBarValue		= 0
	UIData.iTargetBarValue			= 0
	UIData.iBarDifference			= 0
	UIData.iAnimateTimeMS			= 0
	UIData.bSetAnimateTime 			= FALSE
	
	IF (bClearMultiTieredFlag)
		UIData.bMultiTiered			= FALSE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Animates the car club rep UI bar.
/// PARAMS:
///    UIData - Data to set.
///    iBarValue - The updating current bar value.
///    iAnimateTimeMS - Time to animate UI bar in milliseconds.
/// RETURNS: Returns TRUE when the generic meter bar has finished animating. FALSE otherwise.
FUNC BOOL ANIMATE_CAR_CLUB_REP_UI_BAR(CAR_CLUB_REP_UI_STRUCT &UIData, INT &iBarValue)
	
	IF (NOT UIData.bSetAnimateTime)
		RETURN FALSE
	ENDIF
	
	IF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), UIData.tdAnimateTimer)
		INT iTimeDifference 		= ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), UIData.tdAnimateTimer))
		iBarValue					= (UIData.iBarDifference-(ROUND((TO_FLOAT(iTimeDifference)/UIData.iAnimateTimeMS)*UIData.iBarDifference)))+UIData.iStartingBarValue
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Updates the car club rep UI bar is the rep is set via the widgets
/// PARAMS:
///    UIData - Data to set.
PROC DEBUG_UPDATE_CAR_CLUB_REP(CAR_CLUB_REP_UI_STRUCT &UIData)
	
	IF (CarClubRepDebugData.bEnableDebug AND CarClubRepDebugData.RepWidgetData.bGlobalSettingRep)
		CarClubRepDebugData.RepWidgetData.bGlobalSettingRep = FALSE
		
		UIData.iCarClubRep 			= GET_LOCAL_PLAYERS_CAR_CLUB_REP()
		UIData.iCarClubRepTier 		= GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
		
		UIData.iTargetBarValue		= UIData.iCarClubRep
		UIData.iStartingRepTier		= UIData.iCarClubRepTier
		CLEAR_CAR_CLUB_REP_UI_BAR_ANIM_DATA(UIData, UIData.iCarClubRep, TRUE)
	ENDIF
	
ENDPROC
#ENDIF

/// PURPOSE:
///    Maintains the car club rep UI bar.
///    Pressing D-pad down displays the UI bar.
///    The UI bar animates if the car club rep increases.
/// PARAMS:
///    UIData - Data to set.
PROC MAINTAIN_CAR_CLUB_REP_UI(CAR_CLUB_REP_UI_STRUCT &UIData)
	
	#IF IS_DEBUG_BUILD
	DEBUG_UPDATE_CAR_CLUB_REP(UIData)
	#ENDIF
	
	BOOL bAnimateUIBar 	= FALSE
	BOOL bDisplayUIBar 	= FALSE
	
	INT iBarPulseTime	= 0
	
	// D-pad down pressed
	IF IS_BIT_SET(MPGlobalsAmbience.iNGAmbBitSet, iNGABI_DPAD_DOWN_LBD_DISPLAYING)
	AND IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		bDisplayUIBar = TRUE
	ENDIF
	
	// First 10 seconds after purchasing car club membership
	IF (UIData.bHasCarClubMembership != HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID()))
		UIData.bHasCarClubMembership = HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
		IF UIData.bHasCarClubMembership
			START_NET_TIMER(UIData.stHoldAfterMembership)
		ENDIF
	ENDIF
	
	// Hold after membership
	IF HAS_NET_TIMER_STARTED(UIData.stHoldAfterMembership)
		bDisplayUIBar = TRUE
		IF HAS_NET_TIMER_EXPIRED(UIData.stHoldAfterMembership, MAX_NUM_CAR_CLUB_REP_UI_HOLD_AFTER_MEMBERSHIP_MS)
			RESET_NET_TIMER(UIData.stHoldAfterMembership)
		ENDIF
	ENDIF
	
	// Rep has changed
	IF (UIData.iCarClubRep < GET_LOCAL_PLAYERS_CAR_CLUB_REP())
	OR IS_CAR_CLUB_REP_UI_BAR_ANIMATING(UIData)
		bAnimateUIBar = TRUE
		bDisplayUIBar = TRUE
	ENDIF
	
	// Hold at target
	IF HAS_NET_TIMER_STARTED(UIData.stHoldAtTargetTimer)
		bDisplayUIBar = TRUE
		iBarPulseTime = (MAX_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS/4)
		IF HAS_NET_TIMER_EXPIRED(UIData.stHoldAtTargetTimer, MAX_NUM_CAR_CLUB_REP_UI_HOLD_MAX_TIME_MS)
			RESET_NET_TIMER(UIData.stHoldAtTargetTimer)
		ENDIF
	ENDIF
	
	// Don't display when walking in or out of property
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		bAnimateUIBar = FALSE
		bDisplayUIBar = FALSE
	ENDIF
	
	IF (bAnimateUIBar)
		
		// Reset hold timer if rep is earned back to back
		IF HAS_NET_TIMER_STARTED(UIData.stHoldAtTargetTimer)
			RESET_NET_TIMER(UIData.stHoldAtTargetTimer)
		ENDIF
		
		INT iCarClubRepTier 	= GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
		INT iTargetBarValue		= GET_LOCAL_PLAYERS_CAR_CLUB_REP()
		INT iRepTiersGained		= 0
		BOOL bHoldAtTarget		= TRUE
		
		// Rep tier has changed
		IF (UIData.iCarClubRepTier != iCarClubRepTier)
			iRepTiersGained 	= (iCarClubRepTier-UIData.iCarClubRepTier)
			UIData.bMultiTiered = TRUE
		ENDIF
		
		IF (iRepTiersGained > 0)
			iTargetBarValue 	= GET_CAR_CLUB_REP_TOTAL_FOR_TIER(UIData.iCarClubRepTier+1)
			
			// Only hold bar when we have finished animating it.
			// This check is needed so we don't hold bar when just reaching new tier.
			IF (iTargetBarValue != GET_LOCAL_PLAYERS_CAR_CLUB_REP())
				bHoldAtTarget 	= FALSE
			ENDIF
		ENDIF
		
		SET_CAR_CLUB_REP_UI_BAR_ANIM_DATA(UIData, UIData.iCarClubRep, iTargetBarValue)
		IF ANIMATE_CAR_CLUB_REP_UI_BAR(UIData, UIData.iCarClubRep)
			CLEAR_CAR_CLUB_REP_UI_BAR_ANIM_DATA(UIData, UIData.iCarClubRep, bHoldAtTarget)
			IF (bHoldAtTarget)
				START_NET_TIMER(UIData.stHoldAtTargetTimer)
			ENDIF
		ENDIF
		
		iBarPulseTime = (MAX_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS/4)
		
	ENDIF
	
	INT iBarTarget 		= GET_CAR_CLUB_REP_FOR_TIER(UIData.iCarClubRepTier+1)
	INT iBarTierRep		= iBarTarget-(GET_CAR_CLUB_REP_TOTAL_FOR_TIER(UIData.iCarClubRepTier+1)-UIData.iCarClubRep)
	
	// Max rep
	IF (UIData.iCarClubRepTier = MAX_NUM_CAR_CLUB_REP_TIERS)
		iBarTarget -= 1
	ENDIF
	
	IF (bDisplayUIBar)
		SET_PRIORITY_HUD_ELEMENT(PROGRESSHUD_METER, HUD_PRIORITY_FIRST)
		SET_PRIORITY_HUD_ELEMENT(PROGRESSHUD_SCORE, HUD_PRIORITY_SECOND)
		
		DRAW_GENERIC_SCORE(UIData.iCarClubRepTier, "CCR_UI_LVL", -1, HUD_COLOUR_WHITE, HUDORDER_TENTHBOTTOM)
		DRAW_GENERIC_METER(iBarTierRep, iBarTarget, "CCR_UI_PRG", HUD_COLOUR_PINKLIGHT, DEFAULT, HUDORDER_NINETHBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PERCENTAGE_METER_LINE_ALL_20,
						   DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, HUD_COLOUR_REDDARK, iBarPulseTime, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
		
		// Play Level Up Sound On New Tier
		IF (UIData.iStartingRepTier < GET_CAR_CLUB_REP_TIER(UIData.iCarClubRep))
			UIData.iStartingRepTier++
			PLAY_SOUND_FRONTEND(-1, "MP_AWARD", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	ELSE
		CLEAR_ALL_PRIORITY_HUD_ELEMENTS()
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL HAS_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_CONTINUED(BOOL &bResetProgress)
	
	// New streak has started
	IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS) = 0)
		RETURN TRUE
	ELSE
		// Get saved posix time as TIMEOFDAY
		UGC_DATE lastSavedProgressDate
		CONVERT_POSIX_TIME(GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_POSIX), lastSavedProgressDate)
		
		TIMEOFDAY lastSavedProgressTimeOfDay
		SET_TIMEOFDAY(lastSavedProgressTimeOfDay, lastSavedProgressDate.nSecond, lastSavedProgressDate.nMinute, lastSavedProgressDate.nHour, 
					  lastSavedProgressDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, lastSavedProgressDate.nMonth-1), lastSavedProgressDate.nYear)
		
		// Get current posix time as TIMEOFDAY
		UGC_DATE currentDate
		CONVERT_POSIX_TIME(GET_CLOUD_TIME_AS_INT(), currentDate)
		
		TIMEOFDAY currentTimeOfDay
		SET_TIMEOFDAY(currentTimeOfDay, currentDate.nSecond, currentDate.nMinute, currentDate.nHour, 
					  currentDate.nDay, INT_TO_ENUM(MONTH_OF_YEAR, currentDate.nMonth-1), currentDate.nYear)
		
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Last Saved Day: ", GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Last Saved Month: ", GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Last Saved Year: ", GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - ")
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Current Day: ", GET_TIMEOFDAY_DAY(currentTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Current Month: ", GET_TIMEOFDAY_MONTH(currentTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Current Year: ", GET_TIMEOFDAY_YEAR(currentTimeOfDay))
		
		// Check if we have already updated streak for today
		IF GET_TIMEOFDAY_YEAR(currentTimeOfDay) 	= GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay)
		AND GET_TIMEOFDAY_MONTH(currentTimeOfDay) 	= GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay)
		AND GET_TIMEOFDAY_DAY(currentTimeOfDay) 	= GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay)
			// Streak already updated today - exit
			PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Streak has already been updated today, exit")
			RETURN FALSE
		ENDIF
		
		// Get the next streak date
		INT iDaysInMonth 				= GET_NUMBER_OF_DAYS_IN_MONTH(GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay), GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay))
		INT iNextStreakDay 				= GET_TIMEOFDAY_DAY(lastSavedProgressTimeOfDay)+1
		MONTH_OF_YEAR eNextStreakMonth	= GET_TIMEOFDAY_MONTH(lastSavedProgressTimeOfDay)
		INT iNextStreakYear				= GET_TIMEOFDAY_YEAR(lastSavedProgressTimeOfDay)
		
		IF (iNextStreakDay > iDaysInMonth)
			iNextStreakDay = 1
			
			INT iNextStreakMonth = ENUM_TO_INT(eNextStreakMonth)+1
			IF iNextStreakMonth > ENUM_TO_INT(DECEMBER)
				iNextStreakMonth = ENUM_TO_INT(JANUARY)
				iNextStreakYear++
			ENDIF
			eNextStreakMonth = INT_TO_ENUM(MONTH_OF_YEAR, iNextStreakMonth)
		ENDIF
		
		// Populate TIMEOFDAY with next streak date
		TIMEOFDAY nextStreakTimeOfDay
		SET_TIMEOFDAY(nextStreakTimeOfDay, 0, 0, 0, iNextStreakDay, eNextStreakMonth, iNextStreakYear)
		
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Next Streak Day: ", GET_TIMEOFDAY_DAY(nextStreakTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Next Streak Month: ", GET_TIMEOFDAY_MONTH(nextStreakTimeOfDay))
		PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Next Streak Year: ", GET_TIMEOFDAY_YEAR(nextStreakTimeOfDay))
		
		// Check if we are on the next streak date
		IF GET_TIMEOFDAY_YEAR(currentTimeOfDay) 	= GET_TIMEOFDAY_YEAR(nextStreakTimeOfDay)
		AND GET_TIMEOFDAY_MONTH(currentTimeOfDay) 	= GET_TIMEOFDAY_MONTH(nextStreakTimeOfDay)
		AND GET_TIMEOFDAY_DAY(currentTimeOfDay) 	= GET_TIMEOFDAY_DAY(nextStreakTimeOfDay)
			// Streak continues
			PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Streak continues!")
			RETURN TRUE
		ELSE
			// Streak broken
			PRINTLN("[CAR_CLUB_REP] HAS_CAR_CLUB_REP_CAR_MEET_DAY_STREAK_CONTINUED - Streak is broken!")
			bResetProgress = TRUE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS()
	
	IF NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
		EXIT
	ENDIF
	
	BOOL bResetProgress = FALSE
	
	IF HAS_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_CONTINUED(bResetProgress)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_POSIX, GET_CLOUD_TIME_AS_INT())
		
		INT iStreakProgress = GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS, iStreakProgress+1)
		
		PRINTLN("[CAR_CLUB_REP] UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS - Day streak progress increased to: ", iStreakProgress+1)
		
		IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS) = MAX_NUM_CAR_CLUB_VISIT_CAR_MEET_DAY_STREAK_30)
			PRINTLN("[CAR_CLUB_REP] UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS - 30 Day streak has been reached! Awarding rep and resetting progress")
			AWARD_CAR_CLUB_REP(EARN_REP_TYPE_VISIT_CAR_CLUB_30_DAYS)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS, 0)  // Reset progress after reaching max day streak
			
		ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS) = MAX_NUM_CAR_CLUB_VISIT_CAR_MEET_DAY_STREAK_14)
			PRINTLN("[CAR_CLUB_REP] UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS - 14 Day streak has been reached! Awarding rep")
			AWARD_CAR_CLUB_REP(EARN_REP_TYPE_VISIT_CAR_CLUB_14_DAYS)
			
		ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS) = MAX_NUM_CAR_CLUB_VISIT_CAR_MEET_DAY_STREAK_7)
			PRINTLN("[CAR_CLUB_REP] UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS - 7 Day streak has been reached! Awarding rep")
			AWARD_CAR_CLUB_REP(EARN_REP_TYPE_VISIT_CAR_CLUB_7_DAYS)
		ENDIF
	ELSE
		IF (bResetProgress)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS, 0)
			PRINTLN("[CAR_CLUB_REP] UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS - Day streak broken! Resetting progress to: 0")
		ELSE
			PRINTLN("[CAR_CLUB_REP] UPDATE_CAR_CLUB_REP_VISIT_CAR_MEET_DAY_STREAK_PROGRESS - Already updated the day streak today - Do nothing")
		ENDIF
	ENDIF
	
	REQUEST_SAVE(SSR_REASON_CM_PROGRESSION_STREAK, STAT_SAVETYPE_IMMEDIATE_FLUSH)
ENDPROC

PROC INITIALISE_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY_POSIX_TIME(CAR_CLUB_REP_STRUCT &CarClubRepData)
	
	IF NOT IS_BIT_SET(CarClubRepData.iBS, BS_CAR_CLUB_REP_DATA_SET_TIME_ENTERED_PROPERTY)
		SET_BIT(CarClubRepData.iBS, BS_CAR_CLUB_REP_DATA_SET_TIME_ENTERED_PROPERTY)
		CarClubRepData.TrackedTime.eTimeOfDay 	= GET_CURRENT_TIMEOFDAY_POSIX()
		CarClubRepData.TrackedTime.iMinute		= GET_TIMEOFDAY_MINUTE(CarClubRepData.TrackedTime.eTimeOfDay)
		CarClubRepData.TrackedTime.iSeconds 	= GET_TIMEOFDAY_SECOND(CarClubRepData.TrackedTime.eTimeOfDay)
	ENDIF
	
ENDPROC

FUNC BOOL HAS_REAL_TIME_MINUITE_PASSED(CAR_CLUB_REP_STRUCT &CarClubRepData)
	
	CarClubRepData.CurrentTime.eTimeOfDay 	= GET_CURRENT_TIMEOFDAY_POSIX()
	CarClubRepData.CurrentTime.iMinute		= GET_TIMEOFDAY_MINUTE(CarClubRepData.CurrentTime.eTimeOfDay)
	
	IF (CarClubRepData.TrackedTime.iMinute != CarClubRepData.CurrentTime.iMinute)
		CarClubRepData.CurrentTime.iSeconds = GET_TIMEOFDAY_SECOND(CarClubRepData.CurrentTime.eTimeOfDay)
		
		IF (CarClubRepData.CurrentTime.iSeconds >= CarClubRepData.TrackedTime.iSeconds)
			SET_TIMEOFDAY_MINUTE(CarClubRepData.TrackedTime.eTimeOfDay, CarClubRepData.CurrentTime.iMinute)
			CarClubRepData.TrackedTime.iMinute = CarClubRepData.CurrentTime.iMinute
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_IN_GAME_DAY_PASSED(BOOL bSandbox)
	
	IF (GET_FRAME_COUNT() % 60 = 0)
		RETURN FALSE
	ENDIF
	
	MP_INT_STATS LastAwardedRepPosixTimeStat = MP_STAT_CC_REP_TM_CM_POSIX
	IF (bSandbox)
		LastAwardedRepPosixTimeStat = MP_STAT_CC_REP_TM_SB_POSIX
	ENDIF
	
	MP_INT_STATS LastAwardedRepGameDayStat = MP_STAT_CC_REP_TM_CM_GMDY
	IF (bSandbox)
		LastAwardedRepGameDayStat = MP_STAT_CC_REP_TM_SB_GMDY
	ENDIF
	
	BOOL bNewGameDay 					= FALSE
	INT iPosixTime						= GET_CLOUD_TIME_AS_INT()
	DAY_OF_WEEK eCurrentGameDay			= GET_CLOCK_DAY_OF_WEEK()
	DAY_OF_WEEK eLastAwardedRepGameDay	= INT_TO_ENUM(DAY_OF_WEEK, GET_MP_INT_CHARACTER_STAT(LastAwardedRepGameDayStat))
	
	// Check in-game day
	IF (eLastAwardedRepGameDay != eCurrentGameDay)
		SET_MP_INT_CHARACTER_STAT(LastAwardedRepGameDayStat, ENUM_TO_INT(eCurrentGameDay))
		bNewGameDay = TRUE
	ENDIF
	
	// Safety net - Player could be returning after a few real world days, but the
	//				in-game day is the same as when they last logged in. This checks posix time.
	IF (iPosixTime >= GET_MP_INT_CHARACTER_STAT(LastAwardedRepPosixTimeStat)+MAX_NUM_CAR_CLUB_POSIX_GAME_DAY_TIME)
		SET_MP_INT_CHARACTER_STAT(LastAwardedRepPosixTimeStat, iPosixTime)
		bNewGameDay = TRUE
	ENDIF
	
	RETURN bNewGameDay
ENDFUNC

PROC MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY(CAR_CLUB_REP_STRUCT &CarClubRepData, EARN_CAR_CLUB_REP_TYPE eEarnRepType, BOOL bSandbox)
	
	MP_INT_STATS LastAwardedRepPosixTimeStat = MP_STAT_CC_REP_TM_CM_POSIX
	IF (bSandbox)
		LastAwardedRepPosixTimeStat = MP_STAT_CC_REP_TM_SB_POSIX
	ENDIF
	
	MP_INT_STATS LastAwardedRepGameDayStat = MP_STAT_CC_REP_TM_CM_GMDY
	IF (bSandbox)
		LastAwardedRepGameDayStat = MP_STAT_CC_REP_TM_SB_GMDY
	ENDIF
	
	MP_INT_STATS RepAwardedCounterStat = MP_STAT_CC_REP_TM_CM_PRGRSS
	IF (bSandbox)
		RepAwardedCounterStat = MP_STAT_CC_REP_TM_SB_PRGRSS
	ENDIF
	
	// Reset packed stat counter if new in-game day
	IF HAS_IN_GAME_DAY_PASSED(bSandbox)
		SET_MP_INT_CHARACTER_STAT(RepAwardedCounterStat, 0)
		#IF IS_DEBUG_BUILD
		PRINTLN("[CAR_CLUB_REP] MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY - Earn Rep Type: ", DEBUG_GET_CAR_CLUB_EARN_REP_TYPE_STRING(eEarnRepType), " New in-game day, resetting progress")
		#ENDIF
	ENDIF
	
	// Max time spent in property rep earned for this game day
	IF (GET_MP_INT_CHARACTER_STAT(RepAwardedCounterStat) >= MAX_NUM_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY_COUNTER)
		EXIT
	ENDIF
	
	INITIALISE_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY_POSIX_TIME(CarClubRepData)
	IF HAS_REAL_TIME_MINUITE_PASSED(CarClubRepData)
		
		CarClubRepData.iMinutesPassed++
		PRINTLN("[CAR_CLUB_REP] MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY - ", CarClubRepData.iMinutesPassed, " minute(s) passed! Need: ", g_sMPTunables.iTUNER_CARCLUB_TIME_XP_MINUTES,  " minute(s) to receive time spent in property rep")
		
		// How many minutes need to pass before gaining car club rep?
		IF (CarClubRepData.iMinutesPassed >= g_sMPTunables.iTUNER_CARCLUB_TIME_XP_MINUTES)
			
			// Reset time progress
			CarClubRepData.iMinutesPassed = 0
			PRINTLN("[CAR_CLUB_REP] MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY - Player has spent: ", g_sMPTunables.iTUNER_CARCLUB_TIME_XP_MINUTES,  " minutes inside the property, awarding rep")
			
			// Increase time spent in property rep awarded counter
			INT iProgress = GET_MP_INT_CHARACTER_STAT(RepAwardedCounterStat)
			IF (iProgress < MAX_NUM_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY_COUNTER)
				SET_MP_INT_CHARACTER_STAT(RepAwardedCounterStat, iProgress+1)
			ENDIF
			
			// Save game day
			SET_MP_INT_CHARACTER_STAT(LastAwardedRepGameDayStat, ENUM_TO_INT(GET_CLOCK_DAY_OF_WEEK()))
			
			// Save new posix time
			SET_MP_INT_CHARACTER_STAT(LastAwardedRepPosixTimeStat, GET_CLOUD_TIME_AS_INT())
			
			// Award rep
			AWARD_CAR_CLUB_REP(eEarnRepType)
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[CAR_CLUB_REP] MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY - Earn Rep Type: ", DEBUG_GET_CAR_CLUB_EARN_REP_TYPE_STRING(eEarnRepType), " Progress: ", GET_MP_INT_CHARACTER_STAT(RepAwardedCounterStat))
			#ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_CAR_MEET(CAR_CLUB_REP_STRUCT &CarClubRepData)
	IF NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
		EXIT
	ENDIF
	
	EARN_CAR_CLUB_REP_TYPE eRepType = EARN_REP_TYPE_TIME_SPENT_CAR_MEET
	IF IS_PED_WEARING_CAR_MEET_MERCH_STORE_ITEM(PLAYER_PED_ID())
		eRepType = EARN_REP_TYPE_TIME_SPENT_CAR_MEET_MERCH
	ENDIF
	
	MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY(CarClubRepData, eRepType, FALSE)
ENDPROC

PROC MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_CAR_MEET_SANDBOX(CAR_CLUB_REP_STRUCT &CarClubRepData)
	IF NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
		EXIT
	ENDIF
	
	EARN_CAR_CLUB_REP_TYPE eRepType = EARN_REP_TYPE_TIME_SPENT_SANDBOX
	IF IS_PED_WEARING_CAR_MEET_MERCH_STORE_ITEM(PLAYER_PED_ID())
		eRepType = EARN_REP_TYPE_TIME_SPENT_SANDBOX_MERCH
	ENDIF
	
	MAINTAIN_CAR_CLUB_REP_TIME_SPENT_IN_PROPERTY(CarClubRepData, eRepType, TRUE)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ DEBUG WIDGETS ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC INITIALISE_CAR_CLUB_REP_DEBUG_WIDGETS(CAR_CLUB_REP_DEBUG_DATA &DebugData)
	DebugData.iDayStreakProgress = GET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS)
ENDPROC

PROC CREATE_CAR_CLUB_REP_DEBUG_WIDGETS(CAR_CLUB_REP_DEBUG_DATA &DebugData)
	INITIALISE_CAR_CLUB_REP_DEBUG_WIDGETS(DebugData)
	
	START_WIDGET_GROUP("Car Club Rep")
		ADD_WIDGET_BOOL("Enable Debug", DebugData.bEnableDebug)
		
		START_WIDGET_GROUP("On Screen Debug")
			ADD_WIDGET_BOOL("Display", DebugData.OnScreenData.bDisplay)
			ADD_WIDGET_BOOL("Enable Reposition", DebugData.OnScreenData.bReposition)
			ADD_WIDGET_STRING("Note: Progress bar only animates if using 'Rep/Add Rep'.")
			ADD_WIDGET_STRING("Note: Do not use on screen debug when in car meet interior. Will conflict with HUD.")
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Rep")
			ADD_WIDGET_INT_SLIDER("Rep To Add", DebugData.RepWidgetData.iRepToAdd, 0, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("Add Rep", DebugData.RepWidgetData.bAddRep)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Rep To Set", DebugData.RepWidgetData.iRepToSet, 0, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("Set Rep", DebugData.RepWidgetData.bSetRep)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Tier To Set", DebugData.RepWidgetData.iTierToSet, 0, MAX_NUM_CAR_CLUB_REP_TIERS, 1)
			ADD_WIDGET_BOOL("Set Tier", DebugData.RepWidgetData.bSetTier)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_READ_ONLY("Current Rep", DebugData.RepWidgetData.iCurrentRep)
			ADD_WIDGET_INT_READ_ONLY("Current Tier", DebugData.RepWidgetData.iCurrentTier)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Tier Calculator")
			ADD_WIDGET_INT_SLIDER("Car Club Rep", DebugData.RepTierCalculatorData.iTierCalRep, 0, HIGHEST_INT, 1)
			ADD_WIDGET_INT_READ_ONLY("Tier", DebugData.RepTierCalculatorData.iTierCalTier)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Rep Calculator")
			ADD_WIDGET_INT_SLIDER("Tier", DebugData.RepCalculatorData.iRepCalTier, 1, MAX_NUM_CAR_CLUB_REP_TIERS, 1)
			ADD_WIDGET_INT_READ_ONLY("Total Rep For Tier", DebugData.RepCalculatorData.iRepCalTotalRep)
			ADD_WIDGET_STRING("")
			ADD_WIDGET_INT_SLIDER("Starting Tier", DebugData.RepCalculatorData.iRepCalStartTier, 1, MAX_NUM_CAR_CLUB_REP_TIERS, 1)
			ADD_WIDGET_INT_SLIDER("Ending Tier", DebugData.RepCalculatorData.iRepCalEndTier, 1, MAX_NUM_CAR_CLUB_REP_TIERS, 1)
			ADD_WIDGET_INT_READ_ONLY("Rep Between Tiers", DebugData.RepCalculatorData.iRepCalRepBetweenTiers)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Rep Info")
			ADD_WIDGET_BOOL("Output Rep Table", DebugData.RepInfoData.bOutputRepTable)
			DebugData.RepInfoData.twLogSearch = ADD_TEXT_WIDGET("Search In Logs: ")
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_STRING("")
		START_WIDGET_GROUP("Visit Car Meet Streak")
			ADD_WIDGET_INT_SLIDER("Posix Time", DebugData.iVisitPosixTime, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("Override Last Saved Visit Time", DebugData.bOverridePosixTime)
			ADD_WIDGET_INT_SLIDER("Day Streak Progress", DebugData.iDayStreakProgress, 0, MAX_NUM_CAR_CLUB_VISIT_CAR_MEET_DAY_STREAK_30, 1)
			ADD_WIDGET_BOOL("Override Day Streak Progress", DebugData.bOverrideDayStreakProgress)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC OUTPUT_CAR_CLUB_REP_DEBUG_TABLE()
	
	PRINTLN("[CAR_CLUB_REP] ")
	PRINTLN("[CAR_CLUB_REP] Tier		Rep Required	Total Rep Required		TUNER_CARCLUB_REP_INCREMENT_PER_TIER_200: ", g_sMPTunables.iTUNER_CARCLUB_REP_INCREMENT_PER_TIER_200)
	
	INT iTier			= 0
	INT iTierRep 		= 0
	INT iTotalTierRep	= 0
	TEXT_LABEL_63 tlRepInfo
	
	FOR iTier = 0 TO MAX_NUM_CAR_CLUB_REP_TIERS
		iTierRep 		= GET_CAR_CLUB_REP_FOR_TIER(iTier)
		iTotalTierRep	= GET_CAR_CLUB_REP_TOTAL_FOR_TIER(iTier)
			
		tlRepInfo = iTier
		tlRepInfo += "			"
		
		tlRepInfo += iTierRep
		tlRepInfo += "				"
		
		tlRepInfo += iTotalTierRep
		PRINTLN("[CAR_CLUB_REP] ", tlRepInfo)
		
		tlRepInfo = ""
	ENDFOR
	
ENDPROC

/// PURPOSE:
///    DEBUG ONLY - Sets the local players car club rep value.
/// PARAMS:
///    iCarClubRep - Rep value to set.
PROC SET_CAR_CLUB_REP(INT iCarClubRep)
	
	IF (PLAYER_ID() = INVALID_PLAYER_INDEX())
	OR g_sMPTunables.bBLOCK_CAR_CLUB_REP_CHANGE
	OR NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
		EXIT
	ENDIF
	
	IF (iCarClubRep < 0)
		iCarClubRep = 0
	ENDIF
	
	INT iMaxCarClubRep = GET_MAX_CAR_CLUB_REP()
	IF (iCarClubRep > iMaxCarClubRep)
		iCarClubRep = iMaxCarClubRep
	ENDIF
	
	INT iNewCarClubRepTier = GET_CAR_CLUB_REP_TIER(iCarClubRep)
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CAR_CLUB_REP, iCarClubRep)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubRep 	= iCarClubRep
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubTier = iNewCarClubRepTier
	
	UNLOCK_CAR_CLUB_REP_REWARDS(0, iNewCarClubRepTier)
ENDPROC

/// PURPOSE:
///    DEBUG ONLY - Sets the local players car club rep tier. Updates the local players rep based on the tier.
/// PARAMS:
///    iTier - Car club tier to set.
PROC SET_CAR_CLUB_REP_TIER(INT iTier)
	
	IF (PLAYER_ID() = INVALID_PLAYER_INDEX())
	OR g_sMPTunables.bBLOCK_CAR_CLUB_REP_CHANGE
	OR NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
		EXIT
	ENDIF
	
	IF (iTier < 1)
		iTier = 1
	ENDIF
	
	IF (iTier > MAX_NUM_CAR_CLUB_REP_TIERS)
		iTier = MAX_NUM_CAR_CLUB_REP_TIERS
	ENDIF
	
	INT iRepForTier = GET_CAR_CLUB_REP_TOTAL_FOR_TIER(iTier)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CAR_CLUB_REP, iRepForTier)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubRep 	= iRepForTier
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iCarClubTier = iTier
	
	UNLOCK_CAR_CLUB_REP_REWARDS(0, iTier)
ENDPROC

/// PURPOSE: Slightly stripped down version of MAINTAIN_CAR_CLUB_REP_UI for the freemode on screen debug
PROC MAINTAIN_FREEMODE_CAR_CLUB_REP_UI_DEBUG(CAR_CLUB_REP_UI_STRUCT &UIData, FLOAT fPosX, FLOAT fPosY)
	
	#IF IS_DEBUG_BUILD
	DEBUG_UPDATE_CAR_CLUB_REP(UIData)
	#ENDIF
	
	BOOL bAnimateUIBar 	= FALSE
	INT iBarPulseTime	= 0
	
	// Rep has changed
	IF (UIData.iCarClubRep < GET_LOCAL_PLAYERS_CAR_CLUB_REP())
	OR IS_CAR_CLUB_REP_UI_BAR_ANIMATING(UIData)
		bAnimateUIBar = TRUE
	ENDIF
	
	IF (bAnimateUIBar)
		
		INT iCarClubRepTier 	= GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
		INT iTargetBarValue		= GET_LOCAL_PLAYERS_CAR_CLUB_REP()
		INT iRepTiersGained		= 0
		BOOL bHoldAtTarget		= TRUE
		
		// Rep tier has changed
		IF (UIData.iCarClubRepTier != iCarClubRepTier)
			iRepTiersGained 	= (iCarClubRepTier-UIData.iCarClubRepTier)
			UIData.bMultiTiered = TRUE
		ENDIF
		
		IF (iRepTiersGained > 0)
			iTargetBarValue 	= GET_CAR_CLUB_REP_TOTAL_FOR_TIER(UIData.iCarClubRepTier+1)
			
			// Only hold bar when we have finished animating it.
			// This check is needed so we hold bar when just reaching new tier.
			IF (iTargetBarValue != GET_LOCAL_PLAYERS_CAR_CLUB_REP())
				bHoldAtTarget 	= FALSE
			ENDIF
		ENDIF
		
		SET_CAR_CLUB_REP_UI_BAR_ANIM_DATA(UIData, UIData.iCarClubRep, iTargetBarValue)
		IF ANIMATE_CAR_CLUB_REP_UI_BAR(UIData, UIData.iCarClubRep)
			CLEAR_CAR_CLUB_REP_UI_BAR_ANIM_DATA(UIData, UIData.iCarClubRep, bHoldAtTarget)
		ENDIF
		
		iBarPulseTime = (MAX_NUM_CAR_CLUB_REP_UI_ANIMATE_TIME_MS/4)
		
	ENDIF
	
	INT iBarTarget 		= GET_CAR_CLUB_REP_FOR_TIER(UIData.iCarClubRepTier+1)
	INT iBarTierRep		= iBarTarget-(GET_CAR_CLUB_REP_TOTAL_FOR_TIER(UIData.iCarClubRepTier+1)-UIData.iCarClubRep)
	
	// Max rep
	IF (UIData.iCarClubRepTier = MAX_NUM_CAR_CLUB_REP_TIERS)
		iBarTarget -= 1
	ENDIF
	
	DRAW_GENERIC_METER(iBarTierRep, iBarTarget, "STRING", HUD_COLOUR_PINKLIGHT, DEFAULT, HUDORDER_TOP, fPosX, fPosY, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, PERCENTAGE_METER_LINE_ALL_20,
					   DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, HUD_COLOUR_REDDARK, iBarPulseTime, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, ENUM_TO_INT(GFX_ORDER_AFTER_HUD_PRIORITY_HIGH), FALSE)
ENDPROC

PROC DISPLAY_CAR_CLUB_REP_ON_SCREEN_DEBUG(CAR_CLUB_REP_DEBUG_DATA &DebugData)
	
	// Reposition
	IF DebugData.OnScreenData.bReposition	
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			IF NOT IS_PC_VERSION()
				GET_MOUSE_POSITION(DebugData.OnScreenData.fDisplayPosX, DebugData.OnScreenData.fDisplayPosY)
			ELSE
				DebugData.OnScreenData.fDisplayPosX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
				DebugData.OnScreenData.fDisplayPosY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
			ENDIF
		ENDIF
	ENDIF
	
	// Display
	FLOAT fYPos					= 0.0
	FLOAT fRowSpacing 			= 0.04
	
	FLOAT fOffsetX = (DebugData.OnScreenData.fDisplayPosX - 0.204)
	FLOAT fOffsetY = (DebugData.OnScreenData.fDisplayPosY - 0.1875)
	
	// Background
	IF (DebugData.OnScreenData.fDisplayPosX = -1.0)
		DRAW_RECT(0.100, 0.094, 0.150, 0.130, 0, 0, 0, 200)
		fOffsetX = 0.0
		fOffsetY = 0.0
	ELSE
		DRAW_RECT(DebugData.OnScreenData.fDisplayPosX-0.104, DebugData.OnScreenData.fDisplayPosY-0.094, 0.150, 0.130, 0, 0, 0, 200)
	ENDIF
	
	//Title
    SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.0275 + fOffsetX, 0.0295 + fOffsetY, "STRING", "Car Club Rep")
	
	// Rep
	fYPos = (0.5 * fRowSpacing) + 0.05
	fYPos += fOffsetY
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.03 + fOffsetX, fYPos, "STRING", "Rep: ")
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.1 + fOffsetX, fYPos, "NUMBER", DebugData.FreemodeProgressBarData.iCarClubRep)
	
	// Tier
	fYPos = (1.2 * fRowSpacing) + 0.05
	fYPos += fOffsetY
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.03 + fOffsetX, fYPos, "STRING", "Tier: ")
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_NUMBER(0.1 + fOffsetX, fYPos, "NUMBER", DebugData.FreemodeProgressBarData.iCarClubRepTier)
	
	// Progress
	fYPos = (1.9 * fRowSpacing) + 0.05
	fYPos += fOffsetY
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(0.03 + fOffsetX, fYPos, "STRING", "Progress: ")
	MAINTAIN_FREEMODE_CAR_CLUB_REP_UI_DEBUG(DebugData.FreemodeProgressBarData, 0.102 + fOffsetX, fYPos-0.02)
	
ENDPROC

PROC UPDATE_CAR_CLUB_REP_DEBUG_WIDGETS(CAR_CLUB_REP_DEBUG_DATA &DebugData)
	
	IF NOT DebugData.bEnableDebug
		EXIT
	ENDIF
	
	IF (DebugData.OnScreenData.bDisplay)
		DISPLAY_CAR_CLUB_REP_ON_SCREEN_DEBUG(DebugData)
	ENDIF
	
	IF (DebugData.RepWidgetData.bAddRep)
		DebugData.RepWidgetData.bAddRep = FALSE
		ADD_CAR_CLUB_REP(DebugData.RepWidgetData.iRepToAdd, EARN_REP_TYPE_INVALID)
	ENDIF
	
	IF (DebugData.RepWidgetData.bSetRep)
		DebugData.RepWidgetData.bSetRep = FALSE
		DebugData.RepWidgetData.bGlobalSettingRep = TRUE
		SET_CAR_CLUB_REP(DebugData.RepWidgetData.iRepToSet)
	ENDIF
	
	IF (DebugData.RepWidgetData.bSetTier)
		DebugData.RepWidgetData.bSetTier = FALSE
		DebugData.RepWidgetData.bGlobalSettingRep = TRUE
		SET_CAR_CLUB_REP_TIER(DebugData.RepWidgetData.iTierToSet)
	ENDIF
	
	DebugData.RepWidgetData.iCurrentRep = GET_LOCAL_PLAYERS_CAR_CLUB_REP()
	DebugData.RepWidgetData.iCurrentTier = GET_LOCAL_PLAYERS_CAR_CLUB_REP_TIER()
	
	IF (DebugData.RepTierCalculatorData.iTierCalRepCached != DebugData.RepTierCalculatorData.iTierCalRep)
		DebugData.RepTierCalculatorData.iTierCalRepCached = DebugData.RepTierCalculatorData.iTierCalRep
		DebugData.RepTierCalculatorData.iTierCalTier = GET_CAR_CLUB_REP_TIER(DebugData.RepTierCalculatorData.iTierCalRep)
	ENDIF
	
	IF (DebugData.RepCalculatorData.iRepCalTierCached != DebugData.RepCalculatorData.iRepCalTier)
		DebugData.RepCalculatorData.iRepCalTierCached = DebugData.RepCalculatorData.iRepCalTier
		DebugData.RepCalculatorData.iRepCalTotalRep = GET_CAR_CLUB_REP_TOTAL_FOR_TIER(DebugData.RepCalculatorData.iRepCalTier)
	ENDIF
	
	IF (DebugData.RepCalculatorData.iRepCalStartTierCached != DebugData.RepCalculatorData.iRepCalStartTier)
	OR (DebugData.RepCalculatorData.iRepCalEndTierCached != DebugData.RepCalculatorData.iRepCalEndTier)
		DebugData.RepCalculatorData.iRepCalStartTierCached = DebugData.RepCalculatorData.iRepCalStartTier
		DebugData.RepCalculatorData.iRepCalEndTierCached = DebugData.RepCalculatorData.iRepCalEndTier
		
		IF DebugData.RepCalculatorData.iRepCalEndTier <= DebugData.RepCalculatorData.iRepCalStartTier
			DebugData.RepCalculatorData.iRepCalRepBetweenTiers = 0
		ELSE
			INT iEndingTierRep = GET_CAR_CLUB_REP_TOTAL_FOR_TIER(DebugData.RepCalculatorData.iRepCalEndTier)
			INT iStartingTierRep = GET_CAR_CLUB_REP_TOTAL_FOR_TIER(DebugData.RepCalculatorData.iRepCalStartTier)
			DebugData.RepCalculatorData.iRepCalRepBetweenTiers = (iEndingTierRep-iStartingTierRep)
		ENDIF
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(DebugData.RepInfoData.twLogSearch)
		SET_CONTENTS_OF_TEXT_WIDGET(DebugData.RepInfoData.twLogSearch, "[CAR_CLUB_REP]")
	ENDIF
	
	IF (DebugData.RepInfoData.bOutputRepTable)
		DebugData.RepInfoData.bOutputRepTable = FALSE
		OUTPUT_CAR_CLUB_REP_DEBUG_TABLE()
	ENDIF
	
	IF (DebugData.bOverridePosixTime)
		DebugData.bOverridePosixTime = FALSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_POSIX, DebugData.iVisitPosixTime)
	ENDIF
	
	IF (DebugData.bOverrideDayStreakProgress)
		DebugData.bOverrideDayStreakProgress = FALSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CC_REP_VST_CM_PRGRSS, DebugData.iDayStreakProgress)
	ENDIF
	
ENDPROC
#ENDIF	// IS_DEBUG_BUILD
#ENDIF	// FEATURE_TUNER
