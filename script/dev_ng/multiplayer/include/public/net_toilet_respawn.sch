USING "globals.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "net_spawn.sch"

#IF FEATURE_CASINO
ENUM TOILET_RESPAWN_STATE
	TOILET_RESPAWN_STATE_INIT = 0,
	TOILET_RESPAWN_STATE_WAIT,
	TOILET_RESPAWN_STATE_RUN,
	TOILET_RESPAWN_STATE_END
ENDENUM

ENUM TOILET_RESPAWN_SYNC_STATE
	TOILET_RESPAWN_SYNC_INIT = 0,
	TOILET_RESPAWN_SYNC_START,
	TOILET_RESPAWN_SYNC_FADE_IN,
	TOILET_RESPAWN_SYNC_RUN,
	TOILET_RESPAWN_SYNC_CLEANUP
ENDENUM

// SERVER BS
CONST_INT TOILET_RESPAWN_BS_SERVER_LOAD_AUDIO_BANK				0

// PLAYER BS
CONST_INT TOILET_RESPAWN_BS_PLAYER_REQUEST_AUDIO_BANK			0
CONST_INT TOILET_RESPAWN_BS_PLAYER_FINISHED						1

// LOCAL BS
CONST_INT TOILET_RESPAWN_BS_AUDIO_LOADED						0

STRUCT TOILET_RESPAWN_DATA
	CAMERA_INDEX camRespawn
	
	FLOAT fCamFOV = 0.0
	
	INT iBS
	INT iAnim = -1
	INT iToiletSyncSceneID = -1
	INT iPlayerStagger
	
	SCRIPT_TIMER sPassOutTimer
	
	STRING sRespawnAnim
	STRING sRespawnAnimDict
	
	TOILET_RESPAWN_STATE eToiletRespawnState = TOILET_RESPAWN_STATE_INIT
	
	TOILET_RESPAWN_SYNC_STATE eToiletRespawnSyncState = TOILET_RESPAWN_SYNC_INIT
	
	VECTOR vCamLocation
	VECTOR vCamRotation
	VECTOR vLocateCoords1
	VECTOR vLocateCoords2
	VECTOR vSyncSceneLocation
	VECTOR vSyncSceneRotation
ENDSTRUCT

STRUCT TOILET_RESPAWN_SERVER_DATA
	INT iRespawnBS
ENDSTRUCT

STRUCT TOILET_RESPAWN_PLAYER_DATA
	INT iRespawnBS
ENDSTRUCT

PROC SET_TOILET_RESPAWN_STATE(TOILET_RESPAWN_DATA &sToiletRespawnData, TOILET_RESPAWN_STATE eDesiredState)
	sToiletRespawnData.eToiletRespawnState = eDesiredState
	
	PRINTLN("[TOILET_RESPAWN] - SET_TOILET_RESPAWN_STATE - ", ENUM_TO_INT(eDesiredState))
ENDPROC

PROC SET_TOILET_RESPAWN_SYNC_STATE(TOILET_RESPAWN_DATA &sToiletRespawnData, TOILET_RESPAWN_SYNC_STATE eDesiredState)
	sToiletRespawnData.eToiletRespawnSyncState = eDesiredState
	
	PRINTLN("[TOILET_RESPAWN] - SET_TOILET_RESPAWN_SYNC_STATE - ", ENUM_TO_INT(eDesiredState))
ENDPROC

PROC CLEANUP_TOILET_RESPAWN(TOILET_RESPAWN_DATA &sToiletRespawnData)
	IF sToiletRespawnData.eToiletRespawnState > TOILET_RESPAWN_STATE_WAIT
		IF IS_SYNCHRONIZED_SCENE_RUNNING(sToiletRespawnData.iToiletSyncSceneID)
			NETWORK_STOP_SYNCHRONISED_SCENE(sToiletRespawnData.iToiletSyncSceneID)
		ENDIF
		
		REMOVE_ANIM_DICT(sToiletRespawnData.sRespawnAnimDict)
		
		RENDER_SCRIPT_CAMS(FALSE, TRUE)
		
		DESTROY_CAM(sToiletRespawnData.camRespawn)
		
		IF IS_BIT_SET(sToiletRespawnData.iBS, TOILET_RESPAWN_BS_AUDIO_LOADED)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Respawn")
		ENDIF
		
		SET_TOILET_RESPAWN_STATE(sToiletRespawnData, TOILET_RESPAWN_STATE_INIT)
		
		SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_INIT)
		
		PRINTLN("[TOILET_RESPAWN] - CLEANUP_TOILET_RESPAWN")
	ENDIF
ENDPROC

PROC MAINTAIN_TOILET_RESPAWN_AUDIO(TOILET_RESPAWN_SERVER_DATA &sServerData, TOILET_RESPAWN_DATA &sToiletRespawnData)
	IF IS_BIT_SET(sServerData.iRespawnBS, TOILET_RESPAWN_BS_SERVER_LOAD_AUDIO_BANK)
		IF NOT IS_BIT_SET(sToiletRespawnData.iBS, TOILET_RESPAWN_BS_AUDIO_LOADED)
			IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Respawn")
				PRINTLN("[TOILET_RESPAWN] - MAINTAIN_TOILET_RESPAWN_AUDIO - audio bank loaded")
				
				SET_BIT(sToiletRespawnData.iBS, TOILET_RESPAWN_BS_AUDIO_LOADED)
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(sToiletRespawnData.iBS, TOILET_RESPAWN_BS_AUDIO_LOADED)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_BIKER/Respawn")
			
			PRINTLN("[TOILET_RESPAWN] - MAINTAIN_TOILET_RESPAWN_AUDIO - audio bank released")
			
			CLEAR_BIT(sToiletRespawnData.iBS, TOILET_RESPAWN_BS_AUDIO_LOADED)
		ENDIF
	ENDIF
ENDPROC

PROC GET_TOILET_RESPAWN_ANIM(TOILET_RESPAWN_DATA &sToiletRespawnData, INT iAnim)
	IF IS_PED_FEMALE(PLAYER_PED_ID())
		SWITCH iAnim
			CASE 0	sToiletRespawnData.sRespawnAnim = "respawn_a"		BREAK
			CASE 1	sToiletRespawnData.sRespawnAnim = "respawn_b"		BREAK
			CASE 2	sToiletRespawnData.sRespawnAnim = "respawn_c"		BREAK
			CASE 3	sToiletRespawnData.sRespawnAnim = "respawn_d"		BREAK
		ENDSWITCH
	ELSE
		SWITCH iAnim
			CASE 0	sToiletRespawnData.sRespawnAnim = "respawn_a"		BREAK
			CASE 1	sToiletRespawnData.sRespawnAnim = "respawn_b"		BREAK
			CASE 2	sToiletRespawnData.sRespawnAnim = "respawn_c"		BREAK
			CASE 3	sToiletRespawnData.sRespawnAnim = "respawn_d"		BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC GET_RANDOM_TOILET_RESPAWN_ANIM(TOILET_RESPAWN_DATA &sToiletRespawnData)
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 4)
	
	IF (sToiletRespawnData.iAnim = iRandom)
		iRandom++
		
		IF iRandom >= 4
			iRandom = 0
		ENDIF
	ENDIF
	
	sToiletRespawnData.iAnim = iRandom
	
	GET_TOILET_RESPAWN_ANIM(sToiletRespawnData, iRandom)
ENDPROC

FUNC BOOL RUN_TOILET_RESPAWN(TOILET_RESPAWN_DATA &sToiletRespawnData, INTERIOR_INSTANCE_INDEX iInteriorID)
	BOOL bCanRemoveHelmet = TRUE
	
	INT iLocalRespawnScene
	
	SWITCH sToiletRespawnData.eToiletRespawnSyncState
		CASE TOILET_RESPAWN_SYNC_INIT
			BOOL bInteriorIsReady
			
			bInteriorIsReady = TRUE
			
			REQUEST_ANIM_DICT(sToiletRespawnData.sRespawnAnimDict)
			
			SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_RUNNING_SPAWN_ACTIVITY)
			
			#IF FEATURE_CASINO_HEIST
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				bInteriorIsReady = FALSE
				
				IF IS_VALID_INTERIOR(iInteriorID)
					IF IS_INTERIOR_READY(iInteriorID)
						bInteriorIsReady = TRUE
					ELSE
						PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Pinning interior")
						
						PIN_INTERIOR_IN_MEMORY(iInteriorID)
					ENDIF
				ELSE
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Interior is not valid")
				ENDIF
			ENDIF
			#ENDIF
			
			IF HAS_ANIM_DICT_LOADED(sToiletRespawnData.sRespawnAnimDict)
			AND bInteriorIsReady
				IF REQUEST_SCRIPT_AUDIO_BANK("DLC_BIKER/Respawn")
					GET_RANDOM_TOILET_RESPAWN_ANIM(sToiletRespawnData)
					
					IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
						IF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
						OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
						OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
							bCanRemoveHelmet = FALSE
						ENDIF
						
						IF bCanRemoveHelmet
							CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
						ENDIF
					ENDIF
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), sToiletRespawnData.vSyncSceneLocation)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					
					NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE, TRUE)
					
					FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
					
					sToiletRespawnData.camRespawn = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					
					SET_CAM_PARAMS(sToiletRespawnData.camRespawn, sToiletRespawnData.vCamLocation, sToiletRespawnData.vCamRotation, sToiletRespawnData.fCamFOV)
					
					SET_CAM_FAR_CLIP(sToiletRespawnData.camRespawn, 1000)
					
					SHAKE_CAM(sToiletRespawnData.camRespawn, "HAND_SHAKE", 0.25)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					sToiletRespawnData.iToiletSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(sToiletRespawnData.vSyncSceneLocation, sToiletRespawnData.vSyncSceneRotation)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sToiletRespawnData.iToiletSyncSceneID, sToiletRespawnData.sRespawnAnimDict, sToiletRespawnData.sRespawnAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT | RBF_IMPACT_OBJECT)
					
					NETWORK_START_SYNCHRONISED_SCENE(sToiletRespawnData.iToiletSyncSceneID)
					
					#IF FEATURE_CASINO_HEIST
					IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
						IF IS_VALID_INTERIOR(iInteriorID)
						AND IS_INTERIOR_READY(iInteriorID)
							FORCE_ROOM_FOR_ENTITY(PLAYER_PED_ID(), iInteriorID, -584843194)
							FORCE_ROOM_FOR_GAME_VIEWPORT(iInteriorID, -584843194)
						ENDIF
					ENDIF
					#ENDIF
					
					SET_SPAWN_ACTIVITY_AS_READY(SPAWN_ACTIVITY_TOILET_SPEW)
					
					CLEAR_SPAWN_ACTIVITY_FLAGS()
					
					SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
					
					IF IS_SCREEN_FADED_OUT()
						SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_FADE_IN)
					ELSE
						SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_START)
					ENDIF
				ELSE
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Waiting for audio bank")
				ENDIF
			ELSE
				IF NOT bInteriorIsReady
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Interior is not ready")
				ENDIF
				
				IF NOT HAS_ANIM_DICT_LOADED(sToiletRespawnData.sRespawnAnimDict)
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Waiting for anim dict")
				ENDIF
			ENDIF
		BREAK
		
		CASE TOILET_RESPAWN_SYNC_START
			iLocalRespawnScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sToiletRespawnData.iToiletSyncSceneID)
			
			IF iLocalRespawnScene != -1
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				OR NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR)
					SET_SYNCHRONIZED_SCENE_RATE(iLocalRespawnScene, 1.0)
					
					SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_RUN)
				ELSE
					SET_SYNCHRONIZED_SCENE_RATE(iLocalRespawnScene, 0.0)
					
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Waiting for player control")
				ENDIF
			ELSE
				PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Sync scene killed, cleaning up - 1")
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
				ENDIF
				
				SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_CLEANUP)
			ENDIF
		BREAK
		
		CASE TOILET_RESPAWN_SYNC_FADE_IN
			iLocalRespawnScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sToiletRespawnData.iToiletSyncSceneID)
			
			IF iLocalRespawnScene != -1
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalRespawnScene) > 0.02
				AND IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
				ELSE
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Waiting for sync scene phase > 0.02")
				ENDIF
			ELSE
				PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Sync scene killed, cleaning up - 2")
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
				ENDIF
				
				SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_CLEANUP)
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_RUN)
			ELSE
				PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Waiting for screen fade in")
			ENDIF
		BREAK
		
		CASE TOILET_RESPAWN_SYNC_RUN
			iLocalRespawnScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sToiletRespawnData.iToiletSyncSceneID)
			
			IF iLocalRespawnScene != -1
				IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalRespawnScene) > 0.99
					NETWORK_STOP_SYNCHRONISED_SCENE(sToiletRespawnData.iToiletSyncSceneID)
					
					SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_CLEANUP)
				ELSE
					PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Waiting for sync scene phase > 0.99")
				ENDIF
			ELSE
				PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Sync scene killed, cleaning up - 3")
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
				ENDIF
				
				SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_CLEANUP)
			ENDIF
		BREAK
		
		CASE TOILET_RESPAWN_SYNC_CLEANUP
			REMOVE_ANIM_DICT(sToiletRespawnData.sRespawnAnimDict)
			
			PRINTLN("[TOILET_RESPAWN] - RUN_TOILET_RESPAWN - Remove anim dict")
			
			CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_RUNNING_SPAWN_ACTIVITY)
			
			IF IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			
			STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
			DESTROY_CAM(sToiletRespawnData.camRespawn)
			
			SET_TOILET_RESPAWN_SYNC_STATE(sToiletRespawnData, TOILET_RESPAWN_SYNC_INIT)
			
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_TOILET_RESPAWN_LOCATION(TOILET_RESPAWN_DATA &sToiletRespawnData)
	FLOAT fWidth = 1.0
	
	INT iNumberOfToilets = 4
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		iNumberOfToilets = 2
	#IF FEATURE_CASINO_HEIST
	ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		iNumberOfToilets = 1
	#ENDIF
	#IF FEATURE_CASINO_NIGHTCLUB
	ELIF IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
		iNumberOfToilets = 3
	#ENDIF
	ENDIF
	
	INT i
	REPEAT iNumberOfToilets i
		BOOL bToiletBlocked = FALSE
		
		INT iParticipant
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				
				IF PlayerID <> PLAYER_ID()
					IF IS_NET_PLAYER_OK(PlayerID)
						VECTOR vToiletPosition1
						VECTOR vToiletPosition2
						
						IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
							SWITCH i
								CASE 0
									fWidth = 3.25
									vToiletPosition1 = <<983.755066,74.338402,115.164139>>
									vToiletPosition2 = <<984.882019,76.126808,118.164139>>
								BREAK
								
								CASE 1
									fWidth = 3.25
									vToiletPosition1 = <<980.898560,76.768150,115.164139>>
									vToiletPosition2 = <<982.414917,75.806702,118.164139>>
								BREAK
							ENDSWITCH
							
							IF i = 0
							AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != PLAYER_ID()
								PRINTLN("[TOILET_RESPAWN] - GET_TOILET_RESPAWN_LOCATION - Cannot use owner's toilet")
								
								bToiletBlocked = TRUE
							ENDIF
							
							IF i = 1
							AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
							OR NOT HAS_PLAYER_PURCHASED_CASINO_APARTMENT_BEDROOM(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
								PRINTLN("[TOILET_RESPAWN] - GET_TOILET_RESPAWN_LOCATION - Cannot use guests's toilet")
								
								bToiletBlocked = TRUE
							ENDIF
							
							IF NOT bToiletBlocked
							AND IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, fWidth)
								PRINTLN("[TOILET_RESPAWN] - GET_TOILET_RESPAWN_LOCATION - toilet is not safe to use: ", i)
								
								bToiletBlocked = TRUE
								
								iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
							ENDIF
						#IF FEATURE_CASINO_HEIST
						ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
							fWidth = 3.5
							vToiletPosition1 = <<2725.311523,-377.143494,-48.400040>>
							vToiletPosition2 = <<2722.700928,-377.159271,-46.132896>>
							
							IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, fWidth)
								PRINTLN("[TOILET_RESPAWN] - GET_TOILET_RESPAWN_LOCATION - toilet is not safe to use: ", i)
								
								bToiletBlocked = TRUE
								
								iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
							ENDIF
						#ENDIF
						#IF FEATURE_CASINO_NIGHTCLUB
						ELIF IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
							SWITCH i
								CASE 0
									fWidth = 2.5
									vToiletPosition1 = <<1545.173706,249.820999,-50.005981>>
									vToiletPosition2 = <<1545.189453,248.166458,-48.005981>>
								BREAK
								
								CASE 1
									fWidth = 2.5
									vToiletPosition1 = <<1545.198975,248.033646,-50.005981>>
									vToiletPosition2 = <<1545.206665,246.636322,-48.005981>>
								BREAK
								
								CASE 2
									fWidth = 2.5
									vToiletPosition1 = <<1545.197021,246.319489,-50.005981>>
									vToiletPosition2 = <<1545.199585,244.838928,-48.005981>>
								BREAK
							ENDSWITCH
							
							IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, fWidth)
								PRINTLN("[TOILET_RESPAWN] - GET_TOILET_RESPAWN_LOCATION - toilet is not safe to use: ", i)
								
								bToiletBlocked = TRUE
								
								iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
							ENDIF
						#ENDIF
						ELSE
							SWITCH i
								CASE 0
									fWidth = 2.5
									vToiletPosition1 = <<1160.113037,252.248596,-52.040874>>
									vToiletPosition2 = <<1161.580444,252.259689,-50.040874>>
								BREAK
								
								CASE 1
									fWidth = 2.5
									vToiletPosition1 = <<1161.797974,252.286957,-52.040874>>
									vToiletPosition2 = <<1163.295166,252.316650,-50.040874>>
								BREAK
								
								CASE 2
									fWidth = 2.5
									vToiletPosition1 = <<1134.399902,279.530273,-52.040764>>
									vToiletPosition2 = <<1134.372925,278.045593,-50.040764>>
								BREAK
								
								CASE 3
									fWidth = 2.5
									vToiletPosition1 = <<1134.404785,281.266846,-52.029781>>
									vToiletPosition2 = <<1134.403564,279.770203,-50.040764>>
								BREAK
							ENDSWITCH
							
							IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, fWidth)
								PRINTLN("[TOILET_RESPAWN] - GET_TOILET_RESPAWN_LOCATION - toilet is not safe to use: ", i)
								
								bToiletBlocked = TRUE
								
								iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF NOT bToiletBlocked
			IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
				SWITCH i
					CASE 0
						sToiletRespawnData.vSyncSceneLocation = <<982.324, 77.323, 115.4947>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, -30.0>>
						sToiletRespawnData.vCamLocation = <<980.8499, 75.2226, 117.6329>>
						sToiletRespawnData.vCamRotation = <<-39.2270, -0.0000, -36.4705>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
					
					CASE 1
						sToiletRespawnData.vSyncSceneLocation = <<983.275, 75.836, 115.495>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, 60.0>>
						sToiletRespawnData.vCamLocation = <<985.5729, 74.7632, 117.2511>>
						sToiletRespawnData.vCamRotation = <<-27.7893, -0.0000, 63.9288>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
				ENDSWITCH
			#IF FEATURE_CASINO_HEIST
			ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				sToiletRespawnData.vSyncSceneLocation = <<2724.337, -378.419, -48.0696>>
				sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, 180.0>>
				sToiletRespawnData.vCamLocation = <<2723.1802, -375.5263, -45.9867>>
				sToiletRespawnData.vCamRotation = <<-34.1707, -0.0000, -147.3863>>
				sToiletRespawnData.fCamFOV = 50.0
			#ENDIF
			#IF FEATURE_CASINO_NIGHTCLUB
			ELIF IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
				SWITCH i
					CASE 0
						sToiletRespawnData.vSyncSceneLocation = <<1546.002, 249.162, -49.6755>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, -90.0>>
						sToiletRespawnData.vCamLocation = <<1541.9462, 249.2934, -47.5505>>
						sToiletRespawnData.vCamRotation = <<-20.9071, -0.0000, -98.7872>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
					
					CASE 1
						sToiletRespawnData.vSyncSceneLocation = <<1546.002, 247.300, -49.6755>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, -90.0>>
						sToiletRespawnData.vCamLocation = <<1541.9427, 247.7034, -47.5505>>
						sToiletRespawnData.vCamRotation = <<-20.9071, -0.0000, -98.7872>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
					
					CASE 2
						sToiletRespawnData.vSyncSceneLocation = <<1546.002, 245.675, -49.6755>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, -90.0>>
						sToiletRespawnData.vCamLocation = <<1541.9427, 245.8471, -47.5505>>
						sToiletRespawnData.vCamRotation = <<-20.9071, -0.0000, -98.7872>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
				ENDSWITCH
			#ENDIF
			ELSE
				SWITCH i
					CASE 0
						sToiletRespawnData.vSyncSceneLocation = <<1160.771, 252.903, -51.7104>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, 0.0>>
						sToiletRespawnData.vCamLocation = <<1160.0817, 249.2722, -49.6032>>
						sToiletRespawnData.vCamRotation = <<-24.6349, -0.0000, -13.2144>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
					
					CASE 1
						sToiletRespawnData.vSyncSceneLocation = <<1162.479, 252.903, -51.7104>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, 0.0>>
						sToiletRespawnData.vCamLocation = <<1161.7897, 249.2722, -49.6032>>
						sToiletRespawnData.vCamRotation = <<-24.6349, -0.0000, -13.2144>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
					
					CASE 2
						sToiletRespawnData.vSyncSceneLocation = <<1134.953, 278.703, -51.7104>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, -90.0>>
						sToiletRespawnData.vCamLocation = <<1131.2622, 279.4225, -49.2400>>
						sToiletRespawnData.vCamRotation = <<-32.8339, 0.0000, -104.4076>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
					
					CASE 3
						sToiletRespawnData.vSyncSceneLocation = <<1134.953, 280.453, -51.7104>>
						sToiletRespawnData.vSyncSceneRotation = <<0.0, 0.0, -90.0>>
						sToiletRespawnData.vCamLocation = <<1131.2622, 281.1725, -49.2400>>
						sToiletRespawnData.vCamRotation = <<-32.8339, 0.0000, -104.4076>>
						sToiletRespawnData.fCamFOV = 50.0
					BREAK
				ENDSWITCH
			ENDIF
			
			IF IS_PED_FEMALE(PLAYER_PED_ID())
				sToiletRespawnData.sRespawnAnimDict = "anim@amb@luxury_suite@respawn@female@"
			ELSE
				sToiletRespawnData.sRespawnAnimDict = "anim@amb@clubhouse@respawn@male@"
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC HIDE_EVERYTHING_FOR_TOILET_RESPAWN()
	INVALIDATE_IDLE_CAM()
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	DISPLAY_AMMO_THIS_FRAME(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
	DISABLE_SELECTOR_THIS_FRAME()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
ENDPROC

PROC MAINTAIN_TOILET_RESPAWN(TOILET_RESPAWN_PLAYER_DATA &sPlayerData, TOILET_RESPAWN_DATA &sToiletRespawnData, INTERIOR_INSTANCE_INDEX iInteriorID)
	SWITCH sToiletRespawnData.eToiletRespawnState
		CASE TOILET_RESPAWN_STATE_INIT
			SET_TOILET_RESPAWN_STATE(sToiletRespawnData, TOILET_RESPAWN_STATE_WAIT)
		BREAK
		
		CASE TOILET_RESPAWN_STATE_WAIT
			IF HAS_SPAWN_ACTIVITY_BEEN_REQUESTED(SPAWN_ACTIVITY_TOILET_SPEW)
			AND (IS_SKYSWOOP_AT_GROUND()
			OR IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_SPAWNING_IN_SIMPLE_INTERIOR))
				IF GET_TOILET_RESPAWN_LOCATION(sToiletRespawnData)
					SET_SPAWN_ACTIVITY_AS_READY(SPAWN_ACTIVITY_TOILET_SPEW)
					
					IF IS_SKYSWOOP_AT_GROUND()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES)
					ENDIF
					
					SET_BIT(sPlayerData.iRespawnBS, TOILET_RESPAWN_BS_PLAYER_REQUEST_AUDIO_BANK)
					
					SET_TOILET_RESPAWN_STATE(sToiletRespawnData, TOILET_RESPAWN_STATE_RUN)
				ELSE
					g_SpawnData.bDrunkRespawnSafe = FALSE
					
					IF NOT g_SpawnData.bPassedOutDrunk
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
					
					SET_TOILET_RESPAWN_STATE(sToiletRespawnData, TOILET_RESPAWN_STATE_WAIT)
				ENDIF
			ENDIF
		BREAK
		
		CASE TOILET_RESPAWN_STATE_RUN
			HIDE_EVERYTHING_FOR_TOILET_RESPAWN()
			
			IF (IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			OR IS_PLAYER_IN_CORONA())
			AND IS_SCREEN_FADED_OUT()
				CLEANUP_TOILET_RESPAWN(sToiletRespawnData)
				
				EXIT
			ENDIF
			
			IF RUN_TOILET_RESPAWN(sToiletRespawnData, iInteriorID)
				SET_TOILET_RESPAWN_STATE(sToiletRespawnData, TOILET_RESPAWN_STATE_END)
			ENDIF
		BREAK
		
		CASE TOILET_RESPAWN_STATE_END
			SET_BIT(sPlayerData.iRespawnBS, TOILET_RESPAWN_BS_PLAYER_FINISHED)
			
			SET_TOILET_RESPAWN_STATE(sToiletRespawnData, TOILET_RESPAWN_STATE_WAIT)
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_TOILET_RESPAWN_SERVER_AUDIO(TOILET_RESPAWN_SERVER_DATA &sServerData, TOILET_RESPAWN_PLAYER_DATA &sPlayerData, TOILET_RESPAWN_DATA &sToiletRespawnData)
	PLAYER_INDEX pPlayer = INT_TO_PLAYERINDEX(sToiletRespawnData.iPlayerStagger)
	
	IF pPlayer != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(pPlayer, FALSE)
		IF IS_BIT_SET(sPlayerData.iRespawnBS, TOILET_RESPAWN_BS_PLAYER_REQUEST_AUDIO_BANK)
			IF NOT IS_BIT_SET(sServerData.iRespawnBS, TOILET_RESPAWN_BS_SERVER_LOAD_AUDIO_BANK)
				SET_BIT(sServerData.iRespawnBS, TOILET_RESPAWN_BS_SERVER_LOAD_AUDIO_BANK)
			ENDIF
			
			CLEAR_BIT(sPlayerData.iRespawnBS, TOILET_RESPAWN_BS_PLAYER_REQUEST_AUDIO_BANK)
		ENDIF
		
		IF IS_BIT_SET(sPlayerData.iRespawnBS, TOILET_RESPAWN_BS_PLAYER_FINISHED)
			IF IS_BIT_SET(sServerData.iRespawnBS, TOILET_RESPAWN_BS_SERVER_LOAD_AUDIO_BANK)
				CLEAR_BIT(sServerData.iRespawnBS, TOILET_RESPAWN_BS_SERVER_LOAD_AUDIO_BANK)
			ENDIF
			
			CLEAR_BIT(sPlayerData.iRespawnBS, TOILET_RESPAWN_BS_PLAYER_FINISHED)
		ENDIF
	ENDIF
	
	sToiletRespawnData.iPlayerStagger++
	
	IF sToiletRespawnData.iPlayerStagger >= NETWORK_GET_MAX_NUM_PARTICIPANTS()
		sToiletRespawnData.iPlayerStagger = 0
	ENDIF
ENDPROC
#ENDIF
