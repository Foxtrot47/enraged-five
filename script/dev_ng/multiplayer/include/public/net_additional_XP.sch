USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_network.sch"
USING "net_prints.sch"
USING "NET_SCORING_Common.sch"
USING "net_mission.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//      MISSION NAME    :   net_additional_XP.sch
//      CREATED         :   Keith/Rowan
//      DESCRIPTION     :   Contains all Additional XP generation methods.
//		NOTES			:	buddies in vehicle XP.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************





// ===========================================================================================================
//      CONSTS (but tweakable as variables using widgets in debug builds)
// ===========================================================================================================

//MADE THE BELOW CONSTS GLOBALS
//// Buddies in car XP - initial parameters: Every 10sec at least 4 points are given (have to have at least 1 buddy) With each additional buddy there is an additional point meaning the increment will be 25% more than the minium.																			OLD VALUES = 1pt per buddy in car each 10 second update - minimum 100xp given if 18 points (1 min with 3 buddies, 3 mins with 1 buddy)
//TWEAK_INT	BUDDY_IN_CAR_TIMEOUT_msec				10000		// Number of milliseconds between updates
//TWEAK_INT	BUDDY_IN_CAR_TARGET_POINTS				240			// A target value to be reached so that 100xp can be given to the player
//TWEAK_INT	BUDDY_IN_CAR_DEFAULT_POINTS				3			// The minimum increment that is always given - (Setup like this so that each additional buddy will speed up the XP award by 25%)
//TWEAK_INT	BUDDY_IN_CAR_POINTS_PER_BUDDY			1			// The increment to the value for each buddy in the vehicle each update - (We will always have at least 1 buddy, so minimum increment will really be 4)
//TWEAK_INT	BUDDY_IN_CAR_BONUS_XP					100			// The amount of XP to give when the xp target is reached

//ADDITIONAL XP TYPES
CONST_INT CRUISE_WITH_BUDDIES 	0
CONST_INT DRIVING_BOSS			1
CONST_INT RIDING_WITH_BOSS		2


// ===========================================================================================================
//      Debug Routines
// ===========================================================================================================

// PURPOSE:	Create any widgets.
// NOTES:	Routines will become better separated in future, but for now this will suffice
#IF IS_DEBUG_BUILD
PROC Create_Additional_XP_Widgets()

	START_WIDGET_GROUP("KGM: TEAM MEMBERS IN CAR BONUS XP")
		ADD_WIDGET_INT_READ_ONLY("Team Members In Car: ", g_WIDGET_NumberOfBuddiesInCar)
		ADD_WIDGET_INT_READ_ONLY("Points Added Last Update: ", g_WIDGET_BuddiesInCarPointsLastUpdate)
		ADD_WIDGET_INT_SLIDER("Current Points: ", MPGlobalsAmbience.g_currentBuddyInCarPts, 0, 1000, 10)
		
		ADD_WIDGET_INT_SLIDER("Target Points For XP: ", FM_BUDDY_IN_CAR_TARGET_POINTS, 0, 1000, 10)
		ADD_WIDGET_INT_SLIDER("Increase for 1 Passenger: ", FM_BUDDY_IN_CAR_BONUS_XP_1_BUD, 0, 1000, 1)
		ADD_WIDGET_INT_SLIDER("Increase for 2 Passenger: ", FM_BUDDY_IN_CAR_BONUS_XP_2_BUD, 0, 1000, 1)
		ADD_WIDGET_INT_SLIDER("Increase for 3 Passenger: ", FM_BUDDY_IN_CAR_BONUS_XP_3_BUD, 0, 1000, 1)
		ADD_WIDGET_INT_SLIDER("Update Interval (msecs): ", FM_BUDDY_IN_CAR_TIMEOUT_msec, 0, 60000, 1000)
		//ADD_WIDGET_INT_SLIDER("Bonus XP: ", FM_BUDDY_IN_CAR_BONUS_XP, 0, 1000, 10)
	STOP_WIDGET_GROUP()

ENDPROC
#ENDIF




// ===========================================================================================================
//      Initialisation Routines
// ===========================================================================================================

// PURPOSE:	Set the global variables to a good initial value.
PROC Initialise_Additional_XP()

	TIME_DATATYPE networkTimer = GET_NETWORK_TIME()

	// Initial Values for Buddy In Car
	MPGlobalsAmbience.g_nextBuddyInCarTimeout		= GET_TIME_OFFSET(networkTimer , FM_BUDDY_IN_CAR_TIMEOUT_msec)
	MPGlobalsAmbience.g_currentBuddyInCarPts		= 0
	
	MPGlobalsAmbience.g_nextGoonBenefitVehRPTimeout = GET_TIME_OFFSET(networkTimer, g_sMPTunables.itime_interval_between_driving_boss_checks)

ENDPROC





// ===========================================================================================================
//      The Buddies In Car XP Routines
// ===========================================================================================================

// PURPOSE:	Checks if the occupant of the car seat is on the same team as the player
//
// INPUT PARAMS:		paramVehicle			The vehicle index
//						paramSeatID				Vehicle Seat ID
//						paramTeamID				Player's Team ID as an Int
// RETURN VALUE:		BOOL					Returns TRUE if the seat is occupied by a member of the players team but not the player, otherwise FALSE
//
// NOTES:	Assumes the vehicle seat is occupied
FUNC BOOL Is_Vehicle_Seat_Occupant_On_Players_Team(VEHICLE_INDEX paramVehicle, VEHICLE_SEAT paramSeatID, INT paramTeamID)

	// Ensure the seat is occupied - debug only because it is assumed this check has been done prior to calling the function
	#IF IS_DEBUG_BUILD
		IF (IS_VEHICLE_SEAT_FREE(paramVehicle, paramSeatID))
			SCRIPT_ASSERT("Is_Vehicle_Seat_Occupant_On_Players_Team() - The vehicle seat is free when it should be occupied. Tell Keith.")
			
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Return FALSE if the seat is occupied by the player
	PED_INDEX theOccupant = GET_PED_IN_VEHICLE_SEAT(paramVehicle, paramSeatID)
	IF (theOccupant = PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	// Return FALSE if the occupant is not a player
	IF NOT (IS_PED_A_PLAYER(theOccupant))
		RETURN FALSE
	ENDIF
	
	// Return FALSE if the occupant is not on the player's team
	PLAYER_INDEX occupantAsPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(theOccupant)
	IF NOT (GET_PLAYER_TEAM(occupantAsPlayer) = paramTeamID)
		RETURN FALSE
	ENDIF
	
	//Return FALSE if the occupant is no okay
	IF NOT IS_NET_PLAYER_OK(occupantAsPlayer)
		RETURN FALSE
	ENDIF
	
	// Found a team buddy
	RETURN TRUE

ENDFUNC
FUNC BOOL IS_PED_IN_ANY_PERSONAL_VEHICLE(PLAYER_INDEX playerid)
	
	VEHICLE_INDEX tempveh
	
	IF IS_NET_PLAYER_OK(playerid)
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerid))
			tempveh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerid))
			IF IS_VEHICLE_DRIVEABLE(tempveh)
				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
					IF DECOR_EXIST_ON(tempveh, "Player_Vehicle")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Writes to the Feed
PROC DO_ADDITIONAL_XP_TICKER(INT iAdditionalXpType, INT xpToGive)
	SWITCH iAdditionalXpType
		CASE CRUISE_WITH_BUDDIES
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
			OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
				PRINT_TICKER_WITH_INT("TXP_FBUDG", xpToGive) 		//Flying with Buddies XP
			ELSE
				PRINT_TICKER_WITH_INT("TXP_CBUDG", xpToGive) 		//Cruising with Buddies XP
			ENDIF
		BREAK
		CASE DRIVING_BOSS
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PRINT_TICKER_WITH_INT("GB_XP_DRIVEB", xpToGive) 		//Driving the Boss
			ELSE
				IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					PRINT_TICKER_WITH_INT("GB_XP_DRIVEC", xpToGive) 		//Driving the Boss
				ELSE
					PRINT_TICKER_WITH_INT("GB_XP_DRIVE", xpToGive) 		//Driving the Boss
				ENDIF
			ENDIF
		BREAK
		CASE RIDING_WITH_BOSS
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
				PRINT_TICKER_WITH_INT("GB_XP_RIDEB", xpToGive) 		//Riding with the Boss
			ELSE
				IF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
					PRINT_TICKER_WITH_INT("GB_XP_RIDEC", xpToGive) 		//Riding with the Boss
				ELSE
					PRINT_TICKER_WITH_INT("GB_XP_RIDE", xpToGive) 		//Riding with the Boss
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Return the increase based on how many buddies there are
FUNC INT GET_FM_BUDDY_INCREASE(INT numTeamBuddies)
	IF numTeamBuddies = 1
		RETURN ROUND(FM_BUDDY_IN_CAR_BONUS_XP_1_BUD*g_sMPTunables.fxp_tunable_Buddy_in_car_respect_1_buddy)
	ELIF numTeamBuddies = 2
		RETURN ROUND(FM_BUDDY_IN_CAR_BONUS_XP_2_BUD*g_sMPTunables.fxp_tunable_Buddy_in_car_respect_2_buddies)
	ELIF numTeamBuddies >= 3
		RETURN ROUND(FM_BUDDY_IN_CAR_BONUS_XP_3_BUD*g_sMPTunables.fxp_tunable_Buddy_in_car_respect_3_buddies)
	ENDIF
	
	RETURN 0
ENDFUNC		

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Returns the Bonus XP for hanging out with buddies
//
// RETURN VALUE:		INT				The amount of XP to add
//
// NOTES:	When the value is returned the variables get cleared.
//			The value tallies up while both off and on mission, but is currently only given at the end of the mission.
//			This function also adds on the XP.
FUNC INT Get_BonusXP_For_Buddies_In_Car()
	
	// METHOD:	There is a target points value (BUDDY_IN_CAR_TARGET_POINTS) for receiving a specified XP (BUDDY_IN_CAR_BONUS_XP) for this.
	//			The actual XP given is calculated based on the amount of points gained.
	//			Less than the target points will give 0 XP. Target Points will give the specified XP. Greater than the target points will give greater than the specified XP.
	//			Calculation is therefore: xp_given = points_gained/target_points * specified_xp BUT ONLY IF Target Points Value has been reached.
	
	// Locally record the current points
	INT currentBuddiesPoints = MPGlobalsAmbience.g_currentBuddyInCarPts
	
	// Clear the stored points - DO THIS AT THE END INSTEAD
	//MPGlobalsAmbience.g_currentBuddyInCarPts = 0
	
	// Output to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("-----------------------------------------------------------") NET_NL()
		NET_PRINT("...KGM MP: Get_BonusXP_For_Buddies_In_Car() being requested") NET_NL()
		NET_PRINT("...........Current Buddies Points / Target Buddies Points: ") NET_PRINT_INT(currentBuddiesPoints)
		NET_PRINT(" / ") NET_PRINT_INT(FM_BUDDY_IN_CAR_TARGET_POINTS) NET_NL()
		NET_PRINT("...........XP given if Target Points Reached: ") NET_PRINT_INT(g_sMPTunables.fxp_tunable_Buddy_in_car_modifier) NET_NL()
	#ENDIF

	// Return 0 if the points target for giving XP hasn't been reached.
	// Return minimum of points equals points target.
	// Return more XP if points is greater than points target.
	INT xpToGive = 0
	
	// Has target has been equalled?
	IF (currentBuddiesPoints >= FM_BUDDY_IN_CAR_TARGET_POINTS)
		xpToGive = g_sMPTunables.fxp_tunable_Buddy_in_car_modifier
	ENDIF
	
	IF MPGlobalsAmbience.g_currentRidingWithBossPts >= ((g_sMPTunables.igb_rp_riding_with_boss_frequency/1000) / MPGlobalsAmbience.g_currentGangBuddiesInCar)
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === MPGlobalsAmbience.g_currentRidingWithBossPts (", MPGlobalsAmbience.g_currentRidingWithBossPts, ") > (g_sMPTunables.iridingwithbosstotalpoints/1000 (", g_sMPTunables.iridingwithbosstotalpoints/1000, ") / MPGlobalsAmbience.g_currentGangBuddiesInCar) (", MPGlobalsAmbience.g_currentGangBuddiesInCar, ")")
		INT gbXpToGive
		FLOAT fTempXp
		
		fTempXp = TO_FLOAT(g_sMPTunables.igb_rp_riding_with_boss_base_amount) * g_sMPTunables.fgb_rp_riding_with_boss_base_amount_multiplier
		gbXpToGive = ROUND(fTempXp)
		
		DO_ADDITIONAL_XP_TICKER(RIDING_WITH_BOSS, ROUND(gbXpToGive*g_sMPTunables.xpMultiplier))
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === gbXpToGive = g_sMPTunables.igb_rp_riding_with_boss_base_amount (", g_sMPTunables.igb_rp_riding_with_boss_base_amount ," * g_sMPTunables.fgb_rp_riding_with_boss_base_amount_multiplier (", g_sMPTunables.fgb_rp_riding_with_boss_base_amount_multiplier, ")")
		
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === gbXpToGive = ", gbXpToGive)
		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_BUDDY", XPTYPE_VEHICLE, XPCATEGORY_VEHICLE_CRUISE_WITH_BUDDY, gbXpToGive)
		MPGlobalsAmbience.g_currentRidingWithBossPts = 0
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Bonus XP Given for Riding with Boss: ") NET_PRINT_INT(gbXpToGive) NET_NL()
		#ENDIF
	ENDIF
	
	IF MPGlobalsAmbience.g_currentDrivingForBossPts >= ((g_sMPTunables.igb_rp_driving_the_boss_frequency/1000) / MPGlobalsAmbience.g_currentGangBuddiesInCar)
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === MPGlobalsAmbience.g_currentDrivingForBossPts (", MPGlobalsAmbience.g_currentDrivingForBossPts, ") > (g_sMPTunables.igb_rp_driving_the_boss_frequency/1000 (", g_sMPTunables.igb_rp_driving_the_boss_frequency/1000, ") / MPGlobalsAmbience.g_currentGangBuddiesInCar) (", MPGlobalsAmbience.g_currentGangBuddiesInCar, ")")
		INT gbXpToGive
		FLOAT fTempXp
		
		fTempXp = TO_FLOAT(g_sMPTunables.igb_rp_riding_with_boss_base_amount) * g_sMPTunables.fgb_rp_driving_the_boss_base_amount_multiplier
		gbXpToGive = ROUND(fTempXp)
		
		DO_ADDITIONAL_XP_TICKER(DRIVING_BOSS, ROUND(gbXpToGive*g_sMPTunables.xpMultiplier))
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === gbXpToGive = g_sMPTunables.igb_rp_riding_with_boss_base_amount (", g_sMPTunables.igb_rp_riding_with_boss_base_amount ," * g_sMPTunables.fgb_rp_driving_the_boss_base_amount_multiplier (", g_sMPTunables.fgb_rp_driving_the_boss_base_amount_multiplier, ")")
		
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === gbXpToGive = ", gbXpToGive)
		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_BUDDY", XPTYPE_VEHICLE, XPCATEGORY_VEHICLE_CRUISE_WITH_BUDDY, gbXpToGive)
		MPGlobalsAmbience.g_currentDrivingForBossPts = 0
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Bonus XP Given for Driving the Boss: ") NET_PRINT_INT(gbXpToGive) NET_NL()
		#ENDIF
	ENDIF
	
	
	// If Target has been exceeded, calculate the return XP.
	/*IF (currentBuddiesPoints > FM_BUDDY_IN_CAR_TARGET_POINTS)
		xpToGive = currentBuddiesPoints * g_sMPTunables.fxp_tunable_Buddy_in_car_modifier / FM_BUDDY_IN_CAR_TARGET_POINTS
	ENDIF*/
	
	// Add on the XP (the calling routine only requires the value for display purposes)
	IF NOT (xpToGive = 0)
		/*FLOAT fTempXp = (TO_FLOAT(xpToGive)/FM_BUDDY_IN_CAR_XP_INCREMENTS)
		FLOOR(fTempXp)
		xpToGive = ROUND(fTempXp*FM_BUDDY_IN_CAR_XP_INCREMENTS)*/
		
		DO_ADDITIONAL_XP_TICKER(CRUISE_WITH_BUDDIES, ROUND(xpToGive*g_sMPTunables.xpMultiplier))
		GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_BUDDY", XPTYPE_VEHICLE, XPCATEGORY_VEHICLE_CRUISE_WITH_BUDDY, xpToGive)
		MPGlobalsAmbience.g_currentBuddyInCarPts = 0
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Bonus XP Given for Cruising with Buddies: ") NET_PRINT_INT(xpToGive) NET_NL()
		#ENDIF
	ENDIF

	RETURN xpToGive
ENDFUNC

// PURPOSE:	Awards XP when Buddies are hanging out together in a car
PROC Maintain_Buddies_In_Car_XP()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		// Has the timer elapsed?
		TIME_DATATYPE networkTimer = GET_NETWORK_TIME()
		FLOAT fupgrademulitplier = 1
		
		//Check Timer is Up
		IF IS_TIME_LESS_THAN(networkTimer , MPGlobalsAmbience.g_nextBuddyInCarTimeout)
			EXIT
		ENDIF
		
		// Update Widgets before performing checks
		#IF IS_DEBUG_BUILD
			g_WIDGET_NumberOfBuddiesInCar			= 0
			g_WIDGET_BuddiesInCarPointsLastUpdate	= 0
		#ENDIF
		
		//Check player hasn't stayed still
		IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), MPGlobalsAmbience.vLastBuddyInCarUpdateLocation, FALSE)  < FM_BUDDY_IN_CAR_UPDATE_DISTANCE)
			//NET_PRINT(" - Maintain_Buddies_In_Car_XP - DISTANCE CHECK FAIL ") NET_NL()
			EXIT
		//ELSE
		//	NET_PRINT(" - Maintain_Buddies_In_Car_XP - DISTANCE CHECK PASSED ") NET_NL()
		ENDIF
		
		// Timer has elapsed so update the buddies in car XP checks
		// Restart the timer for next update
		MPGlobalsAmbience.g_nextBuddyInCarTimeout = GET_TIME_OFFSET(networkTimer , FM_BUDDY_IN_CAR_TIMEOUT_msec)
		MPGlobalsAmbience.g_currentGangBuddiesInCar = 0
			
		// Make sure the player is in a vehicle
		IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
			EXIT
		ENDIF
		
		// Get the vehicle index for the player's vehicle
		VEHICLE_INDEX theVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		// Ensure the vehicle is driveable
		IF NOT (IS_VEHICLE_DRIVEABLE(theVehicle))
			EXIT
		ENDIF
		
		IF IS_PED_IN_ANY_PERSONAL_VEHICLE(PLAYER_ID())
			NET_PRINT("player in a personal vehicle ") NET_NL()
			fupgrademulitplier = 1+(2*(GET_VEHICLE_UPGRADE_LEVEL(theVehicle)/GET_MAX_VEHICLE_UPGRADE_LEVEL(theVehicle)))
			NET_PRINT("upgrade multiplier:  ") NET_PRINT_FLOAT(fupgrademulitplier) NET_NL()
		ELSE
			NET_PRINT("player not in a personal vehicle ") NET_NL()
		ENDIF

		// How many occupants (passengers + driver)?
		INT occupantsToCheck = GET_VEHICLE_NUMBER_OF_PASSENGERS(theVehicle)
		IF NOT (IS_VEHICLE_SEAT_FREE(theVehicle, VS_DRIVER))
			
			//Exit if the driver is not a player
			PED_INDEX theDriver = GET_PED_IN_VEHICLE_SEAT(theVehicle, VS_DRIVER)
			IF NOT IS_PED_A_PLAYER(theDriver)
				EXIT
			ENDIF
			
			IF theDriver = PLAYER_PED_ID()
				MPGlobalsAmbience.g_isBuddyDriver = TRUE
				PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === Local player is driver.")
			ELSE
				MPGlobalsAmbience.g_isBuddyDriver = FALSE
			ENDIF
			
			occupantsToCheck++
			
		ELSE
			EXIT	//Don't run if there is no driver
		ENDIF
		
		// If there is only one occupant then it must be the player
		IF (occupantsToCheck = 1)
			EXIT
		ENDIF
		
		// Count the number of team buddies in the car - excluding the player
		INT			playersTeam		= GET_PLAYER_TEAM(PLAYER_ID()) 
		INT			numTeamBuddies	= 0

		// The vehicle contains more than one occupant
		// Go through all vehicle seats until all occupants have been checked
		// NOTE: As of 2/7/11 VS_EXTRA_RIGHT_3 is the highest seat ID for the safety cutoff - this may change
		VEHICLE_SEAT	thisSeatId		= VS_DRIVER
		INT				thisSeatIdAsInt	= ENUM_TO_INT(thisSeatId)
		INT				safetyCutoff	= (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(theVehicle)+1)	//ENUM_TO_INT(VS_EXTRA_RIGHT_3) + 1
		
		WHILE (occupantsToCheck > 0)
			thisSeatId = INT_TO_ENUM(VEHICLE_SEAT, thisSeatIdAsInt)

			// Is the seat occupied?
			IF NOT (IS_VEHICLE_SEAT_FREE(theVehicle, thisSeatId))
				IF (Is_Vehicle_Seat_Occupant_On_Players_Team(theVehicle, thisSeatId, playersTeam))
					numTeamBuddies++
				ENDIF
				
				PED_INDEX pedInSeat = GET_PED_IN_VEHICLE_SEAT(theVehicle, thisSeatId)
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInSeat)
					IF piPlayer != INVALID_PLAYER_INDEX()
					AND NETWORK_IS_PLAYER_ACTIVE(piPlayer)
					AND GB_IS_PLAYER_MEMBER_OF_THIS_GANG(piPlayer, GB_GET_LOCAL_PLAYER_GANG_BOSS())
						IF NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInSeat) = GB_GET_LOCAL_PLAYER_GANG_BOSS()
							IF MPGlobalsAmbience.g_isBuddyDriver
								MPGlobalsAmbience.g_currentDrivingForBossPts += 10
								PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === Local player is driver for the boss.")
								PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === Seconds spent driving for the boss: ", MPGlobalsAmbience.g_currentDrivingForBossPts)
							ELSE
								MPGlobalsAmbience.g_currentRidingWithBossPts += 10
								PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === Local player is not driving the boss, just riding together.")
								PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === Seconds spent riding withthe boss: ", MPGlobalsAmbience.g_currentRidingWithBossPts)
							ENDIF
						ENDIF
						
						IF NETWORK_GET_PLAYER_INDEX_FROM_PED(pedInSeat) <> PLAYER_ID()
							MPGlobalsAmbience.g_currentGangBuddiesInCar++
						ENDIF
					ENDIF
				ENDIF
				
				// There is one less occupant to check
				occupantsToCheck--
			ENDIF
		
			
			// Next Seat
			thisSeatIdAsInt++
			
			IF (thisSeatIdAsInt >= safetyCutoff)
			AND (occupantsToCheck > 0)
				// Hit the safety cutoff, so ensure the conditions are set to quit the loop
				occupantsToCheck = 0
				
				SCRIPT_ASSERT("Maintain_Buddies_In_Car_XP() - Reached safety cutoff without checking all vehicle occupants - Tell Keith.")
			ENDIF
		ENDWHILE
		
		PRINTLN(DEBUG_NET_MAGNATE, "=== GOON BENEFITS === Local player is sharing a vehicle with ", MPGlobalsAmbience.g_currentGangBuddiesInCar, " of the organisation.")
		
		//Exit if we have no buddies with us
		IF numTeamBuddies <= 0
			NET_PRINT("Maintain_Buddies_In_Car_XP - EXIT (NO BUDDIES)")
			EXIT
		ENDIF
		
		//Update Distance Checks
		NET_PRINT(" - Maintain_Buddies_In_Car_XP - DISTANCE CHECK PASSED ") NET_NL()
		MPGlobalsAmbience.vLastBuddyInCarUpdateLocation = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		
		// Add on the points
		INT pointsToAdd
		pointsToAdd = GET_FM_BUDDY_INCREASE(numTeamBuddies)
		MPGlobalsAmbience.g_currentBuddyInCarPts += pointsToAdd
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP - Performing Buddies In Car XP update")
			NET_PRINT("   team buddies in vehicle: ") NET_PRINT_INT(numTeamBuddies)
			NET_PRINT("   points to add: ") NET_PRINT_INT(pointsToAdd)
			NET_PRINT("   new points total: ") NET_PRINT_INT(MPGlobalsAmbience.g_currentBuddyInCarPts) NET_PRINT("/") NET_PRINT_INT(FM_BUDDY_IN_CAR_TARGET_POINTS)
			NET_NL()
		#ENDIF
		
		// Update Widgets after performing checks
		#IF IS_DEBUG_BUILD
			g_WIDGET_NumberOfBuddiesInCar			= numTeamBuddies
			g_WIDGET_BuddiesInCarPointsLastUpdate	= pointsToAdd
		#ENDIF
		
		//Check if we should give the player XP
		GET_BONUSXP_FOR_BUDDIES_IN_CAR()
	ENDIF
ENDPROC
