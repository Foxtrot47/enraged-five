/* ------------------------------------------------------------------
* Name: net_betting_common.sch
* Author: James Adwick
* Date: 17/08/2012
* Purpose: Common functions for betting system
* ------------------------------------------------------------------*/

USING "globals.sch"
USING "net_include.sch"
USING "net_scoring_common.sch"
USING "net_common_functions.sch"
USING "FM_In_Corona_Header.sch"
USING "net_script_tunables.sch"

// ---------------- Common Accessors ------------------
FUNC INT GET_CLIENT_BETTING_STATE()
	RETURN MPGlobals.g_MPBettingData.clientBettingState
ENDFUNC

FUNC INT GET_PLAYER_BETTING_SYNC_ID(INT index)
	RETURN GlobalServerBD_Betting.playerBettingData[index].iSyncDataID
ENDFUNC

FUNC BOOL HAS_PLAYER_PLACED_BETS()
	RETURN MPGlobals.g_MPBettingData.bClientHasBet
ENDFUNC

FUNC BETTING_ID GET_PLAYER_BETTING_ID(INT index)
	RETURN GlobalServerBD_Betting.playerBettingData[index].bettingID
ENDFUNC

FUNC BETTING_ID GET_MY_BETTING_ID()
	RETURN GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(PLAYER_ID())].bettingID
ENDFUNC

FUNC BOOL IS_PLAYER_BETTING_POOL_VOID(PLAYER_INDEX playerID)
	RETURN IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(playerID)].iBSPlayerState, BS_BETTING_POOL_VOID)
ENDFUNC

PROC SET_MODE_BETTING_ON(INT iBettingMode)
	IF MPGlobals.g_MPBettingData.iModeBettingOn != iBettingMode
		MPGlobals.g_MPBettingData.iModeBettingOn = iBettingMode
	ENDIF
ENDPROC

FUNC INT GET_MODE_BETTING_ON()
	RETURN MPGlobals.g_MPBettingData.iModeBettingOn
ENDFUNC

/// PURPOSE: (SERVER) Returns the total players involved in active betting slot
FUNC INT GET_ACTIVE_BETTING_MEMBER_COUNT(INT iIndex)
	RETURN GlobalServerBD_Betting.activeBettingData[iIndex].iActiveMembers
ENDFUNC

FUNC BOOL IS_BETTING_INDEX_ACTIVE(INT iIndex)
	RETURN (GET_ACTIVE_BETTING_MEMBER_COUNT(iIndex) > 0)
ENDFUNC

/// PURPOSE: Returns the value of the betting gift if the feature is turned on 
FUNC INT GET_BETTING_GIFT_VALUE()

	BOOL bGiftAvailable = FALSE
	BOOL bApplyRoundMultiplier = FALSE
	//TUNE_CONTEXT_MP_GLOBALS
	REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_MP_GLOBALS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE)
	SWITCH g_FMMC_STRUCT.iMissionType
		CASE FMMC_TYPE_MISSION
			
			// If it is a mission, switch off the subtypes
			SWITCH g_FMMC_STRUCT.iMissionSubType
				CASE FMMC_MISSION_TYPE_LTS
					//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_MISSIONS, TUNE_CONTEXT_FM_LTS)
					IF g_sMPTunables.btoggle_on_bet_stake_gift_LTS 
						bGiftAvailable = TRUE
						bApplyRoundMultiplier = TRUE
					ENDIF
				BREAK
				
				CASE FMMC_MISSION_TYPE_CTF
					//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_MISSIONS, TUNE_CONTEXT_FM_CAPTURE)
					IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_CAPTURE 
						
						bGiftAvailable = TRUE
						bApplyRoundMultiplier = TRUE
					ENDIF
				BREAK
				
				DEFAULT
					//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_MISSIONS, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE)
					IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_MISSIONS 
						bGiftAvailable = TRUE
						bApplyRoundMultiplier = TRUE
					ENDIF
				BREAK
			ENDSWITCH			
		BREAK
		
		CASE FMMC_TYPE_DEATHMATCH
			//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_DM, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE)
			IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_DM 
				bGiftAvailable = TRUE
				IF g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_ROUNDS] > 1
					bApplyRoundMultiplier = TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_BASE_JUMP
			//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_BASEJUMP, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE)
			IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_BASEJUMP 
				bGiftAvailable = TRUE
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_SURVIVAL
			//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_SURVIVAL, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE)
			IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_SURVIAL 
				bGiftAvailable = TRUE
			ENDIF
		BREAK
		
		CASE FMMC_TYPE_RACE
			IF IS_AIR_RACE(g_FMMC_STRUCT.iRaceType)
				//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_RACES, TUNE_CONTEXT_FM_RACES_AIR)
				IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_RACES_AIR 
					bGiftAvailable = TRUE
				ENDIF
			ELIF IS_BOAT_RACE(g_FMMC_STRUCT.iRaceType)
				//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_RACES, TUNE_CONTEXT_FM_RACES_SEA)
				IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_RACES_SEA 
					bGiftAvailable = TRUE
				ENDIF
			ELIF IS_BIKE_RACE_TYPE(g_FMMC_STRUCT.iRaceType)
				//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_RACES, TUNE_CONTEXT_FM_RACES_BIKE)
				IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_RACES_CYCLE 
					bGiftAvailable = TRUE
				ENDIF
			ELSE
				//REFRESH_BETTING_TUNABLES_EARLY(TUNE_CONTEXT_FM_RACES, TUNE_CONTEXT_FM_TYPE_CONTENT_MODIFIER_NONE)
				IF g_sMPTunables.bTOGGLE_ON_BET_STAKE_GIFT_RACES_CAR 
					bGiftAvailable = TRUE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

	// If we have a gift available
	IF bGiftAvailable	
		IF bApplyRoundMultiplier
		
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
				RETURN g_sMPTunables.icash_reward_bet_stake_gift * g_sTransitionSessionOptions.iSelection[ciDM_HOST_OPTION_ROUNDS]
			ELSE
				RETURN g_sMPTunables.icash_reward_bet_stake_gift * g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_ROUNDS]
			ENDIF
			
		ELSE
			RETURN g_sMPTunables.icash_reward_bet_stake_gift
		ENDIF
	ENDIF

	RETURN 0
ENDFUNC

FUNC BOOL IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_INDEX playerId, BOOL bCheckCanBet = FALSE)

	IF NATIVE_TO_INT(playerId) != -1
	AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iFmTutProgBitset, biTrigTut_PlayerInFmActivityTutorialCorona)
		PRINTLN("[JA@BETTING] IS_BETTING_AVAILABLE_TO_PLAYER - FALSE - biTrigTut_PlayerInFmActivityTutorialCorona")
		RETURN FALSE
	ENDIF

	INT index = NATIVE_TO_INT(playerId)	

	// If we are SCTV we should check the player we are spectating settings
	IF IS_PLAYER_SCTV(PLAYER_ID())
		bCheckCanBet = FALSE
		
		index = GET_CORONA_GBD_SLOT() 
	ENDIF

	// Check the player is available to bet
	IF bCheckCanBet
	
		IF g_sMPTunables.bDisableJobBetting
			PRINTLN("[JA@BETTING] BROADCAST_PLAYER_AVAILABLE_FOR_BETTING - g_sMPTunables.bDisableJobBetting = FALSE")
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_CAN_BET(ROUND(g_sMPTunables.fMinBetLimit))
		AND GET_BETTING_GIFT_VALUE() <= 0
			PRINTLN("[JA@BETTING] BROADCAST_PLAYER_AVAILABLE_FOR_BETTING - NETWORK_CAN_BET = FALSE")
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_CAN_SPEND_MONEY(ROUND(g_sMPTunables.fMinBetLimit), FALSE, FALSE, FALSE)
		AND GET_BETTING_GIFT_VALUE() <= 0
			PRINTLN("[JA@BETTING] BROADCAST_PLAYER_AVAILABLE_FOR_BETTING - NETWORK_CAN_SPEND_MONEY = FALSE")
			RETURN FALSE
		ENDIF
	
//		IF ((NOT NETWORK_CAN_BET(ROUND(g_sMPTunables.fMinBetLimit)) OR NOT NETWORK_CAN_SPEND_MONEY(ROUND(g_sMPTunables.fMinBetLimit), FALSE, FALSE, FALSE))
//		AND GET_BETTING_GIFT_VALUE() <= 0)	
//		OR g_sMPTunables.bDisableJobBetting
//		
//			#IF IS_DEBUG_BUILD
//			IF GET_GAME_TIMER() % 4000 < 50
//				PRINTLN("[JA@BETTING] BROADCAST_PLAYER_AVAILABLE_FOR_BETTING - NETWORK_CAN_BET() = ", PICK_STRING(NETWORK_CAN_BET(ROUND(g_sMPTunables.fMinBetLimit)), "TRUE", "FALSE"), ". Player is unable to bet")
//				PRINTLN("[JA@BETTING] BROADCAST_PLAYER_AVAILABLE_FOR_BETTING - NETWORK_CAN_SPEND_MONEY() = ", PICK_STRING(NETWORK_CAN_SPEND_MONEY(ROUND(g_sMPTunables.fMinBetLimit), FALSE, FALSE, FALSE), "TRUE", "FALSE"), ". Player is unable to spend the cash")
//				PRINTLN("[JA@BETTING] BROADCAST_PLAYER_AVAILABLE_FOR_BETTING - g_sMPTunables.bDisableJobBetting = ", PICK_STRING(g_sMPTunables.bDisableJobBetting, "TRUE", "FALSE"))
//			ENDIF
//			#ENDIF
//			
//			RETURN FALSE
//		ENDIF
	ENDIF

	// If race mode is RALLY then check we have more than 1 team (clean this up if more modes are like this)
	IF GET_MODE_BETTING_ON() = ciRACE_SUB_TYPE_RALLY
		IF GlobalServerBD_Betting.playerBettingData[index].iNumberPlayersInPool <= 2
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Block betting in all future deathmatches
	// url:bugstar:7775276 - Deathmatch Update - Block betting in new DMs
	IF IS_THIS_A_DEATHMATCH()
	AND NOT IS_THIS_LEGACY_DM_CONTENT()
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_PLAYER_POOL_ACTIVE)
ENDFUNC

FUNC BOOL IS_BETTING_AVAILABLE_AND_ACCESSIBLE()
	IF GET_CORONA_STATUS() = CORONA_STATUS_IN_CORONA
		RETURN FALSE
	ELSE
		RETURN (IS_BETTING_AVAILABLE_TO_PLAYER(PLAYER_ID(), TRUE) AND (MPGlobals.g_MPBettingData.iClientAvailableCash > 0) AND (GET_CLIENT_BETTING_STATE() != MP_BETTING_WAIT_FOR_ODDS))
	ENDIF
ENDFUNC

FUNC BOOL IS_PLAYER_BETTING_POOL_LOCKED(INT index)
	RETURN IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_PLAYER_POOL_LOCKED)
ENDFUNC

FUNC BOOL ARE_FINAL_ODDS_CALCULATED(INT index)
	RETURN IS_BIT_SET(GlobalServerBD_Betting.playerBettingData[index].iBSPlayerState, BS_BETTING_FINAL_ODDS)
ENDFUNC

PROC GET_PLAYER_BETTING_ODDS(INT index, INT &iOddNum, INT &iOddDenom)

	IF index = -1
		SCRIPT_ASSERT("GET_PLAYER_BETTING_ODDS - trying to get odds of an invalid betting index, index = -1")
		EXIT
	ENDIF

	iOddNum = GlobalServerBD_Betting.activeBettingData[index].iOddNumerator
	iOddDenom = GlobalServerBD_Betting.activeBettingData[index].iOddDenominator
ENDPROC

FUNC INT GET_NUMBER_IN_MY_VOTE()
	RETURN GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iNumberInVote
ENDFUNC
// PURPOSE: Returns the actual fractional value of a player's odds
FUNC FLOAT GET_PLAYER_BETTING_ODDS_VALUE(INT index)
	INT iOddN
	INT iOddD
	GET_PLAYER_BETTING_ODDS(index, iOddN, iOddD)
	
	IF iOddD = 0
		RETURN 0.0
	ELSE
		RETURN (TO_FLOAT(iOddN) / TO_FLOAT(iOddD))
	ENDIF
ENDFUNC

/// PURPOSE: Generates a new ID for clients to sync to.
FUNC INT GET_UNIQUE_BETTING_SYNC_ID(INT iSyncID)

	INT iNewID = GET_RANDOM_INT_IN_RANGE(100, 10000000)
	
	IF iSyncID = iNewID
		iNewID += 1
	ENDIF

	RETURN iNewID
ENDFUNC

/// PURPOSE: Checks if the 2 passed betting IDs are equal
FUNC BOOL ARE_BETTING_IDS_EQUAL(BETTING_ID firstBettingID, BETTING_ID secondBettingID)

	IF 	firstBettingID.iMissionID = secondBettingID.iMissionID
	AND firstBettingID.iMissionVar = secondBettingID.iMissionVar
	AND firstBettingID.iMissionType = secondBettingID.iMissionType
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// DEBUG
#IF IS_DEBUG_BUILD

PROC PRINT_CLIENT_BETTING_STATE_STRING(INT iBettingState)
	SWITCH iBettingState
		CASE MP_BETTING_NULL			PRINTSTRING("MP_BETTING_NULL")			BREAK
		CASE MP_BETTING_READY			PRINTSTRING("MP_BETTING_READY")			BREAK
		CASE MP_BETTING_AVAILABLE		PRINTSTRING("MP_BETTING_AVAILABLE")		BREAK
		CASE MP_BETTING_PLACING_BETS	PRINTSTRING("MP_BETTING_PLACING_BETS")	BREAK
		CASE MP_BETTING_WAIT_FOR_ODDS	PRINTSTRING("MP_BETTING_WAIT_FOR_ODDS")	BREAK
		CASE MP_BETTING_LOCK_DOWN		PRINTSTRING("MP_BETTING_LOCK_DOWN")		BREAK
		DEFAULT	PRINTSTRING("UNKNOWN")	BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_BETTING_ID(BETTING_ID bettingID)
	PRINTINT(bettingID.iMissionID) PRINTSTRING(":")
	PRINTINT(bettingID.iMissionVar) PRINTSTRING(":")
	PRINTINT(bettingID.iMissionType) PRINTSTRING(":")
ENDPROC

FUNC BOOL IS_BETTING_ENABLED()
	//RETURN TRUE
	RETURN GlobalServerBD.g_bDebugBettingEnabled
ENDFUNC

FUNC BOOL IS_BETTING_ALLOWING_TIMER()
	//RETURN FALSE
	RETURN (g_bDebugDisableTimerForBetting = FALSE)
ENDFUNC

#ENDIF

// Set and get the client betting state
PROC SET_CLIENT_BETTING_STATE(INT iBettingState)
	#IF IS_DEBUG_BUILD
	PRINTSTRING("[JA@BETTING] SET_CLIENT_BETTING_STATE - changing from: ") 
	PRINT_CLIENT_BETTING_STATE_STRING(GET_CLIENT_BETTING_STATE())
	PRINTSTRING(" to: ")
	PRINT_CLIENT_BETTING_STATE_STRING(iBettingState)
	PRINTNL()
	#ENDIF
	
	//...If we are moving to ready state, reset sync timer
	IF iBettingState = MP_BETTING_READY
	
		PRINTLN("[JA@BETTING] SET_CLIENT_BETTING_STATE - resetting sync timer")
		MPGlobals.g_BettingSyncTimer = GET_NETWORK_TIME()
	ENDIF
	
	MPGlobals.g_MPBettingData.clientBettingState = iBettingState
ENDPROC

// Check to see if player is on state which should suppress other menu buttons
FUNC BOOL IS_LOCAL_PLAYER_VIEWING_BETTING()

	SWITCH MPGlobals.g_MPBettingData.clientBettingState
		CASE MP_BETTING_PLACING_BETS
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the amount of cash a player (playerBetting) has bet on another player (playerBetOn)
FUNC INT GET_CASH_PLAYER_HAS_BET_ON_PLAYER(PLAYER_INDEX playerBetting, INT iBettingIndex)
	RETURN GlobalplayerBD[NATIVE_TO_INT(playerBetting)].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet
ENDFUNC

/// PURPOSE: Returns amount local player has bet on player
FUNC INT GET_LOCAL_CASH_BET_ON_PLAYER(INT iBettingIndex)
	RETURN GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].clientBetData.betsOnPlayers[iBettingIndex].iPlacedBet
ENDFUNC

FUNC INT GET_TOTAL_CASH_BET_ON_PLAYER(INT iBettingIndex)
	RETURN GlobalServerBD_Betting.activeBettingData[iBettingIndex].iCashBetOnPlayer
ENDFUNC

/// PURPOSE: Tracks the amount of cash we have on the player we highlighting
PROC UPDATE_CASH_BET_ON_CORONA_PLAYER(INT iBettingIndex)
	PRINTLN("[JA@BETTING] UPDATE_CASH_BET_ON_CORONA_PLAYER - resetting current bet on slot: ", iBettingIndex)

	IF iBettingIndex < 0
		MPGlobals.g_MPBettingData.iBetOnCoronaPlayer = 0
	ELSE
		MPGlobals.g_MPBettingData.iBetOnCoronaPlayer = GET_LOCAL_CASH_BET_ON_PLAYER(iBettingIndex) //MPGlobals.coronaPosData.playerID)
	ENDIF
ENDPROC

FUNC INT GET_TOTAL_CASH_BET_ON_SPECIFIC_PLAYER(INT iBettingIndex)

	INT i
	INT iBetsOnSelectedPlayer
	PLAYER_INDEX playerID
	INT iMyGBDSlot = GET_CORONA_GBD_SLOT()
	BETTING_ID myBettingID = GET_PLAYER_BETTING_ID(iMyGBDSlot)
	
	// Loop through all players drawing their faces to betting grid
	REPEAT NUM_NETWORK_PLAYERS i
		
		playerID = INT_TO_PLAYERINDEX(i)
		IF ARE_BETTING_IDS_EQUAL(myBettingID, GET_PLAYER_BETTING_ID(i))
			
			// Get how much this player has bet on highlighted player (bottom Total Bets)
			iBetsOnSelectedPlayer += GET_CASH_PLAYER_HAS_BET_ON_PLAYER(playerID, iBettingIndex)
		ENDIF
	ENDREPEAT
	
	RETURN iBetsOnSelectedPlayer
ENDFUNC

FUNC INT GET_TOTAL_CASH_I_HAVE_BET()

	INT i
	INT iTotalBets

	// Loop through all players drawing their faces to betting grid
	REPEAT NUM_NETWORK_PLAYERS i
		// Get how much this player has bet on highlighted player (bottom Total Bets)
		iTotalBets += GET_CASH_PLAYER_HAS_BET_ON_PLAYER(PLAYER_ID(), i)
		
		// Are we processing ourselves
		IF i = GlobalServerBD_Betting.playerBettingData[NATIVE_TO_INT(PLAYER_ID())].iTeam
		
			// If there is an active gift
			IF MPGlobals.g_MPBettingData.iBetGiftAmount > 0
			
				// Deduct the amount from the total we have bet.
				// If we are beyond the amount deduct the full amount,
				IF GET_CASH_PLAYER_HAS_BET_ON_PLAYER(PLAYER_ID(), i) >= MPGlobals.g_MPBettingData.iBetGiftAmount
					iTotalBets -= MPGlobals.g_MPBettingData.iBetGiftAmount
				ELSE
					// otherwise just deduct the bet as its FREE
					iTotalBets -= GET_CASH_PLAYER_HAS_BET_ON_PLAYER(PLAYER_ID(), i)
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	RETURN iTotalBets
ENDFUNC

FUNC INT GET_TOTAL_CASH_I_HAVE_BET_ON_OTHER_PLAYERS(INT iBettingIndex)

	INT i
	INT iTotalBets

	// Loop through all players drawing their faces to betting grid
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_BETTING_INDEX_ACTIVE(i)
			IF iBettingIndex != i
				// Get how much this player has bet on highlighted player (bottom Total Bets)
				iTotalBets += GET_CASH_PLAYER_HAS_BET_ON_PLAYER(PLAYER_ID(), i)
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN iTotalBets
ENDFUNC

FUNC BOOL IS_BETTING_VALID_FOR_ACTIVITY(INT iMissionType)
	IF iMissionType = FMMC_TYPE_DEATHMATCH
	OR iMissionType = FMMC_TYPE_RACE
	OR iMissionType = FMMC_TYPE_BASE_JUMP
	//OR iMissionType = FMMC_TYPE_TRIATHLON
	OR iMissionType = FMMC_TYPE_MG_ARM_WRESTLING
	OR iMissionType = FMMC_TYPE_MG_DARTS
	OR iMissionType = FMMC_TYPE_MG_TENNIS
	OR iMissionType = FMMC_TYPE_MG_GOLF
	OR iMissionType = FMMC_TYPE_MG_SHOOTING_RANGE
	OR iMissionType = FMMC_TYPE_RACE_TO_POINT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


