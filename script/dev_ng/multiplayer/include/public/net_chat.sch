USING "rage_builtins.sch"
USING "globals.sch"
USING "net_ticker_logic.sch"

#IF IS_DEBUG_BUILD	

PROC CREATE_CHAT_WIDGET()
	START_WIDGET_GROUP("Net Chat")
		ADD_WIDGET_BOOL("GlobalServerBD.bChatOverride", GlobalServerBD.bChatOverride)
		ADD_WIDGET_BOOL("g_bChatOverrideIsActive", g_bChatOverrideIsActive)
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_CHAT_OVERRIDE()
	// chat override
	IF (GlobalServerBD.bChatOverride)
		IF NOT (g_bChatOverrideIsActive)
			NETWORK_OVERRIDE_ALL_CHAT_RESTRICTIONS(TRUE)
			IF NOT (g_iLastChatOverrideTicker = -1)
				THEFEED_REMOVE_ITEM(g_iLastChatOverrideTicker)
			ENDIF
			g_iLastChatOverrideTicker = PRINT_DEBUG_TICKER("Chat override ACTIVE")
			g_bChatOverrideIsActive = TRUE
		ENDIF
	ELSE
		IF (g_bChatOverrideIsActive)
			NETWORK_OVERRIDE_ALL_CHAT_RESTRICTIONS(FALSE)
			IF NOT (g_iLastChatOverrideTicker = -1)
				THEFEED_REMOVE_ITEM(g_iLastChatOverrideTicker)
			ENDIF
			g_iLastChatOverrideTicker = PRINT_DEBUG_TICKER("Chat override DISABLED")
			g_bChatOverrideIsActive = FALSE
		ENDIF
	ENDIF
ENDPROC

#ENDIF
