USING "rage_builtins.sch"
USING "globals.sch"

USING "dialogue_public.sch"
USING "net_comms_public.sch"

#IF IS_DEBUG_BUILD
	USING "net_comms_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_comms_EOM.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP End Of Mission message control routines.
//		NOTES			:	A maintenance function will be called each frame from the MP Comms script.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      General MP EOM message functions
// ===========================================================================================================

// PURPOSE:	Clear out the global EOM message variables
//
// INPUT PARAMS:	paramBeingInitialised		TRUE if this is being called during initialisation [default = FALSE]
PROC Reset_MP_End_Of_Mission_Message_Details(BOOL paramBeingInitialised = FALSE)

	g_sCommsMP.EOM.bitflagsEOM			= CLEAR_ALL_MP_EOM_MESSAGE_BITFLAGS
	g_sCommsMP.EOM.characterEOM			= ""
	g_sCommsMP.EOM.speakerEOM			= ""
	g_sCommsMP.EOM.groupEOM				= ""
	g_sCommsMP.EOM.rootEOM				= ""
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Reset_MP_End_Of_Mission_Message_Details() - All details cleared") NET_NL()
	#ENDIF
	
	// Don't update the widgets if being initialised - the widgets don't yet exist
	IF (paramBeingInitialised)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Clear out the text widgets
		g_WIDGET_EOM_ScriptName = ""
		Update_MP_End_Of_Mission_Text_Widgets()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set an EOM Message as complete
PROC MP_End_Of_Mission_Message_Has_Completed()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: The End of Mission Message has ended") NET_NL()
	#ENDIF
	
	Reset_MP_End_Of_Mission_Message_Details()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set an EOM message as received
PROC Set_MP_End_Of_Mission_Message_Received()
	SET_BIT(g_sCommsMP.EOM.bitflagsEOM, MP_EOM_MESSAGE_RECEIVED)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if there is an End Of Mission message has been received
//
// RETURN VALUE:	BOOL				TRUE if an EOM message has been received, otherwise FALSE
FUNC BOOL Has_An_MP_End_Of_Mission_Message_Been_Received()
	RETURN (IS_BIT_SET(g_sCommsMP.EOM.bitflagsEOM, MP_EOM_MESSAGE_RECEIVED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set an EOM message as active
PROC Set_MP_End_Of_Mission_Message_Active()
	SET_BIT(g_sCommsMP.EOM.bitflagsEOM, MP_EOM_MESSAGE_ACTIVE)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if there is an active End Of Mission message
//
// RETURN VALUE:	BOOL				TRUE if there is an active EOM message, otherwise FALSE
FUNC BOOL Is_There_An_Active_MP_End_Of_Mission_Message()
	RETURN (IS_BIT_SET(g_sCommsMP.EOM.bitflagsEOM, MP_EOM_MESSAGE_ACTIVE))
ENDFUNC





// ===========================================================================================================
//      The Main MP EOM Control Routines
// ===========================================================================================================

// PURPOSE:	A one-off EOM initialisation.
PROC Initialise_MP_End_Of_Mission_Message()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Initialise_MP_End_Of_Mission_Message") NET_NL()
	#ENDIF

	BOOL beingInitialised = TRUE
	Reset_MP_End_Of_Mission_Message_Details(beingInitialised)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	This procedure should be called every frame to control access to the communications systems.
PROC Maintain_MP_End_Of_Mission_Message()

	#IF IS_DEBUG_BUILD
		Maintain_MP_End_Of_Mission_Widgets()
	#ENDIF

	// If an EOM Message hasn't been received then do nothing
	IF NOT (Has_An_MP_End_Of_Mission_Message_Been_Received())
		EXIT
	ENDIF
	
	// There is an EOM message that needs maintained
	structPedsForConversation conversationStruct
	
	
	// If the EOM Message isn't active, then try to activate it
	IF NOT (Is_There_An_Active_MP_End_Of_Mission_Message())
		// Add the speaker of the End of Mission message to the conversation struct
		ADD_PED_FOR_DIALOGUE(ConversationStruct, ConvertSingleCharacter(g_sCommsMP.EOM.speakerEOM), NULL, g_sCommsMP.EOM.characterEOM)
		
		// Request permission from the communications systems to play the end of mission message
		IF (Request_MP_Comms_End_Of_Mission_Message(conversationStruct, g_sCommsMP.EOM.groupEOM, g_sCommsMP.EOM.rootEOM))
			// ...permission granted, end of mission message now being played
			Set_MP_End_Of_Mission_Message_Active()
		ENDIF
		
		EXIT
	ENDIF
	
	// The EOM message must already be active, so check if it has ended
	IF NOT (Is_MP_Comms_Still_Playing())
		MP_End_Of_Mission_Message_Has_Completed()
	ENDIF

ENDPROC






// ===========================================================================================================
//
//									End of Mission Message Public Access Functions
//
// ===========================================================================================================

// PURPOSE:	Allows a MP script to pass in the details of an End of Mission communication.
//
// INPUT PARAMS:	paramCharacter		The Character ID string (from the 'Character' dropdown box in dialogueStar)
//					paramSpeakerID		The Speaker ID string (from the 'Speaker' value in dialogueStar)
//					paramGroupID		The Subtitle Group ID string for the Conversation (from the 'Subtitle Group ID' value in dialogueStar)
//					paramRootID			The Root ID string for the Conversation (from the 'Root' value in dialogueStar)
//
// NOTES:	The details will be stored and the Comms system will trigger this speech using the appropriate method (cellphone or radio).
PROC Store_MP_End_Of_Mission_Message(STRING paramCharacter, STRING paramSpeakerID, STRING paramGroupID, STRING paramRootID)
	
	// Error Checking
	IF (Is_String_Null_Or_Empty(paramCharacter))
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Character ID string is NULL or empty - This should match the 'Character' dropdown box in dialogueStar")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramCharacter) > MAX_LENGTH_MP_COMMS_CHARACTER_ID_STRING)
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Character ID string is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	IF (Is_String_Null_Or_Empty(paramSpeakerID))
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Speaker ID string is NULL or empty - This should match the 'Speaker' value in dialogueStar")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramSpeakerID) > MAX_LENGTH_MP_COMMS_SPEAKER_ID_STRING)
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Speaker ID string should have only one character in range 0-8 or A-Z - This should match the 'Speaker' value in dialogueStar")
		EXIT
	ENDIF
	
	IF (Is_String_Null_Or_Empty(paramGroupID))
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Group ID string is NULL or empty - This should match the 'Subtitle Group ID' value in dialogueStar")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramGroupID) > MAX_LENGTH_MP_COMMS_GROUP_ID_STRING)
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Group ID string is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	IF (Is_String_Null_Or_Empty(paramRootID))
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Group ID string is NULL or empty - This should match the 'Root' value in dialogueStar")
		EXIT
	ENDIF

	IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramRootID) > MAX_LENGTH_MP_COMMS_ROOT_ID_STRING)
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): The Root ID string is longer than expected. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Script requested MP End of Mission message: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NET_PRINT("...........From Character: ") NET_PRINT(paramCharacter) NET_PRINT("   [speaker ID = ") NET_PRINT(paramSpeakerID) NET_PRINT("]") NET_NL()
		NET_PRINT("...........Group ID: ") NET_PRINT(paramGroupID) NET_PRINT("   Root ID: ") NET_PRINT(paramRootID) NET_NL()
	#ENDIF
	
	// Ensure an End Of Mission Message isn't already in progress
	// NOTE: The assert may be too strict - EXIT may be enough
	IF (Is_There_An_Active_MP_End_Of_Mission_Message())
		SCRIPT_ASSERT("Store_MP_End_Of_Mission_Message(): There is already an active MP End of Mission Message. Tell Keith. (NOTE: This Assert may prove to be too strict)")
		EXIT
	ENDIF
	
	// Store the details ready for triggering
	// ...first, clear out the globals
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: New Message Accepted - Clearing Storage Variables") NET_NL()
	#ENDIF
	Reset_MP_End_Of_Mission_Message_Details()
	
	// ...then store the details of the new message and mark it as pending
	g_sCommsMP.EOM.characterEOM		= paramCharacter
	g_sCommsMP.EOM.speakerEOM		= paramSpeakerID
	g_sCommsMP.EOM.groupEOM			= paramGroupID
	g_sCommsMP.EOM.rootEOM			= paramRootID
	Set_MP_End_Of_Mission_Message_Received()
	
	#IF IS_DEBUG_BUILD
		// Store the text widgets that display text that is otherwise non-persistent
		g_WIDGET_EOM_ScriptName = GET_THIS_SCRIPT_NAME()
		Update_MP_End_Of_Mission_Text_Widgets()
	#ENDIF
	
ENDPROC

