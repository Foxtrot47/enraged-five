//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CASINO_NIGHTCLUB.sch																			//
// Description: Functions to implement from net_peds_base.sch and look up tables to return casino nightclub ped data.	//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_CASINO_NIGHTCLUB
USING "net_peds_base.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR _GET_CASINO_NIGHTCLUB_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN <<1551.7854, 249.9202, -48.2297>>	// DJ Stand Coords
ENDFUNC
		
FUNC FLOAT _GET_CASINO_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN 180.00	// DJ Stand Heading
ENDFUNC

PROC _SET_CASINO_NIGHTCLUB_PED_DATA_LAYOUT_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	CLUB_DJS eCurrentDJ = g_clubMusicData.eActiveDJ
	IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
		eCurrentDJ = g_clubMusicData.eNextDJ
	ENDIF
	
	SWITCH iPed
		// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_RAMPA)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_RAMPA)
							Data.iPackedDrawable[0]								= 65536
							Data.iPackedDrawable[1]								= 33619970
							Data.iPackedDrawable[2]								= 65536
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK	
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN)
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK
						CASE CLUB_DJ_PALMS_TRAX
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_PALMS_TRAX)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_PALMS_TRAX)
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK
						DEFAULT 
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK	
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[0] = Data.iActivity 
					#ENDIF
						
					Data.iLevel 										= 0
					
					IF eCurrentDJ = CLUB_DJ_PALMS_TRAX
						Data.vPosition 										= <<-0.3997, 0.0652, -0.0433>>
						Data.vRotation 										= <<0.0, 0.0, 90.0000>>
					ELIF eCurrentDJ = CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						Data.vPosition 										= <<-0.5135, 0.0502, -0.0903>>
						Data.vRotation 										= <<0.0, 0.0, 90.0000>>
					ELIF eCurrentDJ = CLUB_DJ_MOODYMANN
						Data.vPosition 										= <<-0.5197, -0.0198, -0.0903>>
						Data.vRotation 										= <<0.0, 0.0, 90.0000>>
					ELSE
						Data.vPosition 										= <<1.8594, -0.1765, -2.1764>>
						Data.vRotation 										= <<0.0, 0.0, 180.0000>>
					ENDIF
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_ME)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ME)
							Data.vPosition 										=  <<-0.5135, 0.0502, -0.0903>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 33619968
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 16908288
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 65537
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[1] = Data.iActivity 
							#ENDIF
						BREAK
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN_DANC_01)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_01)
							Data.vPosition 										=  <<-0.5326, 0.0402, -0.1703>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[1] = Data.iActivity 
							#ENDIF
						BREAK
						DEFAULT
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_05)
							Data.iLevel 										= 0
							Data.iPackedDrawable[0]								= -1
							Data.iPackedDrawable[1]								= -1
							Data.iPackedDrawable[2]								= -1
							Data.iPackedTexture[0] 								= -1
							Data.iPackedTexture[1] 								= -1
							Data.iPackedTexture[2] 								= -1
							Data.vPosition 										= <<-4.1646, -4.4098, -0.7703>>
							Data.vRotation 										= <<0.0, 0.0, 357.1200>>
							CLEAR_PEDS_BITSET(Data.iBS)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[1] = -1
							#ENDIF
							IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
							ENDIF
						BREAK
					ENDSWITCH
					Data.iLevel 										= 0
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_ADAM)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ADAM)
							Data.vPosition 										=  <<-0.5135, 0.0502, -0.0903>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 16777216
							Data.iPackedDrawable[1]								= 257
							Data.iPackedDrawable[2]								= 1
							Data.iPackedTexture[0] 								= 16777216
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[2] = Data.iActivity 
							#ENDIF
						BREAK
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN_DANC_02)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_02)
							Data.vPosition 										=  <<-0.5326, 0.0402, -0.1703>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[2] = Data.iActivity 
							#ENDIF
						BREAK
						DEFAULT
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
							Data.iLevel 										= 0
							Data.iPackedDrawable[0]								= -1
							Data.iPackedDrawable[1]								= -1
							Data.iPackedDrawable[2]								= -1
							Data.iPackedTexture[0] 								= -1
							Data.iPackedTexture[1] 								= -1
							Data.iPackedTexture[2] 								= -1
							Data.vPosition 										= <<-9.9546, -3.9798, -0.7703>>
							Data.vRotation 										= <<0.0, 0.0, 296.6400>>
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[2] = -1
							#ENDIF
							IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
							ENDIF
						BREAK
					ENDSWITCH
					Data.iLevel 										= 0
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN_DANC_03)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_03)
							Data.vPosition 										=  <<1.9154, 1.6202, 0.0240>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
						BREAK
						DEFAULT
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
							Data.iLevel 										= 0
							Data.iPackedDrawable[0]								= -1
							Data.iPackedDrawable[1]								= -1
							Data.iPackedDrawable[2]								= -1
							Data.iPackedTexture[0] 								= -1
							Data.iPackedTexture[1] 								= -1
							Data.iPackedTexture[2] 								= -1
							Data.vPosition 										= <<-3.6526, 6.2782, -0.7763>>
							Data.vRotation 										= <<0.0, 0.0, 200.0000>>
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
							IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
							ENDIF
						BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67764225
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100990980
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.5226, 6.3682, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 200.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 196611
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.0216, -2.0771, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 292.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 1
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16908288
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-14.6241, 0.9928, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 241.1900>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 262144
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.2184, 0.5224, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 95.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 100925443
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.4806, 0.1028, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 280.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082691
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.6506, -1.5301, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 321.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_RECEPTIONIST)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= -1
					Data.iPackedDrawable[1]								= -1
					Data.iPackedDrawable[2]								= -1
					Data.iPackedTexture[0] 								= -1
					Data.iPackedTexture[1] 								= -1
					Data.iPackedTexture[2] 								= -1
					Data.vPosition 										= <<-23.5591, 0.2105, 2.2243>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 11
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 134479872
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-26.2697, 6.4932, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 12
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 84606979
					Data.iPackedDrawable[1]								= 196611
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 17039360
					Data.iPackedTexture[1] 								= 131074
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-25.1106, 2.9532, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 167.0400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 13
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65539
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 196608
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-19.7671, 4.6432, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 51.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 14
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50855938
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842755
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.8643, -0.3650, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 291.4240>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 15
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 117506049
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<-5.3976, 0.4755, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 188.6640>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 16
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SITTING_AT_BAR_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 151191555
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.0276, 1.8122, 1.9447>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 17
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50855938
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50659328
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.2086, -1.0751, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 272.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 18
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 34144258
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50331652
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.7646, 1.4340, -0.7759>>
					Data.vRotation 										= <<0.0000, 0.0000, 253.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 19
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462721
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 117506050
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-3.9286, -0.1056, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 342.9350>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 20
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 655364
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100925440
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.1426, -1.4282, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 289.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 21
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_06)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 151060480
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.1149, -3.8337, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 311.0400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 22
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 50528259
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.7726, -1.3498, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 285.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 23
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502081
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619969
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.1566, 2.9906, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 228.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 24
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_03)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 84148227
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.7036, -4.9184, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 356.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 25
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 33685506
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67305474
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-3.9886, -7.2012, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 292.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 26
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 101122051
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50397185
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.2766, 2.7649, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 162.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 27
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 100859908
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84082691
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.7906, 0.5902, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 260.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 28
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.8036, 1.5996, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 264.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 29
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33947650
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117440515
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.6816, -2.4212, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 318.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 30
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33751041
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 83951620
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-4.0717, 0.8629, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 229.8250>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 31
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.9286, 1.6202, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 271.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 32
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 67305474
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.6053, -1.1879, -0.9703>>
					Data.vRotation 										= <<0.0000, 0.0000, 67.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 33
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67567620
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50659332
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.5496, 1.2339, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 321.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 34
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 16842753
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67108867
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.8806, -2.5174, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 35
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67764225
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.9386, 3.4149, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 262.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 36
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502084
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 16908289
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.6053, -1.1879, -0.9703>>
					Data.vRotation 										= <<0.0000, 0.0000, 67.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 37
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082691
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 4
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.0201, -0.4912, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 18.5608>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 38
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082691
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117571584
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.5796, -1.9561, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 309.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 39
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33751042
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.4716, 1.6349, -0.7759>>
					Data.vRotation 										= <<0.0000, 0.0000, 267.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 40
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134479873
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-5.0216, -2.0771, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 292.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 41
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 100728834
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.3954, -4.8792, 2.2242>>
					Data.vRotation 										= <<0.0000, 0.0000, 5.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 42
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 34209794
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134217730
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.2136, -7.2787, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 26.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 43
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 67436547
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 33554436
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<0.4551, -4.9087, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 352.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 44
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_SMOKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 83951616
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<4.9644, -4.8248, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 45
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 67108867
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.4825, -3.0968, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 320.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 46
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17432577
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.7366, -3.3324, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 299.6002>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 47
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5984, 5.3428, -0.3803>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 48
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67633152
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84148228
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.4236, -0.5002, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 285.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 49
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 100794369
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.3875, -3.6591, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 348.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 50
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 50528258
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-14.3306, 3.1769, 2.2147>>
					Data.vRotation 										= <<0.0000, 0.0000, 73.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 51
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-17.9615, -2.3321, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 222.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 52
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 100859908
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117702657
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.2626, 2.0180, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 173.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 53
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 134479876
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.6125, 2.4330, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 191.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 54
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 84475907
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151257092
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-15.0076, 3.1322, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 321.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 55
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33751041
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16973824
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-3.5856, 2.1112, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 254.8800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 56
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SITTING_DRINKING_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 34275330
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 17104897
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-17.8516, -3.3348, -1.2763>>
					Data.vRotation 										= <<0.0000, 0.0000, 352.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 57
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KAYLEE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 58
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BODYGUARD)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_BODYGUARD)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 59
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33685506
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131075
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.1906, -0.7923, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 284.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 60
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84410371
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134348800
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.9165, 1.7626, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 81.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 61
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_PATRICIA)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 62
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 84148226
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.8776, 2.2277, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 262.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 63
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_VIP_BAR_DRINKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 50855938
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33751040
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.2866, -2.6721, 2.2251>>
					Data.vRotation 										= <<0.0000, 0.0000, 5.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 64
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_M_CHILD_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84606979
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84082691
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.2246, 4.8802, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 219.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 65
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33751042
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 100925440
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-7.9286, -3.8491, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 347.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 66
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33751041
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 150994944
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-10.4996, -0.9487, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 28.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 67
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 589824
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33751040
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.9706, -0.0028, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 225.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 68
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_02)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 655363
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16973827
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.8746, -2.3898, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 284.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 69
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67502080
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151257089
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8676, 5.5718, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 258.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 70
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65538
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.6344, -4.2998, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 170.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 71
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 67239939
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<7.9044, -7.9548, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 115.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 72
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 50528260
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.4886, -7.2442, 2.2244>>
					Data.vRotation 										= <<0.0000, 0.0000, 52.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 73
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67436547
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 67108868
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-13.7706, -1.9162, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 197.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 74
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 117571585
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-13.1106, -7.0700, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 308.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 75
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_06)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 17498113
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131076
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.1426, -4.8955, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 356.3979>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 76
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_02)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 134217728
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.7256, -2.9170, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 286.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 77
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 16842753
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50528256
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-15.8217, -2.1720, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 78
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 458756
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117637122
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.1154, -1.6198, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 9.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 79
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151060482
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-13.7366, -2.6411, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 301.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 80
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 117702659
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.2246, 4.8802, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 219.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 81
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 50462724
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5955, -0.6598, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 85.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 82
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 131072
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134217730
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-24.9886, 2.4142, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 83
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 83886080
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-17.0605, -7.3328, 2.2547>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 84
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_M_CHILD_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84017155
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.4806, 0.1028, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 280.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 85
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462722
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134479876
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<-7.4806, 0.1028, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 280.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 86
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPC_M_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 34078722
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100663297
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.4836, -2.4810, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 87
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPC_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50397187
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.4836, -2.4810, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 88
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17235968
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 150994946
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.4956, -2.8049, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 30.8639>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 89
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 150994945
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.3654, -4.4860, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 65.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 90
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 84475907
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67174404
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.3614, -3.5167, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 48.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 91
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84148225
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.0626, -4.8758, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 356.3980>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 92
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_03)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 50921474
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117571586
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8356, 0.0632, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 265.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 93
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_03)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 150994946
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8756, 0.9032, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 252.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 94
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_VIP_BAR_DRINKING_M_02)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 393220
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67436547
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-18.0416, -4.6952, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 16.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 95
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MIGUEL)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 96
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<3.6444, 1.3770, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 106.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 97
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 101122048
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100794368
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<0.3374, -4.4888, -0.7762>>
					Data.vRotation 										= <<0.0000, 0.0000, 302.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 98
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 50593793
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908289
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-16.3036, 2.4359, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 125.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 99
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67698692
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 327681
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-27.5836, 1.0065, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 268.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 100
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CHECK_OUT_MIRROR)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 117702658
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.6144, -1.3048, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 86.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 101
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 262144
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.4844, -8.3448, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 102
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151126019
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<5.5144, -4.6648, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 120.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 103
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_06)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 16908289
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.7076, -2.3636, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 289.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 104
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 83951618
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-14.8186, 4.0242, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 204.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 105
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 134348801
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.2246, 4.8802, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 219.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 106
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 101384193
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67108867
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.2286, 4.7352, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 217.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 107
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84148227
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100859906
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.4366, 4.9592, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 159.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 108
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.2366, 4.3692, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 268.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 109
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50790402
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84148225
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.0955, 2.2040, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 110
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777217
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908290
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.1016, -0.3558, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 277.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 111
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50724866
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151126020
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.7976, 0.4510, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 218.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 112
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 101384193
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.0146, 6.2202, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 143.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 113
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 4
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.8947, 6.2102, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 231.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 114
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33685505
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 50331651
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<-8.9386, 3.4149, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 262.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 115
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502084
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 117637124
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.4476, 3.0943, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 164.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 116
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33882114
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 150994946
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.1046, 2.5871, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 249.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 117
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JACKIE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 118
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 151257089
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.5706, 3.2762, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 244.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 119
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SMOKING_WEED_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 67502084
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134414340
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.1954, 6.3102, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 156.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 120
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_SMOKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134414340
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<6.6554, 5.7402, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 121
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134348801
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.2854, -4.9588, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 1.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// Transform the local coords into world coords.
	IF NOT IS_VECTOR_ZERO(Data.vPosition)
		Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_CASINO_NIGHTCLUB_PED_LOCAL_COORDS_BASE_POSITION(), _GET_CASINO_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
	ENDIF
	
	// Transform the local heading into world heading.
	IF (Data.vRotation.z != -1.0)
		Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_CASINO_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
	ENDIF
	
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_DATA_LAYOUT_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	CLUB_DJS eCurrentDJ = g_clubMusicData.eActiveDJ
	IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
		eCurrentDJ = g_clubMusicData.eNextDJ
	ENDIF
	
	SWITCH iPed
		// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_RAMPA)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_RAMPA)
							Data.iPackedDrawable[0]								= 65536
							Data.iPackedDrawable[1]								= 33619970
							Data.iPackedDrawable[2]								= 65536
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK	
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN)
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK
						CASE CLUB_DJ_PALMS_TRAX
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_PALMS_TRAX)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_PALMS_TRAX)
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK
						DEFAULT 
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
						BREAK	
					ENDSWITCH
					
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[0] = Data.iActivity 
					#ENDIF
					
					Data.iLevel 										= 0
					IF eCurrentDJ = CLUB_DJ_PALMS_TRAX
						Data.vPosition 										= <<-0.3997, 0.0652, -0.0433>>
						Data.vRotation 										= <<0.0, 0.0, 90.0000>>
					ELIF eCurrentDJ = CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						Data.vPosition 										= <<-0.5135, 0.0502, -0.0903>>
						Data.vRotation 										= <<0.0, 0.0, 90.0000>>
					ELIF eCurrentDJ = CLUB_DJ_MOODYMANN
						Data.vPosition 										= <<-0.5197, -0.0198, -0.0903>>
						Data.vRotation 										= <<0.0, 0.0, 90.0>>
					ELSE
						Data.vPosition 										= <<1.8594, -0.1765, -2.1764>>
						Data.vRotation 										= <<0.0, 0.0, 180.0000>>
					ENDIF
					
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_ME)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ME)
							Data.vPosition 										=  <<-0.5135, 0.0502, -0.0903>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 33619968
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 16908288
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 65537
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[1] = Data.iActivity 
							#ENDIF
						BREAK
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN_DANC_01)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_01)
							Data.vPosition 										=  <<-0.5326, 0.0402, -0.1703>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[1] = Data.iActivity 
							#ENDIF
						BREAK
						DEFAULT
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_02)
							Data.iLevel 										= 0
							Data.iPackedDrawable[0]								= -1
							Data.iPackedDrawable[1]								= -1
							Data.iPackedDrawable[2]								= -1
							Data.iPackedTexture[0] 								= -1
							Data.iPackedTexture[1] 								= -1
							Data.iPackedTexture[2] 								= -1
							Data.vPosition 										= <<-10.9946, 3.2402, -0.7703>>
							Data.vRotation 										= <<0.0, 0.0, 259.2000>>
							CLEAR_PEDS_BITSET(Data.iBS)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[1] = -1
							#ENDIF
							IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
							ENDIF
						BREAK
					ENDSWITCH
					Data.iLevel 										= 0
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
						CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_ADAM)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ADAM)
							Data.vPosition 										=  <<-0.5135, 0.0502, -0.0903>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 16777216
							Data.iPackedDrawable[1]								= 257
							Data.iPackedDrawable[2]								= 1
							Data.iPackedTexture[0] 								= 16777216
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[2] = Data.iActivity 
							#ENDIF
						BREAK
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN_DANC_02)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_02)
							Data.vPosition 										=  <<-0.5326, 0.0402, -0.1703>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[2] = Data.iActivity 
							#ENDIF
						BREAK
						DEFAULT
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
							Data.iLevel 										= 0
							Data.iPackedDrawable[0]								= -1
							Data.iPackedDrawable[1]								= -1
							Data.iPackedDrawable[2]								= -1
							Data.iPackedTexture[0] 								= -1
							Data.iPackedTexture[1] 								= -1
							Data.iPackedTexture[2] 								= -1
							Data.vPosition 										= <<-9.4426, -4.2098, -0.7703>>
							Data.vRotation 										= <<0.0, 0.0, 330.0000>>
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
							#IF IS_DEBUG_BUILD
							g_DebugAnimSeqDetail.pedActivity[2] = -1
							#ENDIF
							IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
							ENDIF
						BREAK
					ENDSWITCH
					Data.iLevel 										= 0
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					SWITCH eCurrentDJ
						CASE CLUB_DJ_MOODYMANN
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MOODYMANN_DANC_03)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_MOODYMANN_DANCER_03)
							Data.vPosition 										=  <<1.9154, 1.6202, 0.0240>>
							Data.vRotation 										= <<0.0, 0.0, 90.0000>>
							Data.iPackedDrawable[0]								= 0
							Data.iPackedDrawable[1]								= 0
							Data.iPackedDrawable[2]								= 0
							Data.iPackedTexture[0] 								= 0
							Data.iPackedTexture[1] 								= 0
							Data.iPackedTexture[2] 								= 0
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
						BREAK
						DEFAULT
							Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_F_01)
							Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
							Data.iPackedDrawable[0]								= -1
							Data.iPackedDrawable[1]								= -1
							Data.iPackedDrawable[2]								= -1
							Data.iPackedTexture[0] 								= -1
							Data.iPackedTexture[1] 								= -1
							Data.iPackedTexture[2] 								= -1
							Data.vPosition 										= <<7.7544, -8.2748, 2.2297>>
							Data.vRotation 										= <<0.0, 0.0, 0.0000>>
							CLEAR_PEDS_BITSET(Data.iBS)
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
							IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
							ENDIF
							IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
								SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
							ENDIF
						BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 34078722
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619972
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.0706, 4.4262, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 235.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 50397185
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.5734, -4.3271, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 352.8016>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 1
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16908288
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-14.6241, 0.9928, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 241.1900>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPD_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 50331650
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.9434, 0.7738, -0.9803>>
					Data.vRotation 										= <<0.0000, 0.0000, 105.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 117440515
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.3354, -0.7912, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_SMOKING_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67567617
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151126019
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8606, 4.6303, 2.2239>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_RECEPTIONIST)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= -1
					Data.iPackedDrawable[1]								= -1
					Data.iPackedDrawable[2]								= -1
					Data.iPackedTexture[0] 								= -1
					Data.iPackedTexture[1] 								= -1
					Data.iPackedTexture[2] 								= -1
					Data.vPosition 										= <<-23.6451, 0.3655, 2.2243>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 11
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_06)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 101253120
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117702660
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.2926, 5.6602, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 200.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 12
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50397188
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-27.4756, 1.8302, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 239.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 13
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65538
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-19.9531, 4.6957, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 56.5100>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 14
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436547
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.7046, -1.1111, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 289.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 15
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 134283267
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.7426, -0.4076, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 298.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 16
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67108867
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.9246, -0.2492, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 271.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 17
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151126016
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.3436, -0.1441, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 271.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 18
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_VIP_BAR_DRINKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 84082691
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65540
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.0696, -1.2470, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 35.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 19
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_F_PARENT)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 150994944
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.1545, -1.7798, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 301.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 20
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082691
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100794371
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.3676, -3.8672, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 302.1050>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 21
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50987010
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67174404
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.9016, -0.2858, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 277.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 22
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 117637121
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.9006, -2.3505, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 338.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 23
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_06)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 17367041
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67436547
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.9937, -4.8784, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 24
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_03)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84017154
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.1187, -4.9212, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 353.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 25
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50462724
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.0764, 3.7209, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 219.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 26
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 101056516
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134283265
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.9224, -2.3191, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 300.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 27
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 458752
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100925443
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.1504, -3.3401, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 354.1350>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 28
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 33685506
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-26.0216, 6.3058, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 116.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 29
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50790402
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33882114
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.9706, 0.7286, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 243.9750>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 30
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 150994948
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.7526, 2.0378, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 240.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 31
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17367041
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.4204, -1.1742, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 58.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 32
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50921474
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 83951619
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.4596, 0.9657, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 17.4600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 33
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 84410371
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 83886081
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-27.3156, 1.2002, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 348.5800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 34
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 100859908
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100859907
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.9314, 0.8458, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 254.8800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 35
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 134414340
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-5.2816, -2.2768, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 300.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 36
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 1
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50397186
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.2709, 1.0103, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 297.7640>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 37
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082691
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.7056, -1.3081, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 320.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 38
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 17039364
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.8785, -0.0161, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 273.5967>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 39
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33685505
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 151060482
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<-5.2816, -2.2768, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 300.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 40
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 50528260
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-15.6805, 4.5730, 2.2243>>
					Data.vRotation 										= <<0.0000, 0.0000, 215.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 41
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_M_CHILD_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 84541443
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117571584
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.1545, -1.7798, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 301.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 42
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 151257090
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.0836, -7.3387, 2.2245>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 43
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 33685505
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50462720
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<0.4551, -4.9087, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 352.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 44
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_SMOKING_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 100859908
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67371012
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.7144, -5.9948, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 170.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 45
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_04)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 67633152
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.7756, -3.4472, 2.2249>>
					Data.vRotation 										= <<0.0000, 0.0000, 277.9200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 46
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_02)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117702658
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-15.3055, 3.9823, 2.2043>>
					Data.vRotation 										= <<0.0000, 0.0000, 54.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 47
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5984, 5.3428, -0.3803>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 48
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPE_F_CHILD_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151060483
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-12.1545, -1.7798, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 301.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 49
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_04)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67633156
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67436544
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8256, 2.1105, 2.2243>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 50
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 262144
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.4904, -1.7541, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 280.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 51
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84148227
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67305476
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.9606, -1.4710, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 300.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 52
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 100728832
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-2.8026, -1.2898, -0.7765>>
					Data.vRotation 										= <<0.0000, 0.0000, 285.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 53
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_VIP_BAR_DRINKING_M_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 17432577
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50593794
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-18.2036, -4.7636, 2.2254>>
					Data.vRotation 										= <<0.0000, 0.0000, 13.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 54
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 458752
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84082690
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.1246, 6.3302, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 198.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 55
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33751041
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16973825
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<-7.2524, 0.6438, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 324.8900>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 56
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 67371008
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.7126, -7.3262, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 320.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 57
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KAYLEE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 58
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BODYGUARD)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_BODYGUARD)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 59
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 100794368
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.2146, 6.1002, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 146.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 60
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908289
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.1316, 4.4610, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 61
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_PATRICIA)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 62
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 33816579
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.9846, -1.4944, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 320.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 63
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 67436546
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.1024, -4.4252, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 10.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 64
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 524289
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33816577
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.9536, -2.6077, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 320.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 65
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 33751042
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.3326, 1.4115, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 232.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 66
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 50528258
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67239940
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-15.6716, -5.9456, 2.2248>>
					Data.vRotation 										= <<0.0000, 0.0000, 54.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 67
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 83886083
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.6006, 3.6549, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 293.1100>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 68
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134545408
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.8036, -2.4031, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 311.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 69
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84606979
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16973826
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.9846, -1.4944, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 320.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 70
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CLUB_BOUNCERS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_GUARD_STANDING)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65539
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 196608
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.6344, -4.2998, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 170.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 71
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84148227
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<3.6733, -3.2768, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 30.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 72
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 117506049
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.1646, 5.4739, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 192.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 73
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 33619971
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.3766, 4.5194, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 333.6100>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 74
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50921474
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117637120
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-13.8236, -1.9468, 2.2239>>
					Data.vRotation 										= <<0.0000, 0.0000, 206.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 75
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 34275330
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.1146, 2.4979, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 76
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502084
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 84082689
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-13.7316, -2.5178, 2.2239>>
					Data.vRotation 										= <<0.0000, 0.0000, 1.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 77
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 67502080
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 83886084
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.2725, -4.8357, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 78
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_06)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 50462721
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 84082692
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-11.8646, 0.4277, 2.1947>>
					Data.vRotation 										= <<0.0000, 0.0000, 260.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 79
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 134479872
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.3116, 4.9690, 2.2251>>
					Data.vRotation 										= <<0.0000, 0.0000, 225.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 80
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SITTING_DRINKING_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 101056516
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.1836, 1.5536, 2.0147>>
					Data.vRotation 										= <<0.0000, 0.0000, 351.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 81
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_04)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 33947650
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 84082689
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8676, -0.4401, 2.2251>>
					Data.vRotation 										= <<0.0000, 0.0000, 267.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 82
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117571585
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.0166, 3.4892, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 232.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 83
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 50462720
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.8365, 4.1592, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 257.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 84
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67829763
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33882115
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.3564, 1.7718, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 247.2750>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 85
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436548
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 17039362
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.0627, 2.4768, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 215.7211>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 86
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 67305476
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.3376, -3.8792, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 323.1489>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 87
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67633152
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100859904
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.0582, -3.2593, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 272.4489>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 88
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 34275330
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 17039360
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.5106, 3.1499, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 225.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 89
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_F_PARENT)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<2.5264, -3.5971, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 30.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 90
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_M_CHILD_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 34209794
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134217729
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5264, -3.5971, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 30.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 91
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593793
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-8.9666, 3.6592, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 36.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 92
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_03)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134217731
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.0767, 4.6592, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 159.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 93
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 720900
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50331651
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.2017, -6.3158, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 351.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 94
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPD_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17235972
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 117440512
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.9434, 0.7738, -0.9803>>
					Data.vRotation 										= <<0.0000, 0.0000, 105.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 95
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_MIGUEL)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 96
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 134479875
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.5746, 1.8322, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 254.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 97
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84606979
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 151126018
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.1454, -1.5198, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 24.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 98
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33685506
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33816577
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-10.1554, 1.6979, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 311.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 99
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 67108866
					Data.iPackedTexture[1] 								= 6
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-9.8896, 2.4952, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 173.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 100
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSCLE_FLEX)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 50987010
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 327683
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.7144, -2.3048, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 36.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 101
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 101056512
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 17039360
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.8544, -1.3648, -0.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 151.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 102
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 84213764
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 50528260
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.8844, -6.4748, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 36.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 103
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.8414, -4.9097, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 104
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 100794371
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-6.9766, 3.3292, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 115.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 105
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 65540
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-7.5066, 2.5192, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 348.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 106
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134283266
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<4.9944, -4.8348, -0.7763>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 107
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPC_M_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777217
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16973824
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.6324, -0.2480, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 274.0486>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 108
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPD_M_CHILD_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84541443
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50528260
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.9434, 0.7738, -0.9803>>
					Data.vRotation 										= <<0.0000, 0.0000, 105.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 109
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 100728834
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.9954, 2.2002, 0.0297>>
					Data.vRotation 										= <<0.0000, 0.0000, 143.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 110
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DRINKING_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 100794369
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.3416, -5.5158, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 219.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 111
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPC_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 100925444
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 150994946
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.6324, -0.2480, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 274.0486>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 112
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 4
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 262148
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5754, -0.2498, -0.9803>>
					Data.vRotation 										= <<0.0000, 0.0000, 76.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 113
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 67239940
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5754, -0.2498, -0.9803>>
					Data.vRotation 										= <<0.0000, 0.0000, 76.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 114
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462722
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 134217728
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 4
					Data.vPosition 										= <<-9.1384, 1.4049, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 264.3500>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 115
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528258
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 262146
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-5.2876, 1.9243, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 240.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 116
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67567620
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 134283268
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-4.4641, 1.9971, -0.7764>>
					Data.vRotation 										= <<0.0000, 0.0000, 137.3840>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 117
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JACKIE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.7854, -0.0798, -1.7703>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARTNER_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 118
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 5
					Data.iPackedDrawable[2]								= 3
					Data.iPackedTexture[0] 								= 84017152
					Data.iPackedTexture[1] 								= 7
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-26.6306, 6.4662, 2.2297>>
					Data.vRotation 										= <<0.0000, 0.0000, 241.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 119
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 16973828
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.5266, 2.7792, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 265.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 120
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_FEMALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 117440512
					Data.iPackedTexture[1] 								= 8
					Data.iPackedTexture[2] 								= 3
					Data.vPosition 										= <<-12.5266, 2.7792, -1.7903>>
					Data.vRotation 										= <<0.0000, 0.0000, 265.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 121
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_HIPSTER_MALE_04)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_AMBIENT_M_06)
					Data.iLevel 										= g_sMPTunables.iCASINO_NIGHTCLUB_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 67698688
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50462721
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.2854, -4.8688, 2.2247>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bCASINO_NIGHTCLUB_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// Transform the local coords into world coords.
	IF NOT IS_VECTOR_ZERO(Data.vPosition)
		Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_CASINO_NIGHTCLUB_PED_LOCAL_COORDS_BASE_POSITION(), _GET_CASINO_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
	ENDIF
	
	// Transform the local heading into world heading.
	IF (Data.vRotation.z != -1.0)
		Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_CASINO_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_CASINO_NIGHTCLUB_PED_SCRIPT_LAUNCH()
	RETURN TRUE
ENDFUNC

FUNC BOOL _IS_CASINO_NIGHTCLUB_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_CASINO_NIGHTCLUB_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	SWITCH iLayout
		CASE 0
			_SET_CASINO_NIGHTCLUB_PED_DATA_LAYOUT_0(Data, iPed, bSetPedArea)
		BREAK
		CASE 1
			_SET_CASINO_NIGHTCLUB_PED_DATA_LAYOUT_1(Data, iPed, bSetPedArea)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT _GET_CASINO_NIGHTCLUB_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	PRINTLN("[AM_MP_PEDS]  _GET_CASINO_NIGHTCLUB_NETWORK_PED_TOTAL - ped location: ", ePedLocation, " current DJ: ", g_clubMusicData.eActiveDJ, " next DJ: ", g_clubMusicData.eNextDJ, " layout: ", ServerBD.iLayout)
	
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_CASINO_NIGHTCLUB_PED_DATA(ePedLocation, ServerBD, tempData, (iPed+NETWORK_PED_ID_0), ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_CASINO_NIGHTCLUB_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_CASINO_NIGHTCLUB_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_CASINO_NIGHTCLUB_SERVER_PED_LAYOUT_TOTAL()
	RETURN 2
ENDFUNC
FUNC INT _GET_CASINO_NIGHTCLUB_SERVER_PED_LAYOUT()
	RETURN GET_RANDOM_INT_IN_RANGE(0,2)
ENDFUNC

FUNC INT _GET_CASINO_NIGHTCLUB_SERVER_PED_LEVEL()
	RETURN 3
ENDFUNC

PROC _SET_CASINO_NIGHTCLUB_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_CASINO_NIGHTCLUB_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_CASINO_NIGHTCLUB_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_CASINO_NIGHTCLUB_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_CASINO_NIGHTCLUB_NETWORK_PED_TOTAL(ServerBD)
	ENDIF
	PRINTLN("[AM_MP_PEDS] _SET_CASINO_NIGHTCLUB_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_CASINO_NIGHTCLUB_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_CASINO_NIGHTCLUB_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_CASINO_NIGHTCLUB_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_CASINO_NIGHTCLUB_PARENT_PROPERTY(PLAYER_INDEX playerID)
	RETURN IS_PLAYER_IN_CASINO_NIGHTCLUB(playerID)
ENDFUNC

FUNC BOOL _HAS_CASINO_NIGHTCLUB_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL _DOES_CASINO_NIGHTCLUB_ACTIVE_PED_TOTAL_CHANGE()
	RETURN TRUE
ENDFUNC

FUNC INT _GET_CASINO_NIGHTCLUB_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 15	BREAK
		CASE 2	iThreshold = 10	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CASINO_NIGHTCLUB_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_FOOTSTEPS_EVENTS_ENABLED(PedID, FALSE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_PED(NetworkPedID), TRUE)
	
	IF NOT IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(ePedModel)
	
	CLUB_DJS eCurrentDJ = g_clubMusicData.eActiveDJ
	IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
		eCurrentDJ = g_clubMusicData.eNextDJ
	ENDIF
	
	SWITCH eCurrentDJ
		CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB
			SWITCH iPed
				CASE 0	// Rampa
					SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
				BREAK	
				CASE 1	// ME	
					CLEAR_PED_PROP(PedID, ANCHOR_HEAD)
				BREAK	
				CASE 2	// Adam
					SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_RIGHT_WRIST, 0)		
				BREAK	
			ENDSWITCH 
		BREAK
		CASE CLUB_DJ_MOODYMANN
			SWITCH iPed
				CASE 0		
					SET_PED_PROP_INDEX(PedID, ANCHOR_EARS, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
				BREAK
				CASE 1
					SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_EARS, 0)
				BREAK
				CASE 2		
					SET_PED_PROP_INDEX(PedID, ANCHOR_RIGHT_WRIST, 0)
				BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_DJ_PALMS_TRAX
			SWITCH iPed
				CASE 0		
					SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
				BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	SWITCH iPed
		CASE 61	// Patricia
			SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
		BREAK
		CASE 95	// Miguel
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
	ENDSWITCH
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_ALT_MOVEMENT(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(PedID)
	
	SWITCH iPed
		CASE -1
		BREAK
		
//		CASE 0
//			SET_PED_ALTERNATE_MOVEMENT_ANIM(Data.PedID, AAT_IDLE, "random@security_van", "sec_idle")
//			SET_PED_ALTERNATE_MOVEMENT_ANIM(Data.PedID, AAT_WALK, "random@security_van", "sec_walk_calm")
//		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CASINO_NIGHTCLUB_PED_SPEECH_DATA_LAYOUT(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 57	// Kaylee
				SpeechData.fGreetSpeechDistance				= 2.75
				SpeechData.fByeSpeechDistance				= 4.25
				SpeechData.fListenDistance					= 6.10
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 1
				SpeechData.iPedID							= 58	// Bodyguard
				SpeechData.fGreetSpeechDistance				= 2.75
				SpeechData.fByeSpeechDistance				= 4.25
				SpeechData.fListenDistance					= 6.10
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 2
				SpeechData.iPedID							= 61	// Patricia
				SpeechData.fGreetSpeechDistance				= 2.75
				SpeechData.fByeSpeechDistance				= 4.25
				SpeechData.fListenDistance					= 6.10
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 3
				SpeechData.iPedID							= 95	// Miguel
				SpeechData.fGreetSpeechDistance				= 2.75
				SpeechData.fByeSpeechDistance				= 4.25
				SpeechData.fListenDistance					= 6.10
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 4
				SpeechData.iPedID							= 117	// Jackie
				SpeechData.fGreetSpeechDistance				= 2.75
				SpeechData.fByeSpeechDistance				= 4.25
				SpeechData.fListenDistance					= 6.10
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 5
				SpeechData.iPedID							= 13	// Entrance Bouncer
				SpeechData.fGreetSpeechDistance				= 2.5
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
		CASE 1
			_SET_CASINO_NIGHTCLUB_PED_SPEECH_DATA_LAYOUT(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedSpeech)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF g_bDontCrossRunning
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Player is playing Dont Cross The Line")
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE 13		// Entrance Bouncer
			INT iRoomKey
			iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
			IF (iRoomKey != ENUM_TO_INT(CASINO_CLUB_ENRY_HALL))
				PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Ped 13 (Entrance Bouncer) - Bail Reason: Player not in entrance hallway")
				RETURN FALSE
			ENDIF
		BREAK
		CASE 57		// Kaylee
		CASE 58		// Bodyguard
		CASE 61		// Patricia
		CASE 95		// Miguel
		CASE 117	// Jackie
			VECTOR vPedCoords
			VECTOR vPlayerCoords
			vPedCoords = GET_ENTITY_COORDS(PedID)
			vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF NOT ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, vPlayerCoords.z>>, <<0.0, 0.0, vPedCoords.z>>)
				PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - VIP Peds - Bail Reason: Z check failed")
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC PED_SPEECH _GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	PED_SPEECH eSpeech = PED_SPH_INVALID
	
	SWITCH iPed
		CASE 57		// Kaylee
		CASE 58		// Bodyguard
		CASE 61		// Patricia
		CASE 95		// Miguel
		CASE 117	// Jackie
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING						BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE							BREAK
				CASE 2	eSpeech = PED_SPH_PT_LOITER							BREAK
				CASE 3	eSpeech = PED_SPH_PT_CHAMPAGNE						BREAK
				CASE 4	eSpeech = PED_SPH_CT_WHATS_UP						BREAK
				CASE 5	eSpeech = PED_SPH_CT_CASINO_NIGHTCLUB_DJ			BREAK
			ENDSWITCH
		BREAK
		CASE 13	// Entrance Bouncer
			SWITCH iSpeech
				CASE 0	eSpeech = PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_ACCEPT	BREAK
				CASE 1	eSpeech = PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_REJECT	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN eSpeech
ENDFUNC

PROC _GET_CASINO_NIGHTCLUB_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	RESET_PED_CONVO_DATA(convoData)
	
	SWITCH iPed
		CASE 57		convoData.sCharacterVoice = "HS4_KAYLEE"											BREAK
		CASE 58		convoData.sCharacterVoice = "HS4_BODYGUARD1"										BREAK
		CASE 61		convoData.sCharacterVoice = "HS4_PATRICIA"											BREAK
		CASE 95		convoData.sCharacterVoice = "HS4_MIGUEL"											BREAK
		CASE 117	convoData.sCharacterVoice = "HS4_JACKIE"											BREAK
	ENDSWITCH
	
	SWITCH ePedSpeech
		CASE PED_SPH_PT_GREETING						convoData.sRootName = "GENERIC_HI"				BREAK
		CASE PED_SPH_PT_BYE								convoData.sRootName = "GENERIC_BYE"				BREAK
		CASE PED_SPH_PT_LOITER							convoData.sRootName = "LOITER"					BREAK
		CASE PED_SPH_CT_WHATS_UP						convoData.sRootName = "GENERIC_HOWSITGOING"		BREAK
		CASE PED_SPH_PT_CHAMPAGNE						convoData.sRootName = "CHAMP_ORDERED"			BREAK
		CASE PED_SPH_CT_CASINO_NIGHTCLUB_DJ
			CLUB_DJS eCurrentDJ
			eCurrentDJ = g_clubMusicData.eActiveDJ
			IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
				eCurrentDJ = g_clubMusicData.eNextDJ
			ENDIF
			SWITCH eCurrentDJ
				CASE CLUB_DJ_KEINEMUSIK_NIGHTCLUB		convoData.sRootName = "DJ_1"					BREAK
				CASE CLUB_DJ_PALMS_TRAX					convoData.sRootName = "DJ_2"					BREAK
				CASE CLUB_DJ_MOODYMANN					convoData.sRootName = "DJ_3"					BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	SWITCH iPed
		CASE 13	// Entrance Bouncer
			convoData.sCharacterVoice = "S_M_M_BOUNCER_01_BLACK_FULL_01"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_ACCEPT
					convoData.sRootName = "GENERIC_HI"
				BREAK
				CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_REJECT
					convoData.sRootName = "GENERIC_NO"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC PED_SPEECH _GET_CASINO_NIGHTCLUB_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE -1
		BREAK
		
		DEFAULT
			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_CASINO_NIGHTCLUB_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_CASINO_NIGHTCLUB_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════════╡ AMBIENT ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_AMBIENT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_AMBIENT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ LEANING TEXTING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_LEANING_TEXT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_TEXTING_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_LEANING_TEXT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_FEMALE_WALL_BACK_TEXTING_IDLE_A"	BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_LEANING_SMOKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_SMOKING_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_LEANING_SMOKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_SMOKING_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════════╡ HANGOUT ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_HANGOUT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_HANG_OUT_STREET_MALE_C_BASE"				BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_HANGOUT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_HANG_OUT_STREET_FEMALE_HOLD_ARM_IDLE_B"	BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ SMOKING WEED ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_SMOKING_WEED_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_SMOKING_POT_MALE_BASE"						BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_SMOKING_WEED_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_A"													BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════════╡ PHONE ╞═══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_PHONE_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_MOBILE_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_PHONE_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"													BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ MUSCLE FLEX ╞════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_MUSCLE_FLEX_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_MUSCLE_FLEX_ARMS_IN_FRONT_BASE"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ CHECK OUT MIRROR ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_CHECK_OUT_MIRROR_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_WINDOW_SHOP_FEMALE_IDLE_A_BROWSE_B"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════════╡ DRINKING ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DRINKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_DRINKING_BEER_MALE_BASE"					BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DRINKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_DRINKING_BEER_FEMALE_BASE"					BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SITTING DRINKING ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_SITTING_DRINKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_A"			BREAK
		CASE 1	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_B"			BREAK
		CASE 2	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_C"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_SITTING_DRINKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_FEMALE_IDLE_B"		BREAK
		CASE 1	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_FEMALE_IDLE_C"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SITTING AT BAR ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_BAR_MALE_ELBOWS_ON_BAR_IDLE_A"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_BAR_MALE_ELBOWS_ON_BAR_IDLE_C"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_BAR_FEMALE_ELBOWS_ON_BAR_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_F_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"													BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════╕
//╞═══════╡ BOUCER ╞═══════╡
//╘════════════════════════╛

PROC GET_CASINO_NIGHTCLUB_BOUNCER_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, INT iPedID)
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "mini@strip_club@idles@bouncer@base"
			pedAnimData.sAnimClip 		= "base"
			pedAnimData.fBlendInDelta	= REALLY_SLOW_BLEND_IN
			pedAnimData.fBlendOutDelta	= REALLY_SLOW_BLEND_OUT
			// Same for both layouts
			SWITCH iPedID
				CASE 47	// DJ Booth bouncer
					IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_DJ_BOOTH_BOUNCER_WAVE_AWAY_ANIM)
						pedAnimData.sAnimDict 		= "mini@strip_club@idles@bouncer@go_away"
						pedAnimData.sAnimClip 		= "go_away"
						pedAnimData.bRemoveAnimDict = TRUE
					ENDIF
				BREAK
				CASE 13	// Entrance bouncer
					IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_WAVE_AWAY_ANIM)
						pedAnimData.sAnimDict 		= "mini@strip_club@idles@bouncer@go_away"
						pedAnimData.sAnimClip 		= "go_away"
						pedAnimData.bRemoveAnimDict = TRUE
					ELIF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_ACCEPT_ANIM)
						pedAnimData.sAnimDict 		= "mp_cp_welcome_tutgreet"
						pedAnimData.sAnimClip 		= "greet"
						pedAnimData.bRemoveAnimDict = TRUE
					ENDIF
				BREAK
				CASE 70	// Lift bouncer
					IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_LIFT_BOUNCER_WAVE_AWAY_ANIM)
						pedAnimData.sAnimDict 		= "mini@strip_club@idles@bouncer@go_away"
						pedAnimData.sAnimClip 		= "go_away"
						pedAnimData.bRemoveAnimDict = TRUE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞════╡ VIP BAR HANGOUT M 01 ╞═══╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_BAR_HANGOUT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_HANGOUT_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ VIP BAR DRINKING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_MALE_A_BASE"			BREAK
		CASE 1	sAnimClip = "BAR_DRINK_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_MALE_B_BASE"			BREAK
		CASE 1	sAnimClip = "BAR_DRINK_MALE_B_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_MALE_B_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_MALE_B_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_MALE_B_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_M_03_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_MALE_C_BASE"			BREAK
		CASE 1	sAnimClip = "BAR_DRINK_MALE_C_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_MALE_C_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_MALE_C_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_MALE_C_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_F_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_FEMALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ AMBIENT DANCING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CLUB_AMBIENTPEDS@"
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^2"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^2"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^3"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^3"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^4"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^4"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^5"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^5"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^6"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^6"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_M_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_F_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════════╡ FACE DJ ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(BOOL bDancingTransition = FALSE)
	STRING sAnimDict = ""
	
	IF (bDancingTransition)
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_FACEDJ_TRANSITIONS@"
	ELSE
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_FACEDJ@"
	ENDIF
	
	RETURN sAnimDict
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^1"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^1"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^1"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^1"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^1"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^1"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^1"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^1"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^1"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^1"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^1"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^1"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^1"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^1"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^1"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^1"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^1"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^1"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^1"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^1"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^1"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^1"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^1"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^1"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^1"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^1"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^1"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^1"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^1"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^1"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^2"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^2"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^2"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^2"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^2"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^2"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^2"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^2"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^2"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^2"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^2"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^2"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^2"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^2"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^2"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^2"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^2"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^2"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^2"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^2"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^2"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^2"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^2"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^2"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^2"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^2"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^2"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^2"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^2"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^2"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^3"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^3"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^3"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^3"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^3"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^3"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^3"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^3"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^3"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^3"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^3"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^3"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^3"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^3"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^3"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^3"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^3"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^3"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^3"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^3"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^3"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^3"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^3"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^3"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^3"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^3"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^3"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^3"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^3"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^3"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^3"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^3"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^4"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^4"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^4"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^4"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^4"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^4"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^4"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^4"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^4"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^4"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^4"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^4"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^4"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^4"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^4"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^4"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^4"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^4"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^4"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^4"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^4"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^4"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^4"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^4"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^4"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^4"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^4"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^4"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^4"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^4"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^4"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^4"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^5"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^5"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^5"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^5"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^5"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^5"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^5"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^5"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^5"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^5"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^5"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^5"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^5"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^5"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^5"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^5"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^5"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^5"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^5"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^5"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^5"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^5"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^5"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^5"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^5"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^5"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^5"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^5"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^5"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^5"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^5"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^5"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^6"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^6"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^6"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^6"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^6"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^6"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^6"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^6"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^6"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^6"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^6"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^6"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^6"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^6"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^6"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^6"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^6"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^6"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^6"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^6"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^6"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^6"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^6"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^6"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^6"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^6"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^6"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^6"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^6"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^6"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^6"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^6"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^1"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^1"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^1"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^1"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^2"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^2"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^2"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^2"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^3"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^3"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^3"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^3"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^3"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^3"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^3"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^3"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^3"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^3"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^3"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^3"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^3"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^3"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^4"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^4"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^4"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^4"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^4"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^4"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^4"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^4"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^4"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^4"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^4"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^4"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^4"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^4"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^5"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^5"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^5"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^5"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^5"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^5"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^5"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^5"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^5"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^5"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^5"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^5"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^5"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^5"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^6"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^6"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^6"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^6"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^6"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^6"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^6"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^6"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^6"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^6"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^6"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^6"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^6"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^6"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ SINGLE PROP ╞════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(BOOL bDancingTransition = FALSE)
	STRING sAnimDict = ""
	
	IF (bDancingTransition)
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_SINGLE_PROPS_TRANSITIONS@"
	ELSE
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_SINGLE_PROPS@"
	ENDIF
	
	RETURN sAnimDict
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 02	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 03	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^3"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^3"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^3"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^3"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^3"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^3"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 04	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^4"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^4"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^4"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^4"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^4"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^4"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 05	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^5"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^5"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^5"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^5"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^5"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^5"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 06	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^6"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^6"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^6"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^6"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^6"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^6"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 02 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 03 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 04 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 05 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 06 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP A DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPA_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP A DANCING CHILD 01 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_M_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_M_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP B DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═╡ GROUP B DANCING CHILD 01 ╞═╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP C DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═╡ GROUP C DANCING CHILD 01 ╞═╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^2"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v2_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP D DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPD_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP D DANCING CHILD 01 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP D DANCING CHILD 02 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE_^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE_^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE_^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE_^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE_^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE_^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE_^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE_^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE_^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE_^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE_^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE_^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE_^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE_^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE_^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE_^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE_^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE_^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE_^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE_^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE_^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE_^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE_^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE_^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE_^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE_^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE_^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE_^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE_^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP E DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v2_FEMALE^1"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP E DANCING CHILD 01 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v2_FEMALE^2"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP E DANCING CHILD 02 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_M_CHILD_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v2_MALE^1"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_M_CHILD_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════╡ BEACH DANCING ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@BEACHDANCE@"
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 01 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M01"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M01"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M01"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 02 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M02"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M02"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M02"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 03 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M03"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M03"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M03"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 04 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_04_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M04"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M04"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M04"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_04_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_04_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 05 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_05_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M05"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M05"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M05"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M05"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_05_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M05"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M05"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M05"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M05"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M05"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_05_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_05_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_05_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING F 01 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F01"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F01"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F01"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING F 02 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F02"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F02"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F02"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═══╡ CASINO NIGHTCLUB VIP ╞═══╡
//╘══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
	RETURN "ANIM@SCRIPTED@NIGHTCLUB@SPECIAL_PEDS@VIPS@"
ENDFUNC

//╒══════════════════════════════╕
//╞══════════╡ KAYLEE ╞══════════╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_VIP_KAYLEE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0
			sAnimClip 					= "KAYLEE_BASE_KAYLEE"					
			pedAnimData.bLoopingAnim 	= TRUE
			pedAnimData.fBlendOutDelta 	= REALLY_SLOW_BLEND_OUT
		BREAK
		CASE 1
			sAnimClip 					= "KAYLEE_BREAKOUT_01_KAYLEE"
		BREAK
		CASE 2
			sAnimClip 					= "KAYLEE_BREAKOUT_02_KAYLEE"
		BREAK
		CASE 3
			sAnimClip 					= "JACKIE_KAYLEE_ACTION_01_KAYLEE"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞══════════╡ JACKIE ╞══════════╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_VIP_JACKIE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0
			sAnimClip 					= "JACKIE_BASE_JACKIE"
			pedAnimData.bLoopingAnim	= TRUE
			pedAnimData.fBlendOutDelta 	= REALLY_SLOW_BLEND_OUT
		BREAK
		CASE 1
			sAnimClip 					= "JACKIE_BREAKOUT_01_JACKIE"
		BREAK
		CASE 2
			sAnimClip 					= "JACKIE_BREAKOUT_02_JACKIE"
		BREAK
		CASE 3
			sAnimClip 					= "JACKIE_KAYLEE_ACTION_01_JACKIE"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 1
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 4
			sAnimClip 					= "PATRICIA_JACKIE_ACTION_01_JACKIE"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 5
			sAnimClip 					= "PATRICIA_JACKIE_ACTION_02_JACKIE"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═════════╡ PATRICIA ╞═════════╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_VIP_PATRICIA_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0
			sAnimClip 					= "PATRICIA_BASE_PATRICIA"
			pedAnimData.bLoopingAnim 	= TRUE
			pedAnimData.fBlendOutDelta 	= REALLY_SLOW_BLEND_OUT
		BREAK
		CASE 1
			sAnimClip 					= "PATRICIA_BREAKOUT_01_PATRICIA"
		BREAK
		CASE 2
			sAnimClip 					= "PATRICIA_BREAKOUT_02_PATRICIA"
		BREAK
		CASE 3
			sAnimClip 					= "MIGUEL_PATRICIA_ACTION_01_PATRICIA"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 4
			sAnimClip 					= "PATRICIA_JACKIE_ACTION_01_PATRICIA"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 1
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 5
			sAnimClip 					= "PATRICIA_JACKIE_ACTION_02_PATRICIA"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 1
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞══════════╡ MIGUEL ╞══════════╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_VIP_MIGUEL_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	
			sAnimClip 					= "MIGUEL_BASE_MIGUEL"
			pedAnimData.bLoopingAnim 	= TRUE
			pedAnimData.fBlendOutDelta 	= REALLY_SLOW_BLEND_OUT
		BREAK
		CASE 1
			sAnimClip = "MIGUEL_BREAKOUT_01_MIGUEL"
		BREAK
		CASE 2
			sAnimClip = "MIGUEL_BREAKOUT_02_MIGUEL"
		BREAK
		CASE 3
			sAnimClip 					= "MIGUEL_PATRICIA_ACTION_01_MIGUEL"
			pedAnimData.bPartnerAnim 	= TRUE	
			pedAnimData.iPartnerPed		= 1
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 4
			sAnimClip 					= "BODYGUARD_MIGUEL_ACTION_01_MIGUEL"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 5
			sAnimClip 					= "BODYGUARD_MIGUEL_ACTION_02_MIGUEL"
			pedAnimData.bPartnerAnim 	= TRUE
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞══════╡ VIP BODYGUARD ╞═══════╡
//╘══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_VIP_BODYGUARD_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0
			sAnimClip 					= "BODYGUARD_BASE_BODYGUARD"
			pedAnimData.bLoopingAnim 	= TRUE
			pedAnimData.fBlendOutDelta 	= REALLY_SLOW_BLEND_OUT
		BREAK
		CASE 1
			sAnimClip 					= "BODYGUARD_BREAKOUT_01_BODYGUARD"
		BREAK
		CASE 2
			sAnimClip 					= "BODYGUARD_BREAKOUT_02_BODYGUARD"
		BREAK
		CASE 3
			sAnimClip 					= "BODYGUARD_BREAKOUT_02_BODYGUARD"
		BREAK
		CASE 4
			sAnimClip 					= "BODYGUARD_MIGUEL_ACTION_01_BODYGUARD"
			pedAnimData.bPartnerAnim 	= TRUE	
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
		CASE 5
			sAnimClip 					= "BODYGUARD_MIGUEL_ACTION_02_BODYGUARD"
			pedAnimData.bPartnerAnim 	= TRUE	
			pedAnimData.iPartnerPed		= 0
			pedAnimData.fBlendInDelta 	= REALLY_SLOW_BLEND_IN
		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════╡ BEACH DANCING ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@BEACHDANCEPROP@"
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 01 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sMoodAnimDict = "facials@gen_male@base"						pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 02 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 03 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M03"	BREAK
				CASE 4	sAnimClip = "LI_IDLE_D_M03"	BREAK
				CASE 5	sAnimClip = "LI_IDLE_E_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M03"	BREAK
				CASE 4	sAnimClip = "MI_IDLE_D_M03"	BREAK
				CASE 5	sAnimClip = "MI_IDLE_E_M03"	BREAK
				CASE 6	sAnimClip = "MI_IDLE_F_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 04 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_04_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_DROP_M04"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_04_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_04_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING F 01 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F1"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F1"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F1"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F1"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F1"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_DROP_F1"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F1"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F1"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F1"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F1"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F1"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F1"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F1"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F1"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F1"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F1"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F1"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F1"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING F 02 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "TI_IDLE_D_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═══════╡ CLUB DANCING ╞═══════╡
//╘══════════════════════════════╛

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CLUB@"
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING M 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════════════╕
//╞════════╡ DJ_MOODYMANN_DANCER_03 ╞══════╡
//╘════════════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_MOODYMANN_DANCER_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
//				CASE CLUB_MUSIC_INTENSITY_TRANCE
//					SWITCH iClip
//						CASE 0	sAnimClip = "LI_to_TI_F04"			BREAK
//					ENDSWITCH
//					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
//					SWITCH iRand
//						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
//						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
//						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
//					ENDSWITCH
//				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
//				CASE CLUB_MUSIC_INTENSITY_HIGH
//				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
//					SWITCH iClip
//						CASE 0	sAnimClip = "MI_to_HI_F04"			BREAK
//					ENDSWITCH
//					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
//					SWITCH iRand
//						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
//						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
//						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
//					ENDSWITCH
//				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
//				CASE CLUB_MUSIC_INTENSITY_MEDIUM
//					SWITCH iClip
//						CASE 0	sAnimClip = "HI_to_MI_F04"			BREAK
//					ENDSWITCH
//					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
//					SWITCH iRand
//						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
//						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
//					ENDSWITCH
//				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
//				CASE CLUB_MUSIC_INTENSITY_LOW
//					SWITCH iClip
//						CASE 0	sAnimClip = "TI_to_LI_F04"			BREAK
//					ENDSWITCH
//					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
//					SWITCH iRand
//						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
//						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
//						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
//					ENDSWITCH
//				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_MOODYMANN_DANCER_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F04"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F04"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F04"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F04"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F04"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F04"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_IDLE_D_F04"	BREAK
			ENDSWITCH
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F04"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F04"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_MOODYMANN_DANCER_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_MOODYMANN_DANCER_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_MOODYMANN_DANCER_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING M 02 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING M 03 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING F 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F01"	BREAK
			ENDSWITCH
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING F 02 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "LI_IDLE_D_F02"	BREAK
				CASE 5	sAnimClip = "LI_IDLE_E_F02"	BREAK
				CASE 6	sAnimClip = "LI_IDLE_F_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "MI_IDLE_D_F02"	BREAK
				CASE 5	sAnimClip = "MI_IDLE_E_F02"	BREAK
				CASE 6	sAnimClip = "MI_IDLE_F_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "HI_IDLE_E_F02"	BREAK
				CASE 5	sAnimClip = "HI_IDLE_F_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "TI_IDLE_D_F02"	BREAK
				CASE 5	sAnimClip = "TI_IDLE_E_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING F 03 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0  pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F03"	BREAK
				CASE 4	sAnimClip = "LI_IDLE_D_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛

PROC _GET_CASINO_NIGHTCLUB_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_AMBIENT_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@MALE_C@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_AMBIENT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_AMBIENT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_AMBIENT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_TEXTING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_LEANING_TEXT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_LEANING_TEXT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_LEANING_SMOKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_SMOKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_LEANING_SMOKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_HANGOUT_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_HANGOUT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_HANGOUT_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_HANGOUT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SMOKING_WEED_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SMOKING_WEED_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SMOKING_WEED_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@FEMALE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SMOKING_WEED_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_ON_PHONE_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_PHONE_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_ON_PHONE_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_PHONE_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_MUSCLE_FLEX
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_MUSCLE_FLEX_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_CHECK_OUT_MIRROR
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_CHECK_OUT_MIRROR_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_DRINKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DRINKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_DRINKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DRINKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_DRINKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SITTING_DRINKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_DRINKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SITTING_DRINKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_M_02
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_F_02
			pedAnimData.sAnimDict		= "AMB@PROP_HUMAN_SEAT_BAR@FEMALE@ELBOWS_ON_BAR@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_SITTING_AT_BAR_F_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_GUARD_STANDING
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			GET_CASINO_NIGHTCLUB_BOUNCER_ANIM_CLIP(pedAnimData, iClip, iPedID)
		BREAK
		CASE PED_ACT_VIP_BAR_HANGOUT_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_VIP_BAR_HANGOUT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_03
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_M_03_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_VIP_BAR_DRINKING_F_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_06
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_M_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_06
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_AMBIENT_F_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_06
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_M_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_06
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_FACE_DJ_F_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_06
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_06
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPA_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPA@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPA_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPA_M_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPA@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPB@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPB@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPC@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPC@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPD_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPD_M_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPD_M_CHILD_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPE_M_CHILD_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_04_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_05
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_M_05_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_F_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_F_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_F_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_04
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_M_04_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_F_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_F_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_BEACH_PROP_F_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_M_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_M_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_M_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_M_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_MOODYMANN_DANCER_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_F_01
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_F_02
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_F_03
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_CLUB_F_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DJ_SOLOMUN
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB@DJS@SOLOMUN@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_SOL")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ADAM
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@kmuzk@adam_port@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ME
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@kmuzk@andme@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_RAMPA
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@kmuzk@rampa@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_PALMS_TRAX
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_ptrax@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "PTRX_", "_DJ_PTRAX")
		BREAK
		CASE PED_ACT_DJ_MOODYMANN
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_moodm@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_01
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_moodm@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_DANCER_ANIM_CLIP_BASE(iClip, 1), "", "_dancer_a")
		BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_02	
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_moodm@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_DANCER_ANIM_CLIP_BASE(iClip, 2), "", "_dancer_b")
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PARTNER_SYNC_SCENE)
			
			pedAnimData.sAnimClip 		= _GET_CASINO_NIGHTCLUB_VIP_KAYLEE_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PARTNER_SYNC_SCENE)
			
			pedAnimData.sAnimClip 		= _GET_CASINO_NIGHTCLUB_VIP_JACKIE_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PARTNER_SYNC_SCENE)
			
			pedAnimData.sAnimClip 		= _GET_CASINO_NIGHTCLUB_VIP_PATRICIA_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PARTNER_SYNC_SCENE)
			
			pedAnimData.sAnimClip 		= _GET_CASINO_NIGHTCLUB_VIP_MIGUEL_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_BODYGUARD
			pedAnimData.sAnimDict 		= GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PARTNER_SYNC_SCENE)
			
			pedAnimData.sAnimClip 		= _GET_CASINO_NIGHTCLUB_VIP_BODYGUARD_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CLIPBOARD
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_CLIPBOARD@MALE@BASE"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
	ENDSWITCH
	
	IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(eActivity)
		GET_DANCING_CLUB_FACE_MOOD_ANIM_CLIP(pedAnimData, ePedMusicIntensity, bDancingTransition)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROP ANIM DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_CASINO_NIGHTCLUB_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_DANCING_GROUPA_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPA@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 6
								IF NOT (bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPB@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 5
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v2_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v2_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_17_v1_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_17_v1_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
							CASE 7
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_15_v2_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_15_v2_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK	
				CASE 1	//ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_BA_Prop_Battle_Vape_01^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_BA_Prop_Battle_Vape_01^HEEL"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPC@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 4
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v1_Prop_NPC_Phone"
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone"
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_17_v1_Prop_NPC_Phone"
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE 1 //ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 1
								IF (bDancingTransition)
									IF (eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
									OR (eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
										pedPropAnimData.sPropAnimClip = "Trans_Dance_Crowd_MI_to_LI_11_v1_BA_Prop_Battle_Vape_01"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			SWITCH iProp
				CASE 0	//ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_15_v1BA_Prop_Battle_Vape_01^flat"
//									IF (bHeeledPed)
//										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_15_v1BA_Prop_Battle_Vape_01^heel"
//									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1BA_Prop_Battle_Vape_01^flat^1"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1BA_Prop_Battle_Vape_01^heel^1"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 5
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 2
								IF (bDancingTransition)
									IF (eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
									OR (eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
										pedPropAnimData.sPropAnimClip = "Trans_Dance_Crowd_MI_to_LI_11_v1_Prop_NPC_Phone^flat"
										IF (bHeeledPed)
											pedPropAnimData.sPropAnimClip = "Trans_Dance_Crowd_MI_to_LI_11_v1_Prop_NPC_Phone^heel"
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 4
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_11_v2_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_11_v2_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 7
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v2_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v2_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 9
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_17_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_17_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 4
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 9
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_SOLOMUN
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB@DJS@SOLOMUN@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "_HEADPHONES")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_WINEGLASS")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ADAM
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@adam_port@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_bottle")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_tumbler")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ME
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@andme@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_bottle")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_COCONUTDRINK")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_RAMPA
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@rampa@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_tumbler")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_T_BOTTLE")
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_S_GLASS_01")
				BREAK
				CASE 4
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_S_GLASS_02")
				BREAK
				CASE 5
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_S_GLASS_03")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_PALMS_TRAX
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@dj_ptrax@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "PTRX_", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "PTRX_", "_BEER_CAN")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_MOODYMANN
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@dj_moodm@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_turntable_left")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_turntable_right")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_lp_left")
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_lp_right")
				BREAK
				CASE 4
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_bag")
				BREAK
				CASE 5
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_TUMBLER")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_KAYLEE
			pedPropAnimData.sPropAnimDict = GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			SWITCH iProp
				CASE 0	// PROP_AMB_PHONE
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "KAYLEE_BASE_KAYLEE's_Phone"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "KAYLEE_BREAKOUT_01_KAYLEE's_Phone"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "KAYLEE_BREAKOUT_02_KAYLEE's_Phone"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "JACKIE_KAYLEE_ACTION_01_KAYLEE's_Phone"				BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_JACKIE
			pedPropAnimData.sPropAnimDict = GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			SWITCH iProp
				CASE 0	// P_CS_SHOT_GLASS_2_S
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "JACKIE_BASE_JACKIE's_Glass"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "JACKIE_BREAKOUT_01_JACKIE's_Glass"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "JACKIE_BREAKOUT_02_JACKIE's_Glass"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "JACKIE_KAYLEE_ACTION_01_JACKIE's_Glass"				BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_01_JACKIE's_Glass"				BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_02_JACKIE's_Glass"				BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_PATRICIA
			pedPropAnimData.sPropAnimDict = GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			SWITCH iProp
				CASE 0	// P_CS_SHOT_GLASS_2_S
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "PATRICIA_BASE_PATRICIA's_Glass"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "PATRICIA_BREAKOUT_01_PATRICIA's_Glass"					BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "PATRICIA_BREAKOUT_02_PATRICIA's_Glass"					BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "MIGUEL_PATRICIA_ACTION_01_PATRICIA's_Glass"			BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_01_PATRICIA's_Glass"			BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_02_PATRICIA's_Glass"			BREAK
					ENDSWITCH
				BREAK
				CASE 1	// Ba_prop_battle_champ_open_03
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "PATRICIA_BASE_CHAMPAGNE_BOTTLE"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "PATRICIA_BREAKOUT_01_CHAMPAGNE_BOTTLE"					BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "PATRICIA_BREAKOUT_02_CHAMPAGNE_BOTTLE"					BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "MIGUEL_PATRICIA_ACTION_01_CHAMPAGNE_BOTTLE"			BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_01_CHAMPAGNE_BOTTLE"			BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_02_CHAMPAGNE_BOTTLE"			BREAK
					ENDSWITCH
				BREAK
				CASE 2	// h4_prop_h4_T_bottle_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "PATRICIA_BASE_h4_prop_h4_T_bottle_01a"					BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "PATRICIA_BREAKOUT_01_h4_prop_h4_T_bottle_01a"			BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "PATRICIA_BREAKOUT_02_h4_prop_h4_T_bottle_01a"			BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "MIGUEL_PATRICIA_ACTION_01_h4_prop_h4_T_bottle_01a"		BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_01_h4_prop_h4_T_bottle_01a"		BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "PATRICIA_JACKIE_ACTION_02_h4_prop_h4_T_bottle_01a"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CASINO_NIGHTCLUB_VIP_MIGUEL
			pedPropAnimData.sPropAnimDict = GET_CASINO_NIGHTCLUB_VIP_ANIM_DICT()
			SWITCH iProp
				CASE 0	// PROP_AMB_PHONE
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "MIGUEL_BASE_MIGUEL's_Phone"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "MIGUEL_BREAKOUT_01_MIGUEL's_Phone"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "MIGUEL_BREAKOUT_02_MIGUEL's_Phone"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "MIGUEL_PATRICIA_ACTION_01_MIGUEL's_Phone"				BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "BODYGUARD_MIGUEL_ACTION_01_MIGUEL's_Phone"				BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "BODYGUARD_MIGUEL_ACTION_02_MIGUEL's_Phone"				BREAK
					ENDSWITCH
				BREAK
				CASE 1	// P_CS_SHOT_GLASS_2_S
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "MIGUEL_BASE_MIGUEL's_Glass"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "MIGUEL_BREAKOUT_01_MIGUEL's_Glass"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "MIGUEL_BREAKOUT_02_MIGUEL's_Glass"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "MIGUEL_PATRICIA_ACTION_01_MIGUEL's_Glass"				BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "BODYGUARD_MIGUEL_ACTION_01_MIGUEL's_Glass"				BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "BODYGUARD_MIGUEL_ACTION_02_MIGUEL's_Glass"				BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CULLING DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_CASINO_NIGHTCLUB_PED_CULLING_LOCATE_DATA(PED_AREAS ePedArea, PED_CULLING_LOCATE_DATA &pedCullingLocateData)
	SWITCH ePedArea
		CASE PED_AREA_ONE
			pedCullingLocateData.vLocateVectorOne = <<-1025.470337,-221.341888,36.946171>>
			pedCullingLocateData.vLocateVectorTwo = <<-1023.535217,-217.722595,39.422211>>
			pedCullingLocateData.fLocateWidth = 1.250000
		BREAK
		CASE PED_AREA_TWO
			pedCullingLocateData.vLocateVectorOne = <<-1026.587158,-220.765900,36.944595>>
			pedCullingLocateData.vLocateVectorTwo = <<-1024.646851,-217.130463,39.422775>>
			pedCullingLocateData.fLocateWidth = 1.250000
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ HEAD TRACKING DATA ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CASINO_NIGHTCLUB_PED_HEAD_TRACKING_DATA_LAYOUT_0(HEAD_TRACKING_DATA &HeadtrackingData, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				HeadtrackingData.iPedID							= 6
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 1
				HeadtrackingData.iPedID							= 13
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 2
				HeadtrackingData.iPedID							= 47
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 3
				HeadtrackingData.iPedID							= 70
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_HEAD_TRACKING_DATA_LAYOUT_1(HEAD_TRACKING_DATA &HeadtrackingData, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				HeadtrackingData.iPedID							= 6
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 1
				HeadtrackingData.iPedID							= 13
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 2
				HeadtrackingData.iPedID							= 47
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 3
				HeadtrackingData.iPedID							= 70
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 4.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_HEAD_TRACKING_DATA(HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_CASINO_NIGHTCLUB_PED_HEAD_TRACKING_DATA_LAYOUT_0(HeadtrackingData, iArrayID, iHeadTrackingPedID, bNetworkPed)
		BREAK
		CASE 1
			_SET_CASINO_NIGHTCLUB_PED_HEAD_TRACKING_DATA_LAYOUT_1(HeadtrackingData, iArrayID, iHeadTrackingPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ DANCING DATA ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _DOES_CASINO_NIGHTCLUB_PED_LOCATION_USE_DANCING_PEDS()
	RETURN TRUE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PARENT PED DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CASINO_NIGHTCLUB_PED_PARENT_DATA_LAYOUT_0(PARENT_PED_DATA &ParentPedData, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				ParentPedData.iParentID							= 58	// VIP Bodyguard
				ParentPedData.iChildID[0]						= 95	// Miguel
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 1
				ParentPedData.iParentID							= 95	// Miguel
				ParentPedData.iChildID[0]						= 58	// VIP Bodyguard
				ParentPedData.iChildID[1]						= 61	// Patricia
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 2
				ParentPedData.iParentID							= 61	// Patricia
				ParentPedData.iChildID[0]						= 95	// Miguel
				ParentPedData.iChildID[1]						= 117	// Jackie
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 3
				ParentPedData.iParentID							= 117	// Jackie
				ParentPedData.iChildID[0]						= 61	// Patricia
				ParentPedData.iChildID[1]						= 57	// Kaylee
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 4
				ParentPedData.iParentID							= 57	// Kaylee
				ParentPedData.iChildID[0]						= 117	// Jackie
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 5
				ParentPedData.iParentID							= 5
				ParentPedData.iChildID[0]						= 40
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 6
				ParentPedData.iParentID							= 8
				ParentPedData.iChildID[0]						= 85
				ParentPedData.iChildID[1]						= 84
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 7
				ParentPedData.iParentID							= 114
				ParentPedData.iChildID[0]						= 35
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 8
				ParentPedData.iParentID							= 36
				ParentPedData.iChildID[0]						= 32
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 9
				ParentPedData.iParentID							= 86
				ParentPedData.iChildID[0]						= 87
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 10
				ParentPedData.iParentID							= 80
				ParentPedData.iChildID[0]						= 105
				ParentPedData.iChildID[1]						= 64
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_PARENT_DATA_LAYOUT_1(PARENT_PED_DATA &ParentPedData, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				ParentPedData.iParentID							= 58	// VIP Bodyguard
				ParentPedData.iChildID[0]						= 95	// Miguel
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 1
				ParentPedData.iParentID							= 95	// Miguel
				ParentPedData.iChildID[0]						= 58	// VIP Bodyguard
				ParentPedData.iChildID[1]						= 61	// Patricia
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 2
				ParentPedData.iParentID							= 61	// Patricia
				ParentPedData.iChildID[0]						= 95	// Miguel
				ParentPedData.iChildID[1]						= 117	// Jackie
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 3
				ParentPedData.iParentID							= 117	// Jackie
				ParentPedData.iChildID[0]						= 61	// Patricia
				ParentPedData.iChildID[1]						= 57	// Kaylee
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 4
				ParentPedData.iParentID							= 57	// Kaylee
				ParentPedData.iChildID[0]						= 117	// Jackie
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 5
				ParentPedData.iParentID							= 35
				ParentPedData.iChildID[0]						= 39
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 6
				ParentPedData.iParentID							= 62
				ParentPedData.iChildID[0]						= 69
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 7
				ParentPedData.iParentID							= 119
				ParentPedData.iChildID[0]						= 120
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 8
				ParentPedData.iParentID							= 89
				ParentPedData.iChildID[0]						= 90
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 9
				ParentPedData.iParentID							= 7
				ParentPedData.iChildID[0]						= 94
				ParentPedData.iChildID[1]						= 108
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 10
				ParentPedData.iParentID							= 107
				ParentPedData.iChildID[0]						= 111
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 11
				ParentPedData.iParentID							= 113
				ParentPedData.iChildID[0]						= 112
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 12
				ParentPedData.iParentID							= 19
				ParentPedData.iChildID[0]						= 48
				ParentPedData.iChildID[1]						= 41
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CASINO_NIGHTCLUB_PED_PARENT_DATA(PARENT_PED_DATA &ParentPedData, INT iLayout, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_CASINO_NIGHTCLUB_PED_PARENT_DATA_LAYOUT_0(ParentPedData, iArrayID, iParentPedID, bNetworkPed)
		BREAK
		CASE 1
			_SET_CASINO_NIGHTCLUB_PED_PARENT_DATA_LAYOUT_1(ParentPedData, iArrayID, iParentPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENE PEDS ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_HIDE_CASINO_NIGHTCLUB_CUTSCENE_PED(SCRIPT_PED_DATA &LocalData, INT iLayout, INT iPed)
	
	// DJ Peds - Switch Cutscene
	IF IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_DJ_SWITCH_ACTIVE)
		SWITCH iPed
			CASE 0
			CASE 1
			CASE 2
			CASE 3
				RETURN TRUE
			BREAK
		ENDSWITCH
		RETURN FALSE
	ELSE
		// VIP Peds - Intro Cutscene
		SWITCH iLayout
			CASE 0
				SWITCH iPed
					CASE 6
					CASE 51
					CASE 56
					CASE 57
					CASE 58
					CASE 61
					CASE 95
					CASE 117
						RETURN TRUE
					BREAK
				ENDSWITCH
			BREAK
			CASE 1
				SWITCH iPed
					CASE 6
					CASE 57
					CASE 58
					CASE 61
					CASE 95
					CASE 117
						RETURN TRUE
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_CASINO_NIGHTCLUB_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_CASINO_NIGHTCLUB_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_CASINO_NIGHTCLUB_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_CASINO_NIGHTCLUB_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_CASINO_NIGHTCLUB_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal = &_GET_CASINO_NIGHTCLUB_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_CASINO_NIGHTCLUB_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_CASINO_NIGHTCLUB_SERVER_PED_LEVEL
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_CASINO_NIGHTCLUB_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_CASINO_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_CASINO_NIGHTCLUB_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_CASINO_NIGHTCLUB_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_CASINO_NIGHTCLUB_PED_ANIM_DATA
		BREAK
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_CASINO_NIGHTCLUB_PED_PROP_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_CASINO_NIGHTCLUB_PARENT_PROPERTY
		BREAK
		CASE E_DOES_ACTIVE_PED_TOTAL_CHANGE
			interface.returnDoesActivePedTotalChange = &_DOES_CASINO_NIGHTCLUB_ACTIVE_PED_TOTAL_CHANGE
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_CASINO_NIGHTCLUB_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_CASINO_NIGHTCLUB_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_CASINO_NIGHTCLUB_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_CASINO_NIGHTCLUB_PED_PROP_INDEXES
		BREAK
		CASE E_SET_PED_ALT_MOVEMENT
			interface.setPedAltMovement = &_SET_CASINO_NIGHTCLUB_PED_ALT_MOVEMENT
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_CASINO_NIGHTCLUB_PED_BEEN_CREATED
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_CASINO_NIGHTCLUB_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_CASINO_NIGHTCLUB_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_CASINO_NIGHTCLUB_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_CASINO_NIGHTCLUB_PED_CONVO_DATA
		BREAK
		
		// Ped Culling
		CASE E_GET_PED_CULLING_LOCATE_DATA
			interface.getPedCullingLocateData = &_GET_CASINO_NIGHTCLUB_PED_CULLING_LOCATE_DATA
		BREAK
		
		// Head Tracking
		CASE E_SET_PED_HEAD_TRACKING_DATA
			interface.setPedHeadTrackingData = &_SET_CASINO_NIGHTCLUB_PED_HEAD_TRACKING_DATA
		BREAK
		
		// Dancing Data
		CASE E_DOES_PED_LOCATION_USE_DANCING_PEDS
			interface.returnDoesPedLocationUseDancingPeds = &_DOES_CASINO_NIGHTCLUB_PED_LOCATION_USE_DANCING_PEDS
		BREAK
		
		// Parent Ped Data
		CASE E_SET_PED_PARENT_DATA
			interface.setPedParentData = &_SET_CASINO_NIGHTCLUB_PED_PARENT_DATA
		BREAK
		
		// Cutscene Peds
		CASE E_SHOULD_HIDE_CUTSCENE_PED
			interface.returnShouldHideCutscenePed = &_SHOULD_HIDE_CASINO_NIGHTCLUB_CUTSCENE_PED
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_CASINO_NIGHTCLUB
