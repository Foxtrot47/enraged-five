//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_WAREHOUSE.sch																					//
// Description: Functions to implement from net_peds_base.sch and look up tables to return WAREHOUSE ped data.			//
// Written by:  Jan Mencfel																								//
// Date:  		06/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_DLC_1_2022
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"
USING "net_simple_interior_warehouse.sch"

FUNC VECTOR _GET_WAREHOUSE_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN << 0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT _GET_WAREHOUSE_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0.0
ENDFUNC

PROC GET_PED_LOOK_FOR_WAREHOUSE(PEDS_DATA &Data)
	INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
	SWITCH GET_SAVE_SLOT_FOR_WAREHOUSE_FOR_PLAYER(iWarehouse, GET_WAREHOUSE_OWNER(iWarehouse))
		CASE 0
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_WAREHOUSE_BOSS)
			Data.iPackedDrawable[0]								= 0
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
		BREAK
		CASE 1
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_WAREHOUSE_M)
			Data.iPackedDrawable[0]								= 16777217
			Data.iPackedDrawable[1]								= 65793
			Data.iPackedDrawable[2]								= 16777216
			Data.iPackedTexture[0] 								= 16777218
			Data.iPackedTexture[1] 								= 65792
			Data.iPackedTexture[2] 								= 1
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
		BREAK
		CASE 2
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_WAREHOUSE_F)
			Data.iPackedDrawable[0]								= 0
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 1
			Data.iPackedTexture[0] 								= 1
			Data.iPackedTexture[1] 								= 65539
			Data.iPackedTexture[2] 								= 256
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
		BREAK
		CASE 3
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_WAREHOUSE_M)
			Data.iPackedDrawable[0]								= 0
			Data.iPackedDrawable[1]								= 256
			Data.iPackedDrawable[2]								= 16777216
			Data.iPackedTexture[0] 								= 16777216
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
		BREAK
		CASE 4
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_WAREHOUSE_F)
			Data.iPackedDrawable[0]								= 196609
			Data.iPackedDrawable[1]								= 65537
			Data.iPackedDrawable[2]								= 16777217
			Data.iPackedTexture[0] 								= 2
			Data.iPackedTexture[1] 								= 65793
			Data.iPackedTexture[2] 								= 16777216
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_WAREHOUSE_PED_FEMALE()
	INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
	SWITCH GET_SAVE_SLOT_FOR_WAREHOUSE_FOR_PLAYER(iWarehouse, GET_WAREHOUSE_OWNER(iWarehouse))
		CASE 0		
		CASE 2	
		CASE 4
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_SMALL_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO		
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= <<1099.822, -3097.927, -40.02092>>
					Data.vRotation 										= <<0.0000, 0.0000, 156.000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_SMALL_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= <<1103.173, -3101.958, -39.02092>>
					Data.vRotation 										= <<0.0000, 0.0000, 24.120>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_SMALL_2(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= <<1093.597, -3098.03, -40.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, -172.500>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC
PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= << 1071.233, -3097.829, -39.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, 134.750>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO					
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= <<1071.512, -3107.35, -40.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, 50.250>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_2(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= <<1060.139, -3098.929, -40.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.00>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_LARGE_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= << 1025.093, -3098.251, -39.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, 104.000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_LARGE_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= << 1001.805, -3092.803, -40.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, -166.860>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC
PROC _SET_WAREHOUSE_PED_DATA_LAYOUT_LARGE_2(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					IF IS_WAREHOUSE_PED_FEMALE()
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
					ELSE
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
					ENDIF
					Data.iLevel 										= 2
					Data.vPosition 										= << 997.8175, -3109.485, -39.00166>>
					Data.vRotation 										= <<0.0000, 0.0000, -53.740>>
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_PED_LOOK_FOR_WAREHOUSE(Data)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_WAREHOUSE_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(ePedModel)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(iPed)
	INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
	SWITCH GET_SAVE_SLOT_FOR_WAREHOUSE_FOR_PLAYER(iWarehouse, GET_WAREHOUSE_OWNER(iWarehouse))		
		CASE 0
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE 1
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE 2
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 1)
		BREAK
		CASE 3
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 2, 0)
		BREAK
		CASE 4
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 1)
		BREAK
	ENDSWITCH 
ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_WAREHOUSE_PED_SCRIPT_LAUNCH()
	RETURN IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_WAREHOUSE_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_WAREHOUSE_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)

	EWarehouseSize Size = GET_WAREHOUSE_SIZE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
	//WAREHOUSE LAYOUT A
	SWITCH Size
		CASE eWarehouseSmall
			SWITCH iLayout
				CASE 0
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT SMALL WAREHOUSE 0, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_SMALL_0(Data, iPed, bSetPedArea)
				BREAK
				CASE 1
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT SMALL WAREHOUSE 1, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_SMALL_1(Data, iPed, bSetPedArea)
				BREAK
				CASE 2
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT SMALL WAREHOUSE 2, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_SMALL_2(Data, iPed, bSetPedArea)
				BREAK
			ENDSWITCH
		BREAK
		CASE eWarehouseMedium
			SWITCH iLayout
				CASE 0
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT MEDIUM WAREHOUSE 0, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_0(Data, iPed, bSetPedArea)
				BREAK
				CASE 1
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT MEDIUM WAREHOUSE 1, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_1(Data, iPed, bSetPedArea)
				BREAK
				CASE 2
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT MEDIUM WAREHOUSE 2, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_2(Data, iPed, bSetPedArea)
				BREAK
			ENDSWITCH
		BREAK
		CASE eWarehouseLarge
			SWITCH iLayout
				CASE 0
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT LARGE WAREHOUSE 0, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_LARGE_0(Data, iPed, bSetPedArea)
				BREAK
				CASE 1
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT LARGE WAREHOUSE 1, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_LARGE_1(Data, iPed, bSetPedArea)
				BREAK
				CASE 2
					PRINTLN("_SET_WAREHOUSE_PED_DATA   LAYOUT LARGE WAREHOUSE 2, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_LARGE_2(Data, iPed, bSetPedArea)
				BREAK
			ENDSWITCH
		BREAK
		DEFAULT
		SWITCH iLayout
				CASE 4
				DEFAULT
					PRINTLN("_SET_WAREHOUSE_PED_DATA   USING DEFAULT LAYOUT, PED: ", iPed)	
					_SET_WAREHOUSE_PED_DATA_LAYOUT_MEDIUM_0(Data, iPed, bSetPedArea)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	//APPLIES TO ALL 
	SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RESETTABLE_PED)
	IF g_iPedLayout = -1
	OR IS_WAREHOUSE_PED_SOURCING_CARGO(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
	ENDIF

		
ENDPROC

FUNC INT _GET_WAREHOUSE_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0
ENDFUNC

FUNC INT _GET_WAREHOUSE_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ePedLocation)
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_WAREHOUSE_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_WAREHOUSE_SERVER_PED_LAYOUT_TOTAL()
	RETURN 4
ENDFUNC

FUNC INT _GET_WAREHOUSE_SERVER_PED_LAYOUT()

	IF IS_WAREHOUSE_PED_SOURCING_CARGO(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
		RETURN 3
	ENDIF
	
	//Default to location 0 for the intro scene
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
		IF NOT HAS_PLAYER_BD_HAS_VIEWED_WAREHOUSE_INTRO_SCENE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
			RETURN 0
		ENDIF
	ELIF NOT HAS_PLAYER_BD_HAS_VIEWED_WAREHOUSE_INTRO_SCENE(PLAYER_ID())
		RETURN 0
	ENDIF
	
	
	RETURN GET_RANDOM_INT_IN_RANGE(0, _GET_WAREHOUSE_SERVER_PED_LAYOUT_TOTAL()-1)
ENDFUNC

FUNC INT _GET_WAREHOUSE_SERVER_PED_LEVEL()
	RETURN 2  // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

PROC _SET_WAREHOUSE_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_WAREHOUSE_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_WAREHOUSE_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_WAREHOUSE_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_WAREHOUSE_NETWORK_PED_TOTAL(ServerBD)
		PRINTLN("[AM_MP_PEDS][NET_PEDS_WAREHOUSE][HOST] _SET_WAREHOUSE_PED_SERVER_DATA RETURNED ", ServerBD.iLayout)
	ENDIF
	
	
	IF (ServerBD.iLayout!=-1)
		EWarehouseSize Size = GET_WAREHOUSE_SIZE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
		IF ServerBD.iLayout = 3
			g_iPedLayout = -1
		ELSE
			SWITCH Size
				CASE eWarehouseSmall
					g_iPedLayout = ServerBD.iLayout
				BREAK
				CASE eWarehouseMedium
					g_iPedLayout = ServerBD.iLayout + 3
				BREAK
				CASE eWarehouseLarge
					g_iPedLayout = ServerBD.iLayout + 6
				BREAK
				DEFAULT
					g_iPedLayout = ServerBD.iLayout
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		g_iPedLayout = -1
	ENDIF
	
	PRINTLN("[AM_MP_PEDS] _SET_WAREHOUSE_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_WAREHOUSE_PED_SERVER_DATA - g_iPedLayout: ", g_iPedLayout)
	PRINTLN("[AM_MP_PEDS] _SET_WAREHOUSE_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_WAREHOUSE_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_WAREHOUSE_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_WAREHOUSE_PARENT_PROPERTY(PLAYER_INDEX playerID)
PRINTLN("[AM_MP_PEDS] _IS_PLAYER_IN_WAREHOUSE_PARENT_PROPERTY")
	RETURN IS_PLAYER_IN_WAREHOUSE(playerID)
ENDFUNC

FUNC BOOL _HAS_WAREHOUSE_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_WAREHOUSE_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_UseKinematicPhysics, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_Avoidance_Ignore_All, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	
	STOP_PED_SPEAKING_SYNCED(PedID, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_WAREHOUSE_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
	
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_WAREHOUSE_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0
				SpeechData.fGreetSpeechDistance				= 3.0
				SpeechData.fByeSpeechDistance				= 4.5
				SpeechData.fListenDistance					= 6.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
		
ENDPROC

FUNC BOOL _CAN_WAREHOUSE_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH eSpeech)
	UNUSED_PARAMETER(eSpeech)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_PLAY_SPEECH - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	

	SWITCH iPed
		CASE -1
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
FUNC PED_SPEECH _GET_WAREHOUSE_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	UNUSED_PARAMETER(iPed)
	PED_SPEECH eSpeech = PED_SPH_INVALID	
	
	INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
	
	IF  iWarehouse = ciW_Invalid
		PRINTLN("[AM_MP_PEDS][WAREHOUSE] _GET_WAREHOUSE_PED_SPEECH_TYPE   iWarehouse = ciW_Invalid      RETURNING PED_SPH_INVALID")
		RETURN PED_SPH_INVALID
	ENDIF
	
	PLAYER_INDEX owner = GET_WAREHOUSE_OWNER(iWarehouse)
	
	IF owner = INVALID_PLAYER_INDEX()
		PRINTLN("[AM_MP_PEDS][WAREHOUSE] _GET_WAREHOUSE_PED_SPEECH_TYPE   owner = INVALID_PLAYER_INDEX()     RETURNING PED_SPH_INVALID")
		RETURN PED_SPH_INVALID
	ENDIF
	
	SWITCH GET_SAVE_SLOT_FOR_WAREHOUSE_FOR_PLAYER(iWarehouse, owner)
		CASE 0			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
				CASE 4 	eSpeech = PED_SPH_PT_SOURCE_CARGO_POSITIVE							BREAK
				CASE 5 	eSpeech = PED_SPH_PT_SOURCE_CARGO_NEGATIVE							BREAK
			ENDSWITCH
		BREAK
		CASE 1		
		CASE 2
		CASE 3
		CASE 4
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2 	eSpeech = PED_SPH_PT_SOURCE_CARGO_POSITIVE							BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN eSpeech
ENDFUNC

PROC _GET_WAREHOUSE_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	UNUSED_PARAMETER(iPed)
	
	RESET_PED_CONVO_DATA(convoData)
	
	INT iRandSpeech
	
	INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
	SWITCH GET_SAVE_SLOT_FOR_WAREHOUSE_FOR_PLAYER(iWarehouse, GET_WAREHOUSE_OWNER(iWarehouse))
		//BOSS
		CASE 0
			convoData.sCharacterVoice = "SM2_WBOSS1"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 3)
					convoData.sRootName = "GENERIC_HI"
					IF (iRandSpeech = 1)
						convoData.sRootName = "GENERIC_HOWSITGOING"
					ELIF (iRandSpeech = 2)
						IF IS_PLAYER_FEMALE()
							convoData.sRootName = "GREET_OWNER_FEMALE"
						ENDIF
						convoData.sRootName = "GREET_OWNER_MALE"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "GENERIC_BYE"
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER				
					convoData.sRootName = "IDLE"
				BREAK
				CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE			
					convoData.sRootName = "SOURCE_CARGO_POSITIVE"
				BREAK
				CASE PED_SPH_PT_SOURCE_CARGO_NEGATIVE			
					convoData.sRootName = "SOURCE_CARGO_NEGATIVE"
				BREAK
			ENDSWITCH
		BREAK
		//MALE STAFF
		CASE 1
			convoData.sCharacterVoice = "A_M_Y_Ktown_01_Korean_FULL_01"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					convoData.sRootName = "GENERIC_HI"
					IF (iRandSpeech = 1)
						convoData.sRootName = "GENERIC_HOWS_IT_GOING"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "GENERIC_BYE"
				BREAK
				CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE			
					convoData.sRootName = "GENERIC_YES"
				BREAK
			ENDSWITCH
		BREAK
		//FEMALE STAFF
		CASE 2
			convoData.sCharacterVoice = "A_F_M_Bodybuild_01_Black_FULL_01"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					convoData.sRootName = "GENERIC_HI"
					IF (iRandSpeech = 1)
						convoData.sRootName = "GENERIC_HOWS_IT_GOING"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "GENERIC_BYE"
				BREAK
				CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE			
					convoData.sRootName = "GENERIC_YES"
				BREAK
			ENDSWITCH
		BREAK
		//MALE STAFF
		CASE 3
			convoData.sCharacterVoice = "A_M_Y_GenStreet_02_Latino_FULL_01"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					convoData.sRootName = "GENERIC_HI"
					IF (iRandSpeech = 1)
						convoData.sRootName = "GENERIC_HOWS_IT_GOING"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "GENERIC_BYE"
				BREAK
				CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE			
					convoData.sRootName = "GENERIC_YES"
				BREAK
			ENDSWITCH
		BREAK
		//FEMALE STAFF
		CASE 4
			convoData.sCharacterVoice = "A_F_Y_SouCent_03_Latino_FULL_01"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					convoData.sRootName = "GENERIC_HI"
					IF (iRandSpeech = 1)
						convoData.sRootName = "GENERIC_HOWS_IT_GOING"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "GENERIC_BYE"
				BREAK
				CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE			
					convoData.sRootName = "GENERIC_YES"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC PED_SPEECH _GET_WAREHOUSE_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE 0			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_WAREHOUSE_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_WAREHOUSE_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_WAREHOUSE_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_WAREHOUSE_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_WAREHOUSE_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_WAREHOUSE_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_WAREHOUSE_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_WAREHOUSE_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_WAREHOUSE_STANDING_TEXTING(PED_ANIM_DATA &pedAnimData, INT iClip, BOOL bFEMALE = TRUE)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	IF bFEMALE
		SWITCH iClip
			CASE 0
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@BASE"
				pedAnimData.sAnimClip 		= "BASE"
			BREAK
			CASE 1
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
				pedAnimData.sAnimClip 		= "IDLE_A"
			BREAK
			CASE 2
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
				pedAnimData.sAnimClip 		= "IDLE_B"
			BREAK
			CASE 3
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
				pedAnimData.sAnimClip 		= "IDLE_C"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iClip
			CASE 0
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@BASE"
				pedAnimData.sAnimClip 		= "BASE"
			BREAK
			CASE 1
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
				pedAnimData.sAnimClip 		= "IDLE_A"
			BREAK
			CASE 2
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
				pedAnimData.sAnimClip 		= "IDLE_B"
			BREAK
			CASE 3
				pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
				pedAnimData.sAnimClip 		= "IDLE_C"
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC GET_WAREHOUSE_ANIM_NOTEBOOK_TOD(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
//		CASE 3  //ped looks at an absent wristwatch in this one, so we need to skip it
//			pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@idle_a"
//			pedAnimData.sAnimClip 		= "idle_b"
//		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC _GET_WAREHOUSE_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	UNUSED_PARAMETER(iClip)
	RESET_PED_ANIM_DATA(pedAnimData)
	SWITCH eActivity	
		CASE PED_ACT_WAREHOUSE_CLIPBOARD
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)	
			pedAnimData.sAnimClip = "WORLD_HUMAN_CLIPBOARD_FACILITY"
		BREAK	
		CASE PED_ACT_WAREHOUSE_NOTEPAD
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			GET_WAREHOUSE_ANIM_NOTEBOOK_TOD(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_F
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			GET_WAREHOUSE_STANDING_TEXTING(pedAnimData, iClip, TRUE)
		BREAK
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_M
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			GET_WAREHOUSE_STANDING_TEXTING(pedAnimData, iClip, FALSE)
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC INT _GET_WAREHOUSE_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

FUNC VECTOR GET_EXIT_COORD_FOR_WAREHOUSE()
	EWarehouseSize Size = GET_WAREHOUSE_SIZE(GET_WAREHOUSE_ID_FROM_SIMPLE_INTERIOR_ID(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())))
	//WAREHOUSE LAYOUT A
	SWITCH Size
		CASE eWarehouseSmall		RETURN <<1087.53, -3099.41, -40.00>>
		CASE eWarehouseMedium		RETURN <<1048.32, -3097.12, -40.00>>
		CASE eWarehouseLarge		RETURN <<992.48, -3097.83, -39.99>>
	ENDSWITCH
	RETURN <<1087.53, -3099.41, -40.00>>
ENDFUNC

FUNC INT GET_CUSTOM_PED_EXIT_PATH_COUNT()	
	SWITCH g_iPedLayout
		//SMALL WAREHOUSE
		CASE 0
		CASE 2
			RETURN 1
		BREAK
		CASE 1
			RETURN 2
		BREAK
		//MEDIUM WAREHOUSE
		CASE 3
			RETURN 2
		BREAK
		CASE 4
			RETURN 4
		BREAK
		CASE 5
			RETURN 2
		BREAK
		//LARGE WAREHOUSE
		CASE 6
			RETURN 5
		BREAK
		CASE 7
		CASE 8
			RETURN 3
		BREAK
	ENDSWITCH
	RETURN 1
ENDFUNC

//purpose: gets the next vector in the sequence for the stripper to walk to without a navmesh
FUNC VECTOR GET_CUSTOM_PED_EXIT_PATH_LOCATIONS(INT iLocationID)
	
	VECTOR vTemp_Dest	
	SWITCH g_iPedLayout
		CASE 1
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<1101.5233, -3100.6101, -39.9999>>	BREAK
				CASE 2 vTemp_Dest = <<1087.53, -3099.41, -40.00>>	BREAK
			ENDSWITCH
		BREAK
		CASE 0
		CASE 2
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<1087.53, -3099.41, -40.00>>	BREAK
			ENDSWITCH
		BREAK
		CASE 3
		CASE 5
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<1052.4991, -3098.0396, -39.9999>>	BREAK
				CASE 2 vTemp_Dest = <<1048.32, -3097.12, -40.00>>		BREAK
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<1069.4734, -3100.9600, -39.9999>>		BREAK
				CASE 2 vTemp_Dest = <<1063.7432, -3098.0845, -39.9999>>		BREAK
				CASE 3 vTemp_Dest = <<1052.4991, -3098.0396, -39.9999>>		BREAK
				CASE 4 vTemp_Dest = <<1048.3200, -3097.1200, -40.0000>>		BREAK
			ENDSWITCH
		BREAK
		CASE 6
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<1017.7685, -3100.6338, -39.9999>>		BREAK
				CASE 2 vTemp_Dest = <<1002.1666, -3100.7676, -39.9999>>		BREAK		
				CASE 3 vTemp_Dest = <<0997.1729, -3101.9189, -39.9999>>		BREAK
				CASE 4 vTemp_Dest = <<0994.9340, -3102.0842, -39.9908>>		BREAK
				CASE 5 vTemp_Dest = <<0992.4800, -3097.8300, -39.9900>>		BREAK
			ENDSWITCH
		BREAK
		CASE 7
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<997.1729, -3101.9189, -39.9999>>		BREAK
				CASE 2 vTemp_Dest = <<994.9340, -3102.0842, -39.9908>>		BREAK
				CASE 3 vTemp_Dest = <<992.4800, -3097.8300, -39.9900>>		BREAK
			ENDSWITCH
		BREAK
		CASE 8
			SWITCH iLocationID
				CASE 1 vTemp_Dest = <<996.6786, -3102.7542, -39.9999>>		BREAK
				CASE 2 vTemp_Dest = <<994.9340, -3102.0842, -39.9908>>		BREAK
				CASE 3 vTemp_Dest = <<992.4800, -3097.8300, -39.9900>>		BREAK
			ENDSWITCH
		BREAK		
		
	ENDSWITCH
	
	//MP_PROP_OFFSET_STRUCT sTemp = GET_OFFSET_LOCATION_FOR_INTERIOR_VECTOR(iProperty, iBaseProperty, vTemp_Dest, <<0.0, 0.0, 0.0>>)
	
	RETURN vTemp_Dest
ENDFUNC

//Purpose: Builds a custom point route for the stripper based on her current position
PROC BUILD_CUSTOM_PED_EXIT_ROUTE(PED_INDEX pIndex)
	INT iSequence 		= 1
	INT iSequenceEnd	= GET_CUSTOM_PED_EXIT_PATH_COUNT()
	INT iStep 			= 1
			
	CDEBUG1LN( DEBUG_SAFEHOUSE, "BUILD_CUSTOM_PED_EXIT_ROUTE: ", iSequence, " iSequenceEnd ", iSequenceEnd, " iStep ", iStep )
	
	TASK_FLUSH_ROUTE()
	WHILE iSequence != (iSequenceEnd + iStep)
		VECTOR vDestination = GET_CUSTOM_PED_EXIT_PATH_LOCATIONS(iSequence)
		TASK_EXTEND_ROUTE( vDestination )
		CDEBUG1LN( DEBUG_SAFEHOUSE, "BUILD_CUSTOM_PED_EXIT_ROUTE: ", vDestination)
		iSequence += iStep
	ENDWHILE
	
	TASK_FOLLOW_POINT_ROUTE( pIndex, PEDMOVE_WALK, TICKET_SINGLE )
ENDPROC

FUNC INT GET_PED_EVENT_TIMEOUT_FOR_WAREHOUSE()
	SWITCH g_iPedLayout
		//SMALL WAREHOUSE
		CASE 0
		CASE 1
		CASE 2
			RETURN 16000
		BREAK
		//MEDIUM WAREHOUSE
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
		CASE 8
			RETURN 26000
		BREAK
		//LARGE WAREHOUSE
	ENDSWITCH
	RETURN 25000
ENDFUNC

PROC _WAREHOUSE_UPDATE(SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ServerBD)
	
	IF IS_PEDS_BIT_SET(LocalData.PedData[0].iBS, BS_PED_DATA_LAUNCH_CUSTOM_TASK)
		CLEAR_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_LAUNCH_CUSTOM_TASK)
		SET_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_PERFORMING_CUSTOM_TASK)
		//PED_INDEX pedID = LocalData.PedData[0].PedID
		
		IF LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
		OR LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
		OR LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
			SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_PLAY_EXIT_ANIMATION)
			EXIT
		ENDIF
		
		IF IS_ENTITY_ALIVE(LocalData.PedData[0].PedID)
			//CLEAR_PED_TASKS(pedID)
			//START WALKING SEQUENCE
			BUILD_CUSTOM_PED_EXIT_ROUTE(LocalData.PedData[0].PedID)
			SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_IS_PED_EXITING)
			START_NET_TIMER(LocalData.tCustomEventTimer)
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				ServerBD.iLayout = -1
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PEDS_BIT_SET(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_PLAY_EXIT_ANIMATION)
		IF IS_ENTITY_ALIVE(LocalData.PedData[0].PedID)
			STRING sAnimDict 
			
			SWITCH INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[0].iActivity)
				CASE PED_ACT_WAREHOUSE_STAND_TEXTING_F
					sAnimDict = "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@EXIT"
				BREAK
				CASE PED_ACT_WAREHOUSE_STAND_TEXTING_M
				sAnimDict = "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@EXIT"
				BREAK
				CASE PED_ACT_WAREHOUSE_NOTEPAD
				sAnimDict = "amb@medic@standing@timeofdeath@exit"
				BREAK
			ENDSWITCH		
			
			REQUEST_ANIM_DICT(sAnimDict)
			PRINTLN("[WAREHOUSE][CARGO SOURCING] BS_PED_DATA_FORCE_EXIT_ANIMATION-0")
			IF HAS_ANIM_DICT_LOADED(sAnimDict)
				//CLEAR_PED_TASKS(LocalData.PedData[0].PedID)
				TASK_PLAY_ANIM(LocalData.PedData[0].PedID, sAnimDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT)
				PRINTLN("[WAREHOUSE][CARGO SOURCING] BS_PED_DATA_FORCE_EXIT_ANIMATION-1")
				CLEAR_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_PLAY_EXIT_ANIMATION)
				SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_EXIT_ANIMATION_PLAYING)
				//CLEAR_PED_TASKS(pedID)
				//START WALKING SEQUENCE

				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PEDS_BIT_SET(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_EXIT_ANIMATION_PLAYING)
		IF NOT HAS_NET_TIMER_STARTED(LocalData.tCustomEventTimer)
			START_NET_TIMER(LocalData.tCustomEventTimer)
		ELSE
			IF LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
				IF HAS_NET_TIMER_EXPIRED(LocalData.tCustomEventTimer, 5000)	//4800 WORKS
					INT iProp
					REPEAT MAX_NUM_PED_PROPS iProp
						IF DOES_ENTITY_EXIST(LocalData.PedData[0].PropID[iProp])
							PRINTLN("[WAREHOUSE][CARGO SOURCING] PROP ", iProp ," EXISTS. SETTING IT TO INVISIBLE.")
							SET_ENTITY_ALPHA(LocalData.PedData[0].PropID[iProp], 0 , TRUE)
						ENDIF
					ENDREPEAT
					SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_ARE_PROPS_DELETED)
				ENDIF
			ENDIF
		ENDIF
		IF GET_SCRIPT_TASK_STATUS(LocalData.PedData[0].PedID, SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
			BUILD_CUSTOM_PED_EXIT_ROUTE(LocalData.PedData[0].PedID)	
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				ServerBD.iLayout = -1
			ENDIF
			CLEAR_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_EXIT_ANIMATION_PLAYING)
			SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_IS_PED_EXITING)
		ENDIF
	ENDIF
	
	IF IS_PEDS_BIT_SET(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_IS_PED_EXITING)
		IF NOT IS_PEDS_BIT_SET(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_ARE_PROPS_DELETED)
			IF LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
				IF GET_SCRIPT_TASK_STATUS(LocalData.PedData[0].PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK
					OBJECT_INDEX clipboard = GET_CLOSEST_OBJECT_OF_TYPE(LocalData.PedData[0].vPosition, 3.0, P_CS_CLIPBOARD, FALSE, FALSE, FALSE)
					IF DOES_ENTITY_EXIST(clipboard)
						PRINTLN("[WAREHOUSE][CARGO SOURCING] CLIPBOARD EXISTS. SETTING IT TO INVISIBLE.")
						SET_ENTITY_ALPHA(clipboard, 0 , TRUE)
					ENDIF
					SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_ARE_PROPS_DELETED)
				ENDIF
			ELIF LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_F)
			OR LocalData.PedData[0].iActivity = ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
				IF GET_SCRIPT_TASK_STATUS(LocalData.PedData[0].PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK		
					INT iProp
					REPEAT MAX_NUM_PED_PROPS iProp
						IF DOES_ENTITY_EXIST(LocalData.PedData[0].PropID[iProp])
							PRINTLN("[WAREHOUSE][CARGO SOURCING] PROP ", iProp ," EXISTS. SETTING IT TO INVISIBLE.")
							SET_ENTITY_ALPHA(LocalData.PedData[0].PropID[iProp], 0 , TRUE)
						ENDIF
					ENDREPEAT
					SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_ARE_PROPS_DELETED)
				ENDIF
			ELSE
				SET_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_ARE_PROPS_DELETED)
			ENDIF
		ENDIF	
		
		PED_INDEX pedID = LocalData.PedData[0].PedID
		IF IS_ENTITY_ALIVE(LocalData.PedData[0].PedID)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedID), GET_EXIT_COORD_FOR_WAREHOUSE()) <= 2.0
			OR HAS_NET_TIMER_EXPIRED(LocalData.tCustomEventTimer, GET_PED_EVENT_TIMEOUT_FOR_WAREHOUSE())
				SET_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_FADE_PED)
				g_iPedLayout = -1					
				CLEAR_PEDS_BIT(LocalData.PedData[0].iCustomTaskBS, BS_CUSTOM_TASK_IS_PED_EXITING)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _SET_WAREHOUSE_PED_HEAD_TRACKING_DATA_LAYOUT(HEAD_TRACKING_DATA &HeadtrackingData, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				HeadtrackingData.iPedID							= 0
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC BOOL _CAN_WAREHOUSE_PED_HEAD_TRACK(PED_INDEX PedID, INT iPed, INT iLayout)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(iPed)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_HEAD_TRACK - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_HEAD_TRACK - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_HEAD_TRACK - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_HEAD_TRACK - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_HEAD_TRACK - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_WAREHOUSE_PED_HEAD_TRACK - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
//	// Specific conditions
//	SWITCH iPed
//		CASE 1 //Imani.		Only head track when player is on the right side of the desk
//			RETURN g_bShouldImaniHeadtrack			
//		BREAK
//	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC _SET_WAREHOUSE_PED_HEAD_TRACKING_DATA(HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	_SET_WAREHOUSE_PED_HEAD_TRACKING_DATA_LAYOUT(HeadtrackingData, iArrayID, iHeadTrackingPedID, bNetworkPed)
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_WAREHOUSE_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_WAREHOUSE_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_WAREHOUSE_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_WAREHOUSE_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_WAREHOUSE_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_WAREHOUSE_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_WAREHOUSE_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_WAREHOUSE_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_WAREHOUSE_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_WAREHOUSE_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_WAREHOUSE_PED_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_WAREHOUSE_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_WAREHOUSE_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_WAREHOUSE_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_WAREHOUSE_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_WAREHOUSE_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_WAREHOUSE_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_WAREHOUSE_PED_PROP_INDEXES
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_WAREHOUSE_PED_BEEN_CREATED
		BREAK
		
		//Head tracking
		CASE E_SET_PED_HEAD_TRACKING_DATA
			interface.setPedHeadTrackingData = &_SET_WAREHOUSE_PED_HEAD_TRACKING_DATA
		BREAK
		CASE E_CAN_PED_HEAD_TRACK
			interface.returnCanPedHeadTrack = &_CAN_WAREHOUSE_PED_HEAD_TRACK
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_WAREHOUSE_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_WAREHOUSE_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_WAREHOUSE_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_WAREHOUSE_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_WAREHOUSE_PED_CONVO_DATA
		BREAK
		
		//UPDATE FUNCTION
		CASE E_LOCATION_SPECIFIC_UPDATE
			interface.locationSpecificUpdate = &_WAREHOUSE_UPDATE
		BREAK
				
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
