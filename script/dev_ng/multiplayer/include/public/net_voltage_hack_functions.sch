USING "commands_misc.sch"
USING "commands_audio.sch"
USING "net_voltage_hack_helpers.sch"

INT iTargetValue
INT iCurrentValue

INT iLinkCount
INT iActiveLink // url:bugstar:6750166 - [Script] Voltage Minigame - Adjustments to the flow of the voltage minigame
INT iFirstLink = -1 // First active link
INT iSecondLink = -1 // Second active link
// Adding count value after feedback.
// url:bugstar:6722983
FLOAT fCountValue

INT iActiveCounter // Used for counting mod links

ENUM MATH_FUNCTIONS

	TIMES_ONE,
	TIMES_TWO,
	//TIMES_THREE,
	//TIMES_FIVE,
	TIMES_TEN,
	//TIMES_FIFTEEN,
	MF_MAX_MODS

ENDENUM 

// Values to be used by the puzzle
INT iValues[3]

FUNC INT VOLTAGE_APPLY_RANDOM_MOD(INT iValueIn, MATH_FUNCTIONS modToUse)

	SWITCH modToUse
	
		// Times one not necessary
		
		CASE TIMES_TWO
			RETURN iValueIn * 2
		BREAK
		
//		CASE TIMES_THREE
//			RETURN iValueIn * 3
//		BREAK
//		
//		CASE TIMES_FIVE
//			RETURN iValueIn * 5
//		BREAK
		
		CASE TIMES_TEN
			RETURN iValueIn * 10
		BREAK
		
//		CASE TIMES_FIFTEEN 
//			RETURN iValueIn * 15
//		BREAK
	ENDSWITCH
	
	RETURN iValueIn
	
ENDFUNC

MATH_FUNCTIONS eFunctions[3]

// We need to calculate a Target value from the available numbers
// There must always be at least one possible solution
PROC VOLTAGE_CALCULATE_TARGET_VALUE(HG_CONTROL_STRUCT &sControllerStruct)

	iValues[0] = GET_RANDOM_INT_IN_RANGE(1, 10)

	iValues[1] = GET_RANDOM_INT_IN_RANGE(1, 10)
	
	// Make at least one different. Having 3 the same breaks it
	WHILE iValues[0] = iValues[1]
		iValues[1] = GET_RANDOM_INT_IN_RANGE(1, 10)	
	ENDWHILE
	
	iValues[2] = GET_RANDOM_INT_IN_RANGE(1, 10)
	
	WHILE iValues[1] = iValues[2]
		iValues[2] = GET_RANDOM_INT_IN_RANGE(1, 10)	
	ENDWHILE
	
	eFunctions[0] = INT_TO_ENUM(MATH_FUNCTIONS, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MF_MAX_MODS)))
	eFunctions[1] = INT_TO_ENUM(MATH_FUNCTIONS, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MF_MAX_MODS)))
		
	// Make at least one different
	// Avoid repeat functions
	WHILE eFunctions[0] = eFunctions[1]
		eFunctions[1] = INT_TO_ENUM(MATH_FUNCTIONS, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MF_MAX_MODS)))
	ENDWHILE
	
	eFunctions[2] = INT_TO_ENUM(MATH_FUNCTIONS, GET_RANDOM_INT_IN_RANGE(0, 6))
	
	WHILE eFunctions[1] = eFunctions[2]
		eFunctions[2] = INT_TO_ENUM(MATH_FUNCTIONS, GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MF_MAX_MODS)))
	ENDWHILE
	
	INT iFirstFunction = GET_RANDOM_INT_IN_RANGE(0,3)
	INT iSecondFunction = GET_RANDOM_INT_IN_RANGE(0,3)
	INT iThirdFunction = GET_RANDOM_INT_IN_RANGE(0,3)
	
	// Avoid repeat functions
	WHILE iFirstFunction = iSecondFunction
		iSecondFunction = GET_RANDOM_INT_IN_RANGE(0,3)
	ENDWHILE
	
	WHILE iFirstFunction = iThirdFunction
	OR iSecondFunction = iThirdFunction
		iThirdFunction = GET_RANDOM_INT_IN_RANGE(0,3)
	ENDWHILE
	
	// If it's the first time through we'll use set values for the first instance
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FIRST_PLAYED)
		iValues[0] = 5
		iValues[1] = 2
		iValues[2] = 5
	
		iFirstFunction = 0
		iSecondFunction = 1
		iThirdFunction = 2
		
		eFunctions[iFirstFunction] = INT_TO_ENUM(MATH_FUNCTIONS, 0)
		eFunctions[iSecondFunction] = INT_TO_ENUM(MATH_FUNCTIONS, 1)
		eFunctions[iThirdFunction] = INT_TO_ENUM(MATH_FUNCTIONS, 2)
		CDEBUG1LN(DEBUG_MINIGAME, "[BAZ] VOLTAGE_CALCULATE_TARGET_VALUE - ciHGBS_FIRST_PLAYED")
	ENDIF
	
	iTargetValue = 
		VOLTAGE_APPLY_RANDOM_MOD(iValues[0], eFunctions[iFirstFunction]) +
		VOLTAGE_APPLY_RANDOM_MOD(iValues[1], eFunctions[iSecondFunction]) +
		VOLTAGE_APPLY_RANDOM_MOD(iValues[2], eFunctions[iThirdFunction])
		
ENDPROC

FUNC STRING VOLTAGE_GET_ICON_TEXTURE(MATH_FUNCTIONS eFuncToGet)


	SWITCH  eFuncToGet
	
		CASE TIMES_ONE
			RETURN "Icons__x1"
		BREAK
	
		CASE TIMES_TWO
			RETURN "Icons__x2"
		BREAK
		
//		CASE TIMES_THREE
//			RETURN "Icons__x3"
//		BREAK
//		
//		CASE TIMES_FIVE
//			RETURN "Icons__x5"
//		BREAK
	
		CASE TIMES_TEN
			RETURN "Icons__x10"
		BREAK
		
//		CASE TIMES_FIFTEEN
//			RETURN "Icons__x15"
//		BREAK
		
	ENDSWITCH
	
	RETURN "Icons__x1"

ENDFUNC

PROC VOLTAGE_GET_MOD_SET_SFX(MATH_FUNCTIONS eFuncToGet, HACKING_GAME_STRUCT &sGameStruct)

	SWITCH  eFuncToGet
	
		CASE TIMES_ONE
			PLAY_SOUND_FRONTEND(-1, "Connect_Multiply_1", sGameStruct.sAudioSet)
		BREAK
	
		CASE TIMES_TWO
			PLAY_SOUND_FRONTEND(-1, "Connect_Multiply_2", sGameStruct.sAudioSet)
		BREAK
		
//		CASE TIMES_THREE
//			PLAY_SOUND_FRONTEND(-1, "Connect_Multiply_3", sGameStruct.sAudioSet)
//		BREAK
//		
//		CASE TIMES_FIVE
//			PLAY_SOUND_FRONTEND(-1, "Connect_Multiply_5", sGameStruct.sAudioSet)
//		BREAK
	
		CASE TIMES_TEN
			PLAY_SOUND_FRONTEND(-1, "Connect_Multiply_10", sGameStruct.sAudioSet)
		BREAK
		
//		CASE TIMES_FIFTEEN
//			PLAY_SOUND_FRONTEND(-1, "Connect_Multiply_15", sGameStruct.sAudioSet)
//		BREAK
		
	ENDSWITCH

ENDPROC

PROC VOLTAGE_ITER_ACTIVE_LINK()
	iActiveLink++
	
	IF iActiveLink > 2
		iActiveLink = 0
	ENDIF
ENDPROC

PROC VOLTAGE_SUB_ACTIVE_LINK()
	iActiveLink --
	
	IF iActiveLink < 0
		iActiveLink = 2
	ENDIF
ENDPROC

