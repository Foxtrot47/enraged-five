
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Written by:  Conor McGuire																	//
// Date: 		28/10/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
USING "globals.sch"
USING "net_mission.sch"
USING "net_common_functions.sch"
USING "rage_builtins.sch"

#IF IS_DEBUG_BUILD
PROC RUN_MISSION_END_SHARD_TEST(MISSION_END_SHARD_TYPE eType)
	IF eType = MISSION_END_SHARD_TYPE_CASINO_HEIST
		missionEndShardData.bRunOnReturnToFreemode = TRUE
		missionEndShardData.bPass = TRUE
		missionEndShardData.eType = MISSION_END_SHARD_TYPE_CASINO_HEIST
		missionEndShardData.iApproachID = CASINO_HEIST_END_APPROACH_AGGRESSIVE
		missionEndShardData.iTargetID = CASINO_HEIST_END_TARGET_CASH
		missionEndShardData.iTake[0] = 1000000
		missionEndShardData.iTake[1] = 50000
		missionEndShardData.iMissionFee = -1000
		missionEndShardData.npcTake[0].iNPCID = -1
		missionEndShardData.npcTake[0].iTake = -500
		missionEndShardData.npcTake[1].iNPCID = ENUM_TO_INT(CASINO_HEIST_WEAPON_EXPERT__CHARLIE)
		missionEndShardData.npcTake[1].iTake = 100
		missionEndShardData.npcTake[2].iNPCID = ENUM_TO_INT(CASINO_HEIST_HACKER__AVI_SCHWARTZMAN)
		missionEndShardData.npcTake[2].iTake = 100
		missionEndShardData.npcTake[3].iNPCID = ENUM_TO_INT(CASINO_HEIST_DRIVER__EDDIE_TOH)
		missionEndShardData.npcTake[3].iTake = 100
		missionEndShardData.playerCut[0].playerID = PLAYER_ID()
		missionEndShardData.playerCut[0].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[0].iCut = 10000
		missionEndShardData.playerCut[0].bLeader = FALSE
		missionEndShardData.playerCut[1].playerID = PLAYER_ID()
		missionEndShardData.playerCut[1].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[1].iCut = 5000
		missionEndShardData.playerCut[1].bLeader = FALSE
		missionEndShardData.playerCut[2].playerID = PLAYER_ID()
		missionEndShardData.playerCut[2].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[2].iCut = 10000
		missionEndShardData.playerCut[2].bLeader = TRUE
		missionEndShardData.playerCut[3].playerID = PLAYER_ID()
		missionEndShardData.playerCut[3].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[3].iCut = 20000
		missionEndShardData.playerCut[3].bLeader = FALSE
		missionEndShardData.iEliteChallenge = CASINO_HEIST_END_ELITE_RESTART
	ELIF eType = MISSION_END_SHARD_TYPE_ISLAND_HEIST
		missionEndShardData.bRunOnReturnToFreemode = TRUE
		missionEndShardData.bPass = TRUE
		missionEndShardData.eType = MISSION_END_SHARD_TYPE_ISLAND_HEIST
		missionEndShardData.iTime = 20000
		missionEndShardData.iApproachID = ENUM_TO_INT(HIAV_SUBMARINE)
		missionEndShardData.iTargetID = ISLAND_HEIST_TARGET_PANTHER_STATUE
		missionEndShardData.iTake[0] = 1000000
		missionEndShardData.iTake[1] = 50000
		missionEndShardData.iMissionFee = 25000
		missionEndShardData.npcTake[0].iNPCID = -1
		missionEndShardData.npcTake[0].iTake = 10000
		missionEndShardData.playerCut[0].playerID = PLAYER_ID()
		missionEndShardData.playerCut[0].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[0].iCut = 10000
		missionEndShardData.playerCut[0].bLeader = FALSE
		missionEndShardData.playerCut[1].playerID = PLAYER_ID()
		missionEndShardData.playerCut[1].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[1].iCut = 5000
		missionEndShardData.playerCut[1].bLeader = FALSE
		missionEndShardData.playerCut[2].playerID = PLAYER_ID()
		missionEndShardData.playerCut[2].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[2].iCut = 10000
		missionEndShardData.playerCut[2].bLeader = TRUE
		missionEndShardData.playerCut[3].playerID = PLAYER_ID()
		missionEndShardData.playerCut[3].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[3].iCut = 20000
		missionEndShardData.playerCut[3].bLeader = FALSE
		missionEndShardData.iEliteChallenge = ISLAND_HEIST_END_ELITE_RESTART
	ELIF eType = MISSION_END_SHARD_TYPE_TUNER_ROBBERY
		missionEndShardData.bRunOnReturnToFreemode = TRUE
		missionEndShardData.bPass = TRUE
		missionEndShardData.eType = MISSION_END_SHARD_TYPE_TUNER_ROBBERY
		missionEndShardData.iID = 1 //robbery ID
		missionEndShardData.iTime = 20000
		missionEndShardData.iTargetID = 0 //targets
		missionEndShardData.iTake[0] = 100000
		missionEndShardData.npcTake[0].iTake = 100
		missionEndShardData.playerCut[0].playerID = PLAYER_ID()
		missionEndShardData.playerCut[0].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[0].iCut = 10000
		missionEndShardData.playerCut[0].bLeader = FALSE
		missionEndShardData.playerCut[1].playerID = PLAYER_ID()
		missionEndShardData.playerCut[1].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[1].iCut = 5000
		missionEndShardData.playerCut[1].bLeader = FALSE
		missionEndShardData.playerCut[2].playerID = PLAYER_ID()
		missionEndShardData.playerCut[2].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[2].iCut = 10000
		missionEndShardData.playerCut[2].bLeader = TRUE
		missionEndShardData.playerCut[3].playerID = PLAYER_ID()
		missionEndShardData.playerCut[3].playerName = GET_PLAYER_NAME(PLAYER_ID())
		missionEndShardData.playerCut[3].iCut = 20000
		missionEndShardData.playerCut[3].bLeader = FALSE
		missionEndShardData.iApproachID = ENUM_TO_INT(ADDER) //VEHICLE_MODEL
	ELIF eType = MISSION_END_SHARD_TYPE_TUNER_DELIVERY
		missionEndShardData.bRunOnReturnToFreemode = TRUE
		missionEndShardData.bPass = TRUE
		missionEndShardData.eType = MISSION_END_SHARD_TYPE_TUNER_DELIVERY
		missionEndShardData.iTake[0] = 10000//Payment for delivery
		missionEndShardData.iTake[1] = 5000//Service costs
		missionEndShardData.iMissionFee = 2500//Damages

		missionEndShardData.iApproachID = ENUM_TO_INT(ADDER) //VEHICLE_MODEL
	ENDIF
	PRINTLN("RUN_MISSION_END_SHARD_TEST: etype = ",eType," called this frame")
	//MAINTAIN_CASINO_HEIST_END_SHARD(data,TRUE)
ENDPROC
#ENDIF


PROC CLEAR_MISSION_END_SHARD_DATA(MISSION_END_SHARD_DATA &data)
	INT i
	data.bRunonReturnToFreemode = FALSE
	data.bPass = FALSE
	data.eType = MISSION_END_SHARD_TYPE_NULL
	data.iID = -1
	data.iTime = -1
	data.iApproachID = -1
	data.iTargetID = -1
	REPEAT MISSION_END_SHARD_MAX_TAKES i
		data.iTake[i] = -1
	ENDREPEAT
	data.iMissionFee = -1
	REPEAT MISSION_END_SHARD_MAX_TAKES i
		data.npcTake[i].iNPCID = -1
		data.npcTake[i].iTake = -1
	ENDREPEAT
	REPEAT 4 i
		data.playerCut[i].playerID = INVALID_PLAYER_INDEX()
		data.playerCut[i].playerName = ""
		data.playerCut[i].iCut = 0
		data.playerCut[i].bLeader = FALSE
	ENDREPEAT
	data.iEliteChallenge = -1
	data.iEliteChallengeCash = -1
	DEBUG_PRINTCALLSTACK()
	PRINTLN("CLEAR_MISSION_END_SHARD_DATA:called this frame")
ENDPROC

PROC MAINTAIN_MISSION_END_SHARD(MISSION_END_SHARD_DATA &data, BOOL bCelebrationScript = FALSE, BOOL bDelayShardStart = FALSE)
	IF data.bRunOnReturnToFreemode
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
		AND IS_NET_PLAYER_OK(PLAYER_ID(),TRUE)
		AND IS_SKYSWOOP_AT_GROUND()
		AND NOT NETWORK_IS_IN_MP_CUTSCENE() 
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistEndWinnerScene
		OR bCelebrationScript
			INT i
			TEXT_LABEL_23 tempText
			TEXT_LABEL_31 playerName
			PLAYER_INDEX tempPlayer
			#IF FEATURE_DLC_1_2022
			BOOL bCheckpoints,bLanding,bParTime
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			PRINTLN("MAINTAIN_MISSION_END_SHARD: data.bRunOnReturnToFreemode = ",data.bRunOnReturnToFreemode)
			PRINTLN("MAINTAIN_MISSION_END_SHARD: data.bPass = ",data.bPass)
			IF data.bPass
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.eType = ",data.eType)
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iID = ",data.iID)
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iTime = ",data.iTime)
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iApproachID = ", data.iApproachID)
				IF data.eType = MISSION_END_SHARD_TYPE_ISLAND_HEIST
					PRINTLN("MAINTAIN_MISSION_END_SHARD: approach vehicle = ", HEIST_ISLAND_APPROACH_VEHICLES_TO_STRING(INT_TO_ENUM(HEIST_ISLAND_APPROACH_VEHICLES, data.iApproachID)))
				ENDIF
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iTargetID = ",data.iTargetID)
				REPEAT MISSION_END_SHARD_MAX_TAKES i
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iTake[",i,"] = ",data.iTake[i])
				ENDREPEAT
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iMissionFee = ",data.iMissionFee)
				REPEAT MISSION_END_SHARD_MAX_NPCS i
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.npcTake[",i,"].iNPCID = ",data.npcTake[i].iNPCID)
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.npcTake[",i,"].iTake = ",data.npcTake[i].iTake)
				ENDREPEAT
				REPEAT MISSION_END_SHARD_MAX_PLAYERS i
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.playerCut[",i,"].bLeader = ",data.playerCut[i].bLeader)
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.playerCut[",i,"].playerID = ",NATIVE_TO_INT(data.playerCut[i].playerID))
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.playerCut[",i,"].playerName = ",data.playerCut[i].playerName)
					PRINTLN("MAINTAIN_MISSION_END_SHARD: data.playerCut[",i,"].iCut = ",data.playerCut[i].iCut)
				ENDREPEAT
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iEliteChallenge = ",data.iEliteChallenge)
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.iEliteChallengeCash = ",data.iEliteChallengeCash)
				PRINTLN("MAINTAIN_MISSION_END_SHARD: data.bPass = ",data.bPass)
			ENDIF
			#ENDIF
			
			IF data.bPass
				
				BIG_MESSAGE_BIT_SET eBitSet = BIG_MESSAGE_BIT_INVALID
				
				IF bCelebrationScript
					eBitSet = BIG_MESSAGE_BIT_PERSIST_OVER_CELEBRATION|BIG_MESSAGE_BIT_START_AND_PERSIST_OVER_FADE|BIG_MESSAGE_BIT_ALLOW_DELAYED_START
					
					IF bDelayShardStart
						g_bDelayShardStart = TRUE
						PRINTLN("MAINTAIN_MISSION_END_SHARD - g_bDelayShardStart = TRUE")
					ENDIF
					
					PRINTLN("MAINTAIN_MISSION_END_SHARD - Shard bit set: ", eBitSet, " g_bDelayShardStart: ", g_bDelayShardStart)
				ENDIF
				
				g_bEndScreenSuppressFadeIn = TRUE
				
				SWITCH data.eType
					CASE MISSION_END_SHARD_TYPE_CASINO_HEIST
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"CH_END_P","CH_END_NAME",DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
						g_bEndScreenSuppressFadeIn = TRUE
						ADD_MISSION_SUMMARY_TITLE("CH_END_NAME")
					
						tempText = "CH_END_0ST"
						tempText += data.iApproachID
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("CH_END_0ST",tempText)//approach
						tempText = "CH_END_1ST"
						tempText += data.iTargetID
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("CH_END_1ST",tempText)//Target
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_2ST",data.iTake[MISSION_END_SHARD_CH_POT_TAKE])//Potential take
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_3ST",data.iTake[MISSION_END_SHARD_CH_TAKE])//Take
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_10ST",data.iMissionFee)//Buyer Laundering Fee
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_4ST",data.npcTake[MISSION_END_SHARD_NPC_LESTER].iTake)//Lester's cut
						IF data.npcTake[MISSION_END_SHARD_NPC_WEAPON].iNPCID != ENUM_TO_INT(CASINO_HEIST_WEAPON_EXPERT__NONE)
							tempText = "CH_END_5ST"
							tempText += data.npcTake[MISSION_END_SHARD_NPC_WEAPON].iNPCID
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(tempText,data.npcTake[MISSION_END_SHARD_NPC_WEAPON].iTake)//(Gunman Name's) cut
						ENDIF
						IF data.npcTake[MISSION_END_SHARD_NPC_HACKER].iNPCID != ENUM_TO_INT(CASINO_HEIST_HACKER__NONE)
							tempText = "CH_END_6ST"
							tempText += data.npcTake[MISSION_END_SHARD_NPC_HACKER].iNPCID
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(tempText,data.npcTake[MISSION_END_SHARD_NPC_HACKER].iTake)//(Hacker Name's) cut
						ENDIF
						IF data.npcTake[MISSION_END_SHARD_NPC_DRIVER].iNPCID != ENUM_TO_INT(CASINO_HEIST_DRIVER__NONE)
							tempText = "CH_END_7ST"
							tempText += data.npcTake[MISSION_END_SHARD_NPC_DRIVER].iNPCID
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(tempText,data.npcTake[MISSION_END_SHARD_NPC_DRIVER].iTake)//(Driver Name's) cut
						ENDIF
						REPEAT 4 i
							IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
								IF data.playerCut[i].bLeader
									ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
									//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
								ENDIF
							ENDIF
						ENDREPEAT
						REPEAT 4 i
							IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
								IF NOT data.playerCut[i].bLeader
									ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
									//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerID,data.playerCut[i].iCut)
								ENDIF
							ENDIF
						ENDREPEAT
						IF data.iEliteChallenge = CASINO_HEIST_END_ELITE_NO_TRIED
							ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_UNCHECKED)
						ELIF data.iEliteChallenge = CASINO_HEIST_END_ELITE_RESTART
							ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_INVALIDATED)
						ELIF data.iEliteChallenge = CASINO_HEIST_END_ELITE_COMPLETE
							ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_CHECKED)
						ENDIF
						playerName = GET_PLAYER_NAME(PLAYER_ID())
						REPEAT 4 i
							//IF data.playerCut[i].playerID = PLAYER_ID()
							IF ARE_STRINGS_EQUAL(playerName,data.playerCut[i].playerName)
								ADD_MISSION_SUMMARY_END_COMPLETION_CASH("CH_END_END",data.playerCut[i].iCut+data.iEliteChallengeCash)// Your final take
							ENDIF
						ENDREPEAT
					BREAK
					
					CASE MISSION_END_SHARD_TYPE_ISLAND_HEIST
					
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"CH_END_P","IH_END_NAME",DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
				
						ADD_MISSION_SUMMARY_TITLE("IH_END_NAME")
					
						ADD_MISSION_SUMMARY_END_COMPLETION_TIME("IH_END_TIME",data.iTime) //time
						tempText = "IH_END_APV"
						tempText += data.iApproachID
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("IH_END_APV",tempText)//approach veh
						tempText = "IH_END_TAR"
						tempText += data.iTargetID
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("IH_END_TAR",tempText)//main target
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_TART",data.iTake[MISSION_END_SHARD_IH_MAIN_TAKE])//main taget take
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_ATAR",data.iTake[MISSION_END_SHARD_IH_ADD_TAKE])//additional loot take
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_FEE",data.iMissionFee)//fencing fee
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_PAVEL",data.npcTake[0].iTake)//pavel fee
						REPEAT 4 i
							IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
								IF data.playerCut[i].bLeader
									ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
									//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
								ENDIF
							ENDIF
						ENDREPEAT
						REPEAT 4 i
							IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
								IF NOT data.playerCut[i].bLeader
									ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
									//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerID,data.playerCut[i].iCut)
								ENDIF
							ENDIF
						ENDREPEAT
						IF data.iEliteChallenge = ISLAND_HEIST_END_ELITE_NO_TRIED
							ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_UNCHECKED)
						ELIF data.iEliteChallenge = ISLAND_HEIST_END_ELITE_RESTART
							ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_INVALIDATED)
						ELIF data.iEliteChallenge = ISLAND_HEIST_END_ELITE_COMPLETE
							ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_CHECKED)
						ENDIF
						playerName = GET_PLAYER_NAME(PLAYER_ID())
						REPEAT 4 i
							//IF data.playerCut[i].playerID = PLAYER_ID()
							IF ARE_STRINGS_EQUAL(playerName,data.playerCut[i].playerName)
								ADD_MISSION_SUMMARY_END_COMPLETION_CASH("CH_END_END",data.playerCut[i].iCut+data.iEliteChallengeCash)// Your final take
							ENDIF
						ENDREPEAT
					BREAK
					#IF FEATURE_TUNER
					CASE MISSION_END_SHARD_TYPE_TUNER_ROBBERY
						tempText = "ROB_ENDS_NAME"
						tempText += data.iID
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"ROB_ENDS_P",tempText,DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
				
						ADD_MISSION_SUMMARY_TITLE(tempText)
					
						tempText = "ROB_ENDS_TAR"
						tempText += data.iTargetID
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("ROB_ENDS_TAR",tempText)//target
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("ROB_ENDS_TAKE",data.iTake[0])
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("ROB_ENDS_FEE",data.npcTake[0].iTake)
						REPEAT 4 i
							IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
								IF data.playerCut[i].bLeader
									ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
									//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
								ENDIF
							ENDIF
						ENDREPEAT
						REPEAT 4 i
							IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
								IF NOT data.playerCut[i].bLeader
									ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
									//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerID,data.playerCut[i].iCut)
								ENDIF
							ENDIF
						ENDREPEAT
						
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("ROB_ENDS_VEH",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES,data.iApproachID)))//vehicle used
						
						playerName = GET_PLAYER_NAME(PLAYER_ID())
						REPEAT 4 i
							//IF data.playerCut[i].playerID = PLAYER_ID()
							IF ARE_STRINGS_EQUAL(playerName,data.playerCut[i].playerName)
								ADD_MISSION_SUMMARY_END_COMPLETION_CASH("ROB_ENDS_END",data.playerCut[i].iCut)
							ENDIF
						ENDREPEAT
					BREAK
					CASE MISSION_END_SHARD_TYPE_TUNER_DELIVERY

						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"TDEL_ES_P","TDEL_ES_T",DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
				
						ADD_MISSION_SUMMARY_TITLE("TDEL_ES_T")
						
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("TDEL_ES_VEH",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES,data.iApproachID)))//vehicle used
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("TDEL_ES_PAY",data.iTake[0]) //Payment for delivery
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("TDEL_ES_SER",data.iTake[1])//Service costs
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("TDEL_ES_DAM",data.iMissionFee)//Damages
						
						IF data.playerCut[0].iCut != 0
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("TDEL_ES_SAT",data.playerCut[0].iCut)//Satisfaction
						ENDIF
						tempPlayer = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						IF tempPlayer != INVALID_PLAYER_INDEX()
							IF tempPlayer = PLAYER_ID()
								ADD_MISSION_SUMMARY_END_COMPLETION_CASH("ROB_ENDS_END",data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)// Your final take
								//playerName = GET_PLAYER_NAME(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								//ADD_MISSION_SUMMARY_END_COMPLETION_LITERAL_STRING_AND_CASH("ROB_ENDS_ENDB",playerName,data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)
							ELSE
								playerName = GET_PLAYER_NAME(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								ADD_MISSION_SUMMARY_END_COMPLETION_LITERAL_STRING_AND_CASH("ROB_ENDS_ENDB",playerName,data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)
							ENDIF
						ELSE
							ADD_MISSION_SUMMARY_END_COMPLETION_CASH("ROB_ENDS_ENDC",data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)// Your final take
						ENDIF
					BREAK
					#ENDIF
					#IF FEATURE_FIXER
					CASE MISSION_END_SHARD_TYPE_FIXER_PAYPHONE_HITS
						
						STRING sTargetName
						
						// To detect the name of the target I'll use this var.
						SWITCH INT_TO_ENUM(FIXER_PAYPHONE_VARIATION, data.iTargetID) 
							CASE FPV_TAXI				sTargetName =   "FXR_ENDS_TAR0"	BREAK	
							CASE FPV_GOLF				sTargetName =	"FXR_ENDS_TAR1"	BREAK	
							CASE FPV_MOTEL				sTargetName =	"FXR_ENDS_TAR2"	BREAK	
							CASE FPV_CONSTRUCTION		sTargetName = 	"FXR_ENDS_TAR3"	BREAK
							CASE FPV_HIT_LIST			sTargetName = 	"FXR_ENDS_TAR4"	BREAK		
							CASE FPV_JOYRIDER			sTargetName =	"FXR_ENDS_TAR5"	BREAK
							CASE FPV_COORD_ATTACK		sTargetName =	"FXR_ENDS_TAR6"	BREAK
							CASE FPV_APPROACH			sTargetName =	"FXR_ENDS_TAR7"	BREAK
						ENDSWITCH
						
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"FXR_ENDS_PASS",sTargetName,DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
				
						ADD_MISSION_SUMMARY_TITLE(sTargetName)
						
						// Tick box status.
						END_SCREEN_CHECK_MARK_STATUS eMarkStatus
						IF missionEndShardData.iEliteChallenge = 1 // If BKM passed.
							eMarkStatus = ESCM_CHECKED
						ELSE
							eMarkStatus = ESCM_INVALIDATED
						ENDIF
						
						ADD_MISSION_SUMMARY_END_COMPLETION_TIME("FXR_ENDS_TIME", data.iMissionFee) // Time
						
						ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("FXR_ENDS_BOX", eMarkStatus)// Assassination Bonus [Check Box that's ticked if completed with bonus kill]
						
						
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("FXR_ENDS_PAY",data.iTake[0]) //Payment [Base completion payment]
						
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("FXR_ENDS_BONPAY",data.iTake[1])//Bonus Payment [Bonus amount for completing with bonus kill]
					
						ADD_MISSION_SUMMARY_END_COMPLETION_CASH("FXR_ENDS_TOTAL",data.iTake[0]+data.iTake[1])// Total Payment [Payment + Bonus Payment]
					BREAK
					#ENDIF
					#IF FEATURE_DLC_1_2022
					CASE MISSION_END_SHARD_TYPE_SKYDIVING_CHALLENGE


						bCheckpoints = data.iTake[0] != 0
						bParTime = data.iTake[1] != 0
						bLanding = data.iTake[2] != 0

						tempText = "SD_ES_ST"
						tempText += data.iID
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_SKYDIVING_CHALLENGE_SUMMARY,"SD_ES_T",tempText,DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
						IF bCheckpoints AND bLanding AND bParTime
							ADD_MISSION_SUMMARY_TITLE_STRING_WITH_SUB_STRING(tempText,"SD_ES_GOLD")
						ELSE
							ADD_MISSION_SUMMARY_TITLE_STRING_WITH_SUB_STRING(tempText,"SD_ES_PASS")
						ENDIF
						
						IF bCheckpoints
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX("SD_ES_CKPT",data.iTake[0],ESCM_CHECKED)
						ELSE
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX("SD_ES_CKPT",data.iTake[0],ESCM_INVALIDATED)
						ENDIF
						IF bParTime
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX("SD_ES_PAR",data.iTake[1],ESCM_CHECKED)
						ELSE
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX("SD_ES_PAR",data.iTake[1],ESCM_INVALIDATED)
						ENDIF
						IF bLanding
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX("SD_ES_LAND",data.iTake[2],ESCM_CHECKED)
						ELSE
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH_WITH_TICK_BOX("SD_ES_LAND",data.iTake[2],ESCM_INVALIDATED)
						ENDIF

						IF bCheckpoints AND bLanding AND bParTime
							ADD_MISSION_SUMMARY_END_COMPLETION_CASH_AND_MEDAL("SD_ES_TPAY",data.iTake[0] + data.iTake[1] + data.iTake[2],ESMS_GOLD)
						ELSE
							ADD_MISSION_SUMMARY_END_COMPLETION_CASH_AND_MEDAL("SD_ES_TPAY",data.iTake[0] + data.iTake[1] + data.iTake[2])
						ENDIF
					
					BREAK
					CASE MISSION_END_SHARD_TYPE_BIKE_DELIVERY

						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"BSDEL_ES_P","BSDEL_ES_T",DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
				
						ADD_MISSION_SUMMARY_TITLE("BSDEL_ES_T")
						
						ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("BSDEL_ES_VEH",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(INT_TO_ENUM(MODEL_NAMES,data.iApproachID)))//vehicle used
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BSDEL_ES_PAY",data.iTake[0]) //Payment for delivery
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BSDEL_ES_SER",data.iTake[1])//Service costs
						ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BSDEL_ES_DAM",data.iMissionFee)//Damages
						
						IF data.playerCut[0].iCut != 0
							ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("BSDEL_ES_SAT",data.playerCut[0].iCut)//Satisfaction
						ENDIF
						tempPlayer = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						IF tempPlayer != INVALID_PLAYER_INDEX()
							IF tempPlayer = PLAYER_ID()
								ADD_MISSION_SUMMARY_END_COMPLETION_CASH("BSDEL_ENDS_END",data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)// Your Final Payment
								//playerName = GET_PLAYER_NAME(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								//ADD_MISSION_SUMMARY_END_COMPLETION_LITERAL_STRING_AND_CASH("ROB_ENDS_ENDB",playerName,data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)
							ELSE
								
								STRING sBossType
								IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(GB_GET_LOCAL_PLAYER_GANG_BOSS())
									sBossType = "BSDEL_ES_MC" // MC President's Final Payment
								ELIF DOES_PLAYER_OWN_OFFICE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
									sBossType = "BSDEL_ES_CEO" // CEO's Final Payment
								ELSE
									sBossType = "BSDEL_ES_VIP" // VIP's Final Payment
								ENDIF

								ADD_MISSION_SUMMARY_END_COMPLETION_CASH(sBossType, data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut) // ~a~~s~ Final Payment
							ENDIF
						ELSE
							ADD_MISSION_SUMMARY_END_COMPLETION_CASH("BSDEL_ENDS_ENDC",data.iTake[0]+data.iTake[1]+data.iMissionFee+data.playerCut[0].iCut)// Final Payment
						ENDIF
					BREAK
					#ENDIF
				ENDSWITCH
			ELSE
				SWITCH data.eType
					CASE MISSION_END_SHARD_TYPE_CASINO_HEIST
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL_ALT, "CH_END_F", "CH_END_NAME")
					BREAK
					
					CASE MISSION_END_SHARD_TYPE_ISLAND_HEIST
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL_ALT, "CH_END_F", "IH_END_NAME")
					BREAK
					#IF FEATURE_TUNER
					CASE MISSION_END_SHARD_TYPE_TUNER_ROBBERY
						tempText = "ROB_ENDS_NAME"
						tempText += data.iID
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL_ALT, "ROB_ENDS_F", tempText)
					BREAK
					CASE MISSION_END_SHARD_TYPE_TUNER_DELIVERY
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL_ALT, "TDEL_ES_F", "TDEL_ES_T")
					BREAK
					#ENDIF
					#IF FEATURE_FIXER
					CASE MISSION_END_SHARD_TYPE_FIXER_PAYPHONE_HITS
						STRING sTargetString
						
						sTargetString = "FXR_BM_PH_FAI" // Target was not assassinated
						
						IF data.iEliteChallengeCash > 0 // If it has multiple targets.
							sTargetString = "FXR_BM_PH_FAI2" // Targets were not assassinated  
						ENDIF

						SETUP_NEW_BIG_MESSAGE_WITH_STRING(BIG_MESSAGE_GB_END_OF_JOB_FAIL, sTargetString, sTargetString, "FXR_BM_OVER")
					BREAK
					#ENDIF
					
				ENDSWITCH
			ENDIF
				
			//IF bClearData
			CLEAR_MISSION_END_SHARD_DATA(data)
			//ENDIF
			data.bRunOnReturnToFreemode = FALSE
			
			PRINTLN("MAINTAIN_MISSION_END_SHARD - Shard setup")
		ENDIF
	ENDIF
	
#IF IS_DEBUG_BUILD
	IF !data.bRunOnReturnToFreemode
		IF bCelebrationScript
			PRINTLN("MAINTAIN_MISSION_END_SHARD - celebrations.sc - No data setup")
		ENDIF
	ENDIF
#ENDIF
ENDPROC


//PROC MAINTAIN_ISLAND_HEIST_END_SHARD(MISSION_END_SHARD_DATA &data, BOOL bCelebrationScript = FALSE, BOOL bDelayShardStart = FALSE)
//	IF data.bRunOnReturnToFreemode
//		IF NOT NETWORK_IS_ACTIVITY_SESSION()
//		AND IS_NET_PLAYER_OK(PLAYER_ID(),TRUE)
//		AND IS_SKYSWOOP_AT_GROUND()
//		AND NOT NETWORK_IS_IN_MP_CUTSCENE() 
//		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
//		OR bCelebrationScript
//			INT i
//			TEXT_LABEL_23 tempText
//			TEXT_LABEL_31 playerName
//			
//			IF data.bPass
//				
//				BIG_MESSAGE_BIT_SET eBitSet = BIG_MESSAGE_BIT_INVALID
//				
//				IF bCelebrationScript
//					eBitSet = BIG_MESSAGE_BIT_PERSIST_OVER_CELEBRATION|BIG_MESSAGE_BIT_START_AND_PERSIST_OVER_FADE|BIG_MESSAGE_BIT_ALLOW_DELAYED_START
//					
//					IF bDelayShardStart
//						g_bDelayShardStart = TRUE
//						PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD - g_bDelayShardStart = TRUE")
//					ENDIF
//					
//					PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD - Shard bit set: ", eBitSet, " g_bDelayShardStart: ", g_bDelayShardStart)
//				ENDIF
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.bRunOnReturnToFreemode = ",data.bRunOnReturnToFreemode)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.bPass = ",data.bPass)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iTimeToComplete = ",data.iTimeToComplete)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.vehModel = ", HEIST_ISLAND_APPROACH_VEHICLES_TO_STRING(data.eApproachVeh))
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iMainTarget = ",data.iMainTarget)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iMainTake = ",data.iMainTake)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iAdditionalLootTake = ",data.iAdditionalLootTake)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iFencingFee = ",data.iFencingFee)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iPavelCut = ",data.iPavelCut)
//				REPEAT 4 i
//					PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.playerCut[",i,"].bLeader = ",data.playerCut[i].bLeader)
//					PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.playerCut[",i,"].playerID = ",NATIVE_TO_INT(data.playerCut[i].playerID))
//					PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.playerCut[",i,"].playerName = ",data.playerCut[i].playerName)
//					PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.playerCut[",i,"].iCut = ",data.playerCut[i].iCut)
//				ENDREPEAT
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iEliteChallenge = ",data.iEliteChallenge)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.iEliteChallengeCash = ",data.iEliteChallengeCash)
//				PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD: data.bPass = ",data.bPass)
//				
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"CH_END_P","IH_END_NAME",DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
//				g_bEndScreenSuppressFadeIn = TRUE
//				ADD_MISSION_SUMMARY_TITLE("IH_END_NAME")
//			
//				ADD_MISSION_SUMMARY_END_COMPLETION_TIME("IH_END_TIME",data.iTimeToComplete) //time
//				tempText = "IH_END_APV"
//				tempText += ENUM_TO_INT(data.eApproachVeh)
//				ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("IH_END_APV",tempText)//approach veh
//				tempText = "IH_END_TAR"
//				tempText += data.iMainTarget
//				ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("IH_END_TAR",tempText)//main target
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_TART",data.iMainTake)//main taget take
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_ATAR",data.iAdditionalLootTake)//additional loot take
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_FEE",data.iFencingFee)//fencing fee
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("IH_END_PAVEL",data.iPavelCut)//pavel fee
//				REPEAT 4 i
//					IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
//						IF data.playerCut[i].bLeader
//							ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
//							//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				REPEAT 4 i
//					IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
//						IF NOT data.playerCut[i].bLeader
//							ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
//							//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerID,data.playerCut[i].iCut)
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				IF data.iEliteChallenge = ISLAND_HEIST_END_ELITE_NO_TRIED
//					ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_UNCHECKED)
//				ELIF data.iEliteChallenge = ISLAND_HEIST_END_ELITE_RESTART
//					ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_INVALIDATED)
//				ELIF data.iEliteChallenge = ISLAND_HEIST_END_ELITE_COMPLETE
//					ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_CHECKED)
//				ENDIF
//				playerName = GET_PLAYER_NAME(PLAYER_ID())
//				REPEAT 4 i
//					//IF data.playerCut[i].playerID = PLAYER_ID()
//					IF ARE_STRINGS_EQUAL(playerName,data.playerCut[i].playerName)
//						ADD_MISSION_SUMMARY_END_COMPLETION_CASH("CH_END_END",data.playerCut[i].iCut+data.iEliteChallengeCash)// Your final take
//					ENDIF
//				ENDREPEAT
//			ELSE
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL_ALT, "CH_END_F", "IH_END_NAME")
//			ENDIF
//			//IF bClearData
//			CLEAR_ISLAND_HEIST_END_SHARD_DATA(data)
//			//ENDIF
//			data.bRunOnReturnToFreemode = FALSE
//			
//			PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD - Shard setup")
//		ENDIF
//	ENDIF
//	
//#IF IS_DEBUG_BUILD
//	IF !data.bRunOnReturnToFreemode
//		PRINTLN("MAINTAIN_ISLAND_HEIST_END_SHARD - celebrations.sc - No data setup")
//	ENDIF
//#ENDIF
//ENDPROC
//
//#IF IS_DEBUG_BUILD
//PROC RUN_ISLAND_HEIST_END_SCREEN_TEST()
//	islandHeistEndData.bRunOnReturnToFreemode = TRUE
//	islandHeistEndData.bPass = TRUE
//	islandHeistEndData.iTimeToComplete = 20000
//	islandHeistEndData.eApproachVeh = HIAV_SUBMARINE
//	islandHeistEndData.iMainTarget = ISLAND_HEIST_TARGET_PANTHER_STATUE
//	islandHeistEndData.iMainTake = 1000000
//	islandHeistEndData.iAdditionalLootTake  = 50000
//	islandHeistEndData.iFencingFee  = 25000
//	islandHeistEndData.iPavelCut	= 10000
//	islandHeistEndData.playerCut[0].playerID = PLAYER_ID()
//	islandHeistEndData.playerCut[0].playerName = GET_PLAYER_NAME(PLAYER_ID())
//	islandHeistEndData.playerCut[0].iCut = 10000
//	islandHeistEndData.playerCut[0].bLeader = FALSE
//	islandHeistEndData.playerCut[1].playerID = PLAYER_ID()
//	islandHeistEndData.playerCut[1].playerName = GET_PLAYER_NAME(PLAYER_ID())
//	islandHeistEndData.playerCut[1].iCut = 5000
//	islandHeistEndData.playerCut[1].bLeader = FALSE
//	islandHeistEndData.playerCut[2].playerID = PLAYER_ID()
//	islandHeistEndData.playerCut[2].playerName = GET_PLAYER_NAME(PLAYER_ID())
//	islandHeistEndData.playerCut[2].iCut = 10000
//	islandHeistEndData.playerCut[2].bLeader = TRUE
//	islandHeistEndData.playerCut[3].playerID = PLAYER_ID()
//	islandHeistEndData.playerCut[3].playerName = GET_PLAYER_NAME(PLAYER_ID())
//	islandHeistEndData.playerCut[3].iCut = 20000
//	islandHeistEndData.playerCut[3].bLeader = FALSE
//	islandHeistEndData.iEliteChallenge = ISLAND_HEIST_END_ELITE_RESTART
//	PRINTLN("RUN_ISLAND_HEIST_END_SCREEN_TEST: called this frame")
//
//	//MAINTAIN_ISLAND_HEIST_END_SHARD(data,TRUE)
//ENDPROC
//#ENDIF


//PROC MAINTAIN_CASINO_HEIST_END_SHARD(CASINO_HEIST_END_DATA &data, BOOL bCelebrationScript = FALSE, BOOL bDelayShardStart = FALSE)
//	IF data.bRunOnReturnToFreemode
//		IF NOT NETWORK_IS_ACTIVITY_SESSION()
//		AND IS_NET_PLAYER_OK(PLAYER_ID(),TRUE)
//		AND IS_SKYSWOOP_AT_GROUND()
//		AND NOT NETWORK_IS_IN_MP_CUTSCENE() 
//		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
//		OR bCelebrationScript
//			INT i
//			TEXT_LABEL_23 tempText
//			TEXT_LABEL_31 playerName
//			
//			IF data.bPass
//				
//				BIG_MESSAGE_BIT_SET eBitSet = BIG_MESSAGE_BIT_INVALID
//				
//				IF bCelebrationScript
//					eBitSet = BIG_MESSAGE_BIT_PERSIST_OVER_CELEBRATION|BIG_MESSAGE_BIT_START_AND_PERSIST_OVER_FADE|BIG_MESSAGE_BIT_ALLOW_DELAYED_START
//					
//					IF bDelayShardStart
//						g_bDelayShardStart = TRUE
//						PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD - g_bDelayShardStart = TRUE")
//					ENDIF
//					
//					PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD - Shard bit set: ", eBitSet, " g_bDelayShardStart: ", g_bDelayShardStart)
//				ENDIF
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.bRunOnReturnToFreemode = ",data.bRunOnReturnToFreemode)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.bPass = ",data.bPass)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iApproach = ",data.iApproach)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iTarget = ",data.iTarget)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iPotentialTake = ",data.iPotentialTake)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iTake = ",data.iTake)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iBuyerFee = ",data.iBuyerFee)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iLesterCut = ",data.iLesterCut)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.gunman = ",data.gunman)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iGunmanCut = ",data.iGunmanCut)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.hacker = ",data.hacker)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iHackerCut = ",data.iHackerCut)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.driver = ",data.driver)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iDriverCut = ",data.iDriverCut)
//				REPEAT 4 i
//					PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.playerCut[",i,"].bLeader = ",data.playerCut[i].bLeader)
//					PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.playerCut[",i,"].playerID = ",NATIVE_TO_INT(data.playerCut[i].playerID))
//					PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.playerCut[",i,"].playerName = ",data.playerCut[i].playerName)
//					PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.playerCut[",i,"].iCut = ",data.playerCut[i].iCut)
//				ENDREPEAT
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iEliteChallenge = ",data.iEliteChallenge)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.iEliteChallengeCash = ",data.iEliteChallengeCash)
//				PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD: data.bPass = ",data.bPass)
//				
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_HEIST_SUMMARY,"CH_END_P","CH_END_NAME",DEFAULT,DEFAULT,DEFAULT,DEFAULT,eBitSet)
//				g_bEndScreenSuppressFadeIn = TRUE
//				ADD_MISSION_SUMMARY_TITLE("CH_END_NAME")
//			
//				tempText = "CH_END_0ST"
//				tempText += data.iApproach
//				ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("CH_END_0ST",tempText)//approach
//				tempText = "CH_END_1ST"
//				tempText += data.iTarget
//				ADD_MISSION_SUMMARY_STAT_TWO_STRINGS("CH_END_1ST",tempText)//Target
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_2ST",data.iPotentialTake)//Potential take
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_3ST",data.iTake)//Take
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_10ST",data.iBuyerFee)//Buyer Laundering Fee
//				ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH("CH_END_4ST",data.iLesterCut)//Lester's cut
//				IF data.gunman != CASINO_HEIST_WEAPON_EXPERT__NONE
//					tempText = "CH_END_5ST"
//					tempText += ENUM_TO_INT(data.gunman)
//					ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(tempText,data.iGunmanCut)//(Gunman Name's) cut
//				ENDIF
//				IF data.hacker != CASINO_HEIST_HACKER__NONE
//					tempText = "CH_END_6ST"
//					tempText += ENUM_TO_INT(data.hacker)
//					ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(tempText,data.iHackerCut)//(Hacker Name's) cut
//				ENDIF
//				IF data.driver != CASINO_HEIST_DRIVER__NONE
//					tempText = "CH_END_7ST"
//					tempText += ENUM_TO_INT(data.driver)
//					ADD_MISSION_SUMMARY_STAT_STRING_AND_CASH(tempText,data.iDriverCut)//(Driver Name's) cut
//				ENDIF
//				REPEAT 4 i
//					IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
//						IF data.playerCut[i].bLeader
//							ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
//							//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				REPEAT 4 i
//					IF data.playerCut[i].playerID != INVALID_PLAYER_INDEX()
//						IF NOT data.playerCut[i].bLeader
//							ADD_MISSION_SUMMARY_STAT_LITERAL_STRING_AND_CASH(data.playerCut[i].playerName,data.playerCut[i].iCut)
//							//ADD_MISSION_SUMMARY_STAT_PLAYER_WITH_CASH(data.playerCut[i].playerID,data.playerCut[i].iCut)
//						ENDIF
//					ENDIF
//				ENDREPEAT
//				IF data.iEliteChallenge = CASINO_HEIST_END_ELITE_NO_TRIED
//					ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_UNCHECKED)
//				ELIF data.iEliteChallenge = CASINO_HEIST_END_ELITE_RESTART
//					ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_INVALIDATED)
//				ELIF data.iEliteChallenge = CASINO_HEIST_END_ELITE_COMPLETE
//					ADD_MISSION_SUMMARY_STAT_STRING_WITH_TICK_BOX("CH_END_9ST",ESCM_CHECKED)
//				ENDIF
//				playerName = GET_PLAYER_NAME(PLAYER_ID())
//				REPEAT 4 i
//					//IF data.playerCut[i].playerID = PLAYER_ID()
//					IF ARE_STRINGS_EQUAL(playerName,data.playerCut[i].playerName)
//						ADD_MISSION_SUMMARY_END_COMPLETION_CASH("CH_END_END",data.playerCut[i].iCut+data.iEliteChallengeCash)// Your final take
//					ENDIF
//				ENDREPEAT
//			ELSE
//				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_FAIL_ALT, "CH_END_F", "CH_END_NAME")
//			ENDIF
//			//IF bClearData
//			CLEAR_CASINO_HEIST_END_SHARD_DATA(data)
//			//ENDIF
//			data.bRunOnReturnToFreemode = FALSE
//			
//			PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD - Shard setup")
//		ENDIF
//
//	ENDIF
//	
//#IF IS_DEBUG_BUILD
//	IF !data.bRunOnReturnToFreemode
//		IF bCelebrationScript
//			PRINTLN("MAINTAIN_CASINO_HEIST_END_SHARD - celebrations.sc - No data setup")
//		ENDIF
//	ENDIF
//#ENDIF
//
//ENDPROC
