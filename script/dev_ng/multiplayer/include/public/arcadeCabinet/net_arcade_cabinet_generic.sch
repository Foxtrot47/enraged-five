//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arcade_cabinet_generic.sch																			//
// Description: This is the implementation of the generic arcade cabinet as a arcade cabinet. As such this does not 	//
//				expose any public functions. All functions to manipulate arcade cabinet are in net_arcade_cabinet_base	//
//				Generic cabinets are used for games which do not require child script and we only need to create the 	//
//				using arcade cabinet manager and handle player anims for the cabinet. 									//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "net_include.sch"
USING "net_drone.sch"

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet_base.sch"
USING "mp_globals_arcade_propety_consts.sch"

FUNC BOOL DOES_GENERIC_ARCADE_CABINET_HAVE_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &sChildScriptName, BOOL bCheckGameScript)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(sChildScriptName)
	IF bCheckGameScript
		SWITCH eArcadeCabinet
			CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
				sChildScriptName = "scroll_arcade_cabinet"
				RETURN TRUE
			CASE ARCADE_CABINET_CA_STREET_CRIMES
				sChildScriptName = "grid_arcade_cabinet"
				RETURN TRUE
			CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
			CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
			CASE ARCADE_CABINET_CH_PENETRATOR
				sChildScriptName = "degenatron_games"
				RETURN TRUE
			CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
				sChildScriptName = "ggsm_arcade"
				RETURN TRUE
			CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
				sChildScriptName = "gunslinger_arcade"
				RETURN TRUE
			CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
				sChildScriptName = "wizard_arcade"
				RETURN TRUE
			CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
				sChildScriptName = "road_arcade"
				RETURN TRUE
			CASE ARCADE_CABINET_CH_DRONE_MINI_G
				sChildScriptName = "AM_MP_DRONE"
				RETURN TRUE
			CASE ARCADE_CABINET_SUM_QUB3D
				sChildScriptName = "puzzle"
				RETURN TRUE	
			#IF FEATURE_HALLOWEEN_2021
			CASE ARCADE_CABINET_SE_CAMHEDZ
				sChildScriptName = "camhedz_arcade"
				RETURN TRUE
			BREAK	
			#ENDIF
		ENDSWITCH 
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_GENERIC_ARCADE_CABINET_DETAILS(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_space_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_gun_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_race_02a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_race_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_race_01b"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_wizard_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_degenatron_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Monkey_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CH_PENETRATOR
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Penetrator_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_invade_01a"))
			details.tlAudioSceneName = "dlc_vw_am_ip_in_use_scene"
		BREAK
		CASE ARCADE_CABINET_CA_STREET_CRIMES
			SWITCH details.iPropModelVar
				CASE 1		details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_street_01a"))		BREAK
				CASE 4		details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_street_01b"))		BREAK
				CASE 3		details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_street_01c"))		BREAK
				CASE 2		details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_street_01d"))		BREAK
			ENDSWITCH	
			details.tlAudioSceneName = "dlc_vw_am_tw_in_use_scene"
		BREAK	
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Fingerprint_Scanner_01a"))
			details.tlAudioSceneName = "DLC_HEIST_MINIGAME_FLEECA_DRILLING_SCENE"
		BREAK
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Fingerprint_Scanner_01a"))
		BREAK	
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			details.tlAudioSceneName = "DLC_BTL_Hacker_Drone_HUD_Scene"
		BREAK
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_QUB3D
			details.propModel = SUM_PROP_ARCADE_QUB3D_01A
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		#ENDIF
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
			details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_camhedz_01a"))
			details.tlAudioSceneName = "dlc_ch_arcade_machine_in_use_scene"
		BREAK
		#ENDIF
	ENDSWITCH 
ENDPROC	

PROC GET_GENERIC_ARCADE_CABINET_TRIGGER_DATA(ARCADE_CABINETS eArcadeCabinet, VECTOR &vOffset[])
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
		CASE ARCADE_CABINET_CH_PENETRATOR
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
		CASE ARCADE_CABINET_CA_STREET_CRIMES
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_QUB3D
		#ENDIF
			vOffset[ARCADE_CAB_PLAYER_1] = <<0.0, -0.5, 0.0>>
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			vOffset[ARCADE_CAB_PLAYER_1] = <<0.0, -0.5, 0.0>>
			vOffset[ARCADE_CAB_PLAYER_2] = <<1.7, -0.5, 0.0>>	
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			vOffset[ARCADE_CAB_PLAYER_1] = <<-1.5, -0.6, 0.0>>
			vOffset[ARCADE_CAB_PLAYER_2] = <<1.5, -0.6, 0.0>>	
		BREAK
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			vOffset[ARCADE_CAB_PLAYER_1] = <<-0.55, -0.6, 0.0>>
			vOffset[ARCADE_CAB_PLAYER_2] = <<0.55, -0.6, 0.0>>
		BREAK
	ENDSWITCH 
ENDPROC

PROC GET_GENERIC_ARCADE_CABINET_TRIGGER_HELP(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	BOOL bOwner = FALSE
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
		bOwner = TRUE
	ENDIF
	
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
			IF bOwner
				details.strTriggerHelp = "ARC_CAB_TRI_SPM"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RSPM"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			IF bOwner	
				details.strTriggerHelp = "ARC_CAB_TRI_GUNL"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RGUNL"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			IF bOwner
				details.strTriggerHelp = "ARC_CAB_TRI_NIDRB"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RNIDRB"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			IF bOwner
				details.strTriggerHelp = "ARC_CAB_TRI_NIDRC"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RNIDRC"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			IF bOwner	
				details.strTriggerHelp = "ARC_CAB_TRI_NIDRT"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RNIDRT"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
			IF bOwner	
				details.strTriggerHelp = "ARC_CAB_TRI_WIZ"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RWIZ"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
			IF bOwner
				details.strTriggerHelp = "ARC_CAB_TRI_DEF_F"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RDEF_F"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
			IF bOwner
				details.strTriggerHelp = "ARC_CAB_TRI_MONKE"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RMONKE"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_PENETRATOR
			IF bOwner	
				details.strTriggerHelp = "ARC_CAB_TRI_PENET"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_RPENET"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
			IF bOwner
				details.strTriggerHelp = "ARCCAB_ENTER_1"
			ELSE
				details.strTriggerHelp = "ARCCAB_ENTER_R1"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CA_STREET_CRIMES
			IF bOwner	
				details.strTriggerHelp = "ARCCAB_ENTER_0"
			ELSE
				details.strTriggerHelp = "ARCCAB_ENTER_R0"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			details.strTriggerHelp = "ARC_CAB_TRI_HACKG"
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			details.strTriggerHelp = "ARC_CAB_TRI_DRILL"
		BREAK
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			details.strTriggerHelp = "ARC_CAB_TRI_DRONE"
		BREAK
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			IF bOwner	
				details.strTriggerHelp = "ARC_CAB_TRI_CAMHEDZ"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_CAMHEDZ_R"
			ENDIF
		BREAK
		CASE ARCADE_CABINET_SUM_QUB3D
			IF bOwner	
				details.strTriggerHelp = "ARC_CAB_TRI_QUB3D"
			ELSE
				details.strTriggerHelp = "ARC_CAB_TRI_QUB3D_F"
			ENDIF
		BREAK
	ENDSWITCH 
ENDPROC

PROC GET_GENERIC_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iAnimVariation, ARCADE_CABINET_ANIM_DETAIL &eAnimDetails)

	SWITCH eArcadeCabinet	
		// shared anim detail 
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
		CASE ARCADE_CABINET_SUM_QUB3D
			SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
				CASE ARC_CAB_VARIATION_1
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@MALE@LEFT@"
					ELSE
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@FEMALE@LEFT@"
					ENDIF	
				BREAK	
				CASE ARC_CAB_VARIATION_2
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@MALE@RIGHT@"
					ELSE
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@FEMALE@RIGHT@"
					ENDIF	
				BREAK	
			ENDSWITCH 
			
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY			eAnimDetails.sAnimClipName =  "enter"		BREAK	
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE			eAnimDetails.sAnimClipName =  "idle"		BREAK	
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT			eAnimDetails.sAnimClipName =  "exit"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE		eAnimDetails.sAnimClipName =  "PLAYIDLE"	BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2	eAnimDetails.sAnimClipName =  "PLAYIDLE_V2"	BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE			eAnimDetails.sAnimClipName =  "LOSE"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE_BIG		eAnimDetails.sAnimClipName =  "LOSE_BIG"	BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN			eAnimDetails.sAnimClipName =  "WIN"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN_BIG		eAnimDetails.sAnimClipName =  "WIN_BIG"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_KICK			eAnimDetails.sAnimClipName =  "kick"		BREAK
			ENDSWITCH																					
		BREAK																							
		
		// Last gunlingers anim detail 
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
				CASE ARC_CAB_VARIATION_1
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LIGHT_GUN@MALE@LEFT@"
					ELSE
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LIGHT_GUN@FEMALE@LEFT@"
					ENDIF
				BREAK	
				CASE ARC_CAB_VARIATION_2
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LIGHT_GUN@MALE@RIGHT@"
					ELSE
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LIGHT_GUN@FEMALE@RIGHT@"
					ENDIF	
				BREAK	
			ENDSWITCH 
			
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY								eAnimDetails.sAnimClipName =  "enter"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE								eAnimDetails.sAnimClipName =  "idle"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE							eAnimDetails.sAnimClipName =  "playidle"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2						eAnimDetails.sAnimClipName =  "playidle_b"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V3						eAnimDetails.sAnimClipName =  "playidle_c"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V4						eAnimDetails.sAnimClipName =  "playidle_d"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT								eAnimDetails.sAnimClipName =  "exit_from_idle"	BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_IDLE_TO_AIM		eAnimDetails.sAnimClipName =  "idle_to_aim"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_AIM_IDLE			eAnimDetails.sAnimClipName =  "aimidle"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LOSE_LIFE		eAnimDetails.sAnimClipName =  "lose_life"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_LEVEL_COMPLETE	eAnimDetails.sAnimClipName =  "level_complete"	BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN								eAnimDetails.sAnimClipName =  "win"				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE								eAnimDetails.sAnimClipName =  "lose"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM	eAnimDetails.sAnimClipName =  "exit_from_aimed"	BREAK
			ENDSWITCH
		BREAK
		
		// Night drive car anim detail 
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			IF !IS_PLAYER_FEMALE()
				eAnimDetails.strArcCabinetAnimDic = "anim_heist@arcade@driving@car@male@"
			ELSE
				eAnimDetails.strArcCabinetAnimDic = "anim_heist@arcade@driving@car@female@"
			ENDIF
			
			eAnimDetails.iNumPropClips = 1
			
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY			
					SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
						CASE ARC_CAB_VARIATION_1
							eAnimDetails.sAnimClipName = "enter_left_a"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_enter_left_a"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_enter_left_a"
						BREAK
						CASE ARC_CAB_VARIATION_2
							eAnimDetails.sAnimClipName =  "enter_right_a"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_enter_right_a"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_enter_right_a"
						BREAK
					ENDSWITCH	
				BREAK			
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE								
					eAnimDetails.sAnimClipName = "idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_idle"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT			
					SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
						CASE ARC_CAB_VARIATION_1
							eAnimDetails.sAnimClipName = "exit_left"	
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_exit_left"	
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_exit_left"	
						BREAK
						CASE ARC_CAB_VARIATION_2
							eAnimDetails.sAnimClipName = "exit_right"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_exit_right"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_exit_right"
						BREAK
					ENDSWITCH
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_IDLE_TO_RACE		 
					eAnimDetails.sAnimClipName = "idle_to_race"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_idle_to_race"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_idle_to_race"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING			
					eAnimDetails.sAnimClipName = "racing_a"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_a"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_a"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD		
					eAnimDetails.sAnimClipName = "take_lead"		
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_take_lead"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_take_lead"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD		 
					eAnimDetails.sAnimClipName = "lose_lead"		
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_lose_lead"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_lose_lead"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN								
					eAnimDetails.sAnimClipName = "win"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_win"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_win"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE								
					eAnimDetails.sAnimClipName = "lose"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_lose"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_lose"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_ENTER_B		
					SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
						CASE ARC_CAB_VARIATION_1
							eAnimDetails.sAnimClipName = "enter_left_b"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_enter_left_b"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_enter_left_b"
						BREAK
						CASE ARC_CAB_VARIATION_2
							eAnimDetails.sAnimClipName =  "enter_right_b"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_enter_right_b"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_enter_right_b"
						BREAK
					ENDSWITCH	
				BREAK	
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE	
					eAnimDetails.sAnimClipName = "race_to_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_race_to_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_race_to_idle"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_A		
					eAnimDetails.sAnimClipName = "racing_a"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_a"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_a"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_B	
					eAnimDetails.sAnimClipName = "racing_b"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_b"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_b"		
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_C		
					eAnimDetails.sAnimClipName = "racing_c"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_c"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_c"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_D		
					eAnimDetails.sAnimClipName = "racing_d"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_d"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_d"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_E		
					eAnimDetails.sAnimClipName = "racing_e"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_e"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_e"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_F		
					eAnimDetails.sAnimClipName = "racing_f"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_f"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_f"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_G		
					eAnimDetails.sAnimClipName = "racing_g"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_g"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_g"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING_H		
					eAnimDetails.sAnimClipName = "racing_h"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_car_racing_h"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_car_racing_h"	
				BREAK
			ENDSWITCH	
		BREAK
		
		// Night drive bike anim detail 
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			IF !IS_PLAYER_FEMALE()
				eAnimDetails.strArcCabinetAnimDic = "anim_heist@arcade@driving@bike@male@"
			ELSE
				eAnimDetails.strArcCabinetAnimDic = "anim_heist@arcade@driving@bike@female@"
			ENDIF
			
			eAnimDetails.iNumPropClips = 1		
					
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY			
					SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
						CASE ARC_CAB_VARIATION_1
							eAnimDetails.sAnimClipName = "enter_left"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] =  "prop_enter_left"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] =  "prop_enter_left"
						BREAK
						CASE ARC_CAB_VARIATION_2
							eAnimDetails.sAnimClipName = "enter_right"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] =  "prop_enter_right"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] =  "prop_enter_right"
						BREAK
					ENDSWITCH	
				BREAK			
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE						
					eAnimDetails.sAnimClipName =  "idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT			
					SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
						CASE ARC_CAB_VARIATION_1
							eAnimDetails.sAnimClipName = "exit_left"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] =  "prop_exit_left"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] =  "prop_exit_left"
						BREAK
						CASE ARC_CAB_VARIATION_2
							eAnimDetails.sAnimClipName = "exit_right"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] =  "prop_exit_right"
							eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] =  "prop_exit_right"
						BREAK
					ENDSWITCH
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_IDLE_TO_RACE	
					eAnimDetails.sAnimClipName =  "idle_to_race"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle_to_race"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle_to_race"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACING			
					eAnimDetails.sAnimClipName =  "racing"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_racing"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_racing"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_TAKE_LEAD		
					eAnimDetails.sAnimClipName =  "take_lead"		
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_take_lead"		
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_take_lead"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD		
					eAnimDetails.sAnimClipName =  "lose_lead"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_lose_lead"		
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_lose_lead"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN								
					eAnimDetails.sAnimClipName =  "win"		
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] =  "prop_win"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] =  "prop_win"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE								
					eAnimDetails.sAnimClipName =  "lose"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_lose"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_lose"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_TO_RACE_V2	
					eAnimDetails.sAnimClipName =  "idle_to_race_v2"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle_to_race_v2"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle_to_race_v2"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V2	
					eAnimDetails.sAnimClipName =  "idle_v2"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle_v2"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle_v2"	
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V3	
					eAnimDetails.sAnimClipName =  "idle_v3"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle_v3"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle_v3"
				BREAK		
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_IDLE_V4	
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.sAnimClipName =  "idle_v4"	
						eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle_v4"			
						eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle_v4"	
					ELSE
						eAnimDetails.sAnimClipName =  "idle_v3"	
						eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_idle_v3"			
						eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_idle_v3"
					ENDIF
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_LOSE_LEAD_V2
					eAnimDetails.sAnimClipName =  "lose_lead_v2"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_lose_lead_v2"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_lose_lead_v2"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_RACING_V2
					eAnimDetails.sAnimClipName =  "racing_v2"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_racing_v2"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_racing_v2"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_RACING_V3	
					eAnimDetails.sAnimClipName =  "racing_v3"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_racing_v3"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_racing_v3"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_BIKE_TAKE_LEAD_V2		
					eAnimDetails.sAnimClipName =  "take_lead_v2"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_take_lead_v2"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_take_lead_v2"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE	
					eAnimDetails.sAnimClipName = "race_to_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "prop_race_to_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "prop_race_to_idle"
				BREAK
			ENDSWITCH															
		BREAK
		
		//  Defenders of the faith anim detail
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE	
		CASE ARCADE_CABINET_CH_PENETRATOR
			SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
				CASE ARC_CAB_VARIATION_1
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@MALE@LEFT@"
					ELSE
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@FEMALE@LEFT@"
					ENDIF	
				BREAK	
				CASE ARC_CAB_VARIATION_2
					IF !IS_PLAYER_FEMALE()
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@MALE@RIGHT@"
					ELSE
						eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@SHARED@FEMALE@RIGHT@"
					ENDIF	
				BREAK	
			ENDSWITCH 
			
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY			
					IF !IS_ARCADE_CABINET_GLITCHED()
						eAnimDetails.sAnimClipName =  "enter"			
					ELSE
						eAnimDetails.sAnimClipName =  "kick"			
					ENDIF
				BREAK	
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE			eAnimDetails.sAnimClipName =  "idle"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_KICK			eAnimDetails.sAnimClipName =  "kick"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT			eAnimDetails.sAnimClipName =  "exit"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE		eAnimDetails.sAnimClipName =  "PLAYIDLE"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2	eAnimDetails.sAnimClipName =  "PLAYIDLE_V2"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE			eAnimDetails.sAnimClipName =  "LOSE"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE_BIG		eAnimDetails.sAnimClipName =  "LOSE_BIG"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN			eAnimDetails.sAnimClipName =  "WIN"				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN_BIG		eAnimDetails.sAnimClipName =  "WIN_BIG"			BREAK
			ENDSWITCH																						
		BREAK																								
		
		// IP anim detail
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
			IF !IS_PLAYER_FEMALE()
				eAnimDetails.strArcCabinetAnimDic = "anim_casino_a@amb@casino@games@arcadecabinet@maleright"
			ELSE
				eAnimDetails.strArcCabinetAnimDic = "anim_casino_a@amb@casino@games@arcadecabinet@femaleright"
			ENDIF
	
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY			eAnimDetails.sAnimClipName =  "enter"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE			eAnimDetails.sAnimClipName =  "idle"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT			eAnimDetails.sAnimClipName =  "exit"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE		eAnimDetails.sAnimClipName =  "playidle"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2	eAnimDetails.sAnimClipName =  "playidle_v2"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE			eAnimDetails.sAnimClipName =  "lose"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE_BIG		eAnimDetails.sAnimClipName =  "lose_big"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN			eAnimDetails.sAnimClipName =  "win"				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN_BIG		eAnimDetails.sAnimClipName =  "win_big"			BREAK
			ENDSWITCH																						
		BREAK
		
		// Street crime anim detail
		CASE ARCADE_CABINET_CA_STREET_CRIMES
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			IF iAnimVariation % 2 = 0
				IF !IS_PLAYER_FEMALE()
					eAnimDetails.strArcCabinetAnimDic = "anim_casino_a@amb@casino@games@arcadecabinet@maleright"
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "anim_casino_a@amb@casino@games@arcadecabinet@femaleright"
				ENDIF
			ELSE
				IF !IS_PLAYER_FEMALE()
					eAnimDetails.strArcCabinetAnimDic = "anim_casino_a@amb@casino@games@arcadecabinet@maleleft"
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "anim_casino_a@amb@casino@games@arcadecabinet@femaleleft"
				ENDIF
			ENDIF
			
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY				eAnimDetails.sAnimClipName = 	 "enter"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE				eAnimDetails.sAnimClipName = 	 "idle"				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT				eAnimDetails.sAnimClipName = 	 "exit"				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE			eAnimDetails.sAnimClipName = 	 "playidle"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2		eAnimDetails.sAnimClipName = 	 "playidle_v2"		BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_WIN_BIG			eAnimDetails.sAnimClipName = 	 "win_big"			BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LOSE_BIG			eAnimDetails.sAnimClipName = 	 "lose_big"			BREAK
			ENDSWITCH																								
		BREAK
		
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			IF !IS_PLAYER_FEMALE()
				eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@HS3F@IG1_HACK_KEYPAD@ARCADE@MALE@"
			ELSE
				IF !IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@HS3F@IG1_HACK_KEYPAD@ARCADE@MALE@"
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@HS3F@IG1_HACK_KEYPAD@ARCADE@FEMALE@"
				ENDIF	
			ENDIF
			
			eAnimDetails.iNumPropClips = 2
			
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY				
					eAnimDetails.sAnimClipName 						= "action_var_01"				
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "action_var_01_ch_prop_ch_usb_drive01x"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "action_var_01_prop_phone_ing"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_IDLE				
					eAnimDetails.sAnimClipName 						= "hack_loop_var_01"			
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "hack_loop_var_01_ch_prop_ch_usb_drive01x"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "hack_loop_var_01_prop_phone_ing"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT				
					eAnimDetails.sAnimClipName 						= "exit"					
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "exit_ch_prop_ch_usb_drive01x"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "exit_prop_phone_ing"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_HACKING_FAIL			
					eAnimDetails.sAnimClipName 						= "fail_react"				
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "fail_react_ch_prop_ch_usb_drive01x"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "fail_react_prop_phone_ing"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_HACKING_PASS		
					eAnimDetails.sAnimClipName 						= "success_react_exit_var_01"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "success_react_exit_var_01_ch_prop_ch_usb_drive01x"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "success_react_exit_var_01_prop_phone_ing"
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_HACKING_WAIT_IDLE
					eAnimDetails.sAnimClipName 						= "wait_idle"	
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_1] = "wait_idle_ch_prop_ch_usb_drive01x"
					eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_2] = "wait_idle_prop_phone_ing"
				BREAK
			ENDSWITCH	
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			SWITCH INT_TO_ENUM(ARCADE_CABINET_ANIM_VARIATION, iAnimVariation)
				CASE ARC_CAB_VARIATION_1
					eAnimDetails.strArcCabinetAnimDic = "anim@heists@fleeca_bank@drilling"
				BREAK	
				CASE ARC_CAB_VARIATION_2
					eAnimDetails.strArcCabinetAnimDic = "anim@heists@fleeca_bank@drilling"
				BREAK
			ENDSWITCH	
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY				eAnimDetails.sAnimClipName = 	 "intro"			BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH	
ENDPROC

FUNC BOOL SHOULD_ALL_GENERIC_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_PLAYERS_GENERIC_ARCADE_CAN_HAVE(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
		CASE ARCADE_CABINET_CH_PENETRATOR
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
		CASE ARCADE_CABINET_CA_STREET_CRIMES
		CASE ARCADE_CABINET_SUM_QUB3D
			RETURN ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			RETURN ENUM_TO_INT(ARCADE_CAB_PLAYER_2)
		BREAK
	ENDSWITCH 
	RETURN ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
ENDFUNC

/// PURPOSE:
///    Launch IP game script
FUNC BOOL LAUNCH_INVADE_AND_PERSUADE_GAME(ARCADE_CAB_GROUP eArcadeCabinetGroup)
	
	MP_MISSION_DATA missionData
	
	missionData.mdID.idMission = eAM_SCROLLING_ARCADE_CABINET
	missionData.iInstanceId = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT() + ARCADE_PROPERTY_ARCADE_GAME_SCRIPT_INSTANCE_OFFSET + ENUM_TO_INT(eArcadeCabinetGroup)
	
	g_sCasinoArcadeInvadeAndPersuadeVars.ePlayerState = CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_INTRO
	
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("scroll_arcade_cabinet", missionData.iInstanceId, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_ARCADE_CABINET_GAME - LAUNCHED Invade and Persuade II instance: ", missionData.iInstanceId)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Launch street crime game 
FUNC BOOL LAUNCH_STREET_CRIME_GAME(ARCADE_CAB_GROUP eArcadeCabinetGroup, ARCADE_CABINET_PROPERTY ePropertyType)
	
	MP_MISSION_DATA missionData
	TEXT_LABEL_63 ScriptName = "grid_arcade_cabinet"
	
	missionData.mdID.idMission = eAM_SCGW_CABINET
	missionData.iInstanceId = ((NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT() * GET_MAX_NUM_ARCADE_CABINET_GROUP(ARCADE_CABINET_CA_STREET_CRIMES)) + ARCADE_PROPERTY_ARCADE_GAME_SCRIPT_INSTANCE_OFFSET) + ENUM_TO_INT(eArcadeCabinetGroup)
	
	INT iDisplaySlotLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	IF iDisplaySlotLocatePlayerIsIn != -1 
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			ARCADE_CABINETS_SAVE_SLOT_TYPE eSaveSlotType
			eSaveSlotType =  GET_ARCADE_CABINET_SAVE_SLOT_TYPE(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlotLocatePlayerIsIn), ePropertyType)
			SWITCH eSaveSlotType
				CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_A
					missionData.mdGenericInt = 0
				BREAK
				CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_B
					missionData.mdGenericInt = 3
				BREAK
				CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_C
					missionData.mdGenericInt = 2
				BREAK
				CASE ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_D
					missionData.mdGenericInt = 1
				BREAK
			ENDSWITCH	
		ENDIF	
	ELSE
		PRINTLN("[AM_ARC_CAB] - LAUNCH_STREET_CRIME_GAME - iDisplaySlotLocatePlayerIsIn = -1")
	ENDIF
	
	g_sCasinoArcadeStreetCrimeVars.eThisFrameAction = CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_NONE
	g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_INTRO
	
	IF NOT NETWORK_IS_SCRIPT_ACTIVE(ScriptName, missionData.iInstanceId, TRUE)
		REQUEST_SCRIPT(ScriptName)
		IF HAS_SCRIPT_LOADED(ScriptName)
			START_NEW_SCRIPT_WITH_ARGS(ScriptName, missionData, SIZE_OF(missionData), GET_MP_MISSION_STACK_SIZE(missionData.mdID.idMission) )
			SET_SCRIPT_AS_NO_LONGER_NEEDED(ScriptName)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_STREET_CRIME_GAME - LAUNCHED Street Crime: Gang Wars Edition instance: ", missionData.iInstanceId, " missionData.mdGenericInt: ", missionData.mdGenericInt)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_DEFENDER_OF_THE_FAITH_GAME()
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("degenatron_games", -1, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT("degenatron_games", SCRIPT_XML_STACK_SIZE)
			MPGlobalsAmbience.iDegenatronGame = ENUM_TO_INT(DEGENATRON_GAMES_DEFENDER)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_DEFENDER_OF_THE_FAITH_GAME - launched degenatron_games")
			RETURN TRUE
		ENDIF
	ELSE
		MPGlobalsAmbience.iDegenatronGame = ENUM_TO_INT(DEGENATRON_GAMES_DEFENDER)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_MONKEYS_PARAGISE_GAME()
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("degenatron_games", -1, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT("degenatron_games", SCRIPT_XML_STACK_SIZE)
			MPGlobalsAmbience.iDegenatronGame = ENUM_TO_INT(DEGENATRON_GAMES_MONKEY)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_MONKEYS_PARAGISE_GAME - launched degenatron_games")
			RETURN TRUE
		ENDIF
	ELSE
		MPGlobalsAmbience.iDegenatronGame = ENUM_TO_INT(DEGENATRON_GAMES_MONKEY)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_PENETRATOR_GAME()
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("degenatron_games", -1, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT("degenatron_games", SCRIPT_XML_STACK_SIZE)
			MPGlobalsAmbience.iDegenatronGame = ENUM_TO_INT(DEGENATRON_GAMES_PENETRATOR)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_PENETRATOR_GAME - launched degenatron_games")
			RETURN TRUE
		ENDIF
	ELSE 
		MPGlobalsAmbience.iDegenatronGame = ENUM_TO_INT(DEGENATRON_GAMES_PENETRATOR)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_GG_SPACE_MONKEY_GAME()
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("ggsm_arcade", -1, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT("ggsm_arcade", SCRIPT_XML_STACK_SIZE)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_GG_SPACE_MONKEY_GAME - launched ggsm_arcade")
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_LAST_GUNSLINGERS_GAME(ARCADE_CAB_GROUP eArcadeCabinetGroup)
	MP_MISSION_DATA missionData
	missionData.iInstanceId = (NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT() * GET_MAX_NUM_ARCADE_CABINET_GROUP(ARCADE_CABINET_CH_LAST_GUNSLINGERS)) + ENUM_TO_INT(eArcadeCabinetGroup)
	missionData.mdID.idMission = eAM_TLG_ARCADE_CABINET
	
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("gunslinger_arcade", missionData.iInstanceId, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_LAST_GUNSLINGERS_GAME - launched gunslinger_arcade instance: ", missionData.iInstanceId)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_WIZARDS_SLEVE_GAME()
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("wizard_arcade", -1, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT("wizard_arcade", SCRIPT_XML_STACK_SIZE)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_WIZARDS_SLEVE_GAME - launched wizard_arcade")
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_RACING_GAME(RACE_AND_CHASE_GAMES eRaceGameType, ARCADE_CAB_GROUP eArcadeCabinetGroup)
	MP_MISSION_DATA missionData
	missionData.mdUniqueID = ENUM_TO_INT(eRaceGameType)
	missionData.iInstanceId =(NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT() * GET_MAX_NUM_ARCADE_CABINET_GROUP(ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE)) + ENUM_TO_INT(eArcadeCabinetGroup)

	STRING sScriptName = "road_arcade"
	IF DOES_SCRIPT_EXIST(sScriptName)
		REQUEST_SCRIPT(sScriptName)
		IF HAS_SCRIPT_LOADED(sScriptName)
			IF NOT NETWORK_IS_SCRIPT_ACTIVE(sScriptName, missionData.iInstanceId, TRUE)
				THREADID thread = START_NEW_SCRIPT_WITH_ARGS(sScriptName, missionData, SIZE_OF(missionData), MULTIPLAYER_MISSION_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED(sScriptName)
				
				IF NATIVE_TO_INT(thread) > 0 
					PRINTLN("[AM_ARC_CAB] - LAUNCH_RACING_GAME - LAUNCHED racing game instance: ", missionData.iInstanceId, " missionData.mdUniqueID: ", missionData.mdUniqueID)
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF
	ENDIF	

	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_QUB3D_GAME(ARCADE_CABINET_PROPERTY eProperty)
	MP_MISSION_DATA missionData
	missionData.mdID.idMission = eAM_QUB3D_ARCADE_CABINET
	missionData.mdGenericInt = ENUM_TO_INT(eProperty)
	
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("puzzle", -1, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_QUB3D_GAME - launched qu3d instance: ", missionData.iInstanceId)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_CAMHEDZ_ARCADE_GAME(ARCADE_CAB_GROUP eArcadeCabinetGroup)
	MP_MISSION_DATA missionData
	missionData.iInstanceId = (NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT() * GET_MAX_NUM_ARCADE_CABINET_GROUP(ARCADE_CABINET_SE_CAMHEDZ)) + ENUM_TO_INT(eArcadeCabinetGroup)
	missionData.mdID.idMission = eAM_CAMHEDZ_ARCADE_CABINET
	
	IF NOT NETWORK_IS_SCRIPT_ACTIVE("camhedz_arcade", missionData.iInstanceId, TRUE)
		IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
			PRINTLN("[AM_ARC_CAB] - LAUNCH_CAMHEDZ_ARCADE_GAME - launched camhedz_arcade instance: ", missionData.iInstanceId)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL LAUNCH_DRONE_MINIGAME_GAME()
	SIMPLE_INTERIORS ownedSimpleInterior =  GET_OWNED_ARCADE_SIMPLE_INTERIOR(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
	VECTOR vStartCoord
		SWITCH	ownedSimpleInterior
			CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY		vStartCoord = <<-245.64, 6210.96, 45.94>>	BREAK
			CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED		vStartCoord =  <<1695.88, 4783.87, 52.02>>	BREAK
			CASE SIMPLE_INTERIOR_ARCADE_DAVIS			vStartCoord =  <<-115.15, -1771.65, 40.86>>	BREAK
			CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD	vStartCoord =  <<-600.96, 280.47, 90.04>>	BREAK
			CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS	vStartCoord =  <<-1269.72, -304.09, 45.00>>	BREAK
			CASE SIMPLE_INTERIOR_ARCADE_LA_MESA			vStartCoord =  <<758.46, -814.57, 35.30>>	BREAK
		ENDSWITCH
	
	RETURN START_DRONE(DRONE_TYPE_HEIST_FREEMODE, <<0,0,0>>, vStartCoord, <<0,0,0>>)	
ENDFUNC

FUNC BOOL MAINTAIN_GENERIC_ARCADE_CABINET_GAME_SCRIPT_LAUNCH(ARCADE_CABINETS eArcadeCabinet, ARCADE_CAB_GROUP eArcadeCabinetGroup, ARCADE_CABINET_PROPERTY eProperty)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
			RETURN LAUNCH_INVADE_AND_PERSUADE_GAME(eArcadeCabinetGroup)
		BREAK
		CASE ARCADE_CABINET_CA_STREET_CRIMES
			RETURN LAUNCH_STREET_CRIME_GAME(eArcadeCabinetGroup, eProperty)
		BREAK
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
			RETURN LAUNCH_DEFENDER_OF_THE_FAITH_GAME()
		BREAK	
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
			RETURN LAUNCH_MONKEYS_PARAGISE_GAME()
		BREAK	
		CASE ARCADE_CABINET_CH_PENETRATOR
			RETURN LAUNCH_PENETRATOR_GAME()
		BREAK
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
			RETURN LAUNCH_GG_SPACE_MONKEY_GAME()
		BREAK
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			RETURN LAUNCH_LAST_GUNSLINGERS_GAME(eArcadeCabinetGroup)
		BREAK
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
			RETURN LAUNCH_WIZARDS_SLEVE_GAME()
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			RETURN LAUNCH_RACING_GAME(RACE_AND_CHASE_GAMES_BIKE, eArcadeCabinetGroup)
		BREAK	
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			RETURN LAUNCH_RACING_GAME(RACE_AND_CHASE_GAMES_CAR, eArcadeCabinetGroup)
		BREAK	
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			RETURN LAUNCH_RACING_GAME(RACE_AND_CHASE_GAMES_TRUCK, eArcadeCabinetGroup)
		BREAK
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			RETURN LAUNCH_DRONE_MINIGAME_GAME()
		BREAK
		CASE ARCADE_CABINET_SUM_QUB3D
			RETURN LAUNCH_QUB3D_GAME(eProperty)
		BREAK
#IF FEATURE_HALLOWEEN_2021
		CASE ARCADE_CABINET_SE_CAMHEDZ
			RETURN LAUNCH_CAMHEDZ_ARCADE_GAME(eArcadeCabinetGroup)
		BREAK
#ENDIF
	ENDSWITCH 
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINETS arcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip)
	ARCADE_CABINET_ANIM_DETAIL sAnimClip
	
	IF arcadeCabinet != ARCADE_CABINET_INVALID
		GET_GENERIC_ARCADE_CABINET_ANIMATION_DETAIL(arcadeCabinet, eClip, 0, sAnimClip)
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip.sAnimClipName)
			AND NOT IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sAnimClipName)
				
					FLOAT fExitAnimPhase = 0.96
					INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(g_ArcadeCabinetManagerData.iSyncSceneID)
					
					#IF IS_DEBUG_BUILD
					IF g_DebugArcadeCabinetManagerData.bAnimFinishDebugInfo
						DRAW_DEBUG_TEXT_2D(sAnimClip.sAnimClipName, <<0.5,0.45,0.45>>)	
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)), <<0.5,0.5,0.5>>)	
					ENDIF
					#ENDIF
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
						OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLEND_OUT"))
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF	
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIPS eAnimClip)
	IF !IS_ENTITY_DEAD(PLAYER_PED_ID())
		INT iLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		IF iLocatePlayerIsIn > -1
			ARCADE_CABINET_ANIM_DETAIL eAnimDetails
			ARCADE_CABINETS eCabinetPlayerIsIn = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eArcadeCabLocatePlayerIn
			IF eCabinetPlayerIsIn != ARCADE_CABINET_INVALID
				GET_GENERIC_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetPlayerIsIn, eAnimClip, globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadePlayerSlot, eAnimDetails)
				IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sAnimClipName)
				AND !IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
					RETURN IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sAnimClipName)
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_STREET_CRIME_GAME_ANIMATION(ARCADE_CABINETS eCabinetLocatePlayerIsIn , ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eCabinetLocatePlayerIsIn)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eStreetCrimePlayerState != g_sCasinoArcadeStreetCrimeVars.ePlayerState
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eStreetCrimePlayerState = g_sCasinoArcadeStreetCrimeVars.ePlayerState
		ENDIF
	ENDIF	
	
	ARCADE_CABINET_ANIM_DETAIL sAnimClip
	
	SWITCH g_sCasinoArcadeStreetCrimeVars.eThisFrameAction
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_1
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_2
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_3
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_PLAYER_4
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_BY_VEHICLE
			IF NOT IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
			AND NOT IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
			AND details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_LOSE_BIG
				IF IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)
				OR IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2)
					IF NOT HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer)
					OR (HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer) AND HAS_NET_TIMER_EXPIRED(details.sMiniGameAnimTimer, 1500))
						SET_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
						RESET_NET_TIMER(details.sMiniGameAnimTimer)
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM TRUE")
					ENDIF	
				ENDIF	
			ENDIF		
		BREAK
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_1
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_2
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_3
		CASE CASINO_ARCADE_STREET_CRIME_THIS_FRAME_ACTION_KILLED_PLAYER_4
			IF NOT IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
			AND NOT IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
			AND details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_WIN_BIG
				IF IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)
				OR IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2)
					IF NOT HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer)
					OR (HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer) AND HAS_NET_TIMER_EXPIRED(details.sMiniGameAnimTimer, 1500))
						SET_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
						RESET_NET_TIMER(details.sMiniGameAnimTimer)
						PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM TRUE")
					ENDIF
				ENDIF	
			ENDIF	
		BREAK
	ENDSWITCH	
	
	SWITCH g_sCasinoArcadeStreetCrimeVars.ePlayerState
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_READY_UP
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_COUNTDOWN
			INT iDisplaySlot
			iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
			IF iDisplaySlot != -1
				
				GET_GENERIC_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINET_CA_STREET_CRIMES, details.sArcadeCabinetAnimEvent.ePreviousAnimClip, 0, sAnimClip)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip.sAnimClipName)
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sAnimClipName)
					AND HAS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_CA_STREET_CRIMES, details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
						details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
						details.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
						IF GET_RANDOM_BOOL()
							PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)	
						ELSE
							PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2)
						ENDIF
						
						IF g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING
							CLEAR_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
							PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM from playing FALSE")
							CLEAR_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
							PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM from playing FALSE")
						ENDIF	
					ENDIF	
				ENDIF	
			ENDIF	
		BREAK
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_NOT_PLAYING
//			details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
//			details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
//			PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)		
		BREAK
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_GAME_END_WON
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_ROUND_END_WON
			IF NOT IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
				IF NOT HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer)
				OR (HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer) AND HAS_NET_TIMER_EXPIRED(details.sMiniGameAnimTimer, 1500))
					SET_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM  2 TRUE")
				ENDIF	
			ENDIF	
		BREAK
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_GAME_END_LOST
		CASE CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_ROUND_END_LOST
			IF NOT HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer)
			OR (HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer) AND HAS_NET_TIMER_EXPIRED(details.sMiniGameAnimTimer, 1500))
				IF NOT IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
					SET_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM  2 TRUE")
				ENDIF	
			ENDIF	
		BREAK
	ENDSWITCH
	
	IF IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
		IF details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_LOSE_BIG
		AND NOT HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer)
			details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
			PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_LOSE_BIG)
			START_NET_TIMER(details.sMiniGameAnimTimer)
		ELSE
			IF IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
			AND HAS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_CA_STREET_CRIMES, details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
				details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				details.sArcadeCabinetAnimEvent.bLoopAnim = TRUE

				IF GET_RANDOM_BOOL()
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)	
				ELSE
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2)
				ENDIF
				
				IF g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING
					CLEAR_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_LOSE_ANIM FALSE")
				ENDIF	
			ENDIF
		ENDIF	
	ELIF IS_BIT_SET(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
		IF details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_WIN_BIG
		AND NOT HAS_NET_TIMER_STARTED(details.sMiniGameAnimTimer)
			details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
			PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_WIN_BIG)
			START_NET_TIMER(details.sMiniGameAnimTimer)
		ELSE
			IF IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
			AND HAS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_CA_STREET_CRIMES, details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
				details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				details.sArcadeCabinetAnimEvent.bLoopAnim = TRUE

				IF GET_RANDOM_BOOL()
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)	
				ELSE
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2)
				ENDIF
				
				IF g_sCasinoArcadeStreetCrimeVars.ePlayerState = CASINO_ARCADE_STREET_CRIME_PLAYER_STATE_PLAYING
					CLEAR_BIT(details.iLocalBS, ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_STREET_CRIME_GAME_ANIMATION ARCADE_CABINET_MANAGER_LOCAL_BS_STREET_CRIME_WIN_ANIM FALSE")
				ENDIF	
			ENDIF
		ENDIF		
	ENDIF
	
ENDPROC

PROC MAINTAIN_INVADE_AND_PERSUADE_GAME_ANIMATION(ARCADE_CABINETS eCabinetLocatePlayerIsIn , ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eCabinetLocatePlayerIsIn)
	INT iDisplaySlot
	ARCADE_CABINET_ANIM_DETAIL sAnimClip
	SWITCH g_sCasinoArcadeInvadeAndPersuadeVars.ePlayerState
		CASE CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_PLAYING
		CASE CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_GAMEOVER_SCREEN
		CASE CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_STAGE_LOADING
		    iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
			IF iDisplaySlot != -1
				
				GET_GENERIC_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINET_CA_STREET_CRIMES, ARCADE_CABINET_ANIM_CLIP_IDLE, 0, sAnimClip)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip.sAnimClipName)
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sAnimClipName)
					AND HAS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_CA_INVADE_AND_PERSUADE, ARCADE_CABINET_ANIM_CLIP_IDLE)
					AND details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE
					AND details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2
						details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
						details.sArcadeCabinetAnimEvent.bLoopAnim = TRUE
						IF GET_RANDOM_BOOL()
							PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE)	
						ELSE
							PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_PLAY_IDLE_V2)
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF	
		BREAK
		CASE CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_NOT_PLAYING
			//IF HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIP_ENTRY)
//				details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
//				details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
//				PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
			//ENDIF	
		BREAK
		CASE CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_DEAD
			IF details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_LOSE
			AND details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_LOSE_BIG
				details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				IF GET_RANDOM_BOOL()
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_LOSE)	
				ELSE
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_LOSE_BIG)
				ENDIF
			ENDIF
		BREAK
		CASE CASINO_ARCADE_INVADE_PERSUADE_PLAYER_STATE_LEVEL_COMPLETE
			IF details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_WIN
			AND details.sArcadeCabinetAnimEvent.ePreviousAnimClip != ARCADE_CABINET_ANIM_CLIP_WIN_BIG
				details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
				details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
				IF GET_RANDOM_BOOL()
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_WIN)	
				ELSE
					PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_WIN_BIG)
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_BLOCK_DRONE_MINI_GAME_MENU_OPTION(TEXT_LABEL_63 &blockReason)
	IF !IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != PLAYER_ID()
		blockReason = "ARC_START_M_D1"
		RETURN TRUE
	ENDIF
	
	IF !GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
		blockReason = "ARC_START_M_D2"
		RETURN TRUE
	ENDIF
	
	IF !ARE_THERE_MORE_THAN_ONE_PLAYER_USING_DRONE_MINI_GAME()
		blockReason = "ARC_START_M_D3"
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_LASER_OPTION(TEXT_LABEL_63 &blockReason)
	IF !IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	IF !HAS_PLAYER_ACQUIRED_CASINO_HEIST_STEALTH_VAULT_LASER(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) 
	OR GET_PLAYER_CASINO_HEIST_APPROACH(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) != CASINO_HEIST_APPROACH_TYPE__STEALTH
		blockReason = "ARC_START_M_D5"
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_DRILL_OPTION(TEXT_LABEL_63 &blockReason)
	IF !IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
		RETURN TRUE
	ENDIF
	
	IF !HAS_PLAYER_ACQUIRED_CASINO_HEIST_SUBTERFUGE_VAULT_DRILL(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) 
	OR GET_PLAYER_CASINO_HEIST_APPROACH(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) != CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		blockReason = "ARC_START_M_D4"
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION(ARCADE_CABINET_DETAILS &details)
	
	INT iDisplaySlotLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	INT iArcadePlayerIndex = globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadePlayerSlot
	
	BOOL bUseLaser = FALSE
	TEXT_LABEL_63 sReason
	
	IF SHOULD_BLOCK_DRILL_OPTION(sReason)
		bUseLaser = TRUE
		PRINTLN("[AM_ARC_CAB] - MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION bUseLaser = TRUE")
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_DebugArcadeCabinetManagerData.bForceLaser
			g_DebugArcadeCabinetManagerData.bForceDrill = FALSE
			bUseLaser = TRUE
			PRINTLN("[AM_ARC_CAB] - DEBUG FORCE g_DebugArcadeCabinetManagerData.bForceLaser MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION bUseLaser = TRUE")
		ENDIF
		
		IF g_DebugArcadeCabinetManagerData.bForceDrill
			g_DebugArcadeCabinetManagerData.bForceLaser = FALSE
			bUseLaser = FALSE
			PRINTLN("[AM_ARC_CAB] - DEBUG FORCE g_DebugArcadeCabinetManagerData.bForceLaser MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION bUseLaser = TRUE")
		ENDIF
	#ENDIF	
	
	SETUP_DRILL_TYPE(details.sArcadeDrillPractice, bUseLaser, DEFAULT, DEFAULT, FALSE)
	OBJECT_INDEX drillIndex = details.arcadeCabinetProp[iDisplaySlotLocatePlayerIsIn]
	
	IF iDisplaySlotLocatePlayerIsIn > -1
		SWITCH details.sArcadeDrillPractice.iHackProgress
			CASE 0
				REQUEST_VAULT_DRILL_MINIGAME_ASSETS(details.sArcadeDrillPractice)
				INITIALISE_VAULT_MINIGAME_VALUES(details.sArcadeDrillPractice)	
				IF bUseLaser
					BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_ASSET(ciEVENT_VAULT_DRILL_ASSET_REQUEST_LASER)
					PRINTLN("[VAULT_DRILL] - BROADCAST REQUEST LASER")
				ELSE
					BROADCAST_SCRIPT_EVENT_FMMC_VAULT_DRILL_ASSET(ciEVENT_VAULT_DRILL_ASSET_REQUEST)
					PRINTLN("[VAULT_DRILL] - BROADCAST REQUEST DRILL")
				ENDIF
				
				details.sArcadeDrillPractice.iHackProgress = 1
			BREAK
			
			CASE 1
				IF HAVE_VAULT_DRILL_ASSETS_LOADED(details.sArcadeDrillPractice)	
					details.sArcadeDrillPractice.iHackProgress = 2
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION details.sArcadeDrillPractice.iHackProgress = 1")
				ENDIF	
			BREAK 
			
			CASE 2	
				IF IS_TASK_MOVE_NETWORK_ACTIVE(PLAYER_PED_ID())
					SET_PED_CAPSULE(PLAYER_PED_ID(), 0.54)
				ENDIF
				IF iArcadePlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_2)
					drillIndex = details.arcadeCabinetSubObj[iDisplaySlotLocatePlayerIsIn][ARCADE_CAB_OBJECT_1]
				ENDIF
				RUN_VAULT_DRILL_MINIGAME(details.sArcadeDrillPractice, details.miniGameObjs, details.sArcadeDrillPractice.iMiniGameSafetyTimer, drillIndex, PARTICIPANT_ID_TO_INT(), -1, iDisplaySlotLocatePlayerIsIn, FALSE)
			BREAK
		ENDSWITCH
		
		IF IS_BIT_SET(details.sArcadeDrillPractice.iBitset , VAULT_DRILL_BITSET_IS_MINIGAME_COMPLETE)
		OR IS_BIT_SET(details.sArcadeDrillPractice.iBitset , VAULT_DRILL_BITSET_IS_BACK_OUT_COMPLETE)
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			details.sArcadeDrillPractice.iHackProgress = 3
			PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		ENDIF
	ELSE
		PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)
		PRINTLN("[AM_ARC_CAB] - MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION iDisplaySlotLocatePlayerIsIn = -1")
	ENDIF	
ENDPROC

PROC MAINTAIN_NIGHT_DRIVE_ANIMATION(ARCADE_CABINET_DETAILS &details, ARCADE_CABINETS eArcadeCabinet)
	IF details.sArcadeCabinetAnimEvent.ePreviousAnimClip = ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_RACE_TO_IDLE
		IF IS_PLAYER_PLAYING_THIS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP(details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
		AND HAS_GENERIC_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(eArcadeCabinet, details.sArcadeCabinetAnimEvent.ePreviousAnimClip)
			details.sArcadeCabinetAnimEvent.bHoldLastFrame = FALSE
			details.sArcadeCabinetAnimEvent.bLoopAnim = FALSE
			details.sArcadeCabinetAnimEvent.fBlendInDelta = NORMAL_BLEND_IN
			details.sArcadeCabinetAnimEvent.fBlendOutDelta= NORMAL_BLEND_OUT
			PLAY_ARCADE_CABINET_ANIMATION(details.sArcadeCabinetAnimEvent, ARCADE_CABINET_ANIM_CLIP_EXIT)	
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_GENERIC_ARCADE_CABINET_IN_USE_ANIMATION(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
			MAINTAIN_INVADE_AND_PERSUADE_GAME_ANIMATION(eArcadeCabinet, details)
		BREAK
		CASE ARCADE_CABINET_CA_STREET_CRIMES
			MAINTAIN_STREET_CRIME_GAME_ANIMATION(eArcadeCabinet, details)
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			MAINTAIN_DRILLING_LASER_MINI_G_ANIMATION(details)
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			MAINTAIN_NIGHT_DRIVE_ANIMATION(details, eArcadeCabinet)
		BREAK
	ENDSWITCH 
ENDPROC

PROC GET_GENERIC_ARCADE_CABINET_SOUND_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &tlSoundSet, TEXT_LABEL_63 &tlSoundName, BOOL bInUse)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE	
			IF bInUse
				tlSoundName	= "invade_persuade_playing"
			ELSE
				tlSoundName = "invade_persuade"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CA_STREET_CRIMES
			IF bInUse
				tlSoundName	= "gang_wars_playing"
			ELSE
				tlSoundName = "gang_wars"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
			IF bInUse
				tlSoundName	= "space_monkey_playing"
			ELSE
				tlSoundName = "space_monkey"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			IF bInUse
				tlSoundName	= "badlands_revenge_playing"
			ELSE
				tlSoundName = "badlands_revenge"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			IF bInUse
				tlSoundName	= "rc_bike_playing"
			ELSE
				tlSoundName = "rc_bike"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			IF bInUse
				tlSoundName	= "rc_truck_playing"
			ELSE
				tlSoundName = "rc_truck"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			IF bInUse
				tlSoundName	= "rc_car_playing"
			ELSE
				tlSoundName = "rc_car"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
			IF bInUse
				tlSoundName	= "wizards_ruin_playing"
			ELSE
				tlSoundName = "wizards_ruin"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
			IF bInUse
				tlSoundName	= "dg_defender_playing"
			ELSE
				tlSoundName = "dg_defender"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
			IF bInUse
				tlSoundName	= "dg_monkey_playing"
			ELSE
				tlSoundName = "dg_monkey"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		CASE ARCADE_CABINET_CH_PENETRATOR
			IF bInUse
				tlSoundName	= "dg_penetrator_playing"
			ELSE
				tlSoundName = "dg_penetrator"
			ENDIF
			
			tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
		BREAK
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_QUB3D
			IF bInUse
				tlSoundName	= ""
			ELSE
				tlSoundName = "qub3d"
			ENDIF
			
			tlSoundSet = "dlc_sum20_am_cabinet_attract_sounds"
		BREAK
		#ENDIF
		
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
			IF bInUse
				tlSoundName	= "camhedz_playing"
			ELSE
				tlSoundName = "camhedz"
			ENDIF
			
			tlSoundSet = "dlc_tuner_am_cabinet_attract_sounds"
		#ENDIF
	ENDSWITCH 
ENDPROC

PROC GET_GENERIC_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, ACM_SUB_PROP_DETAILS &eArcadeCabSubPropDetail)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			eArcadeCabSubPropDetail.iNumProp = 3
			eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_1] = 0
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<-0.498, 0.13, 0.786>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<-75.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_WpnGun_01a"))
			
			eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_2] = 1
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<0.498, 0.13, 0.786>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] = <<-75.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_WpnGun_01a"))
			
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_3] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_3] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_3] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_gun_bird_01a"))
			
		BREAK
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
			eArcadeCabSubPropDetail.iNumProp = 2
			eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_1] = 0
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<-0.498, 0.13, 0.786>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<-75.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_wpncamhedz_01a"))
			
			eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_2] = 1
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<0.498, 0.13, 0.786>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] = <<-75.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_wpncamhedz_01a"))
		BREAK
		#ENDIF
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			eArcadeCabSubPropDetail.iNumProp = 2
			eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_1] = 0
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<-0.621, -0.064, 0.168 >>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Race_Bike_02a"))
			
			eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_2] = 1
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<0.621, -0.064, 0.168 >>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Race_Bike_02a"))
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			eArcadeCabSubPropDetail.iNumProp = 2
			//eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_1] = 0
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<-0.667, -0.179, 0.266>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Race_Car_01a"))
			
			//eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_2] = 1
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<0.667, -0.179, 0.266>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Race_Car_01b"))
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			eArcadeCabSubPropDetail.iNumProp = 2
			//eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_1] = 0
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<-0.667, -0.179, 0.266>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Race_Truck_01a"))
			
			//eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_2] = 1
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<0.667, -0.179, 0.266>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Race_Truck_01b")) 
		BREAK
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			eArcadeCabSubPropDetail.iNumProp = 2
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<0.0, 0.2, 0.1>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_usb_drive01x"))
			
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<0.0, 0.2, 0.2>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = PROP_PHONE_ING
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			eArcadeCabSubPropDetail.iNumProp = 1
			eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<1.7, -0.18, 0.0>>
			eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] = <<0.0, 0.0, 0.0>>
			eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Fingerprint_Scanner_01a"))
		BREAK
		DEFAULT 
			eArcadeCabSubPropDetail.iNumProp = 0
		BREAK
	ENDSWITCH 
ENDPROC

FUNC VECTOR GET_GENERIC_ARCADE_CABINET_ANIMATION_OFFSET(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iPlayerSlot, BOOL bPropOffset = FALSE)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1	RETURN <<0, 0, 0>>
				CASE ARCADE_CAB_PLAYER_2	
					SWITCH eClip
						CASE ARCADE_CABINET_ANIM_CLIP_ENTRY
						CASE ARCADE_CABINET_ANIM_CLIP_EXIT
							IF !bPropOffset
								RETURN <<0, 0, 0>>
							ELSE
								RETURN << 1.242, 0, 0>>
							ENDIF
						BREAK
						DEFAULT 
							RETURN << 1.242, 0, 0>>
						BREAK	
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1	RETURN <<0, 0, 0>>
				CASE ARCADE_CAB_PLAYER_2	
					IF (eClip != ARCADE_CABINET_ANIM_CLIP_ENTRY
					AND eClip != ARCADE_CABINET_ANIM_CLIP_NIGHT_DRIVE_ENTER_B
					AND eClip != ARCADE_CABINET_ANIM_CLIP_EXIT)
					OR bPropOffset
						RETURN << 1.34, 0, 0>>
					ENDIF	
				BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH	
	RETURN <<0, 0, 0>>
ENDFUNC

PROC GET_GENERIC_ARCADE_CABINET_STARTUP_MENU_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], STRING &strToggleableOptions[], ARCADE_CABINET_MENU_STRUCT &menuStruct)
	UNUSED_PARAMETER(strToggleableOptions)
	TEXT_LABEL_63 blockReason
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			txtTitle = "ARC_START_M_T1"
			strOptions[0] = "ARC_START_M_I1"
			SET_BIT(menuStruct.iMenuAvailable, 0)
			
			strOptions[1] = "ARC_START_M_I2"
			SET_BIT(menuStruct.iMenuAvailable, 1)

			menuStruct.iNumItemInMenu = 2
		BREAK
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			txtTitle = "ARC_START_M_T2"
			strOptions[0] = "ARC_START_M_I3"
			IF !SHOULD_BLOCK_DRILL_OPTION(blockReason)
				SET_BIT(menuStruct.iMenuAvailable, 0)
			ENDIF	
			
			strOptions[1] = "ARC_START_M_I4"
			IF !SHOULD_BLOCK_LASER_OPTION(blockReason)
				SET_BIT(menuStruct.iMenuAvailable, 1)
			ENDIF
			
			menuStruct.iNumItemInMenu = 2
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GENERIC_ARCADE_CABINET_SCREEN_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, INT iPlayerSlot, MODEL_NAMES &eArcadeCabinetScreenModel)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Race_01a_screen_P1"))
				BREAK
				CASE ARCADE_CAB_PLAYER_2
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Race_01a_screen_P2"))
				BREAK
			ENDSWITCH	
		BREAK	
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Race_01b_screen_P1"))
				BREAK
				CASE ARCADE_CAB_PLAYER_2
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Race_01b_screen_P2"))
				BREAK
			ENDSWITCH	
		BREAK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Race_02a_screen_P1"))
				BREAK
				CASE ARCADE_CAB_PLAYER_2
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Race_02a_screen_P2"))
				BREAK
			ENDSWITCH	
		BREAK
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Gun_01a_screen_P1"))
				BREAK
				CASE ARCADE_CAB_PLAYER_2
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Gun_01a_screen_P2"))
				BREAK
			ENDSWITCH	
		BREAK
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH				eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arc_Dege_01a_Screen_UV"))			BREAK
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE						eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arc_Monkey_01a_Screen_UV"))			BREAK
		CASE ARCADE_CABINET_CH_PENETRATOR							eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arc_Pene_01a_Screen_UV"))			BREAK
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY						eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Space_01a_Scrn_UV"))			BREAK
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Invade_01a_Scrn_UV"))			BREAK
		CASE ARCADE_CABINET_CA_STREET_CRIMES						eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Street_01a_Scrn_UV"))			BREAK
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE						eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_Arcade_Wizard_01a_Scrn_UV"))			BREAK
		CASE ARCADE_CABINET_SUM_QUB3D								eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("sum_Prop_Arcade_Qub3d_01a_Scrn_UV"))			BREAK
		
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
			SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
				CASE ARCADE_CAB_PLAYER_1
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_camhedz_01a_screen_p1"))
				BREAK
				CASE ARCADE_CAB_PLAYER_2
					eArcadeCabinetScreenModel = INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_camhedz_01a_screen_p2"))
				BREAK
			ENDSWITCH
		#ENDIF
		
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡  LOOK-UP TABLE  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Below is the function that builds the interface. Because look up table needs to be built 
// on every call, as an optimisation the function retrieves a pointer to one function at a time 
// so that one call = one lookup. Functions are grouped thematically.
PROC BUILD_GENERIC_ARCADE_CABINET_LOOK_UP_TABLE(ARCADE_CABINET_INTERFACE &interface, ARCADE_CABINET_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		CASE E_DOES_ARCADE_CABINET_HAVE_CHILD_SCRIPT
			interface.returnArcadeCabinetHaveChildScript = &DOES_GENERIC_ARCADE_CABINET_HAVE_CHILD_SCRIPT
		BREAK
		CASE E_GET_ARCADE_CABINET_DETAILS
			interface.getArcadeCabinetDetails = &GET_GENERIC_ARCADE_CABINET_DETAILS
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_DATA
			interface.getArcadeCabinetTriggerData = &GET_GENERIC_ARCADE_CABINET_TRIGGER_DATA
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_HELP_TEXT
			interface.returnArcadeCabinetTriggerHelp = &GET_GENERIC_ARCADE_CABINET_TRIGGER_HELP
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_DETAIL
			interface.returnArcadeCabinetAnimationDetail = &GET_GENERIC_ARCADE_CABINET_ANIMATION_DETAIL
		BREAK
		CASE E_SHOULD_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT
			interface.returnArcadeCabinetHandleAllAnimsByChildScript = &SHOULD_ALL_GENERIC_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT
		BREAK
		CASE E_GET_NUM_PLAYES_ARCADE_CABINET_HAVE
			interface.getNumPlayersArcadeCabinetHave = &GET_NUM_PLAYERS_GENERIC_ARCADE_CAN_HAVE
		BREAK
		CASE E_MAINTAIN_LAUNCH_GAME_SCRIPT
			interface.maintainLunchGameScript = &MAINTAIN_GENERIC_ARCADE_CABINET_GAME_SCRIPT_LAUNCH
		BREAK
		CASE E_MAINTAIN_IN_USE_ANIMATIONS
			interface.maintainInUseAnimations = &MAINTAIN_GENERIC_ARCADE_CABINET_IN_USE_ANIMATION
		BREAK
		CASE E_GET_ARCADE_CABINET_SOUND_DETAIL
			interface.getArcadeCabinetSoundDetail = &GET_GENERIC_ARCADE_CABINET_SOUND_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SUB_PROP_DETAIL
			interface.getArcadeCabinetSubPropDetail = &GET_GENERIC_ARCADE_CABINET_SUB_PROP_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_OFFSET
			interface.getArcadeCabinetAnimationOffset = &GET_GENERIC_ARCADE_CABINET_ANIMATION_OFFSET
		BREAK
		CASE E_GET_ARCADE_CABINET_STARTUP_MENU_DETAIL
			interface.getArcadeCabinetStartupMenuDetail = &GET_GENERIC_ARCADE_CABINET_STARTUP_MENU_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SCREEN_PROP_DETAIL
			interface.getArcadeCabinetScreenPropDetail = &GET_GENERIC_ARCADE_CABINET_SCREEN_PROP_DETAIL
		BREAK
	ENDSWITCH
ENDPROC

#ENDIF

