//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arcade_cabinet_strength_test.sch																	//
// Description: This is the implementation of the strength test as a arcade cabinet. As such this does not 				//
//				expose any public functions. All functions to manipulate arcade cabinet are in net_arcade_cabinet_base	//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "ped_component_public.sch"

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet_base.sch"
USING "mp_globals_arcade_propety_consts.sch"

FUNC BOOL DOES_STRENGTH_TEST_ARCADE_CABINET_HAVE_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &sChildScriptName, BOOL bCheckGameScript)
	UNUSED_PARAMETER(eArcadeCabinet)
	IF bCheckGameScript
		RETURN FALSE
	ENDIF
	
	sChildScriptName = "AM_MP_ARCADE_STRENGTH_TEST"
	RETURN TRUE
ENDFUNC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_DETAILS(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_arcade_strength_01a"))
	details.tlAudioSceneName = "dlc_ch_strength_test_gameplay_scene"
ENDPROC	

PROC GET_STRENGTH_TEST_ARCADE_CABINET_TRIGGER_DATA(ARCADE_CABINETS eArcadeCabinet,  VECTOR &vOffset[])
	UNUSED_PARAMETER(eArcadeCabinet)
	vOffset[ARCADE_CAB_PLAYER_1] = <<0.0, -0.55, 0.0>>
ENDPROC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_TRIGGER_HELP(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
		details.strTriggerHelp = "ARC_CAB_TRI_STRET"
	ELSE
		details.strTriggerHelp = "ARC_CAB_TRI_RSTRET"
	ENDIF	
ENDPROC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iAnimVariation, ARCADE_CABINET_ANIM_DETAIL &eAnimDetails)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iAnimVariation)
	
	// Both male and non heeled female peds now use the male animations. 
	// The female animations are just for heeled female peds.
	
	IF IS_PLAYER_FEMALE()
	AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		g_ArcadeCabinetManagerData.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@STRENGTH@FEMALE@"
		eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@STRENGTH@FEMALE@"
	ELSE
		g_ArcadeCabinetManagerData.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@STRENGTH@MALE@"
		eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@STRENGTH@MALE@"
	ENDIF
	
	SWITCH eClip
		CASE ARCADE_CABINET_ANIM_CLIP_ENTRY								eAnimDetails.sAnimClipName =  "ENTER"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_IDLE								eAnimDetails.sAnimClipName =  "IDLE"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_EXIT								eAnimDetails.sAnimClipName =  "EXIT"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_1_LOOP		eAnimDetails.sAnimClipName =  "STAGE_1_LOOP"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_2_LOOP		eAnimDetails.sAnimClipName =  "STAGE_2_LOOP"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_3_LOOP		eAnimDetails.sAnimClipName =  "STAGE_3_LOOP"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_STAGE_4_LOOP		eAnimDetails.sAnimClipName =  "STAGE_4_LOOP"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_BAD			eAnimDetails.sAnimClipName =  "RESULT_BAD"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_AVERAGE		eAnimDetails.sAnimClipName =  "RESULT_AVERAGE"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_GOOD			eAnimDetails.sAnimClipName =  "RESULT_GOOD"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_STRENGTH_TEST_RESULT_PERFECT		eAnimDetails.sAnimClipName =  "RESULT_PERFECT"		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_ALL_STRENGTH_TEST_ANIMATION_HANDLE_BY_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUM_PLAYERS_STRENGTH_TEST_CAN_HAVE(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
ENDFUNC

FUNC BOOL MAINTAIN_STRENGTH_TEST_ARCADE_CABINET_GAME_SCRIPT_LAUNCH(ARCADE_CABINETS eArcadeCabinet, ARCADE_CAB_GROUP eArcadeCabinetGroup, ARCADE_CABINET_PROPERTY eProperty)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinetGroup)
	UNUSED_PARAMETER(eProperty)
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_STRENGTH_TEST_CABINET_IN_USE_ANIMATION(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(details)
ENDPROC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_SOUND_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &tlSoundSet, TEXT_LABEL_63 &tlSoundName, BOOL bInUse)
	UNUSED_PARAMETER(eArcadeCabinet)
	IF bInUse
		tlSoundName	= ""
	ELSE
		tlSoundName = "axe_of_fury"
	ENDIF
	
	tlSoundSet = "dlc_sum20_am_cabinet_attract_sounds"
ENDPROC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, ACM_SUB_PROP_DETAILS &eArcadeCabSubPropDetail)
	UNUSED_PARAMETER(eArcadeCabinet)
	eArcadeCabSubPropDetail.iNumProp 							= 4
	eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_1] 		= 0
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1]		= <<0.005, -0.2, 0.985>>
	eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_1] 		= <<0.0, 180.0, 0.0>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] 	= INT_TO_ENUM(MODEL_NAMES, HASH("sum_Prop_Arcade_Strength_Ham_01a"))
	eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_2] 		= 0
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] 		= <<0.0, 0.0, 0.0>>
	eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_2] 		= <<0.0, 0.0, 0.0>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] 	= INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_arcade_str_lighton"))
	eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_3] 		= 0
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_3] 		= <<0.0, 0.0, 0.0>>
	eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_3] 		= <<0.0, 0.0, 0.0>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_3] 	= INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_arcade_str_lightoff"))
	eArcadeCabSubPropDetail.iTintID[ARCADE_CAB_OBJECT_4] 		= 0
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_4] 		= <<0.0, 0.0, 0.0>>
	eArcadeCabSubPropDetail.vRotation[ARCADE_CAB_OBJECT_4] 		= <<0.0, 0.0, 0.0>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_4] 	= INT_TO_ENUM(MODEL_NAMES, HASH("sum_prop_arcade_str_bar_01a"))
ENDPROC

FUNC VECTOR GET_STRENGTH_TEST_ARCADE_CABINET_ANIMATION_OFFSET(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iPlayerSlot, BOOL bPropOffset = FALSE)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iPlayerSlot)
	UNUSED_PARAMETER(eClip)
	UNUSED_PARAMETER(bPropOffset)
	RETURN <<0, 0, 0>>
ENDFUNC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_STARTUP_MENU_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], STRING &strToggleableOptions[], ARCADE_CABINET_MENU_STRUCT &menuStruct)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(txtTitle)
	UNUSED_PARAMETER(strOptions)
	UNUSED_PARAMETER(menuStruct)
	UNUSED_PARAMETER(strToggleableOptions)
ENDPROC

PROC GET_STRENGTH_TEST_ARCADE_CABINET_SCREEN_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, INT iPlayerSlot, MODEL_NAMES &eArcadeCabinetScreenModel)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iPlayerSlot)
	UNUSED_PARAMETER(eArcadeCabinetScreenModel)
ENDPROC

FUNC BOOL HAS_PLAYER_FINISHED_STRENGTH_TEST_ARCADE_GAME(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerToCheck)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_FINISHED_STRENGTH_TEST)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_LOCAL_PLAYER_FINISHED_STRENGTH_TEST_ARCADE_GAME(BOOL bFinished)
	IF bFinished
		IF NOT IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_FINISHED_STRENGTH_TEST)
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_FINISHED_STRENGTH_TEST)
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SET_LOCAL_PLAYER_FINISHED_STRENGTH_TEST_MINIGAME - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_FINISHED_STRENGTH_TEST)
			CLEAR_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_FINISHED_STRENGTH_TEST)
			PRINTLN("[AM_MP_ARCADE_STRENGTH_TEST] - SET_LOCAL_PLAYER_FINISHED_STRENGTH_TEST_MINIGAME - FALSE")
		ENDIF
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡  LOOK-UP TABLE  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Below is the function that builds the interface. Because look up table needs to be built 
// on every call, as an optimisation the function retrieves a pointer to one function at a time 
// so that one call = one lookup. Functions are grouped thematically.
PROC BUILD_STRENGTH_TEST_ARCADE_CABINET_LOOK_UP_TABLE(ARCADE_CABINET_INTERFACE &interface, ARCADE_CABINET_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		CASE E_DOES_ARCADE_CABINET_HAVE_CHILD_SCRIPT
			interface.returnArcadeCabinetHaveChildScript = &DOES_STRENGTH_TEST_ARCADE_CABINET_HAVE_CHILD_SCRIPT
		BREAK
		CASE E_GET_ARCADE_CABINET_DETAILS
			interface.getArcadeCabinetDetails = &GET_STRENGTH_TEST_ARCADE_CABINET_DETAILS
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_DATA
			interface.getArcadeCabinetTriggerData = &GET_STRENGTH_TEST_ARCADE_CABINET_TRIGGER_DATA
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_HELP_TEXT
			interface.returnArcadeCabinetTriggerHelp = &GET_STRENGTH_TEST_ARCADE_CABINET_TRIGGER_HELP
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_DETAIL
			interface.returnArcadeCabinetAnimationDetail = &GET_STRENGTH_TEST_ARCADE_CABINET_ANIMATION_DETAIL
		BREAK
		CASE E_SHOULD_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT
			interface.returnArcadeCabinetHandleAllAnimsByChildScript = &SHOULD_ALL_STRENGTH_TEST_ANIMATION_HANDLE_BY_CHILD_SCRIPT
		BREAK
		CASE E_GET_NUM_PLAYES_ARCADE_CABINET_HAVE
			interface.getNumPlayersArcadeCabinetHave = &GET_NUM_PLAYERS_STRENGTH_TEST_CAN_HAVE
		BREAK
		CASE E_MAINTAIN_LAUNCH_GAME_SCRIPT
			interface.maintainLunchGameScript = &MAINTAIN_STRENGTH_TEST_ARCADE_CABINET_GAME_SCRIPT_LAUNCH
		BREAK
		CASE E_MAINTAIN_IN_USE_ANIMATIONS
			interface.maintainInUseAnimations = &MAINTAIN_STRENGTH_TEST_CABINET_IN_USE_ANIMATION
		BREAK
		CASE E_GET_ARCADE_CABINET_SOUND_DETAIL
			interface.getArcadeCabinetSoundDetail = &GET_STRENGTH_TEST_ARCADE_CABINET_SOUND_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SUB_PROP_DETAIL
			interface.getArcadeCabinetSubPropDetail = &GET_STRENGTH_TEST_ARCADE_CABINET_SUB_PROP_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_OFFSET
			interface.getArcadeCabinetAnimationOffset = &GET_STRENGTH_TEST_ARCADE_CABINET_ANIMATION_OFFSET
		BREAK
		CASE E_GET_ARCADE_CABINET_STARTUP_MENU_DETAIL
			interface.getArcadeCabinetStartupMenuDetail = &GET_STRENGTH_TEST_ARCADE_CABINET_STARTUP_MENU_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SCREEN_PROP_DETAIL
			interface.getArcadeCabinetScreenPropDetail = &GET_STRENGTH_TEST_ARCADE_CABINET_SCREEN_PROP_DETAIL
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

