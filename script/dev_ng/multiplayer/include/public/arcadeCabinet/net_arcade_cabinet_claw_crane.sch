//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arcade_cabinet_claw_crane.sch																		//
// Description: This is the implementation of the claw crane as a arcade cabinet. As such this does not 				//
//				expose any public functions. All functions to manipulate arcade cabinet are in net_arcade_cabinet_base	//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


USING "net_include.sch"

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet_base.sch"
USING "mp_globals_arcade_propety_consts.sch"

FUNC BOOL DOES_CLAW_CRANE_ARCADE_CABINET_HAVE_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &sChildScriptName, BOOL bCheckGameScript)
	UNUSED_PARAMETER(eArcadeCabinet)
	IF bCheckGameScript
		RETURN FALSE
	ENDIF
	
	sChildScriptName = "AM_MP_ARCADE_CLAW_CRANE"
	RETURN TRUE
ENDFUNC

PROC GET_CLAW_CRANE_ARCADE_CABINET_DETAILS(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_01a"))
	details.tlAudioSceneName = "dlc_ch_claw_machine_gameplay_scene"
ENDPROC	

PROC GET_CLAW_CRANE_ARCADE_CABINET_TRIGGER_DATA(ARCADE_CABINETS eArcadeCabinet, VECTOR &vOffset[])
	UNUSED_PARAMETER(eArcadeCabinet)
	vOffset[ARCADE_CAB_PLAYER_1] = <<0.0, -0.5, 0.0>>
ENDPROC

PROC GET_CLAW_CRANE_ARCADE_CABINET_TRIGGER_HELP(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
		details.strTriggerHelp = "ARC_CAB_TRI_CLAWC"
	ELSE
		details.strTriggerHelp = "ARC_CAB_TRI_RCLAWC"
	ENDIF
ENDPROC

PROC GET_CLAW_CRANE_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iAnimVariation, ARCADE_CABINET_ANIM_DETAIL &eAnimDetails)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iAnimVariation)
	
	IF !IS_PLAYER_FEMALE()
		eAnimDetails.strArcCabinetAnimDic = "anim_heist@arcade@claw@male@"
	ELSE
		eAnimDetails.strArcCabinetAnimDic = "anim_heist@arcade@claw@female@"
	ENDIF
	
	eAnimDetails.iNumPropClips = 1
	
	SWITCH eClip
		CASE ARCADE_CABINET_ANIM_CLIP_ENTRY						eAnimDetails.sAnimClipName = "idle_to_game"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_IDLE						eAnimDetails.sAnimClipName = "game"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_EXIT						eAnimDetails.sAnimClipName = "exit_game"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_GRAB_PRIZE		eAnimDetails.sAnimClipName = "grab_prize"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS			eAnimDetails.sAnimClipName = "miss"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_NEAR_MISS		eAnimDetails.sAnimClipName = "near_miss"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_GRAB		eAnimDetails.sAnimClipName = "press_grab"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_RIGHT	eAnimDetails.sAnimClipName = "press_right"	BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRESS_UP		eAnimDetails.sAnimClipName = "press_up"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_WIN_LOOP		eAnimDetails.sAnimClipName = "win_loop"		BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_PRIZE_WIN		
			eAnimDetails.sAnimClipName = "win"			
			eAnimDetails.sPropAnimClip[ARCADE_CAB_OBJECT_3] = "prop_claw_win"	
			IF g_ArcadeCabinetManagerData.iClawPrizeIndex != -1
				eAnimDetails.sPropAnimClip[g_ArcadeCabinetManagerData.iClawPrizeIndex] = "prop_prize_win"	
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_ALL_CLAW_CRANE_ANIMATION_HANDLE_BY_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_PLAYERS_CLAW_CRANE_CAN_HAVE(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN ENUM_TO_INT(ARCADE_CAB_PLAYER_1)
ENDFUNC

FUNC BOOL MAINTAIN_CLAW_CRANE_ARCADE_CABINET_GAME_SCRIPT_LAUNCH(ARCADE_CABINETS eArcadeCabinet, ARCADE_CAB_GROUP eArcadeCabinetGroup, ARCADE_CABINET_PROPERTY eProperty)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinetGroup)
	UNUSED_PARAMETER(eProperty)
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLAW_CRANE_ARCADE_CABINET_IN_USE_ANIMATION(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(details)
ENDPROC

PROC GET_CLAW_CRANE_ARCADE_CABINET_SOUND_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &tlSoundSet, TEXT_LABEL_63 &tlSoundName, BOOL bInUse)
	UNUSED_PARAMETER(eArcadeCabinet)
	IF bInUse
		tlSoundName	= "claw_crane_playing"
	ELSE
		tlSoundName = "claw_crane"
	ENDIF
	
	tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
ENDPROC

PROC GET_CLAW_CRANE_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, ACM_SUB_PROP_DETAILS &eArcadeCabSubPropDetail)
	UNUSED_PARAMETER(eArcadeCabinet)
	eArcadeCabSubPropDetail.iNumProp = 14

	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_1] = <<CLAW_CRAN_XRAIL_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_XRAIL_MAX_Z_AXIS>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_1] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_R2"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_2] = <<CLAW_CRAN_YRAIL_MIN_X_AXIS, CLAW_CRAN_YRAIL_MIN_Y_AXIS, CLAW_CRAN_YRAIL_MAX_Z_AXIS>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_2] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_R1"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_3] = <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_3] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_C_D"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_4] = <<CLAW_CRAN_MIN_X_AXIS, CLAW_CRAN_MIN_Y_AXIS, CLAW_CRAN_MAX_Z_AXIS>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_4] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_C"))

	// prize props
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_5] = <<0.020, -0.010, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_5] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_01a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_6] = <<0.240, -0.040, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_6] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_02a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_7] = <<0.290, 0.190, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_7] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_03a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_8] = <<0.060, 0.200, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_8] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_9] = <<-0.240, 0.250, 1.020>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_9] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_05a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_10] = <<-0.250, 0.550, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_10] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_06a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_11] = <<0.010, 0.550, 1.040>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_11] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_12] = <<0.300, 0.550, 1.025>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_12] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_13] = <<0.200, 0.370, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_13] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
	
	eArcadeCabSubPropDetail.vOffset[ARCADE_CAB_OBJECT_14] = <<-0.060, 0.370, 1.010>>
	eArcadeCabSubPropDetail.eSubPropModel[ARCADE_CAB_OBJECT_14] = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))
ENDPROC

FUNC VECTOR GET_CLAW_CRANE_ARCADE_CABINET_ANIMATION_OFFSET(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iPlayerSlot, BOOL bPropOffset = FALSE)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iPlayerSlot)
	UNUSED_PARAMETER(eClip)
	UNUSED_PARAMETER(bPropOffset)
	RETURN <<0, 0, 0>>
ENDFUNC

PROC GET_CLAW_CRANE_ARCADE_CABINET_STARTUP_MENU_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], STRING &strToggleableOptions[], ARCADE_CABINET_MENU_STRUCT &menuStruct)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(txtTitle)
	UNUSED_PARAMETER(strOptions)
	UNUSED_PARAMETER(menuStruct)
	UNUSED_PARAMETER(strToggleableOptions)
ENDPROC

PROC GET_CLAW_CRANE_ARCADE_CABINET_SCREEN_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, INT iPlayerSlot, MODEL_NAMES &eArcadeCabinetScreenModel)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iPlayerSlot)
	UNUSED_PARAMETER(eArcadeCabinetScreenModel)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡  LOOK-UP TABLE  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Below is the function that builds the interface. Because look up table needs to be built 
// on every call, as an optimisation the function retrieves a pointer to one function at a time 
// so that one call = one lookup. Functions are grouped thematically.
PROC BUILD_CLAW_CRANE_ARCADE_CABINET_LOOK_UP_TABLE(ARCADE_CABINET_INTERFACE &interface, ARCADE_CABINET_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		CASE E_DOES_ARCADE_CABINET_HAVE_CHILD_SCRIPT
			interface.returnArcadeCabinetHaveChildScript = &DOES_CLAW_CRANE_ARCADE_CABINET_HAVE_CHILD_SCRIPT
		BREAK
		CASE E_GET_ARCADE_CABINET_DETAILS
			interface.getArcadeCabinetDetails = &GET_CLAW_CRANE_ARCADE_CABINET_DETAILS
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_DATA
			interface.getArcadeCabinetTriggerData = &GET_CLAW_CRANE_ARCADE_CABINET_TRIGGER_DATA
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_HELP_TEXT
			interface.returnArcadeCabinetTriggerHelp = &GET_CLAW_CRANE_ARCADE_CABINET_TRIGGER_HELP
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_DETAIL
			interface.returnArcadeCabinetAnimationDetail = &GET_CLAW_CRANE_ARCADE_CABINET_ANIMATION_DETAIL
		BREAK
		CASE E_SHOULD_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT
			interface.returnArcadeCabinetHandleAllAnimsByChildScript = &SHOULD_ALL_CLAW_CRANE_ANIMATION_HANDLE_BY_CHILD_SCRIPT
		BREAK
		CASE E_GET_NUM_PLAYES_ARCADE_CABINET_HAVE
			interface.getNumPlayersArcadeCabinetHave = &GET_NUM_PLAYERS_CLAW_CRANE_CAN_HAVE
		BREAK
		CASE E_MAINTAIN_LAUNCH_GAME_SCRIPT
			interface.maintainLunchGameScript = &MAINTAIN_CLAW_CRANE_ARCADE_CABINET_GAME_SCRIPT_LAUNCH
		BREAK
		CASE E_MAINTAIN_IN_USE_ANIMATIONS
			interface.maintainInUseAnimations = &MAINTAIN_CLAW_CRANE_ARCADE_CABINET_IN_USE_ANIMATION
		BREAK
		CASE E_GET_ARCADE_CABINET_SOUND_DETAIL
			interface.getArcadeCabinetSoundDetail = &GET_CLAW_CRANE_ARCADE_CABINET_SOUND_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SUB_PROP_DETAIL
			interface.getArcadeCabinetSubPropDetail = &GET_CLAW_CRANE_ARCADE_CABINET_SUB_PROP_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_OFFSET
			interface.getArcadeCabinetAnimationOffset = &GET_CLAW_CRANE_ARCADE_CABINET_ANIMATION_OFFSET
		BREAK
		CASE E_GET_ARCADE_CABINET_STARTUP_MENU_DETAIL
			interface.getArcadeCabinetStartupMenuDetail = &GET_CLAW_CRANE_ARCADE_CABINET_STARTUP_MENU_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SCREEN_PROP_DETAIL
			interface.getArcadeCabinetScreenPropDetail = &GET_CLAW_CRANE_ARCADE_CABINET_SCREEN_PROP_DETAIL
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

