//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arcade_cabinet_love_meter.sch																		//
// Description: This is the implementation of the love meter as a arcade cabinet. As such this does not 				//
//				expose any public functions. All functions to manipulate arcade cabinet are in net_arcade_cabinet_base	//
// Written by:  Online Technical Design: Ata Tabrizi																	//
// Date:  		28/08/2019																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet_base.sch"
USING "mp_globals_arcade_propety_consts.sch"

FUNC BOOL DOES_LOVE_METER_ARCADE_CABINET_HAVE_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &sChildScriptName, BOOL bCheckGameScript)
	UNUSED_PARAMETER(eArcadeCabinet)
	IF bCheckGameScript
		RETURN FALSE
	ENDIF
	
	sChildScriptName = "AM_MP_ARCADE_LOVE_METER"
	RETURN TRUE
ENDFUNC

PROC GET_LOVE_METER_ARCADE_CABINET_DETAILS(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	details.propModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_love_01a"))
	details.tlAudioSceneName = "dlc_ch_professor_love_gameplay_scene"
ENDPROC	

PROC GET_LOVE_METER_ARCADE_CABINET_TRIGGER_DATA(ARCADE_CABINETS eArcadeCabinet,  VECTOR &vOffset[])
	UNUSED_PARAMETER(eArcadeCabinet)
	vOffset[ARCADE_CAB_PLAYER_1] = <<-0.4, -0.5, 0.0>>
	vOffset[ARCADE_CAB_PLAYER_2] = <<0.4, -0.5, 0.0>>
ENDPROC

PROC GET_LOVE_METER_ARCADE_CABINET_TRIGGER_HELP(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
		details.strTriggerHelp = "ARC_CAB_TRI_LOVM"
	ELSE
		details.strTriggerHelp = "ARC_CAB_TRI_RLOVM"
	ENDIF
ENDPROC

FUNC INT GET_PLAYER_IN_OTHER_LOVE_METER_LOCATE()
	INT i
	INT iDisplaySlot = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
	
	IF iDisplaySlot != -1
		REPEAT NUM_NETWORK_PLAYERS i 
			PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX,i)
			
			IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			AND piPlayer != PLAYER_ID()
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(piPlayer)
					IF GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(piPlayer) = iDisplaySlot
						RETURN i
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN -1
ENDFUNC

PROC GET_LOVE_METER_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iAnimVariation, ARCADE_CABINET_ANIM_DETAIL &eAnimDetails)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iAnimVariation)
	
	PLAYER_INDEX playerInOtherSlot = INT_TO_PLAYERINDEX(GET_PLAYER_IN_OTHER_LOVE_METER_LOCATE())
	IF !IS_PLAYER_FEMALE()
		SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iAnimVariation)
			CASE ARCADE_CAB_PLAYER_1
				IF playerInOtherSlot != INVALID_PLAYER_INDEX()
				AND !IS_PLAYER_PED_FEMALE(playerInOtherSlot)
				AND !IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerInOtherSlot)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@FEMALE@LEFT@"
					SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@MALE@LEFT@"
				ENDIF
			BREAK
			DEFAULT
				IF playerInOtherSlot != INVALID_PLAYER_INDEX()
				AND !IS_PLAYER_PED_FEMALE(playerInOtherSlot)
				AND !IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerInOtherSlot)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@FEMALE@RIGHT@"
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@MALE@RIGHT@"
				ENDIF	
			BREAK	
		ENDSWITCH	
	ELSE
		SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iAnimVariation)
			CASE ARCADE_CAB_PLAYER_1
				IF playerInOtherSlot != INVALID_PLAYER_INDEX()
				AND IS_PLAYER_PED_FEMALE(playerInOtherSlot)
				AND !IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerInOtherSlot)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@MALE@LEFT@"
					SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@FEMALE@LEFT@"
				ENDIF	
			BREAK
			DEFAULT
				IF playerInOtherSlot != INVALID_PLAYER_INDEX()
				AND IS_PLAYER_PED_FEMALE(playerInOtherSlot)
				AND !IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerInOtherSlot)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@MALE@RIGHT@"
					SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_SWITCHED_LOVE_DIC)
				ELSE
					eAnimDetails.strArcCabinetAnimDic = "ANIM_HEIST@ARCADE@LOVE@FEMALE@RIGHT@"
				ENDIF	
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH eClip
		CASE ARCADE_CABINET_ANIM_CLIP_ENTRY							eAnimDetails.sAnimClipName =  "enter"						BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_IDLE							eAnimDetails.sAnimClipName =  "idle"						BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_EXIT							eAnimDetails.sAnimClipName =  "exit"						BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_BETTER		eAnimDetails.sAnimClipName =  "average_better"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_WORSE		eAnimDetails.sAnimClipName =  "average_worse"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_BETTER			eAnimDetails.sAnimClipName =  "bad_better"					BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_WORSE			eAnimDetails.sAnimClipName =  "bad_worse"					BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_FRIENDZONE_PERFECT eAnimDetails.sAnimClipName =  "friendzoned_perfect"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_FRIENDZONE_WORSE   eAnimDetails.sAnimClipName =  "friendzoned_worst"			BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_BETTER		eAnimDetails.sAnimClipName =  "good_better"					BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_WORSE			eAnimDetails.sAnimClipName =  "good_worse"					BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_BAD			eAnimDetails.sAnimClipName =  "mutual_bad"					BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_AVERAGE		eAnimDetails.sAnimClipName =  "mutual_average"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_GOOD		eAnimDetails.sAnimClipName =  "mutual_good"					BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_PERFECT		eAnimDetails.sAnimClipName =  "mutual_perfect"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_WORSE		eAnimDetails.sAnimClipName =  "mutual_worst"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_PERFECT_BETTER		eAnimDetails.sAnimClipName =  "perfect_better"				BREAK
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_WORSE_WORSE		eAnimDetails.sAnimClipName =  "worst_worse"					BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_ALL_LOVE_METER_ANIMATION_HANDLE_BY_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN FALSE
ENDFUNC

FUNC INT GET_NUM_PLAYERS_LOVE_METER_CAN_HAVE(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN ENUM_TO_INT(ARCADE_CAB_PLAYER_2)
ENDFUNC

FUNC BOOL MAINTAIN_LOVE_METER_ARCADE_CABINET_GAME_SCRIPT_LAUNCH(ARCADE_CABINETS eArcadeCabinet, ARCADE_CAB_GROUP eArcadeCabinetGroup, ARCADE_CABINET_PROPERTY eProperty)
	UNUSED_PARAMETER(eArcadeCabinetGroup)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(eProperty)
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LOVE_METER_CABINET_IN_USE_ANIMATION(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(details)
ENDPROC

PROC GET_LOVE_METER_ARCADE_CABINET_SOUND_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &tlSoundSet, TEXT_LABEL_63 &tlSoundName, BOOL bInUse)
	UNUSED_PARAMETER(eArcadeCabinet)
	IF bInUse
		tlSoundName	= "professor_love_playing"
	ELSE
		tlSoundName = "professor_love"
	ENDIF
	
	tlSoundSet = "dlc_ch_am_cabinet_attract_sounds"
ENDPROC

PROC GET_LOVE_METER_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, ACM_SUB_PROP_DETAILS &eArcadeCabSubPropDetail)
	UNUSED_PARAMETER(eArcadeCabinet)
	eArcadeCabSubPropDetail.iNumProp = 0
ENDPROC

FUNC VECTOR GET_LOVE_METER_ARCADE_CABINET_ANIMATION_OFFSET(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iPlayerSlot, BOOL bPropOffset = FALSE)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(eClip)
	UNUSED_PARAMETER(bPropOffset)
	SWITCH INT_TO_ENUM(ARCADE_CAB_NUM_PLAYERS, iPlayerSlot)
		CASE ARCADE_CAB_PLAYER_1		RETURN <<0.0, 0.0, 0.0>>
		CASE ARCADE_CAB_PLAYER_2		RETURN <<0.0, 0.0, 0.0>>
	ENDSWITCH
	RETURN <<0, 0, 0>>
ENDFUNC

PROC GET_LOVE_METER_ARCADE_CABINET_STARTUP_MENU_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], STRING &strToggleableOptions[], ARCADE_CABINET_MENU_STRUCT &menuStruct)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(txtTitle)
	UNUSED_PARAMETER(strOptions)
	UNUSED_PARAMETER(menuStruct)
	UNUSED_PARAMETER(strToggleableOptions)
ENDPROC

PROC GET_LOVE_METER_ARCADE_CABINET_SCREEN_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, INT iPlayerSlot, MODEL_NAMES &eArcadeCabinetScreenModel)
	UNUSED_PARAMETER(eArcadeCabinet)
	UNUSED_PARAMETER(iPlayerSlot)
	UNUSED_PARAMETER(eArcadeCabinetScreenModel)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡  LOOK-UP TABLE  ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Below is the function that builds the interface. Because look up table needs to be built 
// on every call, as an optimisation the function retrieves a pointer to one function at a time 
// so that one call = one lookup. Functions are grouped thematically.
PROC BUILD_LOVE_METER_ARCADE_CABINET_LOOK_UP_TABLE(ARCADE_CABINET_INTERFACE &interface, ARCADE_CABINET_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		CASE E_DOES_ARCADE_CABINET_HAVE_CHILD_SCRIPT
			interface.returnArcadeCabinetHaveChildScript = &DOES_LOVE_METER_ARCADE_CABINET_HAVE_CHILD_SCRIPT
		BREAK
		CASE E_GET_ARCADE_CABINET_DETAILS
			interface.getArcadeCabinetDetails = &GET_LOVE_METER_ARCADE_CABINET_DETAILS
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_DATA
			interface.getArcadeCabinetTriggerData = &GET_LOVE_METER_ARCADE_CABINET_TRIGGER_DATA
		BREAK
		CASE E_GET_ARCADE_CABINET_TRIGGER_HELP_TEXT
			interface.returnArcadeCabinetTriggerHelp = &GET_LOVE_METER_ARCADE_CABINET_TRIGGER_HELP
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_DETAIL
			interface.returnArcadeCabinetAnimationDetail = &GET_LOVE_METER_ARCADE_CABINET_ANIMATION_DETAIL
		BREAK
		CASE E_SHOULD_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT
			interface.returnArcadeCabinetHandleAllAnimsByChildScript = &SHOULD_ALL_LOVE_METER_ANIMATION_HANDLE_BY_CHILD_SCRIPT
		BREAK
		CASE E_GET_NUM_PLAYES_ARCADE_CABINET_HAVE
			interface.getNumPlayersArcadeCabinetHave = &GET_NUM_PLAYERS_LOVE_METER_CAN_HAVE
		BREAK
		CASE E_MAINTAIN_LAUNCH_GAME_SCRIPT
			interface.maintainLunchGameScript = &MAINTAIN_LOVE_METER_ARCADE_CABINET_GAME_SCRIPT_LAUNCH
		BREAK
		CASE E_MAINTAIN_IN_USE_ANIMATIONS
			interface.maintainInUseAnimations = &MAINTAIN_LOVE_METER_CABINET_IN_USE_ANIMATION
		BREAK
		CASE E_GET_ARCADE_CABINET_SOUND_DETAIL
			interface.getArcadeCabinetSoundDetail = &GET_LOVE_METER_ARCADE_CABINET_SOUND_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SUB_PROP_DETAIL
			interface.getArcadeCabinetSubPropDetail = &GET_LOVE_METER_ARCADE_CABINET_SUB_PROP_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_ANIMATION_OFFSET
			interface.getArcadeCabinetAnimationOffset = &GET_LOVE_METER_ARCADE_CABINET_ANIMATION_OFFSET
		BREAK
		CASE E_GET_ARCADE_CABINET_STARTUP_MENU_DETAIL
			interface.getArcadeCabinetStartupMenuDetail = &GET_LOVE_METER_ARCADE_CABINET_STARTUP_MENU_DETAIL
		BREAK
		CASE E_GET_ARCADE_CABINET_SCREEN_PROP_DETAIL
			interface.getArcadeCabinetScreenPropDetail = &GET_LOVE_METER_ARCADE_CABINET_SCREEN_PROP_DETAIL
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF


