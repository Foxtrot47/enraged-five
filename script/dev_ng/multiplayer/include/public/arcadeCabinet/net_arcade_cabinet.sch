//////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arcade_cabinet.sch														//
// Description: Functions for managing arcade cabinet manager								//						
// Written by:  Online Technical Design: Ata Tabrizi										//
// Date:  		28/08/2019																	//
//////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "mp_globals_fm.sch"
USING "commands_hud.sch"
USING "net_simple_interior.sch"

#IF FEATURE_CASINO_HEIST
USING "net_arcade_cabinet_common.sch"
USING "net_arcade_cabinet_private.sch"

FUNC BOOL SHOULD_KILL_ACM_SCRIPT()
	RETURN IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_KILL_SCRIPT)
ENDFUNC

/// PURPOSE:
///    Kills arcade cabinet mangager and all child scripts
PROC SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT(BOOL bKill)
	IF bKill
		IF !SHOULD_KILL_ACM_SCRIPT()
			SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_KILL_SCRIPT)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[AM_ARC_CAB] - SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT - TRUE")
		ENDIF
	ELSE
		IF SHOULD_KILL_ACM_SCRIPT()
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_KILL_SCRIPT)
			PRINTLN("[AM_ARC_CAB] - SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    set arcade cabinet in glitch mode
PROC SET_ARCADE_CABINET_GLITCHED(BOOL bGlitch)
	IF bGlitch
		IF !IS_ARCADE_CABINET_GLITCHED()
			SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_GLITCH_MODE)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_GLITCHED - TRUE")
		ENDIF
	ELSE
		IF IS_ARCADE_CABINET_GLITCHED()
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_GLITCH_MODE)
			PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_GLITCHED - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set true to stop using arcade cabinet
PROC SET_BAIL_OUT_ARCADE_CABINET(BOOL bBail)
	IF bBail
		IF !SHOULD_BAIL_ARCADE_CABINET()
			SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_FORCE_BAIL)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[AM_ARC_CAB] - SET_BAIL_OUT_ARCADE_CABINET - TRUE")
		ENDIF
	ELSE
		IF SHOULD_BAIL_ARCADE_CABINET()
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_FORCE_BAIL)
			PRINTLN("[AM_ARC_CAB] - SET_BAIL_OUT_ARCADE_CABINET - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This will set once player is triggered the arcade cabinet and playing idle animation
FUNC BOOL IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerToCheck)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_READY_TO_PLAY)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This will set once player is triggered the arcade cabinet and playing idle animation
PROC SET_PLAYER_READY_TO_PLAY_ARCADE_GAME(BOOL bReady)
	IF bReady
		IF !IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(PLAYER_ID())
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_READY_TO_PLAY)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_READY_TO_PLAY_ARCADE_GAME - TRUE")
		ENDIF
	ELSE
		IF IS_PLAYER_READY_TO_PLAY_ARCADE_GAME(PLAYER_ID())
			CLEAR_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_READY_TO_PLAY)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_READY_TO_PLAY_ARCADE_GAME - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This will set once player is playing with AI
FUNC BOOL IS_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(globalPlayerBD[NATIVE_TO_INT(playerToCheck)].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_PLAYING_AI)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This will set once player is triggered the arcade cabinet and playing idle animation
PROC SET_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(BOOL bReady)
	IF bReady
		IF !IS_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(PLAYER_ID())
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_PLAYING_AI)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_PLAYING_ARCADE_GAME_WITH_AI - TRUE")
		ENDIF
	ELSE
		IF IS_PLAYER_PLAYING_ARCADE_GAME_WITH_AI(PLAYER_ID())
			CLEAR_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_PLAYER_PLAYING_AI)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_PLAYING_ARCADE_GAME_WITH_AI - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the player slot which player is in. player slot is number of players can arcade cabinet can accept. Some of the arcade cabinets can have up to ARCADE_CAB_MAX_NUM_PLAYERS players
FUNC INT GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN globalPlayerBD[NATIVE_TO_INT(playerToCheck)].sArcadeManagerGlobalPlayerBD.iArcadePlayerSlot
	ENDIF
	
	RETURN -1
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC STRING GET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE_NAME(ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE eClientState)
	SWITCH eClientState
		CASE ARCADE_CABINET_STATE_LOADING 				RETURN "ARCADE_CABINET_STATE_LOADING"
		CASE ARCADE_CABINET_STATE_IDLE					RETURN "ARCADE_CABINET_STATE_IDLE"
		CASE ARCADE_CABINET_STATE_CLEANUP				RETURN "ARCADE_CABINET_STATE_CLEANUP"	
	ENDSWITCH

	RETURN "INVALID"
ENDFUNC	
#ENDIF

PROC SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE(ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE &eClientState, ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE eSetTo)
	IF eClientState != eSetTo
		PRINTLN("[AM_ARC_CAB] - SET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE - eClientState: ", GET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE_NAME(eClientState), " set to: ", GET_ARCADE_CABINET_CHILD_SCRIPT_CLIENT_STATE_NAME(eSetTo))
		eClientState = eSetTo
	ENDIF	
ENDPROC

/// PURPOSE:
///    This will get set to true as soon as player trigger the arcade cabinet
PROC SET_PLAYER_USING_ARCADE_CABINET(BOOL bUsing)
	IF bUsing
		IF !IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_USING_CABINET)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_USING_ARCADE_CABINET - TRUE")
		ENDIF
	ELSE
		IF IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID())
			CLEAR_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.iArcadeCabinetBS, ARCADE_CABINET_GLOBAL_BD_USING_CABINET)
			PRINTLN("[AM_ARC_CAB] - SET_PLAYER_USING_ARCADE_CABINET - FALSE")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SAFE_TO_START_ARCADE_CABINET_MANAGER_SCRIPT()
	IF IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF g_iSimpleInteriorState = SIMPLE_INT_STATE_INITIALISE
	OR g_iSimpleInteriorState > SIMPLE_INT_STATE_IDLE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC 

/// PURPOSE:
///    Get arcade locate type which player is in
FUNC ARCADE_CABINETS GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN globalPlayerBD[NATIVE_TO_INT(playerToCheck)].sArcadeManagerGlobalPlayerBD.eArcadeCabLocatePlayerIn
	ENDIF
	
	RETURN ARCADE_CABINET_INVALID
ENDFUNC

/// PURPOSE:
///    Set arcade cabine locate type player is in 
PROC SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE(ARCADE_CABINETS eArcadeCabinet = ARCADE_CABINET_INVALID)
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN("[AM_ARC_CAB] - SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE invalid player index")
		EXIT
	ENDIF

	IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eArcadeCabLocatePlayerIn 	!= eArcadeCabinet
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].sArcadeManagerGlobalPlayerBD.eArcadeCabLocatePlayerIn = eArcadeCabinet
		PRINTLN("[AM_ARC_CAB] - SET_PLAYER_IN_ARCADE_CABINET_LOCATE_TYPE ", GET_ARCADE_CABINET_NAME(eArcadeCabinet))
	ENDIF	
ENDPROC

/// PURPOSE:
///    check server BD if slot is not used by any players
/// PARAMS:
///    iSlotToCheck - 0 to GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(eProperty) - 1
/// RETURNS: -1 free and return true else false
///    
FUNC INT GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT(ARCADE_CABINET_MANAGER_SERVER_BD &serverBD, INT iSlotToCheck, INT iPlayerSlot, ARCADE_CABINET_PROPERTY eProperty)
	IF iSlotToCheck > -1 
	AND iSlotToCheck <= GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(eProperty) - 1
		IF iPlayerSlot > -1
		AND iPlayerSlot < ENUM_TO_INT(ARCADE_CAB_MAX_NUM_PLAYERS)
			RETURN serverBD.iDisplaySlotUsedByPlayer[iSlotToCheck][iPlayerSlot]
		ELSE
			PRINTLN("[AM_ARC_CAB] - GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT - INVALID iPlayerSlot: ", iPlayerSlot)
		ENDIF
	ELSE	
		PRINTLN("[AM_ARC_CAB] - GET_PLAYER_INDEX_USING_THIS_ARCADE_CABINET_DISPLAY_SLOT - INVALID iSlotToCheck: ", iSlotToCheck)
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Get arcade cabinet manager current state
FUNC ARCADE_CABINET_MANAGER_STATE GET_ARCADE_CABINET_MANAGER_STATE()
	RETURN g_ArcadeCabinetManagerData.eArcadeCabinetManagerState
ENDFUNC

SCRIPT_TIMER sArcadeCabManagerScriptTimer
THREADID arcadeCabinetManagerThread
/// PURPOSE:
///    Fill ACM_LAUNCHER_STRUCT with arcade cabinet type, coords and heading. Call until it returns true
FUNC BOOL START_ARCADE_CABINET_MANAGER_SCRIPT(ACM_LAUNCHER_STRUCT sArcadeCabManagerLauncherData)
	IF !IS_SAFE_TO_START_ARCADE_CABINET_MANAGER_SCRIPT()
		PRINTLN("[AM_ARC_CAB] START_ARCADE_CABINET_MANAGER_SCRIPT IS_SAFE_TO_START_ARCADE_CABINET_MANAGER_SCRIPT false")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_KILL_ACM_SCRIPT()
		IF NOT HAS_NET_TIMER_STARTED(sArcadeCabManagerScriptTimer)
			START_NET_TIMER(sArcadeCabManagerScriptTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(sArcadeCabManagerScriptTimer, 5000)
				RESET_NET_TIMER(sArcadeCabManagerScriptTimer)
				SET_KILL_ARCADE_CABINET_MANAGER_SCRIPT(FALSE)
			ENDIF	
		ENDIF
	ENDIF
	
	IF NOT IS_THREAD_ACTIVE(arcadeCabinetManagerThread)
	AND NOT HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("AM_MP_ARC_CAB_MANAGER"))
		REQUEST_SCRIPT("AM_MP_ARC_CAB_MANAGER")
	ENDIF

	IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("AM_MP_ARC_CAB_MANAGER"))
	AND NOT IS_THREAD_ACTIVE(arcadeCabinetManagerThread)
	AND !SHOULD_KILL_ACM_SCRIPT()
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_ARC_CAB_MANAGER")) < 1
			IF NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_ARC_CAB_MANAGER", sArcadeCabManagerLauncherData.iInstanceId, TRUE)
				arcadeCabinetManagerThread = START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(HASH("AM_MP_ARC_CAB_MANAGER"), sArcadeCabManagerLauncherData, SIZE_OF(sArcadeCabManagerLauncherData), SCRIPT_XML_STACK_SIZE)
				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("AM_MP_ARC_CAB_MANAGER"))
				RESET_NET_TIMER(sArcadeCabManagerScriptTimer)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AM_ARC_CAB] START_ARCADE_CABINET_MANAGER_SCRIPT launching script")
				#ENDIF
				RETURN TRUE					
			ENDIF
		ENDIF
	ELIF IS_THREAD_ACTIVE(arcadeCabinetManagerThread)
	AND !SHOULD_KILL_ACM_SCRIPT()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get number slots that arcade cabinet take
FUNC INT GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(ARCADE_CABINETS eArcadeCabinet, BOOL bManagementMenu = FALSE)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			RETURN 2
		BREAK
		CASE ARCADE_CABINET_CH_CLAW_CRANE
		CASE ARCADE_CABINET_CH_LOVE_METER
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
			IF bManagementMenu 
				RETURN 2
			ENDIF
		BREAK	
	ENDSWITCH	
	
	RETURN 1
ENDFUNC

/// PURPOSE:
///    Get neighbour slots for 2 player games e.g. night drive
FUNC ARCADE_CAB_MANAGER_SLOT GET_ARCADE_CABINET_NEIGHBOUR_SLOT(ARCADE_CAB_MANAGER_SLOT eSlot)
	SWITCH eSlot
		CASE ACM_SLOT_32	RETURN ACM_SLOT_31
		CASE ACM_SLOT_31	RETURN ACM_SLOT_32
		CASE ACM_SLOT_14	RETURN ACM_SLOT_15
		CASE ACM_SLOT_15	RETURN ACM_SLOT_14
		CASE ACM_SLOT_16	RETURN ACM_SLOT_17
		CASE ACM_SLOT_17	RETURN ACM_SLOT_16
		CASE ACM_SLOT_9		RETURN ACM_SLOT_8
		CASE ACM_SLOT_8		RETURN ACM_SLOT_9
	ENDSWITCH
	
	RETURN eSlot
ENDFUNC

FUNC BOOL IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(ARCADE_CAB_MANAGER_SLOT eArcadeCabinetSlot)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH eArcadeCabinetSlot
			CASE ACM_SLOT_32
			CASE ACM_SLOT_31
			CASE ACM_SLOT_9
			CASE ACM_SLOT_8
			CASE ACM_SLOT_14
			CASE ACM_SLOT_15
			CASE ACM_SLOT_16
			CASE ACM_SLOT_17
				RETURN TRUE
			BREAK	
		ENDSWITCH
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if arcade cabinet is physical game e.g. claw crane
FUNC BOOL IS_THIS_PHYSICAL_ARCADE_CABINET(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_CLAW_CRANE
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
		CASE ARCADE_CABINET_CH_LOVE_METER
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
		#ENDIF
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Check if arcade cabinet needs 4 props (4 players) 
FUNC BOOL IS_THIS_FOUR_PLAYERS_ARCADE_CABINET(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CA_STREET_CRIMES
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Check if display slot is suitable for 4 players arcade e.g. street crime 
FUNC BOOL IS_THIS_DISPLAY_SLOT_SUITABLE_FOR_FOUR_PLAYERS_ARCADE_CABINET(ARCADE_CAB_MANAGER_SLOT eArcadeCabinetSlot)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH eArcadeCabinetSlot
			CASE ACM_SLOT_24
			CASE ACM_SLOT_25
			CASE ACM_SLOT_26
			CASE ACM_SLOT_27
			CASE ACM_SLOT_4
			CASE ACM_SLOT_5
			CASE ACM_SLOT_6
			CASE ACM_SLOT_7
			CASE ACM_SLOT_10
			CASE ACM_SLOT_11
			CASE ACM_SLOT_12
			CASE ACM_SLOT_13
			CASE ACM_SLOT_18
			CASE ACM_SLOT_19
			CASE ACM_SLOT_20
			CASE ACM_SLOT_21
				RETURN TRUE
			BREAK	
		ENDSWITCH
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get group index for street crime to add 4 players in same game
FUNC ARCADE_CAB_GROUP GET_ARCADE_CABINET_GROUP(ARCADE_CAB_MANAGER_SLOT eArcadeCabinetSlot)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH eArcadeCabinetSlot
			CASE ACM_SLOT_24
			CASE ACM_SLOT_25
			CASE ACM_SLOT_26
			CASE ACM_SLOT_27
				RETURN ARCADE_CABINET_GROUP_1
			CASE ACM_SLOT_4
			CASE ACM_SLOT_5
			CASE ACM_SLOT_6
			CASE ACM_SLOT_7
				RETURN ARCADE_CABINET_GROUP_2
			CASE ACM_SLOT_10
			CASE ACM_SLOT_11
			CASE ACM_SLOT_12
			CASE ACM_SLOT_13
				RETURN ARCADE_CABINET_GROUP_3	
			CASE ACM_SLOT_18
			CASE ACM_SLOT_19
			CASE ACM_SLOT_20
			CASE ACM_SLOT_21
				RETURN ARCADE_CABINET_GROUP_4
			CASE ACM_SLOT_8
			CASE ACM_SLOT_9	
				RETURN ARCADE_CABINET_GROUP_1
			CASE ACM_SLOT_14
			CASE ACM_SLOT_15
				RETURN ARCADE_CABINET_GROUP_2	
			CASE ACM_SLOT_16
			CASE ACM_SLOT_17
				RETURN ARCADE_CABINET_GROUP_3
			CASE ACM_SLOT_31
			CASE ACM_SLOT_32
				RETURN ARCADE_CABINET_GROUP_4	
		ENDSWITCH
	ENDIF	
	
	RETURN ARCADE_CABINET_GROUP_INVALID
ENDFUNC

FUNC BOOL IS_THIS_ARCADE_CABINET_LOCKED(ARCADE_CABINETS eArcadeCabinet)
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockAllArcadeCabinets")
		RETURN FALSE
	ENDIF
	#ENDIF

	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_SUM_QUB3D
			IF !g_sMPTunables.bSUM_CABINET_QUB3D_ENABLE
				RETURN TRUE
			ENDIF	
		BREAK
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
			IF !g_sMPTunables.bTUNER_CABINET_CAMHEDZ_ENABLE
				RETURN TRUE
			ENDIF	
		BREAK
		#ENDIF
	ENDSWITCH 

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if display slot is suitable for arcade cabinet type
FUNC BOOL IS_THIS_DISPLAY_SLOT_SUITABLE_FOR_ARCADE_CABINET(ARCADE_CAB_MANAGER_SLOT eArcadeCabinetSlot, ARCADE_CABINETS eArcadeCabinet, BOOL bManagementMenu = FALSE)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH eArcadeCabinetSlot
			CASE ACM_SLOT_1		
			CASE ACM_SLOT_2
				IF GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(eArcadeCabinet) = 2
				OR IS_THIS_FOUR_PLAYERS_ARCADE_CABINET(eArcadeCabinet)
				OR IS_THIS_ARCADE_FOR_A_MINI_GAME(eArcadeCabinet)
					RETURN FALSE
				ENDIF
			BREAK
			CASE ACM_SLOT_3
				IF GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(eArcadeCabinet) = 2
				OR IS_THIS_FOUR_PLAYERS_ARCADE_CABINET(eArcadeCabinet)
				OR IS_THIS_ARCADE_FOR_A_MINI_GAME(eArcadeCabinet)
				#IF FEATURE_SUMMER_2020
				OR (eArcadeCabinet = ARCADE_CABINET_SUM_STRENGTH_TEST)
				#ENDIF
					RETURN FALSE
				ENDIF
			BREAK
			CASE ACM_SLOT_32
			CASE ACM_SLOT_31
			CASE ACM_SLOT_9
			CASE ACM_SLOT_8
			CASE ACM_SLOT_14
			CASE ACM_SLOT_15
			CASE ACM_SLOT_16
			CASE ACM_SLOT_17
				IF IS_THIS_FOUR_PLAYERS_ARCADE_CABINET(eArcadeCabinet)
				OR IS_THIS_ARCADE_FOR_A_MINI_GAME(eArcadeCabinet)
					RETURN FALSE
				ENDIF
			BREAK
			CASE ACM_SLOT_36
				IF eArcadeCabinet != ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
					RETURN FALSE
				ENDIF	
			BREAK
			CASE ACM_SLOT_37
				IF eArcadeCabinet != ARCADE_CABINET_CH_HACKING_MINI_G
					RETURN FALSE
				ENDIF	
			BREAK
			CASE ACM_SLOT_38
			CASE ACM_SLOT_39
			CASE ACM_SLOT_40
			CASE ACM_SLOT_41
				IF eArcadeCabinet != ARCADE_CABINET_CH_DRONE_MINI_G
					RETURN FALSE
				ENDIF	
			BREAK
			DEFAULT 
				IF IS_THIS_DISPLAY_SLOT_SUITABLE_FOR_FOUR_PLAYERS_ARCADE_CABINET(eArcadeCabinetSlot)
					IF ((IS_THIS_PHYSICAL_ARCADE_CABINET(eArcadeCabinet) AND eArcadeCabinet != ARCADE_CABINET_CH_FORTUNE_TELLER)
					OR (!IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(eArcadeCabinetSlot) AND GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(eArcadeCabinet, bManagementMenu) = 2)
					OR IS_THIS_ARCADE_FOR_A_MINI_GAME(eArcadeCabinet))
						RETURN FALSE
					ENDIF	
				ELSE
					IF ((IS_THIS_PHYSICAL_ARCADE_CABINET(eArcadeCabinet) AND eArcadeCabinet != ARCADE_CABINET_CH_FORTUNE_TELLER)
					OR GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(eArcadeCabinet) = 2
					OR IS_THIS_FOUR_PLAYERS_ARCADE_CABINET(eArcadeCabinet)
					OR IS_THIS_ARCADE_FOR_A_MINI_GAME(eArcadeCabinet))
						RETURN FALSE
					ENDIF
				ENDIF	
			BREAK
		ENDSWITCH
	#IF FEATURE_TUNER
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	SWITCH eArcadeCabinetSlot
		CASE ACM_SLOT_1
			SWITCH eArcadeCabinet
				CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
				CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
				CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
					IF HAS_PLAYER_UNLOCKED_AUTO_SHOP_RACE_AND_CHASE(PLAYER_ID())
						RETURN TRUE
					ENDIF
				BREAK
			ENDSWITCH
				
			RETURN FALSE
		BREAK
		CASE ACM_SLOT_2					
			IF eArcadeCabinet != ARCADE_CABINET_SUM_QUB3D
				RETURN FALSE
			ENDIF
		BREAK
		ENDSWITCH
	#ENDIF
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	SWITCH eArcadeCabinetSlot
		CASE ACM_SLOT_1
			IF eArcadeCabinet != ARCADE_CABINET_CH_GG_SPACE_MONKEY
				RETURN FALSE
			ENDIF		
		BREAK
		CASE ACM_SLOT_2
			IF eArcadeCabinet != ARCADE_CABINET_CH_WIZARDS_SLEVE
				RETURN FALSE
			ENDIF		
		BREAK
		ENDSWITCH
	#IF FEATURE_MUSIC_STUDIO
	ELIF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
	SWITCH eArcadeCabinetSlot
		CASE ACM_SLOT_1
			IF eArcadeCabinet != ARCADE_CABINET_SUM_QUB3D
				RETURN FALSE
			ENDIF		
		BREAK
		ENDSWITCH
	#ENDIF
	#ENDIF
	ENDIF	
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///     Get next suitable free display slot 
FUNC INT GET_NEXT_SUITABLE_FREE_DISPLAY_SLOT_FOR_ARCADE_CABINET_TYPE(ARCADE_CABINETS eArcadeCabinetToCheck, ARCADE_CABINET_PROPERTY eProperty)
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(eProperty) - 1
		IF GET_LOCAL_PLAYER_ARCADE_CABINET_SAVE_SLOT_TYPE(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), ACP_ARCADE) = ARCADE_CABINET_SAVE_SLOT_INVALID
			IF IS_THIS_DISPLAY_SLOT_SUITABLE_FOR_ARCADE_CABINET(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), eArcadeCabinetToCheck)
				IF GET_NUM_SLOT_SPACE_ARCADE_CABINET_TAKE(eArcadeCabinetToCheck) =  2
				OR IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i))
					IF GET_LOCAL_PLAYER_ARCADE_CABINET_SAVE_SLOT_TYPE(GET_ARCADE_CABINET_NEIGHBOUR_SLOT(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i)), eProperty) = ARCADE_CABINET_SAVE_SLOT_INVALID
						RETURN i
					ENDIF
				ELSE
					RETURN i
				ENDIF	
			ENDIF	
		ENDIF
	ENDFOR
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Get slot coords and heading for arcade cabinets 
///    bArrow - Get coords for arcade management marker 
PROC GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(ARCADE_CABINET_PROPERTY eProperty, ARCADE_CAB_MANAGER_SLOT eArcadeCabinetSlot, ARCADE_CABINETS eArcadeCabinet, VECTOR &vCoord, FLOAT &fHeading, BOOL bArrow = FALSE)
	
	SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
	
	IF eSimpleInterior = SIMPLE_INTERIOR_INVALID
		PRINTLN("[AM_ARC_CAB] - GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING - eSimpleInterior = SIMPLE_INTERIOR_INVALID")
		EXIT
	ENDIF
	
	SWITCH eProperty
		CASE ACP_ARCADE
			SWITCH eArcadeCabinetSlot
				CASE ACM_SLOT_1
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2731.353, -371.671, -50.0>>
							fHeading = 0.0
						BREAK
						#IF FEATURE_SUMMER_2020
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2731.353, -371.671, -49.99>>
							fHeading = 0.0
						BREAK
						#ENDIF
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2731.353, -371.401, -50.0>>
							fHeading = 0.0
						BREAK
						DEFAULT
							vCoord = <<2731.353, -371.489, -50.0>>
							fHeading = 0.0
						BREAK
					ENDSWITCH	
				BREAK	
				CASE ACM_SLOT_2
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2733.500, -371.671, -50.0>>
							fHeading = 0.0
						BREAK
						#IF FEATURE_SUMMER_2020
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2733.500, -371.671, -49.99>>
							fHeading = 0.0
						BREAK
						#ENDIF
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2733.500, -371.401, -50.0>>
							fHeading = 0.0
						BREAK
						DEFAULT
							vCoord = <<2733.500, -371.489, -50.0>>
							fHeading = 0.0
						BREAK
					ENDSWITCH	
				BREAK	
				CASE ACM_SLOT_3
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2735.393, -371.681, -50.0>>
							fHeading = 0.0
						BREAK
						#IF FEATURE_SUMMER_2020
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2735.393, -371.681, -49.99>>
							fHeading = 0.0
						BREAK
						#ENDIF
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2735.393, -371.401, -50.0>>
							fHeading = 0.0
						BREAK
						DEFAULT
							vCoord = <<2735.393, -371.489, -50.0>>
							fHeading = 0.0
						BREAK
					ENDSWITCH	
				BREAK
				CASE ACM_SLOT_34
					vCoord = <<2730.043, -374.551, -50.0>>
					fHeading = 90.0
				BREAK	
				CASE ACM_SLOT_33
					vCoord = <<2730.043, -375.461, -50.0>>
					fHeading = 90.0
				BREAK	
				CASE ACM_SLOT_32
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2730.474, -377.572, -50.0>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
							vCoord = <<2730.444, -377.692, -50.0>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2730.404, -377.572, -50.0>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2730.404, -377.572, -49.99>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2730.094, -377.572, -50.0>>
							fHeading = 90.0
						BREAK
						DEFAULT
							vCoord = <<2730.144, -377.562, -50.0>>
							fHeading = 90.0
						BREAK
					ENDSWITCH
				BREAK	
				CASE ACM_SLOT_31
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2730.474, -377.572, -50.0>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
							vCoord = <<2730.444, -377.692, -50.0>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2730.404, -377.572, -50.0>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2730.404, -377.572, -49.99>>
							fHeading = 90.0
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2730.094, -377.572, -50.0>>
							fHeading = 90.0
						BREAK
						DEFAULT
							vCoord = <<2730.144, -378.494, -50.0>>
							fHeading = 90.0
						BREAK
					ENDSWITCH
				BREAK	
				CASE ACM_SLOT_30
					vCoord = <<2729.147, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_29
					vCoord = <<2728.201, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_28
					vCoord = <<2727.211, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_27
					vCoord = <<2726.251, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_26
					vCoord = <<2725.291, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_25
					vCoord = <<2724.330, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_24
					vCoord = <<2723.360, -379.508, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_13
					vCoord = <<2732.556, -387.504, -50.0>>
					IF !bArrow
						fHeading = -135.0
					ELSE
						fHeading = -225.0
					ENDIF	
				BREAK	
				CASE ACM_SLOT_12
					vCoord = <<2733.226, -386.830, -50.0>>
					IF !bArrow
						fHeading = -135.0
					ELSE
						fHeading = -225.0
					ENDIF	
				BREAK	
				CASE ACM_SLOT_11
					vCoord = <<2733.891, -386.164, -50.0>>
					IF !bArrow
						fHeading = -135.0
					ELSE
						fHeading = -225.0
					ENDIF	
				BREAK	
				CASE ACM_SLOT_10
					vCoord = <<2734.327, -385.089, -50.0>>
					fHeading = -90.0 
				BREAK	
				CASE ACM_SLOT_9
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2733.950, -382.630, -50.0>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
							vCoord = <<2733.930, -382.840, -50.0>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2734.147, -382.620, -50.0>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2734.147, -382.620, -49.99>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2734.357, -382.620, -50.0>>
							fHeading = -90.0
						BREAK
						DEFAULT
							vCoord = <<2734.237, -383.259, -50.0>>
							fHeading = -90.0
						BREAK
					ENDSWITCH	
				BREAK	
				CASE ACM_SLOT_8
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2733.950, -382.630, -50.0>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
							vCoord = <<2733.930, -382.900, -50.0>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2734.147, -382.620, -50.0>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2734.147, -382.620, -49.99>>
							fHeading = -90.0
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2734.357, -382.620, -50.0>>
							fHeading = -90.0
						BREAK
						DEFAULT
							vCoord = <<2734.237, -382.289, -50.0>>
							fHeading = -90.0 
						BREAK
					ENDSWITCH	
				BREAK	
				CASE ACM_SLOT_7
					vCoord = <<2733.8096, -377.8820, -50.0>>
					fHeading = -90.0 
				BREAK	
				CASE ACM_SLOT_6
					vCoord = <<2733.8096, -376.9826, -50.0>>
					fHeading = -90.0
				BREAK	
				CASE ACM_SLOT_5
					vCoord = <<2733.8096, -376.0827, -50.0>>
					fHeading = -90.0
				BREAK	
				CASE ACM_SLOT_4
					vCoord = <<2733.8096, -375.1803, -50.0>>
					fHeading = -90.0
				BREAK	
				CASE ACM_SLOT_20
					vCoord = <<2729.6401, -383.070, -50.0>>
					fHeading = 180.0
				BREAK	
				CASE ACM_SLOT_21
					vCoord = <<2728.7383, -383.070, -50.0>>
					fHeading = 180.0 
				BREAK	
				CASE ACM_SLOT_23
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_FORTUNE_TELLER
							vCoord = <<2727.888, -383.8990, -50.0>>
							fHeading = -90.0
						BREAK
						DEFAULT
							vCoord = <<2728.3484, -383.8990, -50.0>>
							fHeading = -90.0
						BREAK
					ENDSWITCH	
				BREAK	
				CASE ACM_SLOT_18
					vCoord = <<2728.7383, -384.720, -50.0>>
					fHeading = 0.0
				BREAK	
				CASE ACM_SLOT_19
					vCoord = <<2729.6382, -384.720, -50.0>>
					fHeading = 0.0 
				BREAK	
				CASE ACM_SLOT_22
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_FORTUNE_TELLER
							vCoord = <<2730.479, -383.8990, -50.0>>
							fHeading = 90.0
						BREAK
						DEFAULT
							vCoord = <<2730.1494, -383.8990, -50.0>>
							fHeading = 90.0
						BREAK	
					ENDSWITCH
				BREAK	
				CASE ACM_SLOT_14
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2738.078, -385.993, -49.410>>
							fHeading = -90.0 
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2738.177, -385.993, -49.410>>
							fHeading = -90.0 
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2738.467, -385.969, -49.396>>
							fHeading = -90.0 
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2738.467, -385.969, -49.410>>
							fHeading = -90.0 
						BREAK	
						DEFAULT
							vCoord = <<2738.437, -385.319, -49.410>>
							fHeading = -90.0
						BREAK
					ENDSWITCH
				BREAK	
				CASE ACM_SLOT_15
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2738.078, -385.993, -49.410>>
							fHeading = -90.0 
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2738.177, -385.993, -49.410>>
							fHeading = -90.0 
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2738.177, -385.969, -49.396>>
							fHeading = -90.0 
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2738.177, -385.969, -49.410>>
							fHeading = -90.0 
						BREAK	
						DEFAULT
							vCoord = <<2738.437, -386.659, -49.410>>
							fHeading = -90.0
						BREAK
					ENDSWITCH
				BREAK	
				CASE ACM_SLOT_16
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2731.237, -391.719, -49.410>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
							vCoord = <<2731.607, -391.659, -49.410>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2731.607, -391.839, -49.410>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2731.297, -392.009, -49.396>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2731.297, -392.009, -49.410>>
							fHeading = 180.0
						BREAK
						DEFAULT
							vCoord = <<2731.697, -392.009, -49.410>>
							fHeading = 180.0
						BREAK
					ENDSWITCH
				BREAK	
				CASE ACM_SLOT_17
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
						#IF FEATURE_TUNER
						CASE ARCADE_CABINET_SE_CAMHEDZ
						#ENDIF
							vCoord = <<2731.237, -391.719, -49.410>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
							vCoord = <<2731.607, -391.659, -49.410>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_CH_CLAW_CRANE
							vCoord = <<2731.607, -391.839, -49.410>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_SUM_STRENGTH_TEST
							vCoord = <<2731.297, -392.009, -49.396>>
							fHeading = 180.0
						BREAK
						CASE ARCADE_CABINET_CH_LOVE_METER
							vCoord = <<2731.297, -392.009, -49.410>>
							fHeading = 180.0
						BREAK
						DEFAULT
							vCoord = <<2730.487, -392.009, -49.410>>
							fHeading = 180.0
						BREAK
					ENDSWITCH
				BREAK
				CASE ACM_SLOT_35
					vCoord = <<2730.043, -373.551, -50.0>>
					fHeading = 90.0
				BREAK
				CASE ACM_SLOT_36
					vCoord = <<2693.017, -372.428, -54.442>>
					fHeading = -180.000
				BREAK	
				CASE ACM_SLOT_37
					vCoord = <<2687.497, -370.42, -54.450>>
					fHeading = 90.0
				BREAK
				CASE ACM_SLOT_38
					vCoord = <<2696.054, -368.974, -55.780>>
					fHeading = 135.0
				BREAK
				CASE ACM_SLOT_39
					vCoord = <<2696.054, -370.124, -55.780>>
					fHeading = 45.0
				BREAK
				CASE ACM_SLOT_40
					vCoord = <<2694.954, -370.074, -55.780>>
					fHeading = -45.0
				BREAK
				CASE ACM_SLOT_41
					vCoord = <<2694.954, -368.974, -55.780>>
					fHeading = -135.000
				BREAK
			ENDSWITCH
		BREAK
		CASE ACP_AUTO_SHOP
			SWITCH eArcadeCabinetSlot
				CASE ACM_SLOT_1
					SWITCH eArcadeCabinet
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
						CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
							vCoord = << -1349.791, 145.681, -100.197 >>
							fHeading = -180.0
						BREAK					
					ENDSWITCH
				BREAK
				CASE ACM_SLOT_2
					vCoord = << -1354.709, 144.414, -96.125 >>
					fHeading = 0.0
				BREAK
			ENDSWITCH
		BREAK
		CASE ACP_FIXER_HQ
			SWITCH eArcadeCabinetSlot
				CASE ACM_SLOT_1						
					vCoord = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< -1.6217, 11.999, 8.596 >>, eSimpleInterior)	// Original value at Hawick << 396.644, -63.307, 110.959 >>
					fHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(0.199997, eSimpleInterior)	// Original value at Hawick -109.800
				BREAK
				CASE ACM_SLOT_2					
					vCoord = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 3.15086, -8.38502, 8.596 >>, eSimpleInterior)	// Original value at Hawick << 375.857, -60.820, 110.959 >>
					fHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(89.479996, eSimpleInterior) // Original value at Hawick	-20.520 
				BREAK
			ENDSWITCH
		BREAK
		CASE ACP_MUSIC_STUDIO
			SWITCH eArcadeCabinetSlot
				CASE ACM_SLOT_1					
					vCoord = <<-1007.562, -84.96226, -100.4084>>
					fHeading = 180.0
				BREAK
			ENDSWITCH		
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Get camera coord, rotation and FOV for managemenet menu
PROC GET_ARCADE_CABINET_DISPLAY_SLOT_CAMERA_DETAILS(ARCADE_CAB_MANAGER_SLOT eArcadeCabinetSlot, VECTOR &vCoord, VECTOR &vRot, FLOAT &fFOV)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		SWITCH eArcadeCabinetSlot
			CASE ACM_SLOT_1
			CASE ACM_SLOT_2
			CASE ACM_SLOT_3
				vCoord = <<2732.2646, -375.2436, -48.2872>>
				vRot = <<-1.5224, -0.0000, -3.7113>>
				fFOV = 65.4157
			BREAK	
			CASE ACM_SLOT_31
			CASE ACM_SLOT_32
				vCoord = <<2732.7944, -377.6685, -47.8780>>
				vRot = <<-21.4616, -0.0000, 90.0891>>
				fFOV = 67.4000
			BREAK
			CASE ACM_SLOT_33
			CASE ACM_SLOT_34
			CASE ACM_SLOT_35
				vCoord = <<2734.4597, -373.0278, -47.7808>>
				vRot = <<-8.0694, -0.0000, 123.6167>>
				fFOV = 43.2000
			BREAK	
			CASE ACM_SLOT_24	
			CASE ACM_SLOT_25
				vCoord = <<2724.2939, -382.6304, -48.4285>>
				vRot = <<-0.3996, -0.0000, -1.2700>>
				fFOV = 67.4000
			BREAK
			CASE ACM_SLOT_26
			CASE ACM_SLOT_27
			CASE ACM_SLOT_28
			CASE ACM_SLOT_29
				vCoord = <<2726.1958, -382.6548, -48.4285>>
				vRot = <<-0.3996, -0.0000, -1.2700>>
				fFOV = 67.4000
			BREAK
			CASE ACM_SLOT_30
				vCoord = <<2730.9624, -381.6673, -48.4260>>
				vRot = <<-0.3996, -0.0000, 35.9577>>
				fFOV = 67.4000
			BREAK	
			CASE ACM_SLOT_8
			CASE ACM_SLOT_9
				vCoord = <<2729.6982, -381.6277, -48.0543>>
				vRot = <<-8.5024, 0.0000, -102.5527>>
				fFOV = 46.9500
			BREAK
			CASE ACM_SLOT_10	
			CASE ACM_SLOT_11
			CASE ACM_SLOT_12
			CASE ACM_SLOT_13
				vCoord = <<2730.4231, -384.4293, -48.2102>>
				vRot = <<-3.6031, -0.0000, -125.0199>>
				fFOV = 46.9500
			BREAK	
			CASE ACM_SLOT_4
			CASE ACM_SLOT_5
			CASE ACM_SLOT_6	
			CASE ACM_SLOT_7
				vCoord = <<2731.1306, -376.2206, -47.6664>>
				vRot = <<-14.9062, -0.0000, -89.9739>>
				fFOV = 68.2700
			BREAK	
			CASE ACM_SLOT_20
			CASE ACM_SLOT_21	
				vCoord = <<2732.7856, -379.7342, -47.4701>>
				vRot = <<-14.1115, -0.0000, 135.9931>>
				fFOV = 44.1075
			BREAK
			CASE ACM_SLOT_22
				vCoord = <<2733.8718, -381.7208, -47.4049>>
				vRot = <<-12.5768, -0.0000, 118.4625>>
				fFOV = 44.1075
			BREAK
			CASE ACM_SLOT_23
				vCoord = <<2724.7441, -381.4478, -47.4701>>
				vRot = <<-13.0712, -0.0000, -118.7990>>
				fFOV = 44.1075
			BREAK	
			CASE ACM_SLOT_18
			CASE ACM_SLOT_19	
				vCoord = <<2732.0305, -388.6134, -47.4701>>
				vRot = <<-14.7815, -0.0000, 36.6859>>
				fFOV = 44.1075
			BREAK		
			CASE ACM_SLOT_14
			CASE ACM_SLOT_15
				vCoord = <<2734.9138, -387.1254, -47.9171>>
				vRot = <<-7.8252, -0.0000, -70.2962>>
				fFOV = 65.5289
			BREAK	
			CASE ACM_SLOT_16
			CASE ACM_SLOT_17
				vCoord = <<2733.8889, -387.4872, -47.2307>>
				vRot = <<-12.2466, -0.0000, 155.2240>>
				fFOV = 49.2000
			BREAK
		ENDSWITCH
	#IF FEATURE_TUNER
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		SWITCH eArcadeCabinetSlot
			CASE ACM_SLOT_1								
				vCoord = <<-1346.4277, 149.9023, -97.0959>>
				vRot = <<-19.3025, 0.0000, 141.9718>>
				fFOV = 65.4157
			BREAK
			CASE ACM_SLOT_2							
				vCoord = <<-1355.3109, 141.4747, -93.2596>>
				vRot = <<-24.6498, -0.0000, -12.8019>>
				fFOV = 65.4157
			BREAK
		ENDSWITCH
	#ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Check if we are allowed the arcade cabinet of type
FUNC BOOL SHOULD_SPAWN_THIS_ARCADE_CABINET(ARCADE_CABINETS eArcadeCabinet)
	IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
			IF !HAS_PLAYER_COMPLETED_FULL_ARCADE_SETUP(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
				IF eArcadeCabinet != ARCADE_CABINET_CA_STREET_CRIMES
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF	
		
		SWITCH eArcadeCabinet
			CASE ARCADE_CABINET_CH_DRONE_MINI_G
				RETURN FALSE
		ENDSWITCH
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_GRAB_THE_NEAREST_OBJECT_FROM_INTERIOR_FOR_THIS_ARCADE_CABINET(ARCADE_CABINETS eArcadeCabinet)
	UNUSED_PARAMETER(eArcadeCabinet)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Props are created by interior or part of entity sets
FUNC BOOL DOES_THIS_ARCADE_CABINETS_PROP_INCLUDED_IN_INTERIOR(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Freeze arcade cabinet prop on creation
FUNC BOOL SHOULD_FREEZE_ARCADE_CABINET_ON_CREATION(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Hide arcade cabinet prop on creation
FUNC BOOL SHOULD_ARCADE_CABINET_BE_VISIBLE_ON_CREATION(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Create arcade cabinet props
PROC CREATE_ARCADE_CABINETS(ARCADE_CABINET_DETAILS &sACMData, INT iDisplaySlot, INT iStreetCrimeIndex = 0)
	IF sACMData.eArcadeCab[iDisplaySlot] = ARCADE_CABINET_CA_STREET_CRIMES		
		// Initialise animation variation
		g_ArcadeCabinetManagerData.iAnimVariation[iDisplaySlot] = iStreetCrimeIndex
		sACMData.iPropModelVar = iStreetCrimeIndex
		PRINTLN("[AM_ARC_CAB] - CREATE_ARCADE_CABINETS sACMData.iPropModelVar: ", iStreetCrimeIndex)
	ENDIF
	
	IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
	AND NOT DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iDisplaySlot])
		GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), sACMData.eArcadeCab[iDisplaySlot]
																	, sACMData.vArcadeCabCoords[iDisplaySlot], sACMData.fArcadeCabHeading[iDisplaySlot])
		
		IF SHOULD_SPAWN_THIS_ARCADE_CABINET(sACMData.eArcadeCab[iDisplaySlot])
			GET_ARCADE_CABINET_DETAILS(sACMData.eArcadeCab[iDisplaySlot], sACMData)
			IF REQUEST_LOAD_MODEL(sACMData.propModel)
				IF NOT IS_VECTOR_ZERO(sACMData.vArcadeCabCoords[iDisplaySlot])
					sACMData.arcadeCabinetProp[iDisplaySlot] = CREATE_OBJECT_NO_OFFSET(sACMData.propModel, sACMData.vArcadeCabCoords[iDisplaySlot], FALSE, FALSE, TRUE)
					
					IF SHOULD_FREEZE_ARCADE_CABINET_ON_CREATION(sACMData.eArcadeCab[iDisplaySlot])
						FREEZE_ENTITY_POSITION(sACMData.arcadeCabinetProp[iDisplaySlot], TRUE)
					ENDIF
					
					SET_ENTITY_COORDS_NO_OFFSET(sACMData.arcadeCabinetProp[iDisplaySlot], sACMData.vArcadeCabCoords[iDisplaySlot])
					
					IF !SHOULD_ARCADE_CABINET_BE_VISIBLE_ON_CREATION(sACMData.eArcadeCab[iDisplaySlot])
						SET_ENTITY_VISIBLE(sACMData.arcadeCabinetProp[iDisplaySlot], FALSE)
					ENDIF
					
					SET_ENTITY_HEADING(sACMData.arcadeCabinetProp[iDisplaySlot], sACMData.fArcadeCabHeading[iDisplaySlot])
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sACMData.arcadeCabinetProp[iDisplaySlot], TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(sACMData.arcadeCabinetProp[iDisplaySlot], FALSE)
					SET_ENTITY_INVINCIBLE(sACMData.arcadeCabinetProp[iDisplaySlot], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(sACMData.propModel)
					PRINTLN("[AM_ARC_CAB] - CREATE_ARCADE_CABINETS: ", iDisplaySlot , " type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]))
				ELSE
					PRINTLN("[AM_ARC_CAB] - CREATE_ARCADE_CABINETS: ", iDisplaySlot , " type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]), " coords are zero")
				ENDIF
			ENDIF
		ELIF SHOULD_GRAB_THE_NEAREST_OBJECT_FROM_INTERIOR_FOR_THIS_ARCADE_CABINET(sACMData.eArcadeCab[iDisplaySlot])
			GET_ARCADE_CABINET_DETAILS(sACMData.eArcadeCab[iDisplaySlot], sACMData)
			GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), sACMData.eArcadeCab[iDisplaySlot]
																	, sACMData.vArcadeCabCoords[iDisplaySlot], sACMData.fArcadeCabHeading[iDisplaySlot])
			sACMData.arcadeCabinetProp[iDisplaySlot] = GET_CLOSEST_OBJECT_OF_TYPE(sACMData.vArcadeCabCoords[iDisplaySlot], 10.0, sACMData.propModel, FALSE, FALSE, FALSE)
			PRINTLN("[AM_ARC_CAB] - CREATE_ARCADE_CABINETS: ", iDisplaySlot , " type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]), " grab object from interior")
		ENDIF	
	ENDIF	
ENDPROC

/// PURPOSE:
///    Create arcade cabinet scren prop
PROC CREATE_ARCADE_CABINETS_SCREENS(ARCADE_CABINET_DETAILS &sACMData, INT iDisplaySlot, INT iPlayerSlot)
	IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
	AND NOT DOES_ENTITY_EXIST(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot])
		GET_ARCADE_CABINET_DISPLAY_SLOT_COORD_AND_HEADING(sACMData.eArcadeCabinetPropertyType, INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, iDisplaySlot), sACMData.eArcadeCab[iDisplaySlot]
																	, sACMData.vArcadeCabCoords[iDisplaySlot], sACMData.fArcadeCabHeading[iDisplaySlot])
		IF SHOULD_SPAWN_THIS_ARCADE_CABINET(sACMData.eArcadeCab[iDisplaySlot])
			MODEL_NAMES screenProp 
			GET_ARCADE_CABINET_SCREEN_PROP_DETAIL(sACMData.eArcadeCab[iDisplaySlot], iPlayerSlot, screenProp)
			IF IS_MODEL_VALID(screenProp)
				IF REQUEST_LOAD_MODEL(screenProp)
					IF NOT IS_VECTOR_ZERO(sACMData.vArcadeCabCoords[iDisplaySlot])
						sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot] = CREATE_OBJECT_NO_OFFSET(screenProp, sACMData.vArcadeCabCoords[iDisplaySlot], FALSE, FALSE, TRUE)
						FREEZE_ENTITY_POSITION(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], TRUE)
						SET_ENTITY_COORDS_NO_OFFSET(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], sACMData.vArcadeCabCoords[iDisplaySlot])
						SET_ENTITY_HEADING(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], sACMData.fArcadeCabHeading[iDisplaySlot])
						SET_ENTITY_VISIBLE(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], TRUE)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], TRUE)
						SET_ENTITY_CAN_BE_DAMAGED(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], FALSE)
						SET_ENTITY_INVINCIBLE(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot], TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(screenProp)
						PRINTLN("[AM_ARC_CAB] - CREATE_ARCADE_CABINETS_SCREENS: ", iDisplaySlot , " type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]))
					ELSE
						PRINTLN("[AM_ARC_CAB] - CREATE_ARCADE_CABINETS_SCREENS: ", iDisplaySlot , " type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]), " coords are zero")
					ENDIF
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF	
ENDPROC

PROC REMOVE_ARCADE_CABINETS_SCREEN_PROP(ARCADE_CABINET_DETAILS &sACMData, INT iDisplaySlot, INT iPlayerSlot)
	IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
	AND DOES_ENTITY_EXIST(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot])
		DELETE_OBJECT(sACMData.arcadeCabinetScreenProp[iDisplaySlot][iPlayerSlot])
		PRINTLN("[AM_ARC_CAB] - REMOVE_ARCADE_CABINETS_SCREEN_PROP: ", iDisplaySlot , " type: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]))
	ENDIF
ENDPROC

/// PURPOSE:
///    Remove arcade cabinet prop and screen props
PROC REMOVE_ARCADE_CABINETS(ARCADE_CABINET_DETAILS &sACMData, INT iDisplaySlot)
	IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iDisplaySlot])
		DELETE_OBJECT(sACMData.arcadeCabinetProp[iDisplaySlot])
	ENDIF
ENDPROC

/// PURPOSE:
///    Clean up arcade cabinet sounds
PROC STOP_ALL_ARCADE_CABINET_SOUNDS(ARCADE_CABINET_DETAILS &sACMData)
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(sACMData.eArcadeCabinetPropertyType) - 1
		IF sACMData.eArcadeCab[i] != ARCADE_CABINET_INVALID
			IF sACMData.iIdleSoundID[i] != -1
				STOP_SOUND(sACMData.iIdleSoundID[i])
				RELEASE_SOUND_ID(sACMData.iIdleSoundID[i])
				sACMData.iIdleSoundID[i] = -1
			ENDIF
			IF sACMData.iInUseSoundID[i] != -1
				STOP_SOUND(sACMData.iInUseSoundID[i])
				RELEASE_SOUND_ID(sACMData.iInUseSoundID[i])
				sACMData.iInUseSoundID[i] = -1
			ENDIF
		ENDIF	
	ENDFOR
	
	sACMData.iSoundIndexBS = 0
	sACMData.iArcadeTypeSound = 0
ENDPROC

PROC RESET_ALL_ARCADE_SOUND_INDEX_BIT_SETS(ARCADE_CABINET_DETAILS &sACMData, BOOL bResetSoundIndex = FALSE)
	INT i
	FOR i = 0 TO ENUM_TO_INT(ARCADE_CABINET_COUNT) -1
		IF IS_BIT_SET(sACMData.iSoundIndexBS, 0)
			CLEAR_BIT(sACMData.iSoundIndexBS, 0)
		ENDIF
		IF IS_BIT_SET(sACMData.iArcadeTypeSound, 0)
			CLEAR_BIT(sACMData.iArcadeTypeSound, 0)
		ENDIF
		IF bResetSoundIndex
			sACMData.iArcadeTypeIndex[i] = -1
		ENDIF	
	ENDFOR
ENDPROC

/// PURPOSE:
///    Remove arcade cabinet sup props
FUNC BOOL SERVER_REMOVE_THIS_ARCADE_CABINETS_SUB_PROPS(ARCADE_CABINET_DETAILS &sACMData, ARCADE_CABINET_MANAGER_SERVER_BD &serverBD, INT iDisplaySlot)
	ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
	INT iSubPropIndex, iNumDeletedProp
	IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
		GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[iDisplaySlot], eArcadeCabSubPropDetail)
		IF eArcadeCabSubPropDetail.iNumProp > 0
			FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
						OBJECT_INDEX objToDelete = NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
						DELETE_OBJECT(objToDelete)
						RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) - 1)
						iNumDeletedProp++
						CLEAR_BIT(serverBD.iServerReserveBS[iDisplaySlot], ARCADE_CABINET_MANAGER_SERVER_BD_RESERVE_SUB_OBJECTS)
						PRINTLN("[AM_ARC_CAB] - SERVER_REMOVE_THIS_ARCADE_CABINETS_SUB_PROPS siDisplaySlot: ", iDisplaySlot, " iSubPropIndex: ", iSubPropIndex)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
					ENDIF
				ELSE
					iNumDeletedProp++
				ENDIF
			ENDFOR
		ELSE
			RETURN TRUE
		ENDIF	
		
		IF iNumDeletedProp = eArcadeCabSubPropDetail.iNumProp 
			PRINTLN("[AM_ARC_CAB] - SERVER_REMOVE_THIS_ARCADE_CABINETS_SUB_PROPS deleted all props for iDisplaySlot:", iDisplaySlot)
			RETURN TRUE
		ELSE
			PRINTLN("[AM_ARC_CAB] - SERVER_REMOVE_THIS_ARCADE_CABINETS_SUB_PROPS waiting on: ", iDisplaySlot, " iNumDeletedProp: ", iNumDeletedProp, " eArcadeCabSubPropDetail.iNumProp: ", eArcadeCabSubPropDetail.iNumProp)
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Should freeze sub prop coords
FUNC BOOL SHOULD_FREEZE_THIS_SUB_PROP(ARCADE_CABINETS eCabinet, INT iPropIndex)
	UNUSED_PARAMETER(eCabinet)
	UNUSED_PARAMETER(iPropIndex)
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DISBALE_COLLISION_FOR_THIS_SUB_PROP(ARCADE_CABINETS eCabinet, INT iPropIndex)
	UNUSED_PARAMETER(iPropIndex)
	
	SWITCH eCabinet
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Hide arcade cabinet sub prop on creation
FUNC BOOL SHOULD_ARCADE_CABINET_SUB_PROP_BE_VISIBLE_ON_CREATION(MODEL_NAMES modelToCheck)
	IF modelToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Arcade_Claw_01a_C"))
	OR modelToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_usb_drive01x"))
	OR modelToCheck = INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Fingerprint_Scanner_01a"))
		RETURN FALSE
	ENDIF
	
	SWITCH modelToCheck
		CASE PROP_PHONE_ING
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_ENTITY_A_PRIZE(ENTITY_INDEX entityToCheck)
	IF DOES_ENTITY_EXIST(entityToCheck)
	AND NOT IS_ENTITY_DEAD(entityToCheck)
		MODEL_NAMES prizeModel = GET_ENTITY_MODEL(entityToCheck)
		IF prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_01a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_02a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_03a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_04a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_05a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_arcade_claw_plush_06a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_princess_robo_Plush_07a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_shiny_wasabi_Plush_08a"))
		OR prizeModel = INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_master_09a"))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Server creates all network sub props
FUNC BOOL SERVER_CREATE_THIS_ARCADE_CABINET_SUB_PROPS(ARCADE_CABINET_DETAILS &sACMData, ARCADE_CABINET_MANAGER_SERVER_BD &serverBD, INT iDisplaySlot, BOOL bStartInvisible = FALSE)
	ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
	
	VECTOR vCoord, vRotation
	FLOAT fHeading
	
	IF sACMData.eArcadeCab[iDisplaySlot] != ARCADE_CABINET_INVALID
		IF DOES_ENTITY_EXIST(sACMData.arcadeCabinetProp[iDisplaySlot])
			GET_ARCADE_CABINET_SUB_PROP_DETAIL(sACMData.eArcadeCab[iDisplaySlot], eArcadeCabSubPropDetail)
			
			INT iSubPropIndex
			
			IF eArcadeCabSubPropDetail.iNumProp > 0
				FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
					IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex])
						IF NOT IS_BIT_SET(serverBD.iServerReserveBS[iDisplaySlot], ARCADE_CABINET_MANAGER_SERVER_BD_RESERVE_SUB_OBJECTS)
							IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + eArcadeCabSubPropDetail.iNumProp, FALSE, TRUE)
								RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + eArcadeCabSubPropDetail.iNumProp)
								SET_BIT(serverBD.iServerReserveBS[iDisplaySlot], ARCADE_CABINET_MANAGER_SERVER_BD_RESERVE_SUB_OBJECTS)
								PRINTLN("[AM_ARC_CAB] - SERVER_CREATE_THIS_ARCADE_CABINET_SUB_PROPS ARCADE_CABINET_MANAGER_SERVER_BD_RESERVE_SUB_OBJECTS TRUE")	
							ELSE
								PRINTLN("[AM_ARC_CAB] - SERVER_CREATE_THIS_ARCADE_CABINET_SUB_PROPS CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT false")	
							ENDIF	
						ELSE
							IF REQUEST_LOAD_MODEL(eArcadeCabSubPropDetail.eSubPropModel[iSubPropIndex])
								IF CAN_REGISTER_MISSION_OBJECTS(eArcadeCabSubPropDetail.iNumProp)
									vCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sACMData.arcadeCabinetProp[iDisplaySlot], eArcadeCabSubPropDetail.vOffset[iSubPropIndex])
									fHeading = GET_ENTITY_HEADING(sACMData.arcadeCabinetProp[iDisplaySlot])
									vRotation = eArcadeCabSubPropDetail.vRotation[iSubPropIndex]
									
									IF !SHOULD_ARCADE_CABINET_SUB_PROP_BE_VISIBLE_ON_CREATION(eArcadeCabSubPropDetail.eSubPropModel[iSubPropIndex])
										bStartInvisible = TRUE
									ENDIF	
									
									IF CREATE_NET_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], eArcadeCabSubPropDetail.eSubPropModel[iSubPropIndex]
													, vCoord, TRUE, TRUE, TRUE, FALSE, TRUE, TRUE, bStartInvisible)		
										
										IF SHOULD_FREEZE_THIS_SUB_PROP(sACMData.eArcadeCab[iDisplaySlot], iSubPropIndex)
											FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), TRUE)
										ELSE
											ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]))
										ENDIF	
										
										IF SHOULD_DISBALE_COLLISION_FOR_THIS_SUB_PROP(sACMData.eArcadeCab[iDisplaySlot], iSubPropIndex)
											SET_ENTITY_COLLISION(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), FALSE)
										ENDIF
										
										SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), vCoord)
										
										SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), <<vRotation.x, vRotation.y, fHeading>>)

										NETWORK_USE_HIGH_PRECISION_BLENDING(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], TRUE)

										NETWORK_SET_OBJECT_CAN_BLEND_WHEN_FIXED(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), TRUE)

										SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), TRUE)
										
										SET_OBJECT_TINT_INDEX(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), eArcadeCabSubPropDetail.iTintID[iSubPropIndex])
										
										IF !bStartInvisible
											SET_ENTITY_VISIBLE(NET_TO_OBJ(serverBD.serverArcadeCabinetSubObj[iDisplaySlot][iSubPropIndex]), TRUE)
										ENDIF	
										
										SET_MODEL_AS_NO_LONGER_NEEDED(eArcadeCabSubPropDetail.eSubPropModel[iSubPropIndex])
										PRINTLN("[AM_ARC_CAB] - SERVER_CREATE_THIS_ARCADE_CABINET_SUB_PROPS creating sub prop for cabinet: ", GET_ARCADE_CABINET_NAME(sACMData.eArcadeCab[iDisplaySlot]), " bStartInvisible: ", bStartInvisible)	
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
						PRINTLN("[AM_ARC_CAB] - SERVER_CREATE_THIS_ARCADE_CABINET_SUB_PROPS - waiting on ", iDisplaySlot  , " iSubPropIndex: ", iSubPropIndex)
						RETURN FALSE
					ENDIF
				ENDFOR	
			ENDIF
		ENDIF	
	ENDIF	

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   Gets dislplay save slot type from arcade cabinet. Reverse of GET_ARCADE_CABINET_FROM_DISPLAY_SLOT_TYPE
FUNC ARCADE_CABINETS_SAVE_SLOT_TYPE GET_ARCADE_CABINET_TYPE_FROM_DISPLAY_SLOT(ARCADE_CABINETS eDisplaySaveSlot, INT iStreetCrimeIndex)
	SWITCH eDisplaySaveSlot
		CASE	ARCADE_CABINET_CA_STREET_CRIMES			
			SWITCH iStreetCrimeIndex
				CASE    1			RETURN	ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_A			
				CASE	2			RETURN	ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_B			
				CASE	3			RETURN	ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_C			
				CASE	4			RETURN	ARCADE_CABINET_SAVE_SLOT_CA_STREET_CRIMES_PROP_D	
			ENDSWITCH	
		BREAK	
		CASE	ARCADE_CABINET_CH_GG_SPACE_MONKEY		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_GG_SPACE_MONKEY				
		CASE	ARCADE_CABINET_CH_LAST_GUNSLINGERS		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_LAST_GUNSLINGERS				
		CASE	ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_NIGHT_DRIVE_BIKE				
		CASE	ARCADE_CABINET_CH_NIGHT_DRIVE_CAR		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_NIGHT_DRIVE_CAR				
		CASE	ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_NIGHT_DRIVE_TRUCK				
		CASE	ARCADE_CABINET_CH_WIZARDS_SLEVE			RETURN	ARCADE_CABINET_SAVE_SLOT_CH_WIZARDS_SLEVE					
		CASE	ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH	RETURN	ARCADE_CABINET_SAVE_SLOT_CH_DEFENDER_OF_THE_FAITH			
		CASE	ARCADE_CABINET_CH_MONKEYS_PARADISE		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_MONKEYS_PARAGISE				
		CASE	ARCADE_CABINET_CH_PENETRATOR			RETURN	ARCADE_CABINET_SAVE_SLOT_CH_PENETRATOR						
		CASE	ARCADE_CABINET_CH_CLAW_CRANE			RETURN	ARCADE_CABINET_SAVE_SLOT_CH_CLAW_CRANE										
		CASE	ARCADE_CABINET_CH_FORTUNE_TELLER		RETURN	ARCADE_CABINET_SAVE_SLOT_CH_FORTUNE_TELLER					
		CASE	ARCADE_CABINET_CH_LOVE_METER			RETURN	ARCADE_CABINET_SAVE_SLOT_CH_LOVE_METER						
		CASE	ARCADE_CABINET_CA_INVADE_AND_PERSUADE	RETURN	ARCADE_CABINET_SAVE_SLOT_CA_INVADE_AND_PERSUADE	
		CASE 	ARCADE_CABINET_CH_HACKING_MINI_G		RETURN 	ARCADE_CABINET_SAVE_SLOT_CH_HACKING_MINI_G
		CASE 	ARCADE_CABINET_CH_DRILLING_LASER_MINI_G	RETURN 	ARCADE_CABINET_SAVE_SLOT_CH_DRILLING_LASER_MINI_G
		CASE    ARCADE_CABINET_CH_DRONE_MINI_G			RETURN 	ARCADE_CABINET_SAVE_SLOT_CH_DRONE_MINI_G
		#IF FEATURE_TUNER
		CASE 	ARCADE_CABINET_SE_CAMHEDZ				RETURN 	ARCADE_CABINET_SAVE_SLOT_SE_CAMHEDZ
		#ENDIF
		CASE 	ARCADE_CABINET_SUM_QUB3D				RETURN 	ARCADE_CABINET_SAVE_SLOT_SUM_QUB3D
		CASE	ARCADE_CABINET_SUM_STRENGTH_TEST		RETURN	ARCADE_CABINET_SAVE_SLOT_SUM_STRENGTH_TEST		
	ENDSWITCH
	
	RETURN ARCADE_CABINET_SAVE_SLOT_INVALID
ENDFUNC

/// PURPOSE:
///    Initials player arcade cabinet boradcast data 
PROC SET_ALL_ARCADE_CABINETS_SAVE_SLOT_BROADCAST_DATA()
	INT i
	FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(ACP_ARCADE) - 1
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eArcadeCabinetType[i] = GET_LOCAL_PLAYER_ARCADE_CABINET_SAVE_SLOT_TYPE(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), ACP_ARCADE)
		PRINTLN("[AM_ARC_CAB] - SET_ALL_ARCADE_CABINETS_SAVE_SLOT_BROADCAST_DATA type: ", GET_ARCADE_CABINET_SAVE_SLOT_TYPE_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eArcadeCabinetType[i]))
	ENDFOR
	
	#IF FEATURE_TUNER	
		FOR i = 0 TO GET_ARCADE_SLOT_COUNT_FOR_PROPERTY(ACP_AUTO_SHOP) - 1
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eAutoShopArcadeCabinetType[i] = GET_LOCAL_PLAYER_ARCADE_CABINET_SAVE_SLOT_TYPE(INT_TO_ENUM(ARCADE_CAB_MANAGER_SLOT, i), ACP_AUTO_SHOP)
			PRINTLN("[AM_ARC_CAB] - SET_ALL_ARCADE_CABINETS_SAVE_SLOT_BROADCAST_DATA type: ", GET_ARCADE_CABINET_SAVE_SLOT_TYPE_NAME(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.eAutoShopArcadeCabinetType[i]))
		ENDFOR		
	#ENDIF	
ENDPROC

/// PURPOSE:
///    Gets display slot array start and end index for four player game ( street crime ) 
PROC GET_FOUR_PLAYER_ARCADE_CABINET_START_AND_END_SLOT(ARCADE_CAB_MANAGER_SLOT eSlot, INT &iStart, INT &iEnd)
	SWITCH eSlot
		CASE ACM_SLOT_24
		CASE ACM_SLOT_25
		CASE ACM_SLOT_26
		CASE ACM_SLOT_27
			iStart = ENUM_TO_INT(ACM_SLOT_24)
			iEnd = ENUM_TO_INT(ACM_SLOT_27)
		BREAK	
		CASE ACM_SLOT_4
		CASE ACM_SLOT_5
		CASE ACM_SLOT_6
		CASE ACM_SLOT_7
			iStart = ENUM_TO_INT(ACM_SLOT_4)
			iEnd = ENUM_TO_INT(ACM_SLOT_7)
		BREAK	
		CASE ACM_SLOT_10
		CASE ACM_SLOT_11
		CASE ACM_SLOT_12
		CASE ACM_SLOT_13
			iStart = ENUM_TO_INT(ACM_SLOT_10)
			iEnd = ENUM_TO_INT(ACM_SLOT_13)
		BREAK	
		CASE ACM_SLOT_18
		CASE ACM_SLOT_19
		CASE ACM_SLOT_20
		CASE ACM_SLOT_21
			iStart = ENUM_TO_INT(ACM_SLOT_18)
			iEnd = ENUM_TO_INT(ACM_SLOT_21)
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Number of arcade cabinets should manage when moving to next or previous menu
FUNC INT GET_NUM_SURROUNDED_CABINETS(ARCADE_CAB_MANAGER_SLOT eSlot)
	IF IS_THIS_DISPLAY_SLOT_SUITABLE_FOR_FOUR_PLAYERS_ARCADE_CABINET(eSlot)
		RETURN 3
	ELIF IS_THIS_SLOT_SUITABLE_FOR_TWO_PLAYER_ARCADE_CABINET(eSlot)	
		RETURN 1
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ARCADE_LOVE_METER_ANIM_PLAYING()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		INT iLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		IF iLocatePlayerIsIn > -1
			IF GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_LOVE_METER
				ARCADE_CABINET_ANIM_DETAIL eAnimDetails
				INT i 
				FOR i = ENUM_TO_INT(ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_BETTER) TO ENUM_TO_INT(ARCADE_CABINET_ANIM_CLIP_LOVE_METER_WORSE_WORSE)
					GET_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINET_CH_LOVE_METER, INT_TO_ENUM(ARCADE_CABINET_ANIM_CLIPS, i), GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()), eAnimDetails)
					IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sAnimClipName)
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sAnimClipName)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDFOR	
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PLAYING_THIS_ARCADE_CABINET_ANIMATION_CLIP(ARCADE_CABINET_ANIM_CLIPS eAnimClip)
	IF !IS_ENTITY_DEAD(PLAYER_PED_ID())
		INT iLocatePlayerIsIn = GET_ARCADE_CABINET_LOCATE_DISPLAY_SLOT_PLAYER_IS_IN(PLAYER_ID())
		IF iLocatePlayerIsIn > -1
			ARCADE_CABINET_ANIM_DETAIL eAnimDetails
			ARCADE_CABINETS eCabinetPlayerIsIn = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
			IF eCabinetPlayerIsIn != ARCADE_CABINET_INVALID
				GET_ARCADE_CABINET_ANIMATION_DETAIL(eCabinetPlayerIsIn, eAnimClip, GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID()), eAnimDetails)
				IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sAnimClipName)
				AND !IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
					RETURN IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, eAnimDetails.sAnimClipName)
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if arcade cabinet manager should clean up when this animation clip plays
FUNC BOOL SHOULD_CLEANUP_ARCADE_CABINET_MANAGER_WITH_THIS_ANIM(ARCADE_CABINET_ANIM_CLIPS eAnimClip)
	SWITCH eAnimClip
		CASE ARCADE_CABINET_ANIM_CLIP_EXIT
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_BETTER
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_AVERAGE_WORSE
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_BETTER
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_BAD_WORSE
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_FRIENDZONE_PERFECT
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_FRIENDZONE_WORSE
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_BETTER
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_GOOD_WORSE
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_BAD
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_AVERAGE
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_GOOD
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_PERFECT
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_MUTUAL_WORSE
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_PERFECT_BETTER
		CASE ARCADE_CABINET_ANIM_CLIP_LOVE_METER_WORSE_WORSE
		CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM
		CASE ARCADE_CABINET_ANIM_CLIP_HACKING_PASS
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_ARCADE_CABINET_SOUND_AFTER_EXIT_ANIM_ENDS(ARCADE_CABINET_ANIM_CLIPS eAnimClip)
	SWITCH eAnimClip
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Start script menu to choose diffrent game type
FUNC BOOL DOES_ARCADE_CABINET_HAVE_STARTUP_MENU(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			RETURN TRUE
		BREAK
	ENDSWITCH 
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Locate size should be double in x axis 
FUNC BOOL SHOULD_DOUBLE_LOCATE_X_AXIS_FOR_THIS_ARCADE_CABINET(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_ARCADE_CABINET_HAVE_TWO_ENTER_ANIM_CLIP(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

// PURPOSE: 
//	Returns TRUE if the sync scene is past the specified phase
FUNC BOOL IS_ARCADE_CABINET_SYNC_SCENE_PHASE_PASSED(ARCADE_CABINET_ANIM_CLIPS eClip, FLOAT fPhase)
	ARCADE_CABINETS eArcadeCAbinet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	ARCADE_CABINET_ANIM_DETAIL sAnimClip
	
	IF IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		RETURN FALSE
	ENDIF	
	
	IF !HAS_ANIM_DICT_LOADED(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		RETURN FALSE
	ENDIF
	
	IF eArcadeCAbinet != ARCADE_CABINET_INVALID
		INT iPlayerSlot =  GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
		GET_ARCADE_CABINET_ANIMATION_DETAIL(eArcadeCAbinet, eClip, iPlayerSlot, sAnimClip)
		INT iLocalSceneID = GET_ARCADE_CABINET_LOCAL_SCENE_FROM_NETWORK_ID()
		IF iLocalSceneID != -1
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sAnimClipName)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					PRINTLN(" [AM_ARC_CAB] -  IS_ARCADE_CABINET_SYNC_SCENE_PHASE current phase  ", GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID))
					IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fPhase
						PRINTLN("[AM_ARC_CAB] -  IS_ARCADE_CABINET_SYNC_SCENE_PHASE  true")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE 
ENDFUNC

// PURPOSE: 
//	Returns TRUE if the sync scene specified event fired
FUNC BOOL IS_ARCADE_CABINET_SYNC_SCENE_EVENT_FIRED_ON_PLAYER_ANIM(ARCADE_CABINET_ANIM_CLIPS eClip, STRING fTag)
	ARCADE_CABINETS eArcadeCAbinet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	ARCADE_CABINET_ANIM_DETAIL sAnimClip
	
	IF IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		RETURN FALSE
	ENDIF
	
	IF !HAS_ANIM_DICT_LOADED(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		RETURN FALSE
	ENDIF
	
	IF eArcadeCAbinet != ARCADE_CABINET_INVALID
		INT iPlayerSlot =  GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
		GET_ARCADE_CABINET_ANIMATION_DETAIL(eArcadeCAbinet, eClip, iPlayerSlot, sAnimClip)
		INT iLocalSceneID = GET_ARCADE_CABINET_LOCAL_SCENE_FROM_NETWORK_ID()
		IF iLocalSceneID != -1
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sAnimClipName)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					IF  HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY(fTag))
						PRINTLN("[AM_ARC_CAB] -  IS_ARCADE_CABINET_SYNC_SCENE_EVENT_FIRED  true")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE 
ENDFUNC

// PURPOSE: 
//	Returns TRUE if the sync scene specified event fired
FUNC BOOL IS_ARCADE_CABINET_SYNC_SCENE_EVENT_FIRED_ON_PROP_ANIM(ARCADE_CABINET_DETAILS &sACMData, ARCADE_CABINET_ANIM_CLIPS eClip, INT iDisplaySlot, INT iSubPropIndex, STRING fTag)
	ARCADE_CABINETS eArcadeCAbinet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	ARCADE_CABINET_ANIM_DETAIL sAnimClip
	
	IF IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		RETURN FALSE
	ENDIF
	
	IF !HAS_ANIM_DICT_LOADED(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
		RETURN FALSE
	ENDIF
	
	IF eArcadeCAbinet != ARCADE_CABINET_INVALID
		INT iPlayerSlot =  GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
		GET_ARCADE_CABINET_ANIMATION_DETAIL(eArcadeCAbinet, eClip, iPlayerSlot, sAnimClip)
		INT iLocalSceneID = GET_ARCADE_CABINET_LOCAL_SCENE_FROM_NETWORK_ID()
		IF iLocalSceneID != -1
			IF IS_ENTITY_PLAYING_ANIM(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sPropAnimClip[iSubPropIndex])
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
					IF  HAS_ANIM_EVENT_FIRED(sACMData.arcadeCabinetSubObj[iDisplaySlot][iSubPropIndex], GET_HASH_KEY(fTag))
						PRINTLN("[AM_ARC_CAB] -  IS_ARCADE_CABINET_SYNC_SCENE_EVENT_FIRED  true")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    Check if arcade cabinet animation is ended
FUNC BOOL HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED(ARCADE_CABINET_ANIM_CLIPS eClip, BOOL bStopAnim = FALSE, BOOL bCheckFinalPhase = FALSE, BOOL bCheckPropSyncScene = FALSE)

	ARCADE_CABINETS eArcadeCAbinet = GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID())
	ARCADE_CABINET_ANIM_DETAIL sAnimClip

	IF eArcadeCAbinet != ARCADE_CABINET_INVALID
		INT iPlayerSlot =  GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
		GET_ARCADE_CABINET_ANIMATION_DETAIL(eArcadeCAbinet, eClip, iPlayerSlot, sAnimClip)
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimClip.sAnimClipName)
			AND NOT IS_STRING_NULL_OR_EMPTY(g_ArcadeCabinetManagerData.strArcCabinetAnimDic)
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), g_ArcadeCabinetManagerData.strArcCabinetAnimDic, sAnimClip.sAnimClipName)
				
					FLOAT fExitAnimPhase = GET_ARCADE_CABINET_ANIMATION_EXIT_PHASE(eArcadeCAbinet, eClip)
					INT iLocalSceneID = GET_ARCADE_CABINET_LOCAL_SCENE_FROM_NETWORK_ID()
					
					IF bCheckPropSyncScene
						iLocalSceneID = GET_ARCADE_CABINET_LOCAL_PROP_SCENE_FROM_NETWORK_ID()
					ENDIF
					
					#IF IS_DEBUG_BUILD
					IF g_DebugArcadeCabinetManagerData.bAnimFinishDebugInfo
						DRAW_DEBUG_TEXT_2D(sAnimClip.sAnimClipName, <<0.5, 0.45, 0.45>>)	
						DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID)), <<0.5, 0.5, 0.5>>)	
					ENDIF
					#ENDIF
					
					IF bCheckFinalPhase 
						fExitAnimPhase = 0.98
					ENDIF
					
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase
						OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLEND_OUT"))
						OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("CAN_EARLY_OUT_FOR_MOVEMENT"))
							IF bStopAnim
								IF bCheckPropSyncScene
									STOP_CURRENT_ARCADE_CABINET_PROP_SYNCHED_SCENE()
								ELSE
									STOP_CURRENT_ARCADE_CABINET_SYNCHED_SCENE()
								ENDIF	
							ENDIF	
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF	
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF	
	ELSE
		PRINTLN("[AM_ARC_CAB] - HAS_ARCADE_CABINET_ANIMATION_CLIP_FINISHED invalid arcade cabinet return true")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANY_ONE_IN_MY_ARCADE_PROPERTY()
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX,i)
		IF IS_NET_PLAYER_OK(piPlayer)
			IF IS_PLAYER_IN_ARCADE_PROPERTY(piPlayer)
				IF GlobalPlayerBD[NATIVE_TO_INT(piPlayer)].SimpleInteriorBD.propertyOwner = PLAYER_ID()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_ARCADE_CABINET_DELIVERY()
	IF !HAS_PLAYER_COMPLETED_FULL_ARCADE_SETUP(PLAYER_ID())
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_ArcadeCabinetManagerData.sDeliveryTimer)
		IF HAS_NET_TIMER_EXPIRED(g_ArcadeCabinetManagerData.sDeliveryTimer, 120000)
			IF !IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			AND !IS_ANY_ONE_IN_MY_ARCADE_PROPERTY()
			AND IS_SCREEN_FADED_IN()
				IF NOT IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELAY_DELIVERY_HELP)
					INT i, iNumDeliveried
					TEXT_LABEL_15 tlArcadeName
					ARCADE_CABINETS eArcadeCabinet
					// count number of cabinets delivered
					FOR i = 0 TO ENUM_TO_INT(ARCADE_CABINET_COUNT) - 1
						eArcadeCabinet = INT_TO_ENUM(ARCADE_CABINETS, i)
						IF HAS_PLAYER_PURCHASED_ARCADE_MACHINE(PLAYER_ID(), eArcadeCabinet, ACP_ARCADE)
							IF NOT HAS_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(PLAYER_ID(), eArcadeCabinet, ACP_ARCADE)
								iNumDeliveried++
								PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_DELIVERY iNumDeliveried: ", iNumDeliveried)
							ENDIF	
						ENDIF
					ENDFOR 
					
					IF iNumDeliveried > 1
						FOR i = 0 TO ENUM_TO_INT(ARCADE_CABINET_COUNT) - 1
							eArcadeCabinet = INT_TO_ENUM(ARCADE_CABINETS, i)
							IF HAS_PLAYER_PURCHASED_ARCADE_MACHINE(PLAYER_ID(), eArcadeCabinet, ACP_ARCADE)
								IF NOT HAS_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(PLAYER_ID(), eArcadeCabinet, ACP_ARCADE)
									SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(eArcadeCabinet, TRUE)																	
								ENDIF	
							ENDIF
						ENDFOR
						SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_ARCADE_WENDY, "WENDY_ARC_DELM", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
					ELSE
						FOR i = 0 TO ENUM_TO_INT(ARCADE_CABINET_COUNT) - 1
							eArcadeCabinet = INT_TO_ENUM(ARCADE_CABINETS, i)
							IF HAS_PLAYER_PURCHASED_ARCADE_MACHINE(PLAYER_ID(), eArcadeCabinet, ACP_ARCADE)
								IF NOT HAS_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(PLAYER_ID(), eArcadeCabinet, ACP_ARCADE)
									SET_PLAYER_PURCHASED_ARCADE_MACHINE_DELIVERED(eArcadeCabinet, TRUE)
									tlArcadeName = GET_ARCADE_MACHINE_LABEL(eArcadeCabinet)
									SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(CHAR_ARCADE_WENDY, "WENDY_ARC_DELS",TXTMSG_UNLOCKED, GET_FILENAME_FOR_AUDIO_CONVERSATION(tlArcadeName), -99, ""
																								, STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)														
								ENDIF	
							ENDIF
						ENDFOR
					ENDIF
					SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELIVERY_HELP)
					SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_MANAGMENT_HELP)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_DELIVERY ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELIVERY_HELP true")
					RESET_NET_TIMER(g_ArcadeCabinetManagerData.sDeliveryTimer)
				ELSE
					IF NOT HAS_NET_TIMER_STARTED(g_ArcadeCabinetManagerData.sDeliveryHelpDelayTimer)
						START_NET_TIMER(g_ArcadeCabinetManagerData.sDeliveryHelpDelayTimer)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(g_ArcadeCabinetManagerData.sDeliveryHelpDelayTimer, 10000)
							CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELAY_DELIVERY_HELP)
							PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_DELIVERY ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELAY_DELIVERY_HELP false")
							RESET_NET_TIMER(g_ArcadeCabinetManagerData.sDeliveryHelpDelayTimer)
						ENDIF
					ENDIF
				ENDIF	
			ELSE
				IF NOT IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELAY_DELIVERY_HELP)
					SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELAY_DELIVERY_HELP)
					RESET_NET_TIMER(g_ArcadeCabinetManagerData.sDeliveryHelpDelayTimer)
					PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_DELIVERY ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELAY_DELIVERY_HELP true")
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELIVERY_HELP)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND IS_SCREEN_FADED_IN()
			PRINT_HELP("ARC_DELIVERY_CO")
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELIVERY_HELP)
			PRINTLN("[AM_ARC_CAB] - MAINTAIN_ARCADE_CABINET_DELIVERY ARC_CAB_MANAGER_GLOBAL_BIT_SET_DELIVERY_HELP false")
		ENDIF
	ENDIF	
ENDPROC

FUNC ARCADE_CABINET_SOUND_AREA GET_ARCADE_CABINET_SOUND_AREA_IN_SLOT(ARCADE_CAB_MANAGER_SLOT eSlot)
	SWITCH eSlot
		CASE ACM_SLOT_2
		CASE ACM_SLOT_3
		CASE ACM_SLOT_4
		CASE ACM_SLOT_5
		CASE ACM_SLOT_6
		CASE ACM_SLOT_7
			RETURN ARCADE_CABINET_SOUND_ENTRANCE_AREA_2
		BREAK
		CASE ACM_SLOT_1
		CASE ACM_SLOT_31
		CASE ACM_SLOT_32
		CASE ACM_SLOT_33
		CASE ACM_SLOT_34
		CASE ACM_SLOT_35
			RETURN ARCADE_CABINET_SOUND_ENTRANCE_AREA_1
		BREAK
		CASE ACM_SLOT_14
		CASE ACM_SLOT_15
			RETURN ARCADE_CABINET_SOUND_UPPER_MACHINE_1
		BREAK	
		CASE ACM_SLOT_16
		CASE ACM_SLOT_17
			RETURN ARCADE_CABINET_SOUND_UPPER_MACHINE_2
		BREAK
		CASE ACM_SLOT_24
		CASE ACM_SLOT_25
		CASE ACM_SLOT_26
		CASE ACM_SLOT_27
		CASE ACM_SLOT_28	
		CASE ACM_SLOT_29
		CASE ACM_SLOT_30
			RETURN ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR
		BREAK	
		CASE ACM_SLOT_18
		CASE ACM_SLOT_19
		CASE ACM_SLOT_20
		CASE ACM_SLOT_21
		CASE ACM_SLOT_22
		CASE ACM_SLOT_23
			RETURN ARCADE_CABINET_SOUND_CENTRAL_PILLAR
		BREAK	
		CASE ACM_SLOT_8
		CASE ACM_SLOT_9
		CASE ACM_SLOT_10
		CASE ACM_SLOT_11
		CASE ACM_SLOT_12
		CASE ACM_SLOT_13
			RETURN ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE
		BREAK	
	ENDSWITCH
	RETURN ARCADE_CABINET_SOUND_ENTRANCE_AREA_1
ENDFUNC

FUNC BOOL IS_ARCADE_CABINET_SLOT_IN_THIS_SOUND_AREA(ARCADE_CABINET_SOUND_AREA arcadeCabinetSoundArea, ARCADE_CAB_MANAGER_SLOT eSlot)
	SWITCH arcadeCabinetSoundArea
		CASE ARCADE_CABINET_SOUND_ENTRANCE_AREA_1
			SWITCH eSlot
				CASE ACM_SLOT_1
				CASE ACM_SLOT_31
				CASE ACM_SLOT_32
				CASE ACM_SLOT_33
				CASE ACM_SLOT_34
				CASE ACM_SLOT_35
					RETURN TRUE
				BREAK
			ENDSWITCH 
		BREAK
		CASE ARCADE_CABINET_SOUND_ENTRANCE_AREA_2
			SWITCH eSlot
				CASE ACM_SLOT_2
				CASE ACM_SLOT_3
				CASE ACM_SLOT_4
				CASE ACM_SLOT_5
				CASE ACM_SLOT_6
				CASE ACM_SLOT_7
					RETURN TRUE
				BREAK
			ENDSWITCH 
		BREAK
		CASE ARCADE_CABINET_SOUND_UPPER_MACHINE_1
			SWITCH eSlot
				CASE ACM_SLOT_14
				CASE ACM_SLOT_15
					RETURN TRUE
				BREAK
			ENDSWITCH	
		BREAK	
		CASE ARCADE_CABINET_SOUND_UPPER_MACHINE_2
			SWITCH eSlot
				CASE ACM_SLOT_16
				CASE ACM_SLOT_17
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE ARCADE_CABINET_SOUND_WALL_OPPOSITE_BAR	
			SWITCH eSlot
				CASE ACM_SLOT_24
				CASE ACM_SLOT_25
				CASE ACM_SLOT_26
				CASE ACM_SLOT_27
				CASE ACM_SLOT_28	
				CASE ACM_SLOT_29
				CASE ACM_SLOT_30
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK	
		CASE ARCADE_CABINET_SOUND_CENTRAL_PILLAR
			SWITCH eSlot
				CASE ACM_SLOT_18
				CASE ACM_SLOT_19
				CASE ACM_SLOT_20
				CASE ACM_SLOT_21
				CASE ACM_SLOT_22
				CASE ACM_SLOT_23
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK	
		CASE ARCADE_CABINET_SOUND_LOWER_FLOOR_BARRIER_EDGE
			SWITCH eSlot
				CASE ACM_SLOT_8
				CASE ACM_SLOT_9
				CASE ACM_SLOT_10
				CASE ACM_SLOT_11
				CASE ACM_SLOT_12
				CASE ACM_SLOT_13
					RETURN TRUE
				BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_THIS_ARCADE_CABINET_HAVE_CUSTOM_CAMERA(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
//		CASE ARCADE_CABINET_CH_CLAW_CRANE
//		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
		CASE ARCADE_CABINET_CH_LOVE_METER
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC TABLE_GAMES_CAMERA_TYPE GET_ARCADE_CABINET_CAMERA_TYPE(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_LOVE_METER
			INT iPlayerIndex 
			iPlayerIndex = GET_ARCADE_CABINET_LOCATE_PLAYER_SLOT_PLAYER_IS_IN(PLAYER_ID())
			IF iPlayerIndex = ENUM_TO_INT(ARCADE_CAB_PLAYER_1)		
				RETURN TABLE_GAMES_CAMERA_TYPE_LOVE_PROFESSOR_P1
			ELSE
				RETURN TABLE_GAMES_CAMERA_TYPE_LOVE_PROFESSOR_P2
			ENDIF
		BREAK
	ENDSWITCH
	RETURN TABLE_GAMES_CAMERA_TYPE_SLOT_MACHINE
ENDFUNC	

/// PURPOSE:
///    Should stop gameplay hint camera before enter animation starts
FUNC BOOL SHOULD_STOP_ARCADE_CABINET_HINT_CAMERA(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC	

FUNC BOOL DOES_THIS_ARCADE_CABINET_HAVE_HINT_CAMERA(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_CLAW_CRANE
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
		CASE ARCADE_CABINET_CH_LOVE_METER
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
		#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC	

/// PURPOSE:
///    Arcade cabinets cost 1$ for remote players. These cabinets are free for remote players.
FUNC BOOL IS_ARCADE_CABINET_FREE_TO_USE(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			RETURN FALSE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_HIDE_HUD_AND_RADAR_WHILE_USING_ARCADE_CABINET(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
			RETURN FALSE
		BREAK	
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_ALLOW_CAMERA_CONTROLS_ON_ARCADE_CABINET_TRIGGER(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_FORCE_FP_CAMERA_FOR_ARCADE_CABINET_ENTER_ANIM(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_CLAW_CRANE
			//RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Don't play prop anim just attach them to ped
/// RETURNS:
///    
FUNC BOOL SHOULD_ONLY_ATTACH_SUB_PROP_IN_ANIMS(ARCADE_CABINETS arcadeCabinet)
	UNUSED_PARAMETER(arcadeCabinet)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_THIS_ARCADE_MINI_GAME_ASSETS(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
			RETURN TRUE
		BREAK	
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ARCADE_CABINET_HAVE_VALID_SUB_PROP_ANIM_CLIP(ARCADE_CABINETS arcadeCabinet, ARCADE_CABINET_ANIM_DETAIL eAnimDetails)
	IF arcadeCabinet != ARCADE_CABINET_INVALID
		INT iSubPropIndex
		ACM_SUB_PROP_DETAILS eArcadeCabSubPropDetail
		GET_ARCADE_CABINET_SUB_PROP_DETAIL(arcadeCabinet, eArcadeCabSubPropDetail)
		FOR iSubPropIndex = 0 TO eArcadeCabSubPropDetail.iNumProp - 1
			IF !IS_STRING_NULL_OR_EMPTY(eAnimDetails.sPropAnimClip[iSubPropIndex])
				RETURN TRUE
			ENDIF
		ENDFOR	
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY(BOOL bSet)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, bSet)
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[AM_ARC_CAB] - SET_PLAYER_CONFIG_FLAG_KINEMATIC_MODE_WHEN_STATIONARY bSet: ", GET_STRING_FROM_BOOL(bSet))
		#ENDIF
	ENDIF	
ENDPROC

FUNC BOOL SHOULD_STOP_ARCADE_CABINET_SYNCED_SCENE_BEFORE_STARTING_NEW_CLIP(ARCADE_CABINET_ANIM_CLIPS eClip)
	SWITCH eClip
		CASE ARCADE_CABINET_ANIM_CLIP_CLAW_CRANE_MISS
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Hide arcade cabinets next to pillar in arcade property - used for mocap
PROC HIDE_ARCADE_CABINETS_NEXT_TO_PILLAR(BOOL bHide)
	IF bHide
		IF !IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_NEXT_TO_PILLAR)
			SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_NEXT_TO_PILLAR)
			PRINTLN("[AM_ARC_CAB] - HIDE_ARCADE_CABINETS_NEXT_TO_PILLAR TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_NEXT_TO_PILLAR)
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_NEXT_TO_PILLAR)
			PRINTLN("[AM_ARC_CAB] - HIDE_ARCADE_CABINETS_NEXT_TO_PILLAR FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Hide arcade cabinets in slot 31 and 32 for final mocap
PROC HIDE_ARCADE_CABINETS_FOR_FINAL_MOCAP(BOOL bHide)
	IF bHide
		IF !IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_FOR_FINAL_MOCAP)
			SET_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_FOR_FINAL_MOCAP)
			PRINTLN("[AM_ARC_CAB] - HIDE_ARCADE_CABINETS_FOR_FINAL_MOCAP TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_FOR_FINAL_MOCAP)
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, ARC_CAB_MANAGER_GLOBAL_BIT_SET_HIDE_CABINETS_FOR_FINAL_MOCAP)
			PRINTLN("[AM_ARC_CAB] - HIDE_ARCADE_CABINETS_FOR_FINAL_MOCAP FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Get anim phase which sub prop should attach/detach
FUNC FLOAT GET_ARCADE_CABINET_SUB_PROP_ATTACHMENT_PHASE(ARCADE_CABINETS arcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, ARCADE_CAB_NUM_PLAYERS playerIndex, BOOL bAttach)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			SWITCH eClip
				CASE ARCADE_CABINET_ANIM_CLIP_ENTRY
					SWITCH playerIndex
						CASE ARCADE_CAB_PLAYER_1
							IF bAttach
								IF !IS_PLAYER_FEMALE()
									RETURN 0.356
								ELSE
									RETURN 0.422
								ENDIF
							ENDIF
						BREAK
						CASE ARCADE_CAB_PLAYER_2
							IF bAttach
								IF !IS_PLAYER_FEMALE()
									RETURN 0.190
								ELSE
									RETURN 0.439
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_LAST_GUNSLINGERS_EXIT_FROM_AIM
					SWITCH playerIndex
						CASE ARCADE_CAB_PLAYER_1
							IF !bAttach
								IF !IS_PLAYER_FEMALE()
									RETURN 0.303
								ELSE
									RETURN 0.365 
								ENDIF
							ENDIF
						BREAK
						CASE ARCADE_CAB_PLAYER_2
							IF !bAttach
								IF !IS_PLAYER_FEMALE()
									RETURN 0.367
								ELSE
									RETURN 0.275
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE ARCADE_CABINET_ANIM_CLIP_EXIT
					SWITCH playerIndex
						CASE ARCADE_CAB_PLAYER_1
							IF !bAttach
								IF !IS_PLAYER_FEMALE()
									RETURN 0.320
								ELSE
									RETURN 0.319 
								ENDIF
							ENDIF
						BREAK
						CASE ARCADE_CAB_PLAYER_2
							IF !bAttach
								IF !IS_PLAYER_FEMALE()
									RETURN 0.292
								ELSE
									RETURN 0.352
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH	
	RETURN 0.0
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_CABINET_ENTITY_SPEECH_LISTEN_DISTANCE(PLAYER_INDEX playerID, ENTITY_INDEX cabinet)
	IF NOT DOES_ENTITY_EXIST(cabinet)
	OR NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(playerID))
	OR IS_ENTITY_DEAD(GET_PLAYER_PED(playerID))
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(GET_PLAYER_PED(playerID))
	FLOAT fPlayerZ = vPlayerCoords.z
	IF (fPlayerZ <= ARCADE_OFFICE_Z_HEIGHT)
		RETURN (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_ENTITY_COORDS(cabinet)) <= ARCADE_AMBIENT_SPEECH_LISTEN_DISTANCE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_WITHIN_CABINET_COORDS_SPEECH_LISTEN_DISTANCE(PLAYER_INDEX playerID, VECTOR vCabinetCoords)
	IF NOT DOES_ENTITY_EXIST(GET_PLAYER_PED(playerID))
	OR IS_ENTITY_DEAD(GET_PLAYER_PED(playerID))
		RETURN FALSE
	ENDIF
	
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(playerID)
	FLOAT fPlayerZ = vPlayerCoords.z
	IF (fPlayerZ <= ARCADE_OFFICE_Z_HEIGHT)
		RETURN (VDIST(vPlayerCoords, vCabinetCoords) <= ARCADE_AMBIENT_SPEECH_LISTEN_DISTANCE)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CONVO_SPEECH_HAVE_SUBTITLES(ARCADE_CABINETS eArcadeCabinet)
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
			RETURN (IS_PLAYER_USING_ARCADE_CABINET(PLAYER_ID()) AND GET_ARCADE_CABINET_LOCATE_PLAYER_IS_IN(PLAYER_ID()) = ARCADE_CABINET_CH_FORTUNE_TELLER)
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING GET_CONVO_SPEECH_TEXT_BLOCK(ARCADE_CABINETS eArcadeCabinet)
	STRING sConvoTextBlock = ""
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER	sConvoTextBlock = "HS3MNAU"	BREAK
	ENDSWITCH
	
	RETURN sConvoTextBlock
ENDFUNC

FUNC INT GET_CONVO_SPEECH_SPEAKER_ID(ARCADE_CABINETS eArcadeCabinet)
	INT iSpeakerID = -1
	SWITCH eArcadeCabinet
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER	iSpeakerID = 2	BREAK
	ENDSWITCH
	
	RETURN iSpeakerID
ENDFUNC

FUNC BOOL DOES_THIS_ARCADE_CABINET_HAS_GLITCH_MODE(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE	
		CASE ARCADE_CABINET_CH_PENETRATOR
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_ARCADE_CABINET_TERMINATE_BIT_NAME(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER				RETURN "ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_FORTUNE_TELLER"
		CASE ARCADE_CABINET_CH_CLAW_CRANE					RETURN "ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_CLAW_CRANE"
		CASE ARCADE_CABINET_CH_LOVE_METER					RETURN "ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_LOVE_METER"
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST				RETURN "ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_STRENGTH_TEST"
		#ENDIF
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC	

FUNC INT GET_ARCADE_CABINET_TERMINATE_BIT(ARCADE_CABINETS arcadeCabinet)
	SWITCH arcadeCabinet
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER				RETURN ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_FORTUNE_TELLER
		CASE ARCADE_CABINET_CH_CLAW_CRANE					RETURN ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_CLAW_CRANE
		CASE ARCADE_CABINET_CH_LOVE_METER					RETURN ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_LOVE_METER
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST				RETURN ARC_CAB_MANAGER_GLOBAL_BIT_SET_TERMINATE_CH_STRENGTH_TEST
		#ENDIF
	ENDSWITCH
	
	IF arcadeCabinet != ARCADE_CABINET_INVALID	
		PRINTLN("[AM_MP_ARC_CAB_MANAGER] - GET_ARCADE_CABINET_TERMINATE_BIT arcadeCabinet: ", arcadeCabinet)
		SCRIPT_ASSERT("[AM_MP_ARC_CAB_MANAGER] - GET_ARCADE_CABINET_TERMINATE_BIT ARCADE_CABINETS is invalid ")
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(ARCADE_CABINETS arcadeCabinet)
	RETURN IS_BIT_SET(g_ArcadeCabinetManagerData.iBS, GET_ARCADE_CABINET_TERMINATE_BIT(arcadeCabinet))
ENDFUNC

PROC SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG(ARCADE_CABINETS arcadeCabinet, BOOL bTerminate)
	IF bTerminate
		IF !SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(arcadeCabinet)
			SET_BIT(g_ArcadeCabinetManagerData.iBS, GET_ARCADE_CABINET_TERMINATE_BIT(arcadeCabinet))
			PRINTLN("[AM_MP_ARC_CAB_MANAGER] - SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG TRUE arcadeCabinet: ", arcadeCabinet)
		ENDIF
	ELSE
		IF SHOULD_TERMINATE_PHYSICAL_ARDCADE_GAME(arcadeCabinet)
			CLEAR_BIT(g_ArcadeCabinetManagerData.iBS, GET_ARCADE_CABINET_TERMINATE_BIT(arcadeCabinet))
			PRINTLN("[AM_MP_ARC_CAB_MANAGER] - SET_TERMINATE_PHYSICAL_ARCADE_GAME_FLAG FALSE arcadeCabinet: ", arcadeCabinet)
		ENDIF
	ENDIF
ENDPROC

#ENDIF

