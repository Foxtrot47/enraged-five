//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arcade_cabinet_private.sch																//
// Description: All the private arcade cabinet stuff, none of these should be used by other scripts than	//
//				arcade cabinet scripts.																		//
// Written by:  Online Technical Design: Ata Tabrizi														//
// Date:  		28/08/2019																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_CASINO_HEIST

USING "net_arcade_cabinet_generic.sch"
USING "net_arcade_cabinet_claw_crane.sch"
USING "net_arcade_cabinet_strength_test.sch"
USING "net_arcade_cabinet_fortune_teller.sch"
USING "net_arcade_cabinet_love_meter.sch"


PROC BUILD_ARCADE_CABINET_LOOK_UP_TABLE(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_INTERFACE &table, ARCADE_CABINET_INTERFACE_PROCEDURES eProc)
	SWITCH eArcadeCabinet	
		CASE ARCADE_CABINET_CH_GG_SPACE_MONKEY
		CASE ARCADE_CABINET_CH_LAST_GUNSLINGERS
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_BIKE
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_CAR
		CASE ARCADE_CABINET_CH_NIGHT_DRIVE_TRUCK
		CASE ARCADE_CABINET_CH_WIZARDS_SLEVE
		CASE ARCADE_CABINET_CH_DEFENDER_OF_THE_FAITH
		CASE ARCADE_CABINET_CH_MONKEYS_PARADISE
		CASE ARCADE_CABINET_CH_PENETRATOR
		CASE ARCADE_CABINET_CA_STREET_CRIMES
		CASE ARCADE_CABINET_CA_INVADE_AND_PERSUADE
		CASE ARCADE_CABINET_CH_HACKING_MINI_G
		CASE ARCADE_CABINET_CH_DRILLING_LASER_MINI_G
		CASE ARCADE_CABINET_CH_DRONE_MINI_G
		CASE ARCADE_CABINET_SUM_QUB3D
		#IF FEATURE_TUNER
		CASE ARCADE_CABINET_SE_CAMHEDZ
		#ENDIF
			BUILD_GENERIC_ARCADE_CABINET_LOOK_UP_TABLE(table, eProc)
		BREAK	
		CASE ARCADE_CABINET_CH_CLAW_CRANE
			BUILD_CLAW_CRANE_ARCADE_CABINET_LOOK_UP_TABLE(table, eProc)
		BREAK
		CASE ARCADE_CABINET_CH_FORTUNE_TELLER
			BUILD_FORTUNE_TELLER_ARCADE_CABINET_LOOK_UP_TABLE(table, eProc)
		BREAK
		CASE ARCADE_CABINET_CH_LOVE_METER
			BUILD_LOVE_METER_ARCADE_CABINET_LOOK_UP_TABLE(table, eProc)
		BREAK
		#IF FEATURE_SUMMER_2020
		CASE ARCADE_CABINET_SUM_STRENGTH_TEST
			BUILD_STRENGTH_TEST_ARCADE_CABINET_LOOK_UP_TABLE(table, eProc)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡  FUNCTION REDIRACTION   ╞════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL DOES_ARCADE_CABINET_USE_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &sChildScriptName, BOOL bCheckGameScript)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_DOES_ARCADE_CABINET_HAVE_CHILD_SCRIPT)
	RETURN CALL lookUpTable.returnArcadeCabinetHaveChildScript(eArcadeCabinet, sChildScriptName, bCheckGameScript)
ENDFUNC

PROC GET_ARCADE_CABINET_DETAILS(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_DETAILS)
	CALL lookUpTable.getArcadeCabinetDetails(eArcadeCabinet, details)
ENDPROC

/// PURPOSE:
///    Get offset coord for each trigger locate 
PROC GET_ARCADE_CABINET_TRIGGER_LOCATE_DATA(ARCADE_CABINETS eArcadeCabinet, VECTOR &vOffset[])
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_TRIGGER_DATA)
	CALL lookUpTable.getArcadeCabinetTriggerData(eArcadeCabinet, vOffset)
ENDPROC

/// PURPOSE:
///    Get help text to start the arcade cabinet games
PROC GET_ARCADE_CABINET_TRIGGER_HELP_TEXT(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_TRIGGER_HELP_TEXT)
	CALL lookUpTable.returnArcadeCabinetTriggerHelp(eArcadeCabinet, details)
ENDPROC

/// PURPOSE:
///    Gets the animation dic and clips names 
/// PARAMS: 
///    details - store anim dic name intor struct
///    eClip - pass in animation name (enum)
///    iAnimVariation - animation variation - some animations have more than 1 variation which are stored in diffrenet dictionaries 
/// RETURNS: 
///    name of anim clips 
PROC GET_ARCADE_CABINET_ANIMATION_DETAIL(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip,  INT iAnimVariation, ARCADE_CABINET_ANIM_DETAIL &eAnimDetails)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_ANIMATION_DETAIL)
	CALL lookUpTable.returnArcadeCabinetAnimationDetail(eArcadeCabinet, eClip, iAnimVariation, eAnimDetails)
ENDPROC

/// PURPOSE:
///    Returns true if child script should take control of animation
FUNC BOOL SHOULD_ALL_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT(ARCADE_CABINETS eArcadeCabinet)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_SHOULD_ARCADE_CABINET_ANIMATION_HANDLE_BY_CHILD_SCRIPT)
	RETURN CALL lookUpTable.returnArcadeCabinetHandleAllAnimsByChildScript(eArcadeCabinet)
ENDFUNC

/// PURPOSE:
///    Gets the number of players can arcade cabinet have ARC_CAB_VARIATION_1 - ARC_CAB_VARIATION_TOTAL
FUNC INT GET_NUM_PLAYERS_ARCADE_CABINET_CAN_HAVE(ARCADE_CABINETS eArcadeCabinet)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_NUM_PLAYES_ARCADE_CABINET_HAVE)
	RETURN CALL lookUpTable.getNumPlayersArcadeCabinetHave(eArcadeCabinet)
ENDFUNC

/// PURPOSE:
///    Handles starting game scripts for arcade cabinets which require external game script e.g. street crime
FUNC BOOL MAINTAIN_LAUNCH_GAME_SCRIPT(ARCADE_CABINETS eArcadeCabinet, ARCADE_CAB_GROUP eArcadeCabinetGroup, ARCADE_CABINET_PROPERTY eProperty)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_MAINTAIN_LAUNCH_GAME_SCRIPT)
	RETURN CALL lookUpTable.maintainLunchGameScript(eArcadeCabinet, eArcadeCabinetGroup, eProperty)
ENDFUNC

/// PURPOSE:
///    maintain arcade cabinet animation
PROC MAINTAIN_ARCADE_CABINET_IN_USE_ANIMATIONS(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_DETAILS &details)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_MAINTAIN_IN_USE_ANIMATIONS)
	CALL lookUpTable.maintainInUseAnimations(eArcadeCabinet, details)
ENDPROC

/// PURPOSE:
///    maintain arcade cabinet sound set and sound name
PROC GET_ARCADE_CABINET_SOUND_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &tlSoundSet, TEXT_LABEL_63 &tlSoundName, BOOL bInUse)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_SOUND_DETAIL)
	CALL lookUpTable.getArcadeCabinetSoundDetail(eArcadeCabinet, tlSoundSet, tlSoundName, bInUse)
ENDPROC

/// PURPOSE:
///    Get arcade cabinet sub prop details 
PROC GET_ARCADE_CABINET_SUB_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, ACM_SUB_PROP_DETAILS &eArcadeCabSubPropDetail)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_SUB_PROP_DETAIL)
	CALL lookUpTable.getArcadeCabinetSubPropDetail(eArcadeCabinet, eArcadeCabSubPropDetail)
ENDPROC

/// PURPOSE:
///    Gets offset positon of animation to arcade cabinet prop
/// PARAMS:
///    iPlayerSlot - player slot which player is using some arcade cabinets have more than 1 player 
FUNC VECTOR GET_ARCADE_CABINET_ANIMATION_OFFSET(ARCADE_CABINETS eArcadeCabinet, ARCADE_CABINET_ANIM_CLIPS eClip, INT iPlayerSlot, BOOL bPropOffset = FALSE)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_ANIMATION_OFFSET)
	RETURN CALL lookUpTable.getArcadeCabinetAnimationOffset(eArcadeCabinet, eClip, iPlayerSlot, bPropOffset)
ENDFUNC

/// PURPOSE:
///    Get startup menu's title and option names
PROC GET_ARCADE_CABINET_STARTUP_MENU_DETAIL(ARCADE_CABINETS eArcadeCabinet, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], STRING &strToggleableOptions[], ARCADE_CABINET_MENU_STRUCT &menuStruct)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_STARTUP_MENU_DETAIL)
	CALL lookUpTable.getArcadeCabinetStartupMenuDetail(eArcadeCabinet, txtTitle, strOptions, strToggleableOptions, menuStruct)
ENDPROC

/// PURPOSE:
///    Get startup menu's title and option names
PROC GET_ARCADE_CABINET_SCREEN_PROP_DETAIL(ARCADE_CABINETS eArcadeCabinet, INT iPlayerSlot, MODEL_NAMES &eArcadeCabinetScreenModel)
	ARCADE_CABINET_INTERFACE lookUpTable
	BUILD_ARCADE_CABINET_LOOK_UP_TABLE(eArcadeCabinet, lookUpTable, E_GET_ARCADE_CABINET_SCREEN_PROP_DETAIL)
	CALL lookUpTable.getArcadeCabinetScreenPropDetail(eArcadeCabinet, iPlayerSlot, eArcadeCabinetScreenModel)
ENDPROC

#ENDIF

