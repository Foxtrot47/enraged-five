//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_SPEECH.sch																						//
// Description: Header file containing speech functionality for peds.													//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_culling.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ SET SPEECH DATA ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the local ped speech data.
///    Populates the local speech array. Array size: MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_SPEECH_LOCAL_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iArrayID
		RESET_PED_SPEECH_DATA(LocalData.LocalSpeechData[iArrayID])
		SET_PED_SPEECH_DATA(ePedLocation, LocalData.LocalSpeechData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iLocalSpeechPedID)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the network ped speech data.
///    Populates the network speech array. Array size: MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_SPEECH_NETWORK_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	REPEAT MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS iArrayID
		RESET_PED_SPEECH_DATA(LocalData.NetworkSpeechData[iArrayID])
		SET_PED_SPEECH_DATA(ePedLocation, LocalData.NetworkSpeechData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iNetworkSpeechPedID, TRUE)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped speech local array.
///    Sets all elements to -1.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_SPEECH_LOCAL_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		LocalData.iLocalSpeechPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped speech network array.
///    Sets all elements to -1.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_SPEECH_NETWORK_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		LocalData.iNetworkSpeechPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the speech data for all peds.
///    Populates the speech arrays. 
///    Local array max: MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS
///    Network array size: MAX_NUM_NETWORK_PLAYER_SPEECH_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_ALL_PED_SPEECH_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	INITIALISE_PED_SPEECH_LOCAL_ARRAY_ID(LocalData)
	SET_PED_SPEECH_LOCAL_DATA(ePedLocation, ServerBD, LocalData)
	
	// Local players has to set their own network ped speech data.
	// Each player has to check if they have triggered local and network peds player speech.
	INITIALISE_PED_SPEECH_NETWORK_ARRAY_ID(LocalData)
	SET_PED_SPEECH_NETWORK_DATA(ePedLocation, ServerBD, LocalData)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Checks whether a ped is currently playing any speech.
/// PARAMS:
///    PedID - Speech ped to query.
/// RETURNS: TRUE if the ped is currently playing speech.  
FUNC BOOL IS_PED_PLAYING_SPEECH(PED_INDEX PedID)
	RETURN (IS_ENTITY_ALIVE(PedID) AND IS_ANY_SPEECH_PLAYING(PedID))
ENDFUNC

/// PURPOSE:
///    Checks whether a ped speech type is valid.
/// PARAMS:
///    eSpeech - Ped speech type to check.
/// RETURNS: TRUE if the ped speech type is valid.
FUNC BOOL IS_PED_SPEECH_VALID(PED_SPEECH eSpeech)
	SWITCH eSpeech
		CASE PED_SPH_INVALID
		CASE PED_SPH_PT_TOTAL
		CASE PED_SPH_CT_TOTAL
		CASE PED_SPH_TOTAL
		CASE PED_SPH_LISTEN_DISTANCE
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the ped speech type is player triggered.
///    Player speech is triggered by the player when certain conditions are met, e.g. bumping into a ped. 
/// PARAMS:
///    eSpeech - Ped speech type to check.
/// RETURNS: TRUE if the ped speech type is player triggered.
FUNC BOOL IS_PED_SPEECH_PLAYER_TRIGGERED(PED_SPEECH eSpeech)
	RETURN (eSpeech > PED_SPH_INVALID AND eSpeech < PED_SPH_PT_TOTAL)
ENDFUNC

/// PURPOSE:
///    Checks if the ped speech type is controller triggered.
///    Controller speech is maintained by the host and runs on a single timer.
/// PARAMS:
///    eSpeech - Ped speech type to check.
/// RETURNS: TRUE if the ped speech type is controller triggered.
FUNC BOOL IS_PED_SPEECH_CONTROLLER_TRIGGERED(PED_SPEECH eSpeech)
	RETURN (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
ENDFUNC

/// PURPOSE:
///    Gets the speech distance trigger based on the PED_SPEECH type passed in.
///    Returns cached data.
/// PARAMS:
///    SpeechData - Speech data to query.
///    ePedSpeech - Ped speech to query.
/// RETURNS: The FLOAT trigger distance for a ped speech type.
FUNC FLOAT GET_PED_SPEECH_TRIGGER_DISTANCES(SPEECH_DATA &SpeechData, PED_SPEECH ePedSpeech)

	IF IS_LOCAL_PLAYER_IN_AUTO_SHOP_THEY_OWN()
	AND NOT IS_BIT_SET(g_sAutoShopCallsData.iBS, BS_AUTO_SHOP_CALLS_BLOCKED)
	AND IS_BIT_SET(g_sAutoShopCallsData.iBS, BS_AUTO_SHOP_CALLS_SESSANTA_IS_AT_DESK)
	AND NOT IS_BIT_SET(g_sAutoShopCallsData.iBS, BS_AUTO_SHOP_CALLS_CALL_TRIGGERED)
	AND SpeechData.iPedID = 0
		SWITCH ePedSpeech		
			CASE PED_SPH_PT_GREETING		RETURN 25.0
			CASE PED_SPH_PT_BYE				RETURN 30.0
		ENDSWITCH
	ENDIF

	SWITCH ePedSpeech
		CASE PED_SPH_PT_GREETING		RETURN SpeechData.fGreetSpeechDistance
		CASE PED_SPH_PT_BYE				RETURN SpeechData.fByeSpeechDistance
		CASE PED_SPH_LISTEN_DISTANCE	RETURN SpeechData.fListenDistance
	ENDSWITCH
	RETURN -1.0
ENDFUNC

/// PURPOSE:
///    Checks if a player is within listen range of a ped.
/// PARAMS:
///    playerID - Player to query.
///    SpeechData - Ped speech data to query.
///    pedID - Ped to query.
/// RETURNS:
FUNC BOOL IS_PLAYER_WITHIN_PED_LISTEN_RANGE(PLAYER_INDEX playerID, SPEECH_DATA &SpeechData, PED_INDEX pedID)
	IF IS_ENTITY_ALIVE(pedID)
	AND IS_ENTITY_ALIVE(GET_PLAYER_PED(playerID))
		FLOAT fListenRange = GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_LISTEN_DISTANCE)
		IF (fListenRange != -1.0)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_PLAYER_PED(playerID)), GET_ENTITY_COORDS(pedID)) < fListenRange
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if any player is within the listen distance of a ped.
/// PARAMS:
///    SpeechData - Ped speech data to query.
///    pedID - Ped to query.
/// RETURNS: TRUE if any player is within listen distance of a ped.
FUNC BOOL IS_ANY_PLAYER_WITHIN_PED_LISTEN_RANGE(SPEECH_DATA &SpeechData, PED_INDEX pedID)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		    PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_PLAYER_WITHIN_PED_LISTEN_RANGE(playerID, SpeechData, pedID)
				RETURN TRUE
			ENDIF
	    ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether a ped can play player triggered speech (as opposed to controller triggered speech).
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iPed - Ped to query.
///    ePedActivity - Ped activity to query.
/// RETURNS: TRUE if the ped can play player triggered speech.
FUNC BOOL DOES_PED_PLAY_PLAYER_TRIGGERED_SPEECH(PED_LOCATIONS ePedLocation, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	REPEAT PED_SPH_TOTAL iSpeech
		PED_SPEECH eSpeech = GET_PED_SPEECH_TYPE(ePedLocation, iPed, ePedActivity, iSpeech)
		IF IS_PED_SPEECH_PLAYER_TRIGGERED(eSpeech)
			RETURN TRUE
		ELIF (eSpeech = PED_SPH_INVALID)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether a ped can play controller triggered speech (as opposed to player triggered speech).
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iPed - Ped to query.
///    ePedActivity - Ped activity to query.
/// RETURNS: TRUE if the ped can play controller triggered speech.
FUNC BOOL DOES_PED_PLAY_CONTROLLER_TRIGGERED_SPEECH(PED_LOCATIONS ePedLocation, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	REPEAT PED_SPH_TOTAL iSpeech
		PED_SPEECH eSpeech = GET_PED_SPEECH_TYPE(ePedLocation, iPed, ePedActivity, iSpeech)
		IF IS_PED_SPEECH_CONTROLLER_TRIGGERED(eSpeech)
			RETURN TRUE
		ELIF (eSpeech = PED_SPH_INVALID)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether any local peds can play controller triggered speech.
/// PARAMS:
///    LocalData - Local ped data to query.
/// RETURNS: TRUE if any of the peds play controller triggered speech.
FUNC BOOL DO_ANY_LOCAL_PEDS_PLAY_CONTROLLER_TRIGGERED_SPEECH(SCRIPT_PED_DATA &LocalData)
	RETURN (LocalData.iMaxLocalControllerSpeechPeds > 0)
ENDFUNC

/// PURPOSE:
///    Checks whether any network peds can play controller triggered speech.
/// PARAMS:
///    LocalData - Local ped data to query.
/// RETURNS: TRUE if any of the peds play controller triggered speech.
FUNC BOOL DO_ANY_NETWORK_PEDS_PLAY_CONTROLLER_TRIGGERED_SPEECH(SCRIPT_PED_DATA &LocalData)
	RETURN (LocalData.iMaxNetworkControllerSpeechPeds > 0)
ENDFUNC

/// PURPOSE:
///    Adds a ped to the server speech pool of available peds that can speak.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local ped data to query.
///    iPed - Ped to add.
///    Data - Ped data to query.
PROC ADD_PED_TO_LOCAL_SPEECH_POOL(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, INT iPed, PEDS_DATA Data)
	
	IF DOES_PED_PLAY_CONTROLLER_TRIGGERED_SPEECH(ePedLocation, iPed, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
	AND NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
		IF (LocalData.iMaxLocalControllerSpeechPeds < MAX_NUM_LOCAL_CONTROLLER_SPEECH_PEDS)
			LocalData.iLocalControllerSpeechPeds[LocalData.iMaxLocalControllerSpeechPeds] = iPed
			
			PRINTLN("[AM_MP_PEDS] ADD_PED_TO_LOCAL_SPEECH_POOL - iLocalControllerSpeechPeds[", LocalData.iMaxLocalControllerSpeechPeds, "]: ", iPed)
			LocalData.iMaxLocalControllerSpeechPeds++
		ELSE
			ASSERTLN("[AM_MP_PEDS] ADD_PED_TO_LOCAL_SPEECH_POOL - No. of controller speech peds exceeds MAX_NUM_LOCAL_CONTROLLER_SPEECH_PEDS.")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Adds a ped to the server speech pool of available peds that can speak.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local ped data to query.
///    iPed - Ped to add.
///    Data - Ped data to query.
PROC ADD_PED_TO_NETWORK_SPEECH_POOL(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, INT iPed, PEDS_DATA Data)
	
	IF DOES_PED_PLAY_CONTROLLER_TRIGGERED_SPEECH(ePedLocation, iPed, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
	AND NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
		IF (LocalData.iMaxNetworkControllerSpeechPeds < MAX_NUM_NETWORK_CONTROLLER_SPEECH_PEDS)
			LocalData.iNetworkControllerSpeechPeds[LocalData.iMaxNetworkControllerSpeechPeds] = iPed
			
			PRINTLN("[AM_MP_PEDS] ADD_PED_TO_NETWORK_SPEECH_POOL - iNetworkControllerSpeechPeds[", LocalData.iMaxNetworkControllerSpeechPeds, "]: ", iPed)
			LocalData.iMaxNetworkControllerSpeechPeds++
		ELSE
			ASSERTLN("[AM_MP_PEDS] ADD_PED_TO_NETWORK_SPEECH_POOL - No. of controller speech peds exceeds MAX_NUM_NETWORK_CONTROLLER_SPEECH_PEDS.")
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Populates the local and network controller speech arrays with peds who can play controller speech.
///    The host maintains the network peds controller speech. The local player maintains the local peds controller speech.
///    Max speaking peds is MAX_NUM_LOCAL_CONTROLLER_SPEECH_PEDS.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Ped server data to set.
///    LocalData - Local ped data to query.
PROC SET_PED_CONTROLLER_SPEECH_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	
	// Initialise local array to -1
	REPEAT MAX_NUM_LOCAL_CONTROLLER_SPEECH_PEDS iPed
		LocalData.iLocalControllerSpeechPeds[iPed] = -1
	ENDREPEAT
	
	// Populate local array with local speaking peds that use controller speech
	REPEAT ServerBD.iMaxLocalPeds iPed
		IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_USE_SPEECH)
			ADD_PED_TO_LOCAL_SPEECH_POOL(ePedLocation, LocalData, iPed, LocalData.PedData[iPed])
		ENDIF
	ENDREPEAT
	
	// Initialise network array to -1
	REPEAT MAX_NUM_NETWORK_CONTROLLER_SPEECH_PEDS iPed
		LocalData.iNetworkControllerSpeechPeds[iPed] = -1
	ENDREPEAT
	
	// Populate network array with networked speaking peds that use controller speech
	REPEAT ServerBD.iMaxNetworkPeds iPed
		IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_USE_SPEECH)
			ADD_PED_TO_NETWORK_SPEECH_POOL(ePedLocation, LocalData, GET_PED_NETWORK_ID(iPed), ServerBD.NetworkPedData[iPed].Data)
		ENDIF
	ENDREPEAT
	
	// Shuffle local controller array to randomise the order
	IF (LocalData.iMaxLocalControllerSpeechPeds > 1)
		PERFORM_FISHER_YATES_SHUFFLE(LocalData.iLocalControllerSpeechPeds, LocalData.iMaxLocalControllerSpeechPeds)
	ENDIF
	
	// Shuffle network controller array to randomise the order
	IF (LocalData.iMaxNetworkControllerSpeechPeds > 1)
		PERFORM_FISHER_YATES_SHUFFLE(LocalData.iNetworkControllerSpeechPeds, LocalData.iMaxNetworkControllerSpeechPeds)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the next local ped to play controller speech. If the list of valid controller speech peds has ran out,
///    the server BD list is reshuffled using the Fisher-Yates algorithm and the element count is reset to zero.
/// PARAMS:
///    LocalData - Local ped data to query.
/// RETURNS: The next valid ped to play controller speech.
FUNC INT GET_LOCAL_PED_CONTROLLER_SPEECH_PED(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	IF (LocalData.iMaxLocalControllerSpeechPeds = 1)
		iPed = LocalData.iLocalControllerSpeechPeds[0]
		LocalData.iLocalControllerSpeechPedID = 1
	ELSE
		// Get the next ped in the server BD ordered list
		iPed = LocalData.iLocalControllerSpeechPeds[LocalData.iLocalControllerSpeechPedID]
		LocalData.iLocalControllerSpeechPedID++
		
		// The list of valid speech peds has reached its end
		// Reshuffle the list and reset LocalData.iCurrentControllerSpeechPed to zero
		IF (iPed = -1 OR LocalData.iLocalControllerSpeechPedID > MAX_NUM_LOCAL_CONTROLLER_SPEECH_PEDS-1)
			PERFORM_FISHER_YATES_SHUFFLE(LocalData.iLocalControllerSpeechPeds, LocalData.iMaxLocalControllerSpeechPeds)
			iPed = LocalData.iLocalControllerSpeechPeds[0]
			LocalData.iLocalControllerSpeechPedID = 1
		ENDIF
	ENDIF
	
	RETURN iPed
ENDFUNC

/// PURPOSE:
///    Gets the next network ped to play controller speech. If the list of valid controller speech peds has ran out,
///    the server BD list is reshuffled using the Fisher-Yates algorithm and the element count is reset to zero.
/// PARAMS:
///    LocalData - Local ped data to query.
/// RETURNS: The next valid ped to play controller speech.
FUNC INT GET_NETWORK_PED_CONTROLLER_SPEECH_PED(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	IF (LocalData.iMaxNetworkControllerSpeechPeds = 1)
		iPed = LocalData.iNetworkControllerSpeechPeds[0]
		LocalData.iNetworkControllerSpeechPedID = 1
	ELSE
		// Get the next ped in the server BD ordered list
		iPed = LocalData.iNetworkControllerSpeechPeds[LocalData.iNetworkControllerSpeechPedID]
		LocalData.iNetworkControllerSpeechPedID++
		
		// The list of valid speech peds has reached its end
		// Reshuffle the list and reset LocalData.iCurrentControllerSpeechPed to zero
		IF (iPed = -1 OR LocalData.iNetworkControllerSpeechPedID > MAX_NUM_NETWORK_CONTROLLER_SPEECH_PEDS-1)
			PERFORM_FISHER_YATES_SHUFFLE(LocalData.iNetworkControllerSpeechPeds, LocalData.iMaxNetworkControllerSpeechPeds)
			iPed = LocalData.iNetworkControllerSpeechPeds[0]
			LocalData.iNetworkControllerSpeechPedID = 1
		ENDIF
	ENDIF
	
	RETURN iPed
ENDFUNC

/// PURPOSE:
///    Plays an ambient speech context on a ped.
/// PARAMS:
///    convoData - Convo data to query.
///    PedID - Ped index to query.
///    iPed - Ped ID to query.
PROC PLAY_PED_SPEECH_CONTEXT(PED_CONVO_DATA convoData, PED_INDEX PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	
	IF IS_STRING_NULL_OR_EMPTY(convoData.sRootName)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH - Bail Reason: sRootName is null")
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(convoData.sCharacterVoice)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH - Bail Reason: sCharacterVoice is null")
		EXIT
	ENDIF
	
	IF ARE_STRINGS_EQUAL(convoData.sCharacterVoice, "NULL")
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONTEXT - Playing Speech - iPed: ", iPed, " sRootName: ", convoData.sRootName)
		PLAY_PED_AMBIENT_SPEECH_NATIVE(PedID, convoData.sRootName, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_NORMAL), FALSE)
	ELSE
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONTEXT - Playing Speech With Voice - iPed: ", iPed, " sRootName: ", convoData.sRootName, " sCharacterVoice: ", convoData.sCharacterVoice)
		PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(PedID, convoData.sRootName, convoData.sCharacterVoice, AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE_NORMAL), FALSE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Add any child peds to the conversation.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
///    ParentData - Parent ped data.
///    PedSpeechConversation - Conversation struct to populate.
///    iPed - Parent ped ID.
///    ePedSpeech - Ped speech to play.
///    bNetworkedPed - Wheather the ped is networked or not.
PROC ADD_CHILD_PEDS_TO_CONVO(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &ParentData, structPedsForConversation &PedSpeechConversation, INT iPed, PED_SPEECH ePedSpeech, BOOL bNetworkedPed = FALSE)
	
	IF NOT IS_PEDS_BIT_SET(ParentData.iBS, BS_PED_DATA_PARENT_PED)
		EXIT
	ENDIF
	
	INT iChild
	INT iChildPedID
	PED_CONVO_DATA childConvoData
	
	IF (bNetworkedPed)
		iPed = GET_PED_NETWORK_ARRAY_ID(iPed)
		PRINTLN("[PLAY_PED_SPEECH_CONVO] ADD_CHILD_PEDS_TO_CONVO ADDING CHILD PED TO CONVO: iPed = ", iPed)
		IF (ServerBD.iParentPedID[iPed] = -1)
			EXIT
		ENDIF
		
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
			iChildPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPed]].iChildID[iChild]
			PRINTLN("[PLAY_PED_SPEECH_CONVO] ADD_CHILD_PEDS_TO_CONVO ADDING CHILD PED TO CONVO: iChildPedID = ", iChildPedID)
			IF (iChildPedID != -1)
				RESET_PED_CONVO_DATA(childConvoData)
				GET_PED_CONVO_DATA(ePedLocation, childConvoData, GET_PED_NETWORK_ID(iChildPedID), INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iChildPedID].Data.iActivity), ePedSpeech)
				PRINTLN("[PLAY_PED_SPEECH_CONVO] ADD_CHILD_PEDS_TO_CONVO GOT CHILD PED CONVO DATA   childConvoData.sCharacterVoice = ", childConvoData.sCharacterVoice, "    childConvoData.sRootName = ", childConvoData.sRootName)
				
				IF IS_CONVO_DATA_VALID(childConvoData)		
						
					ADD_PED_FOR_DIALOGUE(PedSpeechConversation, childConvoData.iSpeakerID, GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iChildPedID].niPedID)) , childConvoData.sCharacterVoice)
					PRINTLN("[PLAY_PED_SPEECH_CONVO] ADD_CHILD_PEDS_TO_CONVO  CONVO DATA IS VALID ", childConvoData.sRootName)	
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ELSE
		IF (LocalData.iParentPedID[iPed] = -1)
			EXIT
		ENDIF
		
		REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
			iChildPedID = LocalData.ParentPedData[LocalData.iParentPedID[iPed]].iChildID[iChild]
			IF (iChildPedID != -1)
				RESET_PED_CONVO_DATA(childConvoData)
				GET_PED_CONVO_DATA(ePedLocation, childConvoData, iChildPedID, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iChildPedID].iActivity), ePedSpeech)
				
				IF IS_CONVO_DATA_VALID(childConvoData)
					ADD_PED_FOR_DIALOGUE(PedSpeechConversation, childConvoData.iSpeakerID, LocalData.PedData[iChildPedID].PedID , childConvoData.sCharacterVoice)
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_DISPLAY_SUBTITLES(PED_LOCATIONS ePedLocation, INT iPed, PED_SPEECH ePedSpeech)
	SWITCH ePedLocation
		CASE PED_LOCATION_MUSIC_STUDIO
			IF iPed = NETWORK_PED_ID_0
			AND ENUM_TO_INT(ePedSpeech)> ENUM_TO_INT(PED_SPH_PT_LOITER)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Plays a conversation on a ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
///    Data - The ped data.
///    PedSpeechConversation - Conversation struct to populate.
///    convoData - Convo data to query.
///    PedID - Speech ped ID.
///    iPed - Ped ID to query.
///    ePedSpeech - Ped speech to play.
///    bNetworkedPed - Wheather the ped is networked or not.
PROC PLAY_PED_SPEECH_CONVO(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, structPedsForConversation &PedSpeechConversation, PED_CONVO_DATA convoData, PED_INDEX PedID, INT iPed, PED_SPEECH ePedSpeech, BOOL bNetworkedPed = FALSE)
	
	IF (convoData.iSpeakerID = -1)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONVO - Bail Reason: iSpeakerID is -1")
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(convoData.sRootName)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONVO - Bail Reason: sRootName is null")
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(convoData.sCharacterVoice)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONVO - Bail Reason: sCharacterVoice is null")
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(convoData.sSubtitleTextBlock)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONVO - Bail Reason: sSubtitleTextBlock is null")
		EXIT
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(PedSpeechConversation, convoData.iSpeakerID, PedID, convoData.sCharacterVoice)
	PRINTLN("[PLAY_PED_SPEECH_CONVO][ADD_PED_FOR_DIALOGUE] ADDED ", convoData.sCharacterVoice)
	IF (convoData.bAddChildPedsToConvo)
		PRINTLN("[PLAY_PED_SPEECH_CONVO] ADDING CHILD PEDS")
		ADD_CHILD_PEDS_TO_CONVO(ePedLocation, ServerBD, LocalData, Data, PedSpeechConversation, iPed, ePedSpeech, bNetworkedPed)
	ENDIF
	
	enumSubtitlesState subtitlesState = DO_NOT_DISPLAY_SUBTITLES
	IF SHOULD_DISPLAY_SUBTITLES(ePedLocation, iPed, ePedSpeech)
		subtitlesState = DISPLAY_SUBTITLES
	ENDIF
	CREATE_CONVERSATION(PedSpeechConversation, convoData.sSubtitleTextBlock, convoData.sRootName, CONV_PRIORITY_VERY_HIGH, subtitlesState)

	structPedsForConversation sPedsForConversationBlank
	PedSpeechConversation = sPedsForConversationBlank
	
	PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH_CONVO - Playing Speech - iPed: ", iPed, " sRootName: ", convoData.sRootName, " sCharacterVoice: ", convoData.sCharacterVoice)
	
ENDPROC

/// PURPOSE:
///    Plays speech on a ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
///    Data - The ped data.
///    PedID - Speech ped ID.
///    iPed - Ped to play speech on.
///    ePedSpeech - Ped speech to play.
///    PedSpeechConversation - Conversation struct to populate.
///    bNetworkedPed - Wheather the ped is networked or not.
PROC PLAY_PED_SPEECH(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_INDEX PedID, INT iPed, PED_SPEECH ePedSpeech, structPedsForConversation &PedSpeechConversation, BOOL bNetworkedPed = FALSE)
	
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] PLAY_PED_SPEECH - Bail Reason: Ped is dead")
		EXIT
	ENDIF
	
	PED_CONVO_DATA convoData
	GET_PED_CONVO_DATA(ePedLocation, convoData, iPed, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), ePedSpeech)
	
	IF (convoData.bPlayAsConvo)
		PLAY_PED_SPEECH_CONVO(ePedLocation, ServerBD, LocalData, Data, PedSpeechConversation, convoData, PedID, iPed, ePedSpeech, bNetworkedPed)
	ELSE
		PLAY_PED_SPEECH_CONTEXT(convoData, PedID, iPed)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Sets any data post speech.
/// PARAMS:
///    Data - Ped data to query.
///    SpeechData - Ped speech data to query.
///    ePedSpeech - Ped speech to query.
///    PedID - Speech ped ID.
PROC SET_SPEECH_DATA(PEDS_DATA Data, SPEECH_DATA &SpeechData, PED_SPEECH ePedSpeech, PED_INDEX PedID)
	
	IF NOT IS_ENTITY_ALIVE(PedID)
	OR NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	SWITCH ePedSpeech
		CASE PED_SPH_PT_GREETING
			// "Group Greeting"
			// Description: Set the local players greeting bit if they are within the goodbye distance and another 
			//				player triggers the greeting. Stops the ped spamming the greeting speech for every player.
			IF NOT IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
				IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_ENTITY_COORDS(PedID)) <= GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_PT_BYE))
				AND ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, vPlayerCoords.z>>, <<0.0, 0.0, Data.vPosition.z>>)
					SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
				ENDIF
			ENDIF
		BREAK
		CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_ACCEPT
			IF NOT IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_ACCEPT)
				IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_ENTITY_COORDS(PedID)) <= GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_PT_BYE))
				AND ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, vPlayerCoords.z>>, <<0.0, 0.0, Data.vPosition.z>>)
					SET_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_ACCEPT)
				ENDIF
			ENDIF
		BREAK
		CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_REJECT
			IF NOT IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_WAVE_AWAY)
				IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, GET_ENTITY_COORDS(PedID)) <= GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_PT_BYE))
				AND ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, vPlayerCoords.z>>, <<0.0, 0.0, Data.vPosition.z>>)
					SET_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_WAVE_AWAY)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ EVENTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛


/// PURPOSE:
///    States if the peds from a location are within close proximity with one other.
/// PARAMS:
///    ePedLocation - Location to query.
///    iPed - Ped to query.
/// RETURNS: TRUE if the peds are within close proximity with one other. FALSE otherwise.
FUNC BOOL ARE_SPEECH_PEDS_IN_CLOSE_PROXIMITY(PED_LOCATIONS ePedLocation, INT iPed)
	SWITCH ePedLocation
		DEFAULT
		BREAK
		#IF FEATURE_CASINO_NIGHTCLUB
		CASE PED_LOCATION_CASINO_NIGHTCLUB
			IF (iPed != 13)	// Entrance Bouncer
				RETURN TRUE
			ENDIF
		BREAK
		#ENDIF
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the max number of local speech peds.
/// PARAMS:
///    LocalData - Local ped data to query.
/// RETURNS: The max number of local speech peds.
FUNC INT GET_MAX_LOCAL_SPEECH_PEDS(SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID = 0
	INT iMaxPeds = 0
	REPEAT MAX_NUM_LOCAL_PLAYER_SPEECH_PEDS iArrayID
		IF LocalData.LocalSpeechData[iArrayID].iPedID != -1
			iMaxPeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iMaxPeds
ENDFUNC

/// PURPOSE:
///    Returns a random local speech peds.
/// PARAMS:
///    LocalData - Local ped data to query.
/// RETURNS: A random local speech peds.
FUNC INT GET_RANDOM_LOCAL_SPEECH_PED(SCRIPT_PED_DATA &LocalData)
	
	IF (LocalData.iMaxLocalSpeechPeds = -1)
		LocalData.iMaxLocalSpeechPeds = GET_MAX_LOCAL_SPEECH_PEDS(LocalData)
	ENDIF
	
	INT iRandSpeechPed = GET_RANDOM_INT_IN_RANGE(0, LocalData.iMaxLocalSpeechPeds)
	RETURN LocalData.LocalSpeechData[iRandSpeechPed].iPedID
ENDFUNC

/// PURPOSE:
///    Processes the ped speech events. Triggers the speech if the player can.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
///    eEventType - Speech event type.
///    iEventID - Speech event ID.
PROC PROCESS_PED_SPEECH_EVENTS(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, SCRIPTED_EVENT_TYPES eEventType, INT iEventID)
	
	SWITCH eEventType
		CASE SCRIPT_EVENT_UPDATE_PED_SPEECH
			SCRIPT_EVENT_DATA_PED_SPEECH_UPDATE speechEvent
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, speechEvent, SIZE_OF(speechEvent))
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(speechEvent.Details.FromPlayerIndex)
					
					IF ARE_SPEECH_PEDS_IN_CLOSE_PROXIMITY(ePedLocation, speechEvent.iPed)
						speechEvent.iPed = GET_RANDOM_LOCAL_SPEECH_PED(LocalData)
					ENDIF
					
					IF (speechEvent.iPed >= NETWORK_PED_ID_0)
						IF GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed) != -1
						AND LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)] != -1
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].niPedID)
							AND IS_PLAYER_WITHIN_PED_LISTEN_RANGE(PLAYER_ID(), LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)]], GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].niPedID)))
							AND CAN_PED_PLAY_SPEECH(ePedLocation, GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].niPedID)), speechEvent.iPed, ServerBD.iLayout, speechEvent.ePedSpeech)
								PLAY_PED_SPEECH(ePedLocation, ServerBD, LocalData, ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].Data, GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].niPedID)), speechEvent.iPed, speechEvent.ePedSpeech, LocalData.NetworkPedSpeechConversation, TRUE)
								SET_SPEECH_DATA(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].Data, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)]], speechEvent.ePedSpeech, GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].niPedID)))
							ELSE
								SET_SPEECH_DATA(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].Data, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)]], speechEvent.ePedSpeech, GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(speechEvent.iPed)].niPedID)))
							ENDIF
						ENDIF
					ELSE
						IF speechEvent.iPed != -1
						AND LocalData.iLocalSpeechPedID[speechEvent.iPed] != -1
							IF IS_PLAYER_WITHIN_PED_LISTEN_RANGE(PLAYER_ID(), LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[speechEvent.iPed]], LocalData.PedData[speechEvent.iPed].PedID)
							AND CAN_PED_PLAY_SPEECH(ePedLocation, LocalData.PedData[speechEvent.iPed].PedID, speechEvent.iPed, ServerBD.iLayout,  speechEvent.ePedSpeech)
								PLAY_PED_SPEECH(ePedLocation, ServerBD, LocalData, LocalData.PedData[speechEvent.iPed], LocalData.PedData[speechEvent.iPed].PedID, speechEvent.iPed, speechEvent.ePedSpeech, LocalData.LocalPedSpeechConversation)
								SET_SPEECH_DATA(LocalData.PedData[speechEvent.iPed], LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[speechEvent.iPed]], speechEvent.ePedSpeech, LocalData.PedData[speechEvent.iPed].PedID)
							ELSE
								SET_SPEECH_DATA(LocalData.PedData[speechEvent.iPed], LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[speechEvent.iPed]], speechEvent.ePedSpeech, LocalData.PedData[speechEvent.iPed].PedID)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determines if the player speech type should be broadcast to other players via events.
/// PARAMS:
///    ePedSpeech - Ped speech to query.
///    bNetworkPed - Whether the ped is networked or not.
///    bControllerSpeech - Whether the speech is controller triggered (as opposed to player triggered).
/// RETURNS: TRUE if the speech type should be broadcast, FALSE otherwise.
FUNC BOOL SHOULD_BROADCAST_PLAYER_SPEECH_TYPE(PED_SPEECH ePedSpeech, SPEECH_DATA &speechData, BOOL bNetworkPed = FALSE, BOOL bControllerSpeech = FALSE)
	UNUSED_PARAMETER(speechData)
	// Note: Only trigger local ped controller speech for the local player. Otherwise you could
	//		 potentially have 30 players triggering broadcasted speech on the same local ped, one after another.
	IF (bControllerSpeech AND NOT bNetworkPed)
		RETURN FALSE
	ENDIF
	
	SWITCH ePedSpeech
		CASE PED_SPH_PT_WHERE_TO
		// Note: Adding goodbye speech here to avoid triggering each time a player leaves a speech ped.
		// 		 Group greetings work but group goodbyes do not. If we clear the greeting bit while still
		//		 within the listen distance of a ped the player will most likely trigger the greeting again (not ideal).
		//		 Cleaner to just trigger the goodbye speech for the local player only.
		CASE PED_SPH_PT_BYE
			RETURN FALSE
		BREAK
		
		CASE PED_SPH_PT_LOITER
			IF IS_PEDS_BIT_SET(speechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_DONT_BROADCAST_LOITER)
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the ped speech event can be sent.
/// PARAMS:
///    Data - Ped data to query.
///    SpeechData - Ped speech data to query.
///    pedID - Ped ID to query.
///    bNetworkPed - Whether the ped is networked or not.
/// RETURNS: TRUE if a ped speech event can be sent.
FUNC BOOL CAN_SEND_CONTROLLER_SPEECH_EVENT(PEDS_DATA &Data, SPEECH_DATA &SpeechData, PED_INDEX pedID, BOOL bNetworkPed = FALSE)
	IF (bNetworkPed)
		IF NOT IS_PED_PLAYING_SPEECH(Data.PedID)
		AND NOT IS_PED_CULLING_ACTIVE(Data.iBS)
		AND IS_ANY_PLAYER_WITHIN_PED_LISTEN_RANGE(SpeechData, pedID)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_PLAYING_SPEECH(Data.PedID)
		AND NOT IS_PED_CULLING_ACTIVE(Data.iBS)
		AND IS_PLAYER_WITHIN_PED_LISTEN_RANGE(PLAYER_ID(), SpeechData, pedID)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Attemps to send the ped speech event.
/// PARAMS:
///    ePedLocation - Ped location to query
///    Data - Ped data to query.
///    SpeechData - Ped speech data to query.
///    ePedSpeech - Speech to play.
///    pedID - Ped ID to query.
///    iPed - Ped to play speech on.
///    bNetworkPed - Whether the ped is networked or not.
PROC SEND_CONTROLLER_SPEECH_EVENT(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, SPEECH_DATA &SpeechData, PED_SPEECH ePedSpeech, PED_INDEX pedID, INT iPed, BOOL bNetworkPed = FALSE)
	
	IF NOT IS_ENTITY_ALIVE(Data.PedID)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
		EXIT
	ENDIF
	
	IF CAN_SEND_CONTROLLER_SPEECH_EVENT(Data, SpeechData, pedID, bNetworkPed)
		
		PED_CONVO_DATA convoData
		GET_PED_CONVO_DATA(ePedLocation, convoData, iPed, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), ePedSpeech)
		
		IF (NOT convoData.bConvoRequiresAnimation)
			#IF IS_DEBUG_BUILD
			PRINTLN("[AM_MP_PEDS] SEND_CONTROLLER_SPEECH_EVENT - Calling BROADCAST_PED_SPEECH_DATA - iPed: ", iPed, " ePedSpeech: ", DEBUG_GET_PED_SPEECH_STRING(ePedSpeech))
			#ENDIF
			BROADCAST_PED_SPEECH_DATA(iPed, ePedSpeech, SHOULD_BROADCAST_PLAYER_SPEECH_TYPE(ePedSpeech, SpeechData, bNetworkPed, TRUE))
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[AM_MP_PEDS] SEND_CONTROLLER_SPEECH_EVENT - Speech requires an animation - iPed: ", iPed, " ePedSpeech: ", DEBUG_GET_PED_SPEECH_STRING(ePedSpeech))
			#ENDIF
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
		ENDIF
	ELSE
		PRINTLN("[AM_MP_PEDS] SEND_CONTROLLER_SPEECH_EVENT - Bail Reason: Ped: ", iPed, " is currently speaking. Not calling BROADCAST_PED_SPEECH_DATA")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the min and max controller speech time in milliseconds depending on ped location. 
///    The time is randomised between these two values.
/// PARAMS:
///    ePedLocation - Location to query.
///    iMinMS - Min controller speech time.
///    iMaxMS - Max controller speech time.
PROC GET_PED_CONTROLLER_SPEECH_MIN_AND_MAX_MS(PED_LOCATIONS ePedLocation, INT &iMinMS, INT &iMaxMS)
	iMinMS = 0
	iMaxMS = 0
	
	SWITCH ePedLocation
		CASE PED_LOCATION_ISLAND
			iMinMS = 10000
			iMaxMS = 15001
		BREAK
		CASE PED_LOCATION_SUBMARINE
			iMinMS = 60000
			iMaxMS = 90001
		BREAK
		CASE PED_LOCATION_AUTO_SHOP
			iMinMS = 30000
			iMaxMS = 90001
		BREAK
		CASE PED_LOCATION_MUSIC_STUDIO
			IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
				#IF IS_DEBUG_BUILD
				IF  GET_COMMANDLINE_PARAM_EXISTS("sc_WeedRoomTimer")
					iMinMS = GET_COMMANDLINE_PARAM_INT("sc_WeedRoomTimer")
					iMaxMS = GET_COMMANDLINE_PARAM_INT("sc_WeedRoomTimer")
					EXIT
				ENDIF
				#ENDIF
				iMinMS = g_sMPTunables.iMUSIC_STUDIO_WEED_ROOM_DIALOGUE_MIN_DELAY_MS
				iMaxMS = g_sMPTunables.iMUSIC_STUDIO_WEED_ROOM_DIALOGUE_MAX_DELAY_MS
			ENDIF
		BREAK
	ENDSWITCH
	
	IF (iMinMS = 0)
	AND (iMaxMS = 0)
		iMinMS = 30000
		iMaxMS = 40001
	ENDIF
ENDPROC

/// PURPOSE:
///    Controls the local ped timer and broadcasts the speech event for the controller speech to play.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local ped data to query.
PROC UPDATE_LOCAL_PED_CONTROLLER_SPEECH(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	
	IF NOT HAS_NET_TIMER_STARTED(LocalData.stLocalSpeechTimer)
		
		INT iMinMS
		INT iMaxMS
		GET_PED_CONTROLLER_SPEECH_MIN_AND_MAX_MS(ePedLocation, iMinMS, iMaxMS)
		
		LocalData.iLocalSpeechTimeOffsetMS = GET_RANDOM_INT_IN_RANGE(iMinMS, iMaxMS)
		LocalData.iLocalSpeechPed = GET_LOCAL_PED_CONTROLLER_SPEECH_PED(LocalData)
		
		IF (LocalData.iLocalSpeechPed = -1)
			EXIT
		ENDIF
		
		LocalData.eLocalPedSpeech = GET_PED_CONTROLLER_SPEECH(ePedLocation, LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[LocalData.iLocalSpeechPed]].eCurrentSpeech, LocalData.iLocalSpeechPed, INT_TO_ENUM(PED_ACTIVITIES,LocalData.PedData[LocalData.iLocalSpeechPed].iActivity))
		
		#IF IS_DEBUG_BUILD
		DebugData.SpeechData.iLocalGameTimeSpeechSet = GET_GAME_TIMER()
		#ENDIF
		
		PRINTLN("[AM_MP_PEDS] UPDATE_LOCAL_PED_CONTROLLER_SPEECH - Setting speech time offset (MS) to: ", LocalData.iLocalSpeechTimeOffsetMS)
		START_NET_TIMER(LocalData.stLocalSpeechTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(LocalData.stLocalSpeechTimer, LocalData.iLocalSpeechTimeOffsetMS)
		#IF IS_DEBUG_BUILD
		OR DebugData.SpeechData.bTriggerControlledSpeech
			DebugData.SpeechData.bTriggerControlledSpeech = FALSE
		#ENDIF
			SEND_CONTROLLER_SPEECH_EVENT(ePedLocation, LocalData.PedData[LocalData.iLocalSpeechPed], LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[LocalData.iLocalSpeechPed]], LocalData.eLocalPedSpeech, LocalData.PedData[LocalData.iLocalSpeechPed].PedID, LocalData.iLocalSpeechPed)
			IF NOT IS_PEDS_BIT_SET(LocalData.PedData[LocalData.iLocalSpeechPed].iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
				RESET_NET_TIMER(LocalData.stLocalSpeechTimer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Controls the network ped timer and broadcasts the speech event for the controller speech to play.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
PROC UPDATE_NETWORK_PED_CONTROLLER_SPEECH(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	
	IF NOT HAS_NET_TIMER_STARTED(LocalData.stNetworkSpeechTimer)
		
		INT iMinMS
		INT iMaxMS
		GET_PED_CONTROLLER_SPEECH_MIN_AND_MAX_MS(ePedLocation, iMinMS, iMaxMS)
		
		LocalData.iNetworkSpeechTimeOffsetMS = GET_RANDOM_INT_IN_RANGE(iMinMS, iMaxMS)
		LocalData.iNetworkSpeechPed = GET_NETWORK_PED_CONTROLLER_SPEECH_PED(LocalData)
		
		IF (LocalData.iNetworkSpeechPed = -1)
		OR (LocalData.iNetworkSpeechPed < NETWORK_PED_ID_0)
			EXIT
		ENDIF
		
		LocalData.eNetworkPedSpeech = GET_PED_CONTROLLER_SPEECH(ePedLocation, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)]].eCurrentSpeech, LocalData.iNetworkSpeechPed, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)].Data.iActivity))
		
		#IF IS_DEBUG_BUILD
		DebugData.SpeechData.iNetworkGameTimeSpeechSet = GET_GAME_TIMER()
		#ENDIF
		
		PRINTLN("[AM_MP_PEDS] UPDATE_NETWORK_PED_CONTROLLER_SPEECH - Setting speech time offset (MS) to: ", LocalData.iNetworkSpeechTimeOffsetMS)
		START_NET_TIMER(LocalData.stNetworkSpeechTimer)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(LocalData.stNetworkSpeechTimer, LocalData.iNetworkSpeechTimeOffsetMS)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)].niPedID)
				SEND_CONTROLLER_SPEECH_EVENT(ePedLocation, ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)].Data, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)]], LocalData.eNetworkPedSpeech,  GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)].niPedID)), LocalData.iNetworkSpeechPed, TRUE)
				IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)].Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
					RESET_NET_TIMER(LocalData.stNetworkSpeechTimer)
				ENDIF
			ELSE
				RESET_NET_TIMER(LocalData.stNetworkSpeechTimer)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Wrapper function to set ped loiter speech debug data
/// PARAMS:
///    DebugData - Debug data to set.
///    iLoiterPed - Which ped to loiter.
///    iGameTimeLoiterSpeechSet - Game time the loiter timer was set.
PROC SET_PED_SPEECH_LOITER_DEBUG_DATA(PED_DEBUG_DATA &DebugData, INT iLoiterPed, INT iGameTimeLoiterSpeechSet)
	DebugData.SpeechData.iLoiterPedID = iLoiterPed
	DebugData.SpeechData.iGameTimeLoiterSpeechSet = iGameTimeLoiterSpeechSet
ENDPROC
#ENDIF

/// PURPOSE:
///    Gets the loiter time based on ped location.
/// PARAMS:
///    	ePedLocation - Which location is being queried.
/// RETURNS: The loiter time in milliseconds.
FUNC INT GET_PED_SPEECH_LOITER_TIME_MS(PED_LOCATIONS ePedLocation)
	INT iLoiterTimeMS = 60000
	SWITCH ePedLocation
		CASE PED_LOCATION_SUBMARINE
			iLoiterTimeMS = 120000
		BREAK
		CASE PED_LOCATION_MUSIC_STUDIO
			iLoiterTimeMS = 50000
		BREAK
		CASE PED_LOCATION_NIGHTCLUB
			iLoiterTimeMS = GET_RANDOM_INT_IN_RANGE(15000, 20000)
		BREAK
	ENDSWITCH
	RETURN iLoiterTimeMS
ENDFUNC

/// PURPOSE:
///    Checks if a player triggered speech conditions have been met.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    SpeechData - Speech data to query.
///    PedID - Speech Ped ID.
///    iPed - The ped ID to check.
///    eSpeechType - Speech 
///    LoiterData - Loiter data to set.
/// RETURNS: TRUE if a player triggered speech conditions have been met.
FUNC BOOL HAS_LOCAL_PLAYER_TRIGGERED_PED_SPEECH(PED_LOCATIONS ePedLocation, SPEECH_DATA &SpeechData, PED_INDEX PedID, INT iPed, PED_SPEECH eSpeechType, SCRIPT_PED_LOITER_DATA &LoiterData #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	UNUSED_PARAMETER(iPed)
	
	IF NOT IS_ENTITY_ALIVE(PedID)
	OR NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	BOOL bSpeechTriggered = FALSE
	
	BOOL bShouldTrigger
	
	SWITCH eSpeechType
		CASE PED_SPH_PT_GREETING
			IF NOT IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
				
				IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_USE_AREA)
					bShouldTrigger = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), SpeechData.vAreaPos1, SpeechData.vAreaPos2, SpeechData.fWidth)
				ELSE
					bShouldTrigger =  (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedID)) <= GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, eSpeechType))
				ENDIF
				
				IF bShouldTrigger				
					IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
						IF (GET_CLOUD_TIME_AS_INT() >= SpeechData.iGreetingPosix+MAX_NUM_PED_SPEECH_TIMED_GREETING_POSIX_TIME_SECS)
							SpeechData.iGreetingPosix = GET_CLOUD_TIME_AS_INT()
							SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
							bSpeechTriggered = TRUE
						ENDIF
					ELSE
						SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
						bSpeechTriggered = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PED_SPH_PT_BYE
			IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
				IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_USE_AREA)
					bShouldTrigger = NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), SpeechData.vAreaPos1, SpeechData.vAreaPos2, SpeechData.fWidth)
				ELSE
					bShouldTrigger = (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedID)) > GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, eSpeechType))
				ENDIF
				
				IF bShouldTrigger
					CLEAR_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
					bSpeechTriggered = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE PED_SPH_PT_BUMP
			IF NOT IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_BUMP_ACTIVE)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), PedID)
					SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_BUMP_ACTIVE)
					bSpeechTriggered = TRUE
				ENDIF
			ELSE
				IF NOT IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), PedID)
					CLEAR_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_BUMP_ACTIVE)
				ENDIF
			ENDIF
		BREAK
		CASE PED_SPH_PT_LOITER
			IF NOT HAS_NET_TIMER_STARTED(LoiterData.stLoiterTimer)
				IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_USE_AREA)
					bShouldTrigger = IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), SpeechData.vAreaPos1, SpeechData.vAreaPos2, SpeechData.fWidth)
				ELSE
					bShouldTrigger = (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedID)) <= GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_PT_BYE))
				ENDIF
				
				// Start the loiter timer if player is within range of ped
				IF bShouldTrigger
					START_NET_TIMER(LoiterData.stLoiterTimer)
					LoiterData.loiterPed = PedID
					LoiterData.iLoiterDelay = GET_PED_SPEECH_LOITER_TIME_MS(ePedLocation)
					#IF IS_DEBUG_BUILD
					SET_PED_SPEECH_LOITER_DEBUG_DATA(DebugData, iPed, GET_GAME_TIMER())
					#ENDIF
				ENDIF
			ELSE
				IF (LoiterData.loiterPed = PedID)
				
					// Trigger the speech if loiter timer has expired
					IF HAS_NET_TIMER_EXPIRED(LoiterData.stLoiterTimer, LoiterData.iLoiterDelay)
						RESET_NET_TIMER(LoiterData.stLoiterTimer)
						RETURN TRUE
					ELSE
						IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_USE_AREA)
							//We are outside of the area, so reset.
							bShouldTrigger = NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), SpeechData.vAreaPos1, SpeechData.vAreaPos2, SpeechData.fWidth)
						ELSE
							bShouldTrigger = (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedID)) > GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_PT_BYE))
						ENDIF
				
						// Reset loiter timer if player is no longer near the ped
						IF bShouldTrigger
							RESET_NET_TIMER(LoiterData.stLoiterTimer)
							
							#IF IS_DEBUG_BUILD
							SET_PED_SPEECH_LOITER_DEBUG_DATA(DebugData, -1, 0)
							#ENDIF
						ENDIF
					ENDIF
				ELSE
					
					// If another ped is now closer to the player then loiter that ped instead
					IF IS_ENTITY_ALIVE(LoiterData.loiterPed)
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedID)) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(LoiterData.loiterPed))
						REINIT_NET_TIMER(LoiterData.stLoiterTimer)
						LoiterData.loiterPed = PedID
						
						#IF IS_DEBUG_BUILD
						SET_PED_SPEECH_LOITER_DEBUG_DATA(DebugData, iPed, GET_GAME_TIMER())
						#ENDIF
					ELIF NOT IS_ENTITY_ALIVE(LoiterData.loiterPed)
						PRINTLN("[AM_MP_PEDS][NET_PEDS_SPEECH] ped doesn't exist anymore")
						LoiterData.loiterPed = PedID
						RESET_NET_TIMER(LoiterData.stLoiterTimer)
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE PED_SPH_PT_CHAMPAGNE
			IF g_bChampagneOrdered
				IF (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedID)) <= GET_PED_SPEECH_TRIGGER_DISTANCES(SpeechData, PED_SPH_LISTEN_DISTANCE))
					g_bChampagneOrdered = FALSE
					bSpeechTriggered = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE PED_SPH_PT_WHERE_TO
			IF g_bPavelInCommandRoom
				IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_PAVEL_WHERE_TO_DIALOGUE)
					CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_PAVEL_WHERE_TO_DIALOGUE)
					bSpeechTriggered = TRUE
				ENDIF
			ELSE
				CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_PAVEL_WHERE_TO_DIALOGUE)
			ENDIF
		BREAK
		CASE PED_SPH_PT_BEACH_GUARD
			IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_BEACH_GUARD_TURNAWAY)
				CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_BEACH_GUARD_TURNAWAY)
				bSpeechTriggered = TRUE
			ENDIF
		BREAK
		CASE PED_SPH_PT_SOURCE_CARGO_POSITIVE
			IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_SOURCE_CARGO_POSITIVE)
				CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_SOURCE_CARGO_POSITIVE)
				bSpeechTriggered = TRUE
			ENDIF
		BREAK
		CASE PED_SPH_PT_SOURCE_CARGO_NEGATIVE
			IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_SOURCE_CARGO_NEGATIVE)
				CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_SOURCE_CARGO_NEGATIVE)
				bSpeechTriggered = TRUE
			ENDIF
		BREAK
		CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_ACCEPT
			IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_ACCEPT_SPEECH)
				CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_ACCEPT_SPEECH)
				bSpeechTriggered = TRUE
			ENDIF
		BREAK
		CASE PED_SPH_PT_CASINO_NIGHTCLUB_ENTRY_REJECT
			IF IS_PEDS_BIT_SET(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_WAVE_AWAY_SPEECH)
				CLEAR_PEDS_BIT(g_iPedScriptGlobalBS, BS_GLOBAL_PED_SCRIPT_CN_ENTRY_BOUNCER_WAVE_AWAY_SPEECH)
				bSpeechTriggered = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bSpeechTriggered
ENDFUNC

/// PURPOSE:
///    Returns the max player triggered speech for a specific ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iPed - The ped ID to check.
///    ePedActivity - Ped activity to query.
/// RETURNS: The max player triggered speech for a specific ped.
FUNC INT GET_PED_PLAYER_SPEECH_TOTAL(PED_LOCATIONS ePedLocation, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeechType 		= 0
	INT iMaxPlayerSpeech 	= 0
	PED_SPEECH eSpeech 		= PED_SPH_INVALID
	
	REPEAT PED_SPH_TOTAL iSpeechType
		eSpeech = GET_PED_SPEECH_TYPE(ePedLocation, iPed, ePedActivity, iSpeechType)
		IF IS_PED_SPEECH_PLAYER_TRIGGERED(eSpeech)
			iMaxPlayerSpeech++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iMaxPlayerSpeech
ENDFUNC

/// PURPOSE:
///    Gets the max number of player triggered speech to process per frame.
/// PARAMS:
///    SpeechData - Speech data to query.
/// RETURNS: The max number of player triggered speech to process per frame.
FUNC INT GET_PED_PLAYER_SPEECH_PROCESS_TOTAL(SPEECH_DATA &SpeechData)
	IF (SpeechData.iMaxPedPlayerSpeech < MAX_NUM_PLAYER_SPEECH_PROCESSED_PER_FRAME)
		RETURN SpeechData.iMaxPedPlayerSpeech
	ENDIF
	RETURN MAX_NUM_PLAYER_SPEECH_PROCESSED_PER_FRAME
ENDFUNC

/// PURPOSE:
///    Clears any specific ped speech data.
/// PARAMS:
///    SpeechData - Ped speech data to query.
///    ePedSpeech - Ped speech to query.
PROC CLEAR_SPEECH_DATA(SPEECH_DATA &SpeechData, PED_SPEECH ePedSpeech)
	
	SWITCH ePedSpeech
		CASE PED_SPH_PT_BYE
			IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
				CLEAR_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_GREETING_ACTIVE)
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

/// PURPOSE:
///    -Checks if any player triggered speech conditions have been met. If so it
///     broadcasts an event to tell all players on the script to play the speech on the ped.
///    -GET_PED_PLAYER_SPEECH_PROCESS_TOTAL() amount of player speech types are checked each frame.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    SpeechData - Speech data to query.
///    LoiterData - Loiter data to set.
///    PedID - Speech Ped ID.
///    iPed - The ped ID to check.
///    ePedActivity - Ped activity to query.
///    bNetworkPed - Whether the ped is networked.
PROC UPDATE_PED_PLAYER_SPEECH(PED_LOCATIONS ePedLocation, SPEECH_DATA &SpeechData, SCRIPT_PED_LOITER_DATA &LoiterData, PED_INDEX PedID, INT iPed, PED_ACTIVITIES ePedActivity, BOOL bNetworkPed #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	
	IF NOT IS_PLAYER_WITHIN_PED_LISTEN_RANGE(PLAYER_ID(), SpeechData, PedID)
		// Ensure greeting bit is cleared if outside listen range
		CLEAR_SPEECH_DATA(SpeechData, PED_SPH_PT_BYE)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(SpeechData.iSpeechBS , BS_PED_SPEECH_DATA_PT_MUTE_DURING_CUTSCENE)
		IF NETWORK_IS_IN_MP_CUTSCENE()
		OR IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(ePedLocation)
			IF IS_PED_PLAYING_SPEECH(PedID)			
				STOP_SCRIPTED_CONVERSATION(FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF (bNetworkPed)
		iPed = GET_PED_NETWORK_ID(iPed)
	ENDIF
	
	IF (SpeechData.iMaxPedPlayerSpeech = -1)
		SpeechData.iMaxPedPlayerSpeech = GET_PED_PLAYER_SPEECH_TOTAL(ePedLocation, iPed, ePedActivity)
	ENDIF
	
	INT iCount					= 0
	INT iPlayerTriggeredSpeech	= 0
	BOOL bSpeechFound 			= FALSE
	PED_SPEECH ePedSpeech 		= PED_SPH_INVALID
	
	REPEAT SpeechData.iMaxPedPlayerSpeech iCount
		ePedSpeech = GET_PED_SPEECH_TYPE(ePedLocation, iPed, ePedActivity, SpeechData.iSpeechTriggerToCheck)
		IF IS_PED_SPEECH_PLAYER_TRIGGERED(ePedSpeech)
			
			SpeechData.iSpeechTriggerToCheck++
			IF (SpeechData.iSpeechTriggerToCheck >= SpeechData.iMaxPedPlayerSpeech)
				SpeechData.iSpeechTriggerToCheck = 0
			ENDIF
			
			IF NOT IS_PED_PLAYING_SPEECH(PedID)
			AND HAS_LOCAL_PLAYER_TRIGGERED_PED_SPEECH(ePedLocation, SpeechData, PedID, iPed, ePedSpeech, LoiterData #IF IS_DEBUG_BUILD, DebugData #ENDIF)
				bSpeechFound = TRUE
				BREAKLOOP
			ENDIF
			
			iPlayerTriggeredSpeech++
			IF (iPlayerTriggeredSpeech >= GET_PED_PLAYER_SPEECH_PROCESS_TOTAL(SpeechData))
				BREAKLOOP
			ENDIF
		ELSE
			SpeechData.iSpeechTriggerToCheck = 0
		ENDIF
	ENDREPEAT
	
	IF (bSpeechFound)
		#IF IS_DEBUG_BUILD
		PRINTLN("[AM_MP_PEDS] UPDATE_PED_PLAYER_SPEECH - Calling BROADCAST_PED_SPEECH_DATA - iPed: ", iPed, " ePedSpeech: ", DEBUG_GET_PED_SPEECH_STRING(ePedSpeech))
		#ENDIF
		BROADCAST_PED_SPEECH_DATA(iPed, ePedSpeech, SHOULD_BROADCAST_PLAYER_SPEECH_TYPE(ePedSpeech, SpeechData, bNetworkPed))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if any local player triggered speech conditions have been met. If so it
///    broadcasts an event to tell all players on the script to play the speech on the ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - The local ped data to query.
///    iPed - The ped ID to check.
///    LoiterData - Loiter data to set.
PROC UPDATE_LOCAL_PED_PLAYER_SPEECH(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, INT iPed, SCRIPT_PED_LOITER_DATA &LoiterData #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	
	IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_USE_SPEECH)
		EXIT
	ENDIF	
	
	IF (LocalData.iLocalSpeechPedID[iPed] = -1)
		EXIT
	ENDIF
	
	UPDATE_PED_PLAYER_SPEECH(ePedLocation, LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[iPed]], LoiterData, LocalData.PedData[iPed].PedID, iPed, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPed].iActivity), FALSE #IF IS_DEBUG_BUILD, DebugData #ENDIF)
	
ENDPROC

/// PURPOSE:
///    Checks if any network player triggered speech conditions have been met. If so it
///    broadcasts an event to tell all players on the script to play the speech on the ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - The server ped data to query.
///    LocalData - The local ped data to query.
///    iPed - The ped ID to check.
///    LoiterData - Loiter data to set.
PROC UPDATE_NETWORK_PED_PLAYER_SPEECH(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iPed, SCRIPT_PED_LOITER_DATA &LoiterData #IF IS_DEBUG_BUILD, PED_DEBUG_DATA &DebugData #ENDIF)
	
	IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_USE_SPEECH)
		EXIT
	ENDIF
	
	IF (LocalData.iNetworkSpeechPedID[iPed] = -1)
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPedID)
		UPDATE_PED_PLAYER_SPEECH(ePedLocation, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[iPed]], LoiterData, GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iPed].niPedID)), iPed, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPed].Data.iActivity), TRUE #IF IS_DEBUG_BUILD, DebugData #ENDIF)
	ENDIF
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
