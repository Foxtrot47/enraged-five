USING "globals.sch"
USING "net_include.sch"
USING "transition_crews.sch"
USING "net_ticker_logic.sch"




#IF IS_DEBUG_BUILD
PROC ADD_CREW_INFO_WIDGET()

	INT i
	TEXT_LABEL_63 str

	START_WIDGET_GROUP("Crew Data")
		

		
		START_WIDGET_GROUP("GlobalplayerBD.CrewData")
		REPEAT NUM_NETWORK_PLAYERS i
			str = "Player "
			str += i
			START_WIDGET_GROUP(str)
				ADD_WIDGET_INT_READ_ONLY("ID", GlobalPlayerBD[i].CrewData.ID)
				ADD_WIDGET_BOOL("bIsPrivate", GlobalPlayerBD[i].CrewData.bIsPrivate)
				ADD_WIDGET_BOOL("bIsLeader", GlobalPlayerBD[i].CrewData.bIsLeader)
				ADD_WIDGET_BOOL("bIsFounder", GlobalPlayerBD[i].CrewData.bIsFounder)
				ADD_WIDGET_BOOL("bIsRockstar", GlobalPlayerBD[i].CrewData.bIsRockstar)
				ADD_WIDGET_BOOL("bIsSystem", GlobalPlayerBD[i].CrewData.bIsSystem)
				ADD_WIDGET_INT_READ_ONLY("iMemberCount", GlobalPlayerBD[i].CrewData.iMemberCount)
				ADD_WIDGET_INT_READ_ONLY("iCrewColour_Red", GlobalPlayerBD[i].CrewData.iCrewColour_Red)
				ADD_WIDGET_INT_READ_ONLY("iCrewColour_Blue", GlobalPlayerBD[i].CrewData.iCrewColour_Blue)
				ADD_WIDGET_INT_READ_ONLY("iCrewColour_Green", GlobalPlayerBD[i].CrewData.iCrewColour_Green)
			STOP_WIDGET_GROUP()
		ENDREPEAT	
		STOP_WIDGET_GROUP()
				
		
	STOP_WIDGET_GROUP()

ENDPROC
#ENDIF



PROC UPDATE_LOCAL_PLAYER_GLOBAL_BD_CREW_DATA() // only called when local player joins

	GAMER_HANDLE GamerHandle = GET_LOCAL_GAMER_HANDLE()
	
	IF IS_PLAYER_IN_ACTIVE_CLAN(GamerHandle)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.ID = GET_PLAYER_CLAN_ID(GamerHandle)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.bIsPrivate = IS_PLAYER_CLAN_PRIVATE(GamerHandle)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.bIsLeader = IS_PLAYER_CLAN_LEADER(GamerHandle)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.bIsFounder = IS_PLAYER_CLAN_FOUNDER(GamerHandle)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.bIsRockstar = IS_PLAYER_A_ROCKSTAR(PLAYER_ID())
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.bIsSystem = IS_PLAYER_CLAN_SYSTEM(GamerHandle)		
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.iMemberCount =  GET_PLAYER_CLAN_MEMBER_COUNT(GamerHandle)
		
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.iCrewColour_Red = g_Private_LocalPlayerCrew_Colour_Red
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.iCrewColour_Blue = g_Private_LocalPlayerCrew_Colour_Blue
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.iCrewColour_Green = g_Private_LocalPlayerCrew_Colour_Green

		
		NET_PRINT("UPDATE_LOCAL_PLAYER_GLOBAL_BD_CREW_DATA - updated, player in active crew") NET_NL()		
	ELSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].CrewData.ID = -1 // they are not in a clan
		NET_PRINT("UPDATE_LOCAL_PLAYER_GLOBAL_BD_CREW_DATA - updated, player not in active crew") NET_NL()	
	ENDIF

ENDPROC


FUNC INT ConvertCrewAnimationStringToID(STRING str)
	NET_PRINT("ConvertCrewAnimationStringToID - called with ") NET_PRINT(str) NET_NL()
	IF ARE_STRINGS_EQUAL(str, "Bro Love")
	OR ARE_STRINGS_EQUAL(str, "Animation1")
		RETURN(ENUM_TO_INT(CREW_INTERACTION_BRO_LOVE))
	ENDIF
	IF ARE_STRINGS_EQUAL(str, "The Bird")
	OR ARE_STRINGS_EQUAL(str, "Animation2")
		RETURN(ENUM_TO_INT(CREW_INTERACTION_FINGER))
	ENDIF
	IF ARE_STRINGS_EQUAL(str, "Jerk")
	OR ARE_STRINGS_EQUAL(str, "Animation3")
		RETURN(ENUM_TO_INT(CREW_INTERACTION_WANK))
	ENDIF
	IF ARE_STRINGS_EQUAL(str, "Up Yours")
	OR ARE_STRINGS_EQUAL(str, "Animation4")
		RETURN(ENUM_TO_INT(CREW_INTERACTION_UP_YOURS))
	ENDIF
	IF ARE_STRINGS_EQUAL(str, "null")
		RETURN(ENUM_TO_INT(CREW_INTERACTION_BRO_LOVE))
	ENDIF
	NET_PRINT("ConvertCrewAnimationStringToID - str = ") NET_PRINT(str) NET_NL()
	SCRIPT_ASSERT("ConvertCrewAnimationStringToID - unknown crew animation")
	RETURN(ENUM_TO_INT(CREW_INTERACTION_BRO_LOVE))
ENDFUNC

FUNC INT GetCrewAnim()
	INT iReturn = -1
	GAMER_HANDLE GamerHandle = GET_LOCAL_GAMER_HANDLE()
	IF IS_PLAYER_IN_ACTIVE_CLAN(GamerHandle)	
		TEXT_LABEL_63 strReturn	
		IF NETWORK_CLAN_HAS_CREWINFO_METADATA_BEEN_RECEIVED()
			IF NETWORK_CLAN_CREWINFO_GET_STRING_VALUE("anim", strReturn)
				iReturn = ConvertCrewAnimationStringToID(strReturn)	
			ELSE
				NET_PRINT("GetCrewAnim - NETWORK_CLAN_CREWINFO_GET_STRING_VALUE - could not get string value anim") NET_NL()	
				SCRIPT_ASSERT("GetCrewAnim - NETWORK_CLAN_CREWINFO_GET_STRING_VALUE - could not get string value anim")
			ENDIF
		ELSE
			NET_PRINT("GetCrewAnim - NETWORK_CLAN_HAS_CREWINFO_METADATA_BEEN_RECEIVED = FALSE") NET_NL()
		ENDIF
	ENDIF
	RETURN(iReturn)
ENDFUNC





PROC STORE_UP_TO_DATE_CREW_ANIM()
	MPGlobalsInteractions.iMyCrewInteractionAnim = GetCrewAnim()	
ENDPROC

PROC INIT_LOCAL_PLAYER_CREW_DATA()
	
	STORE_UP_TO_DATE_CREW_ANIM()
	
ENDPROC





