USING "rage_builtins.sch"
USING "globals.sch"

USING "net_mission_control.sch"

USING "net_events.sch"

#IF IS_DEBUG_BUILD
	USING "net_debug.sch"
	USING "net_mission_control_debug.sch"
#ENDIF

USING "net_mission_trigger_public.sch"





// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Control_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Any public functionality that allows access to the Mission Control data.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission Control Events
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Event: Request New Mission Of Type
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to launch any mission of a specific type
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Request_New_Mission_Of_Type(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Request_New_Mission_Of_Type - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Request_New_Mission_Of_Type") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRequestNewMissionOfType theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Request_New_Mission_Of_Type(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Find a free Mission Request slot
	INT freeSlot = Find_Free_MP_Mission_Request_Slot()
	IF (freeSlot = NO_FREE_MISSION_REQUEST_SLOT)
		// ...didn't find an empty slot
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Process_Event_Request_New_Mission_Of_Type(): THERE ARE NO FREE MISSION SLOTS. Ignoring mission request. Tell Keith.")
		#ENDIF
		
		// Broadcast that the request failed (to a specific player unless the requester is a host)
		IF NOT (theEventData.requesterIsHost)
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		ELSE
			Broadcast_Mission_Request_Failed_For_UniqueID(theEventData.uniqueID)
		ENDIF
		
		EXIT
	ENDIF
	
	// Found a free slot, so clear the data
	Clear_One_MP_Mission_Request(freeSlot)
	
	// Store the Data for Mission Request of Type
	Store_MP_Mission_Request_For_Mission_Of_Type(freeSlot, theEventData.Details.FromPlayerIndex, theEventData.requesterIsHost, theEventData.requestFromTeam, theEventData.requestSourceID, theEventData.missionTypeID, theEventData.uniqueID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after Request For New Mission Of Type Accepted") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Request New Specific Mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to launch a specific mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Request_New_Specific_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Request_New_Specific_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Request_New_Specific_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRequestNewSpecificMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Request_New_Specific_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	// Display Mission Name
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Mission: ") NET_PRINT(GET_MP_MISSION_NAME(theEventData.missionIdData.idMission)) NET_NL()
	#ENDIF
	
	// If the player is requesting the mission as an individual, not as a host, then check if there is an existing instance of the mission that the player can join
	IF NOT (theEventData.requesterIsHost)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           Request is from ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_PRINT(" as an individual, not as a script host - check for an existing mission instance to join.")
			NET_NL()
		#ENDIF

		// Ensure player still OK
		IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Player is not OK") NET_NL()
			#ENDIF
			
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
			EXIT
		ENDIF

		// Spectators shouldn't request missions
		IF (IS_PLAYER_SCTV(theEventData.Details.FromPlayerIndex))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Player is a SPECTATOR - shouldn't be requesting mission [")
				NET_PRINT(GET_MP_MISSION_NAME(theEventData.missionIdData.idMission))
				NET_PRINT("]")
				NET_NL()
			#ENDIF
			
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			SCRIPT_ASSERT("Process_Event_Request_New_Specific_Mission(): ERROR: Player as Spectator requested a new specific mission. Tell Keith.")
			
			EXIT
		ENDIF
		
		// Check if the player is busy with another mission (being reserved for another mission is classed as busy)
		m_structPlayerAndTeamInfoMC localPlayerInfo
		Fill_Current_Player_And_Team_Info(localPlayerInfo)
		
		INT playerIndexAsInt = NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
		
		IF NOT (Is_MP_Mission_Request_Player_Free(playerIndexAsInt, localPlayerInfo))
			// Player is not free, but if this is a tutorial mission and the player is doing the pre-game tutorials then allow him to continue
			// KGM 30/9/12: But only if the player is not marked as also 'unavailable'.
			//				NOTE: This can happen if the server hasn't yet received the broadcast data about the player no longer being on ambient (since braodcasts are only sent periodically while events are sent immediately)
			BOOL continueProcessing = FALSE
			IF (Is_MP_Mission_Request_Player_Only_Available_If_Required(playerIndexAsInt, localPlayerInfo))
			AND NOT (Is_MP_Mission_Request_Player_Unavailable(playerIndexAsInt, localPlayerInfo))
				IF (IS_MP_MISSION_A_TUTORIAL(theEventData.missionIdData.idMission))
				OR (theEventData.missionIdData.idMission = eCOC_Do_Cutscene)
					continueProcessing = TRUE
				ENDIF
			ENDIF
			
			IF NOT (continueProcessing)
				#IF IS_DEBUG_BUILD
					NET_PRINT("           BUT: Player is not currently free to start a mission") NET_NL()
					Debug_Output_Player_And_Team_Details(localPlayerInfo)
				#ENDIF
				
				Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
				
				EXIT
			ENDIF
		ENDIF
		
		// Check if there is an existing mission with an appropriate variation that can still be joined by this player
		INT joinedSlot = Add_Player_To_Existing_Mission_Request(localPlayerInfo, theEventData.requestSourceID, theEventData.Details.FromPlayerIndex, theEventData.missionIdData, theEventData.instanceId)
		IF NOT (joinedSlot = NO_FREE_MISSION_REQUEST_SLOT)
			// Managed to add this player to an existing mission request, so broadcast the changed uniqueID back to the player
			Broadcast_Mission_Request_Change_UniqueID(theEventData.Details.FromPlayerIndex, theEventData.uniqueID, Get_MP_Mission_Request_UniqueID(joinedSlot))
			
			EXIT
		ENDIF
	ENDIF
	
	// Find a free Mission Request slot
	INT freeSlot = Find_Free_MP_Mission_Request_Slot()
	IF (freeSlot = NO_FREE_MISSION_REQUEST_SLOT)
		// ...didn't find an empty slot
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Process_Event_Request_New_Specific_Mission(): THERE ARE NO FREE MISSION SLOTS. Ignoring mission request. Tell Keith.")
		#ENDIF
		
		// Broadcast that the request failed (to a specific player unless the requester is a host)
		IF NOT (theEventData.requesterIsHost)
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		ELSE
			Broadcast_Mission_Request_Failed_For_UniqueID(theEventData.uniqueID)
		ENDIF
		
		// Broadcast the reason for failure
		Broadcast_Reason_For_Mission_Request_Failure(theEventData.requesterIsHost, theEventData.Details.FromPlayerIndex, theEventData.requestSourceID, FTLR_ARRAY_FULL, theEventData.missionIdData.idMission)

		EXIT
	ENDIF
	
	// Found a free slot, so clear the data
	Clear_One_MP_Mission_Request(freeSlot)
	
	// Store the Data for New Specific Mission Request
	MP_MISSION_DATA theMissionData
	
	theMissionData.mdID			= theEventData.missionIdData
	theMissionData.iInstanceId	= theEventData.instanceID
	theMissionData.mdUniqueID	= theEventData.uniqueID
	
	Store_MP_Mission_Request_For_Specific_Mission(freeSlot, theEventData.Details.FromPlayerIndex, theEventData.requesterIsHost, theEventData.requestFromTeam, theEventData.requestSourceID, theMissionData)
	
	// KGM 25/2/13: Setting as joinable for individual players now - it can now become 'not joinable during the reservation phase
	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(theEventData.missionIdData.idMission))
		Store_MP_Mission_Request_Option_Players_Can_Join_Mission(freeSlot)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after Request For New Specific Mission Accepted") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Request Join Existing Mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to join an existing specific mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Request_Join_Existing_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Request_Join_Existing_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Request_Join_Existing_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRequestJoinExistingMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Request_Join_Existing_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Find the mission to be joined and join it
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Request is from ")
		NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		NET_PRINT(" - check for the existing mission instance.")
		NET_NL()
	#ENDIF

	// Ensure player still OK
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Player is not OK") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	// Spectators shouldn't be joining missions
	IF (IS_PLAYER_SCTV(theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Player is a SPECTATOR - shouldn't be joining missions [unique ID: ")
			NET_PRINT_INT(theEventData.uniqueID)
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Request_Join_Existing_Mission(): ERROR: Player as Spectator requested to join an existing mission. Tell Keith.")
		
		EXIT
	ENDIF
		
	// Find the mission slot with this unique ID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission doesn't exist, so do nothing (we may broadcast a fail reason later if needed)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Failed to find mission with this uniqueID: ") NET_PRINT_INT(theEventData.uniqueID) NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
		
	// Check if the player is busy with another mission (being reserved for another mission is classed as busy)
	m_structPlayerAndTeamInfoMC localPlayerInfo
	Fill_Current_Player_And_Team_Info(localPlayerInfo)
	
	INT playerIndexAsInt = NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
	
	// KGM 8/6/12: For Heists, free the player from any lower priority mission reservations
	IF (theEventData.sourceID = MP_MISSION_SOURCE_HEIST)
		g_eMPMissionReservationDescs reservationDesc = MP_RESERVED_BY_PLAYER
		Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data(theEventData.Details.FromPlayerIndex, playerIndexAsInt, reservationDesc, localPlayerInfo)
	ENDIF
	
	IF NOT (Is_MP_Mission_Request_Player_Free(playerIndexAsInt, localPlayerInfo))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Player is not currently free to start a mission") NET_NL()
			Debug_Output_Player_And_Team_Details(localPlayerInfo)
		#ENDIF
		
		EXIT
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Add the player to the mission
	g_eMPMissionStatus	theStatus	= Get_MP_Mission_Request_Slot_Status(missionSlot)
	MP_MISSION			theMission	= Get_MP_Mission_Request_MissionID(missionSlot)
	
	SWITCH (theStatus)
		// Acceptable Active Mission States to join (under some circumstances)
		CASE MP_MISSION_STATE_ACTIVE
		CASE MP_MISSION_STATE_ACTIVE_SECONDARY
		CASE MP_MISSION_STATE_OFFERED
			// A Player accepting a Heist invitation shouldn't be joining an active instance of a Heist
			// NOTE: We should be able to safely ignore this without taking any further action
			IF (theEventData.sourceID = MP_MISSION_SOURCE_HEIST)
				// Heist
				#IF IS_DEBUG_BUILD
					NET_PRINT("...........Process_Event_Request_Join_Existing_Mission() - HEIST player trying to join an Active Mission - should only be joining a Pending Heist") NET_NL()
					Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
				#ENDIF
			
				EXIT
			ENDIF

			// Non-Heist, so it's ok to try to join this active mission
			IF NOT (Add_Player_To_Existing_Mission_Request_Offered_Or_Active(missionSlot, theEventData.Details.FromPlayerIndex, theEventData.sourceID, theMission, localPlayerInfo))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...........Process_Event_Request_Join_Existing_Mission() - FAILED TO JOIN EXISTING ACTIVE NON-HEIST MISSION") NET_NL()
				#ENDIF
			
				EXIT
			ENDIF
			BREAK
			
		// Acceptable Pending Mission States to join (under some circumstances)
		CASE MP_MISSION_STATE_RECEIVED
		CASE MP_MISSION_STATE_RESERVED
			// At the moment, only players trying to join a Heist (by upgrading from 'reserved by Leader' to 'Reserved by Player') should join a Pending mission
			// NOTE: Ignore all other attempts to join a Pending Mission
			IF NOT (theEventData.sourceID = MP_MISSION_SOURCE_HEIST)
				// Non-Heist
				#IF IS_DEBUG_BUILD
					NET_PRINT("...........Process_Event_Request_Join_Existing_Mission() - non-HEIST player trying to join a Pending Mission - should only be joining an Active Mission") NET_NL()
					Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
				#ENDIF
			
				EXIT
			ENDIF

			// Heist, so it's ok to try to join this Pending mission
			// NOTE: Don't check for mission full - if over-subscribed the Heist Leader will indicate which players should join when the Heist is started
			IF NOT (Add_Player_To_Existing_Mission_Request_Received_Or_Reserved(missionSlot, theEventData.Details.FromPlayerIndex, theMission, localPlayerInfo, FALSE))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...........Process_Event_Request_Join_Existing_Mission() - FAILED TO BECOME RESERVED AS 'RESERVED-BY-PLAYER' FOR PENDING HEIST") NET_NL()
				#ENDIF
			
				EXIT
			ENDIF
			BREAK
			
		// Unacceptable Mission States to join - but do nothing (we may broadcast a fail reason later if needed)
		CASE NO_MISSION_REQUEST_RECEIVED
			#IF IS_DEBUG_BUILD
				NET_PRINT("...........Process_Event_Request_Join_Existing_Mission() - FAILED TO FIND MISSION IN A JOINABLE STATE") NET_NL()
				Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
			#ENDIF
		
			EXIT
			
		DEFAULT
			SCRIPT_ASSERT("Process_Event_Request_Join_Existing_Mission() - UNKNOWN SWITCH CASE. Needs added. Tell Keith.")
		
			EXIT
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after Request To Join Existing Mission Accepted") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Confirm Reserved For Mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process Confirm Reserved For Mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This is received by an individual player and processed by the Mission Control Client Routines
PROC Process_Event_Confirm_Reserved_For_Mission(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Confirm_Reserved_For_Mission received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventConfirmReservedForMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Confirm_Reserved_For_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store this on this player's local 'MC broadcast' array so that other systems can read it
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_CONFIRMED_RESERVED, theEventData.oldUniqueID, theEventData.numReservedPlayers, theEventData.bitsReservedPlayers)
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_CHANGE_UNIQUE_ID, theEventData.oldUniqueID, theEventData.newUniqueID)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reserve Specific Mission By Player
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to reserve a specific mission and to reserve this player for the mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This will try to add this player to an already reserved mission
PROC Process_Event_Reserve_Mission_By_Player(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reserve_Mission_By_Player - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Reserve_Mission_By_Player") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventReserveMissionByPlayer theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Player(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// If there is an invitin player then ensure that player is still ok
	PLAYER_INDEX	theInvitingPlayer	= theEventData.invitingPlayer
	BOOL			inInvitingPlayerTS	= theEventData.inInvitingPlayerTS
	
	IF (theInvitingPlayer != INVALID_PLAYER_INDEX())
		IF NOT (IS_NET_PLAYER_OK(theInvitingPlayer, FALSE))
			theInvitingPlayer = INVALID_PLAYER_INDEX()
		ENDIF
	ENDIF
	
	IF (theInvitingPlayer = INVALID_PLAYER_INDEX())
		inInvitingPlayerTS = FALSE
	ENDIF
	
	// Check if there is an existing instance of the mission that the player can join
	// ...some debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Request is from: ")	NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))			NET_NL()
		NET_PRINT("              TutorialID  : ")	NET_PRINT_INT(theEventData.tutorialSessionID)								NET_NL()
		NET_PRINT("              SourceID    : ")	NET_PRINT(Convert_Mission_Source_To_String(theEventData.requestSourceID))	NET_NL()
		NET_PRINT("              MissionID   : ")	NET_PRINT(GET_MP_MISSION_NAME(theEventData.missionIdData.idMission))		NET_NL()
		NET_PRINT("              Variation   : ")	NET_PRINT_INT(theEventData.missionIdData.idVariation)						NET_NL()
		NET_PRINT("              CreatorID   : ")	NET_PRINT_INT(theEventData.missionIdData.idCreator)							NET_NL()
		NET_PRINT("              Cloud File  : ")	NET_PRINT(theEventData.missionIdData.idCloudFilename)						NET_NL()
		NET_PRINT("              SharedRegID : ")	NET_PRINT_INT(theEventData.missionIdData.idSharedRegID)						NET_NL()
		NET_PRINT("              Invitor     : ")
		IF (theInvitingPlayer = INVALID_PLAYER_INDEX())
			NET_PRINT("NONE")
		ELSE
			NET_PRINT(GET_PLAYER_NAME(theInvitingPlayer))
		ENDIF
		IF (inInvitingPlayerTS)
			NET_PRINT(" - Already In Inviting Player's Transition Session")
		ENDIF
		NET_NL()
		NET_PRINT("              UniqueID    : ")	NET_PRINT_INT(theEventData.uniqueID)										NET_NL()
	#ENDIF
	
	// ...check if the player is already reserved for the mission
	// KGM 27/1/13: This was added to allow re-reservation by the MissionsAtCoords system following an initial pre-registration when the player accepted an Invite to the mission
	INT matchingSlot = Get_Slot_If_Player_Already_Reserved_By_Player_With_These_Details(theEventData.requestSourceID, theEventData.Details.FromPlayerIndex, theEventData.missionIdData, theEventData.tutorialSessionID)
	IF NOT (matchingSlot = NO_FREE_MISSION_REQUEST_SLOT)	
		// Player already reserved for a mission with these details so Confirm Reservation again and allow the uniqueID to update to the pre-reserved UniqueID
		Broadcast_Confirm_Reserved_For_Mission(theEventData.Details.FromPlayerIndex, Get_MP_Mission_Request_UniqueID(matchingSlot), theEventData.uniqueID, Get_MP_Mission_Request_Reserved_By_Player_Players(matchingSlot))
		
		EXIT
	ENDIF

	// ...ensure the MissionID is valid
	IF (theEventData.missionIdData.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: MissionID is NULL_MISSION") NET_NL()
			SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Player() - ERROR: Requested Mission was eNULL_MISSION. Ignoring Request. Tell Keith.")
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		EXIT
	ENDIF

	// ...ensure player still OK
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Player is not OK") NET_NL()
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		EXIT
	ENDIF

	// ...ensure player is not a spectator
	// KGM 16/4/14: Removed Spectator check, it is now done on the sending player's machine
	
	// ...ensure that the player is part of a team if the mission is team based
	IF (Check_If_Players_Are_In_Teams_For_Mission(theEventData.missionIdData.idMission))
		IF (GET_PLAYER_TEAM(theEventData.Details.FromPlayerIndex) = TEAM_INVALID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Mission Requires Players To Be In Teams and Player Is Not In A Team") NET_NL()
			#ENDIF
			
			// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
			EXIT
		ENDIF
	ENDIF
	
	// ...check if the player is busy with another mission (being reserved for another mission is classed as busy)
	m_structPlayerAndTeamInfoMC localPlayerInfo
	Fill_Current_Player_And_Team_Info(localPlayerInfo)
	
	INT playerIndexAsInt = NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
	
	// This is a 'player reservation' routine, so free the player from any lower priority mission reservations
	g_eMPMissionReservationDescs reservationDesc = MP_RESERVED_BY_PLAYER
	Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data(theEventData.Details.FromPlayerIndex, playerIndexAsInt, reservationDesc, localPlayerInfo)
		
	IF NOT (Is_MP_Mission_Request_Player_Free(playerIndexAsInt, localPlayerInfo))
		// Player is not free, but if this is a tutorial mission and the player is doing the pre-game tutorials then allow him to continue
		BOOL continueProcessing = FALSE
		IF (Is_MP_Mission_Request_Player_Only_Available_If_Required(playerIndexAsInt, localPlayerInfo))
			IF (IS_MP_MISSION_A_TUTORIAL(theEventData.missionIdData.idMission))
			OR (theEventData.missionIdData.idMission = eCOC_Do_Cutscene)
				continueProcessing = TRUE
			ENDIF
		ENDIF
		
		IF NOT (continueProcessing)
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Player is not currently free to start a mission") NET_NL()
				Debug_Output_Player_And_Team_Details(localPlayerInfo)
			#ENDIF
			
			// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
			EXIT
		ENDIF
	ENDIF
	

	// Check if there is already a reserved mission with an appropriate variation that can still be joined by this player
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Check for an existing Reserved Mission with matching details to join:") NET_NL()
	#ENDIF
	
	//If the player is not forcing their way on to the mission
	// Use data passed in through the command rather than this fucntion which checks broadcast data which may be lagging behind.
	// KGM 1/12/14 [BUG 2149729]: Need to allow the reservation to go ahead if this player is already in the inviting player's transition session (JIP from a different session can get blocked when they are alreayd in the transition session but not reserved)
//	IF NOT IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA(theEventData.Details.FromPlayerIndex)
	IF NOT (theEventData.forceOwnInstance)
		INT invitingPlayerSlotFull = -1
		INT joinedSlot = Add_Player_To_Existing_Reserved_Mission(localPlayerInfo, theEventData.requestSourceID, theEventData.Details.FromPlayerIndex, theEventData.missionIdData, theEventData.tutorialSessionID, theInvitingPlayer, inInvitingPlayerTS, invitingPlayerSlotFull)
		IF NOT (joinedSlot = NO_FREE_MISSION_REQUEST_SLOT)	
			// Confirm Reservation in existing mission request slot
			Broadcast_Confirm_Reserved_For_Mission(theEventData.Details.FromPlayerIndex, Get_MP_Mission_Request_UniqueID(joinedSlot), theEventData.uniqueID, Get_MP_Mission_Request_Reserved_By_Player_Players(joinedSlot))
			
			EXIT
		ENDIF
	
		// KGM 24/11/14 [BUG 2143257]: Don't start a new instance if there is an inviting player
		IF (theInvitingPlayer != INVALID_PLAYER_INDEX())
			NET_PRINT("           No Joinable Existing Instance found: This player should only join inviting player. Don't start own instance. Exiting.") NET_NL()
		
			// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
			// KGM 13/1/15 [BUG 2143257]: Also need to broadcast 'mission full' to joining player if they failed to join the inviting player's instance of the mission because it was full
			IF (invitingPlayerSlotFull != -1)
				Broadcast_Mission_Request_Mission_Full(theEventData.Details.FromPlayerIndex, invitingPlayerSlotFull, theEventData.uniqueID)
			ENDIF
			
			EXIT
		ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		NET_PRINT("           Player is forcing their own instance of the corona - theEventData.forceOwnInstance") NET_NL()
	#ENDIF	
	ENDIF
	
	// Don't allow a reservation request from an Invite to start a new reserved mission - the request should already exist.
	IF (theEventData.requestSourceID = MP_MISSION_SOURCE_INVITE)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           No Existing Instance found: This is for an Invite so it should only join existing instances, not start new instances. Exiting.") NET_NL()
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		EXIT
	ENDIF
	
	// If the source is MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN, then change to 'Map Blip' here if a new instance is being setup so that joining players can match with it
	// NOTE: This was spotted when a Spectator chose 'Play Again' and two instances started because the 'source' had matched teh conditions for using MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN
	IF (theEventData.requestSourceID = MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           About to start own instance but with source: MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN. Changing source now to be MP_MISSION_SOURCE_MISSION_FLOW_BLIP") NET_NL()
		#ENDIF
		
		// Change source from the Invite/Map Blip hybrid 'MP_MISSION_SOURCE_MAP_BUT_NOT_WALK_IN' to the vanilla walk-in source: MP_MISSION_SOURCE_MISSION_FLOW_BLIP
		theEventData.requestSourceID = MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	ENDIF
	
	// Find a free Mission Request slot
	INT freeSlot = Find_Free_MP_Mission_Request_Slot()
	IF (freeSlot = NO_FREE_MISSION_REQUEST_SLOT)
		// ...didn't find an empty slot
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Player(): THERE ARE NO FREE MISSION SLOTS. Ignoring request to reserve mission. Tell Keith.")
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		// Broadcast the reason for failure
		BOOL isHost = FALSE
		Broadcast_Reason_For_Mission_Request_Failure(isHost, theEventData.Details.FromPlayerIndex, theEventData.requestSourceID, FTLR_ARRAY_FULL, theEventData.missionIdData.idMission)

		EXIT
	ENDIF
	
	// Found a free slot, so clear the data
	Clear_One_MP_Mission_Request(freeSlot)
	
	// Store the Data for Request to Reserve Mission
	MP_MISSION_DATA	theMissionData
	
	theMissionData.mdID			= theEventData.missionIdData
	theMissionData.mdUniqueID	= theEventData.uniqueID
	
	Store_MP_Mission_Request_To_Reserve_Mission(freeSlot, theEventData.Details.FromPlayerIndex, theEventData.requestFromTeam, theEventData.requestSourceID, theMissionData, theEventData.hasCutscene, theEventData.tutorialSessionID)
	
	// KGM 10/3/14: ASSUME MINIGAMES ARE CLOSED TO MATCHMAKING BY DEFAULT, SO SET THIS TO AVOID A SHORT_TERM 'OPEN' MESSAGE
	IF (theMissionData.mdID.idCreator = FMMC_MINI_GAME_CREATOR_ID)
		// ...ignore this during the launch of a transition session - it was blocking multiple players from joining the same minigame
		IF NOT (IS_TRANSITION_SESSION_RESTARTING())
			#IF IS_DEBUG_BUILD
				NET_PRINT("           Found a MiniGame. Assuming Matchmaking will be closed, so defaulting the Mission Control bitflag to closed") NET_NL()
			#ENDIF
			
			Store_MP_Mission_Request_Option_Closed_To_WalkIns(freeSlot)
		ENDIF
	ENDIF
	
	// KGM 25/2/13: Setting as joinable for individual players now - it can now become 'not joinable during the reservation phase
	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(theEventData.missionIdData.idMission))
		Store_MP_Mission_Request_Option_Players_Can_Join_Mission(freeSlot)
	ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after Request To Reserve Mission Accepted") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF
	
	// Confirm Reservation in new mission request slot
	Broadcast_Confirm_Reserved_For_Mission(theEventData.Details.FromPlayerIndex, Get_MP_Mission_Request_UniqueID(freeSlot), theEventData.uniqueID, Get_MP_Mission_Request_Reserved_By_Player_Players(freeSlot))

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reserve Specific Mission By Leader
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to reserve a specific mission and to reserve all specified players for the mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This will always create a new instance of the mission
PROC Process_Event_Reserve_Mission_By_Leader(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reserve_Mission_By_Leader - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Reserve_Mission_By_Leader") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventReserveMissionByLeader theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Leader(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Perform some initial validation checks
	// ...some debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("        Request is from ")
		NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		NET_PRINT(" - as leader, with the following list of players to be reserved:")
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theEventData.bitsPlayersToReserve)
	#ENDIF

	// ...ensure the MissionID is valid
	IF (theEventData.missionIdData.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: MissionID is NULL_MISSION") NET_NL()
			SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Leader() - ERROR: Requested Mission was eNULL_MISSION. Ignoring Request. Tell Keith.")
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		EXIT
	ENDIF

	// ...ensure player still OK
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Leader is not OK") NET_NL()
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		EXIT
	ENDIF

	// ...ensure leader is not a spectator
	IF (IS_PLAYER_SCTV(theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Leader is a SPECTATOR - shouldn't be trying to reserve a mission [")
			NET_PRINT(GET_MP_MISSION_NAME(theEventData.missionIdData.idMission))
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Leader(): ERROR: Leader as Spectator requested to reserve a mission. Tell Keith.")
		
		EXIT
	ENDIF
	
	// ...check if the player is busy or 'Reserved By Player' for another mission
	m_structPlayerAndTeamInfoMC localPlayerInfo
	Fill_Current_Player_And_Team_Info(localPlayerInfo)
	
	INT playerIndexAsInt = NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
	
	// For the Leader, this is a 'player reservation', so free the leader from any lower priority mission reservations
	g_eMPMissionReservationDescs reservationDesc = MP_RESERVED_BY_PLAYER
	Unreserve_Player_For_Higher_Priority_Reservation_And_Generate_New_Player_And_Team_Data(theEventData.Details.FromPlayerIndex, playerIndexAsInt, reservationDesc, localPlayerInfo)
		
	IF NOT (Is_MP_Mission_Request_Player_Free(playerIndexAsInt, localPlayerInfo))
		// Player is not free, but if this is a tutorial mission and the player is doing the pre-game tutorials then allow him to continue
		// NOTE: 7/6/12 - checking for tutorials here is probably not needed, but it can't do any harm to leave it
		BOOL continueProcessing = FALSE
		IF (Is_MP_Mission_Request_Player_Only_Available_If_Required(playerIndexAsInt, localPlayerInfo))
			IF (IS_MP_MISSION_A_TUTORIAL(theEventData.missionIdData.idMission))
			OR (theEventData.missionIdData.idMission = eCOC_Do_Cutscene)
				continueProcessing = TRUE
			ENDIF
		ENDIF
		
		IF NOT (continueProcessing)
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Leader is not currently free to start a mission") NET_NL()
				Debug_Output_Player_And_Team_Details(localPlayerInfo)
			#ENDIF
			
			// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
			Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
			EXIT
		ENDIF
	ENDIF
	
	// Find a free Mission Request slot
	INT freeSlot = Find_Free_MP_Mission_Request_Slot()
	IF (freeSlot = NO_FREE_MISSION_REQUEST_SLOT)
		// ...didn't find an empty slot
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Process_Event_Reserve_Mission_By_Leader(): THERE ARE NO FREE MISSION SLOTS. Ignoring request to reserve mission. Tell Keith.")
		#ENDIF
		
		// NOTE: we can broadcast failure here because the uniqueID is still specific to this player at this stage
		Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
		
		// Broadcast the reason for failure
		BOOL isHost = FALSE
		Broadcast_Reason_For_Mission_Request_Failure(isHost, theEventData.Details.FromPlayerIndex, theEventData.requestSourceID, FTLR_ARRAY_FULL, theEventData.missionIdData.idMission)

		EXIT
	ENDIF
	
	// Found a free slot, so clear the data
	Clear_One_MP_Mission_Request(freeSlot)
	
	// Store the Data for Request to Reserve Mission - this also reserved the Leader as 'Reserved By Player'
	MP_MISSION_DATA	theMissionData
	
	theMissionData.mdID			= theEventData.missionIdData
	theMissionData.mdUniqueID	= theEventData.uniqueID
	
	Store_MP_Mission_Request_To_Reserve_Mission(freeSlot, theEventData.Details.FromPlayerIndex, theEventData.requestFromTeam, theEventData.requestSourceID, theMissionData, theEventData.hasCutscene)
	
	// KGM 25/2/13: Setting as joinable for individual players now - it can now become 'not joinable during the reservation phase
	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(theEventData.missionIdData.idMission))
		Store_MP_Mission_Request_Option_Players_Can_Join_Mission(freeSlot)
	ENDIF
	
	// Regenerate the player and team struct, then reserve the other players for the mission
	Fill_Current_Player_And_Team_Info(localPlayerInfo)
	Reserve_Players_By_Leader_For_Mission(freeSlot, theEventData.bitsPlayersToReserve, localPlayerInfo)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Player Details after Reservation By Leader") NET_NL()
		Debug_Output_Missions_Player_Details(freeSlot)
		NET_PRINT("...........Contents of all Mission Request Slots after Request To Reserve Mission Accepted") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF
	
	// Confirm Reservation in new mission request slot
	Broadcast_Confirm_Reserved_For_Mission(theEventData.Details.FromPlayerIndex, Get_MP_Mission_Request_UniqueID(freeSlot), theEventData.uniqueID, Get_MP_Mission_Request_Reserved_By_Player_Or_Leader_Players(freeSlot))

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Start Reserved Mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to start a reserved mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This will start an already reserved mission
PROC Process_Event_Start_Reserved_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Start_Reserved_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Start_Reserved_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventStartReservedMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Process_Event_Start_Reserved_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debug Output some player information
	#IF IS_DEBUG_BUILD
		NET_PRINT("         Sent By: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_NL()
		NET_PRINT("    Starting Players passed with the event") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(theEventData.bitsStartingPlayers)
	#ENDIF
	
	// Ensure there are players passed in on the player bitfield
	IF (theEventData.bitsStartingPlayers = ALL_MISSION_CONTROL_BITS_CLEAR)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           IGNORING - There are no Starting Players listed in the bitfield") NET_NL()
			SCRIPT_ASSERT("Process_Event_Start_Reserved_Mission(): The starting players bitfield was empty. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Find the mission slot with this unique ID
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission doesn't exist, players would have been informed of this when it happened so nothing to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Start_Reserved_Mission() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
		
	// Ensure the mission is still reserved - if not then allow the player sending 'start mission' to be added to the mission - the uniqueID is proof enough that the player was already reserved
	// KGM 25/5/13: This is to fix an issue where the new host after migration knows only about a subset of players actually reserved for the mission.
	//				All players that send 'start mission' after the first one is received should get on it if possible, even if not reserved for mission.
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// Check if the sending player is considered free to join the mission - if not free then player is probably already on the mission
		m_structPlayerAndTeamInfoMC localPlayerInfo
		Fill_Current_Player_And_Team_Info(localPlayerInfo)
	
		INT playerIndexAsInt = NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
	
		IF NOT (Is_MP_Mission_Request_Player_Free(playerIndexAsInt, localPlayerInfo))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           IGNORING: Mission is not in the 'Reserved' state and the sending player is not free to join it (so may already be on it)") NET_NL()
				Debug_Output_Player_And_Team_Details(localPlayerInfo)
			#ENDIF
			
			EXIT
		ENDIF
	
		// Player is free to join this mission so can't already be on it
		#IF IS_DEBUG_BUILD
			NET_PRINT("           START MISSION ALREADY RECEIVED. Allow the sending player to join the mission. Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		#ENDIF

		MP_MISSION			theMission	= Get_MP_Mission_Request_MissionID(missionSlot)
		g_eMPMissionSource	theSource	= Get_MP_Mission_Request_Source(missionSlot)
		
		// Add the player to the mission
		IF (Is_MP_Mission_Request_Slot_Status_Received(missionSlot))
			IF NOT (Add_Player_To_Existing_Mission_Request_Received_Or_Reserved(missionSlot, theEventData.Details.FromPlayerIndex, theMission, localPlayerInfo))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...........Process_Event_Start_Reserved_Mission() - FAILED TO JOIN EXISTING MISSION IN 'RECEIVED' STATE") NET_NL()
				#ENDIF
				
				// Send Fail
				Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
				EXIT
			ENDIF
		ELSE
			IF NOT (Add_Player_To_Existing_Mission_Request_Offered_Or_Active(missionSlot, theEventData.Details.FromPlayerIndex, theSource, theMission, localPlayerInfo))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...........Process_Event_Start_Reserved_Mission() - FAILED TO JOIN EXISTING MISSION IN 'OFFERED' or 'ACTIVE' STATE") NET_NL()
				#ENDIF
				
				// Send Fail
				Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)
			
				EXIT
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Start_Reserved_Mission() - Sending Player has been added to Mission") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debug Output some player information
	#IF IS_DEBUG_BUILD
		NET_PRINT("    Players Marked As Reserved For Mission") NET_NL()
		Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Reserved_Players(missionSlot))
	#ENDIF
	
	// Ensure all players listed here are still reserved for the mission, if not then return FAILED - the mission will need set up again for the remaining players
	// ...first, remove players that are no longer playing MP
	INT				bitsUpdatedStartingPlayers	= ALL_MISSION_CONTROL_BITS_CLEAR
	INT				tempLoop					= 0
	PLAYER_INDEX	testPlayer
	
	#IF IS_DEBUG_BUILD
		BOOL playerRemoved = FALSE
	#ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(theEventData.bitsStartingPlayers, tempLoop))
			testPlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(testPlayer, FALSE))
				// ...still in the game
				SET_BIT(bitsUpdatedStartingPlayers, tempLoop)
			ELSE
				#IF IS_DEBUG_BUILD
					playerRemoved = TRUE
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF (playerRemoved)
			NET_PRINT("    At Least One Player Marked As Reserved For Mission Is No Longer In Multiplayer - removing from bitfield and displaying updated list:") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Reserved_Players(missionSlot))
		ENDIF
	#ENDIF
	
	// ...second, check if all listed players still in multiplayer are required for the mission
	IF NOT (Ensure_All_Listed_Players_Are_Reserved_For_Mission(missionSlot, bitsUpdatedStartingPlayers))
		#IF IS_DEBUG_BUILD
			NET_PRINT("    CAUTION - Some starting players are no longer Reserved for the mission. Starting Players list has been updated. Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
			Debug_Output_Player_Names_From_Bitfield(bitsUpdatedStartingPlayers)
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Good To Go: All Starting Players are Marked as Reserved for Mission") NET_NL()
	#ENDIF
	
	// If there are more reserved players than listed here then leave them as reserved if the mission can handle that many players, otherwise tell individual excess players MISSION FULL
	Free_Any_Excess_Players_When_Starting_A_Reserved_Mission_If_Mission_Full(missionSlot, bitsUpdatedStartingPlayers)
	
	// Switch mission from being reserved to received
	Store_MP_Mission_Request_Status_As_Received(missionSlot)
	
	#IF IS_DEBUG_BUILD
		IF (theEventData.debugAllowWithOnePlayer)
			Store_MP_Mission_Request_Debug_Option_Allow_One_Player_Launch(missionSlot)
		ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after Start Reserved Mission request received") NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Cancel Mission Reservation By Player
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to cancel a mission reservation by a player
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This will remove this player from the mission if the mission is still in the 'reserved' state, otherwise leaves the player alone
//			Currently the player will not receive a reply to confirm removal - this may have to change
PROC Process_Event_Cancel_Mission_Reservation_By_Player(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Cancel_Mission_Reservation_By_Player - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Cancel_Mission_Reservation_By_Player") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventCancelMissionReservationByPlayer theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Cancel_Mission_Reservation_By_Player(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Remove the player from a reserved mission
	// ...some debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Request is from ")
		NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		NET_PRINT(" - attempting to remove player from an existing mission that is still in the reserved state.")
		NET_NL()
	#ENDIF

	// ...ensure the UniqueID is valid
	IF (theEventData.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: UniqueID is NO_UNIQUE_ID") NET_NL()
			SCRIPT_ASSERT("Process_Event_Cancel_Mission_Reservation_By_Player() - ERROR: UniqueID was NO_UNIQUE_ID. Ignoring Request. Tell Keith.")
		#ENDIF
		
		// UniqueID is not valid, but don't need to send a reply
		EXIT
	ENDIF

	// ...ensure player still OK
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Player is not OK") NET_NL()
		#ENDIF
		
		// Player is not OK, but don't need to send a reply
		EXIT
	ENDIF
	
	// ...find the mission
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission doesn't exist, players would have been informed of this when it happened so nothing to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Cancel_Mission_Reservation_By_Player() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	// ...ensure the mission is still reserved
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// The Mission is no longer reserved so this player will aleady have been dealt with, so do nothing else here
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Cancel_Mission_Reservation_By_Player() - MISSION IS NO LONGER RESERVED") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found (Reserved) Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Remove the player as reserved for the mission
	Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Player Removed from Reserved List for Reserved Mission in slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Cancel Mission Reserved By Leader
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request to cancel a mission reserved by a leader
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	If the mission is still in the 'reserved' state then removes all players from it and cancels the mission reservation
PROC Process_Event_Cancel_Mission_Reserved_By_Leader(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Cancel_Mission_Reserved_By_Leader - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Cancel_Mission_Reserved_By_Leader") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventCancelMissionReservedByLeader theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Cancel_Mission_Reserved_By_Leader(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Remove all players from a reserved mission and cancel the mission request
	// ...some debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Request is from ")
		NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		NET_PRINT(" - attempting to cancel an existing mission that is still in the reserved state.")
		NET_NL()
	#ENDIF

	// ...ensure the UniqueID is valid
	IF (theEventData.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: UniqueID is NO_UNIQUE_ID") NET_NL()
			SCRIPT_ASSERT("Process_Event_Cancel_Mission_Reserved_By_Leader() - ERROR: UniqueID was NO_UNIQUE_ID. Ignoring Request. Tell Keith.")
		#ENDIF
		
		// UniqueID is not valid, but don't need to send a reply
		EXIT
	ENDIF
	
	// ...find the mission
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission doesn't exist, players would have been informed of this when it happened so nothing to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Cancel_Mission_Reserved_By_Leader() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	// ...ensure the mission is still reserved
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		// The Mission is no longer reserved so this player will aleady have been dealt with, so do nothing else here
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Cancel_Mission_Reserved_By_Leader() - MISSION IS NO LONGER RESERVED") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found (Reserved) Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Remove the player as reserved for the mission
	INT reservedPlayers = Get_MP_Mission_Request_Reserved_Players(missionSlot)
	INT tempLoop = 0
	
	#IF IS_DEBUG_BUILD
		PLAYER_INDEX thisPlayer
	#ENDIF
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(reservedPlayers, tempLoop))
			Remove_PlayerAsInt_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(missionSlot, tempLoop)
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("            Removed Player from Reserved Mission: ")
				NET_PRINT_INT(tempLoop)
				
				thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
				IF (IS_NET_PLAYER_OK(thisPlayer))
					NET_PRINT("   [")
					NET_PRINT(GET_PLAYER_NAME(thisPlayer))
					NET_PRINT("]")
					NET_NL()
				ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........All Players should be Removed from Reserved Mission in slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF
	
	// Fail the mission request now (rather than waiting for the maintenance loop to detect that all reserved players have left the mission and auto-cleaning up)
	//	This is just in case a 'reserve mission by player' comes in this frame which may keep the mission alive if it was waiting to be auto-cleaned up
	MP_Mission_Request_Failed(missionSlot)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Change Reserved Mission Details
//		Added for when navigating the Heist Planning Board
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allow the details of a reserved mission to be changed
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This was added for Heists because the chosen heist can be changed on the planning board
//			This request should only be made by a player that has been 'Reserved-By_Player'.
PROC Process_Event_Change_Reserved_Mission_Details(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MControl]: Process_Event_Change_Reserved_Mission_Details - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Change_Reserved_Mission_Details") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventChangeReservedMissionDetails theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Change_Reserved_Mission_Details(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	// ...ensure the UniqueID is valid
	IF (theEventData.uniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: UniqueID is NO_UNIQUE_ID") NET_NL()
			SCRIPT_ASSERT("Process_Event_Change_Reserved_Mission_Details() - ERROR: UniqueID was NO_UNIQUE_ID. Ignoring Request. Tell Keith.")
		#ENDIF
		
		// NOTE: Don't broadcast failure, we'll leave the existing mission set up - this should be pretty much impossible to happen anyway!
		EXIT
	ENDIF
	
	// Perform some initial validation checks
	// ...some debug output
	#IF IS_DEBUG_BUILD
		NET_PRINT("           Request is from ")
		NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		IF (theEventData.isHost)
			NET_PRINT(" (Acting As Calling Script Host)")
		ENDIF
		NET_PRINT(" for UniqueID: ")
		NET_PRINT_INT(theEventData.uniqueID)
		NET_NL()
	#ENDIF
	
	// ...ensure the mission is still 'reserved'
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission doesn't exist, players would have been informed of this when it happened so nothing to do
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Change_Reserved_Mission_Details() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
		
	// Ensure the mission is still reserved - if not do nothing - this will have been dealt with at the time the mission changed from being reserved
	IF NOT (Is_MP_Mission_Request_Slot_Status_Reserved(missionSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           IGNORING - The Mission Is No longer 'Reserved'. Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
		#ENDIF
		
		EXIT
	ENDIF

	// ...ensure the new MissionID is valid
	IF (theEventData.newMissionIdData.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: MissionID is NULL_MISSION") NET_NL()
			SCRIPT_ASSERT("Process_Event_Change_Reserved_Mission_Details() - ERROR: Requested Mission was eNULL_MISSION. Ignoring Request. Tell Keith.")
		#ENDIF
		
		// NOTE: don't broadcast failure here, just leave the existing details set up - this should be pretty much impossible anyway!
		EXIT
	ENDIF

	// Do additional checks if the request is from an individual rather than a script host
	IF NOT (theEventData.isHost)
		// ...ensure player still OK
		IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Requesting player is not OK") NET_NL()
			#ENDIF
			
			// NOTE: don't broadcast failure, just leave the existing details set up
			EXIT
		ENDIF
		
		// ...only accept requests from players 'Reserved_By_Player'
		IF NOT (Is_Player_Reserved_By_Player_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
			#IF IS_DEBUG_BUILD
				NET_PRINT("           BUT: Requesting player is not Reserved_By_Player, so assuming the player doesn't have authority to make this change") NET_NL()
			#ENDIF
			
			// NOTE: don't broadcast failure, just leave the existing details set up
			EXIT
		ENDIF
	ENDIF
	
	// Store the new details
	Store_MP_Mission_MissionID_Data(missionSlot, theEventData.newMissionIdData)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Contents of all Mission Request Slots after Mission Details Changed in Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Invite Player Onto Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process an invitation to a specific player to join a specific mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Invite_Player_Onto_Mission(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Invite_Player_Onto_Mission received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventInvitePlayerOntoMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Invite_Player_Onto_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Pass the request to the Mission triggering routines
	Gather_Comms_Details_Then_Invite_Player_Onto_Mission(theEventData.missionData, theEventData.bitsOptions)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Force Player Onto Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process forcing a player onto a specific mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Force_Player_Onto_Mission(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Force_Player_Onto_Mission received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventForcePlayerOntoMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Force_Player_Onto_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	IF NOT IS_THIS_EVENT_UNIQUE_ID_OK(theEventData.UniqueId)
		SCRIPT_ASSERT("Process_Event_Force_Player_Onto_Mission(): IS_THIS_EVENT_UNIQUE_ID_OK(theEventData.UniqueId)")
		PRINTLN("[TS] * Process_Event_Force_Player_Onto_Mission(): IS_THIS_EVENT_UNIQUE_ID_OK(theEventData.UniqueId)")
		EXIT
	ENDIF
		
	// Pass the request to the Mission triggering routines
	IF (IS_BIT_SET(theEventData.bitsOptions, MC_BITS_TRIGGERING_USE_COMMS))
		// Force onto Mission with Comms
		Gather_Comms_Details_Then_Force_Player_Onto_Mission(theEventData.missionData, theEventData.bitsOptions)
	ELSE
		// Force onto Mission No Comms
		BOOL showCutscene = IS_BIT_SET(theEventData.bitsOptions, MC_BITS_TRIGGERING_SHOW_CUTSCENE)
		Force_Launch_Mission(theEventData.missionData, showCutscene, TRUE)
	ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reply Player Joined Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a reply from Player to Server to confirm player joined the specified mission.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Reply_Player_Joined_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Joined_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Reply_Player_Joined_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventReplyPlayerJoinedMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Reply_Player_Joined_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Does the replying player still exist?
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		NET_PRINT("           Player is no longer in network game - IGNORING 'Joined Mission' reply") NET_NL()
		
		EXIT
	ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           From Player: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_NL()
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Reply_Player_Joined_Mission() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Ensure the player isn't already on this mission
	IF (Is_Player_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Joined_Mission() - Player has already accepted this mission") NET_NL()
			Debug_Output_Team_Names_From_Bitfield(Get_MP_Mission_Request_Active_Players(missionSlot))
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Reply_Player_Joined_Mission() - PLAYER ALREADY ACTIVE ON MISSION. See Console Log. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Ensure the player is trying to join this mission or has confirmed this mission
	IF NOT (Is_Player_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
	AND NOT (Is_Player_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Joined_Mission() - Player wasn't trying to join or hadn't confirmed this mission") NET_NL()
			Debug_Output_Missions_Player_Details(missionSlot)
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debug Check to highlight if a player is 'joining' and 'confirmed' for a mission - should be one or the other
	#IF IS_DEBUG_BUILD
		IF (Is_Player_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		AND (Is_Player_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Joined_Mission() - DATA INCONSISTENCY: Player is 'joining' and has 'confirmed' mission") NET_NL()
			Debug_Output_Missions_Player_Details(missionSlot)
		
			SCRIPT_ASSERT("Process_Event_Reply_Player_Joined_Mission() - WARNING: Player is 'Joining' and 'Confirmed' for mission - this is a data inconsistency. See Console Log. Tell Keith.")
		ENDIF
	#ENDIF
	
	// Use a variable for better debug output
	#IF IS_DEBUG_BUILD
		BOOL isJoiningMission = Is_Player_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	#ENDIF
	
	// All seems good, so update the player status from 'joining' or 'confirmed' to 'active'
	Clear_Player_As_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Clear_Player_As_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Set_Player_As_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	// Clear the player as having previously rejected the mission (not really necessary, just to tidy things up)
	Clear_Player_As_Rejected_From_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	// Broadcast that the Player is Actively On the Mission
	Broadcast_Mission_Request_Player_On_Mission(missionSlot, theEventData.Details.FromPlayerIndex)
	
	#IF IS_DEBUG_BUILD
		IF (isJoiningMission)
			NET_PRINT("...........Player Moved from Joining List to Active List for Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
		ELSE
			NET_PRINT("...........Player Moved from Confirmed List to Active List for Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
		ENDIF
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF
	
	// Check if this makes the mission full for this player's team
	INT thisTeam = GET_PLAYER_TEAM(theEventData.Details.FromPlayerIndex)
	IF (Check_If_Mission_Or_Team_Full(missionSlot, thisTeam))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Mission Is Now Full For Players Team: ") NET_PRINT(Convert_Team_To_String(thisTeam)) NET_NL()
		#ENDIF
		
		Free_Non_Active_Players_For_Mission_Or_Team(missionSlot, thisTeam)
	ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reply Player Failed To Join Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a reply from Player to Server to indicate player failed to join the specified mission.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Reply_Player_Failed_To_Join_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Failed_To_Join_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Reply_Player_Failed_To_Join_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventReplyPlayerFailedToJoinMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Reply_Player_Failed_To_Join_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Does the replying player still exist?
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		NET_PRINT("           Player is no longer in network game - IGNORING 'Failed To Join Mission' reply") NET_NL()
		
		EXIT
	ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           From Player: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_NL()
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Reply_Player_Failed_To_Join_Mission() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Ensure the player isn't already on this mission
	IF (Is_Player_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Failed_To_Join_Mission() - Player has already accepted this mission") NET_NL()
			Debug_Output_Team_Names_From_Bitfield(Get_MP_Mission_Request_Active_Players(missionSlot))
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Reply_Player_Failed_To_Join_Mission() - PLAYER ALREADY ACTIVE ON MISSION. See Console Log. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Ensure the player is trying to join this mission
	IF NOT (Is_Player_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
	AND NOT (Is_Player_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Failed_To_Join_Mission() - Player wasn't trying to join or hadn't confirmed this mission") NET_NL()
			Debug_Output_Missions_Player_Details(missionSlot)
		#ENDIF
		
		EXIT
	ENDIF
	
	// Use a variable for better debug output
	#IF IS_DEBUG_BUILD
		BOOL isJoiningMission = Is_Player_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	#ENDIF
	
	// All seems good, so update the player status from 'joining' or 'confirmed' to 'excluded' (to prevent the player being offered the mission again)
	Clear_Player_As_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Clear_Player_As_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	// Should the player be marked as having actively rejected the mission?
	IF (theEventData.rejectedByPlayer)
		Set_Player_As_Rejected_From_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (isJoiningMission)
			NET_PRINT("...........Player Removed from Joining List for Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
		ELSE
			NET_PRINT("...........Player Removed from Confirmed List for Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
		ENDIF
		
		IF (theEventData.rejectedByPlayer)
			NET_PRINT("...........Player Marked As Actively Rejecting Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
		ENDIF
		
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Reply Player Ended Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a reply from Player to Server to indicate player left the active mission.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	A player can leave the active mission for a variety of missions, from quitting to completing, to getting booted, etc.
PROC Process_Event_Reply_Player_Ended_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Ended_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Reply_Player_Ended_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventReplyPlayerEndedMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Reply_Player_Ended_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Does the replying player still exist?
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		NET_PRINT("           Player is no longer in network game - IGNORING 'Ended Mission' reply") NET_NL()
		
		EXIT
	ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           From Player: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_NL()
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Reply_Player_Ended_Mission() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Ensure the player is on this mission
	IF NOT (Is_Player_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Reply_Player_Ended_Mission() - Player was not active on the mission being ended") NET_NL()
			Debug_Output_Team_Names_From_Bitfield(Get_MP_Mission_Request_Active_Players(missionSlot))
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Reply_Player_Ended_Mission() - PLAYER NOT ACTIVE ON MISSION. See Console Log. Tell Keith.")
		
		EXIT
	ENDIF
	
	// All seems good, so update the player status from 'active' to 'excluded' (to prevent the player being offered the mission again - unless this is allowed)
	Clear_Player_As_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	MP_MISSION	theMissionID	= Get_MP_Mission_Request_MissionID(missionSlot)
	INT			theVariation	= Get_MP_Mission_Request_MissionVariation(missionSlot)
	
	IF NOT (Does_MP_Mission_Variation_Allow_Rejoin_After_Leaving(theMissionID, theVariation))
		Set_Player_As_Excluded_From_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Player Moved from Active List to Excluded List for Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
			Debug_Output_Missions_Player_Details(missionSlot)
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Player Removed from Active List but is allowed to rejoin, so not excluded: ") NET_PRINT_INT(missionSlot) NET_NL()
			Debug_Output_Missions_Player_Details(missionSlot)
		#ENDIF
	ENDIF
	
	
	// KGM 18/4/12: Some missions allow some players or teams to remain on mission for a period of time after other players leave.
	//				For these missions, issue player-specific 'mission finished' events to allow the mission flows of the early leavers to continue before the mission fully cleans up.
	IF (Should_Mission_Send_Player_Specific_Finished_Events(Get_MP_Mission_Request_MissionID(missionSlot)))
		// ...send player-specific mission finished event
		Broadcast_Mission_Request_Finished_To_Specific_Player(missionSlot, theEventData.Details.FromPlayerIndex)
	ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Finished
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event from Mission Control Server to inform Clients that a Mission Request Finished
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This informs the original mission requester and any players that a started mission has now finished
PROC Process_Event_Mission_Request_Finished(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Request_Finished received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventMissionRequestFinished theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Request_Finished(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store this on this player's local 'MC broadcast' array so that other systems can read it
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_MISSION_FINISHED, theEventData.uniqueID)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Failed
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event from Mission Control Server to inform Clients that a Mission Request was refused.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	This informs the original mission requester that a Request was refused.
PROC Process_Event_Mission_Request_Failed(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Request_Failed received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventMissionRequestFailed theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Request_Failed(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store this on this player's local 'MC broadcast' array so that other systems can read it
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_REQUEST_FAILED, theEventData.uniqueID)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Mission Full
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process informing a player that the Mission is Full
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Mission_Request_Mission_Full(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Request_Mission_Full received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventMissionRequestMissionFull theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Request_Mission_Full(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store this on this player's local 'MC broadcast' array so that other systems can read it
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_MISSION_FULL, theEventData.uniqueID)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Change UniqueID
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process informing a player that a UniqueID has changed
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Mission_Request_Change_UniqueID(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Request_Change_UniqueID received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventMissionRequestChangeUniqueID theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Request_Change_UniqueID(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store this on this player's local 'MC broadcast' array so that other systems can read it
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_CHANGE_UNIQUE_ID, theEventData.oldUniqueID, theEventData.newUniqueID)
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Request Change UniqueID
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

PROC Process_Event_Mission_Request_Player_On_Mission(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Request_Player_On_Mission received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventMissionRequestPlayerOnMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Request_Player_On_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Store this on this player's local 'MC broadcast' array so that other systems can read it
	Store_MP_Mission_Request_Received_Broadcast(MCCBT_PLAYER_ON_MISSION, theEventData.uniqueID)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Check If Still Ok To Join Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request for confirmation from a player making sure the mission is still ok to join.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Check_If_Still_Ok_To_Join_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Check_If_Still_Ok_To_Join_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Check_If_Still_Ok_To_Join_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventCheckIfStillOkToJoinMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Check_If_Still_Ok_To_Join_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Does the replying player still exist?
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		NET_PRINT("           Player is no longer in network game - IGNORING CHECK 'Still Ok To Join Mission'") NET_NL()
		
		EXIT
	ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           From Player: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_NL()
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Check_If_Still_Ok_To_Join_Mission() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
			NET_PRINT("...........NOTE: IGNORING - No response sent to player - Mission Trigger should already have been informed mission is not available") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Ensure player isn't already active on the mission
	IF (Is_Player_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Check_If_Still_Ok_To_Join_Mission() - Player is already on the mission") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Active_Players(missionSlot))
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Check_If_Still_Ok_To_Join_Mission(): Player is already active on the mission. See Console Log. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Ensure player hasn't already checked if it is still ok to join the mission
	IF (Is_Player_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Check_If_Still_Ok_To_Join_Mission() - Player has already checked if it is still ok to join the mission") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Confirmed_Players(missionSlot))
		#ENDIF
		
		SCRIPT_ASSERT("Process_Event_Check_If_Still_Ok_To_Join_Mission(): Player has already checked if it is ok to join mission. See Console Log. Tell Keith.")
		
		EXIT
	ENDIF
	
	// Ensure the player is still being offered this mission
	IF NOT (Is_Player_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Check_If_Still_Ok_To_Join_Mission() - Player is no longer joining the mission") NET_NL()
			Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Joining_Players(missionSlot))
			NET_PRINT("...........NOTE: IGNORING - No response sent to player - Mission Trigger should already have been informed.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check if the mission is already full for this player's team
	INT thisTeam = GET_PLAYER_TEAM(theEventData.Details.FromPlayerIndex)
	IF (Check_If_Mission_Or_Team_Full(missionSlot, thisTeam))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Check_If_Still_Ok_To_Join_Mission() - Mission or Team is full")
			IF (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(missionSlot)))
				NET_PRINT(" for this player's team: ")
				NET_PRINT(Convert_Team_To_String(thisTeam))
				NET_NL()
			ENDIF
			Debug_Output_Missions_Player_Details(missionSlot)
			Broadcast_Mission_Request_Mission_Full(theEventData.Details.FromPlayerIndex, missionSlot)
		#ENDIF
		
		EXIT
	ENDIF
	
	// Output the current joining list
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Joining List for mission in slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Player_Names_From_Bitfield(Get_MP_Mission_Request_Joining_Players(missionSlot))
	#ENDIF
	
	// All seems good, so broadcast a confirmation
	Broadcast_Confirmation_Player_Should_Launch_Mission(theEventData.Details.FromPlayerIndex, missionSlot)
	
	// Update the player status from 'offered' to 'confirmed'
	Clear_Player_As_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Set_Player_As_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Player Moved from Joining List to Confirmed List for Mission: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Confirmation Launch Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Respond to a player that requested 'ok to join' confirmation that it is ok to join the mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Confirmation_Launch_Mission(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Confirmation_Launch_Mission received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventConfirmationLaunchMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Confirmation_Launch_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	// Is a mission triggering request still active?
	IF NOT (Is_Mission_Triggering_Request_In_Progress())
		#IF IS_DEBUG_BUILD
			NET_PRINT("           IGNORING - Mission Triggering Is Not Trying To Launch A Mission.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Make sure the same mission is still valid within the triggering system
	IF NOT (theEventData.missionData.mdUniqueID = g_sTriggerMP.mtMissionData.mdUniqueID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           IGNORING - The unique ID differes from the unique ID of the mission being triggered.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Pass the request to the Mission triggering routines
	Set_MT_Bitflag_As_Confirmed_Ok_To_Join()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Reason For Mission Request Failure
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_control.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process informing a player of why a mission request failed
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Reason_For_Mission_Request_Failure(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Request_Mission_Full received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventReasonForMissionRequestFailure theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Request_Mission_Full(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Display the Reason why the Mission Request Failed To Launch
	SWITCH (theEventData.reason)
		CASE FTLR_ARRAY_FULL
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_PRINT("MC_FAIL_ARRAY")
				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			#ENDIF
			BREAK
			
		CASE FTLR_DATA_SETUP_ERROR
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_PRINT("MC_FAIL_DATA")
				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			#ENDIF
			BREAK
			
		CASE FTLR_TEAM_REQUIREMENTS_NOT_MET
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_PRINT("MC_FAIL_CONFIG")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(theEventData.mission))
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(Convert_Team_Configuration_To_String(Get_MP_Mission_Variation_Team_Configuration(theEventData.mission, NO_MISSION_VARIATION)))
				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			#ENDIF
			BREAK
			
		CASE FTLR_PRISON_MISSION_ALREADY_EXISTS
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_PRINT("MC_FAIL_PRISON")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(theEventData.mission))
				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			#ENDIF
			BREAK
			
		CASE FTLR_MISSION_INSTANCE_ACTIVE
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_PRINT("MC_FAIL_MISSION")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(theEventData.mission))
				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			#ENDIF
			BREAK
			
		CASE FTLR_MISSION_VARIATION_INSTANCE_ACTIVE
			#IF IS_DEBUG_BUILD
				BEGIN_TEXT_COMMAND_PRINT("MC_FAIL_MISSVAR")
					ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_MP_MISSION_NAME(theEventData.mission))
				END_TEXT_COMMAND_PRINT(DEFAULT_GOD_TEXT_TIME, TRUE)
			#ENDIF
			BREAK
			
		DEFAULT
			SCRIPT_ASSERT("Process_Event_Mission_Request_Mission_Full(): Unknown Reason For Failure. Tell Keith.")
			BREAK
	ENDSWITCH

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Not Joinable
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event from a Mission that stops all teams from joining mission.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	Informs the Mission Control host that the mission is no longer joinable.
PROC Process_Event_Mission_Not_Joinable(INT paramEventID)
	
	// Retrieve the data associated with the event
	m_structEventMissionNotJoinable theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Not_Joinable(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Not_Joinable - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF

		// To help recover should this event get lost due to Host Migration, non-hosts on the mission should store this event so it can be re-issued by someone if required
		IF (Is_Player_Actively_On_This_Mission(theEventData.missionData.mdID.idMission))
			#IF IS_DEBUG_BUILD
				 NET_PRINT("          - but player is on this mission so storing the event on the resync array - just in case") NET_NL()
			#ENDIF
			
			Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_MISSION_NOT_JOINABLE, theEventData.missionData.mdUniqueID)
		ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Mission_Not_Joinable") NET_NL()
	#ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Mission_Not_Joinable() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Make sure the mission is the same - a pre-mission cutscene will use the same uniqueID as the mission but should be ignored
	IF NOT (theEventData.missionData.mdID.idMission = Get_MP_Mission_Request_MissionID(missionSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........IGNORING - the missions are different, this is probably the pre-mission cutscene, not the mission") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Clear the Joinable Teams data for this Slot
	Clear_MP_Mission_Request_Joinable_Teams_Bitfield(missionSlot)
	
	// Clear the 'players Can Join' flag for this Slot (used for missions that aren't team-based)
	Clear_MP_Mission_Request_Option_Players_Can_Join_Mission(missionSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Clearing all joinable teams for slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Joinable Again For Players
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event from a Mission that allows players (not teams - that needs more code) to join again after being marked as not joinable.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	Informs the Mission Control host that the mission is joinable again for players.
PROC Process_Event_Mission_Joinable_Again_For_Players(INT paramEventID)
	
	// Retrieve the data associated with the event
	m_structEventMissionNotJoinable theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Joinable_Again_For_Players(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Joinable_Again_For_Players - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF

		// To help recover should this event get lost due to Host Migration, non-hosts on the mission should store this event so it can be re-issued by someone if required
		IF (Is_Player_Actively_On_This_Mission(theEventData.missionData.mdID.idMission))
			#IF IS_DEBUG_BUILD
				 NET_PRINT("          - but player is on this mission so storing the event on the resync array - just in case") NET_NL()
			#ENDIF
			
			Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_MISSION_JOINABLE_AGAIN_FOR_PLAYERS, theEventData.missionData.mdUniqueID)
		ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Mission_Joinable_Again_For_Players") NET_NL()
	#ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Mission_Joinable_Again_For_Players() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Make sure the mission is the same - a pre-mission cutscene will use the same uniqueID as the mission but should be ignored
	IF NOT (theEventData.missionData.mdID.idMission = Get_MP_Mission_Request_MissionID(missionSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........IGNORING - the missions are different, this is probably the pre-mission cutscene, not the mission") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Set the 'players Can Join' flag for this Slot (used for missions that aren't team-based)
	Store_MP_Mission_Request_Option_Players_Can_Join_Mission(missionSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting players as joinable again for slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Mission Ready For Secondary Teams
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event from a Mission that requests all secondary teams join the mission.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
//
// NOTES:	Informs the Mission Control host that the mission is ready for the secondary teams.
PROC Process_Event_Mission_Ready_For_Secondary_Teams(INT paramEventID)
	
	// Retrieve the data associated with the event
	m_structEventMissionReadyForSecondaryTeams theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Mission_Ready_For_Secondary_Teams(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Ready_For_Secondary_Teams - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF

		// To help recover should this event get lost due to Host Migration, non-hosts on the mission should store this event so it can be re-issued by someone if required
		IF (Is_Player_Actively_On_This_Mission(theEventData.missionData.mdID.idMission))
			#IF IS_DEBUG_BUILD
				 NET_PRINT("          - but player is on this mission so storing the event on the resync array - just in case") NET_NL()
			#ENDIF
			
			Store_MC_Broadcast_On_Resync_Storage(SCRIPT_EVENT_MISSION_READY_FOR_SECONDARY_TEAMS, theEventData.missionData.mdUniqueID)
		ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Mission_Ready_For_Secondary_Teams") NET_NL()
	#ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		NET_PRINT("           ")
		Debug_Output_Basic_MP_Mission_Details_As_A_Line_Segment(theEventData.missionData)
		NET_NL()
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.missionData.mdUniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Mission_Ready_For_Secondary_Teams() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Ensure the mission is classed as Active - Secondary Players shouldn't be added to any other type of mission request
	IF NOT (Is_MP_Mission_Request_Slot_Status_Active(missionSlot))
	AND NOT (Is_MP_Mission_Request_Slot_Status_Active_Add_Secondary_Players(missionSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Ready_For_Secondary_Teams(). IGNORING REQUEST: Mission is not active in slot: ") NET_PRINT_INT(missionSlot) NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
		#ENDIF
		
		EXIT
	ENDIF
	
	// Check if the Secondary Players have already been added for this Mission Request
	IF (Is_MP_Mission_Request_Slot_Status_Active_Add_Secondary_Players(missionSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Mission_Ready_For_Secondary_Teams(). IGNORING REQUEST: Mission is already adding Secondary Players for mission in slot: ") NET_PRINT_INT(missionSlot) NET_NL()
			Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
		#ENDIF
		
		EXIT
	ENDIF
	
	// Get the Secondary Teams onto the mission
	// Set all Reserved Teams to become Joinable
	INT reservedTeamsBitfield = Get_MP_Mission_Request_Reserved_Teams(missionSlot)
				
	// Update the Joinable List of teams to match the Reserved List of teams
	#IF IS_DEBUG_BUILD
		NET_PRINT("      All Reserved Teams are now added to the Joinable Teams list.") NET_NL()
	#ENDIF
	
	Replace_MP_Mission_Request_Joinable_Teams_Bitfield(missionSlot, reservedTeamsBitfield)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      The Player Details after the Secondary Teams added.") NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF
	
	// Also set the 'players Can Join' flag for this Slot (used for missions that aren't team-based)
	// NOTE: The assumption for now is that if a mission is requesting secondary players, then players can join it
	IF NOT (Check_If_Players_Are_In_Teams_For_Mission(Get_MP_Mission_Request_MissionID(missionSlot)))
		Store_MP_Mission_Request_Option_Players_Can_Join_Mission(missionSlot)
	ENDIF
	
	Store_MP_Mission_Request_Status_As_Active_Add_Secondary_Players(missionSlot)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Force Player From Mission Request
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Force a player to quit the mission request with the specified uniqueID
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Force_Player_From_Mission_Request(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Force_Player_From_Mission_Request - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Force_Player_From_Mission_Request") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventForcePlayerFromMissionRequest theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Force_Player_From_Mission_Request(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
		NET_PRINT("           UniqueID    : ")
		NET_PRINT_INT(theEventData.uniqueID)
		NET_NL()
	#ENDIF
	
	// Ensure player still OK
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Player is not OK") NET_NL()
		#ENDIF
		
		// Player is not OK, but don't need to send a reply
		EXIT
	ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("           BUT: Failed To Find Mission With This UniqueID") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Remove the player from the mission request and broadcast Failed and Finished to help cleanup any calling function
	Clear_Player_As_Active_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Clear_Player_As_Confirmed_For_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Clear_Player_As_Joining_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Remove_Player_From_All_Reserved_Bitfields_On_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	Broadcast_Mission_Request_Finished_To_Specific_Player(missionSlot, theEventData.Details.FromPlayerIndex)
	Broadcast_Mission_Request_Failed_To_Specific_Player_For_UniqueID(theEventData.uniqueID, theEventData.Details.FromPlayerIndex)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Force Player Quit PreMission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Force a player to quit mission triggering if still in PreMission phase
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Force_Player_Quit_PreMission(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MControl]: Process_Event_Force_Player_Quit_PreMission received by: ") NET_PRINT(GET_PLAYER_NAME(PLAYER_ID())) NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventForcePlayerQuitPreMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Force_Player_Quit_PreMission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("                      Request From: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_NL()
	#ENDIF

	// Pass the request to the Mission Triggering routines
	Force_Player_To_Quit_If_In_PreMission_Phase(theEventData.uniqueID)

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Corona Matchmaking Closed
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event that marks a mission request as closed to Walk-Ins (but still accepts invites).
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Corona_Matchmaking_Closed(INT paramEventID)
	
	// Retrieve the data associated with the event
	m_structEventMissionMatchmakingClosed theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Corona_Matchmaking_Closed(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Corona_Matchmaking_Closed - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Corona_Matchmaking_Closed") NET_NL()
	#ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
		NET_PRINT("           UniqueID    : ")
		NET_PRINT_INT(theEventData.uniqueID)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Corona_Matchmaking_Closed() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Clear the 'players Can Join' flag for this Slot (used for missions that aren't team-based)
	Store_MP_Mission_Request_Option_Closed_To_WalkIns(missionSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting Matchmaking closed (so prevent walk-ins, allow invitees) for slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Corona Matchmaking Open
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event that marks a mission request as open to Walk-Ins.
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Corona_Matchmaking_Open(INT paramEventID)
	
	// Retrieve the data associated with the event
	m_structEventMissionMatchmakingOpen theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Corona_Matchmaking_open(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Corona_Matchmaking_open - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Corona_Matchmaking_open") NET_NL()
	#ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
		NET_PRINT("           UniqueID    : ")
		NET_PRINT_INT(theEventData.uniqueID)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Corona_Matchmaking_open() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Clear the 'players Can Join' flag for this Slot (used for missions that aren't team-based)
	Store_MP_Mission_Request_Option_Open_To_WalkIns(missionSlot)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Setting Matchmaking closed (so prevent walk-ins, allow invitees) for slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Event: Exclude Player From Current Mission
//		(The Process routine only - the Broadcast routine is issued by the server in net_mission_trigger.sch)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the event that excludes the player (so that a kicked player can't walk back into a corona).
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Exclude_Player_From_Current_Mission(INT paramEventID)
	
	// Retrieve the data associated with the event
	m_structEventExcludePlayerFromCurrentMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Exclude_Player_From_Current_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MControl]: Process_Event_Exclude_Player_From_Current_Mission - IGNORED (not the host)") NET_NL()
			NET_PRINT("          ") NET_PRINT_TIME() NET_PRINT(" [MC Host is: ") Print_MC_Host_Name_To_Console_Log() NET_PRINT("]") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MControl]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Exclude_Player_From_Current_Mission") NET_NL()
	#ENDIF
	
	// Output some details to the console log
	#IF IS_DEBUG_BUILD
		IF (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
			NET_PRINT("           Request From: ")
			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
			NET_NL()
		ENDIF
		NET_PRINT("           UniqueID    : ")
		NET_PRINT_INT(theEventData.uniqueID)
		NET_NL()
	#ENDIF
	
	// Find the slot containing this mission data
	INT missionSlot = Find_Slot_Containing_This_MP_Mission(theEventData.uniqueID)
	IF (missionSlot = FAILED_TO_FIND_MISSION)
		// The Mission must have been cleaned up in the previous few frames
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Process_Event_Exclude_Player_From_Current_Mission() - FAILED TO FIND MISSION") NET_NL()
			Debug_Output_Details_Of_All_MP_Mission_Request_Slots_To_Console_Log()
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Found Mission Details In Slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Details_Of_One_MP_Mission_Request_Slot_To_Console_Log(missionSlot)
	#ENDIF
	
	// Set the 'ewxclude player' flag for this Slot (used for missions that aren't team-based)
	Set_Player_As_Excluded_From_MP_Mission_Request_Slot(missionSlot, theEventData.Details.FromPlayerIndex)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Excluding Player From Mission (so prevent walk-in if the player got kicked) for slot: ") NET_PRINT_INT(missionSlot) NET_NL()
		Debug_Output_Missions_Player_Details(missionSlot)
	#ENDIF

ENDPROC




