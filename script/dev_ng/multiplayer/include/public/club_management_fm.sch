///    Author:		Orlando C-H
///    Team:		Online Tech
///    Description:	Header to be used by FM script to manage the popularity of the player's club business.
///    				Includes some functions to help with the Nightclub Safe cash collection & popularity.


USING "net_stat_system.sch"
USING "script_maths.sch"
USING "net_realty_nightclub.sch"
USING "net_ticker_logic.sch"
USING "net_common_functions.sch"
USING "net_gang_boss.sch"

CONST_INT ciCLUB_PAYOUT_INTERVAL_MS 		1000 * 60 * 48 //Play time between payouts (milliseconds)
CONST_INT ciCLUB_STAT_SYNC_INTERVAL_MS		1000 * 15 //How often to update the stat timer (milliseconds). We don't just update the stat every frame in order to save comp power

CONST_INT ciCLUB_HOTSPOT_STAT_SYNC_INTERVAL_MS	1000 * 15	// Sync every 15 secs
CONST_INT ciCLUB_HOTSPOT_STAT_SAVE_INTERVAL_MS	60000 * 2 	// Request save for stat every 2 mins

//Player can only access the safe once per day
CONST_INT ciCLUB_TIME_BETWEEN_SAFE_ACCESS_SECONDS  	60 * 48
//Delay help text from showing again for some times...
//...after collecting money 
CONST_INT ciCLUB_TIME_LEFT_BEFORE_WARN_HELP_SECONDS	ciCLUB_TIME_BETWEEN_SAFE_ACCESS_SECONDS - 10
CONST_INT ciCLUB_BS_WARNING_HELP_SHOWING 		0
CONST_INT ciCLUB_BS_START_CASH_COLLECTION		1
CONST_INT ciCLUB_BS_PROCESSING_CASH_COLLECTION 	2
CONST_INT ciCLUB_BS_SAFE_DOOR_ANIM_RUNNING	 	3
CONST_INT ciCLUB_BS_SAFE_DOOR_OPEN				4
CONST_INT ciCLUB_BS_SAFE_DOOR_NET_RESERVE		5
CONST_INT ciCLUB_BS_SAFE_DOOR_NET_CREATED		6
CONST_INT ciCLUB_BS_TRANSACTION_IN_PROGRESS		7
CONST_INT ciCLUB_BS_CACHED_SAFE_CASH_VALUE		8

ENUM CLUB_PAYOUT_COLLECTION_STAGE
	CPCS_INIT,
	CPCS_IDLE,
	CPCS_ANIM,
	CPCS_PAYOUT
ENDENUM

ENUM CLUB_PAYOUT_COLLECTION_ANIM_STAGE
	CPCAS_INIT = 0,
	CPCAS_GET_DATA,
	CPCAS_DETERMINE_ANIM_CLIP,
	CPCAS_RUNNING,
	CPCAS_CLEANUP
ENDENUM

STRUCT CLUB_PAYOUT_COLLECTION_SAFE_DOOR_STRUCT
	INT iNetSceneID
	FLOAT fPlayerHeading
	STRING sAnimDict
	VECTOR vAnimLoc
	VECTOR vAnimRot
	BOOL bSafeDoorCreated
	NETWORK_INDEX niSafeDoor
	CLUB_PAYOUT_COLLECTION_ANIM_STAGE eAnimDoorStage = CPCAS_INIT
ENDSTRUCT

STRUCT CLUB_PAYOUT_COLLECTION_STRUCT
	INT	iBs
	INT iTransactionStage = 0
	INT iCachedSafeCashValue = 0
	INT iContext = NEW_CONTEXT_INTENTION
	CLUB_PAYOUT_COLLECTION_STAGE eStage
	CLUB_PAYOUT_COLLECTION_SAFE_DOOR_STRUCT sSafeDoor
	CONTRABAND_TRANSACTION_STATE eTransactionResult
	SCRIPT_TIMER stDoorAnimSafetyTimer
	SCRIPT_TIMER stHideInteriorSafeDoorTimer
	PARTICIPANT_INDEX piScriptHost
ENDSTRUCT

/// PURPOSE:
///    This transaction must take place before adding or removing "contraband" (cash in safe) from inventory.
FUNC BOOL PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE(CONTRABAND_TRANSACTION_STATE &eResult, BOOL bIsAddTransaction)
	/*
		AddContrabandMission: Starting new mission
		0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
	*/
	
	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	INT iMissionKey
	IF bIsAddTransaction
		iMissionKey = HASH("CLUB_CONTRABAND_MISSION_ADD_v0")
	ELSE
		iMissionKey = HASH("CLUB_CONTRABAND_MISSION_REMOVE_v0")
	ENDIF
		
	SWITCH eResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
		
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
			OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("appInternet")) > 0
			OR g_sShopSettings.iCount_BrowsingInShop > 0
			OR g_BusinessHubTransactionSlot != BUSINESS_TYPE_INVALID
			OR g_ProductionTimestamp_slot != -1
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE - Transaction waiting Index: ", GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				RETURN FALSE
			ENDIF
		
			INT iInventoryKey
			iInventoryKey = HASH("CLUB_CONTRABAND_MISSION_v0")
			
			//0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_CONTRABAND_MISSION, iMissionKey, NET_SHOP_ACTION_BUY_CONTRABAND_MISSION, 1, 0, 1, CATALOG_ITEM_FLAG_WALLET_THEN_BANK, iInventoryKey)
				eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE - Failed to add mission item")
			ENDIF
			
			IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE - Starting basket checkout")
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE - Failed to start checkout")
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE - Transaction finished - SUCCESS")
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
								
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_MISSION_KEY_FOR_NIGHTCLUB_SAFE - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING)
ENDFUNC

/// PURPOSE:
///   Returns true when the transaction is complete, check eResult for the result.
///   This function adds iQuantity "contraband" (cash to safe). PC Only.
FUNC BOOL PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE(INT iQuantity, CONTRABAND_TRANSACTION_STATE &eResult)
	PRINTLN("[CLUB_CASH] PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE ... (start) CONTRABAND_TRANSACTION_STATE == ", ENUM_TO_INT(eResult))
	
	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
			
				PRINTLN("[CLUB_CASH] PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE ... Hold up for existing basket transaction")
				RETURN FALSE
			ENDIF			
			
			INT iQuantityKey
			iQuantityKey = HASH("NIGHTCLUB_SAFE_CASH_v0")
			
			//0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iQuantityKey, NET_SHOP_ACTION_UPDATE_BUSINESS_GOODS, iQuantity, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
				eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CDEBUG1LN(DEBUG_SHOPS, "PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE - Failed to add mission item")
			ENDIF
			
			IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
				CDEBUG1LN(DEBUG_SHOPS, "PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE - Starting basket checkout")
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CDEBUG1LN(DEBUG_SHOPS, "PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE - Failed to start checkout")
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
				CDEBUG1LN(DEBUG_SHOPS, "PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE - Transaction failed - Lost ref to basket index")
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
			ELIF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CDEBUG1LN(DEBUG_SHOPS, "PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE - Transaction finished - SUCCESS")
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ELSE
					CDEBUG1LN(DEBUG_SHOPS, "PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[CLUB_CASH] PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE ... (end) CONTRABAND_TRANSACTION_STATE == ", ENUM_TO_INT(eResult))
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING)
ENDFUNC

PROC CLEAR_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(CLUB_PAYOUT_COLLECTION_STRUCT &ref_data)
	IF IS_BIT_SET(ref_data.iBs, ciCLUB_BS_CACHED_SAFE_CASH_VALUE)
		CLEAR_BIT(ref_data.iBs, ciCLUB_BS_CACHED_SAFE_CASH_VALUE)
	ENDIF
	ref_data.iCachedSafeCashValue = 0
	CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - CLEAR_NIGHTCLUB_CACHED_SAFE_CASH_VALUE - Clearing Caching Safe Cash")
ENDPROC

PROC SET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(CLUB_PAYOUT_COLLECTION_STRUCT &ref_data)
	IF NOT IS_BIT_SET(ref_data.iBs, ciCLUB_BS_CACHED_SAFE_CASH_VALUE)
		ref_data.iCachedSafeCashValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
		
		#IF IS_DEBUG_BUILD
		IF g_iDebugNightclubSafeCashValue != -1
			ref_data.iCachedSafeCashValue = g_iDebugNightclubSafeCashValue
			CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - SET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE - *USING DEBUG VALUE*: ", g_iDebugNightclubSafeCashValue)
		ENDIF
		#ENDIF
		
		CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - SET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE - Caching Safe Cash: ", ref_data.iCachedSafeCashValue)
		SET_BIT(ref_data.iBs, ciCLUB_BS_CACHED_SAFE_CASH_VALUE)
	ENDIF
ENDPROC

FUNC INT GET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(CLUB_PAYOUT_COLLECTION_STRUCT &ref_data)
	RETURN ref_data.iCachedSafeCashValue
ENDFUNC

/// PURPOSE:
///   Returns true when the transaction is complete, check eResult for the result.
///   This function removed all "contraband" (cash from safe) and gives the player
///   that much money. PC Only.
FUNC BOOL PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH(CONTRABAND_TRANSACTION_STATE &eResult, CLUB_PAYOUT_COLLECTION_STRUCT &ref_data)	
	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			SET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(ref_data)
			
			INT iInventoryKey, iMissionKey, iQuantityKey, iFlagKey, iEarnValue
			iFlagKey		= HASH("CF_MISSION_PASSED_CLB") 
			iInventoryKey 	= HASH("CLUB_CONTRABAND_MISSION_v0")
			iQuantityKey 	= HASH("NIGHTCLUB_SAFE_CASH_v0")
			iMissionKey 	= HASH("CLUB_CONTRABAND_MISSION_REMOVE_v0")
			iEarnValue 		= GET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(ref_data)
			
			CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - iEarnValue: ", iEarnValue)
			
			//0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_CONTRABAND_MISSION, iMissionKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, 1, 0, 1, CATALOG_ITEM_FLAG_WALLET_ONLY, iInventoryKey)
				//1:  CATEGORY_CONTRABAND_QNTY, <quantity>
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iQuantityKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, iEarnValue, 0, 1, CATALOG_ITEM_FLAG_WALLET_ONLY) //Remove iEarnValue so that safe value = 0
					//2:  MISSION_PASSED, CATEGORY_CONTRABAND_QNTY
					IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_FLAGS, iFlagKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, 1, iEarnValue, 0, CATALOG_ITEM_FLAG_WALLET_ONLY, iQuantityKey)
						eResult = CONTRABAND_TRANSACTION_STATE_PENDING
					ELSE
						eResult = CONTRABAND_TRANSACTION_STATE_FAILED
						CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Failed to add flag item")
					ENDIF
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Failed to add quantity item")
				ENDIF
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Failed to add mission item")
			ENDIF
			
			IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Starting basket checkout")
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Failed to start checkout")
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Transaction finished - SUCCESS")
					
					iEarnValue = GET_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(ref_data)
					
					IF iEarnValue > 0
						PRINTLN("[CLUB_CASH] PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH Tarans id: ", GET_BASKET_TRANSACTION_SCRIPT_INDEX(), " - Telem - iEarnValue = ", iEarnValue)
						NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
						NETWORK_EARN_NIGHTCLUB(iEarnValue)
						PLAY_SOUND_FRONTEND(-1, "WEAPON_PURCHASE", "HUD_AMMO_SHOP_SOUNDSET")
						
						GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iNightclubSafeCash = 0
					ENDIF
					
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CLEAR_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(ref_data)
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_COLLECT_NIGHTCLUB_SAFE_CASH - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CLEAR_NIGHTCLUB_CACHED_SAFE_CASH_VALUE(ref_data)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING)
ENDFUNC

/// PURPOSE: Struct to keep track of time for
///    updating the stats related to the player's nightclub.
STRUCT CLUB_POPULARITY_STRUCT
	INT iLastSyncTime 	//Last time we updated the stat timer (uses game timer)
	CONTRABAND_TRANSACTION_STATE 	eTransactionState	
	INT iTransactionValue 
	
	//Widget data 
	#IF IS_DEBUG_BUILD
	BOOL bHaveInitWidgets
	BOOL bForceUpdateTimer
	BOOL bSetClubPopularity
	BOOL bSetCashInSafe
	BOOL bRefresh
	BOOL bAutoRefresh
	BOOL bIgnoreOwnershipCheck
	BOOL bDisplayKeyStats
	BOOL bUpdateWidgetScreenCoords
	
	INT iTimeLeftUntilPayout
	INT iCashInSafe
	INT iLastKnownPopularity
	INT iLastKnownTimeLeft
	INT iLastKnownSafeCash
	INT iTimeLeftTimer
	INT iSafeCashStatValue
	INT iSafeCashBDValue
	
	FLOAT fNewClubPopularity
	FLOAT fScreenX = -1.0
	FLOAT fScreenY = -1.0
	#ENDIF //IS_DEBUG_BUILD
ENDSTRUCT

STRUCT CLUB_HOTSPOT_TROPHY_STRUCT
	INT iLastSyncTime
	INT iLastSaveTime
ENDSTRUCT

PROC MAINTAIN_NIGHTCLUB_HOTSPOT_TROPHY_TIMER(CLUB_HOTSPOT_TROPHY_STRUCT& sHotspotData)
	
	// Timer ticks down from 14 game days, saving progression/milestones
	// Gold Trophy 		= 14 days in a row with 90% or more club popularity
	// Silver Trophy 	= 7 days in a row with 90% or more club popularity
	// Bronze Trophy 	= 3 days in a row with 90% or more club popularity
	
	// Player drops below 90% popularity: timer reset and start again
	
	IF NOT DOES_PLAYER_OWN_A_NIGHTCLUB(PLAYER_ID())
	OR NOT HAS_PLAYER_COMPLETED_ALL_NIGHTCLUB_SETUP_MISSIONS(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT IS_FREEMODE()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		EXIT
	ENDIF
	
	// Player has unlocked gold hotspot trophy - no need to run timers
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_GOLD)
		EXIT
	ENDIF
	
	INT iGameTime = GET_GAME_TIMER()
	INT iHotspot14DayIntervalMS = (60000 * g_sMPTunables.iNightclubHotspotTrophyMinDayLength) * 14
	INT iHotspot7DayIntervalMS	= (60000 * g_sMPTunables.iNightclubHotspotTrophyMinDayLength) * 7
	INT iHotspot3DayIntervalMS	= (60000 * g_sMPTunables.iNightclubHotspotTrophyMinDayLength) * 3
	
	IF GET_LOCAL_CLUB_POPULARITY() >= 90
		
		//If script loaded/reloaded
		IF sHotspotData.iLastSyncTime = 0
			sHotspotData.iLastSyncTime = iGameTime
			EXIT
		ENDIF
		IF sHotspotData.iLastSaveTime = 0
			sHotspotData.iLastSaveTime = iGameTime
			EXIT
		ENDIF
		
		INT iSavedTimeLeft = GET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS)
		//If the timer stat has never been set before then we should set the time left to MAX...
		//...After this, there is no way it can = exactly 0 again.
		IF iSavedTimeLeft = 0
			PRINTLN("[CLUB_TROPHY] HOTSPOT - Setting hotspot timer for first time. Time remaining == ", iHotspot14DayIntervalMS)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS, iHotspot14DayIntervalMS)
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
			PRINTLN("[CLUB_TROPHY] HOTSPOT - Finished setting hotspot timer.")
			EXIT
		ENDIF
		
		INT iTimeSinceLastSave = iGameTime - sHotspotData.iLastSaveTime
		INT iTimeSinceLastSync = iGameTime - sHotspotData.iLastSyncTime
		INT iTimeLeft = iSavedTimeLeft - iTimeSinceLastSync
		
		// Full 14 ingame days complete
		IF iTimeLeft <= 0
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_GOLD)
				PRINTLN("[CLUB_TROPHY] HOTSPOT - Player has unlocked gold hotspot trophy!")
				
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_GOLD, TRUE)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iBSNightclub2, PROPERTY_BROADCAST_BS_NIGHTCLUB_HOTSPOT_TROPHY_GOLD)
				
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS, 0)
				sHotspotData.iLastSyncTime = iGameTime
				sHotspotData.iLastSaveTime = iGameTime
				
				SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
			ENDIF
			
		// 7 ingame days complete
		ELIF iTimeLeft <= iHotspot7DayIntervalMS
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_SILVER)
				PRINTLN("[CLUB_TROPHY] HOTSPOT - Player has unlocked silver hotspot trophy!")
				
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_SILVER, TRUE)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iBSNightclub2, PROPERTY_BROADCAST_BS_NIGHTCLUB_HOTSPOT_TROPHY_SILVER)
				
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS, iTimeLeft)
				sHotspotData.iLastSyncTime = iGameTime
				sHotspotData.iLastSaveTime = iGameTime
				
				SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
			ENDIF
			
		// 3 ingame days complete
		ELIF iTimeLeft <= (iHotspot14DayIntervalMS-iHotspot3DayIntervalMS)
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_BRONZE)
				PRINTLN("[CLUB_TROPHY] HOTSPOT - Player has unlocked bronze hotspot trophy!")
				
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_HOTSPOT_TROPHY_BRONZE, TRUE)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iBSNightclub2, PROPERTY_BROADCAST_BS_NIGHTCLUB_HOTSPOT_TROPHY_BRONZE)
				
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS, iTimeLeft)
				sHotspotData.iLastSyncTime = iGameTime
				sHotspotData.iLastSaveTime = iGameTime
				
				SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
			ENDIF
		ENDIF
		
		// Sync timer
		IF iTimeSinceLastSync >= ciCLUB_HOTSPOT_STAT_SYNC_INTERVAL_MS
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS, iTimeLeft)
			sHotspotData.iLastSyncTime = iGameTime
			PRINTLN("[CLUB_TROPHY] HOTSPOT - Updating hotspot time stat. ", iTimeLeft, " ms left before payout.")
		ENDIF
		
		// Save timer
		IF iTimeSinceLastSave >= ciCLUB_HOTSPOT_STAT_SAVE_INTERVAL_MS
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
			sHotspotData.iLastSaveTime = iGameTime
			PRINTLN("[CLUB_TROPHY] HOTSPOT - Requesting save for hotspot trophy timer stat.")
		ENDIF
		
	ELSE
		// Reset timer
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS) != iHotspot14DayIntervalMS
			PRINTLN("[CLUB_TROPHY] HOTSPOT - Players nightclub popularity has dropped below 90%! Resetting hotspot timer to 14 ingame days")
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_HOTSPOT_TIME_MS, iHotspot14DayIntervalMS)
			sHotspotData.iLastSyncTime = iGameTime
			sHotspotData.iLastSaveTime = iGameTime
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_NIGHTCLUB_TROPHY_TICKER_FEED()
	
	IF NOT DOES_PLAYER_OWN_A_NIGHTCLUB(PLAYER_ID())
	OR NOT HAS_PLAYER_COMPLETED_ALL_NIGHTCLUB_SETUP_MISSIONS(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT IS_FREEMODE()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		EXIT
	ENDIF
	
	// Dancer Trophy
	IF GET_NIGHTCLUB_TOTAL_DANCING_TIME_MS(PLAYER_ID()) >= g_sMPTunables.iNightclubDancerTrophyGoldMins
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GOLD_DANCER_TROPHY_TEXT)
			PRINT_TICKER("NC_GLD_DNCR")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GOLD_DANCER_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Dancer Gold Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ELIF GET_NIGHTCLUB_TOTAL_DANCING_TIME_MS(PLAYER_ID()) >= g_sMPTunables.iNightclubDancerTrophySilverMins
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SILVER_DANCER_TROPHY_TEXT)
			PRINT_TICKER("NC_SIL_DNCR")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SILVER_DANCER_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Dancer Silver Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ELIF GET_NIGHTCLUB_TOTAL_DANCING_TIME_MS(PLAYER_ID()) >= g_sMPTunables.iNightclubDancerTrophyBronzeMins
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRONZE_DANCER_TROPHY_TEXT)
			PRINT_TICKER("NC_BRZ_DNCR")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRONZE_DANCER_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Dancer Bronze Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ENDIF
	
	// Battler Trophy
	IF GET_NIGHTCLUB_TOTAL_BUSINESS_BATTLE_WINS(PLAYER_ID()) >= 20
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GOLD_BATTLER_TROPHY_TEXT)
			PRINT_TICKER("NC_GLD_BTLR")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GOLD_BATTLER_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Battler Gold Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ELIF GET_NIGHTCLUB_TOTAL_BUSINESS_BATTLE_WINS(PLAYER_ID()) >= 10
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SILVER_BATTLER_TROPHY_TEXT)
			PRINT_TICKER("NC_SIL_BTLR")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SILVER_BATTLER_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Battler Silver Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ELIF GET_NIGHTCLUB_TOTAL_BUSINESS_BATTLE_WINS(PLAYER_ID()) >= 3
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRONZE_BATTLER_TROPHY_TEXT)
			PRINT_TICKER("NC_BRZ_BTLR")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRONZE_BATTLER_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Battler Bronze Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ENDIF
	
	// Hotspot Trophy
	IF GET_NIGHTCLUB_HOTSPOT_TROPHY_STATE(PLAYER_ID()) = 3
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GOLD_HOTSPOT_TROPHY_TEXT)
			PRINT_TICKER("NC_GLD_HTSP")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GOLD_HOTSPOT_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Hotspot Gold Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ELIF GET_NIGHTCLUB_HOTSPOT_TROPHY_STATE(PLAYER_ID()) = 2
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SILVER_HOTSPOT_TROPHY_TEXT)
			PRINT_TICKER("NC_SIL_HTSP")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SILVER_HOTSPOT_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Hotspot Silver Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ELIF GET_NIGHTCLUB_HOTSPOT_TROPHY_STATE(PLAYER_ID()) = 1
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRONZE_HOTSPOT_TROPHY_TEXT)
			PRINT_TICKER("NC_BRZ_HTSP")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRONZE_HOTSPOT_TROPHY_TEXT, TRUE)
			PRINTLN("[CLUB_TROPHY] Player has unlocked the Hotspot Bronze Trophy.")
			SET_BIT(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_NCLUB_REQUEST_SAVE_FOR_NCLUB_AWARD)
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_NIGHTCLUB_TRANSACTION_STAGE(NIGHTCLUB_TRANSACTION_STAGE eNewStage)
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[CLUB_CASH] Setting g_eNightclubTransactionStage from ",g_eNightclubTransactionStage," to ",eNewStage)	
	g_eNightclubTransactionStage = eNewStage
ENDPROC

/// PURPOSE:
///    Trigger a transaction to add money to the nightclub safe. PC only.
PROC _CLUB_TRIGGER_STORE_CASH_TRANSACTION(CLUB_POPULARITY_STRUCT& ref_clubData, INT iValue)
	ref_clubData.iTransactionValue = iValue
	SET_NIGHTCLUB_TRANSACTION_STAGE(NTS_PENDING_CONTRABAND)	
ENDPROC

FUNC BOOL _CLUB_IS_SOTRE_CASH_TRANSACTION_IN_PROCESS()
	RETURN g_eNightclubTransactionStage <> NTS_NO_TRANSACTION
ENDFUNC

#IF IS_DEBUG_BUILD
PROC _CLUB_DEBUG_DISPLAY_KEY_STATS(CLUB_POPULARITY_STRUCT& ref_clubData)
	
	FLOAT fCentreX = 0.0
	FLOAT fCentreY = 0.0
	
	IF ref_clubData.fScreenX != -1.0
	AND ref_clubData.fScreenY != -1.0
		fCentreX = ref_clubData.fScreenX - 0.134
		fCentreY = ref_clubData.fScreenY - 0.134
	ENDIF
	
	//Background rectangle
	DRAW_RECT(fCentreX+0.138, fCentreY+0.145, 0.230, 0.225, 0, 0, 0, 200)
	
	// Nightclub Main Heading
    SET_TEXT_SCALE(0.5, 0.5)
	SET_TEXT_COLOUR(0, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.025, fCentreY+0.033, "STRING", "Nightclub")
	
	// Popularity Row Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.025, fCentreY+0.103, "STRING", "Popularity")
	
	// Safe Cash Row Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.025, fCentreY+0.143, "STRING", "Safe Cash")
	
	// Time Until Payout Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.025, fCentreY+0.223, "STRING", "Time Until Payout")
	
	// Total Column Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.110, fCentreY+0.075, "STRING", "Totals")
	
	// Value Column Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.182, fCentreY+0.075, "STRING", "Values")
	
	// Ms Column Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.135, fCentreY+0.200, "STRING", "Ms")
	
	// S Column Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.192, fCentreY+0.200, "STRING", "S")
	
	// Min Column Heading
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.230, fCentreY+0.200, "STRING", "Min")
	
	
	
	// Background Bar Colour
	INT iGreyR, iGreyG, iGreyB, iGreyA
	GET_HUD_COLOUR(HUD_COLOUR_GREYDARK, iGreyR, iGreyG, iGreyB, iGreyA)
	
	// Popularity Bar
	INT iPurpleR, iPurpleG, iPurpleB, iPurpleA
	GET_HUD_COLOUR(HUD_COLOUR_PURPLE, iPurpleR, iPurpleG, iPurpleB, iPurpleA)
	
	FLOAT fPopularityValue = GET_PLAYER_NIGHTCLUB_POPULARITY(PLAYER_ID())
	fPopularityValue /= 100.0
	
	DEBUG_DRAW_PRODUCT_BAR(1, 1, fCentreY+0.119, fCentreX+0.125, 0.075, 239, 0.005, iGreyR, iGreyG, iGreyB)
	DEBUG_DRAW_PRODUCT_BAR(1, fPopularityValue, fCentreY+0.119, fCentreX+0.125, 0.075, 239, 0.005, iPurpleR, iPurpleG, iPurpleB)
	
	// Safe Cash Bar
	INT iGreenR, iGreenG, iGreenB, iGreenA
	GET_HUD_COLOUR(HUD_COLOUR_GREEN, iGreenR, iGreenG, iGreenB, iGreenA)
	
	FLOAT fSafeCash = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
	fSafeCash /= 70000
	
	DEBUG_DRAW_PRODUCT_BAR(1, 1, fCentreY+0.156, fCentreX+0.125, 0.075, 239, 0.005, iGreyR, iGreyG, iGreyB)
	DEBUG_DRAW_PRODUCT_BAR(1, fSafeCash, fCentreY+0.156, fCentreX+0.125, 0.075, 239, 0.005, iGreenR, iGreenG, iGreenB)
	
	
	
	// Popularity Value
	TEXT_LABEL_15 tlPopularityValue = ""
	tlPopularityValue += FLOAT_TO_STRING(GET_PLAYER_NIGHTCLUB_POPULARITY(PLAYER_ID()), 1)
	tlPopularityValue += "/100"
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.182, fCentreY+0.105, "STRING", tlPopularityValue)
	
	// Safe Cash Value
	TEXT_LABEL_15 tlCashValue = ""
	tlCashValue += GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
	tlCashValue += "/70000"
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.175, fCentreY+0.143, "STRING", tlCashValue)
	
	
	
	// Payout Timer Value - Milliseconds
	TEXT_LABEL_15 tlMSTimeValue = ""
	tlMSTimeValue += ref_clubData.iTimeLeftTimer
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.121, fCentreY+0.223, "STRING", tlMSTimeValue)
	
	// Payout Timer Value - Seconds
	INT iSecondsPayoutTimer = ref_clubData.iTimeLeftTimer/1000
	
	TEXT_LABEL_15 tlSTimeValue = ""
	tlSTimeValue += iSecondsPayoutTimer
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.185, fCentreY+0.223, "STRING", tlSTimeValue)
	
	// Payout Timer Value - Minutes
	INT iMinutesPayoutTimer = ref_clubData.iTimeLeftTimer/1000
	iMinutesPayoutTimer /= 60
	
	TEXT_LABEL_15 tlMinTimeValue = ""
	tlMinTimeValue += iMinutesPayoutTimer
	
	SET_TEXT_SCALE(0.3, 0.3)
	SET_TEXT_COLOUR(255, 255, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING(fCentreX+0.233, fCentreY+0.223, "STRING", tlMinTimeValue)
ENDPROC

/// PURPOSE:
///    PRIVATE - Call to update the widgets for club popularity / safe cash.
PROC _CLUB_MAINTAIN_WIDGETS(CLUB_POPULARITY_STRUCT& ref_clubData)
	IF ref_clubData.bHaveInitWidgets
		IF ref_clubData.bUpdateWidgetScreenCoords
			IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
			OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			OR IS_PLAYSTATION_PLATFORM()
				IF IS_XBOX_PLATFORM()
					GET_MOUSE_POSITION(ref_clubData.fScreenX, ref_clubData.fScreenY)
				ELSE
					ref_clubData.fScreenX = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X)
					ref_clubData.fScreenY = GET_DISABLED_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
				ENDIF
			ENDIF
		ENDIF
		IF ref_clubData.bDisplayKeyStats
			_CLUB_DEBUG_DISPLAY_KEY_STATS(ref_clubData)
		ENDIF
		
		IF ref_clubData.bForceUpdateTimer
			ref_clubData.bForceUpdateTimer = FALSE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT, ref_clubData.iTimeLeftUntilPayout)
			PRINTLN("[CLUB_CASH][WIDGET] Setting time until payout stat = ", ref_clubData.iTimeLeftUntilPayout)
			PRINTLN("[CLUB_CASH][WIDGET] GET_MP_INT_CHARACTER_STAT(...) ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT))
			ref_clubData.iLastSyncTime = GET_GAME_TIMER()
		ENDIF 
		IF ref_clubData.bSetClubPopularity
			ref_clubData.bSetClubPopularity = FALSE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_POPULARITY, PERCENTAGE_TO_POP(ref_clubData.fNewClubPopularity))
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.fNightclubPopularity = GET_LOCAL_CLUB_POPULARITY()
			PRINTLN("[CLUB_CASH][WIDGET] Setting club popularity to ", ref_clubdata.fNewClubPopularity)
		ENDIF
		IF ref_clubData.bSetCashInSafe
			ref_clubData.bSetCashInSafe = FALSE
			
			IF USE_SERVER_TRANSACTIONS()
				INT iCashToModify
				INT iCurrentSafeCash = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
				
				IF ref_clubData.iCashInSafe = iCurrentSafeCash
					EXIT // We already have cash amount set
					
				ELIF ref_clubData.iCashInSafe > iCurrentSafeCash
					iCashToModify = ref_clubData.iCashInSafe - iCurrentSafeCash
					PRINTLN("[CLUB_CASH][WIDGET] ADDING - iCashToModify ", iCashToModify)
					_CLUB_TRIGGER_STORE_CASH_TRANSACTION(ref_clubData, iCashToModify)
					
				ELIF ref_clubData.iCashInSafe < iCurrentSafeCash
					IF ref_clubData.iCashInSafe = 0
						iCashToModify = iCurrentSafeCash
					ELSE
						iCashToModify = iCurrentSafeCash - ref_clubData.iCashInSafe
					ENDIF
					
					g_iDebugNightclubSafeCashValue = iCashToModify
					PRINTLN("[CLUB_CASH][WIDGET] REMOVING - iCashToModify ", iCashToModify)
				ENDIF
			ELSE
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE, ref_clubData.iCashInSafe)
				GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iNightclubSafeCash = ref_clubData.iCashInSafe
				PRINTLN("[CLUB_CASH][WIDGET] Setting cash in safe to ", ref_clubdata.iCashInSafe)
			ENDIF
			
		ENDIF
		IF ref_clubData.bRefresh
		OR ref_clubData.bAutoRefresh
			ref_clubData.iLastKnownPopularity 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_POPULARITY)
			ref_clubData.iLastKnownTimeLeft 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT)
			ref_clubData.iLastKnownSafeCash 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
			ref_clubData.bRefresh = FALSE
		ENDIF
		
		ref_clubData.iSafeCashStatValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
		ref_clubData.iSafeCashBDValue = GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iNightclubSafeCash
	ENDIF
ENDPROC
#ENDIF //IS_DEBUG_BUILD

/// PURPOSE:
///    Process the store cash transaction (should only be called on PC)
/// PARAMS:
///    ref_clubData - 
PROC _MAINTAIN_NIGHTLCUB_STORE_CASH_TRANSACTION(CLUB_POPULARITY_STRUCT& ref_clubData)	
	IF g_eNightclubTransactionStage > NTS_NO_TRANSACTION
		IF ref_clubData.iTransactionValue <= 0
			PRINTLN("[CLUB_CASH] Nightclub safe is full : not adding daily payout.")
			
			// Once per full safe
			IF NOT IS_BIT_SET(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
				PRINT_TICKER_WITH_INT("CLUB_NO_PAY", g_sMPTunables.iNightclubMaxSafeValue)
				SET_BIT(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
			ENDIF
			
			SET_NIGHTCLUB_TRANSACTION_STAGE(NTS_NO_TRANSACTION)
		ELSE
			IF g_eNightclubTransactionStage = NTS_PENDING_CONTRABAND
				IF PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE(ref_clubData.iTransactionValue, ref_clubData.eTransactionState)
					PRINTLN("[CLUB_CASH] PROCESS_TRANSACTION_ADD_CASH_TO_NIGHTCLUB_SAFE(..) RETURNED TRUE")
					SWITCH ref_clubData.eTransactionState
						CASE CONTRABAND_TRANSACTION_STATE_FAILED
							PRINTLN("[CLUB_CASH] Add to safe transaction failed.")
						BREAK
						CASE CONTRABAND_TRANSACTION_STATE_SUCCESS
							PRINTLN("[CLUB_CASH] Add to safe transaction succeeded.")
							INCREMENT_TOTAL_NIGHTCLUB_EARNINGS(ref_clubData.iTransactionValue)
							GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iNightclubSafeCash = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
							
							IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE) = g_sMPTunables.iNightclubMaxSafeValue
								PRINT_TICKER_WITH_STRING_AND_INT("CLUB_PAY_MAX", GET_PLAYER_NIGHTCLUB_NAME_AS_TEXT_LABEL(PLAYER_ID()), GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
							ELSE
								PRINT_TICKER_WITH_STRING_AND_INT("CLUB_PAY", GET_PLAYER_NIGHTCLUB_NAME_AS_TEXT_LABEL(PLAYER_ID()), GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
							ENDIF
							
							// Clear to tell player when safe full again
							IF IS_BIT_SET(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
								CLEAR_BIT(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
							ENDIF
						BREAK
					ENDSWITCH
					ref_clubData.iTransactionValue = 0
					SET_NIGHTCLUB_TRANSACTION_STAGE(NTS_NO_TRANSACTION)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
PROC CLUB_CREATE_WIDGETS(CLUB_POPULARITY_STRUCT& ref_clubData)
	START_WIDGET_GROUP("Nightclub payouts")
		ADD_WIDGET_BOOL("Display Key Stats", ref_clubData.bDisplayKeyStats)
		ADD_WIDGET_BOOL("Allow debug display positioning", ref_clubData.bUpdateWidgetScreenCoords)
		ADD_WIDGET_STRING("To reposition the display, Enable using 'Allow debug display positioning' and follow the platform instructions below: ")
		ADD_WIDGET_STRING("PC/Xbox: click on screen where you'd like it positioned.")
		ADD_WIDGET_STRING("PS4: update the position using the touchpad.")
		ADD_WIDGET_STRING("Deselect 'Allow debug display positioning' when done")
		ADD_WIDGET_STRING("")
		
		ADD_WIDGET_INT_READ_ONLY("Accurate time left", ref_clubData.iTimeLeftTimer)
		ADD_WIDGET_INT_SLIDER("Time left", ref_clubData.iTimeLeftUntilPayout, 0, ciCLUB_PAYOUT_INTERVAL_MS, 1)
		ADD_WIDGET_BOOL("Update timer", ref_clubData.bForceUpdateTimer)
		
		ADD_WIDGET_FLOAT_SLIDER("Popularity", ref_clubData.fNewClubPopularity, 0.0, 100.0, 1.0)
		ADD_WIDGET_BOOL("Update popularity", ref_clubData.bSetClubPopularity)
		
		ADD_WIDGET_INT_SLIDER("Cash in Safe", ref_clubData.iCashInSafe, 0, g_sMPTunables.iNightclubMaxSafeValue, 1)
		ADD_WIDGET_BOOL("Update cash", ref_clubData.bSetCashInSafe)
		
		ADD_WIDGET_INT_READ_ONLY("Last sync time", ref_clubData.iLastSyncTime)	
		ADD_WIDGET_STRING("Refresh to get latest values")
		ADD_WIDGET_INT_READ_ONLY("Popularity", ref_clubData.iLastKnownPopularity)
		ADD_WIDGET_INT_READ_ONLY("Cash in safe", ref_clubData.iLastKnownSafeCash)
		ADD_WIDGET_INT_READ_ONLY("Time before pay", ref_clubData.iLastKnownTimeLeft)
		ADD_WIDGET_BOOL("Refresh", ref_clubData.bRefresh)
		ADD_WIDGET_BOOL("Auto refresh", ref_clubData.bAutoRefresh)
		ADD_WIDGET_INT_READ_ONLY("Time left timer", ref_clubData.iTimeLeftTimer)
		ADD_WIDGET_BOOL("Ignore Nightclub ownership check", ref_clubData.bIgnoreOwnershipCheck)
		
		START_WIDGET_GROUP("Cash In Safe Data")
			ADD_WIDGET_INT_READ_ONLY("Stat Value", ref_clubData.iSafeCashStatValue)
			ADD_WIDGET_INT_READ_ONLY("BD Value", ref_clubData.iSafeCashBDValue)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	ref_clubData.bHaveInitWidgets = TRUE
ENDPROC
#ENDIF //IS_DEBUG_BUILD

PROC CLUB_MAINTAIN_POPULARITY_REMINDER()
	PLAYER_INDEX piLocal = PLAYER_ID()
	IF NOT IS_PLAYER_IN_NIGHTCLUB(piLocal) // B* 4853617d
	AND DOES_PLAYER_OWN_A_NIGHTCLUB(piLocal)
	AND HAS_PLAYER_COMPLETED_ALL_NIGHTCLUB_SETUP_MISSIONS(piLocal)
	AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) // B* 4867440
	AND NOT IS_ANY_NON_BUSINESS_BATTLES_BUSINESS_CALL_SET_UP()
		FLOAT fPopularity = GET_LOCAL_CLUB_POPULARITY()
		IF fPopularity = 0 
			GB_SETUP_NIGHTCLUB_POPULARITY_ZERO_CALL()
		ELIF fPopularity < 25.0
			GB_SETUP_NIGHTCLUB_POPULARITY_LOW_CALL() 
		ELIF fPopularity < 50.0
			GB_SETUP_NIGHTCLUB_POPULARITY_HALF_CALL()
		ELIF fPopularity < 75.0
			GB_SETUP_NIGHTCLUB_POPULARITY_DROPPED_CALL()
		ENDIF	
	ELSE
		GB_CLEAR_ALL_QUEUED_NIGHTCLUB_POPULARITY_CALLS()
	ENDIF
ENDPROC

/// PURPOSE:
///    To be called from FREEMODE.sc to maintain the player's nightclub
///    popularity & cash payout, and update related character stats.
PROC CLUB_MAINTAIN_POPULARITY_PAYOUT(CLUB_POPULARITY_STRUCT& ref_clubData)	
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_LockPopularity")
		FLOAT fLockedPopularity = GET_COMMANDLINE_PARAM_FLOAT("sc_LockPopularity")
		IF GET_LOCAL_CLUB_POPULARITY() <> fLockedPopularity
			PRINTLN("[CLUB_CASH] Forcing popularity to ",fLockedPopularity," because of cmdline arg 'sc_LockPopularity'")
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_POPULARITY, PERCENTAGE_TO_POP(fLockedPopularity))
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.fNightclubPopularity = GET_LOCAL_CLUB_POPULARITY()
		ENDIF
	ENDIF

	_CLUB_MAINTAIN_WIDGETS(ref_clubData)
	#ENDIF //IS_DEBUG_BUILD
		
	IF (NOT DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB()
	OR NOT HAS_PLAYER_COMPLETED_ALL_NIGHTCLUB_SETUP_MISSIONS(PLAYER_ID()))
	#IF IS_DEBUG_BUILD
	AND NOT ref_clubData.bIgnoreOwnershipCheck
	#ENDIF
		EXIT
	ENDIF	
	
	IF NOT IS_FREEMODE()
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	OR IS_PLAYER_DANCING(PLAYER_ID())
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
	OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	OR IS_PLAYER_IN_CORONA()
		IF g_eNightclubTransactionStage != NTS_NO_TRANSACTION
			// wait - we're running a transaction
			PRINTLN("[CASH] CLUB_MAINTAIN_POPULARITY_PAYOUT - Transaction pending so do not EXIT")
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	//For PC we have to process some transactions to store cash in safe.
	IF USE_SERVER_TRANSACTIONS() 
		_MAINTAIN_NIGHTLCUB_STORE_CASH_TRANSACTION(ref_clubData)
	ENDIF
	
	INT iGameTime = GET_GAME_TIMER()
	
	//If script loaded/reloaded
	IF ref_clubData.iLastSyncTime = 0
		//Worst case scenario is the player has their payout delayed...
		//... by ciCLUB_STAT_SYNC_INTERVAL_MS milliseconds
		PRINTLN("[CLUB_CASH] Setting inital sync time : ", iGameTime)
		ref_clubData.iLastSyncTime = iGameTime
		EXIT
	ENDIF
	
	INT iSavedTimeLeft = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT)
	//If the timer stat has never been set before then we should set the time left to MAX...
	//...After this, there is no way it can = exactly 0 again.
	IF iSavedTimeLeft = 0
		PRINTLN("[CLUB_CASH] Setting payout timer for first time. Time remaining == ", ciCLUB_PAYOUT_INTERVAL_MS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT, ciCLUB_PAYOUT_INTERVAL_MS)
		PRINTLN("[CLUB_CASH] Finished setting payout timer.")
		EXIT
	ENDIF
	
	INT iTimeSinceLastSync = iGameTime - ref_clubData.iLastSyncTime
	INT iTimeLeft = iSavedTimeLeft - iTimeSinceLastSync
	
	#IF IS_DEBUG_BUILD
	ref_clubData.iTimeLeftTimer = iTimeLeft
	#ENDIF
	
	IF NOT g_bDISABLE_ResetCashNightclubSafe
	AND USE_SERVER_TRANSACTIONS()
	AND NOT _CLUB_IS_SOTRE_CASH_TRANSACTION_IN_PROCESS()
	AND IS_NIGHTCLUB_ID_VALID(g_eCachedPlayerNightclubForNightclubSafe)
	AND g_eCachedPlayerNightclubForNightclubSafe != GET_LOCAL_PLAYER_STAT_OWNED_NIGHTCLUB()
		PRINTLN("[CLUB_CASH] Players owned nightclub has changed from ", GET_NIGHTCLUB_NAME_FROM_ID(g_eCachedPlayerNightclubForNightclubSafe), " to ", GET_NIGHTCLUB_NAME_FROM_ID(GET_LOCAL_PLAYER_STAT_OWNED_NIGHTCLUB()))
		
		IF g_iCachedPlayerCashForNightclubSafe != GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
			PRINTLN("[CLUB_CASH] Safe cash was cached as $", g_iCachedPlayerCashForNightclubSafe, " BUT stat is $", GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
			
			_CLUB_TRIGGER_STORE_CASH_TRANSACTION(ref_clubData, g_iCachedPlayerCashForNightclubSafe - GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
			
		ELSE
			PRINTLN("[CLUB_CASH] Safe cash was cached as $", g_iCachedPlayerCashForNightclubSafe, " and stat is also $", GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
		ENDIF
	ELIF iTimeLeft <= 0
		IF NOT _CLUB_IS_SOTRE_CASH_TRANSACTION_IN_PROCESS()
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT, ciCLUB_PAYOUT_INTERVAL_MS)
			ref_clubData.iLastSyncTime = iGameTime
			PRINTLN("[CLUB_CASH] Updating 'time left before payout' stat. ", ciCLUB_PAYOUT_INTERVAL_MS, " ms left before payout.")
			
			FLOAT fPopularity  		= GET_LOCAL_CLUB_POPULARITY()
			INT iPayout 			= GET_CLUB_PAYOUT_VALUE(fPopularity)
			INT iCurrentSafeValue  	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
			
			PRINTLN("[CLUB_CASH] Adding cash to club safe from payout : Popularity == ",fPopularity, "%, inital payout == $", iPayout, ", current safe value == $", iCurrentSafeValue)
			IF iCurrentSafeValue + iPayout > g_sMPTunables.iNightclubMaxSafeValue
				iPayout = g_sMPTunables.iNightclubMaxSafeValue - iCurrentSafeValue
			ENDIF
			PRINTLN("[CLUB_CASH] Starting payout transaction : Popularity == ",fPopularity, "%, capped payout == $", iPayout, " new safe value == $", iCurrentSafeValue + iPayout)
			
			IF USE_SERVER_TRANSACTIONS()
				_CLUB_TRIGGER_STORE_CASH_TRANSACTION(ref_clubData, iPayout)
			ELSE
				PRINTLN("[CLUB_CASH] not using server transactions - Directly setting Safe cash value from $", iCurrentSafeValue, " to $", iCurrentSafeValue + iPayout)
				IF iPayout > 0
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE, iCurrentSafeValue + iPayout)
					INCREMENT_TOTAL_NIGHTCLUB_EARNINGS(iPayout)
					GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdNightclubData.iNightclubSafeCash = (iCurrentSafeValue + iPayout)
					
					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE) = g_sMPTunables.iNightclubMaxSafeValue
						PRINT_TICKER_WITH_STRING_AND_INT("CLUB_PAY_MAX", GET_PLAYER_NIGHTCLUB_NAME_AS_TEXT_LABEL(PLAYER_ID()), GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
					ELSE
						PRINT_TICKER_WITH_STRING_AND_INT("CLUB_PAY", GET_PLAYER_NIGHTCLUB_NAME_AS_TEXT_LABEL(PLAYER_ID()), GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE))
					ENDIF
					
					// Clear to tell player when safe full again
					IF IS_BIT_SET(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
						CLEAR_BIT(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
					ENDIF
				ELSE
					// Once per full safe
					IF NOT IS_BIT_SET(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
						PRINT_TICKER_WITH_INT("CLUB_NO_PAY", g_sMPTunables.iNightclubMaxSafeValue)
						SET_BIT(g_iNightclubSafeHelpText, BS_NIGHTCLUB_DISPLAY_SAFE_FULL_HELP_TEXT)
					ENDIF
				ENDIF
			ENDIF
			
			NIGHTCLUB_POPULARITY_MODIFIER eModifier = NPM_DECAY_DEFAULT
			IF IS_PLAYER_NIGHTCLUB_STAFF_PURCHASED(PLAYER_ID())
				eModifier = NPM_DECAY_UPGRADE
			ENDIF
			
			//Reduce popularity over time (after every payout)
			CLUB_MODIFY_POPULARITY_BY_TYPE(eModifier)
		ENDIF
	ELSE
		//Maintain updating payout timer stat
		IF iTimeSinceLastSync >= ciCLUB_STAT_SYNC_INTERVAL_MS
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_PAY_TIME_LEFT, iTimeLeft)
			ref_clubData.iLastSyncTime = iGameTime
			PRINTLN("[CLUB_CASH] Updating 'time left before payout' stat. ", iTimeLeft, " ms left before payout.")
		ENDIF
	ENDIF
	
	g_iCachedPlayerCashForNightclubSafe = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUB_SAFE_CASH_VALUE)
	g_eCachedPlayerNightclubForNightclubSafe = GET_LOCAL_PLAYER_STAT_OWNED_NIGHTCLUB()
ENDPROC

 //FEATURE_BUSINESS_BATTLES
