USING "globals.sch"
USING "net_include.sch"
USING "selector_public.sch"

#IF FEATURE_FREEMODE_ARCADE

CONST_INT MAX_CONSOLE_LINES 10

STRUCT SECURITY_SCTV_DATA

	INT iLastUpdateTime
	INT iState = 0
	
	INT iSpectatedPlayer = -1
	

	FLOAT WinX = 0.327
	FLOAT WinY = 0.883
	FLOAT WinHeight = 0.166
	FLOAT WinWidth = 0.301
	FLOAT TextX = 0.176
	FLOAT TextY = 0.799
	FLOAT TextSize = 0.25
	FLOAT TextLineSpace = 0.016
	
	TEXT_LABEL_63 ConsoleStr[MAX_CONSOLE_LINES]
	INT iConsoleCursorLine
	
	PLAYER_INDEX FocusPlayerID
	
ENDSTRUCT

#IF IS_DEBUG_BUILD
PROC CREATE_SECURITY_SCTV_WIDGETS(SECURITY_SCTV_DATA &data)
	START_WIDGET_GROUP("SECURITY SCTV")
		START_WIDGET_GROUP("Globals")
			ADD_WIDGET_BOOL("g_DisableSCTVPlayerSlots", g_DisableSCTVPlayerSlots)
			ADD_WIDGET_INT_SLIDER("g_iNumSCTVSecurity", g_iNumSCTVSecurity, 0, 32, 1)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Data struct")
			ADD_WIDGET_INT_SLIDER("iLastUpdateTime", data.iLastUpdateTime, LOWEST_INT, HIGHEST_INT, 1)	
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Console Window")
			ADD_WIDGET_FLOAT_SLIDER("WinX", data.WinX, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WinY", data.WinY, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WinHeight", data.WinHeight, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("WinWidth", data.WinWidth, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("TextX", data.TextX, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("TextY", data.TextY, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("TextSize", data.TextSize, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("TextLineSpace", data.TextLineSpace, -1.0, 1.0, 0.001)
			
			INT i
			REPEAT MAX_CONSOLE_LINES i
				ADD_WIDGET_STRING(data.ConsoleStr[i])
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

PROC SHIFT_UP_CONSOLE_LINES(SECURITY_SCTV_DATA &data)
	
	INT i
	REPEAT MAX_CONSOLE_LINES i
		IF (i < MAX_CONSOLE_LINES-1)
			data.ConsoleStr[i] = data.ConsoleStr[i+1]	
			data.ConsoleStr[i+1] = ""
		ENDIF
	ENDREPEAT
	
ENDPROC	
PROC CONSOLE_PRINT_TEXT(SECURITY_SCTV_DATA &data, STRING str)
	
	DEBUG_PRINTCALLSTACK()
	
	data.iConsoleCursorLine++
	WHILE (data.iConsoleCursorLine >= MAX_CONSOLE_LINES)
		SHIFT_UP_CONSOLE_LINES(data)
		data.iConsoleCursorLine--
	ENDWHILE
	
	// this shouldn't happen
	IF (data.iConsoleCursorLine < 0)
		data.iConsoleCursorLine = 0
	ENDIF	
	
	PRINTLN("CONSOLE_PRINT_TEXT : ", str ," line ", data.iConsoleCursorLine)
	data.ConsoleStr[data.iConsoleCursorLine] = str
	
ENDPROC

FUNC BOOL IS_PLAYER_SECURITY_SCTV(PLAYER_INDEX playerID)	
	IF IS_PLAYER_SCTV(playerID)
	AND NETWORK_PLAYER_IS_ROCKSTAR_DEV(playerID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT COUNT_SECURITY_SCTV()
	// count current players
	INT iCount = 0
	INT iPlayer
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE)
			IF IS_PLAYER_SECURITY_SCTV(PlayerID)
				iCount++	
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC

FUNC INT COUNT_NON_SECURITY_PLAYERS(SECURITY_SCTV_DATA &data)

	UNUSED_PARAMETER(data)

	// count current players
	INT iCount = 0
	INT iPlayer
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
			IF NOT IS_PLAYER_SECURITY_SCTV(PlayerID)
				iCount++	
			ENDIF
		ENDIF
	ENDREPEAT

//	PRINTLN("[Security_SCTV] COUNT_NON_SECURITY_PLAYERS - returning ", iCount)	
//	TEXT_LABEL_63 str
//	str = "COUNT_NON_SECURITY_PLAYERS = "
//	str += iCount
//	CONSOLE_PRINT_TEXT(data, str)
	
	RETURN iCount
ENDFUNC

FUNC PLAYER_INDEX GET_HIGHEST_NON_SECURITY_PLAYER_ID(SECURITY_SCTV_DATA &data)

	UNUSED_PARAMETER(data)

	// count current players
	INT iPlayer
	PLAYER_INDEX PlayerID
	PLAYER_INDEX LastPlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1)
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
			IF NOT IS_PLAYER_SECURITY_SCTV(PlayerID)
				LastPlayerID = PlayerID
			ENDIF
		ENDIF
	ENDREPEAT
	
//	PRINTLN("[Security_SCTV] COUNT_NON_SECURITY_PLAYERS - LastPlayerID ", NATIVE_TO_INT(LastPlayerID))
//	TEXT_LABEL_63 str
//	str = "GET_HIGHEST_NON_SECURITY_PLAYER_ID = "
//	str += NATIVE_TO_INT(LastPlayerID)
//	CONSOLE_PRINT_TEXT(data, str)

	RETURN LastPlayerID
ENDFUNC


PROC MAKE_SPACE_FOR_SECURITY(SECURITY_SCTV_DATA &data)
	
	// do we need to kick a player out?
	IF (COUNT_NON_SECURITY_PLAYERS(data) > NUM_NETWORK_REAL_PLAYERS())
		
		// is the local player the last player?
		IF PLAYER_ID() = GET_HIGHEST_NON_SECURITY_PLAYER_ID(data)
			PRINTLN("[Security_SCTV] MAKE_SPACE_FOR_SECURITY - kicking this player.")
			IF NETWORK_CAN_BAIL()
				NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_QUIT_MP_CORE_SCRIPT_CRASH_SET_FREEMODE))
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

FUNC BOOL IS_TIME_FOR_SECURITY_UPDATE(SECURITY_SCTV_DATA &data)
	IF (GET_GAME_TIMER() > (data.iLastUpdateTime + 1000))
		PRINTLN("[Security_SCTV] IS_TIME_FOR_SECURITY_UPDATE - TRUE")
		RETURN TRUE
		data.iLastUpdateTime = GET_GAME_TIMER()
	ENDIF	
	RETURN FALSE
ENDFUNC



PROC RENDER_SECURITY_SCTV_CONSOLE(SECURITY_SCTV_DATA &data)
	
	DRAW_RECT(data.WinX, data.WinY, data.WinWidth, data.WinHeight, 0,0,0,128)
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str
	INT iLine
	REPEAT MAX_CONSOLE_LINES iLine
		SET_TEXT_SCALE(0.0000, data.TextSize)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		str = "> "
		str += data.ConsoleStr[iLine]
		DISPLAY_TEXT_WITH_LITERAL_STRING(data.TextX, data.TextY + (TO_FLOAT(iLine) * data.TextLineSpace), "STRING", str)	
	ENDREPEAT
	#ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_SECURITY_SCTV_DEBUG(SECURITY_SCTV_DATA &data)
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
	TEXT_LABEL_63 str
	str = "test print to console "
	str += iRand
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1)
		CONSOLE_PRINT_TEXT(data, str)
	ENDIF
	
ENDPROC
#ENDIF

PROC SetFocusPlayer(SECURITY_SCTV_DATA &data, PLAYER_INDEX PlayerID)
	
	PRINTLN("SetFocusPlayer - called with ", NATIVE_TO_INT(PlayerID))
	
	IF NOT (data.FocusPlayerID = PlayerID)

		TEXT_LABEL_63 str
		str = "SetFocusPlayer from "
		str += NATIVE_TO_INT(data.FocusPlayerID)	
		str += " to "
		str += NATIVE_TO_INT(PlayerID)
		CONSOLE_PRINT_TEXT(data, str)
		
		data.FocusPlayerID = PlayerID
		
		
		NETWORK_SET_IN_SPECTATOR_MODE_EXTENDED(TRUE, GET_PLAYER_PED(data.FocusPlayerID), TRUE)
		
	ENDIF
	
ENDPROC

PROC IncrementFocusPlayer(SECURITY_SCTV_DATA &data)
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("IncrementFocusPlayer - called")
	
	INT iCurrentPlayer = NATIVE_TO_INT(data.FocusPlayerID)
	PRINTLN("IncrementFocusPlayer - iCurrentPlayer = ", iCurrentPlayer)
	iCurrentPlayer++
	
	INT i
	PLAYER_INDEX PlayerID
	INT iNewFocusPlayer
	INT iThisPlayer
	REPEAT NUM_NETWORK_PLAYERS i
	
		iNewFocusPlayer = iCurrentPlayer + i
		IF (iNewFocusPlayer >= NUM_NETWORK_PLAYERS)
			iThisPlayer = iNewFocusPlayer - NUM_NETWORK_PLAYERS
		ELSE
			iThisPlayer = iNewFocusPlayer
		ENDIF
	 	
		PRINTLN("IncrementFocusPlayer - iThisPlayer = ", iThisPlayer)
	 
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iThisPlayer)
		IF IS_NET_PLAYER_OK(PlayerID, FALSE)
			IF NOT IS_PLAYER_SECURITY_SCTV(PlayerID)
				SetFocusPlayer(data, PlayerID)					
				EXIT
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

FUNC BOOL IS_PLAYER_IN_TUTORIAL_SESSION(PLAYER_INDEX PlayerID)
	INT iInstance = NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(PlayerID)
	PRINTLN("[Security_SCTV] IS_PLAYER_IN_TUTORIAL_SESSION player id ", NATIVE_TO_INT(PlayerID), ", iInstance = ", iInstance)
	IF (iInstance > -1)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_SECURITY_SCTV_FOCUS(SECURITY_SCTV_DATA &data)
	IF IS_NET_PLAYER_OK(data.FocusPlayerID, FALSE)

		ENTITY_INDEX AttachedEntity
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			AttachedEntity = GET_ENTITY_ATTACHED_TO(PLAYER_PED_ID())
			IF IS_ENTITY_A_PED(AttachedEntity)
				PED_INDEX AttachedPed = GET_PED_INDEX_FROM_ENTITY_INDEX(AttachedEntity)
				IF (AttachedPed != GET_PLAYER_PED(data.FocusPlayerID))
					DETACH_ENTITY(PLAYER_PED_ID(), FALSE)	
				ENDIF
			ENDIF
		ELSE
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), GET_PLAYER_PED(data.FocusPlayerID), 0, <<0,0,0>>, <<0,0,0>>) 	
		ENDIF
				
		IF IS_PLAYER_IN_TUTORIAL_SESSION(data.FocusPlayerID)
			IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(data.FocusPlayerID, PLAYER_ID())
			
				INT iTutorialInstance = NETWORK_GET_PLAYER_TUTORIAL_SESSION_INSTANCE(data.FocusPlayerID)
				PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_FOCUS - iTutorialInstance = ", iTutorialInstance)

				IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					IF NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
					AND NOT NETWORK_WAITING_POP_CLEAR_TUTORIAL_SESSION()						
						NETWORK_ALLOW_GANG_TO_JOIN_TUTORIAL_SESSION(1, iTutorialInstance)
						PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_FOCUS - joining tutorial session ", iTutorialInstance)	
					ELSE
						PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_FOCUS - waiting to cleanup tutorial session / pending.")	
					ENDIF
				ELSE
					IF NOT NETWORK_WAITING_POP_CLEAR_TUTORIAL_SESSION()
						PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_FOCUS - waiting to cleanup tutorial session.")
						NETWORK_END_TUTORIAL_SESSION()
					ENDIF
				ENDIF

			ELSE
				PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_FOCUS - in same tutorial session.")
			ENDIF
		ELSE
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
			AND NOT NETWORK_WAITING_POP_CLEAR_TUTORIAL_SESSION()
				PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_FOCUS - ending tutorial, focus player is not in tutorial session.")
				NETWORK_END_TUTORIAL_SESSION()
			ENDIF
		ENDIF
		
		IF NETWORK_IS_PLAYER_CONCEALED(data.FocusPlayerID)
			 NETWORK_CONCEAL_PLAYER(data.FocusPlayerID, FALSE)	
		ENDIF

				
	ENDIF
ENDPROC

PROC UPDATE_SECURITY_SCTV_LOGIC(SECURITY_SCTV_DATA &data)
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		IncrementFocusPlayer(data)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
		IncrementFocusPlayer(data)
	ENDIF
	#ENDIF
	
ENDPROC

PROC UPDATE_SECURITY_SCTV_SETTINGS(SECURITY_SCTV_DATA &data)
	
	UNUSED_PARAMETER(data)
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
		PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_SETTINGS - turning control off, leaving camera on")
	ENDIF
	
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE) 
		PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_SETTINGS - setting invisible")
	ENDIF
	
	IF NOT GET_ENTITY_COLLISION_DISABLED(PLAYER_PED_ID()) 
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE) 	
		PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV_SETTINGS - turning off collision")
	ENDIF
	
ENDPROC

PROC UPDATE_SECURITY_SCTV(SECURITY_SCTV_DATA &data)

	IF (g_DisableSCTVPlayerSlots)
		// sctv slots are disabled
		EXIT
	ENDIF
	
	PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV - called.")
	
	// stuff that needs to be staggerd.
	IF IS_TIME_FOR_SECURITY_UPDATE(data)		
		MAKE_SPACE_FOR_SECURITY(data)	
	ENDIF
	
	// is this player security SCTV?
	IF IS_PLAYER_SECURITY_SCTV(PLAYER_ID())
		
		PRINTLN("[Security_SCTV] UPDATE_SECURITY_SCTV - player is Security")
		
		SWITCH data.iState
			CASE 0
				KILL_SKYCAM()
				SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)
				SET_TRANSITION_STRING("")	
				
				IncrementFocusPlayer(data)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
				
				data.iState++
			BREAK
			CASE 1
				UPDATE_SECURITY_SCTV_SETTINGS(data)
				RENDER_SECURITY_SCTV_CONSOLE(data)	
				UPDATE_SECURITY_SCTV_LOGIC(data)
				UPDATE_SECURITY_SCTV_FOCUS(data)				
			BREAK
		ENDSWITCH
		

			
	ENDIF
	
	#IF IS_DEBUG_BUILD
		UPDATE_SECURITY_SCTV_DEBUG(data)
	#ENDIF


ENDPROC

#ENDIF

