USING "globals.sch"
USING "net_include.sch"
USING "net_saved_vehicles.sch"
USING "net_cash_transactions.sch"
USING "net_ambience_common.sch"

CONST_INT NUMBER_OF_IMPOUND_SPACES 16


//FUNC BOOL IS_ENTITY_IN_IMPOUND_YARD(ENTITY_INDEX EntityID)
//	IF IS_ENTITY_IN_ANGLED_AREA(EntityID, <<358.970734,-1597.851685,20.000153>>, <<412.824738,-1640.906860,40.542091>>, 33.250000)
//	OR IS_ENTITY_IN_ANGLED_AREA(EntityID, <<406.625153,-1644.242188,20.042194>>, <<417.126831,-1652.936768,41.951004>>, 20.250000)
//	OR IS_ENTITY_IN_ANGLED_AREA(EntityID, <<414.675842,-1635.666260,20.092776>>, <<423.541534,-1635.644531,41.043156>>, 15.500000)
//		RETURN(TRUE)
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

FUNC BOOL IS_PERSONAL_VEHICLE_IN_IMPOUND_YARD()
	IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
		IF IS_ENTITY_IN_IMPOUND_YARD(PERSONAL_VEHICLE_ID())
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IS_PLAYER_IN_IMPOUND_FINE_AREA()
	IF IS_NET_PLAYER_OK(PLAYER_ID())		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<409.8, -1639.5, 28.6>>) < 2500
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<400.653, -1609.557, 26.941>>, <<405.428, -1604.057, 32.016>>, 8.825)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<415.385, -1621.827, 27.466>>, <<419.485, -1616.952, 31.216>>, 5.825)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<416.776, -1653.729, 27.598>>, <<420.526, -1656.829, 32.073>>, 5.825)
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	RETURN(FALSE)
ENDFUNC




PROC GET_PARKING_SLOT_INFO(INT slotid,VECTOR &vCoords, FLOAT &fHeading)
	
	SWITCH slotid
		CASE 0 //impound
			vCoords=<<396.8110, -1643.6652, 28.2928>>
			fHeading = 140.0127 
		BREAK
		CASE 1
			vCoords=<<401.2413, -1648.0807, 28.2928>>
			fHeading = 139.3439  
		BREAK
		CASE 2
			vCoords=<<411.1246, -1655.9967, 28.2928>>
			fHeading =  139.9064
		BREAK
		CASE 3
			vCoords=<<418.3765, -1646.2842, 28.2940>>
			fHeading = 229.6834
		BREAK
		CASE 4
			vCoords=<<419.9710, -1641.7426, 28.2903>>
			fHeading = 266.1893
		BREAK
		CASE 5
			vCoords=<<421.0552, -1635.9493, 28.2928>>
			fHeading =  269.4205
		BREAK
		CASE 6
			vCoords=<<419.6233, -1629.6582, 28.2928>>
			fHeading =319.6136 
		BREAK
		CASE 7
			vCoords=<<410.8506, -1636.7677, 28.2928>>
			fHeading = 231.3992
		BREAK
		CASE 8
			vCoords=<<373.3945, -1623.8993, 28.2928>>
			fHeading = 138.3859 
		BREAK
		CASE 9
			vCoords=<<373.3945, -1623.8993, 28.2928>>
			fHeading = 138.3859
		BREAK
		CASE 10
			vCoords=<<393.5292, -1608.7450, 28.2928>>
			fHeading = 230.8697 
		BREAK
		CASE 11
			vCoords=<<389.3376, -1613.0973, 28.2928>>
			fHeading = 228.0681	
		BREAK
		CASE 12
			vCoords=<<402.6421, -1615.9017, 28.2928>>
			fHeading = 228.3702 
		BREAK
		CASE 13
			vCoords=<<398.3390, -1620.8989, 28.2928>>
			fHeading = 231.9081 	
		BREAK
		CASE 14
			vCoords= <<394.6593, -1625.9562, 28.2928>>
			fHeading = 231.1938 
		BREAK
		CASE 15
			vCoords=<<385.6364, -1634.1802, 28.2928>>
			fHeading = 140.7528 		
		BREAK
		DEFAULT
			vCoords=<<385.6364, -1634.1802, 28.2928>>
			fHeading = 140.7528 
			NET_PRINT("[impound] INVALID PARKING COORDS RETURNED") NET_NL()
			SCRIPT_ASSERT("[impound] INVALID PARKING COORDS RETURNED")
		BREAK
	ENDSWITCH


ENDPROC

PROC GET_PARKING_SLOT_AREA_INFO(INT slotid,VECTOR &vCoords1,VECTOR &vCoords2)

	SWITCH slotid
	
		CASE 0 //car impounded
			vCoords1=<<394.8110, -1641.6652, 27.2928>>
			vCoords2= <<398.8110, -1645.6652, 29.2928>>
		BREAK
		CASE 1
			vCoords1=<<399.2413, -1646.0807, 27.2928>>
			vCoords2=<<403.2413, -1650.0807, 29.2928>>
		BREAK
		CASE 2
			vCoords1=<<409.1246, -1653.9967, 27.2928>>
			vCoords2=<<413.1246, -1657.9967, 29.2928>>
		BREAK
		CASE 3
			vCoords1=<<416.3765, -1644.2842, 27.2940>>
			vCoords2=<<420.3765, -1648.2842, 29.2940>>
		BREAK
		CASE 4
			vCoords1=<<417.9710, -1639.7426, 27.2903>>
			vCoords2=<<421.9710, -1643.7426, 29.2903>>
		BREAK
		CASE 5	
			vCoords1=<<419.0552, -1632.9493, 27.2928>>
			vCoords2=<<423.0552, -1637.9493, 29.2928>>
		BREAK
		CASE 6
			vCoords1=<<417.6233, -1627.6582, 27.2928>>
			vCoords2=<<421.6233, -1631.6582, 29.2928>>
		BREAK
		CASE 7
			vCoords1=<<48.8506, -1634.7677, 27.2928>>
			vCoords2=<<412.8506, -1638.7677, 29.2928>>
		BREAK
		CASE 8
			vCoords1=<<371.3945, -1621.8993, 27.2928>>
			vCoords2=<<375.3945, -1625.8993, 29.2928>>
		BREAK
		CASE 9
			vCoords1=<<371.3945, -1621.8993, 27.2928>>
			vCoords2=<<375.3945, -1625.8993, 29.2928>>
		BREAK
		CASE 10
			vCoords1=<<391.5292, -1606.7450, 27.2928>>
			vCoords2=<<395.5292, -1610.7450, 29.2928>>
		BREAK
		CASE 11		
			vCoords1=<<387.3376, -1611.0973, 27.2928>>
			vCoords2=<<391.3376, -1615.0973, 29.2928>>
		BREAK
		CASE 12
			vCoords1=<<400.6421, -1613.9017, 27.2928>>
			vCoords2=<<404.6421, -1617.9017, 29.2928>>
		BREAK
		CASE 13
			vCoords1=<<396.3390, -1618.8989, 27.2928>>
			vCoords2=<<400.3390, -1622.8989, 29.2928>>
		BREAK
		CASE 14	
			vCoords1=<<392.6593, -1623.9562, 27.2928>>
			vCoords2=<<396.6593, -1627.9562, 29.2928>>
		BREAK
		CASE 15
			vCoords1=<<383.6364, -1632.1802, 27.2928>>
			vCoords2=<<387.6364, -1636.1802, 29.2928>>
		BREAK
		DEFAULT
			NET_PRINT("[impound] INVALID PARKING AREA COORDS RETURNED") NET_NL()
			SCRIPT_ASSERT("[impound] INVALID PARKING AREA COORDS RETURNED")
			vCoords1=<<383.6364, -1632.1802, 27.2928>>
			vCoords2=<<387.6364, -1636.1802, 29.2928>>
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC INT GET_PLAYER_ASSIGNED_SPACE(INT iSlot)
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF GlobalServerBD.ImpoundDetails[iPlayer].iSlot	= iSlot
			RETURN iPlayer
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC FREE_UP_IMPOUND_PARKING_SLOTS()
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF NOT IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iPlayer), FALSE, FALSE)
			IF NOT (GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -1)
				NET_PRINT("[impound] FREE_UP_IMPOUND_PARKING_SLOTS - clearing for player ") NET_PRINT_INT(iPlayer) NET_PRINT(", was using slot") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()
				GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -1				
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT FIND_NEXT_FREE_IMPOUND_SLOT(INT iStartPos)

	INT i
	INT iThisSlot
	
	REPEAT NUMBER_OF_IMPOUND_SPACES i
		iThisSlot = iStartPos + i
		IF (iThisSlot >= NUMBER_OF_IMPOUND_SPACES)
			iThisSlot -= NUMBER_OF_IMPOUND_SPACES	
		ENDIF
		IF (GET_PLAYER_ASSIGNED_SPACE(iThisSlot) = -1)
			NET_PRINT("[impound] FIND_NEXT_FREE_IMPOUND_SLOT returning ") NET_PRINT_INT(iThisSlot) NET_NL() 
			RETURN(iThisSlot)
		ENDIF
	ENDREPEAT
	
	NET_PRINT("[impound] FIND_NEXT_FREE_IMPOUND_SLOT no free slot found ") NET_NL() 
	RETURN -1
	
ENDFUNC

FUNC INT FIND_A_RANDOM_FREE_IMPOUND_SLOT()
	RETURN FIND_NEXT_FREE_IMPOUND_SLOT(GET_RANDOM_INT_IN_RANGE(0, NUMBER_OF_IMPOUND_SPACES))
ENDFUNC

PROC REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(INT iPlayer)
	IF NETWORK_ENTITY_AREA_DOES_EXIST(GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID)
		NETWORK_REMOVE_ENTITY_AREA(GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID)
	ENDIF
	GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID = -1
	NET_PRINT("[impound] REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER called for player ") NET_PRINT_INT(iPlayer) NET_NL()
ENDPROC

PROC CLEANUP_IMPOUND_SLOT_FOR_PLAYER(INT iPlayer)
	
	// server bd 
	REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(iPlayer)
	SERVER_BD_IMPOUND_DETAILS EmptyServerData
	GlobalServerBD.ImpoundDetails[iPlayer] = EmptyServerData
	
	NET_PRINT("[impound] CLEANUP_IMPOUND_SLOT_FOR_PLAYER called for player ") NET_PRINT_INT(iPlayer) NET_NL()
	
ENDPROC

PROC SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER(INT iPlayer, PLAYER_INDEX piPLayer, BOOL bActive)

	VECTOR vCoords1, vCoords2

	IF GlobalplayerBD[iPlayer].brequestvehiclespawnslot	
	AND bActive
	AND IS_NET_PLAYER_OK(piPLayer, FALSE, FALSE) 
	
		// if the slot hasn't been verified
		IF NOT (GlobalServerBD.ImpoundDetails[iPlayer].bVerified)	
		
			// assign a new slot
			IF (GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -1)
			
				// do a tidy up of existing slots
				FREE_UP_IMPOUND_PARKING_SLOTS()
					
				// find a free slot
				GlobalServerBD.ImpoundDetails[iPlayer].iSlot = FIND_A_RANDOM_FREE_IMPOUND_SLOT()			
				
				NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - assigning slot for player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()
								
			ENDIF
		
			// check assigned slot is not occupied
			IF NOT (GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -1)							
				IF GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID = -1
					GET_PARKING_SLOT_AREA_INFO(GlobalServerBD.ImpoundDetails[iPlayer].iSlot,vCoords1,vCoords2)
					IF NOT NETWORK_ENTITY_AREA_DOES_EXIST(GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID)
						GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID = NETWORK_ADD_ENTITY_AREA(vCoords1,vCoords2)
						GlobalServerBD.ImpoundDetails[iPlayer].iAssignTime = GET_NETWORK_TIME()
						NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - adding entity area for player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()
					ELSE
						REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(iPlayer)
					ENDIF
				ELSE
					IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalServerBD.ImpoundDetails[iPlayer].iAssignTime) < 40000
						IF NETWORK_ENTITY_AREA_DOES_EXIST(GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID )
							IF NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID )
								IF NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(GlobalServerBD.ImpoundDetails[iPlayer].iEntityAreaID)								
									GlobalServerBD.ImpoundDetails[iPlayer].bVerified = TRUE
									REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(iPlayer)		
									NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - area good, player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()									
								ELSE
									// area is occupied, try another position.
									NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - area occupied try another, player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()
									REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(iPlayer)
									GlobalServerBD.ImpoundDetails[iPlayer].iSlot = FIND_NEXT_FREE_IMPOUND_SLOT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot)
								ENDIF
							ENDIF
						ELSE
							REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(iPlayer)	
							NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - entity area doesn't exist, player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()																		
						ENDIF
					ELSE
						// timed out.
						REMOVE_ENTITY_AREA_FOR_IMPOUND_PLAYER(iPlayer)
						GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -1	
						NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - timed out, player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()											
					ENDIF
				ENDIF	
			ENDIF			
			
			// the impound is either full or we timed out, return -2
			IF (GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -1)
				GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -2
				GlobalServerBD.ImpoundDetails[iPlayer].bVerified = TRUE
				NET_PRINT("[impound] SERVER_MAINTAIN_IMPOUND_PARKING_FOR_PLAYER - no impound spaces available, player ") NET_PRINT_INT(iPlayer) NET_PRINT(", slot = ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()													
			ENDIF			
		
		ELSE
			// once the result has been verified we assume that the client will then use this.			
		ENDIF		
	ELSE
		// cleanup if player no longer needs it.
		IF (GlobalServerBD.ImpoundDetails[iPlayer].bVerified)
		OR (GlobalServerBD.ImpoundDetails[iPlayer].iSlot > -1)		
			CLEANUP_IMPOUND_SLOT_FOR_PLAYER(iPlayer)			
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE(VECTOR &vCoords, FLOAT &fHeading)
	

	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	
	GlobalplayerBD[iPlayer].brequestvehiclespawnslot = TRUE
	
	
	IF (GlobalServerBD.ImpoundDetails[iPlayer].bVerified)	
		IF (GlobalServerBD.ImpoundDetails[iPlayer].iSlot > -1)
			GET_PARKING_SLOT_INFO(GlobalServerBD.ImpoundDetails[iPlayer].iSlot ,vCoords,fHeading)
			NET_PRINT("[impound] GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE -  Player vehicle parking slot: ") NET_PRINT_INT(GlobalServerBD.ImpoundDetails[iPlayer].iSlot) NET_NL()
			NET_PRINT("[impound] GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE -  Player vehicle got parking space: ") NET_PRINT_VECTOR(vCoords) NET_NL()
			RETURN(TRUE)
		ELIF (GlobalServerBD.ImpoundDetails[iPlayer].iSlot = -2)
			GlobalplayerBD[iPlayer].brequestvehiclespawnslot = FALSE
			MP_SAVE_VEHICLE_CLEAR_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
			NET_PRINT("[impound] GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE - IMPOUND FULL: ")  NET_NL()	
			RETURN(FALSE)
		ENDIF
	ELSE
		NET_PRINT("[impound] GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE - waiting for server to verify. Player ") NET_PRINT_INT(iPlayer) NET_NL()
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CAN_CONTEXT_BUTTON_BE_TRIGGERED(INT intention_index)

	INT arraypos = GET_CONTEXT_INTENTION_ARRAY_INDEX(intention_index)
	IF arraypos = -1
	CPRINTLN(DEBUG_SHOPS,"CAN_CONTEXT_BUTTON_BE_TRIGGERED  arraypos = -1")
		RETURN FALSE
	ENDIF
	//these may not be safe for multiplayer
	IF NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX())

		PRINTSTRING("CAN_CONTEXT_BUTTON_BE_TRIGGERED:Acceptance prevented due to (NOT IS_PLAYER_PLAYING(GET_PLAYER_INDEX())\n")
		RETURN FALSE
	ENDIF
	IF (IS_PHONE_ONSCREEN())
		PRINTSTRING("CAN_CONTEXT_BUTTON_BE_TRIGGERED:Acceptance prevented due to IS_PHONE_ONSCREEN()\n")
		RETURN FALSE
	ENDIF
	IF (IS_CUTSCENE_PLAYING())
		PRINTSTRING("CAN_CONTEXT_BUTTON_BE_TRIGGERED:Acceptance prevented due to IS_CUTSCENE_PLAYING()\n")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

//FUNC BOOL IS_IMPOUND_FINE_HELP_DISPLAYED()
//	BOOL retval
//	BEGIN_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MP_IMPOUND_FINEB")
//	ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
//	ADD_TEXT_COMPONENT_INTEGER(g_sMPTunables.iCar_impound_fee)
//	retval = END_TEXT_COMMAND_IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(HELP_TEXT_SLOT_STANDARD)
//	RETURN(retval)
//ENDFUNC

PROC PRINT_IMPOUND_FINE_HELP()
	BEGIN_TEXT_COMMAND_DISPLAY_HELP("MP_IMPOUND_FINEB")
	ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(g_strContextButton)
	ADD_TEXT_COMPONENT_INTEGER(g_sMPTunables.iCar_impound_fee)
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE,50)
ENDPROC

PROC UPDATE_IMPOUND_FINE_HELP(BOOL bDisplay)
	IF (bDisplay)
		//IF NOT IS_IMPOUND_FINE_HELP_DISPLAYED()
			PRINT_IMPOUND_FINE_HELP()
		//ENDIF
	ENDIF
ENDPROC

PROC PV_GATE_FINE_TRANSACTION(VEHICLE_INDEX tempveh)
	
	RELEASE_CONTEXT_INTENTION(MPGlobals.iImpoundContextIntention)
	//GIVE_LOCAL_PLAYER_CASH(-250, TRUE)	
	
	NETWORK_BUY_ITEM(g_sMPTunables.iCar_impound_fee,ENUM_TO_INT(GET_ENTITY_MODEL(tempveh)),PURCHASE_CARIMPOUND,DEFAULT,(NETWORK_GET_VC_BANK_BALANCE() >= g_sMPTunables.iCar_impound_fee),"IMPOUND", 0, 0, 0, TRUE)
	PRINT_HELP("MP_IMPOUND_OK")
	MP_SAVE_VEHICLE_CLEAR_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)	
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_RETRIEVING_AFTER_FINE)
	MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(CURRENT_SAVED_VEHICLE_SLOT(),
											g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()] ,
											TRUE)
	REQUEST_SAVE(SSR_REASON_VEH_IMPOUND_RELEASE, STAT_SAVETYPE_IMMEDIATE_FLUSH)	
ENDPROC

PROC MAINAIN_PV_GATE_FINE(VEHICLE_INDEX tempveh)
	
	BOOL bCleanupGateFine = TRUE
	BOOL bDisplayFineHelp = FALSE
	
	SWITCH MPGlobals.iImpoundTransactionState
		CASE 0
	
			IF IS_PLAYER_IN_IMPOUND_FINE_AREA()
				IF CURRENT_SAVED_VEHICLE_SLOT() >= 0
					IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
					
						//IF GET_PLAYER_CASH(PLAYER_ID()) > g_sMPTunables.iCar_impound_fee
						IF (NETWORK_GET_VC_BANK_BALANCE()+NETWORK_GET_VC_WALLET_BALANCE())  > g_sMPTunables.iCar_impound_fee
						OR NETWORK_CAN_SPEND_MONEY(g_sMPTunables.iCar_impound_fee, FALSE, TRUE, FALSE) //Use OR to catch cases where combined balance exceeds SCRIPT_MAX_INT32.
						
							// display help to pay fine
							IF MPGlobals.iImpoundContextIntention = NEW_CONTEXT_INTENTION
								REGISTER_CONTEXT_INTENTION(MPGlobals.iImpoundContextIntention,CP_MEDIUM_PRIORITY,"", TRUE)
								NET_PRINT("[impound] MAINAIN_PV_GATE_FINE: registered context intention for paying fine") NET_NL()
							ENDIF		

							// has player paid fine
							IF HAS_CONTEXT_BUTTON_TRIGGERED(MPGlobals.iImpoundContextIntention)
								NET_PRINT("[impound] MAINAIN_PV_GATE_FINE: Player is paying fine.") NET_NL()
								
								IF USE_SERVER_TRANSACTIONS()
									PRINTLN("[impound] using server transactions...")
									MPGlobals.iImpoundTransactionState++
								ELSE
									
									PRINTLN("[impound] NOT using server transactions.")																	
									PV_GATE_FINE_TRANSACTION(tempveh)
									
								ENDIF
							ELSE
								IF CAN_CONTEXT_BUTTON_BE_TRIGGERED(MPGlobals.iImpoundContextIntention)	
									bDisplayFineHelp = TRUE					
								ENDIF
							ENDIF
							
							//SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
							//SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
							
							bCleanupGateFine = FALSE
					
						ELSE
							
							IF NOT IS_BIT_SET(MPGlobals.iBSImpoundFine, biIMP_NOT_ENOUGH_CASH_HELP)
								IF SAFE_TO_PRINT_PV_HELP()
									//PRINT_HELP("MP_IMPOUND_CASH")
									PRINT_HELP_WITH_NUMBER("MP_IMPOUND_CASHB", g_sMPTunables.iCar_impound_fee)
									SET_BIT(MPGlobals.iBSImpoundFine, biIMP_NOT_ENOUGH_CASH_HELP)	
								ENDIF
							ENDIF
							
						ENDIF
							
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(MPGlobals.iBSImpoundFine, biIMP_NOT_ENOUGH_CASH_HELP)		
			ENDIF
	
		BREAK
		CASE 1
			IF NETWORK_REQUEST_CASH_TRANSACTION(MPGlobals.iImpoundScriptTransactionIndex, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_CAR_IMPOUND, g_sMPTunables.iCar_impound_fee, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
				PRINTLN("[impound] NETWORK_REQUEST_CASH_TRANSACTION = true")
				MPGlobals.iImpoundTransactionState++	
			ELSE
				PRINTLN("[impound] failed on NETWORK_REQUEST_CASH_TRANSACTION")
				DELETE_CASH_TRANSACTION(MPGlobals.iImpoundScriptTransactionIndex)
				MPGlobals.iImpoundTransactionState = 0
			ENDIF
			bCleanupGateFine = FALSE
		BREAK
		CASE 2
			IF IS_CASH_TRANSACTION_COMPLETE(MPGlobals.iImpoundScriptTransactionIndex)
				IF (GET_CASH_TRANSACTION_STATUS(MPGlobals.iImpoundScriptTransactionIndex) = CASH_TRANSACTION_STATUS_SUCCESS)
					PRINTLN("[impound] IS_CASH_TRANSACTION_COMPLETE = true")
					
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(MPGlobals.iImpoundScriptTransactionIndex))
					
					PV_GATE_FINE_TRANSACTION(tempveh)
					
					DELETE_CASH_TRANSACTION(MPGlobals.iImpoundScriptTransactionIndex)
					MPGlobals.iImpoundTransactionState = 0
				ELSE
					PRINTLN("[impound] IS_CASH_TRANSACTION_COMPLETE = transaction failed")
					DELETE_CASH_TRANSACTION(MPGlobals.iImpoundScriptTransactionIndex)
					MPGlobals.iImpoundTransactionState = 0					
				ENDIF
			ELSE
				PRINTLN("[impound] waiting on IS_CASH_TRANSACTION_COMPLETE")
			ENDIF
			bCleanupGateFine = FALSE
		BREAK
	ENDSWITCH
	
	IF (bCleanupGateFine)
		IF NOT (MPGlobals.iImpoundContextIntention = NEW_CONTEXT_INTENTION)
			RELEASE_CONTEXT_INTENTION(MPGlobals.iImpoundContextIntention)	
		ENDIF	
	ENDIF
	
	UPDATE_IMPOUND_FINE_HELP(bDisplayFineHelp)
	
ENDPROC

PROC UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK()
	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.iImpoundYardCheckTimer)) > 3000)
		
		IF IS_ENTITY_IN_IMPOUND_YARD(PLAYER_PED_ID())
	
			IF CURRENT_SAVED_VEHICLE_SLOT() > -1
				IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
					NET_PRINT("[impound] UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK - they have just paid the fine so not giving wanted level.") NET_NL()
					EXIT
				ENDIF
				
				IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_RETRIEVING_AFTER_FINE)
					NET_PRINT("[impound] UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK - they are retrieving impound car after paying fine.") NET_NL()
					EXIT
				ENDIF
			ENDIF
			
			// in impound yard when shouldn't be
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),2)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				NET_PRINT("[impound] UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK - in impound yard, setting wanted level.") NET_NL()
			ENDIF
		
		ELSE
		
			// wait for player to be out of impound with vehicle he is retrieving
			IF CURRENT_SAVED_VEHICLE_SLOT() > -1
				IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_RETRIEVING_AFTER_FINE)				
					IF NOT MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())	
						IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
							IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
								IF NOT IS_ENTITY_IN_IMPOUND_YARD(PERSONAL_VEHICLE_ID())
									NET_PRINT("[impound] UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK - vehicle is no longer in impound, clearing retrieve vehicle flag.") NET_NL()
									CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_RETRIEVING_AFTER_FINE)																		
								ENDIF
							ENDIF 
						ENDIF
					ENDIF
				ENDIF

			ENDIF		
				
		ENDIF
		
		// clear the paid fine global
		IF CURRENT_SAVED_VEHICLE_SLOT() > -1
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
				IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
					IF NOT IS_ENTITY_IN_IMPOUND_YARD(PERSONAL_VEHICLE_ID())
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)				
						NET_PRINT("[impound] UPDATE_WANTED_LEVEL_IN_IMPOUND_YARD_CHECK - vehicle is no longer in impound, clearing paid fine flag.") NET_NL()
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	
		MPGlobalsAmbience.iImpoundYardCheckTimer = GET_NETWORK_TIME()
	ENDIF
	
		
ENDPROC

PROC UPDATE_CHECK_IMPOUND_VEHICLE_IS_STILL_IN_IMPOUND()
	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobalsAmbience.iImpoundVehicleCheckTimer)) > 2000)
		// has someone removed the impounded car from the impound lot?
		IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
			IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
				IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
					IF NOT IS_ENTITY_IN_IMPOUND_YARD(PERSONAL_VEHICLE_ID())
						NET_PRINT("[impound] UPDATE_CHECK_IMPOUND_VEHICLE_IS_STILL_IN_IMPOUND - vehicle is no longer in impound, clearing impound state.") NET_NL()
						MP_SAVE_VEHICLE_CLEAR_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
					ENDIF
				ENDIF 
			ENDIF
		ENDIF
		MPGlobalsAmbience.iImpoundVehicleCheckTimer = GET_NETWORK_TIME()
	ENDIF
ENDPROC

PROC GIVE_VEHICLE_MIN_HEALTH(VEHICLE_INDEX VehicleID, FLOAT fMinHealthValue=300.0)
	IF (GET_VEHICLE_ENGINE_HEALTH(VehicleID) < fMinHealthValue)
		SET_VEHICLE_ENGINE_HEALTH(VehicleID, fMinHealthValue)
		NET_PRINT("GIVE_VEHICLE_MIN_HEALTH - giving minimum engine health") NET_NL()
	ENDIF
	IF (GET_VEHICLE_PETROL_TANK_HEALTH(VehicleID) < fMinHealthValue)
		SET_VEHICLE_PETROL_TANK_HEALTH(VehicleID, fMinHealthValue)
		NET_PRINT("GIVE_VEHICLE_MIN_HEALTH - giving petrol tank health") NET_NL()
	ENDIF
	IF (GET_VEHICLE_BODY_HEALTH(VehicleID) < fMinHealthValue)
		SET_VEHICLE_BODY_HEALTH(VehicleID, fMinHealthValue)
		NET_PRINT("GIVE_VEHICLE_MIN_HEALTH - giving body health") NET_NL()
	ENDIF
ENDPROC

PROC MAINTAIN_PV_IMPOUND()
	VECTOR vSpawnCoords
	FLOAT fSpawnHeading
	VEHICLE_INDEX tempveh
	
	IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
		IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_STOLE_IMPOUND_AMBIENT)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
					tempveh = PERSONAL_VEHICLE_ID()
				ENDIF
				
				IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),FALSE)	!= tempveh
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						
						PRINTLN("[impound] Impound stole ambient wanted 2 given")
					ENDIF
					
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_STOLE_IMPOUND_AMBIENT)
				ENDIF
			ENDIF
		ENDIF
		
		// clear near impound flag if player is dead
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
			
			PRINTLN("[impound] MAINTAIN_PV_IMPOUND - clearing MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND as player is not ok.")
		ENDIF
	ELSE
		CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_STOLE_IMPOUND_AMBIENT)
	ENDIF
	
	IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
		IF IS_NET_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_NET_ID())
			tempveh = PERSONAL_VEHICLE_ID()
		ENDIF
		
		MAINAIN_PV_GATE_FINE(tempveh)
		
		// is it time to destroy the vehicle?
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			INT iStart = g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iImpoundTime
			INT iEnd = GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_TOTAL_PLAYING_TIME)
			IF (iEnd - iStart) > 96 // 48 ingame hours~.
			AND ((NOT DOES_ENTITY_EXIST(tempveh)) OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), tempveh) > 100))
				NET_PRINT("[impound] vehicle has been impounded for over 48 in game hours - destroy") NET_NL()
				
				CLEANUP_MP_SAVED_VEHICLE(DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, TRUE)
				CLEAR_BIT(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
				IF IS_BIT_SET(g_MpSavedVehicles[CURRENT_SAVED_VEHICLE_SLOT()].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_PHONE_CALL_IMPOUND)
				ELSE
					SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_DO_DESTROYED_INSURANCE_REMINDER)
				ENDIF
				SET_SAVE_VEHICLE_AS_DESTROYED(CURRENT_SAVED_VEHICLE_SLOT())
				IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					g_bTriggerCarmodSave = TRUE
				ELSE
					
					REQUEST_SAVE(SSR_REASON_VEH_DESTROYED, STAT_SAVETYPE_IMMEDIATE_FLUSH)
				ENDIF
				EXIT
			ENDIF
		ENDIF
		

		IF HAS_PERSONAL_VEHICLE_BEEN_REQUESTED_TO_WARP_NEAR()
			NET_PRINT("[impound] clearing warp near flag - 0") NET_NL()
			CLEAR_REQUEST_TO_WARP_PERSONAL_VEHICLE_NEAR()
		ENDIF

		IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
			IF DOES_ENTITY_EXIST(tempveh)
				IF IS_VEHICLE_EMPTY(tempveh,TRUE)
					IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
					OR IS_SCREEN_FADED_OUT()
					
						IF GET_IMPOUND_COORDS_FOR_SAVED_VEHICLE(vSpawnCoords, fSpawnHeading)
								
							//NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(2000) // not sure why this was here??
							
							IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
								NET_PRINT("[impound] Requesting control of saved vehicle to impound") NET_NL()
								NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
							ELSE						

								IF NOT (MPGlobals.VehicleData.bFadingOutForWarp)
										
									IF IS_ENTITY_VISIBLE_TO_SCRIPT(tempveh)
										NETWORK_FADE_OUT_ENTITY(tempveh, TRUE, TRUE)
									ENDIF
									
									SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(tempveh, TRUE)
									MPGlobals.VehicleData.bFadingOutForWarp = TRUE
									MPGlobals.VehicleData.FadeWarpTimer = GET_NETWORK_TIME()
									NET_PRINT("[impound] fading out vehicle for impound warp") NET_NL()
									
								ELSE
									
									IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.VehicleData.FadeWarpTimer) > 2000)
									OR NOT IS_ENTITY_VISIBLE_TO_SCRIPT(tempveh)
										MPGlobals.VehicleData.bFadingOutForWarp = FALSE
										NET_PRINT("[impound] setting impound coords: ") NET_PRINT_VECTOR(vSpawnCoords) NET_NL()
										CLEAR_AREA(vSpawnCoords, 4.0, FALSE, FALSE, FALSE, TRUE)
										SET_ENTITY_COORDS(tempveh,vSpawnCoords)
										SET_ENTITY_HEADING(tempveh,fSpawnHeading)
										SET_VEHICLE_LIGHTS(tempveh, SET_VEHICLE_LIGHTS_OFF)
										SET_VEHICLE_LIGHTS(tempveh, FORCE_VEHICLE_LIGHTS_OFF)
										SET_VEHICLE_BRAKE_LIGHTS(tempveh,FALSE)
										SET_VEHICLE_INDICATOR_LIGHTS(tempveh,FALSE,FALSE)
										SET_VEHICLE_INDICATOR_LIGHTS(tempveh,TRUE,FALSE)
										SET_VEHICLE_LIGHTS(tempveh, NO_VEHICLE_LIGHT_OVERRIDE)
										SET_VEHICLE_ENGINE_ON(tempveh,FALSE,TRUE)
										SET_ENTITY_VISIBLE(tempveh, TRUE)
										SET_VEHICLE_SIREN(tempveh,FALSE)
										GIVE_VEHICLE_MIN_HEALTH(tempveh)
										SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh)
										GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].brequestvehiclespawnslot = FALSE
										CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
										
									ELSE
										NET_PRINT("[impound] waiting for vehicle to fade out") NET_NL()
									ENDIF
								ENDIF
								

							ENDIF

						ENDIF
					ENDIF

				ELSE
					// need to make visible again if aborting
					IF (MPGlobals.VehicleData.bFadingOutForWarp)
						IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
							NET_PRINT("[impound] Requesting control of saved vehicle to make visible again") NET_NL()
							NETWORK_REQUEST_CONTROL_OF_ENTITY(tempveh)
						ENDIF
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempveh)
							SET_ENTITY_VISIBLE(tempveh, TRUE)	
							NET_PRINT("[impound] made vehicle visible again.") NET_NL()
							MPGlobals.VehicleData.bFadingOutForWarp = FALSE
						ENDIF						
					ELSE				
						NET_PRINT("[impound] vehicle is not empty clearing impound request") NET_NL()
						MP_SAVE_VEHICLE_CLEAR_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())				
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
					ENDIF
				ENDIF
			ELSE
				NET_PRINT("[impound] vehicle does not exist.") NET_NL()
			ENDIF
		ELSE
			IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_NEAR_PLAYER)
				IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND1")
					IF NOT IS_SCENARIO_GROUP_ENABLED("POLICE_POUND1")
						SET_SCENARIO_GROUP_ENABLED("POLICE_POUND1",TRUE)
						NET_PRINT("[impound] POLICE_POUND1 scenario activated") NET_NL()
					ENDIF
				ENDIF
				IF MPGlobals.VehicleData.iTimesImpounded >=2
					IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND2")
						IF NOT IS_SCENARIO_GROUP_ENABLED("POLICE_POUND2")
							SET_SCENARIO_GROUP_ENABLED("POLICE_POUND2",TRUE)
							NET_PRINT("[impound] POLICE_POUND2 scenario activated") NET_NL()
						ENDIF
					ENDIF
				ENDIF
				IF MPGlobals.VehicleData.iTimesImpounded >=3
					IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND3")
						IF NOT IS_SCENARIO_GROUP_ENABLED("POLICE_POUND3")
							SET_SCENARIO_GROUP_ENABLED("POLICE_POUND3",TRUE)
							NET_PRINT("[impound] POLICE_POUND3 scenario activated") NET_NL()
						ENDIF
					ENDIF
				ENDIF
				IF MPGlobals.VehicleData.iTimesImpounded >=4
					IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND4")
						IF NOT IS_SCENARIO_GROUP_ENABLED("POLICE_POUND4")
							SET_SCENARIO_GROUP_ENABLED("POLICE_POUND4",TRUE)
							NET_PRINT("[impound] POLICE_POUND4 scenario activated") NET_NL()
						ENDIF
					ENDIF
				ENDIF
				IF MPGlobals.VehicleData.iTimesImpounded >=5
					IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND5")
						IF NOT IS_SCENARIO_GROUP_ENABLED("POLICE_POUND5")
							SET_SCENARIO_GROUP_ENABLED("POLICE_POUND5",TRUE)
							NET_PRINT("[impound] POLICE_POUND5 scenario activated") NET_NL()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND1")
					IF IS_SCENARIO_GROUP_ENABLED("POLICE_POUND1")
						SET_SCENARIO_GROUP_ENABLED("POLICE_POUND1",FALSE)
						NET_PRINT("[impound] POLICE_POUND1 scenario off") NET_NL()
					ENDIF
				ENDIF
				IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND2")
					IF IS_SCENARIO_GROUP_ENABLED("POLICE_POUND2")
						SET_SCENARIO_GROUP_ENABLED("POLICE_POUND2",FALSE)
						NET_PRINT("[impound] POLICE_POUND2 scenario off") NET_NL()
					ENDIF
				ENDIF
				IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND3")
					IF IS_SCENARIO_GROUP_ENABLED("POLICE_POUND3")
						SET_SCENARIO_GROUP_ENABLED("POLICE_POUND3",FALSE)
						NET_PRINT("[impound] POLICE_POUND3 scenario off") NET_NL()
					ENDIF
				ENDIF
				IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND4")
					IF IS_SCENARIO_GROUP_ENABLED("POLICE_POUND4")
						SET_SCENARIO_GROUP_ENABLED("POLICE_POUND4",FALSE)
						NET_PRINT("[impound] POLICE_POUND4 scenario off") NET_NL()
					ENDIF
				ENDIF
				IF DOES_SCENARIO_GROUP_EXIST("POLICE_POUND5")
					IF IS_SCENARIO_GROUP_ENABLED("POLICE_POUND5")
						SET_SCENARIO_GROUP_ENABLED("POLICE_POUND5",FALSE)
						NET_PRINT("[impound] POLICE_POUND5 scenario off") NET_NL()
					ENDIF
				ENDIF
			ENDIF
			IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
						IF SAFE_TO_PRINT_PV_HELP()
						AND HAS_NET_TIMER_STARTED(MPGlobals.VehicleData.iImpoundTimer)
						AND NOT IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
							IF HAS_NET_TIMER_EXPIRED(MPGlobals.VehicleData.iImpoundTimer, 6000,TRUE)
								PRINT_HELP_WITH_NUMBER("PLYVEHIMP_NEWB", g_sMPTunables.iCar_impound_fee)
								NET_PRINT("[impound] TRIGGERING IMPOUND HELP") NET_NL()
								CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
							ENDIF
						ELSE
							REINIT_NET_TIMER(MPGlobals.VehicleData.iImpoundTimer)
						ENDIF
					ENDIF
					IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),FALSE)	= tempveh	
								MP_SAVE_VEHICLE_CLEAR_CAR_IMPOUNDED(CURRENT_SAVED_VEHICLE_SLOT())
								
								IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
									IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_GIVE_WANTED)
										IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
											SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),2)
											SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
											NET_PRINT("[impound] Impound wanted 2 given as player is in vehicle") NET_NL()
										ENDIF
										SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_GIVE_WANTED)
									ENDIF
								ENDIF
								
								SET_PERSONAL_VEHICLE_LOCK_STATE(tempveh)
								
								CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
								CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUNDED_WARP)
								CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
								CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
								SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_RETRIEVING_AFTER_FINE)
								
								
								NET_PRINT("[impound]   ----->Impound - Vehicle recovered no longer impounded ") NET_NL()
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_NEAR_IMPOUND)
					AND NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PAID_IMPOUND_FINE)
						IF NOT IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_PLAYER_GIVE_WANTED)
							IF MPGlobals.VehicleData.iTimesImpounded >=5
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),4)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									NET_PRINT("[impound] Impound wanted 4 given") NET_NL()
								ENDIF
							ELIF MPGlobals.VehicleData.iTimesImpounded >=4
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),3)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									NET_PRINT("[impound] Impound wanted 3 given") NET_NL()
								ENDIF
							ELIF MPGlobals.VehicleData.iTimesImpounded >=3
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),3)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									NET_PRINT("[impound] Impound wanted 2 given") NET_NL()
								ENDIF
							ELIF MPGlobals.VehicleData.iTimesImpounded >=2
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),2)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									NET_PRINT("[impound] Impound wanted 2 given") NET_NL()
								ENDIF
							ELSE
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
									SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),2)
									SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
									NET_PRINT("[impound] Impound wanted 2 given") NET_NL()
								ENDIF
							ENDIF
							SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_GIVE_WANTED)
						ENDIF
						
					ELSE
						CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_PLAYER_GIVE_WANTED)
					ENDIF
				ENDIF
			ELSE
				IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
					NET_PRINT("[impound] Last vehicle no longer impounded so clear the impound help flag") NET_NL()
					CLEAR_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_IMPOUND_HELP)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC




