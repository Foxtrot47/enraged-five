USING "rage_builtins.sch"
USING "globals.sch"

USING "net_events.sch"
USING "net_prints.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_missions_shared_cloud_data_public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   To avoid cyclic header issues, the functions that trigger broadcasts to the Shared Cloud-Loaded System
//								will be placed in here to be called by external scripts (ie: mainly Missions At Coords) so that the
//								net_missions_shared_cloud_data.sch can safely call Mission At Coords public functions.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      SERVER DATA ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if this cloud-loaded mission data slot has an active shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the slot contains an active mission, FALSE if not
FUNC BOOL Is_Shared_Cloud_Mission_Slot_In_Use(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscInUse)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the MissionID data for this cloud-loaded mission slot
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
// RETURN VALUE:		MP_MISSION_ID_DATA		The MissionID Data
//
// NOTES:	This data needs to be extracted from the stored data - it isn't already stored in MP_MISSION_ID_DATA format
FUNC MP_MISSION_ID_DATA	Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(INT paramSlot)

	MP_MISSION_ID_DATA	theMissionIdData
	
	Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
	
	IF (Is_Shared_Cloud_Mission_Slot_In_Use(paramSlot))
		theMissionIdData.idMission			= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscMissionID
		theMissionIdData.idVariation		= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscVariation
		theMissionIdData.idCreator			= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscCreatorID
		theMissionIdData.idCloudFilename	= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName
		theMissionIdData.idSharedRegID		= paramSlot
	ENDIF
	
	RETURN theMissionIdData

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a player is registered for this shared cloud-loaded mission
//
// INPUT PARAMS:		paramSlot			Shared Cloud Mission slot
//						paramPlayerID		PlayerID of player being checked
//
// NOTES:	A player registers for the mission if they are in the corona to ensure the mission details don't get cleaned up
FUNC BOOL Is_Player_Registered_For_Shared_Cloud_Mission_In_Slot(INT paramSlot, PLAYER_INDEX paramPlayerID)
	INT	playerBit	= NATIVE_TO_INT(paramPlayerID)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg, playerBit))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------
//     Client Maintenance
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the local copy of the current Shared Mission that this player is registered on
//
// INPUT PARAMS:		paramSlot			The mission slot currently being updated to check for the player being registered
//
// NOTES:	This shouldn't get cleaned up when the player unregisters for the mission - we need to keep this data as transition-safe local data
//				so that the data is still available after a transition - we would need this when replaying another player's UGC when the other
//				player has left the session
// 			Assumes all the valid pre-checks have been made, so in here we either store the data if player is registered and we haven't already, or we don't store it
PROC Maintain_Shared_Mission_Data_Local_Copy(INT paramSlot, BOOL bForce = FALSE)

	// Is player registered with the Shared Mission in this slot
	IF NOT (Is_Player_Registered_For_Shared_Cloud_Mission_In_Slot(paramSlot, PLAYER_ID()))
	AND NOT bForce
		EXIT
	ENDIF
	
	// Player is registered, so copy the details if not already copied
	MP_MISSION_ID_DATA theMissionIdData = Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(paramSlot)
	
	IF NOT (IS_STRING_NULL_OR_EMPTY(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName))
		IF (ARE_STRINGS_EQUAL(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName, theMissionIdData.idCloudFilename))
			// ...already stored
			EXIT
		ENDIF
	ENDIF
	
	// The copy data is different, so copy it
	g_sCopyRegSharedMissionData.crsmdMissionID	= theMissionIdData.idMission
	g_sCopyRegSharedMissionData.crsmdVariation	= theMissionIdData.idVariation
	g_sCopyRegSharedMissionData.crsmdCreatorID	= theMissionIdData.idCreator
	g_sCopyRegSharedMissionData.crsmdHeaderData	= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData
	
	// But use the locally stored version of the Mission Name (if available)
	g_sCopyRegSharedMissionData.crsmdHeaderData.tlMissionName = g_MSCLocalLanguageText.msclldtMissionName[paramSlot]
	
	// And the locally stored version of the Description Hash
	g_sCopyRegSharedMissionData.crsmdHeaderData.iMissionDecHash = g_MSCLocalLanguageText.msclldtDescHash[paramSlot]
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Local Transition-Safe Copy Of Missions Shared Array Pos: ") NET_PRINT_INT(paramSlot)	NET_NL()
		NET_PRINT("      Mission Name    : ")	NET_PRINT(GET_MP_MISSION_NAME(g_sCopyRegSharedMissionData.crsmdMissionID))				NET_NL()
		NET_PRINT("      Variation       : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdVariation)								NET_NL()
		NET_PRINT("      Creator         : ")	NET_PRINT_INT(g_sCopyRegSharedMissionData.crsmdCreatorID)								NET_NL()
		Debug_Output_Cloud_Header_Data(g_sCopyRegSharedMissionData.crsmdHeaderData)
	#ENDIF

ENDPROC


// ===========================================================================================================
//      EVENT BROADCAST FUNCTIONS
// ===========================================================================================================




// -----------------------------------------------------------------------------------------------------------
//      Event: Register MS Cloud Basic Data
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRegisterMSCloudBasicData
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_ID_DATA			MissionIdData			// The Basic Mission Identification Data
	INT							UniqueID				// The uniqueID of the mission being registered
	VECTOR						Location				// The Mission Location
	BOOL						ForCutsceneOnly			// TRUE if the mission is being setup only to trigger a Mocap Cutscene, FALSE if triggering the mission as normal
	BOOL						HeistMidStrandCut		// TRUE if the cutscene is for a Heist Mid-Strand cutscene, FALSE if not (ie: or for a Heist Intro Cutscene)
	BOOL						HeistTutorialCut		// TRUE if this is a Tutorial Cutscene, FALSE if not (ie: not a cutscene, or not a Tutorial Cutscene)
	BOOL						SenderHasData			// TRUE if the data is available on the sender's machine, FALSE if not
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast the basic cloud registration details for a Missions Shared Cloud-loaded mission
//
// INPUT PARAMS:			paramUniqueID			The Mission Controller UniqueID for the mission (ensures that the server waits for the correct set of data)
//							paramCoords				The missions coords as setup, not necessarily as downloaded from the cloud
//							paramForCutsceneOnly	TRUE if this shared mission is for triggering a cutscene only, FALSE if it triggers the mission (as normal)
//							paramHeistMidStrandCut	TRUE if this cutscene is a Heist Mid-Strand Cutscene, FALSE if not (ie: if it is a Heist Intro Cutscene)
//							paramHeistTutorialCut	TRUE if this cutscene is a Heist Tutorial Cutscene, FALSE if not (ie: not a cutscene, or not a tutorial cutscene)
//							paramHasDataLocally		TRUE if the mission details are available on this local machine, otherwise FALSE
// RETURN PARAMS:			refMissionDetails		The mission details that identify which cloud-loaded header data requires to be stored

PROC Broadcast_Register_MS_Cloud_Basic_Data(MP_MISSION_ID_DATA &refMissionDetails, INT paramUniqueID, VECTOR paramCoords, BOOL paramForCutsceneOnly, BOOL paramHeistMidStrandCut, BOOL paramHeistTutorialCut, BOOL paramHasDataLocally)
	
	// Store the Broadcast data
	m_structEventRegisterMSCloudBasicData Event
	
	Event.Details.Type				= SCRIPT_EVENT_REGISTER_MS_CLOUD_BASIC_DATA
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.MissionIdData				= refMissionDetails
	Event.UniqueID					= paramUniqueID
	Event.Location					= paramCoords
	Event.ForCutsceneOnly			= paramForCutsceneOnly
	Event.HeistMidStrandCut			= paramHeistMidStrandCut
	Event.HeistTutorialCut			= paramHeistTutorialCut
	Event.SenderHasData				= paramHasDataLocally
	
	// Broadcast the event - TODO: THIS SHOULD GO TO THE HOST ONLY
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ")	NET_PRINT_TIME() NET_PRINT(" Broadcast_Register_MS_Cloud_Basic_Data")										NET_NL()
		NET_PRINT("      UniqueID      : ") 			NET_PRINT_INT(paramUniqueID)																				NET_NL()
		NET_PRINT("      MissionID     : ") 			NET_PRINT(GET_MP_MISSION_NAME(refMissionDetails.idMission))													NET_NL()
		NET_PRINT("      Variation     : ")				NET_PRINT_INT(refMissionDetails.idVariation)																NET_NL()
		NET_PRINT("      CreatorID     : ")				NET_PRINT(Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(refMissionDetails))				NET_NL()
		NET_PRINT("      Cloud Filename: ")				NET_PRINT(refMissionDetails.idCloudFilename)																NET_NL()
		NET_PRINT("      Shared RegID  : ")				NET_PRINT_INT(refMissionDetails.idSharedRegID)																NET_NL()
		NET_PRINT("      Location      : ")				NET_PRINT_VECTOR(paramCoords)																				NET_NL()
		NET_PRINT("      For Cutscene  : ")
		IF (paramForCutsceneOnly)
			NET_PRINT("[TRUE] - Corona is setup to trigger a Mocap cutscene only, not a mission (ie: Heist Pre-Planning)") NET_NL()
		ELSE
			NET_PRINT("[FALSE] - Corona isn't setup to trigger only a Mocap cutscene only") NET_NL()
		ENDIF
		NET_PRINT("      Mid-Strand Cut: ")
		IF (paramHeistMidStrandCut)
			NET_PRINT("[TRUE] - Mocap Cutscene Corona is for a Heist Mid-Strand Cutscene") NET_NL()
		ELSE
			NET_PRINT("[FALSE] - Mocap Cutscene Corona isn't for a Heist Mid-Strand Cutscene") NET_NL()
		ENDIF
		NET_PRINT("      Tutorial Cut  : ")
		IF (paramHeistTutorialCut)
			NET_PRINT("[TRUE] - Mocap Cutscene Corona is for a Heist Tutorial Cutscene") NET_NL()
		ELSE
			NET_PRINT("[FALSE] - Mocap Cutscene Corona isn't for a Heist Tutorial Cutscene") NET_NL()
		ENDIF
		NET_PRINT("      Has Local Data: ")
		IF (paramHasDataLocally)
			NET_PRINT("[TRUE] - Mission Data is available on this local machine") NET_NL()
		ELSE
			NET_PRINT("[FALSE] - Mission Data is not available on this local machine") NET_NL()
		ENDIF
	#ENDIF

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Unregister MS Cloud Mission
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventUnregisterMSCloudMission
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							SharedRegID				// The Shared Cloud-Loaded Mission Registration ID
	TEXT_LABEL_23				CloudFilename			// The cloud filename
	INT							UniqueID				// The uniqueID of the mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast the request to unregister the player from a Missions Shared Cloud-loaded mission
//
// INPUT PARAMS:			paramSharedRegID			The Shared Cloud-Loaded Missions RegistrationID
//							paramCloudFilename			The Cloud Filename
//							paramUniqueID				The Mission Controller UniqueID for the mission (ensures that the server waits for the correct set of data)

PROC Broadcast_Unregister_MS_Cloud_Mission(INT paramSharedRegID, TEXT_LABEL_23 paramCloudFilename, INT paramUniqueID)
	
	// Store the Broadcast data
	m_structEventUnregisterMSCloudMission Event
	
	Event.Details.Type				= SCRIPT_EVENT_UNREGISTER_MS_CLOUD_MISSION
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.SharedRegID				= paramSharedRegID
	Event.CloudFilename				= paramCloudFilename
	Event.UniqueID					= paramUniqueID
	
	// Broadcast the event - TODO: THIS SHOULD GO TO THE HOST ONLY
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ")	NET_PRINT_TIME() NET_PRINT(" Broadcast_Unregister_MS_Cloud_Mission")				NET_NL()
		NET_PRINT("      UniqueID      : ") 			NET_PRINT_INT(paramUniqueID)														NET_NL()
		NET_PRINT("      Cloud Filename: ")				NET_PRINT(paramCloudFilename)														NET_NL()
		NET_PRINT("      Shared RegID  : ")				NET_PRINT_INT(paramSharedRegID)														NET_NL()
	#ENDIF

ENDPROC





// ===========================================================================================================
//      PRIVATE FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if the player is registered for the specified mission
//
// INPUT PARAMS:		paramPlayerID			The PlayerID
//						paramFilename			The cloud filename
//						paramUniqueID			Uniquely identify an instance of this mission being launched
// RETURN VALUE:		BOOL					TRUE if the player is registered for the specified mission, FALSE if not registered
FUNC BOOL Is_Player_Registered_For_Shared_Cloud_Mission_With_These_Details(PLAYER_INDEX paramPlayerID, TEXT_LABEL_23 paramFilename, INT paramUniqueID)

	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		RETURN FALSE
	ENDIF
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		RETURN FALSE
	ENDIF
	
	IF (paramUniqueID = NO_UNIQUE_ID)
		RETURN FALSE
	ENDIF
	
	// Search the registered missions for these mission details
	INT	tempLoop	= 0
	INT	playerBit	= NATIVE_TO_INT(paramPlayerID)
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		IF (Is_Shared_Cloud_Mission_Slot_In_Use(tempLoop))
			IF (paramUniqueID = GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscUniqueID)
			AND (ARE_STRINGS_EQUAL(paramFilename, GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscHeaderData.tlName))
				// ...found a match, so check if the player is registered for this mission
				IF (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscPlayersReg, playerBit))
					// Player is registered for this mission
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Player isn't registered for a mission with the passed-in details
	RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      PUBLIC ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Accept a request to setup Shared Mission data
//
// INPUT PARAMS:			paramUniqueID			The Mission Controller UniqueID for the mission (ensures that the server waits for the correct set of data)
//							paramCoords				The Mission Location as setup rather than as stored
//							paramForCutsceneOnly	TRUE if this shared mission is for triggering a cutscene only, FALSE if it triggers the mission (as normal)
//							paramHeistMidStrandCut	TRUE if this cutscene is a Heist Mid-Strand cutscene, FALSE if not (ie: if it is a Heist Intro Cutscene)
//							paramHeistTutorialCut	TRUE if this cutscene is a Heist Tutorial cutscene, FALSE if not (ie: not a cutscene, or not a tutorial cutscene)
// REFERENCE PARAMS:		refMissionDetails		The mission details that identify which cloud-loaded header data requires to be stored
PROC Request_Setup_Missions_Shared_Cloud_Data(MP_MISSION_ID_DATA &refMissionDetails, INT paramUniqueID, VECTOR paramCoords, BOOL paramForCutsceneOnly, BOOL paramHeistMidStrandCut, BOOL paramHeistTutorialCut)

	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Request_Setup_Missions_Shared_Cloud_Data(): ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
	#ENDIF

	// Consistency checking
	// DEBUG ONLY: Has a shared registration ID been passed in? If yes, just flag it as an error but otherwise ignore it.
	#IF IS_DEBUG_BUILD
		IF (refMissionDetails.idSharedRegID != ILLEGAL_SHARED_REG_ID)
			NET_PRINT("         INFO: Request already contains a specific SHARED REGISTRATION ID: ") NET_PRINT_INT(refMissionDetails.idSharedRegID) NET_NL()
		ENDIF
	#ENDIF
	
	// If the HeistMidStrandCut flag is TRUE then the ForCutsceneOnly flag should also be TRUE
	IF (paramHeistMidStrandCut)
	AND NOT (paramForCutsceneOnly)
		#IF IS_DEBUG_BUILD
			NET_PRINT("         ERROR: Heist Mid-Strand Cutscene hasn't got the ForCutsceneOnly flag set. Auto-Setting the flag.") NET_NL()
			SCRIPT_ASSERT("Request_Setup_Missions_Shared_Cloud_Data(): ERROR - Heist Mid-Strand Cutscene hasn't got the ForCutsceneOnly flag set. Auto-Setting the flag. Tell Keith.")
		#ENDIF
		
		paramForCutsceneOnly = TRUE
	ENDIF
	
	// If the paramHeistTutorialCut flag is TRUE then the ForCutsceneOnly flag should also be TRUE
	IF (paramHeistTutorialCut)
	AND NOT (paramForCutsceneOnly)
		#IF IS_DEBUG_BUILD
			NET_PRINT("         ERROR: Heist Tutorial Cutscene hasn't got the ForCutsceneOnly flag set. Clearing the 'tutorial' flag.") NET_NL()
			SCRIPT_ASSERT("Request_Setup_Missions_Shared_Cloud_Data(): ERROR - Heist Tutorial Cutscene hasn't got the ForCutsceneOnly flag set. Clearing the 'Tutorial' flag. Tell Keith.")
		#ENDIF
		
		paramHeistTutorialCut = FALSE
	ENDIF
	
	// Every request should have a cloud filename, so ensure it is passed in
	IF (IS_STRING_NULL_OR_EMPTY(refMissionDetails.idCloudFilename))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         ERROR: Cloud Filename was not passed-in. This is required data. Quitting.") NET_NL()
			SCRIPT_ASSERT("Request_Setup_Missions_Shared_Cloud_Data(): ERROR - The cloud filename is NULL or EMPTY. This is required identification data. Quitting Request. Check Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Every request should have a uniqueID, so ensure it is passed in
	IF (paramUniqueID = NO_UNIQUE_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("         ERROR: UniqueID is set to NO_UNIQUE_ID. This is required data. Quitting.") NET_NL()
			SCRIPT_ASSERT("Request_Setup_Missions_Shared_Cloud_Data(): ERROR - UniqueID is set to NO_UNIQUE_ID. This is required identification data. Quitting Request. Check Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// DEBUG ONLY: Highlight if this player is already registered for the shared mission - but send the registration request anyway. This local data could be a few frames out of date
	//		from the data on the server, so it's possible this player had just been removed on the server and is instantly being put back on
	#IF IS_DEBUG_BUILD
		IF (Is_Player_Registered_For_Shared_Cloud_Mission_With_These_Details(PLAYER_ID(), refMissionDetails.idCloudFilename, paramUniqueID))
			NET_PRINT("         WARNING: Player already registered for Shared Cloud Mission. UniqueID: ") NET_PRINT_INT(paramUniqueID) NET_NL()
			SCRIPT_ASSERT("Request_Setup_Missions_Shared_Cloud_Data() - Player already registered for Shared Cloud Mission. Requesting re-registration anyway. Tell Keith.")
		ENDIF
	#ENDIF
	
	// Check If this machine has these cloud-loaded header details available
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Checking if the cloud-loaded header data is locally available on this machine") NET_NL()
	#ENDIF
		
	BOOL detailsAreOnThisMachine = Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally(refMissionDetails)
	
	#IF IS_DEBUG_BUILD
		IF (detailsAreOnThisMachine)
			NET_PRINT("...KGM MP [MShared][CloudData]: Cloud-loaded header data is locally available on this machine") NET_NL()
		ELSE
			NET_PRINT("...KGM MP [MShared][CloudData]: Cloud-loaded header data is not locally available on this machine") NET_NL()
		ENDIF
	#ENDIF
	
	// Register this mission
	Broadcast_Register_MS_Cloud_Basic_Data(refMissionDetails, paramUniqueID, paramCoords, paramForCutsceneOnly, paramHeistMidStrandCut, paramHeistTutorialCut, detailsAreOnThisMachine)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Accept a request to cleanup Shared Mission data
//
// INPUT PARAMS:			paramMissionDetails		The mission details that identify which cloud-loaded header data requires to be stored
//							paramUniqueID			The Mission Controller UniqueID for the mission (ensures that the server cleans up the correct set of data)
PROC Request_Cleanup_Missions_Shared_Cloud_Data(MP_MISSION_ID_DATA paramMissionDetails, INT paramUniqueID)

	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Request_Cleanup_Missions_Shared_Cloud_Data(): ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
	#ENDIF

	// Need two of the following three bits of data to be sure I'm referencing the correct mission
	CONST_INT	REQUIRED_PIECES_OF_ID_DATA			2
	
	INT numPiecesOfIdData	= 0
	
	// Shared Reg ID
	IF (paramMissionDetails.idSharedRegID != ILLEGAL_SHARED_REG_ID)
		// ...got a Shared RegID
		numPiecesOfIdData++
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("      SHARED REG ID: [none]") NET_NL()
		#ENDIF
	ENDIF
	
	// Cloud Filename
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramMissionDetails.idCloudFilename))
		numPiecesOfIdData++
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("      CLOUD FILENAME: [none]") NET_NL()
		#ENDIF
	ENDIF
	
	// Mission Controller UniqueID
	IF (paramUniqueID != NO_UNIQUE_ID)
		numPiecesOfIdData++
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("      UNIQUE ID     : [none]") NET_NL()
		#ENDIF
	ENDIF
	
	// If there are less than two pieces of shared data then do nothing
	IF (numPiecesOfIdData < REQUIRED_PIECES_OF_ID_DATA)
		// ...ignore - thre are not enough pieces of ID data
		#IF IS_DEBUG_BUILD
			NET_PRINT("      - IGNORE: There are not enough pieces of ID data to identify a specific shared mission") NET_NL()
		#ENDIF
	ENDIF
	
	// Unregister this mission
	Broadcast_Unregister_MS_Cloud_Mission(paramMissionDetails.idSharedRegID, paramMissionDetails.idCloudFilename, paramUniqueID)

ENDPROC







