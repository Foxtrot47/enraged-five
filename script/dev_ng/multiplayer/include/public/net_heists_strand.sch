/* ===================================================================================== *\
		MISSION NAME	:	net_heists_strand.sch
		AUTHOR			:	Alastair Costley - 2015/02/05
		DESCRIPTION		:	Controls the heist strand launch board, called from pre-planning.
\* ===================================================================================== */


// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "MP_Scaleform_Functions.sch"
USING "fmmc_corona_controller.sch"
USING "net_heists_event_control.sch"
USING "net_heists_scaleform_header.sch"
USING "fmmc_mp_setup_mission.sch"
USING "fmmc_cloud_loader.sch"
USING "mp_globals_common_definitions.sch"
USING "net_heists_common.sch"
USING "freemode_header.sch"
USING "menu_cursor_3d_public.sch"


CONST_INT ci_HEIST_WAIT_FOR_STRAND_TIMEOUT		60000

TWEAK_FLOAT STRAND_MOUSE_BASE_HEIGHT	0.052	
TWEAK_FLOAT STRAND_MOUSE_GAP_ADJUST 	0.085

#IF IS_DEBUG_BUILD
//INT DEBUG_eStrandFocusPoint, DEBUG_eStrandHelpFlowState
WIDGET_GROUP_ID DEBUG_StrandPlanWidgets
WIDGET_GROUP_ID DEBUG_StrandPlanningParentWidgets
#ENDIF

#IF IS_DEBUG_BUILD
PROC DRAW_HEIST_STRAND_PLANNING_WIDGETS()

	PRINTLN("[AMEC][DEBUG] - DRAW_HEIST_PRE_PLANNING_WIDGETS - Drawing heist strand widgets.")
	
	DEBUG_StrandPlanWidgets = DEBUG_StrandPlanWidgets
	
	IF DOES_WIDGET_GROUP_EXIST(DEBUG_StrandPlanningParentWidgets)
	
		SET_CURRENT_WIDGET_GROUP(DEBUG_StrandPlanningParentWidgets)
		DEBUG_StrandPlanWidgets = START_WIDGET_GROUP("Heist Strand") 
			
		STOP_WIDGET_GROUP()
	
		CLEAR_CURRENT_WIDGET_GROUP(DEBUG_StrandPlanningParentWidgets)
	ENDIF
ENDPROC
#ENDIF


/// PURPOSE:
///    Wrapper for all prerequisite checks.
/// RETURNS:
///    TRUE if no prereqs fail.
FUNC BOOL SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE()

	// This is required because if we hit this check 4 frames after starting the heist strand board, it will begin
	// the process of loading the scaleform anyway. The point of this check to to completely prevent loading.
	IF SHOULD_HEIST_JOB_INVITE_HOLD_PLANNING_BOARD()
		PRINTLN("[TS][HJOBI][AMEC][HEIST_STRAND] - SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE - SHOULD_HEIST_JOB_INVITE_HOLD_PLANNING_BOARD = TRUE, don't allow pre-planning.")
		RETURN FALSE
	ENDIF

	IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE - IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD = FALSE")
		RETURN FALSE
	ENDIF
	
	// IF we are not in a property at all, clean up
	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE - IS_PLAYER_IN_PROPERTY = FALSE")
		RETURN FALSE
	ENDIF
	
	// If we are in our property and there is a strand active, clean up
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
	AND IS_MP_HEIST_STRAND_ACTIVE()
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE - IS_MP_HEIST_STRAND_ACTIVE = TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE - IS_PLAYER_SCTV = TRUE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns the cash value for the passed index
FUNC INT GET_HEIST_STRAND_CASH_VALUE(INT iIndex)
	SWITCH iIndex
		CASE 0	RETURN FMMC_CASH_TUTORIAL
		CASE 1	RETURN FMMC_CASH_PRISON_BREAK
		CASE 2	RETURN FMMC_CASH_BIOLAB
		CASE 3	RETURN FMMC_CASH_NARCOTICS
		CASE 4	RETURN FMMC_CASH_ORNATE_BANK
	ENDSWITCH
	
	PRINTLN("GET_HEIST_STRAND_CASH_VALUE - Unknown heist strand index: ", iIndex)
	SCRIPT_ASSERT("GET_HEIST_STRAND_CASH_VALUE - Unknown heist strand index")
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Loop through the known heist strand data slots to find out if a description hash
///    is present. Such a hash could only be there after it has been downloaded successfully.
/// RETURNS:
///    TRUE if all hashes are present.
FUNC BOOL IS_HEIST_STRAND_DATA_AVAILABLE()

	INT index	

	FOR index = 0 TO (NUM_HEIST_STRANDS-1)
		IF g_HeistStrandClient.sHeistStrandData[index].iHeistDescription = 0
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Create the initial empty strand slots on the board.
PROC CREATE_HEIST_STRAND_SLOTS_EMPTY()

	INT index
	FOR index = 0 TO (NUM_HEIST_STRANDS-1)
		SET_HEIST_STRAND_PLANNING_SLOT_EMPTY(g_HeistSharedClient.PlanningBoardIndex)												
	ENDFOR		

	// Set the title of our strand board
	SET_HEIST_STRAND_BOARD_TITLE(g_HeistSharedClient.PlanningBoardIndex, "HEIST_STR_T")
						
	// Initialise the board with the above data.
	SET_HEIST_STRAND_PLANNING_INITIALISE(g_HeistSharedClient.PlanningBoardIndex) 

	// Make sure we display the right scaleform state.
	SET_HEIST_SWITCH_TO_HEIST_STRAND_BOARD(g_HeistSharedClient.PlanningBoardIndex) 

	#IF IS_DEBUG_BUILD
		DRAW_HEIST_STRAND_PLANNING_WIDGETS()
	#ENDIF
	
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - CREATE_HEIST_PRE_PLANNING_SLOTS - Finished setting initial slot data.")
	
ENDPROC

/// PURPOSE:
///    Function to populate the board with relevant data if the Heist strands
/// PARAMS:
///    bUpdate - Pass TRUE if this is an update after the init of the board
PROC SET_HEIST_STRAND_SLOTS(BOOL bUpdate = FALSE)

	INT index
	TEXT_LABEL_63 sHeistTitle
    TEXT_LABEL_63 sHeistDescription
	TEXT_LABEL_23 tlPicture
	BOOL bAvailable
	INT iCost
	
	FOR index = 0 TO (NUM_HEIST_STRANDS-1) STEP 1
	
		sHeistTitle			= g_HeistStrandClient.sHeistStrandData[index].sHeistTitle
		sHeistDescription	= GET_HEIST_STRAND_GENERIC_DESCRIPTION(Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(index))
		bAvailable			= IS_BIT_SET(g_HeistStrandClient.iStrandAvailabilityBitSet, index)
		iCost				= g_HeistStrandClient.sHeistStrandData[index].iHeistCost
	
		IF g_HeistStrandClient.sPhotoVars[index].bSucess
			tlPicture = g_HeistStrandClient.sHeistStrandData[index].tlPicture
			PRINTLN("[AMEC][HEIST_STRAND] - SET_HEIST_STRAND_SLOTS - bSucess = TRUE for sPhotoVars index: ",index," with texture: ", tlPicture)
		ELSE
			tlPicture = ""
			PRINTLN("[AMEC][HEIST_STRAND] - SET_HEIST_STRAND_SLOTS - bSucess = FALSE for sPhotoVars index: ",index)
		ENDIF
	
		IF NOT bUpdate
			// Call ADD_SLOT if this is the first time we are populating the data
			SET_HEIST_STRAND_ADD_SLOT(g_HeistSharedClient.PlanningBoardIndex, index, sHeistTitle, sHeistDescription, tlPicture, bAvailable, iCost)
		ELSE
			// Call UPDATE_SLOT once we have already initialised the data
			SET_HEIST_STRAND_UPDATE_SLOT(g_HeistSharedClient.PlanningBoardIndex, index, sHeistTitle, sHeistDescription, tlPicture, bAvailable, iCost)
		ENDIF
	ENDFOR	
	
	// If we are currently interacting with the board we should update out highlight
	IF GET_HEIST_STRAND_STATE() = HEIST_STRAND_FLOW_UPDATE_PLANNING
		SET_HEIST_PLANNING_HIGHLIGHT_ITEM(g_HeistSharedClient.PlanningBoardIndex, INVALID_HEIST_DATA)
		SET_HEIST_PLANNING_HIGHLIGHT_ITEM(g_HeistSharedClient.PlanningBoardIndex, g_HeistStrandClient.sBoardState.iHighlight)
	ENDIF
ENDPROC


/// PURPOSE:
///    Check if any strand-board specific help-text is active. Used for distance based culling.
/// RETURNS:
///    TRUE if no strand messages are active.
FUNC BOOL IS_A_HEIST_STRAND_HELP_MESSAGE_CURRENTLY_DISPLAYING()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_NET")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Make sure that there is space for help-text before forcing an update. Prevents rapid
///    fire help-text popping.
/// RETURNS:
///    TRUE when safe to print a new help message.
FUNC BOOL IS_IT_SAFE_TO_DISPLAY_HEIST_STRAND_BOARD_HELP()

	IF IS_A_HEIST_STRAND_HELP_MESSAGE_CURRENTLY_DISPLAYING()
		RETURN FALSE
	ENDIF
	
	// Make sure to defer to any active help-text.
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Get the target context state based on a number of prerequisites. Allows for diffing current state with
///    desired state (aka dynamic label updates depending on environmental factors).
/// RETURNS:
///    INT state value.
FUNC INT GET_HEIST_STRAND_DESIRED_CONTEXT_REGISTRATION_STATE()
	
	INT iReturnState

	IF NOT IS_PLAYER_IN_CORONA() // KGM 23/5/14: Added 'active heist corona' check to cover the corona range when the player walks out. Fixes bug: 1864336
	AND NOT Does_This_Player_Have_An_Active_Heist_Corona()
	AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
	AND NOT Has_Cross_Session_Invite_Been_Recently_Accepted()
	AND NOT IS_THIS_A_SOLO_SESSION()
	AND NOT GET_HEIST_STRAND_DATA_DOWNLOAD_FAILED()
	AND NOT ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
	AND NOT IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()
	AND NOT IS_HEIST_ACTIONING_A_QUICK_JOIN_TRANSITION()
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())		// Avoid any SCTV behaviour (2234149)
	AND NOT IS_ENTERED_GAME_AS_SCTV()
	AND g_sLocalMPHeistControl.lhcContextCutscene = NEW_CONTEXT_INTENTION
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Appinternet")) < 1 // Fix for 1860441.
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
	AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) < HEIST_CUTSCENE_TRIGGER_m)
			
		// Started on call with a heist strand active (i.e. we need to also make sure we maintain existing contexts).
		IF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL() AND NOT SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW())
		OR g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board
		OR Is_Ped_Drunk(PLAYER_PED_ID())
			
			IF GET_HEIST_STRAND_STATE() = HEIST_STRAND_FLOW_UPDATE_EMPTY
				iReturnState = ci_HEIST_REG_STATE_NONE
			ELSE
				iReturnState = ci_HEIST_REG_STATE_ONCALL
			ENDIF
		
		// Heist on call is active, and no strand is active, return the behaviour for NONE (none
		// has a special case for this situation so that it doesn't obliterate everything).
		ELIF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() = ci_HEIST_REG_STATE_IDLE 
		AND AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL() 
		AND NOT SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
			
			iReturnState = ci_HEIST_REG_STATE_NONE
		
		// No on call active, offer the chance to enter on call even though there might not be an 
		// avavilable heist strand.
		ELIF GET_HEIST_STRAND_STATE() = HEIST_STRAND_FLOW_UPDATE_EMPTY
			
			iReturnState = ci_HEIST_REG_STATE_IDLE
		
		ELSE
			iReturnState = ci_HEIST_REG_STATE_DEFAULT
		ENDIF
		
	ELSE
		iReturnState = ci_HEIST_REG_STATE_NONE
	ENDIF
	
	RETURN iReturnState

ENDFUNC


/// PURPOSE:
///    Monitor for input, and automatically progress the script once detected.
PROC MONITOR_HEIST_STRAND_BOARD_INTERACTION(BOOL bAllowDpadRight = TRUE)

	INT iTargetContextState = GET_HEIST_STRAND_DESIRED_CONTEXT_REGISTRATION_STATE()
	
	SWITCH iTargetContextState
	
		CASE ci_HEIST_REG_STATE_DEFAULT
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_DEFAULT
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
				
				IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF g_HeistStrandClient.iHeistStrandContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_DEFAULT")
				REGISTER_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext, CP_MAXIMUM_PRIORITY, "HEIST_STR_BG2")
				SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_DEFAULT)
			ENDIF
			
		BREAK
		
		
		CASE ci_HEIST_REG_STATE_ONCALL
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_ONCALL
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
				
				IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF g_HeistStrandClient.iHeistStrandContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_ONCALL")
				REGISTER_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext, CP_MAXIMUM_PRIORITY, "HEIST_STR_BG")
				SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_ONCALL)
			ENDIF
			
		BREAK
		
		
		CASE ci_HEIST_REG_STATE_IDLE
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_IDLE
			OR g_HeistStrandClient.iHeistStrandContext != NEW_CONTEXT_INTENTION
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
				
				IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
			ENDIF
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				// Make sure Keith's heist strand launch logic is not in a processing state.
				IF g_sLocalMPHeistControl.lhcStage = HEIST_CLIENT_STAGE_NO_HEIST
				OR g_sLocalMPHeistControl.lhcStage = HEIST_CLIENT_STAGE_WAIT_FOR_INITIAL_DELAY_EXPIRY 
			
					PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Register new help label, target state: ci_HEIST_REG_STATE_IDLE")
					PRINT_HELP_FOREVER("HEIST_STR_BG3")
					SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_IDLE)
					
				ENDIF
			ENDIF
			
		BREAK
		

		CASE ci_HEIST_REG_STATE_NONE
			
			IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() = ci_HEIST_REG_STATE_IDLE
			AND AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL() 
			AND g_sLocalMPHeistControl.lhcContextCutscene = NEW_CONTEXT_INTENTION
			AND NOT SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW()
				// This is a special case for idle on-call, where we want to have the behaviour of NONE only while NOT on call.
				EXIT
			ENDIF
		
			IF GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Resetting saved context intention state, value: ", GET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE())
				SET_HEIST_STRAND_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
			ENDIF
			
			IF g_HeistStrandClient.iHeistStrandContext != NEW_CONTEXT_INTENTION
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Resetting context intention.")
				RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Resetting help label: HEIST_STR_BG3")
				CLEAR_HELP()
			ENDIF
			
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) < HEIST_CUTSCENE_TRIGGER_m)
		
				IF IS_IT_SAFE_TO_DISPLAY_HEIST_STRAND_BOARD_HELP()
				
					IF ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("FM_COR_HCLOUD")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF GET_HEIST_DATA_DOWNLOAD_FAILED()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_NET")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_NET")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_NET")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_WNTED")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF IS_THIS_A_SOLO_SESSION()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_SOLO")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
				ENDIF
				
			ELSE
			
				IF IS_A_HEIST_STRAND_HELP_MESSAGE_CURRENTLY_DISPLAYING()
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Strand message is ACTIVE but distance is too far, culling help.")
					CLEAR_HELP()
				ENDIF
				
			ENDIF
		
		EXIT
			
	ENDSWITCH
	
			
	IF NOT IS_PAUSE_MENU_ACTIVE() // Fix for 2165189, make sure we don't listen for inputs if the pause menu is up.
	AND NOT IS_PAUSE_MENU_RESTARTING()
	
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_HeistStrandClient.iHeistStrandContext) 
		AND HAS_CONTEXT_BUTTON_TRIGGERED(g_HeistStrandClient.iHeistStrandContext, TRUE)
		AND bAllowDpadRight
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - MONITOR_HEIST_STRAND_BOARD_INTERACTION - Content intention fired, progressiong to second stage initialisation...")
		
			FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE(TRUE)
		
			RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
			
			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
			
			SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_INIT_PLANNING)
			
			// Make sure we're not interrupted in the planning cam, also place this hear so that
			// the phone doesn't flick onto the screen for a frame or two before closing.
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			KILL_INTERACTION_MENU()
			
		ELIF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_HeistStrandClient.iHeistStrandContext)
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
			AND NOT g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board
			AND NOT Is_Ped_Drunk(PLAYER_PED_ID())
				IF REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD()
					
					RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_BG3")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Create the zoomable planning camera.
PROC CREATE_HEIST_STRAND_PLANNING_FIRST_PERSON_CAMERA(INT iCameraPos, BOOL bDisableShake)

	SET_HEIST_FPS_CAM_FAR(g_HeistStrandClient.sBoardCamData)

	INIT_FIRST_PERSON_CAMERA(	g_HeistStrandClient.sBoardCamData.sFPSCamData, 
								GET_CAMERA_POSITION_FROM_INT(iCameraPos), 
								GET_CAMERA_ROTATION_FROM_INT(ci_HEIST_CAMERA_ROT_GENERIC), 
								GET_CAMERA_FOV_FROM_INT(iCameraPos, GET_IS_WIDESCREEN(), TRUE), 
								g_HeistStrandClient.sBoardCamData.sFPSCamData.iLookXLimit, 
								g_HeistStrandClient.sBoardCamData.sFPSCamData.iLookYLimit, 
								g_HeistStrandClient.sBoardCamData.sFPSCamData.iRollLimit, 
								g_HeistStrandClient.sBoardCamData.sFPSCamData.fMaxZoom, 
								TRUE, 0, -1, bDisableShake)
	
	g_HeistStrandClient.iCameraLocationIndex = iCameraPos

	SET_WIDESCREEN_BORDERS(TRUE,0)	

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - CREATE_HEIST_STRAND_PLANNING_FIRST_PERSON_CAMERA - Created FPS cam.")
	#ENDIF

ENDPROC

PROC CREATE_HEIST_STRAND_PLANNING_BOARD_CAMERA(BOOL bDisableShake)

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - CREATE_HEIST_STRAND_PLANNING_BOARD_CAMERA - Requesting FPS cam, disable shake: ", bDisableShake)
	CREATE_HEIST_STRAND_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, bDisableShake)

ENDPROC

/// PURPOSE:
///    Update the board based on player input
PROC UPDATE_STRAND_INPUT_SELECTION(HEIST_HIGHLIGHT_TYPE eHighlightType)
	
	BOOL bUpdateHighlight = FALSE
	
	// Handle the input type, if the player is moving up and down the list capping at the max number of strands
	SWITCH eHighlightType
		CASE HEIST_HIGHLIGHT_UP
			g_HeistStrandClient.sBoardState.iHighlight--
			
			IF g_HeistStrandClient.sBoardState.iHighlight < ci_HEIST_HIGHLIGHT_STRAND_START_INDEX
				g_HeistStrandClient.sBoardState.iHighlight = ci_HEIST_HIGHLIGHT_STRAND_START_INDEX + (NUM_HEIST_STRANDS-1)
			ENDIF
			
			bUpdateHighlight = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_STRAND_AUDIO] - UPDATE_STRAND_INPUT_SELECTION - UP - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
		BREAK
		
		CASE HEIST_HIGHLIGHT_DOWN
			g_HeistStrandClient.sBoardState.iHighlight++
			
			IF g_HeistStrandClient.sBoardState.iHighlight >= (ci_HEIST_HIGHLIGHT_STRAND_START_INDEX + NUM_HEIST_STRANDS)
				g_HeistStrandClient.sBoardState.iHighlight = ci_HEIST_HIGHLIGHT_STRAND_START_INDEX
			ENDIF
			
			bUpdateHighlight = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_STRAND_AUDIO] - UPDATE_STRAND_INPUT_SELECTION - DOWN - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
		BREAK
		
		CASE HEIST_HIGHLIGHT_MOUSE
		
			bUpdateHighlight = TRUE
		
		BREAK
	
	ENDSWITCH
	
	IF bUpdateHighlight
		// Update our highlight on the strand board and clear any previous displayed help
		SET_HEIST_PLANNING_HIGHLIGHT_ITEM(g_HeistSharedClient.PlanningBoardIndex, g_HeistStrandClient.sBoardState.iHighlight)
		
		// Update our help text as we scroll over rather than only on the button press
		IF IS_BIT_SET(g_HeistStrandClient.iStrandAvailabilityBitSet, (g_HeistStrandClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_STRAND_START_INDEX))
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_STRAND_INPUT_SELECTION - Selectable, clear any help text")
			CLEAR_HELP()
		ELSE
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_H0")
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_STRAND_INPUT_SELECTION - Not selectable, display unavailable help text: HEIST_STR_H0")
				PRINT_HELP_FOREVER("HEIST_STR_H0")
			ENDIF
		ENDIF		
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of it's own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sScaleformStruct - 
PROC UPDATE_HEIST_STRAND_SCALEFORM_INSTRUCTIONS(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct)
									//INT								iSeconds)
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't run the instructional buttons if the text chat box is up.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	IF g_HeistStrandClient.bUpdateInstButtons
	OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		
		REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sScaleformStruct)
		
		IF NOT GET_IS_WIDESCREEN()
		OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
			SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sScaleformStruct, 0.5)
		ELSE
			SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sScaleformStruct, 0.7)
		ENDIF
		
		SWITCH g_HeistStrandClient.sBoardState.eFocusPoint
		
			// Camera focus point is the map.
			CASE HEIST_FOCUS_MAP
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE)
				
				// No look around and zoom using mouse
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL, "HEIST_IB_LOOK", sScaleformStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y, "HEIST_IB_ZOOM", sScaleformStruct, TRUE)
				ENDIF
				
				BREAK
				
			// Camera focus point is he general board / player list.	
			CASE HEIST_FOCUS_BOARD
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD, "HEIST_IB_NAV", sScaleformStruct)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_HEIST", sScaleformStruct, TRUE)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE)
				
				// No look around and zoom using mouse
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL, "HEIST_IB_LOOK", sScaleformStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y, "HEIST_IB_ZOOM", sScaleformStruct, TRUE)
				ENDIF
								
				BREAK
		
		ENDSWITCH
		
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(	g_HeistSharedClient.PlanningInstButtonsIndex,
												spPlacementLocation,
												sScaleformStruct,
												SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sScaleformStruct))
												
		g_HeistStrandClient.bUpdateInstButtons = FALSE		
	ELSE

		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(	g_HeistSharedClient.PlanningInstButtonsIndex,
												spPlacementLocation,
												sScaleformStruct,
												FALSE)
												
	ENDIF
								
ENDPROC

/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location. (may want to move pre planning into common)
PROC HEIST_STRAND_CAM_MOVE_LEFT()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_STRAND] - HEIST_STRAND_CAM_MOVE_LEFT - Move heist strand camera left. Cam location: ", g_HeistStrandClient.iCameraLocationIndex)
	#ENDIF
			
	FLOAT fPreviousBaseFOV = g_HeistStrandClient.sBoardCamData.sFPSCamData.fBaseCamFov	// Save local copy to pass into Trigger.
	
	SWITCH g_HeistStrandClient.iCameraLocationIndex
	
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_OVERVIEW
			g_HeistStrandClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_MAP_TUTORIAL  
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistStrandClient.sBoardState.eFocusPoint = HEIST_FOCUS_MAP
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_STRAND] - HEIST_PRE_PLAN_CAM_MOVE_LEFT - Playing Zoom_Left.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Zoom_Left","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			SET_HEIST_FPS_CAM_NEAR(g_HeistStrandClient.sBoardCamData)
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistStrandClient.sBoardCamData, ci_HEIST_CAMERA_POS_MAP_TUTORIAL, DEFAULT, fPreviousBaseFOV, cf_HEIST_CAM_MAX_ZOOM_FAR)
			
			g_HeistStrandClient.bUpdateInstButtons = TRUE
			
			CLEAR_HELP()
			
			BREAK
			
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location. (Again, may want to make pre planning common)
PROC HEIST_STRAND_CAM_MOVE_RIGHT()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_STRAND] - HEIST_PRE_PLAN_CAM_MOVE_RIGHT - Move heist pre-plan camera right. Cam location: ", g_HeistStrandClient.iCameraLocationIndex)
	#ENDIF
			
	FLOAT fPreviousBaseFOV = g_HeistStrandClient.sBoardCamData.sFPSCamData.fBaseCamFov	// Save local copy to pass into Trigger.
	
	g_HeistStrandClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_OVERVIEW  //ci_HEIST_CAMERA_POS_BOARD
	PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
	
	// Set the highlighting so that that PLAYER list is selected, and player 0 is set for navigation.
	g_HeistStrandClient.sBoardState.iSubHighlight = 0
	g_HeistStrandClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD 
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_STRAND] - HEIST_PRE_PLAN_CAM_MOVE_RIGHT - Playing Zoom_Right.")
	#ENDIF
	PLAY_SOUND_FRONTEND(-1,"Zoom_Right","DLC_HEIST_PLANNING_BOARD_SOUNDS")

	SET_HEIST_FPS_CAM_FAR(g_HeistStrandClient.sBoardCamData)
	TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistStrandClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW, DEFAULT, fPreviousBaseFOV, cf_HEIST_CAM_MAX_ZOOM_NEAR)  //ci_HEIST_CAMERA_POS_BOARD)
		
	g_HeistStrandClient.bUpdateInstButtons = TRUE
	
	CLEAR_HELP()	
ENDPROC


/// PURPOSE:
///    Basic functionality taken from launch of pre planning. 
PROC LAUNCH_HEIST_STRAND_FROM_BOARD()

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
	INT iSelectedStrand
	iSelectedStrand = (g_HeistStrandClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_STRAND_START_INDEX)
	g_HeistStrandClient.iSelectedStrandIndex = iSelectedStrand
	
	PRINTLN("[AMEC][HEIST_STRAND_AUDIO] - LAUNCH_HEIST_STRAND_FROM_BOARD - HEIST_CONTROL_A - Attempting to launch strand: ", iSelectedStrand, ". Setting iSelectedStrandIndex to: ", g_HeistStrandClient.iSelectedStrandIndex)
	
	IF iSelectedStrand >= 0
	AND iSelectedStrand < NUM_HEIST_STRANDS
	
		IF IS_BIT_SET(g_HeistStrandClient.iStrandAvailabilityBitSet, iSelectedStrand)
	
			// If the player can not afford this setup cost for the strand, display relevant help text
			IF NOT (NETWORK_CAN_SPEND_MONEY(g_HeistStrandClient.sHeistStrandData[iSelectedStrand].iHeistCost, FALSE, TRUE, FALSE))
				
				PRINTLN("[AMEC][HEIST_STRAND_AUDIO] - LAUNCH_HEIST_STRAND_FROM_BOARD - HEIST_CONTROL_A - Can not afford strand setup cost: ", iSelectedStrand, ", iCost = ", g_HeistStrandClient.sHeistStrandData[iSelectedStrand].iHeistCost)
				
				PRINT_HELP_WITH_NUMBER("HCUT_NOCASH", g_HeistStrandClient.sHeistStrandData[iSelectedStrand].iHeistCost, HEIST_SEMI_PERMANENT_HELP_TIME_msec)			
			ELSE
				// Set up the strand flow through Keith's function.
				Setup_Specific_Heist_Strand_Using_FinaleRCID(Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(iSelectedStrand))
				PRINTLN("[AMEC][HEIST_STRAND_AUDIO] - LAUNCH_HEIST_STRAND_FROM_BOARD - HEIST_CONTROL_A - Launched strand: ", iSelectedStrand, ", ContID = ", Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(iSelectedStrand))
							
				VECTOR vHeistCoronaLocation
				GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
				
				IF IS_SOCIAL_CLUB_ACTIVE()
					PRINTLN("[AMEC][HEIST_STRAND] - LAUNCH_HEIST_STRAND_FROM_BOARD - Calling CLOSE_SOCIAL_CLUB_MENU().")
					CLOSE_SOCIAL_CLUB_MENU()		
				ENDIF
				
				IF NOT IS_TRANSITION_ACTIVE()
					SET_SKYFREEZE_FROZEN()
					TRIGGER_CORONA_CAMERA_BLUR(TRUE)
				ELSE
					PRINTLN("[AMEC][HEIST_STRAND] - LAUNCH_HEIST_STRAND_FROM_BOARD - Warning! Transition is active when launching mission. g_b_IsInTransition: ", g_b_IsInTransition)
				ENDIF
					
				IF g_HeistStrandClient.iHeistStrandContext != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(g_HeistStrandClient.iHeistStrandContext)
				ENDIF
					
				CLEAN_HEIST_STRAND_PLANNING_LITE(FALSE)	
				
				// Go to wait stage
				g_HeistStrandClient.tActivateStrandTimeout = GET_NETWORK_TIME()
				SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_WAIT_FOR_STRAND)
				
				// Clear our feed as do not need it now
				THEFEED_FLUSH_QUEUE()
				
				// Display spinner
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("")
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
				
				SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(TRUE)
			ENDIF
		ELSE
		
			PRINTLN("[AMEC][HEIST_STRAND] - LAUNCH_HEIST_STRAND_FROM_BOARD - Warning! Unable to launch heist strand: ", iSelectedStrand, ", iStrandAvailabilityBitSet = ", g_HeistStrandClient.iStrandAvailabilityBitSet)
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_H0")
				PRINT_HELP_FOREVER("HEIST_STR_H0")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Get the inputs for the heist pre-planning board. Safe to call every frame.
PROC UPDATE_HEIST_STRAND_PLANNING_INPUTS_AND_CAMERA()

	// Fix for 2165189, make sure we don't listen for inputs if the pause menu is up.
	IF IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	// Fix for 2271532, make sure we don't listen for inputs if the player is actioning an invite.
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
	OR IS_WARNING_MESSAGE_ACTIVE()
		EXIT
	ENDIF
	
	FLOAT fBaseHeight
	FLOAT fGapFudgeFactor
			
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_PLANNING_INPUTS_AND_CAMERA")
	
	HEIST_CONTROL_TYPE eHeistControlType = HEIST_CONTROL_NONE
	
	BOOL bAllowLook = TRUE
	BOOL bAllowZoom = TRUE
	
	IF IS_USING_KEYBOARD_AND_MOUSE( FRONTEND_CONTROL)
	
		//FLOAT fBaseHeight = 0.052		// For 7 item lists
		//FLOAT fGapFudgeFactor = 0.085   // For 7 Item lists

		SET_MOUSE_CURSOR_THIS_FRAME()
		
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_X )
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_Y )
		
		UPDATE_MENU_CURSOR_GLOBALS()
								
		// Default mouse pointer
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
			
		// Board mouse navigation
		IF g_HeistStrandClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
		
		//DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBER",  ENUM_TO_INT(g_HeistPrePlanningClient.sBoardState.eFocusPoint))
		
			bAllowLook = FALSE
			bAllowZoom = FALSE
			
			fBaseHeight = STRAND_MOUSE_BASE_HEIGHT
			fGapFudgeFactor = STRAND_MOUSE_GAP_ADJUST
			
//			DISPLAY_TEXT_WITH_FLOAT(0.1, 0.2, "NUMBER", fBaseHeight, 3 )
//			DISPLAY_TEXT_WITH_FLOAT(0.1, 0.3, "NUMBER", fGapFudgeFactor, 3 )
			
			// Detect menu items.
			FLOAT fYGap = (g_HeistSharedClient.vWorldSize.z - fGapFudgeFactor) / NUM_HEIST_STRANDS
			FLOAT fY
			
			INT iHighlightedItem = -1
			
			VECTOR vBoardDimensions =  g_HeistSharedClient.vWorldSize
			
			vBoardDimensions.x *= 0.60 // Adjust size for heist planning board.
			
			// Work out where the menu items are
			INT i = 0
			WHILE i < NUM_HEIST_STRANDS
				
				// Detect if mouse is inside the item area.
				fY = fBaseHeight + (i * fYGap)	
				IF IS_CURSOR_INSIDE_3D_MENU_ITEM( 0.006, fY, 0.948, 0.050, g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, vBoardDimensions )	
					iHighlightedItem = i			
				ENDIF
				
				++ i
			
			ENDWHILE
		
			
			// Highlighting a valid item.
			IF iHighlightedItem > -1
			
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
					
				// Mouse click
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
					// Select new item
					IF iHighlightedItem != g_HeistStrandClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_STRAND_START_INDEX
					
						g_HeistStrandClient.sBoardState.iHighlight = iHighlightedItem + ci_HEIST_HIGHLIGHT_STRAND_START_INDEX
						UPDATE_STRAND_INPUT_SELECTION(HEIST_HIGHLIGHT_MOUSE)
						
					ELSE
						// Confirm current item
						eHeistControlType = HEIST_CONTROL_A
					ENDIF
				
				ENDIF
				
					
			ENDIF
			
		ENDIF
				
		// Cancel
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			eHeistControlType = HEIST_CONTROL_B
		ENDIF
		
		// Mouse and keyboard support

	ENDIF
		
	// Ensure the camera keeps moving smoothly to the target destination.
	MAINTAIN_SMOOTH_CAM_TRANSITION(g_HeistStrandClient.sBoardCamData, bAllowLook, bAllowZoom)
	
	// Gamepad control if no mouse detected.
	IF eHeistControlType = HEIST_CONTROL_NONE
		eHeistControlType = GET_HEIST_INPUTS_THIS_FRAME_ONLY(g_HeistPlanningClient.sBoardInputData)
	ENDIF
	
			
	SWITCH eHeistControlType
	
		DEFAULT
		
			BREAK
	
		CASE HEIST_CONTROL_UP
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_PLANNING_INPUTS_AND_CAMERA - HEIST_CONTROL_UP")
		
			IF g_HeistStrandClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
				// Scroll up an entry.  
				UPDATE_STRAND_INPUT_SELECTION(HEIST_HIGHLIGHT_UP)
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_DOWN
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_PLANNING_INPUTS_AND_CAMERA - HEIST_CONTROL_DOWN")
			
			IF g_HeistStrandClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
				// Scroll down an entry.  
				UPDATE_STRAND_INPUT_SELECTION(HEIST_HIGHLIGHT_DOWN)
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_B
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_STRAND_AUDIO] - UPDATE_HEIST_PRE_PLANNING_INPUTS_AND_CAMERA - HEIST_CONTROL_B - Playing Highlight_Cancel.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")

			CLEAN_HEIST_STRAND_PLANNING_LITE(TRUE)
			SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_UPDATE_BACKGROUND)
			BREAK
			
		CASE HEIST_CONTROL_A
		
			IF g_HeistStrandClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
					
					
				LAUNCH_HEIST_STRAND_FROM_BOARD()				
			ENDIF
			
			BREAK
	

// Map view no longer needed following discussion with Katie / Rob.
//		CASE HEIST_CONTROL_LB
//		
//			IF g_HeistStrandClient.sBoardState.eFocusPoint != HEIST_FOCUS_MAP	//	Fix for B*2185642 - RowanJ
//				
//				// Ensure camera on overview when switching to map
//				g_HeistStrandClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_OVERVIEW
//				HEIST_STRAND_CAM_MOVE_LEFT()
//			ENDIF
//		
//			BREAK
//			
//		CASE HEIST_CONTROL_RB
//
//			HEIST_STRAND_CAM_MOVE_RIGHT()
//		
//			BREAK
	
	ENDSWITCH
	
ENDPROC












/// PURPOSE:
///    Manage loading the scaleform movies used for heist strand planning.
/// RETURNS:
///    TRUE once loaded.
FUNC BOOL LOAD_HEIST_STRAND_PLANNING_SCALEFORM(BOOL bBackgroundOnly = FALSE)

	IF NOT LOAD_HEIST_SCALEFORM_MOVIES(bBackgroundOnly)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_SCALEFORM - Waiting on heist scaleform movies.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_SCALEFORM - Heist planning scaleform successfully loaded. Board index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningBoardIndex),", Map index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningMapIndex))
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vBoardPosition, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vBoardRotation, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vBoardSize 		= <<1.0, 1.0, 1.0>>
	g_HeistSharedClient.vWorldSize 		= <<4.025, 2.300, 1.0>>
	
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vMapPosition, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vMapRotation, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vMapSize = <<HEIST_MAP_SIZE_X, HEIST_MAP_SIZE_Y, HEIST_MAP_SIZE_Z>>
	g_HeistSharedClient.vMapWorldSize = <<HEIST_MAP_WORLD_SIZE_X, HEIST_MAP_WORLD_SIZE_Y, HEIST_MAP_WORLD_SIZE_Z>>

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_SCALEFORM - Scaleform movie for strand planning board has loaded.")
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Request the audio data for the heist planning board.
/// RETURNS:
///    TRUE once loaded.
FUNC BOOL LOAD_HEIST_STRAND_PLANNING_AUDIO()

	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_AUDIO - Still loading audio bank.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Download the heist titles and description hashes into local global data.
/// RETURNS:
///    TRUE when hash & title data is available.
FUNC BOOL IS_HEIST_STRAND_HEADER_DATA_PARSED()	

	INT index, iRContID
	INT iMissionIndexes[NUM_HEIST_STRANDS]
	BOOL bDownloaded = TRUE
	
	IF NOT IS_HEIST_STRAND_DATA_AVAILABLE()
	
		FOR index = 0 TO (NUM_HEIST_STRANDS-1)
			iRContID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(index)
			IF iRContID != 0
				iMissionIndexes[index] = iRContID
			ELSE
				bDownloaded = FALSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_HEADER_DATA_PARSED - ",index," - Failed to locate heist finale in lookup. Have the tunables been updated?")
			ENDIF
		ENDFOR
		
		GET_INDEXES_OF_MULTIPLE_HEIST_MISSIONS(iMissionIndexes)
		
		FOR index = 0 TO (NUM_HEIST_STRANDS-1)
			IF iMissionIndexes[index] != Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(index)
			AND iMissionIndexes[index] > INVALID_HEIST_DATA
			AND iMissionIndexes[index] < FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
				g_HeistStrandClient.sHeistStrandData[index].sHeistTitle = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndexes[index]].tlMissionName
				g_HeistStrandClient.sHeistStrandData[index].iHeistDescription = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndexes[index]].iMissionDecHash
				g_HeistStrandClient.sHeistStrandData[index].iPhotoVersion = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndexes[index]].iPhotoVersion
				g_HeistStrandClient.sHeistStrandData[index].iHeistCost = CALCULATE_HEIST_STRAND_ENTRY_FEE(GET_HEIST_STRAND_CASH_VALUE(index), Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(index), TRUE)
				
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_HEADER_DATA_PARSED - ",index," - Found strand data in slot. iMissionIndex: ", iMissionIndexes[index])
			ELSE
				bDownloaded = FALSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_HEADER_DATA_PARSED - ",index," - Failed to find strand data. iMissionIndex: ", iMissionIndexes[index])
			ENDIF
		ENDFOR
		
	ENDIF
	
	RETURN bDownloaded

ENDFUNC


/// PURPOSE:
///    Check if the description hash has been resolved by code.
/// RETURNS:
///    TRUE if description straing available.
FUNC BOOL IS_HEIST_STRAND_DESCRIPTION_READY()
	
	// TODO: For strands we may not need to do this as we use the generic descriptions
	
	BOOL bDescriptionReady = TRUE
	
	INT index
	FOR index = 0 TO (NUM_HEIST_STRANDS-1)
		IF g_HeistStrandClient.sHeistStrandData[index].iHeistDescription != 0
		
			IF NOT (REQUEST_AND_LOAD_CACHED_DESCRIPTION_FOR_HEIST(g_HeistStrandClient.sHeistStrandData[index].iHeistDescription, 
														g_HeistStrandClient.sHeistStrandData[index].sMissionDescLoadVars))
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_DESCRIPTION_READY - Waiting on sub HASH: ", g_HeistStrandClient.sHeistStrandData[index].iHeistDescription)
				bDescriptionReady = FALSE
			ELSE
				IF NOT g_HeistStrandClient.sHeistStrandData[index].sMissionDescLoadVars.bSucess
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_DESCRIPTION_READY - ERROR! REQUEST_AND_LOAD_CACHED_DESCRIPTION returned TRUE, but bSucess = FALSE. Hash: ", g_HeistStrandClient.sHeistStrandData[index].iHeistDescription)
					bDescriptionReady = FALSE
				ENDIF 									
			ENDIF
		ENDIF
	ENDFOR
	
	IF bDescriptionReady
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_DESCRIPTION_READY - All description strings are available; beginning dump: ")
			STRING sText
			FOR index = 0 TO (NUM_HEIST_STRANDS-1)
				sText = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_HeistStrandClient.sHeistStrandData[index].iHeistDescription, INVITE_MAX_DESCRIPTION_LENGTH)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - IS_HEIST_STRAND_DESCRIPTION_READY ...  String[",index,"]:	", sText)
			ENDFOR
		#ENDIF
	ENDIF
	
	RETURN bDescriptionReady
	
ENDFUNC


/// PURPOSE:
///    Load the pictures for heist pre-planning missions.
/// PARAMS:
///    sGetUGC_content - 
/// RETURNS:
///    TRUE once loaded.
FUNC BOOL LOAD_HEIST_STRAND_PLANNING_PICTURES(BOOL bBackgroundDownload)
	
	// Make sure that we don't keep hammering the download once it has finished.
	IF bBackgroundDownload
		IF NOT GET_HEIST_STRAND_RETRY_DOWNLOAD_PICTURES()
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_PICTURES - bBackgroundDownload = TRUE, bKeepDownloadingPictures = FALSE. Background picture download is complete.")
			RETURN FALSE
		ENDIF
	ENDIF 
	
	INT index
	BOOL bTextureLoading
	
	FOR index = 0 TO (NUM_HEIST_STRANDS-1)
	
		INT iStrandRContID = Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(index)
		TEXT_LABEL_23 contentID = GET_CONTENTID_FROM_ROOT_CONTENTID(iStrandRContID)
		STRING sTextureName = ""

		
		// Check to make sure we're not downloading pictures that have already been downloaded.
		IF NOT g_HeistStrandClient.sPhotoVars[index].bSucess
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_PICTURES - ROW[",index,"] - !!! Loading photo for FMMC lite using ContentID: ", contentID, " - Background: ",bBackgroundDownload)	
			IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(g_HeistStrandClient.sPhotoVars[index], contentID, 0, 75, 1, TRUE)
				IF g_HeistStrandClient.sPhotoVars[index].bSucess
					sTextureName = TEXTURE_DOWNLOAD_GET_NAME(g_HeistStrandClient.sPhotoVars[index].iTextureDownloadHandle)
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_PICTURES - ROW[",index,"] - TextureName = ", sTextureName)
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_PICTURES - ROW[",index,"] - FAILED to load image, continuing without.")
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_PICTURES - ROW[",index,"] - ... still loading.")
				bTextureLoading = TRUE
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sTextureName)
			AND g_HeistStrandClient.sPhotoVars[index].bSucess
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - LOAD_HEIST_STRAND_PLANNING_PICTURES - ROW[",index,"] - Picture ",index," texture downloaded: ", sTextureName)
				g_HeistStrandClient.sHeistStrandData[index].tlPicture = sTextureName
			ENDIF
		ENDIF
		
	ENDFOR

	// There's a texture/picture still loading, go back and try again!
	IF bTextureLoading
		RETURN FALSE
	ENDIF
	

	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Maintain the request for data even when we've moved on from init if needed.
PROC PROCESS_HEIST_STRAND_BACKGROUND_DOWNLOADS()

	IF GET_HEIST_STRAND_RETRY_DOWNLOAD_HEADERS()
		IF IS_HEIST_STRAND_HEADER_DATA_PARSED()
			SET_HEIST_STRAND_RETRY_DOWNLOAD_HEADERS(FALSE)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
		ELSE
			// If we don't have header data, there's no point in attempting to get picture / text data.
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - PROCESS_HEIST_STRAND_BACKGROUND_DOWNLOADS - HEADERS - Still downloading header data...")
			EXIT
		ENDIF
	ENDIF

	IF GET_HEIST_STRAND_RETRY_DOWNLOAD_PICTURES()
		IF LOAD_HEIST_STRAND_PLANNING_PICTURES(TRUE)
			SET_HEIST_STRAND_RETRY_DOWNLOAD_PICTURES(FALSE)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
		ENDIF
	ENDIF
	
	IF GET_HEIST_STRAND_RETRY_DOWNLOAD_DESC()
		IF IS_HEIST_STRAND_DESCRIPTION_READY()
			SET_HEIST_STRAND_RETRY_DOWNLOAD_DESC(FALSE)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Initial checks for a client to see the strand board in another players property
FUNC BOOL Should_Heist_Replay_Board_Be_Displayed_For_Members()

	// If we are not in our own property we do not need to check this
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
		RETURN FALSE
	ENDIF

	// If we have an invalid player index, don't draw anything
	IF g_HeistSharedClient.iCurrentPropertyOwnerIndex = -1
		RETURN FALSE
	ENDIF
	
	// Otherwise, if the owner has replayable heists, draw
	IF GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].bitsetReplayableHeists != 0
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Load assets and create the board ready for idle or planning mode.
PROC RUN_HEIST_STRAND_FLOW_INIT(BOOL bForceOldBackgroundBoard = FALSE)

	BOOL bQuit
	
	
	// If there is already a heist available, only show the empty board as we don't want to make it confusing.
	IF NOT Should_Heist_Replay_Board_Be_Displayed_In_Apartment()
	AND NOT Should_Heist_Replay_Board_Be_Displayed_For_Members()
	OR bForceOldBackgroundBoard
	OR GET_HEIST_STRAND_DATA_DOWNLOAD_FAILED()
	
		IF NOT LOAD_HEIST_STRAND_PLANNING_SCALEFORM(TRUE)
			EXIT
		ELSE
			SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_UPDATE_EMPTY)
		ENDIF
	
	ELSE
		
		IF g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_SCALEFORM] != INVALID_HEIST_DATA
			IF NOT LOAD_HEIST_STRAND_PLANNING_SCALEFORM(FALSE)
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - WARNING! SCALEFORM NOT YET LOADED.")
				g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_SCALEFORM]++
				bQuit = TRUE
			ENDIF
		ELSE
			IF NOT LOAD_HEIST_STRAND_PLANNING_SCALEFORM(FALSE)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - !! CRITICAL ERROR !! SCALEFORM COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS, TRYING AGAIN....")
				IF NOT g_HeistStrandClient.bCriticalLoadFailure
					g_HeistStrandClient.bCriticalLoadFailure = TRUE
				ENDIF

				EXIT
			ELSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - SUCCESS! HEIST SCALEFORM LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH,"+ FRAMES.")
			ENDIF
		ENDIF
		
		// RECOVERABLE CONDITIONS.
		
		IF g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_AUDIO] != INVALID_HEIST_DATA
			IF NOT LOAD_HEIST_STRAND_PLANNING_AUDIO()
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - WARNING! AUDIO BANK NOT YET LOADED.")
				g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_AUDIO]++
				bQuit = TRUE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - !! ERROR !! AUDIO BANK COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		ENDIF
		
		IF g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_UGC] != INVALID_HEIST_DATA
			IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - WARNING! UGC INITIAL DATA NOT YET LOADED.")
				g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_UGC]++
				bQuit = TRUE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - !! ERROR !! UGC INITIAL DATA COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		ENDIF
		
		IF g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_HEADER] != INVALID_HEIST_DATA
			IF NOT IS_HEIST_STRAND_HEADER_DATA_PARSED()
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - WARNING! HEIST HEADER DATA NOT YET PARSED.")
				g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_HEADER]++
				bQuit = TRUE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - !! ERROR !! HEADER DATA COULD NOT BE PARSED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
			CLEAN_ALL_HEIST_STRAND_PLANNING()
			CLEAN_HEIST_SHARED_DATA()
			SET_HEIST_STRAND_DATA_DOWNLOAD_FAILED(TRUE)
			EXIT
		ENDIF
		
		IF g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_DESCRIPTIONS] != INVALID_HEIST_DATA
			IF NOT IS_HEIST_STRAND_DESCRIPTION_READY()
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - WARNING! DESCRIPTION NOT YET LOADED.")
				#ENDIF
				g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_DESCRIPTIONS]++
				
				IF NOT GET_HEIST_STRAND_RETRY_DOWNLOAD_DESC()
					bQuit = TRUE
				ELSE
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - GET_HEIST_PREP_RETRY_DOWNLOAD_DESC = TRUE, move on from desc download.")
					#ENDIF
				ENDIF
				
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - !! ERROR !! DESCRIPTION COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
			#ENDIF
		ENDIF
		
		IF g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_PICTURES] != INVALID_HEIST_DATA
			IF NOT LOAD_HEIST_STRAND_PLANNING_PICTURES(FALSE)
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - WARNING! MISSION PICTURES NOT YET LOADED.")
				g_HeistStrandClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_PICTURES]++
				
				IF NOT GET_HEIST_STRAND_RETRY_DOWNLOAD_PICTURES()
					bQuit = TRUE
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - GET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES = TRUE, move on from picture download.")
				ENDIF
				
			ENDIF
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - !! ERROR !! MISSION PICTURES COULD NOT BE FULLY LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		ENDIF
		
		INT index
		FOR index = 0 TO COUNT_OF(g_HeistStrandClient.iPreLoadAttempts)-1
			IF g_HeistStrandClient.iPreLoadAttempts[index] >= MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_LOW
				
				IF index = ci_HEIST_PRE_LOAD_ORDER_PICTURES
					
					IF NOT GET_HEIST_STRAND_RETRY_DOWNLOAD_PICTURES()
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - Triggering background download of picture data.")
						SET_HEIST_STRAND_RETRY_DOWNLOAD_PICTURES(TRUE)
					ENDIF
					
				ELIF index = ci_HEIST_PRE_LOAD_ORDER_HEADER
					
					IF NOT GET_HEIST_STRAND_RETRY_DOWNLOAD_HEADERS()
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - Triggering background download of header data.")
						SET_HEIST_STRAND_RETRY_DOWNLOAD_HEADERS(TRUE)
					ENDIF
					
				ELIF index = ci_HEIST_PRE_LOAD_ORDER_DESCRIPTIONS
					
					IF NOT GET_HEIST_STRAND_RETRY_DOWNLOAD_DESC()
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - Triggering background download of description data.")
						SET_HEIST_STRAND_RETRY_DOWNLOAD_DESC(TRUE)
					ENDIF
					
				ELSE
					
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT - Item [",index,", ",GET_HEIST_PREPLAN_INIT_STATE_NAME(index),"] exceeded lower limit for download attempts, granting ",(MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH - g_HeistStrandClient.iPreLoadAttempts[index])," additional frames.")
					IF g_HeistStrandClient.iPreLoadAttempts[index] >= MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH
						g_HeistStrandClient.iPreLoadAttempts[index] = INVALID_HEIST_DATA
					ENDIF
					
				ENDIF
			ENDIF
		ENDFOR
		
		// Check the recoverable conditions. If any have failed, try them again next frame.
		IF bQuit
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT -		INFO	 - WAITING ON RECOVERABLE CONDITION...")
			EXIT
		ENDIF
		
		// Reset the array incase the local player abandons/restarts a new heist strand quickly.
		FOR index = 0 TO COUNT_OF(g_HeistStrandClient.iPreLoadAttempts)-1
			g_HeistStrandClient.iPreLoadAttempts[index] = 0
		ENDFOR
		
		// Grab the current availability of the strands for replay
		IF AM_I_LEADER_OF_HEIST(TRUE)
			g_HeistStrandClient.iStrandAvailabilityBitSet = GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].bitsetReplayableHeists
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT -		INFO	 - Leader. Available strands = ", g_HeistStrandClient.iStrandAvailabilityBitSet)
		ELSE
			IF g_HeistSharedClient.iCurrentPropertyOwnerIndex != -1
				g_HeistStrandClient.iStrandAvailabilityBitSet = GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].bitsetReplayableHeists
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT -		INFO	 - Member. Available strands = ", g_HeistStrandClient.iStrandAvailabilityBitSet)
			#IF IS_DEBUG_BUILD
			ELSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT -		INFO	 - g_HeistSharedClient.iCurrentPropertyOwnerIndex = -1")
			#ENDIF
			ENDIF
		ENDIF
		
		CREATE_HEIST_STRAND_SLOTS_EMPTY()
		
		SET_HEIST_STRAND_SLOTS(TRUE)
		
		SET_HEIST_UPDATE_AVAILABLE(TRUE)
		
		SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_UPDATE_BACKGROUND)
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Draw the background board and check if we need to show the replay board
PROC RUN_HEIST_STRAND_FLOW_UPDATE_EMPTY()

	MAINTAIN_HEIST_BACKGROUND_BOARD()
	
	IF AM_I_LEADER_OF_HEIST(TRUE)
		MONITOR_HEIST_STRAND_BOARD_INTERACTION(FALSE)
	ENDIF

	// Check if our state ever changes
	IF GET_HEIST_UPDATE_AVAILABLE()
	
		IF Should_Heist_Replay_Board_Be_Displayed_In_Apartment()
		OR Should_Heist_Replay_Board_Be_Displayed_For_Members()
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_UPDATE_EMPTY - Replay Board now available")
		
			// Remove our shared data
			CLEAN_ALL_HEIST_PRE_PLANNING()
			CLEAN_HEIST_SHARED_DATA(TRUE)
		
			SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_INIT)
		ENDIF
		
		// Clear the update
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC

/// PURPOSE:
///    Monitors if any data needs to be refreshed on the board (Currently only checks availability)
PROC UPDATE_HEIST_STRAND_BOARD()

	// Confirm the property owner is valid before continuing
	IF g_HeistSharedClient.iCurrentPropertyOwnerIndex = -1
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_BOARD - g_HeistSharedClient.iCurrentPropertyOwnerIndex = -1")
		EXIT
	ENDIF

	// Check if our availability has changed (local v BD Data)
	IF g_HeistStrandClient.iStrandAvailabilityBitSet != GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].bitsetReplayableHeists

		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_BOARD - Availability updated from: ", g_HeistStrandClient.iStrandAvailabilityBitSet, " to: ", GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].bitsetReplayableHeists)
	
		// Update our local knowledge
		g_HeistStrandClient.iStrandAvailabilityBitSet = GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].bitsetReplayableHeists

		// Apply the update to the board
		SET_HEIST_STRAND_SLOTS(TRUE)
	ENDIF
	
	IF GET_HEIST_UPDATE_AVAILABLE()
	
		// Check if the availability of our board has changed
		IF NOT Should_Heist_Replay_Board_Be_Displayed_In_Apartment()
		AND NOT Should_Heist_Replay_Board_Be_Displayed_For_Members()
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_BOARD - Replay Board no longer valid. Check to see if we need to clean up")
		
			// Are we in a invalid init state for this current board
			IF GET_HEIST_STRAND_STATE() != HEIST_STRAND_FLOW_NONE
				
				// Clean up the entire board so we can initialise the background / pre planning board
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - UPDATE_HEIST_STRAND_BOARD - Replay Board no longer valid. Cleanup")
				SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_CLEANUP)
				EXIT
			ENDIF
		ENDIF
		
		// Update our slots
		SET_HEIST_STRAND_SLOTS(TRUE)		
		
		// Clean up our flag
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF
ENDPROC


PROC RUN_HEIST_STRAND_FLOW_UPDATE_BACKGROUND()

	PROCESS_HEIST_STRAND_BACKGROUND_DOWNLOADS()
	
	IF AM_I_LEADER_OF_HEIST(TRUE)
		MONITOR_HEIST_STRAND_BOARD_INTERACTION()
	ENDIF
	
	UPDATE_HEIST_STRAND_BOARD()
	
	DRAW_HEIST_SCALEFORM()
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC


PROC RUN_HEIST_STRAND_FLOW_INIT_PLANNING(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct)

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT_PLANNING - Starting strand planning initialisation.")

	g_HeistStrandClient.iCameraLocationIndex 				= ci_HEIST_CAMERA_POS_BOARD
	g_HeistStrandClient.sBoardCamData.iCamCurrentTarget 	= ci_HEIST_CAMERA_POS_BOARD
	g_HeistStrandClient.sBoardState.eFocusPoint 			= HEIST_FOCUS_BOARD

	CLEAR_ALL_BIG_MESSAGES()
	
	RUN_HEIST_PLANNING_IN_PROGRESS_EVERY_FRAME_COMMANDS()
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION)
	
	SET_ALL_HEIST_HIGHLIGHTS(g_HeistStrandClient.sBoardState, ci_HEIST_HIGHLIGHT_STRAND_START_INDEX) // Confirm highlighter start value with Alistair M.

	// Set the highlight
	SET_HEIST_PLANNING_HIGHLIGHT_ITEM(g_HeistSharedClient.PlanningBoardIndex, g_HeistStrandClient.sBoardState.iHighlight)

	// Set up our new instructional buttons
	UPDATE_HEIST_STRAND_SCALEFORM_INSTRUCTIONS(scaleformStruct)

	// All of the loading is complete, start the help timer if required.
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT_PLANNING - bFPSCamActive = FALSE, creating FPS cam.")
	CREATE_HEIST_STRAND_PLANNING_BOARD_CAMERA(FALSE)

	// Keep the board alive during transition.
	DRAW_HEIST_SCALEFORM()	
	
	SET_HEIST_UPDATE_AVAILABLE(TRUE)
	
	// Update our help text
	IF NOT IS_BIT_SET(g_HeistStrandClient.iStrandAvailabilityBitSet, (g_HeistStrandClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_STRAND_START_INDEX))
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_STR_H0")
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT_PLANNING - Not selectable, display unavailable help text: HEIST_STR_H0")
			PRINT_HELP_FOREVER("HEIST_STR_H0")
		ENDIF
	ENDIF	
	
	g_HeistStrandClient.bUpdateInstButtons = TRUE
	
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_INIT_PLANNING - Progressing to strand planning mode.")
	
	// Build instructional buttons here.
	
	SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_UPDATE_PLANNING)

ENDPROC


PROC RUN_HEIST_STRAND_FLOW_UPDATE_PLANNING(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct)

	RUN_HEIST_PLANNING_IN_PROGRESS_EVERY_FRAME_COMMANDS()
	
	// Disable any background HUD elements that might get in the way. Must be called every frame.
	DISABLE_HUD_ELEMENTS_FOR_CORONA(FALSE)

	PROCESS_HEIST_STRAND_BACKGROUND_DOWNLOADS()

	// Check for inputs.
	IF NOT IS_PAUSE_MENU_ACTIVE() 	
	AND NOT IS_PAUSE_MENU_RESTARTING()
		UPDATE_HEIST_STRAND_PLANNING_INPUTS_AND_CAMERA()
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
			CLEAR_HELP()
		ENDIF
	ENDIF


	IF IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(PLAYER_ID()) 
	OR IS_PLAYER_IN_CORONA()
		
		#IF IS_DEBUG_BUILD
		IF IS_PLAYER_IN_CORONA()
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_UPDATE_PLANNING - ERROR! Heist leader is in CONFIGURE and also in a CORONA, failing over to background mode to prevent further errors.")
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_UPDATE_PLANNING - Player has enabled SCTV, returning to background update.")
		ENDIF
		#ENDIF
		
		CLEAN_HEIST_STRAND_PLANNING_LITE()
		SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_UPDATE_BACKGROUND)
		EXIT
	ENDIF

	// Render our intructional buttons
	UPDATE_HEIST_STRAND_SCALEFORM_INSTRUCTIONS(scaleformStruct)

	// Keep us up to date
	UPDATE_HEIST_STRAND_BOARD()

	DRAW_HEIST_SCALEFORM()
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC

/// PURPOSE:
///    Called each frame we are waiting for a selected strand to become active.
PROC RUN_HEIST_STRAND_FLOW_WAIT_FOR_STRAND()
	
	// Prevent the spinner for the frontend loading appearing until we are out of this state
	RUN_HEIST_PLANNING_IN_PROGRESS_EVERY_FRAME_COMMANDS()
	
	// Have a safety timeout here. 60s, kick.
	IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_HeistStrandClient.tActivateStrandTimeout)) >= ci_HEIST_WAIT_FOR_STRAND_TIMEOUT
		
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_WAIT_FOR_STRAND - Timer has expired. Cleanup player from strand board")
		
		CLEAR_HELP()
		PRINT_HELP("HEIST_ER_DL", ci_HEIST_DOWNLOAD_FAIL_TIMER)
		CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam, g_FMMC_STRUCT.iMissionType, TRUE, FALSE, FALSE)
		CORONA_CLEAN_UP_SCREEN_STATE()
		SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(FALSE)
		CLEAN_HEIST_STRAND_PLANNING_LITE(TRUE)
		SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_UPDATE_BACKGROUND)	
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - RUN_HEIST_STRAND_FLOW_WAIT_FOR_STRAND - Player is now active in heist strand. Cleanup.")
		SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_CLEANUP)
	ENDIF
ENDPROC

PROC RUN_HEIST_STRAND_FLOW_CLEANUP()

	CLEAN_HEIST_SHARED_DATA()
	CLEAN_ALL_HEIST_STRAND_PLANNING()
	
	SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_NONE)
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC


PROC MANAGE_HEIST_STRAND_STATE(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct, BOOL bForceOldBackgroundBoard = FALSE)
	
	SWITCH GET_HEIST_STRAND_STATE()
	
		CASE HEIST_STRAND_FLOW_NONE
			
			// For now, move us into init
			SET_HEIST_STRAND_STATE(HEIST_STRAND_FLOW_INIT)
			BREAK
			
		CASE HEIST_STRAND_FLOW_INIT
		
			RUN_HEIST_STRAND_FLOW_INIT(bForceOldBackgroundBoard)
		
			BREAK
			
		CASE HEIST_STRAND_FLOW_UPDATE_EMPTY
			
			RUN_HEIST_STRAND_FLOW_UPDATE_EMPTY()
		
			BREAK
			
		CASE HEIST_STRAND_FLOW_UPDATE_BACKGROUND
			
			RUN_HEIST_STRAND_FLOW_UPDATE_BACKGROUND()
		
			BREAK
			
		CASE HEIST_STRAND_FLOW_INIT_PLANNING
		
			RUN_HEIST_STRAND_FLOW_INIT_PLANNING(scaleformStruct)
		
			BREAK
			
		CASE HEIST_STRAND_FLOW_UPDATE_PLANNING
		
			RUN_HEIST_STRAND_FLOW_UPDATE_PLANNING(scaleformStruct)
		
			BREAK
			
		CASE HEIST_STRAND_FLOW_WAIT_FOR_STRAND
			
			RUN_HEIST_STRAND_FLOW_WAIT_FOR_STRAND()
			
			BREAK
			
		CASE HEIST_STRAND_FLOW_CLEANUP
		
			RUN_HEIST_STRAND_FLOW_CLEANUP()
		
			BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Maintain method for the heist strand planning board; main entry point.
/// PARAMS:
///    scaleformStruct - Scaleform struct from FMMC_Launcher.
PROC CONTROL_FM_HEIST_STRAND_BOARD(	SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct #IF IS_DEBUG_BUILD , WIDGET_GROUP_ID wgParentWidgetGroup #ENDIF , BOOL bForceOldBackgroundBoard = FALSE)

	#IF IS_DEBUG_BUILD
		DEBUG_StrandPlanningParentWidgets = wgParentWidgetGroup
		
		// Allow players to run with a commandline to disable the replay board
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_disableHeistReplayBoard")
			IF NOT g_HeistStrandClient.bDebugDisableBoard
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - CONTROL_FM_HEIST_STRAND_BOARD - sc_disableHeistReplayBoard exists. Disable Replay Board")
				g_HeistStrandClient.bDebugDisableBoard = TRUE
			ENDIF
		ENDIF
		
	#ENDIF

	IF NOT bForceOldBackgroundBoard
		IF NOT SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE()
			IF GET_HEIST_STRAND_STATE() != HEIST_STRAND_FLOW_NONE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_STRAND] - CONTROL_FM_HEIST_STRAND_BOARD - SHOULD_HEIST_STRAND_PLANNING_BE_ACTIVE = FALSE, reset strand data.")
				CLEAN_HEIST_SHARED_DATA()
				CLEAN_ALL_HEIST_STRAND_PLANNING()
			ENDIF
			EXIT
		ENDIF
	ENDIF

	MANAGE_HEIST_STRAND_STATE(scaleformStruct, bForceOldBackgroundBoard)
	
ENDPROC


// End of file.
