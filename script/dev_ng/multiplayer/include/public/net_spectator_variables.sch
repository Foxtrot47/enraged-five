
// ====================================================================================
// ====================================================================================
//
// Name:        net_spectator_variables.sch
// Description: Declaration of all spectator variables
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================

//USING "globals.sch"
//USING "net_hud_activating.sch"
//USING "commands_decorator.sch"
USING "types.sch"
USING "net_script_timers.sch"
USING "FMMC_SCTV_TICKER.sch"
// =================
// 	Spectator Camera
// =================


CONST_INT 	SPEC_FADE_LOAD_TIME_SHORT							3000
CONST_INT 	SPEC_FADE_LOAD_TIME_NORMAL							8000
CONST_INT 	SPEC_FADE_LOAD_TIME_LONG							10000
CONST_INT 	ciARBITRARY_DELAY_TO_ALLOW_THE_WORLD_TO_STREAM_IN	6000
CONST_INT	SPEC_FADE_SHORT_DISTANCE_SQR						2500//50sqr
CONST_INT	SPEC_FADE_NORMAL_DISTANCE_SQR						90000//300sqr

CONST_FLOAT	SPEC_FADE_LOAD_SPEED_LIMIT							10.0
CONST_FLOAT	SPEC_FADE_LOAD_DISTANCE								50.0
CONST_FLOAT SPEC_FADE_LOAD_DISTANCE_SQR							2500.0

CONST_INT SPEC_CLEAR_SPECIAL_CAMERA_DELAY_TIME					3000

//SPECTATOR_CAM_DATA.iBitSet
CONST_INT SPEC_CAM_BS_INITIAL_TARGET_SET						0
CONST_INT SPEC_CAM_BS_TURNED_ON_CAM								1
CONST_INT SPEC_CAM_BS_RADAR_LOCKED								2
CONST_INT SPEC_CAM_BS_SPECTATOR_HIDDEN							3
CONST_INT SPEC_CAM_BS_THIS_ACTIVE								4
CONST_INT SPEC_CAM_BS_THIS_RUNNING								5
CONST_INT SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING					6
CONST_INT SPEC_CAM_BS_THIS_WANT_TO_LEAVE						7
CONST_INT SPEC_CAM_BS_REDO_LIST									8
CONST_INT SPEC_CAM_BS_HIDE_LOCKED								9
CONST_INT SPEC_CAM_BS_CHECK_LOAD_SCENE							10
CONST_INT SPEC_CAM_BS_FADE_IN_FOR_SWOOP_CALLED					11
CONST_INT SPEC_CAM_BS_INIT_DEATH_CHECK							12
CONST_INT SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP			13
CONST_INT SPEC_CAM_BS_SET_UP_DEATH_FILTER						14
CONST_INT SPEC_CAM_BS_USING_SPECIAL_CAMERA						15
CONST_INT SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR				16
CONST_INT SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET			17
CONST_INT SPEC_CAM_BS_USE_RADIO_CONTROLS						18
CONST_INT SPEC_CAM_DISABLE_ALL_CONTROLS							19

//Spectator Cam Activation Flags Indices - used to designate special properties to a spectator cam
//Must be identical to ASCF_* in net_spectator_cam.sch
CONST_INT ASCF_INDEX_SPECTATE_LOCAL 							0	//Allows the spectator cam to add the local machine's player ped to acceptable targets
CONST_INT ASCF_INDEX_HIDE_LOCAL_PLAYER							1	//Hides the local player ped if alive
CONST_INT ASCF_INDEX_QUIT_SCREEN								2	//Allows the player to press circle and flag a desire to leave the event using a quit screen
CONST_INT ASCF_INDEX_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED	3	//Blocks spectator cam switching target if there are no players suitable to spectate, used in survival as no suitable players means event over
CONST_INT ASCF_INDEX_CELEBRATION_SCREEN							4	//Does not fade out, allows celebration screen to cover loading

//Spectator Cam Deactivation Flags Indices - used to designate special properties to a spectator cam shutting down
//Must be identical to DSCF_* in net_spectator_cam.sch
CONST_INT DSCF_INDEX_FADE_BACK_TO_PLAYER 		0			//Spectator cam will fade and transition back to the player ped before shutting down
CONST_INT DSCF_INDEX_REMAIN_HIDDEN				1			//Player remains hidden and frozen at the origin once cleaned up
CONST_INT DSCF_INDEX_REMAIN_VIEWING_TARGET		2			//Machine remains focussed on the last spectated ped, instead of returning to the local player
CONST_INT DSCF_INDEX_SWOOP_UP					3			//Turns off once having swooped up into the sky for leaderboard
CONST_INT DSCF_INDEX_BAIL_FOR_TRANSITION		4			//Freezes the spectator in place, invisible with no collision at the last camera coords ready for sky swoop up into transition
CONST_INT DSCF_INDEX_DONT_CLEANUP_TIMECYCLES	5			//Does not clean up any timecycles that have been set
CONST_INT DSCF_INDEX_PAUSE_RENDERPHASE			6			//Instantly pauses the renderphase first


/// PURPOSE: List of stages in the fade switch between two spectate targets



// =================
// 	Spectator HUD
// =================


CONST_INT NUMBER_OF_TIMES_TO_DISPLAY_HELP					1

CONST_INT TIME_SWITCH_PROTECT_VALUE 0//5000
CONST_INT TIME_INSTRUCTIONAL_BUTTON_VALUE 500

CONST_INT NUMBER_OF_SPEC_HUD_FILTERS 22
CONST_INT SPEC_HUD_FILTER_CHANGE_TIME 200


//SPECTATOR_HUD_DATA.iBitSet
CONST_INT SPEC_HUD_BS_ACTIVE									0
CONST_INT SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS		1
CONST_INT SPEC_HUD_BS_WAITING_FOR_SWITCH						2
CONST_INT SPEC_HUD_BS_DISABLED									3
CONST_INT SPEC_HUD_BS_SPEC_TEXT_HAS_BEEN_USED					4
CONST_INT SPEC_HUD_BS_REQUEST_UPDATE_SUMMARY_CARD				5
CONST_INT SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD					6
CONST_INT SPEC_HUD_BS_REQUEST_SPEC_HELP_TEXT					7
CONST_INT SPEC_HUD_BS_BLOCK_CIRCLE_INSTRUCTION					8
CONST_INT SPEC_HUD_BS_RTLT_CHANGE_TARGET						9
CONST_INT SPEC_HUD_BS_SHOW_DPAD_DOWN_MAP						10
CONST_INT SPEC_HUD_BS_SET_INITIAL_HIGHLIGHT						11
CONST_INT SPEC_HUD_BS_SHOW_FEED									12
CONST_INT SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD			13
CONST_INT SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD_END		14
CONST_INT SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING		15
CONST_INT SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN			16
CONST_INT SPEC_HUD_BS_DO_HEADSHOT_TIMED_UPDATE					17
CONST_INT SPEC_HUD_BS_HIDE_FEED_DUE_TO_LIST_SIZE				18
CONST_INT SPEC_HUD_BS_EXTERNAL_FADE_FLAG_READY					19
CONST_INT SPEC_HUD_BS_NO_VALID_TARGET_FADE_OUT					20

CONST_INT SPEC_HUD_NUMBER_OF_NEWS_TICKER						4
CONST_INT SPEC_HUD_NEWS_TICKER_TIME_UPDATE						5000
CONST_INT SPEC_HUD_NUM_PLAYERS_TO_HIDE_FEED						8

CONST_INT HUD_TICKER_FREEMODE_STAGES		2
CONST_INT HUD_TICKER_FREEMODE_1				8000
CONST_INT HUD_TICKER_FREEMODE_2				8000

CONST_INT HUD_TICKER_JOB_STAGES				3
CONST_INT HUD_TICKER_JOB_1					8000
CONST_INT HUD_TICKER_JOB_2					8000
CONST_INT HUD_TICKER_JOB_3					4000

CONST_INT HUD_TICKER_PLAYLIST_STAGES		4
CONST_INT HUD_TICKER_PLAYLIST_1				8000
CONST_INT HUD_TICKER_PLAYLIST_2				8000
CONST_INT HUD_TICKER_PLAYLIST_3				4000
CONST_INT HUD_TICKER_PLAYLIST_4				4000

CONST_INT ciSWITCH_TARGET_DELAY				1000
CONST_INT ciSWITCH_TARGET_DELAY_OVERTIME 	8000

// =================
// 	Spectator Data Struct
// =================


