

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// Weapon swapping ///////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_prints.sch"
USING "script_player.sch"
USING "script_network.sch"
USING "net_include.sch"
USING "net_ticker_logic.sch"


USING "weapons_public.sch"


/// PURPOSE:
///    The next set of functions get and set WEAPON_SWAPPING_DATA for a slot, based on the game mode
/// PARAMS:
///    iSlot - which slot (global player BD slot)
FUNC INT GET_WEAPON_SWAP_INTERACTING_WITH(INT iSlot)
	NET_PRINT(" WEAPON_SWAP - GET_WEAPON_SWAP_INTERACTING_WITH = ") NET_PRINT_INT(GlobalplayerBD[iSlot].weaponSwappingData.iPlayerInteractingWith) NET_NL()	
	RETURN(GlobalplayerBD[iSlot].weaponSwappingData.iPlayerInteractingWith)
ENDFUNC

PROC SET_WEAPON_SWAP_INTERACTING_WITH(INT iSlot, INT iPlayer)
	NET_PRINT(" WEAPON_SWAP - SET_WEAPON_SWAP_INTERACTING_WITH = ") NET_PRINT_INT(iPlayer) NET_NL()	
	GlobalplayerBD[iSlot].weaponSwappingData.iPlayerInteractingWith = iPlayer	
ENDPROC

FUNC PLAYER_INDEX GET_PLAYER_WEAPON_SWAP_INTERACTING_WITH(INT iSlot)
	NET_PRINT(" WEAPON_SWAP - GET_PLAYER_WEAPON_SWAP_INTERACTING_WITH = ") NET_PRINT_INT(GlobalplayerBD[iSlot].weaponSwappingData.iPlayerInteractingWith) NET_NL()	
	RETURN NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()))))
ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_SWAP_WEAPON(INT iSlot)
	RETURN GlobalplayerBD[iSlot].weaponSwappingData.wtWeapon	
ENDFUNC

PROC SET_WEAPON_SWAP_WEAPON(INT iSlot, WEAPON_TYPE wtWeapon)
	GlobalplayerBD[iSlot].weaponSwappingData.wtWeapon = wtWeapon	
ENDPROC

FUNC INT GET_WEAPON_SWAP_AMMO(INT iSlot)
	RETURN GlobalplayerBD[iSlot].weaponSwappingData.iAmmo		
ENDFUNC
PROC SET_WEAPON_SWAP_AMMO(INT iSlot, INT iAmmo)
	GlobalplayerBD[iSlot].weaponSwappingData.iAmmo = iAmmo	
ENDPROC

FUNC INT GET_WEAPON_SWAP_STATE(INT iSlot)
	RETURN GlobalplayerBD[iSlot].weaponSwappingData.iState	
ENDFUNC
PROC SET_WEAPON_SWAP_STATE(INT iSlotWeapon, INT iStateWeapon)
	GlobalplayerBD[iSlotWeapon].weaponSwappingData.iState = iStateWeapon	
ENDPROC

FUNC BOOL GET_WEAPON_SWAP_SHOWING_HELP(INT iSlot)
	RETURN GlobalplayerBD[iSlot].weaponSwappingData.bShowingSwapHelp		
ENDFUNC

PROC SET_WEAPON_SWAP_SHOWING_HELP(INT iSlot, BOOL bShowingSwapHelp)
	GlobalplayerBD[iSlot].weaponSwappingData.bShowingSwapHelp = bShowingSwapHelp
ENDPROC

FUNC BOOL IS_WEAPON_SWAP_DISABLED_FOR_PLAYER(PLAYER_INDEX iPlayer)
	IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(iPlayer)) = 6
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WEAPON_SWAP_DISABLED()
	RETURN IS_WEAPON_SWAP_DISABLED_FOR_PLAYER(PLAYER_ID())
ENDFUNC

PROC DISABLE_WEAPON_SWAP()
	SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 6)
	NET_PRINT_STRINGS(" WEAPON_SWAP - disabled on local player called from :", GET_THIS_SCRIPT_NAME()) NET_NL()	
	
ENDPROC

PROC ENABLE_WEAPON_SWAP()
	SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 0)
	NET_PRINT_STRINGS(" WEAPON_SWAP - enabled on local player called from:", GET_THIS_SCRIPT_NAME()) NET_NL()		
ENDPROC



/// PURPOSE: Can I give this player my current weapon?
FUNC BOOL CAN_GIVE_THIS_PLAYER_MY_WEAPON(PLAYER_INDEX PlayerId)
			//IF bDebug = TRUE
			//NET_PRINT(" WEAPON_SWAP - CHECK A") NET_NL()	
			//ENDIF
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_NET_PLAYER_OK(PlayerId)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		// for now, you can't do this while in a vehicle (because the swap & driveby buttons are the same)
				IF GET_PLAYER_TEAM(PlayerId) = GET_PLAYER_TEAM(PLAYER_ID())
				OR GET_CURRENT_GAMEMODE() = GAMEMODE_FM
					IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(playerID)) = 1
						IF IS_ENTITY_AT_ENTITY(GET_PLAYER_PED(PlayerId), PLAYER_PED_ID(), << 2.5, 2.5, 3.0 >>, FALSE, TRUE, TM_ON_FOOT)
						
							IF IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), GET_ENTITY_COORDS(GET_PLAYER_PED(PlayerId)), 100)
								WEAPON_TYPE l_wtTempWeapon
								GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), l_wtTempWeapon)

								IF l_wtTempWeapon != WEAPONTYPE_INVALID
								AND l_wtTempWeapon != WEAPONTYPE_UNARMED
									IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), l_wtTempWeapon) > 0
										NET_PRINT(" WEAPON_SWAP - CAN_GIVE_THIS_PLAYER_MY_WEAPON = TRUE") NET_NL()	
										// Right, I think I can give this guy my gun
										RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE: Is the local player pressing the button for weapon swapping?
FUNC BOOL IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_PICKUP)	//(PAD1, LEFTSHOULDER1)
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())		// for now, you can't do this while in a vehicle (because the swap & driveby buttons are the same)	
			IF NOT IS_PAUSE_MENU_ACTIVE()
				IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Allow me to get anoter player's weapon, or to give another player my weapon
PROC MAINTAIN_PLAYER_WEAPON_SWAPPING(BOOL iIsNetPlayerOK)
	INT l_InteractGBD
		
	IF (iIsNetPlayerOK)
	
		// Am I in a cutscene?
		IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())) < 3
			IF NETWORK_IS_IN_MP_CUTSCENE()
				SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 7)
				NET_PRINT(" WEAPON_SWAP - in a cutscene - Go to state 7") NET_NL()	
			ENDIF
		ENDIF
		
		// Am I involved in a arrest?
		IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())) < 3
			SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 8)
			NET_PRINT(" WEAPON_SWAP - Involved in an arrest - Go to state 8") NET_NL()
		ENDIF
		
		SWITCH(GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())))
			CASE 0
				// Check for player pressing button
				IF IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 1)
					NET_PRINT(" WEAPON_SWAP - pressed button - Go to state 1") NET_NL()	
				ENDIF
			BREAK
			
			CASE 1
				// Check for player releasing button
				IF NOT IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
					NET_PRINT(" WEAPON_SWAP - released button - Go to state 5") NET_NL()	
				ENDIF
			BREAK
			
			CASE 2
				IF NOT CAN_GIVE_THIS_PLAYER_MY_WEAPON(GET_PLAYER_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID())))	//INT_TO_PLAYERINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()))), TRUE)
					// Can't give that player my weapon any more
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
					NET_PRINT(" WEAPON_SWAP - can't give - Go to state 5") NET_NL()	
				ELSE
					// Check for player pressing button
					IF IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()
						// Yes!
						
						SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 3)
						SET_WEAPON_SWAP_WEAPON(NATIVE_TO_INT(PLAYER_ID()), GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()))
						SET_WEAPON_SWAP_AMMO(NATIVE_TO_INT(PLAYER_ID()), GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), GET_WEAPON_SWAP_WEAPON(NATIVE_TO_INT(PLAYER_ID()))))
					
						NET_PRINT(" WEAPON_SWAP - have pressed state 3") NET_NL()	
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()))))
					// Reset
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
					NET_PRINT(" WEAPON_SWAP - part not there - Go to state 5") NET_NL()	
				ELSE
					l_InteractGBD = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID())))))
				
					IF GET_WEAPON_SWAP_STATE(l_InteractGBD) = 4
					AND GET_WEAPON_SWAP_INTERACTING_WITH(l_InteractGBD) = PARTICIPANT_ID_TO_INT()
						// The player I'm interacting with has taken a weapon and was interacting with me
						// Safe to assume that he has taken my weapon
						
						// Remove that weapon from me
						REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GET_WEAPON_SWAP_WEAPON(NATIVE_TO_INT(PLAYER_ID())))
													

						PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("WS_GIVEN", 
						NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID())))), 
						GET_WEAPON_NAME(GET_WEAPON_SWAP_WEAPON(NATIVE_TO_INT(PLAYER_ID()))), 
						HUD_COLOUR_WHITE)

						
						// Reset
						SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
						SET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()), -1)
						
						NET_PRINT(" WEAPON_SWAP - given weapon - Go to state 5") NET_NL()	
					ELSE
						IF NOT CAN_GIVE_THIS_PLAYER_MY_WEAPON(GET_PLAYER_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID())))	//INT_TO_PLAYERINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()))))
							// Can't give that player my weapon any more, reset
							SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
							
							NET_PRINT(" WEAPON_SWAP - can't give any more - Go to state 5") NET_NL()	
						ELSE
							// Wait
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()))))
					// Reset
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
					NET_PRINT(" WEAPON_SWAP - part not there - Go to state 5") NET_NL()	
				ELSE
					l_InteractGBD = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID())))))
				
					IF GET_WEAPON_SWAP_STATE(l_InteractGBD) != 3
					OR GET_WEAPON_SWAP_INTERACTING_WITH(l_InteractGBD) != PARTICIPANT_ID_TO_INT()
						// Reset
						SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 5)
						SET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()), -1)
						NET_PRINT(" WEAPON_SWAP - part has stopped interacting - Go to state 5") NET_NL()	
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
				IF NOT IS_PLAYER_PRESSING_WEAPON_SWAP_BUTTON()
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 0)
					NET_PRINT(" WEAPON_SWAP - part has stopped holding button - Go to state 0") NET_NL()	
				ENDIF
			BREAK
			
			CASE 6
				// swapping disabled, do nothing
			BREAK
			
			CASE 7
				// in a cutscene, wait for not being in a cutscene
				IF NOT NETWORK_IS_IN_MP_CUTSCENE()
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 0)
					NET_PRINT(" WEAPON_SWAP - no longer in a cutscene - Go to state 0") NET_NL()	
				ENDIF
			BREAK
			
			CASE 8
				// Involved in arrest, wait for not being involved in an arrest
//				IF NOT IS_PLAYER_CURRENTLY_INVOLVED_IN_ARREST(PLAYER_ID())
					SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 0)
					NET_PRINT(" WEAPON_SWAP - no longer involved in an arrest - Go to state 0") NET_NL()	
//				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())) != 0
		AND NOT IS_WEAPON_SWAP_DISABLED()
			// Reset my data
			SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 0)
			NET_PRINT(" WEAPON_SWAP - not okay - Go to state 0") NET_NL()	
		ENDIF
	ENDIF
	
	IF GET_WEAPON_SWAP_SHOWING_HELP(NATIVE_TO_INT(PLAYER_ID()))
		IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())) != 2
			CLEAR_HELP()
			//PAUSE_TEXT_QUEUE(FALSE)
			SET_WEAPON_SWAP_SHOWING_HELP(NATIVE_TO_INT(PLAYER_ID()), FALSE)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Can I offer this player a weapon, or are they offering me their weapon?
///    Called during 32 player repeat, for players that aren't me.
PROC CHECK_PLAYER_WEAPON_OFFERING(INT iParticipant, PLAYER_INDEX PlayerID)

	// Check if I can offer this player my weapon
	IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())) = 0
		IF CAN_GIVE_THIS_PLAYER_MY_WEAPON(PlayerID)
			SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 2)
			SET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()), iParticipant)
			
			PRINT_HELP_FOREVER("WS_OFFER")
			
			SET_WEAPON_SWAP_SHOWING_HELP(NATIVE_TO_INT(PLAYER_ID()), TRUE)
			
			NET_PRINT(" WEAPON_SWAP - can offer weapon - Go to state 2") NET_NL()	
		ENDIF
	ELSE
		// Check if this player is offering me their weapon
		IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID())) = 1
			IF GET_WEAPON_SWAP_STATE(NATIVE_TO_INT(playerID)) = 3
			AND GET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(playerID)) = PARTICIPANT_ID_TO_INT()
				SET_WEAPON_SWAP_INTERACTING_WITH(NATIVE_TO_INT(PLAYER_ID()), iParticipant)
				SET_WEAPON_SWAP_STATE(NATIVE_TO_INT(PLAYER_ID()), 4)

				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), 
									GET_WEAPON_SWAP_WEAPON(NATIVE_TO_INT(playerID)),
									GET_WEAPON_SWAP_AMMO(NATIVE_TO_INT(playerID)),
									TRUE)	

				PRINT_TICKER_WITH_STRING_AND_PLAYER_NAME("WS_TAKEN",
				GET_WEAPON_NAME(GET_WEAPON_SWAP_WEAPON(NATIVE_TO_INT(playerID))),
				PlayerID)
				NET_PRINT(" WEAPON_SWAP - taken weapon - Go to state 4") NET_NL()	
			ENDIF
		ENDIF
	ENDIF	

ENDPROC



