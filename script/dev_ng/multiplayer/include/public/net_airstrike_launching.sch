
// ___________________________________________
//
//	Game: 
//	------
//	GTAV.
//
//
//  Script: 
//	-------
// 	net airstrike launching.sch
// 
//
//  Author: 
//	-------
//	William.Kennedy@RockstarNorth.com
//
//
// 	Description: 
//	------------
//	Player can call contact and
// 	pay for an airstrike.
//
// ___________________________________________ 

USING "globals.sch"
USING "script_ped.sch"
USING "commands_player.sch"
USING "script_network.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "net_script_timers.sch"
USING "net_mission.sch"

USING "net_include.sch"
USING "fm_unlocks_header.sch"
USING "net_freemode_cut.sch"

ENUM eAIRSTRIKE_LAUNCHING_STATE
	eAIRSTRIKELAUNCHINGSTATE_INIT = 0,
	eAIRSTRIKELAUNCHINGSTATE_WAIT,
	eAIRSTRIKELAUNCHINGSTATE_LAUNCHING,
	eAIRSTRIKELAUNCHINGSTATE_RUNNING
ENDENUM

STRUCT STRUCT_AIRSTRIKE_LAUNCHING_DATA
	eAIRSTRIKE_LAUNCHING_STATE eState
ENDSTRUCT

#IF IS_DEBUG_BUILD

FUNC STRING GET_AIRSTRIKE_LAUNCHING_STATE_NAME(eAIRSTRIKE_LAUNCHING_STATE eState)
	
	SWITCH eState
		
		CASE eAIRSTRIKELAUNCHINGSTATE_INIT			RETURN "INIT"
		CASE eAIRSTRIKELAUNCHINGSTATE_WAIT			RETURN "WAIT"
		CASE eAIRSTRIKELAUNCHINGSTATE_LAUNCHING		RETURN "LAUNCHING"
		CASE eAIRSTRIKELAUNCHINGSTATE_RUNNING		RETURN "RUNNING"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

#ENDIF

PROC SET_AIRSTRIKE_LAUNCHING_STATE(STRUCT_AIRSTRIKE_LAUNCHING_DATA &sData, eAIRSTRIKE_LAUNCHING_STATE eNewState)
	
	#IF IS_DEBUG_BUILD
		IF sData.eState != eNewState
			NET_PRINT("[WJK] - Airstrike Launching - changing state to ")NET_PRINT(GET_AIRSTRIKE_LAUNCHING_STATE_NAME(eNewState))NET_NL()
		ENDIF
	#ENDIF
	
	sData.eState = eNewState
	
ENDPROC

PROC MAINTAIN_AIRSTRIKE_LAUNCHING(STRUCT_AIRSTRIKE_LAUNCHING_DATA &sData)
	
	SWITCH sData.eState
		
		CASE eAIRSTRIKELAUNCHINGSTATE_INIT
			SET_AIRSTRIKE_LAUNCHING_STATE(sData, eAIRSTRIKELAUNCHINGSTATE_WAIT)
		BREAK
		
		CASE eAIRSTRIKELAUNCHINGSTATE_WAIT
			IF MPGlobalsAmbience.bLaunchAirstrike
				NET_PRINT("[WJK] - Airstrike Launching - MPGlobalsAmbience.bLaunchAirstrike = TRUE.")NET_NL()
				SET_AIRSTRIKE_LAUNCHING_STATE(sData, eAIRSTRIKELAUNCHINGSTATE_LAUNCHING)
				MPGlobalsAmbience.bAirstrikeExplosionsInProgress = FALSE
			ENDIF
		BREAK
		
		CASE eAIRSTRIKELAUNCHINGSTATE_LAUNCHING
			
			// If the script is running on my machine, go to the running state.
			IF DOES_SCRIPT_EXIST("AM_airstrike")
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_airstrike")) <= 0
					
					MP_MISSION_DATA sLaunchdata
					sLaunchdata.mdID.idMission = eAIRSTRIKE
					sLaunchdata.iInstanceId = NATIVE_TO_INT(PLAYER_ID())
					
					IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(sLaunchdata)
						NET_PRINT("[WJK] - Airstrike Launching - script AM_backup_heli now running.")NET_NL()
						SET_AIRSTRIKE_LAUNCHING_STATE(sData, eAIRSTRIKELAUNCHINGSTATE_RUNNING)
					#IF IS_DEBUG_BUILD
					ELSE
						NET_PRINT("[WJK] - Airstrike Launching - launching script AM_airstrike.")NET_NL()
					#ENDIF
					ENDIF
					
				// If I am in free mode and I am not running the script, try to launch it.
				ELSE
					NET_PRINT("[WJK] - Airstrike Launching - script AM_airstrike now running.")NET_NL()
					SET_AIRSTRIKE_LAUNCHING_STATE(sData, eAIRSTRIKELAUNCHINGSTATE_RUNNING)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eAIRSTRIKELAUNCHINGSTATE_RUNNING
			IF DOES_SCRIPT_EXIST("AM_airstrike")
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_airstrike")) <= 0
					NET_PRINT("[WJK] - Airstrike Launching - script AM_airstrike has terminated.")NET_NL()
					MPGlobalsAmbience.bLaunchAirstrike = FALSE
					MPGlobalsAmbience.bAirstrikeExplosionsInProgress = FALSE
					SET_AIRSTRIKE_LAUNCHING_STATE(sData, eAIRSTRIKELAUNCHINGSTATE_INIT)
				ENDIF
			ENDIF
		BREAK
			
	ENDSWITCH
	
ENDPROC



