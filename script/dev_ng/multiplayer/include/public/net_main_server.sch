USING "rage_builtins.sch"
USING "globals.sch"
USING "net_mission_control.sch"
USING "net_activity_selector.sch"
USING "net_prints.sch"
USING "net_spawn.sch"
USING "net_crew_updates.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Main_Server.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all server-only functionality that is shared
//								between all game modes: CnC, FreeRoam, etc.
//		NOTES			:	Over time this will fill out to contain all shared functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// ===========================================================================================================
//      The Gamemode-Independent Main Server-Only Debug Routines
// ===========================================================================================================

// PURPOSE:	Generic server-only debug widgets
//
// INPUT PARAMS:		paramParentWidget				[DEFAULT=NULL] The widget group that this needs to be added to
#IF IS_DEBUG_BUILD
PROC Create_Main_Generic_Server_Widgets(WIDGET_GROUP_ID paramParentWidget = NULL)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		EXIT
	ENDIF

	// Add these to the correct widget group
	IF (DOES_WIDGET_GROUP_EXIST(paramParentWidget))
		SET_CURRENT_WIDGET_GROUP(paramParentWidget)
	ENDIF
	
	// Unset the widget group again
	IF (DOES_WIDGET_GROUP_EXIST(paramParentWidget))
		CLEAR_CURRENT_WIDGET_GROUP(paramParentWidget)
	ENDIF
	
	NET_PRINT("...KGM MP: Create_Main_Generic_Server_Widgets") NET_NL()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Destroy any server-only debug widgets
#IF IS_DEBUG_BUILD
PROC Destroy_Main_Generic_Server_Widgets()
	
	NET_PRINT("...KGM MP: Destroy_Main_Generic_Server_Widgets") NET_NL()
	
ENDPROC
#ENDIF





// ===========================================================================================================
//      The Gamemode-Independent Main Server-Only Routines
// ===========================================================================================================
// PURPOSE:	A one-off generic server-only initialisation.
// NOTES:	This is being initialised prior to registering the broadcast globals, so it should be safe for every player to initialise these without wiping out the real data
//				and should also mean the data only ever gets properly initialised when the first player joins the session.
PROC Initialise_Main_Generic_Server()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: INITIALISE_MAIN_GENERIC_SERVER (before script registered as a netwrk script)") NET_NL()
	#ENDIF

	// Mission Control System
	Initialise_MP_Mission_Control_Server()
	
	// Activity Selector System
	Initialise_MP_Activity_Selector_Server()
	
	// Missions Shared System
	Initialise_MP_Missions_Shared_Server()

ENDPROC

PROC find_next_non_spectating_participant_for_server()
	
	INT iParticipant
	PARTICIPANT_INDEX ParticipantID
	PLAYER_INDEX PlayerID
	INT i
	
	
	//Deal with Continual participant loop
	iParticipant = g_iContinualServerStaggeredParticipant + 1
	IF (iParticipant >= NUM_NETWORK_PLAYERS)
		iParticipant -= NUM_NETWORK_PLAYERS
	ENDIF	
	g_iContinualServerStaggeredParticipant_LastFrame = g_iContinualServerStaggeredParticipant
	g_iContinualServerStaggeredParticipant = iParticipant	
	g_ContinualServerStaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	g_ContinualServerStaggeredPlayer = INT_TO_NATIVE(PLAYER_INDEX, iParticipant)
	g_ContinualServerStaggeredPlayerActive = NETWORK_IS_PLAYER_ACTIVE(g_ContinualServerStaggeredPlayer)	
	
	g_ServerStaggeredPlayerActive = FALSE
	
	FOR i=1 TO NUM_NETWORK_PLAYERS
	
		iParticipant = g_iServerStaggeredParticipant + i
		IF (iParticipant >= NUM_NETWORK_PLAYERS)
			iParticipant -= NUM_NETWORK_PLAYERS
		ENDIF
		
		ParticipantID = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
			PlayerID = NETWORK_GET_PLAYER_INDEX(ParticipantID)
			IF NOT IS_PLAYER_SCTV(PlayerID)
				g_iServerStaggeredParticipant_LastFrame = g_iServerStaggeredParticipant
				g_iServerStaggeredParticipant = iParticipant	
				g_ServerStaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
				g_ServerStaggeredPlayer = PlayerID	
				g_ServerStaggeredPlayerActive = TRUE				
				EXIT
			ENDIF
		ENDIF	
			
	ENDFOR
	
ENDPROC

PROC find_next_staggered_participant_for_server()
	
	INT iParticipant

	iParticipant = g_iServerStaggeredParticipant + 1
	IF (iParticipant >= NUM_NETWORK_PLAYERS)
		iParticipant -= NUM_NETWORK_PLAYERS
	ENDIF
	
	g_iServerStaggeredParticipant_LastFrame = g_iServerStaggeredParticipant
	g_iServerStaggeredParticipant = iParticipant	
	g_ServerStaggeredParticipant = INT_TO_NATIVE(PARTICIPANT_INDEX, iParticipant)
	g_ServerStaggeredPlayer = INT_TO_NATIVE(PLAYER_INDEX, iParticipant)
	g_ServerStaggeredPlayerActive = NETWORK_IS_PLAYER_ACTIVE(g_ServerStaggeredPlayer)	
	
	g_iContinualServerStaggeredParticipant_LastFrame = g_iServerStaggeredParticipant_LastFrame
	g_iContinualServerStaggeredParticipant = g_iServerStaggeredParticipant
	g_ContinualServerStaggeredParticipant = g_ServerStaggeredParticipant
	g_ContinualServerStaggeredPlayer = g_ServerStaggeredPlayer
	g_ContinualServerStaggeredPlayerActive = g_ServerStaggeredPlayerActive
	
ENDPROC


// PURPOSE:	This procedure should be called every frame to control the generic CnC server routines.
PROC Maintain_Main_Generic_Server()

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		OPEN_SCRIPT_PROFILE_MARKER_GROUP("Maintain_Main_Generic_Server") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
	
	IF NOT (g_bStaggeredConsidersEveryone)	
		find_next_non_spectating_participant_for_server()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("find_next_non_spectating_participant_for_server()")
		#ENDIF
		#ENDIF	
	ELSE
		find_next_staggered_participant_for_server()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("find_next_staggered_participant_for_server()")
		#ENDIF
		#ENDIF
	ENDIF
	
	
	// Updates mission control routines, which decides which missions to allow to launch and which players should be on them
	Maintain_MP_Mission_Control_Server()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Mission_Control_Server()")
	#ENDIF
	#ENDIF
	
	// Updates Activity Selector routines, which chooses one mission from a group to be the 'current' mission from the group
	// NOTE: This is similar to the Grouped Missions routines but is intended to replace the Mission Selector and then hopefully integrate the Grouped Missions at a later date
	Maintain_MP_Activity_Selector_Server()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Activity_Selector_Server()")
	#ENDIF
	#ENDIF
	
	// Updates Missions Shared routines, which controls access to an array of missions shared by all players (used by Activity Selector, etc)
	Maintain_MP_Missions_Shared_Server()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("Maintain_MP_Missions_Shared_Server()")
	#ENDIF
	#ENDIF

	// updates any requests made by players to warp to a location.
	SERVER_MAINTAIN_WARP_REQUESTS() 
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("SERVER_MAINTAIN_WARP_REQUESTS()")
	#ENDIF
	#ENDIF
	

	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
	#ENDIF
	#ENDIF

ENDPROC





