//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_PRIVATE.sch																					//
// Description: Private header file containing wrapper functions for the table look ups.								//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_CASINO_NIGHTCLUB
USING "net_peds_casino_nightclub.sch"
#ENDIF
#IF FEATURE_TUNER
USING "net_peds_car_meet.sch"
USING "net_peds_auto_shop.sch"
USING "net_peds_car_meet_sandbox.sch"
#ENDIF
#IF FEATURE_MUSIC_STUDIO
USING "net_peds_music_studio.sch"
#ENDIF
#IF FEATURE_FIXER
USING "net_peds_fixer_hq.sch"
#ENDIF
#IF FEATURE_DLC_1_2022
USING "net_peds_nightclub.sch"
USING "net_peds_clubhouse.sch"
USING "net_peds_warehouse.sch"
#ENDIF
#IF FEATURE_DLC_2_2022
USING "net_peds_juggalo_hideout.sch"
#ENDIF
#IF IS_DEBUG_BUILD
USING "net_peds_debug_location.sch"
#ENDIF
USING "net_peds_island.sch"
USING "net_peds_submarine.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_LOOK_UP_TABLE(PED_LOCATIONS ePedLocation, PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH ePedLocation
		CASE PED_LOCATION_ISLAND
			BUILD_PED_ISLAND_LOOK_UP_TABLE(interface, eProc)
		BREAK
		CASE PED_LOCATION_SUBMARINE
			BUILD_PED_SUBMARINE_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#IF FEATURE_CASINO_NIGHTCLUB
		CASE PED_LOCATION_CASINO_NIGHTCLUB
			BUILD_PED_CASINO_NIGHTCLUB_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#ENDIF
		#IF FEATURE_TUNER
		CASE PED_LOCATION_CAR_MEET
			BUILD_PED_CAR_MEET_LOOK_UP_TABLE(interface, eProc)
		BREAK
		CASE PED_LOCATION_AUTO_SHOP
			BUILD_PED_AUTO_SHOP_LOOK_UP_TABLE(interface, eProc)
		BREAK
		CASE PED_LOCATION_CAR_MEET_SANDBOX	
			BUILD_PED_CAR_MEET_SANDBOX_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#ENDIF
		#IF FEATURE_MUSIC_STUDIO
		CASE PED_LOCATION_MUSIC_STUDIO
			BUILD_PED_MUSIC_STUDIO_LOOK_UP_TABLE(interface,eProc)
		BREAK
		#ENDIF
		#IF FEATURE_FIXER
		CASE PED_LOCATION_FIXER_HQ_HAWICK
		CASE PED_LOCATION_FIXER_HQ_ROCKFORD
		CASE PED_LOCATION_FIXER_HQ_SEOUL
		CASE PED_LOCATION_FIXER_HQ_VESPUCCI
			BUILD_PED_FIXER_HQ_LOOK_UP_TABLE(interface,eProc)
		BREAK
		#ENDIF
		#IF FEATURE_DLC_1_2022
		CASE PED_LOCATION_NIGHTCLUB
			BUILD_PED_NIGHTCLUB_LOOK_UP_TABLE(interface,eProc)
		BREAK
		CASE PED_LOCATION_CLUBHOUSE
			BUILD_PED_CLUBHOUSE_LOOK_UP_TABLE(interface,eProc)
		BREAK
		CASE PED_LOCATION_WAREHOUSE
			BUILD_PED_WAREHOUSE_LOOK_UP_TABLE(interface,eProc)
		BREAK
		#ENDIF
		#IF FEATURE_DLC_2_2022
		CASE PED_LOCATION_JUGGALO_HIDEOUT
			BUILD_PED_JUGGALO_HIDEOUT_LOOK_UP_TABLE(interface,eProc)
		BREAK
		#ENDIF
		#IF IS_DEBUG_BUILD
		CASE PED_LOCATION_DEBUG
			BUILD_PED_DEBUG_LOCATION_LOOK_UP_TABLE(interface, eProc)
		BREAK
		#ENDIF
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ WRAPPER FUNCTIONS ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determines if the ped script should launch if using a conditional launch.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: TRUE if the ped script should launch using a conditional launch.
FUNC BOOL SHOULD_PED_SCRIPT_LAUNCH(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SHOULD_PED_SCRIPT_LAUNCH)
	IF (lookUpTable.returnShouldPedScriptLaunch != NULL)
		RETURN CALL lookUpTable.returnShouldPedScriptLaunch()
	ENDIF
	PRINTLN("[AM_MP_PEDS] SHOULD_PED_SCRIPT_LAUNCH RETURNING FALSE.  Lookup table call is NULL.")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines if the parent script is running a simple interior.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: TRUE if the parent script is running a simple interior.
FUNC BOOL IS_PARENT_A_SIMPLE_INTERIOR(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_IS_PARENT_A_SIMPLE_INTERIOR)
	IF (lookUpTable.returnIsParentASimpleInterior != NULL)
		RETURN CALL lookUpTable.returnIsParentASimpleInterior()
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the total number of local peds used in a ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iLayout - Ped layout set by the host.
/// RETURNS: The total number of local peds used in a ped location.
FUNC INT GET_LOCAL_PED_TOTAL(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_LOCAL_PED_TOTAL)
	IF (lookUpTable.returnGetLocalPedTotal != NULL)
		RETURN CALL lookUpTable.returnGetLocalPedTotal(ServerBD, ePedLocation)
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the total number of networked peds used in a ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iLayout - Ped layout set by the host.
/// RETURNS: The total number of networked peds used in a ped location.
FUNC INT GET_NETWORK_PED_TOTAL(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_NETWORK_PED_TOTAL)
	IF (lookUpTable.returnGetNetworkPedTotal != NULL)
		RETURN CALL lookUpTable.returnGetNetworkPedTotal(ServerBD, ePedLocation)
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the total number of ped layouts used in a ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: The total number of ped layouts used in a ped location.
FUNC INT GET_SERVER_PED_LAYOUT_TOTAL(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_SERVER_PED_LAYOUT_TOTAL)
	IF (lookUpTable.returnGetServerPedLayoutTotal != NULL)
		RETURN CALL lookUpTable.returnGetServerPedLayoutTotal()
	ENDIF
	RETURN 1
ENDFUNC

/// PURPOSE:
///    Returns the server ped layout. This determines a peds coords etc.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: The server ped layout.
FUNC INT GET_SERVER_PED_LAYOUT(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_SERVER_PED_LAYOUT)
	IF (lookUpTable.returnGetServerPedLayout != NULL)
		RETURN CALL lookUpTable.returnGetServerPedLayout()
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the server ped level. All peds up to the level will be created.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: The server ped level.
FUNC INT GET_SERVER_PED_LEVEL(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_SERVER_PED_LEVEL)
	IF (lookUpTable.returnGetServerPedLevel != NULL)
		RETURN CALL lookUpTable.returnGetServerPedLevel()
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns a peds local coords base position.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: A peds local coords base position.
FUNC VECTOR GET_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_LOCAL_COORDS_BASE_POSITION)
	IF (lookUpTable.returnGetPedLocalCoordsBasePosition != NULL)
		RETURN CALL lookUpTable.returnGetPedLocalCoordsBasePosition(ePedLocation)
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Returns a peds local headings base heading.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: A peds local headings base heading.
FUNC FLOAT GET_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_LOCAL_HEADING_BASE_HEADING)
	IF (lookUpTable.returnGetPedLocalHeadingBaseHeading != NULL)
		RETURN CALL lookUpTable.returnGetPedLocalHeadingBaseHeading(ePedLocation)
	ENDIF
	RETURN -1.0
ENDFUNC

/// PURPOSE:
///    Sets the ped data for the passed in ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to populate.
///    iPed - ID to ped to populate data for.
///    iLayout - Ped layout set by the host.
///    bSetPedArea - Will set the peds area. Should only set on initialise and let culling set area thereafter.
PROC SET_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_DATA)
	IF (lookUpTable.setPedData != NULL)
		CALL lookUpTable.setPedData(ePedLocation, ServerBD, Data, iPed, iLayout, bSetPedArea)
	ENDIF
ENDPROC

/// PURPOSE:
///    Host sets the ped server data.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Ped server data to set.
PROC SET_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_SERVER_DATA)
	IF (lookUpTable.setPedServerData != NULL)
		CALL lookUpTable.setPedServerData(ePedLocation, ServerBD)
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines if the local player is inside the parent property based on the ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    playerID - Player to query.
/// RETURNS: TRUE if the local player is inside the parent property.
FUNC BOOL IS_PLAYER_IN_PARENT_PROPERTY(PED_LOCATIONS ePedLocation, PLAYER_INDEX playerID)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_IS_PLAYER_IN_PARENT_PROPERTY)
	IF (lookUpTable.returnIsPlayerInParentProperty != NULL)
		RETURN CALL lookUpTable.returnIsPlayerInParentProperty(playerID)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines if the ped active total can change in this ped location. E.g. the ped total changes based on player count.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: TRUE if the ped active total can change in this ped location.
FUNC BOOL DOES_ACTIVE_PED_TOTAL_CHANGE(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_DOES_ACTIVE_PED_TOTAL_CHANGE)
	IF (lookUpTable.returnDoesActivePedTotalChange != NULL)
		RETURN CALL lookUpTable.returnDoesActivePedTotalChange()
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the active ped level threshold. Used when we need to reduce the number of active peds while in update. E.g. the ped total changes based on player count.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iLevel - The ped level that each ped spawns at.
/// RETURNS: TRUE if the ped active total can change in this ped location.
FUNC INT GET_ACTIVE_PED_LEVEL_THRESHOLD(PED_LOCATIONS ePedLocation, INT iLevel)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_ACTIVE_PED_LEVEL_THRESHOLD)
	IF (lookUpTable.getActivePedLevelThreshold != NULL)
		RETURN CALL lookUpTable.getActivePedLevelThreshold(iLevel)
	ENDIF
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Gets the anim data of a ped activity.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iClip - Clip to get data on.
///    eMusicIntensity - For dancing peds. Populates the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    iPedID - Ped ID to query.
///    eTransitionState - Used if a ped can transition between two ped transition states.
///    bPedTransition - For non dancing peds. Whether a ped should play a transition animation.
PROC GET_PED_ANIM_DATA(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_ANIM_DATA)
	IF (lookUpTable.getPedAnimData != NULL)
		CALL lookUpTable.getPedAnimData(eActivity, pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, iPedID, eTransitionState, bPedTransition)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the prop anim data of a ped activity.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    pedPropAnimData - Struct to populate with ped prop anim data.
///    iClip - Clip to get data on.
///    eMusicIntensity - For dancing peds. Populates the correct data based on the music intensity.
///    ePedMusicIntensity - The current music intensity. Used for dancing peds.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bHeeledPed - Wheather the ped is heeled or not. Determined via ped editor widgets (bit set).
PROC GET_PED_PROP_ANIM_DATA(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_PROP_ANIM_DATA)
	IF (lookUpTable.getPedPropAnimData != NULL)
		CALL lookUpTable.getPedPropAnimData(eActivity, pedPropAnimData, iProp, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bHeeledPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the prop weapon data.
///    For if a ped needs to have a weapon equipped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    pedPropWeaponData - Struct to populate with ped prop weapon data.
///    iPed - Ped to query.
PROC GET_PED_PROP_WEAPON_DATA(PED_LOCATIONS ePedLocation, PED_PROP_WEAPON_DATA &pedPropWeaponData, INT iPed)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_PROP_WEAPON_DATA)
	IF (lookUpTable.getPedPropWeaponData != NULL)
		CALL lookUpTable.getPedPropWeaponData(pedPropWeaponData, iPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the ped properties on a local ped after its creation.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    PedID - Ped to set properties on.
PROC SET_LOCAL_PED_PROPERTIES(PED_LOCATIONS ePedLocation, PED_INDEX &PedID, INT iPed)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_LOCAL_PED_PROPERTIES)
	IF (lookUpTable.setLocalPedProperties != NULL)
		CALL lookUpTable.setLocalPedProperties(PedID, iPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the ped properties on a networked ped after its creation.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    NetworkPedID - Networked ped to set properties on.
///    iPedDataBS - Ped data bitset to query.
PROC SET_NETWORK_PED_PROPERTIES(PED_LOCATIONS ePedLocation, NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_NETWORK_PED_PROPERTIES)
	IF (lookUpTable.setNetworkPedProperties != NULL)
		CALL lookUpTable.setNetworkPedProperties(NetworkPedID, iPedDataBS)
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the ped prop indexes.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    PedID - Ped to set prop indexes on.
///    iPed - Determined by ped ID to look up correct indexes to set.
///    ePedModel - Ped model to query.
PROC SET_PED_PROP_INDEXES(PED_LOCATIONS ePedLocation, PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_PROP_INDEXES)
	IF (lookUpTable.setPedPropIndexes != NULL)
		CALL lookUpTable.setPedPropIndexes(PedID, iPed, iLayout, ePedModel)
	ENDIF
ENDPROC


/// PURPOSE:
///    Sets a peds alternative movement.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    PedID - Ped to set alternative movement on.
///    iPed - Determined by ped ID to look up correct alternative movement to set.
PROC SET_PED_ALT_MOVEMENT(PED_LOCATIONS ePedLocation, PED_INDEX &PedID, INT iPed)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_ALT_MOVEMENT)
	IF (lookUpTable.setPedAltMovement != NULL)
		CALL lookUpTable.setPedAltMovement(PedID, iPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if a ped has been created based on conditions from the ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped to query.
///    iLevel - Up to which level of peds should spawn.
/// RETURNS: TRUE if the ped has been created.
FUNC BOOL HAS_PED_BEEN_CREATED(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, INT iLevel)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_HAS_PED_BEEN_CREATED)
	IF (lookUpTable.returnHasPedBeenCreated != NULL)
		RETURN CALL lookUpTable.returnHasPedBeenCreated(Data, iLevel)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Populates a peds speech data.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    SpeechData - Speech data to set.
///    iLayout - Ped layout to set data for.
///    iArrayID - Speech ped array ID to populate data for.
///    iSpeechPedID - Array that holds the each peds speech array ID.
///    bNetworkPed - Whether the ped is networked.
PROC SET_PED_SPEECH_DATA(PED_LOCATIONS ePedLocation, SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_SPEECH_DATA)
	IF (lookUpTable.setPedSpeechData != NULL)
		CALL lookUpTable.setPedSpeechData(SpeechData, iLayout, iArrayID, iSpeechPedID, bNetworkPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks whether a ped can play dialogue this frame.
/// PARAMS:
///    PedID - Ped index to query.
///    iPed - Ped to query.
///    iLayout - Current ped layout.
/// RETURNS: TRUE if the ped can play speech.
FUNC BOOL CAN_PED_PLAY_SPEECH(PED_LOCATIONS ePedLocation, PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_CAN_PED_PLAY_SPEECH)
	IF (lookUpTable.returnCanPedPlaySpeech != NULL)
		RETURN CALL lookUpTable.returnCanPedPlaySpeech(PedID, iPed, iLayout, ePedSpeech)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks whether a ped can head track this frame. 
/// PARAMS:
///    PedID - Ped index to query.
///    iPed - Ped to query.
///    iLayout - Current ped layout.
/// RETURNS: TRUE if the ped can play speech.
FUNC BOOL CAN_PED_HEAD_TRACK(PED_LOCATIONS ePedLocation, PED_INDEX PedID, INT iPed, INT iLayout)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_CAN_PED_HEAD_TRACK)
	IF (lookUpTable.returnCanPedPlaySpeech != NULL)
		RETURN CALL lookUpTable.returnCanPedHeadTrack(PedID, iPed, iLayout)
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the peds speech type from the speech ID passed in.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iPed - Ped to query.
///    iSpeech - Speech ID to query.
/// RETURNS: The ped speech type of a ped based on the speech ID.
FUNC PED_SPEECH GET_PED_SPEECH_TYPE(PED_LOCATIONS ePedLocation, INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_SPEECH_TYPE)
	IF (lookUpTable.returnGetPedSpeechType != NULL)
		RETURN CALL lookUpTable.returnGetPedSpeechType(iPed, ePedActivity, iSpeech)
	ENDIF
	RETURN PED_SPH_INVALID
ENDFUNC

/// PURPOSE:
///    Gets the next controller speech for a ped.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eCurrentSpeech - Current speech the ped has played. Used to diff against to ensure next speech is different.
///    iPed - Ped to query.
///    ePedActivity - Current ped activity.
/// RETURNS: A valid speech type the speech controller will play on a specified ped.
FUNC PED_SPEECH GET_PED_CONTROLLER_SPEECH(PED_LOCATIONS ePedLocation, PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_CONTROLLER_SPEECH)
	IF (lookUpTable.returnGetPedControllerSpeech != NULL)
		RETURN CALL lookUpTable.returnGetPedControllerSpeech(eCurrentSpeech, iPed, ePedActivity)
	ENDIF
	RETURN PED_SPH_INVALID
ENDFUNC

/// PURPOSE:
///    Gets the convo data for a ped speech type.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    convoData - Temp convo data to populate.
///    iPed - Ped to query.
///    ePedSpeech - Speech type to get the context of.
PROC GET_PED_CONVO_DATA(PED_LOCATIONS ePedLocation, PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_CONVO_DATA)
	IF (lookUpTable.getPedConvoData != NULL)
		CALL lookUpTable.getPedConvoData(convoData, iPed, ePedActivity, ePedSpeech)
	ENDIF
ENDPROC

/// PURPOSE:
///    Populates the PED_CULLING_LOCATE_DATA struct with culling locate data depending on the PED_AREAS.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ePedArea - Which ped area is being queried,
///    pedCullingLocateData - The culling locate struct to populate.
PROC GET_PED_CULLING_LOCATE_DATA(PED_LOCATIONS ePedLocation, PED_AREAS ePedArea, PED_CULLING_LOCATE_DATA &pedCullingLocateData)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_GET_PED_CULLING_LOCATE_DATA)
	IF (lookUpTable.getPedCullingLocateData != NULL)
		CALL lookUpTable.getPedCullingLocateData(ePedArea, pedCullingLocateData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Populates a sequence index with ped patrol data.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    SequenceIndex - Sequence index to populate.
///    iLayout - Ped layout to set data for.
///    iArrayID - Patrol ped array ID to populate data for.
///    iPatrolPedID - Array that holds each peds patrol array ID.
///    bShowProp - Whether to hide or show the ped props for a sequence.
///    iTaskSet - Peds can have a max of MAX_NUM_PATROL_TASKS to perform.
///    iTask - Sequence task to query.
///    bNetworkPed - Whether the ped is networked.
PROC SET_PED_PATROL_DATA(PED_LOCATIONS ePedLocation, SEQUENCE_INDEX &SequenceIndex, INT iLayout, INT iArrayID, INT &iPatrolPedID[], BOOL &bShowProp[], INT iTask, INT &iMaxSequence, BOOL bNetworkPed = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_PATROL_DATA)
	IF (lookUpTable.setPedPatrolData != NULL)
		CALL lookUpTable.setPedPatrolData(SequenceIndex, iLayout, iArrayID, iPatrolPedID, bShowProp, iTask, iMaxSequence, bNetworkPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Populates a peds head tracking data.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    HeadtrackingData - Head tracking data to set.
///    iLayout - Ped layout to set data for.
///    iArrayID - Head tracking ped array ID to populate data for.
///    iHeadTrackingPedID - Array that holds each peds head tracking array ID.
///    bNetworkPed - Whether the ped is networked.
PROC SET_PED_HEAD_TRACKING_DATA(PED_LOCATIONS ePedLocation, HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_HEAD_TRACKING_DATA)
	IF (lookUpTable.setPedHeadTrackingData != NULL)
		CALL lookUpTable.setPedHeadTrackingData(HeadtrackingData, iLayout, iArrayID, iHeadTrackingPedID, bNetworkPed)
	ENDIF
ENDPROC
/// PURPOSE:
///    Populates a peds changed capsule size data.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    HeadtrackingData - Head tracking data to set.
///    iLayout - Ped layout to set data for.
///    iArrayID - Head tracking ped array ID to populate data for.
///    iHeadTrackingPedID - Array that holds each peds head tracking array ID.
///    bNetworkPed - Whether the ped is networked.
PROC SET_PED_CHANGED_CAPSULE_SIZE_DATA(PED_LOCATIONS ePedLocation, CHANGED_CAPSULE_SIZE_DATA &ChangedCapsuleData, INT iLayout, INT iArrayID, INT &iChangedCapsuleSizePedID[], BOOL bNetworkPed = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_CHANGED_CAPSULE_SIZE_DATA)
	IF (lookUpTable.setPedChangedCapsuleSizeData != NULL)
		CALL lookUpTable.setPedChangedCapsuleSizeData(ChangedCapsuleData, iLayout, iArrayID, iChangedCapsuleSizePedID, bNetworkPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines if a ped location uses dancing peds.
/// PARAMS:
///    ePedLocation - Which location is being queried.
/// RETURNS: TRUE if dancing peds are being used at this location.
FUNC BOOL DOES_PED_LOCATION_USE_DANCING_PEDS(PED_LOCATIONS ePedLocation)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_DOES_PED_LOCATION_USE_DANCING_PEDS)
	IF (lookUpTable.returnDoesPedLocationUseDancingPeds != NULL)
		RETURN CALL lookUpTable.returnDoesPedLocationUseDancingPeds()
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Populates a parent peds data. This includes IDs of child peds etc.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ParentPedData - Parent ped data to set.
///    iLayout - Ped layout to set data for.
///    iArrayID - Head tracking ped array ID to populate data for.
///    iParentPedID - Array that holds each parent peds array ID.
///    bNetworkPed - Whether the ped is networked.
PROC SET_PED_PARENT_DATA(PED_LOCATIONS ePedLocation, PARENT_PED_DATA &ParentPedData, INT iLayout, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SET_PED_PARENT_DATA)
	IF (lookUpTable.setPedParentData != NULL)
		CALL lookUpTable.setPedParentData(ParentPedData, iLayout, iArrayID, iParentPedID, bNetworkPed)
	ENDIF
ENDPROC

/// PURPOSE:
///    Determines if a ped should hide in a cutscene based on conditions.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local ped data to query.
///    iLayout - Ped layout to query.
///    iPed - Ped ID to query.
/// RETURNS: TRUE if a ped should hide in a cutscene.
FUNC BOOL SHOULD_HIDE_CUTSCENE_PED(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, INT iLayout, INT iPed)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_SHOULD_HIDE_CUTSCENE_PED)
	IF (lookUpTable.returnShouldHideCutscenePed != NULL)
		RETURN CALL lookUpTable.returnShouldHideCutscenePed(LocalData, iLayout, iPed)
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    An update function that is specific to the ped location. Allows more functionality for peds that can be specific to only one location, doesn't require the expansion of the whole system.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iPed - Ped ID to query.
/// RETURNS: TRUE if a ped should hide in a cutscene.
PROC RUN_LOCATION_SPECIFIC_UPDATE(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)
	PED_INTERFACE lookUpTable
	BUILD_PED_LOOK_UP_TABLE(ePedLocation, lookUpTable, E_LOCATION_SPECIFIC_UPDATE)
	IF (lookUpTable.locationSpecificUpdate != NULL)
		CALL lookUpTable.locationSpecificUpdate(LocalData, ServerBD)
	ENDIF
ENDPROC

