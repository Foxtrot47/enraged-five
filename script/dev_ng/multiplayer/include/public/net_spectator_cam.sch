

// ====================================================================================
// ====================================================================================
//
// Name:        net_spectator_cam.sch
// Description: Controls the spectator cam, contains functions for use in .sc files.
// Written By:  William Kennedy, Kenneth Ross
//
// ====================================================================================
// ====================================================================================


// =================
// 	Using headers
// =================

USING "script_player.sch"
USING "selector_public.sch"
USING "net_hud_activating.sch"
USING "net_spectator_hud.sch"
USING "net_spectator_cam_common.sch"
USING "net_big_message.sch"
USING "net_spectator_variables.sch"
USING "net_SCTV_common.sch"
USING "cheat_handler.sch"
USING "net_kill_cam.sch"
USING "shop_public.sch"
USING "shop_public.sch"
USING "net_kill_strip.sch"
USING "net_mission_details_hud.sch"
USING "fmmc_header.sch"
USING "script_misc.sch"
USING "net_simple_interior_private.sch"
USING "net_simple_interior.sch"

// =================
// 	Procs & funcs
// =================


//NEEDED FOR FINAL PRINTS, moved to outside IS_DEBUG_BUILD
/// PURPOSE:
///    turns eSPEC_FADE_SWITCH_STAGE into a debug string
/// PARAMS:
///    eStage - 
/// RETURNS:
///    eStage as a string
	FUNC STRING GET_SPEC_CAM_FADE_SWITCH_STAGE_NAME(eSPEC_FADE_SWITCH_STAGE eStage)
		SWITCH eStage
			CASE eSPECFADESWITCHSTAGE_SETUP
				RETURN "eSPECFADESWITCHSTAGE_SETUP"
			BREAK
			CASE eSPECFADESWITCHSTAGE_FADE_OUT
				RETURN "eSPECFADESWITCHSTAGE_FADE_OUT"
			BREAK
			CASE eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE
				RETURN "eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE"
			BREAK
			CASE eSPECFADESWITCHSTAGE_WAIT_FOR_LOAD_SCENE
				RETURN "eSPECFADESWITCHSTAGE_WAIT_FOR_LOAD_SCENE"
			BREAK
			CASE eSPECFADESWITCHSTAGE_FADE_IN
				RETURN "eSPECFADESWITCHSTAGE_FADE_IN"
			BREAK
			CASE eSPECFADESWITCHSTAGE_CLEANUP
				RETURN "eSPECFADESWITCHSTAGE_CLEANUP"
			BREAK
		ENDSWITCH
		RETURN "INVALID_STAGE"
	ENDFUNC


#IF IS_DEBUG_BUILD

/// PURPOSE:
///    turns eSPEC_CAM_MODE into a debug string
/// PARAMS:
///    eMode - 
/// RETURNS:
///    eMode as a string 
	FUNC STRING GET_SPEC_CAM_MODE_NAME(eSPEC_CAM_MODE eMode)
		SWITCH eMode
			CASE SPEC_MODE_PLAYERS_RESPAWN_EVENT
				RETURN "SPEC_MODE_PLAYERS_RESPAWN_EVENT"
			BREAK
			CASE SPEC_MODE_PLAYERS_STAY_DEAD_EVENT
				RETURN "SPEC_MODE_PLAYERS_STAY_DEAD_EVENT"
			BREAK
			CASE SPEC_MODE_SCTV
				RETURN "SPEC_MODE_SCTV"
			BREAK
			CASE SPEC_MODE_NEWS
				RETURN "SPEC_MODE_NEWS"
			BREAK
			CASE SPEC_MODE_TV_CHANNEL
				RETURN "SPEC_MODE_TV_CHANNEL"
			BREAK
			CASE SPEC_MODE_HEIST
				RETURN "SPEC_MODE_HEIST"
			BREAK
		ENDSWITCH
		RETURN "INVALID_MODE"
	ENDFUNC
	
/// PURPOSE:
///    turns eSPEC_CAM_STATE into a debug string
/// PARAMS:
///    eState - 
/// RETURNS:
///    eState as a string 
	FUNC STRING GET_SPEC_CAM_STATE_NAME(eSPEC_CAM_STATE eState)
		SWITCH eState
			CASE eSPECCAMSTATE_OFF
				RETURN "eSPECCAMSTATE_OFF"
			BREAK
			CASE eSPECCAMSTATE_INIT
				RETURN "eSPECCAMSTATE_INIT"
			BREAK
			CASE eSPECCAMSTATE_SWITCHING
				RETURN "eSPECCAMSTATE_SWITCHING"
			BREAK
			CASE eSPECCAMSTATE_WATCHING
				RETURN "eSPECCAMSTATE_WATCHING"
			BREAK
			CASE eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER
				RETURN "eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER"
			BREAK
			CASE eSPECCAMSTATE_FINAL_SWOOP_UP
				RETURN "eSPECCAMSTATE_FINAL_SWOOP_UP"
			BREAK
			CASE eSPECCAMSTATE_TURN_OFF
				RETURN "eSPECCAMSTATE_TURN_OFF"
			BREAK
		ENDSWITCH
		RETURN "INVALID_STATE"
	ENDFUNC	

	
/// PURPOSE:
///    turns eSPEC_SKYSWOOP_CLEANUP_STAGE into a debug string
/// PARAMS:
///    eStage - 
/// RETURNS:
///    eStage as a string
	FUNC STRING GET_SPEC_CAM_SKYSWOOP_CLEANUP_STAGE_NAME(eSPEC_SKYSWOOP_CLEANUP_STAGE eStage)
		SWITCH eStage
			CASE eSPECSKYSWOOPCLEANUPSTAGE_OFF
				RETURN "eSPECSKYSWOOPCLEANUPSTAGE_OFF"
			BREAK
			CASE eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT
				RETURN "eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT"
			BREAK
			CASE eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER
				RETURN "eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER"
			BREAK
			CASE eSPECSKYSWOOPCLEANUPSTAGE_FADE_IN
				RETURN "eSPECSKYSWOOPCLEANUPSTAGE_FADE_IN"
			BREAK
		ENDSWITCH
		RETURN "INVALID_STAGE"
	ENDFUNC
	
/// PURPOSE:
///    Create widgets for the spectator cam
/// PARAMS:
///    specCamData - 
	PROC CREATE_SPECTATOR_CAM_WIDGET(SPECTATOR_CAM_DATA &specCamData)
		START_WIDGET_GROUP("Spectator Cam")
			ADD_WIDGET_INT_READ_ONLY("iSelectedInHud", specCamData.iSelectedInHud)
			ADD_WIDGET_BOOL("Spec cam on/off", specCamData.bSwitchSpecCamState)	
			ADD_WIDGET_BOOL("Cam active", specCamData.bSpecCamActive)
			ADD_WIDGET_INT_READ_ONLY("Current Target", specCamData.iCurrentSpecCamTarget)
			ADD_WIDGET_BOOL("Safe to turn on", specCamData.bSafeToTurnOnCam)
			ADD_WIDGET_BOOL("bSpecCamWidgetsActive", specCamData.bSpecCamWidgetsActive)
			ADD_WIDGET_BOOL("bResetSpecCamUponDeath", specCamData.bResetSpecCamUponDeath)
			ADD_WIDGET_BOOL("bLocalPlayerViewable", specCamData.bLocalPlayerViewable)
		STOP_WIDGET_GROUP()	
	ENDPROC
	
/// PURPOSE:
///    Updates widgets for the spectator cam
/// PARAMS:
///    specData - 
	PROC UPDATE_SPECTATOR_CAM_WIDGET(SPECTATOR_DATA_STRUCT &specData)
		// Spectator camera widgets.
		
		specData.specCamData.bSpecCamActive = IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)
		
		specData.specCamData.iSelectedInHud = NATIVE_TO_INT(specData.specHUDData.specHUDCurrentFocusTarget.pedIndex)
		
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
			IF IS_PED_A_PLAYER(GET_SPECTATOR_CURRENT_FOCUS_PED())
				specData.specCamData.iCurrentSpecCamTarget = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			ELSE
				specData.specCamData.iCurrentSpecCamTarget = 99
			ENDIF
		ENDIF
		specData.specCamData.bSafeToTurnOnCam = IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
		
	ENDPROC

#ENDIF


/// PURPOSE:
///    Makes sure any native calls for spectator cam are cleaned up  
PROC CLEAR_SPECTATOR_CAM_FOR_FREEMODE_PRE_GAME()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_SPECTATOR_CAM_FOR_FREEMODE_PRE_GAME()")
	#ENDIF
	SET_IN_SPECTATOR_MODE(FALSE, DEFAULT, FALSE)
ENDPROC

/// PURPOSE:
///    Gets the current spectator camera state.
/// PARAMS:
///    specCamData - struct of spectator camera data.
/// RETURNS:
///    Current spectator camera state.
FUNC eSPEC_CAM_STATE GET_SPEC_CAM_STATE(SPECTATOR_CAM_DATA &specCamData)
	RETURN specCamData.eState
ENDFUNC

/// PURPOSE:
///    Gets if this spectator cam is active.
/// RETURNS:
///    TRUE if active, FALSE if not.
FUNC BOOL IS_THIS_SPECTATOR_CAM_ACTIVE(SPECTATOR_CAM_DATA &specCamData)
	RETURN IS_BIT_SET(specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)
ENDFUNC

/// PURPOSE:
///    Gets if this spectator cam has watched a ped, or is swtiching after having watched a ped.
/// RETURNS:
///    TRUE if running, FALSE if not.
FUNC BOOL IS_THIS_SPECTATOR_CAM_RUNNING(SPECTATOR_CAM_DATA &specCamData)
	RETURN IS_BIT_SET(specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)
ENDFUNC

/// PURPOSE:
///    Gets if this spectator cam is off, is cleaning up, or is transitioning back to the player ped
/// RETURNS:
///    TRUE if off, turning off, or transitioning back to player, FALSE if not.
FUNC BOOL IS_THIS_SPECTATOR_CAM_OFF_OR_DEACTIVATING(SPECTATOR_CAM_DATA &specCamData)
	RETURN IS_BIT_SET(specCamData.iBitSet, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)
ENDFUNC

/// PURPOSE:
///    Gets if the user has used the yes/no quit function of this spectator cam to flag he desires to leave the current job
/// RETURNS:
///    TRUE if player flags they wish to leave the job, FALSE if not.
FUNC BOOL DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(SPECTATOR_CAM_DATA &specCamData)
	RETURN IS_BIT_SET(specCamData.iBitSet, SPEC_CAM_BS_THIS_WANT_TO_LEAVE) OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Quit_Content_Confirmed)
ENDFUNC

PROC CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(SPECTATOR_CAM_DATA &specCamData)
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(specCamData.iBitSet, SPEC_CAM_BS_THIS_WANT_TO_LEAVE)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE")
	ENDIF
	#ENDIF
	CLEAR_BIT(specCamData.iBitSet, SPEC_CAM_BS_THIS_WANT_TO_LEAVE)
ENDPROC

/// PURPOSE:
///    Gets if this spectator cam state is off.
/// RETURNS:
///    TRUE if off, FALSE if not.
FUNC BOOL IS_THIS_SPECTATOR_CAM_OFF(SPECTATOR_CAM_DATA &specCamData)
	RETURN GET_SPEC_CAM_STATE(specCamData) = eSPECCAMSTATE_OFF
ENDFUNC

/// PURPOSE:
///    Instantly resets the player as a visible, unfrozen player
/// PARAMS:
///    specCamData - 
///    bLockHiding - true to stop the player from being able to hide until the spectator cam is turned off
PROC FORCE_CLEAR_SPECTATOR_HIDDEN(SPECTATOR_CAM_DATA &specCamData, BOOL bLockHiding = FALSE, BOOL bDoPlayerStates = TRUE)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === FORCE_CLEAR_SPECTATOR_HIDDEN, vHideReturn = ", specCamData.vHideReturn)
	#ENDIF
	IF bDoPlayerStates = TRUE
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		ENDIF
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		DISABLE_PED_HEATSCALE_OVERRIDE(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
		IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		ENDIF
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
	specCamData.vHideReturn = <<0,0,0>>
	IF bLockHiding
		SET_BIT(specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)
	ELSE
		CLEAR_BIT(specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)
	ENDIF
	CLEAR_BIT(specCamData.iBitset, SPEC_CAM_BS_SPECTATOR_HIDDEN)
ENDPROC

/// PURPOSE:
///    Clear the flag that marks that the spectator cam was started by the celebration screen
/// PARAMS:
///    specData - 
PROC CLEAR_SPEC_CAM_ACTIVATED_BY_CELEBRATION_SCREEN(SPECTATOR_DATA_STRUCT &specData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_SPEC_CAM_ACTIVATED_BY_CELEBRATION_SCREEN")
	#ENDIF
	IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)
		CLEAR_BIT(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Copies the ACTIVATE_SPECTATOR_CAM flag regarding hiding the local player into the local machines's globals
/// PARAMS:
///    specData - 
PROC MAINTAIN_SETTING_SHOULD_SPEC_HIDE_GLOBAL(SPECTATOR_DATA_STRUCT &specData)
	//Global Flags
	IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_HIDE_LOCAL_PLAYER)
		IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)
			SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)")
			#ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)
			CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

//Spectator Cam Activation Flags - used to designate special properties to a spectator cam
//Must be identical to ASCF_INDEX_* in net_spectator_variables.sch
CONST_INT ASCF_NONE 										0
CONST_INT ASCF_SPECTATE_LOCAL 								BIT0	//Allows the spectator cam to add the local machine's player ped to acceptable targets
CONST_INT ASCF_HIDE_LOCAL_PLAYER							BIT1	//Hides the local player ped if alive
CONST_INT ASCF_QUIT_SCREEN									BIT2	//Allows the player to press circle and flag a desire to leave the event using a quit screen
CONST_INT ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED	BIT3	//Blocks spectator cam switching target if there are no players suitable to spectate, used in survival as no suitable players means event over
CONST_INT ASCF_CELEBRATION_SCREEN							BIT4	//Does not fade out, allows celebration screen to cover loading

/// PURPOSE:
///    Activates the spectator cam, so that it will start to move the focus of the local machine to a suitable spectator target.
/// PARAMS:
///    specData - unique spectator data struct for the script. there should be a seperate instance in each individual script, including SCTV.sc
///    eStartMode - spectator mode to begin in. rarely changes ever once set.
///    startPedToSpectate - if we know we wish to watch specfic ped, we can incline heavily towards using this ped unless it is unsuitable
///    iActivateSpectatorCamFlags - list of flag settings for the spectator cam, defined above the definition of this function. Should be of the format "ASCF_", but not "ASCF_INDEX_". Can be or'd together.
PROC ACTIVATE_SPECTATOR_CAM(SPECTATOR_DATA_STRUCT &specData, eSPEC_CAM_MODE eStartMode = SPEC_MODE_PLAYERS_RESPAWN_EVENT, PED_INDEX startPedToSpectate = NULL,
							INT iActivateSpectatorCamFlags = ASCF_NONE)
	
	IF IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
	//removed for 1693662 AND NOT IS_PLAYER_SCTV(PLAYER_ID())	//unless SCTV
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ACTIVATE_SPECTATOR_CAM called after SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP set")
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF GET_GAME_STATE_ON_DEATH() <> AFTERLIFE_SET_SPECTATORCAM
			AND GET_GAME_STATE_ON_DEATH() <> AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME
			AND GET_GAME_STATE_ON_DEATH() <> AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
				IF g_WarnPeopleAboutSpectatorCam = FALSE
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === GET_GAME_STATE_ON_DEATH ---> ")
					// Speirs added 28/02/12
					SWITCH GET_GAME_STATE_ON_DEATH()
						CASE AFTERLIFE_BRINGUPHUD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_BRINGUPHUD")
						BREAK
						CASE AFTERLIFE_SET_SPECTATORCAM
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_SET_SPECTATORCAM")
						BREAK
						CASE AFTERLIFE_JUST_REJOIN
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_JUST_REJOIN")
						BREAK
						CASE AFTERLIFE_JUST_AUTO_REJOIN
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_JUST_AUTO_REJOIN")
						BREAK
						CASE AFTERLIFE_BRINGUPHUD_AFTER_TIME
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_BRINGUPHUD_AFTER_TIME")
						BREAK
						CASE AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME")
						BREAK
						CASE AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP")
						BREAK
						CASE AFTERLIFE_JUST_REJOIN_AFTER_TIME
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_JUST_REJOIN_AFTER_TIME")
						BREAK
						CASE AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ------AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE")
						BREAK
					ENDSWITCH
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === END PRINT ---> ")
					DEBUG_PRINTCALLSTACK()
					g_WarnPeopleAboutSpectatorCam = TRUE
				ENDIF
			ENDIF
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ACTIVATE_SPECTATOR_CAM")
			DEBUG_PRINTCALLSTACK()
		#ENDIF	
		
		IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
			SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)		//Force spectator target list to instantly populate
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH) 3")
			#ENDIF
			SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)				//Flag waiting for that first switch to complete
		ENDIF
		
		//CLEANUP_KILL_CAM()
		//CLEAR_KILL_STRIP_DEATH_EFFECTS()
		IF IS_KILLSTRIP_BEING_DISPLAYED()
			DO_SCREEN_FADE_OUT(500)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ACTIVATE_SPECTATOR_CAM - IS_KILLSTRIP_BEING_DISPLAYED - DO FADE OUT")
		ENDIF
		
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.vStartedSpecCamLocation = GET_PLAYER_COORDS(PLAYER_ID())
		
		SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)						//Flag that this spectator cam is active,
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ACTIVE)								//and also globally that there is a spectator cam active
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
		CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
				
		specData.eStartMode = eStartMode
		
		//Check if a Global has set the Preferred Target
		IF MPSpecGlobals.iPreferredSpectatorPlayerID != -1
			PLAYER_INDEX piPrefPlayer = INT_TO_NATIVE(PLAYER_INDEX, MPSpecGlobals.iPreferredSpectatorPlayerID)
			IF NETWORK_IS_PLAYER_ACTIVE(piPrefPlayer)
				startPedToSpectate = GET_PLAYER_PED(piPrefPlayer)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iPreferredSpectatorPlayerID SET - CHANGING startPedToSpectate TO ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(startPedToSpectate)))
			ENDIF
		ENDIF
		
		IF startPedToSpectate <> NULL
			SET_SPECTATOR_DESIRED_FOCUS_PED(specData, startPedToSpectate)
			#IF IS_DEBUG_BUILD
			IF IS_PED_A_PLAYER(startPedToSpectate)
				IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(startPedToSpectate))
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Activating with initial player = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(startPedToSpectate)))
				ENDIF
			ENDIF
			#ENDIF
		ENDIF
		
		/* moving this futher down as the players can see this player fall through the world at this point. 
		//url:bugstar:3057842 - Gunsmith - When sudden death is initiated, players fell through the map floor.   
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(PLAYER_PED_ID(), FALSE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ACTIVATE_SPECTATOR_CAM - Bullet collision turned off for Gunsmith. See url:bugstar:2989819.")
			ENDIF
		ENDIF
		*/
		
		SET_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)
		
		specData.specCamData.iActivateBitset = iActivateSpectatorCamFlags
		
		//Dpad Down Map for DevSpectator
		IF IS_PLAYER_A_ROCKSTAR_DEV()
			DISABLE_RADAR_MAP(TRUE)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ACTIVATE_SPECTATOR_CAM - g_b_DisableRadarMap = TRUE")
		ENDIF
		
		IF NOT IS_PLAYER_IN_CORONA()
		AND NOT IS_TRANSITION_SESSION_LAUNCHING()
			SET_SKYFREEZE_CLEAR()
		ENDIF
		
	ENDIF
	
	// Print the script that called this command and if turning on camera print argument values.
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ***  ACTIVATE_SPECTATOR_CAM *** ")
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM ===      Called by script ", GET_THIS_SCRIPT_NAME())
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM ===  	bActive = TRUE")
	#ENDIF
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("=== CAM === ***  ACTIVATE_SPECTATOR_CAM *** ")
	PRINTLN_FINAL("=== CAM ===      Called by script ", GET_THIS_SCRIPT_NAME())
	PRINTLN_FINAL("=== CAM ===  	bActive = TRUE")
	#ENDIF
	
	
ENDPROC

/// PURPOSE:
///    Instantly snaps the player ped to the current camera location, frozen, invincible, invulnerable and without collision. Sets a flag that the player was left this way
/// PARAMS:
///    specData - 
PROC SPEC_BAIL_FOR_TRANSITION(SPECTATOR_DATA_STRUCT &specData, BOOL bDoFreeze = TRUE)
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SPEC_BAIL_FOR_TRANSITION")
	#ENDIF
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("=== CAM === SPEC_BAIL_FOR_TRANSITION")
	#ENDIF
	FORCE_CLEAR_SPECTATOR_HIDDEN(specData.specCamData, DEFAULT, FALSE)
	IF IS_PED_INJURED(PLAYER_PED_ID())
	ENDIF
	IF g_bUpdateSpectatorPosition
		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD(), FALSE, TRUE, DEFAULT, FALSE)
	ENDIF
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		SET_PED_HEATSCALE_OVERRIDE(PLAYER_PED_ID(), 0)
	ENDIF
	SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
	AND NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
		SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
	ENDIF
	IF bDoFreeze = TRUE
		SET_SKYFREEZE_FROZEN()
		SET_SPEC_BAILED_FOR_TRANSITION()
	ENDIF
ENDPROC

//PURPOSE: Runs through all players and clears chat override.
PROC KILL_NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS()
	INT i
	PLAYER_INDEX thisPlayer
	REPEAT NUM_NETWORK_PLAYERS i
		thisPlayer = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF thisPlayer != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(thisPlayer, FALSE)
				NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(INT_TO_NATIVE(PLAYER_INDEX, i), FALSE)
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN(" KILL_NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS - CALLED")
ENDPROC

//Spectator Cam Deactivation Flags - used to designate special properties to a spectator cam shutting down
//Must be identical to DSCF_INDEX_* in net_spectator_variables.sch
CONST_INT DSCF_NONE 						0
CONST_INT DSCF_FADE_BACK_TO_PLAYER 			BIT0			//Spectator cam will fade and transition back to the player ped before shutting down
CONST_INT DSCF_REMAIN_HIDDEN				BIT1			//Player remains hidden and frozen at the origin once cleaned up
CONST_INT DSCF_REMAIN_VIEWING_TARGET		BIT2			//Machine remains focussed on the last spectated ped, instead of returning to the local player
CONST_INT DSCF_SWOOP_UP						BIT3			//Turns off once having swooped up into the sky for leaderboard
CONST_INT DSCF_BAIL_FOR_TRANSITION			BIT4			//Freezes the spectator in place, invisible with no collision at the last camera coords ready for sky swoop up into transition
CONST_INT DSCF_DONT_CLEANUP_TIMECYCLES		BIT5			//Does not clean up any timecycles that have been set
CONST_INT DSCF_PAUSE_RENDERPHASE			BIT6			//Instantly pauses the renderphase first

/// PURPOSE:
///    Deactivates the spectator cam, so it will begin to transition back to the local player ped (unless flagged otherwise)
/// PARAMS:
///    specData - data of the spectator cam that is to deactivate
///    iDeactivateSpectatorCamFlags - list of flag settings for deactivating the spectator cam, defined above the definition of this function. Should be of the format "DSCF_", but not "DSCF_INDEX_". Can be or'd together.
PROC DEACTIVATE_SPECTATOR_CAM(SPECTATOR_DATA_STRUCT &specData, INT iDeactivateSpectatorCamFlags = DSCF_NONE)
	#IF IS_DEBUG_BUILD
		g_WarnPeopleAboutSpectatorCam = FALSE
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DEACTIVATE_SPECTATOR_CAM")
	#ENDIF
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("=== CAM === DEACTIVATE_SPECTATOR_CAM")
	#ENDIF
	
	CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ACTIVE)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_USING_HEIST_SPECTATE)
	ENDIF
	NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS_ALL(FALSE)
	KILL_NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS()
	
	IF GET_USINGNIGHTVISION()
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIdHash)
		SET_NIGHTVISION(FALSE)
		DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DEACTIVATE_SPECTATOR_CAM - SET_NIGHTVISION(FALSE)")
	ENDIF
	
	IF NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	OR specData.specCamData.eMode = SPEC_MODE_NEWS
	OR specData.specCamData.eMode = SPEC_MODE_TV_CHANNEL
	 OR specData.specCamData.eMode = SPEC_MODE_HEIST 
		CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)	//clears the marking that the local player could not be spectated
	ENDIF
	
	specData.specCamData.iDeactivateBitset = iDeactivateSpectatorCamFlags
	
	IF IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_PAUSE_RENDERPHASE)		//freeze the scren, if flagged
		TOGGLE_RENDERPHASES(FALSE)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_PAUSE_RENDERPHASE)")
		#ENDIF
	ENDIF
	
	IF IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_BAIL_FOR_TRANSITION)	//if we're deactivating to bail out of the session, react here
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_BAIL_FOR_TRANSITION)")
		#ENDIF
		SPEC_BAIL_FOR_TRANSITION(specData)
	ENDIF
		
	SET_ON_LBD_GLOBAL(FALSE)
	IF NOT SHOULD_SCTV_TICKER_SCORE_DISPLAY()
		SET_SCTV_TICKER_SCORE_ON()
	ENDIF
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("GTAOlogo")
	
	//Dpad Down Map for DevSpectator
//	IF IS_PLAYER_A_ROCKSTAR_DEV() //Removing this as it should turn the radar back on no matter what. bug 2485065
		DISABLE_RADAR_MAP(FALSE)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DEACTIVATE_SPECTATOR_CAM - g_b_DisableRadarMap = FALSE")
//	ENDIF

	//Make sure we haven't ended before taking control of transition
	IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
		TAKE_CONTROL_OF_TRANSITION(FALSE, FALSE)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DEACTIVATE_SPECTATOR_CAM - TAKE_CONTROL_OF_TRANSITION - CALLED")
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns true if the ped passed in is currently being viewed by this machine's spectator cam
/// RETURNS:
///    true/false
FUNC BOOL IS_PED_BEING_SPECTATED_BY_THIS_MACHINE(SPECTATOR_CAM_DATA &specCamData, PED_INDEX thisPed)
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(specCamData)
		IF GET_SPEC_CAM_STATE(specCamData) = eSPECCAMSTATE_WATCHING
		OR GET_SPEC_CAM_STATE(specCamData) = eSPECCAMSTATE_SWITCHING
			IF SPEC_IS_PED_ACTIVE(thisPed)
				IF thisPed = GET_SPECTATOR_CURRENT_FOCUS_PED()
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the spectator camera mode.
/// PARAMS:
///    specCamData - struct of spectator cam data.
///    eMode - mode to set camera to.
PROC SET_SPEC_CAM_MODE(SPECTATOR_CAM_DATA &specCamData, eSPEC_CAM_MODE eMode)
	#IF IS_DEBUG_BUILD
		IF specCamData.eMode != eMode
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === setting mode to ", GET_SPEC_CAM_MODE_NAME(eMode))
		ENDIF
	#ENDIF
	specCamData.eMode = eMode
ENDPROC

/// PURPOSE:
///    Sets the spectator fade switch stage.
/// PARAMS:
///    specCamData - struct of spectator cam data.
///    eStage - desired fade switch stage.
PROC SET_SPEC_FADE_SWITCH_STAGE(SPECTATOR_CAM_DATA &specCamData, eSPEC_FADE_SWITCH_STAGE eStage)
	#IF IS_DEBUG_BUILD
		IF specCamData.fadeSwitchStage != eStage
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === setting fade switch stage to ", GET_SPEC_CAM_FADE_SWITCH_STAGE_NAME(eStage))
		ENDIF
	#ENDIF
	#IF USE_FINAL_PRINTS
	IF specCamData.fadeSwitchStage != eStage
		PRINTLN_FINAL("=== CAM === setting fade switch stage to ", GET_SPEC_CAM_FADE_SWITCH_STAGE_NAME(eStage))
	ENDIF
	#ENDIF
	specCamData.fadeSwitchStage = eStage
ENDPROC

/// PURPOSE:
///    Gets the spectator fade switch stage.
/// PARAMS:
///    specCamData - struct of spectator cam data.
FUNC eSPEC_FADE_SWITCH_STAGE GET_SPEC_FADE_SWITCH_STAGE(SPECTATOR_CAM_DATA &specCamData)
	RETURN specCamData.fadeSwitchStage
ENDFUNC

/// PURPOSE:
///    Sets the spectator skyswoop cleanup stage.
/// PARAMS:
///    specCamData - struct of spectator cam data.
///    eStage - desired skyswoop cleanup stage.
PROC SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPEC_SKYSWOOP_CLEANUP_STAGE eStage)
	#IF IS_DEBUG_BUILD
		IF MPSpecGlobals.skyswoopCleanupStage != eStage
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === setting skyswoop cleanup stage to ", GET_SPEC_CAM_SKYSWOOP_CLEANUP_STAGE_NAME(eStage))
		ENDIF
	#ENDIF
	MPSpecGlobals.skyswoopCleanupStage = eStage
ENDPROC

/// PURPOSE:
///    Gets the spectator skyswoop cleanup stage.
/// PARAMS:
///    specCamData - struct of spectator cam data.
FUNC eSPEC_SKYSWOOP_CLEANUP_STAGE GET_SPEC_SKYSWOOP_CLEANUP_STAGE()
	RETURN MPSpecGlobals.skyswoopCleanupStage
ENDFUNC

/// PURPOSE:
///    Sets the spectator cam to be using a cinematic shot (a fixed scripted camera with a slight shake)
/// PARAMS:
///    specCamData - struct of spectator cam data.
///    vCamPos - position to create camera.
///    entLookAt - entity to look at
PROC CREATE_SPECTATOR_CINEMATIC_CAM(SPECTATOR_CAM_DATA &specCamData, VECTOR vCamPos, ENTITY_INDEX entLookAt)
	IF NOT DOES_CAM_EXIST(specCamData.camCinematic)
		
		specCamData.camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(specCamData.camCinematic, vCamPos)
		//SET_CAM_ROT(specCamData.camCinematic, vCameraRot)
		POINT_CAM_AT_ENTITY(specCamData.camCinematic, entLookAt, <<0,0,0>>)
		//SET_CAM_FOV(specCamData.camCinematic, fCameraFOV)
		SET_CAM_ACTIVE(specCamData.camCinematic, TRUE)
		RENDER_SCRIPT_CAMS (TRUE, FALSE)
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === creating spectator cinematic cam at ", vCamPos)
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Ends the spectator cam to be using a cinematic shot (a fixed scripted camera with a slight shake)
/// PARAMS:
///    specCamData - struct of spectator cam data.
PROC DESTROY_SPECTATOR_CINEMATIC_CAM(SPECTATOR_CAM_DATA &specCamData)
	IF DOES_CAM_EXIST(specCamData.camCinematic)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		IF DOES_CAM_EXIST(specCamData.camCinematic)
			DESTROY_CAM(specCamData.camCinematic)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === destroying spectator cinematic cam")
		#ENDIF
	ENDIF
ENDPROC


//CREATE_A_SPRITE_PLACEMENT_WIDGET(spGTAOLogo, "spGTAOLogo")
//PURPOSE: Draw GTAO Logo on Target Swap
PROC DRAW_GTAO_LOGO(SPECTATOR_DATA_STRUCT &specData)
	IF IS_PLAYER_SCTV(PLAYER_ID())
		REQUEST_STREAMED_TEXTURE_DICT("GTAOlogo")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("GTAOlogo")
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				//Sprite Background
				IF specData.specHUDData.spGTAOLogo.x != 0.147	//(CUSTOM_MENU_PIXEL_WIDTH*212)
					specData.specHUDData.spGTAOLogo.x = 0.147	//(CUSTOM_MENU_PIXEL_WIDTH*212)
					specData.specHUDData.spGTAOLogo.y = 0.204	//(1.0-(CUSTOM_MENU_PIXEL_HEIGHT*210))
					specData.specHUDData.spGTAOLogo.w = 0.145
					specData.specHUDData.spGTAOLogo.h = 0.255
					specData.specHUDData.spGTAOLogo.r = 255
					specData.specHUDData.spGTAOLogo.g = 255
					specData.specHUDData.spGTAOLogo.b = 255
					specData.specHUDData.spGTAOLogo.a = 255
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DRAW_GTAO_LOGO - SETUP GTAO LOGO VARIABLES")
				ENDIF
				
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_BOTTOM)
				SET_SCRIPT_GFX_ALIGN_PARAMS(-0.0755, -0.0755, 0.0, 0.0)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				//DRAW_2D_SPRITE("GTAOlogo", "GTAOTransitionLogo", specData.specHUDData.spGTAOLogo, DEFAULT, DEFAULT, GFX_ORDER_AFTER_FADE)
				SPRITE_PLACEMENT BeforeSprite
				BeforeSprite.x = specData.specHUDData.spGTAOLogo.x
				BeforeSprite.y = specData.specHUDData.spGTAOLogo.y
				BeforeSprite.w = specData.specHUDData.spGTAOLogo.w
				BeforeSprite.h = specData.specHUDData.spGTAOLogo.h
				BeforeSprite.r = specData.specHUDData.spGTAOLogo.r
				BeforeSprite.g = specData.specHUDData.spGTAOLogo.g
				BeforeSprite.b = specData.specHUDData.spGTAOLogo.b
				BeforeSprite.a = specData.specHUDData.spGTAOLogo.a
				BeforeSprite.fRotation = specData.specHUDData.spGTAOLogo.fRotation
				SET_SPRITE_PLACEMENT_ACTIVE_AND_SELECTED(BeforeSprite)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
				DRAW_SPRITE("GTAOlogo", "GTAOTransitionLogo", ADJUST_SCREEN_X_POSITION(BeforeSprite.x), ADJUST_SCREEN_Y_POSITION(BeforeSprite.y), BeforeSprite.w, BeforeSprite.h, BeforeSprite.fRotation, BeforeSprite.r, BeforeSprite.g, BeforeSprite.b, BeforeSprite.a)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				
				RESET_SCRIPT_GFX_ALIGN()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_SPEC_FADE_BIG_FEED(SPECTATOR_DATA_STRUCT &specData)
	//HIDE_HUD_AND_RADAR_THIS_FRAME()
	//TOGGLE_RENDERPHASES(FALSE)
	DRAW_GTAO_LOGO(specData)
ENDPROC

//PURPOSE: Controls displaying the Big Feed and Background Texture when fading out to swap players
PROC START_SPEC_FADE_BIG_FEED(SPECTATOR_DATA_STRUCT &specData)
	IF IS_SCREEN_FADED_OUT()
		BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
		END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
	//	HIDE_HUD_AND_RADAR_THIS_FRAME()
	//	SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(TRUE, TRUE)
		DRAW_GTAO_LOGO(specData)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === START_SPEC_FADE_BIG_FEED - CALLED")
	ENDIF
ENDPROC

PROC CLEANUP_SPEC_FADE_BIG_FEED()
	BUSYSPINNER_OFF()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("GTAOlogo")
	//TOGGLE_RENDERPHASES(TRUE)
	//SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE, FALSE)
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_SPEC_FADE_BIG_FEED - CALLED")
ENDPROC

/// PURPOSE:
///    Sets the spectator cam state.
/// PARAMS:
///    specCamData - struct of spectator cam data.
///    eState - state to set to.
PROC SET_SPEC_CAM_STATE(SPECTATOR_CAM_DATA &specCamData, eSPEC_CAM_STATE eState)
	#IF IS_DEBUG_BUILD
		IF specCamData.eState != eState
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === setting state to ", GET_SPEC_CAM_STATE_NAME(eState))
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	specCamData.eState = eState
ENDPROC

/// PURPOSE:
///    Sets flag to indicate spectator cam load scene is in progress.
/// PARAMS:
///    specCamData - struct of spec cam data.
///    bInProgress - set as in progress or not.
PROC SET_SPECTATOR_CAM_LOAD_SCENE_IN_PROGRESS(SPECTATOR_CAM_DATA &specCamData, BOOL bInProgress)
	specCamData.bStartedUpdateLoadScene = bInProgress
ENDPROC

/// PURPOSE:
///    Gets if the flag to indicate if a spectator cam laod scene is in progress or not.
/// PARAMS:
///    specCamData - struct of spectator cam data.
/// RETURNS:
///    TRUE if in progress, FALSE if not.
FUNC BOOL IS_SPECTATOR_CAM_LOAD_SCENE_IN_PROGRESS(SPECTATOR_CAM_DATA &specCamData)
	RETURN specCamData.bStartedUpdateLoadScene
ENDFUNC

/// PURPOSE: Clears the data used for the spectator cam fade switch between two targets
PROC CLEANUP_FADE_SWITCH_DATA(SPECTATOR_CAM_DATA &specCamData)
	// Since we're turning off the camera, turn off the load scene.
	IF GET_SPEC_FADE_SWITCH_STAGE(specCamData) = eSPECFADESWITCHSTAGE_WAIT_FOR_LOAD_SCENE
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_STOP()
		ENDIF
	ENDIF
	SET_SPEC_FADE_SWITCH_STAGE(specCamData, eSPECFADESWITCHSTAGE_SETUP)
	CLEANUP_SPEC_FADE_BIG_FEED()
ENDPROC

/// PURPOSE: Clears the data used for the skyswoop cleanup
PROC CLEANUP_SKYSWOOP_CLEANUP_DATA()
	SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_OFF)
ENDPROC

/// PURPOSE:
///    Returns if the spectator cam is currently being forced to a certain position
/// PARAMS:
///    specCamData - 
/// RETURNS:
///    true/false
FUNC BOOL IS_FORCE_SPECTATOR_CAM_ACTIVE(SPECTATOR_CAM_DATA &specCamData)
	RETURN specCamData.sForceSpectatorCam.bActivate
ENDFUNC

PROC SET_SPEC_CAM_DATA_DO_TRANSITION(SPECTATOR_CAM_DATA &specCamData, BOOL bSet)
	IF specCamData.bDoTransition != bSet
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_DATA_DO_TRANSITION - Setting specCamData.bDoTransition to ", bSet)
		specCamData.bDoTransition = bSet
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes the spectator cam remain at a certain coords before moving to a different target
/// PARAMS:
///    specCamData - 
///    vPos - position of the camera
///    vRot - rotation of the camera
///    fFOV - FOV of the camera
///    bImmediate - makes the change happen immediately
PROC SETUP_FORCE_SPECTATOR_CAM(SPECTATOR_CAM_DATA &specCamData, VECTOR vPos, VECTOR vRot, FLOAT fFOV, BOOL bImmediate = TRUE)
	specCamData.sForceSpectatorCam.bActivate = TRUE
	specCamData.sForceSpectatorCam.vPos = vPos
	specCamData.sForceSpectatorCam.vRot = vRot
	specCamData.sForceSpectatorCam.fFOV = fFOV
	
	IF bImmediate
		SET_SPEC_CAM_DATA_DO_TRANSITION(specCamData, TRUE)
		SET_SPEC_FADE_SWITCH_STAGE(specCamData, eSPECFADESWITCHSTAGE_SETUP)
		SET_SPEC_CAM_STATE(specCamData, eSPECCAMSTATE_SWITCHING)
		CLEANUP_FADE_SWITCH_DATA(specCamData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears up the spectator cam remaining at a certain coords before moving to a different target
/// PARAMS:
///    specCamData - 
PROC CLEANUP_FORCE_SPECTATOR_CAM(SPECTATOR_CAM_DATA &specCamData)
	specCamData.sForceSpectatorCam.bActivate = FALSE
	specCamData.sForceSpectatorCam.vPos = <<0.0, 0.0, 0.0>>
	specCamData.sForceSpectatorCam.vRot = <<0.0, 0.0, 0.0>>
	specCamData.sForceSpectatorCam.fFOV = 0.0
ENDPROC

/// PURPOSE:
///    Sets up the process for creating the spectator target list. Must be run before the first player in the staggered list, or before running a repeat through the list of players
/// PARAMS:
///    specData - 
PROC PRE_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP(SPECTATOR_DATA_STRUCT &specData)
	INT i = 0
	PLAYER_INDEX playerId
	
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS i
		specData.specHUDData.iNextUpdateListEntry[i] = 0
	ENDREPEAT
	
	//Set flag to indicate that the player is on a vs team job, and that their entire team is dead. will allow other teams onto spectate list
	CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)
	MPSpecGlobals.bFriendlyOtherTeamPlayerAvailableToSpectate = FALSE
	IF g_bVSMission
		SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)
		
		REPEAT NUM_NETWORK_PLAYERS i
			playerId = INT_TO_PLAYERINDEX(i)
			IF IS_NET_PLAYER_OK(playerId)		//If player isnt dead
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].iGeneralBitSet, ciFINISHED_JOB)
					IF (GET_PLAYER_TEAM(PlayerId) = GET_PLAYER_TEAM(PLAYER_ID()))	//and is on my team
						IF (PlayerId <> PLAYER_ID())								//and isnt me
							CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)	//then my team has someone to watch
							i = NUM_NETWORK_PLAYERS
							#IF IS_DEBUG_BUILD
							IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PRE_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP - GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD - CLEARED - THIS PLAYER IS ON MY TEAM - ", GET_PLAYER_NAME(PlayerId))
							ENDIF
							#ENDIF
						ENDIF
					ELIF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PlayerId), GET_PLAYER_TEAM(PLAYER_ID()))
						IF (PlayerId <> PLAYER_ID())
							MPSpecGlobals.bFriendlyOtherTeamPlayerAvailableToSpectate = TRUE
							#IF IS_DEBUG_BUILD
							IF IS_KEYBOARD_KEY_PRESSED(KEY_9)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PRE_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP - THIS PLAYER IS ON ANOTHER FRIENDLY TEAM AND AVAILABLE TO SPECTATE - ", GET_PLAYER_NAME(PlayerId))
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SpecTeamDeadDebug")
		IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD)
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === PRE_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP - GLOBAL_SPEC_BS_TEAM_VS_JOB_ENTIRE_TEAM_DEAD = TRUE")
		ENDIF
		
		IF MPSpecGlobals.bFriendlyOtherTeamPlayerAvailableToSpectate
			CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === PRE_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP - bFriendlyOtherTeamPlayerAvailableToSpectate = TRUE")
		ENDIF
	ENDIF
	#ENDIF
	
	g_bAllPlayersSCTVProcess = TRUE	//set the SCTV stagger check bool to true, will get cleared if ADD_SPECTATOR_LIST_TARGET finds a non-sctv player
	
	CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST)
	
ENDPROC

/// PURPOSE:
///    Compares two specHUDTargetListEntry structs and returns true if any element is different
/// PARAMS:
///    entry1 - 
///    entry2 - 
/// RETURNS:
///    true/false
FUNC BOOL ARE_SPECTATOR_LIST_ENTRIES_DIFFERENT(specHUDTargetListEntry entry1, specHUDTargetListEntry entry2)
	IF entry1.pedIndex <> entry2.pedIndex
	OR entry1.iRank <> entry2.iRank
	OR entry1.playerID <> entry2.playerID
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Adds a specHUDTargetListEntry as the next entry in the spectator target list. Checks if this entry is different to how it previously was and flags a scaleform update.
/// PARAMS:
///    specData - 
///    iListID - ID of the list to add the entry to
///    thisEntry - struct of data to add to the list
PROC ADD_SPECTATOR_LIST_TARGET(SPECTATOR_DATA_STRUCT &specData, INT iListID, specHUDTargetListEntry thisEntry)
	PLAYER_INDEX playerID
	
	BOOL bOk = FALSE
	
	IF specData.specHUDData.iNextUpdateListEntry[iListID] < SPEC_HUD_TARGET_LIST_SIZE
		
		IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, thisEntry.pedIndex)
		
			IF NOT IS_PED_A_PLAYER(thisEntry.pedIndex)			//if this is not a player
				IF iListID <> SPEC_HUD_TARGET_LIST_PLAYERS			//and it's ok for them not to be a player
					specData.specHUDData.specHUDNextUpdateList[iListID].Entry[specData.specHUDData.iNextUpdateListEntry[iListID]] = thisEntry
					bOk = TRUE
				ENDIF
			ELSE
				IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex), FALSE)	//if player ok, set them up as the next entry
					playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex)
					IF NATIVE_TO_INT(playerID) != -1
						//Only if they have done the tutorial
						IF HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(playerID)
						AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iFmTutProgBitset, biTrigTut_PlayerRunningActivityTutorial)
							specData.specHUDData.specHUDNextUpdateList[iListID].Entry[specData.specHUDData.iNextUpdateListEntry[iListID]].pedIndex = thisEntry.pedIndex
							specData.specHUDData.specHUDNextUpdateList[iListID].Entry[specData.specHUDData.iNextUpdateListEntry[iListID]].iRank = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iRank
							specData.specHUDData.specHUDNextUpdateList[iListID].Entry[specData.specHUDData.iNextUpdateListEntry[iListID]].playerID = playerID
							bOk = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bOk
				IF iListID <> SPEC_HUD_TARGET_LIST_PLAYERS //if not the players
					//grab the currently stored list entry that this target will replace, if different, mark it for scaleform update
					IF ARE_SPECTATOR_LIST_ENTRIES_DIFFERENT(specData.specHUDData.specHUDList[iListID].Entry[specData.specHUDData.iNextUpdateListEntry[iListID]],			//current entry
															specData.specHUDData.specHUDNextUpdateList[iListID].Entry[specData.specHUDData.iNextUpdateListEntry[iListID]])	//new entry
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ADD_SPECTATOR_LIST_TARGET processEntry <> thisEntry")
						#ENDIF
						SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST)		//List changed? update the scaleform
					ENDIF
				ENDIF
					
				specData.specHUDData.iNextUpdateListEntry[iListID]++
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Copies over an entry from the buffer of next spectator targets into the actual spectator target list.
/// PARAMS:
///    specData - 
///    iListID - ID of list to copy from and to
///    iEntryID - ID of the entry from the actual list
///    iNextUpdateEntryID - IF of the entry from the list buffer
PROC COPY_OVER_SPECTATOR_LIST_ENTRY(SPECTATOR_DATA_STRUCT &specData, INT iListID, INT iEntryID, INT iNextUpdateEntryID)
	//grab the currently stored list entry that this target will replace, if different, mark it for scaleform update
	IF ARE_SPECTATOR_LIST_ENTRIES_DIFFERENT(specData.specHUDData.specHUDList[iListID].Entry[iEntryID],			//current entry
											specData.specHUDData.specHUDNextUpdateList[iListID].Entry[iNextUpdateEntryID])	//new entry
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === COPY_OVER_SPECTATOR_LIST_ENTRY processEntry <> thisEntry")
		#ENDIF
		SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST)		//List changed? update it
	ENDIF
	//copy in new entry
	specData.specHUDData.specHUDList[iListID].Entry[iEntryID] = specData.specHUDData.specHUDNextUpdateList[iListID].Entry[iNextUpdateEntryID]
ENDPROC

/// PURPOSE:
///    Performed after checking all the players and adding them to the spectator target list buffer (either staggered or repeat loop). Sorts the buffer into order by score and team, checks if the buffer is different from the previous list and then flags the scaleform to update if it's different.
/// PARAMS:
///    specData - 
PROC POST_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP(SPECTATOR_DATA_STRUCT &specData)
	#IF IS_DEBUG_BUILD
	BOOL bExtraDebug = TRUE
	#ENDIF
	
	INT iListCount = 0
	INT iListEntryCount = 0
	INT iBestNextEntry = -1
	INT iBestNextTeamPos = -1
	INT iBestNextPlayerPos = -1
	INT iBestNextPlayerTeam = -1
	INT iPlayerTeam = -1
	INT iPlayerPos = -1
	INT iTeamPos = -1
	INT iLastPlayerTeam = -1
	
	PED_INDEX thisPed
	PLAYER_INDEX thisPlayer
	
	INT iListSizeStore = specData.specHUDData.iCurrentNumberOfTargetsInAllLists	//stores the size of the current target list
	
	INT iPlayerNeedToBeAddedBitset = 0
	INT iNextPlayerEntry = 0
	
	BOOL bSetNewBestPlayer = FALSE
	
	specData.specHUDData.iCurrentNumberOfTargetsInAllLists = 0
		
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS iListCount
		specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount] = specData.specHUDData.iNextUpdateListEntry[iListCount] //Copy list size
	
		IF iListCount = SPEC_HUD_TARGET_LIST_PLAYERS			//If it's the player list, then sort
			
			iPlayerNeedToBeAddedBitset = 0
			iNextPlayerEntry = 0
			
			REPEAT specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount] iListEntryCount	//Store players that exist in list, to use to check
				IF DOES_ENTITY_EXIST(specData.specHUDData.specHUDNextUpdateList[iListCount].Entry[iListEntryCount].pedIndex)//that all players have been sorted
					SET_BIT(iPlayerNeedToBeAddedBitset, iListEntryCount)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iPlayerNeedToBeAddedBitset SET - iListEntryCount = ", iListEntryCount)
				ENDIF
			ENDREPEAT
			
			iLastPlayerTeam = -1
			
			WHILE iPlayerNeedToBeAddedBitset <> 0		//if this int = 0, all bits have been cleared, so all players have been sorted
			
				iBestNextEntry = -1						//clear next best entry variables before searching
				iBestNextTeamPos = -1
				iBestNextPlayerPos = -1
				iBestNextPlayerTeam = -1
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount] = ", specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount])
				
				REPEAT specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount] iListEntryCount	//start searching the list
				
					iPlayerTeam = -1			//clear information extracted from this entry
					iPlayerPos = -1
					iTeamPos = -1
					bSetNewBestPlayer = FALSE
				
					IF IS_BIT_SET(iPlayerNeedToBeAddedBitset, iListEntryCount)		//if player has not already been sorted
						thisPed = specData.specHUDData.specHUDNextUpdateList[iListCount].Entry[iListEntryCount].pedIndex
						IF IS_PED_A_PLAYER(thisPed)
						AND NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)) <> -1		//does player exist and is ok
							thisPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed)
							IF IS_NET_PLAYER_OK(thisPlayer, FALSE)
								
								IF GET_PLAYER_TEAM(thisPlayer) > -1								//if we have team information
								AND GET_PLAYER_TEAM(thisPlayer) < MAX_NUM_SPEC_TEAMS
									iPlayerTeam = GET_PLAYER_TEAM(thisPlayer)					//set up the variables to gather all the players in the same team together
									iTeamPos = MPSpecGlobals.iCurrentJobTeamPositions[iPlayerTeam]
								ENDIF
								
								iPlayerPos = MPSpecGlobals.iCurrentJobPlayerPositions[NATIVE_TO_INT(thisPlayer)]	//gets the player's position in the current event (1st, 2nd etc)
								
								//begin sorting the player in comparison to the current best entry
								
								IF GET_PLAYER_TEAM(PLAYER_ID()) = iPlayerTeam
									#IF IS_DEBUG_BUILD 
									IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - player is on the same team as local player") ENDIF
									#ENDIF
									bSetNewBestPlayer = TRUE
								ELIF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(PLAYER_ID()), iPlayerTeam)
									#IF IS_DEBUG_BUILD 
									IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - player's team likes the local player's team") ENDIF
									#ENDIF
									bSetNewBestPlayer = TRUE
								ELSE
								
									IF iBestNextEntry = -1		// best entry not set
										#IF IS_DEBUG_BUILD 
										IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - best entry not set") ENDIF
										#ENDIF
										bSetNewBestPlayer = TRUE
									ELSE
										IF (iPlayerTeam = -1)		// teams are invalid
											IF (iPlayerPos < iBestNextPlayerPos)		// player is better than current best player
												#IF IS_DEBUG_BUILD 
												IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - teams are invalid, this player better than current best") ENDIF
												#ENDIF
												bSetNewBestPlayer = TRUE
											ENDIF
										ELSE		// teams are valid
											IF (iBestNextPlayerTeam = -1)		// current best team not set
												#IF IS_DEBUG_BUILD 
												IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - current best team not set") ENDIF
												#ENDIF
												bSetNewBestPlayer = TRUE
											ELSE
												IF (iLastPlayerTeam > -1 AND iBestNextPlayerTeam = iLastPlayerTeam)		// last player team is set and current best is on that team
													IF (iPlayerTeam = iLastPlayerTeam AND iPlayerPos < iBestNextPlayerPos)		// this player on last team but better than current best player
														#IF IS_DEBUG_BUILD 
														IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - both this and current best player are on last player team, this player is better") ENDIF
														#ENDIF
														bSetNewBestPlayer = TRUE
													ENDIF
												ELSE		// current best player is not on last player team
													IF (iLastPlayerTeam > -1 AND iPlayerTeam = iLastPlayerTeam)		// last player team is set and this player is on that team
														#IF IS_DEBUG_BUILD 
														IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - this player is on last player team, current best player is not") ENDIF
														#ENDIF
														bSetNewBestPlayer = TRUE
													ELSE		// current best player and this player is not on last player team
														IF (iPlayerTeam = iBestNextPlayerTeam)		// this player is on the same team as current best player
															IF (iPlayerPos < iBestNextPlayerPos)		// this player is better than current best player
																#IF IS_DEBUG_BUILD 
																IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - both this and current best player are on the same team, this player is better") ENDIF
																#ENDIF
																bSetNewBestPlayer = TRUE
															ENDIF
														ELSE		// this player is not on same team as current best player
															IF (iTeamPos < iBestNextTeamPos)		// this team is better than current best team
																#IF IS_DEBUG_BUILD 
																IF bExtraDebug CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bSetNewBestPlayer - this player team better than current best player team") ENDIF
																#ENDIF
																bSetNewBestPlayer = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF bSetNewBestPlayer					//this player is the new best player to add to the list, copy the information over
						iBestNextEntry = iListEntryCount
						iBestNextPlayerPos = iPlayerPos
						iBestNextTeamPos = iTeamPos
						iBestNextPlayerTeam = iPlayerTeam
						#IF IS_DEBUG_BUILD
						IF bExtraDebug
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === New next best list entry player - ", GET_PLAYER_NAME(thisPlayer))
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iBestNextEntry = ", iBestNextEntry)
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iBestNextPlayerPos = ", iBestNextPlayerPos)
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iBestNextTeamPos = ", iBestNextTeamPos)
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iBestNextPlayerTeam = ", iBestNextPlayerTeam)
						ENDIF
						#ENDIF
					ENDIF
					
				ENDREPEAT
				
				IF iBestNextEntry = -1
					iPlayerNeedToBeAddedBitset = 0	//we didn't find anyone, we're done
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iPlayerNeedToBeAddedBitset = 0 - We didn't find anyone, we're done")
				ELSE
					#IF IS_DEBUG_BUILD
					IF bExtraDebug
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === COPY_OVER_SPECTATOR_LIST_ENTRY - iBestNextEntry = ", iBestNextEntry)
					ENDIF
					#ENDIF
				
					//copy in new entry from the buffer to the list
					COPY_OVER_SPECTATOR_LIST_ENTRY(specData, iListCount, iNextPlayerEntry, iBestNextEntry)
					
					iLastPlayerTeam = iBestNextPlayerTeam	//store the team of the last player added to the list, so the other players in that team will be prioritised
					
					//move on
					iNextPlayerEntry++
					CLEAR_BIT(iPlayerNeedToBeAddedBitset, iBestNextEntry)
				ENDIF
				
			ENDWHILE
			
			//Empty out the rest of the space in the spectator target list after the last player
			WHILE iNextPlayerEntry < SPEC_HUD_TARGET_LIST_SIZE
				FLUSH_SPECTATOR_HUD_TARGET_LIST_ENTRY(specData.specHUDData.specHUDList[iListCount].Entry[iNextPlayerEntry])//clear out the rest of the player list
				iNextPlayerEntry++
			ENDWHILE
			
		ELSE										//Otherwise just copy over
			specData.specHUDData.specHUDList[iListCount] = specData.specHUDData.specHUDNextUpdateList[iListCount]
		ENDIF
		
		SWITCH iListCount
			CASE SPEC_HUD_TARGET_LIST_PLAYERS			//Want all list members from these lists displayed
			CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
			CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
				specData.specHUDData.iCurrentNumberOfTargetsInAllLists += specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount]
			BREAK
			CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES	//These lists should just have a single representative, if they are populated
			CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
				IF specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount] > 0
					specData.specHUDData.iCurrentNumberOfTargetsInAllLists += 1
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDREPEAT
		
	IF iListSizeStore <> specData.specHUDData.iCurrentNumberOfTargetsInAllLists	//list changed as the total size is different
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === iListSizeStore <> specData.specHUDData.iCurrentNumberOfTargetsInAllLists")
		#ENDIF
		SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST)		//List changed? update it
	ENDIF
	
	IF IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST)		//Check list changed flag
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST) = TRUE")
		
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ========================= Spectator List Contents ===============================")
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === specData.specHUDData.iCurrentNumberOfTargetsInAllLists = ", specData.specHUDData.iCurrentNumberOfTargetsInAllLists)
		
		REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS iListCount
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ============= ", iListCount, " =================")
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === specData.specHUDData.iCurrentNumberOfTargetsInList[", iListCount, "] = ", specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount])
			REPEAT specData.specHUDData.iCurrentNumberOfTargetsInList[iListCount] iListEntryCount
				//CPRINTLN(DEBUG_SPECTATOR, "=== CAM === specData.specHUDData.specHUDList[", iListCount, "].Entry[", iListEntryCount, "].sName = ", specData.specHUDData.specHUDList[iListCount].Entry[iListEntryCount].sName)
			ENDREPEAT
		ENDREPEAT
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === =================================================================================")
		#ENDIF
		
		CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_REDO_LIST)
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
	ENDIF
	
	g_bAllPlayersSCTV = g_bAllPlayersSCTVProcess
	
	//Empty out the spectator target buffer
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS iListCount
		FLUSH_SPECTATOR_HUD_TARGET_LIST(specData.specHUDData.specHUDNextUpdateList[iListCount])
	ENDREPEAT
	
	CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specData.specHUDData)	//make sure the list values are correct
	
ENDPROC

/// PURPOSE:
///    Checks if the entry struct is valid before adding it to the supplied list
/// PARAMS:
///    specData - 
///    iListID - ID of list to add the entry to
///    thisEntry - Entry struct
///    iEntry - ID of entry in list to copy the entry into
PROC POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(SPECTATOR_DATA_STRUCT &specData, INT iListID, specHUDTargetListEntry thisEntry, INT iEntry)
	BOOL bOk
	
	// Gets Ids.
	IF SPEC_IS_PED_ACTIVE(thisEntry.pedIndex)
		bOk = TRUE
	ELSE
		IF iListID <> SPEC_HUD_TARGET_LIST_PLAYERS		//if not active, clear up this entry
			specHUDTargetListEntry nullEntry
			SWITCH iListID
				CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
					specData.specHUDData.GenericEnemyTarget.Entry[iEntry] = nullEntry
				BREAK
				CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
					specData.specHUDData.SpecialEnemyTarget.Entry[iEntry] = nullEntry
				BREAK
				CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
					specData.specHUDData.GenericAllyTarget.Entry[iEntry] = nullEntry
				BREAK
				CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
					specData.specHUDData.SpecialAllyTarget.Entry[iEntry] = nullEntry
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(thisEntry.pedIndex)
		IF NOT CAN_SPECTATOR_TARGET_IN_CUTSCENE() AND IS_PED_A_PLAYER(thisEntry.pedIndex)
			IF IS_PLAYER_RUNNING_A_CUTSCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisEntry.pedIndex))
				bOk = FALSE
				CPRINTLN(DEBUG_SPECTATOR, " Target ignored as playing cutscene")
			ENDIF
		ENDIF
		
		IF NOT CAN_SPECTATOR_TARGET_THE_DEAD() AND IS_ENTITY_DEAD(thisEntry.pedIndex)
			bOk = FALSE
			CPRINTLN(DEBUG_SPECTATOR, " Target ignored as there dead")
		ENDIF
	ENDIF

	// Process main body of loop.
	IF bOk
		ADD_SPECTATOR_LIST_TARGET(specData, iListID, thisEntry)
	ENDIF

ENDPROC

FUNC BOOL SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST(PLAYER_INDEX playerID)
	IF NOT IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE(playerID)
		PRINTLN("[MAGNATE_GANG_BOSS] SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST - IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT = FALSE")
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX bossId
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(playerID)
		bossId = GB_GET_THIS_PLAYER_GANG_BOSS(playerID)
	ELSE
		PRINTLN("[MAGNATE_GANG_BOSS] SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST - GB_IS_PLAYER_MEMBER_OF_A_GANG = FALSE")
		RETURN FALSE
	ENDIF
	
	IF GB_GET_NUM_GOONS_IN_PLAYER_GANG(bossId) = 0
		PRINTLN("[MAGNATE_GANG_BOSS] SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST - GB_GET_NUM_GOONS_IN_PLAYER_GANG = 0")
		RETURN FALSE
	ENDIF
	
	IF IS_NET_PLAYER_OK(bossId, FALSE)
		IF bossId != playerID
			IF NOT IS_PLAYER_SPECTATING(bossId)
			AND IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE(bossId)
				PRINTLN("[MAGNATE_GANG_BOSS] SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST - Boss ", GET_PLAYER_NAME(bossId)," is critical to FMMC_TYPE_BUSINESS_BATTLES")
				RETURN TRUE
			ENDIF
		ENDIF
		
		INT iGoon
		REPEAT GB_GET_MAX_GANG_GOONS() iGoon
			PLAYER_INDEX goonID = GlobalplayerBD_FM_3[NATIVE_TO_INT(bossId)].sMagnateGangBossData.GangMembers[iGoon]
			IF goonID != INVALID_PLAYER_INDEX()
			AND goonID != playerID
				IF NETWORK_IS_PLAYER_ACTIVE(goonID)
					IF NOT IS_PLAYER_SPECTATING(goonID)
					AND IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE(goonID)
						PRINTLN("[MAGNATE_GANG_BOSS] SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST - Player ", GET_PLAYER_NAME(goonID)," is IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	PRINTLN("[MAGNATE_GANG_BOSS] SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST - No players")
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_IS_PLAYER_ON_MISSION_REQUIRING_GANG_SPECTATE_LIST(PLAYER_INDEX playerID)
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_CHALLENGE(playerID)
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_BIKER_CONTRABAND_DEFEND
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_FMBB_DEFEND
		RETURN TRUE
	ENDIF
	
	IF SHOULD_SHOW_ONLY_GANG_ON_SPECTATE_LIST(playerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GB_SHOULD_ADD_GANG_MEMBER_TO_SPECTATE_LIST(PLAYER_INDEX goonId)
	UNUSED_PARAMETER(goonId)
	
	IF IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE(PLAYER_ID())
	AND NOT IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE(goonId)
		RETURN FALSE
	ENDIF
							
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Performs a stage of the loop of populating all the spectator target list entries. Peforms the pre and post functions on the correct entry.
/// PARAMS:
///    specData - 
///    iEntry - iEntry of the loop to process
PROC POPULATE_SPECTATOR_CAM_TARGET_LISTS_ENTRIES(SPECTATOR_DATA_STRUCT &specData, INT iEntry)
	INT i = 0
	specHUDTargetListEntry entryPlayer
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS i
	
		// Process start of loop, init data etc.
		IF iEntry <= 0 AND i <= 0			//First entry of first list
			PRE_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP(specData)
		ENDIF
	
		SWITCH i
			CASE SPEC_HUD_TARGET_LIST_PLAYERS
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iEntry), FALSE)
					IF GB_SHOULD_RUN_SPEC_CAM()
						#IF FEATURE_FREEMODE_ARCADE
						IF IS_FREEMODE_ARCADE()		//TODO: Rework this to only allow viewing your own arcade team; need global data
							IF PLAYER_ID() != INT_TO_PLAYERINDEX(iEntry)		
								entryPlayer.pedIndex = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntry))
								POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, entryPlayer, iEntry)
							ENDIF
						ELSE
						#ENDIF
							IF GB_IS_PLAYER_ON_MISSION_REQUIRING_GANG_SPECTATE_LIST(PLAYER_ID())
								IF GB_SHOULD_ADD_GANG_MEMBER_TO_SPECTATE_LIST(INT_TO_PLAYERINDEX(iEntry))
									IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
										IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(iEntry), PLAYER_ID(), FALSE)
											entryPlayer.pedIndex = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntry))
											POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, entryPlayer, iEntry)
										ENDIF
									ELSE
										IF GB_IS_PLAYER_MEMBER_OF_THIS_GANG(INT_TO_PLAYERINDEX(iEntry), GB_GET_LOCAL_PLAYER_GANG_BOSS())
											IF PLAYER_ID() != INT_TO_PLAYERINDEX(iEntry)
												entryPlayer.pedIndex = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntry))
												POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, entryPlayer, iEntry)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELIF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
								IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(INT_TO_PLAYERINDEX(iEntry),FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()))
									IF PLAYER_ID() != INT_TO_PLAYERINDEX(iEntry)
										entryPlayer.pedIndex = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntry))
										POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, entryPlayer, iEntry)
									ENDIF
								ENDIF
							ELSE
								IF PLAYER_ID() != INT_TO_PLAYERINDEX(iEntry)		
									entryPlayer.pedIndex = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntry))
									POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, entryPlayer, iEntry)
								ENDIF
							ENDIF
						#IF FEATURE_FREEMODE_ARCADE
						ENDIF
						#ENDIF
					ELSE
						entryPlayer.pedIndex = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iEntry))
						POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, entryPlayer, iEntry)
						PRINTLN("POPULATE_SPECTATOR_CAM_TARGET_LISTS_ENTRIES - 4")
					ENDIF
				ENDIF
			BREAK
			CASE SPEC_HUD_TARGET_LIST_GENERIC_ENEMIES
				POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, specData.specHUDData.GenericEnemyTarget.Entry[iEntry], iEntry)
			BREAK
			CASE SPEC_HUD_TARGET_LIST_SPECIAL_ENEMIES
				POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, specData.specHUDData.SpecialEnemyTarget.Entry[iEntry], iEntry)
			BREAK
			CASE SPEC_HUD_TARGET_LIST_GENERIC_ALLIES
				POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, specData.specHUDData.GenericAllyTarget.Entry[iEntry], iEntry)
			BREAK
			CASE SPEC_HUD_TARGET_LIST_SPECIAL_ALLIES
				POPULATE_SPECTATOR_CAM_TARGET_LIST_ENTRY(specData, i, specData.specHUDData.SpecialAllyTarget.Entry[iEntry], iEntry)
			BREAK
		ENDSWITCH
		
		IF iEntry >= SPEC_HUD_TARGET_LIST_SIZE-1 AND i >= NUMBER_OF_SPEC_HUD_TARGET_LISTS-1		//last entry of last list, do post process
			POST_PROCESS_SPECTATOR_CAM_TARGET_LISTS_LOOP(specData)
		ENDIF
	ENDREPEAT

ENDPROC

/// PURPOSE:
///    Will do the pre process, loop through the entire list of players and post-process the spectator target list in a single frame, instead of staggering through them. Very expensive.
/// PARAMS:
///    specData - 
PROC POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(SPECTATOR_DATA_STRUCT &specData)

	INT i = 0
	REPEAT SPEC_HUD_TARGET_LIST_SIZE i
	
		POPULATE_SPECTATOR_CAM_TARGET_LISTS_ENTRIES(specData, i)
		
	ENDREPEAT
	
	specData.specCamData.iLoopCheckFrameCount = 0			//Reset loop for regular ongoing process
ENDPROC

/// PURPOSE:
///    Performs then staggered loop populating the spectator target list. Will reset to 0 when it recognises the list has gone over the limit. Reacts to the "process list instantly" flag by completing the rest of the stagger in a single frame.
/// PARAMS:
///    specData - 
PROC POPULATE_SPECTATOR_CAM_TARGET_LISTS(SPECTATOR_DATA_STRUCT &specData)

	IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)
		POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
		CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)
	ELSE
	
		IF specData.specCamData.iLoopCheckFrameCount < 0					//Process one target per frame
		OR specData.specCamData.iLoopCheckFrameCount >= SPEC_HUD_TARGET_LIST_SIZE
			specData.specCamData.iLoopCheckFrameCount = 0
		ENDIF
		
		POPULATE_SPECTATOR_CAM_TARGET_LISTS_ENTRIES(specData, specData.specCamData.iLoopCheckFrameCount)
												
		specData.specCamData.iLoopCheckFrameCount ++			//Do next target next frame
		
	ENDIF

ENDPROC

/// PURPOSE:
///    Sets the spec cam stage to the cleanup stage required based on the DEACTIVATE_SPECTATOR_CAM flags
/// PARAMS:
///    specData - 
PROC SET_SPEC_CAM_STATE_TO_APPROPRIATE_TURN_OFF_METHOD(SPECTATOR_DATA_STRUCT &specData)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_STATE_TO_APPROPRIATE_TURN_OFF_METHOD")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_SPEC_1	//SPEC_HUD_FILTER_NONE
	IF IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_SWOOP_UP)
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_FADE_IN_FOR_SWOOP_CALLED)
		SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_FINAL_SWOOP_UP)
	ELIF IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_FADE_BACK_TO_PLAYER)
		POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
		SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP)
		SET_SPEC_CAM_DATA_DO_TRANSITION(specData.specCamData, TRUE)
		SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER)
	ELSE
		SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_TURN_OFF)
	ENDIF
ENDPROC

/// PURPOSE:
///    Initialises spectator camera data.
///    Setups first ped to look at and sorts global data.
/// PARAMS:
///    specCamData - struct of spec cam data.
PROC INITIALISE_SPEC_CAM_DATA(SPECTATOR_DATA_STRUCT &specData)
	
	PED_INDEX ped
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === initialising data")
	#ENDIF
	
	// Local data.
//	specData.specCamData.iWaitToSwitchTimer = 0
	
	// We haven't started a load scene yet.
	specData.specCamData.bStartedUpdateLoadScene = FALSE
	
	// Kenneth Ross stuff.
	SET_SPEC_CAM_DATA_DO_TRANSITION(specData.specCamData, TRUE)
	CLEANUP_FADE_SWITCH_DATA(specData.specCamData)
	CLEANUP_SKYSWOOP_CLEANUP_DATA()
	
	//Initialize the list variables
	//RESET_SPECTATOR_HUD_VARIABLES()
	FLUSH_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData)
	
	// Set mode to whatever it was set to when whatever script turned on the camera asked for.
	SET_SPEC_CAM_MODE(specData.specCamData, specData.eStartMode)
	
	// If we want to spectate local player
	IF IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_SPECTATE_LOCAL)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === spectate local player flag = true, first ped to spectate set to local player.")
		#ENDIF
		SET_SPECTATOR_DESIRED_FOCUS_PED(specData, PLAYER_PED_ID())
		SET_SPECTATOR_CURRENT_FOCUS_PED(specData, PLAYER_PED_ID())
		
	// If want to spectate a different player.	
	ELSE 
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === We are going to check the inital ped we want to spectate now.")
		#ENDIF
		//SET_SPECTATOR_CURRENT_FOCUS_PED(specData, GET_SPECTATOR_DESIRED_FOCUS_PED())
	
		// Get ped I want to view.
		ped = GET_SPECTATOR_DESIRED_FOCUS_PED()
		// If a ped cannot be viewed.
		IF NOT IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, ped) 
		
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === can't view player, will try agian next frame.")
			#ENDIF
			
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Initial ped is fine, proceed using this ped.")
			#ENDIF
		ENDIF
		
		// Kenneth R.
		// Commenting out this line as the pedToSpectate != pedInBox condition fails, preventing
		// the camera switch to take place. Also setting the timer so that the switch takes
		// place immediately.
		//specData.specCamData.pedSelectedToSpectate = specData.specCamData.playerInBox
//		specData.specCamData.iWaitToSwitchTimer = (GET_GAME_TIMER() + 3000)
		
	ENDIF
	
	SET_CONTROL_SHAKE(PLAYER_CONTROL, 0, 0)
	
	NETWORK_SET_OVERRIDE_SPECTATOR_MODE(TRUE)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === NETWORK_SET_OVERRIDE_SPECTATOR_MODE set TRUE.")
	#ENDIF
	
	// Complete.
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === initialisation complete.")
	#ENDIF
	SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_INITIAL_TARGET_SET)

ENDPROC

/// PURPOSE:
///    Checks if it's safe to have the spectator cam on every fraeme and turns it off when it becomes unsafe
/// PARAMS:
///    specData - 
PROC PRIVATE_MAINTAIN_TURN_OFF_SPEC_CAM_IF_NOT_SAFE_TO_RUN(SPECTATOR_DATA_STRUCT &specData)
	
	IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_SCTV
	AND NOT IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
		SET_SPEC_CAM_STATE_TO_APPROPRIATE_TURN_OFF_METHOD(specData)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === not safe to have the spec cam on, turning off.")
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Disables normal gameplay hud elements to make way for the spectator HUD
PROC SETUP_SPECTATOR_CAMERA_HUD()
	
	DISABLE_SCRIPT_HUD(HUDPART_RANKBAR, TRUE)

	Disable_MP_Comms()
	
ENDPROC

/// PURPOSE:
///    Gets the variables for a scripted cam for a cinematic cam outside of a specified shop
/// PARAMS:
///    thisShop - Shop to get variables though
///    vPos - vector to store the camera position
///    vRot - vector to store the camera rotation
///    fFOV - float to store camera FOV
/// RETURNS:
///    true/false based on if it can get variables for the shop supplied
FUNC BOOL GET_SHOP_SPECTATOR_CAM_VARIABLES(SHOP_NAME_ENUM thisShop, VECTOR &vPos, VECTOR &vRot, FLOAT &fFOV)

	fFOV = 0.0
	
	SWITCH thisShop
		/*CASE HAIRDO_SHOP_01_BH		vPos=<<-825.0226, -204.8411, 45.0103>>	vRot=<<-16.3972, 8.2865, -0.3175>>		fFOV=37.5091	BREAK
		CASE HAIRDO_SHOP_02_SC		vPos=<<136.9756, -1722.1371, 32.3599>>	vRot=<<-14.2036, -4.2759, 31.6715>>		fFOV=34.3938	BREAK
		CASE HAIRDO_SHOP_03_V		vPos=<<-1293.0195, -1110.3087, 9.7181>>	vRot=<<-14.2898, 1.5910, -157.3973>>	fFOV=38.1542	BREAK
		CASE HAIRDO_SHOP_04_SS		vPos=<<1931.3949, 3714.2742, 37.8709>>	vRot=<<-13.7245, -4.2728, -29.1247>>	fFOV=44.8712	BREAK
		CASE HAIRDO_SHOP_05_MP		vPos=<<1200.9170, -464.3727, 72.1261>>	vRot=<<-23.7595, 2.0422, -149.3284>>	fFOV=40.5772	BREAK
		CASE HAIRDO_SHOP_06_HW		vPos=<<-36.4110, -138.7854, 61.1651>>	vRot=<<-15.8968, -0.0000, -135.2212>>	fFOV=44.5615	BREAK
		CASE HAIRDO_SHOP_07_PB		vPos=<<-294.0869, 6231.1699, 33.3891>>	vRot=<<2.3306, 3.4445, -76.3981>>		fFOV=47.6643	BREAK
		CASE CLOTHES_SHOP_L_01_SC	vPos=<<92.9286, -1378.2771, 36.6846>>	vRot=<<-10.6433, -3.3006, 139.1048>>	fFOV=45.8307	BREAK
		CASE CLOTHES_SHOP_L_02_GS	vPos=<<1674.8564, 4831.6465, 47.6007>>	vRot=<<-16.0288, 3.4203, -118.6406>>	fFOV=43.4869	BREAK
		CASE CLOTHES_SHOP_L_03_DT	vPos=<<409.1436, -823.9545, 36.8324>>	vRot=<<-8.7582, -3.1799, -28.7015>>		fFOV=48.5093	BREAK
		CASE CLOTHES_SHOP_L_04_CS	vPos=<<-1080.2869, 2711.6096, 24.7722>>	vRot=<<-10.4889, 3.6258, 118.6127>>		fFOV=43.7236	BREAK
		CASE CLOTHES_SHOP_L_05_GSD	vPos=<<1216.6460, 2693.4614, 44.8105>>	vRot=<<-12.7709, -0.0000, 70.1971>>		fFOV=31.7432	BREAK
		CASE CLOTHES_SHOP_L_06_VC	vPos=<<-802.2242, -1081.5177, 19.0731>>	vRot=<<-16.6263, 3.7775, 74.0606>>		fFOV=42.0263	BREAK
		CASE CLOTHES_SHOP_L_07_PB	vPos=<<-18.2801, 6513.8081, 37.7176>>	vRot=<<-10.3252, 4.3040, -76.6497>>		fFOV=39.0741	BREAK
		CASE CLOTHES_SHOP_M_01_SM	vPos=<<-1225.0177, -787.2939, 25.6703>>	vRot=<<-14.0033, 2.5543, -77.5533>>		fFOV=35.2399	BREAK
		CASE CLOTHES_SHOP_M_03_H	vPos=<<633.5383, 2730.1638, 48.9654>>	vRot=<<-7.1804, 5.4855, 49.7986>>		fFOV=37.5737	BREAK
		CASE CLOTHES_SHOP_M_04_HW	vPos=<<137.1248, -191.7156, 61.7675>>	vRot=<<-6.4238, 3.7072, 163.1345>>		fFOV=43.7556	BREAK
		CASE CLOTHES_SHOP_M_05_GOH	vPos=<<-3150.4851, 1070.3776, 29.4062>>	vRot=<<-13.1631, 1.1208, 110.4334>>		fFOV=42.5483	BREAK
		CASE CLOTHES_SHOP_H_01_BH	vPos=<<-740.1749, -145.5243, 42.9351>>	vRot=<<-1.2485, -0.0000, -120.7165>>	fFOV=37.2385	BREAK
		CASE CLOTHES_SHOP_H_02_B	vPos=<<-144.1528, -291.5560, 44.1838>>	vRot=<<-9.8021, 3.9868, 146.7777>>		fFOV=38.8990	BREAK
		CASE CLOTHES_SHOP_H_03_MW	vPos=<<-1476.6699, -232.5086, 55.8085>>	vRot=<<-7.2710, 2.9818, -74.4797>>		fFOV=40.1894	BREAK
		CASE CLOTHES_SHOP_A_01_VB	vPos=<<-1336.8195, -1293.8062, 9.6464>>	vRot=<<-6.3128, 5.2949, 7.5189>>		fFOV=38.4729	BREAK
		CASE TATTOO_PARLOUR_01_HW	vPos=<<307.8909, 177.1020, 107.8547>>	vRot=<<-7.7731, 3.4984, -93.9593>>		fFOV=42.7310	BREAK
		CASE TATTOO_PARLOUR_02_SS	vPos=<<1862.3781, 3731.9233, 36.0793>>	vRot=<<-1.1820, 3.1121, 17.6280>>		fFOV=42.8305	BREAK
		CASE TATTOO_PARLOUR_03_PB	vPos=<<-295.4742, 6215.2588, 34.5549>>	vRot=<<-3.9004, 0.0000, -150.2699>>		fFOV=36.3448	BREAK
		CASE TATTOO_PARLOUR_04_VC	vPos=<<-1172.6919, -1417.3562, 7.6585>>	vRot=<<-1.8373, 2.0274, -93.4363>>		fFOV=38.7609	BREAK
		CASE TATTOO_PARLOUR_05_ELS	vPos=<<1304.3976, -1647.5983, 56.2799>>	vRot=<<-2.0905, 1.6171, -98.3848>>		fFOV=41.6140	BREAK
		CASE TATTOO_PARLOUR_06_GOH	vPos=<<-3165.2654, 1059.9387, 25.8545>>	vRot=<<-7.5728, -3.9564, -1.0632>>		fFOV=40.7837	BREAK
		CASE GUN_SHOP_01_DT			vPos=<<24.3617, -1140.5265, 35.7963>>	vRot=<<-0.3826, 2.4467, 33.7836>>		fFOV=41.6029	BREAK
		CASE GUN_SHOP_02_SS			vPos=<<1695.3192, 3734.2109, 38.1158>>	vRot=<<-3.8070, -2.7521, -22.8045>>		fFOV=36.7945	BREAK
		CASE GUN_SHOP_03_HW			vPos=<<238.6252, -31.3112, 71.9314>>	vRot=<<0.6823, -0.7773, -175.0664>>		fFOV=39.3160	BREAK
		CASE GUN_SHOP_04_ELS		vPos=<<851.1727, -1009.0773, 40.3958>>	vRot=<<-22.5173, -7.0891, 144.1277>>	fFOV=43.7516	BREAK
		CASE GUN_SHOP_05_PB			vPos=<<-301.9745, 6079.4624, 35.3554>>	vRot=<<-3.9010, 1.9598, 82.8295>>		fFOV=38.1217	BREAK
		CASE GUN_SHOP_06_LS			vPos=<<-675.5945, -952.7264, 22.3794>>	vRot=<<9.6581, 0.8725, -66.2343>>		fFOV=40.1072	BREAK
		CASE GUN_SHOP_07_MW			vPos=<<-1341.6755, -382.6225, 43.4149>>	vRot=<<-2.7220, 3.8926, -106.6988>>		fFOV=38.4948	BREAK
		CASE GUN_SHOP_08_CS			vPos=<<-1111.9117, 2665.0034, 21.3423>>	vRot=<<5.4818, 0.3762, 3.2214>>			fFOV=36.3457	BREAK
		CASE GUN_SHOP_09_GOH		vPos=<<-3162.9824, 1069.0001, 26.0712>>	vRot=<<-13.2243, -0.0000, -6.8001>>		fFOV=37.7301	BREAK
		CASE GUN_SHOP_10_VWH		vPos=<<2579.6624, 325.0421, 117.5605>> 	vRot=<<-12.9871, 3.1895, 145.7108>>		fFOV=44.8741	BREAK*/
		CASE CARMOD_SHOP_01_AP		vPos=<<-1136.7097, -1964.5654, 22.8574>>vRot=<<-6.3999, 2.6770, 173.2871>>		fFOV=39.6765	BREAK
		CASE CARMOD_SHOP_05_ID2		vPos=<<717.5815, -1103.5057, 26.0196>>	vRot=<<-3.2283, 1.0608, -8.8086>>		fFOV=37.1615	BREAK
		CASE CARMOD_SHOP_06_BT1		vPos=<<-371.4004, -147.4492, 47.6023>>	vRot=<<-14.7200, 3.5163, -34.0187>>		fFOV=44.1101	BREAK
		CASE CARMOD_SHOP_07_CS1		vPos=<<136.2616, 6628.9619, 39.4024>>	vRot=<<-14.1496, 4.8889, 122.6043>>		fFOV=42.0108	BREAK
		CASE CARMOD_SHOP_08_CS6		vPos=<<1162.5962, 2652.6826, 46.3299>>	vRot=<<-14.4757, 8.7634, -93.4153>>		fFOV=44.0285	BREAK
		CASE CARMOD_SHOP_SUPERMOD	vPos=<<-196.310699,-1295.006104,35.513680>>	vRot=<<-4.539959,0.000000,142.596176>>	fFOV=43.437363 BREAK
	ENDSWITCH
	
	IF fFOV = 0.0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the variables for a scripted cam for a cinematic cam outside of a specified hold-up location
/// PARAMS:
///    crimHoldUpPoint - hold-up to get variables though
///    vPos - vector to store the camera position
///    vRot - vector to store the camera rotation
///    fFOV - float to store camera FOV
/// RETURNS:
///    true/false based on if it can get variables for the hold-up supplied
FUNC BOOL GET_HOLD_UP_SPECTATOR_CAM_VARIABLES(Crim_HOLD_UP_POINT crimHoldUpPoint, VECTOR &vPos, VECTOR &vRot, FLOAT &fFOV)

	fFOV = 0.0
	
	SWITCH crimHoldUpPoint
		CASE eCRIM_HUP_SHOP247_1	vPos=<<1389.1110, 3566.6831, 41.4773>>	vRot=<<-7.1819, 0.7321, -12.8108>>		fFOV=19.5268	BREAK
		CASE eCRIM_HUP_SHOP247_2	vPos=<<-3024.1372, 578.9738, 20.7457>>	vRot=<<-31.0843, -0.0000, 54.2092>>		fFOV=26.7196	BREAK
		CASE eCRIM_HUP_SHOP247_3	vPos=<<-3231.3469, 989.7024, 24.3032>>	vRot=<<-32.9766, -11.2753, 33.3745>>	fFOV=28.3043	BREAK
		CASE eCRIM_HUP_SHOP247_4	vPos=<<528.8997, 2680.7898, 50.6731>>	vRot=<<-23.9925, -0.0000, -119.4603>>	fFOV=32.3990	BREAK
		CASE eCRIM_HUP_SHOP247_5	vPos=<<2571.5505, 407.3090, 121.1596>>	vRot=<<-25.5078, 4.3260, 155.2103>>		fFOV=23.7592	BREAK
		CASE eCRIM_HUP_SHOP247_6	vPos=<<2696.6130, 3288.7932, 65.8592>>	vRot=<<-32.3165, 0.0000, 119.7235>>		fFOV=29.6403	BREAK
		CASE eCRIM_HUP_SHOP247_7	vPos=<<1718.9119, 6407.9155, 45.9629>>	vRot=<<-37.5799, -6.2407, -79.0794>>	fFOV=34.5373	BREAK
		CASE eCRIM_HUP_SHOP247_8	vPos=<<1983.7601, 3736.3589, 46.9698>>	vRot=<<-37.3780, 6.7404, 71.9633>>		fFOV=30.7878	BREAK
		CASE eCRIM_HUP_SHOP247_9	vPos=<<9.4290, -1363.9402, 50.7143>>	vRot=<<-34.7732, -9.9862, -54.4029>>	fFOV=25.8192	BREAK
		CASE eCRIM_HUP_SHOP247_10	vPos=<<356.7479, 317.3951, 114.9157>>	vRot=<<-26.1727, -5.7435, -68.3662>>	fFOV=28.5373	BREAK
		
		//LIQUOR STORES
		CASE eCRIM_HUP_LIQUOR_1		vPos=<<1160.6912, 2689.8877, 43.4445>>	vRot=<<-12.9000, -0.0000, -17.5223>>	fFOV=37.2120	BREAK
		CASE eCRIM_HUP_LIQUOR_2		vPos=<<-2984.7170, 398.7042, 24.3334>>	vRot=<<-26.3468, -7.3225, -131.2773>>	fFOV=31.5510	BREAK
		CASE eCRIM_HUP_LIQUOR_3		vPos=<<-1253.1483, -896.9529, 26.5321>>	vRot=<<-17.6119, 4.5663, -97.7435>>		fFOV=31.7331	BREAK
		CASE eCRIM_HUP_LIQUOR_4		vPos=<<1145.2141, -966.4751, 54.6699>>	vRot=<<-26.4074, 1.7608, 168.8267>>		fFOV=24.7514	BREAK
		CASE eCRIM_HUP_LIQUOR_5		vPos=<<-1514.7036, -381.7701, 60.5074>>	vRot=<<-36.0923, 1.3910, -93.3540>>		fFOV=26.3008	BREAK
		
		//GAS STATIONS
		CASE eCRIM_HUP_GAS_1		vPos=<<1702.4146, 4945.1362, 50.4858>>	vRot=<<-26.1720, -9.6990, 169.9167>>	fFOV=39.3281	BREAK
		CASE eCRIM_HUP_GAS_2		vPos=<<-699.9158, -928.0194, 32.3763>>	vRot=<<-37.6951, 8.0833, 45.5064>>		fFOV=26.1050	BREAK
		CASE eCRIM_HUP_GAS_3		vPos=<<-48.8125, -1772.4747, 42.1362>>	vRot=<<-37.6162, 9.4268, 18.3950>>		fFOV=29.7956	BREAK
		CASE eCRIM_HUP_GAS_4		vPos=<<1178.1198, -337.2957, 86.2041>>	vRot=<<-36.9452, 8.1060, 59.9333>>		fFOV=20.8998	BREAK
		CASE eCRIM_HUP_GAS_5		vPos=<<-1803.0295, 788.5015, 151.3613>>	vRot=<<-31.9419, 4.8167, 89.0009>>		fFOV=26.6543	BREAK
	
	ENDSWITCH
	
	IF fFOV = 0.0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

CONST_INT SPEC_CAM_CINEMA_DETECT_DISTANCE_SQR 10000//100sqr

/// PURPOSE:
///    Gets the variables for a scripted cam for a cinematic cam outside of a specified cinema location
/// PARAMS:
///    vCinemaLocation - vector of the cinema's location
///    vPos - vector to store the camera position
///    vRot - vector to store the camera rotation
///    fFOV - float to store camera FOV
/// RETURNS:
///    true/false based on if it can get variables for the cinema supplied
FUNC BOOL GET_CINEMA_SPECTATOR_CAM_VARIABLES(VECTOR vCinemaLocation, VECTOR &vPos, VECTOR &vRot, FLOAT &fFOV)

	fFOV = 0.0
	
	IF VDIST2(vCinemaLocation, <<368.8175, -702.3699, 16.7201>>) < SPEC_CAM_CINEMA_DETECT_DISTANCE_SQR 			//downtown
		vPos=<<411.5287, -690.7221, 34.6860>>	vRot=<<-7.6054, 0.0001, 153.5167>>		fFOV=36.7018
	ENDIF
	
	IF fFOV = 0.0
	AND VDIST2(vCinemaLocation, <<310.2254, 278.9754, 90.1912>>) < SPEC_CAM_CINEMA_DETECT_DISTANCE_SQR 			//vinewood
		vPos=<<278.9962, 170.2905, 107.3217>>	vRot=<<5.8711, 0.0000, -38.8062>>		fFOV=35.6667
	ENDIF
	
	IF fFOV = 0.0
	AND VDIST2(vCinemaLocation, <<-1436.2330, -229.5830, 22.9699>>) < SPEC_CAM_CINEMA_DETECT_DISTANCE_SQR 		//morningwood
		vPos=<<-1441.5149, -180.6728, 51.6060>>	vRot=<<1.1510, -0.0000, -132.2837>>		fFOV=40.6889
	ENDIF
	
	IF fFOV = 0.0
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(SPECTATOR_CAM_DATA &specCamData)
	RETURN DOES_CAM_EXIST(specCamData.camCinematic)
ENDFUNC

FUNC BOOL IS_REMOTE_SCRIPT_USING_SPECIAL_SPECTATOR_CAM()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM)
ENDFUNC

//PURPOSE: Sets a flag to the passed in value. This is used for times when scripts like Pilot School make use of the Special Spectator Cam.
PROC SET_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM(BOOL bSet)
	IF bSet = TRUE
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM)
		PRINTLN(" SET_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM - iABI_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM - SET")
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM)
		PRINTLN(" SET_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM - iABI_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM - CLEARED")
	ENDIF
ENDPROC

PROC CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY(SPECTATOR_DATA_STRUCT &specData, ENTITY_INDEX entityId, VECTOR vOffset, BOOL bHideRadar, VECTOR vCameraCoord)
	
	// Early exit if we are in a corona status
	IF IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF
	
	IF NOT DOES_CAM_EXIST(specData.specCamData.camCinematic)
		
		VECTOR vPos 
		IF DOES_ENTITY_EXIST(entityId)
			vPos = GET_ENTITY_COORDS(entityId)
		ELSE
			vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCameraCoord, 0, vOffset)
		ENDIF
		
		specData.specCamData.camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		
		IF DOES_ENTITY_EXIST(entityId)
			ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, entityId, vOffset, TRUE)
			MP_SET_CAMERA_ROT_TO_SAME_AS_ENTITY(specData.specCamData.camCinematic, entityId, vOffset)
		ELSE
			SET_CAM_COORD(specData.specCamData.camCinematic, vCameraCoord)
		ENDIF
		
		SET_CAM_ACTIVE(specData.specCamData.camCinematic, TRUE)
		//SHAKE_CAM(specData.specCamData.camCinematic, "HAND_SHAKE", fShakeAmount)
		RENDER_SCRIPT_CAMS (TRUE, FALSE)
		
		SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY - SET_BIT(specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA) - CREATED ATTACHED TO ENTITY AT = ", vPos)
				
		IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
			DISPLAY_RADAR(NOT bHideRadar)
		ENDIF
		
		IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
			vPos = GET_SPECTATOR_OVERRIDE_COORDS()
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY - USE GET_SPECTATOR_OVERRIDE_COORDS FOR FOCUS = ", vPos)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vPos)
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
				SET_FOCUS_POS_AND_VEL(vPos, <<0,0,0>>)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY - SET_FOCUS_POS_AND_VEL(", vPos, ", <<0,0,0>>)")
			ENDIF
		ENDIF
		
		NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
		
		SET_IN_SPECTATOR_MODE(FALSE)
		
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
		
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY - DONE")
	ELSE
		IF DOES_ENTITY_EXIST(entityId)
			ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, entityId, vOffset, TRUE)
			MP_SET_CAMERA_ROT_TO_SAME_AS_ENTITY(specData.specCamData.camCinematic, entityId, vOffset)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a scripted camera at the location supplied, adds shake and moves focus etc
/// PARAMS:
///    specData - 
///    fShakeAmount - amount to shake camera
///    vPos - position of camera
///    vRot - rotation of camera
///    fFOV - FOV of camera
///    bHideRadar - hide the radar when this camera is being shown
PROC CREATE_SPECIAL_SPECTATOR_CAMERA(SPECTATOR_DATA_STRUCT &specData, FLOAT fShakeAmount, VECTOR vPos, VECTOR vRot, FLOAT fFOV, BOOL bHideRadar, BOOL bIsPropertyCam = FALSE)
	
	// Early exit if we are in a corona status
	IF bIsPropertyCam = FALSE
		IF IS_PLAYER_IN_CORONA()
			EXIT
		ENDIF
	ENDIF
	
	IF NOT DOES_CAM_EXIST(specData.specCamData.camCinematic)
		specData.specCamData.camCinematic = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_COORD(specData.specCamData.camCinematic, vPos)
		SET_CAM_ROT(specData.specCamData.camCinematic, vRot)
		SET_CAM_FOV(specData.specCamData.camCinematic, fFOV)
		SET_CAM_ACTIVE(specData.specCamData.camCinematic, TRUE)
		SHAKE_CAM(specData.specCamData.camCinematic, "HAND_SHAKE", fShakeAmount)
		RENDER_SCRIPT_CAMS (TRUE, FALSE)
		
		SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA - SET_BIT(specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA) - CREATED CAM POS = ", vPos)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA - bIsPropertyCam = ", PICK_STRING(bIsPropertyCam, "TRUE", "FALSE"))
				
		IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
			DISPLAY_RADAR(NOT bHideRadar)
		ENDIF
		
		IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
			vPos = GET_SPECTATOR_OVERRIDE_COORDS()
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA - USE GET_SPECTATOR_OVERRIDE_COORDS FOR FOCUS = ", vPos)
		ENDIF
		
		IF NOT IS_VECTOR_ZERO(vPos)
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
				SET_FOCUS_POS_AND_VEL(vPos, <<0,0,0>>)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA - SET_FOCUS_POS_AND_VEL(", vPos, ", <<0,0,0>>)")
			ENDIF
		ENDIF
		
		NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
		
		SET_IN_SPECTATOR_MODE(FALSE)
		
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
		
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CREATE_SPECIAL_SPECTATOR_CAMERA - DONE")
	ENDIF
ENDPROC

/// PURPOSE:
///    Destroys the scripted fixed spectator cam
/// PARAMS:
///    specData - 
PROC CLEANUP_SPECIAL_SPECTATOR_CAMERA(SPECTATOR_DATA_STRUCT &specData)
	IF DOES_CAM_EXIST(specData.specCamData.camCinematic)
		//STOP_CAM_SHAKING(specData.specCamData.camCinematic, TRUE)
		IF GET_RENDERING_CAM() = specData.specCamData.camCinematic
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		DESTROY_CAM(specData.specCamData.camCinematic)
		
		IF SHOULD_CORONA_PLAYER_CLEAR_CORONA_CAMERAS()
		AND NOT IS_PLAYER_IN_CORONA()
			CLEAR_FOCUS()
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_SPECIAL_SPECTATOR_CAMERA - CLEAR_FOCUS")
		ENDIF
		
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_SPECIAL_SPECTATOR_CAMERA - CLEAR_BIT(specCamData.iBitset, SPEC_CAM_BS_USING_SPECIAL_CAMERA)")
		
		//NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		
		IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
		AND specData.specHUDData.eCurrentFilter != SPEC_HUD_FILTER_NEWS
		AND specData.specHUDData.eCurrentFilter != SPEC_HUD_FILTER_GTAOTV
			DISPLAY_RADAR(TRUE)
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEANUP_SPECIAL_SPECTATOR_CAMERA - DISPLAY_RADAR(TRUE) ")
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_DESIRED_FOCUS_PED())
			PLAYER_INDEX PlayerIndex =  NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())
			IF PlayerIndex != INVALID_PLAYER_INDEX()
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEANUP_SPECIAL_SPECTATOR_CAMERA - SET_IN_SPECTATOR_MODE (GET_SPECTATOR_DESIRED_FOCUS_PED()) = ",  GET_PLAYER_NAME(PlayerIndex))
			ENDIF
		ENDIF	
		#ENDIF
		
		specData.specCamData.iSpecialCamNumber = 0
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === CLEANUP_SPECIAL_SPECTATOR_CAMERA - iSpecialCamNumber = 0")
		
		SET_IN_SPECTATOR_MODE(TRUE, GET_SPECTATOR_DESIRED_FOCUS_PED())
		
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		REQUEST_SPEC_INSTRUCTIONAL_BUTTON_UPDATE()
		
		SET_REMOTE_SCRIPT_IS_USING_SPECIAL_SPECTATOR_CAM(FALSE)
	
		CLEAR_SPECTATOR_OVERRIDE_COORDS()
	
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_SPECIAL_SPECTATOR_CAMERA - DONE")
	ENDIF
	
	IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_SPECIAL_SPECTATOR_CAMERA - CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)")
	ENDIF
	IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_SPECIAL_SPECTATOR_CAMERA - CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)")
	ENDIF
ENDPROC

/// PURPOSE:
///    Flags the special spectator camera as requiring a time delay after the point where the camera is no longer needed. I.E. after the player leaves a property
/// PARAMS:
///    specData - 
PROC SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(SPECTATOR_DATA_STRUCT &specData)
	IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
		SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR - SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)")
		#ENDIF
	ENDIF
	IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR - CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns whether or not we can clear the special spectator camera based on if it needs a delay or not
/// PARAMS:
///    specData - 
///    playerID - Player that is currently being spectated
/// RETURNS:
///    
FUNC BOOL IS_SPECIAL_SPECTATOR_CAMERA_READY_TO_CLEAR(SPECTATOR_DATA_STRUCT &specData, PLAYER_INDEX playerID)
	IF NETWORK_IS_PLAYER_ACTIVE(playerID)
		IF NATIVE_TO_INT(playerID) > -1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)
				IF NOT NETWORK_IS_PLAYER_CONCEALED(playerID)
					IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL)
						IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
							IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)
								IF GET_GAME_TIMER() > specData.specCamData.iSpecialCameraClearDelayTimer
									RETURN TRUE
								ELSE
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_SPECIAL_SPECTATOR_CAMERA_READY_TO_CLEAR - Clearing special cam in ", (specData.specCamData.iSpecialCameraClearDelayTimer - GET_GAME_TIMER()))
								ENDIF
							ELSE
								specData.specCamData.iSpecialCameraClearDelayTimer = GET_GAME_TIMER() + SPEC_CLEAR_SPECIAL_CAMERA_DELAY_TIME
								SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_SPECIAL_SPECTATOR_CAMERA_READY_TO_CLEAR - SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_TIMER_SET)")
							ENDIF
						ELSE
							//#IF IS_DEBUG_BUILD
							//	CPRINTLN(DEBUG_SPECTATOR, 	"=== CAM === NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)", 
							//								(specData.specCamData.iSpecialCameraClearDelayTimer - GET_GAME_TIMER()))
							//#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Perform every-frame checks to see if the spectated player is in a situation which requires a specific fixed scripted camera
/// PARAMS:
///    specData - 
PROC MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS(SPECTATOR_DATA_STRUCT &specData)
	//PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS: Called this frame")
	#IF IS_DEBUG_BUILD
	BOOL bPrintSpecStuff
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
		bPrintSpecStuff = TRUE
	ENDIF
	#ENDIF
	
	PED_INDEX pedOfInterest
	
	FLOAT fShake = 0.25	//hearty shake
	IF IS_PLAYER_SCTV(PLAYER_ID())
		fShake = 0.15	//lighter shake
	ENDIF
	
	IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
	AND GET_RENDERING_CAM() = specData.specCamData.camCinematic
		USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
		SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(GET_FINAL_RENDERED_CAM_COORD())
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
		AND NOT IS_PLAYER_TELEPORT_ACTIVE()
			VECTOR vTemp = GET_CAM_COORD(specData.specCamData.camCinematic)
			IF HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
				vTemp = GET_SPECTATOR_OVERRIDE_COORDS()
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS - USE GET_SPECTATOR_OVERRIDE_COORDS FOR FOCUS = ", vTemp)
			ENDIF
			IF NOT IS_VECTOR_ZERO(vTemp)
				SET_FOCUS_POS_AND_VEL(vTemp, <<0,0,0>>)			
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS - SET_FOCUS_POS_AND_VEL(", vTemp, ", <<0,0,0>>)")
			ENDIF
		ENDIF
	ENDIF
	
	IF specData.specCamData.bDoTransition
	AND GET_SPEC_FADE_SWITCH_STAGE(specData.specCamData) > eSPECFADESWITCHSTAGE_FADE_OUT	//if not fading out for transition
		pedOfInterest = GET_SPECTATOR_SELECTED_PED()
	ELSE
		pedOfInterest = GET_SPECTATOR_CURRENT_FOCUS_PED()
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedOfInterest)
		#IF IS_DEBUG_BUILD
		IF bPrintSpecStuff
			PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS: entity exists")
		ENDIF
		#ENDIF
		IF IS_PED_A_PLAYER(pedOfInterest)
			PLAYER_INDEX playerID
			playerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedOfInterest)
			IF NETWORK_IS_PLAYER_ACTIVE(playerID)
				INT iPlayerID = NATIVE_TO_INT(playerID)
				IF iPlayerID <> -1
					INT iCurrentProperty = -1
					INT iCurrentYacht = -1
					iCurrentYacht = GET_YACHT_PLAYER_IS_ON(playerID)
					BOOL bSpecialCameraSetup = FALSE
					
					IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
						iCurrentProperty = GlobalplayerBD_FM[iPlayerID].iTransitionCutsceneProperty
					ELSE
						iCurrentProperty = GlobalplayerBD_FM[iPlayerID].propertyDetails.iCurrentlyInsideProperty
					ENDIF
					#IF IS_DEBUG_BUILD
					IF bPrintSpecStuff
					PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS:iCurrentProperty = ",iCurrentProperty)
					ENDIF
					#ENDIF
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					
						VECTOR vPos, vRot
						VECTOR vPosTemp, vRotTemp
						INT iTemp
						FLOAT fFOV
						
						IF NOT bSpecialCameraSetup
							IF GlobalplayerBD[iPlayerID].iCurrentShop <> -1		//player is in a shop
							AND NOT IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
							AND NOT IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
							AND NOT IS_PLAYER_IN_ARENA_GARAGE(playerID)
								
								SHOP_TYPE_ENUM eShopType = GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[iPlayerID].iCurrentShop))
								
								IF eShopType <> SHOP_TYPE_GUN
								AND NOT IS_PLAYER_IN_MOBILE_SIMPLE_INTERIOR_THAT_HAS_CAR_MOD_SHOP(eShopType, playerID)
								
									#IF IS_DEBUG_BUILD
									IF bPrintSpecStuff
									PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS: player in a shop")
									ENDIF
									#ENDIF
									bSpecialCameraSetup = TRUE
									
									IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
										#IF IS_DEBUG_BUILD
										IF bPrintSpecStuff
										PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS: NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST")
										ENDIF
										#ENDIF
										IF GET_SHOP_SPECTATOR_CAM_VARIABLES(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[iPlayerID].iCurrentShop), vPos, vRot, fFOV)
											#IF IS_DEBUG_BUILD
											IF bPrintSpecStuff
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - IN SHOP - iSpecialCamNumber = 1")
											ENDIF
											#ENDIF
											specData.specCamData.iSpecialCamNumber = 1
											CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vPos, vRot, fFOV, TRUE)
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bPrintSpecStuff
										PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS: DOES_SPECIAL_SPECTATOR_CAMERA_EXIST")
										ENDIF
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						/*IF GlobalplayerBD[iPlayerID].iCurrentHoldUp <> -1		//player is in a holdup
						
							/*bSpecialCameraSetup = TRUE
							
							IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specCamData)
								VECTOR vPos, vRot
								FLOAT fFOV
								IF GET_HOLD_UP_SPECTATOR_CAM_VARIABLES(INT_TO_ENUM(Crim_HOLD_UP_POINT, GlobalplayerBD[iPlayerID].iCurrentHoldUp), vPos, vRot, fFOV)
									CREATE_SPECIAL_SPECTATOR_CAMERA(specCamData, fShake, vPos, vRot, fFOV, TRUE)
								ENDIF
							ENDIF
						ENDIF*/
						
						//Overide current property for the bridge
						IF (NOT IS_PED_INJURED(pedOfInterest) AND GET_ROOM_KEY_FROM_ENTITY(pedOfInterest) = GET_HASH_KEY("YachtRm_Bridge"))
						OR IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayerEnteringYachtBridge)//GlobalplayerBD_FM[iPlayerID].bPlayerEnteringYachtBridge
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Player inside or entering yacht bridge overiding -1 val for current property to Yacht: ", iCurrentProperty, " iCurrentYacht", iCurrentYacht)
							iCurrentProperty = PROPERTY_YACHT_APT_1_BASE
						ENDIF
						
						IF bSpecialCameraSetup
							SIMPLE_INTERIORS eCurrentInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(playerID)
							IF eCurrentInterior = SIMPLE_INTERIOR_INVALID
								eCurrentInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(playerID)
							ENDIF

							// MOC Camera
							IF eCurrentInterior = SIMPLE_INTERIOR_ARMORY_TRUCK_1
							OR eCurrentInterior = SIMPLE_INTERIOR_CREATOR_TRAILER_1
							OR eCurrentInterior = SIMPLE_INTERIOR_HACKER_TRUCK
								MODEL_NAMES vehModel 
								IF eCurrentInterior = SIMPLE_INTERIOR_HACKER_TRUCK 
									vehModel = TERBYTE
								ELSE
									vehModel = TRAILERLARGE
								ENDIF
								
								IF globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
								
									VEHICLE_INDEX armouryTruck = GET_CLOSEST_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint, 50.0, vehModel, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_ALLOW_TRAILERS)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS bSpecialCameraSetup TRUE Player inside armoury truck. Moved local player to ", GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
									
									VECTOR vCameraOffset = <<0.0, -15.0, 5.0>>
									#IF IS_DEBUG_BUILD
									IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset)
										vCameraOffset = MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset
									ENDIF
									#ENDIF
									
									IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
										IF DOES_ENTITY_EXIST(armouryTruck)
											#IF IS_DEBUG_BUILD
											IF bPrintSpecStuff
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST(armouryTruck) falseeee ")
											ENDIF
											#ENDIF
											MP_SET_CAMERA_ROT_TO_SAME_AS_ENTITY(specData.specCamData.camCinematic, armouryTruck, vCameraOffset)
										ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bPrintSpecStuff
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST property owner invalid!")
									ENDIF
									#ENDIF
								ENDIF	
							ELIF eCurrentInterior = SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
							OR eCurrentInterior = SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1
								
								
								VEHICLE_INDEX armouryAircraft = GET_CLOSEST_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord, 50.0, AVENGER, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_PLANES_ONLY | VEHICLE_SEARCH_FLAG_RETURN_HELICOPTORS_ONLY | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED | VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Player inside armoury aircraft. Moved local player to ", GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
								
								
								VECTOR vCameraOffset = <<0.0, -15.0, 5.0>>
								#IF IS_DEBUG_BUILD
								IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset)
									vCameraOffset = MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset
								ENDIF
								#ENDIF
								
								IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
			
									IF DOES_ENTITY_EXIST(armouryAircraft)
										MP_SET_CAMERA_ROT_TO_SAME_AS_ENTITY(specData.specCamData.camCinematic, armouryAircraft, vCameraOffset)
									ENDIF
									
								ENDIF
								
							ENDIF
							
						ENDIF
						IF NOT bSpecialCameraSetup
							#IF IS_DEBUG_BUILD
							IF bPrintSpecStuff
							PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS:bSpecialCameraSetup falseee")
							ENDIF
							#ENDIF
							SIMPLE_INTERIORS eCurrentInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(playerID)
							IF eCurrentInterior = SIMPLE_INTERIOR_INVALID
								eCurrentInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(playerID)
							ENDIF

							// MOC Camera
							IF eCurrentInterior = SIMPLE_INTERIOR_ARMORY_TRUCK_1
							OR eCurrentInterior = SIMPLE_INTERIOR_CREATOR_TRAILER_1
							OR eCurrentInterior = SIMPLE_INTERIOR_HACKER_TRUCK
							
								MODEL_NAMES vehModel 
								IF eCurrentInterior = SIMPLE_INTERIOR_HACKER_TRUCK 
									vehModel = TERBYTE
								ELSE
									vehModel = TRAILERLARGE
								ENDIF
								
								IF globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
									VECTOR vSearchCenter = GlobalplayerBD[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.propertyOwner)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
									VEHICLE_INDEX armouryTruck = GET_CLOSEST_VEHICLE(vSearchCenter, 50.0, vehModel, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_ALLOW_TRAILERS)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS bSpecialCameraSetup FALSE Player inside armoury truck. Moved local player to ", vSearchCenter)
									
									IF NOT IS_VECTOR_ZERO(vSearchCenter)
										SET_SPECTATOR_OVERRIDE_COORDS(vSearchCenter)
									ENDIF
									
									VECTOR vCameraOffset = <<0.0, -15.0, 5.0>>
									#IF IS_DEBUG_BUILD
									IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset)
										vCameraOffset = MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset
									ENDIF
									#ENDIF
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS Player inside armoury truck. Camera offset is ", vCameraOffset)
									
									IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
										#IF IS_DEBUG_BUILD
										IF bPrintSpecStuff
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_SPECIAL_SPECTATOR_CAMERA_EXIST false ")
										ENDIF
										#ENDIF
										IF DOES_ENTITY_EXIST(armouryTruck)
											#IF IS_DEBUG_BUILD
											IF bPrintSpecStuff
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST(armouryTruck) true ")
											ENDIF
											#ENDIF
											CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY(specData, armouryTruck, vCameraOffset, FALSE, vSearchCenter)
										ELSE
											IF NOT IS_VECTOR_ZERO(vSearchCenter)
												SET_FOCUS_POS_AND_VEL(vSearchCenter, <<0,0,0>>)	
											ENDIF
											CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY(specData, armouryTruck, vCameraOffset, FALSE, vSearchCenter)
											#IF IS_DEBUG_BUILD
											IF bPrintSpecStuff
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST(armouryTruck) false ")
											ENDIF
											#ENDIF
										ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										IF bPrintSpecStuff
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_SPECIAL_SPECTATOR_CAMERA_EXIST true ")
										ENDIF
										#ENDIF
										IF DOES_ENTITY_EXIST(armouryTruck)
											#IF IS_DEBUG_BUILD
											IF bPrintSpecStuff
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST(armouryTruck) trueee ")
											ENDIF
											#ENDIF
											ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, armouryTruck, vCameraOffset, TRUE)
											MP_SET_CAMERA_ROT_TO_SAME_AS_ENTITY(specData.specCamData.camCinematic, armouryTruck, vCameraOffset)
										ELSE
											#IF IS_DEBUG_BUILD
											IF bPrintSpecStuff
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST(armouryTruck) false again ? ")
											ENDIF
											#ENDIF
										ENDIF
									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF bPrintSpecStuff
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS DOES_ENTITY_EXIST player in truck has invalid owner ")
									ENDIF
									#ENDIF
								ENDIF	
								
								bSpecialCameraSetup  = TRUE
							// Avenger	
							ELIF eCurrentInterior = SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
							OR eCurrentInterior = SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1
								
								VEHICLE_INDEX armouryAircraft = GET_CLOSEST_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord, 50.0, AVENGER, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_PLANES_ONLY | VEHICLE_SEARCH_FLAG_RETURN_HELICOPTORS_ONLY | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS | VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED | VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Player inside armoury aircraft. Moved local player to ", GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
								
								IF NOT IS_VECTOR_ZERO(GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
									SET_SPECTATOR_OVERRIDE_COORDS(GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
								ENDIF
								
								VECTOR vCameraOffset = <<0.0, -15.0, 5.0>>
								#IF IS_DEBUG_BUILD
								IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset)
									vCameraOffset = MPGlobalsAmbience.sMagnateGangBossData.vMOCSpectatorCamOffset
								ENDIF
								#ENDIF
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Player inside armoury Aircraft. Camera offset is ", vCameraOffset)
								
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									IF DOES_ENTITY_EXIST(armouryAircraft)
										CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY(specData, armouryAircraft, vCameraOffset, FALSE, GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
										MPGlobalsAmbience.sMagnateGangBossData.viMOCSpectatorIndex = armouryAircraft
										
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === viMOCSpectatorIndex = ", NATIVE_TO_INT(MPGlobalsAmbience.sMagnateGangBossData.viMOCSpectatorIndex))
									ELSE
										IF NOT IS_VECTOR_ZERO(GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
											SET_FOCUS_POS_AND_VEL(GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord, <<0,0,0>>)	
										ENDIF
										CREATE_SPECIAL_SPECTATOR_CAMERA_ATTACHED_TO_ENTITY(specData, armouryAircraft, vCameraOffset, FALSE, GlobalplayerBD[NATIVE_TO_INT(playerId)].SimpleInteriorBD.vSpectatorCamCoord)
									ENDIF
								ELSE
									IF MPGlobalsAmbience.sMagnateGangBossData.viMOCSpectatorIndex != armouryAircraft
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === viMOCSpectatorIndex != armouryAircraft - ",NATIVE_TO_INT(MPGlobalsAmbience.sMagnateGangBossData.viMOCSpectatorIndex)," != ",NATIVE_TO_INT(armouryAircraft)," Cleaning up cam")
										CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
									ELSE
										IF DOES_ENTITY_EXIST(armouryAircraft)
											ATTACH_CAM_TO_ENTITY(specData.specCamData.camCinematic, armouryAircraft, vCameraOffset, TRUE)
											MP_SET_CAMERA_ROT_TO_SAME_AS_ENTITY(specData.specCamData.camCinematic, armouryAircraft, vCameraOffset)
										ENDIF
									ENDIF
								ENDIF
								
								bSpecialCameraSetup = TRUE
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_IN_PROPERTY(playerID, TRUE) 
							OR IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
							//Checks for entry/exit to the yacht bridge
							OR (NOT IS_PED_INJURED(pedOfInterest) AND GET_ROOM_KEY_FROM_ENTITY(pedOfInterest) = GET_HASH_KEY("YachtRm_Bridge"))
							OR IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayerEnteringYachtBridge)//GlobalplayerBD_FM[iPlayerID].bPlayerEnteringYachtBridge
								IF iCurrentProperty <> -1					//player is in a property
									IF NOT IS_PROPERTY_YACHT_APARTMENT(iCurrentProperty)
									OR (IS_PROPERTY_YACHT_APARTMENT(iCurrentProperty) AND iCurrentYacht != -1)
										bSpecialCameraSetup = TRUE
										SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
										
										//Check if we should override lower priority Cams
										IF specData.specCamData.iSpecialCamNumber > 2
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - IN PROPERTY - CLEANUP LOWER PRIORITY CAM - iSpecialCamNumber = ", specData.specCamData.iSpecialCamNumber)
											CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
										ENDIF
										
										IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
											
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - IN PROPERTY - iSpecialCamNumber = 2")
											specData.specCamData.iSpecialCamNumber = 2
											IF IS_PROPERTY_YACHT_APARTMENT(iCurrentProperty)
												
												SET_SPECTATOR_OVERRIDE_COORDS(mpYachts[iCurrentYacht].yachtPropertyDetails.vBlipLocation[0])
												CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake,mpYachts[iCurrentYacht].yachtPropertyDetails.camData.vPos,mpYachts[iCurrentYacht].yachtPropertyDetails.camData.vRot, mpYachts[iCurrentYacht].yachtPropertyDetails.camData.fFOV, TRUE, TRUE)
											ELSE
												SET_SPECTATOR_OVERRIDE_COORDS(mpProperties[iCurrentProperty].vBlipLocation[0])
												CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, mpProperties[iCurrentProperty].camData.vPos, mpProperties[iCurrentProperty].camData.vRot, mpProperties[iCurrentProperty].camData.fFOV, TRUE, TRUE)
											ENDIF
											
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(playerID)
							OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(playerID)
	
								SIMPLE_INTERIORS eCurrentInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(playerID)
								IF eCurrentInterior = SIMPLE_INTERIOR_INVALID
									eCurrentInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(playerID)
								ENDIF
								
								IF eCurrentInterior != SIMPLE_INTERIOR_INVALID
									IF (NOT NETWORK_IS_ACTIVITY_SESSION()
									OR (NETWORK_IS_ACTIVITY_SESSION()
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_BUNKER))
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_HACKER_TRUCK
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_CREATOR_AIRCRAFT
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_CREATOR_TRAILER
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_CASINO
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_CASINO_VALET_GARAGE
									#IF FEATURE_CASINO_HEIST
									AND GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) != SIMPLE_INTERIOR_TYPE_ARCADE
									#ENDIF
										BOOL bSecondaryDetails = FALSE
										
										IF GET_SIMPLE_INTERIOR_TYPE(eCurrentInterior) = SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
											bSecondaryDetails = TRUE
										ENDIF
										
										// Player is in simple interior - warehouses, factories etc.
										SIMPLE_INTERIOR_DETAILS interiorDetails
										GET_SIMPLE_INTERIOR_DETAILS(eCurrentInterior, interiorDetails, bSecondaryDetails)
										
										SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
										
										IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
											#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - Simple interior ", GET_SIMPLE_INTERIOR_DEBUG_NAME(eCurrentInterior))
											#ENDIF
											specData.specCamData.iSpecialCamNumber = 14
											VECTOR vSpecCamPos = interiorDetails.entryAnim.establishingCameraPos
											VECTOR vSpecCamRot = interiorDetails.entryAnim.establishingCameraRot
											FLOAT fSpecCamFov = interiorDetails.entryAnim.fEstablishingCameraFov
											SET_SPECTATOR_OVERRIDE_COORDS(g_SimpleInteriorData.vMidPoints[ENUM_TO_INT(eCurrentInterior)])
											
											IF VDIST(vSpecCamPos, <<0.0, 0.0, 0.0>>) < 0.01
												// If no establishing shot camera was set up use entry camera
												vSpecCamPos = interiorDetails.entryAnim.cameraPos
												vSpecCamRot = interiorDetails.entryAnim.cameraRot
												fSpecCamFov = interiorDetails.entryAnim.cameraFov
												#IF IS_DEBUG_BUILD
													ASSERTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS - Simple interior doesn't have establishing shot camera set up, using entry cam")
												#ENDIF
											ENDIF
											
											IF NOT IS_VECTOR_ZERO(vSpecCamPos)
												CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vSpecCamPos, vSpecCamRot, fSpecCamFov, TRUE, TRUE)
												bSpecialCameraSetup = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID, MPAM_TYPE_STRIPCLUB)	//player is in the stripclub
							
								bSpecialCameraSetup = TRUE
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - STRIP CLUB - iSpecialCamNumber = 3")
									specData.specCamData.iSpecialCamNumber = 3
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, <<122.0654, -1327.9208, 33.6793>>, <<-2.9863, 2.0975, -9.7207>>, 32.8998, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID, MPAM_TYPE_LESTER_CUTSCENE)	//player is watching the Lester cutscene
							
								bSpecialCameraSetup = TRUE
								SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - LESTER CUTSCENE - iSpecialCamNumber = 4")
									specData.specCamData.iSpecialCamNumber = 4
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, <<1291.5598, -1730.0697, 58.7646>>, <<-7.1113, -0.0000, 65.8008>>, 41.2371, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID, MPAM_TYPE_TREVOR_CUTSCENE)	//player is watching the Trevor cutscene
							
								bSpecialCameraSetup = TRUE
								SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - TREVOR CUTSCENE - iSpecialCamNumber = 5")
									specData.specCamData.iSpecialCamNumber = 5
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, <<1989.2953, 3812.5586, 33.4393>>, <<-0.5562, -0.0000, 69.5306>>, 40.2409, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID, MPAM_TYPE_LESTER_WAREHOUSE_CUTSCENE)	//player is watching the Lester Warehouse cutscene
							
								bSpecialCameraSetup = TRUE
								SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - LESTER WAREHOUSE CUTSCENE - iSpecialCamNumber = 6")
									specData.specCamData.iSpecialCamNumber = 6
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, <<689.612488,-996.789673,37.070427>>, <<-7.501717,-0.000000,-43.414612>>, 45.0, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE(playerID)
								bSpecialCameraSetup = TRUE
								SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									vPos = GET_FINAL_RENDERED_CAM_COORD()
									vRot = GET_FINAL_RENDERED_CAM_ROT()
									fFOV = GET_FINAL_RENDERED_CAM_FOV()
									
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - PASSIVE MODE CUTSCENE - iSpecialCamNumber = 7")
									specData.specCamData.iSpecialCamNumber = 7
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vPos, vRot, fFOV, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						//Heist Apartment
						IF NOT bSpecialCameraSetup
							IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)//GlobalplayerBD_FM[iPlayerID].g_bInMissionCreatorApartment = TRUE
								bSpecialCameraSetup = TRUE
								SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									SETUP_CAMERA_FOR_WARP(vPosTemp, vRotTemp, vPos, vRot, iTemp, g_FMMC_STRUCT.iBuildingToWarpTo)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - HEIST APARTMENT - iSpecialCamNumber = 8")
									specData.specCamData.iSpecialCamNumber = 8
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vPos, vRot, 40.0, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						//Using Fairground Ride
						IF NOT bSpecialCameraSetup
							IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingFerrisWheel)
							OR IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bUsingRollerCoaster)
							OR NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(playerID, "AM_ROLLERCOASTER", -1)
							OR NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(playerID, "AM_FERRISWHEEL", -1)
								bSpecialCameraSetup = TRUE
								SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
							
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - FAIRGROUND - iSpecialCamNumber = 9")
									specData.specCamData.iSpecialCamNumber = 9
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, <<-1703.8540, -1082.2223, 42.0060>>, <<-8.3096, 0.0000, -111.8213>>, 45.0, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						//Pilot School - Lesson Menu
						IF NOT bSpecialCameraSetup
							IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_PILOT_SCHOOL_CAM)
								bSpecialCameraSetup = TRUE
								//SET_SPECIAL_SPECTATOR_CAMERA_DELAYED_CLEAR(specData)
								IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
									CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SPECIAL_CAMERA_DELAY_CLEAR)
								ENDIF
								IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
									SET_SPEC_CORONA_SCREEN_EFFECTS_ACTIVE(FALSE)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - PILOT SCHOOL - iSpecialCamNumber = 10")
									specData.specCamData.iSpecialCamNumber = 10
									CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, <<-1155.4,-2715.5,64.0>>, << -10.3, 0.0, 113.1>>, 50.0, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
						//LOOK AT CORONA POS
						IF NOT bSpecialCameraSetup
							IF IS_PLAYER_SCTV(PLAYER_ID())
								IF IS_THIS_PLAYER_IN_CORONA(playerID) 
								AND GET_PLAYER_CORONA_STATUS(playerID) < CORONA_STATUS_LAUNCH 
								AND NOT IS_PLAYER_IN_PROPERTY(playerID, TRUE) 					//Catch player entering corona who is about to pop below the map
								//AND NOT IS_PLAYER_IN_CORONA()									//Only if we are not already in the corona

									IF GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_WATCHING	//we've just watched them walk into corona
									AND IS_SAFE_TO_USE_PLAYER_COORDS_FOR_CAMERA(playerID)
									AND g_FMMC_STRUCT.bMostRecentLoadSuccessful
										IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
											bSpecialCameraSetup = TRUE
										ELSE											
											bSpecialCameraSetup = TRUE
											
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using player coords for special cam. ", GET_PLAYER_NAME(playerID))
											#ENDIF
											
											vPos = GET_FINAL_RENDERED_CAM_COORD()
											vRot = GET_FINAL_RENDERED_CAM_ROT()
											fFOV = GET_FINAL_RENDERED_CAM_FOV()
											
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using player coords for special cam FINAL_RENDERED vPos = ", vPos , GET_PLAYER_NAME(playerID))
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using player coords for special cam FINAL_RENDERED vRot = ", vRot , GET_PLAYER_NAME(playerID))
											#ENDIF
											
											IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT.vCameraPanPos)
											AND IS_PLAYER_IN_CORONA()
												vPos = g_FMMC_STRUCT.vCameraPanPos
												vRot = g_FMMC_STRUCT.vCameraPanRot
												
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using player coords for special cam FMMC_STRUCT vPos = ", vPos , GET_PLAYER_NAME(playerID))
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using player coords for special cam FMMC_STRUCT vRot = ", vRot , GET_PLAYER_NAME(playerID))
												#ENDIF
											ENDIF
											
											SET_SKYFREEZE_CLEAR()
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - USING PLAYER COORDS - iSpecialCamNumber = 11")
											specData.specCamData.iSpecialCamNumber = 11
											CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vPos, vRot, fFOV, TRUE)											
										ENDIF
									ELSE																	//we're transitioning to them in corona
										IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
											bSpecialCameraSetup = TRUE
										ELSE
											IF GlobalplayerBD[iPlayerID].vTruePlayerCoords.z > 0.0
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using final rendering cam when going into corona: ", GET_PLAYER_NAME(playerID))
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- SPEC CAM STATE = ", ENUM_TO_INT(GET_SPEC_CAM_STATE(specData.specCamData)))
												#ENDIF
												
												vPos = GET_FINAL_RENDERED_CAM_COORD()
												vRot = GET_FINAL_RENDERED_CAM_ROT()
												fFOV = GET_FINAL_RENDERED_CAM_FOV()
												
												bSpecialCameraSetup = TRUE
												
												IF IS_VECTOR_ZERO(vPos)
													CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Using player true coords for special cam. ", GET_PLAYER_NAME(playerID))
													vPos = GlobalplayerBD[iPlayerID].vTruePlayerCoords
													vPos.x += 0.0
													vPos.y += -2.3332
													vPos.z += 3.2914
													vRot = <<-40.6168, 0.0, 0.0>>
													fFOV = 40.3433
												ENDIF
												
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - USING PLAYER TRUE COORDS - iSpecialCamNumber = 12")
												specData.specCamData.iSpecialCamNumber = 12
												CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vPos, vRot, fFOV, TRUE)
											ELSE
												#IF IS_DEBUG_BUILD
												CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- Trying to use player true coords for special cam but invalid. ", GET_PLAYER_NAME(playerID))
												#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(playerID, MPAM_TYPE_CINEMA)		//player is in a cinema
									
									bSpecialCameraSetup = TRUE
								
									IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
										IF GET_CINEMA_SPECTATOR_CAM_VARIABLES(GET_PLAYER_COORDS(playerID), vPos, vRot, fFOV)
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CREATE_SPECIAL_SPECTATOR_CAMERA - IN CINEMAS - iSpecialCamNumber = 13")
											specData.specCamData.iSpecialCamNumber = 13
											CREATE_SPECIAL_SPECTATOR_CAMERA(specData, fShake, vPos, vRot, fFOV, TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
					
					
					
					//===***===***===***=== CLEAN UP SPECIAL CAM IF NOT SET ONE UP AT THIS POINT ===***===***===***===
					IF NOT bSpecialCameraSetup
					AND NOT IS_PLAYER_IN_CORONA()
						IF IS_SPECIAL_SPECTATOR_CAMERA_READY_TO_CLEAR(specData, playerID)
						AND NOT IS_REMOTE_SCRIPT_USING_SPECIAL_SPECTATOR_CAM()
							//CPRINTLN(DEBUG_SPECTATOR, "=== CAM === -Special- CLEANUP_SPECIAL_SPECTATOR_CAMERA - A")
							CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
						ENDIF
					ENDIF
					//===***===***===***===***===***===***===***===***===***===***===***===***===***===***===***===
					
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS: ped intrest entity  not exists")
	ENDIF
ENDPROC

/// PURPOSE:
///    Processes spectator camera off state.
///    Waits for spectator camera to turns on and moves to init state.
/// PARAMS:
///    specCamData - struct of spectator camera data.
PROC PROCESS_eSPECCAMSTATE_OFF(SPECTATOR_DATA_STRUCT &specData)
	
	// If a script has turned on the spectator camera, check it is safe to run and turn it on. If it is not safe to run assert at the offender!
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
		IF specData.eStartMode = SPEC_MODE_SCTV
		OR IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
			IF NOT IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
			//removed for 1693662 OR specData.eStartMode = SPEC_MODE_SCTV
				SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_INIT)
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === spec cam turned on after being turned off due to skyswoop up.")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === spec cam turned on while not safe to do so. Please bug owner of mission that has turned this on.")
			#ENDIF
		ENDIF
	ENDIF
	
ENDPROC






////**** HEIST SPECTATE ****////

//PURPOSE: Makes the local Player stop using Heist Spectate
PROC CLEANUP_HEIST_SPECTATE()
	SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
	PRINTLN(" PIM - HEIST SPECTATE - CLEANUP_HEIST_SPECTATE")
ENDPROC

PROC CLEANUP_HEIST_SPECTATE_FOR_NON_HEISTS()
	SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE_NOW)
	PRINTLN(" PIM - HEIST SPECTATE - iABI_STOP_HEIST_SPECTATE_NOW")
ENDPROC

//PURPOSE: Disable Heist Spectate option for local Player
PROC DISABLE_HEIST_SPECTATE(BOOL bDisable)
	IF bDisable = TRUE
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_HEIST_SPECTATE)
		PRINTLN(" PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - TRUE")
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_HEIST_SPECTATE)
		PRINTLN(" PIM - HEIST SPECTATE - DISABLE_HEIST_SPECTATE - FALSE")
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the local Player has Heist Spectate disabled
FUNC BOOL IS_HEIST_SPECTATE_DISABLED()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DISABLE_HEIST_SPECTATE)
ENDFUNC

//PURPOSE: Checks if it is safe for the player to use Heist Spectate
FUNC BOOL IS_SAFE_TO_USE_HEIST_SPECTATE(BOOL bDoAllChecks = TRUE)
	IF bDoAllChecks
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 3
			RETURN FALSE
		ENDIF
		IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
		IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_UNDERWATER(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), FALSE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns TRUE if the local player should quit Heist Spectate
FUNC BOOL SHOULD_QUIT_HEIST_SPECTATE()
		
	//DON'T DO QUIT CHECKS
	BOOL bTargetOnMissionEnding
	IF IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget, FALSE)
		bTargetOnMissionEnding = IS_MP_DECORATOR_BIT_SET(MPGlobalsAmbience.piHeistSpectateTarget, MP_DECORATOR_BS_MISSION_ENDING)
	ENDIF
	//LOCAL PLAYER CHECKS
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE_NOW)
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - iABI_STOP_HEIST_SPECTATE_NOW")
		RETURN TRUE
	ENDIF
	
	IF (g_bMissionEnding = TRUE OR bTargetOnMissionEnding = TRUE)
	AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = TRUE
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = FALSE - (g_bMissionEnding OR MP_DECORATOR_BS_MISSION_ENDING = TRUE) AND (GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = TRUE)")
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE - GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = ", PICK_STRING(GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(), "TRUE", "FALSE"))
		RETURN FALSE
	ENDIF 
	
	IF IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET(NATIVE_TO_INT(MPGlobalsAmbience.piHeistSpectateTarget))
	AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = TRUE
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE - IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET = TRUE")
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE - GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = ", PICK_STRING(GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(), "TRUE", "FALSE"))
		RETURN FALSE
	ENDIF
	
	IF bTargetOnMissionEnding = TRUE
	AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = TRUE
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE - bTargetOnMissionEnding = TRUE")
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE - GET_TOGGLE_PAUSED_RENDERPHASES_STATUS = ", PICK_STRING(GET_TOGGLE_PAUSED_RENDERPHASES_STATUS(), "TRUE", "FALSE"))
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = FALSE - LOCAL PLAYER IS WATCHING A MOCAP")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SAFE_TO_USE_HEIST_SPECTATE(FALSE)
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = FALSE - IS_SAFE_TO_USE_HEIST_SPECTATE = FALSE")
		RETURN FALSE
	ENDIF
	
	//DO QUIT CHECKS
	
	//LOCAL PLAYER CHECKS
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - iABI_STOP_HEIST_SPECTATE")
		RETURN TRUE
	ENDIF
	IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)	
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - LOCAL PLAYER NOT OK")
			RETURN TRUE
		ENDIF
	ENDIF
	//TARGET PLAYER CHECKS
	IF NOT NETWORK_IS_PLAYER_ACTIVE(MPGlobalsAmbience.piHeistSpectateTarget)
		PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - TARGET PLAYER NOT ACTIVE")
		RETURN TRUE
	ENDIF
	
	IF IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget)
	AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	AND NOT MPGlobalsAmbience.bTeamTurnsSpectateCheck
	AND NOT MPGlobalsAmbience.bIgnoreInvalidToSpectateBail
		IF IS_PLAYER_SPECTATING(MPGlobalsAmbience.piHeistSpectateTarget)
			PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - TARGET PLAYER IS SPECTATING")
			RETURN TRUE
		ENDIF
		
		IF IS_MP_DECORATOR_BIT_SET(MPGlobalsAmbience.piHeistSpectateTarget, MP_DECORATOR_BS_INVALID_TO_SPECTATE)
			PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - MP_DECORATOR_BS_INVALID_TO_SPECTATE SET")
			RETURN TRUE
		ENDIF
		
		/*IF IS_PLAYER_WATCHING_A_MOCAP(MPGlobalsAmbience.piHeistSpectateTarget)
			PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - IS_PLAYER_WATCHING_A_MOCAP")
			RETURN TRUE
		ENDIF*/
		
	//ELSE
	//	PRINTLN(" PIM - HEIST SPECTATE - SHOULD_QUIT_HEIST_SPECTATE = TRUE - TARGET PLAYER NOT OK")
	//	RETURN TRUE
	ENDIF
			
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the Heist Spectate option
PROC PROCESS_HEIST_SPECTATE(SPECTATOR_DATA_STRUCT &specData)
	
	//DEBUG - TO HELP TRACK 2101853
	/*#IF IS_DEBUG_BUILD
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		PRINTLN(" PIM - HEIST SPECTATE - PROCESS_HEIST_SPECTATE - A")
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		PRINTLN(" PIM - HEIST SPECTATE - PROCESS_HEIST_SPECTATE - B")
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
		PRINTLN(" PIM - HEIST SPECTATE - PROCESS_HEIST_SPECTATE - C")
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
		PRINTLN(" PIM - HEIST SPECTATE - PROCESS_HEIST_SPECTATE - D")
	ENDIF
	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
		PRINTLN(" PIM - HEIST SPECTATE - PROCESS_HEIST_SPECTATE - E")
	ENDIF
	IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
		PRINTLN(" PIM - HEIST SPECTATE - PROCESS_HEIST_SPECTATE - F")
	ENDIF
	#ENDIF*/
	
	IF USING_HEIST_SPECTATE()
		//Turn on Spectator Cam
		IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
			IF IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget)
				IF IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
					IF IS_WAYPOINT_ACTIVE()
						MPGlobalsAmbience.vHeistSpectateStoredWaypoint = GET_BLIP_INFO_ID_COORD(GET_FIRST_BLIP_INFO_ID(GET_WAYPOINT_BLIP_ENUM_ID()))
						PRINTLN(" PIM - HEIST SPECTATE - STORE WAYPOINT - vHeistSpectateStoredWaypoint = ", MPGlobalsAmbience.vHeistSpectateStoredWaypoint)
					ELSE
						MPGlobalsAmbience.vHeistSpectateStoredWaypoint = <<0, 0, 0>>
					ENDIF
					CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_USE_RADIO_CONTROLS)
					SET_RADIO_TO_STATION_NAME("OFF")
					PRINTLN(" PIM - HEIST SPECTATE - TURN RADIO OFF")
					ENABLE_SPEC_CAN_FIND_NEW_FOCUS()
					ACTIVATE_SPECTATOR_CAM(specData, SPEC_MODE_HEIST, GET_PLAYER_PED(MPGlobalsAmbience.piHeistSpectateTarget), ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
					PRINTLN(" PIM - HEIST SPECTATE - ACTIVATE_SPECTATOR_CAM - TARGET = ", GET_PLAYER_NAME(MPGlobalsAmbience.piHeistSpectateTarget))
				ELSE
					PRINTLN(" PIM - HEIST SPECTATE - IS_SAFE_TO_TURN_ON_SPECTATOR_CAM = FALSE")
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
				ENDIF
			ELSE
				PRINTLN(" PIM - HEIST SPECTATE - TARGET NOT OKAY")
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
			ENDIF
			
			//Force Quit
			/*IF USING_HEIST_SPECTATE()
				IF SHOULD_QUIT_HEIST_SPECTATE()
					DEACTIVATE_SPECTATOR_CAM(specData, DSCF_FADE_BACK_TO_PLAYER)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
					PRINTLN(" PIM - HEIST SPECTATE - DEACTIVATE_SPECTATOR_CAM - B")
					EXIT
				ENDIF
			ENDIF*/
			
		//Process Spectator Cam
		ELSE
			//Force Quit
			IF SHOULD_QUIT_HEIST_SPECTATE()
				IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.vHeistSpectateStoredWaypoint)
					SET_NEW_WAYPOINT(MPGlobalsAmbience.vHeistSpectateStoredWaypoint.x, MPGlobalsAmbience.vHeistSpectateStoredWaypoint.y)
					PRINTLN(" PIM - HEIST SPECTATE - SET WAYPOINT - vHeistSpectateStoredWaypoint = ", MPGlobalsAmbience.vHeistSpectateStoredWaypoint)
					MPGlobalsAmbience.vHeistSpectateStoredWaypoint = <<0, 0, 0>>
				ENDIF
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
				OR MPGlobalsAmbience.bTeamTurnsSpectateCheck
					MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
					DEACTIVATE_SPECTATOR_CAM(specData, DSCF_NONE)
				ELIF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
					DEACTIVATE_SPECTATOR_CAM(specData, DSCF_NONE)
				ELSE
					DEACTIVATE_SPECTATOR_CAM(specData, DSCF_FADE_BACK_TO_PLAYER)
				ENDIF
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
				CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
				PRINTLN(" PIM - HEIST SPECTATE - DEACTIVATE_SPECTATOR_CAM - B")
				EXIT
			ENDIF
			
			//Chosen Quit
			FE_WARNING_FLAGS iButtonBits = FE_WARNING_YESNO
			IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
					IF ALLOW_SPECTATOR_INPUT(specData)
					AND CAN_SPECTATOR_QUIT()
						IF NOT IS_PAUSE_MENU_ACTIVE()
							PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE", iButtonBits)
							SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
							PRINTLN(" PIM - HEIST SPECTATE - QUIT BUTTON PRESSED")
						ELSE
							PRINTLN(" PIM - HEIST SPECTATE - QUIT BUTTON PRESSED - DO NOTHING - PAUSE MENU ACTIVE")
						ENDIF
					ELSE
						PRINTLN(" PIM - HEIST SPECTATE - QUIT BUTTON PRESSED - DO NOTHING - ALLOW_SPECTATOR_INPUT = FALSE")
					ENDIF
				ENDIF
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER("SPEC_LEAVE", "SPEC_SURE", iButtonBits)
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					
					IF NOT IS_VECTOR_ZERO(MPGlobalsAmbience.vHeistSpectateStoredWaypoint)
						SET_NEW_WAYPOINT(MPGlobalsAmbience.vHeistSpectateStoredWaypoint.x, MPGlobalsAmbience.vHeistSpectateStoredWaypoint.y)
						PRINTLN(" PIM - HEIST SPECTATE - SET WAYPOINT - vHeistSpectateStoredWaypoint = ", MPGlobalsAmbience.vHeistSpectateStoredWaypoint)
						MPGlobalsAmbience.vHeistSpectateStoredWaypoint = <<0, 0, 0>>
					ENDIF
					DO_SCREEN_FADE_OUT(0)
					PRINTLN(" PIM - HEIST SPECTATE - QUIT - DO INSTANT FADE OUT")
					PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
					OR MPGlobalsAmbience.bTeamTurnsSpectateCheck
						MPGlobalsAmbience.piHeistSpectateTarget = INVALID_PLAYER_INDEX()
						DEACTIVATE_SPECTATOR_CAM(specData, DSCF_NONE)
					ELSE
						DEACTIVATE_SPECTATOR_CAM(specData, DSCF_FADE_BACK_TO_PLAYER)
					ENDIF
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_STOP_HEIST_SPECTATE)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
					PRINTLN(" PIM - HEIST SPECTATE - DEACTIVATE_SPECTATOR_CAM - BUTTON PRESS")
					EXIT
				ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_SPEC_HEIST_WARNING_SCREEN)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_SCREEN_FADED_OUT()
			BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("") 
			END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
		ELSE
			IF BUSYSPINNER_IS_ON()
				BUSYSPINNER_OFF()
				PRINTLN(" PIM - HEIST SPECTATE - SET BUSY SPINNER OFF")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_SPECTATOR_TEAM_VOICE_CHAT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Process spectator cam initialisation state.
///    Initialises required ata to turn on the spectator cam from the off state and moves to the appropriate state depending on spec cam mode.
/// PARAMS:
///    specCamData - struct of spec cam data.
PROC PROCESS_eSPECCAMSTATE_INIT(SPECTATOR_DATA_STRUCT &specData)
	
	// Set initial data as required.
	INITIALISE_SPEC_CAM_DATA(specData)
	
	//SETUP_PC_CONTROLS(specData) // Commented out as pc controls now hard wired due to B* 2249255
	
	//Disable character selection
	//DISABLE_SELECTOR()
	
	// Go to the correct spectator camera stage depending on if we are in deathmatch or not.
	SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP)
	SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_SWITCHING)

	NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		/*INT i
		IF specData.specCamData.eMode = SPEC_MODE_NEWS	
			NETWORK_SET_VOICE_ACTIVE(FALSE)	//for 1344491
		ELSE
			REPEAT MAX_TEAM_VALUE i
				NETWORK_OVERRIDE_CHAT_RESTRICTIONS_TEAM_RECEIVE(i, TRUE)//for 1332302
			ENDREPEAT
		ENDIF*/
		IF specData.specCamData.eMode = SPEC_MODE_NEWS	
			//NETWORK_SET_VOICE_ACTIVE(FALSE)	//for 1344491, remove for 1613108
		
		ELIF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
		OR IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
			NETWORK_SET_NO_SPECTATOR_CHAT(FALSE)
			PRINTLN("[CHAT_FUNC], PROCESS_eSPECCAMSTATE_INIT, NETWORK_SET_NO_SPECTATOR_CHAT(FALSE) - HEIST")
			
		ELIF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_SURVIVAL
		OR g_bOnCoopMission
			NETWORK_SET_IGNORE_SPECTATOR_CHAT_LIMITS_SAME_TEAM(TRUE)
			PRINTLN("[CHAT_FUNC], PROCESS_eSPECCAMSTATE_INIT, NETWORK_SET_IGNORE_SPECTATOR_CHAT_LIMITS_SAME_TEAM(TRUE) ")
		ENDIF
	ENDIF

	SETUP_CAM_VIEW_MODES_CINEMATIC(specData.specCamData.camViewMode, FALSE)
	
	// Turn off required hud.
	SETUP_SPECTATOR_CAMERA_HUD()
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND specData.specCamData.eMode <> SPEC_MODE_NEWS
	AND specData.specCamData.eMode <> SPEC_MODE_TV_CHANNEL
	AND specData.specCamData.eMode <> SPEC_MODE_HEIST
	AND (NOT IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID()) OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID()))
		
		IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) <> FMMC_TYPE_SURVIVAL	//IF we're not on survival, or if we are, we're either dead or jip
		OR IS_PLAYER_DEAD(PLAYER_ID())
		OR DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
		
			SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
			TickerEventData.TickerEvent = TICKER_EVENT_SPECTATOR_START
			BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS_ON_SCRIPT(FALSE))
			PRINTLN("[1910861] PROCESS_eSPECCAMSTATE_INIT, SPEC_TCK1 ")
		ENDIF
	ENDIF
		
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH) 2")
	#ENDIF
	SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
	
	CLEAR_GPS_PLAYER_WAYPOINT()
	DELETE_WAYPOINTS_FROM_THIS_PLAYER()
	SET_WAYPOINT_OFF()
	SET_MINIMAP_BLOCK_WAYPOINT(TRUE)
	
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
	CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(specData.specCamData)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WAS_LAST_DEACTIVATED_BY_QUIT_SCREEN)
	
	ENABLE_SPEC_CAN_FIND_NEW_FOCUS()
	
	IF IS_ON_RACE_GLOBAL_SET()
		IF IS_PLAYER_SCTV(PLAYER_ID())
			NETWORK_SET_NO_SPECTATOR_CHAT(TRUE)
			NETWORK_SET_TALKER_PROXIMITY(0)	
			PRINTLN("[CHAT_FUNC], PROCESS_eSPECCAMSTATE_INIT, NETWORK_SET_NO_SPECTATOR_CHAT(TRUE) ")
		ENDIF
	ELSE
		IF NOT IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
			IF NOT (IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
			OR IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED))
			OR IS_PLAYER_SCTV(PLAYER_ID())
				NETWORK_SET_NO_SPECTATOR_CHAT(TRUE)
				NETWORK_SET_TALKER_PROXIMITY(0)		
				PRINTLN("[CHAT_FUNC], PROCESS_eSPECCAMSTATE_INIT, NETWORK_SET_NO_SPECTATOR_CHAT(TRUE) ")
			ENDIF
		ENDIF
	ENDIF
	
	g_bDeactivateRestrictedAreas = TRUE
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())			//If we're not dead
	 AND NOT USING_HEIST_SPECTATE() 
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	ENDIF
	
	
	IF NOT USING_HEIST_SPECTATE()
		SET_MAX_WANTED_LEVEL(0)
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NET_NL()NET_PRINT("PROCESS_eSPECCAMSTATE_INIT: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE) ")
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF IS_FREEMODE_ARCADE()
	AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
		SET_SPEC_HUD_MODE(specData.specHUDData, SPEC_HUD_MODE_MINIMAL)
	ENDIF
	#ENDIF

	// We are now fully on!
	SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_TURNED_ON_CAM)

ENDPROC

/// PURPOSE:
///    Returns true if the player is flagging that any spectator cams watching them should skyswoop up with them
/// PARAMS:
///    playerID - 
///    bPrintDebug - print debug on if this function returns true or false
/// RETURNS:
///    
FUNC BOOL IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP(PLAYER_INDEX playerID, BOOL bPrintDebug = FALSE)
	#IF IS_DEBUG_BUILD
	IF (GET_COMMANDLINE_PARAM_EXISTS("sc_SctvPlayerSwoopFlags"))
		bPrintDebug = TRUE
	ENDIF
	#ENDIF
	IF playerID != INVALID_PLAYER_INDEX()
		INT iPlayer = NATIVE_TO_INT(playerID)
		IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_GOINGUP)
			IF bPrintDebug
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM ===  playerID = ", GET_PLAYER_NAME(playerID), " - IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP SPEC_INFO_BS_SKYSWOOP_GOINGUP")
				#ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC)
			IF bPrintDebug
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === playerID = ", GET_PLAYER_NAME(playerID), " - IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP SPEC_INFO_BS_SKYSWOOP_INSKYSTATIC")
				#ENDIF
			ENDIF
			RETURN TRUE
		ENDIF
		IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_SKYSWOOP_INSKYMOVING)
			IF IS_PLAYER_SWITCH_IN_PROGRESS()	//if we're already in the air
				IF bPrintDebug
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === playerID = ", GET_PLAYER_NAME(playerID), " - IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP SPEC_INFO_BS_SKYSWOOP_INSKYMOVING and already switching")
					#ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the situation is inappropriate to fade out during switching between two players
/// RETURNS:
///    true/false
FUNC BOOL IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()

	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)		// Avoid clash with ending a mission fades
		IF GET_SPEC_SKYSWOOP_CLEANUP_STAGE() <> eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT
		AND GET_SPEC_SKYSWOOP_CLEANUP_STAGE() <> eSPECSKYSWOOPCLEANUPSTAGE_FADE_IN
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES TRUE, IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)")
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES TRUE, IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup)")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)		// An event has disabled spectator fades
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES TRUE, GLOBAL_SPEC_BS_FADES_DISABLED")
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES TRUE, GLOBAL_SPEC_BS_FADES_DISABLED")
		#ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	// is player doing a group warp? - neilf - url:bugstar:3049248 - Paleto Bay Clubhouse - Local Player was faded in too early when transitioning from the Clubhouse to FreeMode, this resulted in low LOD and then being warped again to the same location once loading was done.
	IF IS_LOCAL_PLAYER_DOING_A_GROUP_WARP()
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES TRUE, IS_LOCAL_PLAYER_DOING_A_GROUP_WARP")
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES TRUE, IS_LOCAL_PLAYER_DOING_A_GROUP_WARP")
		#ENDIF
		#ENDIF
		RETURN TRUE		
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the player is indicating that any player spectating them should fade out to hide popping, respawning etc
/// PARAMS:
///    playerID - player to check
///    bPrintDebug - print debug on if this function returns true or false, and why
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_FLAGGING_SHOULD_FADE_OUT(PLAYER_INDEX playerID, BOOL bPrintDebug = FALSE)
	INT iPlayer = NATIVE_TO_INT(playerID)
	//Bail if it's -1
	IF iPlayer = -1
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT iPlayer = -1")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADED_OUT)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bIsOnWarningScreen)//GlobalplayerBD_FM[iPlayer].bIsOnWarningScreen
		IF bPrintDebug
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT SPEC_INFO_BS_FADED_OUT")
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT SPEC_INFO_BS_FADED_OUT")
			#ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
		IF bPrintDebug
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT SPEC_INFO_BS_RESPAWNING")
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT SPEC_INFO_BS_RESPAWNING")
			#ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iSpecInfoBitset, SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL)	//if modshop tutorial flag is set
		IF GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop = -1//if not in shop
			IF bPrintDebug
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL and not in shop")
				#ENDIF
			ENDIF
			RETURN TRUE
		ELSE
			IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(playerId)].iCurrentShop)) <> SHOP_TYPE_CARMOD //or shop not mod
				IF bPrintDebug
					#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT SPEC_INFO_BS_IN_MOD_SHOP_TUTORIAL and not in mod shop")
				#ENDIF
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	IF GB_IS_GLOBAL_CLIENT_BIT1_SET(playerId, eGB_GLOBAL_CLIENT_BITSET_1_DOING_DELIVERY_CUTSCENE)
		IF bPrintDebug
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT - PLAYER DOING BUNKER DELIVERY CUTSCENE")
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== CAM === IS_PLAYER_FLAGGING_SHOULD_FADE_OUT - PLAYER DOING BUNKER DELIVERY CUTSCENE")
			#ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MISSION_END_FOR_SCTV()
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF g_bMissionEnding = TRUE
		OR g_b_JobEnded = TRUE
		OR IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_JIP_BAIL_IF_NO_SUITABLE_FOCUS()
		
	IF IS_SPECTATOR_RUNNING_CUTSCENE()
	OR IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SHOULD_JIP_BAIL_IF_NO_SUITABLE_FOCUS false, MOCAP RUNNING")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SHOULD_JIP_BAIL_IF_NO_SUITABLE_FOCUS false, IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST")
		RETURN FALSE
	ENDIF
							
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Call every frame to make the local player go through the process of switching focus from one spectator target to another. Will fade and load scene for a length of time based on how close the two targets are.
/// PARAMS:
///    specData - 
///    bTargetMustBeSuitable - will only transition if the target is sutiable
///    bOnlyFadeInIfTargetAlive - will only fade in if the target is alive (not respawning)
/// RETURNS:
///    true once completed
FUNC BOOL MANAGE_FADING_SPECTATOR_TARGET_SWITCH(SPECTATOR_DATA_STRUCT &specData, BOOL bTargetMustBeSuitable = TRUE, BOOL bOnlyFadeInIfTargetAlive = FALSE)
	BOOL bMoveOn = FALSE
	PED_INDEX pedSwitchingTo
	PLAYER_INDEX playerSwitchTo
	FLOAT fDistanceSQR
	VECTOR vDistanceCheck
	VECTOR vLoadScenePosition
	
	IF IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_FADING_SPECTATOR_TARGET_SWITCH - EXIT LOCAL PLAYER IS WATCHING A MOCAP")
		RETURN FALSE
	ENDIF
	
	// Get who we are switching to.
	IF NATIVE_TO_INT(GET_SPECTATOR_DESIRED_FOCUS_PED()) <> -1
	AND DOES_ENTITY_EXIST(GET_SPECTATOR_DESIRED_FOCUS_PED())
	AND ((NOT bTargetMustBeSuitable) OR IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, GET_SPECTATOR_DESIRED_FOCUS_PED()))
	AND NOT IS_PLAYER_WATCHING_A_MOCAP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
		pedSwitchingTo = GET_SPECTATOR_DESIRED_FOCUS_PED()		//Desired ped is suitable for purpose
	ELSE
		IF SHOULD_FIND_ANOTHER_SPECTATOR_TARGET(specData)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] FORCE FIND SUITABLE DESIRED FOCUS")
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] FORCE FIND SUITABLE DESIRED FOCUS")
			#ENDIF			
			POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
			INT iList, iEntry
			IF GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY(specData, iList, iEntry, FALSE, TRUE, TRUE)
				SET_SPECTATOR_DESIRED_FOCUS_PED(specData, GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, iList, iEntry))
				SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iEntry)
				SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] FORCE FOUND DESIRED FOCUS, eSPECFADESWITCHSTAGE_SETUP")
				#ENDIF
				#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] FORCE FOUND DESIRED FOCUS, eSPECFADESWITCHSTAGE_SETUP")
				#ENDIF
			ELSE
				IF IS_PLAYER_SCTV(PLAYER_ID())
					IF SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS()
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, 	"=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Fade switch can't force find desired focus")
						#ENDIF
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Fade switch can't force find desired focus")
						#ENDIF
						BAIL_FROM_SCTV_FOLLOW()
						MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_NO_SUITABLE_TARGET
					ENDIF
				ELIF DID_I_JOIN_MISSION_AS_SPECTATOR()
					
					// url:bugstar:7828676
					IF g_bForceBailWhenNoValidSpectatorTargetForJIP
					
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_RaceSpectatorJIPBailTest")						
					#ENDIF
					
						IF SHOULD_JIP_BAIL_IF_NO_SUITABLE_FOCUS()
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, 	"=== CAM === Can't find suitable focus")
							#ENDIF
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("=== CAM === Can't find suitable focus")
							#ENDIF
							DO_SCREEN_FADE_OUT(0)//snap to black!
							SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_SCTV_NO_OTHER_PLAYERS)
							NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_BAIL_FROM_SCTV_FOLLOW_NO_OTHER_PLAYERS))
							MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_NO_SUITABLE_TARGET	
						ENDIF
						
					ENDIF
					
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] WANT TO FIND FOCUS, BUT NOT ALLOWED AS SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE")
			#ENDIF
			#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] WANT TO FIND FOCUS, BUT NOT ALLOWED AS SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE")
			#ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedSwitchingTo)
		// If we want to switch to another ped and it is safe to do so.
		IF specData.specCamData.bDoTransition
			
			#IF IS_DEBUG_BUILD
			VECTOR PlayersCurrentPosition
			#ENDIF
			
			SWITCH GET_SPEC_FADE_SWITCH_STAGE(specData.specCamData)		//switch statement for all the stages of switching to a target
				CASE eSPECFADESWITCHSTAGE_SETUP
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] SWITCH START, eSPECFADESWITCHSTAGE_SETUP")
					#ENDIF
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] SWITCH START, eSPECFADESWITCHSTAGE_SETUP")
					#ENDIF
					
					// 2641477 - Focus not being set when close
					specData.specCamData.bSkipFade = FALSE
				
					IF specData.specCamData.eMode = SPEC_MODE_NEWS	
						specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_LONG
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_LONG")
						#ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(pedSwitchingTo)
							
							IF IS_PED_INJURED(pedSwitchingTo)
							ENDIF
							
							IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
							AND GET_SPECTATOR_CURRENT_FOCUS_PED() <> pedSwitchingTo
								vDistanceCheck = GET_ENTITY_COORDS(GET_SPECTATOR_CURRENT_FOCUS_PED(), FALSE)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] GET_SPECTATOR_CURRENT_FOCUS_PED, vDistanceCheck = ", vDistanceCheck)
								#ENDIF
							ELSE
								vDistanceCheck = GET_FINAL_RENDERED_CAM_COORD()
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] GET_FINAL_RENDERED_CAM_COORD, vDistanceCheck = ", vDistanceCheck)
								#ENDIF
							ENDIF
			
							fDistanceSQR = VDIST2(vDistanceCheck, GET_ENTITY_COORDS(pedSwitchingTo, FALSE))
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] fDistanceSQR = ", fDistanceSQR)
							#ENDIF
							
							IF IS_PLAYER_SCTV(PLAYER_ID())	//to stop fading during skycam from one event to another in SCTV
							AND IS_PLAYER_SWITCH_IN_PROGRESS()
							AND IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED()))
								specData.specCamData.iSwitchFadeTime = 0
								specData.specCamData.bSkipFade = TRUE
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP, specData.specCamData.iSwitchFadeTime = 0")
								#ENDIF
							ELSE
								IF specData.specCamData.bFirstTransitionComplete
								AND NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)	//so we always fade from special shot
								AND fDistanceSQR < SPEC_FADE_SHORT_DISTANCE_SQR
								AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
									specData.specCamData.iSwitchFadeTime = 0							//targets are near, instant switch
									specData.specCamData.bSkipFade = TRUE
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = 0")
									#ENDIF
								ELIF fDistanceSQR < SPEC_FADE_SHORT_DISTANCE_SQR
								AND USING_HEIST_SPECTATE()
								AND IS_THIS_ROCKSTAR_MISSION_NEW_VS_Relay(g_FMMC_STRUCT.iRootContentIDHash)
									specData.specCamData.iSwitchFadeTime = 100							//targets are near, instant switch
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = 0")
									#ENDIF
								ELIF fDistanceSQR < SPEC_FADE_SHORT_DISTANCE_SQR
								AND USING_HEIST_SPECTATE()
								AND (IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
								OR MPGlobalsAmbience.bTeamTurnsSpectateCheck)
									specData.specCamData.iSwitchFadeTime = 100
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = 100")
									#ENDIF
								ELIF fDistanceSQR < SPEC_FADE_NORMAL_DISTANCE_SQR
									specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_SHORT	//targets are moderately near, short fade time
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_SHORT")
									#ENDIF
								ELSE
									IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
									OR MPGlobalsAmbience.bTeamTurnsSpectateCheck
										specData.specCamData.iSwitchFadeTime = 100
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = 100")
										#ENDIF
									ELSE
										specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_NORMAL	//targets are far, long fade time
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_NORMAL")
										#ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ELSE
							specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_NORMAL
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = SPEC_FADE_LOAD_TIME_NORMAL")
							#ENDIF
							IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
							OR MPGlobalsAmbience.bTeamTurnsSpectateCheck
								specData.specCamData.iSwitchFadeTime = 100
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] specData.specCamData.iSwitchFadeTime = 100")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
					SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_FADE_OUT)
				BREAK
				CASE eSPECFADESWITCHSTAGE_FADE_OUT
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_OUT")
					IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
						THEFEED_HIDE_THIS_FRAME()
						THEFEED_FLUSH_QUEUE()
					ENDIF
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					
					IF NOT IS_SCREEN_FADED_OUT()
					AND g_TransitionSessionNonResetVars.sPostMissionCleanupData.iSplashProg = 0
					AND specData.specCamData.iSwitchFadeTime > 0
					
						IF NOT IS_SCREEN_FADING_OUT()
							//LOAD TEXTURE
								START_SPEC_FADE_BIG_FEED(specData)
							//ENDIF
							
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Clash test switch fade out")
							#ENDIF
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("=== CAM === Clash test switch fade out")
							#ENDIF
							IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
							AND NOT IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)	//dont fade as hidden by renderphase
							AND NOT SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
								IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() = TRUE
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_OUT - DO FADE OUT")
									DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
								ELSE
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] DON'T FADE OUT THE RENDERPHASES ARE PAUSED")
									START_SPEC_FADE_BIG_FEED(specData)
									SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE)	//Fuck it, we'll do it anyway!
								ENDIF
							ELSE
								START_SPEC_FADE_BIG_FEED(specData)
								SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE)	//Fuck it, we'll do it anyway!
							ENDIF
						ENDIF
					ELSE
						SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE)
					ENDIF
				BREAK
				CASE eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE		
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_SETUP_LOAD_SCENE")
					CLEANUP_KILL_CAM()
					CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
					CLEANUP_BIG_MESSAGE()
					IF NOT g_bCelebrationScreenIsActive
						CLEANUP_ALL_KILL_STRIP()
					ENDIF
					
					IF NOT IS_PLAYER_SCTV(PLAYER_ID())
					OR NOT IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED()))
						IF NOT IS_PLAYER_WATCHING_A_MOCAP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED()))
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Switch TAKE_CONTROL_OF_TRANSITION")
							#ENDIF
							TAKE_CONTROL_OF_TRANSITION()
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Switch not TAKE_CONTROL_OF_TRANSITION as IS_PLAYER_WATCHING_A_MOCAP = TRUE")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Switch not TAKE_CONTROL_OF_TRANSITION as IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP")
						#ENDIF
					ENDIF
										
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					IF DOES_ENTITY_EXIST(pedSwitchingTo)
						IF IS_PED_INJURED(pedSwitchingTo)		//stupid dead checks
						OR NOT IS_PED_INJURED(pedSwitchingTo)
							
		
							IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
								IF NOT IS_PED_INJURED(GET_SPECTATOR_CURRENT_FOCUS_PED())
									SET_PED_LOD_MULTIPLIER(GET_SPECTATOR_CURRENT_FOCUS_PED(), 1.0)
								ENDIF
							ENDIF
							
							IF NOT SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
								SET_IN_SPECTATOR_MODE(TRUE, pedSwitchingTo)
							#IF IS_DEBUG_BUILD
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] Switch do not turn on spectator mode as corona is controlling the transition")
							#ENDIF
							
							ENDIF
						ENDIF
						
						
						IF pedSwitchingTo = PLAYER_PED_ID()
							CLEANUP_CAM_VIEW_MODES_CINEMATIC(specData.specCamData.camViewMode, FALSE)		//if going back to player, set the camera back to normal
						ELSE
							SETUP_CAM_VIEW_MODES_CINEMATIC(specData.specCamData.camViewMode, FALSE)		//otherwise, cinematic time! //removed for 1628663
						ENDIF
						
						/*IF NOT IS_ENTITY_DEAD(pedSwitchingTo)	//do this at the end, not here
							SET_SPECTATOR_CURRENT_FOCUS_PED(specData, pedSwitchingTo)
						ENDIF*/
						
						IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRequestLeaderBoardCam
							IF DOES_CAM_EXIST(g_TransitionSessionNonResetVars.sPostMissionCleanupData.ciLeaderBoardCam)
								IF pedSwitchingTo = PLAYER_PED_ID()				//Going back to player, camera on
									CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)
									IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
										DISPLAY_RADAR(FALSE)
									ENDIF
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)")
									#ENDIF
								ELSE											//Going to someone else, camera off
									SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)
									IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_CHANGING_RADAR)
									AND GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
										DISPLAY_RADAR(TRUE)
									ENDIF
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)")
									#ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] set new ped to spectate.")
						#ENDIF
						
						//if the player is moving at a slow speed, do a load scene
						//IF GET_ENTITY_SPEED(pedSwitchingTo) < SPEC_FADE_LOAD_SPEED_LIMIT
						IF (specData.specCamData.iSwitchFadeTime > 0
						OR specData.specCamData.bSkipFade)
						AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							
							IF specData.specCamData.iFrameDelay < 5
							AND NOT IS_ENTITY_FOCUS(PLAYER_PED_ID())
								specData.specCamData.iFrameDelay++
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] DELAY TO ALLOW PLAYER SWITCH CAM TO PROPERLY FINISH - iFrameDelay = ", specData.specCamData.iFrameDelay)
								RETURN FALSE
							ENDIF
							specData.specCamData.iFrameDelay = 0
							
							MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS(specData)		//force property shot
						
							IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
								specData.specCamData.vLoadScene = GET_CAM_COORD(specData.specCamData.camCinematic)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| specData.specCamData.vLoadScene = GET_CAM_COORD ", specData.specCamData.vLoadScene)	
							ELSE
								specData.specCamData.vLoadScene = GET_ENTITY_COORDS(pedSwitchingTo, FALSE)
								IF NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo) != INVALID_PLAYER_INDEX()
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| specData.specCamData.vLoadScene = GET_ENTITY_COORDS(", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo)),") = ", specData.specCamData.vLoadScene)
								ENDIF
							ENDIF
							
							IF (g_bUpdateSpectatorPosition)
							OR NETWORK_IS_ACTIVITY_SESSION()
								vLoadScenePosition = specData.specCamData.vLoadScene
								vLoadScenePosition.z = (vLoadScenePosition.z + 25.0)
								
								IF GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_TV_CHANNEL
								AND GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_NEWS
								 AND GET_SPEC_CAM_MODE(specData.specCamData) <> SPEC_MODE_HEIST 
								AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
								AND ABSF(vLoadScenePosition.z) < 2600
									IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
										IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
											SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
											SET_PED_HEATSCALE_OVERRIDE(PLAYER_PED_ID(), 0)
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Ped was still visible. Set visible flag to false before set entity coords. Returning false, to avoid setting ped invisible and warping on same frame.")
											RETURN FALSE
										ENDIF
									ENDIF
									IF IS_NET_PLAYER_OK(PLAYER_ID())
										SET_ENTITY_COORDS(PLAYER_PED_ID(), vLoadScenePosition, FALSE, TRUE, DEFAULT, FALSE)
										SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
										SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
									ELSE
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Player Ped is still dead... Return false here if we see ped corpse.")
									ENDIF
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| SET_ENTITY_COORDS(PLAYER_PED_ID()) = ", vLoadScenePosition)
									PlayersCurrentPosition = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Player is instantly at = ", PlayersCurrentPosition)
									#ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not moving local player to focus position.")
							ENDIF
							
							// Several NTs come in about focus to Bobby and James. 
							// We originally cleared focus but actually want to focus on position where we are going to load scene because of special cam!
							IF DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
								IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
									SET_FOCUS_POS_AND_VEL(specData.specCamData.vLoadScene, <<0,0,0>>)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| setting focus to load scene: SET_FOCUS_POS_AND_VEL(", specData.specCamData.vLoadScene, ", <<0,0,0>>)") 
								#IF IS_DEBUG_BUILD
								ELSE
									IF IS_PLAYER_SWITCH_IN_PROGRESS()
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not calling SET_FOCUS_POS_AND_VEL! Player switch in progress!")
									ELIF IS_NEW_LOAD_SCENE_ACTIVE()
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not calling SET_FOCUS_POS_AND_VEL! Load scene is already active!")
									ENDIF
								#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraSpectatorDebug")	
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| pedSwitchingTo = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo)))
									IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
										IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()) != INVALID_PLAYER_INDEX()
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| GET_SPECTATOR_CURRENT_FOCUS_PED = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())))
										ELSE
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| GET_SPECTATOR_CURRENT_FOCUS_PED = INVALID_PLAYER_INDEX. PED EXISTS, BUT PLAYER IS NO LONGER ATTACHED.")
										ENDIF
									ENDIF
									IF DOES_ENTITY_EXIST(GET_SPECTATOR_DESIRED_FOCUS_PED())
										IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()) != INVALID_PLAYER_INDEX()
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| GET_SPECTATOR_DESIRED_FOCUS_PED = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())))
										ELSE
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| GET_SPECTATOR_DESIRED_FOCUS_PED = INVALID_PLAYER_INDEX. PED EXISTS, BUT PLAYER IS NO LONGER ATTACHED.")
										ENDIF
									ENDIF
									IF DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
										IF NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED()) != INVALID_PLAYER_INDEX()
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| GET_SPECTATOR_SELECTED_PED = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_SELECTED_PED())))
										ELSE
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| GET_SPECTATOR_SELECTED_PED = INVALID_PLAYER_INDEX. PED EXISTS, BUT PLAYER IS NO LONGER ATTACHED.")
										ENDIF
									ENDIF
								ENDIF	
								#ENDIF
								
								IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
									SET_FOCUS_ENTITY(pedSwitchingTo)
									#IF IS_DEBUG_BUILD
									VECTOR vCoords
									vCoords = GET_ENTITY_COORDS(pedSwitchingTo, FALSE)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| setting focus to load scene: SET_FOCUS_ENTITY - PLAYER NAME ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo)), " COORDS ", vCoords)
									#ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									IF IS_PLAYER_SWITCH_IN_PROGRESS()
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not calling SET_FOCUS_ENTITY! Player switch in progress! - PLAYER NAME ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo)))
									ELIF IS_NEW_LOAD_SCENE_ACTIVE()
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not calling SET_FOCUS_ENTITY! Load scene is already active! - PLAYER NAME ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo)))
									ENDIF
								#ENDIF
								ENDIF
							ENDIF
							IF NOT NETWORK_IS_IN_SPECTATOR_MODE()
								IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
								AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
									NEW_LOAD_SCENE_START_SPHERE(specData.specCamData.vLoadScene, SPEC_FADE_LOAD_DISTANCE)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Calling NEW_LOAD_SCENE_START_SPHERE at ", specData.specCamData.vLoadScene, " with distance of ", SPEC_FADE_LOAD_DISTANCE)
								#IF IS_DEBUG_BUILD
								ELSE
									IF IS_PLAYER_SWITCH_IN_PROGRESS()
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not calling NEW_LOAD_SCENE_START_SPHERE! Player switch in progress!")
									ELIF IS_NEW_LOAD_SCENE_ACTIVE()
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Not calling NEW_LOAD_SCENE_START_SPHERE! Load scene is already active!")
									ENDIF
								#ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| MANAGE_FADING_SPECTATOR_TARGET_SWITCH - NETWORK_IS_IN_SPECTATOR_MODE = TRUE not calling NEW_LOAD_SCENE_START_SPHERE")
								#ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| doing a load scene!")
							#ENDIF
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| speed is ", GET_ENTITY_SPEED(pedSwitchingTo))
							#ENDIF
							SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_CHECK_LOAD_SCENE)
						ELSE
							specData.specCamData.vLoadScene = <<0,0,0>>
							CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_CHECK_LOAD_SCENE)
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| not doing a load scene!")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] pedSwitchingTo does not exist")
						#ENDIF
					ENDIF
					
					MAINTAIN_SPEC_FADE_BIG_FEED(specData)
					
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)	//might as well redo the hud since we're fade out
					specData.specCamData.timeSwitchSafety = GET_NETWORK_TIME()
					MAINTAIN_SETTING_SHOULD_SPEC_HIDE_GLOBAL(specData)
					RESET_NET_TIMER(specData.stFocusTargetDelay)
					SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_WAIT_FOR_LOAD_SCENE)
				BREAK
				CASE eSPECFADESWITCHSTAGE_WAIT_FOR_LOAD_SCENE
					bMoveOn = FALSE
										
					IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_CHECK_LOAD_SCENE)
						IF IS_NEW_LOAD_SCENE_ACTIVE()
						AND IS_NEW_LOAD_SCENE_LOADED()				//if switch over
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Switch loading completed")
							#ENDIF
							bMoveOn = TRUE
							
							IF IS_PED_A_PLAYER(pedSwitchingTo)
								IF IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo))
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| but we're in an unloaded vehicle")
									#ENDIF
									bMoveOn = FALSE
								ENDIF
							ENDIF
						
							#IF IS_DEBUG_BUILD
							IF bMoveOn
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Switch moving on as loading complete")
							ENDIF
							#ENDIF
						ELSE
							//clear load scene if we've left that area and disregard it
							IF NOT DOES_SPECIAL_SPECTATOR_CAMERA_EXIST(specData.specCamData)
								IF IS_ENTITY_DEAD(pedSwitchingTo)
								ENDIF
								IF VDIST2(specData.specCamData.vLoadScene, GET_ENTITY_COORDS(pedSwitchingTo, FALSE)) > SPEC_FADE_LOAD_DISTANCE_SQR
									NEW_LOAD_SCENE_STOP()
									CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_CHECK_LOAD_SCENE)
									bMoveOn = TRUE
									
									IF IS_FM_PLAYER_CRITICAL_TO_BUSINESS_BATTLE(PLAYER_ID())
										bMoveOn = FALSE
									ENDIF
									
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| we've left the load scene area, cleanup and disregard load scene")
									#ENDIF
								ENDIF
								IF GET_ENTITY_SPEED(pedSwitchingTo) > 2
									NEW_LOAD_SCENE_STOP()
									CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_CHECK_LOAD_SCENE)
									bMoveOn = FALSE
									RESET_NET_TIMER(specData.stFocusTargetDelay)
									START_NET_TIMER(specData.stFocusTargetDelay)
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| we're moving too fast cleanup and disregard load scene")
									#ENDIF
								ENDIF
								IF HAS_NET_TIMER_STARTED(specData.stFocusTargetDelay)
									IF NOT HAS_NET_TIMER_EXPIRED(specData.stFocusTargetDelay, ciARBITRARY_DELAY_TO_ALLOW_THE_WORLD_TO_STREAM_IN)
										RETURN FALSE
									ELSE
										bMoveOn = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bMoveOn
						IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), specData.specCamData.timeSwitchSafety) > specData.specCamData.iSwitchFadeTime
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Switch moving on as time is up")
							bMoveOn = TRUE
						ENDIF
					ENDIF
					
					IF IS_PLAYER_ON_STRAND_MISSION_CUTSCENE(playerSwitchTo)
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| IS_PLAYER_ON_STRAND_MISSION_CUTSCENE = TRUE - DON'T MOVE ON YET ")
						bMoveOn = FALSE
					ENDIF
					
					// If we are joining a player in the corona and on the vehicle screen, we should wait until we are in the correct state ourselves
					playerSwitchTo = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo)
					IF playerSwitchTo != INVALID_PLAYER_INDEX()
						IF IS_NET_PLAYER_OK(playerSwitchTo, FALSE)
							IF IS_THIS_PLAYER_IN_CORONA(playerSwitchTo)
								IF IS_THIS_PLAYER_ACTIVE_IN_CORONA_PAST_TRANSITION_SESSION(playerSwitchTo)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Spectated player is active in corona past transition screen")
									
									// Check we have made it through to the correct stage of the corona
									IF IS_PLAYER_IN_CORONA()
										IF NOT IS_CORONA_BIT_SET(CORONA_SCTV_FULLY_SETUP_IN_CORONA_SCREEN)
											IF GET_CORONA_STATUS() = CORONA_STATUS_INIT_TRAN_SCRIPT
												IF IS_SCREEN_FADED_OUT()
												AND NOT IS_SCREEN_FADING_IN()
													DO_SCREEN_FADE_IN(500)
													CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| We have not fully initialised our corona screen yet - DO_SCREEN_FADE_IN(500)")
												ENDIF
											ENDIF
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| We have not fully initialised our corona screen yet")
											bMoveOn = FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
//					//2375043 - Move on if screen is already faded in
//					IF IS_SCREEN_FADED_IN()
//						bMoveOn = TRUE
//						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Moving on since screen has already faded in")
//					ENDIF
					
					IF bMoveOn
					
						IF DOES_ENTITY_EXIST(pedSwitchingTo)
							IF NOT IS_PED_INJURED(pedSwitchingTo)
								IF IS_SCREEN_FADED_OUT()
									SET_PED_LOD_MULTIPLIER(pedSwitchingTo, 2.5)
								ENDIF
							ENDIF
						ENDIF
					
						IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_INIT_DEATH_CHECK)
							IF specData.specCamData.eMode <> SPEC_MODE_NEWS
							AND specData.specCamData.eMode <> SPEC_MODE_TV_CHANNEL
							 AND specData.specCamData.eMode <> SPEC_MODE_HEIST 
							AND NOT IS_PLAYER_SCTV(PLAYER_ID())
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] SET_BIT(specData.specspecCamData.iBitset, SPEC_CAM_BS_INIT_DEATH_CHECK), dead")
								#ENDIF
								SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SET_UP_DEATH_FILTER)
							ELSE
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] SET_BIT(specData.specspecCamData.iBitset, SPEC_CAM_BS_INIT_DEATH_CHECK), alive")
								#ENDIF
							ENDIF
							SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_INIT_DEATH_CHECK)
						ENDIF
						
						NEW_LOAD_SCENE_STOP()
						DONT_RENDER_IN_GAME_UI(FALSE)
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] DONT_RENDER_IN_GAME_UI(FALSE) - A")
						RESET_NET_TIMER(specData.stFocusTargetDelay)
						SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_FADE_IN)
					ELSE
					
						//OVERRIDE_LODSCALE_THIS_FRAME(1.0)
					
						#IF IS_DEBUG_BUILD
							IF GET_FRAME_COUNT() % 10 = 0
								CPRINTLN(DEBUG_SPECTATOR, 	"=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| Faded Switch Loading - Time left until fade in = ", 
															specData.specCamData.iSwitchFadeTime - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), specData.specCamData.timeSwitchSafety))
								PlayersCurrentPosition = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] |LOADING| WAIT_FOR_LOAD_SCENE Player is at = ", PlayersCurrentPosition)
							ENDIF
						#ENDIF
					ENDIF
					
					MAINTAIN_SPEC_FADE_BIG_FEED(specData)
				BREAK
				CASE eSPECFADESWITCHSTAGE_FADE_IN
					
					MAINTAIN_SPEC_FADE_BIG_FEED(specData)
					
					//Clear the Skip Fade Flag if in a Corona
					IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
						IF IS_PLAYER_IN_CORONA()
						OR IS_MISSION_END_FOR_SCTV()
							CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING - CLEARED")
						ENDIF
					ENDIF
					
					IF bOnlyFadeInIfTargetAlive
					AND IS_PED_INJURED(pedSwitchingTo)
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - TARGET INJURED - eSPECFADESWITCHSTAGE_CLEANUP")
						SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_CLEANUP)
					ELSE			
						IF NOT IS_SCREEN_FADED_IN()
							IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
								IF NOT IS_SCREEN_FADING_IN()
									IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
									AND NOT IS_PLAYER_FLAGGING_SHOULD_FADE_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedSwitchingTo), MPGlobalsAmbience.bTeamTurnsSpectateCheck)
										IF IS_PLAYER_IN_CORONA()
										OR IS_MISSION_END_FOR_SCTV()
											DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - FADE IN")
										ELSE
											SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING - SET")
										ENDIF
										RESET_NET_TIMER(specData.specHUDData.FadeInTimer)
										BUSYSPINNER_OFF()
										SET_CONTROL_VALUE_NEXT_FRAME(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_LR, 1)
									ELSE
										CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - FADE CLASH - eSPECFADESWITCHSTAGE_CLEANUP")
										SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_CLEANUP)	//Fuck it, we'll do it anyway!
									ENDIF
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING - eSPECFADESWITCHSTAGE_CLEANUP")
								SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_CLEANUP)
							ENDIF
						ELSE
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] eSPECFADESWITCHSTAGE_FADE_IN - ALREADY FADED IN - eSPECFADESWITCHSTAGE_CLEANUP")
							SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_CLEANUP)
						ENDIF
					ENDIF
				BREAK
				CASE eSPECFADESWITCHSTAGE_CLEANUP
					CLEANUP_SPEC_FADE_BIG_FEED()
					CLEANUP_FORCE_SPECTATOR_CAM(specData.specCamData)
					IF NOT bOnlyFadeInIfTargetAlive				//If allowed to fade in whenever
					OR NOT IS_PED_INJURED(pedSwitchingTo)		//or criteria for fade is met
						NEW_LOAD_SCENE_STOP()
					ENDIF
					SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP)
					SET_SPEC_CAM_DATA_DO_TRANSITION(specData.specCamData, FALSE) //We do not want to switch any more
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	// Complete
	IF NOT specData.specCamData.bDoTransition
	
		specData.specCamData.bFirstTransitionComplete = TRUE
		
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)")
		#ENDIF
		CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_WAITING_FOR_SWITCH)
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_CHECK_LOAD_SCENE)
		
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)		//Update HUD display of lists
		
		SET_REDO_NEWS_HUD_DUE_TO_EVENT()
		
		SET_SPECTATOR_CURRENT_FOCUS_PED(specData, GET_SPECTATOR_DESIRED_FOCUS_PED())
		
		IF IS_PED_A_PLAYER(GET_SPECTATOR_DESIRED_FOCUS_PED())
			specData.specCamData.iSpectateTargetPlayerID = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
		ELSE
			specData.specCamData.iSpectateTargetPlayerID = -1
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Manage the spectator cam swooping up to return to the local player
/// PARAMS:
///    specData - 
/// RETURNS:
///    true when complete
FUNC BOOL MANAGE_SPECTATOR_SWOOP_UP(SPECTATOR_DATA_STRUCT &specData)

	IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_FADE_IN_FOR_SWOOP_CALLED)
		IF IS_SCREEN_FADED_IN()
		OR IS_SCREEN_FADING_IN()
			SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_FADE_IN_FOR_SWOOP_CALLED)
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Clash test manage swoop fade in")
			#ENDIF
			IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_FADE_IN_FOR_SWOOP_CALLED)
			ENDIF
		ENDIF
	ENDIF

	IF SET_SKYSWOOP_UP(TRUE)
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_FADE_IN_FOR_SWOOP_CALLED)
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 10 = 0
				CPRINTLN(DEBUG_SPECTATOR, 	"=== CAM === Spectator swooping up")
			ENDIF
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs every frame update of the radar so that it is positioned based on the spectator target, and rotated based on the local rendering camera
/// PARAMS:
///    specCamData - 
PROC MAINTAIN_RADAR_FOR_SPECTATOR_CAM(SPECTATOR_CAM_DATA &specCamData)

	// If the local player is in a corona we don't need to centre the map
	IF IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF

	// update radar position
	BOOL bZoomSet = FALSE
	IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
		IF IS_ENTITY_DEAD(GET_SPECTATOR_CURRENT_FOCUS_PED())
		ENDIF
		VECTOR vPos = GET_ENTITY_COORDS(GET_SPECTATOR_CURRENT_FOCUS_PED(), FALSE)
		VECTOR vRot = GET_FINAL_RENDERED_CAM_ROT()
		INT iRot = ROUND(vRot.z)
		IF iRot < 0
			iRot += 360
		ENDIF
		LOCK_MINIMAP_ANGLE(iRot)
		LOCK_MINIMAP_POSITION(vPos.x, vPos.y)
		
		//LOCK_MINIMAP_ANGLE(ROUND(GET_ENTITY_HEADING(GET_SPECTATOR_CURRENT_FOCUS_PED())))
		IF IS_PED_IN_ANY_VEHICLE(GET_SPECTATOR_CURRENT_FOCUS_PED())
			SET_RADAR_ZOOM(900)
			bZoomSet = TRUE
		ENDIF
		SET_BIT(specCamData.iBitset, SPEC_CAM_BS_RADAR_LOCKED)
	ENDIF
	IF NOT bZoomSet
		SET_RADAR_ZOOM(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the local customised radar position and rotation
/// PARAMS:
///    specCamData - 
PROC CLEANUP_RADAR_FOR_SPECTATOR_CAM(SPECTATOR_CAM_DATA &specCamData)
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	SET_RADAR_ZOOM(0)
	SET_BIT(specCamData.iBitset, SPEC_CAM_BS_RADAR_LOCKED)
ENDPROC

/// PURPOSE:
///    Process switching to a new spec cam view target. 
///    Loads scene at new targets coords then switches cam to view them.
/// PARAMS:
///    specCamData - struct of spec cam data.
PROC PROCESS_eSPECCAMSTATE_SWITCHING(SPECTATOR_DATA_STRUCT &specData)
	
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
		//If we are not waiting to confirm aim state - 1825602 - [NT] After choosing to spectate a friend who was in a freeaim session, while I was already friend spectating someone else, I got stuck on skycam.
		IF NOT IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF)
			IF MANAGE_FADING_SPECTATOR_TARGET_SWITCH(specData)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Moving into watching")
				#ENDIF
				
				SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_WATCHING)
			ENDIF
		ENDIF		
	ELSE
		
		CLEANUP_SKYSWOOP_CLEANUP_DATA()
		CLEANUP_FADE_SWITCH_DATA(specData.specCamData)
		SET_SPEC_CAM_STATE_TO_APPROPRIATE_TURN_OFF_METHOD(specData)
	ENDIF
	
	PRIVATE_MAINTAIN_TURN_OFF_SPEC_CAM_IF_NOT_SAFE_TO_RUN(specData)
	
ENDPROC

/// PURPOSE:
///    Checks if the spectated player is in an event where they can end up with a new ped (respawn)
/// PARAMS:
///    specData - 
/// RETURNS:
///    true/false
FUNC BOOL CAN_SPECTATE_TARGET_GET_NEW_PED(SPECTATOR_DATA_STRUCT &specData)
	IF specData.specCamData.iSpectateTargetPlayerID <> -1
		IF NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(specData.specCamData.iSpectateTargetPlayerID))
			IF GlobalplayerBD_FM[specData.specCamData.iSpectateTargetPlayerID].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
			OR GlobalplayerBD_FM[specData.specCamData.iSpectateTargetPlayerID].iCurrentMissionType = FMMC_TYPE_RACE
				//IF IS_BIT_SET(GlobalplayerBD[iSpectateTargetPlayerID].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
					RETURN TRUE
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Mimics the local machine fading in and out based on what the spectator target is doing
PROC MANAGE_SPECTATOR_COPY_TARGET_FADING(SPECTATOR_DATA_STRUCT &specData)
	INT iPlayer = NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
							
	BOOL bBlockFadeOut = FALSE
	
	BOOL bBlockFading = FALSE
	
	IF iPlayer > -1
		IF IS_PLAYER_IN_PROPERTY(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()), TRUE)
		OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
		OR IS_BIT_SET(GlobalplayerBD_FM[iPlayer].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)
		OR IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(INT_TO_PLAYERINDEX(iPlayer))
		OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(INT_TO_PLAYERINDEX(iPlayer))
			bBlockFadeOut = TRUE	//Allow only fade in
		ENDIF
	ENDIF
	
	IF ARE_SPECTATOR_COPY_TARGET_FADES_BLOCKED()
		bBlockFadeOut = TRUE
		bBlockFading = TRUE
	ENDIF
	
	IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_IN_ONCE_SPEC_HUD_IS_RENDERING)
		bBlockFading = TRUE
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_WARP_TO_END_MOCAP_IN_PROGRESS)
		bBlockFading = TRUE
	ENDIF
	
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet2, iABI2_BLOCK_SPECTATOR_TARGET_COPY_FADING_IN)
		bBlockFading = TRUE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
		bBlockFading = TRUE
	ENDIF
	
	IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(PLAYER_ID())
		IF IS_PLAYER_DEAD(PLAYER_ID())
			bBlockFading = TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_RUNNING_ANY_TYPE_OF_POST_MISSION_SCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
	OR g_bCelebrationScreenIsActive = TRUE
		bBlockFading = TRUE
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF GET_SPEC_SKYSWOOP_CLEANUP_STAGE() = eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT
		OR GET_SPEC_SKYSWOOP_CLEANUP_STAGE() = eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER
			bBlockFading = TRUE
		ENDIF
		
		IF IS_PLAYER_IN_CORONA()
			bBlockFading = TRUE
		ENDIF
		IF IS_MISSION_END_FOR_SCTV()
			bBlockFading = TRUE
		ENDIF
	ENDIF	
	
	// If we've begun fading in the screen we no longer care about this timer.
	IF IS_SCREEN_FADING_IN()
	OR IS_SCREEN_FADED_IN()
		RESET_NET_TIMER(specData.specHUDData.FadeInTimerWaitForNetSync)
	ENDIF
	
	IF NOT bBlockFading
		IF IS_PLAYER_FLAGGING_SHOULD_FADE_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
		AND NOT bBlockFadeOut
			// fade out immediately
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				#IF IS_DEBUG_BUILD
				IS_PLAYER_FLAGGING_SHOULD_FADE_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()), TRUE)//print debug
				#ENDIF
				IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD[iPlayer].iSpecInfoBitset, SPEC_INFO_BS_FADING_OUT)
			OR IS_BIT_SET(GlobalPlayerBD_FM_3[iPlayer].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_PLAYING_DELIVERY_CUTSCENE)
			OR IS_BIT_SET(GlobalPlayerBD_FM_3[iPlayer].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
			OR IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_FADE_OUT)
			AND NOT bBlockFadeOut
				// fade out
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADING_IN()
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Clash test player I am spectating is fading out")
					#ENDIF
					IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
				ENDIF
			ELSE
				IF (NOT IS_PLAYER_FLAGGING_SHOULD_FADE_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
				AND NOT IS_PLAYER_RESPAWNING(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())))
				OR GET_SPECTATOR_DESIRED_FOCUS_PED() = NULL
					// fade in
					IF IS_SCREEN_FADED_OUT()
					AND NOT IS_SCREEN_FADING_IN()
					AND NOT IS_SCREEN_FADING_OUT()
						IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN)
							#IF IS_DEBUG_BUILD
							IF (NOT IS_PLAYER_FLAGGING_SHOULD_FADE_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
							AND NOT IS_PLAYER_RESPAWNING(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())))
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Clash test player I am spectating is fading in, as they are no longer respawning")
							ELIF GET_SPECTATOR_DESIRED_FOCUS_PED() = NULL
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Clash test player I am spectating is fading in, as desired focus = NULL.")
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Clash test player I am spectating is fading in. If this is printing, something has gone wrong.")
							ENDIF
							#ENDIF
								
							IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()), TRUE)
							
								IF HAS_COLLISION_LOADED_AROUND_ENTITY(GET_SPECTATOR_DESIRED_FOCUS_PED())
									
									IF NOT HAS_NET_TIMER_STARTED(specData.specHUDData.FadeInTimerWaitForNetSync) // Extra little timer to allow the client we're spectating to netsync back to the ground as sometimes they are viewed as under the map even though locally the client is not.
										START_NET_TIMER(specData.specHUDData.FadeInTimerWaitForNetSync)
									ENDIF									
									IF HAS_NET_TIMER_EXPIRED_READ_ONLY(specData.specHUDData.FadeInTimerWaitForNetSync, 1500)
										INT iFadeTime = DEFAULT_FADE_TIME_LONG
										IF IS_TRANSFORM_RACE()
										OR CONTENT_IS_USING_ARENA()
											iFadeTime = 3500
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Fading in time set to ", iFadeTime, " as this is a transform race or arena content.")
										ELSE
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Fading in time set to ", iFadeTime)
										ENDIF
										
										IF NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
											IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
												DO_SCREEN_FADE_IN(iFadeTime)
												SET_CONTROL_VALUE_NEXT_FRAME(PLAYER_CONTROL, INPUT_VEH_CINEMATIC_LR, 1)
											ENDIF
										ENDIF
									ENDIF								
									
								ELSE
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Player collision not loaded yet. Waiting until it is.")
								ENDIF
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Player is Concealed. Waiting until they are not.")
							ENDIF
							
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_FADING - Clash test  IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DO_FADE_OUT_FOR_POST_CELEB_SCREEN) = TRUE")
							#ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Mimics the local machine swooping up and down based on what the spectator target is doing
/// PARAMS:
///    specCamData - 
PROC MANAGE_SPECTATOR_COPY_TARGET_SWITCHING(SPECTATOR_DATA_STRUCT &specData)
	
	BOOL bIsTargetPlayerFlaggingShouldSkyswoopUp = FALSE		//decide what player to base the local swooping on
	
	PLAYER_INDEX desiredFocusPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())
		
	IF desiredFocusPlayer != INVALID_PLAYER_INDEX()
	AND NETWORK_IS_PLAYER_ACTIVE(desiredFocusPlayer)
		IF IS_BIT_SET(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE)
			//If the target player is in the sky
			IF (IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) 
			//And they are not spectating
			AND NOT IS_PLAYER_SPECTATING(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget) 
			//And they are not at the ent of the mission
			AND NOT IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
				IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
				OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bIsTargetPlayerFlaggingShouldSkyswoopUp - SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE - playerSCTVTarget = FMMC_TYPE_RACE")
						bIsTargetPlayerFlaggingShouldSkyswoopUp = TRUE
					ENDIF
				ELSE			
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bIsTargetPlayerFlaggingShouldSkyswoopUp - SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE - playerSCTVTarget")
					bIsTargetPlayerFlaggingShouldSkyswoopUp = TRUE
				ENDIF
			ENDIF
			
			//If the current focus player is in the sky
			IF IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bIsTargetPlayerFlaggingShouldSkyswoopUp - SCTV_BIT_SPECTATING_TEMP_TARGET_AS_FOLLOW_TARGET_UNSUITABLE - GET_SPECTATOR_DESIRED_FOCUS_PED")
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bIsTargetPlayerFlaggingShouldSkyswoopUp - ")
				bIsTargetPlayerFlaggingShouldSkyswoopUp = TRUE
			ENDIF
		ELSE
			IF IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bIsTargetPlayerFlaggingShouldSkyswoopUp - GET_SPECTATOR_DESIRED_FOCUS_PED")			
				bIsTargetPlayerFlaggingShouldSkyswoopUp = TRUE
			ENDIF
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MANAGE_SPECTATOR_COPY_TARGET_SWITCHING - GET_SPECTATOR_DESIRED_FOCUS_PED = INVALID_PLAYER_INDEX or NOT ACTIVE. GET_SPECTATOR_DESIRED_FOCUS_PED = ", NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())))		
	ENDIF
	
	//Don't follow if Player is GTAOTV Spectator in Tournament
	IF bIsTargetPlayerFlaggingShouldSkyswoopUp = TRUE
		IF IS_PLAYER_A_ROCKSTAR_DEV()
		AND IS_THIS_TRANSITION_SESSION_IS_A_TOURNAMENT_PLAYLIST()
			bIsTargetPlayerFlaggingShouldSkyswoopUp = FALSE
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bIsTargetPlayerFlaggingShouldSkyswoopUp - RESET TO FALSE - PLAYER IS GTAOTV SPECTATOR IN TOURNAMENT")
		ENDIF
	ENDIF
	
	IF IS_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()	//Spectator player is actually skyswooping, not mimicing spectate target
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		OR GET_SKYSWOOP_STAGE() = SKYSWOOP_INSKYSTATIC //url:bugstar:6586129 - D-Day - Local Player spectating is stuck on Skycam after Remote Players complete mission.
			CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
		ENDIF
	ELSE											//Spectator player is mimicing the spectate target
	
		IF bIsTargetPlayerFlaggingShouldSkyswoopUp
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			OR GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP
				IF NOT HAS_NET_TIMER_STARTED(specData.stBailDelay)
				OR HAS_NET_TIMER_EXPIRED(specData.stBailDelay, 5000)
				OR GET_SKYSWOOP_STAGE() = SKYSWOOP_GOINGUP
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SKYSWOOP_UP as IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP")
					#ENDIF
	//				//If we're not actioning a follow invite
	//				IF NOT TRANSITION_SESSION_ACTIONING_FOLLOW_INVITE()
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
						
						RESET_NET_TIMER(specData.stBailDelay)
						
						SPEC_BAIL_FOR_TRANSITION(specData, IS_TRANSITION_SESSION_ACTIONING_FOLLOW_INVITE())
						
						SET_IN_SPECTATOR_MODE(FALSE)
						
						SET_SKYSWOOP_UP(TRUE, TRUE, TRUE, SWITCH_TYPE_LONG, FALSE, 0.0, 0.0, 0.0, TRUE, FALSE, FALSE, TRUE)
	//				ENDIF
				ENDIF
			ELSE
				REINIT_NET_TIMER(specData.stBailDelay)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === - REINIT_NET_TIMER - stBailDelay")
			ENDIF
		ELSE
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			
				IF GET_SPEC_SKYSWOOP_CLEANUP_STAGE() = eSPECSKYSWOOPCLEANUPSTAGE_OFF
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Taking control of transition as IS_PLAYER_FLAGGING_SHOULD_SKYSWOOP_UP = FALSE")
					#ENDIF
					//can't skyswoop down to moving target, have to fade down
					SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT)
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH GET_SPEC_SKYSWOOP_CLEANUP_STAGE()
		
			CASE eSPECSKYSWOOPCLEANUPSTAGE_OFF
				
			BREAK
			
			CASE eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT
				IF NOT IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_OUT()
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Clash test skyswoop cleanup fade out")
						#ENDIF
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("=== CAM === Clash test skyswoop cleanup fade out")
						#ENDIF
						IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
						AND NOT IS_BIT_SET(specData.specCamData.iActivateBitset, ASCF_INDEX_CELEBRATION_SCREEN)	//dont fade as hidden by renderphase
						AND NOT SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
							DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
						ELSE
							SET_IN_SPECTATOR_MODE(TRUE, GET_SPECTATOR_CURRENT_FOCUS_PED())
							CLEAR_SPEC_BAILED_FOR_TRANSITION()
							TAKE_CONTROL_OF_TRANSITION()
							SET_EXTERNAL_SCTV_QUIT_HAS_BEEN_CANCELLED(TRUE)
							SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_OFF)	//Fuck it, take control of transition and turn off
						ENDIF
					ENDIF
				ELSE
					specData.specCamData.timeSkyswoopCleanupSafety = GET_NETWORK_TIME()
					SET_IN_SPECTATOR_MODE(TRUE, GET_SPECTATOR_CURRENT_FOCUS_PED())
					CLEAR_SPEC_BAILED_FOR_TRANSITION()
					TAKE_CONTROL_OF_TRANSITION()
					SET_EXTERNAL_SCTV_QUIT_HAS_BEEN_CANCELLED(TRUE)
					SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER)
				ENDIF
			BREAK
			
			CASE eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER
				IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), specData.specCamData.timeSkyswoopCleanupSafety) > SPEC_FADE_LOAD_TIME_NORMAL
					SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_FADE_IN)
				ELSE
					#IF IS_DEBUG_BUILD
						IF GET_FRAME_COUNT() % 10 = 0
							CPRINTLN(DEBUG_SPECTATOR, 	"=== CAM === |LOADING| Skyswoop cleanup Loading - Time left until fade in = ", 
														SPEC_FADE_LOAD_TIME_NORMAL - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), specData.specCamData.timeSkyswoopCleanupSafety))
						ENDIF
					#ENDIF
				ENDIF
			BREAK
			
			CASE eSPECSKYSWOOPCLEANUPSTAGE_FADE_IN
				IF NOT IS_SCREEN_FADED_IN()
					IF NOT IS_SCREEN_FADING_IN()
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Clash test skyswoop cleanup fade in")
						#ENDIF
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("=== CAM === Clash test skyswoop cleanup fade in")
						#ENDIF
						IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						ELSE
							SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_OFF)	//Fuck it, we'll do it anyway!
						ENDIF
					ENDIF
				ELSE
					SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_OFF)
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if the current target is valid, and either tries to find a new target or locks the camera in place when they are not valid
/// PARAMS:
///    specData - 
///    bPopulateTargetLists - Allow the list to instantly, expensively update to find a new target
///    bFadeOutIfFound - Fade out instantly if the target was found to hide any pops
PROC REACT_TO_TARGET_NO_LONGER_BEING_VALID(SPECTATOR_DATA_STRUCT &specData, BOOL bPopulateTargetLists, BOOL bFadeOutIfFound)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === REACT_TO_TARGET_NO_LONGER_BEING_VALID")
	#ENDIF
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR (IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData) AND IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData))
		IF bPopulateTargetLists
			POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
		ENDIF
		INT iList, iEntry
		IF SHOULD_FIND_ANOTHER_SPECTATOR_TARGET(specData)
		AND GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY(specData, iList, iEntry, FALSE, FALSE)
			SET_SPECTATOR_DESIRED_FOCUS_PED(specData, GET_SPECTATOR_HUD_TARGET_LIST_ENTRY_PED_ID(specData.specHUDData, iList, iEntry))
			SETUP_FORCE_SPECTATOR_CAM(specData.specCamData, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
			IF bFadeOutIfFound
			AND NOT IS_TRANSITION_SESSION_RESTARTING()
			AND GET_SCTV_MODE() != SCTV_MODE_FREE_CAM
				DO_SCREEN_FADE_OUT(0)
			ENDIF
		ENDIF
	ELSE
		SETUP_FORCE_SPECTATOR_CAM(specData.specCamData, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
	ENDIF
ENDPROC

FUNC BOOL BLOCK_SWITCH_BETWEEN_PLAYLIST_MISSIONS()
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND NATIVE_TO_INT(PLAYER_ID()) != -1
	AND IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_INVALID
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEANUP_RESTRICTED_INTERIOR_FOR_SPECTATOR(SPECTATOR_DATA_STRUCT &specData)
	IF specData.playerInterior != RESTRICTED_INTERIOR_INVALID
		IF DOES_PLAYER_HAVE_ACCESS_TO_RESTRICTED_INTERIOR(PLAYER_ID(), specData.playerInterior)
			SET_RESTRICTED_INTERIOR_ACCESS_FOR_LOCAL_PLAYER(specData.playerInterior, FALSE)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_RESTRICTED_INTERIOR_FOR_SPECTATOR, CLEANUP_RESTRICTED_INTERIOR_FOR_SPECTATOR")
		ENDIF
		specData.playerInterior = RESTRICTED_INTERIOR_INVALID
	ENDIF
ENDPROC

PROC MAINTAIN_RESTRICTED_INTERIOR_FOR_SPECTATOR(SPECTATOR_DATA_STRUCT &specData, PED_INDEX pedId)
	IF DOES_PLAYER_HAVE_ACCESS_TO_A_RESTRICTED_INTERIOR(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId))
		IF specData.playerInterior = RESTRICTED_INTERIOR_INVALID
			specData.playerInterior = GET_RESTRICTED_INTERIOR_ENTITY_IS_IN(pedId)
		ELSE
			IF NOT DOES_PLAYER_HAVE_ACCESS_TO_RESTRICTED_INTERIOR(PLAYER_ID(), specData.playerInterior)
				SET_RESTRICTED_INTERIOR_ACCESS_FOR_LOCAL_PLAYER(specData.playerInterior, TRUE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_RESTRICTED_INTERIOR_FOR_SPECTATOR, SET_RESTRICTED_INTERIOR_ACCESS_FOR_LOCAL_PLAYER")
			ENDIF
		ENDIF
	ELSE
		CLEANUP_RESTRICTED_INTERIOR_FOR_SPECTATOR(specData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Performs the watching stage of the spectator cam. Watches out for needing to switch to a different target and changes stage appropriately
/// PARAMS:
///    specData - 
PROC PROCESS_eSPECCAMSTATE_WATCHING(SPECTATOR_DATA_STRUCT &specData)
	
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
	
		IF NOT IS_TRANSITION_ACTIVE()
			
			IF specData.specCamData.bFirstTransitionComplete
				
				
				IF IS_PLAYER_SCTV(PLAYER_ID())
					IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCTV_IN_MOCAP)					
						PRINTLN(" PROCESS_eSPECCAMSTATE_WATCHING - EXIT EARLY AND HIDE HUD DUE TO MOCAP / SHOULD_SPECTATOR_BE_ROAMING_AT_START()")
						HIDE_HUD_AND_RADAR_THIS_FRAME()
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
						HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
						//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
						HIDE_HELP_TEXT_THIS_FRAME()
						THEFEED_HIDE_THIS_FRAME()
						EXIT
					ENDIF
				ENDIF
				
				// We shouldn't need to return control to the player here: 1801852
				IF DID_I_JOIN_MISSION_AS_SPECTATOR()
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DID_I_JOIN_MISSION_AS_SPECTATOR, SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)")
						NET_PRINT("***  SET_PLAYER_CONTROL *** FAKE THIS IS SPECTATOR CAM OVERRIDING TO HAVE CONTROL ON") NET_NL()
						NET_PRINT("     Called by script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
						#ENDIF
					ENDIF
				ENDIF
				
				IF SHOULD_SPECTATOR_BE_ROAMING_AT_START()
				AND NOT IS_PLAYER_SCTV(PLAYER_ID())
					PRINTLN("[SPEC_SPEC] PROCESS_eSPECCAMSTATE_WATCHING - SHOULD_SPECTATOR_BE_ROAMING_AT_START is set, forcing exit.")
					EXIT
				ENDIF
				
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	            ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
	            ALLOW_PAUSE_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
				
				MAINTAIN_SETTING_SHOULD_SPEC_HIDE_GLOBAL(specData)
				
				IF CAN_SPECTATE_TARGET_GET_NEW_PED(specData)		//if we're in an event where the player can get a new ped
					IF GET_PLAYER_PED(INT_TO_PLAYERINDEX(specData.specCamData.iSpectateTargetPlayerID)) <> GET_SPECTATOR_CURRENT_FOCUS_PED()//if the player has a new ped
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === UPDATING SPECTATED TARGET PLAYER PED")
						#ENDIF
						SET_SPECTATOR_CURRENT_FOCUS_PED(specData, GET_PLAYER_PED(INT_TO_PLAYERINDEX(specData.specCamData.iSpectateTargetPlayerID)))	//move to this new ped
						SET_SPECTATOR_DESIRED_FOCUS_PED(specData, GET_PLAYER_PED(INT_TO_PLAYERINDEX(specData.specCamData.iSpectateTargetPlayerID)))
						SET_IN_SPECTATOR_MODE(TRUE, GET_SPECTATOR_CURRENT_FOCUS_PED())
						SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
					ENDIF
				ENDIF
				
				IF SPEC_IS_PED_ACTIVE(GET_SPECTATOR_DESIRED_FOCUS_PED())
				
					IF GET_SPECTATOR_DESIRED_FOCUS_PED() <> PLAYER_PED_ID()
						IF IS_PED_A_PLAYER(GET_SPECTATOR_DESIRED_FOCUS_PED())
							
							//Mimic target player fading
							MANAGE_SPECTATOR_COPY_TARGET_FADING(specData)
							
							IF IS_PLAYER_SCTV(PLAYER_ID())
								//Mimic target player switching
								MANAGE_SPECTATOR_COPY_TARGET_SWITCHING(specData)
							ENDIF
							
							//Fake wanted level
							INT iCurrentWanted = GET_PLAYER_WANTED_LEVEL(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
							IF GET_PLAYER_FAKE_WANTED_LEVEL(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())) > iCurrentWanted
								iCurrentWanted = GET_PLAYER_FAKE_WANTED_LEVEL(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
							ENDIF
							IF iCurrentWanted <> specData.specHUDData.iCurrentFakeWantedLevel
								specData.specHUDData.iCurrentFakeWantedLevel = iCurrentWanted
								SET_FAKE_WANTED_LEVEL(specData.specHUDData.iCurrentFakeWantedLevel)
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_WATCHING - SET_FAKE_WANTED_LEVEL = ", specData.specHUDData.iCurrentFakeWantedLevel)
							ENDIF
							//Check if Wanted Stars should be flashing
							IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
								IF ARE_PLAYER_STARS_GREYED_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())) != specData.specHUDData.bFakeWantedLevelFlashing
									specData.specHUDData.bFakeWantedLevelFlashing = ARE_PLAYER_STARS_GREYED_OUT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
									FLASH_WANTED_DISPLAY(specData.specHUDData.bFakeWantedLevelFlashing)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_WATCHING - FLASH_WANTED_DISPLAY = ", PICK_STRING(specData.specHUDData.bFakeWantedLevelFlashing, "TRUE", "FALSE"))
								ENDIF
							ENDIF
							
							BOOL bShouldResetTimer = TRUE

							IF specData.specCamData.eMode = SPEC_MODE_NEWS
							OR specData.specCamData.eMode = SPEC_MODE_TV_CHANNEL
							 OR specData.specCamData.eMode = SPEC_MODE_HEIST 
								IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
									IF IS_THIS_PLAYER_IN_CORONA(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED()))
										//ohshitwhatthefu- immediately fade out as the player is just about to, or has already, disappeared
										SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
										IF IS_SCREEN_FADED_IN()
										AND NOT IS_SCREEN_FADING_OUT()
											DO_SCREEN_FADE_OUT(0)
											#IF IS_DEBUG_BUILD
											CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_WATCHING - FADE OUT DESIRED TARGET IS IN CORONA")
											#ENDIF
										ENDIF
										bShouldResetTimer = FALSE
									ENDIF
								ENDIF
							ENDIF
							
							IF HAS_NET_TIMER_STARTED(specData.stSwitchTargetDelay)
								IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(GET_SPECTATOR_DESIRED_FOCUS_PED(),  GET_SPEC_CAM_MODE(specData.specCamData))
								AND bShouldResetTimer
									RESET_NET_TIMER(specData.stSwitchTargetDelay)
									CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_WATCHING - Timer had started. RESET_NET_TIMER(specData.stSwitchTargetDelay)")
								ENDIF
							ENDIF
							
							MAINTAIN_RESTRICTED_INTERIOR_FOR_SPECTATOR(specData, GET_SPECTATOR_DESIRED_FOCUS_PED())
							
						ENDIF
						
					ENDIF
					
					// Turn on Kev's hud stuff.
					//on_player_selected_screen = TRUE // Set player on single player selected screen when hud is turned on
					IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
						IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_DISABLED)
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
							CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SPEC_HUD_BS_ACTIVE - SET")
							POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
						ENDIF
					ENDIF
					
					// If we do not have to turn off.
					IF GET_SPEC_CAM_STATE(specData.specCamData) != eSPECCAMSTATE_TURN_OFF
					AND GET_SPEC_CAM_STATE(specData.specCamData) != eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER
					AND GET_SPEC_CAM_STATE(specData.specCamData) != eSPECCAMSTATE_FINAL_SWOOP_UP
						IF GET_SPECTATOR_DESIRED_FOCUS_PED() !=  GET_SPECTATOR_CURRENT_FOCUS_PED()
							IF NOT BLOCK_SWITCH_BETWEEN_PLAYLIST_MISSIONS()
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Ped are not the same, so switch now")
								#ENDIF
								//CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
								//on_player_selected_screen = FALSE // Turn off single selected screen
								SET_SPEC_FADE_SWITCH_STAGE(specData.specCamData, eSPECFADESWITCHSTAGE_SETUP)
								SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_SWITCHING)
								SET_SPEC_CAM_DATA_DO_TRANSITION(specData.specCamData, TRUE)
								CLEANUP_FADE_SWITCH_DATA(specData.specCamData)
							ENDIF
						ENDIF
					ENDIF
					
				ELSE														//GONE WRONG
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Current focus target is not OK")
					#ENDIF		
//					IF NOT TRANSITION_SESSION_ACTIONING_FOLLOW_INVITE()
						REACT_TO_TARGET_NO_LONGER_BEING_VALID(specData, TRUE, FALSE)
//					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
	
		CLEANUP_FADE_SWITCH_DATA(specData.specCamData)
		SET_SPEC_CAM_STATE_TO_APPROPRIATE_TURN_OFF_METHOD(specData)
	ENDIF
	
	PRIVATE_MAINTAIN_TURN_OFF_SPEC_CAM_IF_NOT_SAFE_TO_RUN(specData)
	
ENDPROC

/// PURPOSE:
///    Process switching back to the player as the spec cam turns off 
///    Loads scene at new targets coords then switches cam to view them.
/// PARAMS:
///    specCamData - struct of spec cam data.
PROC PROCESS_eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER(SPECTATOR_DATA_STRUCT &specData)
	
	IF GET_SPECTATOR_DESIRED_FOCUS_PED() <> PLAYER_PED_ID()
		SET_SPECTATOR_DESIRED_FOCUS_PED(specData, PLAYER_PED_ID())
		SET_SPEC_CAM_DATA_DO_TRANSITION(specData.specCamData, TRUE)
	ENDIF

	IF MANAGE_FADING_SPECTATOR_TARGET_SWITCH(specData, FALSE, TRUE)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Moving into turn off")
		#ENDIF
		SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_TURN_OFF)
	ENDIF

ENDPROC

/// PURPOSE:
///    Process swooping up to the sky as the spec cam turns off 
/// PARAMS:
///    specCamData - struct of spec cam data.
PROC PROCESS_eSPECCAMSTATE_FINAL_SWOOP_UP(SPECTATOR_DATA_STRUCT &specData)
	
	IF MANAGE_SPECTATOR_SWOOP_UP(specData)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Moving into turn off")
		#ENDIF
		SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_TURN_OFF)
	ENDIF

ENDPROC

/// PURPOSE:
///    Performs the stage that turns off and tidies up the spectator cam
/// PARAMS:
///    specData - 
PROC PROCESS_eSPECCAMSTATE_TURN_OFF(SPECTATOR_DATA_STRUCT &specData)

	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	CLEANUP_RESTRICTED_INTERIOR_FOR_SPECTATOR(specData)

	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		/*INT i
		IF specData.specCamData.eMode = SPEC_MODE_NEWS	
			NETWORK_SET_VOICE_ACTIVE(TRUE)	//for 1344491
		ELSE
			REPEAT MAX_TEAM_VALUE i
				NETWORK_OVERRIDE_CHAT_RESTRICTIONS_TEAM_RECEIVE(i, FALSE)//for 1332302
			ENDREPEAT
		ENDIF*/
		IF specData.specCamData.eMode = SPEC_MODE_NEWS	
			//NETWORK_SET_VOICE_ACTIVE(TRUE)	//for 1344491, remove for 1613108
		ELIF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
			PRINTLN("[CHAT_FUNC], PROCESS_eSPECCAMSTATE_INIT, CHAT - HEIST")
			
		ELIF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_SURVIVAL
		OR g_bOnCoopMission
			NETWORK_SET_IGNORE_SPECTATOR_CHAT_LIMITS_SAME_TEAM(FALSE)
			PRINTLN("[CHAT_FUNC], PROCESS_eSPECCAMSTATE_INIT, NETWORK_SET_IGNORE_SPECTATOR_CHAT_LIMITS_SAME_TEAM(FALSE) ")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
		IF NOT IS_PED_INJURED(GET_SPECTATOR_CURRENT_FOCUS_PED())
			SET_PED_LOD_MULTIPLIER(GET_SPECTATOR_CURRENT_FOCUS_PED(), 1.0)
		ENDIF
	ENDIF
	
	IF NOT DID_QUIT_GAME_WITH_SPECTATOR_ACTIVE()
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WAS_LAST_DEACTIVATED_BY_QUIT_SCREEN)//if we're leaving the game, don't drop a fake ped behind
		SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE)
		NET_NL()NET_PRINT("PROCESS_eSPECCAMSTATE_TURN_OFF: SET_PLAYER_LEAVE_PED_BEHIND(PLAYER_ID(), FALSE) ")
		
	ENDIF
	
	// fade back in if necessary
	IF NOT IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_SWOOP_UP)
	AND (NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() OR IS_PLAYER_SCTV(PLAYER_ID()))	
		IF NOT IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_FADE_BACK_TO_PLAYER)
		OR NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Clash test cleanup fade in")
			#ENDIF
			IF NOT IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				OR MPSpecGlobals.SCTVData.currentMode = MPSpecGlobals.SCTVData.switchToMode
					IF (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT())
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === No cleanup fade, player is SCTV doing mode switch")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === IS_FADE_SWITCH_CLASHING_WITH_OTHER_FADES()")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Enable character selector
	//ENABLE_SELECTOR()
	
	// Clear data if spectator cam is turned off.
	CLEANUP_SPECTATOR_CAM_HUD(specData)
	
	//SET_SPECTATOR_CURRENT_FOCUS_PED(specData, PLAYER_PED_ID()) // NeilF: Don't reset the focus ped as we might want to return to watch the last player. 
	//SET_SPECTATOR_DESIRED_FOCUS_PED(specData, PLAYER_PED_ID())
	//RESET_SPECTATOR_HUD_VARIABLES()
	FLUSH_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData)
	
	CLEANUP_SPECIAL_SPECTATOR_CAMERA(specData)
	
	CLEANUP_FORCE_SPECTATOR_CAM(specData.specCamData)
	
	ENABLE_SPEC_CAN_FIND_NEW_FOCUS()
	
	specData.specCamData.bFirstTransitionComplete = FALSE

	UNLOCK_MINIMAP_POSITION()
	
	CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_ACTIVE)
	specData.specCamData.iLoadSceneFailSafeTimer			= NULL
	specData.specCamData.bStartedUpdateLoadScene 			= FALSE
	SET_SPEC_CAM_DATA_DO_TRANSITION(specData.specCamData, FALSE)
	specData.specCamData.iWaitForNoStreamVolFailSafeTimer 	= NULL
	specData.specCamData.iTimer								= NULL	
	
	SET_MINIMAP_BLOCK_WAYPOINT(FALSE)
	
	CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_TURNED_ON_CAM)
	CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_INITIAL_TARGET_SET)
	CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_CHECK_LOAD_SCENE)
	
	IF NOT IS_BIT_SET(specData.specCamData.iDeactivateBitset, DSCF_INDEX_REMAIN_VIEWING_TARGET)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DSCF_INDEX_REMAIN_VIEWING_TARGET = FALSE")
		#ENDIF
		IF NOT IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
			NETWORK_SET_OVERRIDE_SPECTATOR_MODE(FALSE) 
		ENDIF
		SET_IN_SPECTATOR_MODE(FALSE)
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DSCF_INDEX_REMAIN_VIEWING_TARGET = TRUE")
		#ENDIF
	ENDIF
		
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	CLEANUP_FADE_SWITCH_DATA(specData.specCamData)
	CLEANUP_SKYSWOOP_CLEANUP_DATA()
	CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	
	IF IS_THIS_MODE_ALLOWING_SPECTATOR_CROSS_TALK()
		ALLOW_SPECTATORS_TO_CHAT(TRUE)
	ELSE
		NETWORK_SET_NO_SPECTATOR_CHAT(FALSE)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === NETWORK_SET_NO_SPECTATOR_CHAT = FALSE")
	ENDIF	
	
	CLEANUP_CAM_VIEW_MODES_CINEMATIC(specData.specCamData.camViewMode, FALSE)
	
	NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CAM VIEW MODE SET BACK TO PLAYER ORIGINAL")
	#ENDIF
	
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)
	
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_SCRIPT_CAMS_OFF_REQUESTED)
	
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_RUNNING)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
	
	CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_INIT_DEATH_CHECK)
	CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SET_UP_DEATH_FILTER)
	
	CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)")
	#ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType <> FMMC_TYPE_RACE
		SET_SPEC_CAM_MODE(specData.specCamData, SPEC_MODE_PLAYERS_RESPAWN_EVENT)
	ENDIF
	
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADE_DISABLE_LOCKED)
		ENABLE_SPECTATOR_FADES()
	ENDIF
	ENABLE_SPECTATOR_HUD(specData.specHUDData)
	
	IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_RADAR_LOCKED)
		CLEANUP_RADAR_FOR_SPECTATOR_CAM(specData.specCamData)
	ENDIF
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_LIONS_DEN(g_FMMC_STRUCT.iAdversaryModeType)
		g_bDeactivateRestrictedAreas = FALSE
	ENDIF
	
	// Horde mode needs the wanted level to remain on max wanted level zero - B*1636391. 
	IF NOT g_bOnHorde
	 AND NOT USING_HEIST_SPECTATE() 
		SET_MAX_WANTED_LEVEL(5)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF - SET MAX WANTED LEVEL BACK TO 5")
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF - g_bOnHorde = TRUE - SO LEAVE MAX WANTED LEVEL")
	ENDIF
	
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Finished Cleaning up")
	#ENDIF
	
	specData.specCamData.iActivateBitset = 0
	specData.specCamData.iDeactivateBitset = 0
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpecCamDoingTransition = FALSE
	PRINTLN("[PMC] - set g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpecCamDoingTransition to FALSE.")
	
	CLEANUP_ALL_KILL_STRIP()
	specData.specHUDData.iSpecKillStripStage = 0
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF - MANAGE_SPEC_KILL_STRIP - CLEANUP_ALL_KILL_STRIP - iSpecKillStripStage = ", specData.specHUDData.iSpecKillStripStage)
	
	CLEAR_FOCUS()
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PROCESS_eSPECCAMSTATE_TURN_OFF - CLEAR_FOCUS")
	
	//CLEANUP_PC_CONTROLS(specData) // Commented out as pc controls now hard wired due to B* 2249255
		
	SET_SPEC_CAM_STATE(specData.specCamData, eSPECCAMSTATE_OFF)
ENDPROC

/// PURPOSE:
///    Instantly forces the spectator cam clean up and turn off
/// PARAMS:
///    specData - 
///    bBailForTransition - Force the player invisible, frozen, no collision, no control
PROC FORCE_CLEANUP_SPECTATOR_CAM(SPECTATOR_DATA_STRUCT &specData, BOOL bBailForTransition = FALSE)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === FORCE_CLEANUP_SPECTATOR_CAM")
	#ENDIF
	#IF USE_FINAL_PRINTS
	DEBUG_PRINTCALLSTACK()
	PRINTLN_FINAL("=== CAM === FORCE_CLEANUP_SPECTATOR_CAM")
	#ENDIF
	IF GET_SPEC_CAM_STATE(specData.specCamData) <> eSPECCAMSTATE_OFF
		IF bBailForTransition
			SPEC_BAIL_FOR_TRANSITION(specData)
		ENDIF
		PROCESS_eSPECCAMSTATE_TURN_OFF(specData)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ACTIVE)
		CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_ACTIVE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Keeps the player frozen, invisible with no collision in a location passed
/// PARAMS:
///    specCamData - 
///    vHideLocation - location to hide the local player ped
///    bLockUntilUnhideCalled - will keep the player here until unhide called
PROC HIDE_SPECTATOR_PLAYER(SPECTATOR_CAM_DATA &specCamData, BOOL bLockUntilUnhideCalled = FALSE)	/*VECTOR vHideLocation, */
	NET_PRINT("HIDE_SPECTATOR_PLAYER called...") NET_NL()
	IF NOT IS_BIT_SET(specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)
		IF NOT IS_BIT_SET(specCamData.iBitset, SPEC_CAM_BS_SPECTATOR_HIDDEN)
			IF bLockUntilUnhideCalled
				SET_BIT(specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)
			ENDIF
		
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			SET_PED_HEATSCALE_OVERRIDE(PLAYER_PED_ID(), 0)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) <> PPS_INVALID
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					SET_ENTITY_COLLISION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
					SET_ENTITY_VISIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
					CLEAR_PED_TASKS_IMMEDIATELY( PLAYER_PED_ID() ) //NOTE(Owain): B* 2343753
				ENDIF
			ELSE
				IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
			INVALIDATE_IDLE_CAM()
			specCamData.vHideReturn = GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<0,0,1>>
			//SET_ENTITY_COORDS(PLAYER_PED_ID(), vHideLocation, DEFAULT, TRUE, DEFAULT, FALSE)
			//CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT SPEC_CAM_BS_SPECTATOR_HIDDEN, vHideLocation = ", vHideLocation)
			SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(),TRUE)
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_GUNSMITH(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType)
			OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRAP_DOOR(g_FMMC_STRUCT.iAdversaryModeType)
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					SET_ENTITY_COMPLETELY_DISABLE_COLLISION(PLAYER_PED_ID(), FALSE)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === [MANAGE_FADING_SPECTATOR_TARGET_SWITCH] - Bullet collision turned off for Gunsmith. See url:bugstar:2989819. Added freeze too FREEZE_ENTITY_POSITION")
				ENDIF
			ENDIF
					
			
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT SPEC_CAM_BS_SPECTATOR_HIDDEN")//, vHideReturn = ", specCamData.vHideReturn)
			#ENDIF
			SET_BIT(specCamData.iBitset, SPEC_CAM_BS_SPECTATOR_HIDDEN)
		ELSE
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			SET_PED_HEATSCALE_OVERRIDE(PLAYER_PED_ID(), 0)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) <> PPS_INVALID
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					SET_ENTITY_COLLISION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE)
					SET_ENTITY_VISIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FALSE)
					CLEAR_PED_TASKS_IMMEDIATELY( PLAYER_PED_ID() ) //NOTE(Owain): B* 2343753
				ENDIF
			ELSE
				IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
			INVALIDATE_IDLE_CAM()
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the player back to location they last were when HIDE_SPECTATOR_PLAYER was called
/// PARAMS:
///    specCamData - 
///    bForceCleanupInvisibleFreeze - Returns the player to visible, not invincible, unfrozen and with collision
PROC UNHIDE_SPECTATOR_PLAYER(SPECTATOR_CAM_DATA &specCamData, BOOL bForceCleanupInvisibleFreeze = FALSE)
	IF NOT IS_BIT_SET(specCamData.iBitset, SPEC_CAM_BS_HIDE_LOCKED)
		IF IS_BIT_SET(specCamData.iBitset, SPEC_CAM_BS_SPECTATOR_HIDDEN)
			
			BOOL bFinishedWarping = FALSE
			
			SET_EVERYONE_IGNORE_PLAYER(PLAYER_ID(),FALSE)
			IF specCamData.vHideReturn.x <> 0.0
			AND specCamData.vHideReturn.y <> 0.0
			AND specCamData.vHideReturn.z <> 0.0
				#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ATTEMPTING UNHIDE_SPECTATOR_PLAYER, vHideReturn = ", specCamData.vHideReturn)
				#ENDIF
				IF NET_WARP_TO_COORD(specCamData.vHideReturn, 0.0, FALSE, FALSE)
					#IF IS_DEBUG_BUILD
					DEBUG_PRINTCALLSTACK()
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === COMPLETE UNHIDE_SPECTATOR_PLAYER, vHideReturn = ", specCamData.vHideReturn)
					#ENDIF
					specCamData.vHideReturn = <<0.0, 0.0, 0.0>>
					bFinishedWarping = TRUE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				DEBUG_PRINTCALLSTACK()
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === UNHIDE_SPECTATOR_PLAYER but no vHideReturn")
				#ENDIF
				bFinishedWarping = TRUE
			ENDIF
			IF bFinishedWarping
				IF NOT NETWORK_IS_IN_SPECTATOR_MODE() // added because of url:bugstar:3601237
					IF NOT IS_BIT_SET(specCamData.iDeactivateBitset, DSCF_INDEX_REMAIN_HIDDEN)
					OR bForceCleanupInvisibleFreeze
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						DISABLE_PED_HEATSCALE_OVERRIDE(PLAYER_PED_ID())
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SPECTATOR INVISIBLE FREEZE CLEANUP")
						#ENDIF
					ENDIF
					CLEAR_BIT(specCamData.iBitset, SPEC_CAM_BS_SPECTATOR_HIDDEN)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Makes sure the Spectator can hear their Targets team
PROC MAINTAIN_SPECTATOR_CHAT(SPECTATOR_DATA_STRUCT &specData)
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR (IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData) AND IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData))
		//Set Spectator to only hear Targets Team when on a Mission
		IF DOES_ENTITY_EXIST(MPSpecGlobals.pedCurrentFocus)
			PLAYER_INDEX thisPlayer
			PLAYER_INDEX focusPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(MPSpecGlobals.pedCurrentFocus) 
			IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
				thisPlayer = GET_PLAYER_INDEX()
				IF thisPlayer != INVALID_PLAYER_INDEX()
					INT iPart
					FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
						PARTICIPANT_INDEX tempPart = INT_TO_PARTICIPANTINDEX(iPart)	
						IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
							PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(tempPart)
							IF ARE_PLAYERS_ON_SAME_TEAM(tempPlayer, thisPlayer, DEFAULT, TRUE)
							OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_DEAD_SPECTATOR_TO_CHAT_ALL_TEAMS)
								PRINTLN("[JS] [SPECREZ] - MAINTAIN_SPECTATOR_CHAT - Setting localplayer can chat to part ", iPart)
								NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(tempPlayer, TRUE)
								NETWORK_OVERRIDE_SEND_RESTRICTIONS(tempPlayer, TRUE)	
							ELSE
								PRINTLN("[JS] [SPECREZ] - MAINTAIN_SPECTATOR_CHAT - Setting localplayer CANT chat to part ", iPart)
								NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(tempPlayer, FALSE)
								NETWORK_OVERRIDE_SEND_RESTRICTIONS(tempPlayer, FALSE)	
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ELSE
				thisPlayer = INT_TO_PLAYERINDEX(specData.specCamData.iChatPlayer)
				IF thisPlayer != INVALID_PLAYER_INDEX()
				AND focusPlayer != INVALID_PLAYER_INDEX()
					IF IS_NET_PLAYER_OK(thisPlayer, FALSE)
					AND IS_NET_PLAYER_OK(focusPlayer, FALSE)
						IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
						OR (IS_PLAYER_A_ROCKSTAR_DEV() AND IS_PLAYER_SCTV(PLAYER_ID()))
						OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_DEAD_SPECTATOR_TO_CHAT_ALL_TEAMS)
							NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(thisPlayer, TRUE)
						ELSE
							IF NOT IS_PLAYER_SCTV(PLAYER_ID())
							AND IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
								NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(thisPlayer, TRUE)
								//IF IS_PLAYER_SPECTATING(thisPlayer) //Removed this check for Bug 2168163
									NETWORK_OVERRIDE_SEND_RESTRICTIONS(thisPlayer, TRUE)
								//ENDIF
							ELSE
								IF ARE_PLAYERS_ON_SAME_TEAM(focusPlayer, thisPlayer, DEFAULT, TRUE)
								OR GET_PLAYER_TEAM(focusPlayer) = -1
									NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(thisPlayer, TRUE)
								ELSE
									NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(thisPlayer, FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		specData.specCamData.iChatPlayer++
		IF specData.specCamData.iChatPlayer >= NUM_NETWORK_REAL_PLAYERS()
			specData.specCamData.iChatPlayer = 0
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Calls a command to stop the Spectator creating ambient vehicles around the race grid when on a race.
PROC KEEP_RACE_GRID_CLEAR()
	IF NOT g_b_RaceCutDone // 2355338, Race has started when this is TRUE
		IF NETWORK_IS_ACTIVITY_SESSION()
			IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()), eFM_RACE_CLOUD, 0)
				AND NOT IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), eFM_RACE_CLOUD, 0)
					CLEAR_AREA_OF_VEHICLES(g_FMMC_STRUCT.vStartingGrid, RACE_CLEAR_VEHICLE_AREA, FALSE, FALSE, FALSE, FALSE)	//Clears all normal vehicles
					CLEAR_AREA_OF_VEHICLES(g_FMMC_STRUCT.vStartingGrid, RACE_CLEAR_VEHICLE_AREA, FALSE, FALSE, TRUE, FALSE)		//Clears all wrecked vehicles
					CLEAR_AREA_OF_VEHICLES(g_FMMC_STRUCT.vStartingGrid, RACE_CLEAR_VEHICLE_AREA, FALSE, FALSE, FALSE, TRUE)		//Clears all abondoned vehicles
					SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === KEEP_RACE_GRID_CLEAR - CALL CLEAR_STARTING_GRID_TRAFFIC")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Turns on NightVision when target player has it on
PROC MAINTAIN_SPECTATOR_NIGHTVISION(SPECTATOR_DATA_STRUCT &specData)
	IF specData.specCamData.iSpectateTargetPlayerID != -1 
		IF IS_BIT_SET(GlobalPlayerBD_SCTV[specData.specCamData.iSpectateTargetPlayerID].iGeneralBitSet, GPBDS_GBI_USING_NIGHTVISION)
		OR (IS_THIS_LEGACY_FMMC_CONTENT() AND g_bFMMCLightsTurnedOff)
			IF NOT GET_USINGNIGHTVISION()
				SET_NIGHTVISION(TRUE)
				DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_SPECTATOR_NIGHTVISION - SET_NIGHTVISION(TRUE)")
			ENDIF
		ELIF IS_BIT_SET(GlobalPlayerBD_SCTV[specData.specCamData.iSpectateTargetPlayerID].iGeneralBitSet, GPBDS_GBI_USING_THERMAL)
			IF NOT GET_USINGSEETHROUGH()
				SET_SEETHROUGH(TRUE)
				g_fSeethrough_cached_max_thickness = SEETHROUGH_GET_MAX_THICKNESS()
				SEETHROUGH_SET_MAX_THICKNESS(fCONST_SEETHROUGH_CACHED_DEFAULT_THICKNESS)
				DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_SPECTATOR_NIGHTVISION - SET_SEETHROUGH(TRUE)")
			ENDIF
		ELSE
			IF GET_USINGNIGHTVISION()
			AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_BVS1(g_FMMC_STRUCT.iRootContentIdHash)
				SET_NIGHTVISION(FALSE)
				DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_SPECTATOR_NIGHTVISION - SET_NIGHTVISION(FALSE)")
			ELIF GET_USINGSEETHROUGH()
				SEETHROUGH_SET_MAX_THICKNESS(g_fSeethrough_cached_max_thickness)
				g_fSeethrough_cached_max_thickness = -1.0
				SET_SEETHROUGH(FALSE)
				DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_SPECTATOR_NIGHTVISION - SET_SEETHROUGH(FALSE)")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the spectator needs to find a new target, and recaculates the number of players on the spectator list
/// PARAMS:
///    specData - 
PROC MANAGE_EVERY_FRAME_CHECKS(SPECTATOR_DATA_STRUCT &specData)
	BOOL bNeedToChangeCurrentFocus = FALSE
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
	OR GET_SCTV_MODE()  = SCTV_MODE_FOLLOW_CAM
		IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)
			IF NOT specData.specCamData.bDoTransition
				IF NOT IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, GET_SPECTATOR_CURRENT_FOCUS_PED())
					IF NOT IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
						IF DOES_ENTITY_EXIST( GET_SPECTATOR_CURRENT_FOCUS_PED())
						AND NOT IS_PLAYER_WATCHING_A_MOCAP(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
							IF NOT IS_PLAYER_IN_CORONA()
							
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Current focus not suitable!")
								#ENDIF
								bNeedToChangeCurrentFocus = TRUE
							
							#IF IS_DEBUG_BUILD
							ELSE
								CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Current focus not suitable but in corona!")
							#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bShouldFindAnotherSpecTarget
	IF bNeedToChangeCurrentFocus
		IF NOT SHOULD_FIND_ANOTHER_SPECTATOR_TARGET(specData)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = FALSE, clearing bNeedToChangeCurrentFocus")
			bNeedToChangeCurrentFocus = FALSE
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SHOULD_FIND_ANOTHER_SPECTATOR_TARGET = TRUE, bShouldFindAnotherSpecTarget SET TO TRUE")
			bShouldFindAnotherSpecTarget = TRUE
		ENDIF
	ENDIF
	
	IF NOT bNeedToChangeCurrentFocus
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())

			IF NOT CAN_SPECTATOR_TARGET_IN_CUTSCENE()
				IF IS_PED_A_PLAYER(GET_SPECTATOR_CURRENT_FOCUS_PED()) AND IS_PLAYER_RUNNING_A_CUTSCENE(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === current focus ped is in cutscene")
					bNeedToChangeCurrentFocus = TRUE
				ENDIF
			ENDIF
			
			IF NOT CAN_SPECTATOR_TARGET_THE_DEAD()
				IF IS_ENTITY_DEAD(GET_SPECTATOR_CURRENT_FOCUS_PED())
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === current focus ped is dead")
					bNeedToChangeCurrentFocus = TRUE
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	IF bNeedToChangeCurrentFocus
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_UPDATE_INSTRUCTIONAL_BUTTONS)
		INT iList, iEntry
		POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
		
		BOOL bSuccesfullyFound = FALSE
		
		IF bShouldFindAnotherSpecTarget	//SHOULD_FIND_ANOTHER_SPECTATOR_TARGET(specData)
			
			IF (GET_SPECTATOR_DESIRED_FOCUS_PED() <> GET_SPECTATOR_CURRENT_FOCUS_PED())
			AND IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(specData, GET_SPECTATOR_DESIRED_FOCUS_PED())
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Desired already suitable, will move to desired soon ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())))
				#ENDIF
				bSuccesfullyFound = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Desired is not suitable either, we really need a new focus", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_DESIRED_FOCUS_PED())))
				#ENDIF
			ENDIF
			
			IF NOT bSuccesfullyFound
				
				BOOL bTargetOnMissionEnding
				BOOL bTargetWatchingMocap
				IF IS_NET_PLAYER_OK(MPGlobalsAmbience.piHeistSpectateTarget, FALSE)
					bTargetOnMissionEnding = IS_MP_DECORATOR_BIT_SET(MPGlobalsAmbience.piHeistSpectateTarget, MP_DECORATOR_BS_MISSION_ENDING)
					bTargetWatchingMocap = IS_PLAYER_WATCHING_A_MOCAP(MPGlobalsAmbience.piHeistSpectateTarget)
				ENDIF
				
				IF specData.specHUDData.bToldPlayerCantSpectateChosenPlayer = FALSE
				AND CAN_SPECTATOR_SHOW_WARNING_PLAYER_CHOSEN_TO_SPECTATE_GONE()
				AND NOT IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET(NATIVE_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].playerSCTVTarget))
				AND NOT IS_PLAYLIST_BETWEEN_LEADERBOARDS()
				AND NOT IS_MISSION_OVER_AND_ON_LB_FOR_SCTV(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
				AND NOT IS_GLOBAL_CLIENT_FINISHED_JOB_BIT_SET(NATIVE_TO_INT(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())))
				AND g_bMissionEnding = FALSE
				AND g_b_JobEnded = FALSE
				AND bTargetOnMissionEnding = FALSE
				AND bTargetWatchingMocap = FALSE
				AND NOT IS_SPECTATOR_RUNNING_CUTSCENE()
				AND NOT IS_PLAYER_WATCHING_A_MOCAP(PLAYER_ID())
				AND NOT TRANSITION_SESSIONS_ACTIVE_JOB_WARNING()
				AND NOT IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_HEIST_SPECTATE_WHILE_DEAD_ENABLED)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
				AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
					WARNING_SCREEN_RETURN aScreenResult = RUN_WARNINGSCREEN_PLAYER_CHOSEN_TO_SPECTATE_GONE()
					IF aScreenResult = WARNING_SCREEN_RETURN_ACCEPT
						specData.specHUDData.bToldPlayerCantSpectateChosenPlayer = TRUE
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bToldPlayerCantSpectateChosenPlayer = TRUE")
					ELSE
						CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specData.specHUDData)
						EXIT
					ENDIF
					
				ELSE
					CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specData.specHUDData)

					IF GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY(specData, iList, iEntry, TRUE, FALSE, TRUE)
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Found suitable focus, switching to player")
						SET_SPECTATOR_HUD_CURRENT_FOCUS_TARGET_FROM_LIST(specData, iList, iEntry)
						bSuccesfullyFound = TRUE
						specData.specHUDData.bToldPlayerCantSpectateChosenPlayer = FALSE
						CPRINTLN(DEBUG_SPECTATOR, "=== CAM === bToldPlayerCantSpectateChosenPlayer = FALSE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bSuccesfullyFound
			//no standardised way to handle this, event should probably end?
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Can't find suitable focus")
			#ENDIF
			IF NOT CAN_SPECTATOR_QUIT_WHEN_NO_TARGETS()
				IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_FADE_OUT)
					SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_FADE_OUT)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === NO VALID TARGET - Fade out")
				ENDIF
			ELIF IS_PLAYER_SCTV(PLAYER_ID())
				IF SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS()
					IF NOT HAS_NET_TIMER_STARTED(specData.stBailNoOneTimOut)
						START_NET_TIMER(specData.stBailNoOneTimOut)
					ELIF HAS_NET_TIMER_EXPIRED(specData.stBailNoOneTimOut, 5000)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === Can't find suitable focus")
						#ENDIF
						#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("=== SCTV === Can't find suitable focus")
						#ENDIF
						BAIL_FROM_SCTV_FOLLOW()
						MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_NO_SUITABLE_TARGET
					ENDIF
				ELSE
					//If we have nobody to spectate, but shouldn't bail whilst on a mission then set flag to show Leaderboard.
					IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID(), FALSE)
						IF NOT IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
							SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
							PRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === NO VALID TARGET AND NO BAIL - SHOW LEADERBOARD - SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD")
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("=== SCTV === NO VALID TARGET AND NO BAIL - SHOW LEADERBOARD - SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD)
				SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD_END)
				PRINTLN(DEBUG_SPECTATOR, 	"=== SCTV === FOUND VALID TARGET- SHOW LEADERBOARD END - SPEC_HUD_BS_NO_VALID_TARGET_SHOW_LEADERBOARD_END")
			ENDIF
		ENDIF
	ELSE
		IF NOT CAN_SPECTATOR_QUIT_WHEN_NO_TARGETS()
			IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
				IF IS_BIT_SET(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_FADE_OUT)
					CLEAR_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_NO_VALID_TARGET_FADE_OUT)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === VALID TARGET - Fade in")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	CALCULATE_NUMBER_OF_TARGETS_ON_A_PAGE(specData.specHUDData)
	
	MAINTAIN_SPECTATOR_CHAT(specData)

	KEEP_RACE_GRID_CLEAR()
	//MAINTAIN_VEHICLE_DIALS()

	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR (IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData) AND IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData))
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		MAINTAIN_SPECTATOR_NIGHTVISION(specData)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the player to pass the info of into MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION
/// PARAMS:
///    specData - spectator struct
/// RETURNS:
///    INT of player ID ot update this frame
FUNC INT GET_SPECTATOR_LIST_PLAYER_ID_FOR_THIS_FRAME(SPECTATOR_DATA_STRUCT &specData)
	RETURN specData.specHUDData.iProcessNextPlayerID
ENDFUNC

/// PURPOSE:
///    Copies this frame's player entry into the position array
/// PARAMS:
///    specData - spectator struct
///    iThisFramePlayerPosition - Current position in the job (aka 1st in a race or DM, highest kills in survival etc). 
///    Use GET_SPECTATOR_LIST_PLAYER_ID_FOR_THIS_FRAME to manage which score to pass
PROC MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION(SPECTATOR_DATA_STRUCT &specData, INT iThisFramePlayerPosition)

	specData.specHUDData.iProcessSpectatorJobPlayerPositions[specData.specHUDData.iProcessNextPlayerID] = iThisFramePlayerPosition
	
	specData.specHUDData.iProcessNextPlayerID++
	IF specData.specHUDData.iProcessNextPlayerID >= NUM_NETWORK_PLAYERS
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			IF MPSpecGlobals.iCurrentJobPlayerPositions[i] <> specData.specHUDData.iProcessSpectatorJobPlayerPositions[i]
				SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()		//If there's been change, redo the list
			ENDIF
			MPSpecGlobals.iCurrentJobPlayerPositions[i] = specData.specHUDData.iProcessSpectatorJobPlayerPositions[i]	//copy to used list
			specData.specHUDData.iProcessSpectatorJobPlayerPositions[i] = -1	//clear process list entry
		ENDREPEAT
		specData.specHUDData.iProcessNextPlayerID = 0
		
		/*#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ========================= Spectator Job Player Positions ===============================")
		REPEAT NUM_NETWORK_PLAYERS i
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(i)), " ", MPSpecGlobals.iCurrentJobPlayerPositions[i])
			ELSE
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === INVALID ", MPSpecGlobals.iCurrentJobPlayerPositions[i])
			ENDIF
		ENDREPEAT
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ========================================================================================")
		#ENDIF*/
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the team to pass the info of into MAINTAIN_SPECTATOR_TEAM_LIST_POSITION
/// PARAMS:
///    specData - spectator struct
/// RETURNS:
///    INT of team ID ot update this frame
FUNC INT GET_SPECTATOR_LIST_TEAM_FOR_THIS_FRAME(SPECTATOR_DATA_STRUCT &specData)
	RETURN specData.specHUDData.iProcessNextTeam
ENDFUNC

/// PURPOSE:
///    Copies this frame's team entry into the position array
/// PARAMS:
///    specData - spectator struct
///    iThisFrameTeamPosition - Current position in the job (aka 1st in a race or DM, highest kills in survival etc). 
///    Use GET_SPECTATOR_LIST_PLAYER_ID_FOR_THIS_FRAME to manage which score to pass
PROC MAINTAIN_SPECTATOR_TEAM_LIST_POSITION(SPECTATOR_DATA_STRUCT &specData, INT iThisFrameTeamPosition)

	specData.specHUDData.iProcessSpectatorJobTeamPositions[specData.specHUDData.iProcessNextTeam] = iThisFrameTeamPosition
	
	specData.specHUDData.iProcessNextTeam++
	IF specData.specHUDData.iProcessNextTeam >= MAX_NUM_SPEC_TEAMS
		
		INT i
		REPEAT MAX_NUM_SPEC_TEAMS i
			IF MPSpecGlobals.iCurrentJobTeamPositions[i] <> specData.specHUDData.iProcessSpectatorJobTeamPositions[i]
				SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()		//If there's been change, redo the list
			ENDIF
			MPSpecGlobals.iCurrentJobTeamPositions[i] = specData.specHUDData.iProcessSpectatorJobTeamPositions[i]	//copy to used list
			specData.specHUDData.iProcessSpectatorJobTeamPositions[i] = -1	//clear process list entry
		ENDREPEAT
		specData.specHUDData.iProcessNextTeam = 0
		
		/*#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ========================= Spectator Job Team Positions ===============================")
		REPEAT MAX_NUM_SPEC_TEAMS i
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === Team ", i, " Position ", MPSpecGlobals.iCurrentJobTeamPositions[i])
		ENDREPEAT
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ======================================================================================")
		#ENDIF*/
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Process the spectator cam. Always running.
/// PARAMS:
///    specCamData - struct of spectator cam data.
PROC MAINTAIN_SPECTATOR(SPECTATOR_DATA_STRUCT &specData)

	// So the post mission cleanup doesn't interfere with any spec cam transitional camera work. 
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSpecCamDoingTransition =  specData.specCamData.bDoTransition

	IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_TO_SESSION
	OR GET_SCTV_SWITCHING_TO_MODE() = SCTV_MODE_FOLLOW_TO_SESSION
	OR (IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE() AND NOT IS_PLAYER_SCTV(PLAYER_ID()))
		// Do not process this function if we are following our target to a session.
		EXIT
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_DROP_ZONE(g_FMMC_STRUCT.iRootContentIDHash)
		IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		AND NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			IF DOES_ENTITY_EXIST(GET_SPECTATOR_CURRENT_FOCUS_PED())
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(GET_SPECTATOR_CURRENT_FOCUS_PED())
			ENDIF
		ENDIF
	ENDIF
	
	//If we're following
	IF GET_SCTV_MODE() = SCTV_MODE_FOLLOW_CAM
		//And we're in a corona after a random/restart
		IF IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
		AND IS_PLAYER_IN_CORONA()
			
			IF IS_SCREEN_FADED_OUT()
				IF GET_SPEC_SKYSWOOP_CLEANUP_STAGE() = eSPECSKYSWOOPCLEANUPSTAGE_FADE_OUT
				OR GET_SPEC_SKYSWOOP_CLEANUP_STAGE() = eSPECSKYSWOOPCLEANUPSTAGE_WAIT_FOR_TIMER
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					SET_SPEC_SKYSWOOP_CLEANUP_STAGE(eSPECSKYSWOOPCLEANUPSTAGE_OFF)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_SPECTATOR - FORCE FADE IN - RESET eSPECSKYSWOOPCLEANUPSTAGE_OFF")
				ENDIF
			ENDIF
			
			//Then don'e swithc player, wait for the follow. 
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)	//to protect from skyswoop crashes
		IF DID_QUIT_GAME_WITH_SPECTATOR_ACTIVE()
			IF GET_SPEC_CAM_STATE(specData.specCamData) <> eSPECCAMSTATE_OFF
				FORCE_CLEANUP_SPECTATOR_CAM(specData, TRUE)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)")
				#ENDIF
				SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_SCTV(PLAYER_ID())
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())									//If player is not on a mission
			IF IS_MP_DECORATOR_BIT_SET(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)//and is marked as not suitable to spectate
				CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_INVALID_TO_SPECTATE)//clear that flag, making the player spectatable
			ENDIF
			IF NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADE_DISABLE_LOCKED)
				ENABLE_SPECTATOR_FADES()
			ENDIF
			ENABLE_SPECTATOR_HUD(specData.specHUDData)
		ENDIF
	ENDIF
	
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData)
	
		POPULATE_SPECTATOR_CAM_TARGET_LISTS(specData)	//stagger through the player list adding them to the spectator target list
		
		IF SHOULD_PLAYER_BE_HIDDEN_DUE_TO_SPECTATING()							//Should local player be hiding when spectating
		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		
			HIDE_SPECTATOR_PLAYER(specData.specCamData)	//, <<SPECTATOR_HIDE_POSITION_X, SPECTATOR_HIDE_POSITION_Y, SPECTATOR_HIDE_POSITION_Z>>)	//Try Removing for Bug 2034308
		ENDIF
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_MG_GOLF
			MAINTAIN_RADAR_FOR_SPECTATOR_CAM(specData.specCamData)	//keep the radar accurate to what the local player is seeing
		ELSE
			IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_RADAR_LOCKED)
				CLEANUP_RADAR_FOR_SPECTATOR_CAM(specData.specCamData)
			ENDIF
		ENDIF
		
	ELSE
		UNHIDE_SPECTATOR_PLAYER(specData.specCamData)
		
		IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_RADAR_LOCKED)
			CLEANUP_RADAR_FOR_SPECTATOR_CAM(specData.specCamData)
		ENDIF
	ENDIF
	
	IF GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_OFF
	OR GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_INIT
	OR GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_TURN_OFF
		IF IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)")
			#ENDIF
			CLEAR_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)
			CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_RUNNING)
		ENDIF
	ELSE
		IF specData.specCamData.eMode = SPEC_MODE_NEWS
		//OR specData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_NEWS
			SET_CINEMATIC_NEWS_CHANNEL_ACTIVE_THIS_UPDATE()	//special rules for code-based cinematic cam
		ENDIF
		
		IF GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_WATCHING
			IF NOT IS_BIT_SET(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)")
				#ENDIF
				SET_BIT(specData.specCamData.iBitSet, SPEC_CAM_BS_THIS_RUNNING)
				SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_RUNNING)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_OFF
	OR GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER
	OR GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_FINAL_SWOOP_UP
	OR GET_SPEC_CAM_STATE(specData.specCamData) = eSPECCAMSTATE_TURN_OFF
		IF NOT IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)")
			#ENDIF
			SET_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)
		ENDIF
	ELSE
		IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)")
			#ENDIF
			CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_THIS_OFF_OR_DEACTIVATING)
		ENDIF
		
		//Move to special cameras if spectator target is in certain locations
		MANAGE_SPECIAL_SPECTATOR_CAMERA_SHOTS(specData)
		
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
		
	ENDIF
	
	IF IS_BIT_SET(specData.specCamData.iBitset, SPEC_CAM_BS_SET_UP_DEATH_FILTER)
		IF NOT g_bCelebrationScreenIsActive
			SETUP_DEATH_INITIAL_FILTER(specData.specHUDData)
		ENDIF
		CLEAR_BIT(specData.specCamData.iBitset, SPEC_CAM_BS_SET_UP_DEATH_FILTER)
	ENDIF							
	
	SWITCH GET_SPEC_CAM_STATE(specData.specCamData)
	
		CASE eSPECCAMSTATE_OFF
			PROCESS_eSPECCAMSTATE_OFF(specData)
			
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_OFF ")
					ENDIF
				ENDIF
			#ENDIF
			
		BREAK
		
		CASE eSPECCAMSTATE_INIT
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_INIT ")
					ENDIF
				ENDIF
			#ENDIF
		
			PROCESS_eSPECCAMSTATE_INIT(specData)
		BREAK
		
		CASE eSPECCAMSTATE_SWITCHING
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_SWITCHING ")
					ENDIF
				ENDIF
			#ENDIF
		
			PROCESS_eSPECCAMSTATE_SWITCHING(specData)
		BREAK
		
		CASE eSPECCAMSTATE_WATCHING
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_WATCHING ")
					ENDIF
				ENDIF
			#ENDIF
		
			PROCESS_eSPECCAMSTATE_WATCHING(specData)
		BREAK
		
		CASE eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER ")
					ENDIF
				ENDIF
			#ENDIF
		
			PROCESS_eSPECCAMSTATE_FINAL_SWITCH_TO_PLAYER(specData)
		BREAK
		
		CASE eSPECCAMSTATE_FINAL_SWOOP_UP
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_FINAL_SWOOP_UP ")
					ENDIF
				ENDIF
			#ENDIF
		
			PROCESS_eSPECCAMSTATE_FINAL_SWOOP_UP(specData)
		BREAK
		
		CASE eSPECCAMSTATE_TURN_OFF
		
			#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisplaySCTVStages")
					IF GET_FRAME_COUNT() % 10 = 0
						CPRINTLN(DEBUG_SPECTATOR, 	"=== SCTVSTAGES === GET_SPEC_CAM_STATE = eSPECCAMSTATE_TURN_OFF ")
					ENDIF
				ENDIF
			#ENDIF
		
			PROCESS_eSPECCAMSTATE_TURN_OFF(specData)
		BREAK
		
	ENDSWITCH
	
	IF SHOULD_SPECTATOR_REDO_LIST_DUE_TO_EVENT()	//redo player list and summary card if a player has left or joined
		POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(specData)
		SET_BIT(specData.specHUDData.iBitset, SPEC_HUD_BS_REQUEST_REDO_SUMMARY_CARD)
		CLEAR_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
		OR (IS_THIS_SPECTATOR_CAM_ACTIVE(specData.specCamData) AND IS_THIS_SPECTATOR_CAM_RUNNING(specData.specCamData))
			INT iList, iEntry
			IF NOT SPEC_IS_PED_ACTIVE(GET_SPECTATOR_CURRENT_FOCUS_PED())
			OR NOT FIND_PED_IN_SPECTATOR_HUD_TARGET_LISTS(specData.specHUDData, iList, iEntry, GET_SPECTATOR_CURRENT_FOCUS_PED())
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SHOULD_SPECTATOR_REDO_LIST_DUE_TO_EVENT, need to find a new target")
				#ENDIF
				REACT_TO_TARGET_NO_LONGER_BEING_VALID(specData, FALSE, TRUE)
				IF IS_PLAYER_SCTV(PLAYER_ID())
					IF NOT GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY(specData, iList, iEntry, FALSE, FALSE, FALSE)
						IF SHOULD_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()							
							SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
							IF NOT HAS_NET_TIMER_STARTED(specData.stBailTimOut)
								START_NET_TIMER(specData.stBailTimOut)
							ELIF HAS_NET_TIMER_EXPIRED(specData.stBailTimOut, 10000)
								QUIT_SCTV_BACK_TO_GTAO()
							ENDIF							
						ELSE
							IF SHOULD_SCTV_BAIL_IF_NO_SUITABLE_FOCUS()
							AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
								IF STRAND_MISSION_SHOULD_SKIP_INTRO_IN_MC()
									IF NOT HAS_NET_TIMER_STARTED(specData.stBailStrandDelay)
										START_NET_TIMER(specData.stBailStrandDelay)
										CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === STRAND MISSION - START BAIL DELAY TIMER")
										#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("=== SCTV === STRAND MISSION - START BAIL DELAY TIMER")
										#ENDIF
									ELIF HAS_NET_TIMER_EXPIRED(specData.stBailStrandDelay, 5000)
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === STRAND MISSION - Updated list due to event and GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY is false")
										#ENDIF
										#IF USE_FINAL_PRINTS
										PRINTLN_FINAL("=== SCTV === STRAND MISSION - Updated list due to event and GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY is false")
										#ENDIF
										BAIL_FROM_SCTV_FOLLOW()
										MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_NO_SUITABLE_TARGET
									ENDIF		
								ELSE
									#IF IS_DEBUG_BUILD
									CPRINTLN(DEBUG_SPECTATOR, "=== SCTV === Updated list due to event and GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY is false")
									#ENDIF
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("=== SCTV === Updated list due to event and GET_SPECTATOR_HUD_CURRENTLY_VALID_LISTS_ENTRY is false")
									#ENDIF
									BAIL_FROM_SCTV_FOLLOW()
									MPSpecGlobals.iBailReason = SCTV_BAIL_REASON_NO_SUITABLE_TARGET
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	CHECK_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
				
	PROCESS_SPECTATOR_HUD(specData)

	MANAGE_EVERY_FRAME_CHECKS(specData)
	
ENDPROC
