//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        net_club_dj_battle_screen.sch														
/// Description: Header for displaying bink movies on a dj battle screen often found in the nightclubs
///    			 this is a rewritten version of the one used for the original nightclubs, it supports swapping
///    			 configurations and changing color tints on beat
/// Written by:  Online Technical Team: Tom Turner														
/// Date:  		23/09/2020																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
USING "net_club_dj_battle_screen_typedefs.sch"
USING "net_render_targets.sch"
USING "website_public.sch"

/// PURPOSE:
///    Cleans upd the battles screens render target and bink movie
/// PARAMS:
///    sInst - the battle screen instance
PROC _CLUB_DJ_BATTLE_SCREEN__CLEANUP_ASSETS(CLUB_DJ_BATTLE_SCREEN &sInst)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	RELEASE_REGISTERED_RENDER_TARGET(CLUB_DJ_BATTLE_SCREEN__GET_RENDER_TEXTURE_NAME(sInst))
	_CLUB_DJ_BATTLE_SCREEN__SET_RENDER_TARGET_ID(sInst, -1)
	PRINTLN("[CLUB_DJ_BATTLE_SCREEN] CLEANUP_ASSETS - Assets cleaned up")
ENDPROC

/// PURPOSE:
///    Clears the battle screen to black if the render target exists
/// PARAMS:
///    sInst - the battle screen instance
PROC CLUB_DJ_BATTLE_SCREEN__CLEAR_SCREEN(CLUB_DJ_BATTLE_SCREEN &sInst)
	INT iTarget = CLUB_DJ_BATTLE_SCREEN__GET_RENDER_TARGET_ID(sInst)
	
	IF iTarget = -1
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] CLEAR_SCREEN - Render target not created")
		EXIT
	ENDIF
	
	SET_TEXT_RENDER_ID(iTarget)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	PRINTLN("[CLUB_DJ_BATTLE_SCREEN] CLEAR_SCREEN - Cleared screen")
ENDPROC

/// PURPOSE:
///    Gets a color variant at the given index according to the assigned color lookup
///    will fail if no lookup is assigned
/// PARAMS:
///    sInst - the battle screen instance
///    iIndex - index of the color variation
///    sColorOut - the returned color
/// RETURNS:
///    TRUE if successfull, FALSE if failed to grab color or no lookup is assigned
FUNC BOOL _CLUB_DJ_BATTLE_SCREEN__GET_COLOUR_VARIANT(CLUB_DJ_BATTLE_SCREEN &sInst, INT iIndex, COLOR_STRUCT &sColorOut)
	T_GET_CLUB_DJ_BATTLE_SCREEN_COLOR_VARIATION fpLookup = CLUB_DJ_BATTLE_SCREEN__GET_COLOR_VARIANT_LOOKUP(sInst)
	
	IF fpLookup = NULL
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] GET_COLOUR_VARIANT - No lookup assigned")
		RETURN FALSE
	ENDIF
	
	RETURN CALL fpLookup(iIndex, sColorOut)
ENDFUNC

/// PURPOSE:
///    Detects if the browser is open and transitions to the off state
///    this is required because we can only have one bink movie at a time and the browser runs movies
///    on some web pages
/// PARAMS:
///    sInst - the battle screen instance
/// RETURNS:
///    TRUE if transitioning to the off state
FUNC BOOL _CLUB_DJ_BATTLE_SCREEN__MAINTAIN_BROWSER_BAIL_TRANSITION(CLUB_DJ_BATTLE_SCREEN &sInst)
	IF IS_BROWSER_OPEN()
		CLUB_DJ_BATTLE_SCREEN__CLEAR_SCREEN(sInst)
		_CLUB_DJ_BATTLE_SCREEN__CLEANUP_ASSETS(sInst)
		_CLUB_DJ_BATTLE_SCREEN__SET_STATE(sInst, CDJBWS__OFF)
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] MAINTAIN_BROWSER_BAIL_TRANSITION - Browser open, turning screen off")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Update state for waiting for the render target to be linked to the target prop and texture
///    transitions on to the draw state when the render target is created
/// PARAMS:
///    sInst - the battle screen instance
PROC _CLUB_DJ_BATTLE_SCREEN__UPDATE_LINK_RENDER_TARGET_STATE(CLUB_DJ_BATTLE_SCREEN &sInst)
	// Turn off if browser is open, we can only have one bink movie
	IF _CLUB_DJ_BATTLE_SCREEN__MAINTAIN_BROWSER_BAIL_TRANSITION(sInst)
		EXIT
	ENDIF
	
	STRING strRenderTarget = CLUB_DJ_BATTLE_SCREEN__GET_RENDER_TEXTURE_NAME(sInst)
	
	IF IS_STRING_NULL_OR_EMPTY(strRenderTarget)
		ASSERTLN("[CLUB_DJ_BATTLE_SCREEN] UPDATE_LINK_RENDER_TARGET_STATE - No render target assigned!")
		EXIT
	ENDIF
	
	MODEL_NAMES eTargetModel = CLUB_DJ_BATTLE_SCREEN__GET_WALL_PROP(sInst)
	
	IF eTargetModel = DUMMY_MODEL_FOR_SCRIPT
		ASSERTLN("[CLUB_DJ_BATTLE_SCREEN] UPDATE_LINK_RENDER_TARGET_STATE - No Model assigned!")
		EXIT
	ENDIF
	
	// Progress to drawing once render target is linked
	IF IS_NAMED_RENDERTARGET_REGISTERED(strRenderTarget) // Short circuit intended
	OR REGISTER_AND_LINK_RENDER_TARGET(strRenderTarget, eTargetModel)	
		_CLUB_DJ_BATTLE_SCREEN__SET_RENDER_TARGET_ID(sInst, GET_RENDER_TARGET_ID(strRenderTarget))
		STRING strPlaylist = CLUB_DJ_BATTLE_SCREEN__GET_BINK_PLAYLIST(sInst)
		SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_1, strPlaylist, TRUE)
		SET_TV_AUDIO_FRONTEND(TRUE)
		SET_TV_VOLUME(-5.0)
		SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_1)
		_CLUB_DJ_BATTLE_SCREEN__GET_COLOUR_VARIANT(sInst, 0, sInst.sCurrentColor)
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] UPDATE_LINK_RENDER_TARGET_STATE - Render target:", strRenderTarget, " linked, starting playlist: ", strPlaylist)
		_CLUB_DJ_BATTLE_SCREEN__SET_STATE(sInst, CDJBWS__DRAW)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the battle screen should change the color of the bink movie tint
///    this will happen either on beat or on bar according to current intensity level
/// PARAMS:
///    sInst - the battle screen instance
/// RETURNS:
///    FALSE if no color lookup is assigned or should not change, TRUE if audio is high intensity and a beat occurs
///    OR if a new bar starts on mid to low intensity music
FUNC BOOL _CLUB_DJ_BATTLE_SCREEN__SHOULD_CHANGE_COLOR(CLUB_DJ_BATTLE_SCREEN &sInst)
	// No colours to change to
	IF CLUB_DJ_BATTLE_SCREEN__GET_COLOR_VARIANT_LOOKUP(sInst) = NULL
		RETURN FALSE
	ENDIF
	
	// TODO: add tint exception lookup that clears tint? or handle it in color lookup
	
	FLOAT fTimeS
	FLOAT fBPM
	INT iBeatNum
	
	// If music is intense change every beat
	IF sInst.eAudioIntensity = CLUB_MUSIC_INTENSITY_HIGH OR sInst.eAudioIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS
		// Every new beat change tint
		IF GET_NEXT_AUDIBLE_BEAT(fTimeS, fBPM, iBeatNum)
			IF sInst.iCurrentBeat != iBeatNum
				sInst.iCurrentBeat = iBeatNum
				RETURN TRUE
			ENDIF
		ENDIF
		
		// On same beat, no color change
		RETURN FALSE
	ENDIF
	
	// Every bar change tint when low/mid intensity (beat goes 1, 2, 3, 4 then back to 1) 
	IF GET_NEXT_AUDIBLE_BEAT(fTimeS, fBPM, iBeatNum)
		RETURN iBeatNum = 1
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Increments to the next bink movie color tint if the color should change
/// PARAMS:
///    sInst - the battle screen instance
PROC _CLUB_DJ_BATTLE_SCREEN__UPDATE_CURRENT_TINT_COLOR(CLUB_DJ_BATTLE_SCREEN &sInst)
	IF NOT _CLUB_DJ_BATTLE_SCREEN__SHOULD_CHANGE_COLOR(sInst)
		EXIT
	ENDIF
	
	// Get next color
	++sInst.iCurrentColorVariation
	PRINTLN("[CLUB_DJ_BATTLE_SCREEN] UPDATE_CURRENT_TINT_COLOR - Swapping colour to: ", sInst.iCurrentColorVariation)
	
	// Reset to white in case lookup fails
	sInst.sCurrentColor.r = 255
	sInst.sCurrentColor.g = 255
	sInst.sCurrentColor.b = 255
	sInst.sCurrentColor.a = 255
	
	// Get color variation, if it fails we've hit the end so grab the first
	IF NOT _CLUB_DJ_BATTLE_SCREEN__GET_COLOUR_VARIANT(sInst, sInst.iCurrentColorVariation, sInst.sCurrentColor)
		sInst.iCurrentColorVariation = 0
		_CLUB_DJ_BATTLE_SCREEN__GET_COLOUR_VARIANT(sInst, sInst.iCurrentColorVariation, sInst.sCurrentColor)
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] UPDATE_CURRENT_TINT_COLOR - Hit end of lookup setting color to: ", sInst.iCurrentColorVariation)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update state for drawing the bink movie to the screen
///    Will transition to off if the browser is open
/// PARAMS:
///    sInst - the battle screen instance
PROC _CLUB_DJ_BATTLE_SCREEN__UPDATE_DRAW_STATE(CLUB_DJ_BATTLE_SCREEN &sInst)
	// Turn off if browser is open, we can only have one bink movie
	IF _CLUB_DJ_BATTLE_SCREEN__MAINTAIN_BROWSER_BAIL_TRANSITION(sInst)
		EXIT
	ENDIF
	
	INT iTargetID = CLUB_DJ_BATTLE_SCREEN__GET_RENDER_TARGET_ID(sInst)
	_CLUB_DJ_BATTLE_SCREEN__UPDATE_CURRENT_TINT_COLOR(sInst)
	
	SET_TEXT_RENDER_ID(iTargetID)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	
	// Only draw with tint if a color variation has been assigned
	IF CLUB_DJ_BATTLE_SCREEN__GET_COLOR_VARIANT_LOOKUP(sInst) = NULL
		DRAW_TV_CHANNEL(0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ELSE
		DRAW_TV_CHANNEL(0.5, 0.5, 1.0, 1.0, 0.0, sInst.sCurrentColor.r, sInst.sCurrentColor.g, sInst.sCurrentColor.b, 255)
	ENDIF
	
	// Must return to default after draw
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
ENDPROC

/// PURPOSE:
///    Cleans up the battle screen clearing all assets and resetting it back to the start state
///    this will not clear remove assigned lookups
/// PARAMS:
///    sInst - the battle screen instance
PROC CLUB_DJ_BATTLE_SCREEN__CLEANUP(CLUB_DJ_BATTLE_SCREEN &sInst)
	_CLUB_DJ_BATTLE_SCREEN__CLEANUP_ASSETS(sInst)
	_CLUB_DJ_BATTLE_SCREEN__SET_STATE(sInst, CDJBWS__LINK_RENDER_TARGET)
	sInst.iCurrentColorVariation = 0
	PRINTLN("[CLUB_DJ_BATTLE_SCREEN] CLEANUP - Finished")
ENDPROC

/// PURPOSE:
///    Links a render texture to the assigned dj screen prop and plays the assigned bink movie on it
///    It will handle swapping bink color tints based on the assigned colour variation lookup
/// PARAMS:
///    sInst - the bink wall instance
///    eAudioIntensity - the current intensity of the music, high and high hands will cause more frequent color swaps
PROC CLUB_DJ_BATTLE_SCREEN__MAINTAIN(CLUB_DJ_BATTLE_SCREEN &sInst, CLUB_MUSIC_INTENSITY eAudioIntensity)
	sInst.eAudioIntensity = eAudioIntensity
	
	SWITCH CLUB_DJ_BATTLE_SCREEN__GET_STATE(sInst)
		CASE CDJBWS__LINK_RENDER_TARGET
			_CLUB_DJ_BATTLE_SCREEN__UPDATE_LINK_RENDER_TARGET_STATE(sInst)
		BREAK
		CASE CDJBWS__DRAW
			_CLUB_DJ_BATTLE_SCREEN__UPDATE_DRAW_STATE(sInst)
		BREAK
		CASE CDJBWS__OFF
			IF NOT IS_BROWSER_OPEN()
				_CLUB_DJ_BATTLE_SCREEN__SET_STATE(sInst, CDJBWS__LINK_RENDER_TARGET)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Wrapper for showing DJ bink movies acoording to a lookup of configs
/// PARAMS:
///    sInst - the bink wall instance
///    eAudioIntensity - the current intensity of the music, high and high hands will cause more frequent color swaps
///    fpWallConfigs - the lookup of dj wall configs
///    eWallToShow - the enum of the wall config you want to show, should match enum in lookup, invalid enum value should be -1, if it is invalid the wall will not run
PROC CLUB_DJ_BATTLE_SCREEN__MAINTAIN_WITH_LOOKUP(CLUB_DJ_BATTLE_SCREEN &sInst,  CLUB_MUSIC_INTENSITY eAudioIntensity, T_CLUB_DJ_BATTLE_SCREEN_LOOKUP fpWallConfigs, ENUM_TO_INT eWallToShow, INT &iVariation[])
	INT iWallToShow = ENUM_TO_INT(eWallToShow)
	
	IF iWallToShow = -1
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] MAINTAIN_WITH_LOOKUP - Wall config invalid, not running")
		EXIT
	ENDIF
	
	// Current config has changed so swap
	IF CLUB_DJ_BATTLE_SCREEN__GET_ID(sInst) != iWallToShow
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] MAINTAIN_WITH_LOOKUP - Wall config changed, swapping")
		SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
		
		// Get new config
		sInst = CALL fpWallConfigs(iWallToShow, iVariation)		
		sInst.iCurrentColorVariation = 0
		_CLUB_DJ_BATTLE_SCREEN__GET_COLOUR_VARIANT(sInst, sInst.iCurrentColorVariation, sInst.sCurrentColor)
		_CLUB_DJ_BATTLE_SCREEN__SET_ID(sInst, iWallToShow)
		_CLUB_DJ_BATTLE_SCREEN__SET_STATE(sInst, CDJBWS__LINK_RENDER_TARGET)
		PRINTLN("[CLUB_DJ_BATTLE_SCREEN] MAINTAIN_WITH_LOOKUP - New config set to: ", iWallToShow)
	ENDIF
	
	// Run the current config
	CLUB_DJ_BATTLE_SCREEN__MAINTAIN(sInst, eAudioIntensity)
ENDPROC
