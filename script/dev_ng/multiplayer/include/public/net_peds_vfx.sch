//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CLOTHING.sch																					//
// Description: Header file containing functionality for ped VFX effects.												//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		23/02/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_common.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ VFX CLEANUP ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Stops/removes any active ped looped VFX effects.
/// PARAMS:
///    LoopingPTFX - PTFX to clean up.
PROC CLEANUP_PED_VFX(PTFX_ID &LoopingPTFX)
	IF (LoopingPTFX != NULL)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(LoopingPTFX)
			STOP_PARTICLE_FX_LOOPED(LoopingPTFX, TRUE)
			//ELSE
			//REMOVE_PARTICLE_FX(LoopingPTFX)
		ENDIF
		LoopingPTFX = NULL
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ INITIALISE VFX DATA ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Initialises the ped VFX local array.
///    Sets all elements to -1 and then populates the array with VFX peds.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_VFX_LOCAL_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	INT iVFXPed = 0
	INT iOneShotTrigger = 0
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		LocalData.iVFXPedID[iPed] = -1
	ENDREPEAT
	
	REPEAT MAX_NUM_LOCAL_VFX_PEDS iPed
		REPEAT MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS iOneShotTrigger
			LocalData.VFXData[iPed].fOneShotTriggerPhase[iOneShotTrigger] = -1.0
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		IF (iVFXPed < MAX_NUM_LOCAL_VFX_PEDS)
			IF IS_PEDS_BIT_SET(LocalData.PedData[iPed].iBS, BS_PED_DATA_USE_VFX)
				LocalData.iVFXPedID[iPed] = iVFXPed
				iVFXPed++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped VFX network array.
///    Sets all elements to -1 and then populates the array with VFX peds.
/// PARAMS:
///    ServerBD - Server ped data to set.
PROC INITIALISE_PED_VFX_NETWORK_ARRAY_ID(SERVER_PED_DATA &ServerBD)
	
	INT iPed
	INT iVFXPed = 0
	INT iOneShotTrigger = 0
	
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		ServerBD.iVFXPedID[iPed] = -1
	ENDREPEAT
	
	REPEAT MAX_NUM_NETWORK_VFX_PEDS iPed
		REPEAT MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS iOneShotTrigger
			ServerBD.VFXData[iPed].fOneShotTriggerPhase[iOneShotTrigger] = -1.0
		ENDREPEAT
	ENDREPEAT
	
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		IF (iVFXPed < MAX_NUM_NETWORK_VFX_PEDS)
			IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPed].Data.iBS, BS_PED_DATA_USE_VFX)
				ServerBD.iVFXPedID[iPed] = iVFXPed
				iVFXPed++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the VFX data for all peds.
///    Populates the VFX arrays. 
///    Local array max: MAX_NUM_LOCAL_VFX_PEDS
///    Network array size: MAX_NUM_NETWORK_VFX_PEDS
/// PARAMS:
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_ALL_PED_VFX_DATA(SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	INITIALISE_PED_VFX_LOCAL_ARRAY_ID(LocalData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INITIALISE_PED_VFX_NETWORK_ARRAY_ID(ServerBD)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ VFX ACTIVATION ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the one shot trigger bit.
///    The one shot VFX effect can trigger MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS times per animation.
///    We need to set the correct bit when that instance plays to ensure it only triggers once.
/// PARAMS:
///    iOneShotTrigger - One shot trigger to query.
/// RETURNS: One shot VFX effect instance bit.
FUNC INT GET_PED_VFX_ONE_SHOT_TRIGGER_BIT(INT iOneShotTrigger)
	SWITCH iOneShotTrigger
		CASE 0	RETURN BS_PED_DATA_VFX_ONE_SHOT_TRIGGER_INSTANCE_ONE
		CASE 1	RETURN BS_PED_DATA_VFX_ONE_SHOT_TRIGGER_INSTANCE_TWO
		CASE 2	RETURN BS_PED_DATA_VFX_ONE_SHOT_TRIGGER_INSTANCE_THREE
		CASE 3	RETURN BS_PED_DATA_VFX_ONE_SHOT_TRIGGER_INSTANCE_FOUR
		CASE 4	RETURN BS_PED_DATA_VFX_ONE_SHOT_TRIGGER_INSTANCE_FIVE
	ENDSWITCH
	RETURN BS_PED_DATA_VFX_ONE_SHOT_TRIGGER_INSTANCE_ONE
ENDFUNC

/// PURPOSE:
///    Resets the ped VFX data.
/// PARAMS:
///    Data - Ped data to query.
///    VFXData - VFX ped data to reset.
PROC RESET_PED_VFX_DATA(PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData)
	
	// Only reset looping VFX data if it's not looping
	IF (NOT pedAnimData.VFXLoopingData.bLoopForever)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_LOOPING_ANIM_PHASES_SET)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_START_LOOPING)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_END_LOOPING)
		VFXData.fLoopingEndPhase		= -1.0
		VFXData.fLoopingStartPhase		= -1.0
	ENDIF
	
	// Reset one shot VFX data
	INT iOneShotTrigger = 0
	REPEAT MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS iOneShotTrigger
		CLEAR_PEDS_BIT(Data.iBS, GET_PED_VFX_ONE_SHOT_TRIGGER_BIT(iOneShotTrigger))
	ENDREPEAT	
	
	CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_ONE_SHOT_ANIM_PHASES_SET)
	//CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_ASSETS_LOADED)
ENDPROC

/// PURPOSE:
///    Resets the stored local ped VFX data.
///    Needs to be called each time a new animation is triggered.
/// PARAMS:
///    LocalData - All local ped data to query.
///    iPedID - Ped ID to query.
PROC RESET_LOCAL_PED_VFX_DATA(SCRIPT_PED_DATA &LocalData, PED_ANIM_DATA &pedAnimData, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_VFX)
		EXIT
	ENDIF
	
	IF (LocalData.iVFXPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	RESET_PED_VFX_DATA(LocalData.PedData[iPedID], pedAnimData, LocalData.VFXData[LocalData.iVFXPedID[iPedID]])
ENDPROC

/// PURPOSE:
///    Resets the stored network ped VFX data.
///    Needs to be called each time a new animation is triggered.
/// PARAMS:
///    ServerBD - Server ped data to query.
///    iPedID - Ped ID to query.
PROC RESET_NETWORK_PED_VFX_DATA(SERVER_PED_DATA &ServerBD, PED_ANIM_DATA &pedAnimData, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_VFX)
		EXIT
	ENDIF
	
	IF (ServerBD.iVFXPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	RESET_PED_VFX_DATA(ServerBD.NetworkPedData[iPedID].Data, pedAnimData, ServerBD.VFXData[ServerBD.iVFXPedID[iPedID]])
ENDPROC

/// PURPOSE:
///    Requests and loads a PTFX asset.
/// PARAMS:
///    sPTFXAsset - The PTFX asset to request and load.
/// RETURNS: TRUE when the PTFX asset has been requested and loaded, FALSE otherwise.
FUNC BOOL REQUEST_AND_LOAD_PTFX_ASSET(STRING sPTFXAsset)
	IF NOT IS_STRING_NULL_OR_EMPTY(sPTFXAsset)
		REQUEST_NAMED_PTFX_ASSET(sPTFXAsset)
		RETURN HAS_NAMED_PTFX_ASSET_LOADED(sPTFXAsset)
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds the anim phases for when the looping VFX should trigger. Gets this via visible to script anim event tags.
/// PARAMS:
///    pedAnimData - Populated ped anim data.
///    VFXData - VFX ped data to set.
/// RETURNS: TRUE if all the VFX anim phases are found, FALSE otherwise.
FUNC BOOL FIND_LOOPING_VFX_ANIM_PHASES(PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData)
	
	FLOAT fNull		 	= -1.0
	FLOAT fTriggerPhase	= -1.0
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.VFXLoopingData.sAnimEventStartName)
		IF FIND_ANIM_EVENT_PHASE(pedAnimData.sAnimDict, pedAnimData.sAnimClip, pedAnimData.VFXLoopingData.sAnimEventStartName, fTriggerPhase, fNull)
			VFXData.fLoopingStartPhase = fTriggerPhase
		ENDIF
		IF ARE_STRINGS_EQUAL(pedAnimData.VFXLoopingData.sAnimEventStartName, "FOREVER")
			VFXData.fLoopingStartPhase = 0.0
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.VFXLoopingData.sAnimEventEndName)
		IF FIND_ANIM_EVENT_PHASE(pedAnimData.sAnimDict, pedAnimData.sAnimClip, pedAnimData.VFXLoopingData.sAnimEventEndName, fTriggerPhase, fNull)
			VFXData.fLoopingEndPhase = fTriggerPhase
		ENDIF
		IF ARE_STRINGS_EQUAL(pedAnimData.VFXLoopingData.sAnimEventEndName, "FOREVER")
			VFXData.fLoopingEndPhase = 1.0
		ENDIF
	ENDIF
	
	RETURN (VFXData.fLoopingEndPhase != -1.0 AND VFXData.fLoopingStartPhase != -1.0)
ENDFUNC

/// PURPOSE:
///    Finds the anim phases for when the one shot VFX should trigger. Gets this via visible to script anim event tags.
/// PARAMS:
///    pedAnimData - Populated ped anim data.
///    VFXData - VFX ped data to set.
/// RETURNS: TRUE if all the VFX anim phases are found, FALSE otherwise.
FUNC BOOL FIND_ONE_SHOT_VFX_ANIM_PHASES(PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData)
	
	INT iOneShotTrigger			= 0
	FLOAT fNull		 			= -1.0
	FLOAT fTriggerPhase			= -1.0
	
	REPEAT MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS iOneShotTrigger
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.VFXOneShotData.sAnimEvent[iOneShotTrigger])
			IF FIND_ANIM_EVENT_PHASE(pedAnimData.sAnimDict, pedAnimData.sAnimClip, pedAnimData.VFXOneShotData.sAnimEvent[iOneShotTrigger], fTriggerPhase, fNull)
				VFXData.fOneShotTriggerPhase[iOneShotTrigger] = fTriggerPhase
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the current anim phase.
///    Determines if the animation is a sync scene or task anim and returns correct current phase.
/// PARAMS:
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
/// RETURNS: The current anim phase.
FUNC FLOAT GET_CURRENT_ANIM_PHASE(PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData)
	FLOAT fAnimPhase = 0.0
	
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
		
		INT iSyncSceneID = Data.animData.iSyncSceneID
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
			iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(Data.animData.iSyncSceneID)
		ENDIF
		
		IF (iSyncSceneID != -1 AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID))
			fAnimPhase = GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID)
		ENDIF
		
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
		fAnimPhase = GET_CURRENT_TASK_ANIM_PHASE(Data)
	ENDIF
	
	RETURN fAnimPhase
ENDFUNC

/// PURPOSE:
///    Plays a VFX sound when triggering the one shot effect.
/// PARAMS:
///    VFXOneShotData - VFX one shot data to query.
///    PedID - Ped to play the VFX sound from.
PROC PLAY_ONE_SHOT_VFX_SOUND(PED_VFX_ONE_SHOT_DATA &VFXOneShotData, PED_INDEX &PedID)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(VFXOneShotData.MainData.sPTFXSoundName)
	AND NOT IS_STRING_NULL_OR_EMPTY(VFXOneShotData.MainData.sPTFXSoundSetName)
		PLAY_SOUND_FROM_ENTITY(-1, VFXOneShotData.MainData.sPTFXSoundName, PedID, VFXOneShotData.MainData.sPTFXSoundSetName, VFXOneShotData.MainData.bPTFXSoundOverNetwork, VFXOneShotData.MainData.iPFTXSoundNetworkRange)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Plays a particle FX on a ped prop.
///    Prop ID is specified within the anim data: pedAnimData
/// PARAMS:
///    Data - Ped data to query.
///    MainData - Populated VFX main data.
///    VFXData - VFX ped data to set.
///    bLoopVFX - Whether to play the VFX as looped or not.
///    bOnlyPlaySoundEffect - Whether to skip the VFX effect and just play sound effects.
/// RETURNS: TRUE if the particle FX has been played on the ped prop, FALSE otherwise.
FUNC BOOL PLAY_PARTICLE_FX_ON_PROP(PEDS_DATA &Data, PED_VFX_DATA &MainData, VFX_DATA &VFXData, BOOL bLoopVFX, BOOL bOnlyPlaySoundEffect = FALSE)
	IF (bOnlyPlaySoundEffect)
		RETURN TRUE
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET(MainData.sPTFXAssetName)
	
	IF HAS_NAMED_PTFX_ASSET_LOADED(MainData.sPTFXAssetName)
	AND DOES_ENTITY_EXIST(Data.PropID[MainData.iPropID])
		USE_PARTICLE_FX_ASSET(MainData.sPTFXAssetName)
		
		IF (bLoopVFX)
			VFXData.LoopingPTFX = START_PARTICLE_FX_LOOPED_ON_ENTITY(MainData.sPTFXEffectName, Data.PropID[MainData.iPropID], MainData.vCoordOffset, MainData.vRotationOffset, MainData.fScale)
		ELSE
			START_PARTICLE_FX_NON_LOOPED_ON_ENTITY(MainData.sPTFXEffectName, Data.PropID[MainData.iPropID], MainData.vCoordOffset, MainData.vRotationOffset, MainData.fScale)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Plays a particle FX on a ped bone.
///    Ped bone is specified within the anim data: pedAnimData
/// PARAMS:
///    Data - Ped data to query.
///    MainData - Populated VFX main data.
///    VFXData - VFX ped data to set.
///    bLoopVFX - Whether to play the VFX as looped or not.
///    bOnlyPlaySoundEffect - Whether to skip the VFX effect and just play sound effects.
FUNC BOOL PLAY_PARTICLE_FX_ON_PED_BONE(PEDS_DATA &Data, PED_VFX_DATA &MainData, VFX_DATA &VFXData, BOOL bLoopVFX, BOOL bOnlyPlaySoundEffect = FALSE)
	IF (bOnlyPlaySoundEffect)
		RETURN TRUE
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET(MainData.sPTFXAssetName)
	
	IF HAS_NAMED_PTFX_ASSET_LOADED(MainData.sPTFXAssetName)
		USE_PARTICLE_FX_ASSET(MainData.sPTFXAssetName)
		
		IF (bLoopVFX)
			VFXData.LoopingPTFX = START_PARTICLE_FX_LOOPED_ON_PED_BONE(MainData.sPTFXEffectName, Data.PedID, MainData.vCoordOffset, MainData.vRotationOffset, MainData.eBoneTag, MainData.fScale)
		ELSE
			START_PARTICLE_FX_NON_LOOPED_ON_PED_BONE(MainData.sPTFXEffectName, Data.PedID, MainData.vCoordOffset, MainData.vRotationOffset, MainData.eBoneTag, MainData.fScale)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Wrapper function to determine if the VFX anim phase has surpassed.
///    Anim event phases times are used alongside listening for the anim events firing. This is to accommodate the staggering of the peds.
///    The const float MAX_PED_VFX_ONE_SHOT_ANIM_PHASE_RANGE is used to check if the anim event phase time is within an acceptable range to trigger.
/// PARAMS:
///    fCurrentAnimPhase - Phase of the current animation playing.
///    fTargetAnimPhase - Target anim phase to trigger the VFX from.
/// RETURNS: TRUE if the VFX anim phase has surpassed, FALSE otherwise.
FUNC BOOL HAS_PED_VFX_ANIM_PHASE_SURPASSED(FLOAT fCurrentAnimPhase, FLOAT fTargetAnimPhase)
	
	IF (fTargetAnimPhase != -1.0)
	AND (fCurrentAnimPhase >= fTargetAnimPhase)
	AND (fCurrentAnimPhase - MAX_PED_VFX_ONE_SHOT_ANIM_PHASE_RANGE < fTargetAnimPhase)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines if a peds should trigger a VFX effect.
///    Listens for anim events and phase times of visible to script anim tags.
///    Will try to trigger on anim event, but uses phase time of anim event as a backup if peds are staggered.
/// PARAMS:
///    pedID - Ped ID to query anim event tags.
///    fCurrentAnimPhase - Phase of the current animation playing.
///    fTargetAnimPhase - Target anim phase to trigger the VFX from.
///    sAnimEvent - Anim event tag to trigger VFX from.
///    bIgnoreAnimEventPhases - Ignores the anim event phases and soley relies on the anim events firing.
///    bIgnoreAnimEvents - Ignores the anim events and soley replies on the anim event phases.
/// RETURNS: TRUE when a ped should trigger a VFX effect, FALSE otherwise.
FUNC BOOL SHOULD_TRIGGER_PED_VFX(PED_INDEX pedID, FLOAT fCurrentAnimPhase, FLOAT fTargetAnimPhase, STRING sAnimEvent, BOOL bIgnoreAnimEventPhases = FALSE, BOOL bIgnoreAnimEvents = FALSE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimEvent)
		BOOL bTriggerVFX = FALSE
		IF (NOT bIgnoreAnimEvents)
			IF DOES_ENTITY_EXIST(pedID)
			AND HAS_ANIM_EVENT_FIRED(pedID, GET_HASH_KEY(sAnimEvent))
				bTriggerVFX = TRUE
			ENDIF
		ENDIF
		IF (NOT bIgnoreAnimEventPhases)
			IF HAS_PED_VFX_ANIM_PHASE_SURPASSED(fCurrentAnimPhase, fTargetAnimPhase)
				bTriggerVFX = TRUE
			ENDIF
		ENDIF
		RETURN bTriggerVFX
	ELSE
		RETURN HAS_PED_VFX_ANIM_PHASE_SURPASSED(fCurrentAnimPhase, fTargetAnimPhase)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_HIDING_VFX_IN_CUTSCENE(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData, FLOAT fCurrentAnimPhase)

	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_LOOPING_ANIM_PHASES_SET)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(ePedLocation)
		
		CLEANUP_PED_VFX(VFXData.LoopingPTFX)
		
		IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_SHOULD_BE_LOOPING_AFTER_CUTSCENE)
		AND SHOULD_TRIGGER_PED_VFX(Data.PedID, fCurrentAnimPhase, VFXData.fLoopingStartPhase, pedAnimData.VFXLoopingData.sAnimEventStartName, pedAnimData.VFXLoopingData.bIgnoreAnimEventPhases, pedAnimData.VFXLoopingData.bIgnoreAnimEvents)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_START_LOOPING)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_SHOULD_BE_LOOPING_AFTER_CUTSCENE)
		ENDIF
		
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_SHOULD_BE_LOOPING_AFTER_CUTSCENE)
		AND SHOULD_TRIGGER_PED_VFX(Data.PedID, fCurrentAnimPhase, VFXData.fLoopingEndPhase, pedAnimData.VFXLoopingData.sAnimEventEndName)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_SHOULD_BE_LOOPING_AFTER_CUTSCENE)
		ENDIF
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_HIDING_ONE_SHOT_VFX_IN_CUTSCENE(PED_LOCATIONS ePedLocation, PEDS_DATA &Data)

	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(ePedLocation)
				
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Updates the looping VFX effect.
/// PARAMS:
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    VFXData - VFX ped data to set.
///    fCurrentAnimPhase - Phase of the current animation.
PROC UPDATE_PED_LOOPING_VFX(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData, FLOAT fCurrentAnimPhase)
	
	//	TURN OFF VFX DURING CUTSCENE IF PED SHOULD BE HIDDEN
	IF MAINTAIN_HIDING_VFX_IN_CUTSCENE(ePedLocation, Data, pedAnimData, VFXData, fCurrentAnimPhase)
		EXIT
	ENDIF
	
	// Looping VFX has already started and ended for current anim
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_START_LOOPING)
	AND IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_END_LOOPING)
		EXIT
	ENDIF
	
	// Get looping VFX anim phases
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_LOOPING_ANIM_PHASES_SET)
		IF pedAnimData.VFXLoopingData.bUseCustomPhase
			pedAnimData.VFXLoopingData.bIgnoreAnimEvents = TRUE
			VFXData.fLoopingStartPhase = pedAnimData.VFXLoopingData.fCustomStartPhase
			VFXData.fLoopingEndPhase = pedAnimData.VFXLoopingData.fCustomEndPhase
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_LOOPING_ANIM_PHASES_SET)	
		ENDIF
	ENDIF
	
	// Get looping VFX anim phases
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_LOOPING_ANIM_PHASES_SET)
		IF FIND_LOOPING_VFX_ANIM_PHASES(pedAnimData, VFXData)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_LOOPING_ANIM_PHASES_SET)
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	// Start looping VFX
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_START_LOOPING)
		IF SHOULD_TRIGGER_PED_VFX(Data.PedID, fCurrentAnimPhase, VFXData.fLoopingStartPhase, pedAnimData.VFXLoopingData.sAnimEventStartName, pedAnimData.VFXLoopingData.bIgnoreAnimEventPhases, pedAnimData.VFXLoopingData.bIgnoreAnimEvents)
		OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_SHOULD_BE_LOOPING_AFTER_CUTSCENE)	
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_SHOULD_BE_LOOPING_AFTER_CUTSCENE)	
			IF (pedAnimData.VFXLoopingData.MainData.iPropID != -1)
				CLEANUP_PED_VFX(VFXData.LoopingPTFX)
				IF PLAY_PARTICLE_FX_ON_PROP(Data, pedAnimData.VFXLoopingData.MainData, VFXData, TRUE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_START_LOOPING)
					IF (pedAnimData.VFXLoopingData.bLoopForever)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_END_LOOPING)
					ENDIF
				ENDIF
				
			ELIF (pedAnimData.VFXLoopingData.MainData.eBoneTag != BONETAG_NULL)
				CLEANUP_PED_VFX(VFXData.LoopingPTFX)
				IF PLAY_PARTICLE_FX_ON_PED_BONE(Data, pedAnimData.VFXLoopingData.MainData, VFXData, TRUE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_START_LOOPING)
					IF (pedAnimData.VFXLoopingData.bLoopForever)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_END_LOOPING)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	// End looping VFX
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_END_LOOPING)
		IF SHOULD_TRIGGER_PED_VFX(Data.PedID, fCurrentAnimPhase, VFXData.fLoopingEndPhase, pedAnimData.VFXLoopingData.sAnimEventEndName)
		AND DOES_PARTICLE_FX_LOOPED_EXIST(VFXData.LoopingPTFX)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_END_LOOPING)
			CLEANUP_PED_VFX(VFXData.LoopingPTFX)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Updates the one shot VFX effect.
/// PARAMS:
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    VFXData - VFX ped data to set.
///    fCurrentAnimPhase - Phase of the current animation.
PROC UPDATE_PED_ONE_SHOT_VFX(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData, FLOAT fCurrentAnimPhase)
	
	//	TURN OFF VFX DURING CUTSCENE IF PED SHOULD BE HIDDEN
	IF MAINTAIN_HIDING_ONE_SHOT_VFX_IN_CUTSCENE(ePedLocation, Data)
		EXIT
	ENDIF
	// Get one shot VFX anim phases
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_ONE_SHOT_ANIM_PHASES_SET)
		IF FIND_ONE_SHOT_VFX_ANIM_PHASES(pedAnimData, VFXData)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_ONE_SHOT_ANIM_PHASES_SET)
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	// One shot VFX
	INT iOneShotTrigger = 0
	REPEAT MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS iOneShotTrigger
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.VFXOneShotData.sAnimEvent[iOneShotTrigger])
			
			IF NOT IS_PEDS_BIT_SET(Data.iBS, GET_PED_VFX_ONE_SHOT_TRIGGER_BIT(iOneShotTrigger))
			AND SHOULD_TRIGGER_PED_VFX(Data.PedID, fCurrentAnimPhase, VFXData.fOneShotTriggerPhase[iOneShotTrigger], pedAnimData.VFXOneShotData.sAnimEvent[iOneShotTrigger], pedAnimData.VFXOneShotData.bIgnoreAnimEventPhases, pedAnimData.VFXOneShotData.bIgnoreAnimEvents)
				
				IF (pedAnimData.VFXOneShotData.MainData.iPropID != -1)
					IF PLAY_PARTICLE_FX_ON_PROP(Data, pedAnimData.VFXOneShotData.MainData, VFXData, FALSE, pedAnimData.VFXOneShotData.bOnlyPlaySoundEffect)
						PLAY_ONE_SHOT_VFX_SOUND(pedAnimData.VFXOneShotData, Data.PedID)
						SET_PEDS_BIT(Data.iBS, GET_PED_VFX_ONE_SHOT_TRIGGER_BIT(iOneShotTrigger))
						BREAKLOOP
					ENDIF
					
				ELIF (pedAnimData.VFXOneShotData.MainData.eBoneTag != BONETAG_NULL)
					IF PLAY_PARTICLE_FX_ON_PED_BONE(Data, pedAnimData.VFXOneShotData.MainData, VFXData, FALSE, pedAnimData.VFXOneShotData.bOnlyPlaySoundEffect)
						PLAY_ONE_SHOT_VFX_SOUND(pedAnimData.VFXOneShotData, Data.PedID)
						SET_PEDS_BIT(Data.iBS, GET_PED_VFX_ONE_SHOT_TRIGGER_BIT(iOneShotTrigger))
						BREAKLOOP
					ENDIF
				ENDIF
				
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Updates the peds VFX effects.
///    Each VFX ped can play two VFX effects: Looping and One Shot.
///    Looping VFX effect loops throughout the animation clip and can be triggered off and on.
///    One shot VFX effect can be triggered MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS times throughout the animation.
///        -Anim event tags are used to determine when to trigger the VFX effects.
///        -This presents a problem for staggered peds however as they may miss the frame the event tag is triggered on.
///    	   -The anim event tag phases are searched for and stored as a back up. Anim phase time is checked incase the event has been missed.
/// PARAMS:
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    VFXData - VFX ped data to set.
PROC UPDATE_PED_VFX(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, VFX_DATA &VFXData, INT iPedID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, BOOL bDancingTransition)
		
	// Regrab anim data as it is out of scope
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, iPedID)
	
	// Check if current clip uses VFX
	IF (NOT pedAnimData.bVFXAnimClip)
		EXIT
	ENDIF
	
	// Request and load VFX assets
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_VFX_ASSETS_LOADED)
		IF REQUEST_AND_LOAD_PTFX_ASSET(pedAnimData.VFXLoopingData.MainData.sPTFXAssetName)
		AND REQUEST_AND_LOAD_PTFX_ASSET(pedAnimData.VFXOneShotData.MainData.sPTFXAssetName)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_VFX_ASSETS_LOADED)
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	// Anim dictionary needs to be loaded to find phases from event tags
	IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
		EXIT
	ENDIF
	
	FLOAT fCurrentAnimPhase = GET_CURRENT_ANIM_PHASE(Data, pedAnimData)
	
	UPDATE_PED_LOOPING_VFX(ePedLocation, Data, pedAnimData, VFXData, fCurrentAnimPhase)
	UPDATE_PED_ONE_SHOT_VFX(ePedLocation,Data, pedAnimData, VFXData, fCurrentAnimPhase)
	
ENDPROC

/// PURPOSE:
///    Main update function for the local ped vfx effects.
/// PARAMS:
///    LocalData - All local ped data to query.
///    pedAnimData - Populated ped anim data.
///    iPedID - Ped ID to query.
PROC MAINTAIN_LOCAL_PED_VFX(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, PED_ANIM_DATA &pedAnimData, INT iPedID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, BOOL bDancingTransition)
	
	IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_VFX)
		EXIT
	ENDIF		
	
	IF (LocalData.iVFXPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	UPDATE_PED_VFX(ePedLocation, LocalData.PedData[iPedID], pedAnimData, LocalData.VFXData[LocalData.iVFXPedID[iPedID]], iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
ENDPROC

/// PURPOSE:
///    Main update function for the network ped vfx effects.
/// PARAMS:
///    ServerBD - Server ped data to query.
///    pedAnimData - Populated ped anim data.
///    iPedID - Ped ID to query.
PROC MAINTAIN_NETWORK_PED_VFX(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PED_ANIM_DATA &pedAnimData, INT iPedID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, BOOL bDancingTransition)
	
	IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_VFX)
		EXIT
	ENDIF
		
	IF (ServerBD.iVFXPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	UPDATE_PED_VFX(ePedLocation, ServerBD.NetworkPedData[iPedID].Data, pedAnimData, ServerBD.VFXData[ServerBD.iVFXPedID[iPedID]], iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
