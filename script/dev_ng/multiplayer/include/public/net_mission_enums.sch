


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_mission_enums.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Extracts any CNC Mission Enums into their own header.
//		NOTES			:	This had to be done when the MP_MISSION_DATA struct had to be put into its own file to allow it to be
//								included in multiple global files.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

USING "mp_globals_new_features_TU.sch"



// ===========================================================================================================
//      The MP MISSION enum - mission IDs
// ===========================================================================================================

ENUM MP_MISSION
								  
	eNULL_MISSION,	//0
	
//	eCR_TEST_MISSION_1,
	
	// Naming Example
	// CR - CRIM, CP - COP
	
	// Add missions here....
	
	// Cop Random Event Missions	
//	eAM_CP_BADCOP,
//	eAM_CP_ScanChase,
//	eAM_CP_SkyEye,
//	eAM_CP_Snitch,
//	eAM_CP_TowTruck,	//6
	
	// Cop Tutorials
//	eCP_WelcomeTut,	
//	eCP_SNITCH_TUT,
//	eCP_STOLEN_TUT,
//	eCP_PARTNER_TUT,
//	eCP_ARMOURY_TUT,	//11
	
	// Large cop investigations
//	eAM_CP_ARREST,
//	eCP_InformHeli,
//	eCP_PrisonTrans,	//14
//	eCP_ProWitCourt,
//	eCP_vip_tour,	
//	eCP_weapons_ambush,
//	eCP_takeback_territory,
	
	// Crook Ambient Missions
	eAM_HOLD_UP,
	eAM_CR_SecurityVan,
	eAM_CR_SELL_DRUGS,
	eAM_STRIPPER,
	eAM_STRIPCLUB,
	eAM_ARMWRESTLING,
	eAM_IMPORT_EXPORT,
	eAM_TENNIS,
	eAM_DARTS,
	eAM_FistFight,
	eAM_DropOffHooker,
	eAM_Safehouse,
	eAM_Hitchhiker,
	
	// Crook Tutorials
//	eCR_WELCOME,		//32
//	eCR_SELL_CARS_TUT,
//	eCR_HOOKER_TUT,
//	eCR_GUN_SHOP_TUT,
//	eCR_RIVAL_TUT,
//	eCR_JOBLIST_TUT,	//37
//	eCR_DRUGS_TUT,
//	eCR_SNITCH_TUT,
//	eCR_HOLD_UP_TUT,
//	eCR_PROPERTY_TUT,
	
	// Crook missions
//	eCR_BossMeet,
//	eCR_Convoy_Steal,
//	eCR_DefendBase,
//	eCR_DisposeOfVehicle,
//	eCR_escortCrimBoss,
//	eCR_farmhouse,
//	eCR_FBI_grab,
//	eCR_Funeral,//49
//	eCR_GANGWAR,
//	eCR_GET_VEHICLE,	
//	eCR_GOGET,
//	eCR_JET_SKI,
#IF IS_DEBUG_BUILD
	eCR_oceandrop,
#ENDIF
//	eCR_PACKAGE_GRAB,
//	eCR_parley,
//	eCR_PolBustOut,
//	eCR_PLANE_DROP,
//	eCR_PRISON_BREAK,
//	eCR_Races, //60
//	eCR_SHIPMENT_STEAL_1,
//	eCR_Showroom,
//	eCR_Steal_Vehicle,
//	eCR_STEAL_BIKES,
//	eCR_van_rescue,
//	eCR_AttackBoat,
//	eCR_Airport_Takeover,
//	eCR_Mayhem,
//	eCR_CokeChase,	//69
//	eCR_CarBlowUp,
//	eCR_hostage_steal,
//	eCR_MilitaryBase,
	
	// Heists
//	eCR_BANK_HEIST_1,
//	eCR_COUNTRY_HEIST_1,
//	eCR_FBI_HEIST_1,
	
	//Cop or crim mission
//	eCR_ASIN,
//	eAM_CR_CLEANAREA,
	
	//NPC chaser script
//	eAM_NPC_Chasers,
//	eAM_ADV_CLEANUP,
	//The cutscene missio
	eCOC_Do_Cutscene,		//80
	
	eAM_DOORS,
//	eCR_TRIGGER_TUT,
	eMG_RACE_TO_POINT,
	eAM_PI_MENU,
	eAM_CRATE_DROP,
	eAM_FM_IMP_EXP,
	eFM_INTRO,
	eAM_PROSTITUTE,
	eAM_TAXI,
	eAM_TAXI_LAUNCHER,
	eAM_GANG_CALL,			//90
	eHELI_GUN,
	eBACKUP_HELI,
	eAIRSTRIKE,
	eAM_AMMO_DROP,
	eAM_VEHICLE_DROP,
	eAM_BRU_BOX,
	eAM_GA_PICKUPS,
	eAM_JOYRIDER,
	eAM_PLANE_TAKEDOWN,
	eAM_DISTRACT_COPS,
	eAM_DESTROY_VEH,
	eAM_HOT_TARGET,
	eAM_KILL_LIST,
//	eAM_PLANE_DROP,
	eAM_TIME_TRIAL,
	eAM_CP_COLLECTION,
	eAM_CHALLENGES,
	eAM_PENNED_IN,
//	eAM_MULTI_TARGET,
	eAM_PASS_THE_PARCEL,
	eAM_HOT_PROPERTY,
	eAM_DEAD_DROP,
	eAM_KING_OF_THE_CASTLE,
	eAM_CRIMINAL_DAMAGE,
//	eAM_INFECTION,
	//eAM_MYSTERIOUS_TEXT,
	eAM_HUNT_THE_BEAST,
	eAM_BOSS_ATTACK,
	eAM_BOSS_VS_BOSS_DM,
	//eAM_BOSS_CHALLENGES,
	eAM_BOSS_STEAL_VEHICLE,
	eAM_BOSS_POINT_TO_POINT,
	eAM_TERMINATE,
	eAM_YACHT_ROBBERY,
	eAM_BELLY_OF_THE_BEAST,
	eAM_CHAL_FIVESTAR,
	eAM_CHAL_ROB_SHOP,
	eAM_CHAL_COLLECT_MONEY,
//	eAM_CHAL_INFIGHTING,
	eAM_BOSS_ASSAULT,
	eAM_BOSS_VEH_SURVIVAL,
	eAM_BOSS_SIGHTSEER,
	eAM_BOSS_FLYING_IN_STYLE,
	eAM_BOSS_FINDERSKEEPERS,
	eAM_BOSS_HUNT_THE_BOSS,
	eAM_CARJACKING,
	eAM_BOSS_HEADHUNTER,
	eAM_CONTRABAND_BUY,
	eAM_CONTRABAND_SELL,
	eAM_CONTRABAND_DEFEND,
	eAM_BOSS_AIRFREIGHT,
	eAM_CHAL_CASHOUT,
	eAM_CHAL_SALVAGE,
	eAM_BOSS_FRAGILE_GOODS,
	#IF FEATURE_STUNT_FM_EVENTS
	eAM_OFFROAD_RACE,
	eAM_STOP_THE_STUNT,
	eAM_BLAST,
	eAM_TARGET_PRACTICE,
	#ENDIF
	eAM_VEHICLE_EXPORT_BUY,
	eAM_VEHICLE_EXPORT_SELL,
	eAM_IMPEXP_PLOUGHED,
	eAM_IMPEXP_FULLY_LOADED,
	eAM_IMPEXP_AMPHIBIOUS_ASSAULT,
	eAM_IMPEXP_TRANSPORTER,
	eAM_IMPEXP_FORTIFIED,
	eAM_IMPEXP_VELOCITY,
	eAM_IMPEXP_RAMPED_UP,
	eAM_IMPEXP_STOCKPILING,
	eAM_BIKER_RACE_P2P,
	eAM_BIKER_JOUST,
	eAM_BIKER_UNLOAD_WEAPONS,
	eAM_MISSION_PLACEHOLDER,
	eAM_BIKER_DEAL_GONE_WRONG,
	eAM_BIKER_RESCUE_CONTACT,
	eAM_BIKER_LAST_RESPECTS,
	eAM_BIKER_CONTRACT_KILLING,
//	eAM_BIKER_FIGHT_OR_FLIGHT,
	eAM_BIKER_SELL,
	eAM_BIKER_DEFEND,
	eAM_BIKER_BUY,
	eAM_BIKER_DRIVEBY_ASSASSIN,
	eAM_BIKER_RIPPIN_IT_UP,
	eAM_BIKER_FREE_PRISONER,
//	eAM_BIKER_BURN_ASSETS,
	eAM_BIKER_SAFECRACKER,
	eAM_BIKER_STEAL_BIKES,
	eAM_BIKER_SEARCH_AND_DESTROY,
	eAM_BIKER_CAGED_IN,
	eAM_BIKER_STAND_YOUR_GROUND,
	eAM_BIKER_CRIMINAL_MISCHIEF,
	eAM_BIKER_DESTROY_VANS,
	eAM_BIKER_BURN_ASSETS,
	eAM_BIKER_SHUTTLE,	
	eAM_BIKER_WHEELIE_RIDER,
	eAM_GUNRUNNING_BUY,	
	eAM_GUNRUNNING_SELL,
	eAM_GUNRUNNING_DEFEND,
	eAM_SMUGGLER_BUY,	
	eAM_SMUGGLER_SELL,
	eAM_SMUGGLER_SETUP,
	eAM_FM_GANGOPS,
	eAM_BUSINESS_BATTLES,
	eAM_FMBB_SELL,				
	eAM_FMBB_DEFEND,			
	eAM_FMBB_SECURITY_VAN,		
	eAM_FMBB_TARGET_PURSUIT,	
	eAM_FMBB_JEWEL_STORE_GRAB,	
	eAM_FMBB_BANK_JOB,			
	eAM_FMBB_DATA_HACK,	
	eAM_FMBB_INFILTRATION,	
	eAM_FMBB_PHONECALL,
	eAM_FMBB_CLUB_MANAGEMENT,				
	eAM_GB_CASINO,
	eAM_GB_CASINO_HEIST,
	eAM_FMC_BUSINESS_BATTLES,
	
	#IF FEATURE_COPS_N_CROOKS
	eAM_SERVE_AND_PROTECT,
	eAM_DISPATCH,
	#ENDIF
	
	eAM_DRUG_VEHICLE,
	eAM_MOVIE_PROPS,
	
	#IF FEATURE_HEIST_ISLAND
	eAM_ISLAND_HEIST_PREP,
	eAM_ISLAND_DJ_MISSION,
	eAM_ISLAND_BACKUP_HELI,
	eAM_GOLDEN_GUN,
	#ENDIF
	
	#IF FEATURE_TUNER
	eAM_TUNER_ROBBERY,				
	eAM_VEHICLE_LIST_EVENT,					
	eAM_SANDBOX_ACTIVITY,	
	eAM_TUNER_CLIENT_DELIVERY,
	#ENDIF
	
	#IF FEATURE_FIXER
	eAM_FIXER_PAYPHONE,	
	eAM_FIXER_SECURITY,	
	eAM_FIXER_VIP,	
	eAM_METAL_DETECTOR,
	eAM_AGENCY_SUV,
	#ENDIF
	
	#IF FEATURE_HALLOWEEN_2021
	eAM_PHANTOM_CAR,
	eAM_SLASHER,
	eAM_SIGHTSEEING,
	#ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	eAM_HSW_TIME_TRIAL,
	eAM_GEN9_MP_INTRO,
	eAM_HSW_SETUP,
	#ENDIF
	
	#IF FEATURE_DLC_1_2022
	eAM_SMUGGLER_TRAIL,
	eAM_SMUGGLER_PLANE,
	eAM_SKYDIVING_CHALLENGE,
	eAM_CERBERUS,
	eAM_PARACHUTER,
	eAM_CRIME_SCENE,
	eAM_BAR_RESUPPLY,
	eAM_BIKE_SHOP_DELIVERY,
	eAM_CLUBHOUSE_CONTRACTS,
	eAM_SPECIAL_CARGO,
	eAM_EXPORT_CARGO,
	eAM_GUNRUNNING_AMMUNATION,
	eAM_GUNRUNNING_RESUPPLY,
	eAM_SOURCE_RESEARCH,
	eAM_CLUB_MANAGEMENT,
	eAM_CLUB_ODD_JOBS,
	eAM_CLUB_SOURCE,
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	eAM_GANG_CONVOY,
	eAM_ROBBERY,
	eAM_ACID_LAB_SETUP,
	eAM_ACID_LAB_SOURCE,
	eAM_ACID_LAB_SELL,
	eAM_DRUG_LAB_WORK,
	eAM_STASH_HOUSE,
	eAM_TAXI_DRIVER,
	eAM_XMAS_MUGGER,
	eAM_BANK_SHOOTOUT,
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	eAM_FMC_TEST,
	#ENDIF
	
	eAM_BOAT_TAXI,
	eAM_HELI_TAXI,
	eFM_HOLD_UP_TUT,
	eAM_CARMOD_TUT,
	eAM_CONTACT_REQUESTS,
	eAM_SMALL_MISSION,
	eAM_NPC_INVITES,
	eAM_LESTER_CUTSCENE,
	eAM_LESTER_HEIST_INT,
	
	eAM_LOWRIDER_INT,
	
	eAM_VEHICLE_SPAWN,
	eAM_TREVOR_CUTSCENE,
	eAM_ARMY_BASE,
	eAM_PRISON,
	eAM_ROLLERCOASTER,
	eAM_FERRISWHEEL,
	eAM_LAUNCHER,
	eAM_DAILY_OBJECTIVES,
	
	// FM Activities (data not held on cloud) - Keith 12/11/12 (to get FM to use Mission Controller)
	eFM_ARM_WRESTLING,
	eFM_DARTS,
	eFM_GOLF,				//100
	eFM_SHOOTING_RANGE,
	eFM_TENNIS,
	eFM_PILOT_SCHOOL,
	eFM_IMPROMPTU_DM, 
	
	// FM Activities (data held on cloud) - Keith 4/1/13 (to allow FM to use Mission Controller)
	eFM_BASEJUMP_CLOUD,
	eFM_DEATHMATCH_CLOUD,
	eFM_GANG_ATTACK_CLOUD,
	eFM_MISSION_CLOUD,
	eFM_RACE_CLOUD,
	eFM_SURVIVAL_CLOUD,		//108
	
	
#IF IS_DEBUG_BUILD
	eCR_TUT_INTRO,
	eCR_TUT_KILL_GANG,
	eCR_TUT_DRUG,
	eCR_TUT_TERR,
	eCR_TUT_SNITCH,
	eCP_takeback_territory,
	eCR_PROPERTY_TUT,
	eCP_Tut_Welcome,
	eCP_Tut_Stolen,
#ENDIF

	eAM_DONT_CROSS_THE_LINE,
	
	eAM_APARTMENT_DARTS,
	eAM_APARTMENT_ARMWRESTLING,	

	eAM_SCGW_CABINET,
	eAM_SCROLLING_ARCADE_CABINET,
	eAM_EXAMPLE_ARCADE_CABINET,
	eAM_ROAD_ARCADE_CABINET,
	eAM_TLG_ARCADE_CABINET,
	eAM_TWS_ARCADE_CABINET,
	eAM_CASINO_LIMO,
	eAM_CASINO_LUXURY_CAR,
	eAM_DEGENATRON_GAMES_CABINET,
	eAM_GGSM_ARCADE_CABINET,
	eAM_QUB3D_ARCADE_CABINET,
	eAM_CAMHEDZ_ARCADE_CABINET,
	//SCTV
	eSCTV,
#IF FEATURE_COPS_N_CROOKS
	eCnC_HOLDUP,
	eCnC_JOB,
#ENDIF
	// Don't add any missions below here.
	eMAX_NUM_MP_MISSION		//110	- NOTE: This is now the 'in use' maximum but the array maximum will be larger to reserve space for the CnC missions coming back
	
ENDENUM

// The maximum array size for missions - used to reserve the global space we'll need (estimate) when CnC returns so that the array of globals doesn't need to change size with the DLC
// NOTE: We were on 122 CnC + Freemode missions before I removed the CnC mission references
CONST_INT	MP_MISSIONS_ARRAY_MAX		240





