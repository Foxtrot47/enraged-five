

// ====================================================================================
// ====================================================================================
//
// Name:        net_gang_angry.sch
// Description: Controls the checks to see if a gang is still angry at you, contains functions for use in .sc files.
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================


// =================
// 	Using headers
// =================
USING "globals.sch"

// =================
// 	Structs and Declarations
// =================

CONST_INT GANG_ANGRY_TIME_SHORT				60000	//1min
CONST_INT GANG_ANGRY_TIME_MEDIUM			300000	//5min
CONST_INT GANG_ANGRY_TIME_LONG				600000	//10min


CONST_INT GANG_ANGRY_BS_SET_UP				0

/// PURPOSE: Data for gang anger management
STRUCT GANG_ANGRY_DATA
	INT iBitSet = 0
	INT iTimeStartBitSet = 0
	INT iStaggeredCounter = 0
	
	TIME_DATATYPE timeGangAngryStart[GANG_CALL_TYPE_TOTAL_NUMBER]
	
	#IF IS_DEBUG_BUILD
		BOOL bDebugAngry[GANG_CALL_TYPE_TOTAL_NUMBER]
		INT iDebugTimeToForgiven[GANG_CALL_TYPE_TOTAL_NUMBER]
		BOOL bDebugRequestForgiveness[GANG_CALL_TYPE_TOTAL_NUMBER]
		BOOL bDebugRequestAngry[GANG_CALL_TYPE_TOTAL_NUMBER]
	#ENDIF
	
ENDSTRUCT

/// PURPOSE:
///    Returns if the specified gang ID is still angry at the local player
/// PARAMS:
///    iGangID - ID of the gang to check
/// RETURNS:
///    true/false
FUNC BOOL IS_GANG_ANGRY_AT_PLAYER(INT iGangID)
	IF IS_BIT_SET(MPGlobals.iAngryGangBitset, iGangID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Stops the specified gang from being angry at the local player
/// PARAMS:
///    iGangID - ID of the gang to check
PROC CLEAR_GANG_ANGRY_AT_PLAYER(INT iGangID)
	IF IS_GANG_ANGRY_AT_PLAYER(iGangID)
		CLEAR_BIT(MPGlobals.iAngryGangBitset, iGangID)
		MPGlobals.iAngryGangForgivenessTime[iGangID] = 0
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== NET_GANG_ANGRY === CLEAR_GANG_ANGRY_AT_PLAYER iGangID = ", iGangID)
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Begins the gang being angry at the local player for a set amount of time
/// PARAMS:
///    iGangID - ID of the gang to become angry
///    iTimeToForgiveness - time for the gang to be angry for in ms
PROC SET_GANG_ANGRY_AT_PLAYER(INT iGangID, INT iTimeToForgiveness)
	IF MPGlobals.iAngryGangForgivenessTime[iGangID] < iTimeToForgiveness
		MPGlobals.iAngryGangForgivenessTime[iGangID] = iTimeToForgiveness
	ENDIF
	SET_BIT(MPGlobals.iAngryGangBitset, iGangID)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== NET_GANG_ANGRY === SET_GANG_ANGRY_AT_PLAYER iGangID = ", iGangID)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Staggers through all the gang IDs and tests if the gang angry timer has elapsed. If so, gang is cleared of being angry.
/// PARAMS:
///    gangAngryData - local gang angry data
PROC PRIVATE_TEST_GANG_TIMER(GANG_ANGRY_DATA &gangAngryData)
	IF IS_GANG_ANGRY_AT_PLAYER(gangAngryData.iStaggeredCounter)
		
		IF NOT IS_BIT_SET(gangAngryData.iTimeStartBitSet, gangAngryData.iStaggeredCounter)
			gangAngryData.timeGangAngryStart[gangAngryData.iStaggeredCounter] = GET_NETWORK_TIME()
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== NET_GANG_ANGRY === SET_BIT(gangAngryData.iTimeStartBitSet, ", gangAngryData.iStaggeredCounter, ")")
			#ENDIF
			SET_BIT(gangAngryData.iTimeStartBitSet, gangAngryData.iStaggeredCounter)
		ENDIF
		
		IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), gangAngryData.timeGangAngryStart[gangAngryData.iStaggeredCounter]) > MPGlobals.iAngryGangForgivenessTime[gangAngryData.iStaggeredCounter]
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== NET_GANG_ANGRY === GET_TIME_DIFFERENCE = ",
										GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), gangAngryData.timeGangAngryStart[gangAngryData.iStaggeredCounter]),
										" iAngryGangForgivenessTime = ", MPGlobals.iAngryGangForgivenessTime[gangAngryData.iStaggeredCounter])
			#ENDIF
			
			CLEAR_GANG_ANGRY_AT_PLAYER(gangAngryData.iStaggeredCounter)
		ENDIF
	ELSE
		IF IS_BIT_SET(gangAngryData.iTimeStartBitSet, gangAngryData.iStaggeredCounter)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== NET_GANG_ANGRY === CLEAR_BIT(gangAngryData.iTimeStartBitSet, ", gangAngryData.iStaggeredCounter, ")")
			#ENDIF
			CLEAR_BIT(gangAngryData.iTimeStartBitSet, gangAngryData.iStaggeredCounter)
		ENDIF
	ENDIF
	
	gangAngryData.iStaggeredCounter++
	IF gangAngryData.iStaggeredCounter >= GANG_CALL_TYPE_TOTAL_NUMBER
		gangAngryData.iStaggeredCounter = 0
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Creates widgets for angry gangs
/// PARAMS:
///    gangAngryData - local gang angry data
PROC CREATE_GANG_ANGRY_WIDGETS(GANG_ANGRY_DATA &gangAngryData)
	START_WIDGET_GROUP("Gang Angry")
		START_WIDGET_GROUP("Time to Forgiveness")
			ADD_WIDGET_BOOL("Thief Angry", gangAngryData.bDebugAngry[GANG_CALL_TYPE_SPECIAL_THIEF])
			ADD_WIDGET_INT_READ_ONLY("Thief Forgiveness", gangAngryData.iDebugTimeToForgiven[GANG_CALL_TYPE_SPECIAL_THIEF])
			ADD_WIDGET_BOOL("Merryweather Angry", gangAngryData.bDebugAngry[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
			ADD_WIDGET_INT_READ_ONLY("Merryweather Forgiveness", gangAngryData.iDebugTimeToForgiven[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Request Forgiveness")
			ADD_WIDGET_BOOL("Request Thief Forgiveness", gangAngryData.bDebugRequestForgiveness[GANG_CALL_TYPE_SPECIAL_THIEF])
			ADD_WIDGET_BOOL("Request Merryweather Forgiveness", gangAngryData.bDebugRequestForgiveness[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Request Angry")
			ADD_WIDGET_BOOL("Request Thief Angry", gangAngryData.bDebugRequestAngry[GANG_CALL_TYPE_SPECIAL_THIEF])
			ADD_WIDGET_BOOL("Request Merryweather Angry", gangAngryData.bDebugRequestAngry[GANG_CALL_TYPE_SPECIAL_MERRYWEATHER])
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Update widgets for angry gangs
/// PARAMS:
///    gangAngryData - local gang angry data
PROC UPDATE_GANG_ANGRY_WIDGETS(GANG_ANGRY_DATA &gangAngryData)
	INT i
	REPEAT GANG_CALL_TYPE_TOTAL_NUMBER i
	
		gangAngryData.bDebugAngry[i] = IS_GANG_ANGRY_AT_PLAYER(i)
	
		IF IS_GANG_ANGRY_AT_PLAYER(i)
			gangAngryData.iDebugTimeToForgiven[i] = MPGlobals.iAngryGangForgivenessTime[i] -  GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), gangAngryData.timeGangAngryStart[i])
			
			IF gangAngryData.bDebugRequestForgiveness[i]
				CLEAR_GANG_ANGRY_AT_PLAYER(i)
				gangAngryData.bDebugRequestForgiveness[i] = FALSE
			ENDIF
		ENDIF
		IF gangAngryData.bDebugRequestAngry[i]
			SET_GANG_ANGRY_AT_PLAYER(i, GANG_ANGRY_TIME_SHORT)
			gangAngryData.bDebugRequestAngry[i] = FALSE
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

// =================
// 	Procs & funcs
// =================

/// PURPOSE:
///    Maintains gangs remaining angry at the local player for set amounts of time based on the action that made them angry in the first place
/// PARAMS:
///    gangAngryData - local gang angry data
PROC MAINTAIN_GANG_ANGRY(GANG_ANGRY_DATA &gangAngryData)

	IF NOT IS_BIT_SET(gangAngryData.iBitSet, GANG_ANGRY_BS_SET_UP)
		INT i
		REPEAT GANG_CALL_TYPE_TOTAL_NUMBER i
			#IF IS_DEBUG_BUILD
				gangAngryData.iDebugTimeToForgiven[i] = 0
				gangAngryData.bDebugRequestForgiveness[i] = FALSE
				gangAngryData.bDebugRequestAngry[i] = FALSE
			#ENDIF
		ENDREPEAT
	
		gangAngryData.iStaggeredCounter = 0
		MPGlobals.iAngryGangBitset = 0
		SET_BIT(gangAngryData.iBitSet, GANG_ANGRY_BS_SET_UP)
		
	ELSE
		PRIVATE_TEST_GANG_TIMER(gangAngryData)
	ENDIF
ENDPROC
