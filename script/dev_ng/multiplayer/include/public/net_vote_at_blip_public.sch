USING "rage_builtins.sch"
USING "globals.sch"

USING "selector_public.sch"
USING "Net_Entity_Controller.sch" 





// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Vote_At_Blip_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Extracts Vote At Blip generic functionality required by Mission At Coords system into a public header.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Vote At Blip Access Functions
// ===========================================================================================================

// PURPOSE:	Setup a new vote for this player
//
// INPUT PARAMS:		paramMyGBD			The player's GBD slot
//						paramMissionID		Mission ID
//						paramVariation		Mission Variation
//
// NOTES:	This currently uses the CnC voting routines
PROC Setup_My_Vote(INT paramMyGBD, MP_MISSION paramMissionID, INT paramVariation)

	GlobalplayerBD_FM_2[paramMyGBD].missionToStart		= ENUM_TO_INT(paramMissionID)
	GlobalplayerBD_FM_2[paramMyGBD].missionVariation	= paramVariation
	
	SET_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biIwantToVote)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Setup My Vote Variables ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
		NET_PRINT(" (Variation = ")
		NET_PRINT_INT(paramVariation)
		NET_PRINT(")")
		NET_NL()
	#ENDIF
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clean up this player's vote variables
//
// INPUT PARAMS:		paramMyGBD			The player's GBD slot
PROC CLEAN_UP_MY_VOTE_VARIABLES(INT paramMyGBD, BOOL bRestInvincibility = TRUE)

	NET_PRINT("...KGM MP...BWW...MP_VOTE_SYSTEM -  CLEAN_UP_MY_VOTE_VARIABLES - Cleaning up biIwantToVote") NET_NL()
	
	IF bRestInvincibility
		PRINTLN("SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)")
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
	ENDIF
	
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biIwantToVote)
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biVoteWithOpsiteTeam)
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biVoteHasPassed) 
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biVoteSetUp) 
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biPlayerWarpingToCorona)
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biIPressedVote)	
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biIPressedVoteSecondTime)	
	CLEAR_BIT(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biVoteIsGoinOn) 
	SET_BIT  (GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biILeftTheArea)
	
	GlobalplayerBD_FM_2[paramMyGBD].missionToStart		= ciNULL_MISSION
	GlobalplayerBD_FM_2[paramMyGBD].missionVariation 	= ciNULL_VARIATION
	GlobalplayerBD_FM_2[paramMyGBD].iVoteToUse			= -1
	GlobalplayerBD_FM_2[paramMyGBD].iNumberInVote		= 0
	GlobalplayerBD_FM_2[paramMyGBD].bHasPlayerVoted 	= FALSE
	
	IF IS_SELECTOR_DISABLED()
		NET_PRINT("...KGM MP...BWW...CLEAN_UP_MY_VOTE_VARIABLES - ENABLE_SELECTOR") NET_NL()
		ENABLE_SELECTOR()
	ENDIF
	//g_bVoteDisabledeSelector  = FALSE
	
	IF IS_OVERHEAD_EVENT_ACTIVE(DISPLAYOVERHEADS_EVENTS_IS_VOTING, PLAYER_ID())
		BROADCAST_SCRIPT_EVENT_OHD_MAIN(DISPLAYOVERHEADS_EVENTS_IS_VOTING, FALSE)
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the current vote has passed
//
// INPUT PARAMS:		paramMyGBD			The player's GBD slot
// RETURN VALUE:		BOOL				TRUE if the current vote has passed, FALSE if not
//
// NOTES:	Also checks the debug 'trigger mission with one player' plag
FUNC BOOL Has_My_Vote_Passed(INT paramMyGBD)

	// Check if the vote has passed
	IF (IS_BIT_SET(GlobalplayerBD_FM_2[paramMyGBD].iBitSet, biVoteHasPassed))
		NET_PRINT("...KGM MP...BWW...MP_VOTE_SYSTEM - Vote Has Passed") NET_NL()
		RETURN TRUE
	ENDIF
	
	// Check if the vote should be classed as passed because of a debug widget
	#IF IS_DEBUG_BUILD
		// Code Development build should class vote as passed
		IF (NETWORK_IS_IN_CODE_DEVELOPMENT_MODE())
			NET_PRINT("...KGM MP...BWW...MP_VOTE_SYSTEM - DEBUG: Vote Has Passed [CODE DEVELOPMENT BUILD]") NET_NL()
			RETURN TRUE
		ENDIF

		// Check for the 'one player' widget
		IF (sCNCFlowVoteStruct.bAllowMissionsWithOnePlayer)
			NET_PRINT("...KGM MP...BWW...MP_VOTE_SYSTEM - DEBUG: Vote Has Passed [Allow Missions To Start With One Player]") NET_NL()
			RETURN TRUE
		ENDIF
	#ENDIF
	
	// Vote hasn't passed
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Check if a player has voted
///  
/// PARAMS:
///    aPlayer - Player to check
/// RETURNS:
///    TRUE if player has voted
FUNC BOOL Has_Player_Voted(PLAYER_INDEX aPlayer)

	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(aPlayer)
		IF GlobalplayerBD_FM_2[NATIVE_TO_INT(aPlayer)].bHasPlayerVoted
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is already in a specific tutorial session that should be maintained
//
// RETURN VALUE:		INT					Existing Tutorial Session (or TUTORIAL_SESSION_NOT_ACTIVE if none)
FUNC INT Get_Players_Existing_Tutorial_Session()
	RETURN (GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iFmTutorialSession)
ENDFUNC








