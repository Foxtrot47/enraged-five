USING "globals.sch"

#IF FEATURE_CASINO_HEIST
USING "ArcadeBusiness\arcade_office_safe_constants.sch"
USING "ArcadeBusiness\arcade_payout_constants.sch"
USING "cash_pile.sch"

/// PURPOSE: 
///    Holds data for running the arcade office safe
STRUCT ARCADE_OFFICE_SAFE_STRUCT
	CONTRABAND_TRANSACTION_STATE eCollectTransactionState
	PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE eTransactionStage
	ARCADE_OFFICE_SAFE_STATE_ENUM eState
	CASH_PILE_STRUCT sCashPile
	OBJECT_INDEX objSafeDoor
	OBJECT_INDEX objSafeBody
	BLIP_INDEX biSafeBlip
	PLAYER_INDEX iSafeOwnerID
	INTERIOR_INSTANCE_INDEX iInteriorID
	SCRIPT_TIMER sSafetyTimer
	INT iInitialSafeAmount = 0// Stores the safe amount at the start of a collect transaction, used only for telemetry
	INT iNetSyncSceneID = -1
	
	INT iContext
	INT iBS// ARCADE_OFFICE_SAFE_BITSET_ENUM
ENDSTRUCT

TYPEDEF PROC ARCADE_OFFICE_SAFE_FUNC(ARCADE_OFFICE_SAFE_STRUCT &sInst)

/// PURPOSE:
///    Default function pointer
PROC ARCADE_OFFICE_SAFE_FUNC_DEFAULT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	UNUSED_PARAMETER(sInst)
ENDPROC

#ENDIF // FEATURE_CASINO_HEIST
