USING "globals.sch"
USING "MP_globals_Tunables.sch"

#IF FEATURE_CASINO_HEIST

/// -------------------------- 
///    TIMING
/// -------------------------- 
  
TWEAK_INT _ARCADE_PAYOUT_INTERVAL_MS 		1000 * 60 * 48 // Play time between payouts (milliseconds)
CONST_INT _ARCADE_STAT_SYNC_INTERVAL_MS		1000 * 15 // How often to update the stat timer (milliseconds). We don't just update the stat every frame in order to save comp power
CONST_INT _ARCADE_TIME_BETWEEN_SAFE_ACCESS_SECONDS  	60 * 48// Player can only access the safe once per day
CONST_INT _ARCADE_MAX_PAYOUT_TEXT_MESSAGE	3

/// -------------------------- 
///    PAYOUT VALUES - Swapped to tunables
/// -------------------------- 

// Limits
FUNC INT ARCADE_PAYOUT_GET_MAX_PAYOUT_PER_DAY()
	RETURN g_sMPTunables.iMaxArcadePayoutPerDay
ENDFUNC

FUNC INT ARCADE_PAYOUT_GET_MAX_SAFE_STORAGE()
	RETURN g_sMPTunables.iMaxArcadeSafeStorage
ENDFUNC

// Cabinets
FUNC INT ARCADE_PAYOUT_GET_MIN_PAYOUT_PER_CABINET()
	RETURN g_sMPTunables.iMinPayoutPerCabinet
ENDFUNC

FUNC INT ARCADE_PAYOUT_GET_MAX_PAYOUT_PER_CABINET()
	RETURN g_sMPTunables.iMaxPayoutPerCabinet
ENDFUNC

FUNC INT ARCADE_PAYOUT_GET_CABINET_PAYOUT_INCREMENT()
	RETURN g_sMPTunables.iArcadeCabinetPayoutIncrement
ENDFUNC

// Bonuses
FUNC INT ARCADE_PAYOUT_GET_SCORE_SCREENS_PAYOUT_BONUS()
	RETURN g_sMPTunables.iArcadeScoreScreensPayoutBonus
ENDFUNC

FUNC INT ARCADE_PAYOUT_GET_NEON_FEATURE_PAYOUT_BONUS()
	RETURN g_sMPTunables.iArcadeNeonFeaturePayoutBonus
ENDFUNC

FUNC INT ARCADE_PAYOUT_GET_ALL_TROPHIES_PAYOUT_BONUS()
	RETURN g_sMPTunables.iArcadeAllTrophiesPayoutBonus
ENDFUNC

// Trophies
FUNC INT ARCADE_PAYOUT_GET_PAYOUT_PER_TROPHY()
	RETURN g_sMPTunables.iArcadePayoutPerTrophy
ENDFUNC

CONST_INT _ARCADE_PAYOUT_PER_TROPHY 50

#ENDIF // FEATURE_CASINO_HEIST
