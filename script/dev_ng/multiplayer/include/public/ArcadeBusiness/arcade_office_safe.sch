USING "globals.sch"

#IF FEATURE_CASINO_HEIST

USING "ArcadeBusiness\arcade_office_safe_struct.sch"
USING "ArcadeBusiness\arcade_payout.sch"

USING "net_include.sch"
USING "rc_helper_functions.sch"
USING "context_control_public.sch"
USING "lineactivation.sch"
USING "freemode_header.sch"
USING "cash_pile.sch"

///------------------------
///   DEBUG
///------------------------

#IF IS_DEBUG_BUILD
/// PURPOSE:
///    Prints out the current collect cash transaction result
DEBUGONLY PROC _ARCADE_OFFICE_SAFE__DEBUG_PRINT_COLLECT_TRANSACTION_RESULT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	SWITCH sInst.eCollectTransactionState
		CASE CONTRABAND_TRANSACTION_STATE_FAILED
			PRINTLN("[ARCADE_OFFICE_SAFE] MAINTAIN_TRANSACTIONS - remove contraband transaction failed")
		BREAK
		CASE CONTRABAND_TRANSACTION_STATE_SUCCESS
			PRINTLN("[ARCADE_OFFICE_SAFE] MAINTAIN_TRANSACTIONS - remove contraband transaction succeeded")	
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Prints the message at an interval with the arcade safe api prefix
DEBUGONLY PROC _ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT(STRING strText, INT iInterval = 120)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableArcadeSafeUpdatePrints")
		EXIT
	ENDIF
	#ENDIF
	
	IF GET_FRAME_COUNT() % iInterval = 0
		PRINTLN("[ARCADE_OFFICE_SAFE] ", strText)
	ENDIF
ENDPROC
#ENDIF

///------------------------
///    CASH CONFIG
///------------------------    

/// PURPOSE:
///    A lookup of the models of the cash in the safe
/// PARAMS:
///    iCashSlot - the index of the cash to get the model for
/// RETURNS:
///    The model of the cash at the given index	
FUNC MODEL_NAMES _ARCADE_OFFICE__GET_CASH_MODEL_NAME(INT iCashSlot)
	MODEL_NAMES eCashModel = DUMMY_MODEL_FOR_SCRIPT
	
	SWITCH iCashSlot
		CASE 0	eCashModel = PROP_ANIM_CASH_PILE_01	BREAK
		CASE 1	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 2	eCashModel = PROP_ANIM_CASH_NOTE  	BREAK
		CASE 3	eCashModel = PROP_ANIM_CASH_NOTE  	BREAK
		CASE 4	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 5	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 6	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 7	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 8	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 9	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 10	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 11	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 12	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 13	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 14	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 15	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 16	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 17	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 18	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 19	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 20	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 21	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 22	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 23	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 24	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 25	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 26	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 27	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 28	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 29	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 30	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 31	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 32	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 33	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 34	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 35	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 36	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 37	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 38	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 39	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 40	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 41	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 42	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 43	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 44	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 45	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 46	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
		CASE 47	eCashModel = PROP_ANIM_CASH_PILE_02	BREAK
	 ENDSWITCH
		
	RETURN eCashModel
ENDFUNC

/// PURPOSE:
///    A lookup of the positions of the cash in the safe
/// PARAMS:
///    iCashSlot - the index of the cash to get the positions for
/// RETURNS:
///    The position of the cash at the given index
FUNC VECTOR _ARCADE_OFFICE__GET_CASH_MODEL_POSITION(INT iCashIndex)
	SWITCH iCashIndex
		CASE 0	RETURN <<2729.02,-374.317,-47.6015>>
		CASE 1	RETURN <<2729.28,-374.261,-47.5928>>
		CASE 2	RETURN <<2728.92,-374.333,-47.608 >>
		CASE 3	RETURN <<2728.97,-374.511,-47.608 >>
		CASE 4	RETURN <<2729.28,-374.413,-47.5928>>
		CASE 5	RETURN <<2729.28,-374.566,-47.5928>>
		CASE 6	RETURN <<2729.2,-374.566,-47.5928 >>
		CASE 7	RETURN <<2729.2,-374.413,-47.5928 >>
		CASE 8	RETURN <<2729.2,-374.261,-47.5928 >>
		CASE 9	RETURN <<2729.12,-374.566,-47.5928>>
		CASE 10	RETURN <<2729.12,-374.413,-47.5928>>
		CASE 11	RETURN <<2729.12,-374.261,-47.5928>>
		CASE 12	RETURN <<2729.12,-374.566,-47.5499>>
		CASE 13	RETURN <<2729.12,-374.413,-47.5499>>
		CASE 14	RETURN <<2729.12,-374.261,-47.5499>>
		CASE 15	RETURN <<2729.2,-374.261,-47.5499 >>
		CASE 16	RETURN <<2729.28,-374.261,-47.5499>>
		CASE 17	RETURN <<2729.28,-374.413,-47.5499>>
		CASE 18	RETURN <<2729.2,-374.413,-47.5499 >>
		CASE 19	RETURN <<2729.28,-374.566,-47.5499>>
		CASE 20	RETURN <<2729.2,-374.566,-47.5499 >>
		CASE 21	RETURN <<2729.12,-374.413,-47.506 >>
		CASE 22	RETURN <<2729.12,-374.413,-47.4631>>
		CASE 23	RETURN <<2729.2,-374.413,-47.4631 >>
		CASE 24	RETURN <<2729.2,-374.413,-47.506  >>
		CASE 25	RETURN <<2729.28,-374.413,-47.506 >>
		CASE 26	RETURN <<2729.28,-374.566,-47.506 >>
		CASE 27	RETURN <<2729.2,-374.566,-47.506  >>
		CASE 28	RETURN <<2729.2,-374.261,-47.506  >>
		CASE 29	RETURN <<2729.28,-374.261,-47.506 >>
		CASE 30	RETURN <<2729.12,-374.261,-47.506 >>
		CASE 31	RETURN <<2729.12,-374.566,-47.4631>>
		CASE 32	RETURN <<2729.12,-374.261,-47.4631>>
		CASE 33	RETURN <<2729.2,-374.261,-47.4631 >>
		CASE 34	RETURN <<2729.28,-374.261,-47.4631>>
		CASE 35	RETURN <<2729.28,-374.413,-47.4631>>
		CASE 36	RETURN <<2729.12,-374.566,-47.506 >>
		CASE 37	RETURN <<2729.28,-374.566,-47.4631>>
		CASE 38	RETURN <<2729.2,-374.566,-47.4631 >>
		CASE 39	RETURN <<2729.28,-374.261,-47.4197>>
		CASE 40	RETURN <<2729.28,-374.413,-47.4197>>
		CASE 41	RETURN <<2729.28,-374.566,-47.4197>>
		CASE 42	RETURN <<2729.2,-374.566,-47.4197 >>
		CASE 43	RETURN <<2729.2,-374.413,-47.4197 >>
		CASE 44	RETURN <<2729.2,-374.261,-47.4197 >>
		CASE 45	RETURN <<2729.12,-374.566,-47.4197>>
		CASE 46	RETURN <<2729.12,-374.413,-47.4197>>
		CASE 47	RETURN <<2729.12,-374.261,-47.4197>>
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    A lookup of the rotations of the cash in the safe
/// PARAMS:
///    iCashSlot - the index of the cash to get the rotation for
/// RETURNS:
///    The rotation of the cash at the given index
FUNC VECTOR _ARCADE_OFFICE__GET_CASH_MODEL_ROTATION(INT iCashSlot)
	SWITCH iCashSlot
		CASE 0	RETURN <<0,0,0>>
		CASE 1	RETURN <<0,0,0>>
		CASE 2	RETURN <<0,0,-170>>
		CASE 3	RETURN <<0,0,160>>
		CASE 4	RETURN <<0,0,0>>
		CASE 5	RETURN <<0,0,0>>
		CASE 6	RETURN <<0,0,0>>
		CASE 7	RETURN <<0,0,0>>
		CASE 8	RETURN <<0,0,0>>
		CASE 9	RETURN <<0,0,0>>
		CASE 10	RETURN <<0,0,0>>
		CASE 11	RETURN <<0,0,0>>
		CASE 12	RETURN <<0,0,0>>
		CASE 13	RETURN <<0,0,0>>
		CASE 14	RETURN <<0,0,0>>
		CASE 15	RETURN <<0,0,0>>
		CASE 16	RETURN <<0,0,0>>
		CASE 17	RETURN <<0,0,0>>
		CASE 18	RETURN <<0,0,0>>
		CASE 19	RETURN <<0,0,0>>
		CASE 20	RETURN <<0,0,0>>
		CASE 21	RETURN <<0,0,0>>
		CASE 22	RETURN <<0,0,0>>
		CASE 23	RETURN <<0,0,0>>
		CASE 24	RETURN <<0,0,0>>
		CASE 25	RETURN <<0,0,0>>
		CASE 26	RETURN <<0,0,0>>
		CASE 27	RETURN <<0,0,0>>
		CASE 28	RETURN <<0,0,0>>
		CASE 29	RETURN <<0,0,0>>
		CASE 30	RETURN <<0,0,0>>
		CASE 31	RETURN <<0,0,0>>
		CASE 32	RETURN <<0,0,0>>
		CASE 33	RETURN <<0,0,0>>
		CASE 34	RETURN <<0,0,0>>
		CASE 35	RETURN <<0,0,0>>
		CASE 36	RETURN <<0,0,0>>
		CASE 37	RETURN <<0,0,0>>
		CASE 38	RETURN <<0,0,0>>
		CASE 39	RETURN <<0,0,0>>
		CASE 40	RETURN <<0,0,0>>
		CASE 41	RETURN <<0,0,0>>
		CASE 42	RETURN <<0,0,0>>
		CASE 43	RETURN <<0,0,0>>
		CASE 44	RETURN <<0,0,0>>
		CASE 45	RETURN <<0,0,0>>
		CASE 46	RETURN <<0,0,0>>
		CASE 47	RETURN <<0,0,0>>
 	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

///------------------------
///    ANIM
///------------------------

/// PURPOSE:
///    Gets the model name of the safe door model
/// RETURNS:
///    The model name of the safe door model
FUNC MODEL_NAMES _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_MODEL_NAME()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_arcade_safe_door"))
ENDFUNC

/// PURPOSE:
///    Gets the position of the safe door in the world
/// RETURNS:
///    The position of the safe door in the world
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_POSITION()
	RETURN <<2728.8481, -374.6315, -47.6312>>
ENDFUNC

/// PURPOSE:
///    Gets the rotation of the safe door in the world
/// RETURNS:
///    The rotation of the safe door in the world
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_ROTATION()
	RETURN <<0, 0,-90>>
ENDFUNC

/// PURPOSE:
///    Gets the safe door object index
/// RETURNS:
///    The safe door object index
FUNC OBJECT_INDEX _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_OBJECT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RETURN sInst.objSafeDoor
ENDFUNC

/// PURPOSE:
///    Gets the model name of the safe body model
/// RETURNS:
///    The model name of the safe body model
FUNC MODEL_NAMES _ARCADE_OFFICE_SAFE__GET_SAFE_BODY_MODEL_NAME()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_prop_ch_arcade_safe_body"))
ENDFUNC

/// PURPOSE:
///    Gets the world position of the safe body
/// RETURNS:
///    The world position of the safe body
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SAFE_BODY_POSITION()
	RETURN <<2728.8481, -374.6315, -47.6312>>
ENDFUNC

/// PURPOSE:
///    Gets the safe body object index from the world
///    This will cache the model for future use
/// RETURNS:
///    The safe body object index, NULL if could not grab body
FUNC OBJECT_INDEX _ARCADE_OFFICE_SAFE__GET_SAFE_BODY_OBJECT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF sInst.objSafeBody = NULL OR NOT DOES_ENTITY_EXIST(sInst.objSafeBody)
		sInst.objSafeBody = GET_CLOSEST_OBJECT_OF_TYPE(_ARCADE_OFFICE_SAFE__GET_SAFE_BODY_POSITION(), 4,
		_ARCADE_OFFICE_SAFE__GET_SAFE_BODY_MODEL_NAME(), FALSE, FALSE, FALSE)
		
		IF sInst.objSafeBody = NULL
			PRINTLN("[ARCADE_OFFICE_SAFE] GET_SAFE_BODY_OBJECT - failed to get safe object")
		ELSE
			PRINTLN("[ARCADE_OFFICE_SAFE] GET_SAFE_BODY_OBJECT - cached safe object")
		ENDIF		
	ENDIF
	
	RETURN sInst.objSafeBody
ENDFUNC

/// PURPOSE:
///    Gets the position of the safe body object
/// RETURNS:
///    The safe body position or 0,0,0 and asserts if the safe doesn't exist
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SAFE_BODY_OBJECT_POSITION(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	OBJECT_INDEX objSafeBody = _ARCADE_OFFICE_SAFE__GET_SAFE_BODY_OBJECT(sInst)
	IF NOT DOES_ENTITY_EXIST(objSafeBody)
		PRINTLN("[ARCADE_OFFICE_SAFE] GET_SAFE_BODY_OBJECT_POSITION - safe object doesn't exist")
		RETURN <<0,0,0>>
	ENDIF
	
	RETURN GET_ENTITY_COORDS(objSafeBody)
ENDFUNC

/// PURPOSE:
///    Gets the offset from the safe body to the middle of the safe
/// RETURNS:
///    The offset from the safe body to the middle of the safe
FUNC VECTOR _ARCADE_OFFICE__GET_SAFE_AVERAGE_POSITION_OFFSET_FROM_SAFE_BODY()
	RETURN <<0.242, 0.242, 0.242>>
ENDFUNC

/// PURPOSE:
///    Gets the general middle position of the safe in the world
/// RETURNS:
///    The general position of the safe in the world
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SAFE_AVERAGE_POSITION(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RETURN _ARCADE_OFFICE__GET_SAFE_AVERAGE_POSITION_OFFSET_FROM_SAFE_BODY() + 
	_ARCADE_OFFICE_SAFE__GET_SAFE_BODY_OBJECT_POSITION(sInst)
ENDFUNC

/// PURPOSE:
///    Gets the offset of the 'use safe' prompt trigger from the safe body
/// RETURNS:
///    The offset of the 'use safe' prompt trigger from the safe body
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_PROMPT_TRIGGER_OFFSET_FROM_SAFE_BODY()
	RETURN <<-0.308, 0.308, -0.308>>
ENDFUNC

/// PURPOSE:
///    Gets the position of the 'use safe' prompt trigger
/// RETURNS:
///     The position of the 'use safe' prompt trigger
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_PROMPT_TRIGGER_POSITION(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RETURN _ARCADE_OFFICE_SAFE__GET_PROMPT_TRIGGER_OFFSET_FROM_SAFE_BODY() + 
	_ARCADE_OFFICE_SAFE__GET_SAFE_BODY_OBJECT_POSITION(sInst)
ENDFUNC

/// PURPOSE:
///    Gets the size of the 'use safe' prompt trigger
/// RETURNS:
///    The size of the 'use safe' prompt trigger
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_PROMPT_TRIGGER_SIZE()
	RETURN <<0.600, 0.35, 1>>
ENDFUNC

/// PURPOSE:
///    Gets the position of the safe open/close animation sync scene
/// RETURNS:
///    The position of the safe open/close animation sync scene
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SYNC_SCENE_POSITION()
	RETURN <<2730.05, -380.785, -50.00>>
ENDFUNC

/// PURPOSE:
///    Gets the rotation of the safe open/close animation sync scene
/// RETURNS:
///    The rotation of the safe open/close animation sync scene
FUNC VECTOR _ARCADE_OFFICE_SAFE__GET_SYNC_SCENE_ROTATION()
	RETURN << -0.000, -0.000, -0.00 >>
ENDFUNC

/// PURPOSE:
///    Gets the safe animation dictionary for female characters
/// RETURNS:
///    The safe animation dictionary for female characters
FUNC STRING _ARCADE_OFFICE_SAFE__GET_FEMALE_ANIM_DICT()
	RETURN "anim_heist@arcade_property@arcade_safe_open@female@"
ENDFUNC

/// PURPOSE:
///    Gets the safe animation dictionary for male characters
/// RETURNS:
///    The safe animation dictionary for male characters
FUNC STRING _ARCADE_OFFICE_SAFE__GET_MALE_ANIM_DICT()
	RETURN "anim_heist@arcade_property@arcade_safe_open@male@"
ENDFUNC

/// PURPOSE:
///    Gets the safe animation dictionary according to the local player 
///    character gender and if they are wearing heels
/// RETURNS:
///    Female anim dict if the player is female and wearing heels, 
///    Male anim dict if the player is male or female and not wearing heels
FUNC STRING _ARCADE_OFFICE_SAFE__GET_ANIM_DICT()
	IF IS_PLAYER_FEMALE()
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
		RETURN _ARCADE_OFFICE_SAFE__GET_FEMALE_ANIM_DICT()
	ENDIF
	
	RETURN _ARCADE_OFFICE_SAFE__GET_MALE_ANIM_DICT()
ENDFUNC

/// PURPOSE:
///    Gets the open safe anim for the player
/// RETURNS:
///    The open safe anim for the player
FUNC STRING _ARCADE_OFFICE_SAFE__GET_PLAYER_OPEN_SAFE_ANIM()
	RETURN "safe_open"
ENDFUNC

/// PURPOSE:
///    Gets the close safe anim for the player
/// RETURNS:
///    The close safe anim for the player
FUNC STRING _ARCADE_OFFICE_SAFE__GET_PLAYER_CLOSE_SAFE_ANIM()
	RETURN "safe_close"
ENDFUNC

/// PURPOSE:
///    Gets the open safe anim for the safe door
/// RETURNS:
///    The open safe anim for the safe door
FUNC STRING _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_OPEN_ANIM()
	RETURN "safe_open_safedoor"
ENDFUNC

/// PURPOSE:
///    Gets the close safe anim for the safe door
/// RETURNS:
///    The close safe anim for the safe door
FUNC STRING _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_CLOSE_ANIM()
	RETURN "safe_close_safedoor"
ENDFUNC

/// PURPOSE:
///    Gets the player starting position and rotation for either the open anim or close anim
/// PARAMS:
///    vPos - returns the start position for the anim
///    vRot - returns the start rotation for the anim
///    bOpenAnim - TRUE: gets open anim, FALSE: gets close anim
PROC _ARCADE_OFFICE_SAFE__GET_PLAYER_SAFE_ANIM_START_POSITION(VECTOR &vPos, VECTOR &vRot, BOOL bOpenAnim)
	VECTOR vSafeScenePos = _ARCADE_OFFICE_SAFE__GET_SYNC_SCENE_POSITION()
	VECTOR vSafeSceneRot = _ARCADE_OFFICE_SAFE__GET_SYNC_SCENE_ROTATION()
	STRING strAnimDict = _ARCADE_OFFICE_SAFE__GET_ANIM_DICT()
	
	STRING strAnimClip = ""
	
	IF bOpenAnim
		strAnimClip = _ARCADE_OFFICE_SAFE__GET_PLAYER_OPEN_SAFE_ANIM()
	ELSE
		strAnimClip = _ARCADE_OFFICE_SAFE__GET_PLAYER_CLOSE_SAFE_ANIM()
	ENDIF
	
	vPos = GET_ANIM_INITIAL_OFFSET_POSITION(strAnimDict, strAnimClip, vSafeScenePos, vSafeSceneRot, 0.01)
	vRot = GET_ANIM_INITIAL_OFFSET_ROTATION(strAnimDict, strAnimClip, vSafeScenePos, vSafeSceneRot, 0.01)
ENDPROC

/// PURPOSE:
///    Loads the required anim dict according to the players gender
/// RETURNS:
///    TRUE is anim assets have loaded, ASSERTS id requested anim dict is null or empty
FUNC BOOL _ARCADE_OFFICE_SAFE__LOAD_ANIM_DICT()
	STRING strAnimDict = _ARCADE_OFFICE_SAFE__GET_ANIM_DICT()
	IF IS_STRING_NULL_OR_EMPTY(strAnimDict)
		ASSERTLN("[ARCADE_OFFICE_SAFE] LOAD_ANIM_DICT - ",
		"attempting to load empty or null anim dictionary")
		RETURN FALSE
	ENDIF
	
	REQUEST_ANIM_DICT(strAnimDict)	
	RETURN HAS_ANIM_DICT_LOADED(strAnimDict)
ENDFUNC

/// PURPOSE:
///    Removes the required anim dict according to the players gender
/// RETURNS:
///    TRUE is anim assets have loaded, ASSERTS id requested anim dict is null or empty 
PROC _ARCADE_OFFICE_SAFE__REMOVE_ANIM_DICT()
	STRING strAnimDict = _ARCADE_OFFICE_SAFE__GET_ANIM_DICT()
	IF IS_STRING_NULL_OR_EMPTY(strAnimDict)
		ASSERTLN("[ARCADE_OFFICE_SAFE] REMOVE_ANIM_DICT - ",
		"attempting to remove empty or null anim dictionary")
		EXIT
	ENDIF
	
	REMOVE_ANIM_DICT(strAnimDict)
ENDPROC

/// PURPOSE:
///    Checks if we can play an anim on the local player safely
/// RETURNS:
///    TRUE if its safe to play anim on the local player
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_SAFE_TO_PLAY_ANIM_ON_PLAYER()
	PED_INDEX piPlayerPed = GET_PLAYER_PED(PLAYER_ID())
	
	IF NOT DOES_ENTITY_EXIST(piPlayerPed)
		PRINTLN("[ARCADE_OFFICE_SAFE] IS_SAFE_TO_PLAY_ANIM_ON_PLAYER - player ped doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(piPlayerPed)
		PRINTLN("[ARCADE_OFFICE_SAFE] IS_SAFE_TO_PLAY_ANIM_ON_PLAYER - player ped is dead")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

/// PURPOSE:
///    Checks if we can play an anim on the safe door safely
/// RETURNS:
///    TRUE if its safe to play anim on the safe door
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_SAFE_TO_PLAY_ANIM_ON_SAFE(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	OBJECT_INDEX objSafe = _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_OBJECT(sInst)
	
	IF NOT DOES_ENTITY_EXIST(objSafe)
		PRINTLN("[ARCADE_OFFICE_SAFE] IS_SAFE_TO_PLAY_ANIM_ON_SAFE - safe doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_HAVE_DRAWABLE(objSafe)
		PRINTLN("[ARCADE_OFFICE_SAFE] IS_SAFE_TO_PLAY_ANIM_ON_SAFE - safe doesn't have drawable")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE	
ENDFUNC

/// PURPOSE:
///    Creates the animation sync scene for opening/closing the safe
PROC _ARCADE_OFFICE_SAFE__CREATE_SYNC_SCENE(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	sInst.iNetSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(_ARCADE_OFFICE_SAFE__GET_SYNC_SCENE_POSITION(),
	_ARCADE_OFFICE_SAFE__GET_SYNC_SCENE_ROTATION()) 
ENDPROC 

/// PURPOSE:
///    Plays an animation syncs scene adding both the player and the safe door
/// PARAMS:
///    strPlayerAnim - the animation the player should play 
///    strSafeAnim - the animation the safe should play 
/// RETURNS:
///    TRUE if animation played successfully
FUNC BOOL _ARCADE_OFFICE_SAFE__PLAY_ANIM(ARCADE_OFFICE_SAFE_STRUCT &sInst, STRING strPlayerAnim, STRING strSafeAnim)
	IF IS_STRING_NULL_OR_EMPTY(strPlayerAnim)
		PRINTLN("[ARCADE_OFFICE_SAFE] PLAY_SAFE_ANIM - empty player anim string passed")
		RETURN FALSE
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strSafeAnim)
		PRINTLN("[ARCADE_OFFICE_SAFE] PLAY_SAFE_ANIM - empty safe anim string passed")
		RETURN FALSE
	ENDIF
	
	// Will check that safe exists
	IF NOT _ARCADE_OFFICE_SAFE__IS_SAFE_TO_PLAY_ANIM_ON_SAFE(sInst)
		PRINTLN("[ARCADE_OFFICE_SAFE] PLAY_ANIM - not safe to play anim on safe")
		RETURN FALSE
	ENDIF
	
	// Will check that player exists
	IF NOT _ARCADE_OFFICE_SAFE__IS_SAFE_TO_PLAY_ANIM_ON_PLAYER()
		PRINTLN("[ARCADE_OFFICE_SAFE] PLAY_ANIM - not safe to play anim on player")
		RETURN FALSE
	ENDIF
	
	_ARCADE_OFFICE_SAFE__CREATE_SYNC_SCENE(sInst)
	STRING strAnimDict = _ARCADE_OFFICE_SAFE__GET_ANIM_DICT()
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(GET_PLAYER_PED(PLAYER_ID()), sInst.iNetSyncSceneID,
	strAnimDict, strPlayerAnim, WALK_BLEND_IN, WALK_BLEND_OUT, 
	SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
	
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_OBJECT(sInst), sInst.iNetSyncSceneID,
	strAnimDict, strSafeAnim, WALK_BLEND_IN, WALK_BLEND_OUT, 
	SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS)
	
	NETWORK_START_SYNCHRONISED_SCENE(sInst.iNetSyncSceneID)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the safe sync scene has finished
/// PARAMS:
///    fPhaseEndPoint - phase threshold to count sync scene as finished
/// RETURNS:
///    TRUE if the sync scene has finished or hasn't been started
FUNC BOOL _ARCADE_OFFICE_SAFE__HAS_SYNC_SCENE_FINISHED(ARCADE_OFFICE_SAFE_STRUCT &sInst, FLOAT fPhaseEndPoint = 0.9)
	INT iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sInst.iNetSyncSceneID)
	
	IF iSyncSceneID = -1
		PRINTLN("[ARCADE_OFFICE_SAFE] HAS_SYNC_SCENE_FINISHED - invalid local scene id")
		RETURN TRUE
	ENDIF
	
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAKOUT"))
		PRINTLN("[ARCADE_OFFICE_SAFE] HAS_SYNC_SCENE_FINISHED - breakout event")
		RETURN TRUE
	ENDIF
	
	RETURN GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID) >= fPhaseEndPoint 
ENDFUNC

/// PURPOSE:
///    Stops the safe sync scene
PROC _ARCADE_OFFICE_SAFE__STOP_SYNC_SCENE(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	INT iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sInst.iNetSyncSceneID)
	
	IF iSyncSceneID = -1
		PRINTLN("[ARCADE_OFFICE_SAFE] STOP_SYNC_SCENE - invalid local scene id")
		EXIT
	ENDIF
	
	NETWORK_STOP_SYNCHRONISED_SCENE(iSyncSceneID)
ENDPROC

/// PURPOSE:
///    Plays the open safe animation on the player and safe door
/// RETURNS:
///    TRUE if animation started successfully
FUNC BOOL _ARCADE_OFFICE_SAFE__PLAY_OPEN_SAFE_ANIM(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RETURN _ARCADE_OFFICE_SAFE__PLAY_ANIM(sInst, 
	_ARCADE_OFFICE_SAFE__GET_PLAYER_OPEN_SAFE_ANIM(),
	_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_OPEN_ANIM())
ENDFUNC

/// PURPOSE:
///    Plays the close safe animation on the player and safe door
/// RETURNS:
///    TRUE if animation started successfully
FUNC BOOL _ARCADE_OFFICE_SAFE__PLAY_CLOSE_SAFE_ANIM(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RETURN _ARCADE_OFFICE_SAFE__PLAY_ANIM(sInst, 
	_ARCADE_OFFICE_SAFE__GET_PLAYER_CLOSE_SAFE_ANIM(),
	_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_CLOSE_ANIM())
ENDFUNC

/// PURPOSE:
///    Checks if the player is in the prompt trigger area
/// RETURNS:
///    TRUE if the player is in the prompt trigger area
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_PLAYER_IN_PROMPT_AREA(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	PED_INDEX piPlayerPed = GET_PLAYER_PED(PLAYER_ID())
	
	IF NOT DOES_ENTITY_EXIST(piPlayerPed)
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_IN_PROMPT_AREA - player ped doesn't exist")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(piPlayerPed)
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_IN_PROMPT_AREA - player ped is dead")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	VECTOR vPromptPos = _ARCADE_OFFICE_SAFE__GET_PROMPT_TRIGGER_POSITION(sInst)
	
	IF IS_ENTITY_AT_COORD(piPlayerPed, 
	vPromptPos,	_ARCADE_OFFICE_SAFE__GET_PROMPT_TRIGGER_SIZE())
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_IN_PROMPT_AREA - in prompt area")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_IN_PROMPT_AREA - not in prompt area")
	#ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the player is facing in the safes general direction
/// RETURNS:
///    TRUE if the player is facing in the safes general direction
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_PLAYER_FACING_SAFE(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	PED_INDEX piPlayerPed = GET_PLAYER_PED(PLAYER_ID())
	
	IF NOT DOES_ENTITY_EXIST(piPlayerPed)
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_FACING_SAFE - player ped doesn't exist")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_DEAD(piPlayerPed)
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_FACING_SAFE - player ped is dead")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	VECTOR vSafePos = _ARCADE_OFFICE_SAFE__GET_SAFE_AVERAGE_POSITION(sInst)
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
	FLOAT fDirHeaing = GET_HEADING_BETWEEN_VECTORS_2D(vPlayerPos, vSafePos)	
	
	IF IS_HEADING_ACCEPTABLE_CORRECTED(fDirHeaing, fPlayerHeading, _ARCADE_OFFICE_SAFE_HEADING_REQUIREMENT)
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_FACING_SAFE - facing safe")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("IS_PLAYER_FACING_SAFE - NOT facing safe")
	#ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if we should show the safe interaction prompt
/// RETURNS:
///    TRUE if we should show the safe interaction prompt
FUNC BOOL _ARCADE_OFFICE_SAFE__SHOULD_SHOW_PROMPT(ARCADE_OFFICE_SAFE_STRUCT &sInst)	
	IF NOT DOES_PLAYER_OWN_AN_ARCADE_PROPERTY(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_SHOW_PROMPT - player does not own arcade")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_SHOW_PROMPT - interaction menu is open")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT _ARCADE_OFFICE_SAFE__IS_PLAYER_IN_PROMPT_AREA(sInst) 
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_SHOW_PROMPT - not in prompt area")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT _ARCADE_OFFICE_SAFE__IS_PLAYER_FACING_SAFE(sInst)
		#IF IS_DEBUG_BUILD
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_SHOW_PROMPT - not in facing safe")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_SHOW_PROMPT - true")
	#ENDIF
	RETURN TRUE
ENDFUNC

///--------------------------
///    STATE TRANSITIONING
///--------------------------

/// PURPOSE:
///   	Handles transitioning between states calling onEnter and on exit
/// PARAMS:
///    eNewState - state to transition to
///    onExit - on exit behaviour for current state
///    onEnter - on enter behaviour for next state
PROC _ARCADE_OFFICE_SAFE__TRANSITION_STATE(ARCADE_OFFICE_SAFE_STRUCT &sInst,
ARCADE_OFFICE_SAFE_STATE_ENUM eNewState, ARCADE_OFFICE_SAFE_FUNC onExit, ARCADE_OFFICE_SAFE_FUNC onEnter)
	PRINTLN("[ARCADE_OFFICE_SAFE] TRANSITION_STATE - ",
	_DEBUG_GET_ARCADE_OFFICE_SAFE_STATE_ENUM_AS_STRING(sInst.eState),
	" --> ", _DEBUG_GET_ARCADE_OFFICE_SAFE_STATE_ENUM_AS_STRING(eNewState))
	
	CALL onExit(sInst)
	sInst.eState = eNewState
	CALL onEnter(sInst)	
ENDPROC

/// PURPOSE:
///    Sets the current state
PROC _ARCADE_OFFICE_SAFE__SET_STATE(ARCADE_OFFICE_SAFE_STRUCT &sInst, ARCADE_OFFICE_SAFE_STATE_ENUM eNewState)
	PRINTLN("[ARCADE_OFFICE_SAFE] SET_STATE - ", _DEBUG_GET_ARCADE_OFFICE_SAFE_STATE_ENUM_AS_STRING(sInst.eState),
	" --> ", _DEBUG_GET_ARCADE_OFFICE_SAFE_STATE_ENUM_AS_STRING(eNewState))
	sInst.eState = eNewState
ENDPROC

/// PURPOSE:
///    Checks if the safe door is open
/// RETURNS:
///    TRUE if the safe door is open
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_OPEN(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF NOT DOES_ENTITY_EXIST(sInst.objSafeDoor)
		RETURN FALSE
	ENDIF
	
	/// If the player leaves with another player in the property the door will not be
	/// reset to closed so we need to check the rotation instead of just relying on state
	VECTOR vCurrentRot = GET_ENTITY_ROTATION(sInst.objSafeDoor)
	VECTOR vStartRot = _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_ROTATION()
	RETURN NOT ARE_VECTORS_ALMOST_EQUAL(vCurrentRot, vStartRot)
ENDFUNC

/// PURPOSE:
///    Checks if a cash collection transaction is pending
/// RETURNS:
///    TRUE if a cash collection transaction is pending
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_TRANSACTION_PENDING(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RETURN IS_BIT_SET_ENUM(sInst.iBS, AOS_BS_TRANSACTION_PENDING)
ENDFUNC

/// PURPOSE:
///  	Sets if a cash collection transaction is pending
PROC _ARCADE_OFFICE_SAFE__SET_IS_TRANSACTION_PENDING(ARCADE_OFFICE_SAFE_STRUCT &sInst, BOOL bPending)
	IF bPending
		SET_BIT_ENUM(sInst.iBS, AOS_BS_TRANSACTION_PENDING)
	ELSE
		CLEAR_BIT_ENUM(sInst.iBS, AOS_BS_TRANSACTION_PENDING)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the safe interaction prompt label depending on if the safe is
///    open or closed
/// RETURNS:
///    Safe open: ARCADE_SAFE_C, Safe closed: ARCADE_SAFE_O
FUNC STRING _ARCADE_OFFICE_SAFE__GET_PROMPT_LABEL(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	// Should give close safe prompt if safe is open
	IF _ARCADE_OFFICE_SAFE__IS_OPEN(sInst)
		RETURN "ARCADE_SAFE_C"
	ENDIF
	
	RETURN "ARCADE_SAFE_O"
ENDFUNC

/// PURPOSE:
///    Creates the safe blip according to cash stored
PROC _ARCADE_OFFICE_SAFE__CREATE_BLIP(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	// The safe must exist so we can add the blip at its position
	IF NOT DOES_ENTITY_EXIST(_ARCADE_OFFICE_SAFE__GET_SAFE_BODY_OBJECT(sInst))
		EXIT
	ENDIF
	
	ADD_BASIC_BLIP(sInst.biSafeBlip, _ARCADE_OFFICE_SAFE__GET_SAFE_AVERAGE_POSITION(sInst),
	RADAR_TRACE_FINANCIER_STRAND, "ARC_BLIP_SAFE", _ARCADE_OFFICE_SAFE_BLIP_SCALE, TRUE)
	
	IF NOT ARCADE_PAYOUT_IS_PROPERTY_SAFE_FULL()
		SET_BLIP_COLOUR(sInst.biSafeBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN))
	ELSE
		SET_BLIP_COLOUR(sInst.biSafeBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED))
	ENDIF
ENDPROC

/// PURPOSE:
///    Ensures blip colour reflects safe cash amount
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_BLIP_COLOR(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF GET_BLIP_COLOUR(sInst.biSafeBlip) = GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN)
		IF ARCADE_PAYOUT_IS_PROPERTY_SAFE_FULL()
			SET_BLIP_COLOUR(sInst.biSafeBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_RED))
		ENDIF
	ELSE
		IF NOT ARCADE_PAYOUT_IS_PROPERTY_SAFE_FULL()
			SET_BLIP_COLOUR(sInst.biSafeBlip, GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Makes sure the safe blip represents the cash amount stored
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_BLIP(ARCADE_OFFICE_SAFE_STRUCT &sInst)	
	IF ARCADE_PAYOUT_GET_CASH_STORED_IN_PROPERTY_SAFE() <= 0
	OR IS_LOCAL_PLAYER_PED_IN_ARCADE_ROOM(ARC_RM_BASEMENT)
	OR IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		SAFE_REMOVE_BLIP(sInst.biSafeBlip)
		EXIT
	ENDIF 
	
	IF NOT DOES_BLIP_EXIST(sInst.biSafeBlip)
		_ARCADE_OFFICE_SAFE__CREATE_BLIP(sInst)
	ELSE
		_ARCADE_OFFICE_SAFE__MAINTAIN_BLIP_COLOR(sInst)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the safe should be disabled
/// RETURNS:
///    TRUE if the safe should be disabled
FUNC BOOL _ARCADE_OFFICE_SAFE__IS_DISABLED()
	IF IS_SCREEN_FADING_OUT()
		#IF IS_DEBUG_BUILD		
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_DISABLE - disabling - screen fading out")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		#IF IS_DEBUG_BUILD	
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_DISABLE - disabling - screen faded out")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		#IF IS_DEBUG_BUILD	
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_DISABLE - disabling - interaction menu is open")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	PED_INDEX piLocalPlayerPed = PLAYER_PED_ID()
	
	IF IS_PED_INJURED(piLocalPlayerPed)
		#IF IS_DEBUG_BUILD	
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_DISABLE - disabling - player injured")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(piLocalPlayerPed)
		#IF IS_DEBUG_BUILD	
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_DISABLE - disabling - player doesn't exist")
		#ENDIF
		RETURN TRUE
	ENDIF

	IF IS_PLAYER_DANCING(PLAYER_ID())
		#IF IS_DEBUG_BUILD	
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("SHOULD_DISABLE - disabling - interaction menu is open")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///------------------------
///    STATE ON ENTERS
///------------------------

/// PURPOSE:
///    Prompt state on enter behaviour
PROC _ARCADE_OFFICE_SAFE__PROMPT_ON_ENTER(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RELEASE_CONTEXT_INTENTION(sInst.iContext)
	REGISTER_CONTEXT_INTENTION(sInst.iContext, CP_MAXIMUM_PRIORITY, _ARCADE_OFFICE_SAFE__GET_PROMPT_LABEL(sInst))
	PRINTLN("[ARCADE_OFFICE_SAFE] PROMPT_ON_ENTER - context created")
ENDPROC

/// PURPOSE:
///    Load assets on enter behaviour
PROC _ARCADE_OFFICE_SAFE__LOAD_ASSETS_ON_ENTER(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
	START_NET_TIMER(sInst.sSafetyTimer, TRUE)	
	DISABLE_INTERACTION_MENU()
	PRINTLN("[ARCADE_OFFICE_SAFE] LOAD_ASSETS_ON_ENTER - called")
ENDPROC

/// PURPOSE:
///    Starts the walk to anim start task so that the player move to the animation starting point
PROC _ARCADE_OFFICE_SAFE__START_WALK_TO_ANIM_START_TASK(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	VECTOR vPos = <<0, 0, 0>>
	VECTOR vRot = <<0, 0, 0>>
	_ARCADE_OFFICE_SAFE__GET_PLAYER_SAFE_ANIM_START_POSITION(vPos, vRot, NOT _ARCADE_OFFICE_SAFE__IS_OPEN(sInst))
	
	TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vPos,
	PEDMOVE_WALK, DEFAULT, vRot.z, _ARCADE_OFFICE_SAFE_WALK_TO_ANIM_START_RADIUS)
ENDPROC

/// PURPOSE:
///    Walk to anim start on enter behaviour
PROC _ARCADE_OFFICE_SAFE__WALK_TO_ANIM_START_ON_ENTER(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	START_NET_TIMER(sInst.sSafetyTimer, TRUE)
	_ARCADE_OFFICE_SAFE__START_WALK_TO_ANIM_START_TASK(sInst)
ENDPROC

/// PURPOSE:
///    Anim on enter behaviour
PROC _ARCADE_OFFICE_SAFE__ANIM_ON_ENTER(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF _ARCADE_OFFICE_SAFE__IS_OPEN(sInst)
		IF NOT _ARCADE_OFFICE_SAFE__PLAY_CLOSE_SAFE_ANIM(sInst)
			PRINTLN("[ARCADE_OFFICE_SAFE] ANIM_ON_ENTER - close anims failed to start")
		ENDIF
	ELSE
		IF NOT _ARCADE_OFFICE_SAFE__PLAY_OPEN_SAFE_ANIM(sInst)
			PRINTLN("[ARCADE_OFFICE_SAFE] ANIM_ON_ENTER - open anims failed to start")
		ENDIF
	ENDIF
	
	START_NET_TIMER(sInst.sSafetyTimer, TRUE)	
	PRINTLN("[ARCADE_OFFICE_SAFE] ANIM_ON_ENTER - called")
ENDPROC

///------------------------
///    STATE ON EXITS
///------------------------

/// PURPOSE:
///    Prompt on exit behaviour
PROC _ARCADE_OFFICE_SAFE__PROMPT_ON_EXIT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RELEASE_CONTEXT_INTENTION(sInst.iContext)
	PRINTLN("[ARCADE_OFFICE_SAFE] PROMPT_ON_EXIT - context released")
	PRINTLN("[ARCADE_OFFICE_SAFE] PROMPT_ON_EXIT - called")
ENDPROC

/// PURPOSE:
///    On exit when bailing from a state, makes sure control is re-enabled after exiting state
PROC _ARCADE_OFFICE_SAFE__BAIL_ON_EXIT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	RESET_NET_TIMER(sInst.sSafetyTimer)
ENDPROC

/// PURPOSE:
///    Resets the state safety timer 
PROC _ARCADE_OFFICE_SAFE__RESET_SAFETY_TIMER(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	RESET_NET_TIMER(sInst.sSafetyTimer)
ENDPROC

/// PURPOSE:
///    Anim on exit behaviour 
PROC _ARCADE_OFFICE_SAFE__ANIM_ON_EXIT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	_ARCADE_OFFICE_SAFE__STOP_SYNC_SCENE(sInst)
	_ARCADE_OFFICE_SAFE__REMOVE_ANIM_DICT()
	RESET_NET_TIMER(sInst.sSafetyTimer)
	ENABLE_INTERACTION_MENU()
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	PRINTLN("[ARCADE_OFFICE_SAFE] ANIM_ON_EXIT - called")
ENDPROC

///------------------------
///    STATE UPDATES
///------------------------

/// PURPOSE:
///    Updates the idle state waiting for the player to enter the prompt trigger
PROC _ARCADE_OFFICE_SAFE__UPDATE_IDLE(ARCADE_OFFICE_SAFE_STRUCT &sInst)	
	IF _ARCADE_OFFICE_SAFE__IS_DISABLED()
		EXIT
	ENDIF

	IF _ARCADE_OFFICE_SAFE__SHOULD_SHOW_PROMPT(sInst)
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_PROMPT,
		&ARCADE_OFFICE_SAFE_FUNC_DEFAULT, &_ARCADE_OFFICE_SAFE__PROMPT_ON_ENTER)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Updates the prompt state waiting for the player to interact with the safe
PROC _ARCADE_OFFICE_SAFE__UPDATE_PROMPT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF _ARCADE_OFFICE_SAFE__IS_DISABLED()
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE,
		&_ARCADE_OFFICE_SAFE__PROMPT_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
		EXIT
	ENDIF

	IF NOT _ARCADE_OFFICE_SAFE__SHOULD_SHOW_PROMPT(sInst)
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE,
		&_ARCADE_OFFICE_SAFE__PROMPT_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
		EXIT
	ENDIF	
	
	IF HAS_CONTEXT_BUTTON_TRIGGERED(sInst.iContext)
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_LOAD_ASSETS,
		&_ARCADE_OFFICE_SAFE__PROMPT_ON_EXIT, &_ARCADE_OFFICE_SAFE__LOAD_ASSETS_ON_ENTER)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the load assets state waiting for all required anims to finish loading
PROC _ARCADE_OFFICE_SAFE__UPDATE_LOAD_ASSETS(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF _ARCADE_OFFICE_SAFE__IS_DISABLED()
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE,
		&_ARCADE_OFFICE_SAFE__BAIL_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
		EXIT
	ENDIF
	
	IF _ARCADE_OFFICE_SAFE__LOAD_ANIM_DICT()
		IF NOT DOES_ENTITY_EXIST(_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_OBJECT(sInst))
			PRINTLN("[ARCADE_OFFICE_SAFE] UPDATE_LOAD_ASSETS - failed to get safe object, returning to idle")
			_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE,
			&_ARCADE_OFFICE_SAFE__BAIL_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
			EXIT
		ENDIF
	
		PRINTLN("[ARCADE_OFFICE_SAFE] UPDATE_LOAD_ASSETS - assets loaded")
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_WALK_TO_ANIM_START, 
		&_ARCADE_OFFICE_SAFE__RESET_SAFETY_TIMER, &_ARCADE_OFFICE_SAFE__WALK_TO_ANIM_START_ON_ENTER)
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sInst.sSafetyTimer, _ARCADE_OFFICE_SAFE_SAFETY_TIMER_DURATION, TRUE)
		PRINTLN("[ARCADE_OFFICE_SAFE] UPDATE_LOAD_ASSETS - state exceeded safety timer, transitioning")
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE, 
		&_ARCADE_OFFICE_SAFE__BAIL_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Updates the walk to anim start state waiting for the player to move to the starting position of the sync scene
PROC _ARCADE_OFFICE_SAFE__UPDATE_WALK_TO_ANIM_START(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	SCRIPTTASKSTATUS eMoveToStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
	
	IF eMoveToStatus = FINISHED_TASK
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_ANIM, 
		&_ARCADE_OFFICE_SAFE__RESET_SAFETY_TIMER, &_ARCADE_OFFICE_SAFE__ANIM_ON_ENTER)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sInst.sSafetyTimer, _ARCADE_OFFICE_SAFE_SAFETY_TIMER_DURATION, TRUE)
		PRINTLN("[ARCADE_OFFICE_SAFE] UPDATE_WALK_TO_ANIM_START - state exceeded safety timer, transitioning")
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE, 
		&_ARCADE_OFFICE_SAFE__BAIL_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the anim state waiting for the animation sync scene to finish
PROC _ARCADE_OFFICE_SAFE__UPDATE_ANIM(ARCADE_OFFICE_SAFE_STRUCT &sInst)	
	/// We do not bail on disable in this state because it would
	/// look weird if the animation was cut short
	
	IF _ARCADE_OFFICE_SAFE__HAS_SYNC_SCENE_FINISHED(sInst)
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE, 
		&_ARCADE_OFFICE_SAFE__ANIM_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
		EXIT
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sInst.sSafetyTimer, _ARCADE_OFFICE_SAFE_SAFETY_TIMER_DURATION, TRUE)
		PRINTLN("[ARCADE_OFFICE_SAFE] UPDATE_ANIM - state exceeded safety timer, transitioning")
		_ARCADE_OFFICE_SAFE__TRANSITION_STATE(sInst, AOS_IDLE, 
		&_ARCADE_OFFICE_SAFE__BAIL_ON_EXIT, &ARCADE_OFFICE_SAFE_FUNC_DEFAULT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the safe interaction state machine
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_SAFE_INTERACTION(ARCADE_OFFICE_SAFE_STRUCT &sInst)	
	SWITCH sInst.eState
		CASE AOS_IDLE				_ARCADE_OFFICE_SAFE__UPDATE_IDLE(sInst) 				BREAK
		CASE AOS_PROMPT				_ARCADE_OFFICE_SAFE__UPDATE_PROMPT(sInst)				BREAK
		CASE AOS_LOAD_ASSETS		_ARCADE_OFFICE_SAFE__UPDATE_LOAD_ASSETS(sInst) 			BREAK
		CASE AOS_WALK_TO_ANIM_START _ARCADE_OFFICE_SAFE__UPDATE_WALK_TO_ANIM_START(sInst)	BREAK
		CASE AOS_ANIM				_ARCADE_OFFICE_SAFE__UPDATE_ANIM(sInst) 				BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Maintains processing requested transactions
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_TRANSACTIONS(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF NOT _ARCADE_OFFICE_SAFE__IS_TRANSACTION_PENDING(sInst)
		EXIT
	ENDIF
	
	SWITCH sInst.eTransactionStage
		CASE PPTS_PENDING_MISSION
			IF PROCESS_TRANSACTION_MISSION_KEY_FOR_ARCADE_SAFE(sInst.eCollectTransactionState, FALSE)
				IF sInst.eCollectTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
					sInst.eTransactionStage = PPTS_PENDING_CONTRABAND
					sInst.eCollectTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
					PRINTLN("[ARCADE_OFFICE_SAFE] COLLECT TRANSACTION - MISSION KEY - SUCCESS")
				ELSE
					sInst.eTransactionStage = PPTS_NO_TRANSACTION
					sInst.eCollectTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
					PRINTLN("[ARCADE_OFFICE_SAFE] COLLECT TRANSACTION - MISSION KEY - FAILED")
				ENDIF
			ENDIF
		BREAK
		
		CASE PPTS_PENDING_CONTRABAND
			IF ARCADE_PAYOUT_PROCESS_TRANSACTION_COLLECT_CASH_FROM_ARCADE_SAFE(sInst.eCollectTransactionState, sInst.iInitialSafeAmount)	
				#IF IS_DEBUG_BUILD
				SWITCH sInst.eCollectTransactionState
					CASE CONTRABAND_TRANSACTION_STATE_FAILED
						PRINTLN("[ARCADE_OFFICE_SAFE] COLLECT TRANSACTION - CONTRABAND - FAILED")
					BREAK
					CASE CONTRABAND_TRANSACTION_STATE_SUCCESS
						PRINTLN("[ARCADE_OFFICE_SAFE] COLLECT TRANSACTION - CONTRABAND - SUCCESS")	
					BREAK
				ENDSWITCH
				#ENDIF
				
				_ARCADE_OFFICE_SAFE__SET_IS_TRANSACTION_PENDING(sInst, FALSE)
				sInst.eTransactionStage = PPTS_NO_TRANSACTION
				sInst.eCollectTransactionState = CONTRABAND_TRANSACTION_STATE_DEFAULT
				sInst.iInitialSafeAmount = 0
			ENDIF
		BREAK
		CASE PPTS_NO_TRANSACTION
			PRINTLN("[ARCADE_OFFICE_SAFE] COLLECT TRANSACTION - CONTRABAND - No transaction!")
			_ARCADE_OFFICE_SAFE__SET_IS_TRANSACTION_PENDING(sInst, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks if the player is safe to collect the cash
/// RETURNS:
///    TRUE if the player is safe to collect the cash
FUNC BOOL _ARCADE_OFFICE_SAFE__CAN_PLAYER_COLLECT_CASH()
	RETURN NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_INTERACTION_MENU_OPEN()
ENDFUNC

/// PURPOSE:
///    Gives the player the cash from the safe without using a transaction
PROC _ARCADE_OFFICE_SAFE__GIVE_PLAYER_SAFE_CASH()
	INT iSafeCash = GET_MP_INT_CHARACTER_STAT(MP_STAT_ARCADE_SAFE_CASH_VALUE)
	PRINTLN("[ARCADE_OFFICE_SAFE] GIVE_PLAYER_SAFE_CASH Not using server transactions : Giving player $", iSafeCash)
	
	NETWORK_EARN_ARCADE(iSafeCash, 1, iSafeCash, 0, 0)
	ARCADE_PAYOUT_ADD_CASH_TO_TOTAL_EARNINGS(iSafeCash)
	SET_MP_INT_CHARACTER_STAT(MP_STAT_ARCADE_SAFE_CASH_VALUE, 0)
	ARCADE_PAYOUT_SET_CASH_STORED_IN_PROPERTY_SAFE(0)	
	
	PLAY_SOUND_FRONTEND(-1, "WEAPON_PURCHASE", "HUD_AMMO_SHOP_SOUNDSET")
	REQUEST_SAVE(SSR_REASON_ARCADE_CASH, STAT_SAVETYPE_END_SHOPPING)
ENDPROC

/// PURPOSE:
///    Gives player cash or starts a cash transaction if the player enters the collect cash trigger 
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_CASH_COLLECTION_TRIGGER(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF NOT _ARCADE_OFFICE_SAFE__IS_OPEN(sInst)
		EXIT
	ENDIF
	
	IF ARCADE_PAYOUT_GET_CASH_STORED_IN_PROPERTY_SAFE() <= 0
		EXIT
	ENDIF
	
	IF NOT _ARCADE_OFFICE_SAFE__IS_PLAYER_IN_PROMPT_AREA(sInst)
		EXIT
	ENDIF
	
	// Don't allow collection until open anim has finished
	IF NOT _ARCADE_OFFICE_SAFE__HAS_SYNC_SCENE_FINISHED(sInst)
		EXIT
	ENDIF
	
	IF USE_SERVER_TRANSACTIONS()
		IF _ARCADE_OFFICE_SAFE__IS_TRANSACTION_PENDING(sInst)
			EXIT
		ENDIF
	
		PRINTLN("[ARCADE_OFFICE_SAFE] MAINTAIN_CASH_COLLECTION_TRIGGER - transaction triggered")
		_ARCADE_OFFICE_SAFE__SET_IS_TRANSACTION_PENDING(sInst, TRUE)
		sInst.eTransactionStage = PPTS_PENDING_MISSION
		sInst.iInitialSafeAmount = ARCADE_PAYOUT_GET_CASH_STORED_IN_PROPERTY_SAFE()// Hold on to safe amount for telemetry
	ELSE
		PRINTLN("[ARCADE_OFFICE_SAFE] MAINTAIN_CASH_COLLECTION_TRIGGER - paying player direct")
		_ARCADE_OFFICE_SAFE__GIVE_PLAYER_SAFE_CASH()
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates collecting cash from the safe
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_CASH_COLLECTION(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	_ARCADE_OFFICE_SAFE__MAINTAIN_CASH_COLLECTION_TRIGGER(sInst)

	IF  USE_SERVER_TRANSACTIONS()
		_ARCADE_OFFICE_SAFE__MAINTAIN_TRANSACTIONS(sInst)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates showing the help text that is shown only 3 times on entering the arcade.
///    This will be 3 times for the cash in safe help text and
///    3 times for the safe capacity reached help text
PROC _ARCADE_OFFICE_SAFE__MAINTAIN_ONE_TIME_HELP_TEXT(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF IS_BIT_SET_ENUM(sInst.iBS, AOS_BS_HELP_TEXT_SHOWN)
		EXIT
	ENDIF
	
	IF ARCADE_PAYOUT_IS_PROPERTY_SAFE_FULL()
		IF g_iArcadeSafeFullHelpTextCount >= _ARCADE_OFFICE_SAFE_MAX_HELP_TEXT_DISPLAYS
			EXIT
		ENDIF
		
		++g_iArcadeSafeFullHelpTextCount
		PRINT_HELP("ARCADE_PAY_ENTER_M")
		SET_BIT_ENUM(sInst.iBS, AOS_BS_HELP_TEXT_SHOWN)
		EXIT
	ENDIF
	
	IF ARCADE_PAYOUT_GET_CASH_STORED_IN_PROPERTY_SAFE() > 0
		IF g_iArcadeSafeHelpTextCount >= _ARCADE_OFFICE_SAFE_MAX_HELP_TEXT_DISPLAYS
			EXIT
		ENDIF
		
		++g_iArcadeSafeHelpTextCount
		PRINT_HELP("ARCADE_PAY_ENTER")
		SET_BIT_ENUM(sInst.iBS, AOS_BS_HELP_TEXT_SHOWN)
		EXIT
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the safe door if owned by the local client and they are the only one running the script
PROC _ARCADE_OFFICE_SAFE__CLEANUP_SAFE_DOOR(ARCADE_OFFICE_SAFE_STRUCT &sInst, NETWORK_INDEX &niSafeDoor)
	IF NOT DOES_ENTITY_EXIST(sInst.objSafeDoor)
		PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_SAFE_DOOR - Door does not exist")
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_SAFE_DOOR - NETWORK_IS_GAME_IN_PROGRESS false")
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NETWORK_HAS_CONTROL_OF_ENTITY(sInst.objSafeDoor)
			SAFE_DELETE_OBJECT(sInst.objSafeDoor)
		ENDIF
		PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_SAFE_DOOR - Not script host so deleting door")
		EXIT
	ENDIF
	
	IF NETWORK_GET_NUM_PARTICIPANTS() > 1
		PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_SAFE_DOOR - More than 1 script participant: ", NETWORK_GET_NUM_PARTICIPANTS())
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(sInst.objSafeDoor)
		PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_SAFE_DOOR - More than 1 script participant: ", NETWORK_GET_NUM_PARTICIPANTS())
		EXIT
	ENDIF

	SAFE_DELETE_OBJECT(sInst.objSafeDoor)
	DELETE_NET_ID(niSafeDoor)
	PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_SAFE_DOOR - Deleted safe door and its net ID")
ENDPROC

/// PURPOSE:
///    Cleans up the arcade office safe ensuring assets and blips are removed
PROC CLEANUP_ARCADE_OFFICE_SAFE(ARCADE_OFFICE_SAFE_STRUCT &sInst, NETWORK_INDEX &niSafeDoor)
	_ARCADE_OFFICE_SAFE__REMOVE_ANIM_DICT()
	_ARCADE_OFFICE_SAFE__SET_STATE(sInst, AOS_IDLE)
	_ARCADE_OFFICE_SAFE__STOP_SYNC_SCENE(sInst)
	_ARCADE_OFFICE_SAFE__CLEANUP_SAFE_DOOR(sInst, niSafeDoor)
	CLEANUP_CASH_PILE(sInst.sCashPile)
	RELEASE_CONTEXT_INTENTION(sInst.iContext)
	SAFE_REMOVE_BLIP(sInst.biSafeBlip)
	CLEAR_BIT_ENUM(sInst.iBS, AOS_BS_INITIALIZED)
	CLEAR_BIT_ENUM(sInst.iBS, AOS_BS_ORIGINAL_SAFE_DOOR_HIDDEN)	
	PRINTLN("[ARCADE_OFFICE_SAFE] CLEANUP_ARCADE_OFFICE_SAFE - Cleanup called")
ENDPROC

/// PURPOSE:
///    Hides the safe door created by the interior
FUNC BOOL _ARCADE_OFFICE_SAFE__HIDE_ORIGINAL_SAFE_DOOR(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF IS_BIT_SET_ENUM(sInst.iBS, AOS_BS_ORIGINAL_SAFE_DOOR_HIDDEN)
		RETURN TRUE
	ENDIF
	
	CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_POSITION(), 2,
		_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_MODEL_NAME(), TRUE)
	
	SET_BIT_ENUM(sInst.iBS, AOS_BS_ORIGINAL_SAFE_DOOR_HIDDEN)
	PRINTLN("[ARCADE_OFFICE_SAFE] HIDE_ORIGINAL_SAFE_DOOR - Hidden door")
	RETURN TRUE
ENDFUNC

FUNC BOOL _ARCADE_OFFICE_SAFE__CREATE_SAFE_DOOR(ARCADE_OFFICE_SAFE_STRUCT &sInst, NETWORK_INDEX &niSafeDoor)			
	// If door has been created bail
	IF NETWORK_DOES_NETWORK_ID_EXIST(niSafeDoor)
		IF NOT DOES_ENTITY_EXIST(sInst.objSafeDoor)
			sInst.objSafeDoor = NET_TO_OBJ(niSafeDoor)
		ENDIF
		
		RETURN TRUE
	ENDIF	
	
	MODEL_NAMES eModel =_ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_MODEL_NAME()
	REQUEST_MODEL(eModel)
	
	IF NOT HAS_MODEL_LOADED(eModel)
		PRINTLN("[ARCADE_OFFICE_SAFE] CREATE_SAFE_DOOR - Loading model")
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_REGISTER_MISSION_OBJECTS(1)
		RETURN FALSE
	ENDIF
		
	VECTOR vPos = _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_POSITION()
	VECTOR vRot = _ARCADE_OFFICE_SAFE__GET_SAFE_DOOR_ROTATION()
	
	sInst.objSafeDoor = CREATE_OBJECT_NO_OFFSET(eModel, vPos)
	
	IF DOES_ENTITY_EXIST(sInst.objSafeDoor)
		niSafeDoor = OBJ_TO_NET(sInst.objSafeDoor)
	ENDIF
	
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(sInst.objSafeDoor, TRUE)
	
	SET_ENTITY_DYNAMIC(sInst.objSafeDoor, FALSE)
	SET_ENTITY_HAS_GRAVITY(sInst.objSafeDoor, FALSE)
	SET_ENTITY_ROTATION(sInst.objSafeDoor, vRot)
	SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sInst.objSafeDoor, FALSE)
	
	IF sInst.iInteriorID != NULL
		FORCE_ROOM_FOR_ENTITY(sInst.objSafeDoor, sInst.iInteriorID, HASH("Mainw_RM"))
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
	PRINTLN("[ARCADE_OFFICE_SAFE] CREATE_SAFE_DOOR - Created Successfully")
	RETURN TRUE
ENDFUNC

PROC _ARCADE_OFFICE_SAFE__TAKE_CONTROL_OF_SAFE_DOOR(ARCADE_OFFICE_SAFE_STRUCT &sInst)
	IF DOES_ENTITY_EXIST(sInst.objSafeDoor)
	AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(sInst.objSafeDoor)
		NETWORK_REQUEST_CONTROL_OF_ENTITY(sInst.objSafeDoor)
	ENDIF
ENDPROC

/// PURPOSE:
///    Initializes the office safe
PROC INIT_ARCADE_OFFICE_SAFE(ARCADE_OFFICE_SAFE_STRUCT &sInst, PLAYER_INDEX iSafeOwnerID = NULL, INTERIOR_INSTANCE_INDEX iInteriorID = NULL)
	INIT_CASH_PILE(sInst.sCashPile,  
	&_ARCADE_OFFICE__GET_CASH_MODEL_POSITION,
	&_ARCADE_OFFICE__GET_CASH_MODEL_ROTATION,
	&_ARCADE_OFFICE__GET_CASH_MODEL_NAME)
	sInst.iInteriorID = iInteriorID
	sInst.iSafeOwnerID = iSafeOwnerID
	
	RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
	
	SET_BIT_ENUM(sInst.iBS, AOS_BS_INITIALIZED)
ENDPROC

FUNC BOOL _ARCADE_OFFICE_MAINTAIN_SAFE_ASSETS(ARCADE_OFFICE_SAFE_STRUCT &sInst, NETWORK_INDEX &niSafeDoor)
	IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) != HASH("Mainw_RM")
		#IF IS_DEBUG_BUILD	
		_ARCADE_OFFICE_SAFE__DEBUG_INTERVAL_PRINT("[ARCADE_OFFICE_SAFE] CLEANUP_ARCADE_OFFICE_SAFE - not in main room")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// Hide original door first
	_ARCADE_OFFICE_SAFE__HIDE_ORIGINAL_SAFE_DOOR(sInst)

	// If the host create a networked duplicate of the door
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF NOT _ARCADE_OFFICE_SAFE__CREATE_SAFE_DOOR(sInst, niSafeDoor)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Maintains interactions with the arcade safe 
PROC MAINTAIN_ARCADE_OFFICE_SAFE(ARCADE_OFFICE_SAFE_STRUCT &sInst, NETWORK_INDEX &niSafeDoor, BOOL bCanShowHelpText = FALSE)		
	IF sInst.iSafeOwnerID = PLAYER_ID()
		_ARCADE_OFFICE_SAFE__MAINTAIN_BLIP(sInst)
	ENDIF
	
	IF NOT _ARCADE_OFFICE_MAINTAIN_SAFE_ASSETS(sInst, niSafeDoor)
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sInst.objSafeDoor)
		EXIT
	ENDIF	
	
	// If only one player and they aren't the property owner take control of safe door
	IF NETWORK_GET_NUM_PARTICIPANTS() <= 1 
	AND sInst.iSafeOwnerID != PLAYER_ID()
		// Make sure the property owner control of the entity
		_ARCADE_OFFICE_SAFE__TAKE_CONTROL_OF_SAFE_DOOR(sInst)
	ENDIF
	
	// If not property owner they can't interact with the safe
	IF sInst.iSafeOwnerID != PLAYER_ID()
		EXIT
	ENDIF
	
	// If they are the property owner they should always control the safe door
	_ARCADE_OFFICE_SAFE__TAKE_CONTROL_OF_SAFE_DOOR(sInst)
	
	IF NOT IS_BIT_SET_ENUM(sInst.iBS, AOS_BS_INITIALIZED)
		ASSERTLN("[ARCADE_OFFICE_SAFE] MAINTAIN_ARCADE_OFFICE_SAFE - Office safe has not been initialized")
		EXIT
	ENDIF
	
	// Will handle removing blip if not in main room
	_ARCADE_OFFICE_SAFE__MAINTAIN_SAFE_INTERACTION(sInst)
	_ARCADE_OFFICE_SAFE__MAINTAIN_CASH_COLLECTION(sInst)
	
	IF bCanShowHelpText
		_ARCADE_OFFICE_SAFE__MAINTAIN_ONE_TIME_HELP_TEXT(sInst)
	ENDIF
	
	// Only create cash after door has first been opened
	IF sInst.eState >= AOS_WALK_TO_ANIM_START 
	OR _ARCADE_OFFICE_SAFE__IS_OPEN(sInst) 
		MAINTAIN_CASH_PILE(sInst.sCashPile, ARCADE_PAYOUT_GET_CASH_STORED_IN_PROPERTY_SAFE())
	ENDIF
ENDPROC

#ENDIF // FEATURE_CASINO_HEIST

