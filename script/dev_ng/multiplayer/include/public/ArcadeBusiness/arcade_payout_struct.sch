USING "globals.sch"

#IF FEATURE_CASINO_HEIST
USING "ArcadeBusiness\arcade_payout_constants.sch"

/// -------------------------- 
///   ENUMS
/// -------------------------- 

ENUM PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE
	PPTS_NO_TRANSACTION,
	PPTS_PENDING_MISSION,
	PPTS_PENDING_CONTRABAND
ENDENUM

/// PURPOSE: 
///    Bitsets used for aracade payout
ENUM PASSIVE_INCOME_PAYOUT_BIT_SET_ENUM
	PIP_BS_ARCADE_DISPLAY_SAFE_FULL_HELP_TEXT,
	PIP_BS_ARCADE_TEXT_MESSAGE_SENT,
	PIP_BS_FIXER_HQ_DISPLAY_SAFE_FULL_HELP_TEXT,
	PIP_BS_FIXER_HQ_TEXT_MESSAGE_SENT
ENDENUM

/// PURPOSE: 
///    Holds data used for running an passive payout instance
STRUCT PASSIVE_INCOME_PAYOUT_STRUCT
	INT iLastSyncTime
	INT iTimeSinceLastStatSync
	INT iTimeLeft	
	INT iTransactionValue
	CONTRABAND_TRANSACTION_STATE eTransactionResult
	PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE eTransactionStage// which transaction needs to be made
	
	#IF IS_DEBUG_BUILD
		BOOL bWidgetsCreated = FALSE
		BOOL bDebugTriggerPayout = FALSE
	#ENDIF
ENDSTRUCT

DEBUGONLY FUNC STRING _DEBUG_GET_PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE_AS_STRING(PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE eEnum)
	SWITCH eEnum
		CASE PPTS_NO_TRANSACTION		RETURN	"PPTS_NO_TRANSACTION"
		CASE PPTS_PENDING_MISSION		RETURN	"PPTS_PENDING_MISSION"
		CASE PPTS_PENDING_CONTRABAND	RETURN	"PPTS_PENDING_CONTRABAND"
	ENDSWITCH

	ASSERTLN("GET_PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the name of the bitset as a string for debug
/// RETURNS:
///    The name of the bitset, ASSERTS if name is missing
DEBUGONLY FUNC STRING _DEBUG_GET_PASSIVE_INCOME_PAYOUT_BIT_SET_ENUM_AS_STRING(PASSIVE_INCOME_PAYOUT_BIT_SET_ENUM eEnum)
	SWITCH eEnum
		CASE PIP_BS_ARCADE_DISPLAY_SAFE_FULL_HELP_TEXT		RETURN "PIP_BS_ARCADE_DISPLAY_SAFE_FULL_HELP_TEXT"
		CASE PIP_BS_ARCADE_TEXT_MESSAGE_SENT				RETURN "PIP_BS_ARCADE_TEXT_MESSAGE_SENT"
		CASE PIP_BS_FIXER_HQ_DISPLAY_SAFE_FULL_HELP_TEXT	RETURN "PIP_BS_FIXER_HQ_DISPLAY_SAFE_FULL_HELP_TEXT"
		CASE PIP_BS_FIXER_HQ_TEXT_MESSAGE_SENT				RETURN "PIP_BS_FIXER_HQ_TEXT_MESSAGE_SENT"
		
	ENDSWITCH

	ASSERTLN("GET_PASSIVE_INCOME_PAYOUT_BIT_SET_ENUM_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

///---------------------
///    GETTERS
///---------------------    

FUNC INT PASSIVE_INCOME_PAYOUT_GET_LAST_SYNC_TIME(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.iLastSyncTime
ENDFUNC

FUNC INT PASSIVE_INCOME_PAYOUT_GET_TIME_SINCE_LAST_STAT_SYNC(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.iTimeSinceLastStatSync
ENDFUNC

FUNC INT PASSIVE_INCOME_PAYOUT_GET_TIME_LEFT(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.iTimeLeft
ENDFUNC

FUNC INT PASSIVE_INCOME_PAYOUT_GET_TRANSACTION_VALUE(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.iTransactionValue
ENDFUNC

FUNC CONTRABAND_TRANSACTION_STATE PASSIVE_INCOME_PAYOUT_GET_TRANSACTION_RESULT(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.eTransactionResult
ENDFUNC

FUNC PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE PASSIVE_INCOME_PAYOUT_GET_TRANSACTION_STAGE(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.eTransactionStage
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC BOOL PASSIVE_INCOME_PAYOUT_GET_WIDGETS_CREATED(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.bWidgetsCreated
ENDFUNC

FUNC BOOL PASSIVE_INCOME_PAYOUT_GET_DEBUG_TRIGGER_PAYOUT(PASSIVE_INCOME_PAYOUT_STRUCT &sInst)
	RETURN sInst.bDebugTriggerPayout
ENDFUNC
#ENDIF

///-------------------
///    SETTERS
///-------------------

PROC PASSIVE_INCOME_PAYOUT_SET_LAST_SYNC_TIME(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, INT iLastSyncTime)
	sInst.iLastSyncTime = iLastSyncTime
ENDPROC

PROC PASSIVE_INCOME_PAYOUT_SET_TIME_SINCE_LAST_STAT_SYNC(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, INT iTimeSinceLastStatSync)
	sInst.iTimeSinceLastStatSync = iTimeSinceLastStatSync
ENDPROC

PROC PASSIVE_INCOME_PAYOUT_SET_TIME_LEFT(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, INT iTimeLeft)
	sInst.iTimeLeft = iTimeLeft
ENDPROC

PROC PASSIVE_INCOME_PAYOUT_SET_TRANSACTION_VALUE(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, INT iTransactionValue)
	sInst.iTransactionValue = iTransactionValue
ENDPROC

PROC PASSIVE_INCOME_PAYOUT_SET_TRANSACTION_RESULT(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, CONTRABAND_TRANSACTION_STATE eTransactionResult)
	sInst.eTransactionResult = eTransactionResult
ENDPROC

PROC PASSIVE_INCOME_PAYOUT_SET_TRANSACTION_STAGE(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, PASSIVE_INCOME_PAYOUT_TRANSACTION_STAGE eTransactionStage)
	sInst.eTransactionStage = eTransactionStage
ENDPROC

#IF IS_DEBUG_BUILD
PROC PASSIVE_INCOME_PAYOUT_SET_WIDGETS_CREATED(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, BOOL bWidgetsCreated)
	sInst.bWidgetsCreated = bWidgetsCreated
ENDPROC

PROC PASSIVE_INCOME_PAYOUT_SET_DEBUG_TRIGGER_PAYOUT(PASSIVE_INCOME_PAYOUT_STRUCT &sInst, BOOL bDebugTriggerPayout)
	sInst.bDebugTriggerPayout = bDebugTriggerPayout
ENDPROC
#ENDIF

#ENDIF // FEATURE_CASINO_HEIST
