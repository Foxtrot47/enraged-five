/* ===================================================================================== *\
		MISSION NAME	:	net_heists_scaleform_header.sc
		AUTHOR			:	Alastair Costley - 2013/12/03
		DESCRIPTION		:	Contains the set and update methods for dealing with the heist board scaleform.
\* ===================================================================================== */


USING "commands_graphics.sch"
USING "mp_globals_fm.sch"
USING "net_heists_public.sch"

//// See the wiki page for multiplayer heist board UI: http://rsgediwiki1/wiki/index.php/Scaleform_MP_Heist_UI



/* ======================================================================= *\
	HEIST SCALEFORM - ENUMS
\* ======================================================================= */

//==== Enums for placeholder Player Portraits ====
ENUM ePLACEHOLDER_PORTRAIT
	PORTRAIT_LEADER = 0,
	PORTRAIT_CHAR1,
	PORTRAIT_CHAR2,
	PORTRAIT_CHAR3
ENDENUM

//==== Enums for ROLE Icons ====
ENUM eROLE_ICON
	ROLE_NONE = 0,
	ROLE_LEADER,
	ROLE_HACKER,
	ROLE_DRIVER,
	ROLE_GUNMAN
ENDENUM

ENUM eHEIST_READY_STATUS
	STATUS_BLANK = 0,
	STATUS_READY = 6,
	STATUS_NOT_READY
ENDENUM

//==== Enums for TODO List Ticks ====
ENUM eTODO_LIST_TICKBOX
	NO_TICK = 0,
	TICK
ENDENUM


//==== Enums for the five highlightable items ====
ENUM eHIGHLIGHTED_ITEM
	HIGHLIGHT_PLAYER_NONE = -1,
	HIGHLIGHT_PLAYER_ONE = 0,
	HIGHLIGHT_PLAYER_TWO,
	HIGHLIGHT_PLAYER_THREE,
	HIGHLIGHT_PLAYER_FOUR,
	HIGHLIGHT_PLAYER_FIVE,
	HIGHLIGHT_PLAYER_SIX,
	HIGHLIGHT_PLAYER_SEVEN,
	HIGHLIGHT_PLAYER_EIGHT,
	HIGHLIGHT_HEIST_ITEM,
	
	HIGHLIGHT_TODO_LIST = 11
ENDENUM

ENUM eSUB_HIGHLIGHTED_BOARD_ITEM
	SUB_HIGHLIGHT_BOARD_NONE = -1,
	SUB_HIGHLIGHT_BOARD_ROLE = 0,
	SUB_HIGHLIGHT_BOARD_CUT,
	SUB_HIGHLIGHT_BOARD_STATUS,
	SUB_HIGHLIGHT_BOARD_GAMERTAG,
	SUB_HIGHLIGHT_BOARD_CODENAME
ENDENUM

ENUM eSUB_HIGHLIGHTED_MAP_ITEM
	SUB_HIGHLIGHT_MAP_NONE = -1,
	SUB_HIGHLIGHT_MAP_TASK_1 = 0,
	SUB_HIGHLIGHT_MAP_TASK_2,
	SUB_HIGHLIGHT_MAP_TASK_3,
	SUB_HIGHLIGHT_MAP_TASK_4,
	SUB_HIGHLIGHT_MAP_TASK_5,
	SUB_HIGHLIGHT_MAP_TASK_6,
	SUB_HIGHLIGHT_MAP_TASK_7,
	SUB_HIGHLIGHT_MAP_TASK_8,
	SUB_HIGHLIGHT_MAP_TASK_9,
	SUB_HIGHLIGHT_MAP_TASK_10,
	SUB_HIGHLIGHT_MAP_TASK_11,
	SUB_HIGHLIGHT_MAP_TASK_12,
	SUB_HIGHLIGHT_MAP_TASK_13,
	SUB_HIGHLIGHT_MAP_TASK_14,
	SUB_HIGHLIGHT_MAP_TASK_15,
	SUB_HIGHLIGHT_MAP_TASK_16,
	SUB_HIGHLIGHT_MAP_TASK_17
ENDENUM

STRUCT HEIST_HIGHLIGHTED_ITEM
	INT iCurrentHighlightedItem
	INT iSubHighlightedItem
	INT iLeftArrow
	INT iRightArrow
ENDSTRUCT
HEIST_HIGHLIGHTED_ITEM sHighlightedItem	

ENUM MOUSE_TODO_ITEMS_STATE
	MOUSE_TODO_ITEMS_STATE_NOT_READY,
	MOUSE_TODO_ITEMS_STATE_START,
	MOUSE_TODO_ITEMS_STATE_REQUEST_HEIGHT,
	MOUSE_TODO_ITEMS_STATE_GET_RETURN_VALUE,
	MOUSE_TODO_ITEMS_STATE_GET_NEXT_ITEM,
	MOUSE_TODO_ITEMS_STATE_COMPLETE
ENDENUM

// PC Mouse and keyboard
INT iTodoItemHeight[MAX_HEIST_TASKS]
INT iTodoItemPostItIndex[MAX_HEIST_TASKS]
INT iNumTodoItems = 0
INT iCurrentMouseTodoItem = 0
INT iCurrentMouseTodoItemWaitCount = 0
INT iMousePostItIndex = 0
SCALEFORM_RETURN_INDEX iMouseTodoScaleformReturnIndex


MOUSE_TODO_ITEMS_STATE eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_NOT_READY


/* ======================================================================= *\
	HEIST SCALEFORM - ACCESSOR FUNCTIONS
\* ======================================================================= */


FUNC BOOL IS_HIGHLIGHTED_ITEM_NEW(INT iHighlightedItem, INT iSubHighlightedItem, INT iLeftArrowVisible, INT iRightArrowVisible)
	IF sHighlightedItem.iCurrentHighlightedItem 	!= iHighlightedItem
	OR sHighlightedItem.iSubHighlightedItem 		!= iSubHighlightedItem
	OR sHighlightedItem.iLeftArrow 					!= iLeftArrowVisible
	OR sHighlightedItem.iRightArrow 				!= iRightArrowVisible
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SAVE_HIGHLIGHTED_ITEM(INT iHighlightedItem, INT iSubHighlightedItem, INT iLeftArrowVisible, INT iRightArrowVisible)
	sHighlightedItem.iCurrentHighlightedItem 	= iHighlightedItem
	sHighlightedItem.iSubHighlightedItem 		= iSubHighlightedItem
	sHighlightedItem.iLeftArrow 				= iLeftArrowVisible
	sHighlightedItem.iRightArrow 				= iRightArrowVisible
ENDPROC


/* ======================================================================= *\
	HEIST SCALEFORM - DATA CACHE
\* ======================================================================= */

/// PURPOSE:
///    Check if the heist member data that has been passed into scaleform is fresh or old.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_MEMBER_DATA_NEW(	INT iPlayersSlot, 		STRING sGamerTag, 
									INT iPlayerRespect, 	STRING sPlayerPortrait, 
									STRING sPlayerRole, 	INT iPlayerRoleIcon, 
									STRING sPlayerStatus, 	INT iPlayerStatusIcon, 
									INT iPlayerPercent, 	STRING sPlayerPseudonym,
									STRING sPlayerOutfit,	INT iRemainingSlots,
									INT iVoiceState,		INT iInvalidTeam)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_MEMBER_DATA_NEW - Checking if heist member data has changed...")
//	#ENDIF
		
	IF GET_HASH_KEY(sGamerTag) 			!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sGamerTag
	OR iPlayerRespect					!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerRespect
	OR GET_HASH_KEY(sPlayerPortrait)	!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerPortrait
	OR GET_HASH_KEY(sPlayerRole)		!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerRole
	OR iPlayerRoleIcon					!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerRoleIcon
	OR GET_HASH_KEY(sPlayerStatus)		!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerStatus
	OR iPlayerStatusIcon				!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerStatusIcon
	OR iPlayerPercent 					!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerPercent
	OR GET_HASH_KEY(sPlayerPseudonym) 	!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerPseudonym
	OR GET_HASH_KEY(sPlayerOutfit)		!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerOutfit
	OR iRemainingSlots					!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iRemainingSlots
	OR iVoiceState						!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iVoiceState
	OR iInvalidTeam						!= g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iInvalidTeam
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if the heist team list and totals data that has been passed into scaleform is fresh or old.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_TEAM_DATA_NEW(	STRING sRoleOne,	INT iRoleOneTotal,
									STRING sRoleTwo, 	INT iRoleTwoTotal, 
									STRING sRoleThree, 	INT iRoleThreeTotal, 
									STRING sRoleFour, 	INT iRoleFourTotal,
									INT iCurrentNumPlayers, INT iTotalNumPlayers)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_TEAM_DATA_NEW - Checking if heist team list and totals data has changed...")
//	#ENDIF
		
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleOne)
		IF GET_HASH_KEY(sRoleOne) 			!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleOne
		OR iRoleOneTotal					!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleOneTotal
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleTwo)
		IF GET_HASH_KEY(sRoleTwo) 			!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleTwo
		OR iRoleTwoTotal					!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleTwoTotal
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleThree)
		IF GET_HASH_KEY(sRoleThree) 		!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleThree
		OR iRoleThreeTotal					!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleThreeTotal
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleFour)
		IF GET_HASH_KEY(sRoleFour) 			!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleFour
		OR iRoleFourTotal					!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleFourTotal
			RETURN TRUE
		ENDIF
	ENDIF
		
	IF iCurrentNumPlayers				!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iCurrentNumPlayers
	OR iTotalNumPlayers					!= g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iTotalNumPlayers
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if the heist task list team selection data that has been passed into scaleform is fresh or old.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_TASK_DATA_NEW(INT iTeam)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_TASK_DATA_NEW - Checking if heist task list team selection data has changed...")
//	#ENDIF


	IF iTeam != g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSelectedTeam
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Check if the pic chart and cuts data that has been passed into scaleform is fresh or old.
/// RETURNS:
///    TRUE if data is different (new).
//FUNC BOOL IS_HEIST_PIE_CHART_DATA_NEW(INT iPotentialTake, INT iCosts, HEIST_CUTS_DATA sCutData)
FUNC BOOL IS_HEIST_PIE_CHART_DATA_NEW(INT iPotentialTake, HEIST_CUTS_DATA sCutData)

//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PIE_CHART_DATA_NEW - Checking if piechart and cuts data has changed...")
//	#ENDIF
	
	BOOL 	bCutsChanged = FALSE
	INT 	index
	
	FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
		IF sCutData.iCutPercent[index] != g_HeistPlanningClient.sHeistDataCache.sPieChartCached.sCutData.iCutPercent[index]
			bCutsChanged = TRUE
		ENDIF
	ENDFOR
	
	IF iPotentialTake 		!= g_HeistPlanningClient.sHeistDataCache.sPieChartCached.iPotentialTake
	//OR iCosts				!= g_HeistPlanningClient.sHeistDataCache.sPieChartCached.iCosts
	OR bCutsChanged
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if the highlight indexes that has been passed into scaleform are fresh or old.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_HIGHLIGHT_INDEX_NEW(INT iHighlightedItem, INT iSubHighlightedItem, INT iLeftArrowVisible, INT iRightArrowVisible)

//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_HIGHLIGHT_INDEX_NEW - Checking if piechart and cuts data has changed...")
//	#ENDIF

	IF iHighlightedItem 		!= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iHighlightedItem
	OR iSubHighlightedItem 		!= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iSubHighlightedItem
	OR iLeftArrowVisible 		!= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iLeftArrowVisible
	OR iRightArrowVisible 		!= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iRightArrowVisible
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Check if the heist pre-planning board data is fresh.
/// RETURNS:
///    TRUE if fresh (updated).
FUNC BOOL IS_HEIST_PRE_PLANNING_SLOT_DATA_NEW(	INT iItemSlot, 		STRING sAllImagesTXD, STRING sTitle, 		
												STRING sTextureForA, STRING sImageNameA, 
												STRING sTextureForB, STRING sImageNameB, 
												STRING sTextureForC, STRING sImageNameC, INT iChosenOption,
												BOOL bAvailable)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_DATA_NEW - Checking if pre-planning data has changed...")
//	#ENDIF
												
	IF GET_HASH_KEY(sAllImagesTXD) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sAllImagesTXD
	OR GET_HASH_KEY(sTitle) 			!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTitle
	OR GET_HASH_KEY(sTextureForA) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTextureForA
	OR GET_HASH_KEY(sImageNameA) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sImageNameA
	OR GET_HASH_KEY(sTextureForB) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTextureForB
	OR GET_HASH_KEY(sImageNameB) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sImageNameB
	OR GET_HASH_KEY(sTextureForC) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTextureForC
	OR GET_HASH_KEY(sImageNameC) 		!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sImageNameC
	OR iChosenOption					!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].iChosenOption
	OR bAvailable						!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].bAvailable
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC 


/// PURPOSE:
///    Check if the heist pre-planning board data exists in the cache
/// RETURNS:
///    TRUE if fresh (updated).
FUNC BOOL IS_HEIST_PRE_PLANNING_SLOT_DATA_SAVED(INT iItemSlot)
												
	IF g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].bHasData
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_DATA_SAVED - Data DOES exist, move to cleanup.")
		#ENDIF
	
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC 


/// PURPOSE:
///    Check if the heist pre-planning board data for the text field on the left side is fresh.
/// RETURNS:
///    TRUE if fresh (updated).
FUNC BOOL IS_HEIST_PRE_PLANNING_SLOT_LEFT_DATA_NEW(	INT iItemSlot, STRING sDescription)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_LEFT_DATA_NEW - Checking if pre-planning data has changed...")
//	#ENDIF
												
	IF GET_HASH_KEY(sDescription) 	!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sDescription
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_LEFT_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC 


/// PURPOSE:
///    Check if the heist pre-planning board data for the text field on the right side is fresh.
/// RETURNS:
///    TRUE if fresh (updated).
FUNC BOOL IS_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA_NEW(INT iItemSlot, STRING sParticipantLabel, 
													STRING sPlayer1, STRING sPlayer2, STRING sPlayer3, STRING sPlayer4)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA_NEW - Checking if pre-planning data has changed...")
//	#ENDIF
												
	IF GET_HASH_KEY(sParticipantLabel) 	!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sParticipantLabel
	OR GET_HASH_KEY(sPlayer1) 			!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer1
	OR GET_HASH_KEY(sPlayer2) 			!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer2
	OR GET_HASH_KEY(sPlayer3) 			!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer3
	OR GET_HASH_KEY(sPlayer4) 			!= g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer4
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC 


/// PURPOSE:
///    Check if the heist planning board data for the provided preview box is new.
/// RETURNS:
///    TRUE if new (updated).
FUNC BOOL IS_HEIST_PLANNING_PREVIEW_DATA_NEW(INT iSelectedWindow, STRING sTitleText, STRING sTexture)

	IF GET_HASH_KEY(sTitleText) 		!= g_HeistPlanningClient.sHeistDataCache.sPreviewCached[iSelectedWindow].sPreviewTitle
	OR GET_HASH_KEY(sTexture) 			!= g_HeistPlanningClient.sHeistDataCache.sPreviewCached[iSelectedWindow].sPreviewTexture
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_PLANNING_PREVIEW_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC 


/// PURPOSE:
///    Check if the heist planning board data to see if the button status has changed.
/// RETURNS:
///    TRUE if new (updated).
FUNC BOOL IS_HEIST_LAUNCH_BUTTON_ENABLED_DATA_NEW(BOOL bEnabled)

	IF bEnabled != g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonEnabled
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_LAUNCH_BUTTON_ENABLED_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Check if the heist planning board data to see if the button status has changed.
/// RETURNS:
///    TRUE if new (updated).
FUNC BOOL IS_HEIST_LAUNCH_BUTTON_HIGHLIGHT_DATA_NEW(BOOL bEnabled)

	IF bEnabled != g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonHighlighted
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_LAUNCH_BUTTON_HIGHLIGHTED_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Check if the heist member stats card data has changed.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_STAT_CARD_DATA_NEW(INT iPlayersSlot, STRING sPlayerHeadshot, STRING sPlayerRole)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_MEMBER_DATA_NEW - Checking if heist member data has changed...")
//	#ENDIF
		
	IF GET_HASH_KEY(sPlayerHeadshot) 			!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iPlayerHeadshot
	OR GET_HASH_KEY(sPlayerRole) 				!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iPlayerRole
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_STAT_CARD_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if the stats card medal data is new.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_STATS_CARD_MEDAL_DATA_NEW(	INT iPlayersSlot, 	INT iMedalSlot1, 
												INT iMedalSlot2, 	INT iMedalSlot3, 
												INT iMedalSlot4, 	INT iMedalSlot5)
			
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_MEMBER_DATA_NEW - Checking if heist member data has changed...")
//	#ENDIF
		
	IF iMedalSlot1 						!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot1
	OR iMedalSlot2 						!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot2
	OR iMedalSlot3 						!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot3
	OR iMedalSlot4 						!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot4
	OR iMedalSlot5 						!= g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot5

		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_STATS_CARD_MEDAL_DATA_NEW - Data has changed, moving to save.")
		#ENDIF

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if the heist member voice status is new.
/// RETURNS:
///    TRUE if data is different (new).
FUNC BOOL IS_HEIST_VOICE_STATE_DATA_NEW(INT iPlayersSlot, INT iVoiceState)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_VOICE_STATE_DATA_NEW - Checking if heist member data has changed...")
//	#ENDIF
		
	IF iVoiceState != g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iVoiceState

		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - IS_HEIST_VOICE_STATE_DATA_NEW - Data has changed, moving to save.")
		#ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




// ************************************* SAVE DATA ************************************* //

/// PURPOSE:
///    Save pic chart and cuts data to the data cache. 
//PROC SAVE_HEIST_PIE_CHART_DATA(INT iPotentialTake, INT iCosts, HEIST_CUTS_DATA sCutData)
PROC SAVE_HEIST_PIE_CHART_DATA(INT iPotentialTake, HEIST_CUTS_DATA sCutData)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_PIE_CHART_DATA - Saving new pie chart and cuts to data cache.")
	#ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sPieChartCached.iPotentialTake 	= iPotentialTake
//	g_HeistPlanningClient.sHeistDataCache.sPieChartCached.iCosts			= iCosts
	
	INT index
	FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
		g_HeistPlanningClient.sHeistDataCache.sPieChartCached.sCutData.iCutPercent[index] = sCutData.iCutPercent[index]
	ENDFOR

ENDPROC


/// PURPOSE:
///    Save team list and totals data to the data cache. 
PROC SAVE_HEIST_TEAM_DATA(	STRING sRoleOne,		INT iRoleOneTotal,
							STRING sRoleTwo, 		INT iRoleTwoTotal, 
							STRING sRoleThree, 		INT iRoleThreeTotal, 
							STRING sRoleFour, 		INT iRoleFourTotal,
							INT iCurrentNumPlayers, INT iTotalNumPlayers)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_TEAM_DATA - Saving new team list and totals to data cache.")
	#ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleOne)
		g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleOne			= GET_HASH_KEY(sRoleOne)
	ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleOneTotal			= iRoleOneTotal	
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleTwo)
		g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleTwo			= GET_HASH_KEY(sRoleTwo)
	ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleTwoTotal			= iRoleTwoTotal	
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleThree)
		g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleThree		= GET_HASH_KEY(sRoleThree)
	ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleThreeTotal		= iRoleThreeTotal		
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sRoleFour)
		g_HeistPlanningClient.sHeistDataCache.sTeamListCached.sRoleFour			= GET_HASH_KEY(sRoleFour)
	ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iRoleFourTotal		= iRoleFourTotal		
	g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iCurrentNumPlayers	= iCurrentNumPlayers
	g_HeistPlanningClient.sHeistDataCache.sTeamListCached.iTotalNumPlayers		= iTotalNumPlayers					
							
ENDPROC


/// PURPOSE:
///    Save heist currently selected team index to the data cache. 
PROC SAVE_HEIST_TASK_DATA(INT iTeam)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_TASK_DATA - Saving new selected team to data cache.")
	#ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSelectedTeam = iTeam
	
ENDPROC


/// PURPOSE:
///    Save heist member data to the data cache. 
PROC SAVE_HEIST_MEMBER_DATA(INT iPlayersSlot, 		STRING sGamerTag, 
							INT iPlayerRespect, 	STRING sPlayerPortrait, 
							STRING sPlayerRole, 	INT iPlayerRoleIcon, 
							STRING sPlayerStatus, 	INT iPlayerStatusIcon, 
							INT iPlayerPercent, 	STRING sPlayerPseudonym,
							STRING sPlayerOutfit,	INT iRemainingSlots,
							INT iVoiceState,		INT iInvalidTeam)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_MEMBER_DATA - Saving new member slot to data cache.")
	#ENDIF
							
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sGamerTag			= GET_HASH_KEY(sGamerTag) 
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerRespect	= iPlayerRespect
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerPortrait	= GET_HASH_KEY(sPlayerPortrait)
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerRole		= GET_HASH_KEY(sPlayerRole)
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerRoleIcon	= iPlayerRoleIcon
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerStatus		= GET_HASH_KEY(sPlayerStatus)
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerStatusIcon	= iPlayerStatusIcon
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iPlayerPercent	= iPlayerPercent
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerPseudonym	= GET_HASH_KEY(sPlayerPseudonym)		
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].sPlayerOutfit		= GET_HASH_KEY(sPlayerOutfit)	
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iRemainingSlots 	= iRemainingSlots
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iVoiceState		= iVoiceState
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iInvalidTeam		= iInvalidTeam
							
ENDPROC

/// PURPOSE:
///    Save highlight indexes to the data cache. 
PROC SAVE_HEIST_HIGHLIGHT_INDEX(INT iHighlightedItem, INT iSubHighlightedItem, INT iLeftArrowVisible, INT iRightArrowVisible)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_HIGHLIGHT_INDEX - Saving new highlight index to data cache.")
	#ENDIF
	
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iHighlightedItem		= iHighlightedItem
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iSubHighlightedItem	= iSubHighlightedItem
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iLeftArrowVisible 	= iLeftArrowVisible
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iRightArrowVisible	= iRightArrowVisible
	
ENDPROC


PROC SAVE_HEIST_PRE_PLANNING_SLOT_DATA(	INT iItemSlot, 		STRING sAllImagesTXD, STRING sTitle, 		
										STRING sTextureForA, STRING sImageNameA, 
										STRING sTextureForB, STRING sImageNameB, 
										STRING sTextureForC, STRING sImageNameC, INT iChosenOption,
										BOOL bAvailable)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_PRE_PLANNING_SLOT_DATA - Saving new pre-planning data to cache...")
	#ENDIF
												
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sAllImagesTXD	= GET_HASH_KEY(sAllImagesTXD)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTitle			= GET_HASH_KEY(sTitle)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTextureForA	= GET_HASH_KEY(sTextureForA)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sImageNameA		= GET_HASH_KEY(sImageNameA)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTextureForB	= GET_HASH_KEY(sTextureForB)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sImageNameB		= GET_HASH_KEY(sImageNameB)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sTextureForC	= GET_HASH_KEY(sTextureForC)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sImageNameC		= GET_HASH_KEY(sImageNameC)
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].iChosenOption	= iChosenOption
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].bAvailable		= bAvailable
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].bHasData		= TRUE

ENDPROC


PROC CLEAR_HEIST_PRE_PLANNING_SLOT_DATA(INT iItemSlot)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - CLEAR_HEIST_PRE_PLANNING_SLOT_DATA - Cleaning old pre-planning data from cache...")
	#ENDIF
												
	HEIST_PRE_PLAN_CACHED_DATA sTempStruct
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot] = sTempStruct

ENDPROC


PROC SAVE_HEIST_PRE_PLANNING_SLOT_LEFT_DATA(INT iItemSlot, STRING sDescription)
											
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_PRE_PLANNING_SLOT_LEFT_DATA - Saving new pre-planning data to cache...")
	#ENDIF
	
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sDescription			= GET_HASH_KEY(sDescription)		
											
ENDPROC


PROC SAVE_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA(INT iItemSlot, STRING sParticipantLabel, 
											STRING sPlayer1, STRING sPlayer2, STRING sPlayer3, STRING sPlayer4)
											
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA - Saving new pre-planning data to cache...")
	#ENDIF
	
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sParticipantLabel	= GET_HASH_KEY(sParticipantLabel)		
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer1			= GET_HASH_KEY(sPlayer1)		
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer2			= GET_HASH_KEY(sPlayer2)		
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer3			= GET_HASH_KEY(sPlayer3)		
	g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer4			= GET_HASH_KEY(sPlayer4)		
											
ENDPROC


PROC SAVE_HEIST_PLANNING_PREVIEW_DATA(INT iSelectedWindow, STRING sTitleText, STRING sTexture)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_PLANNING_PREVIEW_DATA - Saving new planning preview data to cache...")
	#ENDIF
		
	g_HeistPlanningClient.sHeistDataCache.sPreviewCached[iSelectedWindow].sPreviewTitle			= GET_HASH_KEY(sTitleText)		
	g_HeistPlanningClient.sHeistDataCache.sPreviewCached[iSelectedWindow].sPreviewTexture		= GET_HASH_KEY(sTexture)

ENDPROC


PROC SAVE_HEIST_LAUNCH_BUTTON_ENABLED_DATA(BOOL bEnabled)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_LAUNCH_BUTTON_ENABLED_DATA - Saving new launch button enabled data to cache...")
	#ENDIF

	g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonEnabled = bEnabled

ENDPROC

PROC SAVE_HEIST_LAUNCH_BUTTON_HIGHLIGHT_DATA(BOOL bHighlighted)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_LAUNCH_BUTTON_HIGHLIGHT_DATA - Saving new launch button highlight data to cache...")
	#ENDIF

	g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonHighlighted = bHighlighted

ENDPROC

PROC SAVE_HEIST_STAT_CARD_DATA(INT iPlayersSlot, STRING sPlayerHeadshot, STRING sPlayerRole)
		
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_STAT_CARD_DATA - Saving new launch button highlight data to cache...")
	#ENDIF
		
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iPlayerHeadshot = GET_HASH_KEY(sPlayerHeadshot) 
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iPlayerRole = GET_HASH_KEY(sPlayerRole)

ENDPROC

PROC SAVE_HEIST_STATS_CARD_MEDAL_DATA(	INT iPlayersSlot, 	INT iMedalSlot1, 
											INT iMedalSlot2, 	INT iMedalSlot3, 
											INT iMedalSlot4, 	INT iMedalSlot5)
			
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_STATS_CARD_MEDAL_DATA - Saving new player card medal data to cache...")
	#ENDIF
		
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot1 = iMedalSlot1
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot2 = iMedalSlot2
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot3 = iMedalSlot3
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot4 = iMedalSlot4
	g_HeistPlanningClient.sHeistDataCache.sStatsCard[iPlayersSlot].iMedalSlot5 = iMedalSlot5

ENDPROC


PROC SAVE_HEIST_VOICE_STATE_DATA(INT iPlayersSlot, INT iVoiceState)
		
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SAVE_HEIST_VOICE_STATE_DATA - Saving new voice state data to cache...")
	#ENDIF
		
	g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[iPlayersSlot].iVoiceState = iVoiceState

ENDPROC











/* ======================================================================= *\
	HEIST SCALEFORM - SET METHODS, USED ON INIT
\* ======================================================================= */


/****************** BOARD ******************/


/// PURPOSE:
///    Set up the headers for the heist planning board.
PROC SET_HEIST_PLANNING_LABELS(	SCALEFORM_INDEX sfBoardIndex, 
								STRING sTheLaunch,
								STRING sTheTake,
								STRING sThePlan,
								STRING sPieChartTotal,
								STRING sPieChartPlayerCut,
								STRING sPlayerRole,
								STRING sPlayerCut,
								STRING sPlayerStatus,
								STRING sPlayerOutfit)
									
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_LABELS - Setup initial heist planning labels called.")
//	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_LABELS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTheLaunch)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTheTake)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sThePlan)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPieChartTotal)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPieChartPlayerCut)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerRole)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerCut)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerStatus)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerOutfit)
	END_SCALEFORM_MOVIE_METHOD()
		
ENDPROC


/// PURPOSE:
///    Set the specific heist name - only call after SET_HEIST_PLANNING_LABELS has been done.
PROC SET_HEIST_PLANNING_NAME(	SCALEFORM_INDEX sfBoardIndex, 
								TEXT_LABEL_63 	tlHeistName)
								//TEXT_LABEL_63 	&tlHeistDesc[],
								//STRING 			sLeaderName)  // Removed as part of 1845891.
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_NAME - Setup heist text name and description.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_NAME ...  Name:	", tlHeistName)
	#ENDIF	
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_HEIST_NAME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlHeistName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HTITLE_DEF")
		
		// Print an array of text labels into a single string.
//		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_OFFLN_HD")
//			INT iLoop 
//			FOR iLoop = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
//				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlHeistDesc[iLoop])
//			ENDFOR			
//		END_TEXT_COMMAND_SCALEFORM_STRING()
		
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sLeaderName)  // Removed as part of 1845891.
		
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


/// PURPOSE:
///    Set the cost that the heist leader paid, so that all clients can see why the leader has 
///    a greater cut percentage than other members by defult.
PROC SET_HEIST_PLANNING_LEADER_COST(SCALEFORM_INDEX sfBoardIndex, 
									STRING 			sLabel,
									INT				iCashValue)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_LEADER_COST - Setup heist leader cost.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_LEADER_COST ...  sLabel:		", sLabel)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_LEADER_COST ...  iCashValue:	", iCashValue)
	#ENDIF	
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_LEADER_COST")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCashValue)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Set the specific heist name - only call after SET_HEIST_PLANNING_LABELS has been done.
PROC SET_HEIST_PRE_PLANNING_NAME(	SCALEFORM_INDEX sfBoardIndex, 
									TEXT_LABEL_63 tlHeistName, 
									STRING sHeistInfoLabel,
									STRING sHeistTeamLabel) 
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_NAME - Setup heist text name and headings/labels.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_NAME ...  tlHeistName:		", tlHeistName)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_NAME ...  sHeistInfoLabel: 	", sHeistInfoLabel)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_NAME ...  sHeistTeamLabel: 	", sHeistTeamLabel)
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_NAME - Set pre-planning name: ", tlHeistName)
	#ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_HEIST_NAME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlHeistName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sHeistInfoLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sHeistTeamLabel) 
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Updates the specific heist name - only call after SET_HEIST_PLANNING_LABELS has been done.
PROC UPDATE_HEIST_PRE_PLANNING_NAME(	SCALEFORM_INDEX sfBoardIndex, 
									TEXT_LABEL_63 tlHeistName, 
									STRING sHeistInfoLabel,
									STRING sHeistTeamLabel) 
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - UPDATE_HEIST_PRE_PLANNING_NAME - Setup heist text name and headings/labels.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - UPDATE_HEIST_PRE_PLANNING_NAME ...  tlHeistName:		", tlHeistName)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - UPDATE_HEIST_PRE_PLANNING_NAME ...  sHeistInfoLabel: 	", sHeistInfoLabel)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - UPDATE_HEIST_PRE_PLANNING_NAME ...  sHeistTeamLabel: 	", sHeistTeamLabel)
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - UPDATE_HEIST_PRE_PLANNING_NAME - Update pre-planning name: ", tlHeistName)
	#ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_HEIST_NAME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(tlHeistName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sHeistInfoLabel)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sHeistTeamLabel) 
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

/// PURPOSE:
///    Configure a player slot. Can be used at any time after initial setup. Also works to overwrite
///    existing players.
/// PARAMS:
///    sfBoardIndex 	- Planning board scaleform index.
///    iPlayerSlot 		- Player slot (1-8 - currently only 4).
///    sGamerTag 		- Text value for slot gamertag.
///    iPlayerRespect 	- Displayed in the player portrait section.
///    iPlayerPortrait 	- Player portrait.
///    sPlayerRole 		- Text label to represent role (e.g. Driver, Gunman, Leader).
///    iPlayerRoleIcon 	- Respective role icon (see eRoleIcon).
///    sPlayerStatus 	- Player current connection status (0=NOTREADY 1=READY)
///    fPlayerPercent 	- Percentage of the prixe pot for the slot.
///    iPlayerGangIcon 	- Shield icon.
///    sPlayerPseudonym	- Slot assigned codename.
PROC SET_HEIST_PLANNING_CREW_SLOT(	SCALEFORM_INDEX sfBoardIndex,
									INT 	iPlayerSlot,
									STRING	sGamerTag,
									INT 	iPlayerRespect,
									STRING	sPlayerPortrait,
									STRING	sPlayerRole,
									INT		iPlayerRoleIcon,
									STRING	sPlayerStatus,
									INT		iPlayerStatusIcon,
									INT		iPlayerPercent,
									STRING	sPlayerPseudonym,
									INT		iPlayerCutActual = 0)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_CREW_SLOT - Set the details for a player slot.")
	#ENDIF	
									
	// Calculate the monetary value from the slot percentage of the total kitty. This cuts
	// down on some extraneous data that was being passed in before.
	//INT iRemainingTotal = GlobalServerBD_HeistPlanning.iHeistTotalPay - GlobalServerBD_HeistPlanning.iHeistSetupCosts
	//INT iPaymentAmount = iPlayerPercent * (iRemainingTotal / 100)
	
	INT iPaymentAmount
	
	IF iPlayerCutActual = 0
		iPaymentAmount = iPlayerPercent * (GlobalServerBD_HeistPlanning.iHeistTotalPay / 100)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_CREW_SLOT - Setting cut actual amount using global data, value: ", iPaymentAmount)
		#ENDIF	
	ELSE
		iPaymentAmount = iPlayerCutActual
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_CREW_SLOT - Setting cut actual amount using passed in optional data, value: ", iPaymentAmount)
		#ENDIF	
	ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_CREW_MEMBER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sGamerTag)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerRespect)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sPlayerPortrait)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerRole)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerRoleIcon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerStatus)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStatusIcon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPaymentAmount)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerPercent)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerPseudonym)
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC

/// PURPOSE:
///    Setup a placeholder slot for a player. This is required for when the board is initially set up
///    for the player to be later slotted in.
/// PARAMS:
///    sfBoardIndex - INDEX planning board scaleform.
///    iPlayerSlot - INT slot number.
PROC SET_HEIST_PLANNING_CREW_SLOT_EMPTY(	SCALEFORM_INDEX sfBoardIndex, 
											INT iPlayerSlot)
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_CREW_SLOT_EMPTY - Set up a placeholder blank player slot.")
//	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_CREW_MEMBER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC


/// PURPOSE:
///    Set the team list, respective icons, and totals for the number of players conencred and total capacity.
PROC SET_HEIST_PLANNING_TEAM_LIST(	SCALEFORM_INDEX sfBoardIndex,
									TEXT_LABEL_15 	sRoleOne, 	INT iRoleOneTotal, 	INT iRoleOneMax,
									TEXT_LABEL_15	sRoleTwo, 	INT iRoleTwoTotal, 	INT iRoleTwoMax,
									TEXT_LABEL_15	sRoleThree, INT	iRoleThreeTotal,INT iRoleThreeMax,
									TEXT_LABEL_15 	sRoleFour,  INT	iRoleFourTotal, INT iRoleFourMax,
									INT		iCurrentNumPlayers, INT	iTotalNumPlayers)
									
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TEAM_LIST - Set the team list, respective icons and totals: ")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...sRoleOne 	/ iRoleOneTotal 	/ iRoleOneMax	: ", sRoleOne, ", ", iRoleOneTotal, ", ", iRoleOneMax)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...sRoleTwo 	/ iRoleTwoTotal 	/ iRoleTwoMax	: ", sRoleTwo, ", ", iRoleTwoTotal, ", ", iRoleTwoMax)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...sRoleThree 	/ iRoleThreeTotal 	/ iRoleThreeMax	: ", sRoleThree, ", ", iRoleThreeTotal, ", ", iRoleThreeMax)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...sRoleFour 	/ iRoleFourTotal 	/ iRoleFourMax	: ", sRoleFour, ", ", iRoleFourTotal, ", ", iRoleFourMax)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...iCurrentNumPlayers: ", iCurrentNumPlayers)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...iTotalNumPlayers: ", iTotalNumPlayers)
	#ENDIF	
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_TEAM_LIST")
	
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentNumPlayers)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalNumPlayers)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sRoleOne)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sRoleOne)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleOneTotal)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleOneMax)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sRoleTwo)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sRoleTwo)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleTwoTotal)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleTwoMax)	
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sRoleThree)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sRoleThree)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleThreeTotal)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleThreeMax)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sRoleFour)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sRoleFour)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleFourTotal)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRoleFourMax)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
		ENDIF
	
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Set the piechart to the values stored in ServerBD.
PROC SET_HEIST_PLANNING_PIECHART(	SCALEFORM_INDEX sfBoardIndex,
									INT iPotentialTake,
//									INT iCosts,
									HEIST_CUTS_DATA sCutData)
		
	INT index
		
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_PIECHART - Pie chart init data: ")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...iPotentialTake: ", iPotentialTake)
		//PRINTLN("[AMEC][HEIST_SCALEFORM] ...iCosts: ", iCosts)
		
		FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
			PRINTLN("[AMEC][HEIST_SCALEFORM] ...iPercentage[",index,"]: ", sCutData.iCutPercent[index])
		ENDFOR
		
	#ENDIF	
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_PIECHART")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPotentialTake)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCosts)
		
		// Because we're trying to make the interface look simple, the cuts are delt with in incrememnts of 5 between 0 and 100.
		// The 0 to 100% that the player sees is actually 0-(100 - cost)%. As an example, the cost might be 10%, this makes what the user
		// has between 0 and 90%, but displays as 0 to 100 for simplicity.
		
		FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
		// Stop any divide by zero errors, and make sure not to divide out the cost percentage by itself.
//			IF i < MAX_HEIST_PLAYER_SLOTS
//			AND sCutData.iCutPercent[i] > 0
//				INT iRemainingPercent = (100 - sCutData.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
//				FLOAT fAdjustedPercent = ((TO_FLOAT(sCutData.iCutPercent[i]) * TO_FLOAT(iRemainingPercent)) / TO_FLOAT(100))
//				sCutData.iCutPercent[i] = ROUND(fAdjustedPercent)
//			ENDIF
		
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCutData.iCutPercent[index])
		ENDFOR
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Works out the info needed for the mouse support for the TODO list to work.
///    Does it in an asynchronous way to avoid stalling the MP script while 
///    waiting for the scaleform to return values.
///    
/// PARAMS:
///    iTeam - Team to process
PROC PROCESS_MOUSE_TODO_ITEM_INFO( INT iTeam )

	IF NOT IS_PC_VERSION()
		EXIT
	ENDIF
	
	// Workaround for compile error
	IF iTodoItemPostItIndex[0] = 0
	OR iTodoItemHeight[0] = 0
	
	ENDIF
	
	// Team must be valid
	IF iTeam < 0
		CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_STATES - Team is invalid, exiting." )
		EXIT
	ENDIF

	iNumTodoItems = g_HeistPlanningClient.iVisibleRulesForTeam[iTeam]

	SWITCH eMouseTodoItemState
	
		// Startup state - items have never been initialised.
		CASE MOUSE_TODO_ITEMS_STATE_NOT_READY
		
			// Do nothing
		
		BREAK
		
		// Initialisation state
		CASE MOUSE_TODO_ITEMS_STATE_START
		
			CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - MOUSE_TODO_ITEMS_STATE_START - Rules to process = ", iNumTodoItems )
		
			iCurrentMouseTodoItem = 0
			eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_REQUEST_HEIGHT
			iMousePostItIndex = -1
		
		BREAK
		
		// Request height of TODO item from scaleform
		CASE MOUSE_TODO_ITEMS_STATE_REQUEST_HEIGHT
		
			CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - MOUSE_TODO_ITEMS_STATE_REQUEST_HEIGHT Item ", iCurrentMouseTodoItem )
		
			iCurrentMouseTodoItemWaitCount = 0
			iTodoItemHeight[iCurrentMouseTodoItem] = -1
			
			BEGIN_SCALEFORM_MOVIE_METHOD (g_HeistSharedClient.PlanningBoardIndex, "GET_TODO_ITEM_LINE_COUNT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentMouseTodoItem)
			iMouseTodoScaleformReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			
			eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_GET_RETURN_VALUE
		
		BREAK
		
		// Get the return value and store it
		CASE MOUSE_TODO_ITEMS_STATE_GET_RETURN_VALUE
		
			CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - STATE = MOUSE_TODO_ITEMS_STATE_GET_RETURN_VALUE - Item ", iCurrentMouseTodoItem)
	
			IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(iMouseTodoScaleformReturnIndex)
			
				iTodoItemHeight[iCurrentMouseTodoItem] = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(iMouseTodoScaleformReturnIndex)
				
				CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - Item ", iCurrentMouseTodoItem, " height = ", iTodoItemHeight[iCurrentMouseTodoItem] )
			
				// Also storing the indexes of the post it-notes so we know which ones to highlight when
				// we click mouse directly.4
				// Also allows us to work out which rule a post-it belongs to when clicking on them on the map.
				IF IS_BIT_SET(g_HeistPlanningClient.iTodoListBitset, iCurrentMouseTodoItem)
					++ iMousePostItIndex
					iTodoItemPostItIndex[iCurrentMouseTodoItem] = iMousePostItIndex
					CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - Rule ", iCurrentMouseTodoItem, " PostIt Index = ", iMousePostItIndex)
				ElSE
					iTodoItemPostItIndex[iCurrentMouseTodoItem] = iMousePostItIndex
					CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - Rule ", iCurrentMouseTodoItem, " NO POST-IT ASSIGNED - using ", iMousePostItIndex)
				ENDIF
				
				eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_GET_NEXT_ITEM

			ELSE
			
				CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - MOUSE_TODO_ITEMS_STATE_GET_RETURN_VALUE - Waiting frame ", iCurrentMouseTodoItemWaitCount)
				// Time out
				++ iCurrentMouseTodoItemWaitCount
				
				IF iCurrentMouseTodoItemWaitCount > 10
					CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - Rule ", iCurrentMouseTodoItem, " TIME OUT! SCALEFORM DID NOT RETURN VALUE IN TIME!")
					iTodoItemHeight[iCurrentMouseTodoItem] = -1
					iTodoItemPostItIndex[iCurrentMouseTodoItem] = -1
					eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_GET_NEXT_ITEM
				ENDIF
			
			ENDIF
			
		BREAK
		
		// Go to the next item
		CASE MOUSE_TODO_ITEMS_STATE_GET_NEXT_ITEM
		
				CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - STATE = MOUSE_TODO_ITEMS_STATE_GET_NEXT_ITEM")
				
				++ iCurrentMouseTodoItem
				
				IF iCurrentMouseTodoItem < iNumTodoItems
					eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_REQUEST_HEIGHT
				ELSE
					eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_COMPLETE
					CPRINTLN(DEBUG_INTERNET, "PROCESS_MOUSE_TODO_ITEM_INFO - STATE = MOUSE_TODO_ITEMS_STATE_COMPLETE")
				ENDIF
		
		BREAK
			
		
		CASE MOUSE_TODO_ITEMS_STATE_COMPLETE
		
			// Do nothing.
		
		BREAK
	
	
	ENDSWITCH


ENDPROC


/// PURPOSE:
///    Gets the height in lines of a todo item.
/// PARAMS:
///    iIndex - Index of the todo item
/// RETURNS:
///    The height of the item
///    
//FUNC INT GET_TODO_ITEM_HEIGHT( INT iIndex )
//
//	IF NOT IS_PC_VERSION()
//		RETURN 0
//	ENDIF	
//	
//	INT iHeight = -1
//	INT iCount = 0
//	
//	SCALEFORM_RETURN_INDEX iScaleformReturnIndex
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD (g_HeistSharedClient.PlanningBoardIndex, "GET_TODO_ITEM_LINE_COUNT")
//	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iIndex)
//		
//	iScaleformReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
//		
//	WHILE NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY (iScaleformReturnIndex)
//	AND iCount < 10
//		WAIT (0)
//		CPRINTLN(DEBUG_INTERNET, "GET_TODO_ITEM_HEIGHT - Waiting on scaleform return for item ", iIndex )
//		++iCount
//	ENDWHILE
//
//	IF iCount >= 10
//		SCRIPT_ASSERT("GET_TODO_ITEM_HEIGHT - Timeout waiting on scaleform return! Returning -1")
//		RETURN -1
//	ENDIF
//
//	iHeight = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT (iScaleformReturnIndex)
//	CPRINTLN(DEBUG_INTERNET, "GET_TODO_ITEM_HEIGHT - iHeight = ", iHeight)
//
//	RETURN iHeight
//
//ENDFUNC

/// PURPOSE:
///    Gets the height in lines of the todo items, so we can work out the mouse hitboxes.
/// PARAMS:
///    iVisibleRules - The number of visible rules.
//PROC STORE_TODO_ITEM_MOUSE_INFO(INT iVisibleRules)
//
//	IF NOT IS_PC_VERSION()
//		EXIT
//	ENDIF	
//	
//	iNumTodoItems = iVisibleRules // Store this to make it easier to get later.
//	
//	CPRINTLN(DEBUG_INTERNET, "STORE_TODO_ITEM_MOUSE_INFO - Started")
//	
//	INT iRule = 0
//	INT iPostItIndex = 0
//	
//	// Workaround for compile error
//	IF iTodoItemPostItIndex[iRule] = 0
//	OR iTodoItemHeight[iRule] = 0
//	
//	ENDIF
//
//	WHILE iRule < iNumTodoItems
//		
//		iTodoItemHeight[iRule] = GET_TODO_ITEM_HEIGHT(iRule)
//		CPRINTLN(DEBUG_INTERNET, "STORE_TODO_ITEM_MOUSE_INFO - Rule ", iRule, " height = ", iTodoItemHeight[iRule])
//		
//		IF iTodoItemPostItIndex[iRule] = 0
//		
//		ENDIF
//		
//		// Also storing the indexes of the post it-notes so we know which ones to highlight when
//		// we click mouse directly.
//		// Also allows us to work out which rule a post-it belongs to when clicking on them on the map.
//		IF IS_BIT_SET(g_HeistPlanningClient.iTodoListBitset, iRule)
//			iTodoItemPostItIndex[iRule] = iPostItIndex
//			CPRINTLN(DEBUG_INTERNET, "STORE_TODO_ITEM_MOUSE_INFO - Rule ", iRule, " PostIt Index = ", iPostItIndex)
//			++ iPostItIndex
//		ElSE
//			iTodoItemPostItIndex[iRule] = -1
//			CPRINTLN(DEBUG_INTERNET, "STORE_TODO_ITEM_MOUSE_INFO - Rule ", iRule, " NO POST-IT ASSIGNED")
//		ENDIF
//	
//		++ iRule
//			
//	ENDWHILE
//	
//	
//	CPRINTLN(DEBUG_INTERNET, "STORE_TODO_ITEM_MOUSE_INFO - Completed")
//
//ENDPROC


/// PURPOSE:
///    Set the task list.
PROC SET_HEIST_PLANNING_TASK_LIST(SCALEFORM_INDEX sfBoardIndex, INT iTeam)

	IF iTeam < 0
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - WARNING! No team value entered into set, leaving blank...")
		#ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_TODO_LIST")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		END_SCALEFORM_MOVIE_METHOD()
		
		EXIT
	ENDIF
	
	INT iRule, iVisibleRules
	TEXT_LABEL_3 tlPrefix
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_TODO_LIST")
	
		REPEAT MAX_HEIST_TASKS iRule
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_ON_PLANNING_BOARD)
			
				IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
//					#IF IS_DEBUG_BUILD
//						PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - ERROR! Rule string does not exist in Rockstar Cloud but is marked as show!")
//					#ENDIF
				ELSE
				
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetTwo[iRule], ciBS_RULE2_HIDE_ON_MAP)
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - Showing numbered rule for iVisibleRules: ", iVisibleRules)
						#ENDIF
						SET_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
						tlPrefix = "["
					ELSE
						CLEAR_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
						tlPrefix = "]"
					ENDIF
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - RULE ",iRule," TEXT: ",tlPrefix, g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
					#ENDIF
				
					// Ready the task description straight from the cloud.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BRIEF_STRING2")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPrefix)
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
					END_TEXT_COMMAND_SCALEFORM_STRING()
										
					//SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])	
					iVisibleRules++
				ENDIF
				
			ENDIF

		ENDREPEAT 
		
		//If it's a strand mission the add the strand mission rules
		IF g_sHeistStrandMissionRules.bStrandMissionForHeistBoard		
			//Loop all the strand mission rules
			REPEAT g_sHeistStrandMissionRules.iNumberOfObjectives[iTeam] iRule		
				IF iVisibleRules < MAX_HEIST_TASKS
					//If we're not skipping it
					IF NOT IS_BIT_SET(g_sHeistStrandMissionRules.iRuleBitset[iTeam][iRule], ciBS_RULE_HIDE_ON_PLANNING_BOARD)				
					
						//and the string is valid
						IF NOT IS_STRING_NULL_OR_EMPTY(g_sHeistStrandMissionRules.tl63Objective[iTeam][iRule])					
							
							IF NOT IS_BIT_SET(g_sHeistStrandMissionRules.iRuleBitSetTwo[iTeam][iRule], ciBS_RULE2_HIDE_ON_MAP)
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - STRAND - Showing numbered rule for iVisibleRules: ", iVisibleRules)
								#ENDIF
								SET_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
								tlPrefix = "["
							ELSE
								CLEAR_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
								tlPrefix = "]"
							ENDIF
							
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - STRAND - RULE ",iRule," TEXT: ",tlPrefix, g_sHeistStrandMissionRules.tl63Objective[iTeam][iRule])
							#ENDIF
							
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BRIEF_STRING2")
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPrefix)
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sHeistStrandMissionRules.tl63Objective[iTeam][iRule])
							END_TEXT_COMMAND_SCALEFORM_STRING()
							
							iVisibleRules++
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_TASK_LIST - STRAND - ERROR! Rule string does not exist in Rockstar Cloud but is marked as show!")
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT 
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
		
	g_HeistPlanningClient.iVisibleRulesForTeam[iTeam] = iVisibleRules
ENDPROC


/// PURPOSE:
///    Set the init of the player card. This must only be used against player 0 (aka the leader).
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iPlayerSlot - slot number.
///    sPlayerName - Gamertag.
///    iPlayerRank - Respect/rank level.
///    sPlayerRole - Role label.
///    sPlayerPortrait - portrait TXD.
///    bAccessPlane - Can access PLANES.
///    bAccessHeli - Can access HELICOPTERS.
///    bAccessBoat - Can access BOATS.
///    bAccessCar - Can access CARS.
///    fPlayerKillDeathRatio - String formatted K/D ratio.
///    sPlayerTitle - Player's title.
///    sPlayerRating - Good / Bad sport.
///    iPlayerStamina - Stamina stat.
///    iPlayerShooting - Shooting stat.
///    iPlayerStealth - Stealth stat.
///    iPlayerFlying - Flying stat.
///    iPlayerDriving - Driving stat.
///    iPlayerMentalState - Mental state int.
PROC SET_HEIST_PLANNING_PLAYER_CARD(SCALEFORM_INDEX sfBoardIndex,
									INT 	iPlayerSlot,
									STRING	sPlayerName,
									INT		iPlayerRank,
									STRING	tlPlayerRole,
									STRING	sPlayerPortrait,
									BOOL	bAccessPlane,
									BOOL	bAccessHeli,
									BOOL	bAccessBoat,
									BOOL	bAccessCar,
									FLOAT	fPlayerKillDeathRatio,
									STRING	sPlayerTitle,
									STRING	sPlayerRating,
									INT		iPlayerStamina,
									INT		iPlayerShooting,
									INT		iPlayerStealth,
									INT 	iPlayerFlying,
									INT		iPlayerDriving,
									INT		iPlayerMentalState)
	
	IF NOT IS_HEIST_STAT_CARD_DATA_NEW(iPlayerSlot, sPlayerPortrait, tlPlayerRole)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_PLAYER_CARD - Set the details for the player stats card.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... sPlayerPortrait: 						", sPlayerPortrait)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... Access: Plane/Heli/Boat/Car: 			", bAccessPlane, ",  ", bAccessHeli, ", ", bAccessBoat, ", ", bAccessCar)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerSlot 		& sPlayerName: 		", iPlayerSlot, ",  ", sPlayerName)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerRank 		& tlPlayerRole: 	", iPlayerRank, ",  ", tlPlayerRole)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerKDR			& sPlayerTitle: 	", fPlayerKillDeathRatio, ",  ", sPlayerTitle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... sPlayerRating 		& sPlayerStamina: 	", sPlayerRating, ",  ", iPlayerStamina)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerShooting 	& iPlayerStealth: 	", iPlayerShooting, ",  ", iPlayerStealth)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerFlying 		& iPlayerDriving: 	", iPlayerFlying, ",  ", iPlayerDriving)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerMentalState	: ", iPlayerMentalState)
	#ENDIF	
	
	SAVE_HEIST_STAT_CARD_DATA(iPlayerSlot, sPlayerPortrait, tlPlayerRole)
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_PLAYERCARD")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerRank)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlPlayerRole)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sPlayerPortrait)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessPlane)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessHeli)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessBoat)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessCar)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fPlayerKillDeathRatio)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerRating)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStamina)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerShooting)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStealth)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerFlying)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerDriving)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerMentalState)
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    Add the initial heist pre-planning choices to the board.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    sAllImagesTXD - TXD name for all images
///    sImageTexture1 - image 1 Texture name
///    iSelectedItem1 - Selected item for 1. 0, 1, or 2 corresponds to A, B, or C.
///    sImageTexture2 - image 2 Texture name
///    iSelectedItem2 - Selected item for 2. 0, 1, or 2 corresponds to A, B, or C.
///    sImageTexture3 - image 3 Texture name
///    iSelectedItem3 - Selected item for 3. 0, 1, or 2 corresponds to A, B, or C.
///    sImageTexture4 - image 4 Texture name
///    iSelectedItem4 - Selected item for 4. 0, 1, or 2 corresponds to A, B, or C.
///    sImageTexture5 - image 5 Texture name
///    iSelectedItem5 - Selected item for 5. 0, 1, or 2 corresponds to A, B, or C.
///    sImageTexture6 - image 6 Texture name
///    iSelectedItem6 - Selected item for 6. 0, 1, or 2 corresponds to A, B, or C.
PROC SET_HEIST_PRE_PLANNING_CHOICES(SCALEFORM_INDEX sfBoardIndex,
									STRING	sAllImagesTXD,
									STRING 	sImageTexture1,
									INT		iSelectedItem1,
									STRING 	sImageTexture2,
									INT		iSelectedItem2,
									STRING 	sImageTexture3,
									INT		iSelectedItem3,
									STRING 	sImageTexture4,
									INT		iSelectedItem4,
									STRING 	sImageTexture5,
									INT		iSelectedItem5,
									STRING 	sImageTexture6,
									INT		iSelectedItem6)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_CHOICES - Setup the pre-planning choices.")
	#ENDIF						
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_PLANNING_CHOICES")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sAllImagesTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sImageTexture1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedItem1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sImageTexture2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedItem2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sImageTexture3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedItem3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sImageTexture4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedItem4)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sImageTexture5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedItem5)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sImageTexture6)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedItem6)
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    Display up to three rows of planning images, up to three images per row.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iItemSlot - item Slot (0, 1, or 2).
///    sAllImagesTXD - TXD name for all images
///    sTitle - title (eg: PICK UP AN OUTFIT).
///    sTextureForA - Texture 1 - image for option A.
///    SImageNameA - image 1 name (eg: BUGSTARS).
///    sTextureForB - Texture 2 - image for option B.
///    SImageNameB - image 2 name (eg: POLICE)
///    sTextureForC - Texture 3 - image for option C.
///    SImageNameC - image 3 name (eg: SECURITY)
///    iChosenOption - chosen option - highlights a previously selected option by the player - 0, 1, or 3. Pass 99 for none chosen.
PROC SET_HEIST_PRE_PLANNING_SLOTS(	SCALEFORM_INDEX sfBoardIndex,
									INT 	iItemSlot,
									STRING	sAllImagesTXD,
									STRING	sTitle,
									STRING	sTextureForA,
									STRING	SImageNameA,
									STRING	sTextureForB,
									STRING	SImageNameB,
									STRING	sTextureForC,
									STRING	SImageNameC,
									INT		iChosenOption)
									
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_SLOTS - Setup the pre-planning slots.")
	#ENDIF			
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_SLOTS - Adding planning slot: ", iItemSlot, ", Title: ", sTitle)
	#ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_PLANNING_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iItemSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sAllImagesTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureForA)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(SImageNameA)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureForB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(SImageNameB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureForC)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(SImageNameC)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iChosenOption)
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    Build a frame for the board to populate.
PROC SET_HEIST_PRE_PLANNING_SLOT_EMPTY(SCALEFORM_INDEX sfBoardIndex)
															
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_SLOT_EMPTY - Set empty pre-planning slot.")
	#ENDIF
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_SLOT_EMPTY")
	#ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BLANK_PLANNING_SLOT")
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    This will set the display and focus, and is the end of initial setup. Only call once.
PROC SET_HEIST_PLANNING_INITIALISE(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_INITIALISE - Full planning board init has been called, initial setup complete.")
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "INITIALISE_HEISTBOARD")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    This will set the display and focus, and is the end of initial setup. Only call once.
PROC SET_HEIST_PRE_PLANNING_INITIALISE(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_INITIALISE - Pre-planning init has been called, initial pre-planning setup complete.")
	#ENDIF
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_INITIALISE - Pre-planning init called.")
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "INITIALISE_PLANNINGBOARD")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/****************** MAP ******************/

/// PURPOSE:
///    DEBUG - Toggle showing debug map ovelay.
//PROC SET_HEIST_MAP_DEBUG_SHOW(SCALEFORM_INDEX sfBoardIndex, BOOL bDoShow)
//
////	#IF IS_DEBUG_BUILD
////		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_DEBUG_SHOW - Show debug map method called.")
////	#ENDIF
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_DEBUG_MAP")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDoShow)
//	END_SCALEFORM_MOVIE_METHOD()
//	
//ENDPROC

/// PURPOSE:
///    DEBUG - Set the debug map overlay alpha.
/// PARAMS:
///    sfBoardIndex - Movie Index.
///    iAlpha - Colour Alpha percentage component (use 100 unless you want it semi-transparent)
//PROC SET_HEIST_MAP_DEBUG_ALPHA(SCALEFORM_INDEX sfBoardIndex, INT iAlpha)
//
////	#IF IS_DEBUG_BUILD
////		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_DEBUG_ALPHA - Setting debug map alpha.")
////	#ENDIF
//	
//	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_DEBUG_MAP_ALPHA")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
//	END_SCALEFORM_MOVIE_METHOD()
//	
//ENDPROC

/// PURPOSE:
///    Add a pin to the heist map. Only call ONCE (unless updating position).
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iPinEnum - Pin enum (from 0 onwards)
///    iPinType - Pin type (0 dot with outline, 1 simple dot)
///    iX, iY - World coords.
///    iRed, iGreen, iBlue - RGB colour componens.
///    iAlpha - Colour Alpha percentage component (use 100 unless you want it semi-transparent)
///    iScale - Scale percentage. Use 100 unless it looks too small.
PROC SET_HEIST_MAP_ADD_PIN(	SCALEFORM_INDEX sfBoardIndex, 
							INT iPinEnum,
							INT iPinType,
							FLOAT fX, FLOAT fY,
							INT iRed, INT iGreen, INT iBlue,
							INT iAlpha,
							INT iScale = 100)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_ADD_PIN - Adding pin to heist map. Enum: ", iPinEnum, ". Pin X,Y,Scale:", fX, ",", fX, ",", iScale)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_PIN")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPinEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPinType)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(fX))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ROUND(fY))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScale)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Set the colour of a specific pin on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie Index.
///    iPinEnum - Pin enum (from 0 onwards)
///    iRed, iGreen, iBlue - RGB colour components.
///    iAlpha - Colour Alpha percentage component.
PROC SET_HEIST_MAP_COLOUR_PIN(	SCALEFORM_INDEX sfBoardIndex,
								INT iPinEnum,
								INT iRed, INT iGreen, INT iBlue,
								INT iAlpha)

//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_COLOUR_PIN - Setting map pin colour:")
//		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Pin Enum:		", iPinEnum)
//		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Pin R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
//	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "COLOUR_PIN")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPinEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Add text to the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iTextEnum - Text enum to modify. 
///    sText - The string to display. You can use html line breaks.
///    iX, iY - World coords X & Y.
///    iAlpha - Alpha percentage of text. Stick to 100 unless you want the text semi-transparent.
///    iTextfieldAngle - Angle of textfield in degrees. Zero is normal. You can specify minus values.
///    iTextfieldWidth - Textfield Width in pixels relative to movie dimensions. Stick to 150 unless you want a wider textfield.
///    iFontSize - Font point size. Use 18 unless you need bigger or smaller text.
PROC SET_HEIST_MAP_ADD_TEXT(SCALEFORM_INDEX sfBoardIndex, 
							INT iTextEnum,
							STRING sText,
							FLOAT fX, FLOAT fY,
							INT iAlpha,
							INT iTextfieldAngle = 0,
							INT iTextfieldWidth = 150,
							INT iFontSize = 18)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_ADD_TEXT - Adding text to heist map:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Text Enum:			", iTextEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Text Value:			", sText)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Text X,Y:			", fX, ",", fY)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Angle/Width/Size:	", iTextfieldAngle, ", ", iTextfieldWidth, ", ", iFontSize)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTextEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sText)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTextfieldAngle)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iFontSize)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTextfieldWidth)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Add a post it note to the map containing the task list.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iTextEnum - Text enum to modify. 
///    iTaskNumber - The number of the task/rule to display
///    iX, iY - World coords X & Y.
PROC SET_HEIST_MAP_ADD_POSTIT(	SCALEFORM_INDEX sfBoardIndex, 
								INT iPostitEnum,
								INT iTaskNumber,
								FLOAT fX, FLOAT fY)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_ADD_POSTIT - Adding postit to heist map:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Postit Enum:	", iPostitEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Int Value:		", iTaskNumber)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		PostIt X,Y:		", fX, ", ", fY)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_POSTIT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPostitEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTaskNumber)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
ENDPROC


/// PURPOSE:
///    This lets you add big marker-style highlight circles in the same way you add pins.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iHighlightEnum - Highlight enum (from 0 onwards - can use the same enums as pins or text as they are all seperate).
///    fX - start X position in world coords
///    fY - start Y position in world coords
///    iWorldSize - size world meters (avoid <1000 as the image breaks up when really small)
///    iRed, iGreen, iBlue, iAlpha - Red, Green Blue colours & alpha transparency.
PROC SET_HEIST_MAP_ADD_HIGHLIGHT(	SCALEFORM_INDEX sfBoardIndex, 
									INT iHighlightEnum,
									FLOAT fX, FLOAT fY,
									INT iWorldSize,
									INT iRed, INT iGreen, INT iBlue,
									INT iAlpha)
									
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_ADD_HIGHLIGHT - Adding highlight to heist map:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Highlight Enum:	", iHighlightEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Loc: X,Y:		", fX, ",", fY)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		iWorldSize:		", iWorldSize)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Col: R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_HIGHLIGHT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlightEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iWorldSize)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

ENDPROC


/// PURPOSE:
///    This will add a circular area to the map at the world coordinates you specify.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iAreaEnum - Area enum (from 0 onwards - can use the same enums as pins or text as they are all seperate).
///    fX - start X position in world coords
///    fY - start Y position in world coords
///    fWorldDiameter - Diameter of the area in world units
///    iRed, iGreen, iBlue, iAlpha - Red, Green Blue colours & alpha transparency.
PROC SET_HEIST_MAP_ADD_AREA(	SCALEFORM_INDEX sfBoardIndex, 
								INT iAreaEnum,
								FLOAT fX, FLOAT fY,
								FLOAT fWorldDiameter,
								INT iRed, INT iGreen, INT iBlue,
								INT iAlpha)
									
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_ADD_AREA - Adding area to heist map:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Area Enum:		", iAreaEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Loc: X,Y:		", fX, ",", fY)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		fWorldDiameter:	", fWorldDiameter)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Col: R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_AREA")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAreaEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fX)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fY)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fWorldDiameter)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Set the colour of text on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iTextEnum - Text enum to modify.
///    iRed, iGreen, iBlue, iAlpha - RGB value plus Alpha transparency. 
PROC SET_HEIST_MAP_COLOUR_TEXT(	SCALEFORM_INDEX sfBoardIndex,
								INT iTextEnum,
								INT iRed, INT iGreen, INT iBlue,
								INT iAlpha)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_COLOUR_TEXT - Setting map text colour:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Text Enum:		", iTextEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Text R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "COLOUR_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTextEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Set the colour of a PostIt on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iPostItEnum - Text enum to modify.
///    iRed, iGreen, iBlue, iAlpha - RGB value plus Alpha transparency. 
PROC SET_HEIST_MAP_COLOUR_POSTIT(	SCALEFORM_INDEX sfBoardIndex,
									INT iPostItEnum,
									INT iRed, INT iGreen, INT iBlue,
									INT iAlpha)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_COLOUR_POSTIT - Setting map post-it colour:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		PostIt Enum:		", iPostItEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		PostIt R,G,B,A:		", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "COLOUR_POSTIT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPostItEnum)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Set the colour of an arrow on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iArrowEnum - Arrow enum to modify.
///    iRed, iGreen, iBlue, iAlpha - RGB value plus Alpha transparency. 
PROC SET_HEIST_MAP_COLOUR_ARROW(SCALEFORM_INDEX sfBoardIndex,
								INT iArrowEnum,
								INT iRed, INT iGreen, INT iBlue,
								INT iAlpha)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_COLOUR_ARROW - Setting map arrow colour:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Arrow Enum:		", iArrowEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Arrow R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "COLOUR_ARROW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArrowEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Set the colour of an area on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iAreaEnum - Area enum to modify.
///    iRed, iGreen, iBlue, iAlpha - RGB value plus Alpha transparency. 
PROC SET_HEIST_MAP_COLOUR_AREA(	SCALEFORM_INDEX sfBoardIndex,
								INT iAreaEnum,
								INT iRed, INT iGreen, INT iBlue,
								INT iAlpha)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_COLOUR_AREA - Setting map area colour:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Area Enum:		", iAreaEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Area R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "COLOUR_AREA")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAreaEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Set the colour of a highlight on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iHighlightEnum - Highlight enum to modify.
///    iRed, iGreen, iBlue, iAlpha - RGB value plus Alpha transparency. 
PROC SET_HEIST_MAP_COLOUR_HIGHLIGHT(SCALEFORM_INDEX sfBoardIndex,
									INT iHighlightEnum,
									INT iRed, INT iGreen, INT iBlue,
									INT iAlpha)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_COLOUR_HIGHLIGHT - Setting map highlight colour:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Highlight Enum:		", iHighlightEnum)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...		Highlight R,G,B,A:	", iRed, ",", iGreen, ",", iBlue, ",", iAlpha)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "COLOUR_HIGHLIGHT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlightEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRed)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iGreen)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iBlue)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAlpha)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Set the colour of the launch button on the heist planning board. (UNTESTED PROC)
/// PARAMS:
///    sfBoardIndex - Movie index
///    colour - Colour to set to HUD_COLOURS enum
PROC SET_HEIST_LAUNCH_BUTTON_COLOUR(SCALEFORM_INDEX sfBoardIndex, HUD_COLOURS colour)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_LAUNCH_BUTTON_COLOUR - Setting launch button colour to:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... Colour: ", colour)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_LAUNCH_BUTTON_COLOUR")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(colour))
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

/// PURPOSE:
///    Remove a specific pin from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iPinEnum - Specific pin enum.
PROC SET_HEIST_MAP_REMOVE_PIN(	SCALEFORM_INDEX sfBoardIndex, INT iPinEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_PIN - Remove map pin. Enum: ", iPinEnum)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_PIN")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPinEnum)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

/// PURPOSE:
///    Remove all pins on the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC SET_HEIST_MAP_REMOVE_ALL_PINS(	SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ALL_PINS - Remove all pins.")
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ALL_PINS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF	
ENDPROC


/// PURPOSE:
///    Remove specific tex from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iTextEnum - Text enum to remove.
PROC SET_HEIST_MAP_REMOVE_TEXT(	SCALEFORM_INDEX sfBoardIndex, INT iTextEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_TEXT - Remove map text. Enum: ", iTextEnum)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_TEXT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTextEnum)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove specific post-it from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iPostItEnum - Post-it enum to remove.
PROC SET_HEIST_MAP_REMOVE_POSTIT(	SCALEFORM_INDEX sfBoardIndex, INT iPostItEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_POSTIT - Remove map post-it. Enum: ", iPostItEnum)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_POSTIT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPostItEnum)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove specific tex from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iTextEnum - Text enum to remove.
PROC SET_HEIST_MAP_REMOVE_ALL_POSTITS(	SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ALL_POSTITS - Remove all map post-its.")
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ALL_POSTITS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove all text from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC SET_HEIST_MAP_REMOVE_ALL_TEXT(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ALL_TEXT - Removing all map text.")
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ALL_TEXT")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove a single arrow from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC SET_HEIST_MAP_REMOVE_ARROW(SCALEFORM_INDEX sfBoardIndex, INT iArrowEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ARROW - Removing singular arrow: ", iArrowEnum)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ARROW")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArrowEnum)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove all arrows from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC SET_HEIST_MAP_REMOVE_ALL_ARROWS(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ALL_ARROWS - Removing all map arrows.")
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ALL_ARROWS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove a single area from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iAreaEnum - Area to remove.
PROC SET_HEIST_MAP_REMOVE_AREA(SCALEFORM_INDEX sfBoardIndex, INT iAreaEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_AREA - Removing singular area: ", iAreaEnum)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_AREA")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAreaEnum)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove all areas from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC SET_HEIST_MAP_REMOVE_ALL_AREAS(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ALL_AREAS - Removing all map areas.")
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ALL_AREAS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove a single highlight from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iHighlightEnum - Highlight to remove.
PROC SET_HEIST_MAP_REMOVE_HIGHLIGHT(SCALEFORM_INDEX sfBoardIndex, INT iHighlightEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_HIGHLIGHT - Removing singular highlight: ", iHighlightEnum)
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_HIGHLIGHT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlightEnum)
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Remove all highlights from the heist map.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC SET_HEIST_MAP_REMOVE_ALL_HIGHLIGHTS(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_REMOVE_ALL_HIGHLIGHTS - Removing all highlight areas.")
	#ENDIF
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "REMOVE_ALL_HIGHLIGHTS")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


/// PURPOSE:
///    Bring the specified map pin to the top layer.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC BRING_HEIST_MAP_PIN_TO_FRONT(SCALEFORM_INDEX sfBoardIndex, INT iPinEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - BRING_HEIST_MAP_PIN_TO_FRONT - Bringing pin to front. Value: ", iPinEnum)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BRING_PIN_TO_FRONT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPinEnum)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Bring the specified map text to the top layer.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC BRING_HEIST_MAP_TEXT_TO_FRONT(SCALEFORM_INDEX sfBoardIndex, INT iTextEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - BRING_HEIST_MAP_TEXT_TO_FRONT - Bringing text to front. Value: ", iTextEnum)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BRING_TEXT_TO_FRONT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTextEnum)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Bring the specified map text to the top layer.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC BRING_HEIST_MAP_POSTIT_TO_FRONT(SCALEFORM_INDEX sfBoardIndex, INT iPostItEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - BRING_HEIST_MAP_POSTIT_TO_FRONT - Bringing PostIt to front. Value: ", iPostItEnum)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BRING_POSTIT_TO_FRONT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPostItEnum)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


PROC SET_HEIST_MAP_POSTIT_SCALE(SCALEFORM_INDEX sfBoardIndex, INT iPostItEnum, INT iScale)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_MAP_POSTIT_SCALE - Setting iPostItEnum: ", iPostItEnum, ", to scale: ", iScale)
	#ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SCALE_POSTIT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPostItEnum)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScale)
	END_SCALEFORM_MOVIE_METHOD()	

ENDPROC


/// PURPOSE:
///    Bring the specified map arrow to the top layer.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC BRING_HEIST_MAP_ARROW_TO_FRONT(SCALEFORM_INDEX sfBoardIndex, INT iArrowEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - BRING_HEIST_MAP_ARROW_TO_FRONT - Bringing arrow to front. Value: ", iArrowEnum)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BRING_ARROW_TO_FRONT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArrowEnum)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Bring the specified map highlight to the top layer.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC BRING_HEIST_MAP_HIGHLIGHT_TO_FRONT(SCALEFORM_INDEX sfBoardIndex, INT iHighlightEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - BRING_HEIST_MAP_HIGHLIGHT_TO_FRONT - Bringing highlight to front. Value: ", iHighlightEnum)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BRING_HIGHLIGHT_TO_FRONT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlightEnum)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:
///    Bring the specified map area to the top layer.
/// PARAMS:
///    sfBoardIndex - Movie index.
PROC BRING_HEIST_MAP_AREA_TO_FRONT(SCALEFORM_INDEX sfBoardIndex, INT iAreaEnum)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - BRING_HEIST_MAP_AREA_TO_FRONT - Bringing area to front. Value: ", iAreaEnum)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BRING_AREA_TO_FRONT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iAreaEnum)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC







/* ======================================================================= *\
	HEIST SCALEFORM - UPDATE METHODS, USED WHEN THE SETUP CHANGES
\* ======================================================================= */


/// PURPOSE:
///    Do not call every frame! Call when the highlighted item needs to change.
PROC SET_HEIST_PLANNING_HIGHLIGHT_ITEM(	SCALEFORM_INDEX 			sfBoardIndex,
										INT iHighlightedItem 		= -1,
										INT iSubHighlightedItem 	= -1,
										INT iLeftArrowVisible 		= -1, 
										INT iRightArrowVisible 		= -1,
										BOOL bIsPrePlanning			= FALSE,
										BOOL bIgnoreMapUpdate		= FALSE)
	
	IF NOT IS_HEIST_HIGHLIGHT_INDEX_NEW(iHighlightedItem, iSubHighlightedItem, iLeftArrowVisible, iRightArrowVisible)
		EXIT
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_HIGHLIGHT_ITEM - New highlight data:")
		PRINTLN("[AMEC][HEIST_SCALEFORM]	...Highlighted Item: ", 	iHighlightedItem)
		PRINTLN("[AMEC][HEIST_SCALEFORM]	...Sub Highlighted Item: ", iSubHighlightedItem)
		PRINTLN("[AMEC][HEIST_SCALEFORM]	...Left/Right Arrow Visible: ", 	iLeftArrowVisible,", ",iRightArrowVisible)
	#ENDIF
	
	IF NOT bIsPrePlanning
		// New highlight item means the playercard has changed. Make sure the player card is updated.
		g_HeistPlanningClient.bPlayerCardsInSync = FALSE
	ENDIF
	
	// Check if the new highlight data is in the player range. If so, we need to do a map update.
	IF NOT bIgnoreMapUpdate
		IF iHighlightedItem >= 0
		AND iHighlightedItem <= (MAX_HEIST_PLANNING_ROWS-1)
			PRINTLN("[AMEC][HEIST_LAUNCH] - SET_HEIST_PLANNING_HIGHLIGHT_ITEM - New highlight data is a player, updating map.")
			SET_HEIST_UPDATE_MAP_ALL(TRUE)
		ENDIF
	ENDIF
	
	SAVE_HEIST_HIGHLIGHT_INDEX(iHighlightedItem, iSubHighlightedItem, iLeftArrowVisible, iRightArrowVisible)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "HIGHLIGHT_ITEM")
		IF iHighlightedItem != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHighlightedItem)
			
			IF iSubHighlightedItem != -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSubHighlightedItem)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iLeftArrowVisible)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRightArrowVisible)
			ENDIF
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Update the heist name. Only call when updated.
/// PARAMS:
///    sfBoardIndex - Board Index
///    sHeistName - Name text
///    sHeistDesc - Description
PROC SET_HEIST_PLANNING_UPDATE_NAME(SCALEFORM_INDEX sfBoardIndex,
									STRING 	sHeistName, STRING sHeistLeaderName)
									
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_NAME - Updating heist name to: \"", sHeistName, "\"")
//	#ENDIF
	
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_HEIST_NAME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sHeistName)
		
		// Print an array of text labels into a single string.
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_OFFLN_HD")
			INT iLoop 
			FOR iLoop = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT.tl63MissionDecription[iLoop])
			ENDFOR			
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sHeistLeaderName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("HTITLE_DEF")
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:
///    Set the task list.
PROC SET_HEIST_PLANNING_UPDATE_TASK_LIST(SCALEFORM_INDEX sfBoardIndex, INT iTeam)
	
	IF NOT IS_HEIST_TASK_DATA_NEW(iTeam)
		EXIT
	ENDIF
	
	IF iTeam < 0
	OR iTeam > (FMMC_MAX_TEAMS-1)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - WARNING! Passed team index is not in range! Value: ", iTeam)
		#ENDIF

		//SET_HEIST_PLANNING_BLANK_TASK_LIST(sfBoardIndex)
		// Set the control flags.
		SET_HEIST_UPDATE_MAP_ALL(TRUE)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_TODO_LIST")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		END_SCALEFORM_MOVIE_METHOD()
		
		SAVE_HEIST_TASK_DATA(iTeam)
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - Task list team data is new, updating saved data in the cache...")
	#ENDIF
	
	// Set the control flags.
	SET_HEIST_UPDATE_MAP_ALL(TRUE)
	//g_HeistPlanningClient.bHaveDoneTaskListCleanup = FALSE
	
	SAVE_HEIST_TASK_DATA(iTeam)
	
	INT iRule, iVisibleRules, iPostitCount = 0
	TEXT_LABEL_3 tlPrefix
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_TODO_LIST")
	
		REPEAT MAX_HEIST_TASKS iRule
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[iRule], ciBS_RULE_HIDE_ON_PLANNING_BOARD)
			
				IF IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
//					#IF IS_DEBUG_BUILD
//						PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - ERROR! Team: ", iTeam, " objective: ", iRule, " does not exist in cloud data. Ignoring...")
//					#ENDIF
				ELSE
				
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetTwo[iRule], ciBS_RULE2_HIDE_ON_MAP)
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - Showing numbered rule for iVisibleRules: ", iVisibleRules)
						#ENDIF
						SET_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
						iPostitCount ++		//	Seperate counter for the postits alone - B* 2164062
						tlPrefix = "["
					ELSE
						CLEAR_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
						tlPrefix = "]"
					ENDIF
				
					PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - RULE ",iRule," TEXT: ",tlPrefix,g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
	
					// Ready the task description straight from the cloud.
					BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BRIEF_STRING2")
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPrefix)
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[iRule])
					END_TEXT_COMMAND_SCALEFORM_STRING()
					
					iVisibleRules++
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - SKIP RULE - Team: ", iTeam, ", Rule: ", iRule)
				#ENDIF
			ENDIF

		ENDREPEAT 
		
		//If it's a strand mission the add the strand mission rules
		IF g_sHeistStrandMissionRules.bStrandMissionForHeistBoard		
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - STRAND - g_sHeistStrandMissionRules.bStrandMissionForHeistBoard = TRUE")
			REPEAT g_sHeistStrandMissionRules.iNumberOfObjectives[iTeam] iRule
				IF iVisibleRules < MAX_HEIST_TASKS
					IF NOT IS_BIT_SET(g_sHeistStrandMissionRules.iRuleBitset[iTeam][iRule], ciBS_RULE_HIDE_ON_PLANNING_BOARD)		
					
						IF NOT IS_STRING_NULL_OR_EMPTY(g_sHeistStrandMissionRules.tl63Objective[iTeam][iRule])					
						
							IF NOT IS_BIT_SET(g_sHeistStrandMissionRules.iRuleBitSetTwo[iTeam][iRule], ciBS_RULE2_HIDE_ON_MAP)
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - STRAND - Showing numbered rule for iVisibleRules: ", iVisibleRules)
								#ENDIF
								SET_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
								iPostitCount ++
								tlPrefix = "["
							ELSE
								CLEAR_BIT(g_HeistPlanningClient.iTodoListBitset, iVisibleRules)
								tlPrefix = "]"
							ENDIF
							
							PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - STRAND - RULE ",iRule," TEXT: ",tlPrefix,g_sHeistStrandMissionRules.tl63Objective[iTeam][iRule])
							
							// Ready the task description straight from the cloud.
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING("BRIEF_STRING2")
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(tlPrefix)
								ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_sHeistStrandMissionRules.tl63Objective[iTeam][iRule])
							END_TEXT_COMMAND_SCALEFORM_STRING()
									
							iVisibleRules++
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - STRAND - ERROR! Team: ", iTeam, " objective: ", iRule, " does not exist in cloud data. Ignoring...")
							#ENDIF	
						ENDIF					
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_TASK_LIST - STRAND - SKIP RULE - Team: ", iTeam, ", Rule: ", iRule)
						#ENDIF
					ENDIF
				ENDIF
			ENDREPEAT 
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
	// Start up getting the mouse todo list data - Does this Asynchronously as we have to wait for scaleform to return data.
	eMouseTodoItemState = MOUSE_TODO_ITEMS_STATE_START
	
	//STORE_TODO_ITEM_MOUSE_INFO(iVisibleRules) // Required for PC mouse support
	
	g_HeistPlanningClient.iPostitRulesForTeam[iTeam] = iPostitCount
	g_HeistPlanningClient.iVisibleRulesForTeam[iTeam] = iVisibleRules
ENDPROC


/// PURPOSE:
///    Update a heist member slot.
PROC SET_HEIST_PLANNING_UPDATE_CREW_SLOT(SCALEFORM_INDEX sfBoardIndex,
										INT 			iPlayerSlot,
										STRING			sGamerTag,
										INT 			iPlayerRespect,
										STRING			sPlayerPortrait,
										TEXT_LABEL_15	tlPlayerRole,
										INT				iPlayerRoleIcon,
										STRING			sPlayerStatus,
										INT				iPlayerStatusIcon,
										INT				iPlayerPercent,
										STRING			sPlayerPseudonym,
										STRING			sPlayerOutfit,
										INT				iRemainingSlots,
										INT 			iVoiceState,
										INT				iInvalidTeam,
										INT				iPlayerCutActual = 0,
										BOOL			bSuppressHeadshot = FALSE)

	IF NOT IS_HEIST_MEMBER_DATA_NEW(iPlayerSlot, 		sGamerTag, 
									iPlayerRespect, 	sPlayerPortrait, 
									tlPlayerRole, 		iPlayerRoleIcon, 
									sPlayerStatus, 		iPlayerStatusIcon, 
									iPlayerPercent, 	sPlayerPseudonym, 
									sPlayerOutfit, 		iRemainingSlots, 
									iVoiceState, 		iInvalidTeam)
		EXIT
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_CREW_SLOT - New crew slot data:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Slot: 		", 	iPlayerSlot)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Portrait:	", 	sPlayerPortrait)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Tag:			", 	sGamerTag)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Role: 		", 	tlPlayerRole)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player RoleIcon:	", 	iPlayerRoleIcon)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Status: 		", 	sPlayerStatus)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player StatusIcon:	", 	iPlayerStatusIcon)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Cut(%):		",	iPlayerPercent)
//		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player Outfit: 		",	sPlayerOutfit)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player RemSlots: 	",	iRemainingSlots)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player VoiceState: 	",	iVoiceState)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...Player InvalidTeam: ",	iInvalidTeam)
	#ENDIF
	
	// Modified member details will require changes to the player card. Make sure the player card is updated.
	g_HeistPlanningClient.bPlayerCardsInSync = FALSE
	
	SAVE_HEIST_MEMBER_DATA(	iPlayerSlot, 		sGamerTag, 
							iPlayerRespect, 	sPlayerPortrait, 
							tlPlayerRole, 		iPlayerRoleIcon, 
							sPlayerStatus, 		iPlayerStatusIcon, 
							iPlayerPercent, 	sPlayerPseudonym, 
							sPlayerOutfit, 		iRemainingSlots, 
							iVoiceState, 		iInvalidTeam)

	// Calculate the monetary value from the slot percentage of the total kitty. This cuts
	// down on some extraneous data that was being passed in before.

	INT iPaymentAmount
	
	IF iPlayerCutActual = 0
		iPaymentAmount = iPlayerPercent * (GlobalServerBD_HeistPlanning.iHeistTotalPay / 100)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_CREW_SLOT - Setting cut actual amount using global data, value: ", iPaymentAmount)
		#ENDIF	
	ELSE
		iPaymentAmount = iPlayerCutActual
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_CREW_SLOT - Setting cut actual amount using passed in optional data, value: ", iPaymentAmount)
		#ENDIF	
	ENDIF
	
	BOOL bInvalidTeam
	
	IF iInvalidTeam = ci_HEIST_TEAM_STATUS_VALID
		bInvalidTeam = FALSE
	ELSE
		bInvalidTeam = TRUE
	ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_CREW_MEMBER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sGamerTag)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerRespect)
		IF NOT bSuppressHeadshot
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sPlayerPortrait)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("")
			PRINTLN("[RBJ][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_CREW_SLOT - Suppressing Headshot updates.")
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlPlayerRole)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerRoleIcon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerStatus)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStatusIcon)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPaymentAmount)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerPercent)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerPseudonym)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerOutfit)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iRemainingSlots)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVoiceState)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bInvalidTeam)
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


//PARAM NOTES: player Slot 0, 1, 2, 3, etc
//PURPOSE: This will blank out the desired crew member slot
PROC SET_HEIST_PLANNING_UPDATE_CREW_SLOT_BLANK(SCALEFORM_INDEX sfBoardIndex, INT iPlayersSlot)								

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_CREW_SLOT_BLANK - Slot blank-out requested for: ", iPlayersSlot)
	#ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BLANK_CREW_MEMBER")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayersSlot)
	END_SCALEFORM_MOVIE_METHOD() 														
ENDPROC

/// PURPOSE:
///    Update the medal index (to be shown at the bottom of the player card on final palnning board)
FUNC INT MODIFY_MEDAL_INDEX_FOR_SCALEFORM(INT medal)

	SWITCH medal
		CASE 0
			RETURN 4
		CASE 1
			RETURN 0
		CASE 2 
			RETURN 1
		CASE 3
			RETURN 2
		CASE 4 
			RETURN 3
		DEFAULT
			RETURN medal
	ENDSWITCH

	//
	//Script							Modify to					|Scaleform (hesit_mp, "ADD_PLAYERCARD_MEDALS")
   //-1 - invalid mission index				no change				| -1 'nothing displayed'
	//0 - none (didn't participate-black)  	4 (show black)			|  0 Gold
	//1	- Gold								0						|  1 Silver
	//2	- Silver							1						|  2 Bronze
	//3	- Bronze							2						|  3 black empty circle
	//4	- Baddie (No medal)					3(empty circle)			|  4 black circle with line through
	//5																| 5+ blue
	//																| (no method call, nothing displayed)
ENDFUNC

/// PURPOSE:
///    Update the medals shown at the bottom of the player card.
PROC SET_HEIST_PLANNING_UPDATE_PLAYERCARD_MEDALS(SCALEFORM_INDEX sfBoardIndex, INT iPlayerSlot, INT iMedalSlot1, INT iMedalSlot2, INT iMedalSlot3, INT iMedalSlot4, INT iMedalSlot5)

	IF NOT IS_HEIST_STATS_CARD_MEDAL_DATA_NEW(iPlayerSlot, iMedalSlot1, iMedalSlot2, iMedalSlot3, iMedalSlot4, iMedalSlot5)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD //debug channel check player slot
		CPRINTLN(DEBUG_MISSION,"[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_PLAYERCARD_MEDALS - Updating heist player card medal data: ")
		CPRINTLN(DEBUG_MISSION,"[AMEC][HEIST_SCALEFORM] ...  iMedalSlot1: ", iMedalSlot1, ",	iMedalSlot2: ", iMedalSlot2)
		CPRINTLN(DEBUG_MISSION,"[AMEC][HEIST_SCALEFORM] ...  iMedalSlot3: ", iMedalSlot3, ",	iMedalSlot4: ", iMedalSlot4)
		CPRINTLN(DEBUG_MISSION,"[AMEC][HEIST_SCALEFORM] ...  iMedalSlot5: ", iMedalSlot5)
	#ENDIF
	
	SAVE_HEIST_STATS_CARD_MEDAL_DATA(iPlayerSlot, iMedalSlot1, iMedalSlot2, iMedalSlot3, iMedalSlot4, iMedalSlot5)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_PLAYERCARD_MEDALS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MODIFY_MEDAL_INDEX_FOR_SCALEFORM(iMedalSlot1))		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MODIFY_MEDAL_INDEX_FOR_SCALEFORM(iMedalSlot2))		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MODIFY_MEDAL_INDEX_FOR_SCALEFORM(iMedalSlot3))		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MODIFY_MEDAL_INDEX_FOR_SCALEFORM(iMedalSlot4))		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(MODIFY_MEDAL_INDEX_FOR_SCALEFORM(iMedalSlot5))
		
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:
///    Update the player card details. Only use once, and then update when required. DO NOT CALL EVERY FRAME.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iPlayerSlot - slot number.
///    sPlayerName - Gamertag.
///    iPlayerRank - Respect/rank level.
///    sPlayerRole - Role label.
///    sPlayerPortrait - portrait TXD.
///    bAccessPlane - Can access PLANES.
///    bAccessHeli - Can access HELICOPTERS.
///    bAccessBoat - Can access BOATS.
///    bAccessCar - Can access CARS.
///    fPlayerKillDeathRatio - float formatted K/D ratio.
///    sPlayerTitle - Player's title.
///    sPlayerRating - Good / Bad sport.
///    iPlayerStamina - Stamina stat.
///    iPlayerShooting - Shooting stat.
///    iPlayerStealth - Stealth stat.
///    iPlayerFlying - Flying stat.
///    iPlayerDriving - Driving stat.
///    iPlayerMentalState - Mental state int.
PROC SET_HEIST_PLANNING_UPDATE_PLAYER_CARD(	SCALEFORM_INDEX sfBoardIndex,
											INT 	iPlayerSlot,
											STRING	sPlayerName,
											INT		iPlayerRank,
											STRING	sPlayerRole,
											STRING	sPlayerPortrait,
											BOOL	bAccessPlane,
											BOOL	bAccessHeli,
											BOOL	bAccessBoat,
											BOOL	bAccessCar,
											FLOAT	fPlayerKillDeathRatio,
											STRING	sPlayerTitle,
											STRING	sPlayerRating,
											INT		iPlayerStamina,
											INT		iPlayerShooting,
											INT		iPlayerStealth,
											INT 	iPlayerFlying,
											INT		iPlayerDriving,
											INT		iPlayerMentalState,
											INT 	iPlayerStrength)
	
	
	IF NOT IS_HEIST_STAT_CARD_DATA_NEW(iPlayerSlot, sPlayerPortrait, sPlayerRole)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_PLAYER_CARD - Update the details for the player stats card.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... sPlayerPortrait: 						", sPlayerPortrait)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... Access: Plane/Heli/Boat/Car: 			", bAccessPlane, ",  ", bAccessHeli, ", ", bAccessBoat, ", ", bAccessCar)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerSlot 		& sPlayerName: 		", iPlayerSlot, ",  ", sPlayerName)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerRank 		& tlPlayerRole: 	", iPlayerRank, ",  ", sPlayerRole)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerKDR			& sPlayerTitle: 	", fPlayerKillDeathRatio, ",  ", sPlayerTitle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... sPlayerRating 		& sPlayerStamina: 	", sPlayerRating, ",  ", iPlayerStamina)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerShooting 	& iPlayerStealth: 	", iPlayerShooting, ",  ", iPlayerStealth)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerFlying 		& iPlayerDriving: 	", iPlayerFlying, ",  ", iPlayerDriving)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerMentalState	& iPlayerStrength:	", iPlayerMentalState, ",  ", iPlayerStrength)
	#ENDIF	
	
	SAVE_HEIST_STAT_CARD_DATA(iPlayerSlot, sPlayerPortrait, sPlayerRole)
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PLAYERCARD")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerRank)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerRole)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sPlayerPortrait)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessPlane)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessHeli)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessBoat)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAccessCar)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fPlayerKillDeathRatio)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sPlayerRating)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStamina)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerShooting)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStealth)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerFlying)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerDriving)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerMentalState)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerStrength)
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    Update the heist planning board PIE CHART. Safe to call every frame.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iPotentialTake - Total take (no costs removed).
///    iCosts - The total costs amount.
///    sCutData - Cuts array. 0-7 players; 8 = cost.
PROC SET_HEIST_PLANNING_UPDATE_PIECHART(SCALEFORM_INDEX sfBoardIndex,
										INT iPotentialTake,
										HEIST_CUTS_DATA sCutData)
	
	IF NOT IS_HEIST_PIE_CHART_DATA_NEW(iPotentialTake, sCutData)
		EXIT
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		INT index
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_PIECHART - Pie chart update data: ")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ...iPotentialTake: ", iPotentialTake)
		//PRINTLN("[AMEC][HEIST_SCALEFORM] ...iCosts: ", iCosts)
		
		FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
			PRINTLN("[AMEC][HEIST_SCALEFORM] ...iPercentage[",index,"]: ", sCutData.iCutPercent[index])
		ENDFOR
		
	#ENDIF		
	
	SAVE_HEIST_PIE_CHART_DATA(iPotentialTake, sCutData)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PIECHART")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPotentialTake)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCosts)
		
		// Because we're trying to make the interface look simple, the cuts are delt with in incrememnts of 5 between 0 and 100.
		// The 0 to 100% that the player sees is actually 0-(100 - cost)%. As an example, the cost might be 10%, this makes what the user
		// has between 0 and 90%, but displays as 0 to 100 for simplicity.
		INT i
		FOR i = 0 TO MAX_HEIST_PLAYER_SLOTS
		// Stop any divide by zero errors, and make sure not to divide out the cost percentage by itself.
//			IF i < MAX_HEIST_PLAYER_SLOTS
//			IF sCutData.iCutPercent[i] > 0
//				//INT iRemainingPercent = (100 - sCutData.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
//				FLOAT fAdjustedPercent = ((TO_FLOAT(sCutData.iCutPercent[i]) * TO_FLOAT(iRemainingPercent)) / TO_FLOAT(100))
//				sCutData.iCutPercent[i] = ROUND(fAdjustedPercent)
//			ENDIF
		
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sCutData.iCutPercent[i])
		ENDFOR 
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

//
///// PURPOSE:
/////    Update & display up to six choices from the planning board.
///// PARAMS:
//PROC SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_CHOICES(SCALEFORM_INDEX sfBoardIndex,
//													STRING sTitleGear,	STRING sGearTexture,
//													STRING sTitleOutfit,STRING sOutfitTexture)
//	
////	IF NOT IS_HEIST_PRE_PLANNING_CHOICES_DATA_NEW(sImageTexture1, iSelectedItem1, sImageTexture2, iSelectedItem2, sImageTexture3, iSelectedItem3, sImageTexture4, iSelectedItem4, sImageTexture5, iSelectedItem5, sImageTexture6, iSelectedItem6)
////		EXIT
////	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_CHOICES - Update the pre-planning choices.")
//	#ENDIF	
//	
////	SAVE_HEIST_PRE_PLANNING_CHOICES_DATA(sImageTexture1, iSelectedItem1, sImageTexture2, iSelectedItem2, sImageTexture3, iSelectedItem3, sImageTexture4, iSelectedItem4, sImageTexture5, iSelectedItem5, sImageTexture6, iSelectedItem6)
//								
//	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PLANNING_CHOICES")
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sTitleGear)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sGearTexture)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sTitleOutfit)
//		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sOutfitTexture)
//	END_SCALEFORM_MOVIE_METHOD()
//						
//ENDPROC

PROC SET_HEIST_PLANNING_UPDATE_PREVIEW(SCALEFORM_INDEX sfBoardIndex, INT iSelectedWindow, STRING sTitleText, STRING sTexture)

	IF NOT IS_HEIST_PLANNING_PREVIEW_DATA_NEW(iSelectedWindow, sTitleText, sTexture)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_PREVIEW - Update preview box: ", iSelectedWindow)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_PREVIEW ... sTitleText: 	", sTitleText)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_PREVIEW ... sTexture: 		", sTexture)
	#ENDIF	
	
	SAVE_HEIST_PLANNING_PREVIEW_DATA(iSelectedWindow, sTitleText, sTexture)
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PREVIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedWindow)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitleText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTexture)
	END_SCALEFORM_MOVIE_METHOD()					

ENDPROC

PROC SET_HEIST_PLANNING_BLANK_ALL_PREVIEWS(SCALEFORM_INDEX sfBoardIndex)

	INT index

	FOR index = 0 TO (MAX_HEIST_PREVIEW_WINDOWS-1)
		IF NOT IS_HEIST_PLANNING_PREVIEW_DATA_NEW(index, "", "")
			EXIT
		ENDIF
	ENDFOR

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_BLANK_ALL_PREVIEWS - New BLANK method called, removing all preview boxes.")
	#ENDIF

	FOR index = 0 TO (MAX_HEIST_PREVIEW_WINDOWS-1)
		SAVE_HEIST_PLANNING_PREVIEW_DATA(index, "", "")
	ENDFOR

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BLANK_PREVIEW")
	END_SCALEFORM_MOVIE_METHOD()	

ENDPROC


/// PURPOSE:
///    Clear the image of a specific preview box, but leave the title as is. This maintains the border around the image,
/// PARAMS:
///    sfBoardIndex - 
///    iSelectedWindow - 
///    sTitleText - 
PROC SET_HEIST_PLANNING_CLEAR_SPECIFIC_PREVIEW(SCALEFORM_INDEX sfBoardIndex, INT iSelectedWindow, STRING sTitleText)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_CLEAR_SPECIFIC_PREVIEW - Clear preview box: ", iSelectedWindow)
	#ENDIF	
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PREVIEW")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelectedWindow)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sTitleText)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("")
	END_SCALEFORM_MOVIE_METHOD()				

ENDPROC


/// PURPOSE:
///    Update only the voice status of a finale planning board slot.
PROC SET_HEIST_PLANNING_UPDATE_VOICE_STATE(SCALEFORM_INDEX sfBoardIndex,
											INT 	iPlayerSlot,
											INT		iVoiceState)
	
	
	IF NOT IS_HEIST_VOICE_STATE_DATA_NEW(iPlayerSlot, iVoiceState)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PLANNING_UPDATE_VOICE_STATE - Update details for voice state: ")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iPlayerSlot: 	", iPlayerSlot)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iVoiceState: 	", iVoiceState)
	#ENDIF	
	
	SAVE_HEIST_VOICE_STATE_DATA(iPlayerSlot, iVoiceState)
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_PLAYERLIST_ICON")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iPlayerSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iVoiceState)
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    Update & display up to three rows of planning images, up to three images per row.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iItemSlot - item Slot (0, 1, or 2).
///    sTitle - title (eg: PICK UP AN OUTFIT).
///    sTextureForA - TXD 1 - image for option A.
///    SImageNameA - image 1 name (eg: BUGSTARS).
///    sTextureForB - TXD 2 - image for option B.
///    SImageNameB - image 2 name (eg: POLICE)
///    sTextureForC - TXD 3 - image for option C.
///    SImageNameC - image 3 name (eg: SECURITY)
///    iChosenOption - chosen option - highlights a previously selected option by the player - 0, 1, or 3. Pass 99 for none chosen.
PROC SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOT(	SCALEFORM_INDEX sfBoardIndex,
													INT 	iItemSlot,
													STRING	sAllImagesTXD,
													STRING	sTitle,
													STRING	sTextureForA, 	STRING	sImageNameA,
													STRING	sTextureForB, 	STRING	sImageNameB,
													STRING	sTextureForC,	STRING	sImageNameC,
													INT		iChosenOption,
													BOOL	bAvailable,
													BOOL	bShowCircle,
													BOOL	bFadeCrosses)
	
	IF NOT IS_HEIST_PRE_PLANNING_SLOT_DATA_NEW(iItemSlot, sAllImagesTXD, sTitle, sTextureForA, sImageNameA, sTextureForB, sImageNameB, sTextureForC, sImageNameC, iChosenOption, bAvailable)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOTS - Updating the pre-planning slot.")
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iItemSlot: 	", 	iItemSlot)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... sAllImagesTXD: ", 	sAllImagesTXD)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... sTitle: 		", 	sTitle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... A) Tex,Name: 	",	sTextureForA,", ",sImageNameA)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... B) Tex,Name: 	",	sTextureForB,", ",sImageNameB)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... C) Tex,Name: 	",	sTextureForC,", ",sImageNameC)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... iChosenOption: ", 	iChosenOption)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... bAvailable: 	", 	bAvailable)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... bShowCircle: 	", 	bShowCircle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... bFadeCross: 	", 	bFadeCrosses)
	#ENDIF	
	
	
	SAVE_HEIST_PRE_PLANNING_SLOT_DATA(iItemSlot, sAllImagesTXD, sTitle, sTextureForA, sImageNameA, sTextureForB, sImageNameB, sTextureForC, sImageNameC, iChosenOption, bAvailable)
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PLANNING_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iItemSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sAllImagesTXD)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureForA)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sImageNameA)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureForB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sImageNameB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(sTextureForC)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sImageNameC)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iChosenOption)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bShowCircle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bFadeCrosses) // Make crosses always fade in 2167609.
	END_SCALEFORM_MOVIE_METHOD()
	
	//	B* 2217696
	//	Save highlighted cached data locally
	INT iHighlightedItem 		= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iHighlightedItem
	INT iSubHighlightedItem = g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iSubHighlightedItem
	INT iLeftArrowVisible	= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iLeftArrowVisible
	INT iRightArrowVisible 	= g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iRightArrowVisible	
	
	//	Clear it to ensure the SET_HEIST_PLANNING_HIGHLIGHT_ITEM run the highlight again
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iHighlightedItem		= -1
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iSubHighlightedItem	= -1
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iLeftArrowVisible	= -1
	g_HeistPlanningClient.sHeistDataCache.sHighlightCached.iRightArrowVisible	= -1
	
	SET_HEIST_PLANNING_HIGHLIGHT_ITEM(sfBoardIndex,	iHighlightedItem, iSubHighlightedItem, iLeftArrowVisible, iRightArrowVisible, TRUE)
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOT - Updated slot: ", iItemSlot, ", Title: ", sTitle)
	#ENDIF
								
ENDPROC


/// PURPOSE:
///    Blank out all planning choices.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
PROC SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_CHOICES_BLANK(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_CHOICES_BLANK - Blanking out all planning choices.")
	#ENDIF	

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BLANK_PLANNING_CHOICES")
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:
///    Blank out a specific planning slot.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    iItemSlot - Item slot to blank out.
PROC SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOT_BLANK(SCALEFORM_INDEX sfBoardIndex, INT iItemSlot)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOT_BLANK - blanking out specific planning slot #", iItemSlot)
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOT_BLANK - Blanking planning slot: ", iItemSlot)
	#ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BLANK_PLANNING_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iItemSlot)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:
///    Zoom the map the the general area in which the heist is taking place. This is all handled on the scaleform side.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    bDoShow - TRUE to toggle the zoom on.
PROC TOGGLE_ZOOM_MAP_TO_HEIST_AREA(SCALEFORM_INDEX sfBoardIndex, BOOL bDoShow)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - ZOOM_MAP_TO_HEIST_AREA - Zooming map to heist geographical area. bDoShow: ", bDoShow)
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ZOOM_MAP")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bDoShow)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC 


/// PURPOSE:
///    Set the text on the LEFT hand side of the centre picture. This side is used for misc helpful details
///    such as the time available, minimum players and the cost. Hard-coded to a limit of 3.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iItemSlot - Mission number.
///    sDescription - Mission desciption text.
PROC SET_HEIST_PRE_PLANNING_UPDATE_SLOT_LEFT(SCALEFORM_INDEX sfBoardIndex, INT iItemSlot)
	
	IF NOT IS_HEIST_PRE_PLANNING_SLOT_LEFT_DATA_NEW(iItemSlot, g_HeistPrePlanningClient.sPlanningData.sMissionData[iItemSlot].tl63MissionDecription[0])
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_SLOT_LEFT - EXIT for slot number",iItemSlot)
		EXIT
	ENDIF
												
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_SLOT_LEFT - Updating LEFT side strings for slot number: ",iItemSlot)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... Description: 					", g_HeistPrePlanningClient.sPlanningData.sMissionData[iItemSlot].tl63MissionDecription[0])
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_UPDATE_SLOT_LEFT - Update pre-planning desc: ", g_HeistPrePlanningClient.sPlanningData.sMissionData[iItemSlot].tl63MissionDecription[0])
	#ENDIF
	
	SAVE_HEIST_PRE_PLANNING_SLOT_LEFT_DATA(iItemSlot, g_HeistPrePlanningClient.sPlanningData.sMissionData[iItemSlot].tl63MissionDecription[0])
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PLANNING_SLOT_LEFT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iItemSlot)
		//SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sDescription)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("FMMC_OFFLN_HD")
		PRINTLN("[CORONA] CORONA_SET_HEADER_TITLE - and Description: ")
			INT iLoop 
			FOR iLoop = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION - 1)
				PRINTLN("g_HeistPrePlanningClient.sPlanningData.sMissionData[", iItemSlot, "].tl63MissionDecription[", iLoop, "] = ", g_HeistPrePlanningClient.sPlanningData.sMissionData[iItemSlot].tl63MissionDecription[iLoop]) 
				ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(g_HeistPrePlanningClient.sPlanningData.sMissionData[iItemSlot].tl63MissionDecription[iLoop])
			ENDFOR			
		END_TEXT_COMMAND_SCALEFORM_STRING()	
	END_SCALEFORM_MOVIE_METHOD()
											
ENDPROC


/// PURPOSE:
///    Set the text on the RIGHT hand side of the centre picture. This side is used for listing the players who participated in this event. 
///    Hard-coded to a limit of 4 (to match the maximum players in a heist), takes string literal.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iItemSlot - The specific picture number.
///    sTitle - Lebel for "participants" title text.
///    sText_1 to 4 - String literal value.
/// RETURNS:
///    TRUE if we have updated the data on the board
FUNC BOOL SET_HEIST_PRE_PLANNING_UPDATE_SLOT_RIGHT(	SCALEFORM_INDEX sfBoardIndex, INT iItemSlot, STRING sTitle, 
												TEXT_LABEL_63 sPlayerName_1, TEXT_LABEL_63 sPlayerName_2,
												TEXT_LABEL_63 sPlayerName_3, TEXT_LABEL_63 sPlayerName_4)
		
	IF NOT IS_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA_NEW(iItemSlot, sTitle, sPlayerName_1, sPlayerName_2, sPlayerName_3, sPlayerName_4)
		RETURN FALSE
	ENDIF
												
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_UPDATE_SLOT_RIGHT - Updating RIGHT side strings for slot number: ",iItemSlot, " Title: ", sTitle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] ... PL Names 1,2,3,4: 	", 	sPlayerName_1, ", ", 	sPlayerName_2, ", ", 	sPlayerName_3, ", ", 	sPlayerName_4)
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_PRE_PLANNING_UPDATE_SLOT_RIGHT - Update pre-planning names 1,2,3,4: ", sPlayerName_1, ", ", sPlayerName_2, ", ", sPlayerName_3, ", ", sPlayerName_4)
	#ENDIF
	
	SAVE_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA(iItemSlot, sTitle, sPlayerName_1, sPlayerName_2, sPlayerName_3, sPlayerName_4)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PLANNING_SLOT_RIGHT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iItemSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_4)
	END_SCALEFORM_MOVIE_METHOD()
		
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Set the text on the RIGHT hand side of the centre picture. This side is used for listing the players who participated in this event. 
///    Hard-coded to a limit of the maximum players in a heist, it takes string literal.
///    Will not shows medals. Used when on heist planning mission, medals are not yet determined.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    iItemSlot - The specific picture number.
///    sTitle - Lebel for "participants" title text.
///    sText_1 to x - String literal value.
/// RETURNS:
///    TRUE if we have updated the data on the board
FUNC BOOL SET_HEIST_PRE_PLANNING_UPDATE_SLOT_RIGHT_NO_MEDALS(SCALEFORM_INDEX sfBoardIndex, INT iItemSlot, STRING sTitle, 
												TEXT_LABEL_63 sPlayerName_1, TEXT_LABEL_63 sPlayerName_2,
												TEXT_LABEL_63 sPlayerName_3, TEXT_LABEL_63 sPlayerName_4)
		
	IF NOT IS_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA_NEW(iItemSlot, sTitle, sPlayerName_1, sPlayerName_2, sPlayerName_3, sPlayerName_4)
	OR (g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer1 = GET_HASH_KEY("hideMedal")
	AND g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer2 = GET_HASH_KEY("hideMedal")
	AND g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer3 = GET_HASH_KEY("hideMedal")
	AND g_HeistPrePlanningClient.sHeistDataCache[iItemSlot].sSlotDataCached.sPlayer4 = GET_HASH_KEY("hideMedal")
	)
		RETURN FALSE
	ENDIF
	
	SAVE_HEIST_PRE_PLANNING_SLOT_RIGHT_DATA(iItemSlot, sTitle, "hideMedal", "hideMedal", "hideMedal", "hideMedal")
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_PLANNING_SLOT_RIGHT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iItemSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME("")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(sPlayerName_4)
	END_SCALEFORM_MOVIE_METHOD()
		
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Show or hide the pre-planning images. Used for the strand intro cutscene.
/// PARAMS:
///    sfBoardIndex - Movie index.
///    bShowImages - TRUE to show images.
PROC SET_HEIST_PRE_PLANNING_SHOW_IMAGES(SCALEFORM_INDEX sfBoardIndex, BOOL bShowImages)
	
	IF bShowImages
		IF NOT g_HeistPlanningClient.sHeistDataCache.sMiscCached.bPicturesVisible
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_SHOW_IMAGES - Enabling planning board images.")
				DEBUG_PRINTCALLSTACK()
			#ENDIF	
			BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_PLANNING_IMAGES")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
			END_SCALEFORM_MOVIE_METHOD()
			g_HeistPlanningClient.sHeistDataCache.sMiscCached.bPicturesVisible = TRUE
		ENDIF
	ELSE
		IF g_HeistPlanningClient.sHeistDataCache.sMiscCached.bPicturesVisible
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLANNING_SHOW_IMAGES - Disabling planning board images.")
				DEBUG_PRINTCALLSTACK()
			#ENDIF	
			BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_PLANNING_IMAGES")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)
			END_SCALEFORM_MOVIE_METHOD()
			g_HeistPlanningClient.sHeistDataCache.sMiscCached.bPicturesVisible = FALSE
		ENDIF
	ENDIF
											
ENDPROC




/* ======================================================================= *\
	HEIST SCALEFORM - SWITCH BETWEEN PRE-PLANNING AND PLANNING BOARD
\* ======================================================================= */


/// PURPOSE:
///    Calling this will automatically blank all entries made on the planning board to free up txd refs.
/// PARAMS:
///    sfBoardIndex - scaleform index.
PROC SET_HEIST_SWITCH_TO_PRE_PLANNING(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_SWITCH_TO_PRE_PLANNING - Switching to PRE PLANNING.")
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_SWITCH_TO_PRE_PLANNING - Switch to prep mode")
	#ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_PLANNINGBOARD")
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


/// PURPOSE:
///    Calling this will automatically blank all player crew member slots and any planning choices that were displayed to free up txd refs.
/// PARAMS:
///    sfBoardIndex - scaleform index.
PROC SET_HEIST_SWITCH_TO_HEIST_BOARD(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_SWITCH_TO_HEIST_BOARD - Switching to HEIST PLANNING.")
	#ENDIF	
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - SET_HEIST_SWITCH_TO_HEIST_BOARD - Switch to finale mode")
	#ENDIF

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_HEISTBOARD")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Set the visibility of the "LAUNCH HEIST" button.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    bVisible - Is visible.
PROC SET_HEIST_LAUNCH_BUTTON_VISIBILITY(SCALEFORM_INDEX sfBoardIndex, BOOL bVisible)
	
	IF g_HeistPlanningClient.sHeistDataCache.bLaunchButtonVisible != bVisible
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_LAUNCH_BUTTON_VISIBILITY - Launch button visibilibty toggle, playing sound? ", PICK_STRING(bVisible, "TRUE","FALSE"))
		#ENDIF	
	
		g_HeistPlanningClient.sHeistDataCache.bLaunchButtonVisible = bVisible
		
		IF bVisible
			PLAY_SOUND_FRONTEND(-1,"Continue_Appears","DLC_HEIST_PLANNING_BOARD_SOUNDS")
		ENDIF
		
		BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "LAUNCH_BUTTON_VISIBLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bVisible)
		END_SCALEFORM_MOVIE_METHOD()
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Set hee "LAUNCH HEIST" button to enabled / disabled.
/// PARAMS:
///    sfBoardIndex - Scaleform index.
///    bEnable - Enable the button.
PROC SET_HEIST_LAUNCH_BUTTON_ENABLED(SCALEFORM_INDEX sfBoardIndex, BOOL bEnable)

	IF NOT IS_HEIST_LAUNCH_BUTTON_ENABLED_DATA_NEW(bEnable)
		EXIT
	ENDIF

	SAVE_HEIST_LAUNCH_BUTTON_ENABLED_DATA(bEnable)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_LAUNCH_BUTTON_ENABLED - Launch button enabled: ", bEnable)
	#ENDIF	

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "LAUNCH_BUTTON_ENABLED")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bEnable)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC


PROC SET_HEIST_PRE_PLAN_TEXT_VISIBLE(SCALEFORM_INDEX sfBoardIndex, BOOL bVisible)

	IF bVisible
		IF NOT g_HeistPlanningClient.sHeistDataCache.sMiscCached.bTextVisible
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLAN_TEXT_VISIBLE - Pre-plan text set to VISIBLE")
			#ENDIF	
			g_HeistPlanningClient.sHeistDataCache.sMiscCached.bTextVisible = TRUE
			BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_ALL_TEXT")
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ELSE
		IF g_HeistPlanningClient.sHeistDataCache.sMiscCached.bTextVisible
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_PRE_PLAN_TEXT_VISIBLE - Pre-plan text set to INVISIBLE")
			#ENDIF
			g_HeistPlanningClient.sHeistDataCache.sMiscCached.bTextVisible = FALSE
			BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "HIDE_ALL_TEXT")
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF

ENDPROC




////////// HEIST STRAND BOARD \\\\\\\\\\

/// PURPOSE:
///    Build a frame for the board to populate.
PROC SET_HEIST_STRAND_PLANNING_SLOT_EMPTY(SCALEFORM_INDEX sfBoardIndex)
															
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_STRAND_PLANNING_SLOT_EMPTY - Set empty pre-planning slot.")
	#ENDIF
								
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "BLANK_STRAND_SLOT")
	END_SCALEFORM_MOVIE_METHOD()
								
ENDPROC


/// PURPOSE:
///    This will set the display and focus, and is the end of initial setup. Only call once.
PROC SET_HEIST_STRAND_PLANNING_INITIALISE(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_STRAND_PLANNING_INITIALISE - Pre-planning init has been called, initial pre-planning setup complete.")
	#ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "INITIALISE_STRANDBOARD")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


/// PURPOSE:
///    Clear any previous planning board graphics, does not release TXDs.
/// PARAMS:
///    sfBoardIndex - scaleform index.
PROC SET_HEIST_SWITCH_TO_HEIST_STRAND_BOARD(SCALEFORM_INDEX sfBoardIndex)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_SWITCH_TO_HEIST_STRAND_BOARD - Switching to HEIST STRAND PLANNING.")
	#ENDIF	

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SHOW_STRANDBOARD")
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC

/// PURPOSE:
///    Method to add our strand slots to the heist board
/// PARAMS:
///    sfBoardIndex - scaleform index
///    iStrandSlot - The index of the strand (row on board)
///    strTitle - Title of the strand
///    strDescription - Description of the strand
///    strTxn - Texture name string
///    bCompleted - Whether the player has completed or not (will always be TRUE)
///    bAvailable - If the strand is available (greyed out)
PROC SET_HEIST_STRAND_ADD_SLOT(SCALEFORM_INDEX sfBoardIndex, INT iStrandSlot, STRING strTitle, STRING strDescription, STRING strTxn, BOOL bAvailable, INT iCost)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_STRAND_ADD_SLOT - Add new heist strand slot:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							iStrandSlot		: ", iStrandSlot)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							strTitle		: ", strTitle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							strDescription	: ", strDescription)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							bAvailable		: ", PICK_STRING(bAvailable, "TRUE", "FALSE"))
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							iCost			: ", iCost)
	#ENDIF	

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "ADD_STRAND_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStrandSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("MPHEIST_BIOLAB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strDescription)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strTxn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HEIST_STR_C")
			ADD_TEXT_COMPONENT_INTEGER(iCost)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

/// PURPOSE:
///    Similar function as above. NOTE: Kept separate for now if we need to extend this to check if IS_DATA_NEW.
PROC SET_HEIST_STRAND_UPDATE_SLOT(SCALEFORM_INDEX sfBoardIndex, INT iStrandSlot, STRING strTitle, STRING strDescription, STRING strTxn, BOOL bAvailable, INT iCost)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_STRAND_UPDATE_SLOT - Add new heist strand slot:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							iStrandSlot		: ", iStrandSlot)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							strTitle		: ", strTitle)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							strDescription	: ", strDescription)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							strTxn			: ", strTxn)
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							bAvailable		: ", PICK_STRING(bAvailable, "TRUE", "FALSE"))
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							iCost			: ", iCost)
	#ENDIF	

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "UPDATE_STRAND_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStrandSlot)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING("MPHEIST_BIOLAB")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(strTitle)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strDescription)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_TEXTURE_NAME_STRING(strTxn)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bAvailable)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("HEIST_STR_C")
			ADD_TEXT_COMPONENT_INTEGER(iCost)
		END_TEXT_COMMAND_SCALEFORM_STRING()
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC

/// PURPOSE:
///    Update the title of the strand heist board
PROC SET_HEIST_STRAND_BOARD_TITLE(SCALEFORM_INDEX sfBoardIndex, STRING strTitle)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_SCALEFORM] - SET_HEIST_STRAND_BOARD_TITLE - Set title of strand board:")
		PRINTLN("[AMEC][HEIST_SCALEFORM] - 							strTitle		: ", strTitle)
	#ENDIF	

	BEGIN_SCALEFORM_MOVIE_METHOD(sfBoardIndex, "SET_STRAND_BOARD_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strTitle)
	END_SCALEFORM_MOVIE_METHOD()

ENDPROC




// End of file.
