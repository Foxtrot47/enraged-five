//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_async_grid_warp.sch														//
// Description: Warping designed for outside properties that uses shapetest to reliably find an empty		//
//				spawn space.																				//
// Written by:  Tymon																						//
// Date:  		4/08/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "commands_misc.sch"
USING "commands_debug.sch"


USING "net_simple_interior_async_grid_warp_locations.sch"

/*
FUNC INT _GET_ASYNC_GRID_POINT_INDEX(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data, INT x, INT y)
	INT tooMuchX = FLOOR(TO_FLOAT(x) / TO_FLOAT(data.iGridWidth))
	x = x % data.iGridWidth
	RETURN (y + tooMuchX) * data.iGridWidth + x
ENDFUNC
*/

PROC _GET_ASYNC_GRID_POINT_BIT(INT i, INT &iArray, INT &iBit)
	IF i > -1 AND i < CI_ASYNC_GRID_SPAWN_MAX_POINTS 
		iArray = FLOOR(TO_FLOAT(i) / 32.0)
		iBit = i - (iArray*32)
	ELSE
		iArray = 0
		iBit = 0
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_POINT_BIT - Index is out of grid bounds!")
		#ENDIF
	ENDIF
ENDPROC

FUNC VECTOR _GET_ASYNC_GRID_DIRECTION(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data)
	RETURN GET_HEADING_AS_VECTOR(data.fGridHeading)
ENDFUNC

/// PURPOSE:
///    from 0 to iRange - 1
/// PARAMS:
///    x - 
///    iRange - 
/// RETURNS:
///    
FUNC INT _GET_RAND_INT_FOR_POINT_ID(INT x, INT iRange)
	INT iSeed = 7364982 + x // We want the same rand number for given point ID so hence hardcoded seed
    iSeed = (iSeed * 58321) + 11113
	RETURN SHIFT_RIGHT(iSeed, 16) % iRange
ENDFUNC

FUNC VECTOR _GET_ASYNC_GRID_RANDOM_POINT_OFFSET(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data, INT iPointID)
	
	FLOAT fPosX = TO_FLOAT(iPointID % data.iGridWidth) * data.fSpacingX
	INT y = FLOOR(TO_FLOAT(iPointID) / TO_FLOAT(data.iGridWidth))
	FLOAT fXShift = 0.0
	IF y % 2 = 1
		IF FLOOR(TO_FLOAT(y) / 2.0) % 2 = 0
			fXShift = 0.5
		ELSE
			fXShift = -0.5
		ENDIF
	ENDIF
	
	FLOAT fOffsetAmount = SIN((fPosX + fXShift) * data.fSinSpread) * data.fSinAmp
	VECTOR vDir = _GET_ASYNC_GRID_DIRECTION(data)
	
	VECTOR vOffset = vDir * fOffsetAmount
	
	//*/
	
	/*
	UNUSED_PARAMETER(data)
	
	FLOAT fOffsetAmount = TO_FLOAT(_GET_RAND_INT_FOR_POINT_ID(iPointID, 11) - 5) * 0.1 * data.fSpacingY
	
	VECTOR vOffset = <<0.0, fOffsetAmount, 0.0>>
	*/
	
	/*
	IF iPointID != 0
		IF iPointID % 2 = 0
			vOffset.Y += 0.15
			vOffset.X -= 0.15
		ENDIF
		
		IF iPointID % 3 = 0
			vOffset.Y -= 0.33
			vOffset.X += 0.15
		ENDIF
		
		IF iPointID % 4 = 0
			vOffset.Y -= 0.2
			vOffset.X += 0.2
		ENDIF
		
		IF iPointID % 5 = 0
			vOffset.Y += 0.5
			vOffset.X -= 0.25
		ENDIF
		
		IF iPointID % 6 = 0
			vOffset.Y += 0.25
			vOffset.X -= 0.15
		ENDIF
		
		IF iPointID % 7 = 0
			vOffset.Y -= 0.5
			vOffset.X += 0.25
		ENDIF
	ENDIF
	*/
	
	RETURN vOffset
ENDFUNC

FUNC VECTOR _GET_ASYNC_GRID_POINT(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data, INT i)
	VECTOR v
	IF i > -1 AND i < CI_ASYNC_GRID_SPAWN_MAX_POINTS
		INT y = FLOOR(TO_FLOAT(i) / TO_FLOAT(data.iGridWidth))
		INT xt = i - data.iGridWidth * y
		INT x
		
		// Pick x once from left once from right of the start vector
		IF xt % 2 = 1
			x = CEIL(TO_FLOAT(xt) / 2.0)
		ELSE
			x = FLOOR(TO_FLOAT(xt) / 2.0) * -1
		ENDIF
		
		VECTOR vDir = _GET_ASYNC_GRID_DIRECTION(data)
		VECTOR vSide = ROTATE_VECTOR_ABOUT_Z_ORTHO(vDir, ROTSTEP_NEG_90)
		
		// Shift every uneven row alternatively left or right to create less regular pattern
		FLOAT fXShift = 0
		IF y % 2 = 1
			IF FLOOR(TO_FLOAT(y) / 2.0) % 2 = 0
				fXShift = 0.5
			ELSE
				fXShift = -0.5
			ENDIF
		ENDIF
		
		v = data.vStartCoord + (TO_FLOAT(x) + fXShift) * vSide * data.fSpacingX + TO_FLOAT(y) * vDir * data.fSpacingY
		
		IF data.bHumanizePos
			v += _GET_ASYNC_GRID_RANDOM_POINT_OFFSET(data, i)
		ENDIF
		
		FLOAT z
		IF GET_GROUND_Z_FOR_3D_COORD(v, z)
			v.z = z + data.fProbeRadius + data.fOffsetZ
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_POINT - Index is out of grid bounds!")
		#ENDIF
	ENDIF
	RETURN v
ENDFUNC

FUNC INT _GET_ASYNC_GRID_NEXT_SORTED_POINT_ID(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data)
	IF NOT g_AsyncGridSpawnData.bUsePresortedArray
		SCRIPT_ASSERT("[ASYNC_GRID_WARP] Calling _GET_ASYNC_GRID_NEXT_SORTED_POINT_ID but bUsePresortedArray is FALSE.")
	ENDIF
	
	IF g_AsyncGridSpawnData.iCurrentSortedIndex < 0 OR g_AsyncGridSpawnData.iCurrentSortedIndex >= COUNT_OF(g_AsyncGridSpawnData.iPointsIndices)
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_SORTED_POINT_ID - iCurrentSortedIndex is over array limit.")
		#ENDIF
		RETURN -1
	ENDIF
	
	INT iArray, iBit
	BOOL bGotNextPoint = FALSE
	INT iPointID = -1
	
	WHILE NOT bGotNextPoint
		iPointID = g_AsyncGridSpawnData.iPointsIndices[g_AsyncGridSpawnData.iCurrentSortedIndex]
		IF iPointID > -1
			_GET_ASYNC_GRID_POINT_BIT(iPointID, iArray, iBit)
			
			IF NOT IS_BIT_SET(g_AsyncGridSpawnData.iUsedPointsBS[iArray], iBit)
			AND NOT IS_BIT_SET(data.iExcludedPointsBS[iArray], iBit)
			AND NOT IS_BIT_SET(g_AsyncGridSpawnData.iPointsUsedByOthersBS[ENUM_TO_INT(g_AsyncGridSpawnData.location)][iArray], iBit)
				bGotNextPoint = TRUE
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
		
		IF g_AsyncGridSpawnData.iCurrentSortedIndex < COUNT_OF(g_AsyncGridSpawnData.iPointsIndices) - 1
			g_AsyncGridSpawnData.iCurrentSortedIndex += 1
		ENDIF
	ENDWHILE
	
	IF NOT bGotNextPoint
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_SORTED_POINT_ID - We're at iCurrentSortedIndex ", g_AsyncGridSpawnData.iCurrentSortedIndex, " and we hit the end of available points!")
		#ENDIF
		RETURN -1
	ENDIF
	
	RETURN iPointID
ENDFUNC

FUNC INT _GET_ASYNC_GRID_NEXT_POINT_ID(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data)
	
	// BRUTE FORCE SORTING DUH
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - We're gonna find the closest point.")
		BOOL bPrintDebug = TRUE // g_AsyncGridSpawnData.db_bGridCreatorActive OR g_AsyncGridSpawnData.db_bDrawPoints
	#ENDIF
	
	INT iClosestPoint = -1
	VECTOR vPoint
	FLOAT fMinDist = 9999.9
	FLOAT fDist
	INT iPointsCount = IMIN(data.iGridWidth * data.iGridHeight, CI_ASYNC_GRID_SPAWN_MAX_POINTS)
	INT i
	INT iArray, iBit
	INT iOriginPointID = data.iSpawnOriginPointID
	
	#IF IS_DEBUG_BUILD
	IF g_AsyncGridSpawnData.db_iOverwritePoint[ENUM_TO_INT(g_AsyncGridSpawnData.location)] > -1
		iOriginPointID = g_AsyncGridSpawnData.db_iOverwritePoint[ENUM_TO_INT(g_AsyncGridSpawnData.location)]
	ENDIF
	#ENDIF
			
	VECTOR vSpawnOrigin = _GET_ASYNC_GRID_POINT(data, iOriginPointID)
	
	REPEAT iPointsCount i
		_GET_ASYNC_GRID_POINT_BIT(i, iArray, iBit)
		
		IF NOT IS_BIT_SET(g_AsyncGridSpawnData.iUsedPointsBS[iArray], iBit)
		AND NOT IS_BIT_SET(data.iExcludedPointsBS[iArray], iBit)
		AND NOT IS_BIT_SET(g_AsyncGridSpawnData.iPointsUsedByOthersBS[ENUM_TO_INT(g_AsyncGridSpawnData.location)][iArray], iBit)
		//AND NOT IS_BIT_SET(GlobalServerBD_BlockC.AsyncGroupWarpBD.iPointsUsed[ENUM_TO_INT(g_AsyncGridSpawnData.location)][iArray], iBit) // Not used set on server
			
			vPoint = _GET_ASYNC_GRID_POINT(data, i)
			fDist = VDIST(vPoint, vSpawnOrigin)
			
			#IF IS_DEBUG_BUILD
				IF bPrintDebug
					PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Checking point ", i, ": fDist = ", fDist, ", fMinDist = ", fMinDist)
				ENDIF
			#ENDIF
			
			IF fDist < fMinDist
				fMinDist = fDist
				iClosestPoint = i
				
				#IF IS_DEBUG_BUILD
					IF bPrintDebug
						PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Point ", i, " is now the closest.")
					ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF bPrintDebug
					IF IS_BIT_SET(g_AsyncGridSpawnData.iUsedPointsBS[iArray], iBit)
						PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Point ", i, " was already checked.")
					ENDIF
					
					IF IS_BIT_SET(data.iExcludedPointsBS[iArray], iBit)
						PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Point ", i, " is excluded from the grid.")
					ENDIF
					
					IF IS_BIT_SET(g_AsyncGridSpawnData.iPointsUsedByOthersBS[ENUM_TO_INT(g_AsyncGridSpawnData.location)][iArray], iBit)
						PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Point ", i, " has been used by a different player.")
					ENDIF
					/*
					IF IS_BIT_SET(GlobalServerBD_BlockC.AsyncGroupWarpBD.iPointsUsed[ENUM_TO_INT(g_AsyncGridSpawnData.location)][iArray], iBit)
						PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Point ", i, " has been blocked on server.")
					ENDIF
					*/
				ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_NEXT_POINT_ID - Found closest point to grid origin: ", iClosestPoint, " with fMinDist: ", fMinDist)
	#ENDIF
	
	RETURN iClosestPoint
ENDFUNC

/// PURPOSE:
///    We can define angled zones where heading is overriden
/// PARAMS:
///    location - 
///    data - 
///    vPoint - 
/// RETURNS:
///    
FUNC FLOAT _GET_ASYNC_GRID_POINT_HEADING(ASYNC_GRID_SPAWN_LOCATIONS location, ASYNC_GRID_SPAWN_LOCATION_DETAILS &data, VECTOR &vPoint)
	VECTOR vPos1, vPos2
	INT i
	FLOAT fWidth, fHeading
	WHILE GET_ASYNC_GRID_LOCATION_HEADING_OVERRIDE_ZONE(location, i, vPos1, vPos2, fWidth, fHeading)
		IF IS_POINT_IN_ANGLED_AREA(vPoint, vPos1, vPos2, fWidth)
			#IF IS_DEBUG_BUILD
				PRINTLN("[ASYNC_GRID_WARP] _GET_ASYNC_GRID_POINT_HEADING - Point ", vPoint, " has an overriden heading from zone ", i)
			#ENDIF
			RETURN fHeading
		ENDIF
		i += 1
	ENDWHILE
	
	RETURN data.fGridHeading
ENDFUNC

FUNC SHAPETEST_INDEX _START_ASYNC_GRID_SHAPETEST(VECTOR vPoint, FLOAT fRadius, FLOAT fProbeHeight)
	VECTOR vEndPoint = vPoint + <<0.0, 0.0, fProbeHeight>>
	SHAPETEST_INDEX index = START_SHAPE_TEST_CAPSULE(vPoint, vEndPoint, fRadius, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED, NULL, 0)
	
	IF NATIVE_TO_INT(index) != 0
		PRINTLN("[ASYNC_GRID_WARP] _START_ASYNC_GRID_SHAPETEST - Started shaptest for point ", vPoint, " with radius ", fRadius)
	ELSE
		PRINTLN("[ASYNC_GRID_WARP] _START_ASYNC_GRID_SHAPETEST - Failed to start shapetest for point ", vPoint)
	ENDIF
	
	RETURN index
ENDFUNC

PROC _SWAP_IN_ASYNC_ARRAYS(FLOAT &fArray[], INT &iIndices[], INT a, INT b)
    FLOAT fTmp = fArray[a]
    fArray[a] = fArray[b]
    fArray[b] = fTmp
	
	INT iTmp = iIndices[a]
    iIndices[a] = iIndices[b]
    iIndices[b] = iTmp
ENDPROC

/// PURPOSE:
///    Insert sort for async grid points
/// PARAMS:
///    fDists - 
///    iIndices - 
///    iLength - 
PROC _INSERT_SORT_ASYNC_GRID(FLOAT &fDists[], INT &iIndices[], INT iLength)
	INT i, j
	FOR i = 1 TO iLength - 1
		j = i
		WHILE j > 0 AND fDists[j-1] > fDists[j]
			_SWAP_IN_ASYNC_ARRAYS(fDists, iIndices, j, j-1)
			j = j-1
		ENDWHILE
	ENDFOR
ENDPROC

PROC _FILL_ASYNC_GRID_POINTS_DIST_AND_INDICES(ASYNC_GRID_SPAWN_LOCATIONS location)
	INT i, iArray, iBit, iPointID
	ASYNC_GRID_SPAWN_LOCATION_DETAILS data
	data = GET_ASYNC_GRID_LOCATION_DETAILS(location)
	INT iPointsCount = IMIN(data.iGridWidth * data.iGridHeight, CI_ASYNC_GRID_SPAWN_MAX_POINTS)
	
	FLOAT fDist
	VECTOR vPoint
	INT iOriginPointID = data.iSpawnOriginPointID
	
	#IF IS_DEBUG_BUILD
	IF g_AsyncGridSpawnData.db_iOverwritePoint[ENUM_TO_INT(g_AsyncGridSpawnData.location)] > -1
		iOriginPointID = g_AsyncGridSpawnData.db_iOverwritePoint[ENUM_TO_INT(g_AsyncGridSpawnData.location)]
	ENDIF
	#ENDIF
			
	VECTOR vSpawnOrigin = _GET_ASYNC_GRID_POINT(data, iOriginPointID)
	
	REPEAT COUNT_OF(g_AsyncGridSpawnData.iPointsIndices) i
		g_AsyncGridSpawnData.iPointsIndices[i] = -1
		g_AsyncGridSpawnData.fPointsDist[i] = 9999.9
	ENDREPEAT
	
	i = 0
	REPEAT iPointsCount iPointID
		_GET_ASYNC_GRID_POINT_BIT(iPointID, iArray, iBit)
		
		vPoint = _GET_ASYNC_GRID_POINT(data, iPointID)
		fDist = VDIST(vPoint, vSpawnOrigin)
		
		g_AsyncGridSpawnData.fPointsDist[i] = fDist
		g_AsyncGridSpawnData.iPointsIndices[i] = iPointID
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _FILL_ASYNC_GRID_POINTS_DIST_AND_INDICES - fPointsDist[", i, "] = ", fDist, ", iPointsIndices[", i, "] = ", iPointID)
		#ENDIF
		
		i += 1
	ENDREPEAT
ENDPROC

PROC _START_ASYNC_GRID_WARP(ASYNC_GRID_SPAWN_LOCATIONS location)
	IF NOT g_AsyncGridSpawnData.bDoingWarp
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _START_ASYNC_GRID_WARP - Starting for location ", GET_ASYNC_GRID_SPAWN_LOCATION_NAME(location))
			g_AsyncGridSpawnData.db_processedLocation = location
			g_AsyncGridSpawnData.db_iProcessedPointsCount = 0
		#ENDIF
		BROADCAST_ASYNC_GRID_WARP_REQUEST(PLAYER_ID(), location)
		g_AsyncGridSpawnData.location = location
		g_AsyncGridSpawnData.bGotSpawnPoint = FALSE
		g_AsyncGridSpawnData.bDoingWarp = TRUE
		g_AsyncGridSpawnData.iCurrentShapeTestPoint = 0
		g_AsyncGridSpawnData.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
		g_AsyncGridSpawnData.iStartTime = GET_GAME_TIMER()
		//g_AsyncGridSpawnData.bDoingLoadScene = FALSE
		g_AsyncGridSpawnData.bGotNextPoint = FALSE
		
		IF g_AsyncGridSpawnData.bUsePresortedArray
			_FILL_ASYNC_GRID_POINTS_DIST_AND_INDICES(location)
			g_AsyncGridSpawnData.bSortDone = FALSE
			g_AsyncGridSpawnData.iCurrentSortedIndex = 0
		ENDIF
		
		REINIT_NET_TIMER(g_AsyncGridSpawnData.requestTimer)
		
		INT i
		REPEAT COUNT_OF(g_AsyncGridSpawnData.iUsedPointsBS) i
			g_AsyncGridSpawnData.iUsedPointsBS[i] = 0
		ENDREPEAT
		
		// If points used by others werent updated in the past 3s for that location then clear them
		IF HAS_NET_TIMER_EXPIRED(g_AsyncGridSpawnData.timerPointsUsedByOthers[ENUM_TO_INT(location)], 3000)
			REPEAT 8 i
				g_AsyncGridSpawnData.iPointsUsedByOthersBS[ENUM_TO_INT(g_AsyncGridSpawnData.location)][i] = 0
			ENDREPEAT
			#IF IS_DEBUG_BUILD
				PRINTLN("[ASYNC_GRID_WARP] _START_ASYNC_GRID_WARP - iPointsUsedByOthersBS is out of date so clearing!")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC _STOP_ASYNC_GRID_WARP()
	IF g_AsyncGridSpawnData.bDoingWarp
		BROADCAST_ASYNC_GRID_WARP_FINISH(PLAYER_ID(), g_AsyncGridSpawnData.location, g_AsyncGridSpawnData.iCurrentShapeTestPoint)
		g_AsyncGridSpawnData.bDoingWarp = FALSE
		g_AsyncGridSpawnData.iCurrentShapeTestPoint = 0
		g_AsyncGridSpawnData.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
		g_AsyncGridSpawnData.iEndTime = GET_GAME_TIMER()
		//g_AsyncGridSpawnData.bDoingLoadScene = FALSE
		g_AsyncGridSpawnData.bGotNextPoint = FALSE
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _STOP_ASYNC_GRID_WARP - Stopped, total time: ", g_AsyncGridSpawnData.iEndTime - g_AsyncGridSpawnData.iStartTime)
		#ENDIF
	ENDIF
ENDPROC

PROC _DO_ASYNC_GRID_WARP_FALLBACK(ASYNC_GRID_SPAWN_LOCATION_DETAILS &data)
	IF g_AsyncGridSpawnData.bDoingWarp
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] _DO_ASYNC_GRID_WARP_FALLBACK - Falling back to grid origin.")
		#ENDIF
		g_AsyncGridSpawnData.bGotSpawnPoint = TRUE
		g_AsyncGridSpawnData.vSpawnPoint = _GET_ASYNC_GRID_POINT(data, data.iSpawnOriginPointID)
		g_AsyncGridSpawnData.iCurrentShapeTestPoint = data.iSpawnOriginPointID
	ENDIF
ENDPROC

/// PURPOSE:
///    If we're not granted the right to warp resend the request to server
PROC _MAINTAIN_ASYNC_GRID_WARP_REQUEST_RESENDING()
	IF g_AsyncGridSpawnData.bDoingWarp
		IF g_AsyncGridSpawnData.location != GRID_SPAWN_LOCATION_INVALID
			IF HAS_NET_TIMER_EXPIRED(g_AsyncGridSpawnData.requestTimer, 3000)
			AND GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[g_AsyncGridSpawnData.location] != PLAYER_ID()
				// It's been 3s since our request and we still havent gotten chance to do the warp, resend the request
				#IF IS_DEBUG_BUILD
					PRINTLN("[ASYNC_GRID_WARP] _MAINTAIN_ASYNC_GRID_WARP_REQUEST_RESENDING - Our request timed out, resending!")
				#ENDIF
				BROADCAST_ASYNC_GRID_WARP_REQUEST(PLAYER_ID(), g_AsyncGridSpawnData.location)
				REINIT_NET_TIMER(g_AsyncGridSpawnData.requestTimer)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				ASSERTLN("[ASYNC_GRID_WARP] _MAINTAIN_ASYNC_GRID_WARP_REQUEST_RESENDING - We're warping but location is invalid!")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL _IS_ANY_PLAYER_IN_GIVEN_PROBE(VECTOR vCoords, FLOAT fRadius, FLOAT fHeight)
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP] _IS_ANY_PLAYER_IN_GIVEN_PROBE - Checking for probe ", vCoords, ", fRadius = ", fRadius, ", fHeight = ", fHeight)
	#ENDIF
	
	INT i
	PLAYER_INDEX playerID
	VECTOR vPlayerPos, vFlatPlayerPos
	VECTOR vFlatProbeCoords = vCoords
	vFlatProbeCoords.Z = 0.0
	FLOAT fRadius2 = fRadius * fRadius
	
	REPEAT NUM_NETWORK_REAL_PLAYERS() i
		playerID = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(playerID)
			vPlayerPos = GET_PLAYER_COORDS(playerID)
			vFlatPlayerPos = vPlayerPos
			vFlatPlayerPos.Z = 0.0
			
			IF VDIST2(vFlatPlayerPos, vFlatProbeCoords) < fRadius2
				IF vPlayerPos.Z >= vCoords.Z - fRadius AND vPlayerPos.Z < vCoords.Z + fHeight + fRadius
					#IF IS_DEBUG_BUILD
						PRINTLN("[ASYNC_GRID_WARP] _IS_ANY_PLAYER_IN_GIVEN_PROBE - Yeah, player ", GET_PLAYER_NAME(playerID), " is in this probe.")
					#ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP] _IS_ANY_PLAYER_IN_GIVEN_PROBE - No hits.")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_ASYNC_GRID_WARP_TO_LOCATION(ASYNC_GRID_SPAWN_LOCATIONS location, BOOL bDoQuickWarp = FALSE)
	IF location = GRID_SPAWN_LOCATION_INVALID
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Called with invalid location!")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	INT iArray, iBit
	
	IF NOT g_AsyncGridSpawnData.bDoingWarp
		_START_ASYNC_GRID_WARP(location)
	ELSE
	
		_MAINTAIN_ASYNC_GRID_WARP_REQUEST_RESENDING()
		
		IF g_AsyncGridSpawnData.bUsePresortedArray
			IF NOT g_AsyncGridSpawnData.bSortDone
				#IF IS_DEBUG_BUILD
					PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Sorting hasn't been done yet, running _INSERT_SORT_ASYNC_GRID.")
				#ENDIF
				ASYNC_GRID_SPAWN_LOCATION_DETAILS data
				data = GET_ASYNC_GRID_LOCATION_DETAILS(location)
				INT iPointsCount = IMIN(data.iGridWidth * data.iGridHeight, CI_ASYNC_GRID_SPAWN_MAX_POINTS)
				_INSERT_SORT_ASYNC_GRID(g_AsyncGridSpawnData.fPointsDist, g_AsyncGridSpawnData.iPointsIndices, iPointsCount)
				g_AsyncGridSpawnData.bSortDone = TRUE
				
				#IF IS_DEBUG_BUILD
					IF g_AsyncGridSpawnData.db_bGridCreatorActive
					OR g_AsyncGridSpawnData.db_bDrawPoints
						INT i
						REPEAT 256 i
							PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - iPointsIndices[", i, "] = ", g_AsyncGridSpawnData.iPointsIndices[i], " --- fPointsDist[", i, "] = ", g_AsyncGridSpawnData.fPointsDist[i])
						ENDREPEAT
					ENDIF
				#ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	
		IF GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[location] = PLAYER_ID()
			// It's our turn, let's do it
			ASYNC_GRID_SPAWN_LOCATION_DETAILS data
			data = GET_ASYNC_GRID_LOCATION_DETAILS(location)
			
			// No shape test is running, start a new one
			IF NOT g_AsyncGridSpawnData.bGotSpawnPoint
			AND NATIVE_TO_INT(g_AsyncGridSpawnData.currentShapeTest) = 0
			
				IF NOT g_AsyncGridSpawnData.bGotNextPoint
					IF g_AsyncGridSpawnData.bUsePresortedArray
						g_AsyncGridSpawnData.iCurrentShapeTestPoint = _GET_ASYNC_GRID_NEXT_SORTED_POINT_ID(data)
					ELSE
						g_AsyncGridSpawnData.iCurrentShapeTestPoint = _GET_ASYNC_GRID_NEXT_POINT_ID(data)
					ENDIF
					
					IF g_AsyncGridSpawnData.iCurrentShapeTestPoint > -1
						g_AsyncGridSpawnData.vSpawnPoint = _GET_ASYNC_GRID_POINT(data, g_AsyncGridSpawnData.iCurrentShapeTestPoint)
						g_AsyncGridSpawnData.bGotNextPoint = TRUE
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Got point to run shapetest on.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							IF g_AsyncGridSpawnData.bUsePresortedArray
								PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - _GET_ASYNC_GRID_NEXT_SORTED_POINT_ID returned -1, oops! Must have run out of points. Gonna fallback to origin of grid.")
							ELSE
								PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - _GET_ASYNC_GRID_NEXT_POINT_ID returned -1, oops! Must have run out of points. Gonna fallback to origin of grid.")
							ENDIF
						#ENDIF
						
						_DO_ASYNC_GRID_WARP_FALLBACK(data)
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF g_AsyncGridSpawnData.bGotNextPoint
					/*
					BOOL bCanStartShapetest
					IF g_AsyncGridSpawnData.bLoadScenePerProbe
						IF NOT g_AsyncGridSpawnData.bDoingLoadScene
							IF NEW_LOAD_SCENE_START_SPHERE(g_AsyncGridSpawnData.vSpawnPoint, data.fProbeRadius, NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
								g_AsyncGridSpawnData.bDoingLoadScene = TRUE
								#IF IS_DEBUG_BUILD
									PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Starting NEW_LOAD_SCENE for current probe.")
								#ENDIF
							ENDIF
						ENDIF
						
						IF g_AsyncGridSpawnData.bDoingLoadScene
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								#IF IS_DEBUG_BUILD
									PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Waiting for NEW_LOAD_SCENE_TO_FINISH")
								#ENDIF
								IF IS_NEW_LOAD_SCENE_LOADED()
									#IF IS_DEBUG_BUILD
										PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - NEW_LOAD_SCENE_TO_FINISH is finished, stopping!")
									#ENDIF
									bCanStartShapetest = TRUE
									NEW_LOAD_SCENE_STOP()
								ENDIF
							ELSE
								bCanStartShapetest = TRUE
							ENDIF
						ENDIF
					ELSE
						bCanStartShapetest = TRUE
					ENDIF
					*/
					//IF bCanStartShapetest
					
					IF NOT _IS_ANY_PLAYER_IN_GIVEN_PROBE(g_AsyncGridSpawnData.vSpawnPoint, data.fProbeRadius, data.fProbeHeight)
						g_AsyncGridSpawnData.currentShapeTest = _START_ASYNC_GRID_SHAPETEST(g_AsyncGridSpawnData.vSpawnPoint, data.fProbeRadius, data.fProbeHeight)
						IF NATIVE_TO_INT(g_AsyncGridSpawnData.currentShapeTest) != 0
							_GET_ASYNC_GRID_POINT_BIT(g_AsyncGridSpawnData.iCurrentShapeTestPoint, iArray, iBit)
							// Mark this point as used
							SET_BIT(g_AsyncGridSpawnData.iUsedPointsBS[iArray], iBit)
						ENDIF
					ELSE
						// We hit a player wihtout doing shapetest, move to the next point
						g_AsyncGridSpawnData.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
						g_AsyncGridSpawnData.bGotNextPoint = FALSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - We found a player in that probe, skipping.")
						#ENDIF
					ENDIF
					//ENDIF
				ENDIF
			ENDIF
			
			IF NOT g_AsyncGridSpawnData.bGotSpawnPoint
				IF NATIVE_TO_INT(g_AsyncGridSpawnData.currentShapeTest) != 0
					INT iHitSomething
					VECTOR vHitPos, vHitNormal
					ENTITY_INDEX entityHit
					
					SHAPETEST_STATUS status = GET_SHAPE_TEST_RESULT(g_AsyncGridSpawnData.currentShapeTest, iHitSomething, vHitPos, vHitNormal, entityHit)
					
					IF status = SHAPETEST_STATUS_RESULTS_READY
						IF iHitSomething > 0
							#IF IS_DEBUG_BUILD
								PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Got result for shapetest for point ", g_AsyncGridSpawnData.iCurrentShapeTestPoint, " the result is - hit: ", iHitSomething, ", hit pos: ", vHitPos, ", hit normal: ", vHitNormal, ", hit entity: ", NATIVE_TO_INT(entityHit))
								g_AsyncGridSpawnData.db_iProcessedPointsCount += 1
							#ENDIF
							
							// Move on to the next point next frame
							g_AsyncGridSpawnData.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
							//g_AsyncGridSpawnData.bDoingLoadScene = FALSE
							g_AsyncGridSpawnData.bGotNextPoint = FALSE
							
							RETURN FALSE
						ELSE
							// WE'VE GOT A POINT
							#IF IS_DEBUG_BUILD
								INT iTotalTime = GET_GAME_TIMER() - g_AsyncGridSpawnData.iStartTime
								PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Got result for shapetest for point ", g_AsyncGridSpawnData.iCurrentShapeTestPoint, " (coords ", g_AsyncGridSpawnData.vSpawnPoint, ")- didn't hit anything! That took us ", iTotalTime)
							#ENDIF
							
							g_AsyncGridSpawnData.bGotSpawnPoint = TRUE
						ENDIF
						
					ELIF status = SHAPETEST_STATUS_NONEXISTENT
						#IF IS_DEBUG_BUILD
							PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Shapetest for point ", g_AsyncGridSpawnData.iCurrentShapeTestPoint, " is not existent, wtf? resetting...")
						#ENDIF
						g_AsyncGridSpawnData.currentShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					ENDIF
				ENDIF
			ENDIF
			
			// Got spawn point
			IF g_AsyncGridSpawnData.bGotSpawnPoint
				VECTOR vFinalSpawn = g_AsyncGridSpawnData.vSpawnPoint
				FLOAT fFinalZ
				FLOAT fHeading = _GET_ASYNC_GRID_POINT_HEADING(g_AsyncGridSpawnData.location, data, g_AsyncGridSpawnData.vSpawnPoint)
				
				IF GET_GROUND_Z_FOR_3D_COORD(g_AsyncGridSpawnData.vSpawnPoint, fFinalZ)
					vFinalSpawn.Z = fFinalZ
				ENDIF
				
				IF NET_WARP_TO_COORD(vFinalSpawn, fHeading, FALSE, FALSE, FALSE, FALSE, TRUE, bDoQuickWarp, TRUE, TRUE)
					// Finished warp
					_STOP_ASYNC_GRID_WARP()
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[location] != INVALID_PLAYER_INDEX()
					PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Waiting for our turn to spawn to chosen location, currently allowed player is: ", GET_PLAYER_NAME(GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[location]))
				ELSE
					PRINTLN("[ASYNC_GRID_WARP] DO_ASYNC_GRID_WARP_TO_LOCATION - Waiting for our turn to spawn to chosen location, currently nobody is allowed (INVALID PLAYER)")
				ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT _GET_ORDER_SLOT_OF_PLAYER(PLAYER_INDEX player, ASYNC_GRID_SPAWN_LOCATIONS location)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[ENUM_TO_INT(location)][i] = player
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC


#IF IS_DEBUG_BUILD
PROC _PRINT_ASYNC_ORDER_QUEUE_DEBUG(ASYNC_GRID_SPAWN_LOCATIONS location)
	IF location = GRID_SPAWN_LOCATION_INVALID
		EXIT
	ENDIF
	
	INT iLocation = ENUM_TO_INT(location)
	
	PRINTLN("[ASYNC_GRID_WARP][SERVER] _PRINT_ASYNC_ORDER_QUEUE_DEBUG - Queue for ", GET_ASYNC_GRID_SPAWN_LOCATION_NAME(location))
	PRINTLN("[ASYNC_GRID_WARP][SERVER] _PRINT_ASYNC_ORDER_QUEUE_DEBUG - Length ", GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation])
	INT i
	REPEAT GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] i
		PRINTLN("[ASYNC_GRID_WARP][SERVER] _PRINT_ASYNC_ORDER_QUEUE_DEBUG - Slot[", i, "] = ", GET_PLAYER_NAME(GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][i]), " (", NATIVE_TO_INT(GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][i]), ")")
	ENDREPEAT
ENDPROC
#ENDIF

FUNC BOOL ASYNC_ORDER_QUEUE_DOES_EXIST(PLAYER_INDEX player, ASYNC_GRID_SPAWN_LOCATIONS location)
	IF location = GRID_SPAWN_LOCATION_INVALID
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_DOES_EXIST - Called for location ", GET_ASYNC_GRID_SPAWN_LOCATION_NAME(location))
	#ENDIF
	
	INT iLocation = ENUM_TO_INT(location)
	
	INT i
	REPEAT GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] i
		IF GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][i] = player
			#IF IS_DEBUG_BUILD
				PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_DOES_EXIST - Yeah, at slot ", i)
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ASYNC_ORDER_QUEUE_PUSH(PLAYER_INDEX player, ASYNC_GRID_SPAWN_LOCATIONS location)
	IF location = GRID_SPAWN_LOCATION_INVALID
		RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_PUSH - Called for location ", GET_ASYNC_GRID_SPAWN_LOCATION_NAME(location), " and player ", GET_PLAYER_NAME(player))
	#ENDIF
	
	INT iLocation = ENUM_TO_INT(location)
	
	// Check if player isn't already in the queue
	IF ASYNC_ORDER_QUEUE_DOES_EXIST(player, location)
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_PUSH - Player already in the queue")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// Check if there's space in queue
	IF GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] = NUM_NETWORK_PLAYERS
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_PUSH - Not enough space in the queue!")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation]] = player
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_PUSH - Pushed player ", GET_PLAYER_NAME(player), " at queue position ", GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation])
	#ENDIF
	
	GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] += 1
	
	#IF IS_DEBUG_BUILD
		_PRINT_ASYNC_ORDER_QUEUE_DEBUG(location)
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC PLAYER_INDEX ASYNC_ORDER_QUEUE_POP(ASYNC_GRID_SPAWN_LOCATIONS location)
	IF location = GRID_SPAWN_LOCATION_INVALID
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_POP - Called for location ", GET_ASYNC_GRID_SPAWN_LOCATION_NAME(location))
	#ENDIF
	
	INT iLocation = ENUM_TO_INT(location)
	
	IF GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] = 0
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_POP - Queue is empty!")
		#ENDIF
		RETURN INVALID_PLAYER_INDEX()
	ENDIF
	
	PLAYER_INDEX pPopped = GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][0]
	GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] -= 1
	
	IF GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation] > 0
		INT i
		REPEAT (GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[iLocation]) i
			GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][i] = GlobalServerBD_BlockC.AsyncGroupWarpBD.pWarpOrder[iLocation][i+1]
		ENDREPEAT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_POP - Popped player ", GET_PLAYER_NAME(pPopped))
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		_PRINT_ASYNC_ORDER_QUEUE_DEBUG(location)
	#ENDIF
	
	RETURN pPopped
ENDFUNC

FUNC INT ASYNC_ORDER_QUEUE_LENGTH(ASYNC_GRID_SPAWN_LOCATIONS location)
	IF location = GRID_SPAWN_LOCATION_INVALID
		#IF IS_DEBUG_BUILD
			PRINTLN("[ASYNC_GRID_WARP][SERVER] ASYNC_ORDER_QUEUE_LENGTH - Called for invalid location!")
		#ENDIF
		RETURN 0
	ENDIF
	
	RETURN GlobalServerBD_BlockC.AsyncGroupWarpBD.iQueueLength[ENUM_TO_INT(location)]
ENDFUNC

/// PURPOSE:
///    Main server proc
PROC MAINTAIN_ASYNC_GRID_WARP_SERVER()
	//This is now checked immediatley prior to calling this function
	//IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
	
	#IF IS_DEBUG_BUILD
	IF g_AsyncGridSpawnData.db_bQueueTestIsOn	
		EXIT
	ENDIF
	#ENDIF
	
	INT iLocation // , iPlayer
	ASYNC_GRID_SPAWN_LOCATIONS location
	
	REPEAT COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS) iLocation
		
		location = INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, iLocation)
		PLAYER_INDEX pWarping = GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[iLocation]
		
		IF ASYNC_ORDER_QUEUE_LENGTH(location) > 0
		AND pWarping = INVALID_PLAYER_INDEX()
		
			PLAYER_INDEX pCurrent = ASYNC_ORDER_QUEUE_POP(location)
			
			/*
			IF pCurrent = INVALID_PLAYER_INDEX()
			OR NOT IS_NET_PLAYER_OK(pCurrent)
				// Dind't pick anything from the order queue... Pick anyone
				REPEAT NUM_NETWORK_REAL_PLAYERS() iPlayer
					pCurrent = INT_TO_PLAYERINDEX(iPlayer)
					
					IF IS_BIT_SET(GlobalServerBD_BlockC.AsyncGroupWarpBD.iPlayersRequestingWarpsBS[iLocation], iPlayer)
					AND IS_NET_PLAYER_OK(pCurrent)
						// This player is requesting warp, give him warping, no fancy ordering here
						BREAKLOOP
					ELSE
						pCurrent = INVALID_PLAYER_INDEX()
					ENDIF
				ENDREPEAT
			ENDIF
			*/
			
			IF pCurrent != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(pCurrent)
				// TODO: Add some checks to see if this player can be warped
				SET_BIT(GlobalServerBD_BlockC.AsyncGroupWarpBD.iPlayersRequestingWarpsBS[iLocation], NATIVE_TO_INT(pCurrent))
				GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[iLocation] = pCurrent
				GlobalServerBD_BlockC.AsyncGroupWarpBD.timeCurrentWarperStart[iLocation] = GET_NETWORK_TIME()
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[ASYNC_GRID_WARP][SERVER] MAINTAIN_ASYNC_GRID_WARP_SERVER - Letting player ", GET_PLAYER_NAME(pCurrent), " do the warp.")
				#ENDIF
			ENDIF
			
		ELSE
			IF pWarping != INVALID_PLAYER_INDEX()
				// Check if that player is still warping
				BOOL bFinishedWarp, bWarperNotOK, bWarperTimedOut
				
				bFinishedWarp = NOT IS_BIT_SET(GlobalServerBD_BlockC.AsyncGroupWarpBD.iPlayersRequestingWarpsBS[iLocation], NATIVE_TO_INT(pWarping))
				IF NOT bFinishedWarp
					bWarperNotOK = NOT IS_NET_PLAYER_OK(pWarping)
					
					IF NOT bWarperNotOK
						bWarperTimedOut = ABSI(GET_TIME_DIFFERENCE(GlobalServerBD_BlockC.AsyncGroupWarpBD.timeCurrentWarperStart[iLocation], GET_NETWORK_TIME())) > 4000
					ENDIF
				ENDIF
				
				IF bFinishedWarp 
				OR bWarperNotOK 
				OR bWarperTimedOut 
					
					GlobalServerBD_BlockC.AsyncGroupWarpBD.pAllowedWarpers[iLocation] = INVALID_PLAYER_INDEX()
					
					#IF IS_DEBUG_BUILD
						IF bFinishedWarp
							PRINTLN("[ASYNC_GRID_WARP][SERVER] MAINTAIN_ASYNC_GRID_WARP_SERVER - Player ", GET_PLAYER_NAME(pWarping), " no longer wants to do warp i.e. probably finished, clearing their turn.")
						ENDIF
						
						IF bWarperNotOK
							PRINTLN("[ASYNC_GRID_WARP][SERVER] MAINTAIN_ASYNC_GRID_WARP_SERVER - Player ", GET_PLAYER_NAME(pWarping), " is no longer OK, removing them from queue!")
						ENDIF
						
						IF bWarperTimedOut
							PRINTLN("[ASYNC_GRID_WARP][SERVER] MAINTAIN_ASYNC_GRID_WARP_SERVER - Player ", GET_PLAYER_NAME(pWarping), " timed out!")
						ENDIF
					#ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_ASYNC_GRID_EVENT_WARP()
	IF g_AsyncGridSpawnData.bDoEventWarp
		IF DO_ASYNC_GRID_WARP_TO_LOCATION(g_AsyncGridSpawnData.locationFromEvent)
			g_AsyncGridSpawnData.bDoEventWarp = FALSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[ASYNC_GRID_WARP] MAINTAIN_ASYNC_GRID_EVENT_WARP - Done!")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_ASYNC_GRID_WARP_DEBUG_WIDGETS()

	// Default values for creator grid
	g_AsyncGridSpawnData.db_gridCreator.iGridWidth = 3
	g_AsyncGridSpawnData.db_gridCreator.iGridHeight = 3
	g_AsyncGridSpawnData.db_gridCreator.fSpacingX = 0.1
	g_AsyncGridSpawnData.db_gridCreator.fSpacingY = 0.1
	g_AsyncGridSpawnData.db_gridCreator.fOffsetZ = 0.1
	g_AsyncGridSpawnData.db_gridCreator.fProbeRadius = 0.1
	
	INT i
	REPEAT COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS) i
		g_AsyncGridSpawnData.db_iOverwritePoint[i] = -1
	ENDREPEAT

	START_WIDGET_GROUP("ASYNC GRID WARP")
		ADD_WIDGET_BOOL("Creator active", g_AsyncGridSpawnData.db_bGridCreatorActive)
		START_WIDGET_GROUP("Grid creator")
			ADD_WIDGET_FLOAT_SLIDER("Grid heading", g_AsyncGridSpawnData.db_gridCreator.fGridHeading, 0.0, 360.0, 0.01)
			ADD_WIDGET_INT_SLIDER("Grid width", g_AsyncGridSpawnData.db_gridCreator.iGridWidth, 1, 100, 1)
			ADD_WIDGET_INT_SLIDER("Grid height", g_AsyncGridSpawnData.db_gridCreator.iGridHeight, 1, 100, 1)
			ADD_WIDGET_FLOAT_SLIDER("Spacing X", g_AsyncGridSpawnData.db_gridCreator.fSpacingX, 0.1, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Spacing Y", g_AsyncGridSpawnData.db_gridCreator.fSpacingY, 0.1, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Offset Z", g_AsyncGridSpawnData.db_gridCreator.fOffsetZ, 0.0, 100.0, 0.01)
			ADD_WIDGET_INT_SLIDER("Spawn origin", g_AsyncGridSpawnData.db_gridCreator.iSpawnOriginPointID, 0, CI_ASYNC_GRID_SPAWN_MAX_POINTS - 1, 1)
			ADD_WIDGET_FLOAT_SLIDER("Probe height", g_AsyncGridSpawnData.db_gridCreator.fProbeHeight, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Probe radius", g_AsyncGridSpawnData.db_gridCreator.fProbeRadius, 0.1, 10.0, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Start coord", g_AsyncGridSpawnData.db_gridCreator.vStartCoord, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_BOOL("(grab player pos as start)", g_AsyncGridSpawnData.db_bGrabPlayerPosForCreatorStart)
			
			ADD_WIDGET_BOOL("Humanise pos", g_AsyncGridSpawnData.db_gridCreator.bHumanizePos)
			ADD_WIDGET_FLOAT_SLIDER("fSinSpread", g_AsyncGridSpawnData.db_gridCreator.fSinSpread, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fSinAmp", g_AsyncGridSpawnData.db_gridCreator.fSinAmp, -100.0, 100.0, 0.01)
			
			g_AsyncGridSpawnData.db_textWidgetID = ADD_TEXT_WIDGET("Location name")
			ADD_WIDGET_BOOL("Dump grid", g_AsyncGridSpawnData.db_bDumpCreatedGrid)
			ADD_WIDGET_BOOL("Clear excluded", g_AsyncGridSpawnData.db_bClearCreatorExcludedPoints)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("Draw points", g_AsyncGridSpawnData.db_bDrawPoints)
		ADD_WIDGET_BOOL("Use precomputed distances", g_AsyncGridSpawnData.bUsePresortedArray)
		
		TEXT_LABEL_63 txtTitle
		REPEAT COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS) i
			txtTitle = GET_ASYNC_GRID_SPAWN_LOCATION_NAME(INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, i))
			txtTitle += " ["
			txtTitle += i
			txtTitle += "]"
			START_WIDGET_GROUP(txtTitle)
				ADD_WIDGET_BOOL("Start warp to location", g_AsyncGridSpawnData.db_bStartWarp[i])
				ADD_WIDGET_INT_SLIDER("Overwrite point", g_AsyncGridSpawnData.db_iOverwritePoint[i], -1, CI_ASYNC_GRID_SPAWN_MAX_POINTS - 1, 1)
				ADD_WIDGET_BOOL("Load to creator", g_AsyncGridSpawnData.db_bLoadGridToCreator[i])
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
		START_WIDGET_GROUP("GROUP WARP TEST")
			ADD_WIDGET_INT_SLIDER("Location", g_AsyncGridSpawnData.db_iGroupWarpLocation, 0, COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS), 1)
			REPEAT NUM_NETWORK_PLAYERS i
				txtTitle = "Place["
				txtTitle += i
				txtTitle += "]"
				g_AsyncGridSpawnData.db_iGroupPlaces[i] = -1
				ADD_WIDGET_INT_SLIDER(txtTitle, g_AsyncGridSpawnData.db_iGroupPlaces[i], -1, NUM_NETWORK_PLAYERS, 1)
			ENDREPEAT
			ADD_WIDGET_BOOL("Start group warp", g_AsyncGridSpawnData.db_bStartGroupWarp)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("QUEUE TEST")
			ADD_WIDGET_BOOL("Enable", g_AsyncGridSpawnData.db_bQueueTestIsOn)
			ADD_WIDGET_INT_SLIDER("Push player ID", g_AsyncGridSpawnData.db_iQueuePlayerToPush, 0, NUM_NETWORK_PLAYERS, 1) 
			ADD_WIDGET_BOOL("Push", g_AsyncGridSpawnData.db_bQueueTestPush)
			ADD_WIDGET_BOOL("Pop", g_AsyncGridSpawnData.db_bQueueTestPop)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	SET_CONTENTS_OF_TEXT_WIDGET(g_AsyncGridSpawnData.db_textWidgetID, "")
ENDPROC

PROC MAINTAIN_ASYNC_GRID_WARP_DEBUG_WIDGETS()
	INT i
	
	IF g_AsyncGridSpawnData.db_bQueueTestPush
		ASYNC_ORDER_QUEUE_PUSH(INT_TO_PLAYERINDEX(g_AsyncGridSpawnData.db_iQueuePlayerToPush), _DBG_GRID_SPAWN_LOCATION_CREATOR)
		IF g_AsyncGridSpawnData.db_iQueuePlayerToPush < NUM_NETWORK_PLAYERS
			g_AsyncGridSpawnData.db_iQueuePlayerToPush += 1
		ENDIF
		g_AsyncGridSpawnData.db_bQueueTestPush = FALSE
	ENDIF
	
	IF g_AsyncGridSpawnData.db_bQueueTestPop
		ASYNC_ORDER_QUEUE_POP(_DBG_GRID_SPAWN_LOCATION_CREATOR)
		g_AsyncGridSpawnData.db_bQueueTestPop = FALSE
	ENDIF
	
	IF g_AsyncGridSpawnData.db_bGridCreatorActive
		INT iPointsCount = g_AsyncGridSpawnData.db_gridCreator.iGridWidth * g_AsyncGridSpawnData.db_gridCreator.iGridHeight
		INT iArray, iBit
		
		VECTOR vPoint
		
		// Mouse picking points
		IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_LEFT_BTN)
			VECTOR vMouse2DPos, vPoint2DPos
			FLOAT fMinDist, fCurrentDist
			INT iClosestPoint = -1
			
			GET_MOUSE_POSITION(vMouse2DPos.x, vMouse2DPos.y)
			fMinDist = 99999.9
			
			REPEAT iPointsCount i
				vPoint = _GET_ASYNC_GRID_POINT(g_AsyncGridSpawnData.db_gridCreator, i)
				GET_SCREEN_COORD_FROM_WORLD_COORD(vPoint, vPoint2DPos.x, vPoint2DPos.y)
				fCurrentDist = VDIST(vMouse2DPos, vPoint2DPos)
				IF fCurrentDist < 0.02
				AND fCurrentDist < fMinDist
					fMinDist = fCurrentDist
					iClosestPoint = i
				ENDIF
			ENDREPEAT
			
			IF iClosestPoint > -1
				_GET_ASYNC_GRID_POINT_BIT(iClosestPoint, iArray, iBit)
				IF NOT IS_BIT_SET(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[iArray], iBit)
					SET_BIT(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[iArray], iBit)
				ELSE
					CLEAR_BIT(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[iArray], iBit)
				ENDIF
			ENDIF
		ENDIF
		
		// Draw points
		TEXT_LABEL_15 txtPointNumber
		REPEAT iPointsCount i
			vPoint = _GET_ASYNC_GRID_POINT(g_AsyncGridSpawnData.db_gridCreator, i)
			_GET_ASYNC_GRID_POINT_BIT(i, iArray, iBit)
			txtPointNumber = "["
			txtPointNumber += i
			txtPointNumber += "]"
			DRAW_DEBUG_TEXT(txtPointNumber, vPoint, 255, 255, 255, 255)
			IF IS_BIT_SET(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[iArray], iBit)
				DRAW_DEBUG_SPHERE(vPoint, g_AsyncGridSpawnData.db_gridCreator.fProbeRadius, 255, 0, 128, 128)
			ELSE
				IF g_AsyncGridSpawnData.db_gridCreator.iSpawnOriginPointID = i
					DRAW_DEBUG_SPHERE(vPoint, g_AsyncGridSpawnData.db_gridCreator.fProbeRadius, 0, 255, 255, 128)
					DRAW_DEBUG_SPHERE(vPoint + <<0.0, 0.0, g_AsyncGridSpawnData.db_gridCreator.fProbeHeight + g_AsyncGridSpawnData.db_gridCreator.fProbeRadius>>, 0.1, 0, 255, 255, 255)
				ELSE
					DRAW_DEBUG_SPHERE(vPoint, g_AsyncGridSpawnData.db_gridCreator.fProbeRadius, 0, 255, 128, 128)
					DRAW_DEBUG_SPHERE(vPoint + <<0.0, 0.0, g_AsyncGridSpawnData.db_gridCreator.fProbeHeight + g_AsyncGridSpawnData.db_gridCreator.fProbeRadius>>, 0.1, 0, 255, 128, 255)
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF g_AsyncGridSpawnData.db_bGrabPlayerPosForCreatorStart
			g_AsyncGridSpawnData.db_gridCreator.vStartCoord = GET_PLAYER_COORDS(PLAYER_ID())
			g_AsyncGridSpawnData.db_bGrabPlayerPosForCreatorStart = FALSE
		ENDIF
		
		IF g_AsyncGridSpawnData.db_bClearCreatorExcludedPoints
			REPEAT COUNT_OF(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS) i
				g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[i] = 0
			ENDREPEAT
			g_AsyncGridSpawnData.db_bClearCreatorExcludedPoints = FALSE
		ENDIF
	ELSE
		/*
		IF g_AsyncGridSpawnData.db_bDrawPoints
			IF g_AsyncGridSpawnData.db_processedLocation != GRID_SPAWN_LOCATION_INVALID
				ASYNC_GRID_SPAWN_LOCATION_DETAILS data
				data = GET_ASYNC_GRID_LOCATION_DETAILS(g_AsyncGridSpawnData.db_processedLocation)
				
				VECTOR vPoint
				REPEAT g_AsyncGridSpawnData.db_iProcessedPointsCount i
					vPoint = _GET_ASYNC_GRID_POINT(data, i)
					IF data.iSpawnOriginPointID = i
						DRAW_DEBUG_SPHERE(vPoint, data.fProbeRadius, 255, 255, 128, 128)
					ELSE
						DRAW_DEBUG_SPHERE(vPoint, data.fProbeRadius, 255, 0, 128, 128)
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		*/
	ENDIF
	
	REPEAT COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS) i
		IF g_AsyncGridSpawnData.db_bStartWarp[i]
			IF DO_ASYNC_GRID_WARP_TO_LOCATION(INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, i))
				g_AsyncGridSpawnData.db_bStartWarp[i] = FALSE
			ENDIF
		ENDIF
		
		IF g_AsyncGridSpawnData.db_bLoadGridToCreator[i]
			g_AsyncGridSpawnData.db_gridCreator = GET_ASYNC_GRID_LOCATION_DETAILS(INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, i))
			SET_CONTENTS_OF_TEXT_WIDGET(g_AsyncGridSpawnData.db_textWidgetID, GET_ASYNC_GRID_SPAWN_LOCATION_NAME(INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, i)))
			g_AsyncGridSpawnData.db_bLoadGridToCreator[i] = FALSE
		ENDIF
	ENDREPEAT
	
	// Dump
	IF g_AsyncGridSpawnData.db_bDumpCreatedGrid
		INT j
		STRING locationName = GET_CONTENTS_OF_TEXT_WIDGET(g_AsyncGridSpawnData.db_textWidgetID)
		TEXT_LABEL_15 textIndent = ""
		
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		
		IF NOT IS_STRING_NULL_OR_EMPTY(locationName)
			SAVE_STRING_TO_DEBUG_FILE("//════════════════════════════════════════════════════════════════════") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_STRING_TO_DEBUG_FILE(locationName) SAVE_NEWLINE_TO_DEBUG_FILE()
			textIndent = "	"
		ENDIF
		
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.vStartCoord = ") 			SAVE_VECTOR_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.vStartCoord) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fSpacingX = ") 			SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fSpacingX) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fSpacingY = ") 			SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fSpacingY) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fOffsetZ = ") 				SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fOffsetZ) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fProbeRadius = ") 			SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fProbeRadius) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fGridHeading = ") 			SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fGridHeading) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.iGridWidth = ") 			SAVE_INT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.iGridWidth) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.iGridHeight = ") 			SAVE_INT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.iGridHeight) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.iSpawnOriginPointID = ") 	SAVE_INT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.iSpawnOriginPointID) SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fProbeHeight = ") 			SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fProbeHeight) SAVE_NEWLINE_TO_DEBUG_FILE()
		
		IF g_AsyncGridSpawnData.db_gridCreator.bHumanizePos
			SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.bHumanizePos = TRUE") SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fSinSpread = ") 	SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fSinSpread) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.fSinAmp = ") 	SAVE_FLOAT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.fSinAmp) SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		REPEAT COUNT_OF(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS) i
			SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("details.iExcludedPointsBS[") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE("] = 0") SAVE_NEWLINE_TO_DEBUG_FILE() // SAVE_INT_TO_DEBUG_FILE(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[i])  SAVE_NEWLINE_TO_DEBUG_FILE()
			REPEAT 32 j
				IF IS_BIT_SET(g_AsyncGridSpawnData.db_gridCreator.iExcludedPointsBS[i], j)
					SAVE_STRING_TO_DEBUG_FILE(textIndent) SAVE_STRING_TO_DEBUG_FILE("SET_BIT(details.iExcludedPointsBS[") SAVE_INT_TO_DEBUG_FILE(i) SAVE_STRING_TO_DEBUG_FILE("], ") SAVE_INT_TO_DEBUG_FILE(j) SAVE_STRING_TO_DEBUG_FILE(")") SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		IF NOT IS_STRING_NULL_OR_EMPTY(locationName)
			SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
		
		g_AsyncGridSpawnData.db_bDumpCreatedGrid = FALSE
	ENDIF
	
	// Group warp
	IF g_AsyncGridSpawnData.db_bStartGroupWarp
	
		// change this so instead of bitsets we capture event when players requesting warp or maybe server confirming event or something
		// because we might miss the window in which this is true!
		IF g_AsyncGridSpawnData.db_iGroupPlaces[g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace] > -1
		AND NOT IS_BIT_SET(g_AsyncGridSpawnData.iWarpConfirmedPlayersBS[g_AsyncGridSpawnData.db_iGroupWarpLocation], g_AsyncGridSpawnData.db_iGroupPlaces[g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace])
			IF NOT HAS_NET_TIMER_STARTED(g_AsyncGridSpawnData.db_timer)
			//OR HAS_NET_TIMER_EXPIRED(g_AsyncGridSpawnData.db_timer, 1000)
				
				PLAYER_INDEX pWarper = INT_TO_PLAYERINDEX(g_AsyncGridSpawnData.db_iGroupPlaces[g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace])
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[ASYNC_GRID_WARP][TYMON] Sending broadcast to ", GET_PLAYER_NAME(pWarper))
				#ENDIF
				
				//BROADCAST_DO_ASYNC_GRID_WARP(pWarper, INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, g_AsyncGridSpawnData.db_iGroupWarpLocation))
				//REINIT_NET_TIMER(g_AsyncGridSpawnData.db_timer)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF IS_BIT_SET(g_AsyncGridSpawnData.iWarpConfirmedPlayersBS[g_AsyncGridSpawnData.db_iGroupWarpLocation], g_AsyncGridSpawnData.db_iGroupPlaces[g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace])
					PRINTLN("[ASYNC_GRID_WARP][TYMON] Ok server confirmed they got it, moving on to the next player")
				ENDIF
			#ENDIF
				
			g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace += 1
			
			IF g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace = NUM_NETWORK_PLAYERS
				g_AsyncGridSpawnData.db_bStartGroupWarp = FALSE
				g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace = 0
				REPEAT COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS) i
					g_AsyncGridSpawnData.iWarpConfirmedPlayersBS[i] = 0
				ENDREPEAT
			ENDIF
		ENDIF
		
		/*
	
		IF NOT g_AsyncGridSpawnData.bRequestingGroupWarp
			PLAYER_INDEX pOrder[NUM_NETWORK_PLAYERS]
			REPEAT NUM_NETWORK_PLAYERS i
				pOrder[i] = INT_TO_PLAYERINDEX(g_AsyncGridSpawnData.db_iGroupPlaces[i])
			ENDREPEAT
			ASYNC_GRID_SPAWN_LOCATIONS location = INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, g_AsyncGridSpawnData.db_iGroupWarpLocation)
			START_ASYNC_GROUP_WARP(location, pOrder)
		ELSE
			IF DOES_SERVER_CONFIRM_ASYNC_GROUP_WARP()
				PLAYER_INDEX pWarper
				REPEAT NUM_NETWORK_PLAYERS i
					pWarper = INT_TO_PLAYERINDEX(g_AsyncGridSpawnData.db_iGroupPlaces[i])
					IF pWarper != INVALID_PLAYER_INDEX()
						BROADCAST_DO_ASYNC_GRID_WARP(pWarper, INT_TO_ENUM(ASYNC_GRID_SPAWN_LOCATIONS, g_AsyncGridSpawnData.db_iGroupWarpLocation))
					ENDIF
				ENDREPEAT
				g_AsyncGridSpawnData.db_bStartGroupWarp = FALSE
				g_AsyncGridSpawnData.db_iGroupWarpCurrentPlace = 0
			ENDIF
		ENDIF
		
		*/
		
	ENDIF
ENDPROC
#ENDIF

