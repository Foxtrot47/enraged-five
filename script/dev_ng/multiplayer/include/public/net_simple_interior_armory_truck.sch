//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_armory_truck.sch																//
// Description: This is implementation of Armory Truck as simple interior. As such this does not expose any			//
//				public functions. All functions to manipulate simple interiors are in net_simple_interior			//
// Written by:  Ata																								//
// Date:  		25/01/2017																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_simple_interior_common.sch"
USING "net_include.sch"
USING "cutscene_help.sch"
USING "net_property_sections_armory_truck.sch"
USING "net_simple_interior_cs_header_include.sch"

CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER 					0 // We entered armory truck as owner (but not boss)
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER		1 // We entered armory truck as guest (not member of owner's organization or gang)
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BOSS 					2 	// We entered the bunker as boss (also owner)
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_GOON 					3 	// We entered the bunker as goon
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_GOON_MC 				4 	// We entered the bunker as goon in an MC
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_ASSOCIATE 				5 	// We entered the bunker as an associate
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_MEMBER_OF_OTHER_GANG	6 	// We entered as a member of someone elses gang
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER_LEFT_GAME		7 	// The owner has left the game
CONST_INT SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_EXIT_OWNER_JOINED_GANG 8
CONST_INT BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_BUNKER_TRADE	9
CONST_INT BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED	10
CONST_INT BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AFTER_WARNING_SC	11

CONST_INT KICK_TO_BUNK_EXT_REASON_OWNER_ON_MISSION_NON_GANG_MEMBER		0
CONST_INT KICK_TO_BUNK_EXT_REASON_IAM_OWNER_TRADED_IN_BUNKER			1
CONST_INT KICK_TO_BUNK_EXT_REASON_MY_OWNER_TRADED_IN_BUNKER				2
CONST_INT KICK_TO_BUNK_EXT_REASON_OWNER_LEFT							3

//Blip data
CONST_INT MAX_ARMORY_TRUCK_BLIPS	1

FUNC BOOL DOES_ARMORY_TRUCK_USE_EXTERIOR_SCRIPT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_ARMORY_TRUCK_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			vPosition = <<1103.562378, -3000.0, -40.0>>
			fHeading = 0.0
			txtType = "gr_grdlc_int_01"
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_ARMORY_TRUCK_LOAD_SECONDARY_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
		SET_INTERIOR_FLOOR_INDEX(GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER())
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VEHICLE_INDEX GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	#IF IS_DEBUG_BUILD
	CONST_INT ciDEBUG_PRINT_FRAME_COUNT 60
	BOOL bPrint = FALSE
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % ciDEBUG_PRINT_FRAME_COUNT) = 0
		AND NOT IS_BIT_SET(g_SimpleInteriorData.iDebugBS, BS_SMPL_INTERIOR_DBG_PRINTED_MOC_VEH_INDEX_HELP)
			bPrint = TRUE
			SET_BIT(g_SimpleInteriorData.iDebugBS, BS_SMPL_INTERIOR_DBG_PRINTED_MOC_VEH_INDEX_HELP)
		ENDIF
	#ENDIF

	IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentWalkingInOrOutOfSimpleInterior != SIMPLE_INTERIOR_INVALID	
	AND IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
	AND GET_SIMPLE_INTERIOR_TYPE(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentWalkingInOrOutOfSimpleInterior) = SIMPLE_INTERIOR_TYPE_BUNKER
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % ciDEBUG_PRINT_FRAME_COUNT = 0
			AND bPrint
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_TRAILER_VEH_INDEX - entering bunker setting vehicle as invalid ") 
			ENDIF
		#ENDIF
		RETURN INT_TO_NATIVE(VEHICLE_INDEX,-1)
	ENDIF
	
	// If player is bunker check nearby truck
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
	AND NOT IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
		INT iVeh, iInstance
		VEHICLE_INDEX nearbyVehs[25]
		STRING scriptName
		INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
		
		REPEAT iNearbyVehs iVeh
			IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
			AND IS_ENTITY_A_VEHICLE(nearbyVehs[iVeh])
				IF NOT IS_ENTITY_DEAD(nearbyVehs[iVeh])
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
						IF IS_VEHICLE_A_PERSONAL_TRUCK(nearbyVehs[iVeh])
							scriptName = GET_ENTITY_SCRIPT(nearbyVehs[iVeh], iInstance)
							IF NOT IS_STRING_NULL_OR_EMPTY(scriptName)				
								IF ARE_STRINGS_EQUAL(scriptName,"am_mp_bunker")
									IF IS_NET_PLAYER_OK(g_ownerOfBunkerPropertyIAmIn)
										IF GET_OWNER_OF_PERSONAL_TRUCK(nearbyVehs[iVeh]) = g_ownerOfBunkerPropertyIAmIn
											#IF IS_DEBUG_BUILD
												IF GET_FRAME_COUNT() % ciDEBUG_PRINT_FRAME_COUNT = 0
												AND bPrint
													PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_TRAILER_VEH_INDEX - I'm in ", GET_PLAYER_NAME(g_ownerOfBunkerPropertyIAmIn), " vehicle") 
												ENDIF
											#ENDIF
											RETURN nearbyVehs[iVeh]
										ENDIF
									ENDIF	
								ENDIF
							ENDIF	
						ENDIF	
					ENDIF	
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	// if player is part of gang
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
	AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF PLAYER_ID() != GB_GET_LOCAL_PLAYER_GANG_BOSS()
			INT iVeh
			VEHICLE_INDEX nearbyVehs[25]
			
			INT iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			
			REPEAT iNearbyVehs iVeh
				IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[iVeh])
					IF NOT IS_ENTITY_DEAD(nearbyVehs[iVeh])
						IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
							IF IS_VEHICLE_A_PERSONAL_TRUCK(nearbyVehs[iVeh])
								IF IS_NET_PLAYER_OK(GB_GET_LOCAL_PLAYER_GANG_BOSS())
									IF GET_OWNER_OF_PERSONAL_TRUCK(nearbyVehs[iVeh]) = GB_GET_LOCAL_PLAYER_GANG_BOSS()
										#IF IS_DEBUG_BUILD
											IF GET_FRAME_COUNT() % ciDEBUG_PRINT_FRAME_COUNT = 0
											AND bPrint
												PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_TRAILER_VEH_INDEX - I'm in gang returning ", GET_PLAYER_NAME(GB_GET_LOCAL_PLAYER_GANG_BOSS()), " vehicle") 
											ENDIF
										#ENDIF
										RETURN nearbyVehs[iVeh]
									ENDIF
								ENDIF	
							ENDIF	
						ENDIF	
					ENDIF
				ENDIF
			ENDREPEAT	
		ENDIF
	ENDIF
	
	// check if player is passenger
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
			INT iVeh
			VEHICLE_INDEX nearbyVehs[25]
			
			INT iNearbyVehs
			IF NOT DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTrailerEnteringWithDriver)
				iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
				
				REPEAT iNearbyVehs iVeh
					IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
					AND IS_ENTITY_A_VEHICLE(nearbyVehs[iVeh])
						IF NOT IS_ENTITY_DEAD(nearbyVehs[iVeh])
							IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
								IF IS_VEHICLE_A_PERSONAL_TRUCK(nearbyVehs[iVeh])
									IF IS_NET_PLAYER_OK(g_SimpleInteriorData.driverPlayer)
										IF GET_OWNER_OF_PERSONAL_TRUCK(nearbyVehs[iVeh]) = g_SimpleInteriorData.driverPlayer
											
											#IF IS_DEBUG_BUILD
												IF bPrint
													PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_TRAILER_VEH_INDEX - I'm entering with driver returning ", GET_PLAYER_NAME(g_SimpleInteriorData.driverPlayer), " vehicle") 
												ENDIF
											#ENDIF
											MPGlobalsAmbience.vehTrailerEnteringWithDriver = nearbyVehs[iVeh]
											RETURN nearbyVehs[iVeh]
										ENDIF
									ENDIF	
								ENDIF	
							ENDIF	
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				RETURN MPGlobalsAmbience.vehTrailerEnteringWithDriver
			ENDIF	
		ENDIF
	ELSE
		IF IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
			SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
		ENDIF	
	ENDIF
	
	RETURN MPGlobalsAmbience.vehTruckVehicle[1]
ENDFUNC

FUNC PLAYER_INDEX GET_OWNER_OF_TRAILER_VEH_INDEX()
	RETURN GET_OWNER_OF_PERSONAL_TRUCK(GET_ARMORY_TRUCK_TRAILER_VEH_INDEX())
ENDFUNC


FUNC VEHICLE_INDEX GET_ARMORY_TRUCK_VEH_INDEX()
	RETURN MPGlobalsAmbience.vehTruckVehicle[0]
ENDFUNC

PROC PROCESS_EXTERIOR_LEFT_CAM_SHAPETEST(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
    VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	
	SWITCH entryAnim.iExteriorCamShapeTestStage[0]
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
            IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)	
				// See if we can collide with building on left side of truck 
				VECTOR vStartPointCamLeft
				vStartPointCamLeft = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<-6.0,10,2.0>>)
				
				VECTOR vEndPointCamLeft
				vEndPointCamLeft = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<-6.0,-4.5,2.0>>)

				entryAnim.sti_ExteriorCam[0] = START_SHAPE_TEST_CAPSULE(vStartPointCamLeft, vEndPointCamLeft, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 1.9 , SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT, truckIndex)
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
					DRAW_DEBUG_CROSS(vStartPointCamLeft, 2.0,0,0,255)
					DRAW_DEBUG_CROSS(vEndPointCamLeft, 2.0,0,0,255)
					DRAW_DEBUG_SPHERE(vEndPointCamLeft, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 1.9 , 0, 0, 255, 64)
					DRAW_DEBUG_SPHERE(vStartPointCamLeft, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 1.9 , 0, 0, 255, 64)
				ENDIF
				#ENDIF
				IF NATIVE_TO_INT(entryAnim.sti_ExteriorCam[0]) != 0
					entryAnim.iExteriorCamShapeTestStage[0] 		= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_LEFT_CAM_SHAPETEST iExteriorCamShapeTestStage[0] = 0 TO 1 ")
				ENDIF	
            ENDIF
        BREAK
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
        
            INT iHitSomething
            VECTOR vPosHit, vNormalAtPosHit
            ENTITY_INDEX entityHit
			
           	SHAPETEST_STATUS shapetestStatus 
			shapetestStatus = GET_SHAPE_TEST_RESULT(entryAnim.sti_ExteriorCam[0], iHitSomething, vPosHit, vNormalAtPosHit, entityHit)
			
			// result is ready for left area
            IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
                #IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_LEFT_CAM_SHAPETEST - We got shapetest results!")
				#ENDIF
				
				IF iHitSomething = 0
					
					g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] g_SimpleInteriorData.iExteriorCamShapeTestHitSomething = left SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						//DRAW_DEBUG_TEXT_2D("Left area hit somthing = FALSE", <<0.1,0.1,0.5>>,0,255,0)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "left area hit somthing = FALSE", 1,0,255,0)
					ENDIF
					#ENDIF
					
                ELSE
				
                    g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_LEFT_CAM_SHAPETEST  SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "left area hit somthing = TRUE", 1)
						//DRAW_DEBUG_TEXT_2D("Left area hit somthing = TRUE", <<0.1,0.1,0.5>>,255,0,0)
					ENDIF
					#ENDIF
					entryAnim.sti_ExteriorCam[0] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					entryAnim.iExteriorCamShapeTestStage[0] 	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT  
					
                ENDIF

				 
			ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
				entryAnim.iExteriorCamShapeTestStage[0] 	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT    
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_LEFT_CAM_SHAPETEST iExteriorCamShapeTestStage[0] = 1 TO 0 ")
            ENDIF

        BREAK
    ENDSWITCH
ENDPROC

PROC PROCESS_EXTERIOR_RIGHT_CAM_SHAPETEST(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
    SWITCH entryAnim.iExteriorCamShapeTestStage[1]
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
            IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
				// See if we can collide with building on right side of truck 
				VECTOR vStartPointCamRight
				vStartPointCamRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<6.0,10,2.0>>)
				
				VECTOR vEndPointCamRight
				vEndPointCamRight = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<6.0,-4.5,2.0>>)
				
				entryAnim.sti_ExteriorCam[1] = START_SHAPE_TEST_CAPSULE(vStartPointCamRight, vEndPointCamRight, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 1.9 , SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT, truckIndex)
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
					DRAW_DEBUG_CROSS(vStartPointCamRight, 2.0,0,255,0)
					DRAW_DEBUG_CROSS(vEndPointCamRight, 2.0,0,255,0)
					DRAW_DEBUG_SPHERE(vEndPointCamRight, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 1.9 , 0, 255, 0, 64)
					DRAW_DEBUG_SPHERE(vStartPointCamRight, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 1.9 , 0, 255, 0, 64)
				ENDIF
				#ENDIF
				IF NATIVE_TO_INT(entryAnim.sti_ExteriorCam[1]) != 0
					entryAnim.iExteriorCamShapeTestStage[1] 		= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_RIGHT_CAM_SHAPETEST iExteriorCamShapeTestStage[1] = 0 TO 1 ")
				ENDIF
            ENDIF
        BREAK
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
        
            INT iHitSomething
            VECTOR vPosHit, vNormalAtPosHit
            ENTITY_INDEX entityHit
			
			SHAPETEST_STATUS shapetestStatus 
			shapetestStatus = GET_SHAPE_TEST_RESULT(entryAnim.sti_ExteriorCam[1], iHitSomething, vPosHit, vNormalAtPosHit, entityHit)
			// result is ready for right area
            IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
                #IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_RIGHT_CAM_SHAPETEST - We got shapetest results!")
				#ENDIF
				
				IF iHitSomething = 0
				
                  	g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_RIGHT_CAM_SHAPETEST= right SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "right area hit somthing = FALSE", 3,0,255,0)
						//DRAW_DEBUG_TEXT_2D("right area hit somthing = TRUE", <<0.6,0.1,0.5>>,0,255,0)
					ENDIF
					#ENDIF
                ELSE
                    g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_RIGHT_CAM_SHAPETEST right = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						//DRAW_DEBUG_TEXT_2D("right area hit somthing = TRUE", <<0.6,0.1,0.5>>,255,0,0)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "right area hit somthing = TRUE", 3)
					ENDIF
					#ENDIF
					entryAnim.sti_ExteriorCam[1] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					entryAnim.iExteriorCamShapeTestStage[1] 	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT  
                ENDIF
				 
			ELIF shapetestStatus	= SHAPETEST_STATUS_NONEXISTENT
               entryAnim.iExteriorCamShapeTestStage[1]		= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT 
			   PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_RIGHT_CAM_SHAPETEST iExteriorCamShapeTestStage[1] = 1 TO 0 ")
            ENDIF
            
        BREAK
    ENDSWITCH
ENDPROC

PROC PROCESS_EXTERIOR_TOP_CAM_SHAPETEST(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
    SWITCH entryAnim.iExteriorCamShapeTestStage[2]
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
            IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)

				// See if we can collide with building on top of truck 
				VECTOR vStartPointCamTop
				vStartPointCamTop = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0,10,7.0>>)
				
				VECTOR vEndPointCamTop
				vEndPointCamTop = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0,-2.5,7.0>>)
				
				entryAnim.sti_ExteriorCam[2] = START_SHAPE_TEST_CAPSULE(vStartPointCamTop, vEndPointCamTop, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) - 1.5 , SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT, truckIndex)
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
					DRAW_DEBUG_CROSS(vStartPointCamTop, 2.0,255,0,0)
					DRAW_DEBUG_CROSS(vEndPointCamTop, 2.0,255,0,0)
					DRAW_DEBUG_SPHERE(vEndPointCamTop, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) -1.5 , 255, 0, 0, 64)
					DRAW_DEBUG_SPHERE(vStartPointCamTop, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) -1.5  , 255, 0, 0, 64)
				ENDIF
				#ENDIF
				
				IF NATIVE_TO_INT(entryAnim.sti_ExteriorCam[2]) != 0
					//g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2] = -1
                	entryAnim.iExteriorCamShapeTestStage[2]        	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_TOP_CAM_SHAPETEST iExteriorCamShapeTestStage[2] = 0 TO 1 ")
				ENDIF
            ENDIF
        BREAK
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
        
            INT iHitSomething
            VECTOR vPosHit, vNormalAtPosHit
            ENTITY_INDEX entityHit
			
			SHAPETEST_STATUS shapetestStatus 
			shapetestStatus = GET_SHAPE_TEST_RESULT(entryAnim.sti_ExteriorCam[2], iHitSomething, vPosHit, vNormalAtPosHit, entityHit)
			// result is ready for top area
			IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
	            #IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_TOP_CAM_SHAPETEST - We got shapetest results!")
				#ENDIF
				
				IF iHitSomething = 0
	            		 
					g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_TOP_CAM_SHAPETEST SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						//DRAW_DEBUG_TEXT_2D("top area hit somthing = TRUE", <<0.3,0.1,0.5>>,0,255,0)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "top area hit somthing = FALSE", 6,0,255,0)
					ENDIF
					#ENDIF
					
	            ELSE
	                g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_TOP_CAM_SHAPETEST SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						//DRAW_DEBUG_TEXT_2D("top area hit somthing = TRUE", <<0.3,0.1,0.5>>,255,0,0)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "top area hit somthing = TRUE", 6)
					ENDIF
					#ENDIF
					
					entryAnim.sti_ExteriorCam[2] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					entryAnim.iExteriorCamShapeTestStage[2] 	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT  
					
	             ENDIF
				
	        ELIF shapetestStatus 	= SHAPETEST_STATUS_NONEXISTENT
			    entryAnim.iExteriorCamShapeTestStage[2]		= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT    
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_TOP_CAM_SHAPETEST iExteriorCamShapeTestStage[2] = 1 to 0 ")
	        ENDIF
        BREAK
    ENDSWITCH
ENDPROC

FUNC BOOL CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(VEHICLE_INDEX mVeh, VECTOR vPoint, FLOAT &fOffset)
	FLOAT z
	VECTOR vehPos
	GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(GET_ENTITY_COORDS(mVeh), z)

	vehPos = GET_ENTITY_COORDS(mVeh)
	
	GET_GROUND_Z_EXCLUDING_OBJECTS_FOR_3D_COORD(vPoint, fOffset)
	
	fOffset += (vehPos.z - z) - 0.32
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_EXTERIOR_BACK_AREA_SHAPETEST(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
    SWITCH entryAnim.iExteriorCamShapeTestStage[3]
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
            IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)

				// See if we can collide with building on back of truck 
				VECTOR vStartPointCamBack
				FLOAT  fGroundZ1, fGroundZ2
				
				vStartPointCamBack = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0,-9.0, 0>>)
				
				VECTOR vEndPointCamBack
				vEndPointCamBack =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0,-20.0, 0>>)
				
				CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(truckIndex, vStartPointCamBack, fGroundZ1)
				CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(truckIndex, vEndPointCamBack, fGroundZ2)
				
				entryAnim.sti_ExteriorCam[3] = START_SHAPE_TEST_CAPSULE(<<vStartPointCamBack.x,vStartPointCamBack.y, fGroundZ1>>, <<vEndPointCamBack.x,vEndPointCamBack.y, fGroundZ1>>, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 2.0 , SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT, truckIndex)
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
					DRAW_DEBUG_CROSS(<<vStartPointCamBack.x,vStartPointCamBack.y, fGroundZ1>>, 2.0,255,255,0)
					DRAW_DEBUG_CROSS(<<vEndPointCamBack.x,vEndPointCamBack.y, fGroundZ1>>, 2.0,255,255,0)
					DRAW_DEBUG_SPHERE(<<vEndPointCamBack.x,vEndPointCamBack.y, fGroundZ1>>, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 2.5 , 255, 255, 0, 64)
					DRAW_DEBUG_SPHERE(<<vStartPointCamBack.x,vStartPointCamBack.y, fGroundZ1>>, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(truckIndex)) / 2.5  , 255, 255, 0, 64)
				ENDIF
				#ENDIF
				IF NATIVE_TO_INT(entryAnim.sti_ExteriorCam[3]) != 0
	                entryAnim.iExteriorCamShapeTestStage[3]        	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  PROCESS_EXTERIOR_BACK_CAM_SHAPETEST iExteriorCamShapeTestStage[3] = 0 TO 1 ")
				ENDIF
            ENDIF
        BREAK
        CASE SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_PROCESS
        
            INT iHitSomething
            VECTOR vPosHit, vNormalAtPosHit
            ENTITY_INDEX entityHit
			
			SHAPETEST_STATUS shapetestStatus
			shapetestStatus = GET_SHAPE_TEST_RESULT(entryAnim.sti_ExteriorCam[3], iHitSomething, vPosHit, vNormalAtPosHit, entityHit)
			// result is ready for top area
			IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
	            #IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_BACK_CAM_SHAPETEST - We got shapetest results!")
				#ENDIF
				
				IF iHitSomething = 0
	                 
					g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  g_SimpleInteriorData.iExteriorCamShapeTestHitSomething =  back SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING ")
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						//DRAW_DEBUG_TEXT_2D("top area hit somthing = TRUE", <<0.3,0.1,0.5>>,0,255,0)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "back area hit somthing = FALSE", 9,0,255,0)
					ENDIF
					#ENDIF
					
	            ELSE
	                g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  g_SimpleInteriorData.iExteriorCamShapeTestHitSomething back CAM = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING ")
					
					CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_TOP_CAM)
					CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_RIGHT_CAM)
					CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_LEFT_CAM)
					
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawArmoryTruckShapeTest")
						//DRAW_DEBUG_TEXT_2D("top area hit somthing = TRUE", <<0.3,0.1,0.5>>,255,0,0)
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(truckIndex, "back area hit somthing = TRUE", 9)
					ENDIF
					#ENDIF
					
					entryAnim.sti_ExteriorCam[3] = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					entryAnim.iExteriorCamShapeTestStage[3] 	= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT  
					
	            ENDIF
				
	        ELIF shapetestStatus 	= SHAPETEST_STATUS_NONEXISTENT
				entryAnim.iExteriorCamShapeTestStage[3]		= SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT    
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXTERIOR_TOP_CAM_SHAPETEST iExteriorCamShapeTestStage[2] = 1 to 0 ")
	        ENDIF
        BREAK
    ENDSWITCH
ENDPROC

FUNC STRING GET_DEBUG_HIT_SOMTHING_TEXT(INT iHit)
	SWITCH iHit
		CASE SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_INIT				RETURN 	"SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_INIT"		
		CASE SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING	RETURN 	"SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING"	
		CASE SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING		RETURN 	"SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_HIT_SOMETHING"	
	ENDSWITCH
	
	RETURN "-1"
ENDFUNC

PROC GET_ARMORY_TRUCK_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocate)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
				IF GET_ENTITY_MODEL(truckIndex) = TRAILERLARGE
					FLOAT fGroundZ
					vLocate = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckIndex, <<0.0,-8.9, -2.0>>)//GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0.0,-8.9,-1.5>>)
					IF GET_GROUND_Z_FOR_3D_COORD(vLocate,fGroundZ)
						vLocate = <<vLocate.x,vLocate.y,fGroundZ>>
//						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_ENTRY_LOCATE, GET_GROUND_Z_FOR_3D_COORD, passed vLocate = ", vLocate)
					ELSE
//						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_ENTRY_LOCATE, GET_GROUND_Z_FOR_3D_COORD, failed vLocate: ", vLocate)					
						vLocate = <<vLocate.x,vLocate.y,vLocate.z-1.5>>
					ENDIF
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
			IF IS_VECTOR_ZERO(vLocate)
				IF g_SimpleInteriorData.driverPlayer != INVALID_PLAYER_INDEX()
					IF !IS_VECTOR_ZERO(GlobalplayerBD[NATIVE_TO_INT(g_SimpleInteriorData.driverPlayer)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint)
						vLocate = GlobalplayerBD[NATIVE_TO_INT(g_SimpleInteriorData.driverPlayer)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_ENTRY_LOCATE I'm passenger and entry locate is zero new locate is: ", vLocate)
					ELSE
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_ENTRY_LOCATE I'm passenger owner's truck entry locate is zero ")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

PROC GET_ARMORY_TRUCK_HEADING(SIMPLE_INTERIORS eSimpleInteriorID, FLOAT &fHeading)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
				IF GET_ENTITY_MODEL(truckIndex) = TRAILERLARGE
					fHeading = GET_ENTITY_HEADING(truckIndex)
				ENDIF
			ENDIF	
		BREAK
	ENDSWITCH	
ENDPROC

FUNC BOOL FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE(VEHICLE_INDEX vehTocheck)
	IF DOES_ENTITY_EXIST(vehTocheck)
	AND IS_VEHICLE_DRIVEABLE(vehTocheck)
		SWITCH GET_ENTITY_MODEL(vehTocheck)
			CASE APC
			CASE INSURGENT
			CASE INSURGENT2
			CASE INSURGENT3
			CASE CARACARA
			CASE CARACARA2
				//PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - APC has big weapon")
				RETURN TRUE
			BREAK
			CASE VOODOO
			CASE VOODOO2
			CASE VIGILANTE
			CASE VISIONE
			CASE DELUXO
			CASE OPPRESSOR2
			CASE MENACER
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - Voodoo does not have ground clearance")
				RETURN TRUE
			BREAK		
		ENDSWITCH
		
		INT iCount
		REPEAT 8 iCount
			IF IS_VEHICLE_TYRE_BURST(vehTocheck, INT_TO_ENUM(SC_WHEEL_LIST, iCount))
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - IS_VEHICLE_TYRE_BURST true")
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		FLOAT fTemp
		fTemp = GET_VEHICLE_ENGINE_HEALTH(vehTocheck)/1000
		IF fTemp < 0.4
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - GET_VEHICLE_ENGINE_HEALTH true fTemp: ", fTemp)
			RETURN TRUE
		ENDIF
		
		fTemp = TO_FLOAT(GET_ENTITY_HEALTH(vehTocheck))/1000
		IF fTemp < 0.4
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - GET_ENTITY_HEALTH true fTemp: ", fTemp)
			RETURN TRUE
		ENDIF
	ENDIF
	
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
		AND NOT IS_ENTITY_DEAD(truckIndex)
			INT iNode
			FLOAT x, y, z, w
			VECTOR vCutSceneCoord, vPlayerPos, vTruckCoords
			vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
			IF GET_NTH_CLOSEST_VEHICLE_NODE(vPlayerPos, iNode, vCutSceneCoord)
				IF VDIST2(vPlayerPos, vCutSceneCoord) > 2500
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - no vehicle node availabe distance to nearest road node: ", VDIST2(vPlayerPos, vCutSceneCoord))
					RETURN TRUE
				ENDIF	
			ENDIF
			
			GET_ENTITY_QUATERNION(truckIndex ,x,y,z,w)
			IF y < -0.03 
			OR y > 0.03
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE - vehicle y is not suitable: ", y)
				RETURN TRUE
			ENDIF
			
			VECTOR vTrailerCoord

			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
				// trailer coords
				vTruckCoords =	GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckIndex, <<0.0,-10,0>>)
				// position where player vehicle going to warp for cutscene
				vTrailerCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckIndex, <<0.0,-15.5,0>>)
				
				IF ABSF(vTruckCoords.z - vTrailerCoord.z ) > 0.8
					SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE vehicle z is higher than trailer z: ", vTruckCoords.z - vTrailerCoord.z )
					RETURN TRUE
				ENDIF
				
			ENDIF	
	
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ALL_SHAPETESTS_PROCESSED(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, VEHICLE_INDEX vehTocheck)
	
	IF FORCE_FADE_OUT_CUT_SCENE_FOR_THIS_VEHICLE(vehTocheck)
		CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_TOP_CAM)
		CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_RIGHT_CAM)
		CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_LEFT_CAM)
		SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(TRUE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_SHAPETESTS_PROCESSED - Force fade out! ")
		RETURN TRUE
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(entryAnim.sShapeTestTimer)
			START_NET_TIMER(entryAnim.sShapeTestTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(entryAnim.sShapeTestTimer, 1500)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_SHAPETESTS_PROCESSED - NONE of the shape test results are ready after 5 seconds - left: ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0])
						, " right: " , GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1])
						, " top: ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2]))
				RESET_NET_TIMER(entryAnim.sShapeTestTimer)
				CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_TOP_CAM)
				CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_RIGHT_CAM)
				CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_LEFT_CAM)
				SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(TRUE)
				RETURN TRUE
			ELSE
				IF g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
				AND g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					RESET_NET_TIMER(entryAnim.sShapeTestTimer)
					PRINTLN("HAS_ALL_SHAPETESTS_PROCESSED - g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0]) )
					SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_LEFT_CAM)
					RETURN TRUE
				ELIF (g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
				AND g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING)
				OR IS_PLAYER_IN_BUNKER(PLAYER_ID())
					RESET_NET_TIMER(entryAnim.sShapeTestTimer)
					PRINTLN("HAS_ALL_SHAPETESTS_PROCESSED - g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1]) )
					SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_RIGHT_CAM)
					RETURN TRUE
				ELIF g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
				AND g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3] = SIMPLE_INTERIOR_ENTRY_CAM_AREA_CHECK_NOT_HIT_ANYTHING
					RESET_NET_TIMER(entryAnim.sShapeTestTimer)
					PRINTLN("HAS_ALL_SHAPETESTS_PROCESSED - g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2]) )
					SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_TOP_CAM)
					RETURN TRUE
				ELSE	
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_SHAPETESTS_PROCESSED - FAIL g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0]) )
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_SHAPETESTS_PROCESSED - FAIL g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1]) )
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_SHAPETESTS_PROCESSED - FAIL g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2]) )
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_SHAPETESTS_PROCESSED - FAIL g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3] = ", GET_DEBUG_HIT_SOMTHING_TEXT(g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3]) )
				ENDIF	
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC SIMPLE_INTERIOR_DETAILS_BS GET_ARMORY_TRUCK_PROPERTIES_BS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_DETAILS_BS structReturn
	
	//BS1
	SET_BIT(structReturn.iBS[0], BS_SIMPLE_INTERIOR_DETAILS_INSTANCE_DETERMINED_BY_OWNERSHIP)
	
	//BS2
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_PARENT_INT_SCRIPT_DOES_LOAD_SCENE)
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_ACTIVATE_COLLISION_OF_ENTRY_VEH_AFTER_SCENE)
	
	RETURN structReturn
ENDFUNC

PROC GET_ARMORY_TRUCK_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSMPLIntPreCache)
	details.sProperties = GET_ARMORY_TRUCK_PROPERTIES_BS(eSimpleInteriorID)
	
	//Generic data that is the same for all armory truck
	details.strInteriorChildScript = "AM_MP_ARMORY_TRUCK"
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
	details.bInteriorIsSeamless = FALSE
	
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-2.300,9.690,0.060>>
	details.exitData.vLocateCoords2[0] = <<2.300,9.690,2.305>>
	details.exitData.fLocateWidths[0] = 1.000
	details.exitData.fHeadings[0] = 349.3822
	
	VEHICLE_INDEX TrailerIndex 
	IF DOES_ENTITY_EXIST(details.entryAnim.cloneTrailer)
		TrailerIndex = details.entryAnim.cloneTrailer
	ELSE
		TrailerIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	ENDIF
	
	SWITCH eSimpleInteriorID
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1 // pass 1
			IF GET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED() = 0
			OR IS_PLAYER_IN_BUNKER(PLAYER_ID())	
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_DETAILS entrance 0 ")
			ELSE
				IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
					IF DOES_ENTITY_EXIST(TrailerIndex)
					AND NOT IS_ENTITY_DEAD(TrailerIndex)				
						IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_LEFT_CAM)
						#IF IS_DEBUG_BUILD
						OR g_bUseVehicleEntryLeftCam
						#ENDIF
							
							IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GR_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY)
							AND NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CAMERA_PAN_STARTED)
								SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(FALSE)
							ENDIF
							
							details.entryAnim.cameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<-7.0529, -0.4481, 0.6091>>)
							details.entryAnim.cameraRot = << -9.1560 , -0.1534, GET_ENTITY_HEADING(TrailerIndex) - 150.2327 >>
							details.entryAnim.cameraFov = 25.3500
							details.entryAnim.syncScenePos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<-7.0529, -0.4481, 0.6091>>)
							details.entryAnim.syncSceneRot = << -9.1560 , -0.1534, GET_ENTITY_HEADING(TrailerIndex) - 150.2327  >>
							details.entryAnim.startingPhase = 0.200
							details.entryAnim.endingPhase = 0.460
							details.entryAnim.establishingCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<-6.8478, -0.6934, 0.6801>>)
							details.entryAnim.establishingCameraRot =<< -9.1560 , -0.1534, GET_ENTITY_HEADING(TrailerIndex) - 148.6520>>
							details.entryAnim.fEstablishingCameraFov = 25.3500
							details.entryAnim.fPanDuration = 5.0
							details.entryAnim.fEstablishingCameraShake = 0.7
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_DETAILS for SIMPLE_INTERIOR_ARMORY_TRUCK_1 selecting vCameraPos1 ")
							
						ELIF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_RIGHT_CAM)
						
						#IF IS_DEBUG_BUILD
						OR g_bUseVehicleEntryRightCam
						#ENDIF
							
							IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GR_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY)
							AND NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CAMERA_PAN_STARTED)
								SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(FALSE)
							ENDIF
							IF NOT IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
								details.entryAnim.cameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<7.1157, 0.6104, -1.3546>>)
								details.entryAnim.cameraRot =  << -3.0776 , -0.1478, GET_ENTITY_HEADING(TrailerIndex) - 208.6628>>
								details.entryAnim.cameraFov = 17.6665
								details.entryAnim.syncScenePos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<7.1157, 0.6104, -1.3546>>)
								details.entryAnim.syncSceneRot = << -3.0776 , -0.1478, GET_ENTITY_HEADING(TrailerIndex) + 208.6628>>
								details.entryAnim.startingPhase = 0.200
								details.entryAnim.endingPhase = 0.460
								details.entryAnim.establishingCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<6.4840, -0.2448, -0.9>>)
								details.entryAnim.establishingCameraRot = << -3.0776 , -0.1478, GET_ENTITY_HEADING(TrailerIndex) - 213.1267>>
								details.entryAnim.fEstablishingCameraFov = 17.6665
								details.entryAnim.fPanDuration = 5.0
								details.entryAnim.fEstablishingCameraShake = 0.7
							ELSE
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									details.entryAnim.cameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex),  <<10.3856, -2.1387, -2.2365>>)
									details.entryAnim.cameraRot = << 6.5787 , -0.0233  , GET_ENTITY_HEADING(TrailerIndex) - 231.4331  >> 
									details.entryAnim.cameraFov = 26.4029
									details.entryAnim.syncScenePos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex),  <<10.3856, -2.1387, -2.2365>>)
									details.entryAnim.syncSceneRot = << 6.5787 , -0.0233  , GET_ENTITY_HEADING(TrailerIndex) - 231.4331 >>
									details.entryAnim.startingPhase = 0.200
									details.entryAnim.endingPhase = 0.460
									details.entryAnim.establishingCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex),  <<9.9211, -2.3221, -2.1676>>)
									details.entryAnim.establishingCameraRot =  << 7.8365 , -0.0233 , GET_ENTITY_HEADING(TrailerIndex) + 110.5488 >>
									details.entryAnim.fEstablishingCameraFov = 26.4029
									details.entryAnim.fPanDuration = 9.0
									details.entryAnim.fEstablishingCameraShake = 0.4
								ENDIF	
							ENDIF
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_DETAILS for SIMPLE_INTERIOR_ARMORY_TRUCK_1 selecting vCameraPos2 ")
							
						ELIF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_TOP_CAM)
						#IF IS_DEBUG_BUILD
						OR g_bUseVehicleEntryTopCam
						#ENDIF
							
							IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GR_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY)
							AND NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CAMERA_PAN_STARTED)
								SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(FALSE)
							ENDIF
							
							details.entryAnim.cameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex), <<-1.0858, -3.2694, 6.2579>>)
							details.entryAnim.cameraRot = << -37.5675 , -0.1538, GET_ENTITY_HEADING(TrailerIndex) + 171.9186>>
							details.entryAnim.cameraFov = 29.0636
							details.entryAnim.syncScenePos = << 1831.551, -1182.593, 90.599 >>
							details.entryAnim.syncSceneRot = << 0.000, -0.000, 98.500 >>
							details.entryAnim.startingPhase = 0.200
							details.entryAnim.endingPhase = 0.460
							details.entryAnim.establishingCameraPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(TrailerIndex), GET_ENTITY_HEADING(TrailerIndex),<<-0.8503, -3.3972, 5.8824>>)
							details.entryAnim.establishingCameraRot = << -43.3133 , -0.1538, GET_ENTITY_HEADING(TrailerIndex) + 187.9544 >> 
							details.entryAnim.fEstablishingCameraFov = 29.0636
							details.entryAnim.fPanDuration = 5.0
							details.entryAnim.fEstablishingCameraShake = 0.7
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_DETAILS for SIMPLE_INTERIOR_ARMORY_TRUCK_1 selecting vCameraPos3 ")
							
						ELSE
							IF NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GR_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY)
							AND NOT IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CAMERA_PAN_STARTED)
							AND NOT NETWORK_IS_IN_MP_CUTSCENE()
								SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(TRUE)
							ENDIF
							details.entryAnim.cameraPos = <<0.0, 0.0, 0.0>>
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_DETAILS for SIMPLE_INTERIOR_ARMORY_TRUCK_1 ALL locations are occupied ")
						ENDIF
					ENDIF	
				ELSE
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_DETAILS entrance 1 but not in driver?? ")
				ENDIF
			ENDIF
			//details.entryAnim.cameraPos = <<0.0, 0.0, 0.0>>
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC STRING GET_ARMORY_TRUCK_NAME(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1 RETURN "ARMORY_TRUCK_1"
	ENDSWITCH
	
	RETURN "MISSING"
ENDFUNC

FUNC STRING GET_ARMORY_TRUCK_GAME_TEXT(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_GAME_TEXT eTextID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SWITCH eTextID
		CASE SIGT_PIM_INVITE_TOOLTIP			RETURN "PIM_INVARM_HELP"	BREAK
		CASE SIGT_PIM_INVITE_MENU_LABEL			RETURN "PIM_INVARMTRU"		BREAK
		CASE SIGT_PIM_INVITE_MENU_TITLE			RETURN "PIM_TITLEARMTRU"	BREAK
		CASE SIGT_PROPERTY_INVITE_TICKER		RETURN "PIM_INVAARMTRU"		BREAK
		CASE SIGT_PROPERTY_INVITE_ALL_TICKER	RETURN ""					BREAK
		CASE SIGT_PROPERTY_INVITE_RECIPT		RETURN "CELL_ARMTRUINV"		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_ARMORY_TRUCK_SHOW_PIM_INVITE_OPTION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &tlReturn, BOOL bReturnMenuLabel, BOOL &bSelectable)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(tlReturn)
	
	bSelectable = TRUE
	
	IF bReturnMenuLabel
		tlReturn = GET_ARMORY_TRUCK_GAME_TEXT(eSimpleInteriorID, SIGT_PIM_INVITE_MENU_LABEL)
	ELSE
		tlReturn = "ciPI_TYPE_INVITE_TO_SIMPLE_INTERIOR - ARMORY_TRUCK"
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK) 
	AND (IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)) // Are we in interior as owner of it?
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_ARMORY_TRUCK_INSIDE_BOUNDING_BOX(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bbox  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			bbox.vInsideBBoxMin = <<1097.5347, -3016.0115, -40.7658>>
			bbox.vInsideBBoxMax = <<1109.2977, -2983.6902, -34.1882>>
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_ARMORY_TRUCK_VEHICLE_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vLocate, VECTOR &vLocatePos1, VECTOR &vLocatePos2, FLOAT &fLocateWidth, FLOAT &fEnterHeading)
//	UNUSED_PARAMETER(vLocate)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
				IF GET_ENTITY_MODEL(truckIndex) = TRAILERLARGE
					FLOAT fGroundZ
					
					vLocate 		= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(truckIndex, <<0, -5, 0>>)
					
					//GET_ENTITY_COORDS(truckIndex)
					
					vLocatePos1 	= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vLocate, GET_ENTITY_HEADING(truckIndex), <<0.0,6,-1.5>>)
					IF GET_GROUND_Z_FOR_3D_COORD(vLocatePos1,fGroundZ)
						vLocatePos1 = <<vLocatePos1.x , vLocatePos1.y ,fGroundZ >>
					ENDIF
					vLocatePos2		= GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vLocate, GET_ENTITY_HEADING(truckIndex), <<0.0,-6.0,2.5>>)
					fLocateWidth 	= GET_MODEL_WIDTH(TRAILERLARGE)
					fEnterHeading 	= GET_ENTITY_HEADING(truckIndex)
					//PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_VEHICLE_ENTRY_LOCATE for SIMPLE_INTERIOR_ARMORY_TRUCK_1 = vLocate: ", vLocate, " vLocatePos1: ", vLocatePos1, " vLocatePos2: ", vLocatePos2, " fLocateWidth: ", fLocateWidth , " fEnterHeading: ", fEnterHeading)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC BROADCAST_ARMORY_TRUCK_EXIT_SPAWN_POINT(BOOL bBroadCastNow = FALSE)
	IF GET_FRAME_COUNT() % 60 = 0
	OR bBroadCastNow
		VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
		IF DOES_ENTITY_EXIST(truckIndex)
		AND NOT IS_ENTITY_DEAD(truckIndex)
		AND NOT IS_PLAYER_TRUCK_DESTROYED(PLAYER_ID())
		AND NOT DO_I_NEED_TO_RENOVATE_TRUCK_CAB(PLAYER_ID())
		AND NOT DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(PLAYER_ID())
			IF GET_OWNER_OF_PERSONAL_TRUCK(truckIndex) = PLAYER_ID()
			AND NOT IS_ARMORY_TRUCK_UNDER_MAP(PLAYER_ID())
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.vMobileInteriorExitSpawnPoint = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<2,-17, 0>>)
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.fMobileInteriorExitHeading = GET_ENTITY_HEADING(truckIndex)
			ENDIF
		ENDIF	
	ENDIF	
ENDPROC

PROC GET_ARMORY_TRUCK_ENTRY_LOCATE_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT &iR, INT &iG, INT &iB, INT &iA)
	UNUSED_PARAMETER(eSimpleInteriorID)
	GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
ENDPROC

FUNC BOOL IS_TRUCK_CLOSE_TO_BUNKER(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX truckIndex, VECTOR vTruckEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)

		INT i
		FOR i =	ENUM_TO_INT(SIMPLE_INTERIOR_BUNKER_1) TO ENUM_TO_INT(SIMPLE_INTERIOR_BUNKER_12)
			IF GET_DISTANCE_BETWEEN_COORDS(vTruckEntryCoord, GET_BUNKER_ENTRY_CORONA_COORDS(INT_TO_ENUM(SIMPLE_INTERIORS,i))) < 5
				RETURN TRUE
			ENDIF
		ENDFOR	
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_HANGAR_ENTRY_CORONA_COORDS_FOR_MOC(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HANGAR_1 	RETURN <<-1153.0896, -3411.8274, 12.9450>>
		CASE SIMPLE_INTERIOR_HANGAR_2 	RETURN <<-1396.2682, -3268.0266, 12.9448>>
		CASE SIMPLE_INTERIOR_HANGAR_3 	RETURN <<-2020.3280, 3158.7747, 31.8103>>
		CASE SIMPLE_INTERIOR_HANGAR_4 	RETURN <<-1877.0913, 3110.3411, 31.8103>>
		CASE SIMPLE_INTERIOR_HANGAR_5 	RETURN <<-2469.0989, 3276.5771, 31.8302>>
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_TRUCK_CLOSE_TO_HANGAR(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX truckIndex, VECTOR vTruckEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)

		INT i
		FOR i =	ENUM_TO_INT(SIMPLE_INTERIOR_HANGAR_1) TO ENUM_TO_INT(SIMPLE_INTERIOR_HANGAR_5)
			IF GET_DISTANCE_BETWEEN_COORDS(vTruckEntryCoord, GET_HANGAR_ENTRY_CORONA_COORDS_FOR_MOC(INT_TO_ENUM(SIMPLE_INTERIORS,i))) < 5
				RETURN TRUE
			ENDIF
		ENDFOR	
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_BUSINESS_HUB_ENTRY_CORONA_COORDS_FOR_MOC(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA				RETURN <<733.9667, -1291.1207, 25.2851>>
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW			RETURN <<333.1833, -994.4169, 28.3291>>
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE	RETURN <<-164.3258, -1292.6255, 30.3191>>
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD			RETURN <<-21.8431, 219.4443, 105.7201>>
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS			RETURN <<908.8449, -2097.3716, 29.5575>>
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE			RETURN <<-670.8386, -2392.5562, 12.9445>>	//LSIA warehouse
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND			RETURN <<227.7781, -3136.8979, 4.7903>>		//Elysian Island Warehouse
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD		RETURN <<377.8120, 228.4137, 102.0406>>		//Terminal Warehouse
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING		RETURN <<-1237.5542, -693.9150, 22.7756>>	//Del Perro Building
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS		RETURN <<-1173.5220, -1173.8550, 4.6236>>	//Vespucci Canals Building
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_BUSINESS_HUB_ENTRY_LOCATE_FOR_MOC(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
			RETURN << 733.792, -1291.26, 25.2845 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
			RETURN << 333.38, -994.532, 28.2185 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
			RETURN << -164.142, -1292.8, 30.2921 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
			RETURN <<-21.6571, 219.415, 105.7120>>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
			RETURN << 908.823, -2097.34, 29.5510 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
			RETURN << -670.829, -2392.53, 12.9431 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
			RETURN << 227.62, -3136.88, 4.7903 >>
		BREAK	
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
			RETURN << 377.998, 228.485, 102.0406 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
			RETURN << -1237.53, -693.801, 22.7721 >>
		BREAK
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			RETURN << -1173.46, -1173.71, 4.6236 >>
		BREAK
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC BOOL IS_TRUCK_CLOSE_TO_NIGHTCLUB(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX truckIndex, VECTOR vTruckEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)

		INT i
		FOR i =	ENUM_TO_INT(SIMPLE_INTERIOR_HUB_LA_MESA) TO ENUM_TO_INT(SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS)
			IF VDIST2(vTruckEntryCoord, GET_BUSINESS_HUB_ENTRY_CORONA_COORDS_FOR_MOC(INT_TO_ENUM(SIMPLE_INTERIORS,i))) <=  20 * 20
			OR VDIST2(vTruckEntryCoord, GET_BUSINESS_HUB_ENTRY_LOCATE_FOR_MOC(INT_TO_ENUM(SIMPLE_INTERIORS,i))) <=  20 * 20
				RETURN TRUE
			ENDIF
		ENDFOR	
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_DEFUNCT_BASE_ENTRY_CORONA_COORDS_FOR_MOD(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1 	RETURN <<1273.1376, 2835.0068, 48.0734>>	// Grand Senora Desert Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2 	RETURN <<34.4699, 2620.9768, 84.6202>>		// Route 68 Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3 	RETURN <<2755.9807, 3907.2722, 44.3148>>	// Sandy Shores Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4 	RETURN <<3389.6028, 5508.9712, 24.875>>		// Mount Gordo Facility
//		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_5 	RETURN <<-827.4337, 5682.7979, 21.1210>>	// Paleto Forest Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6 	RETURN <<19.4492, 6825.3613, 14.4952>>		// Paleto Bay Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7 	RETURN <<-2229.4080, 2399.4102, 11.0106>>	// Lago Zancudo Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8 	RETURN <<-3.0095, 3344.4888, 40.2769>>		// Zancudo River Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9 	RETURN <<2086.0674, 1761.3461, 103.043>>	// Ron Alternates Wind Farm Facility
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10 	RETURN <<1864.8027, 269.0474, 163.0169>>	// Land Act Resevoir Facility
	ENDSWITCH

	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_BASE(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)

		IF NOT DOES_PLAYER_OWN_A_DEFUNCT_BASE(PLAYER_ID())
			RETURN FALSE
		ENDIF
		
		IF VDIST2(GET_DEFUNCT_BASE_ENTRY_CORONA_COORDS_FOR_MOD(GET_OWNED_DEFUNCT_BASE_SIMPLE_INTERIOR(GET_OWNER_OF_TRAILER_VEH_INDEX())), vMOCEntryCoord) <=  60 * 60
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_ARENA_WORKSHOP(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)

		IF NOT DOES_PLAYER_OWN_AN_ARENA_GARAGE(PLAYER_ID())
			RETURN FALSE
		ENDIF
		
		IF VDIST2(<<-376.2393, -1878.2488, 19.5278>>, vMOCEntryCoord) <= 1600 //40 * 40
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_CASINO(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)

		IF VDIST2(<<931.99, 43.83, 80.10>>, vMOCEntryCoord) <= 900 //30 * 30
		OR VDIST2(<<930.45, 41.36, 80.10>>, vMOCEntryCoord) <= 900 //30 * 30
		OR VDIST2(<<964.4556, 57.9093, 112.1931>>, vMOCEntryCoord) <= 900 //30 * 30
		OR VDIST2(<<926.4164, 45.5401, 59.9018>>, vMOCEntryCoord) <= 900
		OR VDIST2(<<936.3319, 1.1849, 77.7649>>, vMOCEntryCoord) <= 900
		OR VDIST2(<<968.591003,63.923855,111.552979>>, vMOCEntryCoord) <= 900
		OR VDIST2(<<967.22, 62.69, 111.5531>>, vMOCEntryCoord) <= 900
		OR VDIST2(<<936.5542, 1.1273, 77.7649>>, vMOCEntryCoord) <= 900
		OR VDIST2(<<925.04, 46.48, 80.0960>>, vMOCEntryCoord) <= 900
		OR VDIST2(<<924.507019,60.331799,79.896294>>, vMOCEntryCoord) <= 900
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_ARCADE(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)
		
		IF VDIST2(<<-237.1200, 6228.7554, 30.5005>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<1710.83, 4760.13, 41.0504>>	, vMOCEntryCoord) <= 900	
		OR VDIST2(<<-105.19, -1778.30, 28.51>>	, vMOCEntryCoord) <= 900		
		OR VDIST2(<<-617.7421, 285.8363, 80.6871>>	, vMOCEntryCoord) <= 900	
		OR VDIST2(<<-1287.1135, -278.0045, 37.5935>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<726.7626, -822.4685, 23.8603>>	, vMOCEntryCoord) <= 900	
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_AUTO_SHOP(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)

		IF VDIST2(<<759.36, -675.13, 27.86>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<-144.58, -1341.37, 28.87>>	, vMOCEntryCoord) <= 900	
		OR VDIST2(<<-171, -31.4, 51.27>>		, vMOCEntryCoord) <= 900		
		OR VDIST2( <<231.54, -1875.70, 25.41>>	, vMOCEntryCoord) <= 900	
		OR VDIST2(<<488.98, -897.41, 24.79>>	, vMOCEntryCoord) <= 900	
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_CAR_MEET(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)

		IF VDIST2(<<779.3155, -1867.5061, 28.2965>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<755.8645, -1842.6772, 28.2916>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<755.2990, -1850.0665, 28.2916>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<754.5680, -1857.5647, 28.2916>>	, vMOCEntryCoord) <= 900	
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CLOSE_TO_MUSIC_STUDIO(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX MOCVeh, VECTOR vMOCEntryCoord)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF DOES_ENTITY_EXIST(MOCVeh)
	AND NOT IS_ENTITY_DEAD(MOCVeh)

		// Fixer HQ
		IF VDIST2(<<391.31, -75.76, 67.18>>		, vMOCEntryCoord) <= 900
		OR VDIST2(<<-1019.17, -414.74, 38.62>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<-591.11, -707.96, 35.28>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<-1038.72, -758.01, 18.84>>	, vMOCEntryCoord) <= 900	
		OR VDIST2(<<359.15, -74.61, 66.11>>		, vMOCEntryCoord) <= 900
		OR VDIST2(<<-1030.96, -411.51, 32.27>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<-615.79, -738.30, 26.86>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<-986.93, -764.86, 14.72>>	, vMOCEntryCoord) <= 900	
		
		// music studio
		OR VDIST2(<<-841.3613, -228.9672, 36.2652>>	, vMOCEntryCoord) <= 900
		OR VDIST2(<<-866.6130, -221.8429, 38.6000>>	, vMOCEntryCoord) <= 900
		
			RETURN TRUE
		ENDIF

	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARMORY_TRUCK_LOCATE_NEAR_ANY_PROPERTY_ENTRANCE(VECTOR vLocateCoord)
	//-- Each instance of AM_MP_PROPERTY_EXT.sc stores the entrances for that property in the global vMyPropertyEntrances

	INT i
	REPEAT MAX_ACTIVE_PROPERTY_ENTRANCES i
		IF NOT ARE_VECTORS_EQUAL(mpPropMaintain.vMyPropertyEntrances[i], << 0.0, 0.0, 0.0>>)
			IF VDIST2(mpPropMaintain.vMyPropertyEntrances[i], vLocateCoord) <=  15 * 15
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARMORY_TRUCK_LOCATE_NEAR_MISSION_CORONA()
	IF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGH5")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MATC_DPADRIGHT")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL SHOULD_THIS_ARMORY_TRUCK_CORONA_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MOC_CORONA_NEAR_ANY_VEHICLE(VEHICLE_INDEX truckIndex)
	
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)
	
		INT iVeh
		VEHICLE_INDEX nearbyVehs[25]
	
		INT iNearbyVehs
	
	
		
		VECTOR vEntryCoord 
		GET_ARMORY_TRUCK_ENTRY_LOCATE(SIMPLE_INTERIOR_ARMORY_TRUCK_1, vEntryCoord)
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			iNearbyVehs = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), nearbyVehs)
			
			REPEAT iNearbyVehs iVeh
				IF DOES_ENTITY_EXIST(nearbyVehs[iVeh])
				AND IS_ENTITY_A_VEHICLE(nearbyVehs[iVeh])
					IF NOT IS_ENTITY_DEAD(nearbyVehs[iVeh])
					AND NOT IS_VEHICLE_A_PERSONAL_TRUCK(nearbyVehs[iVeh])
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(nearbyVehs[iVeh], vEntryCoord) < 8	

							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_MOC_CORONA_NEAR_ANY_VEHICLE - vehicle too close to entry")
							RETURN TRUE

						ENDIF	
					ENDIF
				ENDIF
			ENDREPEAT				
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DISPLAY_MOVE_MOC_HELP(PLAYER_INDEX pMOCOwner)
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			IF PLAYER_ID() != pMOCOwner
				RETURN FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN TRUE
ENDFUNC 


FUNC BOOL SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF )
	
	IF NOT CAN_PLAYER_USE_PROPERTY(TRUE #IF IS_DEBUG_BUILD , bPrintReason #ENDIF)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN - Returning TRUE, CAN_PLAYER_USE_PROPERTY is FALSE and we dont have to have access.")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	BOOL bTempPrintReason = FALSE
	#IF IS_DEBUG_BUILD
	bTempPrintReason = bPrintReason
	#ENDIF
	
	VEHICLE_INDEX MOCVehIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	PLAYER_INDEX MOCOwnerIndex = GET_OWNER_OF_PERSONAL_TRUCK(MOCVehIndex)

	IF IS_PLAYER_BLOCKED_FROM_PROPERTY(bTempPrintReason, PROPERTY_ARMORY_TRUCK)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN - Returning TRUE, IS_PLAYER_BLOCKED_FROM_PROPERTY = TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
		IF IS_ENTITY_DEAD(MOCVehIndex)
		AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN true IS_ENTITY_DEAD true")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
		IF NOT IS_VEHICLE_ON_ALL_WHEELS(MOCVehIndex)
		AND NOT IS_DRIVER_ENTERING_THIS_SIMPLE_INTERIOR(PLAYER_ID(), SIMPLE_INTERIOR_ARMORY_TRUCK_1)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN - Returning TRUE, IS_VEHICLE_ON_ALL_WHEELS = false")
				ENDIF
			#ENDIF
			IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
		IF !IS_ENTITY_VISIBLE(MOCVehIndex)
		AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
		AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN true IS_ENTITY_VISIBLE false")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
	AND NOT IS_ENTITY_DEAD(MOCVehIndex)
		IF GET_ENTITY_SPEED(MOCVehIndex) > 0.5
		AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
		AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN true GET_ENTITY_SPEED(MOCVehIndex) > 0.5")
				ENDIF
			#ENDIF
			SET_OVERRIDE_SIMPLE_INTERIOR_ENTRY_CORONA_COLOUR(FALSE,eSimpleInteriorID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WITH_A_PROSTITUTE()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN true IS_PLAYER_WITH_A_PROSTITUTE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN true player is in corona")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_CORONA_POS_BLOCKED)
	AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE BS_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_CORONA_POS_BLOCKED true")
			ENDIF
		#ENDIF
		
		IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(MOCOwnerIndex)
		AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
			IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF
		ENDIF	
		RETURN TRUE
	ENDIF
	
	IF DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(MOCOwnerIndex)	
	OR DO_I_NEED_TO_RENOVATE_TRUCK_CAB(MOCOwnerIndex)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE owner is renovating")
			ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
	AND NOT IS_ENTITY_DEAD(MOCVehIndex)
		IF IS_ENTITY_IN_AIR(MOCVehIndex)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE trailer in air")
				ENDIF
			#ENDIF
			
			IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
	AND NOT IS_ENTITY_DEAD(MOCVehIndex)
		IF IS_ENTITY_IN_AIR(MOCVehIndex)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE truck in air")
				ENDIF
			#ENDIF
			
			IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		IF PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN player in a gang and not boss blocking his own truck")
				ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF

	IF IS_OWNER_RENOVATED_TRUCK_CAB(MOCOwnerIndex)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN player is renovating truck cab")
			ENDIF
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	VECTOR vMOCEntryCoord
	GET_ARMORY_TRUCK_ENTRY_LOCATE(eSimpleInteriorID, vMOCEntryCoord)
	
	IF IS_TRUCK_CLOSE_TO_BUNKER(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_TRUCK_CLOSE_TO_BUNKER true")
			ENDIF
		#ENDIF
		
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_TRUCK_CLOSE_TO_HANGAR(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_TRUCK_CLOSE_TO_HANGAR true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_TRUCK_CLOSE_TO_NIGHTCLUB(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_TRUCK_CLOSE_TO_NIGHTCLUB true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_BASE(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_BASE true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_ARENA_WORKSHOP(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_ARENA_WORKSHOP true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_CASINO(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_CASINO true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_ARCADE(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_ARCADE true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_AUTO_SHOP(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_AUTO_SHOP true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_CAR_MEET(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_CAR_MEET true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CLOSE_TO_MUSIC_STUDIO(eSimpleInteriorID, MOCVehIndex, vMOCEntryCoord)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MOC_CLOSE_TO_MUSIC_STUDIO true")
			ENDIF
		#ENDIF
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_USED_VEHICLE())
			VECTOR vEntryCoord 
			GET_ARMORY_TRUCK_ENTRY_LOCATE(eSimpleInteriorID, vEntryCoord)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(GET_PLAYERS_LAST_USED_VEHICLE(), vEntryCoord) < 1.5
				#IF IS_DEBUG_BUILD
					IF bPrintReason
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN FALSE player vehicle too close to corona")
					ENDIF
				#ENDIF
				
				RETURN TRUE
			ENDIF	
		ENDIF	
	ENDIF
	
	IF IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_PLAYER_ENTERING_OR_EXITING_PROPERTY true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MP_SAVED_TRUCK_BEING_CLEANED_UP()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_MP_SAVED_TRUCK_BEING_CLEANED_UP true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_PROPERTY(MOCOwnerIndex)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_PROPERTY true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF MOCOwnerIndex != INVALID_PLAYER_INDEX()
	AND MOCOwnerIndex != PLAYER_ID()
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(MOCOwnerIndex)].SimpleInteriorBD.iBS,  BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR true")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF MOCOwnerIndex != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(MOCOwnerIndex)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER true")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(MOCOwnerIndex)].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER true")
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)	// Don't check truck entry cut scene 
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE player in MP cutscene true ")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()) 
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT true ")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF

	IF MPGlobalsAmbience.bMobileCommandCentreIsStuck 
	AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE MPGlobalsAmbience.bMobileCommandCentreIsStuck true ")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE player is moving from bunker to truck")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	VECTOR vEntryCoord, vTrailerCoord
	GET_ARMORY_TRUCK_ENTRY_LOCATE(eSimpleInteriorID, vEntryCoord)
	
	IF DOES_ENTITY_EXIST(MOCVehIndex)
	AND NOT IS_ENTITY_DEAD(MOCVehIndex)
	AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(MOCOwnerIndex)
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
		vTrailerCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(MOCVehIndex, <<0.0,-8.1,0>>)
		
		IF ABSF(vTrailerCoord.z - vEntryCoord.z) > 4
			IF SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF	
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE trailer is higher than entry point distance is: ", vTrailerCoord.z - vEntryCoord.z)
				ENDIF
			#ENDIF
			RETURN TRUE
		ENDIF
		
	ENDIF	
	
	IF IS_ARMORY_TRUCK_LOCATE_NEAR_ANY_PROPERTY_ENTRANCE(vEntryCoord)
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE as IS_ARMORY_TRUCK_LOCATE_NEAR_ANY_PROPERTY_ENTRANCE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_ARMORY_TRUCK_LOCATE_NEAR_MISSION_CORONA()
		IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		ENDIF
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE as IS_ARMORY_TRUCK_LOCATE_NEAR_MISSION_CORONA is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_REQUESTED_TRUCK_IN_FREEMODE(MOCOwnerIndex)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE owner is requesting truk to freemode")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_bPlayerViewingTurretHud  
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE g_bPlayerViewingTurretHud is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.sGlobalController.iBS, SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iTenthBS, BS10_SIMPLE_INTERIOR_ACCEPTED_INVITE_TO_ORIGINAL_HEIST_LOBBY)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE BS10_SIMPLE_INTERIOR_ACCEPTED_INVITE_TO_ORIGINAL_HEIST_LOBBY is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
	AND AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH()
	AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN TRUE IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT1_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER) 
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN false eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_MOC_CORONA_NEAR_ANY_VEHICLE(MOCVehIndex)
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
		IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(MOCOwnerIndex)
		AND NOT IS_PLAYER_IN_BUNKER(PLAYER_ID())
		AND SHOULD_DISPLAY_MOVE_MOC_HELP(MOCOwnerIndex)
			IF !IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
				SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			ENDIF
		ENDIF	
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN false IS_MOC_CORONA_NEAR_ANY_VEHICLE  true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		IF GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(PLAYER_ID())) != SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
			#IF IS_DEBUG_BUILD
				IF bPrintReason
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN false entering another simple interior: ",GET_SIMPLE_INTERIOR_PLAYER_IS_WALKING_IN_OR_OUT_FROM(PLAYER_ID()))
				ENDIF
			#ENDIF	
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN false IS_LOCAL_PLAYER_DELIVERING_BOUNTY  true")
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_BLOCKED_EXITING_PROPERTY_FOR_INTRO()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN - IS_PLAYER_BLOCKED_EXITING_PROPERTY_FOR_INTRO")
		RETURN TRUE
	ENDIF
	#ENDIF

	// Keep this at the end of this function
	IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_ARMORY_TRUCK(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(playerID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF IS_PED_TRYING_TO_ENTER_OR_EXIT_VEHICLE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_ARMORY_TRUCK_IN_VEHICLE(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eModel)
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		INT i
		PLAYER_INDEX thePlayer
		REPEAT NUM_NETWORK_PLAYERS i
			thePlayer = INT_TO_PLAYERINDEX(i)
			
			IF thePlayer != PLAYER_ID()
			AND thePlayer != INVALID_PLAYER_INDEX()
			//AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
				IF IS_NET_PLAYER_OK(thePlayer)
					IF DOES_ENTITY_EXIST(pedVeh)
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), pedVeh, FALSE)
							IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(GET_PLAYER_PED(thePlayer))
							OR g_MultiplayerSettings.g_bSuicide
							OR IS_SELECTOR_ONSCREEN()
							OR  GET_IS_TASK_ACTIVE(GET_PLAYER_PED(thePlayer), CODE_TASK_EXIT_VEHICLE)
								#IF IS_DEBUG_BUILD
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK: ", GET_PLAYER_NAME(thePlayer)," not ready not set flag")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_PLAYER_COORDS(thePlayer)) <= 125
								IF NOT IS_REMOTE_PLAYER_IN_NON_CLONED_VEHICLE(thePlayer)
									IF GET_VEHICLE_PED_IS_ENTERING(GET_PLAYER_PED(thePlayer)) = pedVeh
										#IF IS_DEBUG_BUILD
										PRINTLN("[SIMPLE_INTERIOR][SMPL_H] ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK: ", GET_PLAYER_NAME(thePlayer)," not ready entering")
										#ENDIF
										RETURN FALSE
									ENDIF
								ENDIF
							ENDIF
							IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), pedVeh, TRUE)
							OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), pedVeh)
							
							
								#IF IS_DEBUG_BUILD
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_EXIT_ARMORY_TRUCK: ", GET_PLAYER_NAME(thePlayer)," entering or attached")
								#ENDIF
								
								RETURN FALSE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_PASSANGERS_ALIVE()
	INT iVehSeat
	VEHICLE_SEAT vSeat
	PED_INDEX pedPassanger
	VEHICLE_INDEX playerVeh =  GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	FOR iVehSeat = VS_FRONT_RIGHT TO VS_EXTRA_RIGHT_3
		vSeat = INT_TO_ENUM(VEHICLE_SEAT, iVehSeat)
		pedPassanger = GET_PED_IN_VEHICLE_SEAT(playerVeh, vSeat)
		IF DOES_ENTITY_EXIST(pedPassanger)
		AND IS_ENTITY_DEAD(pedPassanger)
			RETURN FALSE
		ENDIF
	ENDFOR
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_USE_VEHICLE(SIMPLE_INTERIOR_DETAILS &details,VEHICLE_INDEX pedVeh, VEHICLE_INDEX truckVeh, BOOL bCheckSectionOwnerShip = TRUE)
	OWNED_PROPERTIES_IN_BUILDING ownedDetails
	IF NOT DOES_ENTITY_EXIST(pedVeh)
	OR IS_ENTITY_DEAD(pedVeh)
	OR NOT IS_VEHICLE_DRIVEABLE(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE vehicle not drivable")
		RETURN FALSE
	ENDIF	
	
	IF IS_ENTITY_ON_FIRE(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE vehicle on is on fire")
		RETURN FALSE	
	ENDIF
	
	IF IS_ENTITY_UPSIDEDOWN(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE IS_ENTITY_UPSIDEDOWN")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_VEHICLE_ON_ALL_WHEELS(pedVeh)
	AND NOT IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
	AND GET_ENTITY_MODEL(pedVeh) != OPPRESSOR2
	AND NOT IS_DRIVER_ENTERING_THIS_SIMPLE_INTERIOR(PLAYER_ID(), SIMPLE_INTERIOR_ARMORY_TRUCK_1)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE IS_VEHICLE_ON_ALL_WHEELS")
		RETURN FALSE
	ENDIF
	
	IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 1.0
	AND NOT IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE GET_ENTITY_SPEED > 1.0")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WITH_A_PROSTITUTE()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE IS_PLAYER_WITH_A_PROSTITUTE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE player in corona")
		RETURN FALSE
	ENDIF
	
	
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID()) 
		MODEL_NAMES eVehModel = GET_ENTITY_MODEL(pedVeh)
		IF eVehModel = CADDY2
		OR eVehModel = CADDY3
		OR eVehModel = HAULER2
		OR eVehModel = PHANTOM3
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE player is in truck/golf caddy in bunker")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT ARE_ALL_PASSANGERS_ALIVE()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE FALSE ARE_ALL_PASSANGERS_ALIVE FALSE")
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE IS_CUSTOM_MENU_ON_SCREEN true")
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE IS_INTERACTION_MENU_OPEN true")
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsAmbience.R2Pdata.bStartingRacePointMenuFromPI
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE MPGlobalsAmbience.R2Pdata.bStartingRacePointMenuFromPI true")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE IS_SKYSWOOP_AT_GROUND false")
		RETURN FALSE
	ENDIF
	
	IF !ARE_ALL_PASSANGERS_PLAYERS()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE ARE_ALL_PASSANGERS_PLAYERS false")
		RETURN FALSE
	ENDIF
	
	IF bCheckSectionOwnerShip
		IF NOT ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK()
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE ARE_ALL_PEDS_IN_VEHICLE_READY_FOR_ENTER_ARMORY_TRUCK FALSE")
			RETURN FALSE
		ENDIF
		
		BOOL bOwnVehicleStorage = IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
		BOOL bOwnCarmodSection = IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
		
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)

			IF NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY(pedVeh,PROPERTY_TRUCK_REFERENCE_ONLY,ownedDetails, DEFAULT, DEFAULT, bOwnVehicleStorage, bOwnCarmodSection #IF IS_DEBUG_BUILD , FALSE ,#ENDIF  #IF NOT IS_DEBUG_BUILD , #ENDIF truckVeh)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE - IS_VEHICLE_SUITABLE_FOR_PROPERTY FALSE")
				RETURN FALSE
			ENDIF	
		
		ENDIF
		
	ENDIF
	
	IF IS_VEHICLE_ONLY_MODABLE_IN_DEFUNCT_BASE_MOD_SHOP(GET_ENTITY_MODEL(pedVeh))
		PRINTLN("[SIMPLE_INTERIOR][INT_SCRIPT] - IS_SAFE_TO_USE_VEHICLE  IS_VEHICLE_ONLY_MODABLE_IN_DEFUNCT_BASE_MOD_SHOP true")
		RETURN FALSE
	ENDIF
	
	IF DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(PLAYER_ID())
	OR DO_I_NEED_TO_RENOVATE_TRUCK_CAB(PLAYER_ID())
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  false DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER true ")
		RETURN FALSE
	ENDIF	
	
	IF GET_OWNER_OF_TRAILER_VEH_INDEX() != PLAYER_ID()	
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  false I'm not owner of this trailer")
		RETURN FALSE
	ENDIF
	
	IF !IS_VEHICLE_MY_PERSONAL_VEHICLE(pedVeh)
	AND NOT IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(pedVeh)
	AND IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  trying to enter mod shop without personal vehicle")
		RETURN FALSE
	ENDIF
	
	IF MPGlobalsAmbience.vehPegasusVehicle != pedVeh
	AND IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(pedVeh)
	AND IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  trying to enter mod shop with someone else's pegasus vehicle")
		RETURN FALSE
	ENDIF
	
	IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(pedVeh)
	AND MPGlobals.OldRemotePegV[NATIVE_TO_INT(PLAYER_ID())] = pedVeh
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  trying to enter mod shop with old pegasus vehicle")
		RETURN FALSE
	ENDIF
	
	IF g_sMPTunables.bDisable_storage_of_weaponised_vehicle_in_truck
	AND IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  trying to enter mod shop but bDisable_storage_of_weaponised_vehicle_in_truck is true")
		RETURN FALSE
	ENDIF	
	
	IF g_sMPTunables.bDisable_store_PV
	AND IS_VEHICLE_A_PERSONAL_VEHICLE(pedVeh)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  false vehicle personal vehicle but storage blocked by tuneable")
		
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP_NO_SOUND("PROP_NO_PVTUN")
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF g_sMPTunables.bDisable_storage_of_weaponised_vehicle_in_truck
	AND IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
	AND IS_VEHICLE_WEAPONISED_VEHICLE(pedVeh)
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			PRINT_HELP_NO_SOUND("PROP_NO_PVTUN")
		ENDIF
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  trying to enter mod shop but bDisable_storage_of_weaponised_vehicle_in_truck is true")
		RETURN FALSE
	ENDIF	
	
	IF g_sMPTunables.bDisable_swap_PVs_storage_module_drive_new
		INT iSaveSlot, iDisplaySlot
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
		MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),iDisplaySlot)
		//PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN- current save = ",CURRENT_SAVED_VEHICLE_SLOT()," current displaySlot = ",iDisplaySlot)
		IF iDisplaySlot = DISPLAY_SLOT_START_ARMOURY_TRUCK
		AND IS_VEHICLE_MY_PERSONAL_VEHICLE(pedVeh)
		ELIF iSaveSlot > 0
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE  false vehicle stored in truck and tuneable disables swap.")
			
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_NO_SOUND("PROP_NO_PVTUN")
			ENDIF
			
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF HAS_VEHICLE_BEEN_RIGGED_WITH_EXPLOSIVES(pedVeh, 0)
	OR HAS_VEHICLE_BEEN_RIGGED_WITH_EXPLOSIVES(pedVeh, 1)
		PRINTLN("[SIMPLE_INTERIOR] IS_SAFE_TO_USE_VEHICLE  vehicle has explosives")
		RETURN FALSE
	ENDIF	
		
	RETURN TRUE
ENDFUNC 

FUNC BOOL IS_PLAYER_INSIDE_ARMORY_TRUCK_ENTRY_AREA(VEHICLE_INDEX truckIndex)
	
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)
		IF GET_ENTITY_MODEL(truckIndex) = TRAILERLARGE

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID()
			, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0.0,-15,-7.0>>)
			, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0.0,-6.5,2.5>>)
			, GET_MODEL_WIDTH(TRAILERLARGE) * 2.5)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_ARMORY_TRUCK_REASON_FOR_BLOCKED_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)
	UNUSED_PARAMETER(iEntranceUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	
	IF SHOULD_BLOCK_SIMPLE_INTERIOR_ENTRY_FOR_ON_CALL()
		RETURN "SI_ENTR_BLCK4A"
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		RETURN ""
	ENDIF	
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT NETWORK_IS_IN_MP_CUTSCENE()
	AND PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
		VEHICLE_INDEX pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF NOT IS_VEHICLE_A_PERSONAL_TRUCK(pedVeh)
		AND NOT IS_VEHICLE_MODEL(pedVeh, CADDY2)
		AND NOT IS_VEHICLE_MODEL(pedVeh, CADDY3)
			IF !IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
			AND !IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
				IF IS_VEHICLE_A_PERSONAL_VEHICLE(pedVeh)
					// You do not currently own the Truck vehicle storage module. Purchase one at www.warstock-cache-and-carry.com.
					RETURN "GR_TRUCK_STOR_P"
				ELIF IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_MOD_SHOP(pedVeh)
					// You do not currently own the Truck vehicle mod & armory module. Purchase one at www.warstock-cache-and-carry.com.
					RETURN "GR_TRUCK_MOD_P"
				ELSE
					// You do not currently own the Truck vehicle storage module or the Truck mod & armory module. Purchase one at www.warstock-cache-and-carry.com.
					RETURN "GR_TRUCK_BUY_A"
				ENDIF
			ENDIF	
		ENDIF	
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		//AND !GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			
			PLAYER_INDEX piGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
				// You no longer have access to the Mobile Operations Center as you became a Prospect.
				RETURN "MP_TRUCK_KICKd"
			ELSE
				IF piGangBoss != INVALID_PLAYER_INDEX()
				AND DOES_PLAYER_OWN_OFFICE(piGangBoss)
					//You no longer have access to the Truck as you became an Associate.
					RETURN "MP_TRUCK_KICKa"
				ELSE
					// You no longer have access to the Truck as you became a Bodyguard.
					RETURN "MP_TRUCK_KICKb"
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
		
	IF (PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
	OR GB_GET_LOCAL_PLAYER_GANG_BOSS() = GET_OWNER_OF_TRAILER_VEH_INDEX())
		IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
			RETURN "JUG_BLOCK_MOC"
		ENDIF
		
		IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
			// You cannot enter whilst delivering a bounty.
			RETURN "BOUNTY_PROP_BLCK"
		ENDIF
	ENDIF
	
	IF IS_PLAYER_WITH_A_PROSTITUTE()
		// You cannot enter your mobile operations center with a prostitute.
		RETURN "TRUC_MC_BLOCK_P"
	ENDIF
	
	IF IS_NPC_IN_VEHICLE()
		// You cannot enter whilst an NPC is in the vehicle.
		RETURN "NPC_BLOCK"
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID()) 
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()) 
		// You cannot enter your Mobile Operations Center while active in a mission.
		RETURN "TRUC_MC_BLOCK_E"
	ENDIF
	
	IF GET_PLAYER_RIVAL_ENTITY_TYPE(PLAYER_ID()) = eRIVALENTITYTYPE_GUNRUNNING
		// you can't enter the armory truck while carrying supplies
		RETURN "SUP_BLOCK_MOC"
	ENDIF
	
	IF g_iPreventPropertyAccessBS != 0
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				// You cannot enter your Mobile Operations Center while in a mission vehicle.
				RETURN "TRUC_MC_BLOCK_V"
			ENDIF	
		ENDIF	
	ENDIF	
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			OR GET_SEAT_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) = ENUM_TO_INT(VS_DRIVER)
				IF PLAYER_ID() != GET_OWNER_OF_TRAILER_VEH_INDEX()
					CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
					PRINTLN("GET_ARMORY_TRUCK_REASON_FOR_BLOCKED_ENTRY player is not truck owner and is in vehicle driver seat")
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BLOCK_SUITABLE_AREA)
		// You are unable to access the the Mobile Operations Center in this area. Move the Mobile Operations Center to suitable area.
		RETURN "GR_TRUCK_SUTAR"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ARMORY_TRUCK_REASON_FOR_BLOCKED_EXIT(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iExitUsed)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iExitUsed)
	
	IF SHOULD_JOB_ENTRY_TYPE_BLOCK_SIMPLE_INTERIOR_TRANSITION()
		RETURN "SI_EXIT_BLCK4A"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL CAN_PLAYER_INVITE_OTHERS_TO_ARMORY_TRUCK(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(playerID)
	
	IF PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
		IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty = -1	//Only invited players will have this set whilst they autowarp so block invites
			IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(PLAYER_ID())
			OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)		
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_INVITED_TO_ARMORY_TRUCK(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF playerID != INVALID_PLAYER_INDEX()
			IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance != globalPlayerBD[NATIVE_TO_INT(playerID)].SimpleInteriorBD.iInstance
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		PRINTLN("CAN_PLAYER_BE_INVITED_TO_ARMORY_TRUCK FALSE IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY")
		RETURN FALSE
	ENDIF
	
	IF playerID != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerID)
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerID))
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_BE_INVITED_TO_ARMORY_TRUCK_BY_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
		IF ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), pInvitingPlayer, FALSE, TRUE)
		OR HAVE_I_GOT_TRUCK_INVITE_INSTANCE()
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GET_ARMORY_TRUCK_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			txtTitle = "ARMORY_TITLE"
		BREAK
	ENDSWITCH
	
	strOptions[0] = "BWH_ENTRM_ALONE"
	eventOptions[0] = SI_EVENT_ENTRY_MENU_ENTER_ALONE
	
	//Enter with org members
	strOptions[1] = "PROP_HEI_E_3"
	//Enter with nearby friends & crew
	strOptions[2] = "PROP_HEI_E_2"
	
	eventOptions[1] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	eventOptions[2] = SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY
	iOptionsCount = 3
ENDPROC

/// PURPOSE:
///    Determines our gang staus on entry to the armory truck. Uses this t set g_SimpleInteriorData.iAccessBS
/// PARAMS:
///    playerBoss - The local players gang boss
///    bWarpingToPlayersProperty - If we are using iOverrideAutoWarpToPlayersProperty to determine which armory truck to enter
PROC SET_ARMORY_TRUCK_GANG_STATUS_BS(PLAYER_INDEX playerBoss, BOOL bWarpingToPlayersProperty = FALSE)
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
	AND (playerBoss = PLAYER_ID()
	OR GB_ARE_PLAYERS_IN_SAME_GANG(playerBoss, PLAYER_ID()))
		// Create flags for everyone who potentially we would like to join in the armory truck
		IF NOT bWarpingToPlayersProperty
			IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_BOSS)
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)	
				g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID()
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_GANG_STATUS_BS - Entering as the boss & Owner")
			ELSE
				//We are a goon
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_GOON)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_GANG_STATUS_BS - Entering as a goon")
				
				IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
					
					//We are in an MC
					SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_GOON_MC)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_GANG_STATUS_BS - Entering as a goon in an MC")
					
				ELIF playerBoss != INVALID_PLAYER_INDEX()
				AND DOES_PLAYER_OWN_OFFICE(playerBoss)
					
					//We are an associate
					SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_ASSOCIATE)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_GANG_STATUS_BS - Entering as an associate")
					
				ENDIF
				g_ownerOfArmoryTruckPropertyIAmIn = playerBoss
			ENDIF
		ELSE
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_MEMBER_OF_OTHER_GANG)
			g_ownerOfArmoryTruckPropertyIAmIn = playerBoss
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_GANG_STATUS_BS - Entering as a member of another gang")
		ENDIF
	ELSE
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_GANG_STATUS_BS - Entering whilst not a member of a gang")
	ENDIF
ENDPROC

PROC SET_ARMORY_TRUCK_ACCESS_BS(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS = 0
	g_SimpleInteriorData.iAccessBS = 0
	PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	
	CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS before assign roles: g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty : ", g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty,
	" IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK: ", IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK() )
	
	IF (IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK())
	AND g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
	AND g_ownerOfBunkerPropertyIAmIn != PLAYER_ID()
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
		IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty != NATIVE_TO_INT(PLAYER_ID())

			IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty != -1
					
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER)
				
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H] SET_ARMORY_TRUCK_ACCESS_BS moving between truck and bunker - joining : ", 
							GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)), " Owner is : ", GET_PLAYER_NAME(g_ownerOfBunkerPropertyIAmIn))
							
				g_ownerOfArmoryTruckPropertyIAmIn = g_ownerOfBunkerPropertyIAmIn
			ELSE
			
				IF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty = -1 
					SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(g_ownerOfBunkerPropertyIAmIn)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SET_ARMORY_TRUCK_ACCESS_BS - g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty bunker to truck is -1 setting to : ", NATIVE_TO_INT(g_ownerOfBunkerPropertyIAmIn))
				ENDIF	
				
				g_ownerOfArmoryTruckPropertyIAmIn = g_ownerOfBunkerPropertyIAmIn
			ENDIF
			
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = g_ownerOfArmoryTruckPropertyIAmIn
		ELSE
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SET_ARMORY_TRUCK_ACCESS_BS - HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK ture but failing conditions")
		ENDIF	
		
	ELIF g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty != -1
	AND g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty != NATIVE_TO_INT(PLAYER_ID())
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())	
		SET_ARMORY_TRUCK_GANG_STATUS_BS(playerBoss, TRUE)
		
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER)
		g_ownerOfArmoryTruckPropertyIAmIn = INT_TO_PLAYERINDEX(g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS - We want to join owner: ", 
					GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)), " (", g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty, ")")
		
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = g_ownerOfArmoryTruckPropertyIAmIn
	ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
	AND NOT IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
	
		SET_ARMORY_TRUCK_GANG_STATUS_BS(playerBoss)
		
		IF playerBoss != INVALID_PLAYER_INDEX()			
			//Set this so we know who the owner is
			globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = playerBoss
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS - We want to join our boss: ", GET_PLAYER_NAME(playerBoss))
		ENDIF
	ELIF IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
		SET_ARMORY_TRUCK_GANG_STATUS_BS(playerBoss, TRUE)
		
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER)
		g_ownerOfArmoryTruckPropertyIAmIn = g_SimpleInteriorData.driverPlayer
		CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS - We want to join owner in vehicle as passenger: ", 
					GET_PLAYER_NAME(g_SimpleInteriorData.driverPlayer))
					
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = g_SimpleInteriorData.driverPlayer
	ELIF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(PLAYER_ID())
	AND (IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER) OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
	AND PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
		
		g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID()	
			
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			//SET_CONCEAL_CURRENT_PERSONAL_VEHICLE(PLAYER_ID(), ENUM_TO_INT(SIMPLE_INTERIOR_ARMORY_TRUCK_1), PV_CONCEAL_TYPE_ARMORY_TRUCK)
			IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				SET_CONCEAL_CURRENT_PEGASUS_VEHICLE(PLAYER_ID(), ENUM_TO_INT(SIMPLE_INTERIOR_ARMORY_TRUCK_1), PV_CONCEAL_TYPE_ARMORY_TRUCK)
			ELSE
				IF IS_VEHICLE_A_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					SET_CONCEAL_CURRENT_PERSONAL_VEHICLE(PLAYER_ID(), ENUM_TO_INT(SIMPLE_INTERIOR_ARMORY_TRUCK_1), PV_CONCEAL_TYPE_ARMORY_TRUCK)
				ENDIF
			ENDIF
		ENDIF
		
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS - we are entering a truck that we own. Non gang entry")
		globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = g_ownerOfArmoryTruckPropertyIAmIn
	ENDIF
	
	IF globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != INVALID_PLAYER_INDEX()
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
			g_SimpleInteriorData.iBunkerIDTruckIsIn = ENUM_TO_INT(GET_OWNED_BUNKER(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS - g_SimpleInteriorData.iBunkerIDTruckIsIn: ", g_SimpleInteriorData.iBunkerIDTruckIsIn)
		ENDIF
	ENDIF
	
	
	CLEAR_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE()
	PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SET_ARMORY_TRUCK_ACCESS_BS - clearing iOverrideAutoWarpToPlayersProperty.")
	
ENDPROC

PROC TRIGGER_LOCAL_PLAYER_ARMORY_TRUCK_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID)
	SET_ARMORY_TRUCK_ACCESS_BS(eSimpleInteriorID, -1)
			
	// We need to set this manually so DO_SIMPLE_INTERIOR_AUTOWARP will work, 
	// otherwise it will return TRUE straight away as it checks if local player is in the interior, 
	// it's a bit of a hack but a really small, harmless one.
	g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID 
			
	SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, eSimpleInteriorID)
	g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
	g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE
ENDPROC

PROC PROCESS_ARMORY_TRUCK_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)
		
	CONST_INT MAX_NEARBY_PEDS_TO_CHECK 10
	BOOL bFriendsNearby, bGangMembersNearby
	INT i, iPeds
	INT iLocalCrewID = -1
	PED_INDEX mPed[MAX_NEARBY_PEDS_TO_CHECK]
	VECTOR vEntryLocate
	
	IF CAN_PLAYER_INVITE_OTHERS_TO_ARMORY_TRUCK(PLAYER_ID(), eSimpleInteriorID)
		
		IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			iLocalCrewID = GET_LOCAL_PLAYER_CLAN_ID()
		ENDIF
		
		//GET_ARMORY_TRUCK_ENTRY_LOCATE(eSimpleInteriorID, vEntryLocate)
		
		vEntryLocate = GET_PLAYER_COORDS(PLAYER_ID())
		
		iPeds = GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), mPed)
		
		IF iPeds > 0
			REPEAT iPeds i 
				IF NOT IS_ENTITY_DEAD(mPed[i])
				AND IS_PED_A_PLAYER(mPed[i])
				
					IF VDIST(GET_ENTITY_COORDS(mPed[i]), vEntryLocate) <= GET_SIMPLE_INTERIOR_INVITE_RADIUS(eSimpleInteriorID)
						
						PLAYER_INDEX piPlayer 		= NETWORK_GET_PLAYER_INDEX_FROM_PED(mPed[i])
						GAMER_HANDLE playerHandle 	= GET_GAMER_HANDLE_PLAYER(piPlayer)
						
						IF NOT bFriendsNearby
							IF iLocalCrewID != -1
							AND iLocalCrewID = GET_MP_PLAYER_CLAN_ID(playerHandle)
							OR NETWORK_IS_FRIEND(playerHandle)
								IF NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(piPlayer))
								AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piPlayer, TRUE)
								AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(piPlayer)
								AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(piPlayer, eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
								AND NOT IS_PLAYER_AN_ANIMAL(piPlayer)
									bFriendsNearby = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bGangMembersNearby
						AND GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), piPlayer)
						AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(piPlayer))
						AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(piPlayer, TRUE)
						AND NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(piPlayer)
						AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(piPlayer, eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
						AND NOT IS_PLAYER_AN_ANIMAL(piPlayer)
							bGangMembersNearby = TRUE
						ENDIF
					ENDIF
					
					//The list of peds in order of proximity to the local player
					//If the current ped isn't close enough none of the others will be
					//Also if we've found both gang members & friends close enough we don't need to search anymore
					IF bGangMembersNearby 
					AND bFriendsNearby
						BREAKLOOP
					ENDIF
				ENDIF
			ENDREPEAT		
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] PROCESS_ARMORY_TRUCK_ENTRANCE_MENU - bGangMembersNearby: ", bGangMembersNearby, " bFriendsNearby: ", bFriendsNearby)
		#ENDIF
		
		IF bGangMembersNearby
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, TRUE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		ENDIF
		
		IF bFriendsNearby
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, TRUE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, FALSE)
		ENDIF
		
		//We have to do this here, otherwise we'll end up with the server giving us the incorrect instance
		IF (bGangMembersNearby OR bFriendsNearby)
		AND NOT IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
		AND NOT IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PLAYER_ID())
			SET_ARMORY_TRUCK_ACCESS_BS(eSimpleInteriorID, -1)
		ENDIF
		
		BOOL bFromBunkerToTruck = IS_PLAYER_NEED_TO_CHECK_FOR_NEARBY_PLAYERS(PLAYER_ID())
		
		IF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(0, menu)
			SET_ARMORY_TRUCK_ACCESS_BS(eSimpleInteriorID, -1)
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(1, menu)
			IF bGangMembersNearby
				BROADCAST_ARMORY_TRUCK_EXIT_SPAWN_POINT(TRUE)
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID, vEntryLocate
																, TRUE, FALSE, FALSE, bFromBunkerToTruck , globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] PROCESS_ARMORY_TRUCK_ENTRANCE_MENU - Not sending broadcast as there's no gang members around entrance.")
				#ENDIF
			ENDIF
			
			TRIGGER_LOCAL_PLAYER_ARMORY_TRUCK_ENTRY(eSimpleInteriorID)
			
		ELIF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(2, menu)
			IF bFriendsNearby
				BROADCAST_ARMORY_TRUCK_EXIT_SPAWN_POINT(TRUE)
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID, vEntryLocate
																, FALSE, FALSE, TRUE, bFromBunkerToTruck  , globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] PROCESS_BUNKER_ENTRANCE_MENU - Not sending broadcast as there's no friends and crew around entrance.")
				#ENDIF
			ENDIF
			
			TRIGGER_LOCAL_PLAYER_ARMORY_TRUCK_ENTRY(eSimpleInteriorID)
		ENDIF
	ELSE
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 2, FALSE)
	ENDIF
ENDPROC

FUNC STRING GET_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT_REASON(INT iReason)
	STRING sReturn
	
	SWITCH iReason
		CASE KICK_TO_BUNK_EXT_REASON_OWNER_ON_MISSION_NON_GANG_MEMBER
			RETURN "MP_BUNKER_KICKj"
		CASE KICK_TO_BUNK_EXT_REASON_IAM_OWNER_TRADED_IN_BUNKER
			RETURN "MP_PROP_PUR_KICK0"
		CASE KICK_TO_BUNK_EXT_REASON_MY_OWNER_TRADED_IN_BUNKER
			RETURN "MP_PROP_PUR_KICK1"
		CASE KICK_TO_BUNK_EXT_REASON_OWNER_LEFT
			RETURN "MP_TRUCK_KICKi"
	ENDSWITCH
	
	RETURN sReturn
ENDFUNC

FUNC BOOL SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT(INT &iKickOutReason)

	UNUSED_PARAMETER(iKickOutReason)
	
	BOOL bOnMission = FALSE

	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
			//AND !GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
					
				PLAYER_INDEX piGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				
				IF piGangBoss != INVALID_PLAYER_INDEX()
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					ELSE
						SET_ARMORY_VEHICLE_OWNER_BECAME_PART_OF_GANG(TRUE)
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT - owner joined a gang.")
						RETURN TRUE
					ENDIF	
				ENDIF
			ENDIF
		ENDIF	
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(g_ownerOfArmoryTruckPropertyIAmIn) = FMMC_TYPE_GUNRUNNING_BUY
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(g_ownerOfArmoryTruckPropertyIAmIn) = FMMC_TYPE_GUNRUNNING_SELL
			IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT TRUE FMMC_TYPE_GUNRUNNING_SELL or FMMC_TYPE_GUNRUNNING_BUY")
				bOnMission = TRUE
			ENDIF
		ENDIF
		IF g_SimpleInteriorData.iBunkerIDTruckIsIn != ENUM_TO_INT(GET_OWNED_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn))
		AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
		AND g_SimpleInteriorData.iBunkerIDTruckIsIn != -1
			
			IF g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID() 
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
				ENDIF	
			ENDIF
			
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_BUNKER_TRADE)
			AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
				SET_BIT(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_BUNKER_TRADE)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT - owner traded bunker")
				
				IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
					//You no longer have access to the property as you have traded it in.
					iKickOutReason = KICK_TO_BUNK_EXT_REASON_IAM_OWNER_TRADED_IN_BUNKER
				ELSE
					// You no longer have access to the property as the owner has traded it in.
					iKickOutReason = KICK_TO_BUNK_EXT_REASON_MY_OWNER_TRADED_IN_BUNKER
				ENDIF	
				
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_SKYSWOOP_AT_GROUND()
			IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT - sky cam is not in ground")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET (g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF NOT IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn, FALSE, TRUE)
			AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
		
				// owner left you no longer have access to property 
				iKickOutReason = KICK_TO_BUNK_EXT_REASON_OWNER_LEFT
				
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	IF bOnMission
		
		IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn) 
		OR IS_PLAYER_TRUCK_DESTROYED(g_ownerOfArmoryTruckPropertyIAmIn)
			RETURN FALSE
		ENDIF
		
		IF g_ownerOfArmoryTruckPropertyIAmIn != PLAYER_ID()
		AND g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ELSE	
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
				AND GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), g_ownerOfArmoryTruckPropertyIAmIn)
					//We were kicked because we're on mission with the owner
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT kicked as part of the gang doing a gunrun mission")
				ELSE
					//We were kicked as the owner was on a mission
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT kicked as the owner has started a mission and I'm not part of their gang")
					iKickOutReason = KICK_TO_BUNK_EXT_REASON_OWNER_ON_MISSION_NON_GANG_MEMBER
				ENDIF
								
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LP_BE_KICKED_FROM_ARMOURY_TRUCK_TO_BUNKER_EXT TRUE all players kicked out")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
		
	IF NOT IS_BIT_SET (g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF NOT IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn, FALSE, TRUE)
			AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER_LEFT_GAME)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK - owner left the session.")
				RETURN TRUE
			ENDIF
		ENDIF	
		
	ELIF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
		//AND !GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			
			PLAYER_INDEX piGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			
			IF piGangBoss != INVALID_PLAYER_INDEX()
				SET_ARMORY_VEHICLE_OWNER_BECAME_PART_OF_GANG(TRUE)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK - owner joined a gang.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(g_ownerOfArmoryTruckPropertyIAmIn)
		OR DO_I_NEED_TO_RENOVATE_TRUCK_CAB(g_ownerOfArmoryTruckPropertyIAmIn)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
			ELSE
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
				AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK - owner renovated trailer kicking owner out ")
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF

		IF NOT IS_BIT_SET (g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(g_ownerOfArmoryTruckPropertyIAmIn, FALSE)
				//The owner has joined another gang
				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_EXIT_OWNER_JOINED_GANG)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK TRUE SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_EXIT_OWNER_JOINED_GANG ")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF NOT IS_SKYSWOOP_AT_GROUND()
			IF !IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK - sky cam is not in ground")
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBS,  BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR)
		AND PLAYER_ID() != g_ownerOfArmoryTruckPropertyIAmIn
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED)
					SET_BIT(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED TRUE")
				ENDIF
			ELSE	
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
				AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK TRUE owner kicked me out")
					RETURN TRUE
				ENDIF	
			ENDIF	
		ENDIF
		
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_BIT_SET(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED)
					SET_BIT(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED TRUE")
				ENDIF
			ELSE	
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
				AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK TRUE all players kicked out")
					RETURN TRUE
				ENDIF	
			ENDIF	
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED)
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) != WAITING_TO_START_TASK
		AND IS_SCREEN_FADED_IN()
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AS_TRUCK_DESTROYED TRUE kick player out")
			RETURN TRUE
		ENDIF
		
		IF (MPGlobalsAmbience.bMobileCommandCentreIsStuck)
		AND (MPGlobalsAmbience.bMobileCommandCentreShouldLeave)
		AND IS_SCREEN_FADED_IN()
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK TRUE MPGlobalsAmbience.bMobileCommandCentreIsStuck AND MPGlobalsAmbience.bMobileCommandCentreShouldLeave")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_TRUCK_CUSTOM_SPAWN_POINTS(VECTOR &vSpawnPoint, INT iIndex)
	FLOAT fDist, fSpawnPointsDist
	fSpawnPointsDist = 3.5
	VECTOR vCentre
	FLOAT fAngle
	
	IF IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn)
		IF IS_ARMORY_VEHICLE_OWNER_BECAME_PART_OF_GANG()
			IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[1])
			AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehTruckVehicle[1])
				vCentre = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(MPGlobalsAmbience.vehTruckVehicle[1]), GET_ENTITY_HEADING(MPGlobalsAmbience.vehTruckVehicle[1]), <<2,-17, 0>>)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_TRUCK_CUSTOM_SPAWN_POINTS - owner getting kicked out as joined other gang")
			ENDIF	
		ELSE	
			vCentre = GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
		
		ENDIF
		
		IF (iIndex = 0)
			vSpawnPoint =  vCentre	
		ELSE
			fAngle = (TO_FLOAT(iIndex) * 90.0)										
			SWITCH iIndex
				CASE 1
				CASE 2
				CASE 3
				CASE 4
					fAngle += 22.5
					fDist = fSpawnPointsDist * 1.0
				BREAK
				CASE 5
				CASE 6
				CASE 7
				CASE 8
					fAngle += 67.5
					fDist = fSpawnPointsDist * (1.4142 * 1.0)
				BREAK
				CASE 9
				CASE 10
				CASE 11
				CASE 12
					fAngle += 22.5
					fDist = fSpawnPointsDist * 2.0
				BREAK
				CASE 13
				CASE 14
				CASE 15
				CASE 16
					fAngle += 67.5
					fDist = fSpawnPointsDist * (1.4142 * 2.0)
				BREAK
			ENDSWITCH
			
			vSpawnPoint.x = (SIN((fAngle ))) * fDist
			vSpawnPoint.y = (COS((fAngle ))) * fDist
			vSpawnPoint.z = 0.0
			
			vSpawnPoint += vCentre
			vSpawnPoint.z += 1.0						
		ENDIF
		FLOAT fGroundZ
		IF GET_GROUND_Z_FOR_3D_COORD(vSpawnPoint, fGroundZ)
			vSpawnPoint.z = fGroundZ
		ELSE
			vSpawnPoint.z = vCentre.z
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_ARMORY_TRUCK_KICK_OUT_REASON(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(txtPlayerNameString)
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER_LEFT_GAME)
		RETURN "MP_TRUCK_KICKi"
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		//AND !GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
			
			PLAYER_INDEX piGangBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
				// You no longer have access to the Mobile Operations Center as you became a Prospect.
				RETURN "MP_TRUCK_KICKd"
			ELSE
				IF piGangBoss != INVALID_PLAYER_INDEX()
				AND DOES_PLAYER_OWN_OFFICE(piGangBoss)
					//You no longer have access to the Truck as you became an Associate.
					RETURN "MP_TRUCK_KICKa"
				ELSE
					// You no longer have access to the Truck as you became a Bodyguard.
					RETURN "MP_TRUCK_KICKb"
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_EMPTY_ARMORY_TRUCK_TRAILER)
		AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBSTwo,  BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_RENOVATED_MOC)
			IF PLAYER_ID() = g_ownerOfArmoryTruckPropertyIAmIn
				RETURN "MP_TRUCK_KICKe"
			ELSE
				RETURN "MP_TRUCK_KICKf"
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_EXIT_OWNER_JOINED_GANG)
		OR (IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.iBS,  BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYERS_OUT_OF_ARMORY_TRUCK_INTERIOR) AND !IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER))
			// Owner has requested all to leave the Mobile Operations Center.
			RETURN "MP_TRUCK_KICKc"
		ENDIF
	ENDIF

	RETURN ""
ENDFUNC

FUNC BOOL GET_ARMORY_TRUCK_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle  , BOOL bUseSecondaryInteriorDetails = FALSE )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)

	//Assign Default Positions, Truck section 3 middle line.
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			SWITCH iSpawnPoint
			CASE 0
			    vSpawnPoint = <<1.0796, 7.4929, 0.0124>>
			    fSpawnHeading = 171.0729
			    
			BREAK
			CASE 1
			    vSpawnPoint = <<0.7036, 6.0105, 0.0501>>
			    fSpawnHeading = 174.0729
			    
			BREAK
			CASE 2
			    vSpawnPoint = <<-0.2024, 4.8689, 0.0124>>
			    fSpawnHeading = 184.0729
			    
			BREAK
			CASE 3
			    vSpawnPoint = <<-1.0444, 7.2759, 0.0124>>
			    fSpawnHeading = 180.0729
			    
			BREAK
			CASE 4
			    vSpawnPoint = <<-0.1373, 4.0171, 0.0124>>
			    fSpawnHeading = 181.0729
			    
			BREAK
			CASE 5
			    vSpawnPoint = <<0.0206, 6.9861, 0.0124>>
			    fSpawnHeading = 184.0729
			    
			BREAK
			CASE 6
			    vSpawnPoint = <<-0.8264, 5.8521, 0.0124>>
			    fSpawnHeading = 184.0729
			BREAK
			CASE 7
			    vSpawnPoint = <<0.1387, 8.3999, 0.0124>>
			    fSpawnHeading = 184.0729
			BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			IF g_ownerOfArmoryTruckPropertyIAmIn = INVALID_PLAYER_INDEX()
				PRINTLN("[SIMPLE_INTERIOR][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - owner is invalid, assigning default.")
				//Just  , already assigned default.
				RETURN TRUE
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - owner is valid, picking a location based on setup. owner Name: ", GET_PLAYER_NAME(g_ownerOfArmoryTruckPropertyIAmIn))
			IF !bSpawnInVehicle
			OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), HAKUCHOU2) AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER)
			OR (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_TURRET_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GET_SEAT_PED_IS_IN(PLAYER_PED_ID())))
				IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_EMPTY_DOUBLE
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_SECOND_SECTION = AT_ST_EMPTY_DOUBLE")
					SWITCH iSpawnPoint
						CASE 0
						    vSpawnPoint = <<1.0796, 7.7930, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 1
						    vSpawnPoint = <<0.9036, 6.0105, 0.0501>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 2
						    vSpawnPoint = <<-1.1024, 4.4690, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 3
						    vSpawnPoint = <<-1.1444, 7.2759, 0.0124>>
						    fSpawnHeading = 180.0729
						    RETURN TRUE
						BREAK
						CASE 4
						    vSpawnPoint = <<0.6626, 2.6169, 0.0124>>
						    fSpawnHeading = 181.0729
						    RETURN TRUE
						BREAK
						CASE 5
						    vSpawnPoint = <<0.9207, 3.9861, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 6
						    vSpawnPoint = <<-0.8264, 5.8521, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 7
						    vSpawnPoint = <<0.1387, 8.3999, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK

					ENDSWITCH
				ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_CARMOD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_SECOND_SECTION = AT_ST_CARMOD")
					SWITCH iSpawnPoint
						CASE 0
						    vSpawnPoint = <<2.3196, 6.7981, 0.0124>>
						    fSpawnHeading = 179.0729
						    RETURN TRUE
						BREAK
						CASE 1
						    vSpawnPoint = <<-1.3563, -0.2839, 0.0501>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 2
						    vSpawnPoint = <<1.0366, -1.8259, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 3
						    vSpawnPoint = <<-2.1044, 6.2808, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 4
						    vSpawnPoint = <<1.1027, 8.5220, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 5
						    vSpawnPoint = <<1.6606, -0.5081, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 6
						    vSpawnPoint = <<-1.2874, -1.9431, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
						CASE 7
						    vSpawnPoint = <<-0.7214, 8.3049, 0.0124>>
						    fSpawnHeading = 184.0729
						    RETURN TRUE
						BREAK
					ENDSWITCH
				ELSE
					SWITCH GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, g_ownerOfArmoryTruckPropertyIAmIn)
						CASE AT_ST_LIVING_ROOM
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_THIRD_SECTION = AT_ST_LIVING_ROOM")
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<0.0338, 9.7378, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<0.0186, 8.2380, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.5964, 7.0381, 0.0013>>
								    fSpawnHeading = 194.5643
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<0.4877, 6.5381, 0.0013>>
								    fSpawnHeading = 164.5643
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.0272, 5.3379, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<-0.0425, 4.3379, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.0577, 3.3379, 0.0013>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
						CASE AT_ST_GUNMOD
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_THIRD_SECTION = AT_ST_GUNMOD")
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<0.0326, 8.8320, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<0.9302, 7.6123, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.5931, 6.1360, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<0.9066, 6.1125, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.6167, 4.6362, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<0.8831, 4.6128, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.6403, 3.1365, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<0.8595, 3.1130, 0.0000>>
								    fSpawnHeading = 179.1013
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
						CASE AT_ST_EMPTY_SINGLE
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_THIRD_SECTION = AT_ST_EMPTY_SINGLE")
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<-0.9473, 9.3569, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<1.0527, 9.3418, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.9625, 7.3569, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<1.0375, 7.3418, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.9778, 5.6570, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<1.0222, 5.6421, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.9930, 3.9570, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<1.0070, 3.9419, 0.0023>>
								    fSpawnHeading = 179.5643
								    RETURN TRUE
								BREAK
							ENDSWITCH	
						BREAK
						CASE AT_ST_COMMAND_CENTER
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_THIRD_SECTION = AT_ST_COMMAND_CENTER")
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<1.0796, 7.4929, 0.0124>>
								    fSpawnHeading = 171.0729
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<0.7036, 6.0105, 0.0501>>
								    fSpawnHeading = 174.0729
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.2024, 4.8689, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<-1.0444, 7.2759, 0.0124>>
								    fSpawnHeading = 180.0729
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.1373, 4.0171, 0.0124>>
								    fSpawnHeading = 181.0729
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<0.0206, 6.9861, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<-0.8264, 5.8521, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<0.1387, 8.3999, 0.0124>>
								    fSpawnHeading = 184.0729
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
						CASE AT_ST_VEHICLE_STORAGE
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - ATS_THIRD_SECTION = AT_ST_VEHICLE_STORAGE")
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<1.5734, 9.0129, 0.0000>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<-1.8969, 9.0129, 0.0000>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<1.5736, 7.1130, 0.0000>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<-1.8964, 7.2129, 0.0000>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<1.5734, 5.1130, 0.0000>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<-1.8969, 5.2129, 0.0000>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<1.5734, 3.6130, 0.0012>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<-1.7964, 3.8130, 0.0012>>
								    fSpawnHeading = 180.0000
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK

						DEFAULT
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H][ARMORY_TRUCK] GET_ARMORY_TRUCK_SPAWN_POINT - DEFAULT ?")
							SWITCH iSpawnPoint
								CASE 0
								    vSpawnPoint = <<0.6499, -0.5879, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 1
								    vSpawnPoint = <<-0.1263, -2.4700, 0.0501>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 2
								    vSpawnPoint = <<-0.0323, -8.5120, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 3
								    vSpawnPoint = <<-0.0743, -7.1050, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
								CASE 4
								    vSpawnPoint = <<-0.6674, -1.7639, 0.0013>>
								    fSpawnHeading = 310.3200
								    RETURN TRUE
								BREAK
								CASE 5
								    vSpawnPoint = <<-0.1094, -10.1951, 0.0501>>
								    fSpawnHeading = 351.7200
								    RETURN TRUE
								BREAK
								CASE 6
								    vSpawnPoint = <<0.1437, -3.9290, 0.0013>>
								    fSpawnHeading = 0.0000
								    RETURN TRUE
								BREAK
								CASE 7
								    vSpawnPoint = <<-0.1914, 0.6189, 0.0013>>
								    fSpawnHeading = 188.2607
								    RETURN TRUE
								BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				ENDIF	
			ELSE
				IF !IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(g_ownerOfArmoryTruckPropertyIAmIn)
					SWITCH iSpawnPoint
						CASE 0
						    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VIGILANTE)
								OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), RROCKET)
								OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), CARACARA)
								OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), CARACARA2)
									vSpawnPoint = <<0.0967, 4.0+0.50, -0.0>>
									fSpawnHeading = 180.0
									RETURN TRUE
								ENDIF
							ENDIF	
							vSpawnPoint = <<0.0967, 2.9719, -0.0>>
							fSpawnHeading = 180.0
							RETURN TRUE
						BREAK
					ENDSWITCH
				ELSE
					SWITCH iSpawnPoint
						CASE 0
						   vSpawnPoint = <<-0.0044, 6.1121, 0.0000>>
							fSpawnHeading = 180.0
							RETURN TRUE
						BREAK
						CASE 1
							vSpawnPoint = <<1103.5580, -2993.8879, -38.8634>>
							fSpawnHeading = 180.0000
   							RETURN FALSE //so this is not considered for normal vehicle spawning
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
			ENDIF
		BREAK
	ENDSWITCH
		
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS GET_ARMORY_TRUCK_CUSTOM_INTERIOR_WARP_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS eParams
	
	eParams.bIgnoreObstructions 	= FALSE
	eParams.bIgnoreDistanceChecks 	= FALSE
	
	RETURN eParams
ENDFUNC

FUNC BOOL GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	UNUSED_PARAMETER(bSpawnInVehicle)
	
	IF (IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_TO_BUNKER_EXT_SELECTED(g_ownerOfArmoryTruckPropertyIAmIn) OR IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_EXIT_FROM_VEHICLE_INTERIOR_TO_OUTSIDE_SIMPLE_EXTERIOR))
	AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
		
			IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_EXIT_INTERIOR_DUE_TO_BUNKER_TRADE)
				eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(INT_TO_ENUM(FACTORY_ID,g_SimpleInteriorData.iBunkerIDTruckIsIn)))
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT - spawning outside old bunker ")
			ELSE
				eSimpleInteriorID = GET_OWNED_BUNKER_SIMPLE_INTERIOR(g_ownerOfArmoryTruckPropertyIAmIn)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT - spawning outside bunker ")
			ENDIF
		
		RETURN GET_BUNKER_OUTSIDE_SPAWN_POINT(eSimpleInteriorID,  iSpawnPoint,  vSpawnPoint,  fSpawnHeading,  bSpawnInVehicle) 
	ELSE	
	
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			
				IF iSpawnPoint > 16 // Exit once all spawn points have been added
					RETURN FALSE
				ENDIF
				
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT - g_ownerOfArmoryTruckPropertyIAmIn = ", NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn))
				
				IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(g_ownerOfArmoryTruckPropertyIAmIn,FALSE)
					GET_TRUCK_CUSTOM_SPAWN_POINTS(vSpawnPoint, iSpawnPoint)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT ",iSpawnPoint," -  vSpawnPoint: ", vSpawnPoint)				
					fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT ",iSpawnPoint," - fSpawnHeading: ", fSpawnHeading)
					IF NOT IS_VECTOR_ZERO(vSpawnPoint)					
						RETURN TRUE
					#IF IS_DEBUG_BUILD
					ELSE
						ASSERTLN("[SIMPLE_INTERIOR] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT - vSpawnPoint is zero")
					#ENDIF
					ENDIF
				ENDIF
				
				vSpawnPoint = g_SimpleInteriorData.vExitPosition
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT - Owner invalid - ",iSpawnPoint," -  vSpawnPoint: ", vSpawnPoint)	
				fSpawnHeading = g_SimpleInteriorData.fExitHeading - 180
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT - Owner invalid - ",iSpawnPoint," - fSpawnHeading: ", fSpawnHeading)
				
				RETURN TRUE							
			BREAK
		ENDSWITCH
	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_ARMORY_TRUCK_ASYNC_SPAWN_GRID_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_ARMORY_TRUCK_ASYNC_SPAWN_GRID_INSIDE_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC VECTOR GET_ARMORY_TRUCK_TELEPORT_IN_SVM_COORD(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC GET_ARMORY_TRUCK_EXIT_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
	UNUSED_PARAMETER(iCurrentExit)
	IF COUNT_OF(strOptions) != SIMPLE_INTERIOR_MENU_MAX_OPTIONS
		CASSERTLN(DEBUG_PROPERTY, "GET_CLUBHOUSE_EXIT_MENU_TITLE_AND_OPTIONS - options array has wrong size.")
		EXIT
	ENDIF
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			txtTitle = "ARMORY_TITLE"
		BREAK
		DEFAULT
			txtTitle = "ARMORY_TITLE"
		BREAK
	ENDSWITCH
	
	iOptionsCount = 2
	strOptions[0] = "GR_TRUCK_EX1"
	
	IF g_SimpleInteriorData.bTruckCabOwnerAccess
		strOptions[1] = "GR_TRUCK_EX3"
	ELSE
		strOptions[1] = "GR_TRUCK_EX2"
	ENDIF
ENDPROC

FUNC BOOL CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_ARMORY_TRUCK(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitId)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iExitId)
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_OWNER)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ARMORY_TRUCK_MAX_BLIPS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN MAX_ARMORY_TRUCK_BLIPS
ENDFUNC
   
FUNC BOOL SHOULD_ARMORY_TRUCK_BE_BLIPPED_THIS_FRAME(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() = eSimpleInteriorID
		RETURN FALSE
	ENDIF
	
	
	// ALWAYS RETURN FALSE FOR NOW UNTIL WE HAVE DECISION ON DESIGN !!!!!
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARMORY_TRUCK_BLIP_SHORT_RANGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	
	IF GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BLIP_SPRITE GET_ARMORY_TRUCK_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN RADAR_TRACE_WAREHOUSE
ENDFUNC

FUNC VECTOR GET_ARMORY_TRUCK_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1 
			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
				IF GET_ENTITY_MODEL(truckIndex) = TRAILERLARGE
					RETURN GET_ENTITY_COORDS(truckIndex)
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BLIP_PRIORITY GET_ARMORY_TRUCK_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIPPRIORITY_MED
ENDFUNC

PROC MAINTAIN_ARMORY_TRUCK_BLIP_EXTRA_FUNCTIONALITY(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(blipIndex)
	UNUSED_PARAMETER(iBlipType)
	UNUSED_PARAMETER(iBlipNameHash)
	UNUSED_PARAMETER(OverlayBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
ENDPROC

FUNC INT GET_ARMORY_TRUCK_BLIP_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIP_COLOUR_PINK
ENDFUNC

FUNC VECTOR GET_ARMORY_TRUCK_MAP_MIDPOINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
			RETURN <<848.7985, -3236.5745, -100.0893>>
		ELSE
			IF g_ownerOfArmoryTruckPropertyIAmIn != PLAYER_ID()
				RETURN GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
			ELSE
				IF DOES_ENTITY_EXIST(truckIndex)
				AND NOT IS_ENTITY_DEAD(truckIndex)
					RETURN GET_ENTITY_COORDS(truckIndex)
				ELSE
					RETURN GlobalplayerBD[NATIVE_TO_INT(g_ownerOfArmoryTruckPropertyIAmIn)].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
				ENDIF	
			ENDIF
		ENDIF	
	ELSE
		IF DOES_ENTITY_EXIST(truckIndex)
		AND NOT IS_ENTITY_DEAD(truckIndex)
			RETURN GET_ENTITY_COORDS(truckIndex)
		ELSE
			IF GET_OWNER_OF_PERSONAL_TRUCK(truckIndex) != INVALID_PLAYER_INDEX()
				RETURN GlobalplayerBD[NATIVE_TO_INT(GET_OWNER_OF_PERSONAL_TRUCK(truckIndex))].SimpleInteriorBD.vMobileInteriorExitSpawnPoint
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IS_VEHICLE_SUITABLE_FOR_ARMORY_TRUCK(MODEL_NAMES theModel)
	IF IS_SVM_VEHICLE_MODEL(theModel)
	OR IS_VEHICLE_A_CYCLE(theModel)
	OR theModel = RALLYTRUCK
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC PROCESS_CORONA_ENTRY_SHAPE_TEST(SIMPLE_INTERIOR_DETAILS &details)
	
	#IF IS_DEBUG_BUILD
		BOOL bInRange = FALSE
		VECTOR vCoronaPos
		
		GET_ARMORY_TRUCK_ENTRY_LOCATE(SIMPLE_INTERIOR_ARMORY_TRUCK_1, vCoronaPos)
		
		IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCoronaPos) < 144 //(12*12)
			bInRange = TRUE
		ENDIF
	#ENDIF
	
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_START_CORONA_SHAPETEST)
				
				// See if we can collide with building on back of truck 
				VECTOR vStartPointCamBack
				FLOAT  fGroundZ1, fGroundZ2
				
				vStartPointCamBack = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0,-7.9, 0>>)
				
				VECTOR vEndPointCamBack
				vEndPointCamBack =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0.0,-8.5, 0>>)
				
				CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(truckIndex, vStartPointCamBack, fGroundZ1)
				CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(truckIndex, vEndPointCamBack, fGroundZ2)
				
				VEHICLE_INDEX pedVeh
				IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(GET_OWNER_OF_TRAILER_VEH_INDEX()))
					pedVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(GET_OWNER_OF_TRAILER_VEH_INDEX()))
				ENDIF	
				IF DOES_ENTITY_EXIST(pedVeh)
				AND NOT IS_ENTITY_DEAD(pedVeh)
					details.entryAnim.sti_MobileIntEntryShapeTest = START_SHAPE_TEST_CAPSULE(<<vStartPointCamBack.x,vStartPointCamBack.y, fGroundZ1>>, <<vEndPointCamBack.x,vEndPointCamBack.y, fGroundZ2>>, 1
					, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT , pedVeh)
				ELSE
					details.entryAnim.sti_MobileIntEntryShapeTest = START_SHAPE_TEST_CAPSULE(<<vStartPointCamBack.x,vStartPointCamBack.y, fGroundZ1>>, <<vEndPointCamBack.x,vEndPointCamBack.y, fGroundZ2>>, 2
					, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE, truckIndex)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF g_bActivateArmoryTruckDebugShapeTest
						DRAW_DEBUG_CROSS(vStartPointCamBack, 2.0,0,0,255)
						DRAW_DEBUG_CROSS(vEndPointCamBack, 2.0,0,255,0)
					ENDIF	
				#ENDIF
				
				IF NATIVE_TO_INT(details.entryAnim.sti_MobileIntEntryShapeTest) != 0
					SET_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_START_CORONA_SHAPETEST)
					#IF IS_DEBUG_BUILD
						IF bInRange
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_CORONA_ENTRY_SHAPE_TEST - Shapetest started! From: ", vStartPointCamBack, ", to: ", vEndPointCamBack)
						ENDIF
					#ENDIF
				ENDIF
			
			ELSE
				INT iHitSomething
				VECTOR vHitPoint, vHitNormal
				ENTITY_INDEX hitEntity
				
				SHAPETEST_STATUS shapetestStatus = GET_SHAPE_TEST_RESULT(details.entryAnim.sti_MobileIntEntryShapeTest, iHitSomething, vHitPoint, vHitNormal, hitEntity)
				
				IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
					IF iHitSomething = 0
						#IF IS_DEBUG_BUILD
							IF bInRange
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_CORONA_ENTRY_SHAPE_TEST - Didn't hit anything!")
							ENDIF
						#ENDIF
						CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_CORONA_POS_BLOCKED)
						details.entryAnim.sti_MobileIntEntryShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
						CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_START_CORONA_SHAPETEST)
					ELSE
						details.entryAnim.sti_MobileIntEntryShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
						SET_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_CORONA_POS_BLOCKED)
						CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_START_CORONA_SHAPETEST)
						#IF IS_DEBUG_BUILD
							IF bInRange
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_CORONA_ENTRY_SHAPE_TEST - Damn, we hit something! Entity ID: ", NATIVE_TO_INT(hitEntity))
							ENDIF
						#ENDIF	
					ENDIF
				ELIF shapetestStatus = SHAPETEST_STATUS_NONEXISTENT
					CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_ANIM_ON_START_CORONA_SHAPETEST)
					details.entryAnim.sti_MobileIntEntryShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_CORONA_ENTRY_SHAPE_TEST - Shapetest status is nonexistent.")
					#ENDIF	
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST(SIMPLE_INTERIOR_DETAILS &details)
	
	IF DOES_ENTITY_EXIST(g_viGunRunTruckTailerIamIn)
	AND NOT IS_ENTITY_DEAD(g_viGunRunTruckTailerIamIn)
	AND !IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_TRUCK_EXIT_SPAWN_SHAPETEST)
				
				// See if we can collide with building on back of truck 
				VECTOR vStartPointCamBack
				FLOAT  fGroundZ1, fGroundZ2
				
				vStartPointCamBack = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(g_viGunRunTruckTailerIamIn), GET_ENTITY_HEADING(g_viGunRunTruckTailerIamIn), <<0,-8.1, 0>>)
				
				VECTOR vEndPointCamBack
				vEndPointCamBack =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(g_viGunRunTruckTailerIamIn), GET_ENTITY_HEADING(g_viGunRunTruckTailerIamIn), <<0.0,-20.0, 0>>)
				
				CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(g_viGunRunTruckTailerIamIn, vStartPointCamBack, fGroundZ1)
				CALCULATE_Z_OFFSET_FOR_TRUCK_BACK_SHAPE_TEST(g_viGunRunTruckTailerIamIn, vEndPointCamBack, fGroundZ2)
				

				details.entryAnim.sti_MobileIntExitSpawnShapeTest = START_SHAPE_TEST_CAPSULE(<<vStartPointCamBack.x,vStartPointCamBack.y, fGroundZ1>>, <<vEndPointCamBack.x,vEndPointCamBack.y, fGroundZ2>>, GET_MODEL_HEIGHT(GET_ENTITY_MODEL(g_viGunRunTruckTailerIamIn))
					, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_GLASS | SCRIPT_INCLUDE_OBJECT , g_viGunRunTruckTailerIamIn)
				
				#IF IS_DEBUG_BUILD
					IF g_bActivateArmoryTruckDebugShapeTest
						DRAW_DEBUG_CROSS(vStartPointCamBack, 2.0,0,0,255)
						DRAW_DEBUG_CROSS(vEndPointCamBack, 2.0,0,255,0)
					ENDIF	
				#ENDIF
				
				IF NATIVE_TO_INT(details.entryAnim.sti_MobileIntExitSpawnShapeTest) != 0
					SET_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_TRUCK_EXIT_SPAWN_SHAPETEST)
					#IF IS_DEBUG_BUILD
					
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST - Shapetest started! Owner of trailer is : ", GET_PLAYER_NAME(GET_OWNER_OF_PERSONAL_TRUCK(g_viGunRunTruckTailerIamIn)))
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST - Shapetest started! From: ", vStartPointCamBack, ", to: ", vEndPointCamBack)
					#ENDIF
				ENDIF
			
			ELSE
				INT iHitSomething
				VECTOR vHitPoint, vHitNormal
				ENTITY_INDEX hitEntity
				
				SHAPETEST_STATUS shapetestStatus = GET_SHAPE_TEST_RESULT(details.entryAnim.sti_MobileIntExitSpawnShapeTest, iHitSomething, vHitPoint, vHitNormal, hitEntity)
				
				IF shapetestStatus = SHAPETEST_STATUS_RESULTS_READY
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST - We got shapetest results!")
					#ENDIF
					IF iHitSomething = 0
						#IF IS_DEBUG_BUILD
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST - Didn't hit anything!")
						#ENDIF
						CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_EXIT_SPAWN_POS_BLOCKED)
						details.entryAnim.sti_MobileIntExitSpawnShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
						CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_TRUCK_EXIT_SPAWN_SHAPETEST)
					ELSE
						details.entryAnim.sti_MobileIntExitSpawnShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
						SET_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_EXIT_SPAWN_POS_BLOCKED)
						CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_TRUCK_EXIT_SPAWN_SHAPETEST)
						#IF IS_DEBUG_BUILD
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST - Damn, we hit something! Entity ID: ", NATIVE_TO_INT(hitEntity))
						#ENDIF	
					ENDIF
				ELIF shapetestStatus = SHAPETEST_STATUS_NONEXISTENT
					CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_TRUCK_EXIT_SPAWN_SHAPETEST)
					details.entryAnim.sti_MobileIntExitSpawnShapeTest = INT_TO_NATIVE(SHAPETEST_INDEX, 0)
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] PROCESS_EXIT_SPAWN_COORD_SHAPE_TEST - Shapetest status is nonexistent.")
					#ENDIF	
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER(SIMPLE_INTERIORS eSimpleInteriorID)
	
	IF IS_WARNING_MESSAGE_ACTIVE()
	OR SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(eSimpleInteriorID #IF IS_DEBUG_BUILD , TRUE #ENDIF )
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER TRUE")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF !IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			IF NOT IS_SKYSWOOP_AT_GROUND()
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER TRUE NOT IS_SKYSWOOP_AT_GROUND")
				RETURN TRUE
			ENDIF
			IF IS_SYSTEM_UI_BEING_DISPLAYED()
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER TRUE IS_SYSTEM_UI_BEING_DISPLAYED")
				RETURN TRUE
			ENDIF
			IF IS_COMMERCE_STORE_OPEN()
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER TRUE IS_COMMERCE_STORE_OPEN")
				RETURN TRUE
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				IF g_SimpleInteriorData.driverPlayer != INVALID_PLAYER_INDEX()
					IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(g_SimpleInteriorData.driverPlayer)
					AND NOT IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(g_SimpleInteriorData.driverPlayer)
					AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(g_SimpleInteriorData.driverPlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
						RETURN TRUE
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER driver is not entrying anymore TRUE")
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(SIMPLE_INTERIOR_DETAILS &details,BOOL bCleanup = FALSE)
	//PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT Original door state = ",details.entryAnim.vehDoorLocks, " vehicle = ", NATIVE_TO_INT(details.entryAnim.vehEntered))
	IF NOT bCleanup
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT: called with bCleanup: FALSE")
		#ENDIF
		IF NOT IS_INTERACTION_MENU_DISABLED()
			DISABLE_INTERACTION_MENU()
		ENDIF
		NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
		IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , TRUE)
		ENDIF	
		IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
			SET_LOCAL_PLAYER_NOT_ALLOW_EXIT_BUNKER_WITH_VEH(TRUE)
		ENDIF
		IF NOT IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
			SET_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT: BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE TRUE")
		ENDIF
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, TRUE)							
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, FALSE)
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_FRONTEND_THIS_FRAME()
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
		IF !IS_WARNING_MESSAGE_ACTIVE()
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		details.entryAnim.vehEntered = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , 1 , 1)
		IF NOT IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
			details.entryAnim.vehDoorLocks = GET_VEHICLE_DOOR_LOCK_STATUS(details.entryAnim.vehEntered )
			SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(details.entryAnim.vehEntered , TRUE)
			SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(details.entryAnim.vehEntered , TRUE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered )
				SET_VEHICLE_DOORS_LOCKED(details.entryAnim.vehEntered ,VEHICLELOCK_LOCKED)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT Locking car doors for entry ")
			ENDIF
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] -SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT Saving Original door state = ",details.entryAnim.vehDoorLocks, " vehicle = ", NATIVE_TO_INT(details.entryAnim.vehEntered))
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT knocked off vehicle NEVER")
			SET_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
			SET_VEHICLE_DOORS_LOCKED_IF_OPEN(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE set")
		ENDIF	
	ELSE
		IF IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
		OR (IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) AND GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID()) = SIMPLE_INTERIOR_ARMORY_TRUCK_1)
			#IF IS_DEBUG_BUILD
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT: called with bCleanup: TRUE")
			#ENDIF
			
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_BLOCK_MOC_BAIL_FIX)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("AM_MP_ARMORY_TRUCK")) < 1
				AND NOT IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<1097.5347, -3016.0115, -40.7658>>, <<1109.2977, -2983.6902, -34.1882>>)
					IF NOT IS_PLAYER_IN_BUNKER(PLAYER_ID()) 
						g_SimpleInteriorData.bForceTerminateInteriorScript = TRUE
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT: g_SimpleInteriorData.bForceTerminateInteriorScript = TRUE")
					ENDIF
				ENDIF
			ENDIF	
			
			IF DOES_ENTITY_EXIST(details.entryAnim.vehEntered)
			AND IS_VEHICLE_DRIVEABLE(details.entryAnim.vehEntered,TRUE)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(details.entryAnim.vehEntered, FALSE)
					IF details.entryAnim.vehDoorLocks != VEHICLELOCK_NONE
					AND details.entryAnim.vehDoorLocks != GET_VEHICLE_DOOR_LOCK_STATUS(details.entryAnim.vehEntered)
						SET_VEHICLE_DOORS_LOCKED(details.entryAnim.vehEntered,details.entryAnim.vehDoorLocks)
					ENDIF
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT: setting door states back to original after aborting exit data.vehDoorLock = ",details.entryAnim.vehDoorLocks)
					CLEAR_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT: getting control of entering vehicle")
				ENDIF
			ELSE
				CLEAR_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)

					NETWORK_REQUEST_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , FALSE)
						ENDIF	
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					ENDIF
				ENDIF	
			ELSE
				IF DOES_ENTITY_EXIST(details.entryAnim.vehEntered)
				AND NOT IS_ENTITY_DEAD(details.entryAnim.vehEntered)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					IF NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
						SET_ENTITY_INVINCIBLE(details.entryAnim.vehEntered , FALSE)
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
					ENDIF
				ENDIF
			ENDIF
			IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
				CLEAR_BIT(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
			ENDIF
			RESET_SIMPLE_INT_EXTERIOR_SCRIPT(SIMPLE_INTERIOR_ARMORY_TRUCK_1)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
			CLEAR_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)	
			DO_SCREEN_FADE_IN(500)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)	
			IF IS_SKYSWOOP_AT_GROUND()
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
			START_VEHICLE_ENTRY_CUT_FROM_BUNKER_TO_TRUCK(FALSE)
			SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)
			IF IS_INTERACTION_MENU_DISABLED()
				ENABLE_INTERACTION_MENU()
			ENDIF
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT setting knocked off vehicle DEFAULT")
		ENDIF
	ENDIF
ENDPROC

PROC KICK_REMOTE_PLAYER_OUT_ARMORY_TRUCK(SIMPLE_INTERIORS eSimpleInteriorID)
	SET_BIT(g_SimpleInteriorData.iAccessBS,BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AFTER_WARNING_SC)
	IF IS_SCREEN_FADED_OUT()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_PED)
		ELSE
			IF IS_SIMPLE_INTERIOR_INT_SCRIPT_RUNNING()
				SET_KILL_INTERIOR_SCRIPT_FOR_ENTERING_PASSENGER(TRUE)
			ENDIF	
			SIMPLE_INTERIORS eBunkerSimpleInteriorID = GET_OWNED_BUNKER_SIMPLE_INTERIOR(g_ownerOfArmoryTruckPropertyIAmIn)
			INT iSpawnPoint
			VECTOR vSpawnPoint
			FLOAT fSpawnHeading
			GET_BUNKER_OUTSIDE_SPAWN_POINT(eBunkerSimpleInteriorID,  iSpawnPoint,  vSpawnPoint,  fSpawnHeading,  FALSE)
			IF NET_WARP_TO_COORD(vSpawnPoint, fSpawnHeading)
				
				SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
				g_SimpleInteriorData.driverPlayer = NULL
				SET_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK(FALSE)
				DO_SCREEN_FADE_IN(500)
				RESET_SIMPLE_INT_EXTERIOR_SCRIPT(eSimpleInteriorID)
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
				SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
				SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
				SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)	
				g_SimpleInteriorData.bForceTerminateInteriorScript = FALSE
				IF NETWORK_IS_IN_MP_CUTSCENE()
					NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
				ENDIF
				CLEAR_BIT(g_SimpleInteriorData.iAccessBS,BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AFTER_WARNING_SC)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS passenger is in warning message moving from bunker to truck so kick them outside of bunker")	
			ENDIF
		ENDIF
	ELSE
		DO_SCREEN_FADE_OUT(500)
	ENDIF
ENDPROC

PROC MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)

	UNUSED_PARAMETER(eState)
	UNUSED_PARAMETER(bExtScriptMustRun)
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED()
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED(FALSE)
			ENDIF	
		ENDIF
		IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("GtaMloRoom001")
			AND IS_GAMEPLAY_CAM_RENDERING()
				IF DOES_ENTITY_EXIST(details.entryAnim.cloneTrailer)
					DELETE_VEHICLE(details.entryAnim.cloneTrailer)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] ARMORY_TRUCK delete clone trailer ")
				ENDIF
			ENDIF	
		ELSE
			IF DOES_ENTITY_EXIST(truckIndex)
			AND NOT IS_ENTITY_DEAD(truckIndex)
			AND IS_NET_PLAYER_OK(PLAYER_ID())
				PROCESS_CORONA_ENTRY_SHAPE_TEST(details)
			ENDIF	
		ENDIF
		
		IF IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
			IF (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID()))
			OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS,BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AFTER_WARNING_SC)
				IF (IS_WARNING_MESSAGE_ACTIVE()
				AND GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER)
				OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS,BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AFTER_WARNING_SC)
					IF !IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
					AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
						g_SimpleInteriorData.driverPlayer = NULL
						SET_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK(FALSE)
						DO_SCREEN_FADE_IN(500)
						RESET_SIMPLE_INT_EXTERIOR_SCRIPT(eSimpleInteriorID)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						SET_KILL_INTERIOR_SCRIPT_FOR_ENTERING_PASSENGER(TRUE)
						SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
						SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)	
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS passenger is in warning message")
					ELSE
						KICK_REMOTE_PLAYER_OUT_ARMORY_TRUCK(eSimpleInteriorID)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
		OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING) 
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, FALSE)
		ENDIF

		IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE) 
			IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
			ENDIF	
		ENDIF
		IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
			IF PLAYER_ID() = g_ownerOfArmoryTruckPropertyIAmIn		
				IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
					IF IS_PLAYER_TRIGGERED_EXIT_TO_BUNKER_FROM_TRUCK_WITH_VEH(PLAYER_ID())
					OR IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_PROPERTY(PLAYER_ID())
					OR HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK()
					OR HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED()
					OR g_bPlayerLeavingCurrentInteriorInVeh
						DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
						PRINTLN("FIX_FOR_3977184 disbale phone this frame")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
			AND IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
				PED_INDEX driverPed = GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				IF DOES_ENTITY_EXIST(driverPed)
					
					PLAYER_INDEX driverPlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(driverPed)
					IF IS_NET_PLAYER_OK(driverPlayer)
						IF IS_DRIVER_ENTERING_SIMPLE_INTERIOR(driverPlayer)
							IF GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(driverPlayer) = SIMPLE_INTERIOR_ARMORY_TRUCK_1
								IF SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(eSimpleInteriorID #IF IS_DEBUG_BUILD , TRUE #ENDIF )
								AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
									SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
									SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
									DO_SCREEN_FADE_IN(500)
									RESET_SIMPLE_INT_EXTERIOR_SCRIPT(eSimpleInteriorID)
									NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
									IF NETWORK_IS_IN_MP_CUTSCENE()
										NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
										PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS NETWORK_SET_IN_MP_CUTSCENE FALSE")
									ENDIF
									SET_KILL_INTERIOR_SCRIPT_FOR_ENTERING_PASSENGER(TRUE)
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS passenger SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN FALSE")
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF	
		ENDIF
		
		IF IS_PLAYER_TRIGGERED_EXIT_TO_BUNKER_FROM_TRUCK_WITH_VEH(PLAYER_ID())
			IF IS_PAUSE_MENU_ACTIVE()
				SET_FRONTEND_ACTIVE(FALSE)
			ELSE
				DISABLE_FRONTEND_THIS_FRAME()
			ENDIF	
		ENDIF
	ENDIF	
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		IF g_SimpleInteriorData.bSafeToDoInteriorWarp
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		ENDIF
	ENDIF
	
	IF SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(eSimpleInteriorID)
	AND NOT IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	AND PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
	AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
	OR NOT IS_SKYSWOOP_AT_GROUND()
		IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT")
		ENDIF	
	ENDIF
	
	IF ((IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_PLAYER_BD_FADE_FOR_ENTRY_TO_ARMORY_TRUCK)
	OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_ENTERED_TRUCK_WITH_DRIVER))
	AND SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER(eSimpleInteriorID)
	AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(GET_OWNER_OF_TRAILER_VEH_INDEX()))
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS,BS_SIMPLE_INTERIOR_GLOBAL_DATA_KICK_PLAYER_OUT_AFTER_WARNING_SC)
		IF !IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
		AND NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: TASK_LEAVE_ANY_VEHICLE")
			ENDIF
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("am_mp_smpl_interior_int")) > 0
				FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE()
				SET_KILL_INTERIOR_SCRIPT_FOR_ENTERING_PASSENGER(TRUE)
			ENDIF	
			SET_DRIVER_ENTERING_SIMPLE_INTERIOR(FALSE)
			IF NETWORK_IS_IN_MP_CUTSCENE()
				NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS NETWORK_SET_IN_MP_CUTSCENE FALSE")
			ENDIF
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
			SET_TRANS_DONT_CLEAR_ENTERING_FLAG(FALSE)
			SET_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK(FALSE)	
			RESET_BUNKER_EXT_SCRIPT(FALSE)
			DO_SCREEN_FADE_IN(500)
			RESET_SIMPLE_INT_EXTERIOR_SCRIPT(eSimpleInteriorID)
			SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
			NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: kicking player out of vehicle and returning player control and fade in")
		ELSE
			KICK_REMOTE_PLAYER_OUT_ARMORY_TRUCK(eSimpleInteriorID)
		ENDIF
	ENDIF	
	
	IF IS_PLAYER_INSIDE_ARMORY_TRUCK_ENTRY_AREA(truckIndex)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
		AND IS_SAFE_TO_USE_VEHICLE(details,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), truckIndex)
		AND !SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(eSimpleInteriorID)
			// skip
		ELSE
			IF NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
			AND PLAYER_ID() = GET_OWNER_OF_TRAILER_VEH_INDEX()
				SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
			ENDIF	
		ENDIF
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			IF PLAYER_ID() = GET_OWNER_OF_PERSONAL_TRUCK(truckIndex)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
			ENDIF
		ENDIF	
	ENDIF
	
	IF HAS_VEHICLE_CUTSCENE_STARTED_FROM_BUNKER_TO_TRUCK()
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
	
	
	IF IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
	OR IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID()) 
		DISABLE_FRONTEND_THIS_FRAME()
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
ENDPROC

PROC RESET_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
	SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(FALSE)
	
	CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CAMERA_PAN_STARTED)
	IF IS_BIT_SET(details.entryAnim.iBS, BS_SIMPLE_INTERIOR_SET_PED_VEH_INVINCIBLE)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , FALSE)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: SET_ENTITY_INVINCIBLE ped vehicle FALSE")
			ENDIF	
		ELSE
			IF DOES_ENTITY_EXIST(details.entryAnim.vehEntered)
			AND NOT IS_ENTITY_DEAD(details.entryAnim.vehEntered)
				SET_ENTITY_INVINCIBLE(details.entryAnim.vehEntered , FALSE)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: SET_ENTITY_INVINCIBLE vehEntered FALSE")
			ENDIF
		ENDIF	
	ENDIF	
	
	CLEAR_BIT(g_SimpleInteriorData.iBS ,BS_SIMPLE_INTERIOR_GLOBAL_DATA_ALL_EXIT_BUNKER_FROM_TRUCK_TRIGGERED)
	PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: clear BS_SIMPLE_INTERIOR_GLOBAL_DATA_ALL_EXIT_BUNKER_FROM_TRUCK_TRIGGERED")
	
	SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
	PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS: called")
ENDPROC

PROC MAINTAIN_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
			CLEAR_HELP()
			RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
		ENDIF	
	ENDIF
	SET_ENTER_TRUCK_FROM_BUNKER_WITH_VEH_TRIGGERED(FALSE)
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		BOOL BulletProofFlag, FlameProofFlag, ExplosionProofFlag, CollisionProofFlag, MeleeProofFlag, SteamProofFlag, DontResetDamageFlagsOnCleanupMissionState, SmokeProofFlag
		
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND GET_ENTITY_PROOFS(PLAYER_PED_ID(), BulletProofFlag, FlameProofFlag, ExplosionProofFlag, CollisionProofFlag, MeleeProofFlag, SteamProofFlag, DontResetDamageFlagsOnCleanupMissionState, SmokeProofFlag)
			IF NOT BulletProofFlag OR NOT FlameProofFlag OR NOT ExplosionProofFlag OR NOT CollisionProofFlag OR NOT MeleeProofFlag
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_DISABLE_WEAPON_BLADE_FORCES(TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eSimpleInteriorID)
	details.entryAnim.iExteriorCamShapeTestStage[0] = SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
	details.entryAnim.iExteriorCamShapeTestStage[1] = SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
	details.entryAnim.iExteriorCamShapeTestStage[2] = SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
	details.entryAnim.iExteriorCamShapeTestStage[3] = SIMPLE_INTERIOR_ENTRY_CAM_SHAPETEST_INIT
	g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[0]  = -1
	g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[1]  = -1
	g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[2]  = -1
	g_SimpleInteriorData.iExteriorCamShapeTestHitSomething[3]  = -1
	RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
	RESET_NET_TIMER(details.entryAnim.sShapeTestTimer)
	SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(FALSE)
	CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_CAMERA_PAN_STARTED)
	SET_PLAYER_IS_READY_AFTER_SPECIAL_CUT_SCENE(FALSE)
	IF NOT IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID())
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, FALSE)
		ENDIF	
	ENDIF	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayersDontDragMeOutOfCar, FALSE)
	ENDIF	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
	CLEAR_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER)
	CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_TOP_CAM)
	CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_RIGHT_CAM)
	CLEAR_BIT(g_SimpleInteriorData.iBS, BS_SIMPLE_INTERIOR_GLOBAL_DATA_ANIM_ON_ENTER_USE_LEFT_CAM)
	IF DOES_ENTITY_EXIST(details.entryAnim.cloneTrailer)
		DELETE_VEHICLE(details.entryAnim.cloneTrailer)	
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] ARMORY_TRUCK delete clone trailer ")
	ENDIF

	SET_TRUCK_PERSONAL_CAR_MOD_TO_LEAVE_PROPERTY(FALSE)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE FALSE")
	ENDIF
	IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTrailerEnteringWithDriver)
		MPGlobalsAmbience.vehTrailerEnteringWithDriver = NULL
	ENDIF
	details.entryAnim.iTruckCutSceneStage = 0
	SET_TRUCK_PERSONAL_CAR_MOD_LEAVE_TO_BUNKER_EXT_SELECTED(FALSE)
	CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_GLOBAL_DATA_MOBILE_INTERIOR_EXIT_SPAWN_POS_BLOCKED)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		NETWORK_SET_ENTITY_CAN_BLEND(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE) 
		IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
		AND (GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = DELUXO
		OR GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = OPPRESSOR2)
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
				SET_DISABLE_HOVER_MODE_FLIGHT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),FALSE)
			ENDIF
		ENDIF	
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS NETWORK_SET_ENTITY_CAN_BLEND - true")	
	ELSE
		IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
		AND NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
			NETWORK_SET_ENTITY_CAN_BLEND(GET_PLAYERS_LAST_VEHICLE(), TRUE) 
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS GET_PLAYERS_LAST_VEHICLE NETWORK_SET_ENTITY_CAN_BLEND - true")	
		ENDIF
	ENDIF
	SET_GOT_TRUCK_INVITE_INSTANCE(FALSE)
	SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER)
		PRINTLN("RESET_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_DATA_OWNER_TRADE_IN_BUNKER false ")
	ENDIF
	g_bTurretFadeOut = FALSE
	IF IS_PLAYER_INVINCIBLE_FOR_MOBILE_SMPL_INTERIOR_ENTRY()
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)	
		ENDIF	
		SET_PLAYER_INVINCIBLE_FOR_MOBILE_SMPL_INTERIOR_ENTRY(FALSE)
	ENDIF
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
			
			SET_DISABLE_WEAPON_BLADE_FORCES(FALSE)
		ENDIF
	ENDIF
	PRINTLN("[SIMPLE_INTERIOR][SMPL_H] RESET_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS ")
ENDPROC

//PROC ARMORY_TRUCK_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS()
//	VEHICLE_INDEX theVeh
//	PED_INDEX vehiclePed
//	PLAYER_INDEX vehiclePlayer
//	 
//	//INT i, iSeat
//	//BOOL bPlayerInVehicleDoingDropOff
//	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_WARPING_WITH_DRIVER_INTO_BUNKER)
//		IF IS_NET_PLAYER_OK(PLAYER_ID())
//			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
//				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//				IF IS_VEHICLE_OK(theVeh)
//					vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
//					IF NOT IS_PED_INJURED(vehiclePed)
//						IF IS_PED_A_PLAYER(vehiclePed)
//							vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
//							IF vehiclePlayer != PLAYER_ID()
//							AND IS_NET_PLAYER_OK(vehiclePlayer)
//							AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_OWNER_TRIGGERED_EXIT_TO_BUNKER_FROM_TRUCK_WITH_VEH)
//								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(vehiclePlayer)].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_DRIVER_WARPING_INTO_BUNKER)
//									IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
//									AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
//										TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
//										PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_BUNKER_ACCESS: TASK_LEAVE_ANY_VEHICLE")
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

FUNC BOOL DOES_ARMORY_TRUCK_BLOCK_WEAPONS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	BOOL bAllowWeaponLoadout
	
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
		IF g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID()
			IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(PLAYER_ID())
			AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
				// allow
				bAllowWeaponLoadout = TRUE
			ELSE
				bAllowWeaponLoadout = FALSE
			ENDIF
		ELSE
			IF IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(g_ownerOfArmoryTruckPropertyIAmIn)
				IF (DOES_PLAYER_OWN_ANY_GUN_LOCKER())	// does player own clubhouse gun locker
				AND (NOT NETWORK_IS_IN_MP_CUTSCENE())
				AND NOT IS_PLAYER_USING_OFFICE_SEATID_VALID()
					// allow
					bAllowWeaponLoadout = TRUE
				ELSE
					bAllowWeaponLoadout = FALSE
				ENDIF
			ELSE
				bAllowWeaponLoadout = FALSE
			ENDIF	
		ENDIF	
	ENDIF
	IF !bAllowWeaponLoadout
			
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)     
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
	
	ELSE 
		HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
	ENDIF
	
	HOLSTER_WEAPON_IN_SIMPLE_INTERIOR()
		
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)

	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_LEFT)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_RIGHT)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_UP)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_GUN_DOWN)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_JUMP)
	
	IF !g_bEnableJumpingOffHalfTrackInProperty
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK) // prevents players from being able to sneak ontop of tables and couches etc.
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MELEE_HOLD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MELEE_LEFT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_VEH_MELEE_RIGHT)
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PASSENGER_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO_TRACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO_TRACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ARMORY_TRUCK_RESTRICT_PLAYER_MOVEMENT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_ARMORY_TRUCK_CAR_GENS_BLOCK_AREA(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vMin)
	UNUSED_PARAMETER(vMax)
	bDoBlockCarGen = FALSE
ENDPROC

FUNC BOOL GET_ARMORY_TRUCK_OUTSIDE_MODEL_HIDE(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iIndex)
	UNUSED_PARAMETER(vPos)
	UNUSED_PARAMETER(fRadius)
	UNUSED_PARAMETER(fActivationRadius)
	UNUSED_PARAMETER(eModel)
	UNUSED_PARAMETER(bSurviveMapReload)
	RETURN FALSE
ENDFUNC

PROC GET_ARMORY_TRUCK_SCENARIO_PED_REMOVAL_SPHERE(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(vCoords)
	UNUSED_PARAMETER(fRadius)
	bDoRemoveScenarioPeds = FALSE
ENDPROC

FUNC INT GET_ARMORY_TRUCK_ENTRANCES_COUNT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 2
ENDFUNC

FUNC INT GET_ARMORY_TRUCK_INVITE_ENTRY_POINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN -1
ENDFUNC

PROC GET_ARMORY_TRUCK_ENTRANCE_DATA(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)
	SWITCH iEntranceID
		CASE 0
			GET_ARMORY_TRUCK_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition)
			
			data.vLocatePos1 = <<1, 1, 2>>
			
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_DYNAMIC)
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
		BREAK
		CASE 1
			GET_ARMORY_TRUCK_VEHICLE_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition, data.vLocatePos1, data.vLocatePos2, data.fLocateWidth, data.fEnterHeading)
		
			data.iType = 1
			
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_USES_ANGLED_AREA)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IN_CAR_ONLY)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_DYNAMIC)
			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_INSIDE_ARMORY_TRUCK_ANGLED_AREA()
	
	VEHICLE_INDEX truckIndex = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	IF DOES_ENTITY_EXIST(truckIndex)
	AND NOT IS_ENTITY_DEAD(truckIndex)
		IF GET_ENTITY_MODEL(truckIndex) = TRAILERLARGE

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID()
				, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0.0,6,-1.5>>)
				, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckIndex), GET_ENTITY_HEADING(truckIndex), <<0.0,-5.9,2.5>>)
				, GET_MODEL_WIDTH(TRAILERLARGE))
					RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_FACING_ARMORY_TRUCK_DOOR(VECTOR vLookAt)
	VECTOR vToTrail = NORMALISE_VECTOR((vLookAt - GET_ENTITY_COORDS(PLAYER_PED_ID())))
	FLOAT fDot = DOT_PRODUCT(GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID()), vToTrail)
	
	RETURN fDot >= 0
ENDFUNC


FUNC BOOL HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO()

	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX pedVeh 
			pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(pedVeh)
				IF GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh) != 0
					INT i ,iNumWaiting 
					REPEAT NUM_NETWORK_PLAYERS i
							IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i))
								IF INT_TO_PLAYERINDEX(i) != PLAYER_ID() 
									IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(INT_TO_PLAYERINDEX(i)),pedVeh)
										IF IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(INT_TO_PLAYERINDEX(i))
											iNumWaiting++ 
										ENDIF
									ENDIF
								ENDIF
							ENDIF	
					ENDREPEAT
					IF iNumWaiting = GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh)
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO - return True iNumWaiting: ", iNumWaiting, " Number of passengers : ", GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh) )
						RETURN TRUE
					ELSE
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO -  iNumWaiting: ", iNumWaiting, " Number of passengers : ", GET_VEHICLE_NUMBER_OF_PASSENGERS(pedVeh) )
					ENDIF
				ELSE	
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO GET_VEHICLE_NUMBER_OF_PASSENGERS = 0 ")
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] return false veh doesn't exist ")
			ENDIF	
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PASSANGER_ALLOWED_TO_ENTER_WITH_DRIVER()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_KICK_PASSANGERS_OUT_OF_VEHICLE_BEFORE_TRANS_TO_TRUCK)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_PASSANGER_ALLOWED_TO_ENTER_WITH_DRIVER false")
				RETURN FALSE	
			ENDIF
		ENDIF
	ENDIF
		
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK(SIMPLE_INTERIORS eSimpleInteriorID)
	VEHICLE_INDEX theVeh
	PED_INDEX vehiclePed
	PLAYER_INDEX vehiclePlayer
	//INT i, iSeat
	//BOOL bPlayerInVehicleDoingDropOff
	IF IS_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(PLAYER_ID()) 
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(theVeh)
					vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
					IF NOT IS_PED_INJURED(vehiclePed)
						IF IS_PED_A_PLAYER(vehiclePed)
							vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
							IF vehiclePlayer != PLAYER_ID()
							AND IS_NET_PLAYER_OK(vehiclePlayer)
								IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
								AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(vehiclePlayer)) = SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
									IF SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER(eSimpleInteriorID)
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
											NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
											PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK: TASK_LEAVE_ANY_VEHICLE")
										ENDIF
										IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_PLAYER_BD_FADE_FOR_ENTRY_TO_ARMORY_TRUCK)
										
											IF IS_SCREEN_FADING_OUT()
											OR IS_SCREEN_FADED_OUT()
												DO_SCREEN_FADE_IN(500)
											ENDIF
											NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
											SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
											SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
											PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK: kicking player out of vehicle and returning player control")
										ENDIF
									ELSE
										IF IS_PASSANGER_ALLOWED_TO_ENTER_WITH_DRIVER()
											
											IF IS_SCREEN_FADED_IN()
												NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
												DO_SCREEN_FADE_OUT(500)
												SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(TRUE)
											ENDIF	
												
											IF IS_SCREEN_FADED_OUT()
											
												//NETWORK_SET_ENTITY_CAN_BLEND(theVeh, FALSE) 
												
												IF !NETWORK_IS_IN_MP_CUTSCENE()
												AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
													SET_ENTITY_VISIBLE_IN_CUTSCENE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),TRUE)
													SET_ENTITY_VISIBLE_IN_CUTSCENE(GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())),TRUE)
													SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
													NETWORK_SET_IN_MP_CUTSCENE(TRUE, FALSE)
													PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK NETWORK_SET_IN_MP_CUTSCENE TRUE")
												ENDIF
												SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
												SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(TRUE)
												SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
												PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK: set on local player")
											ENDIF	
										ELSE
											IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
												IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), HALFTRACK)
													TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID() , 0 , ECF_WARP_PED)
												ELSE
													TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
												ENDIF
												IF IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
													IF NETWORK_IS_IN_MP_CUTSCENE()
														NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
														PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE NETWORK_SET_IN_MP_CUTSCENE FALSE")
													ENDIF
													SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
												ENDIF
												IF IS_BIT_SET(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_KICK_PASSANGERS_OUT_OF_VEHICLE_BEFORE_TRANS_TO_TRUCK)
													CLEAR_BIT(g_SimpleInteriorData.iSecondBS, BS2_SIMPLE_INTERIOR_KICK_PASSANGERS_OUT_OF_VEHICLE_BEFORE_TRANS_TO_TRUCK)
													PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK: BS2_SIMPLE_INTERIOR_KICK_PASSANGERS_OUT_OF_VEHICLE_BEFORE_TRANS_TO_TRUCK false")
												ENDIF
												SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF g_bAllowKickFromMOCVehEntryAsPassenger
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(theVeh)
					vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
					IF NOT IS_PED_INJURED(vehiclePed)
						IF IS_PED_A_PLAYER(vehiclePed)
							vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
							IF vehiclePlayer != PLAYER_ID()
							AND IS_NET_PLAYER_OK(vehiclePlayer)
								IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
								AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(vehiclePlayer)) = SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
									IF SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER(eSimpleInteriorID)
									OR NOT IS_PASSANGER_ALLOWED_TO_ENTER_WITH_DRIVER()
									OR NOT IS_SKYSWOOP_AT_GROUND()
									OR IS_SYSTEM_UI_BEING_DISPLAYED()
									OR IS_COMMERCE_STORE_OPEN()
										IF IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())
											IF NETWORK_IS_IN_MP_CUTSCENE()
												NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
												PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE NETWORK_SET_IN_MP_CUTSCENE FALSE")
											ENDIF
										ENDIF
										SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
										SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
										SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
											PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK: TASK_LEAVE_ANY_VEHICLE")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
									
	ENDIF
ENDPROC

//PROC TRIGGER_LOCAL_PLAYER_ARMORY_TRUCK_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID)
//
//	// We need to set this manually so DO_SIMPLE_INTERIOR_AUTOWARP will work, 
//	// otherwise it will return TRUE straight away as it checks if local player is in the interior, 
//	// it's a bit of a hack but a really small, harmless one.
//	g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID 
//			
//	SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, eSimpleInteriorID)
//	g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
//	g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE
//ENDPROC


FUNC BOOL CAN_VEHICLE_BE_SWAPPED_TO_PROPERTY_SLOT(MODEL_NAMES theVehModel, INT iPropertySlot)
	IF g_sMPTUNABLES.bdisable_transfer_vehicle_between_garage
		RETURN FALSE
	ENDIF
	
	RETURN IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,iPropertySlot)
ENDFUNC



FUNC BOOL CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN(SIMPLE_INTERIOR_DETAILS &details,SIMPLE_INTERIOR_ENTRANCE_DATA &data) 
	VEHICLE_INDEX vehID
	INT iDisplaySlot
	INt iSaveSlot = -1
	BOOL bPersonalVehicle, bAnotherPlayerPersonalVehicle,bPersonalVehicleCantBeSwapped
	
	FE_WARNING_FLAGS iInputBS = FE_WARNING_OKCANCEL
	vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
		IF DECOR_EXIST_ON(vehID, "Player_Vehicle") 
			IF DECOR_GET_INT(vehID, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
				bPersonalVehicle = TRUE
			ELSE
				bAnotherPlayerPersonalVehicle = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bPersonalVehicle 
	OR NOT bAnotherPlayerPersonalVehicle AND DOES_ENTITY_EXIST(vehID)
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
		//MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),iDisplaySlot)
		MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(CURRENT_SAVED_VEHICLE_SLOT(),iDisplaySlot)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN- current save = ",CURRENT_SAVED_VEHICLE_SLOT()," current displaySlot = ",iDisplaySlot)
		IF iDisplaySlot = DISPLAY_SLOT_START_ARMOURY_TRUCK
		AND bPersonalVehicle
			SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_DRIVE_CAR_INTO_TRUCK)
			CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
			CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
			NET_PRINT("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN player entering in personal vehicle belonging to truck") NET_NL()
			RETURN TRUE
		ELIF iSaveSlot < 0
		//OR iDisplaySlot = DISPLAY_SLOT_START_ARMOURY_TRUCK
			globalPropertyEntryData.tempStoredVehicle = vehID
			CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
			IF NOT bPersonalVehicle
				MP_SAVE_VEHICLE_STORE_CAR_DETAILS_STRUCT(vehID,globalPropertyEntryData.replacingVehicle,TRUE)
				globalPropertyEntryData.bCachedModInfo_Setup = FALSE
				IF GET_NUM_MOD_KITS(vehID) > 0
					SET_VEHICLE_MOD_KIT(vehID, 0)
				ENDIF
				IF GET_VEHICLE_MOD_KIT(vehID) >= 0
					globalPropertyEntryData.iCachedModInfo_EngineCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ENGINE)+1
					globalPropertyEntryData.iCachedModInfo_BrakesCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_BRAKES)+1
					globalPropertyEntryData.iCachedModInfo_ExhaustCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_EXHAUST)+1
					globalPropertyEntryData.iCachedModInfo_WheelsCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_WHEELS)+1
					globalPropertyEntryData.iCachedModInfo_HornCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_HORN)+1
					globalPropertyEntryData.iCachedModInfo_ArmourCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ARMOUR)+1
					globalPropertyEntryData.iCachedModInfo_SuspensionCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_SUSPENSION)+1
					globalPropertyEntryData.iCachedModInfo_Colours 			= GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(vehID)
					globalPropertyEntryData.eCachedModInfo_MKT 				= GET_VEHICLE_MOD_KIT_TYPE(vehID)
					globalPropertyEntryData.fCachedModInfo_PriceModifier 	= GET_VEHICLE_MOD_PRICE_MODIFIER(vehID)
					globalPropertyEntryData.bCachedModInfo_Setup 			= TRUE
				ENDIF
				SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
				//delete vehicle (INTERIOR SCRIPT)
			ENDIF
			//truck is empty just carry on
			SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_DRIVE_CAR_INTO_TRUCK)
			RETURN TRUE
		ELSE
			MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
			IF bPersonalVehicle 
			AND NOT CAN_VEHICLE_BE_SWAPPED_TO_PROPERTY_SLOT(g_MpSavedVehicles[iSaveSlot].VehicleSetupMP.VehicleSetup.eModel,GET_PROPERTY_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot))
				bPersonalVehicleCantBeSwapped = TRUE
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN: current vehicle can't be swapped out")
			ENDIF
			IF bPersonalVehicle
			AND iDisplaySlot = DISPLAY_SLOT_TEROBYTE_OPP2
				IF g_MpSavedVehicles[iSaveSlot].VehicleSetupMP.VehicleSetup.eModel != OPPRESSOR2
					bPersonalVehicleCantBeSwapped = TRUE
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN: current vehicle can't be swapped out- only oppressor2 in terobyte")
				ENDIF
			ENDIF
			
			//can vehicle in truck be swapped to property current PV came from
			IF bPersonalVehicle 
			AND bPersonalVehicleCantBeSwapped = FALSE
				SET_WARNING_MESSAGE_WITH_HEADER("MP_TRUCK_FULL","MP_TRUCK_FULL2",iInputBS)
			ELSE
				IF bPersonalVehicle 
				AND bPersonalVehicleCantBeSwapped
					iInputBS = FE_WARNING_CANCEL
					IF iDisplaySlot = DISPLAY_SLOT_TEROBYTE_OPP2
						SET_WARNING_MESSAGE_WITH_HEADER("MP_TRUCK_FULL","MP_TRUCK_FULLT",iInputBS)
					ELSE
						SET_WARNING_MESSAGE_WITH_HEADER("MP_TRUCK_FULL","MP_TRUCK_FULLC",iInputBS)
					ENDIF
				ELSE
					SET_WARNING_MESSAGE_WITH_HEADER("MP_TRUCK_FULL","MP_TRUCK_FULL1",iInputBS)
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(data.iSettingsBS,SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_PV_ENTRY_PRESSED_ACCEPT)
			AND NOT (bPersonalVehicle AND bPersonalVehicleCantBeSwapped)
				IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT) OR IS_BIT_SET(data.iSettingsBS,SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_PV_ENTRY_ACCEPTED_WARNING))
				//AND CAN_PLAYER_USE_MENU_INPUT()
				//AND NOT IS_ANYONE_TRYING_TO_ENTER_CURRENT_VEHICLE()
//					IF IS_BIT_SET(ilocalBS,LOCAL_BS_SKIP_ENTER_FADE)
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: Skipping fade as bit set")
//						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_FADE)
//					ELSE
//						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_WARPING_INTO_GARAGE_CS)
//						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iEnteringPropertyID = iCurrentPropertyID
//					ENDIF
					globalPropertyEntryData.tempStoredVehicle = vehID
					PLAY_SOUND_FRONTEND(-1, "OK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					MP_SAVE_VEHICLE_STORE_CAR_DETAILS_STRUCT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),globalPropertyEntryData.replacingVehicle,TRUE)
					globalPropertyEntryData.bCachedModInfo_Setup = FALSE
					IF GET_NUM_MOD_KITS(vehID) > 0
						SET_VEHICLE_MOD_KIT(vehID, 0)
					ENDIF
					IF GET_VEHICLE_MOD_KIT(vehID) >= 0
						globalPropertyEntryData.iCachedModInfo_EngineCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ENGINE)+1
						globalPropertyEntryData.iCachedModInfo_BrakesCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_BRAKES)+1
						globalPropertyEntryData.iCachedModInfo_ExhaustCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_EXHAUST)+1
						globalPropertyEntryData.iCachedModInfo_WheelsCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_WHEELS)+1
						globalPropertyEntryData.iCachedModInfo_HornCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_HORN)+1
						globalPropertyEntryData.iCachedModInfo_ArmourCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ARMOUR)+1
						globalPropertyEntryData.iCachedModInfo_SuspensionCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_SUSPENSION)+1
						globalPropertyEntryData.iCachedModInfo_Colours 			= GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(vehID)
						globalPropertyEntryData.eCachedModInfo_MKT 				= GET_VEHICLE_MOD_KIT_TYPE(vehID)
						globalPropertyEntryData.fCachedModInfo_PriceModifier 	= GET_VEHICLE_MOD_PRICE_MODIFIER(vehID)
						globalPropertyEntryData.bCachedModInfo_Setup 			= TRUE
					ENDIF
						
//					SET_REPLACING_VEHICLE_ON_ENTRY(TRUE)
//						IF globalPropertyEntryData.iReplaceSlot >= 0
//							CLEAR_MP_SAVED_VEHICLE_SLOT(globalPropertyEntryData.iReplaceSlot)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT; Destroying previous personal vehicle outside range of garage size.")
//						ENDIF
//						globalPropertyEntryData.iVehSlot = -1
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupOldPersonalVehicle = TRUE - C")
//						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
//						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
//					
////					IF CHECK_EVERYONE_IN_VEHICLE_IS_READy()
////						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
////						vehFadeOut = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
////						IF GET_PED_IN_VEHICLE_SEAT(vehFadeOut) = PLAYER_PED_ID()
////							SET_ENTITY_VISIBLE(vehFadeOut,FALSE)
////							SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
////							SET_VEHICLE_FIXED(vehFadeOut)
////					        SET_ENTITY_HEALTH(vehFadeOut, 1000) 
////					        SET_VEHICLE_ENGINE_HEALTH(vehFadeOut, 1000) 
////					        SET_VEHICLE_PETROL_TANK_HEALTH(vehFadeOut, 1000) 
////							SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
////							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
////							CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE()")
////						ENDIF
////						#IF FEATURE_HEIST_PLANNING
////						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
////						#ENDIF
//	//						ELSE
//	//							
//	////							SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
//	////							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_MADE_INVISIBLE_ON_EXTERIOR)
//	////							CDEBUG1LN(DEBUG_SAFEHOUSE, "NETWORK_FADE_OUT_ENTITY: called from HANDLE_WARP_INTO_GARAGE() - PED")
//	////							SET_BIT(iLocalBS2,LOCAL_BS2_bFadeOutWarpIntoGarage)
//	//						ENDIF
//						PLAY_SOUND_FRONTEND(-1, "OK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
//						MP_SAVE_VEHICLE_STORE_CAR_DETAILS_STRUCT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),globalPropertyEntryData.replacingVehicle,TRUE)
//						
//						// Kenneth R. - We also need to cache some info for the car app.
//						globalPropertyEntryData.bCachedModInfo_Setup = FALSE
//						VEHICLE_INDEX vehID
//						vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
//						IF GET_NUM_MOD_KITS(vehID) > 0
//							SET_VEHICLE_MOD_KIT(vehID, 0)
//						ENDIF
//						IF GET_VEHICLE_MOD_KIT(vehID) >= 0
//							globalPropertyEntryData.iCachedModInfo_EngineCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ENGINE)+1
//							globalPropertyEntryData.iCachedModInfo_BrakesCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_BRAKES)+1
//							globalPropertyEntryData.iCachedModInfo_ExhaustCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_EXHAUST)+1
//							globalPropertyEntryData.iCachedModInfo_WheelsCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_WHEELS)+1
//							globalPropertyEntryData.iCachedModInfo_HornCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_HORN)+1
//							globalPropertyEntryData.iCachedModInfo_ArmourCount 		= GET_NUM_VEHICLE_MODS(vehID, MOD_ARMOUR)+1
//							globalPropertyEntryData.iCachedModInfo_SuspensionCount 	= GET_NUM_VEHICLE_MODS(vehID, MOD_SUSPENSION)+1
//							globalPropertyEntryData.iCachedModInfo_Colours 			= GET_VEHICLE_COLOURS_WHICH_CAN_BE_SET(vehID)
//							globalPropertyEntryData.iCachedModInfo_DefaultHorn 		= GET_VEHICLE_DEFAULT_HORN_IGNORE_MODS(vehID)
//							globalPropertyEntryData.iCachedModInfo_HornMod0 		= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 0)
//							globalPropertyEntryData.iCachedModInfo_HornMod1 		= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 1)
//							globalPropertyEntryData.iCachedModInfo_HornMod2 		= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 2)
//							globalPropertyEntryData.iCachedModInfo_HornMod3 		= GET_VEHICLE_MOD_MODIFIER_VALUE(vehID, MOD_HORN, 3)
//							globalPropertyEntryData.eCachedModInfo_MKT 				= GET_VEHICLE_MOD_KIT_TYPE(vehID)
//							globalPropertyEntryData.fCachedModInfo_PriceModifier 	= GET_VEHICLE_MOD_PRICE_MODIFIER(vehID)
//							globalPropertyEntryData.bCachedModInfo_Setup 			= TRUE
//						ENDIF
//						
//						SET_REPLACING_VEHICLE_ON_ENTRY(TRUE)
//						IF globalPropertyEntryData.iReplaceSlot >= 0
//							CLEAR_MP_SAVED_VEHICLE_SLOT(globalPropertyEntryData.iReplaceSlot)
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT; Destroying previous personal vehicle outside range of garage size.")
//						ENDIF
//						globalPropertyEntryData.iVehSlot = -1
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "AM_MP_PROPERTY_EXT: bCleanupOldPersonalVehicle = TRUE - C")
//						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_REPLACING_CAR)
//						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
//					ENDIF
//					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE - TRUE - 2")
					SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_DRIVE_CAR_INTO_TRUCK)
					SET_BIT(data.iSettingsBS,SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_PV_ENTRY_ACCEPTED_WARNING)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN: player is overwritting vehicle in truck")
					IF bPersonalVehicle
					AND bPersonalVehicleCantBeSwapped = FALSE
						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
						CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN: swapping cars")
					ELSE
						SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ADDING_NEW_VEHICLE)
						CLEAR_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_CAR_REPLACEMENT)
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN: adding as new car")
						IF NOT bPersonalVehicle
						//AND MPGlobals.VehicleData.bDriveableInFreemode
							CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE,TRUE)
						ENDIF
						IF bPersonalVehicleCantBeSwapped
						OR NOT bPersonalVehicle
							SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_REPLACING_TRUCK_VEHICLE)
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN: adding as new car- overwritting vehicle in the trailer")
						ENDIF
					ENDIF
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					CLEAR_BIT(data.iSettingsBS,SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_PV_ENTRY_PRESSED_ACCEPT)
				ENDIF
			ENDIF
			IF NOT IS_BIT_SET(data.iSettingsBS,SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_PV_ENTRY_PRESSED_CANCEL)
				IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL) OR IS_SYSTEM_UI_BEING_DISPLAYED())
				OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				//AND CAN_PLAYER_USE_MENU_INPUT()
					//cancel out of entry
					PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					//IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
					CLEAR_HELP()
					RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
					//ENDIF	
					SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
					IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						SET_LOCAL_PLAYER_NOT_ALLOW_EXIT_BUNKER_WITH_VEH(FALSE)
					ENDIF
					NET_PRINT("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN NOT entering avoiding overwritting vehicle- killing context") NET_NL()
				ENDIF
			ELSE
				IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					CLEAR_BIT(data.iSettingsBS,SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_PV_ENTRY_PRESSED_CANCEL)
				ENDIF
			ENDIF
			//accept
				//if ambient
					//store details
					//delete vehicle
				//elif personal
					//store display slot
					//delete vehicle
				//endif
			//ELSE
				//abort entry
			//ENDIF
		ENDIF
		
		
		
		//IF bPersonalVehicle
		//your personal vehicle and truck vehicle storage is empty
			//set display slot when inside
		
		//truck vehicle storage is not empty
			//swap display slots when inside
		//ELSE
		//your personal vehicle and truck vehicle storage is empty
			//set display slot when inside
		
		//truck vehicle storage is not empty
			//swap display slots when inside
		//ENDIF
	ELIF  bAnotherPlayerPersonalVehicle
		//not allowed in another's personal vehicle
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
			CLEAR_HELP()
			RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
			IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
				SET_LOCAL_PLAYER_NOT_ALLOW_EXIT_BUNKER_WITH_VEH(FALSE)
			ENDIF
		ENDIF	

		NET_PRINT("[SIMPLE_INTERIOR][SMPL_H] CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN NOT entering another players personal vehicle") NET_NL()
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_FADED_OUT_FOR_CUT_SCENE()
	
	IF !SHOULD_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY()
		RETURN TRUE
	ELSE
		IF IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
		IF IS_SCREEN_FADED_OUT()
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE IS_LOCAL_PLAYER_FADED_OUT_FOR_CUT_SCENE returning false")
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
	PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE is running")
	
	bReturnSetExitCoords = TRUE
	
	IF HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK()
	OR HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED()
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE false HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK: ", HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK()
				, "HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED", HAS_EXIT_BUNKER_FROM_TRUCK_WITH_VEH_TRIGGERED())
		RETURN FALSE		
	ENDIF
	
	IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_APC_TUR")
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  CAN_PLAYER_US_SIMPLE_ENTRY_IN_CURRENT_VEHICLE_FOR_ARMORY_TRUCK HELP_APC_TUR is being displayed")
			RETURN FALSE
		ENDIF
		IF GET_OWNER_OF_TRAILER_VEH_INDEX() != PLAYER_ID()
			PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  CAN_PLAYER_US_SIMPLE_ENTRY_IN_CURRENT_VEHICLE_FOR_ARMORY_TRUCK FALSE not owner of the trailer!")
			RETURN FALSE
		ENDIF
	ENDIF	
	
	IF (g_sMPTunables.BGR_DISABLE_WEAPON_AND_VEHICLE_WORKSHOP AND IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID()))
	//OR (g_sMPTunables.BGR_DISABLE_PERSONAL_VEHICLE_STORAGE AND IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID()))
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE FALSE blocked by BGR_DISABLE_WEAPON_AND_VEHICLE_WORKSHOP")
		RETURN FALSE		
	ENDIF
	
	VEHICLE_INDEX truckVeh = GET_ARMORY_TRUCK_TRAILER_VEH_INDEX()
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
	OR IS_PAUSE_MENU_ACTIVE_EX()
	OR IS_PAUSE_MENU_ACTIVE()
		SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE IS_SKYSWOOP_AT_GROUND FALSE cleanup!")
	ENDIF
	
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF !IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE) 
			IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
			ENDIF
			SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
		ENDIF
	ENDIF
	
	
	IF IS_THIS_SIMPLE_INTERIOR_ENTRANCE_IN_CAR_ONLY(data)
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(),CODE_TASK_EXIT_VEHICLE)
		
			IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
				IF DOES_ENTITY_EXIST(truckVeh)
				AND NOT IS_ENTITY_DEAD(truckVeh)

					IF DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(PLAYER_ID())
						RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
					ENDIF
					
					IF (IS_PLAYER_INSIDE_ARMORY_TRUCK_ENTRY_AREA(truckVeh)
					AND IS_PLAYER_FACING_ARMORY_TRUCK_DOOR(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckVeh), GET_ENTITY_HEADING(truckVeh), <<0,-1,0>>))
					AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
					AND IS_SAFE_TO_USE_VEHICLE(details,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), truckVeh)
					AND !SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(data.eSimpleInteriorID))
					OR (HAS_VEHICLE_CUTSCENE_STARTED_FROM_BUNKER_TO_TRUCK() AND !SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(data.eSimpleInteriorID) AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID()))
					
						IF IS_PC_VERSION()
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VOLTIC2)
								OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), OPPRESSOR)
									IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
									OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("GR_ENTERTRUCK", "STRING")
									OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("GR_STORETRUCK", "STRING")
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), SCRAMJET)
									IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
									OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("GR_ENTERTRUCK", "STRING")
									OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("GR_STORETRUCK", "STRING")
										DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
									ENDIF
								ENDIF
							ENDIF	
						ENDIF

						DISABLE_VEHICLE_MINES(TRUE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
						IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
							
							//display help but do not allow
							IF IS_VEHICLE_ATTACHED_TO_TRAILER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							OR IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
										VEHICLE_INDEX vehAA
										BOOL bUseAA = FALSE
										IF GET_VEHICLE_TRAILER_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehAA)
											IF GET_ENTITY_MODEL(vehAA) = TRAILERSMALL2
												bUseAA = TRUE
												DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFA_TRAIL", FALSE)
											ENDIF
										ENDIF
										IF NOT bUseAA
											DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFE_TRAIL", FALSE)
										ENDIF
									ELIF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
										DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFS_TRAIL", FALSE)
									ENDIF
								ENDIF
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_US_SIMPLE_ENTRY_IN_CURRENT_VEHICLE_FOR_ARMORY_TRUCK FALSE vehicle is attached")
								RETURN FALSE
							ENDIF
							
							IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
										DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFE_AIR", FALSE)
									ELIF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
										DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFS_AIR", FALSE)
									ENDIF
								ENDIF
								
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_US_SIMPLE_ENTRY_IN_CURRENT_VEHICLE_FOR_ARMORY_TRUCK FALSE vehicle is aircraft")
								
								RETURN FALSE
							ENDIF
							
							IF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
							AND IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									DISPLAY_HELP_TEXT_THIS_FRAME("MP_TRUCK_PEG", FALSE)
								ENDIF
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_SAFE_TO_USE_VEHICLE False pegasus vehicle and has vehicle storage module")
								RETURN FALSE
							ENDIF
							
							IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
							AND NOT IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_MOD_SHOP(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFE_NOTV", FALSE)
								ENDIF
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE  but IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK FALSE")
								RETURN FALSE
							ENDIF
							
							IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
							//player has a car in carmod section already
							AND (IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())
							OR IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID()))
								IF NOT IS_PAUSE_MENU_ACTIVE()
								AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
								AND NOT IS_CUSTOM_MENU_ON_SCREEN()
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									
										DISPLAY_HELP_TEXT_THIS_FRAME("GR_ETFE_VEHIN", FALSE)
									ENDIF
								ENDIF
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE but a vehicle is inside Personal: ",IS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID())," Pegaus: ",IS_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(PLAYER_ID()) )
								RETURN FALSE	
							ENDIF
													
							STRING sText
	
							IF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
								sText = "GR_ENTERTRUCK"
							ELIF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
								sText = "GR_STORETRUCK"
							ENDIF
							
							//disable oppressor boost
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_TRANSFORM)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CAR_JUMP)
							
							IF details.entryAnim.iContextID = NEW_CONTEXT_INTENTION
							AND IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
								REGISTER_CONTEXT_INTENTION(details.entryAnim.iContextID, CP_MAXIMUM_PRIORITY, sText)
								IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
									SET_LOCAL_PLAYER_NOT_ALLOW_EXIT_BUNKER_WITH_VEH(FALSE)
								ENDIF
							ENDIF
							
							IF IS_PHONE_ONSCREEN()
								RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
							ENDIF
							
							IF (HAS_CONTEXT_BUTTON_TRIGGERED(details.entryAnim.iContextID) AND NOT IS_WARNING_MESSAGE_ACTIVE())
							OR IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
								
								SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details)
								
								IF NOT IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
								OR CHECK_AND_ASSIGN_VEHICLE_STORAGE_FOR_DRIVE_IN(details,data) 
									IF HAS_ALL_SHAPETESTS_PROCESSED(details.entryAnim, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									
										RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
										
										IF IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
											CLEAR_BIT(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
										ENDIF	
										ARMORY_TRUCK_SECTIONS_ENUM iSection
										IF DOES_ARMORY_TRUCK_SECTION_EXIST(AT_ST_CARMOD , iSection) 
											SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS,BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING_TRUCK_MOD)
										ELIF DOES_ARMORY_TRUCK_SECTION_EXIST(AT_ST_VEHICLE_STORAGE  , iSection)
											SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS,BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING_TRUCK_VEH_STORAGE)
										ENDIF
										
										IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
											
											SET_TRANS_DONT_CLEAR_ENTERING_FLAG(TRUE)
				
											MODIFY_VEHICLE_TOP_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),0)
											SET_VEHICLE_MAX_SPEED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),-1)
											STOP_BRINGING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										ENDIF	
										IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
											BROADCAST_OWNER_ENTER_SIMPL_INT_WITH_VEHICLE(TRUE)
											SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE)
											PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE BS_SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_IS_CAR_ONLY_SPECIAL_CUT_SCENE TRUE")
										ENDIF
										
										SET_DRIVER_ENTERING_SIMPLE_INTERIOR(TRUE  , SIMPLE_INTERIOR_ARMORY_TRUCK_1  )
										START_VEHICLE_ENTRY_CUT_FROM_BUNKER_TO_TRUCK(TRUE)
									ENDIF
								ENDIF
								
							ENDIF	
						ELSE
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE waiting for remote players to fade out")
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE)
							DISABLE_CELLPHONE_THIS_FRAME_ONLY()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
							SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
							SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
							DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
							DISABLE_FRONTEND_THIS_FRAME()
							SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
							BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) , 1 , 1)
							IF HAS_ALL_PASSENGERS_FADED_OUT_FOR_INTRO()	
							AND IS_LOCAL_PLAYER_FADED_OUT_FOR_CUT_SCENE()	
								IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
									SET_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK(TRUE)
									RESET_BUNKER_EXT_SCRIPT(TRUE)
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE entring from bunker to truck TRUE")
								ENDIF	
								
								
								SET_SIMPLE_INTERIOR_ENTRY_POINT_LOCAL_PLAYER_USED(1)
								GET_ARMORY_TRUCK_DETAILS(SIMPLE_INTERIOR_ARMORY_TRUCK_1, details, FALSE, FALSE)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
								IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
									SET_CONCEAL_CURRENT_PEGASUS_VEHICLE(PLAYER_ID(), ENUM_TO_INT(SIMPLE_INTERIOR_ARMORY_TRUCK_1), PV_CONCEAL_TYPE_ARMORY_TRUCK)
									
								ELSE
									IF IS_VEHICLE_A_PERSONAL_VEHICLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
										SET_CONCEAL_CURRENT_PERSONAL_VEHICLE(PLAYER_ID(), ENUM_TO_INT(SIMPLE_INTERIOR_ARMORY_TRUCK_1), PV_CONCEAL_TYPE_ARMORY_TRUCK)
									ENDIF
								ENDIF
								
								CUTSCENE_HELP_PREPARE_AREA_FOR_CUTSCENE(GET_PLAYER_COORDS(PLAYER_ID()),25)
								
								DISABLE_VEHICLE_MINES(FALSE)
								
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H]  CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE TRUE")
								RETURN TRUE

							ENDIF	
						ENDIF
					ELSE
						DISABLE_VEHICLE_MINES(FALSE)
						IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
							PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE IS_PLAYER_INSIDE_ARMORY_TRUCK_ENTRY_AREA - RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)")
							CLEAR_HELP()
							RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
						ENDIF
//						PRINTLN("CDM 1 = ",IS_PLAYER_INSIDE_ARMORY_TRUCK_ENTRY_AREA())
//						PRINTLN("CDM 2 = ",IS_PLAYER_FACING_ARMORY_TRUCK_DOOR(GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(truckVeh), GET_ENTITY_HEADING(truckVeh), <<0,-1,0>>)))
//						PRINTLN("CDM 3 = ",NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling))
//						PRINTLN("CDM 4 = ",IS_SAFE_TO_USE_VEHICLE(details,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
//						PRINTLN("CDM 5 = ",!SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(data.eSimpleInteriorID))
//						PRINTLN("CDM 5 = ",(HAS_VEHICLE_CUTSCENE_STARTED_FROM_BUNKER_TO_TRUCK() AND !SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN(data.eSimpleInteriorID)))
						SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
					ENDIF	
				ELSE
					IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
						IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
							CLEAR_HELP()
							RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
						ENDIF	
					ENDIF
					SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
				ENDIF	
			ELSE
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE - MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK .....")
				MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK(data.eSimpleInteriorID)
				IF IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(PLAYER_ID())	
					IF NOT HAS_NET_TIMER_STARTED(details.entryAnim.sShapeTestTimer)
						START_NET_TIMER(details.entryAnim.sShapeTestTimer)		// using this timer as safe timer as we will never use this timer for remote player 
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE - start safe timer")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(details.entryAnim.sShapeTestTimer, 10000)
							IF IS_SCREEN_FADED_OUT()
		
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE: TASK_LEAVE_ANY_VEHICLE")
								ENDIF
								IF NETWORK_IS_IN_MP_CUTSCENE()
									NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE NETWORK_SET_IN_MP_CUTSCENE FALSE")
								ENDIF
								DO_SCREEN_FADE_IN(500)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
								SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
								SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
								RESET_NET_TIMER(details.entryAnim.sShapeTestTimer)
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE: kicking player out of vehicle and returning player control")
							ENDIF	
						ENDIF
					ENDIF	
					IF IS_NET_PLAYER_OK(g_SimpleInteriorData.driverPlayer)
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE g_SimpleInteriorData.driverPlayer is OKAY")
					ELSE
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE g_SimpleInteriorData.driverPlayer is NOT OKAY!")
					ENDIF
					
					
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE eCurrentSimpleInterior: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(data.eSimpleInteriorID))

					
					IF !IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(g_SimpleInteriorData.driverPlayer) AND IS_PLAYER_IN_ARMORY_TRUCK(g_SimpleInteriorData.driverPlayer) 

						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
						DISABLE_CELLPHONE_THIS_FRAME_ONLY()
						SET_SIMPLE_INTERIOR_AUTOWARP_OVERRIDE(g_SimpleInteriorData.driverPlayer)
						SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_ARMORY_TRUCK_ACCESS_BS_NON_GANG_MEMBER)
						globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INT_TO_PLAYERINDEX(g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)
						SET_FADE_OUT_SCREEN_FOR_VEHICLE_ENTRY(TRUE)
						SET_DRIVER_ENTERING_SIMPLE_INTERIOR(TRUE  , SIMPLE_INTERIOR_ARMORY_TRUCK_1  )
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
						g_ownerOfArmoryTruckPropertyIAmIn = INT_TO_PLAYERINDEX(g_SimpleInteriorData.iOverrideAutoWarpToPlayersProperty)
						RESET_NET_TIMER(details.entryAnim.sShapeTestTimer)
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE - [passenger] TRUE")
					ENDIF
					IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(g_SimpleInteriorData.driverPlayer) OR IS_PLAYER_IN_ARMORY_TRUCK(g_SimpleInteriorData.driverPlayer) 
						IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
							SET_TRANS_DONT_CLEAR_ENTERING_FLAG(TRUE)
							SET_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK(TRUE)	
							RESET_BUNKER_EXT_SCRIPT(TRUE)
						ENDIF
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE IS_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER FALSE ")
					IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_PLAYER_BD_FADE_FOR_ENTRY_TO_ARMORY_TRUCK)
						IF GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(g_SimpleInteriorData.driverPlayer) != SIMPLE_INTERIOR_ARMORY_TRUCK_1
						AND !IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
							IF IS_SCREEN_FADED_OUT()
		
								IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
									TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE: TWO TASK_LEAVE_ANY_VEHICLE")
								ENDIF
								IF NETWORK_IS_IN_MP_CUTSCENE()
									NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE TWO NETWORK_SET_IN_MP_CUTSCENE FALSE")
								ENDIF
								DO_SCREEN_FADE_IN(500)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								SET_PASSENGER_STARTING_TO_GET_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
								SET_PASSENGER_ENTERING_WITH_DRIVER_TO_TRUCK(FALSE)
								SET_PASSENGER_READY_TO_ENTER_TRUCK_WITH_DRIVER(FALSE)
								CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBSTwo, BS_SIMPLE_INTERIOR_GLOBAL_BS_TWO_PLAYER_BD_FADE_FOR_ENTRY_TO_ARMORY_TRUCK)
								RESET_NET_TIMER(details.entryAnim.sShapeTestTimer)
								PRINTLN("[SIMPLE_INTERIOR][SMPL_H] CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE: TWO kicking player out of vehicle and returning player control")
								
							ENDIF
						ENDIF	
					
					ENDIF
				ENDIF	
			ENDIF
		ELSE
			IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
				CLEAR_HELP()
				RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
			ENDIF	
			SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details,TRUE)
		ENDIF	
	ELSE
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(details.entryAnim.iContextID)
			CLEAR_HELP()
			RELEASE_CONTEXT_INTENTION(details.entryAnim.iContextID)
		ENDIF	
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_ETFE_NOTV")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_ETFE_TRAIL")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_ETFS_TRAIL")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("GR_ETFA_TRAIL")
		CLEAR_HELP()
	ENDIF
	
	RETURN FALSE	
ENDFUNC

PROC MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ARMOUR_TRUCK_ACCESS(SIMPLE_INTERIOR_DETAILS &details)
	
	IF IS_BIT_SET(details.entryAnim.iBS , BS_SIMPLE_INTERIOR_ANIM_ON_ENTER_TRIGGERED_FOR_SCPECIAL_CUT_SCENE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ARMOUR_TRUCK_ACCESS: called ")
		IF DOES_ENTITY_EXIST(details.entryAnim.vehEntered)
		AND IS_VEHICLE_DRIVEABLE(details.entryAnim.vehEntered,TRUE)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
				SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(details.entryAnim.vehEntered, FALSE)
				IF details.entryAnim.vehDoorLocks != VEHICLELOCK_NONE
				AND details.entryAnim.vehDoorLocks != GET_VEHICLE_DOOR_LOCK_STATUS(details.entryAnim.vehEntered)
					SET_VEHICLE_DOORS_LOCKED(details.entryAnim.vehEntered,details.entryAnim.vehDoorLocks)
				ENDIF
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ARMOUR_TRUCK_ACCESS: setting door states back to original after aborting exit data.vehDoorLock = ",details.entryAnim.vehDoorLocks)
				SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details, TRUE)
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(details.entryAnim.vehEntered)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ARMOUR_TRUCK_ACCESS: getting control of entering vehicle")
			ENDIF
		ELSE
			SETUP_PLAYER_AND_VEHICLE_FOR_ENTRY_AT(details, TRUE)
		ENDIF
		NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ARMOUR_TRUCK_ACCESS setting knocked off vehicle DEFAULT")
	ELIF g_bAllowKickFromMOCVehEntryAsPassenger
		VEHICLE_INDEX theVeh
		PED_INDEX vehiclePed
		PLAYER_INDEX vehiclePlayer

		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND NOT IS_PLAYER_GETTING_IN_OR_OUT_OF_VEHICLE(PLAYER_ID())
				theVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF IS_VEHICLE_OK(theVeh)
					vehiclePed = GET_PED_IN_VEHICLE_SEAT(theVeh)
					IF NOT IS_PED_INJURED(vehiclePed)
						IF IS_PED_A_PLAYER(vehiclePed)
							vehiclePlayer = NETWORK_GET_PLAYER_INDEX_FROM_PED(vehiclePed)
							IF vehiclePlayer != PLAYER_ID()
							AND IS_NET_PLAYER_OK(vehiclePlayer)
								IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(vehiclePlayer)].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
								AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(vehiclePlayer)) = SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
									IF SHOULD_ARMORY_TRUCK_ENTRY_ABORT_FOR_PASSENGER(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(vehiclePlayer))
									OR NOT IS_PASSANGER_ALLOWED_TO_ENTER_WITH_DRIVER()
									OR NOT IS_SKYSWOOP_AT_GROUND()
									OR IS_SYSTEM_UI_BEING_DISPLAYED()
									OR IS_COMMERCE_STORE_OPEN()
										IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
											TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
											PRINTLN("[SIMPLE_INTERIOR][SMPL_H] MAINTAIN_WARP_WITH_DRIVER_INTO_TRUCK: TASK_LEAVE_ANY_VEHICLE")
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	

ENDPROC

FUNC BOOL IS_ANY_PLAYER_IN_VEHICLE_STILL_IN_TRANSITION()
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX pedVeh 
			pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(pedVeh)
				INT i  
				REPEAT NUM_NETWORK_PLAYERS i
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i))
						IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(INT_TO_PLAYERINDEX(i)),pedVeh)
							IF IS_TRANS_WITH_VEH_BETWEEN_TRUCK_BUNKER_DONE(INT_TO_PLAYERINDEX(i))
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF	
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_FLAGS_WHEN_LEAVING_TRUCK(VEHICLE_INDEX vehiID,BOOL bDisableStuff)
	//SET_VEH_RADIO_STATION(vehiID,radioStation)
	
	IF bDisableStuff
		SET_ENTITY_VISIBLE(vehiID,FALSE) //hidden for test
		FREEZE_ENTITY_POSITION(vehiID,TRUE)
		SET_ENTITY_COLLISION(vehiID,FALSE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] AM_MP_SIMPLE_INTERIOR_INT: SET_VEHICLE_FLAGS_WHEN_LEAVING_TRUCK- disabling everything")
	ELSE
		SET_ENTITY_COLLISION(vehiID,TRUE)
		SET_ENTITY_VISIBLE(vehiID,TRUE) //hidden for test
		FREEZE_ENTITY_POSITION(vehiID,FALSE)
		SET_VEHICLE_LIGHTS(vehiID,NO_VEHICLE_LIGHT_OVERRIDE)
		SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vehiID, FALSE)  
		SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vehiID,FALSE)
		SET_VEHICLE_IS_STOLEN(vehiID, FALSE) 
		SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(vehiID,FALSE)
		SET_VEHICLE_RESPECTS_LOCKS_WHEN_HAS_DRIVER(vehiID,FALSE)		
		SET_ENTITY_CAN_BE_DAMAGED(vehiID,TRUE)
		SET_CAN_USE_HYDRAULICS(vehiID,TRUE)
		MODIFY_VEHICLE_TOP_SPEED(vehiID,0)
		SET_VEHICLE_MAX_SPEED(vehiID,-1)
		STOP_BRINGING_VEHICLE_TO_HALT(vehiID)
		IF GET_VEHICLE_MOD(vehiID, MOD_ENGINE) != -1
			SET_VEHICLE_MOD(vehiID, MOD_ENGINE, GET_VEHICLE_MOD(vehiID, MOD_ENGINE))
		ENDIF
		IF GET_VEHICLE_MOD(vehiID, MOD_GEARBOX) != -1
			SET_VEHICLE_MOD(vehiID, MOD_GEARBOX, GET_VEHICLE_MOD(vehiID, MOD_GEARBOX))
		ENDIF	
		IF GET_VEHICLE_MOD(vehiID, MOD_BRAKES) != -1
			SET_VEHICLE_MOD(vehiID, MOD_BRAKES, GET_VEHICLE_MOD(vehiID, MOD_BRAKES))
		ENDIF
		TOGGLE_VEHICLE_MOD(vehiID , MOD_TOGGLE_TURBO,IS_TOGGLE_MOD_ON(vehiID, MOD_TOGGLE_TURBO))
		IF GET_ENTITY_MODEL(vehiID) = DELUXO
		OR GET_ENTITY_MODEL(vehiID) = OPPRESSOR2
			SET_DISABLE_HOVER_MODE_FLIGHT(vehiID,FALSE)
			SET_SPECIAL_FLIGHT_MODE_ALLOWED(vehiID,TRUE)
		ENDIF	
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] AM_MP_SIMPLE_INTERIOR_INT: SET_VEHICLE_FLAGS_WHEN_LEAVING_TRUCK- enabling everything")
	ENDIF
ENDPROC

BOOL bCheckForPVSlot
FUNC BOOL SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)
	UNUSED_PARAMETER(eSimpleInteriorID)

	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
		IF IS_CELLPHONE_CAMERA_IN_USE()
		OR  IS_BROWSER_OPEN()
			RETURN FALSE
		ENDIF
		IF IS_PLAYER_ENTERED_TRUCK_WITH_WEAPONISED_VEHICLE(PLAYER_ID())
			RETURN FALSE
		ENDIF
	ENDIF	
	
	IF g_bPlayerLeavingCurrentInteriorInVeh
		IF g_TransitionData.SwoopStage != SKYSWOOP_NONE
		OR IS_SKYCAM_PAST_FIRST_CUT()
			g_bPlayerLeavingCurrentInteriorInVeh = FALSE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK: kicking player out of vehicle 1")
				ENDIF
			ENDIF	
			RETURN FALSE
		ENDIF	
	ENDIF	
		
	IF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK: kicking player out of vehicle 2")
			ENDIF
		ENDIF	
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED() 
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_WARP_IF_DOOR_IS_BLOCKED|ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK: kicking player out of vehicle 3")
			ENDIF
		ENDIF	
		RETURN FALSE
	ENDIF
	
	IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()

		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iBS, BS_SIMPLE_INTERIOR_GLOBAL_PLAYER_BD_DRIVER_ENTERING)
			VEHICLE_INDEX vTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF interiorOwner = PLAYER_ID()
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(vTemp)
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK FALSE IS_VEHICLE_ON_ALL_WHEELS false ")
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_PLAYER_TELEPORT_ACTIVE()
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK FALSE IS_PLAYER_TELEPORT_ACTIVE true ")
					#ENDIF
					RETURN FALSE
				ENDIF	
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) = WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
				OR GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_VEHICLE) = WAITING_TO_START_TASK
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK FALSE script request to leave the vehicle ")
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK False player shuffling")
					RETURN FALSE
				ENDIF
				
				IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK FALSE Code request to leave the vehicle ")
					#ENDIF
					RETURN FALSE
				ENDIF
				
				IF IS_ANY_PLAYER_IN_VEHICLE_STILL_IN_TRANSITION()
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK FALSE some on in vehicle still doing transition ")
					#ENDIF
					RETURN FALSE
				ENDIF

				IF GET_IS_VEHICLE_ENGINE_RUNNING(vTemp)
				AND g_bPlayerLeavingCurrentInteriorInVeh = TRUE
				//AND g_iMissionEnteryType = ciMISSION_ENTERY_TYPE_INVALID
					IF GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) != 0
					OR GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_VEH_BRAKE) != 0
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					//OR IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_TO_BUNKER_EXT_SELECTED(PLAYER_ID())
						DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
						SET_PEGASUS_VEHICLE_EXIST_IN_TRUCK_CAR_MOD_SECTION(FALSE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SHUFFLE, FALSE)
						
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_NEVER)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(vTemp, TRUE) 
						SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(vTemp, TRUE)
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(vTemp, PLAYER_ID(), FALSE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						SET_VEHICLE_FLAGS_WHEN_LEAVING_TRUCK(vTemp,FALSE)
						IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
						AND IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
							IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(vTemp)
								FORCE_CONCEAL_MY_ACTIVE_PEGASUS_VEHICLE()
							ELSE
								//SET_PV_AWAITING_ASSIGN_TO_MAIN_SCRIPT(TRUE)
								IF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
									IF GET_SAVE_GAME_ARRAY_SLOT() != -1
										IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed != -1
										AND MPGlobals.VehicleData.bDriveableInFreemode 
											CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE,TRUE)	
										ENDIF	
									ENDIF	
									INT iSaveSlot = -1
									MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
									IF iSaveSlot >= 0
										//SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)	
									ENDIF
								ENDIF
								ASSIGN_TO_MAIN_SCRIPT_DELAYED()
								FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE()
							ENDIF
						ENDIF	
						#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK TRUE -1")
						#ENDIF
						
						IF NOT IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)	
							IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
							AND NOT IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(vTemp)
								//SET_PV_AWAITING_ASSIGN_TO_MAIN_SCRIPT(TRUE)
								IF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
									IF GET_SAVE_GAME_ARRAY_SLOT() != -1
										IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed != -1
										AND MPGlobals.VehicleData.bDriveableInFreemode 
											CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE,TRUE)	
										ENDIF	
									ENDIF	
									INT iSaveSlot = -1
									MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
									IF iSaveSlot >= 0
										//SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
										SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)	
									ENDIF
								ENDIF
								ASSIGN_TO_MAIN_SCRIPT_DELAYED()
								FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE()
							ENDIF
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
							RETURN TRUE
						ELIF IS_TRUCK_PERSONAL_CAR_MOD_LEAVE_TO_BUNKER_EXT_SELECTED(PLAYER_ID())
						AND IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(g_ownerOfArmoryTruckPropertyIAmIn)	
							SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
							RETURN TRUE
						ELSE
							IF !IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
								bCheckForPVSlot = TRUE
							ELSE
								SET_PERSONAL_CAR_MOD_TRUCK_LEAVE_ON_FOOT(FALSE)
								SET_LOCAL_PLAYER_TRIGGERED_EXIT_TO_BUNKER_FROM_TRUCK_WITH_VEH(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bCheckForPVSlot
					DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
					IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(vTemp)
						SET_CONCEAL_CURRENT_PEGASUS_VEHICLE(PLAYER_ID(),ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID())), PV_CONCEAL_TYPE_BUNKER)
						FORCE_CONCEAL_MY_ACTIVE_PEGASUS_VEHICLE()
					ELSE
						IF GET_SAVE_GAME_ARRAY_SLOT() != -1
							IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iLastSavedCarUsed != -1
							AND MPGlobals.VehicleData.bDriveableInFreemode 
								CLEANUP_MP_SAVED_VEHICLE(TRUE,FALSE,TRUE)	
							ENDIF	
						ENDIF	
						INT iSaveSlot = -1
						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
						SET_VEHICLE_FLAGS_WHEN_LEAVING_TRUCK(vTemp,FALSE)
						IF iSaveSlot >= 0
							SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)
							PRINTLN("[personal_vehicle] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK - SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK -, MPGlobals.VehicleData.bAssignToMainScript = TRUE")
							IF IS_OWNERS_ARMORY_TRUCK_INSIDE_BUNKER(PLAYER_ID())
								SET_PV_AWAITING_ASSIGN_TO_MAIN_SCRIPT(FALSE)
								SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)	
							ELSE	
								ASSIGN_TO_MAIN_SCRIPT_DELAYED()
							ENDIF
							SET_CONCEAL_CURRENT_PERSONAL_VEHICLE(PLAYER_ID(),ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID())), PV_CONCEAL_TYPE_BUNKER)
							FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE()
						ELSE
							PRINTLN("[personal_vehicle][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK - save invalid on exit??")
						ENDIF
					ENDIF
						
						SET_LOCAL_PLAYER_TRIGGERED_EXIT_TO_BUNKER_FROM_TRUCK_WITH_VEH(TRUE)
						SET_PERSONAL_CAR_MOD_TRUCK_LEAVE_ON_FOOT(FALSE)	
				ENDIF
			ELSE
				IF GET_PED_IN_VEHICLE_SEAT(vTemp, VS_DRIVER) = GET_PLAYER_PED(interiorOwner)
				AND bOwnerDrivingOut
					#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK TRUE ")
					#ENDIF
					SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED() 
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC SET_PASSANGERS_VISIBLE_IN_CUTSCENE()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		INT iVehSeat
		VEHICLE_SEAT vSeat
		PED_INDEX pedPassanger
		VEHICLE_INDEX playerVeh =  GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		FOR iVehSeat = VS_FRONT_RIGHT TO VS_EXTRA_RIGHT_3
			vSeat = INT_TO_ENUM(VEHICLE_SEAT, iVehSeat)
			pedPassanger = GET_PED_IN_VEHICLE_SEAT(playerVeh, vSeat)
			IF DOES_ENTITY_EXIST(pedPassanger)
			AND IS_PED_A_PLAYER(pedPassanger)
			AND NOT IS_TURRET_SEAT(playerVeh , vSeat)
				SET_ENTITY_VISIBLE_IN_CUTSCENE(pedPassanger, TRUE)
			ENDIF
		ENDFOR
	ENDIF	
ENDPROC 

PROC GET_VEHICLE_SPEED_FOR_CUT_SCENE(VEHICLE_INDEX vehIndex, FLOAT &fSpeed)
	IF DOES_ENTITY_EXIST(vehIndex)
		SWITCH GET_ENTITY_MODEL(vehIndex)
			CASE HALFTRACK
				fSpeed = 8
			BREAK
			CASE TECHNICAL2
			CASE TECHNICAL3
				fSpeed = 10
			BREAK
			DEFAULT
				IF NOT IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
					fSpeed = 12
				ELSE
					fSpeed  = 8
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF	
ENDPROC

FUNC BOOL IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details)
	
	IF !REQUEST_LOAD_MODEL(TRAILERLARGE)
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY REQUEST_LOAD_MODEL TRAILERLARGE false ")
		RETURN FALSE
	ENDIF
	
	IF !REQUEST_LOAD_MODEL(GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()))
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY REQUEST_LOAD_MODEL false ")
		RETURN FALSE
	ENDIF
	
	IF !IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
		#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY IS_PLAYER_IN_VEH_SEAT FALSE ")
		#ENDIF
		RETURN FALSE
	ENDIF
	
//	IF IS_WARNING_MESSAGE_ACTIVE()
//		#IF IS_DEBUG_BUILD
//		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY FALSE IS_WARNING_MESSAGE_ACTIVE TRUE ")
//		#ENDIF
//		RETURN FALSE
//	ENDIF
	
	VEHICLE_INDEX trailerIndex//, cabIndex
	
	trailerIndex = MPGlobalsAmbience.vehTruckVehicle[1]
	
	IF NOT IS_ENTITY_DEAD(trailerIndex)
	AND NOT IS_VECTOR_ZERO(details.entryAnim.vTrailerCoord)
		SET_FOCUS_POS_AND_VEL(details.entryAnim.vTrailerCoord,<<0,0,0>>)
	ENDIF
	
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(details.entryAnim.vTrailerCoord.x - 100.0, details.entryAnim.vTrailerCoord.y - 100.0, details.entryAnim.vTrailerCoord.x + 100.0, details.entryAnim.vTrailerCoord.y + 100.0)
	
	IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
	AND DOES_ENTITY_EXIST(trailerIndex)
	AND NOT IS_ENTITY_DEAD(trailerIndex)
	
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY entryAnim.iTruckCutSceneStage: ", details.entryAnim.iTruckCutSceneStage)
		VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF DOES_ENTITY_EXIST(playerVeh)
		AND NOT IS_ENTITY_DEAD(playerVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(trailerIndex)
				VEHICLE_SETUP_STRUCT_MP trailerDetails
				PED_INDEX driverPed = PLAYER_PED_ID()	
				CUTSCENE_HELP_PREPARE_AREA_FOR_CUTSCENE(details.entryAnim.vTrailerCoord, 25)
				IF DOES_ENTITY_EXIST(details.entryAnim.cloneTrailer)
				AND NOT IS_ENTITY_DEAD(details.entryAnim.cloneTrailer)
					SET_TRAILER_LEGS_LOWERED(details.entryAnim.cloneTrailer)
				ENDIF	
				SWITCH details.entryAnim.iTruckCutSceneStage
					CASE 0
						IF NOT DOES_ENTITY_EXIST(details.entryAnim.cloneTrailer)
							IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
		
								details.entryAnim.vTrailerCoord = << 842.616, -3239.21, -96.8731 >>
								details.entryAnim.fTrailerHeading = 62.260525
								details.entryAnim.vVectorArray[0] = <<1.47961, 0.00453185, 62.2534>>
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_SMPL_INTERIOR,"[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY entering from bunker to truck ")
								#ENDIF
							ELSE
								IF NOT IS_ENTITY_DEAD(trailerIndex)
									details.entryAnim.vTrailerCoord = GET_ENTITY_COORDS(trailerIndex)
									details.entryAnim.fTrailerHeading =  GET_ENTITY_HEADING(trailerIndex)
									details.entryAnim.vVectorArray[0] = GET_ENTITY_ROTATION(trailerIndex)
								ENDIF	
								#IF IS_DEBUG_BUILD
								CDEBUG1LN(DEBUG_SMPL_INTERIOR,"[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY entering from freemode to truck ")
								#ENDIF
							ENDIF
							
							details.entryAnim.cloneTrailer = CREATE_VEHICLE(TRAILERLARGE, <<details.entryAnim.vTrailerCoord.x, details.entryAnim.vTrailerCoord.y, details.entryAnim.vTrailerCoord.z - 70>>, details.entryAnim.fTrailerHeading, FALSE, FALSE)
							
							FREEZE_ENTITY_POSITION(details.entryAnim.cloneTrailer,TRUE)
							SET_ENTITY_COLLISION(details.entryAnim.cloneTrailer,FALSE)
							
							GET_VEHICLE_SETUP_MP(trailerIndex, trailerDetails)
							SET_VEHICLE_SETUP_MP(details.entryAnim.cloneTrailer, trailerDetails)
						ELSE
							details.entryAnim.iTruckCutSceneStage = 1
							CDEBUG1LN(DEBUG_SMPL_INTERIOR,"[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY entryAnim.iTruckCutSceneStage = 1")
						ENDIF					
						
					BREAK
					CASE 1
						IF DOES_ENTITY_EXIST(details.entryAnim.cloneTrailer)
							IF NOT NETWORK_IS_IN_MP_CUTSCENE()
								SET_ENTITY_VISIBLE_IN_CUTSCENE(playerVeh,TRUE)
								SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(TRUE, FALSE)
								
								NETWORK_SET_IN_MP_CUTSCENE(TRUE, FALSE)
								
								IF NOT IS_AUDIO_SCENE_ACTIVE("DLC_GR_MOC_Drive_Up_Ramp_Scene")
									START_AUDIO_SCENE("DLC_GR_MOC_Drive_Up_Ramp_Scene")
									PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY start audio scene DLC_GR_MOC_Drive_Up_Ramp_Scene")
								ENDIF
							ENDIF
							IF NETWORK_IS_IN_MP_CUTSCENE()
								IF NOT IS_ENTITY_DEAD(details.entryAnim.cloneTrailer)
									
									// Set trailer coords
									SET_ENTITY_COORDS_NO_OFFSET(details.entryAnim.cloneTrailer, details.entryAnim.vTrailerCoord)
									SET_ENTITY_HEADING(details.entryAnim.cloneTrailer, details.entryAnim.fTrailerHeading)
									SET_ENTITY_ROTATION(details.entryAnim.cloneTrailer, details.entryAnim.vVectorArray[0])
									FREEZE_ENTITY_POSITION(details.entryAnim.cloneTrailer,FALSE)
									SET_ENTITY_COLLISION(details.entryAnim.cloneTrailer,TRUE)
									//SET_VEHICLE_GRAVITY(entryAnim.cloneTrailer, FALSE)
									
									SET_ENTITY_INVINCIBLE(details.entryAnim.cloneTrailer, TRUE)
									PLAY_SOUND_FROM_ENTITY(-1, "Enter_Car_Ramp_Deploy", details.entryAnim.cloneTrailer, "DLC_GR_MOC_Enter_Exit_Sounds")
									SET_VEHICLE_DOOR_OPEN(details.entryAnim.cloneTrailer,SC_DOOR_BOOT)
									SET_TRAILER_LEGS_LOWERED(details.entryAnim.cloneTrailer)
									FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(details.entryAnim.cloneTrailer)
									
									GET_ARMORY_TRUCK_DETAILS(eSimpleInteriorID, details  , FALSE, FALSE)
									// Set Camera this frame to avoid pops
									details.entryAnim.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
						
									SET_CAM_PARAMS(details.entryAnim.camera, details.entryAnim.cameraPos, details.entryAnim.cameraRot, details.entryAnim.cameraFov)
									SET_CAM_FAR_CLIP(details.entryAnim.camera, 1000)
									SHAKE_CAM(details.entryAnim.camera, "HAND_SHAKE", details.entryAnim.fCamShake)
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
									
									// Set player coords
									VECTOR vPlayerCoord
									FLOAT fGroundZ
									vPlayerCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(details.entryAnim.vTrailerCoord, details.entryAnim.fTrailerHeading, <<0.0,-16.5,0.0>>)
									GET_GROUND_Z_FOR_3D_COORD(vPlayerCoord, fGroundZ)
									CLEAR_AREA(<<vPlayerCoord.x,vPlayerCoord.y, fGroundZ>>,5,FALSE)
									REMOVE_PARTICLE_FX_IN_RANGE(<<vPlayerCoord.x,vPlayerCoord.y, fGroundZ>>,5)
									SET_ENTITY_COORDS_NO_OFFSET(details.entryAnim.cloneTrailer, details.entryAnim.vTrailerCoord)
									
									IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
										SET_ENTITY_COORDS_NO_OFFSET(playerVeh ,vPlayerCoord )
									ELSE	
										SET_ENTITY_COORDS_NO_OFFSET(playerVeh ,<<vPlayerCoord.x,vPlayerCoord.y, fGroundZ>> )
									ENDIF	
									
									SET_ENTITY_HEADING(playerVeh, details.entryAnim.fTrailerHeading)
									SET_VEHICLE_ON_GROUND_PROPERLY(playerVeh, 0)
									
									SET_PASSANGERS_VISIBLE_IN_CUTSCENE()
									
									FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(playerVeh)
									
									
									
									details.entryAnim.iTruckCutSceneStage = 2
									CDEBUG1LN(DEBUG_SMPL_INTERIOR,"[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY entryAnim.iTruckCutSceneStage = 2")
								ENDIF	
							ENDIF
						ENDIF
					
					BREAK

					CASE 2
						IF NOT IS_ENTITY_DEAD(details.entryAnim.cloneTrailer)
						AND NETWORK_IS_IN_MP_CUTSCENE()

							FLOAT fSpeed

							MODIFY_VEHICLE_TOP_SPEED(playerVeh,0)
							SET_VEHICLE_MAX_SPEED(playerVeh,-1)
							STOP_BRINGING_VEHICLE_TO_HALT(playerVeh)
							GET_VEHICLE_SPEED_FOR_CUT_SCENE(playerVeh, fSpeed)
							
							PLAY_SOUND_FROM_ENTITY(-1, "Enter_Car_Ramp_Hits_Ground", details.entryAnim.cloneTrailer, "DLC_GR_MOC_Enter_Exit_Sounds")
							CUTSCENE_HELP_VEHICLE_MOVE_FORWARD(driverPed,playerVeh, 3000, fSpeed , TEMPACT_GOFORWARD_HARD)
							details.entryAnim.iTruckCutSceneStage = 0
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SMPL_INTERIOR,"[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY All assets ready start task ")
							#ENDIF
							SET_PLAYER_IS_READY_AFTER_SPECIAL_CUT_SCENE(FALSE)
							SET_EXT_CUT_SCENE_IS_READY_FOR_INT_WARP(FALSE)
							RETURN TRUE
							
						ELSE
							#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SMPL_INTERIOR,"[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY entryAnim.cloneTrailer doesn't exist or player not in MP cut scene something went wrong in stage 3")
							#ENDIF
						ENDIF	
					BREAK
				ENDSWITCH
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(trailerIndex)
				#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY NETWORK_REQUEST_CONTROL_OF_ENTITY(trailerIndex) ")
				#ENDIF
			ENDIF	
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY  playerVeh doesn't exist ")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER): ", IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER))
		#ENDIF
		#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR][SMPL_H] IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY DOES_ENTITY_EXIST(trailerIndex): ", DOES_ENTITY_EXIST(trailerIndex))
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_ARMORY_TRUCK_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(details)
ENDPROC		

FUNC SIMPLE_INTERIORS GET_OWNED_SMPL_INT_OF_TYPE_ARMORY_TRUCK(PLAYER_INDEX playerID, INT iPropertySlot, INT iFactoryType)
	UNUSED_PARAMETER(iFactoryType)
	UNUSED_PARAMETER(iPropertySlot)
	IF IS_PLAYER_GUNRUNNING_TRUCK_PURCHASED(playerID)		
		RETURN SIMPLE_INTERIOR_ARMORY_TRUCK_1
	ENDIF
	
	RETURN SIMPLE_INTERIOR_INVALID
ENDFUNC

PROC ARMORY_TRUCK_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(bSetOnGround)
	
	IF NOT HAS_EXIT_TO_BUNKER_FROM_TRUCK_TRIGGERED()
		IF IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
			IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(veh)
				SET_SIMPLE_INTERIOR_VEHICLE_FLAGS_WHEN_LEAVING(veh,FALSE)
				CLEAR_CONCEAL_CURRENT_PEGASUS_VEHICLE()
				FORCE_CONCEAL_MY_ACTIVE_PEGASUS_VEHICLE(FALSE)
			ELIF IS_VEHICLE_A_PERSONAL_VEHICLE(veh)
				IF IS_PLAYER_PURCHASED_ANY_VEHICLE_STORAGE_SECTION_FOR_TRUCK(PLAYER_ID())
					INT iSaveSlot = -1
					MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_START_ARMOURY_TRUCK,iSaveSlot)
					SET_SIMPLE_INTERIOR_VEHICLE_FLAGS_WHEN_LEAVING(veh,FALSE)
					IF iSaveSlot >= 0
						FORCE_CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE(FALSE)
						CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE()
						SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)
						PRINTLN("[personal_vehicle] ARMORY_TRUCK_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT - SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK -, MPGlobals.VehicleData.bAssignToMainScript = TRUE")
						SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
					ELSE
						PRINTLN("[personal_vehicle] ARMORY_TRUCK_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT - save invalid on exit??")
					ENDIF
				ELIF IS_PLAYER_PURCHASED_ANY_CARMOD_SECTION_FOR_TRUCK(PLAYER_ID())
					SET_SIMPLE_INTERIOR_VEHICLE_FLAGS_WHEN_LEAVING(veh,FALSE)
					CLEAR_CONCEAL_CURRENT_PERSONAL_VEHICLE()
					SET_PV_ASSIGN_TO_MAIN_SCRIPT(TRUE)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Below is the function that builds the interface.
// Because look up table needs to be build on every call as an optimisation
// the function retrieves a pointer to one function at a time so that one call = one lookup
// Functions are grouped thematically.

PROC BUILD_ARMORY_TRUCK_LOOK_UP_TABLE(SIMPLE_INTERIOR_INTERFACE &interface, SIMPLE_INTERIOR_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_ARMORY_TRUCK_USE_EXTERIOR_SCRIPT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION
			interface.getSimpleInteriorInteriorTypeAndPosition = &GET_ARMORY_TRUCK_TYPE_AND_POSITION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS
			interface.getSimpleInteriorDetails = &GET_ARMORY_TRUCK_DETAILS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS_BS
			interface.getSimpleInteriorDetailsPropertiesBS = &GET_ARMORY_TRUCK_PROPERTIES_BS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_NAME
			interface.simpleInteriorFP_ReturnS_ParamID = &GET_ARMORY_TRUCK_NAME
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
			interface.getSimpleInteriorInsideBoundingBox = &GET_ARMORY_TRUCK_INSIDE_BOUNDING_BOX
		BREAK
		CASE E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
			interface.getOwnedSimpleInteriorOfType = &GET_OWNED_SMPL_INT_OF_TYPE_ARMORY_TRUCK
		BREAK
		CASE E_SIMPLE_INTERIOR_LOAD_SECONDARY_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_ARMORY_TRUCK_LOAD_SECONDARY_INTERIOR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_GAME_TEXT
			interface.simpleInteriorFP_ReturnS_ParamID_gameTextID = &GET_ARMORY_TRUCK_GAME_TEXT
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_SHOW_PIM_INVITE_OPTION
			interface.simpleInteriorFP_ReturnB_ParamID_tl63_AndBoolx2 = &SHOULD_ARMORY_TRUCK_SHOW_PIM_INVITE_OPTION
		BREAK
		//════════════════════════════════════════════════════════════════════
		/*
		CASE E_GET_SIMPLE_INTERIOR_ENTRY_LOCATE
			interface.getSimpleInteriorEntryLocate = &GET_ARMORY_TRUCK_ENTRY_LOCATE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRY_LOCATE_COLOUR
			interface.getSimpleInteriorEntryLocateColour = &GET_ARMORY_TRUCK_ENTRY_LOCATE_COLOUR
		BREAK
		*/
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCES_COUNT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_ARMORY_TRUCK_ENTRANCES_COUNT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_DATA
			interface.getSimpleInteriorEntranceData = &GET_ARMORY_TRUCK_ENTRANCE_DATA
		BREAK			
		CASE E_GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_ARMORY_TRUCK_INVITE_ENTRY_POINT
		BREAK	
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS
			interface.getSimpleInteriorEntranceSceneDetails = &GET_ARMORY_TRUCK_ENTRANCE_SCENE_DETAILS
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_LOCATE_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_ARMORY_TRUCK_LOCATE_BE_HIDDEN
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_CORONA_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_ARMORY_TRUCK_CORONA_BE_HIDDEN
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR_IN_VEHICLE
			interface.simpleInteriorFP_ReturnB_ParamPIDAndModel = &CAN_PLAYER_ENTER_ARMORY_TRUCK_IN_VEHICLE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPID_ID_AndInt = &CAN_PLAYER_ENTER_ARMORY_TRUCK
		BREAK
		CASE E_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE
			interface.canPlayerUseEntryInCurrentVeh = &CAN_PLAYER_USE_ARMORY_TRUCK_ENTRY_IN_CURRENT_VEHICLE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_ENTRY
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_ARMORY_TRUCK_REASON_FOR_BLOCKED_ENTRY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_EXIT
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_ARMORY_TRUCK_REASON_FOR_BLOCKED_EXIT
		BREAK
		CASE E_CAN_PLAYER_INVITE_OTHERS_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_INVITE_OTHERS_TO_ARMORY_TRUCK
		BREAK
		CASE E_CAN_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_BE_INVITED_TO_ARMORY_TRUCK
		BREAK
		CASE E_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER
			interface.intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer = &CAN_LOCAL_PLAYER_BE_INVITED_TO_ARMORY_TRUCK_BY_PLAYER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_ARMORY_TRUCK_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDAndEMenu = &PROCESS_ARMORY_TRUCK_ENTRANCE_MENU
		BREAK
		CASE E_SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_ARMORY_TRUCK
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_KICK_OUT_REASON
			interface.getSimpleInteriorKickOutReason = &GET_ARMORY_TRUCK_KICK_OUT_REASON
		BREAK
		CASE E_SET_SIMPLE_INTERIOR_ACCESS_BS
			interface.setSimpleInteriorAccessBS = &SET_ARMORY_TRUCK_ACCESS_BS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_SPAWN_POINT
			interface.getSimpleInteriorSpawnPoint = &GET_ARMORY_TRUCK_SPAWN_POINT
		BREAK
		CASE E_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS
			interface.getSimpleInteriorCustomInteriorWarpParams = &GET_ARMORY_TRUCK_CUSTOM_INTERIOR_WARP_PARAMS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT
			interface.getSimpleInteriorOutsideSpawnPoint = &GET_ARMORY_TRUCK_OUTSIDE_SPAWN_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_ARMORY_TRUCK_ASYNC_SPAWN_GRID_LOCATION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_INSIDE_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_ARMORY_TRUCK_ASYNC_SPAWN_GRID_INSIDE_LOCATION
		BREAK
		CASE E_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS
			interface.maintainLeaveentryVehIfNoAccess = &MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ARMOUR_TRUCK_ACCESS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_TELE_IN_SVM_COORD
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_ARMORY_TRUCK_TELEPORT_IN_SVM_COORD
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS
			interface.getSimpleInteriorExitMenuTitleAndOptions = &GET_ARMORY_TRUCK_EXIT_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_ARMORY_TRUCK
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_MAX_BLIPS
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_ARMORY_TRUCK_MAX_BLIPS
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_BE_BLIPPED_THIS_FRAME
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_ARMORY_TRUCK_BE_BLIPPED_THIS_FRAME
		BREAK
		CASE E_IS_SIMPLE_INTERIOR_BLIP_SHORT_RANGE
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &IS_ARMORY_TRUCK_BLIP_SHORT_RANGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_SPRITE
			interface.simpleInteriorFP_ReturnEBS_ParamIDAndInt = &GET_ARMORY_TRUCK_BLIP_SPRITE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COORDS
			interface.simpleInteriorFP_ReturnV_ParamIDAndInt = &GET_ARMORY_TRUCK_BLIP_COORDS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY
			interface.maintainSimpleInteriorBlipExtraFunctionality = &MAINTAIN_ARMORY_TRUCK_BLIP_EXTRA_FUNCTIONALITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_PRIORITY
			interface.getSimpleInteriorBlipPriority	= &GET_ARMORY_TRUCK_BLIP_PRIORITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COLOUR
			interface.simpleInteriorFP_ReturnI_ParamIDAndInt = &GET_ARMORY_TRUCK_BLIP_COLOUR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_MAP_MIDPOINT
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_ARMORY_TRUCK_MAP_MIDPOINT
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.maintainSimpleInteriorOutsideGameplayModifiers = &MAINTAIN_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_ARMORY_TRUCK_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &MAINTAIN_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_ARMORY_TRUCK_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_BLOCK_WEAPONS
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_ARMORY_TRUCK_BLOCK_WEAPONS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_RESTRICT_PLAYER_MOVEMENT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_ARMORY_TRUCK_RESTRICT_PLAYER_MOVEMENT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA
			interface.getSimpleInteriorCarGensBlockArea = &GET_ARMORY_TRUCK_CAR_GENS_BLOCK_AREA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE
			interface.getSimpleInteriorOutsideModelHide = &GET_ARMORY_TRUCK_OUTSIDE_MODEL_HIDE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE
			interface.getSimpleInteriorScenarioPedRemovalSphere = &GET_ARMORY_TRUCK_SCENARIO_PED_REMOVAL_SPHERE
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_TRIGGER_EXIT_IN_CAR
			interface.simpleInteriorFP_ReturnB_ParamID_PID_AndB = &SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FOR_ARMORY_TRUCK
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_IS_SPECIAL_CUT_SCENE_ASSETS_READY
			interface.isSpecialCutSceneAssetsReady = &IS_ARMORY_TRUCK_SPECIAL_CUT_SCENE_ASSETS_READY
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
			interface.DoCustomActionForStateWarpToSpawnPoint = &ARMORY_TRUCK_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
		BREAK
	ENDSWITCH
ENDPROC
