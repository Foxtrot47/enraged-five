USING "globals.sch"
USING "net_vehicle_setup.sch"

#IF FEATURE_FIXER

/// ------------------------------------------------------------------------------------

// Neil F.

// Description:  Header file that maintains Franklin and Tanisha's vehicles outside his house in the hills.

// Notes: 
// - UPDATE_FRANKLINS_CARS_SERVER & UPDATE_FRANKLINS_CARS_CLIENT are both called from freemode.sc
// - The vehicles are NOT networked, this is to avoid using up precious Network Ids
// - Because they are not networked synced, they must be frozen in place and also invincible.
// - I've allowed the windows to be smashed (smashing things is fun!) we can live without that being synced.
// - The server function maintains which 'layout' of the cars i.e. where they are parked, to make it look like Franklin and Tanisha
//	 are constanly using the vehicles. 
//			- there are 6 layouts and 2 vehicles, the vehicles can be in either slot, effectively giving us 12 layouts in total.
// - The server updates the layout every 10 mins, only if no players are around that area.
// - There are some globals in MP_globals_common_tu.sch to allow us to hook in via bgscript if required.
//      - g_bFranklinCarsActive will deactivate the system.
// - The local player will only create the cars locally if they are not on a mission etc. see IsSafeToRunFranklinCars()
//
/// ------------------------------------------------------------------------------------


// const ints for now, make global to allow for bg fix
CONST_INT NUM_FRANKLIN_CAR_LAYOUTS  6

CONST_FLOAT NUM_FRANKLIN_CAR_TRIGGER_X  15.6692
CONST_FLOAT NUM_FRANKLIN_CAR_TRIGGER_Y  548.4118
CONST_FLOAT NUM_FRANKLIN_CAR_TRIGGER_Z  175.2091

CONST_INT FRANKLIN_CARS_STATE_INTIALISING 				0
CONST_INT FRANKLIN_CARS_STATE_CREATING 					1
CONST_INT FRANKLIN_CARS_STATE_WAIT_FOR_COLLISION 		2
CONST_INT FRANKLIN_CARS_STATE_WAIT_FOR_PHYSICS 			3
CONST_INT FRANKLIN_CARS_STATE_IS_ACTIVE		 			4

STRUCT FRANKLIN_CAR_DATA

	INT iServerUpdateGameTime
	INT iClientUpdateGameTime
	
	INT iRequestLayout = -1
	INT iState = FRANKLIN_CARS_STATE_INTIALISING
	VEHICLE_INDEX CarId[2]
	MODEL_NAMES ModelName[2]


ENDSTRUCT

#IF IS_DEBUG_BUILD
PROC CREATE_WIDGET_FOR_FRANKLINS_CARS(FRANKLIN_CAR_DATA &sData)
	START_WIDGET_GROUP("Franklins Cars")
		START_WIDGET_GROUP("server bd")
			ADD_WIDGET_INT_SLIDER("GlobalServerBD_BlockC.iFranklinsParkedCarLayout", GlobalServerBD_BlockC.iFranklinsParkedCarLayout, -1, 5, 1)
			ADD_WIDGET_INT_SLIDER("GlobalServerBD_BlockC.iFranklinsParkedCarModelOrder", GlobalServerBD_BlockC.iFranklinsParkedCarModelOrder, 0, 1, 1)
			ADD_WIDGET_INT_SLIDER("GlobalServerBD_BlockC.iFranklinsParkedCarNextUpdate", GlobalServerBD_BlockC.iFranklinsParkedCarNextUpdate, -1, HIGHEST_INT, 1)		
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_INT_SLIDER("iServerUpdateGameTime", sData.iServerUpdateGameTime, -1, HIGHEST_INT, 1)		
		ADD_WIDGET_INT_SLIDER("iClientUpdateGameTime", sData.iClientUpdateGameTime, -1, HIGHEST_INT, 1)		
		ADD_WIDGET_INT_SLIDER("iRequestLayout", sData.iRequestLayout, -1, HIGHEST_INT, 1)	
		ADD_WIDGET_INT_SLIDER("iState", sData.iState, -1, HIGHEST_INT, 1)
		
		
		START_WIDGET_GROUP("globals")
			ADD_WIDGET_BOOL("g_bFranklinCarsActive", g_bFranklinCarsActive)
			ADD_WIDGET_INT_SLIDER("g_iFranklinCarsServerChangeSetupInterval", g_iFranklinCarsServerChangeSetupInterval, -1, HIGHEST_INT, 1)	
			ADD_WIDGET_INT_SLIDER("g_iFranklinCarsServerUpdateInterval", g_iFranklinCarsServerUpdateInterval, -1, HIGHEST_INT, 1)	
			ADD_WIDGET_FLOAT_SLIDER("g_fFranklinCarsActivationRadius", g_fFranklinCarsActivationRadius, -1.0, 9999999.9, 0.01)	
			ADD_WIDGET_FLOAT_SLIDER("g_fFranklinCarsActivationRadiusSquared", g_fFranklinCarsActivationRadiusSquared, -1.0, 9999999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fFranklinCarsDeActivationDistSquared", g_fFranklinCarsDeActivationDistSquared, -1.0, 9999999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fFranklinCarsIsClearRadiusCheck", g_fFranklinCarsIsClearRadiusCheck, -1.0, 9999999.9, 0.01)
			ADD_WIDGET_BOOL("g_IsSafeToRunFranklinCars_Override", g_IsSafeToRunFranklinCars_Override)
			ADD_WIDGET_BOOL("g_IsSafeToRunFranklinCars_Override_Value", g_IsSafeToRunFranklinCars_Override_Value)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF


PROC GetFranklinCarVehicleSetup(INT iCar, VEHICLE_SETUP_STRUCT_MP &sData)
	
	// empty the struct
	VEHICLE_SETUP_STRUCT_MP sEmpty
	COPY_SCRIPT_STRUCT(sData, sEmpty, SIZE_OF(sEmpty)) 

	SWITCH iCar
		// franklins car
		CASE 0
			sData.VehicleSetup.eLockState = VEHICLELOCK_CANNOT_ENTER
			sData.VehicleSetup.eModel = IGNUS
			sData.VehicleSetup.tlPlateText = "FRANKL1N"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 53
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255			
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)			
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 16
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 9
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 16
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 10
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1	
		BREAK
		// tanisha car
		CASE 1
			sData.VehicleSetup.eLockState = VEHICLELOCK_CANNOT_ENTER
			sData.VehicleSetup.eModel = ASTRON
			sData.VehicleSetup.tlPlateText = "TAN1SHA"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 7
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 7
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 4
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 4
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
		BREAK
	ENDSWITCH

ENDPROC


PROC GetFranklinCarSetupForSlot(INT iSlot, VEHICLE_SETUP_STRUCT_MP &sData)

	// the iFranklinsParkedCarModelOrder determines which car (franklins or tanisha's will be in slot one)
	
	SWITCH GlobalServerBD_BlockC.iFranklinsParkedCarModelOrder
	
		// franklins car in slot 0
		CASE 0
		DEFAULT
			IF (iSlot = 0)
				GetFranklinCarVehicleSetup(0, sData)	
			ELSE
				GetFranklinCarVehicleSetup(1, sData)
			ENDIF
		BREAK
		
		
		// tanisha's car in slot 0
		CASE 1
			IF (iSlot = 0)
				GetFranklinCarVehicleSetup(1, sData)	
			ELSE
				GetFranklinCarVehicleSetup(0, sData)
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

FUNC MODEL_NAMES GetFranklinModelNameForSlot(INT iSlot)

	VEHICLE_SETUP_STRUCT_MP sData
	GetFranklinCarSetupForSlot(iSlot, sData)
	
	RETURN sData.VehicleSetup.eModel

ENDFUNC


PROC GetFranklinCarLayoutDetails(INT iLayout, INT iSlot, VECTOR &vPosition, FLOAT &fHeading, VEHICLE_SETUP_STRUCT_MP &sData)

	GetFranklinCarSetupForSlot(iSlot, sData)

	SWITCH iLayout
	
		// layout 0 and 1 is a single car, in the middle of the driveway 
		
		// facing forwards (towards garage)
		CASE 0
			SWITCH iSlot
				CASE 0
					vPosition = <<12.7311, 547.3156, 175.0073>>
					fHeading = 262.7820		
				BREAK
				CASE 1
					// the 2nd slot should be empty
					sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
				BREAK
			ENDSWITCH
		BREAK
		
		// facing backwards
		CASE 1
			SWITCH iSlot
				CASE 0
					vPosition = <<12.7311, 547.3156, 175.0073>>
					fHeading = 82.7
				BREAK
				CASE 1
					// the 2nd slot should be empty
					sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
				BREAK
			ENDSWITCH
		BREAK
		
		// layout 2 - 5 is both cars, all combinations of facing forwards or back.
		
		// both cars facing forwards (towards garage)
		CASE 2
			SWITCH iSlot
				CASE 0
					vPosition = <<15.0336, 549.0245, 175.2618>>
					fHeading = 270.2375
				BREAK
				CASE 1
					vPosition = <<12.9157, 545.5204, 174.9342>>
					fHeading =  267.7842
				BREAK
			ENDSWITCH
		BREAK
		
		// first car facing forwards
		CASE 3
			SWITCH iSlot
				CASE 0
					vPosition = <<15.0336, 549.0245, 175.2618>>
					fHeading = 270.2375
				BREAK
				CASE 1
					vPosition = <<12.9157, 545.5204, 174.9342>>
					fHeading =  87.7842
				BREAK
			ENDSWITCH		
		BREAK
		
		// both cars facing backwards
		CASE 4
			SWITCH iSlot
				CASE 0
					vPosition = <<15.0336, 549.0245, 175.2618>>
					fHeading = 90.2375
				BREAK
				CASE 1
					vPosition = <<12.9157, 545.5204, 174.9342>>
					fHeading =  87.7842
				BREAK
			ENDSWITCH		
		BREAK		
	
		// 2nd car facing forwards
		CASE 5
			SWITCH iSlot
				CASE 0
					vPosition = <<15.0336, 549.0245, 175.2618>>
					fHeading = 90.2375
				BREAK
				CASE 1
					vPosition = <<12.9157, 545.5204, 174.9342>>
					fHeading =  267.7842
				BREAK
			ENDSWITCH			
		BREAK		
	
	ENDSWITCH

ENDPROC



PROC DeleteFranklinCars(FRANKLIN_CAR_DATA &Data)
	INT i
	REPEAT 2 i
		IF DOES_ENTITY_EXIST(Data.CarId[i])
			DELETE_VEHICLE(Data.CarId[i])
			PRINTLN("[FRANKLINS_CARS] - DeleteFranklinCars - deleting car ", i)
		ENDIF
	ENDREPEAT
ENDPROC

PROC UnloadFranklinCarAssets(FRANKLIN_CAR_DATA &Data)
	INT i
	
	REPEAT 2 i
		IF (Data.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
			SET_MODEL_AS_NO_LONGER_NEEDED(Data.ModelName[i])
		ENDIF			
	ENDREPEAT

ENDPROC

PROC CleanupFranklinCars(FRANKLIN_CAR_DATA &Data)
	DeleteFranklinCars(Data)
	UnloadFranklinCarAssets(Data)
	Data.iState = 0
	Data.iRequestLayout = -1
ENDPROC

FUNC BOOL HasFranklinCarsAssetsLoaded(FRANKLIN_CAR_DATA &Data)
	INT i
	
	// request all models
	REPEAT 2 i
		IF (Data.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
			REQUEST_MODEL(Data.ModelName[i])	
		ENDIF			
	ENDREPEAT
	
	// check if all models have loaded
	REPEAT 2 i
		IF (Data.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
			IF NOT HAS_MODEL_LOADED(Data.ModelName[i])
				#IF IS_DEBUG_BUILD
				PRINTLN("[FRANKLINS_CARS] - HasFranklinCarsAssetsLoaded - waiting on model ", GET_MODEL_NAME_FOR_DEBUG(Data.ModelName[i]))
				#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HasFranklinCarsCollisionStreamedIn(FRANKLIN_CAR_DATA &Data)
	
	INT i
	REPEAT 2 i
		IF (Data.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
			IF DOES_ENTITY_EXIST(Data.CarId[i])
				IF IS_ENTITY_WAITING_FOR_WORLD_COLLISION(Data.CarId[i])
					PRINTLN("[FRANKLINS_CARS] - HasFranklinCarsCollisionStreamedIn - waiting on collision for car ", i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

FUNC BOOL AreFranklinCarsSettled(FRANKLIN_CAR_DATA &Data)
	
	INT i
	REPEAT 2 i
		IF (Data.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
			IF DOES_ENTITY_EXIST(Data.CarId[i])
				IF (VMAG(GET_ENTITY_VELOCITY(Data.CarId[i])) > 0.001)
					PRINTLN("[FRANKLINS_CARS] - AreFranklinCarsSettled - waiting on physics to settle for car ", i)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE

ENDFUNC

FUNC BOOL CreateFranklinCars(FRANKLIN_CAR_DATA &sData)

	VECTOR vPosition
	FLOAT fHeading
	VEHICLE_SETUP_STRUCT_MP sVehData
	BOOL bReturn = TRUE

	// do a rough check that no vehicles are currently parked there.
	IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(<<NUM_FRANKLIN_CAR_TRIGGER_X, NUM_FRANKLIN_CAR_TRIGGER_Y, NUM_FRANKLIN_CAR_TRIGGER_Z>>, g_fFranklinCarsIsClearRadiusCheck, DEFAULT, DEFAULT, DEFAULT, FALSE, FALSE, FALSE, 0.0)

		INT i
		REPEAT 2 i
			IF (sData.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
				
				IF NOT DOES_ENTITY_EXIST(sData.CarId[i])
			
					GetFranklinCarLayoutDetails(sData.iRequestLayout, i, vPosition, fHeading, sVehData)					
					sData.CarId[i] = CREATE_VEHICLE(sData.ModelName[i], vPosition, fHeading, FALSE, FALSE) 			
					SET_VEHICLE_SETUP_MP(sData.CarId[i], sVehData)
					
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sData.CarId[i], TRUE)
					SET_ENTITY_INVINCIBLE(sData.CarId[i], TRUE)					
									
					PRINTLN("[FRANKLINS_CARS] - CreateFranklinCars - created vehicle ", i )
				
				ENDIF
			
			ENDIF
		ENDREPEAT
		
		// release the models
		REPEAT 2 i
			IF (sData.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.ModelName[i])
			ENDIF
		ENDREPEAT
	
	ELSE
		PRINTLN("[FRANKLINS_CARS] - CreateFranklinCars - point not ok for creation." )
		bReturn = FALSE
	ENDIF

	RETURN bReturn
	
ENDFUNC

PROC FreezeFranklinCars(FRANKLIN_CAR_DATA &sData)

	INT i
	REPEAT 2 i
		IF (sData.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT)			
			IF DOES_ENTITY_EXIST(sData.CarId[i])
				FREEZE_ENTITY_POSITION(sData.CarId[i], TRUE)
				PRINTLN("[FRANKLINS_CARS] - FreezeFranklinCars - frozen vehicle ", i )
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC InitialiseFranklinCarsClientData(FRANKLIN_CAR_DATA &sData)
	
	sData.iRequestLayout = GlobalServerBD_BlockC.iFranklinsParkedCarLayout
	PRINTLN("[FRANKLINS_CARS] - InitialiseFranklinCarsClientData - setting iRequestLayout = to ", sData.iRequestLayout)
	
	VECTOR vPosition
	FLOAT fHeading
	VEHICLE_SETUP_STRUCT_MP sVehData
	
	INT i
	REPEAT 2 i
		GetFranklinCarLayoutDetails(sData.iRequestLayout, i, vPosition, fHeading, sVehData)
		
		sData.ModelName[i] = sVehData.VehicleSetup.eModel
		#IF IS_DEBUG_BUILD
		IF sData.ModelName[i] != DUMMY_MODEL_FOR_SCRIPT
			PRINTLN("[FRANKLINS_CARS] - InitialiseFranklinCarsClientData - setting sData.ModelName[", i, "] to ", GET_MODEL_NAME_FOR_DEBUG(sData.ModelName[i]))
		ELSE
			PRINTLN("[FRANKLINS_CARS] - InitialiseFranklinCarsClientData - setting sData.ModelName[", i, "] to DUMMY_MODEL_FOR_SCRIPT")
		ENDIF
		#ENDIF
	ENDREPEAT
	

ENDPROC

FUNC BOOL IsSafeToRunFranklinCars(BOOL bActivationCheck)

	// I feel this would be the function we might want to override in a bgscript.
	IF (g_IsSafeToRunFranklinCars_Override)
		RETURN g_IsSafeToRunFranklinCars_Override_Value
	ENDIF

	// Are these all the gamestates we need to clean up?
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
	//OR IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
	OR IS_TRANSITION_ACTIVE()
	OR IS_PLAYER_IN_CORONA()
	OR NETWORK_IS_ACTIVITY_SESSION()
		//PRINTLN("IsSafeToRunFranklinCars = FALSE, gamestate.")
		RETURN FALSE
	ENDIF
	
	// the local player gone too far away
	IF (bActivationCheck)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<NUM_FRANKLIN_CAR_TRIGGER_X, NUM_FRANKLIN_CAR_TRIGGER_Y, NUM_FRANKLIN_CAR_TRIGGER_Z>>) > g_fFranklinCarsActivationRadiusSquared
			//PRINTLN("IsSafeToRunFranklinCars = FALSE, player too far away (activation).")
			RETURN FALSE
		ENDIF	
	ELSE
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<NUM_FRANKLIN_CAR_TRIGGER_X, NUM_FRANKLIN_CAR_TRIGGER_Y, NUM_FRANKLIN_CAR_TRIGGER_Z>>) > g_fFranklinCarsDeActivationDistSquared
			//PRINTLN("IsSafeToRunFranklinCars = FALSE, player too far away (deactivation).")
			RETURN FALSE
		ENDIF	
	ENDIF
		
	RETURN TRUE
ENDFUNC

PROC UPDATE_FRANKLINS_CARS_CLIENT(FRANKLIN_CAR_DATA &sData)

	IF NOT (g_bFranklinCarsActive)
		EXIT
	ENDIF

	// reasons to cleanup. 
	IF (sData.iState > FRANKLIN_CARS_STATE_INTIALISING)
	
		IF NOT IsSafeToRunFranklinCars(FALSE)
			PRINTLN("UPDATE_FRANKLINS_CARS_CLIENT - cleaning up - IsSafeToRunFranklinCars() = FALSE")
			CleanupFranklinCars(sData)
			EXIT
		ENDIF
	
		// has the server changed the layout
		IF (sData.iRequestLayout != GlobalServerBD_BlockC.iFranklinsParkedCarLayout)	
			PRINTLN("UPDATE_FRANKLINS_CARS_CLIENT - cleaning up - sData.iRequestLayout != GlobalServerBD_BlockC.iFranklinsParkedCarLayout")
			CleanupFranklinCars(sData)
			EXIT
		ENDIF
		
	ENDIF
	
	// initialise when player gets near and is in correct playing state.
	IF (sData.iState = FRANKLIN_CARS_STATE_INTIALISING)
		// we dont need to check this trigger every frame.
		IF ((GET_GAME_TIMER() - sData.iClientUpdateGameTime) > 1000)
			IF (IsSafeToRunFranklinCars(TRUE))
				InitialiseFranklinCarsClientData(sData)			
				sData.iState = FRANKLIN_CARS_STATE_CREATING
			ENDIF
			sData.iClientUpdateGameTime = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
	// load and create
	IF (sData.iState = FRANKLIN_CARS_STATE_CREATING)
		IF HasFranklinCarsAssetsLoaded(sData)
			IF CreateFranklinCars(sData)
				sData.iState = FRANKLIN_CARS_STATE_WAIT_FOR_COLLISION
			ENDIF
		ENDIF
	ENDIF
	
	// make sure collision has streamed in
	IF (sData.iState = FRANKLIN_CARS_STATE_WAIT_FOR_COLLISION)
		IF HasFranklinCarsCollisionStreamedIn(sData)
			sData.iClientUpdateGameTime = GET_GAME_TIMER() 
			sData.iState = FRANKLIN_CARS_STATE_WAIT_FOR_PHYSICS
		ENDIF
	ENDIF
	
	// wait breifly for physics to settle before freezing in position
	IF (sData.iState = FRANKLIN_CARS_STATE_WAIT_FOR_PHYSICS)
		IF (GET_GAME_TIMER() - sData.iClientUpdateGameTime > 500)
		AND AreFranklinCarsSettled(sData)
			FreezeFranklinCars(sData)	
			sData.iState = FRANKLIN_CARS_STATE_IS_ACTIVE
		ENDIF
	ENDIF
	
	// we dont do anything here apart from wait for the vehicles to be cleaned up.
	IF (sData.iState = FRANKLIN_CARS_STATE_IS_ACTIVE)
	
	ENDIF

ENDPROC

PROC ChangeFranklinCarLayout()

	INT iRand = GET_RANDOM_INT_IN_RANGE(0, NUM_FRANKLIN_CAR_LAYOUTS)
	
	// make sure we are changing from old value
	WHILE iRand = GlobalServerBD_BlockC.iFranklinsParkedCarLayout
		iRand = GET_RANDOM_INT_IN_RANGE(0, NUM_FRANKLIN_CAR_LAYOUTS)	
	ENDWHILE
	
	GlobalServerBD_BlockC.iFranklinsParkedCarNextUpdate = GET_CLOUD_TIME_AS_INT() + g_iFranklinCarsServerChangeSetupInterval
	
	// switch the model order (TRUE - will show Franklins model in slot 0)
	GlobalServerBD_BlockC.iFranklinsParkedCarModelOrder++
	IF (GlobalServerBD_BlockC.iFranklinsParkedCarModelOrder > 1)
		GlobalServerBD_BlockC.iFranklinsParkedCarModelOrder = 0
	ENDIF
	
	PRINTLN("[FRANKLINS_CARS] ChangeFranklinCarLayout - changed from, ", GlobalServerBD_BlockC.iFranklinsParkedCarLayout, " to ", iRand, " next update time at ", GlobalServerBD_BlockC.iFranklinsParkedCarNextUpdate)
	GlobalServerBD_BlockC.iFranklinsParkedCarLayout = iRand
	
ENDPROC


PROC UPDATE_FRANKLINS_CARS_SERVER(FRANKLIN_CAR_DATA & Data)
	
	IF NOT (g_bFranklinCarsActive)
		EXIT
	ENDIF
	
	// the server only need to check this periodically, it's not vital that the setup changes. 
	IF (GET_GAME_TIMER() - Data.iServerUpdateGameTime > g_iFranklinCarsServerUpdateInterval)

	
		IF (GlobalServerBD_BlockC.iFranklinsParkedCarLayout = -1)
			ChangeFranklinCarLayout()
			PRINTLN("UPDATE_FRANKLINS_CARS_SERVER - initialising, layout = ", GlobalServerBD_BlockC.iFranklinsParkedCarLayout)
		ELSE
			// use the cloud time as this will migrate. 
			IF (GET_CLOUD_TIME_AS_INT() > GlobalServerBD_BlockC.iFranklinsParkedCarNextUpdate)
				// check no players are around area while we change it.
				INT retPlayerIds, retNumber
				IF NOT NETWORK_IS_ANY_PLAYER_NEAR(retPlayerIds, retNumber, <<NUM_FRANKLIN_CAR_TRIGGER_X, NUM_FRANKLIN_CAR_TRIGGER_Y, NUM_FRANKLIN_CAR_TRIGGER_Z>>, g_fFranklinCarsActivationRadius)					
					// we are good to update layout. 
					ChangeFranklinCarLayout()				
				ENDIF
			ENDIF
		ENDIF
	
		Data.iServerUpdateGameTime = GET_GAME_TIMER()
		PRINTLN("UPDATE_FRANKLINS_CARS_SERVER - updated")
		
	ENDIF
	

ENDPROC

#ENDIF

