USING "rage_builtins.sch"
USING "globals.sch"
USING "net_include.sch"
USING "script_maths.sch"
USING "net_spawn_areas.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Nodes.sch
//      CREATED         :   Keith (but these are Neil's functions extracted from net_spawn.sch)
//      DESCRIPTION     :   Extract some useful node selection functions from net_spawn.sc to make them publically available.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




CONST_FLOAT ENEMY_OCCLUSION_RADIUS	10.0
//TWEAK_INT MAX_RESPAWN_NODES_CHECKED	40
TWEAK_INT MAX_DIST_FAIL_NODES	60
//TWEAK_FLOAT AREA_OCC_RADIUS_CHECK 1.0

CONST_FLOAT MIN_WIDTH_VEHICLE_DISABLED_NODES 3.5
CONST_FLOAT MIN_WIDTH_VEHICLE_ACTIVE_NODES 1.5

// -----------------------------------------------------------------------------------------------------------
//      Spawn Nodes - Helper Functions
// -----------------------------------------------------------------------------------------------------------

FUNC BOOL IsPointInViewingRangeOfPoint(VECTOR vCoord, VECTOR vViewingCoord, FLOAT fViewingHeading, FLOAT fViewingRange, FLOAT fViewingWidth, FLOAT fMaxZDiff=5.0)
	
	VECTOR vViewVector
	VECTOR vFarPoint
	
	fViewingHeading *= -1.0
	fViewingHeading += 360.0
	
	vViewVector.x = SIN(fViewingHeading)
	vViewVector.y = COS(fViewingHeading)
	vViewVector.z = 0.0
	vViewVector /= VMAG(vViewVector)
	vViewVector *= fViewingRange
	
	vFarPoint = vViewingCoord + vViewVector
	
	vFarPoint.z = vViewingCoord.z
	
	vFarPoint.z += fMaxZDiff
	vViewingCoord.z += fMaxZDiff
	
	
	RETURN(IS_POINT_IN_ANGLED_AREA (vCoord, vViewingCoord, vFarPoint, fViewingWidth))
	

ENDFUNC

FUNC BOOL IsAnyEnemyInViewingRangeOfPoint(VECTOR vCoord, FLOAT fHeading, FLOAT fViewingRange, FLOAT fViewingWidth, FLOAT fMaxZDiff=5.0)

	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF NOT (PLAYER_ID() = PlayerID)
			IF IS_NET_PLAYER_OK(PlayerID)
			AND IS_PLAYER_VISIBLE_TO_SCRIPT(PlayerID)
			AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
				IF NOT ARE_PLAYERS_ON_SAME_TEAM(PLAYER_ID(), PlayerID)
					IF IsPointInViewingRangeOfPoint(GET_PLAYER_COORDS(PlayerID), vCoord, fHeading, fViewingRange, fViewingWidth, fMaxZDiff)
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)

ENDFUNC

FUNC BOOL IsAnyEnemyNearPoint(VECTOR vCoord, FLOAT fDist)
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF IS_NET_PLAYER_OK(PlayerID)
		AND IS_PLAYER_VISIBLE_TO_SCRIPT(PlayerID)
		AND NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
		
			IF ((GET_PLAYER_TEAM(PLAYER_ID()) = -1) AND (GET_PLAYER_TEAM(PlayerID) = -1))
			AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
				RETURN(FALSE)
			ELSE

				IF (GET_PLAYER_TEAM(PLAYER_ID()) = -1) AND NOT (PLAYER_ID() = PlayerID)
				OR NOT ARE_PLAYERS_ON_SAME_TEAM(PLAYER_ID(), PlayerID)
					IF (VDIST(vCoord, GET_PLAYER_COORDS(PlayerID)) < fDist)
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDIF

		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC


FUNC BOOL IsObjectFMMCObject(ENTITY_INDEX EntityID, INT iType)

	INT i
	
	MODEL_NAMES modelName = GET_ENTITY_MODEL(EntityID)
	VECTOR vPos = GET_ENTITY_COORDS(EntityID, FALSE)

	// fmmc prop
	IF iType = 0
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfProps > 0)
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn = modelName
					IF VDIST(vPos, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos) < 0.01
						PRINTLN("[spawning] IsObjectFMMCObject - returning TRUE, fmmc prop, i = ", i)
						RETURN TRUE
					ENDIF				
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	// fmmc object
	IF iType = 1
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0)
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = modelName
					IF VDIST(vPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos) < 0.01
						PRINTLN("[spawning] IsObjectFMMCObject - returning TRUE, fmmc object, i = ", i)
						RETURN TRUE
					ENDIF				
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF	
	
	// fmmc vehicle
	IF iType = 2
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0)
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn = modelName
					IF VDIST(vPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos) < 0.01
						PRINTLN("[spawning] IsObjectFMMCObject - returning TRUE, fmmc vehicle, i = ", i)
						RETURN TRUE
					ENDIF				
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF	
	
	PRINTLN("[spawning] IsObjectFMMCObject - returning FALSE")
	RETURN FALSE

ENDFUNC


PROC SET_OBJECT_AS_FMMC_PROP_FOR_SPAWNING_PURPOSES(ENTITY_INDEX EntityID)

	IF IsObjectFMMCObject(EntityID, 0)
		PRINTLN("[spawning] SET_OBJECT_AS_FMMC_PROP_FOR_SPAWNING_PURPOSES - entity already an fmmc prop, entity id = ", NATIVE_TO_INT(EntityID))
	ELSE		
		g_FMMC_STRUCT_ENTITIES.iNumberOfProps++
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[g_FMMC_STRUCT_ENTITIES.iNumberOfProps].mn = GET_ENTITY_MODEL(EntityID)
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[g_FMMC_STRUCT_ENTITIES.iNumberOfProps].vPos = GET_ENTITY_COORDS(EntityID, FALSE)
		g_FMMC_STRUCT_ENTITIES.sPlacedProp[g_FMMC_STRUCT_ENTITIES.iNumberOfProps].fHead = GET_ENTITY_HEADING(EntityID)
		
		PRINTLN("[spawning] SET_OBJECT_AS_FMMC_PROP_FOR_SPAWNING_PURPOSES - entity set as fmmc prop, entity id = ", NATIVE_TO_INT(EntityID), " g_FMMC_STRUCT_ENTITIES.iNumberOfProps = ", g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
	ENDIF

ENDPROC

PROC CLEAR_FMMC_PROPS_FOR_SPAWNING_PURPOSES()
	PRINTLN("[spawning] CLEAR_FMMC_PROPS_FOR_SPAWNING_PURPOSES called")
	DEBUG_PRINTCALLSTACK()
	INT i
	REPEAT FMMC_MAX_NUM_OBJECTS i
		g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	g_FMMC_STRUCT_ENTITIES.iNumberOfProps = 0
ENDPROC

FUNC BOOL IsAnyFMMCPropAtCoord(VECTOR vCoord, FLOAT fAdditionalDistAroundProp = 0.5)	
	INT i
	
//	VECTOR vMax, vMin
//	FLOAT fLength, fWidth, fHeight, fRadius
					
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		
		// fmmc props
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfProps > 0)
			
			#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				PRINTLN("[spawning] IsAnyFMMCPropAtCoord - g_FMMC_STRUCT_ENTITIES.iNumberOfProps = ", g_FMMC_STRUCT_ENTITIES.iNumberOfProps)
			ENDIF
			#ENDIF	
		
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfProps i
				IF g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn != DUMMY_MODEL_FOR_SCRIPT				
					IF IsPointWithinModelBounds(vCoord, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].fHead, g_FMMC_STRUCT_ENTITIES.sPlacedProp[i].mn, fAdditionalDistAroundProp)
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsAnyFMMCPropAtCoord - true at ", vCoord, " for prop ", i)
						ENDIF
						#ENDIF
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		
		// fmmc objects
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0)
			
			#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				PRINTLN("[spawning] IsAnyFMMCPropAtCoord - g_FMMC_STRUCT_ENTITIES.iNumberOfObjects = ", g_FMMC_STRUCT_ENTITIES.iNumberOfObjects)
			ENDIF
			#ENDIF	
		
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfObjects i
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn != DUMMY_MODEL_FOR_SCRIPT				
					IF IsPointWithinModelBounds(vCoord, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].fHead, g_FMMC_STRUCT_ENTITIES.sPlacedObject[i].mn, 0.5)
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsAnyFMMCPropAtCoord - true at ", vCoord, " for object ", i)
						ENDIF
						#ENDIF
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF	
		
		// fmmc placed vehicles
		IF (g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles > 0)
			
			#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				PRINTLN("[spawning] IsAnyFMMCPropAtCoord - g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles = ", g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles)
			ENDIF
			#ENDIF	
		
			REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfVehicles i
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn != DUMMY_MODEL_FOR_SCRIPT				
					IF IsPointWithinModelBounds(vCoord, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].fHead, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[i].mn, 0.5)
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsAnyFMMCPropAtCoord - true at ", vCoord, " for vehicle ", i)
						ENDIF
						#ENDIF
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF	
		
		// current mission vehicles
		IF (gBG_MC_serverBD_VARS.iNumVehicles > 0)
			
			#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				PRINTLN("[spawning] IsAnyFMMCPropAtCoord - gBG_MC_serverBD_VARS.iNumVehicles = ", gBG_MC_serverBD_VARS.iNumVehicles)
			ENDIF
			#ENDIF	
		
			REPEAT gBG_MC_serverBD_VARS.iNumVehicles i
				IF DOES_ENTITY_EXIST(gBG_MC_serverBD_VARS.viVehicles[i])
				AND NOT IS_ENTITY_DEAD(gBG_MC_serverBD_VARS.viVehicles[i])							
					IF IsPointWithinModelBounds(vCoord, GET_ENTITY_COORDS(gBG_MC_serverBD_VARS.viVehicles[i]), GET_ENTITY_HEADING(gBG_MC_serverBD_VARS.viVehicles[i]), GET_ENTITY_MODEL(gBG_MC_serverBD_VARS.viVehicles[i]), 0.5)
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsAnyFMMCPropAtCoord - true at ", vCoord, " for gBG_MC_serverBD_VARS.viVehicles[ ", i, "]")
						ENDIF
						#ENDIF
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF		
		
		// current mission objects
		IF (gBG_MC_serverBD_VARS.iNumObjects > 0)
			
			#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bShowAdvancedSpew)
				PRINTLN("[spawning] IsAnyFMMCPropAtCoord - gBG_MC_serverBD_VARS.iNumObjects = ", gBG_MC_serverBD_VARS.iNumObjects)
			ENDIF
			#ENDIF	
		
			REPEAT gBG_MC_serverBD_VARS.iNumObjects i
				IF DOES_ENTITY_EXIST(gBG_MC_serverBD_VARS.oiObjects[i])
				AND NOT IS_ENTITY_DEAD(gBG_MC_serverBD_VARS.oiObjects[i])							
					IF IsPointWithinModelBounds(vCoord, GET_ENTITY_COORDS(gBG_MC_serverBD_VARS.oiObjects[i]), GET_ENTITY_HEADING(gBG_MC_serverBD_VARS.oiObjects[i]), GET_ENTITY_MODEL(gBG_MC_serverBD_VARS.oiObjects[i]), 0.5)
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsAnyFMMCPropAtCoord - true at ", vCoord, " for gBG_MC_serverBD_VARS.oiObjects[ ", i, "]")
						ENDIF
						#ENDIF
						RETURN(TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF		
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
			PRINTLN("[spawning] IsAnyFMMCPropAtCoord - player not on any fm mission ")
		ENDIF
		#ENDIF			
	ENDIF
	#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bShowAdvancedSpew)
		PRINTLN("[spawning] IsAnyFMMCPropAtCoord - returning FALSE at ", vCoord)
	ENDIF
	#ENDIF	
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IsAnotherPlayerCurrentlyWarpingToVehicleBounds(VECTOR vVehPos, FLOAT fVehicleHeading, MODEL_NAMES VehicleModel, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE)
	INT i
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		IF NOT (NATIVE_TO_INT(LocalPlayerID) = i)
		OR (bCheckThisPlayer = TRUE)
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
			AND IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)

					IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
						IF IsPointWithinModelBounds(GlobalServerBD.g_vWarpRequest[i], vVehPos, fVehicleHeading, VehicleModel)
							PRINTLN("IsAnotherPlayerCurrentlyWarpingToVehicleBounds - player ", i, " is currently warping there")
							RETURN(TRUE)		
						ENDIF
					ELSE
						// other player has perhaps arrived there and is still standing there
						IF IsPointWithinModelBounds(GET_PLAYER_COORDS(PlayerID), vVehPos, fVehicleHeading, VehicleModel)
							PRINTLN("IsAnotherPlayerCurrentlyWarpingToVehicleBounds - player ", i, " is currently warping there (arrived)")
							RETURN(TRUE)
						ENDIF
					ENDIF

			ELSE
				IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
					IF IsPointWithinModelBounds(GlobalServerBD.g_vWarpRequest[i], vVehPos, fVehicleHeading, VehicleModel)
						PRINTLN("IsAnotherPlayerCurrentlyWarpingToVehicleBounds - (PLAYERS NOT OK) player ", i, " is currently warping there")
						RETURN(TRUE)		
					ENDIF
				ELSE
					// other player has perhaps arrived there and is still standing there
					IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE) // the other player could be coming from transistion, so 2nd param should be false, but need to check if player has a ped index. fix for 2782022 
						IF DOES_ENTITY_EXIST(GET_PLAYER_PED(PlayerID))
							IF IsPointWithinModelBounds(GET_PLAYER_COORDS(PlayerID), vVehPos, fVehicleHeading, VehicleModel)
								PRINTLN("IsAnotherPlayerCurrentlyWarpingToVehicleBounds - (PLAYERS NOT OK) player ", i, " is currently warping there (arrived)")
								RETURN(TRUE)
							ENDIF
						ELSE
							PRINTLN("IsAnotherPlayerCurrentlyWarpingToVehicleBounds - (PLAYERS NOT OK) ped doesnt exist yet, player ", i)  							
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsAnotherPlayerCurrentlyWarpingToPosition(VECTOR vPos, FLOAT fMinRadius, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE, BOOL bCheckIfEnemies=FALSE)
	INT i
	PLAYER_INDEX PlayerID
	BOOL bIgnoreThisPlayer
	REPEAT NUM_NETWORK_PLAYERS i
		IF NOT (NATIVE_TO_INT(LocalPlayerID) = i)
		OR (bCheckThisPlayer = TRUE)
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			
			bIgnoreThisPlayer = FALSE
			
			IF (bCheckIfEnemies)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
				AND IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)
					IF (GET_PLAYER_TEAM(PlayerID) = GET_PLAYER_TEAM(LocalPlayerID))
						PRINTLN("IsAnotherPlayerCurrentlyWarpingToPosition - player ", i, " is not an enemy")
						bIgnoreThisPlayer = TRUE
					ENDIF
				ENDIF		
			ENDIF
			
			IF NOT (bIgnoreThisPlayer)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
				AND IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)
					//IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(LocalPlayerID, PlayerID)
						IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
							IF (VDIST(GlobalServerBD.g_vWarpRequest[i], vPos) < fMinRadius)
								PRINTLN("IsAnotherPlayerCurrentlyWarpingToPosition - player ", i, " is currently warping there - enemy check ", bCheckIfEnemies)
								RETURN(TRUE)		
							ENDIF
						ELSE
							// other player has perhaps arrived there and is still standing there
							IF (VDIST(GET_PLAYER_COORDS(PlayerID), vPos) < 1.0)
								PRINTLN("IsAnotherPlayerCurrentlyWarpingToPosition - player ", i, " is currently warping there (arrived)- enemy check ", bCheckIfEnemies)
								RETURN(TRUE)
							ENDIF
						ENDIF
					//ENDIF
				ELSE
					IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
						IF (VDIST(GlobalServerBD.g_vWarpRequest[i], vPos) < fMinRadius)
							PRINTLN("IsAnotherPlayerCurrentlyWarpingToPosition - (PLAYERS NOT OK) player ", i, " is currently warping there- enemy check ", bCheckIfEnemies)
							RETURN(TRUE)		
						ENDIF
					ELSE
						// other player has perhaps arrived there and is still standing there
						IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
							IF (VDIST(GET_PLAYER_COORDS(PlayerID), vPos) < 1.0)
								PRINTLN("IsAnotherPlayerCurrentlyWarpingToPosition - (PLAYERS NOT OK) player ", i, " is currently warping there (arrived)- enemy check ", bCheckIfEnemies)
								RETURN(TRUE)
							ENDIF
						ENDIF
					ENDIF				
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PrintVehicleSpawnRequestData(VEH_SPAWN_REQ_DATA Data)
	IF IS_MODEL_VALID(Data.ModelName)
	AND NOT (Data.ModelName = DUMMY_MODEL_FOR_SCRIPT)
		STRING strName = GET_MODEL_NAME_FOR_DEBUG(Data.ModelName)
		PRINTLN("Veh Spawn Request Data, vPos = ", Data.vPos, ", fHeading = ", Data.fHeading, ",  ModelName = ", strName)
	ELSE
		PRINTLN("Veh Spawn Request Data, vPos = ", Data.vPos, ", fHeading = ", Data.fHeading)
	ENDIF
ENDPROC

PROC PrintVehicleSpawnRequestDataForPlayer(PLAYER_INDEX PlayerID)
	PRINTLN("[spawning] Vehicle spawn request data for player ", NATIVE_TO_INT(PlayerID)) 
	INT i
	REPEAT NUM_SERVER_STORED_VEH_REQUESTS i
		PrintVehicleSpawnRequestData(GlobalServerBD.g_VehSpawnReqData[NATIVE_TO_INT(PlayerID)][i])		
	ENDREPEAT	
ENDPROC
#ENDIF

FUNC BOOL DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds(VECTOR vVehPos, FLOAT fVehHeading, MODEL_NAMES VehicleModel, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE)
	
	#IF IS_DEBUG_BUILD	
		IF IS_MODEL_VALID(VehicleModel)
			STRING strModelName = GET_MODEL_NAME_FOR_DEBUG(VehicleModel)
			PRINTLN("DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds(", vVehPos, ", ", fVehHeading, ", ", strModelName, ", ", NATIVE_TO_INT(LocalPlayerID), ", ", bCheckThisPlayer, ")")
		ELSE
			PRINTLN("DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds(", vVehPos, ", ", fVehHeading, ", model hash ", ENUM_TO_INT(VehicleModel), ", ", NATIVE_TO_INT(LocalPlayerID), ", ", bCheckThisPlayer, ")")
		
		ENDIF
	#ENDIF
	
	INT i
	INT j
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		IF NOT (NATIVE_TO_INT(LocalPlayerID) = i)
		OR (bCheckThisPlayer = TRUE)
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			// check they are within range first before doing the more expensive check.
			
			REPEAT NUM_SERVER_STORED_VEH_REQUESTS j
				IF DoVehicleMinRadiusOverlap(vVehPos, VehicleModel, GlobalServerBD.g_VehSpawnReqData[i][j].vPos, GlobalServerBD.g_VehSpawnReqData[i][j].ModelName)
					
					PRINTLN("DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds - true - min radius overlap - player ", i, " :")
					#IF IS_DEBUG_BUILD
						PrintVehicleSpawnRequestData(GlobalServerBD.g_VehSpawnReqData[i][j])
					#ENDIF

					IF DoVehicleBoundsOverlap(vVehPos, fVehHeading, VehicleModel, GlobalServerBD.g_VehSpawnReqData[i][j].vPos, GlobalServerBD.g_VehSpawnReqData[i][j].fHeading, GlobalServerBD.g_VehSpawnReqData[i][j].ModelName)
						PRINTLN("DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds - true, player ", i)
						#IF IS_DEBUG_BUILD
							PrintVehicleSpawnRequestData(GlobalServerBD.g_VehSpawnReqData[i][j])
						#ENDIF
						
						IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
						AND IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)
							PRINTLN("DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds - player ", i, " is currently spawning a vehicle there")
							RETURN(TRUE)		
						ELSE
							PRINTLN("DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds - (PLAYERS NOT OK) player ", i, " is currently spawning a vehicle there") 
							RETURN(TRUE)		
						ENDIF	
					ELSE
						
					ENDIF
				ELSE
					
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsAnotherPlayerCurrentlySpawningVehicleAtPosition(VECTOR vPos, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE)
	INT i, j
	PLAYER_INDEX PlayerID
	REPEAT NUM_NETWORK_PLAYERS i
		IF NOT (NATIVE_TO_INT(LocalPlayerID) = i)
		OR (bCheckThisPlayer = TRUE)
			PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
			REPEAT NUM_SERVER_STORED_VEH_REQUESTS j
				IF NOT (ENUM_TO_INT(GlobalServerBD.g_VehSpawnReqData[i][j].ModelName) = 0)
					IF IsPointWithinModelBounds(vPos, GlobalServerBD.g_VehSpawnReqData[i][j].vPos, GlobalServerBD.g_VehSpawnReqData[i][j].fHeading, GlobalServerBD.g_VehSpawnReqData[i][j].ModelName)
						PRINTLN("IsAnotherPlayerCurrentlySpawningVehicleAtPosition - true, player ", i)
						#IF IS_DEBUG_BUILD
							PrintVehicleSpawnRequestData(GlobalServerBD.g_VehSpawnReqData[i][j])
						#ENDIF
						IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
						AND IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)
							PRINTLN("IsAnotherPlayerCurrentlySpawningVehicleAtPosition - player ", i, " is currently spawning a vehicle there")
							RETURN(TRUE)		
						ELSE
							PRINTLN("IsAnotherPlayerCurrentlySpawningVehicleAtPosition - (PLAYERS NOT OK) player ", i, " is currently spawning a vehicle there") 
							RETURN(TRUE)		
						ENDIF	
					ENDIF
				ENDIF
			ENDREPEAT			
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(VECTOR vPos, FLOAT fMinRadiusPlayer, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE, BOOL bCheckThisPlayerVehicle=FALSE)
	IF IsAnotherPlayerCurrentlyWarpingToPosition(vPos, fMinRadiusPlayer, LocalPlayerID, bCheckThisPlayer)
	OR IsAnotherPlayerCurrentlySpawningVehicleAtPosition(vPos, LocalPlayerID, bCheckThisPlayerVehicle)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IsAnotherPlayerOrPlayerVehicleSpawningInCarBounds(VECTOR vPos, FLOAT fHeading, MODEL_NAMES VehicleModel, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE, BOOL bCheckThisPlayerVehicle=FALSE)
	IF IsAnotherPlayerCurrentlyWarpingToVehicleBounds(vPos, fHeading, VehicleModel, LocalPlayerID, bCheckThisPlayer)
	//OR IsAnotherPlayerCurrentlySpawningVehicleAtPosition(vPos, LocalPlayerID, bCheckThisPlayerVehicle)
	OR DoesAnotherPlayersCurrentlySpawningVehicleOverlapTheseVehicleBounds(vPos, fHeading, VehicleModel, LocalPlayerID, bCheckThisPlayerVehicle) 
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC FLOAT ClosestCurrentWarpingPlayerToPosition(VECTOR vPos, PLAYER_INDEX LocalPlayerID, BOOL bCheckThisPlayer=FALSE)
	
	FLOAT fDist = 999999.9
	FLOAT fThisDist
	
	IF IS_NET_PLAYER_OK(LocalPlayerID, FALSE, TRUE)
		INT i
		PLAYER_INDEX PlayerID
		REPEAT NUM_NETWORK_PLAYERS i
			IF NOT (NATIVE_TO_INT(LocalPlayerID) = i)
			OR (bCheckThisPlayer = TRUE)
				PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
				IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)

					//IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(LocalPlayerID, PlayerID)
						IF GET_PLAYER_TEAM(PlayerID) <> GET_PLAYER_TEAM(LocalPlayerID)
						OR ((GET_PLAYER_TEAM(PlayerID) = -1) AND  (GET_PLAYER_TEAM(LocalPlayerID) = -1))
							
							IF NOT GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerID, LocalPlayerID)
							
								IF (GlobalServerBD.bWarpRequestTimeIntialised[i])
									fThisDist = VDIST(GlobalServerBD.g_vWarpRequest[i], vPos) 							
									IF (fThisDist < fDist)
										PRINTLN("ClosestCurrentWarpingPlayerToPosition - player ", i, " warping to ", GlobalServerBD.g_vWarpRequest[i], " dist = ", fThisDist)
										fDist = fThisDist
									ENDIF
								ELSE
									//PRINTLN("ClosestCurrentWarpingPlayerToPosition - player ", i, " bWarpRequestTimeIntialised=FALSE")
								ENDIF
							ELSE
								PRINTLN("ClosestCurrentWarpingPlayerToPosition - player ", i, " GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG")
							ENDIF
							
						ELSE
							//PRINTLN("ClosestCurrentWarpingPlayerToPosition - player ", i, " team check fail")
						ENDIF
					//ELSE						
						//PRINTLN("ClosestCurrentWarpingPlayerToPosition - player ", i, " same session fail")
					//ENDIF

				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN(fDist)
ENDFUNC

FUNC BOOL IsPointOkForSpawning(VECTOR vCoord, FLOAT fHeading, BOOL bForAVehicle, BOOL bAtGanghouse, BOOL bDoVisibleChecks, BOOL bDoVisibilityChecksOnTeammates, FLOAT fVisibleDistance=120.0, FLOAT fViewingRange = 20.0, FLOAT fViewingWidth = 20.0, FLOAT fMaxZDiff=5.0, FLOAT fEnemyOcclusionRadius=ENEMY_OCCLUSION_RADIUS, FLOAT fTeammateVisibleRange = 0.0, BOOL bConsiderVisiblePlayersOnly=FALSE, BOOL bIgnoreLocalPlayerChecks=FALSE)
	
	#IF IS_DEBUG_BUILD
	IF (g_SpawnData.bShowAdvancedSpew)
		PRINTLN("[spawning] IsPointOkForSpawning - called with:")
		PRINTLN("[spawning]     vCoord = ", vCoord)
		IF (bForAVehicle)
			PRINTLN("[spawning]     bForAVehicle = TRUE")
		ELSE
			PRINTLN("[spawning]     bForAVehicle = FALSE")
		ENDIF
		IF (bAtGanghouse)
			PRINTLN("[spawning]     bAtGanghouse = TRUE")
		ELSE
			PRINTLN("[spawning]     bAtGanghouse = FALSE")
		ENDIF
		IF (bDoVisibleChecks)
			PRINTLN("[spawning]     bDoVisibleChecks = TRUE")
		ELSE
			PRINTLN("[spawning]     bDoVisibleChecks = FALSE")
		ENDIF	
		IF (bDoVisibilityChecksOnTeammates)
			PRINTLN("[spawning]     bDoVisibilityChecksOnTeammates = TRUE")
		ELSE
			PRINTLN("[spawning]     bDoVisibilityChecksOnTeammates = FALSE")
		ENDIF	
	ENDIF
	#ENDIF
	
	g_SpawnData.iIsPointOkForSpawningPassMarks = 0
	
	// spawning at the gang house is less strict on players who can see that point as there it likely that someone will always see all the points
	IF NOT IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(vCoord, 0.5, PLAYER_ID())
		g_SpawnData.iIsPointOkForSpawningPassMarks += 1
		IF (bAtGanghouse) 
			IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoord, 3.65, 0.5, 1.5, 1.0, FALSE, bDoVisibleChecks, bDoVisibilityChecksOnTeammates, fVisibleDistance, bIgnoreLocalPlayerChecks, DEFAULT, DEFAULT, DEFAULT, DEFAULT, fTeammateVisibleRange, bConsiderVisiblePlayersOnly)
				g_SpawnData.iIsPointOkForSpawningPassMarks += g_SpawnData.iIsPointOKForNetEntityCreationPassMarks
				IF NOT IsAnyEnemyNearPoint(vCoord, fEnemyOcclusionRadius)
					g_SpawnData.iIsPointOkForSpawningPassMarks += 1
					IF NOT IsAnyFMMCPropAtCoord(vCoord)
						g_SpawnData.iIsPointOkForSpawningPassMarks += 1
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
						PRINTLN("[spawning] IsPointOkForSpawning - returning TRUE 1")
						ENDIF
						#ENDIF
						RETURN(TRUE)							
					ELSE
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
						PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 1b - IsAnyFMMCPropAtCoord=TRUE")
						ENDIF
						#ENDIF
					ENDIF				
				
				ELSE
					#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
					PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE - 1 - enemy near point")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				g_SpawnData.iIsPointOkForSpawningPassMarks += g_SpawnData.iIsPointOKForNetEntityCreationPassMarks
				#IF IS_DEBUG_BUILD
				IF (g_SpawnData.bShowAdvancedSpew)
				PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 2 - not ok for net entity creation")
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			IF NOT (bForAVehicle)
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoord, 3.65, 0.5, 1.5, 1.0, FALSE, bDoVisibleChecks, bDoVisibilityChecksOnTeammates, fVisibleDistance, bIgnoreLocalPlayerChecks, DEFAULT, DEFAULT, DEFAULT, DEFAULT, fTeammateVisibleRange, bConsiderVisiblePlayersOnly )
					g_SpawnData.iIsPointOkForSpawningPassMarks += g_SpawnData.iIsPointOKForNetEntityCreationPassMarks
					IF NOT IsAnyEnemyNearPoint(vCoord, fEnemyOcclusionRadius)
						g_SpawnData.iIsPointOkForSpawningPassMarks++
						
						IF NOT IsAnyEnemyInViewingRangeOfPoint(vCoord, fHeading, fViewingRange, fViewingWidth)
							
							g_SpawnData.iIsPointOkForSpawningPassMarks++
						
							IF NOT IsAnyFMMCPropAtCoord(vCoord)
								g_SpawnData.iIsPointOkForSpawningPassMarks++
								
								#IF IS_DEBUG_BUILD
								IF (g_SpawnData.bShowAdvancedSpew)
								PRINTLN("[spawning] IsPointOkForSpawning - returning TRUE 2")
								ENDIF
								#ENDIF
								RETURN(TRUE)							
							ELSE
								#IF IS_DEBUG_BUILD
								IF (g_SpawnData.bShowAdvancedSpew)
								PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 3 - IsAnyFMMCPropAtCoord=TRUE")
								ENDIF
								#ENDIF
							ENDIF
					
						ELSE
						
							// check no fmmc props have been created there.
							
							#IF IS_DEBUG_BUILD
							IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 3 - enemy in viewing range")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
						PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 4 - enemy near point ")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					g_SpawnData.iIsPointOkForSpawningPassMarks += g_SpawnData.iIsPointOKForNetEntityCreationPassMarks
					#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
					PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 5 - not ok for entity creation")
					ENDIF
					#ENDIF
				ENDIF
			ELSE
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vCoord, 6.0, 5, 5, 5,FALSE,bDoVisibleChecks, bDoVisibilityChecksOnTeammates, fVisibleDistance, bIgnoreLocalPlayerChecks, DEFAULT, DEFAULT, DEFAULT, DEFAULT, fTeammateVisibleRange, bConsiderVisiblePlayersOnly )
					
					g_SpawnData.iIsPointOkForSpawningPassMarks += g_SpawnData.iIsPointOKForNetEntityCreationPassMarks
					IF NOT IsAnyEnemyNearPoint(vCoord, fEnemyOcclusionRadius)
						g_SpawnData.iIsPointOkForSpawningPassMarks++
						
						IF NOT IsAnyEnemyInViewingRangeOfPoint(vCoord, fHeading, fViewingRange, fViewingWidth, fMaxZDiff)
						
							g_SpawnData.iIsPointOkForSpawningPassMarks++
							IF NOT IsAnyFMMCPropAtCoord(vCoord)
								g_SpawnData.iIsPointOkForSpawningPassMarks++
								#IF IS_DEBUG_BUILD
								IF (g_SpawnData.bShowAdvancedSpew)
								PRINTLN("[spawning] IsPointOkForSpawning - returning TRUE 3")
								ENDIF
								#ENDIF
								RETURN(TRUE)							
							ELSE
								#IF IS_DEBUG_BUILD
								IF (g_SpawnData.bShowAdvancedSpew)
								PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 5b - IsAnyFMMCPropAtCoord=TRUE")
								ENDIF
								#ENDIF
							ENDIF						
							
						ELSE
							#IF IS_DEBUG_BUILD
							IF (g_SpawnData.bShowAdvancedSpew)
							PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 6 - enemy in viewing range")
							ENDIF
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF (g_SpawnData.bShowAdvancedSpew)
						PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 7 - enemy near point ")
						ENDIF
						#ENDIF
					ENDIF
				ELSE
					g_SpawnData.iIsPointOkForSpawningPassMarks += g_SpawnData.iIsPointOKForNetEntityCreationPassMarks
					#IF IS_DEBUG_BUILD
					IF (g_SpawnData.bShowAdvancedSpew)
					PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 8 - not ok for entity creation ")
					ENDIF
					#ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF (g_SpawnData.bShowAdvancedSpew)
		PRINTLN("[spawning] IsPointOkForSpawning - returning FALSE 9 - another player is warping there")	
		ENDIF
		#ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC



PROC GetClosestMPSpawnCoordToPlayer(MP_SPAWN_POINT &SpawnPoints[], VECTOR &vPos, FLOAT &fHead, PLAYER_INDEX PlayerID)
	FLOAT fDist = 999999999.9
	FLOAT fThisDist
	INT i
	INT iReturn = -1
	
	REPEAT  COUNT_OF(SpawnPoints) i
		fThisDist = VDIST(SpawnPoints[i].Pos, GET_PLAYER_COORDS(PlayerID))
		IF (fThisDist < fDist)
			IF NOT IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(SpawnPoints[i].Pos, 5.0, PlayerID)
				fDist = fThisDist
				iReturn = i
			ELSE
				PRINTLN("GetClosestMPSpawnCoordToPlayer - another player is currently warping or spawning to this point")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT (iReturn = -1)
		vPos = 	SpawnPoints[iReturn].Pos
		fHead = SpawnPoints[iReturn].Heading
	ELSE
		SCRIPT_ASSERT("GetClosestMPSpawnCoordToPlayer - could not find any points!")
	ENDIF
	
ENDPROC

PROC GetClosestMPSpawnCoordToCoord(MP_SPAWN_POINT &SpawnPoints[], VECTOR &vPos, FLOAT &fHead, VECTOR vCoord)
	FLOAT fDist = 999999999.9
	FLOAT fThisDist
	INT i
	INT iReturn = -1

	REPEAT  COUNT_OF(SpawnPoints) i
		fThisDist = VDIST(SpawnPoints[i].Pos, vCoord)
		IF (fThisDist < fDist)
			IF NOT IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(SpawnPoints[i].Pos, 5.0, PLAYER_ID())
				fDist = fThisDist
				iReturn = i
			ELSE
				PRINTLN("GetClosestMPSpawnCoordToCoord - another player is currently warping or spawning to this point")
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT (iReturn = -1)
		vPos = 	SpawnPoints[iReturn].Pos
		fHead = SpawnPoints[iReturn].Heading
	ELSE
		SCRIPT_ASSERT("GetClosestMPSpawnCoordToCoord - could not find any points!")
	ENDIF

	
ENDPROC


//PROC RotateVec(VECTOR &InVec, VECTOR rotation)
//	
//	FLOAT CosAngle
//	FLOAT SinAngle
//	VECTOR ReturnVec
//
//	// Rotation about the x axis 
//	CosAngle = COS(rotation.x)
//	SinAngle = SIN(rotation.x)
//	ReturnVec.x = InVec.x
//	ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
//	ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
//	InVec = ReturnVec
//
//	// Rotation about the y axis
//	CosAngle = COS(rotation.y)
//	SinAngle = SIN(rotation.y)
//	ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
//	ReturnVec.y = InVec.y
//	ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
//	InVec = ReturnVec
//	
//	// Rotation about the z axis 
//	CosAngle = COS(rotation.z)
//	SinAngle = SIN(rotation.z)
//	ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
//	ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
//	ReturnVec.z = InVec.z
//	InVec = ReturnVec
//
//ENDPROC


FUNC BOOL IsVehicleNodeFacingPoint(VECTOR vNodeCoord, FLOAT fNodeHeading, VECTOR vFacing)
	
	// get forward vec
	VECTOR vForward = <<0.0, 1.0, 0.0>>
	RotateVec(vForward, <<0.0, 0.0, fNodeHeading>> )

	// vec to vFacing
	VECTOR vVec = vFacing - vNodeCoord

	IF DOT_PRODUCT(vVec, vForward) >= 0.0
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC




//
//STRUCT NearestCarNodeSearch
//	VECTOR vFavourFacing 
//
//	FLOAT fMinPlayerDist=0.0
//	FLOAT fMinDistFromCoords=0.0	
//	FLOAT fMaxDistance=0.0
//	FLOAT fUpperZLimit=4.0
//	FLOAT fLowerZLimit=9999.0
//	
//	BOOL bDoSafeForSpawnCheck=TRUE
//	BOOL bGetSafeOffset=FALSE	
//	BOOL bConsiderOnlyActiveNodes=TRUE
//	BOOL bWaterNodesOnly=FALSE 	
//	BOOL bCheckVehicleSpawning=FALSE
//	BOOL bConsiderHighways=TRUE 
//	BOOL bConsiderTunnels=FALSE 	
//	BOOL bCheckSpawnExclusionZones=TRUE
//	BOOL bAllowFallbackToInactiveNodes=TRUE
//	
//	BOOL bStartOfMissionPVSpawn=FALSE
//	
//	BOOL bCheckInsideArea
//	VECTOR vAreaCoords1
//	VECTOR vAreaCoords2
//	FLOAT fAreaFloat
//	INT iAreaShape
//	
//	BOOL bCouldNotFindWaterNode
//	
//	BOOL bAllowReducingVisibility
//	BOOL bReturnRandomGoodNode
//	BOOL bReturnNearestGoodNode
//	
//	BOOL bCheckOwnVisibility = TRUE
//	BOOL bUsingOffsetOrigin = FALSE
//	
//	INT iFallbackLevel=0 
//	
//	MODEL_NAMES CarModel=TAILGATER // fairly generic sized vehicle
//	
//	VECTOR vStartCoords
//	
//	VECTOR vAvoidCoords[MAX_NUM_AVOID_RADIUS]
//	FLOAT fAvoidRadius[MAX_NUM_AVOID_RADIUS]
//	
//	BOOL bCheckPVNoSpawnZone
//	FLOAT fVisibleDistance = 120.0
//	
//	BOOL bDoneMoveFallback
//	BOOL bIsForFlyingVehicle
//	
//	BOOL bCheckForLowestEnemiesNearPoint = FALSE
//	INT iLowestNumEnemiesNearPoint = 999
//	FLOAT fCheckForLowestnemiesMaxDist = 9999.9 
//
//	BOOL bDoVisibilityChecks = TRUE
//
//ENDSTRUCT



#IF IS_DEBUG_BUILD
PROC Print_NearestCarNodeSearch(NearestCarNodeSearch &SearchParams)
	PRINTLN("NearestCarNodeSearch:") 
	PRINTLN("     vFavourFacing=", SearchParams.vFavourFacing) 
	
	PRINTLN("     fMinPlayerDist=", SearchParams.fMinPlayerDist) 
	PRINTLN("     fMinDistFromCoords=", SearchParams.fMinDistFromCoords) 
	PRINTLN("     fMaxDistance=", SearchParams.fMaxDistance) 
	PRINTLN("     fUpperZLimit=", SearchParams.fUpperZLimit) 
	PRINTLN("     fLowerZLimit=", SearchParams.fLowerZLimit) 
	
	PRINTLN("     bDoSafeForSpawnCheck = ", SearchParams.bDoSafeForSpawnCheck) 
	PRINTLN("     bGetSafeOffset = ", SearchParams.bGetSafeOffset) 
	PRINTLN("     bConsiderOnlyActiveNodes = ", SearchParams.bConsiderOnlyActiveNodes) 
	PRINTLN("     bWaterNodesOnly = ", SearchParams.bWaterNodesOnly) 
	PRINTLN("     bCheckVehicleSpawning = ", SearchParams.bCheckVehicleSpawning) 
	PRINTLN("     bConsiderHighways = ", SearchParams.bConsiderHighways) 
	PRINTLN("     bConsiderTunnels = ", SearchParams.bConsiderTunnels) 
	PRINTLN("     bCheckSpawnExclusionZones = ", SearchParams.bCheckSpawnExclusionZones) 
	PRINTLN("     bAllowFallbackToInactiveNodes = ", SearchParams.bAllowFallbackToInactiveNodes) 
	
	PRINTLN("     bStartOfMissionPVSpawn = ", SearchParams.bStartOfMissionPVSpawn) 
	
	PRINTLN("     bCheckInsideArea = ", SearchParams.bCheckInsideArea) 	
	PRINTLN("     vAreaCoords1 = ", SearchParams.vAreaCoords1) 
	PRINTLN("     vAreaCoords2 = ", SearchParams.vAreaCoords2) 
	PRINTLN("     fAreaFloat = ", SearchParams.fAreaFloat) 
	PRINTLN("     iAreaShape = ", SearchParams.iAreaShape) 
	
	PRINTLN("     bCouldNotFindWaterNode = ", SearchParams.bCouldNotFindWaterNode) 
	
	PRINTLN("     bAllowReducingVisibility = ", SearchParams.bAllowReducingVisibility) 
	PRINTLN("		bReturnRandomGoodNode = ", SearchParams.bReturnRandomGoodNode) 
	PRINTLN("		bReturnNearestGoodNode = ", SearchParams.bReturnNearestGoodNode) 
	
	PRINTLN("		bCheckOwnVisibility = ", SearchParams.bCheckOwnVisibility) 		
	PRINTLN("		bUsingOffsetOrigin = ", SearchParams.bUsingOffsetOrigin) 
	
	PRINTLN("     iFallbackLevel=", SearchParams.iFallbackLevel) 
	
	PRINTLN("		CarModel=", ENUM_TO_INT(SearchParams.CarModel)) 
	
	PRINTLN("		vStartCoords = ", SearchParams.vStartCoords) 
	
	INT i
	REPEAT MAX_NUM_AVOID_RADIUS i
		PRINTLN("		vAvoidCoords[", i, "] = ", SearchParams.vAvoidCoords[i]) 
		PRINTLN("		fAvoidRadius[", i, "] = ",  SearchParams.fAvoidRadius[i]) 
	ENDREPEAT
	
	PRINTLN("		bCheckPVNoSpawnZone = ", SearchParams.bCheckPVNoSpawnZone) 
	PRINTLN("		fVisibleDistance = ", SearchParams.fVisibleDistance) 
	
	PRINTLN("		bDoneMoveFallback = ", SearchParams.bDoneMoveFallback) 
	PRINTLN("     bIsForFlyingVehicle = ", SearchParams.bIsForFlyingVehicle) 
	
	PRINTLN("     bCheckForLowestEnemiesNearPoint = ", SearchParams.bCheckForLowestEnemiesNearPoint) 
	PRINTLN("     iLowestNumEnemiesNearPoint = ", SearchParams.iLowestNumEnemiesNearPoint) 
	PRINTLN("     fCheckForLowestnemiesMaxDist = ", SearchParams.fCheckForLowestnemiesMaxDist) 
	
	PRINTLN("	    bDoVisibilityChecks = ", SearchParams.bDoVisibilityChecks) 
	PRINTLN("	    bCheckThisPlayerVehicle = ", SearchParams.bCheckThisPlayerVehicle) 
	PRINTLN("	    bEnforceMinDistCheckForCustomNodes = ", SearchParams.bEnforceMinDistCheckForCustomNodes) 
	
	PRINTLN("	    bIgnoreLocalPlayerChecks = ", SearchParams.bIgnoreLocalPlayerChecks) 
	
	PRINTLN("	    bUseCustomNodesOnly = ", SearchParams.bUseCustomNodesOnly) 
	PRINTLN("	    bCanFaceOncomingTraffic = ", SearchParams.bCanFaceOncomingTraffic) 
ENDPROC
#ENDIF

FUNC FLOAT GetCarModelWidth(MODEL_NAMES CarModel, FLOAT fMinWidth)
	VECTOR vMin, vMax
	FLOAT fWidth
	SAFE_GET_MODEL_DIMENSIONS(CarModel, vMin, vMax)	
	fWidth = vMax.x - vMin.x
	IF (fWidth < fMinWidth)
		PRINTLN("GetNodeSpawnPosition - returning minimum width of ", fMinWidth, " actual width is ", fWidth)
		RETURN fMinWidth
	ENDIF
	RETURN fWidth
ENDFUNC

FUNC BOOL IsPointUnderBridge(VECTOR vCoord)
	
	FLOAT fZ

	IF GET_GROUND_Z_FOR_3D_COORD(<<vCoord.x, vCoord.y, vCoord.z+500.0>>, fZ)
		fZ = fZ - vCoord.z
		PRINTLN("IsPointUnderBridge - diff ", fZ)
		IF (fZ > 6.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_POINT_ON_OR_NEAR_HIGHWAY(VECTOR vPoint, FLOAT fDist=20.0, FLOAT fZDiff=5.0)
	
	INT iDensity
	INT iProperties
	INT iNode
	VECTOR vNodeCoords

	REPEAT 3 iNode

		IF GET_NTH_CLOSEST_VEHICLE_NODE(vPoint, iNode, vNodeCoords)

			PRINTLN("IS_POINT_ON_OR_NEAR_HIGHWAY - vPoint = ", vPoint, ", iNode ", iNode, ", at ", vNodeCoords)

			IF (VDIST(vPoint, vNodeCoords) < fDist)
				
				FLOAT fZDelta = vPoint.z - vNodeCoords.z
				IF (ABSF(fZDelta) < fZDiff)
				
					GET_VEHICLE_NODE_PROPERTIES(vNodeCoords, iDensity, iProperties)
			
					IF NOT ((iProperties & ENUM_TO_INT(VNP_HIGHWAY)) = 0)
						PRINTLN("IS_POINT_ON_OR_NEAR_HIGHWAY - highway node - returning true. ")
						RETURN TRUE
					ELSE
						PRINTLN("IS_POINT_ON_OR_NEAR_HIGHWAY - not a highway node.")	
					ENDIF
				
				ELSE
					PRINTLN("IS_POINT_ON_OR_NEAR_HIGHWAY - fZDelta greater than ", fZDiff)	
				ENDIF
			ELSE
				PRINTLN("IS_POINT_ON_OR_NEAR_HIGHWAY - dist greater than ", fDist)	
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("IS_POINT_ON_OR_NEAR_HIGHWAY - false. ")
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_POINT_IN_OR_NEAR_TUNNEL(VECTOR vPoint, FLOAT fDist=20.0, FLOAT fZDiff=5.0)
	
	INT iDensity
	INT iProperties
	INT iNode
	VECTOR vNodeCoords

	REPEAT 3 iNode

		IF GET_NTH_CLOSEST_VEHICLE_NODE(vPoint, iNode, vNodeCoords)

			PRINTLN("IS_POINT_IN_OR_NEAR_TUNNEL - vPoint = ", vPoint, ", iNode ", iNode, ", at ", vNodeCoords)

			IF (VDIST(vPoint, vNodeCoords) < fDist)
				
				FLOAT fZDelta = vPoint.z - vNodeCoords.z
				IF (ABSF(fZDelta) < fZDiff)
				
					GET_VEHICLE_NODE_PROPERTIES(vNodeCoords, iDensity, iProperties)
			
					IF NOT ((iProperties & ENUM_TO_INT(VNP_TUNNEL_OR_INTERIOR)) = 0)
						PRINTLN("IS_POINT_IN_OR_NEAR_TUNNEL - tunnel node - returning true. ")
						RETURN TRUE
					ELSE
						PRINTLN("IS_POINT_IN_OR_NEAR_TUNNEL - not a tunnel node.")	
					ENDIF
				
				ELSE
					PRINTLN("IS_POINT_IN_OR_NEAR_TUNNEL - fZDelta greater than ", fZDiff)	
				ENDIF
			ELSE
				PRINTLN("IS_POINT_IN_OR_NEAR_TUNNEL - dist greater than ", fDist)	
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("IS_POINT_IN_OR_NEAR_TUNNEL - false. ")
	RETURN FALSE

ENDFUNC


FUNC VECTOR GetNodeSpawnPosition(VECTOR vNodeCoord, FLOAT &fNodeHeading, INT iNoOfLanes, BOOL bParkedPosition, VECTOR vFavourFacing, BOOL bIgnoreSwitchedOffNodes, BOOL bWaterNodesOnly, MODEL_NAMES CarModel, BOOL &bDoubleSided, BOOL bSameAsPreviousNode, BOOL bAllowReturnZero=TRUE, BOOL bIsForFlyingVehicle=FALSE, BOOL bAllowFlipInOncomingTraffic=FALSE)
	
	// if this is for a flying vehicle just return the node coords
	IF (bIsForFlyingVehicle)
		PRINTLN("GetNodeSpawnPosition - this is for a flying vehicle")
		// should we flip the heading?
		IF (VMAG(vFavourFacing) > 0.0)
			IF NOT IsVehicleNodeFacingPoint(vNodeCoord, fNodeHeading, vFavourFacing)
				PRINTLN("GetNodeSpawnPosition - flip heading for flying vehicle")
				fNodeHeading += 180.0
			ENDIF
		ENDIF
		PRINTLN("GetNodeSpawnPosition - returning for flying vehicle vNodeCoord ", vNodeCoord, ", fNodeHeading ", fNodeHeading)
		RETURN(vNodeCoord)
	ENDIF


   	CONST_FLOAT LANE_OFFSET 4.2 // was 4.5
	VECTOR vReturn
	INT iDensity
	INT iProperties
	VECTOR vEndNode
	INT iLanesSouth
	INT iLanesNorth
	FLOAT centralReservationWidth
	FLOAT offsetToKerb
	BOOL bNodeIsSouth
	BOOL bIsOneDirectional
	//VECTOR vMin, vMax
	FLOAT fWidth	
	
	GET_VEHICLE_NODE_PROPERTIES(vNodeCoord, iDensity, iProperties)
	PRINTLN("GetNodeSpawnPosition - iProperties = ", iProperties)

	// if this is a water node we don't need to do the extensive checks below
	IF  ((iProperties & ENUM_TO_INT(VNP_WATER)) = 0)
	AND NOT (bWaterNodesOnly)
		
		GET_CLOSEST_ROAD(vNodeCoord, 1, 1, vEndNode, vEndNode, iLanesSouth, iLanesNorth, centralReservationWidth, bIgnoreSwitchedOffNodes)											

		PRINTLN("GetNodeSpawnPosition - vNodeCoord = ", vNodeCoord) 
		PRINTLN("GetNodeSpawnPosition - vEndNode = ", vEndNode) 
		PRINTLN("GetNodeSpawnPosition - vEndNode = ", vEndNode) 
		PRINTLN("GetNodeSpawnPosition - iLanesSouth = ", iLanesSouth) 
		PRINTLN("GetNodeSpawnPosition - iLanesNorth = ", iLanesNorth) 
		PRINTLN("GetNodeSpawnPosition - iNoOfLanes = ", iNoOfLanes) 
		PRINTLN("GetNodeSpawnPosition - centralReservationWidth = ", centralReservationWidth) 
		
		// is this double sided
		IF (iLanesSouth = iLanesNorth)
			bDoubleSided = TRUE
			PRINTLN("GetNodeSpawnPosition - this is double sided.")
		ELSE
			PRINTLN("GetNodeSpawnPosition - this is NOT double sided.") 
		ENDIF		
		
		IF (bAllowReturnZero)
		
			IF NOT ((iProperties & ENUM_TO_INT(VNP_JUNCTION)) = 0)
				PRINTLN("GetNodeSpawnPosition - junction node - ignore this node  ") 
				RETURN(<<0.0, 0.0, 0.0>>)	
			ENDIF
			
			IF NOT ((iProperties & ENUM_TO_INT(VNP_TRAFFIC_LIGHT)) = 0)
				PRINTLN("GetNodeSpawnPosition - traffic light node - ignore this node  ")
				RETURN(<<0.0, 0.0, 0.0>>)	
			ENDIF	
			
			IF NOT ((iProperties & ENUM_TO_INT(VNP_GIVE_WAY)) = 0)
				PRINTLN("GetNodeSpawnPosition - give way node - ignore this node  ") 
				RETURN(<<0.0, 0.0, 0.0>>)	
			ENDIF
			
			IF ((iLanesSouth + iLanesNorth) != iNoOfLanes)
				PRINTLN("GetNodeSpawnPosition - south and north lanes don't equal total - ignore this node  ")  
				RETURN(<<0.0, 0.0, 0.0>>)	
			ENDIF
			
			
			// dont let heli's spawn under bridges
			IF IS_THIS_MODEL_A_HELI(CarModel)
			AND IsPointUnderBridge(vNodeCoord)
				PRINTLN("GetNodeSpawnPosition - point is under a bridge for heli - ignore this node  ")  
				RETURN(<<0.0, 0.0, 0.0>>)		
			ENDIF
			
		ENDIF
		
		// should we flip heading if this is the same as last node and it's double sided?
		IF (bSameAsPreviousNode) 
		AND (bDoubleSided)
			fNodeHeading += 180.0
			IF (fNodeHeading > 360.0)
				fNodeHeading += -360.0	
			ENDIF		
			PRINTLN("GetNodeSpawnPosition - same as last node and double sided, flipping heading.  ")
		ENDIF
		
								
		IF (fNodeHeading <= 90.0) OR (fNodeHeading > 270.0)
			bNodeIsSouth = TRUE
			PRINTLN("GetNodeSpawnPosition - road is facing south  ") 
		ELSE
			bNodeIsSouth  = FALSE
			PRINTLN("GetNodeSpawnPosition - road is facing north  ") 
		ENDIF
								
		bIsOneDirectional = FALSE
		IF (bNodeIsSouth)
			IF (iLanesSouth = 0)
				PRINTLN("GetNodeSpawnPosition - south road but no lanes south - ignore this node  ")  
				IF (bAllowReturnZero)
					RETURN(<<0.0, 0.0, 0.0>>)
				ENDIF
			ELSE
				IF (iNoOfLanes = iLanesSouth)
					bIsOneDirectional = TRUE
					PRINTLN("GetNodeSpawnPosition - south road is one directional  ")  
				ELSE
					PRINTLN("GetNodeSpawnPosition - south road is NOT one directional  ")  
				ENDIF
			ENDIF
		ELSE
			IF (iLanesNorth = 0)
				PRINTLN("GetNodeSpawnPosition - north road but no lanes north - ignore this node  ")  
				IF (bAllowReturnZero)
					RETURN(<<0.0, 0.0, 0.0>>)
				ENDIF
			ELSE
				IF (iNoOfLanes = iLanesNorth)
					bIsOneDirectional = TRUE
					PRINTLN("GetNodeSpawnPosition - north road is one directional  ")  
				ELSE
					PRINTLN("GetNodeSpawnPosition - north road is NOT one directional  ")  	
				ENDIF
			ENDIF
		ENDIF

	    IF centralReservationWidth < 0
	    	offsetToKerb = 0
	    ELSE

			IF (bNodeIsSouth)
				IF (bIsOneDirectional)
					offsetToKerb = (LANE_OFFSET*(TO_FLOAT(iLanesSouth)*0.5))  
				ELSE
	    			offsetToKerb = (LANE_OFFSET*(TO_FLOAT(iLanesSouth))) 
				ENDIF
				
				// additional offset for more than 1 lane
				IF (bIsOneDirectional) 
					IF (iLanesSouth > 2)
						offsetToKerb += ((iLanesSouth-2) * 1.0)
					ENDIF
				ELSE
					IF (iLanesSouth > 1)
						offsetToKerb += ((iLanesSouth-1) * 1.0)
					ENDIF
				ENDIF
				
				
			ELSE
				IF (bIsOneDirectional)
					offsetToKerb = (LANE_OFFSET*(TO_FLOAT(iLanesNorth)*0.5)) 
				ELSE
					offsetToKerb = (LANE_OFFSET*(TO_FLOAT(iLanesNorth)))  
				ENDIF
				
				// additional offset for more than 1 lane
				IF (bIsOneDirectional) 
					IF (iLanesNorth > 2)
						offsetToKerb += ((iLanesNorth-2) * 1.0)
					ENDIF
				ELSE
					IF (iLanesNorth > 1)
						offsetToKerb += ((iLanesNorth-1) * 1.0)
					ENDIF
				ENDIF
				
			ENDIF
			// if it is a highway node apply the central reservation
			IF NOT ((iProperties & ENUM_TO_INT(VNP_HIGHWAY)) = 0)				
				offsetToKerb += (0.95 * centralReservationWidth)
				PRINTLN("GetNodeSpawnPosition - applying central reservation, offsetToKerb=", offsetToKerb)  											
			ENDIF
			// if its on a non-big vehicle road reduce by 0.5
			IF NOT ((iProperties & ENUM_TO_INT(VNP_NO_BIG_VEHICLES)) = 0)
			OR NOT ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) = 0)				 
				offsetToKerb += -0.5	
				PRINTLN("GetNodeSpawnPosition - no big vehicles - reducing kerb offset by 0.5, offsetToKerb=", offsetToKerb) 								
			ENDIF	
			// if its a dead end (but active and not big vehicles) reduce by 0.5
			IF NOT ((iProperties & ENUM_TO_INT(VNP_LEADS_TO_DEAD_END)) = 0)
			AND ((iProperties & ENUM_TO_INT(VNP_NO_BIG_VEHICLES)) = 0)
			AND ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) = 0)				
				offsetToKerb += -1.0			
				PRINTLN("GetNodeSpawnPosition - dead end (but active and allows big vehicles) - reducing kerb offset by 1.0, offsetToKerb=", offsetToKerb)  
			ENDIF		
			// if dont want a safe offset move back onto road 
			IF NOT (bParkedPosition)
			OR NOT ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) = 0)				 
				offsetToKerb += (LANE_OFFSET * -0.5)
				PRINTLN("GetNodeSpawnPosition - move back on road, offsetToKerb=", offsetToKerb) 
			ENDIF
			
			// if this is for a particularly wide car model then move back towards the road.
			IF NOT (CarModel = DUMMY_MODEL_FOR_SCRIPT)
					
				IF ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) != 0)
					fWidth = GetCarModelWidth(CarModel, MIN_WIDTH_VEHICLE_DISABLED_NODES)
				ELSE
					fWidth = GetCarModelWidth(CarModel, MIN_WIDTH_VEHICLE_ACTIVE_NODES)
				ENDIF	
				PRINTLN("GetNodeSpawnPosition - fWidth = ", fWidth)
				IF (fWidth > 1.8)
					offsetToKerb += ((fWidth-1.8) * -0.5)
					PRINTLN("GetNodeSpawnPosition - wide vehicle,, offsetToKerb=", offsetToKerb)
				ENDIF
			ENDIF

	    ENDIF
		
	ELSE
		
		PRINTLN("GetNodeSpawnPosition - water node  ")  
		
	ENDIF
	
	IF (VMAG(vFavourFacing) > 0.0)
	
		
		PRINTLN("GetNodeSpawnPosition - vFavourFacing = ", vFavourFacing)
		PRINTLN("GetNodeSpawnPosition - bAllowFlipInOncomingTraffic = ", bAllowFlipInOncomingTraffic)

		IF NOT IsVehicleNodeFacingPoint(vNodeCoord, fNodeHeading, vFavourFacing)
		
			// only flip if parked, or a water node			
			IF ((bParkedPosition) OR (bAllowFlipInOncomingTraffic))
			OR ((((iProperties & ENUM_TO_INT(VNP_WATER)) != 0) OR (vNodeCoord.z = 0.0)) AND (bWaterNodesOnly)) // url:bugstar:3965421 - Submarine - Finale: Stack overflow after confronting the and interrogating the ped in the submarine brig
				PRINTLN("GetNodeSpawnPosition - IsVehicleNodeFacingPoint = FALSE")  
				fNodeHeading += 180.0
			ELSE
				PRINTLN("GetNodeSpawnPosition - can't flip this heading and not facing correct direction. ignore this node.") 
				IF (bAllowReturnZero)
					RETURN(<<0.0, 0.0, 0.0>>)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	PRINTLN("GetNodeSpawnPosition - offsetToKerb = ", offsetToKerb) 
	
	IF (offsetToKerb < 0.0)
		PRINTLN("GetNodeSpawnPosition - offsetToKerb < 0.0. resetting. ") 
		offsetToKerb = 0.0
	ENDIF

	vReturn =  GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vNodeCoord, fNodeHeading, <<offsetToKerb,0,0>>)
	
	PRINTLN("GetNodeSpawnPosition - returning ", vReturn)  

	VECTOR vExtremity
	VECTOR vToExtremity
	VECTOR vec
	

	IF (bParkedPosition)
		IF GET_ROAD_BOUNDARY_USING_HEADING(vNodeCoord, fNodeHeading, vExtremity ) 
			
			PRINTLN("GetNodeSpawnPosition - vExtremity ", vExtremity)  

			// need to subtract width of car
			vToExtremity = vExtremity - vNodeCoord
			
			// need to subtract width of car
			IF NOT (CarModel = DUMMY_MODEL_FOR_SCRIPT)
				vec = vToExtremity/VMAG(vToExtremity)
			
				IF ((iProperties & ENUM_TO_INT(VNP_SWITCHED_OFF)) != 0)
					fWidth = GetCarModelWidth(CarModel, MIN_WIDTH_VEHICLE_DISABLED_NODES)
				ELSE
					fWidth = GetCarModelWidth(CarModel, MIN_WIDTH_VEHICLE_ACTIVE_NODES)
				ENDIF				
				
				PRINTLN("GetNodeSpawnPosition - fWidth=", fWidth)
				vec *= (fWidth * 0.5)
				vToExtremity -= vec
				vExtremity = vNodeCoord + vToExtremity
			ENDIF		
			
			PRINTLN("GetNodeSpawnPosition - vExtremity minus car width ", vExtremity)  

			vec = vReturn - vExtremity
			//PRINTLN("GetNodeSpawnPosition - diff ", VMAG(vec))  	
			
			vReturn = vExtremity
			
		ENDIF
	ENDIF

	RETURN(vReturn)
	
ENDFUNC

FUNC BOOL IsNodeCoordInExclusionZone(VECTOR vStartCoords, VECTOR &vCoords, VECTOR &vAvoidCoords[], FLOAT &fAvoidRadius[], BOOL bMove=FALSE, BOOL bCheckServerRefusalList=TRUE)

	//PRINTLN("IsNodeCoordInExclusionZone - called with ", vStartCoords, vCoords)

	IF DO_GLOBAL_EXCLUSION_ZONES_APPLY(vStartCoords)
		//PRINTLN("IsNodeCoordInExclusionZone - DO_GLOBAL_EXCLUSION_ZONES_APPLY ")
		IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vCoords, bMove, DEFAULT, TRUE)
			IF (bMove)			
				PRINTLN("IsNodeCoordInExclusionZone - moved point out of global exclusion zone - vCoords ", vCoords)
			ELSE
				PRINTLN("IsNodeCoordInExclusionZone - return true global exclusion zone - vCoords ", vCoords)
			ENDIF
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	IF IS_POINT_IN_MISSION_EXCLUSION_ZONE(vCoords, bMove, TRUE)
		IF (bMove)
			PRINTLN("IsNodeCoordInExclusionZone - moved point out of mission exclusion zone - vCoords ", vCoords)
		ELSE
			PRINTLN("IsNodeCoordInExclusionZone - returned True mission exclusion zone - vCoords ", vCoords)
		ENDIF
		RETURN(TRUE)
	ENDIF

	IF (bCheckServerRefusalList)
		IF IS_POINT_IN_SERVER_REFUSAL_LIST(vCoordS)
			PRINTLN("IsNodeCoordInExclusionZone - returned True IS_POINT_IN_SERVER_REFUSAL_LIST - vCoords ", vCoords)
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	INT i
	BOOL bIsInAvoidCoord = FALSE
	REPEAT COUNT_OF(vAvoidCoords) i
		IF (VDIST(vCoords, vAvoidCoords[i]) < fAvoidRadius[i])
			PRINTLN("IsNodeCoordInExclusionZone - point is inside avoid radius ", i, " ", vCoords)
			IF (bMove)
				MovePointOutsideSphere(vCoords, vAvoidCoords[i], fAvoidRadius[i])
				PRINTLN("IsNodeCoordInExclusionZone - moved point outside avoid radius - vCoords ", vCoords)
			ENDIF		
			bIsInAvoidCoord = TRUE
		ENDIF
	ENDREPEAT
	IF (bIsInAvoidCoord)
		RETURN(TRUE)
	ENDIF

	RETURN(FALSE)
ENDFUNC



FUNC BOOL IsPointInsideNoPVSpawnZone(VECTOR &vPoint, BOOL bMove=FALSE)
	
	BOOL bReturn = FALSE
	
	IF (g_SpawnData.MissionSpawnPersonalVehicleData.bPersonalVehicleNoSpawnZoneEnabled)		
		
		SWITCH g_SpawnData.MissionSpawnPersonalVehicleData.iPersonalVehicleNoSpawnShape
			CASE SPAWN_AREA_SHAPE_CIRCLE
				IF IsPointInsideSphere(vPoint, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min, g_SpawnData.MissionSpawnPersonalVehicleData.fPersonalVehicleNoSpawnFloat, FALSE, FALSE)
					bReturn = TRUE
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX
				IF IsPointInsideBox(vPoint, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Max, FALSE, FALSE)
					bReturn = TRUE
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				IF IS_POINT_IN_ANGLED_AREA(vPoint, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Max, g_SpawnData.MissionSpawnPersonalVehicleData.fPersonalVehicleNoSpawnFloat)
					bReturn = TRUE
				ENDIF
			BREAK
		ENDSWITCH
		
		IF (bReturn)
			IF (bMove)
				vPoint = GetNearestCarNodeNotInsideZone(vPoint, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min, g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Max, g_SpawnData.MissionSpawnPersonalVehicleData.fPersonalVehicleNoSpawnFloat, g_SpawnData.MissionSpawnPersonalVehicleData.iPersonalVehicleNoSpawnShape)
				PRINTLN("[personal_vehicle][spawning] IsPointInsideNoPVSpawnZone - moved to ", vPoint)
			ENDIF	
		ENDIF
		
	ENDIF
	
	RETURN(bReturn)
ENDFUNC


FUNC BOOL IsPointInsideCheckingArea(VECTOR vPoint, NearestCarNodeSearch &SearchParams)
	IF (SearchParams.bCheckInsideArea)
		
		SWITCH SearchParams.iAreaShape
			CASE SPAWN_AREA_SHAPE_CIRCLE			
				IF IsPointInsideSphere(vPoint, SearchParams.vAreaCoords1, SearchParams.fAreaFloat, FALSE, FALSE)
					RETURN(TRUE)
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX
				IF IsPointInsideBox(vPoint, SearchParams.vAreaCoords1, SearchParams.vAreaCoords2, FALSE, FALSE)
					RETURN(TRUE)
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED	
				IF IS_POINT_IN_ANGLED_AREA(vPoint, SearchParams.vAreaCoords1, SearchParams.vAreaCoords2, SearchParams.fAreaFloat)
					RETURN(TRUE)
				ENDIF			
			BREAK		
		ENDSWITCH
		
//		IF NOT (SearchParams.bAreaIsAngled)
//			IF (VDIST(vPoint, SearchParams.vAreaCoords1) <= SearchParams.fAreaFloat)
//				RETURN(TRUE)
//			ENDIF
//		ELSE
//			IF IS_POINT_IN_ANGLED_AREA(vPoint, SearchParams.vAreaCoords1, SearchParams.vAreaCoords2, SearchParams.fAreaFloat)
//				RETURN(TRUE)
//			ENDIF
//		ENDIF
		PRINTLN("IsPointInsideCheckingArea - returning FALSE for point ", vPoint)
		RETURN(FALSE)
	ENDIF
	RETURN(TRUE)
ENDFUNC

PROC ShiftNearestCarNodeResultsUp(INT iIndex)
	WHILE iIndex < MAX_VEHICLE_NODES_CHECKED-1
		IF (iIndex < MAX_VEHICLE_NODES_CHECKED-1)
			g_NearestCarNodeResults.vCoords[iIndex] = g_NearestCarNodeResults.vCoords[iIndex+1]
			g_NearestCarNodeResults.fHeading[iIndex] = g_NearestCarNodeResults.fHeading[iIndex+1]
		ENDIF
		iIndex++
	ENDWHILE
	PRINTLN("ShiftNearestCarNodeResultsUp - all shifted up")
ENDPROC

PROC RemoveNearestCarNodeNonAreaResults(INT iIndex, NearestCarNodeSearch &SearchParams)
	PRINTLN("RemoveNearestCarNodeNonAreaResults - called with index ", iIndex)
	IF NOT IsPointInsideCheckingArea(g_NearestCarNodeResults.vCoords[iIndex], SearchParams)	
		g_NearestCarNodeResults.iNumResults--
		ShiftNearestCarNodeResultsUp(iIndex)
		IF (g_NearestCarNodeResults.iNumResults > g_NearestCarNodeResults.iNumResultsInsideArea)
			RemoveNearestCarNodeNonAreaResults(	iIndex, SearchParams)
		ENDIF
	ELSE
		IF (iIndex < MAX_VEHICLE_NODES_CHECKED-1)
			RemoveNearestCarNodeNonAreaResults(	iIndex+1, SearchParams)
		ENDIF	
	ENDIF
	PRINTLN("RemoveNearestCarNodeNonAreaResults - finished ")
ENDPROC


//FUNC FLOAT GET_MIN_RADIUS_FOR_VEHICLE_MODEL(MODEL_NAMES CarModel, FLOAT fAdditionalDist = 0.1)
//
//	IF (CarModel = DUMMY_MODEL_FOR_SCRIPT)
//		RETURN(5.0) // default value for no model
//	ENDIF
//	
//	VECTOR vMin, vMax
//	VECTOR vDiff
//	FLOAT fMaxDiff
//	
//	SAFE_GET_MODEL_DIMENSIONS(CarModel, vMin, vMax)
//	vDiff = vMax - vMin
//
//	IF (ABSF(vDiff.x) > fMaxDiff)
//		fMaxDiff = ABSF(vDiff.x)
//	ENDIF
//	IF (ABSF(vDiff.y) > fMaxDiff)
//		fMaxDiff = ABSF(vDiff.y)
//	ENDIF
//	IF (ABSF(vDiff.z) > fMaxDiff)
//		fMaxDiff = ABSF(vDiff.z)
//	ENDIF
//	
//	RETURN ((fMaxDiff*0.5) + fAdditionalDist)
//	
//ENDFUNC


PROC InsertCarNodeResultIntoArray(VECTOR vCoord, FLOAT fHeading, INT iSlot)
	VECTOR vStoredCoords
	FLOAT fStoredHeading
	vStoredCoords = g_NearestCarNodeResults.vCoords[iSlot]
	fStoredHeading = g_NearestCarNodeResults.fHeading[iSlot]
	
	g_NearestCarNodeResults.vCoords[iSlot] = vCoord
	g_NearestCarNodeResults.fHeading[iSlot] = fHeading
	
	PRINTLN("InsertCarNodeResultIntoArray - called with point ", vCoord, fHeading, " inserting into slot ", iSlot) 
	
	IF (iSlot <= g_NearestCarNodeResults.iNumResults)
	AND (iSlot < (MAX_VEHICLE_NODES_CHECKED - 1))
		IF (VMAG(vStoredCoords) > 0.0)
			InsertCarNodeResultIntoArray(vStoredCoords, fStoredHeading, iSlot+1)
		ENDIF
	ENDIF
	
ENDPROC

PROC GetRandomCarNode(INT iNumNodesFailedDist, VECTOR vCoords, INT &iNodeNumber, VECTOR &vNodeCoord, FLOAT &fNodeHeading, NearestCarNodeSearch &SearchParams, BOOL bIgnoreSwitchedOffNodes, INT iNoOfLanes, NODE_FLAGS NodeFlag, FLOAT zMeasureMult, FLOAT zTolerance, BOOL bDoubleSided)
	
	INT iAttempt = 0

	WHILE iAttempt < 30
		iNodeNumber = GET_RANDOM_INT_IN_RANGE(1+iNumNodesFailedDist, MAX_VEHICLE_NODES_CHECKED+iNumNodesFailedDist)
		PRINTLN("GetRandomCarNode - fallbacks failed, choosing random node ", iNodeNumber)	
		IF GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vCoords, iNodeNumber, vNodeCoord, fNodeHeading, iNoOfLanes, NodeFlag, zMeasureMult, zTolerance)
			IF (VMAG(vNodeCoord) > 0.0)
				vNodeCoord = GetNodeSpawnPosition(vNodeCoord, fNodeHeading, iNoOfLanes, SearchParams.bGetSafeOffset, SearchParams.vFavourFacing, bIgnoreSwitchedOffNodes, SearchParams.bWaterNodesOnly, SearchParams.CarModel, bDoubleSided, FALSE, FALSE, SearchParams.bIsForFlyingVehicle, SearchParams.bCanFaceOncomingTraffic)
				IF NOT IS_POINT_IN_PROBLEM_NODE_AREA(vNodeCoord)		
					iAttempt = 999
					PRINTLN("GetRandomCarNode - vNodeCoord", vNodeCoord, " NOT in problem node area")
					EXIT
				ELSE
					PRINTLN("GetRandomCarNode - vNodeCoord", vNodeCoord, " in problem node area")
				ENDIF
			ELSE
				PRINTLN("GetRandomCarNode - vNodeCoord is zero")	
			ENDIF
		ELSE
			PRINTLN("GetRandomCarNode - GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING = FALSE")		
		ENDIF
		iAttempt++
	ENDWHILE
					
ENDPROC

FUNC BOOL GetNearestCarMapNode(VECTOR &vCoords, FLOAT &fHeading, NearestCarNodeSearch &SearchParams ) //, VECTOR vFavourFacing, BOOL bDoSafeForSpawnCheck=TRUE, BOOL bGetSafeOffset=FALSE, FLOAT fMinPlayerDist=0.0, BOOL bConsiderOnlyActiveNodes=TRUE, BOOL bWaterNodesOnly=FALSE, FLOAT fMinDistFromCoords=0.0, BOOL bCheckVehicleSpawning=FALSE, BOOL bConsiderHighways=TRUE, BOOL bConsiderTunnels=FALSE, INT iFallbackLevel=0, FLOAT fMaxDistance=0.0, BOOL bCheckSpawnExclusionZones=FALSE)

	#IF IS_DEBUG_BUILD
		IF VMAG(vCoords) = 0.0
			SCRIPT_ASSERT("GetNearestCarMapNode - origin passed in")
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
		
		PRINTLN("called GetNearestCarMapNode(", vCoords, ", ", fHeading)
		
		Print_NearestCarNodeSearch(SearchParams)
		
		PRINTLN("GetNearestCarMapNode - g_SpawnData.vExclusionCoord = ", g_SpawnData.vExclusionCoord)
		PRINTLN("GetNearestCarMapNode - g_SpawnData.fNearExclusionRadius = ", g_SpawnData.fNearExclusionRadius)

	#ENDIF

	//INT iNodeAddress
	INT iNodeNumber = 0
	VECTOR vNodeCoord
	FLOAT fNodeHeading
	NODE_FLAGS NodeFlag
	INT iDensity
	INT iProperties
	VEHICLE_NODE_ID NodeID
	INT iNoOfLanes
	BOOL bIgnoreSwitchedOffNodes
	//FLOAT fMinVehicleModelRadius = GET_MIN_RADIUS_FOR_VEHICLE_MODEL(SearchParams.CarModel)
	BOOL bDoubleSided
	BOOL bSameAsPreviousNode

	IF NOT (VMAG(SearchParams.vStartCoords) > 0.0)
		SearchParams.vStartCoords = vCoords
	ENDIF
	
	// if starting point is inside an exclusion zone then move it out.
	IF (SearchParams.bCheckSpawnExclusionZones)
		IF IsNodeCoordInExclusionZone(SearchParams.vStartCoords, vCoords, SearchParams.vAvoidCoords, SearchParams.fAvoidRadius, TRUE)
			PRINTLN("GetNearestCarMapNode - moved point out of exclusion zone - vCoords ", vCoords)

			// since we moved the initial point we need to ignore the upper z
			SearchParams.fUpperZLimit = 9999.9
			SearchParams.fLowerZLimit = 9999.9
			PRINTLN("GetNearestCarMapNode setting SearchParams.fUpperZLimit = ", SearchParams.fUpperZLimit)
		ENDIF
	ENDIF
	
	// if this is for a flying vehicle then ignore the upper z
	IF (SearchParams.bIsForFlyingVehicle)
		SearchParams.fUpperZLimit = 9999.9
		PRINTLN("GetNearestCarMapNode, for flying vehicle setting SearchParams.fUpperZLimit = ", SearchParams.fUpperZLimit)
	ENDIF
	
	// if starting point is inside pv no spawn zone then move it out
	IF (SearchParams.bCheckPVNoSpawnZone) 
		IF IsPointInsideNoPVSpawnZone(vCoords, TRUE)
			PRINTLN("GetNearestCarMapNode - moved point out of pv no spawn zone - vCoords ", vCoords)
		ENDIF
	ENDIF
	
	// if search point is below -100 ignor upperzlimit
	IF (vCoords.z < -80.0)
		SearchParams.fUpperZLimit = 9999.9
		SearchParams.fLowerZLimit = 9999.9
		PRINTLN("GetNearestCarMapNode z is < -80, ignoring fUpperZLimit = ", SearchParams.fUpperZLimit)
	ENDIF
	
	NodeFlag = NF_NONE
	bIgnoreSwitchedOffNodes = TRUE
	
	IF (SearchParams.bWaterNodesOnly)
		NodeFlag += NF_INCLUDE_BOAT_NODES
		NodeFlag += NF_INCLUDE_SWITCHED_OFF_NODES
		bIgnoreSwitchedOffNodes = FALSE
		PRINTLN("GetNearestCarMapNode - to consider boat nodes only")
	ELSE
	
		IF (SearchParams.bConsiderOnlyActiveNodes = FALSE) 
		OR (SearchParams.iFallbackLevel > 0)
		AND (SearchParams.bAllowFallbackToInactiveNodes)
			NodeFlag += NF_INCLUDE_SWITCHED_OFF_NODES
			bIgnoreSwitchedOffNodes = FALSE
			PRINTLN("GetNearestCarMapNode - NodeFlag = NF_INCLUDE_SWITCHED_OFF_NODES")
		ENDIF		
		
	ENDIF

	// ignore slip lanes
	NodeFlag += NF_IGNORE_SLIPLANES	


	PRINTLN("GetNearestCarMapNode - NodeFlag = ", ENUM_TO_INT(NodeFlag))

	FLOAT zMeasureMult = 3.0
	FLOAT zTolerance = 5.0
	
	SWITCH SearchParams.iFallbackLevel
		CASE 0
			zMeasureMult = 3.0
			zTolerance = 5.0
		BREAK
		CASE 1
			zMeasureMult = 2.75
			zTolerance = 7.5
		BREAK
		DEFAULT
			zMeasureMult = 2.5
			zTolerance = 10.0
		BREAK
	ENDSWITCH

	INT iNumNodesFailedDist = 0

	
	g_NearestCarNodeResults.iNumResults = 0
	g_NearestCarNodeResults.iNumResultsInsideArea = 0
	
	g_NearestCarNodeResults.iLastVehicleNode = -99
	g_NearestCarNodeResults.vLastVehicleNodePos = <<0.0, 0.0, 0.0>>
	
	// make sure any previous results are cleared
	INT i 
	REPEAT MAX_VEHICLE_NODES_CHECKED i
		g_NearestCarNodeResults.vCoords[i] = <<0.0, 0.0, 0.0>>
		g_NearestCarNodeResults.fHeading[i] = 0.0
	ENDREPEAT

	INT iNodeMultiplier = 1

	// if this is for the moc, then we want to grab further apart nodes. 
	IF (GET_COMBINED_MODEL_ID_FROM_MODEL_NAME(SearchParams.CarModel) != CM_NULL)
		iNodeMultiplier = 3
		SearchParams.fUpperZLimit = 9999.9
		SearchParams.fLowerZLimit = 9999.9		
		SearchParams.bCheckInsideArea = FALSE
	ENDIF
		
	WHILE TRUE


		//IF (GET_NTH_CLOSEST_VEHICLE_NODE_WITH_HEADING(vCoords, iNodeNumber, vNodeCoord, fNodeHeading, iNoOfLanes, NodeFlag, zMeasureMult, zTolerance)) 
		
		NodeID  = GET_NTH_CLOSEST_VEHICLE_NODE_ID_WITH_HEADING(vCoords, (iNodeNumber * iNodeMultiplier), fNodeHeading, iNoOfLanes, NodeFlag, zMeasureMult, zTolerance )
		PRINTLN("GetNearestCarMapNode - to considering iNodeNumber ", iNodeNumber)
					
		IF IS_VEHICLE_NODE_ID_VALID(NodeID)
			GET_VEHICLE_NODE_POSITION(NodeID, vNodeCoord )
			
			// is this the same node as previous one?
			bSameAsPreviousNode = FALSE
			IF (g_NearestCarNodeResults.iLastVehicleNode = NATIVE_TO_INT(NodeID))		
				bSameAsPreviousNode = TRUE
				PRINTLN("GetNearestCarMapNode - setting bSameAsPreviousNode = TRUE")
			ENDIF
			g_NearestCarNodeResults.vLastVehicleNodePos = vNodeCoord
			
			PRINTLN("GetNearestCarMapNode - to considering Node ", iNodeNumber, " at coords ", vNodeCoord, " from starting coords ", vCoords, " fNodeHeading = ", fNodeHeading)
									
			//NodeID = GET_NTH_CLOSEST_VEHICLE_NODE_ID(vNodeCoord, 1, NodeFlag)
			
			PRINTLN("GetNearestCarMapNode - NodeID = ", NATIVE_TO_INT(NodeID))
			
			
			IF ((SearchParams.bConsiderOnlyActiveNodes) OR (SearchParams.iFallbackLevel > 0))
			OR NOT GET_VEHICLE_NODE_IS_SWITCHED_OFF(NodeID) 
			OR GET_VEHICLE_NODE_IS_GPS_ALLOWED(NodeID)
			
				GET_VEHICLE_NODE_PROPERTIES(vNodeCoord, iDensity, iProperties)
				
				PRINTLN("GetNearestCarMapNode - iProperties = ", iProperties)								
				
				// check point is more than the min dist away
				//IF (VDIST(vNodeCoord, vCoords) > SearchParams.fMinDistFromCoords)
				IF (VDIST(vNodeCoord, SearchParams.vStartCoords) > SearchParams.fMinDistFromCoords) // check against original passed in coords. fix for 2807565
				
					IF NOT IS_POINT_IN_NEAR_EXCLUSION_RADIUS(vNodeCoord)
				
						IF (SearchParams.bConsiderHighways)
						OR ((iProperties & ENUM_TO_INT(VNP_HIGHWAY)) = 0)
						OR (SearchParams.iFallbackLevel = 1)
					
							IF (SearchParams.bConsiderTunnels)
							OR ((iProperties & ENUM_TO_INT(VNP_TUNNEL_OR_INTERIOR)) = 0)
							//OR (SearchParams.iFallbackLevel = 1) // never consider tunnels unless specifically asked as they can be in inaccessible places. 2036571
								
								IF ((iProperties & ENUM_TO_INT(VNP_JUNCTION)) = 0)
								AND ((iProperties & ENUM_TO_INT(VNP_TRAFFIC_LIGHT)) = 0)
								AND ((iProperties & ENUM_TO_INT(VNP_GIVE_WAY)) = 0)
								
									// ignore problem nodes
									IF NOT IS_POINT_IN_PROBLEM_NODE_AREA(vNodeCoord)	
																		
										// get the spawn position for this node
										vNodeCoord = GetNodeSpawnPosition(vNodeCoord, fNodeHeading, iNoOfLanes, SearchParams.bGetSafeOffset, SearchParams.vFavourFacing, bIgnoreSwitchedOffNodes, SearchParams.bWaterNodesOnly, SearchParams.CarModel, bDoubleSided, bSameAsPreviousNode, DEFAULT, SearchParams.bIsForFlyingVehicle, SearchParams.bCanFaceOncomingTraffic)
										IF (VMAG(vNodeCoord) > 0.0)
																																
											// ignore where fmmc props have been created
											IF NOT IsAnyFMMCPropAtCoord(vNodeCoord, 5.0)
												
												IF (vNodeCoord.z >= SearchParams.vStartCoords.z - SearchParams.fLowerZLimit)
												OR (SearchParams.iFallbackLevel >= 2)			
																
													IF (vNodeCoord.z <= SearchParams.vStartCoords.z + SearchParams.fUpperZLimit)
													OR (SearchParams.iFallbackLevel >= 2)		
																
														// ignore points outside of the checking area (if one is setup)
														IF IsPointInsideCheckingArea(vNodeCoord, SearchParams)
														//OR (SearchParams.iFallbackLevel >= 2) // don't allow fallback outside of checking area - 2778498
													
															
															// if this is for a personal vehicle check it's not inside a forbidden area
															IF ((SearchParams.bCheckPVNoSpawnZone) AND NOT IsPointInsideNoPVSpawnZone(vNodeCoord))
															OR (SearchParams.bCheckPVNoSpawnZone = FALSE)
																																								
																BOOL bIncrementNodes = TRUE
																
																// is this a valid double sided node?
																IF NOT (bSameAsPreviousNode)
																	IF (bDoubleSided)
																		iNodeNumber += -1
																		PRINTLN("GetNearestCarMapNode - double sided, reducing iNodeNumber.")	
																		
																		// Fix for B*3973274
																		bIncrementNodes = FALSE
																		PRINTLN("GetNearestCarMapNode - bIncrementNodes = TRUE")
																	ENDIF
																ENDIF
															
																// check point is valid
																IF VMAG(vNodeCoord) > 0.0
																												
																	IF (SearchParams.fMaxDistance > 0.0) AND( VDIST(<<vNodeCoord.x, vNodeCoord.y, 0.0>>, <<SearchParams.vStartCoords.x, SearchParams.vStartCoords.y, 0.0>>) < SearchParams.fMaxDistance)
																	OR (SearchParams.fMaxDistance <= 0.0)
																	OR (SearchParams.iFallbackLevel >= 2)
																			
																														
																			
																		//IF ((SearchParams.bCheckVehicleSpawning) AND NOT (IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(vNodeCoord, 5.0, fMinVehicleModelRadius, PLAYER_ID(), FALSE, TRUE)))
																		IF ((SearchParams.bCheckVehicleSpawning) AND NOT (IsAnotherPlayerOrPlayerVehicleSpawningInCarBounds(vNodeCoord, fNodeHeading, SearchParams.CarModel, PLAYER_ID(), FALSE, SearchParams.bCheckThisPlayerVehicle)))
																		OR NOT (SearchParams.bCheckVehicleSpawning)
																				
																			IF NOT (SearchParams.bCheckSpawnExclusionZones)
																			OR NOT IsNodeCoordInExclusionZone(SearchParams.vStartCoords, vNodeCoord, SearchParams.vAvoidCoords, SearchParams.fAvoidRadius)

																				IF (SearchParams.bDoSafeForSpawnCheck)
																				
																					BOOL bCheckThisPlayerSight = SearchParams.bCheckOwnVisibility
																					BOOL bDoAnyPlayerSeePointCheck = TRUE
																					BOOL bCheckVisibilityForOwnTeam = TRUE
																					FLOAT fVisibleDistance = SearchParams.fVisibleDistance														
																					
																					IF NOT (SearchParams.bDoVisibilityChecks)
																						bCheckThisPlayerSight = FALSE
																						bDoAnyPlayerSeePointCheck = FALSE
																						bCheckVisibilityForOwnTeam = FALSE
																						fVisibleDistance = 1.0
																						
																					ELSE
																					
																						IF (SearchParams.bStartOfMissionPVSpawn)
																							
																							bCheckThisPlayerSight = FALSE
																							bDoAnyPlayerSeePointCheck = FALSE
																							bCheckVisibilityForOwnTeam = FALSE
																							
																							IF (SearchParams.iFallbackLevel = 1)		
																								fVisibleDistance = fVisibleDistance * 0.375
																							ENDIF
																							
																						ELSE
																						
																							bDoAnyPlayerSeePointCheck = TRUE
																							bCheckVisibilityForOwnTeam = TRUE
																							
																							IF (SearchParams.bAllowReducingVisibility)
																								IF (SearchParams.iFallbackLevel = 1)		
																									fVisibleDistance = fVisibleDistance * 0.375
																								ENDIF		
																							ENDIF
																						
																						ENDIF
																						
																					ENDIF
																					
																					BOOL bPointOkForCreation = FALSE
																					PRINTLN("GetNearestCarMapNode - SearchParams.fMinPlayerDist = ", SearchParams.fMinPlayerDist)
																					IF NOT DOES_NEAREST_VEHICLE_COLLIDE_WITH_VEHICLE_BOUNDS(vNodeCoord, fNodeHeading, SearchParams.CarModel)
																						IF (SearchParams.fMinPlayerDist > 7.0)
																							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNodeCoord,6,1,1,5,bCheckThisPlayerSight,bDoAnyPlayerSeePointCheck,bCheckVisibilityForOwnTeam,fVisibleDistance,SearchParams.bIgnoreLocalPlayerChecks,-1,TRUE,SearchParams.fMinPlayerDist)
																								bPointOkForCreation = TRUE																					
																							ENDIF
																						ELSE
																							IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNodeCoord,6,1,1,5,bCheckThisPlayerSight,bDoAnyPlayerSeePointCheck,bCheckVisibilityForOwnTeam,fVisibleDistance,SearchParams.bIgnoreLocalPlayerChecks,-1,TRUE,0.0)
																							AND NOT IS_ANY_PLAYER_IN_VEHICLE_BOUNDS(vNodeCoord, fNodeHeading, SearchParams.CarModel)
																								bPointOkForCreation = TRUE	
																							ENDIF
																						ENDIf
																					ELSE
																						PRINTLN("GetNearestCarMapNode - point collides with nearest vehicle")
																					ENDIF
																				
																					IF ((bPointOkForCreation)
																					OR (SearchParams.iFallbackLevel >= 2))
																					
																					#IF IS_DEBUG_BUILD
																					AND NOT (g_SpawnData.bForceNodeSearchFail)
																					#ENDIF
																						
																						IF (SearchParams.bReturnRandomGoodNode)
																						OR (SearchParams.bReturnNearestGoodNode)
																						OR (SearchParams.bCheckForLowestEnemiesNearPoint)
																						OR (SearchParams.iFallbackLevel >= 2) // if it's the final fallback choose a random point to try and avoid cars on top of one another. 
																							
																							INT iEnemiesNearPoint
																							FLOAT fEnemyDist = 0.0
																							
																							IF (SearchParams.bCheckForLowestEnemiesNearPoint)
																								iEnemiesNearPoint = NumberEnemiesNearPoint(vNodeCoord, SearchParams.fCheckForLowestnemiesMaxDist, fEnemyDist)
																							ENDIF
																							
																							IF NOT (SearchParams.bCheckForLowestEnemiesNearPoint)
																							OR ((SearchParams.bCheckForLowestEnemiesNearPoint) AND (iEnemiesNearPoint <= SearchParams.iLowestNumEnemiesNearPoint))
																							
																								// should we clear previously stored results?
																								IF (SearchParams.bCheckForLowestEnemiesNearPoint)
																									IF (iEnemiesNearPoint < SearchParams.iLowestNumEnemiesNearPoint)
																									
																										PRINTLN("GetNearestCarMapNode - new lowest number of enemies, clearing previous results, iEnemiesNearPoint = ", iEnemiesNearPoint)
																										REPEAT g_NearestCarNodeResults.iNumResults i
																											g_NearestCarNodeResults.vCoords[i] = <<0.0, 0.0, 0.0>>
																											g_NearestCarNodeResults.fHeading[i] = 0.0			
																										ENDREPEAT
																										g_NearestCarNodeResults.iNumResults = 0
																										
																										SearchParams.iLowestNumEnemiesNearPoint = iEnemiesNearPoint
																									ENDIF
																								ENDIF
																							
																							
																								#IF IS_DEBUG_BUILD
																									IF NOT (SearchParams.bReturnRandomGoodNode)
																									AND NOT (SearchParams.bReturnNearestGoodNode)
																									AND (SearchParams.iFallbackLevel >= 2)
																										PRINTLN("GetNearestCarMapNode - final fallback, counting random nodes. ")
																									ENDIF
																								#ENDIF
																								
																								PRINTLN("GetNearestCarMapNode - found good vehicle node! adding to list as point ", g_NearestCarNodeResults.iNumResults) 
																								PRINTLN(", ", vNodeCoord)
																								PRINTLN(", ", fNodeHeading)
																								
																							
																								
																								IF (SearchParams.bReturnNearestGoodNode)
																						
																									IF (g_NearestCarNodeResults.iNumResults = 0)
																										g_NearestCarNodeResults.vCoords[0] = vNodeCoord
																										g_NearestCarNodeResults.fHeading[0] = fNodeHeading						
																									ELSE 																							
																										REPEAT (g_NearestCarNodeResults.iNumResults+1) i
																											IF (i < MAX_VEHICLE_NODES_CHECKED)
																												IF (VDIST2(vNodeCoord, SearchParams.vStartCoords) < VDIST2(g_NearestCarNodeResults.vCoords[i], SearchParams.vStartCoords))
																													InsertCarNodeResultIntoArray(vNodeCoord, fNodeHeading, i)
																													i = g_NearestCarNodeResults.iNumResults+1
																												ENDIF
																											ENDIF
																										ENDREPEAT
																									ENDIF
																									
																									g_NearestCarNodeResults.iNumResults++
																									
																									IF (g_NearestCarNodeResults.iNumResults >= NUM_OF_STORED_SPAWN_RESULTS)
																										
																										IF ((SearchParams.bCheckForLowestEnemiesNearPoint) AND (SearchParams.iLowestNumEnemiesNearPoint = 0))
																										OR (SearchParams.bCheckForLowestEnemiesNearPoint = FALSE)
																										
																											iNodeNumber = (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES)
																											PRINTLN("GetNearestCarMapNode - got enough good results from nearest nodes! ")
																											
																										ELSE
																											IF (g_NearestCarNodeResults.iNumResults = MAX_VEHICLE_NODES_CHECKED)	
																												iNodeNumber = (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES)
																												PRINTLN("GetNearestCarMapNode - stored max number of valid nodes ")
																											ENDIF
																										ENDIF
																									ENDIF
																									
																								ELSE
																								
																									g_NearestCarNodeResults.vCoords[g_NearestCarNodeResults.iNumResults] = vNodeCoord
																									g_NearestCarNodeResults.fHeading[g_NearestCarNodeResults.iNumResults] = fNodeHeading
																									g_NearestCarNodeResults.iNumResults++
																									
																									IF IsPointInsideCheckingArea(vNodeCoord, SearchParams)																			
																										g_NearestCarNodeResults.iNumResultsInsideArea++
																										PRINTLN("GetNearestCarMapNode - good point is inside checking area. iNumResultsInsideArea = ", g_NearestCarNodeResults.iNumResultsInsideArea) 
																									ENDIF
																									
																									// if we have enough decent results then skip to the end (optimisation)
																									IF (g_NearestCarNodeResults.iNumResults >= 10)
																									
																										IF ((SearchParams.bCheckForLowestEnemiesNearPoint) AND (SearchParams.iLowestNumEnemiesNearPoint = 0))
																										OR (SearchParams.bCheckForLowestEnemiesNearPoint = FALSE)
																									
																											iNodeNumber = (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES)
																											PRINTLN("GetNearestCarMapNode - got enough good results! ")
																											
																										ELSE
																											IF (g_NearestCarNodeResults.iNumResults = MAX_VEHICLE_NODES_CHECKED)	
																												iNodeNumber = (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES)
																												PRINTLN("GetNearestCarMapNode - stored max number of valid nodes (non nearest) ")
																											ENDIF
																										ENDIF
																									ENDIF
																								
																								ENDIF
																								
																							ELSE
																								PRINTLN("GetNearestCarMapNode - too many enemies near point, fail. iEnemiesNearPoint = ", iEnemiesNearPoint)
																							ENDIF
																							
																						ELSE						
																							vCoords = vNodeCoord
																							fHeading = fNodeHeading
																							PRINTLN("GetNearestCarMapNode - found good vehicle node! vNodeCoord = ", vNodeCoord)
																							//EXIT					
																							RETURN TRUE
																						ENDIF
																						
																					ELSE
																						// Fix for B*3973274
																						IF bIncrementNodes
																							PRINTLN("GetNearestCarMapNode - final fail - (skipping next node) bStartOfMissionPVSpawn = ", SearchParams.bStartOfMissionPVSpawn)																	
																							
																							// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
																							iNodeNumber++
																						ENDIF
																					ENDIF
																				
																				ELSE
																					vCoords = vNodeCoord
																					fHeading = fNodeHeading
																					PRINTLN("GetNearestCarMapNode - found a good node!")
																					//EXIT 
																					RETURN TRUE
																				ENDIF

																				
																			ELSE
																				PRINTLN("GetNearestCarMapNode - bCheckSpawnExclusionZones - fail ")  								
																			ENDIF									
																		ELSE
																			iNumNodesFailedDist++ // because in large games with all player spawning in same area, we can run out of nodes pretty fast.
																			PRINTLN("GetNearestCarMapNode - bCheckVehicleSpawning - fail - iNumNodesFailedDist =  ", iNumNodesFailedDist)  														 
																		ENDIF

																		
																	ELSE
																		PRINTLN("GetNearestCarMapNode - not within fMaxDistance - fail ")  
																		// assume that the rest of the nodes are also going to be outside the checking area, so quit out to save processing.
																		iNodeNumber = (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES)
																	ENDIF	

																ELSE
																	PRINTLN("GetNearestCarMapNode - node is 0.0 ")  	
																ENDIF
																			
															ELSE
															
																PRINTLN("GetNearestCarMapNode - is inside personal vehicle no spawn zone - fail. ")  
																// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
																iNodeNumber++
															
															ENDIF
																																			
														ELSE
															IF NOT (SearchParams.bUsingOffsetOrigin)
																PRINTLN("GetNearestCarMapNode - node is outside checking area, setting iNodeNumber to max ")  
																// assume that the rest of the nodes are also going to be outside the checking area, so quit out to save processing.
																iNodeNumber = (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES)
															ENDIF
														ENDIF
																
													ELSE
														PRINTLN("GetNearestCarMapNode - not within upper z range - fail. ")  
														// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
														iNodeNumber++
													ENDIF										
												ELSE
													PRINTLN("GetNearestCarMapNode - not within lower z range - fail. ")  
													// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
													iNodeNumber++
												ENDIF	
											ELSE
												PRINTLN("GetNearestCarMapNode - node is too near to an fmmc prop ")  											
											ENDIF
											
										ELSE
											PRINTLN("GetNearestCarMapNode - get nodecoords failed ")  	
										ENDIF
											
									ELSE
										PRINTLN("GetNearestCarMapNode - node is a problem node ")  	
										// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
										iNodeNumber++
									ENDIF
								ELSE
									PRINTLN("GetNearestCarMapNode - node is a juntion/trafficlight or give_way node ")  
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("GetNearestCarMapNode - node is tunnel.")
								#ENDIF
								// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
								iNodeNumber++
							ENDIF					
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("GetNearestCarMapNode - node is highway.")
							#ENDIF
							// if we failed here, then it's likely that the next node will also fail, so skip it too. So this will reduce the total processing.															
							iNodeNumber++
						ENDIF
						
					ELSE
						// dont consider this node as it's inside the near exclusion range
						iNumNodesFailedDist++
						PRINTLN("GetNearestCarMapNode - node is inside near exlusion range. iNumNodesFailedDist = ", iNumNodesFailedDist)						
					
					ENDIF
				
				ELSE
					
					
					// because this node is too near, do start considering until we are out of range.
					iNumNodesFailedDist++
					PRINTLN("GetNearestCarMapNode - node is too near, iNumNodesFailedDist = ", iNumNodesFailedDist)	
					
				ENDIF
			
			ELSE
			
				#IF IS_DEBUG_BUILD
					IF GET_VEHICLE_NODE_IS_SWITCHED_OFF(NodeID) 
						PRINTLN("GetNearestCarMapNode - node is switched off, ignoring.")
					ENDIF
					IF NOT GET_VEHICLE_NODE_IS_GPS_ALLOWED(NodeID)
						PRINTLN("GetNearestCarMapNode - GPS not allow to node, ignoring.")
					ENDIF
				#ENDIF
			
			ENDIF


			iNodeNumber++	
			
			PRINTLN("GetNearestCarMapNode - iNodeNumber at end of iteration  = ", iNodeNumber)
			
			IF (iNodeNumber >= (MAX_VEHICLE_NODES_CHECKED+iNumNodesFailedDist))
			OR (iNodeNumber >= (MAX_VEHICLE_NODES_CHECKED+MAX_DIST_FAIL_NODES))
				
				
				IF (g_NearestCarNodeResults.iNumResults > 0)
				AND ((SearchParams.bReturnRandomGoodNode) OR (SearchParams.bReturnNearestGoodNode) OR (SearchParams.iFallbackLevel >= 2))
					
					IF (SearchParams.bReturnNearestGoodNode)
						
						vCoords = g_NearestCarNodeResults.vCoords[0]
						fHeading = g_NearestCarNodeResults.fHeading[0]
							
						PRINTLN("GetNearestCarMapNode - reached MAX_NODES_CHECKED - returning nearest node ")  
						PRINTLN(", ", vCoords, ", ", fHeading)
			
						RETURN TRUE
						
					ELSE
					
						// if there are points inside the checking area then cleanup results to remove those that are not.
						IF (g_NearestCarNodeResults.iNumResultsInsideArea > 0)
						AND NOT (g_NearestCarNodeResults.iNumResultsInsideArea = g_NearestCarNodeResults.iNumResults)
							PRINTLN("GetNearestCarMapNode - reached MAX_NODES_CHECKED - iNumResultsInsideArea = ", g_NearestCarNodeResults.iNumResultsInsideArea) 
							PRINTLN(", iNumResults before cleanup = ", g_NearestCarNodeResults.iNumResults)
							
							RemoveNearestCarNodeNonAreaResults(0, SearchParams)	
							
							PRINTLN("GetNearestCarMapNode - reached MAX_NODES_CHECKED - iNumResultsInsideArea = ", g_NearestCarNodeResults.iNumResultsInsideArea) 
							PRINTLN(", iNumResults after cleanup = ", g_NearestCarNodeResults.iNumResults)
						ENDIF
						
						INT iRand = GET_RANDOM_INT_IN_RANGE(0, g_NearestCarNodeResults.iNumResults)
						
						IF (SearchParams.bCheckInsideArea)
						AND (SearchParams.bReturnNearestGoodNode)
							iRand = 0
							PRINTLN("GetNearestCarMapNode - reached MAX_NODES_CHECKED - but returning nearest instead of random ")
						ENDIF
						
						// make random choice position 0 in array
						
						VECTOR vStoredCoords
						FLOAT fStoredHeading
						
						vStoredCoords = g_NearestCarNodeResults.vCoords[0]
						fStoredHeading = g_NearestCarNodeResults.fHeading[0]
						
						g_NearestCarNodeResults.vCoords[0] = g_NearestCarNodeResults.vCoords[iRand]
						g_NearestCarNodeResults.fHeading[0] = g_NearestCarNodeResults.fHeading[iRand]
						
						g_NearestCarNodeResults.vCoords[iRand] = vStoredCoords
						g_NearestCarNodeResults.fHeading[iRand] = fStoredHeading
						
						vCoords = g_NearestCarNodeResults.vCoords[0]
						fHeading = g_NearestCarNodeResults.fHeading[0] 
						
						PRINTLN("GetNearestCarMapNode - reached MAX_NODES_CHECKED - returning random good node ", iRand, " of total ", g_NearestCarNodeResults.iNumResults) 
						PRINTLN(", ", vCoords, ", ", fHeading)
						
						
						RETURN TRUE
						
					ENDIF
					
				ELSE
				
					PRINTLN("GetNearestCarMapNode - reached MAX_NODES_CHECKED!")
					
					SearchParams.iFallbackLevel++
					
					IF (SearchParams.iFallbackLevel < 3)		
						PRINTLN("GetNearestCarMapNode - doing fallback ", SearchParams.iFallbackLevel)				
						//GetNearestCarMapNode(vCoords, fHeading, SearchParams)
						//EXIT
						RETURN FALSE
					ELSE
				
						GetRandomCarNode(iNumNodesFailedDist, vCoords, iNodeNumber, vNodeCoord, fNodeHeading, SearchParams, bIgnoreSwitchedOffNodes, iNoOfLanes, NodeFlag, zMeasureMult, zTolerance, bDoubleSided)
				
						VECTOR vNewCoords
						FLOAT fNewHeading

						vNewCoords = vNodeCoord
						fNewHeading = fNodeHeading
						
						BOOL bDoMove
						IF NOT (SearchParams.bDoneMoveFallback)
							bDoMove = TRUE
						ELSE
							bDoMove = FALSE
						ENDIF
						

						IF IsNodeCoordInExclusionZone(SearchParams.vStartCoords, vNewCoords, SearchParams.vAvoidCoords, SearchParams.fAvoidRadius, bDoMove)
						OR IsPointInsideNoPVSpawnZone(vNewCoords, bDoMove)
						
							IF NOT (SearchParams.bDoneMoveFallback)
								SearchParams.iFallbackLevel = 0
								SearchParams.bDoneMoveFallback = TRUE
								
								vCoords = vNewCoords
								fHeading = fNewHeading
								
								// since we moved the initial point we need to ignore the upper z
								SearchParams.fUpperZLimit = 9999.9
								SearchParams.fLowerZLimit = 9999.9							
								
								PRINTLN("GetNearestCarMapNode - moving fallback ")
								//GetNearestCarMapNode(vCoords, fHeading, SearchParams)
								//EXIT
								RETURN FALSE
							ELSE
								vCoords = vNewCoords
								fHeading = fNewHeading															
								PRINTLN("GetNearestCarMapNode - returning random node (done move fallback) ", vCoords, ", ", fHeading)														
								RETURN TRUE	
							ENDIF
						ELSE
							vCoords = vNewCoords
							fHeading = fNewHeading
							PRINTLN("GetNearestCarMapNode - returning random node ", vCoords, ", ", fHeading)						
							RETURN TRUE						
						ENDIF
						
						
							
						
					ENDIF
				
				ENDIF
			ENDIF	
			
		ELSE

			// couldn't find any car nodes use nearest fallback coord (placed around coast)
			SearchParams.iFallbackLevel++
			IF (SearchParams.iFallbackLevel < 3)		
				PRINTLN("GetNearestCarMapNode - could not find any car nodes, doing fallback ", SearchParams.iFallbackLevel)				
				//GetNearestCarMapNode(vCoords, fHeading, SearchParams)
				//EXIT
				RETURN FALSE
			ELSE
				PRINTLN("GetNearestCarMapNode - could not find any car nodes, using SpawnPoints_SafeCoastalPoints ")
				//GetClosestMPSpawnCoordToPlayer(SpawnPoints_SafeCoastalPoints, vCoords, fHeading, PLAYER_ID())
				GetClosestMPSpawnCoordToCoord(SpawnPoints_SafeCoastalPoints, vCoords, fHeading, vCoords)
				IF (SearchParams.bWaterNodesOnly)
					SearchParams.bCouldNotFindWaterNode = TRUE
					PRINTLN("GetNearestCarMapNode - could not find a water node. ")
				ENDIF
				RETURN TRUE
			ENDIF
				
		ENDIF

		g_NearestCarNodeResults.iLastVehicleNode = NATIVE_TO_INT(NodeID)

	ENDWHILE

	
	RETURN FALSE

ENDFUNC



FUNC INT GetNearestCustomVehicleNodeCoord(VECTOR vSearchCoord, FLOAT fMinDist, FLOAT &fReturnDist)
	
	INT iNearest = -1
	FLOAT fNearestDist = 99999999.9
	FLOAT fThisDist

	INT i
	REPEAT g_SpawnData.iNumberOfCustomVehicleNodes i
		fThisDist = VDIST2(vSearchCoord, g_SpawnData.CustomVehicleNodes[i].vPos) 
		IF (fThisDist < fNearestDist)
		AND (fThisDist > fMinDist)
			iNearest = i
			fNearestDist = fThisDist
		ENDIF
	ENDREPEAT
	
	fReturnDist = fNearestDist
	RETURN iNearest

ENDFUNC

#IF IS_DEBUG_BUILD
PROC PrintCustomVehicleNodeOrder()
	INT i
	INT j
	IF (g_SpawnData.iNumberOfCustomVehicleNodes > 0)
	AND (g_SpawnData.iNumberOfCustomVehicleNodes < MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES)
		REPEAT g_SpawnData.iNumberOfCustomVehicleNodes i
			j = g_SpawnData.iCustomVehicleNodesOrder[i]
			IF (j > 0)
			AND (j < MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES)
				PRINTLN("g_SpawnData.iCustomVehicleNodesOrder[", i, "] =",  g_SpawnData.iCustomVehicleNodesOrder[i], " at position ", g_SpawnData.CustomVehicleNodes[j].vPos)			
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC
#ENDIF

PROC OrderCustomVehicleNodesClosestToPoint(VECTOR vSearchCoord)

	PRINTLN("OrderCustomVehicleNodesClosestToPoint called")

	FLOAT fMinDist = -1.0
	INT iNearest
	INT iAdded
	
	WHILE iAdded < g_SpawnData.iNumberOfCustomVehicleNodes
		iNearest = GetNearestCustomVehicleNodeCoord(vSearchCoord, fMinDist, fMinDist)	
		
		#IF IS_DEBUG_BUILD
		IF (iNearest > -1)
			PRINTLN("OrderCustomVehicleNodesClosestToPoint - iAdded ", iAdded, " iNearest ", iNearest, ", g_SpawnData.CustomVehicleNodes[iNearest].vPos", g_SpawnData.CustomVehicleNodes[iNearest].vPos, " fMinDist ", fMinDist)
		ENDIF
		#ENDIF
		
		g_SpawnData.iCustomVehicleNodesOrder[iAdded] = iNearest
		iAdded++
	ENDWHILE
	
ENDPROC

PROC RandomiseCustomVehicleNodesOrder()

	PRINTLN("RandomiseCustomVehicleNodesOrder called")

	INT iCount = 0
	WHILE iCount < g_SpawnData.iNumberOfCustomVehicleNodes
		g_SpawnData.iCustomVehicleNodesOrder[iCount] = iCount	
		iCount++
	ENDWHILE
	
	// to 'randomise' we perform a number of random swaps
	iCount = 0
	INT iRand1, iRand2
	INT iStored
	WHILE iCount < g_SpawnData.iNumberOfCustomVehicleNodes
		iRand1 = GET_RANDOM_INT_IN_RANGE(0, g_SpawnData.iNumberOfCustomVehicleNodes)
		iRand2 = GET_RANDOM_INT_IN_RANGE(0, g_SpawnData.iNumberOfCustomVehicleNodes)
		iStored = g_SpawnData.iCustomVehicleNodesOrder[iRand1]
		g_SpawnData.iCustomVehicleNodesOrder[iRand1] = g_SpawnData.iCustomVehicleNodesOrder[iRand2]
		g_SpawnData.iCustomVehicleNodesOrder[iRand2] = iStored
		iCount++
	ENDWHILE
	
ENDPROC

PROC KeepOriginalCustomVehicleNodesOrder()

	PRINTLN("KeepOriginalCustomVehicleNodesOrder called")

	INT iCount = 0
	WHILE iCount < g_SpawnData.iNumberOfCustomVehicleNodes
		g_SpawnData.iCustomVehicleNodesOrder[iCount] = iCount	
		iCount++
	ENDWHILE
ENDPROC


FUNC BOOL GetNearestCustomCarNode(VECTOR &vCoords, FLOAT &fHeading, NearestCarNodeSearch &SearchParams )

	INT iRand

	IF (g_SpawnData.iNumberOfCustomVehicleNodes > 0)

		#IF IS_DEBUG_BUILD
			IF VMAG(vCoords) = 0.0
				SCRIPT_ASSERT("GetNearestCustomCarNode - origin passed in")
			ENDIF
			
			DEBUG_PRINTCALLSTACK()
			
			PRINTLN("called GetNearestCustomCarNode(", vCoords, ", ", fHeading)
			
			Print_NearestCarNodeSearch(SearchParams)
			
		#ENDIF
		
		INT iNodeNumber = 0
		INT iNearNodeNumber = 0
		VECTOR vNodeCoord
		FLOAT fNodeHeading
		//FLOAT fMinVehicleModelRadius = GET_MIN_RADIUS_FOR_VEHICLE_MODEL(SearchParams.CarModel)

		IF NOT (VMAG(SearchParams.vStartCoords) > 0.0)
			SearchParams.vStartCoords = vCoords
		ENDIF
		
		// if starting point is inside an exclusion zone then move it out.
		IF (SearchParams.bCheckSpawnExclusionZones)
			IF IsNodeCoordInExclusionZone(SearchParams.vStartCoords, vCoords, SearchParams.vAvoidCoords, SearchParams.fAvoidRadius, TRUE)
				PRINTLN("GetNearestCustomCarNode - moved point out of exclusion zone - vCoords ", vCoords)
				
				// since we moved the initial point we need to ignore the upper z
				SearchParams.fUpperZLimit = 9999.9
				SearchParams.fLowerZLimit = 9999.9
				
			ENDIF
		ENDIF
		
		// if starting point is inside pv no spawn zone then move it out
		IF (SearchParams.bCheckPVNoSpawnZone) 
			IF IsPointInsideNoPVSpawnZone(vCoords, TRUE)
				PRINTLN("GetNearestCustomCarNode - moved point out of pv no spawn zone - vCoords ", vCoords)
				
				// since we moved the initial point we need to ignore the upper z
				SearchParams.fUpperZLimit = 9999.9
				SearchParams.fLowerZLimit = 9999.9
				
			ENDIF
		ENDIF
		
		// if search point is below -100 ignor upperzlimit
		IF (vCoords.z < -80.0)
			SearchParams.fUpperZLimit = 9999.9
			SearchParams.fLowerZLimit = 9999.9
			PRINTLN("GetNearestCustomCarNode z is < -80, ignoring fUpperZLimit = ", SearchParams.fUpperZLimit)
		ENDIF		

		g_NearestCarNodeResults.iNumResults = 0
		g_NearestCarNodeResults.iNumResultsInsideArea = 0
		
		// make sure any previous results are cleared
		INT i 
		REPEAT MAX_VEHICLE_NODES_CHECKED i
			g_NearestCarNodeResults.vCoords[i] = <<0.0, 0.0, 0.0>>
			g_NearestCarNodeResults.fHeading[i] = 0.0
		ENDREPEAT		
		
		// order custom vehicle nodes to be closest to in coord.
		IF (SearchParams.bReturnNearestGoodNode)
			OrderCustomVehicleNodesClosestToPoint(vCoords)
		ELIF (SearchParams.bReturnRandomGoodNode)
			RandomiseCustomVehicleNodesOrder()
		ELSE
			KeepOriginalCustomVehicleNodesOrder()	
		ENDIF
		//#IF IS_DEBUG_BUILD
			//PrintCustomVehicleNodeOrder()
		//#ENDIF
		
		
		REPEAT g_SpawnData.iNumberOfCustomVehicleNodes iNodeNumber
		
			iNearNodeNumber = g_SpawnData.iCustomVehicleNodesOrder[iNodeNumber]
		
			IF (iNearNodeNumber > -1)
			AND (iNearNodeNumber < MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES)
			
				vNodeCoord = g_SpawnData.CustomVehicleNodes[iNearNodeNumber].vPos
				fNodeHeading = g_SpawnData.CustomVehicleNodes[iNearNodeNumber].fHeading
			
				// check point is more than the min dist away
				IF VMAG(vNodeCoord) > 0.0
				
					IF ((SearchParams.bEnforceMinDistCheckForCustomNodes) AND (VDIST(vNodeCoord,SearchParams.vStartCoords) > SearchParams.fMinDistFromCoords))
					OR (SearchParams.bEnforceMinDistCheckForCustomNodes = FALSE)
					
						// check point is less than max dist away.
						IF (SearchParams.fMaxDistance > 0.0) AND( VDIST(<<vNodeCoord.x, vNodeCoord.y, 0.0>>, <<SearchParams.vStartCoords.x, SearchParams.vStartCoords.y, 0.0>>) < SearchParams.fMaxDistance)
						OR (SearchParams.fMaxDistance <= 0.0)			
						
						
							//IF ((SearchParams.bCheckVehicleSpawning) AND NOT (IsAnotherPlayerOrPlayerVehicleSpawningAtPosition(vNodeCoord, 5.0, fMinVehicleModelRadius, PLAYER_ID(), FALSE, TRUE)))
							IF ((SearchParams.bCheckVehicleSpawning) AND NOT (IsAnotherPlayerOrPlayerVehicleSpawningInCarBounds(vNodeCoord, fNodeHeading, SearchParams.CarModel, PLAYER_ID(), FALSE, SearchParams.bCheckThisPlayerVehicle)))
							OR NOT (SearchParams.bCheckVehicleSpawning)
								
								IF NOT (SearchParams.bCheckSpawnExclusionZones)
								OR NOT IsNodeCoordInExclusionZone(SearchParams.vStartCoords, vNodeCoord, SearchParams.vAvoidCoords, SearchParams.fAvoidRadius)
									
									IF (SearchParams.bDoSafeForSpawnCheck)
									
										BOOL bCheckThisPlayerSight = SearchParams.bCheckOwnVisibility
										BOOL bDoAnyPlayerSeePointCheck = TRUE
										BOOL bCheckVisibilityForOwnTeam = TRUE
										FLOAT fVisibleDistance = SearchParams.fVisibleDistance														
									
										IF NOT (SearchParams.bDoVisibilityChecks)
											bCheckThisPlayerSight = FALSE
											bDoAnyPlayerSeePointCheck = FALSE
											bCheckVisibilityForOwnTeam = FALSE
											fVisibleDistance = 1.0
											
										ELSE
										
											IF (SearchParams.bStartOfMissionPVSpawn)
												
												bCheckThisPlayerSight = FALSE
												bDoAnyPlayerSeePointCheck = FALSE
												bCheckVisibilityForOwnTeam = FALSE
												
												IF (SearchParams.iFallbackLevel = 1)		
													fVisibleDistance = fVisibleDistance * 0.375
												ENDIF
												
											ELSE
											
												bDoAnyPlayerSeePointCheck = TRUE
												bCheckVisibilityForOwnTeam = TRUE
												
												IF (SearchParams.bAllowReducingVisibility)
													IF (SearchParams.iFallbackLevel = 1)		
														fVisibleDistance = fVisibleDistance * 0.375
													ENDIF		
												ENDIF
											
											ENDIF
										ENDIF
										
																						
										BOOL bPointOkForCreation = FALSE
										PRINTLN("GetNearestCustomCarNode - SearchParams.fMinPlayerDist = ", SearchParams.fMinPlayerDist)
										IF NOT DOES_NEAREST_VEHICLE_COLLIDE_WITH_VEHICLE_BOUNDS(vNodeCoord, fNodeHeading, SearchParams.CarModel)
											IF (SearchParams.fMinPlayerDist > 7.0)
												IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNodeCoord,6,1,1,5,bCheckThisPlayerSight,bDoAnyPlayerSeePointCheck,bCheckVisibilityForOwnTeam,fVisibleDistance,SearchParams.bIgnoreLocalPlayerChecks,-1,TRUE,SearchParams.fMinPlayerDist)
													bPointOkForCreation = TRUE																					
												ENDIF
											ELSE
												IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(vNodeCoord,6,1,1,5,bCheckThisPlayerSight,bDoAnyPlayerSeePointCheck,bCheckVisibilityForOwnTeam,fVisibleDistance,SearchParams.bIgnoreLocalPlayerChecks,-1,TRUE,0.0)
												AND NOT IS_ANY_PLAYER_IN_VEHICLE_BOUNDS(vNodeCoord, fNodeHeading, SearchParams.CarModel, DEFAULT, DEFAULT, DEFAULT, DEFAULT, SearchParams.bIgnoreLocalPlayerChecks)
													bPointOkForCreation = TRUE	
												ENDIF
											ENDIf	
										ELSE
											PRINTLN("GetNearestCustomCarNode - point collides with nearest vehicle")	
										ENDIF
										
										IF (bPointOkForCreation)										
										#IF IS_DEBUG_BUILD
										AND NOT (g_SpawnData.bForceNodeSearchFail)
										#ENDIF
											
											IF (SearchParams.bReturnRandomGoodNode)
											OR (SearchParams.bReturnNearestGoodNode)
											OR (SearchParams.bCheckForLowestEnemiesNearPoint)
												
												
												INT iEnemiesNearPoint
												FLOAT fEnemyDist = 0.0
												
												IF (SearchParams.bCheckForLowestEnemiesNearPoint)
													iEnemiesNearPoint = NumberEnemiesNearPoint(vNodeCoord, SearchParams.fCheckForLowestnemiesMaxDist, fEnemyDist)
												ENDIF
												
												IF NOT (SearchParams.bCheckForLowestEnemiesNearPoint)
												OR ((SearchParams.bCheckForLowestEnemiesNearPoint) AND (iEnemiesNearPoint <= SearchParams.iLowestNumEnemiesNearPoint))
													
													
													// should we clear previously stored results?
													IF (SearchParams.bCheckForLowestEnemiesNearPoint)
														IF (iEnemiesNearPoint < SearchParams.iLowestNumEnemiesNearPoint)
														
															PRINTLN("GetNearestCustomCarNode - new lowest number of enemies, clearing previous results, iEnemiesNearPoint = ", iEnemiesNearPoint)
															REPEAT g_NearestCarNodeResults.iNumResults i
																g_NearestCarNodeResults.vCoords[i] = <<0.0, 0.0, 0.0>>
																g_NearestCarNodeResults.fHeading[i] = 0.0			
															ENDREPEAT
															g_NearestCarNodeResults.iNumResults = 0
															
															SearchParams.iLowestNumEnemiesNearPoint = iEnemiesNearPoint
														ENDIF
													ENDIF
													
													PRINTLN("GetNearestCustomCarNode - found good vehicle node! adding to list as point ", g_NearestCarNodeResults.iNumResults) 
													PRINTLN(", ", vNodeCoord)
													PRINTLN(", ", fNodeHeading)
													
													
													IF (SearchParams.bReturnNearestGoodNode)
											
														IF (g_NearestCarNodeResults.iNumResults = 0)
															g_NearestCarNodeResults.vCoords[0] = vNodeCoord
															g_NearestCarNodeResults.fHeading[0] = fNodeHeading
														ELSE
															REPEAT (g_NearestCarNodeResults.iNumResults+1) i
																IF (i < MAX_VEHICLE_NODES_CHECKED)
																	IF (VDIST2(vNodeCoord, SearchParams.vStartCoords) < VDIST2(g_NearestCarNodeResults.vCoords[i], SearchParams.vStartCoords))
																		InsertCarNodeResultIntoArray(vNodeCoord, fNodeHeading, i)
																		i = g_NearestCarNodeResults.iNumResults+1
																	ENDIF
																ENDIF
															ENDREPEAT
														ENDIF

														g_NearestCarNodeResults.iNumResults++
														
														IF (g_NearestCarNodeResults.iNumResults >= NUM_OF_STORED_SPAWN_RESULTS)
															IF ((SearchParams.bCheckForLowestEnemiesNearPoint) AND (SearchParams.iLowestNumEnemiesNearPoint = 0))
															OR (SearchParams.bCheckForLowestEnemiesNearPoint = FALSE)
															
																iNodeNumber = g_SpawnData.iNumberOfCustomVehicleNodes
																PRINTLN("GetNearestCustomCarNode - got enough good results from nearest nodes! ")
																
															ELSE
																IF (g_NearestCarNodeResults.iNumResults = MAX_VEHICLE_NODES_CHECKED)	
																	iNodeNumber = g_SpawnData.iNumberOfCustomVehicleNodes
																	PRINTLN("GetNearestCustomCarNode - stored max number of valid nodes ")
																ENDIF
															ENDIF
														ENDIF
														
													ELSE	
													
														g_NearestCarNodeResults.vCoords[g_NearestCarNodeResults.iNumResults] = vNodeCoord
														g_NearestCarNodeResults.fHeading[g_NearestCarNodeResults.iNumResults] = fNodeHeading
														g_NearestCarNodeResults.iNumResults++						
														
														// if we have enough decent results then skip to the end (optimisation)
														IF (g_NearestCarNodeResults.iNumResults >= 10)
															
															IF ((SearchParams.bCheckForLowestEnemiesNearPoint) AND (SearchParams.iLowestNumEnemiesNearPoint = 0))
															OR (SearchParams.bCheckForLowestEnemiesNearPoint = FALSE)
														
																iNodeNumber = g_SpawnData.iNumberOfCustomVehicleNodes
																PRINTLN("GetNearestCustomCarNode - got enough good results! ")
																
															ELSE
																IF (g_NearestCarNodeResults.iNumResults = MAX_VEHICLE_NODES_CHECKED)	
																	iNodeNumber = g_SpawnData.iNumberOfCustomVehicleNodes
																	PRINTLN("GetNearestCustomCarNode - stored max number of valid nodes (non nearest) ")
																ENDIF
															ENDIF
															
														ENDIF
													
													ENDIF
												ELSE
													PRINTLN("GetNearestCustomCarNode - too many enemies near point, fail. iEnemiesNearPoint = ", iEnemiesNearPoint)
												ENDIF
												
											ELSE
												vCoords = vNodeCoord
												fHeading = fNodeHeading
												PRINTLN("GetNearestCustomCarNode - found good vehicle node! vNodeCoord = ", vNodeCoord)
												RETURN(TRUE)
											ENDIF
											
										ELSE
										
											PRINTLN("GetNearestCustomCarNode - final fail - bStartOfMissionPVSpawn = ", SearchParams.bStartOfMissionPVSpawn)																	

										ENDIF
									
									ELSE
										vCoords = vNodeCoord
										fHeading = fNodeHeading
										PRINTLN("GetNearestCustomCarNode - found a good node!")
										RETURN TRUE 
									ENDIF					
								ELSE
									PRINTLN("GetNearestCustomCarNode - bCheckSpawnExclusionZones - fail ")  	
								ENDIF
								
							ENDIF
						ELSE
							PRINTLN("GetNearestCustomCarNode - too far away. ")  							
						ENDIF
					ELSE
						PRINTLN("GetNearestCustomCarNode - too near - enforced! ")  	
					ENDIF
				ELSE
					PRINTLN("GetNearestCustomCarNode - node coord is origin ")  	
				ENDIF
			ELSE
				PRINTLN("GetNearestCustomCarNode - iNearNodeNumber not valid - ",  iNearNodeNumber)
			ENDIF
			
		ENDREPEAT
		
		
		IF (g_NearestCarNodeResults.iNumResults > 0)
		
			IF (SearchParams.bReturnNearestGoodNode) 
				
				vCoords = g_NearestCarNodeResults.vCoords[0]
				fHeading = g_NearestCarNodeResults.fHeading[0]

				PRINTLN("GetNearestCustomCarNode - reached MAX_NODES_CHECKED - returning nearest node ")  
				PRINTLN(", ", vCoords, ", ", fHeading)
								
				
				RETURN TRUE	
				
			ELSE
			
				// if there are points inside the checking area then cleanup results to remove those that are not.
				IF (g_NearestCarNodeResults.iNumResultsInsideArea > 0)
				AND NOT (g_NearestCarNodeResults.iNumResultsInsideArea = g_NearestCarNodeResults.iNumResults)
					PRINTLN("GetNearestCustomCarNode - reached MAX_NODES_CHECKED - iNumResultsInsideArea = ", g_NearestCarNodeResults.iNumResultsInsideArea) 
					PRINTLN(", iNumResults before cleanup = ", g_NearestCarNodeResults.iNumResults)
					
					RemoveNearestCarNodeNonAreaResults(0, SearchParams)	
					
					PRINTLN("GetNearestCustomCarNode - reached MAX_NODES_CHECKED - iNumResultsInsideArea = ", g_NearestCarNodeResults.iNumResultsInsideArea) 
					PRINTLN(", iNumResults after cleanup = ", g_NearestCarNodeResults.iNumResults)
				ENDIF			
		
				iRand = GET_RANDOM_INT_IN_RANGE(0, g_NearestCarNodeResults.iNumResults)
				
				
				// make random choice position 0 in array
				
				VECTOR vStoredCoords
				FLOAT fStoredHeading
				
				vStoredCoords = g_NearestCarNodeResults.vCoords[0]
				fStoredHeading = g_NearestCarNodeResults.fHeading[0]
						
				g_NearestCarNodeResults.vCoords[0] = g_NearestCarNodeResults.vCoords[iRand]
				g_NearestCarNodeResults.fHeading[0] = g_NearestCarNodeResults.fHeading[iRand]
						
				g_NearestCarNodeResults.vCoords[iRand] = vStoredCoords
				g_NearestCarNodeResults.fHeading[iRand] = fStoredHeading
				
				vCoords = g_NearestCarNodeResults.vCoords[0]
				fHeading = g_NearestCarNodeResults.fHeading[0]
			
				PRINTLN("GetNearestCustomCarNode - reached MAX_NODES_CHECKED - returning random good node ", iRand, " of total ", g_NearestCarNodeResults.iNumResults) 
				PRINTLN(", ", vCoords, ", ", fHeading)
				
				
				RETURN TRUE		
				
			ENDIF
		
		ELSE
			// fallback
			SearchParams.iFallbackLevel++
			IF (SearchParams.iFallbackLevel < 2)		
				PRINTLN("GetNearestCustomCarNode - could not find any car nodes, doing fallback ", SearchParams.iFallbackLevel)				
				//RETURN GetNearestCustomCarNode(vCoords, fHeading, SearchParams)
				RETURN FALSE
			ELSE
				PRINTLN("GetNearestCustomCarNode - could not find any suitable custom car nodes, returning normal car nodes ")					
				IF (SearchParams.bUseCustomNodesOnly)
				AND (g_SpawnData.iNumberOfCustomVehicleNodes > 0)
				
					// return a random custom node.
					iRand = GET_RANDOM_INT_IN_RANGE(0, g_SpawnData.iNumberOfCustomVehicleNodes)
					
					vCoords = g_SpawnData.CustomVehicleNodes[iRand].vPos
					fHeading = g_SpawnData.CustomVehicleNodes[iRand].fHeading
					
					PRINTLN("GetNearestCustomCarNode - bUseCustomNodesOnly = TRUE, returning random custom node. rand= ", iRand, " vCoords = ", vCoords) 
					PRINTLN(", ", vCoords, ", ", fHeading)
					
					RETURN TRUE
				
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF	
		ENDIF
		
	ELSE
		PRINTLN("GetNearestCustomCarNode - none setup!")
		SCRIPT_ASSERT("GetNearestCustomCarNode - none setup!") 
	ENDIF
	RETURN(FALSE)
	
ENDFUNC

PROC GetNearestCarNode(VECTOR &vCoords, FLOAT &fHeading, NearestCarNodeSearch &SearchParams )
	INT iIteration = 0
	
	// try custom vehicle nodes first
	IF (g_SpawnData.iNumberOfCustomVehicleNodes > 0)
		iIteration = 0
		WHILE ((GetNearestCustomCarNode(vCoords, fHeading, SearchParams) = FALSE) AND (iIteration < 2))
			iIteration++
			PRINTLN("[spawning] GetNearestCarNode - GetNearestCarMapNode was FALSE, iIteration = ", iIteration)		
		ENDWHILE
		// did we fail?
		IF (iIteration = 2)
			SearchParams.iFallbackLevel = 0	
			PRINTLN("[spawning] GetNearestCarNode - GetNearestCustomCarNode FAILED")
		ELSE
			PRINTLN("[spawning] GetNearestCarNode - GetNearestCustomCarNode PASSED")
			EXIT
		ENDIF
	ENDIF
	
	// try regular map nodes
	iIteration = 0
	WHILE ((GetNearestCarMapNode(vCoords, fHeading, SearchParams) = FALSE) AND (iIteration < 6))
		iIteration++
		PRINTLN("[spawning] GetNearestCarNode - GetNearestCarMapNode was FALSE, iIteration = ", iIteration)	
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
	IF (iIteration = 6)
		SCRIPT_ASSERT("GetNearestCarNode - iIteration = 3, should not get here, please bug NeilF.")
		PRINTLN("GetNearestCarNode - iIteration = 3, should not get here, please bug NeilF.")
	ENDIF 
	#ENDIF

ENDPROC


//
//PROC GetNearestSpawnNode(VECTOR &vCoords, FLOAT &fHeading, VECTOR vFavourFacing, BOOL bDoSafeForSpawnCheck=TRUE, BOOL bIsAtGanghouse=FALSE, FLOAT fMinPlayerDist=0.0, BOOL bConsiderHighways = TRUE)
//	IF (bIsAtGanghouse)
//		PRINTLN("GetNearestSpawnNode - this is for gang house!")	
//	ENDIF
//	BOOL bConsiderOnlyActiveNodes
//	IF (bConsiderInactiveNodes)
//		bConsiderOnlyActiveNodes = FALSE
//	ELSE
//		bConsiderOnlyActiveNodes = TRUE
//	ENDIF
//	
//	NearestCarNodeSearch SearchParams
//	SearchParams.vFavourFacing = vFavourFacing
//	SearchParams.bDoSafeForSpawnCheck = bDoSafeForSpawnCheck
//	SearchParams.fMinPlayerDist = fMinPlayerDist
//	SearchParams.bConsiderOnlyActiveNodes = bConsiderOnlyActiveNodes
//	SearchParams.bConsiderHighways = bConsiderHighways
//	SearchParams.bConsiderTunnels = bConsiderInactiveNodes
//	
//	GetNearestCarMapNode(vCoords, fHeading, SearchParams)
//ENDPROC

FUNC BOOL IsPointOffRoad(VECTOR vPoint)

	VEHICLE_NODE_ID NearestNodeID
	NearestNodeID = GET_NTH_CLOSEST_VEHICLE_NODE_ID(vPoint, 1)
	
	IF IS_VEHICLE_NODE_ID_VALID(NearestNodeID)
		IF GET_VEHICLE_NODE_IS_SWITCHED_OFF(NearestNodeID)
			RETURN(TRUE)
		ENDIF
	ENDIF
	
	RETURN(FALSE)

ENDFUNC

FUNC VECTOR GetNearestCarNodeInsideAreaNoNearEnemy(VECTOR vStart, VECTOR vCoords1, VECTOR vCoords2, FLOAT fFloat, INT iShape, FLOAT fEnemyDist, BOOL bCheckMissionExclusionZones, BOOL bForFlyingVehicle, BOOL bActiveOnly, BOOL &bFoundANode)
	
//	VECTOR vReturn
//	FLOAT fReturnHeading
//	NearestCarNodeSearch SearchParams
//	
//	vReturn = vStart
//
//	SearchParams.bGetSafeOffset = FALSE
//	
//	SearchParams.bCheckInsideArea = TRUE
//	SearchParams.iAreaShape = iShape
//	SearchParams.vAreaCoords1 = vCoords1
//	SearchParams.vAreaCoords2 = vCoords2
//	SearchParams.fAreaFloat = fFloat
//	SearchParams.bCheckForLowestEnemiesNearPoint = TRUE
//	SearchParams.fCheckForLowestnemiesMaxDist = fEnemyDist
//	
//	GetNearestCarMapNode(vReturn, fReturnHeading, SearchParams ) 
//	
//	RETURN vReturn
	
	PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - called with ", vStart, vCoords1, vCoords2, fFloat, iShape, fEnemyDist)
	
	INT i
	INT iLowestNumEnemiesNearPoint = 999
	INT iNumEnemiesNearThisPoint
	VECTOR vNodeCoords
	VECTOR vStoredCoords
	BOOL bPointIsInArea
	FLOAT fHighestNearestEnemyDist = 0.0
	FLOAT fNearestEnemyDist
	
	vStoredCoords = vStart
	
	NODE_FLAGS NodeFlags
	IF (bActiveOnly)
		NodeFlags = NF_NONE
	ELSE
		NodeFlags = NF_INCLUDE_SWITCHED_OFF_NODES
	ENDIF
	
	REPEAT 200 i
		GET_NTH_CLOSEST_VEHICLE_NODE(vStart, i, vNodeCoords, NodeFlags, 0, 0)
		
		PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - considering vNodeCoords ", vNodeCoords)
		
		IF (bForFlyingVehicle)
			SWITCH iShape
				CASE SPAWN_AREA_SHAPE_CIRCLE
					IF (vNodeCoords.z < (vCoords1.z - fFloat))
						vNodeCoords.z = vCoords1.z
						PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - bForFlyingVehicle = TRUE, and z is lower. (sphere) setting z to  ", vNodeCoords.z)	
					ENDIF
				BREAK
				CASE SPAWN_AREA_SHAPE_BOX
				CASE SPAWN_AREA_SHAPE_ANGLED
					IF (vNodeCoords.z < vCoords1.z)
					AND (vNodeCoords.z < vCoords2.z)
						vNodeCoords.z = (vCoords1.z + vCoords2.z) * 0.5
						PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - bForFlyingVehicle = TRUE, and z is lower. (box/area) setting z to  ", vNodeCoords.z)
					ENDIF
				BREAK
			ENDSWITCH				
		ENDIF
		
				
		SWITCH iShape
			CASE SPAWN_AREA_SHAPE_CIRCLE
				bPointIsInArea = IsPointInsideSphere(vNodeCoords, vCoords1, fFloat, FALSE, FALSE)
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX
				bPointIsInArea = IsPointInsideBox(vNodeCoords, vCoords1, vCoords2, FALSE, FALSE)
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED
				bPointIsInArea = IS_POINT_IN_ANGLED_AREA(vNodeCoords, vCoords1, vCoords2, fFloat)
			BREAK
		ENDSWITCH
		

		// check if this point is in a mission exclusion zone
		IF (bCheckMissionExclusionZones)
			IF (bPointIsInArea)
				IF IS_POINT_IN_MISSION_EXCLUSION_ZONE(vNodeCoords)
					PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - vNodeCoords inside a mission exclusion zone. ignoring. ", vNodeCoords)	
					bPointIsInArea = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF (bPointIsInArea)
			bFoundANode = TRUE
			
			iNumEnemiesNearThisPoint = NumberEnemiesNearPoint(vNodeCoords, fEnemyDist, fNearestEnemyDist)
			PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - iNumEnemiesNearThisPoint = ", iNumEnemiesNearThisPoint)	
			PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - fNearestEnemyDist = ", fNearestEnemyDist)	
			
			IF (iNumEnemiesNearThisPoint = 0)
				PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - no enemies near - returning ", vNodeCoords)
				RETURN (vNodeCoords)
			ELSE
				IF (iNumEnemiesNearThisPoint < iLowestNumEnemiesNearPoint)
					PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - fewest enemies near, storing ", vNodeCoords)
					fHighestNearestEnemyDist = fNearestEnemyDist
					vStoredCoords = vNodeCoords	
					iLowestNumEnemiesNearPoint = iNumEnemiesNearThisPoint
				ELIF (iNumEnemiesNearThisPoint = iLowestNumEnemiesNearPoint)
					IF (fNearestEnemyDist > fHighestNearestEnemyDist)
						PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - nearest enemy is further, storing ", vNodeCoords)
						fHighestNearestEnemyDist = fNearestEnemyDist
						vStoredCoords = vNodeCoords	
					ENDIF				
				ENDIF
			ENDIF
			
			
			
		ELSE
			PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - vNodeCoords not in area.")	
		ENDIF
		
	ENDREPEAT
	
	PRINTLN("[spawning] GetNearestCarNodeInsideAreaNoNearEnemy - returning vStoredCoords = ", vStoredCoords)	
	RETURN(vStoredCoords)
	
ENDFUNC











