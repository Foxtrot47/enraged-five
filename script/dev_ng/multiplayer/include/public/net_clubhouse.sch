//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//		SCRIPT NAME		:	net_clubhouse.sch												//
//		AUTHOR			:	Online Technical Team: A.Tabrizi, 								//
//		DESCRIPTION		:	Header file that contains all public functions for club house	//
//							related stuff.													//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

USING "net_clubhouse_private.sch"
USING "net_simple_interior.sch"

FUNC BOOL RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE(CLUBHOUSE_STRUCT& sClubHouse,INT iCurrentProperty,PLAYER_INDEX clubHouseOwner)
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		IF DOES_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH(clubHouseOwner)
		AND !g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_MOD_SHOP
			//Make sure the car mod script cleanup flag is false
			IF g_bCleanUpCarmodShop = TRUE
				g_bCleanUpCarmodShop = FALSE
				PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - Setting g_bCleanUpCarmodShop To FALSE")
			ENDIF
		
			IF NOT IS_BIT_SET(sClubHouse.iLocalBS,CLUB_HOUSE_BS_IS_CAR_MOD_SCRIPT_READY)
				sClubHouse.sCarmodScript = "carmod_shop"
				REQUEST_SCRIPT(sClubHouse.sCarmodScript)
				IF HAS_SCRIPT_LOADED(sClubHouse.sCarmodScript)
				AND NOT IS_THREAD_ACTIVE(sClubHouse.CarModThread)
				AND !g_bCleanUpCarmodShop
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sClubHouse.sCarmodScript)) < 1
						IF NOT NETWORK_IS_SCRIPT_ACTIVE(sClubHouse.sCarmodScript,g_iCarModInstance,TRUE)
							SHOP_LAUNCHER_STRUCT sShopLauncherData
							sShopLauncherData.bLinkedShop = FALSE
							sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
							sShopLauncherData.iNetInstanceID = g_iCarModInstance
							
							IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
								sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_BIKER_ONE
							ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
								sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_BIKER_TWO
							ELSE	
								SCRIPT_ASSERT("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - Unknown club house variation")
							ENDIF
							
							g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation) 
							sClubHouse.CarModThread = START_NEW_SCRIPT_WITH_ARGS(sClubHouse.sCarmodScript, sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
							SET_SCRIPT_AS_NO_LONGER_NEEDED(sClubHouse.sCarmodScript)
							SET_BIT(sClubHouse.iLocalBS,CLUB_HOUSE_BS_IS_CAR_MOD_SCRIPT_READY)
							g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
							g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
							
							PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - TRUE - PersonalCarModVariation:", sShopLauncherData.ePersonalCarModVariation)
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - carmod is already running")
						RETURN TRUE 
					ENDIF
				ENDIF
			ELSE
				PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - return true IS_BIT_SET(sClubHouse.iLocalBS,CLUB_HOUSE_BS_IS_CAR_MOD_SCRIPT_READY) is TRUE")
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - return true - DOES_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH is FALSE")
			#ENDIF
			RETURN TRUE
		ENDIF	
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - return true - this property is not club house")
		#ENDIF
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("RUN_CARMOD_SCRIPT_FOR_CLUBHOUSE - return false")
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAN_UP_PERSONAL_CAR_MOD(CLUBHOUSE_STRUCT& sClubHouse)
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD - called")
	g_bCleanUpCarmodShop = TRUE
	g_bPersonalCarModTriggered = FALSE
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	CLEAR_BIT(sClubHouse.iLocalBS,CLUB_HOUSE_BS_IS_CAR_MOD_SCRIPT_READY)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX pedVeh
		pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		NETWORK_REQUEST_CONTROL_OF_ENTITY(pedVeh)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(pedVeh)
			SET_VEHICLE_RADIO_ENABLED(pedVeh, TRUE)
			#IF IS_DEBUG_BUILD
			PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD - enable radio")
			#ENDIF
		ENDIF	
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_CLUBHOUSE_DEBUG_WIDGETS(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu, CLUBHOUSE_MC_NAME_STRUCT& sMCName)
	START_WIDGET_GROUP("Clubhouse Mission Menu")
		IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS("clubhouse")
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			sClubHouseMissionMenu.bDebugDraw = TRUE
		ENDIF
		
		ADD_WIDGET_BOOL("bDebugDraw", sClubHouseMissionMenu.bDebugDraw)
		ADD_WIDGET_BOOL("bDebugUpdateMenu", sClubHouseMissionMenu.bDebugUpdateMenu)
		ADD_WIDGET_BOOL("bDebugInvalidateAll", sClubHouseMissionMenu.bDebugInvalidateAll)
		ADD_WIDGET_FLOAT_SLIDER("fMISSION_TRIGGER_RADIUS", fCONST_CLUBHOUSE_MISSION_TRIGGER_RADIUS, 0,5,0.5)
		ADD_WIDGET_VECTOR_SLIDER("sTriggerOffset.vLoc", sClubHouseMissionMenu.sTriggerOffset.vLoc, -10000, 10000, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("sTriggerOffset.vRot", sClubHouseMissionMenu.sTriggerOffset.vRot, -180, 180, 0.1)
		
		ADD_WIDGET_STRING("Cam Debug")
		ADD_WIDGET_VECTOR_SLIDER("sStoredOffset.vPoint", sClubHouseMissionMenu.sStoredOffset.vPoint, -10000, 10000, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("sCurrentOffset.vPoint", sClubHouseMissionMenu.sCurrentOffset.vPoint, -10000, 10000, 0.01)
		
		ADD_WIDGET_STRING("Scaleform")
		ADD_WIDGET_FLOAT_SLIDER("Centre X", sClubHouseMissionMenu.fCentreX, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Centre Y", sClubHouseMissionMenu.fCentreY, -1.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Width", sClubHouseMissionMenu.fWidth, 0.0, 1.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("Height", sClubHouseMissionMenu.fHeight, 0.0, 1.0, 0.001)
		
		START_WIDGET_GROUP("GlobalPlayerBD_FM ClubhouseMissionData")
			IF NETWORK_IS_HOST()
				ADD_WIDGET_INT_SLIDER("iBIKER_CLUBHOUSE_WALL_REFRESH (seconds)", g_sMPTunables.iBIKER_CLUBHOUSE_WALL_REFRESH, 0, HIGHEST_INT, 1)
			ELSE
				ADD_WIDGET_INT_READ_ONLY("iBIKER_CLUBHOUSE_WALL_REFRESH (seconds)", g_sMPTunables.iBIKER_CLUBHOUSE_WALL_REFRESH)
			ENDIF
			
			INT i
			REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
				ADD_WIDGET_INT_SLIDER("iCHMissions", GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i], -1, ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM), 1)
			ENDREPEAT
			ADD_WIDGET_INT_SLIDER("iHighlightedMission", GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iHighlightedMission, -1, ENUM_TO_INT(CLUBHOUSE_MISSION_MAX_MENU_ITEM), 1)
		//	ADD_WIDGET_INT_SLIDER("timestamp", GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.timestamp, LOWEST_INT, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	START_WIDGET_GROUP("Clubhouse MC Name")
		ADD_WIDGET_STRING("[SAR]")
		ADD_WIDGET_FLOAT_SLIDER("Centre X", sMCName.fCentreX, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Centre Y", sMCName.fCentreY, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Width", sMCName.fWidth, 0.0, 1.0, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("Height", sMCName.fHeight, 0.0, 1.0, 0.01)
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_CLUBHOUSE_DEBUG_WIDGETS(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu)
	IF sClubHouseMissionMenu.bDebugDraw
		
		VECTOR vDebugLoc = sClubHouseMissionMenu.sTriggerOffset.vLoc+<<0,0,1>>
		
		TEXT_LABEL_63 str
		INT iStrOffset = 0
		INT iRed, iGreen, iBlue, iAlpha
		HUD_COLOURS hcColour = HUD_COLOUR_RED
		IF NOT IS_PLAYER_FACING_MISSION_MENU(sClubHouseMissionMenu)
			hcColour = HUD_COLOUR_BLUE
		ENDIF
		GET_HUD_COLOUR(hcColour, iRed, iGreen, iBlue, iAlpha)
		
		IF sClubHouseMissionMenu.eMenuState != CHM_MENU_STAGE_SELECT_MISSION
//			DRAW_DEBUG_LINE_WITH_TWO_COLOURS(GET_ENTITY_COORDS(PLAYER_PED_ID()),vDebugLoc,
//					255-iRed, 255-iGreen, 255-iBlue, DEFAULT,
//					iRed, iGreen, iBlue, DEFAULT)
			DRAW_DEBUG_SPHERE(vDebugLoc, fCONST_CLUBHOUSE_MISSION_TRIGGER_RADIUS,
					iRed, iGreen, iBlue, 064)
			
			VECTOR vRotOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vDebugLoc, sClubHouseMissionMenu.sTriggerOffset.vRot.z, <<0,4,0>>)
//			DRAW_DEBUG_LINE(vRotOffset, vDebugLoc,
//					000, 255, 000)
			DRAW_DEBUG_SPHERE(vRotOffset, 0.25,
					000, 255, 000)
			
			str  = ""
			str += GET_CLUBHOUSE_MISSION_MENU_STAGE_NAME(sClubHouseMissionMenu.eMenuState)
			DRAW_DEBUG_TEXT_WITH_OFFSET(str, vDebugLoc,
					0, iStrOffset*10, 255-iRed, 255-iGreen, 255-iBlue, iAlpha)	iStrOffset++
			DRAW_DEBUG_TEXT_WITH_OFFSET(sClubHouseMissionMenu.tl23TriggerRoom, vDebugLoc,
					0, iStrOffset*10, 255-iRed, 255-iGreen, 255-iBlue, iAlpha)	iStrOffset++
		ENDIF
		
		TIME_DATATYPE timestamp = GlobalServerBD_FM.ClubhouseMissionData.timestamp	//GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.timestamp
		
		
		TEXT_LABEL_15 tl15_time = GET_TEXT_OF_TIME(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), timestamp), TRUE)
		TEXT_LABEL_15 tl15_timediff = GET_TEXT_OF_TIME(g_sMPTunables.iBIKER_CLUBHOUSE_WALL_REFRESH*1000, TRUE)
		
		str  = "timestamp:"
		str += GET_TIME_AS_STRING(timestamp)
		str += " diff:"
		str += tl15_time
		str += " < "
		str += tl15_timediff
		DRAW_DEBUG_TEXT_WITH_OFFSET(str, vDebugLoc,
				0, iStrOffset*10, 000, 000, 256, 256)	iStrOffset++
		
		INT i
		REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
			CLUBHOUSE_MISSION_ENUM eCHMission_player = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[i])
			CLUBHOUSE_MISSION_ENUM eCHMission_server = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i])
			CLUBHOUSE_MISSION_ENUM eCHMission_local = sClubHouseMissionMenu.eCHMissions[i]
			
			IF eCHMission_player != CLUBHOUSE_MISSION_invalid
				HUD_COLOURS eHudColour = HUD_COLOUR_BLUE
				TEXT_LABEL_63 str2 = GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission_player))
				
				IF eCHMission_player != eCHMission_server
					eHudColour = HUD_COLOUR_RED
					str2 += "!"
					IF eCHMission_server != CLUBHOUSE_MISSION_invalid
						str2 += GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission_server))
					ELSE
						str2 += "invalid"
					ENDIF
				ENDIF
				IF eCHMission_player != eCHMission_local
					eHudColour = HUD_COLOUR_RED
					str2 += "$"
					IF eCHMission_local != CLUBHOUSE_MISSION_invalid
						str2 += GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission_local))
					ELSE
						str2 += "invalid"
					ENDIF
				ENDIF
				
				str2 += " "
				str2 += GET_CH_MISSION_MENU_DESCRIPTION(sClubHouseMissionMenu.pOwnerID, eCHMission_player)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(str2)
					INT iRed2=0, iGreen2=0, iBlue2=255, iAlpha2=255
					GET_HUD_COLOUR(eHudColour, iRed2, iGreen2, iBlue2, iAlpha2)
					DRAW_DEBUG_TEXT_WITH_OFFSET(str2, vDebugLoc, 0, iStrOffset*10, iRed2, iGreen2, iBlue2, iAlpha2)	iStrOffset++
				ENDIF
			ELSE
				TEXT_LABEL_63 str2 = "invalid"
				
				IF eCHMission_player != eCHMission_server
					str2 += "!"
					IF eCHMission_server != CLUBHOUSE_MISSION_invalid
						str2 += GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission_server))
					ELSE
						str2 += "invalid"
					ENDIF
				ELIF eCHMission_player != eCHMission_local
					str2 += "$"
					IF eCHMission_local != CLUBHOUSE_MISSION_invalid
						str2 += GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission_local))
					ELSE
						str2 += "invalid"
					ENDIF
				ENDIF
				
				DRAW_DEBUG_TEXT_WITH_OFFSET(str2, vDebugLoc, 0, iStrOffset*10, 000, 000, 256, 256)	iStrOffset++
			ENDIF
		ENDREPEAT
		
		str  = "iHighlightedMission["
		str += NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)
		str += "]"
		str += GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iHighlightedMission
		str += " / "
		str += sClubHouseMissionMenu.iCurrentMission
		DRAW_DEBUG_TEXT_WITH_OFFSET(str, vDebugLoc, 0, iStrOffset*10, 255-iRed, 255-iGreen, 255-iBlue, iAlpha)	iStrOffset++
		
	ENDIF
ENDPROC
#ENDIF


PROC MAINTAIN_CLUBHOUSE_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,INT iCurrentProperty, PLAYER_INDEX clubhouseOwner)
	SWITCH sWLoadoutCustomization.eCustomizationStage
		CASE VMC_STAGE_INIT
			CLEAN_UP_CLUBHOUSE_GUN_LOCKER(sWLoadoutCustomization)
			
			sWLoadoutCustomization.eCustomizationStage = VWC_STAGE_WAITING_TO_TRIGGER
		BREAK
		
		CASE VWC_STAGE_WAITING_TO_TRIGGER 				
			MANAGE_CLUBHOUSE_GUN_LOCKER_WEAPONS(sWLoadoutCustomization,clubhouseOwner,iCurrentProperty)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PLAYER_IN_BIKER_GUN_LOCKER_TRIGGER_AREA(iCurrentProperty)
			AND IS_SAFE_TO_START_CLUBHOUSE_GUN_LOCKER_MENUS(TRUE)
			AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
			AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND IS_PED_STILL(PLAYER_PED_ID())

				GET_NUM_AVAILABLE_WEAPON_GROUP(sWLoadoutCustomization)
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_SIT")
					CLEAR_HELP()
				ENDIF
				IF sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
					IF NOT IS_HELP_MESSAGE_ON_SCREEN()
						IF NOT IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_GUN_VAULT_DOOR_OPENED)
							REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU")
						ELSE 
							REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU_C")
						ENDIF
					ENDIF	
				ENDIF
				IF NOT IS_INTERACTION_MENU_OPEN()
					IF HAS_CONTEXT_BUTTON_TRIGGERED(sWLoadoutCustomization.iVaultWeaponContext)
						LOAD_MENU_ASSETS()
						BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
						//PRINT_WEAPON_LOADOUT_PACKED_STAT_INFO()
						IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
							TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 276.0970, 0)	
						ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
							TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), 178.3187, 0)	
						ENDIF
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)

						RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
						sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CUSTOMIZING 
					ENDIF
				ENDIF	
			ELSE
				RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
			ENDIF
		BREAK
		CASE VMC_STAGE_CUSTOMIZING
			PROCESS_VAULT_WEAPON_LOADOUT_MENU(sWLoadoutCustomization)
		BREAK
		CASE VMC_STAGE_CLEANUP
			CLEAN_UP_CLUBHOUSE_GUN_LOCKER(sWLoadoutCustomization)
			START_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAR_CLUBHOUSE_MISSION_FAIL_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHM_INITF0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHM_INITF1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHM_INITF2")
		CLEAR_HELP()
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintain clubhouse mission menu 
PROC MAINTAIN_CLUBHOUSE_MISSION_MENU(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu,INT iCurrentProperty)
	
	SWITCH sClubHouseMissionMenu.eMenuState
		
		CASE CHM_MENU_STAGE_INIT
			INIT_CHM_MENU(sClubHouseMissionMenu,iCurrentProperty)
		BREAK 
		CASE CHM_MENU_STAGE_WAITING_TO_TRIGGER

			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PLAYER_FACING_MISSION_MENU(sClubHouseMissionMenu)
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),sClubHouseMissionMenu.sTriggerOffset.vLoc) < fCONST_CLUBHOUSE_MISSION_TRIGGER_RADIUS
				IF IS_SAFE_TO_START_CLUBHOUSE_MISSION_MENU()
					IF sClubHouseMissionMenu.iCHMissionMenuContext = NEW_CONTEXT_INTENTION
						CLEAR_CLUBHOUSE_MISSION_FAIL_HELP()
						REGISTER_CONTEXT_INTENTION(sClubHouseMissionMenu.iCHMissionMenuContext, CP_HIGH_PRIORITY, "CHM_INIT")
					ENDIF
					IF NOT IS_INTERACTION_MENU_OPEN()
						IF HAS_CONTEXT_BUTTON_TRIGGERED(sClubHouseMissionMenu.iCHMissionMenuContext)
						//	LOAD_MENU_ASSETS()
						//	BUILD_CH_MISSION_MAIN_MENU(sClubHouseMissionMenu)
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
							RELEASE_CONTEXT_INTENTION(sClubHouseMissionMenu.iCHMissionMenuContext)
							SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_ENTER_ANIM)
						ENDIF
					ENDIF
				ELSE
					IF sClubHouseMissionMenu.iCHMissionMenuContext != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(sClubHouseMissionMenu.iCHMissionMenuContext)
					ENDIF
					
					IF IS_PHONE_ONSCREEN()
					OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
					OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
					OR MPGlobals.PlayerInteractionData.iTargetPlayerInt != -1 // interaction menu open
					OR IS_BROWSER_OPEN() //browser open
					OR IS_SELECTOR_ONSCREEN()
					OR IS_CUSTOM_MENU_ON_SCREEN()
					OR IS_CUTSCENE_ACTIVE()
					OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_SELL
					OR (IS_PLAYER_USING_OFFICE_SEATID_VALID() OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_SIT"))
					OR IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED()
						//do nothing, using internet
						CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_CLUBHOUSE_MISSION_MENU: do nothing, using internet")
						CLEAR_CLUBHOUSE_MISSION_FAIL_HELP()
						
					ELIF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
						
						// Ignore keypress when it's not safe.
						BOOL bKeyPressSafe
						bKeyPressSafe = TRUE
						IF IS_PAUSE_MENU_ACTIVE()
						OR IS_SYSTEM_UI_BEING_DISPLAYED()
						OR IS_WARNING_MESSAGE_ACTIVE()
						OR NETWORK_TEXT_CHAT_IS_TYPING()
						OR g_sShopSettings.bProcessStoreAlert
							bKeyPressSafe = FALSE
						ELIF NETWORK_IS_ACTIVITY_SESSION()						
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHM_INITF1")
								PRINT_HELP("CHM_INITF1")			//You cannot access Clubhouse missions while on a mission.
							ENDIF
							bKeyPressSafe = FALSE
						ELIF  GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG() // url:bugstar:7776059 - Biker Clubhouse - Hit The Roof - Players on mission were not shown as being part of the MC on the D-pad down menu / player list. 
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHM_INITF2")
								PRINT_HELP("CHM_INITF2")			//You need to be the President of a Motorcycle Club to access Clubhouse Contracts.
							ENDIF
							bKeyPressSafe = FALSE
						ELIF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()	//url:bugstar:2987321 - Weapon of choice - Rival local player was able to kill the targets after starting their own MC during the mission
							bKeyPressSafe = FALSE
						ELIF GB_GET_NUM_BOSSES_IN_SESSION() >= GB_GET_MAX_NUM_GANGS()
							PRINT_HELP("CHM_INITF2")
							bKeyPressSafe = FALSE
						ENDIF
						
						IF bKeyPressSafe
						AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CHM_INITF0")
							PRINT_HELP_FOREVER("CHM_INITF0")				//You need to be the President of a Motorcycle Club to access Clubhouse missions. ~n~Press ~INPUT_FRONTEND_ACCEPT~ to found a Motorcycle Club.
						ENDIF
						
						IF (bKeyPressSafe AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
									
						
						//	SET_BIT(iBoolsBitSet2, bi2_StartBeingBoss)
							#IF IS_DEBUG_BUILD
							IF GET_COMMANDLINE_PARAM_EXISTS("sc_GBAlwaysAllowRename")
						//		SET_BIT(iMaintainBitSet, biM_KeyboardInUse)
						//		SET_BIT(iBoolsBitSet2, bi2_RenameOrganization)
								IF NOT IS_TEMP_PASSIVE_MODE_ENABLED()
									SET_TEMP_PASSIVE_MODE()
								ENDIF
							ELSE
							#ENDIF
								GB_BOSS_CREATE_GANG(TRUE, GT_BIKER)
						//		CLEAR_BIT(iBoolsBitSet, biConfirmSet)
						//		CLEAR_BIT(iBoolsBitSet, biListenForDoubleTap)
						//		CPRINTLN(DEBUG_AMBIENT, "[Script_PIM] ciPI_SUB_MENU_MAGNATE - selected ciPI_TYPE_M2_MAGNATE_BOSS_SETTING ")
						//		g_bPIM_ResetMenuNow = TRUE
						//		ePiStage = ePI_CLEANUP
						//		CLEAR_BIT(iBoolsBitSet2, bi2_StartBeingBoss)
								TEXT_LABEL_63 stName
								stName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_GB_GANG_NAME, MP_STAT_GB_GANG_NAME2)
								IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
									stName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_MC_GANG_NAME, MP_STAT_MC_GANG_NAME2)
								ENDIF
								IF IS_STRING_NULL_OR_EMPTY(stName)
						//			SET_BIT(iMaintainBitSet, biM_KeyboardInUse)
						//			SET_BIT(iBoolsBitSet2, bi2_RenameOrganization)
									IF NOT IS_TEMP_PASSIVE_MODE_ENABLED()
										SET_TEMP_PASSIVE_MODE()
									ENDIF
								ELSE
									CLEANUP_TEMP_PASSIVE_MODE()
									GB_SET_ORGANIZATION_NAME_ON_ENTRY_TO_FM()
									GB_SET_OFFICE_NAME_ON_ENTRY_TO_FM()
								ENDIF
							#IF IS_DEBUG_BUILD
							ENDIF
							#ENDIF
						//	CLEAR_BIT(iBoolsBitSet2, bi2_LocalWarningMessageActive)
							CLEAR_CLUBHOUSE_MISSION_FAIL_HELP()
							
						ENDIF
						
					ELSE
						//some other unsafe reason...
						CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_CLUBHOUSE_MISSION_MENU: some other unsafe reason...")
						
					ENDIF
				ENDIF
			ELSE
				CLEAR_CLUBHOUSE_MISSION_FAIL_HELP()
				IF sClubHouseMissionMenu.iCHMissionMenuContext != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(sClubHouseMissionMenu.iCHMissionMenuContext)
				ENDIF
			ENDIF
		BREAK
		CASE CHM_MENU_STAGE_ENTER_ANIM
			SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_SELECT_MISSION)	
		BREAK
		CASE CHM_MENU_STAGE_SELECT_MISSION
			PROCESS_CH_MISSION_MENU(sClubHouseMissionMenu,iCurrentProperty)
		BREAK
		CASE CHM_MENU_STAGE_EXIT_ANIM
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			SET_CLUBHOUSE_MISSION_MENU_STATE(sClubHouseMissionMenu,CHM_MENU_STAGE_EXIT_ANIM)	
		BREAK
		CASE CHM_MENU_STAGE_CLEANUP
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			CLEANUP_CH_MISSION_MENU(sClubHouseMissionMenu)
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC VECTOR GET_CLUBHOUSE_JUKEBOX_COORDS(INT iCurrentProperty)
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		RETURN <<1001.0933, -3171.0508, -35.0421>>
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN <<1122.5051, -3152.9944, -38.0211>>
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

/// PURPOSE:
///    Returns the name of a playlist for given ID
FUNC STRING GET_CLUBHOUSE_PLAYLIST_NAME_FROM_ID(INT iPlaylistID)
	SWITCH iPlaylistID
		CASE 0 RETURN "HIDDEN_RADIO_BIKER_CLASSIC_ROCK"
		CASE 1 RETURN "HIDDEN_RADIO_BIKER_MODERN_ROCK"
		CASE 2 RETURN "HIDDEN_RADIO_BIKER_HIP_HOP"
		CASE 3 RETURN "HIDDEN_RADIO_BIKER_PUNK"
		#IF FEATURE_TUNER
		CASE 4 RETURN "RADIO_36_AUDIOPLAYER"
		#ENDIF
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "<JUKEBOX> GET_CLUBHOUSE_PLAYLIST_NAME_FROM_ID invalid playist ID: ", iPlaylistID)
	RETURN "HIDDEN_RADIO_BIKER_CLASSIC_ROCK"
ENDFUNC

/// PURPOSE:
///    Sets the station for all emitters in the current clubhouse
PROC CHANGE_CLUBHOUSE_JUKEBOX_PLAYLIST(INT iCurrentProperty, INT iPlaylistID)
	
	STRING sPlaylistName = GET_CLUBHOUSE_PLAYLIST_NAME_FROM_ID(iPlaylistID)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "<JUKEBOX> CHANGE_CLUBHOUSE_JUKEBOX_PLAYLIST Local Playlist changed to ", iPlaylistID)	
	
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		SET_EMITTER_RADIO_STATION("SE_bkr_biker_dlc_int_02_REC", sPlaylistName)
		SET_EMITTER_RADIO_STATION("SE_bkr_biker_dlc_int_02_GRG", sPlaylistName)
		EXIT
		
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		SET_EMITTER_RADIO_STATION("SE_bkr_biker_dlc_int_01_BAR", sPlaylistName)
		SET_EMITTER_RADIO_STATION("SE_bkr_biker_dlc_int_01_GRG", sPlaylistName)
		SET_EMITTER_RADIO_STATION("SE_bkr_biker_dlc_int_01_REC", sPlaylistName)
		EXIT
		
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "CHANGE_CLUBHOUSE_JUKEBOX_PLAYLIST invalid property ID: ", iCurrentProperty)
ENDPROC

/// PURPOSE:
///    Turn on or off the clubhouse static emitters
PROC SET_CLUBHOUSE_JUKEBOX_STATIC_EMITTERS_ON_OFF_STATE(INT iCurrentProperty, BOOL bEnabled)

	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		SET_STATIC_EMITTER_ENABLED("SE_bkr_biker_dlc_int_02_REC", bEnabled)
		SET_STATIC_EMITTER_ENABLED("SE_bkr_biker_dlc_int_02_GRG", bEnabled)
		
		EXIT
		
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		SET_STATIC_EMITTER_ENABLED("SE_bkr_biker_dlc_int_01_BAR", bEnabled)
		SET_STATIC_EMITTER_ENABLED("SE_bkr_biker_dlc_int_01_GRG", bEnabled)
		SET_STATIC_EMITTER_ENABLED("SE_bkr_biker_dlc_int_01_REC", bEnabled)
		
		EXIT
		
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "<JUKEBOX> SET_CLUBHOUSE_JUKEBOX_STATIC_EMITTERS_ON_OFF_STATE invalid property ID: ", iCurrentProperty)
ENDPROC

FUNC INT GET_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID(PLAYER_INDEX piPlayer)
	RETURN GlobalPlayerBD_FM[NATIVE_TO_INT(piPlayer)].propertyDetails.iClubhouseFavouritePlaylist
ENDFUNC

FUNC TEXT_LABEL_15 GET_CLUBHOUSE_JB_PLAYLIST_TEXT_LABEL_FROM_ID(INT iPlaylistID)
	TEXT_LABEL_15 tlName = "JBOX_PLIST_"
	tlName += iPlaylistID
	
	RETURN tlName
ENDFUNC

FUNC VECTOR GET_CLUBHOUSE_WALL_CREW_EMBLEM_COORDS(INT iCurrentProperty)
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		RETURN <<1013.8388, -3170.3479, -33.7679>>
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN <<1122.9613, -3142.2639, -36.1325>>
	ENDIF
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_CLUBHOUSE_TABLE_CREW_EMBLEM_COORDS(INT iCurrentProperty)
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		RETURN <<1006.9067, -3150.9619, -39.1031>>
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN <<1115.4890, -3145.0930, -37.2305>>
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS(CLUBHOUSE_DISPLAY_EMBLEMS_STRUCT& sDisplayEmblems, INT iCurrentProperty)
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		SWITCH sDisplayEmblems.eEmblemState
			CASE CHM_EMBLEMS_STATE_INIT
				
				GAMER_HANDLE sPropertyOwnerGH
				GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), sPropertyOwnerGH)
				IF IS_GAMER_HANDLE_VALID(sPropertyOwnerGH)
					IF NETWORK_IS_GAMER_IN_MY_SESSION(sPropertyOwnerGH)
						sDisplayEmblems.pOwnerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sPropertyOwnerGH)
						IF sDisplayEmblems.pOwnerID = PLAYER_ID()
							PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - LOCAL PLAYER IS THE CLUBHOUSE OWNER")
						ELSE
							PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - LOCAL PLAYER IS *NOT THE CLUBHOUSE OWNER")
						ENDIF
					ENDIF
				
					IF NETWORK_CLAN_PLAYER_GET_DESC(sDisplayEmblems.memberInfo, SIZE_OF(sDisplayEmblems.memberInfo), sPropertyOwnerGH)
						sDisplayEmblems.iCrewID = sDisplayEmblems.memberInfo.Id
					ENDIF
				ENDIF
				
				IF sDisplayEmblems.pOwnerID != INVALID_PLAYER_INDEX()
					IF (GB_IS_GLOBAL_CLIENT_BIT0_SET(sDisplayEmblems.pOwnerID, eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
					OR GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
					OR (DOES_ENTITY_EXIST(GET_PLAYER_PED(sDisplayEmblems.pOwnerID)) AND GB_IS_THIS_PLAYERS_GANG_NAME_BLOCKED(sDisplayEmblems.pOwnerID)))
					AND NOT IS_DURANGO_AND_CAN_VIEW_GAMER_USER_CONTENT(sDisplayEmblems.pOwnerID)
						sDisplayEmblems.bRestrictedAccount = TRUE
					ENDIF
				ELSE
					IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PLAYER_ID(), eGB_GLOBAL_CLIENT_BITSET_0_RESTRICTED_ACCOUNT)
						sDisplayEmblems.bRestrictedAccount = TRUE
					ENDIF
				ENDIF
				
				PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - Emblem Global Data: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sDisplayEmblems.pOwnerID)].iClubhouseEmblem)
				
				IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(sDisplayEmblems.pOwnerID)].iClubhouseEmblem > 0
				
					TEXT_LABEL_31 tlCustomLogo
					tlCustomLogo = "MPClubPreset"
					tlCustomLogo += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sDisplayEmblems.pOwnerID)].iClubhouseEmblem
					
					REQUEST_STREAMED_TEXTURE_DICT(tlCustomLogo)
					IF HAS_STREAMED_TEXTURE_DICT_LOADED(tlCustomLogo)
						sDisplayEmblems.bRequestCrewEmblem = FALSE
						sDisplayEmblems.tlCrewTxd = tlCustomLogo
						PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - [0] Changing to state: CHM_EMBLEMS_STATE_LINK_RT - tlCrewTxd: ", sDisplayEmblems.tlCrewTxd)
						sDisplayEmblems.eEmblemState = CHM_EMBLEMS_STATE_LINK_RT
					ENDIF
				
				ELIF IS_PLAYER_IN_ACTIVE_CLAN(sPropertyOwnerGH)
				AND NOT sDisplayEmblems.bRestrictedAccount
					IF NETWORK_CLAN_REQUEST_EMBLEM(sDisplayEmblems.iCrewID)
						IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(sPropertyOwnerGH, sDisplayEmblems.tlCrewTxd)
							PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - [1] Changing to state: CHM_EMBLEMS_STATE_LINK_RT - tlCrewTxd: ", sDisplayEmblems.tlCrewTxd)
							sDisplayEmblems.bRequestCrewEmblem = TRUE
							sDisplayEmblems.eEmblemState = CHM_EMBLEMS_STATE_LINK_RT
						ENDIF
					ENDIF
				ELSE
					CREATE_MODEL_HIDE(GET_CLUBHOUSE_WALL_CREW_EMBLEM_COORDS(iCurrentProperty), 20.0, BKR_PROP_RT_CLUBHOUSE_TABLE, TRUE)
					CREATE_MODEL_HIDE(GET_CLUBHOUSE_TABLE_CREW_EMBLEM_COORDS(iCurrentProperty), 20.0, BKR_PROP_RT_CLUBHOUSE_TABLE, TRUE)
					PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - tlCrewTxd: ", sDisplayEmblems.tlCrewTxd, " is NULL - Hiding the RT Model")
					sDisplayEmblems.tlCrewTxd = ""
					sDisplayEmblems.bRequestCrewEmblem = FALSE
					sDisplayEmblems.eEmblemState = CHM_EMBLEMS_STATE_UPDATE_SCALEFORM
				ENDIF
				
			BREAK
			CASE CHM_EMBLEMS_STATE_LINK_RT
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED("clubhouse_table")  //prop_rt_clubhouse_table
					REGISTER_NAMED_RENDERTARGET("clubhouse_table")
					IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_RT_CLUBHOUSE_TABLE)
						LINK_NAMED_RENDERTARGET(BKR_PROP_RT_CLUBHOUSE_TABLE)
						IF sDisplayEmblems.iRenderTargetID = -1
							sDisplayEmblems.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("clubhouse_table")
						ENDIF
					ENDIF
				ENDIF
				PRINTLN("MAINTAIN_CLUBHOUSE_DISPLAY_EMBLEMS - Changing to state: CHM_EMBLEMS_STATE_UPDATE_SCALEFORM - iRenderTargetID: ", sDisplayEmblems.iRenderTargetID)
				sDisplayEmblems.eEmblemState = CHM_EMBLEMS_STATE_UPDATE_SCALEFORM
			BREAK
			CASE CHM_EMBLEMS_STATE_UPDATE_SCALEFORM
				// Constantly request crew emblem to avoid B* 3101023
				IF sDisplayEmblems.bRequestCrewEmblem
					NETWORK_CLAN_REQUEST_EMBLEM(sDisplayEmblems.iCrewID)
				ENDIF
				
				SET_TEXT_RENDER_ID(sDisplayEmblems.iRenderTargetID)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sDisplayEmblems.tlCrewTxd)
					DRAW_SPRITE_NAMED_RENDERTARGET(sDisplayEmblems.tlCrewTxd, sDisplayEmblems.tlCrewTxd, 0.5, 0.5, 1.0, 1.0, 90.0, 255, 255, 255, 255)
				ENDIF
				
				RESET_SCRIPT_GFX_ALIGN()
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC CLEANUP_CLUBHOUSE_DISPLAY_EMBLEMS(CLUBHOUSE_DISPLAY_EMBLEMS_STRUCT& sDisplayEmblems, INT iCurrentProperty, BOOL bReleaseRT = TRUE)
	IF bReleaseRT
		IF IS_NAMED_RENDERTARGET_REGISTERED("clubhouse_table")   
			RELEASE_NAMED_RENDERTARGET("clubhouse_table")
		ENDIF
		sDisplayEmblems.iRenderTargetID = -1
	ENDIF
	sDisplayEmblems.eEmblemState = CHM_EMBLEMS_STATE_INIT
	sDisplayEmblems.bRestrictedAccount = FALSE
	sDisplayEmblems.bRequestCrewEmblem = FALSE
	NETWORK_CLAN_RELEASE_EMBLEM(sDisplayEmblems.iCrewID)
	IF NOT IS_STRING_NULL_OR_EMPTY(sDisplayEmblems.tlCrewTxd)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sDisplayEmblems.tlCrewTxd)
	ENDIF
	REMOVE_MODEL_HIDE(GET_CLUBHOUSE_WALL_CREW_EMBLEM_COORDS(iCurrentProperty), 20.0, BKR_PROP_RT_CLUBHOUSE_TABLE)
	REMOVE_MODEL_HIDE(GET_CLUBHOUSE_TABLE_CREW_EMBLEM_COORDS(iCurrentProperty), 20.0, BKR_PROP_RT_CLUBHOUSE_TABLE)
ENDPROC

PROC DEFAULT_CLUBHOUSE_MEMORIAL_WALL_DATA(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	sMemorialWall.pPresidentID 				= INVALID_PLAYER_INDEX()
	sMemorialWall.pVPresidentID				= INVALID_PLAYER_INDEX()
	sMemorialWall.pRoadCaptainID			= INVALID_PLAYER_INDEX()
	sMemorialWall.pSergeantID 				= INVALID_PLAYER_INDEX()
	sMemorialWall.pEnforcerID 				= INVALID_PLAYER_INDEX()
	
//	sMemorialWall.tlPresidentHeadShot  	 	= ""
//	sMemorialWall.tlVPresidentHeadShot  	= ""
//	sMemorialWall.tlRoadCaptainHeadShot 	= ""
//	sMemorialWall.tlSergeantHeadShot    	= ""
//	sMemorialWall.tlEnforcerHeadShot    	= ""
	
	sMemorialWall.pedPresidentHeadShotID	= NULL
	sMemorialWall.pedVPresidentHeadShotID	= NULL
	sMemorialWall.pedRoadCaptainHeadShotID	= NULL
	sMemorialWall.pedSergeantHeadShotID		= NULL
	sMemorialWall.pedEnforcerHeadShotID		= NULL
	
	sMemorialWall.iTexturesRequested 		= 0
	sMemorialWall.iTexturesLoaded 			= 0
	sMemorialWall.iValidPlayerTotal			= 0
	sMemorialWall.iPedHeadshotTotal			= 0
	
ENDPROC

PROC GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	// President
	GAMER_HANDLE sPropertyOwnerGH
	GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), sPropertyOwnerGH)
	IF IS_GAMER_HANDLE_VALID(sPropertyOwnerGH)
		IF NETWORK_IS_GAMER_IN_MY_SESSION(sPropertyOwnerGH)
		
			PLAYER_INDEX pPlayer 
			pPlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sPropertyOwnerGH)
			sMemorialWall.pClubhouseOwner = pPlayer
			PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - OWNER ID: ", NATIVE_TO_INT(pPlayer))
			
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pPlayer, TRUE)
				PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - OWNER IS MEMBER OF A BIKER GANG")
				
				IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(pPlayer)
					PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - OWNER BOSS OF BIKER GANG, USING HEADSHOT")
					sMemorialWall.pPresidentID = pPlayer
				ELSE
					PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - OWNER ONLY MEMBER OF BIKER GANG, GETTING BOSS HEADSHOT")
					sMemorialWall.pPresidentID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				ENDIF
			ELSE
				IF NOT GB_IS_PLAYER_MEMBER_OF_GANG_TYPE(pPlayer, TRUE, GT_VIP)
					PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - OWNER NOT IN BIKER GANG, USING HEADSHOT")
					sMemorialWall.pPresidentID = pPlayer
				ELSE
					PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - OWNER IN VIP ORG, RETURNING INVALID PLAYER INDEX")
					sMemorialWall.pPresidentID = INVALID_PLAYER_INDEX()
				ENDIF
			ENDIF		
			
		ENDIF
		IF sMemorialWall.pClubhouseOwner != INVALID_PLAYER_INDEX()
			sMemorialWall.iTotalGangMembers = GET_NUMBER_OF_PLAYERS_IN_PLAYERS_GANG(sMemorialWall.pPresidentID)
		ENDIF
	ENDIF
	
	// Gang Members
	REPEAT NUM_NETWORK_PLAYERS sMemorialWall.iParticipant
		sMemorialWall.pParticipantId = INT_TO_PLAYERINDEX(sMemorialWall.iParticipant)
		IF sMemorialWall.pParticipantId != INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(sMemorialWall.pParticipantId, FALSE)
				IF GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(sMemorialWall.pParticipantId, sMemorialWall.pPresidentID, FALSE)
				AND sMemorialWall.pParticipantId != sMemorialWall.pPresidentID
				AND NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sMemorialWall.pParticipantId)
					SWITCH GB_GET_PLAYER_GANG_ROLE(sMemorialWall.pParticipantId)
						CASE GR_VP					
							sMemorialWall.pVPresidentID = sMemorialWall.pParticipantId
							PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - Vice President ID: ", NATIVE_TO_INT(sMemorialWall.pVPresidentID))
						BREAK
						CASE GR_ROAD_CAPTAIN		
							sMemorialWall.pRoadCaptainID = sMemorialWall.pParticipantId
							PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - Road Captain ID: ", NATIVE_TO_INT(sMemorialWall.pRoadCaptainID))
						BREAK
						CASE GR_SERGEANT_AT_ARMS	
							sMemorialWall.pSergeantID = sMemorialWall.pParticipantId
							PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - Sergeant ID: ", NATIVE_TO_INT(sMemorialWall.pSergeantID))
						BREAK
						CASE GR_ENFORCER			
							sMemorialWall.pEnforcerID = sMemorialWall.pParticipantId
							PRINTLN("MEMORIAL WALL - GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES - Enforcer ID: ", NATIVE_TO_INT(sMemorialWall.pEnforcerID))
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
        ENDIF
	ENDREPEAT
	
	IF sMemorialWall.pPresidentID != INVALID_PLAYER_INDEX()
		sMemorialWall.iValidPlayerTotal++
	ENDIF
	IF sMemorialWall.pVPresidentID != INVALID_PLAYER_INDEX()
		sMemorialWall.iValidPlayerTotal++
	ENDIF
	IF sMemorialWall.pRoadCaptainID != INVALID_PLAYER_INDEX()
		sMemorialWall.iValidPlayerTotal++
	ENDIF
	IF sMemorialWall.pSergeantID != INVALID_PLAYER_INDEX()
		sMemorialWall.iValidPlayerTotal++
	ENDIF
	IF sMemorialWall.pEnforcerID != INVALID_PLAYER_INDEX()
		sMemorialWall.iValidPlayerTotal++
	ENDIF
ENDPROC

FUNC BOOL GET_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_IDS(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	// President
	IF sMemorialWall.pPresidentID != INVALID_PLAYER_INDEX()
		sMemorialWall.pedPresidentHeadShotID = Get_HeadshotID_For_Player(sMemorialWall.pPresidentID)
		IF sMemorialWall.pedPresidentHeadShotID != NULL
			sMemorialWall.iPedHeadshotTotal++
		ENDIF
	ENDIF
	
	// Vice President
	IF sMemorialWall.pVPresidentID != INVALID_PLAYER_INDEX()
		sMemorialWall.pedVPresidentHeadShotID = Get_HeadshotID_For_Player(sMemorialWall.pVPresidentID)
		IF sMemorialWall.pedVPresidentHeadShotID != NULL
			sMemorialWall.iPedHeadshotTotal++
		ENDIF
	ENDIF
	
	// Road Captain
	IF sMemorialWall.pRoadCaptainID != INVALID_PLAYER_INDEX()
		sMemorialWall.pedRoadCaptainHeadShotID = Get_HeadshotID_For_Player(sMemorialWall.pRoadCaptainID)
		IF sMemorialWall.pedRoadCaptainHeadShotID != NULL
			sMemorialWall.iPedHeadshotTotal++
		ENDIF
	ENDIF
	
	// Enforcer
	IF sMemorialWall.pEnforcerID != INVALID_PLAYER_INDEX()
		sMemorialWall.pedEnforcerHeadShotID = Get_HeadshotID_For_Player(sMemorialWall.pEnforcerID)
		IF sMemorialWall.pedEnforcerHeadShotID != NULL
			sMemorialWall.iPedHeadshotTotal++
		ENDIF
	ENDIF
	
	// Sergeant
	IF sMemorialWall.pSergeantID != INVALID_PLAYER_INDEX()
		sMemorialWall.pedSergeantHeadShotID = Get_HeadshotID_For_Player(sMemorialWall.pSergeantID)
		IF sMemorialWall.pedSergeantHeadShotID != NULL
			sMemorialWall.iPedHeadshotTotal++
		ENDIF
	ENDIF
	
	IF sMemorialWall.iPedHeadshotTotal = sMemorialWall.iValidPlayerTotal
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	// President
	IF sMemorialWall.pPresidentID != INVALID_PLAYER_INDEX()
		IF sMemorialWall.pedPresidentHeadShotID != NULL
			sMemorialWall.tlPresidentHeadShot = GET_PEDHEADSHOT_TXD_STRING(sMemorialWall.pedPresidentHeadShotID)
		ENDIF
	ENDIF
	
	// Vice President
	IF sMemorialWall.pVPresidentID != INVALID_PLAYER_INDEX()
		IF sMemorialWall.pedVPresidentHeadShotID != NULL
			sMemorialWall.tlVPresidentHeadShot = GET_PEDHEADSHOT_TXD_STRING(sMemorialWall.pedVPresidentHeadShotID)
		ENDIF
	ENDIF
	
	// Road Captain
	IF sMemorialWall.pRoadCaptainID != INVALID_PLAYER_INDEX()
		IF sMemorialWall.pedRoadCaptainHeadShotID != NULL
			sMemorialWall.tlRoadCaptainHeadShot = GET_PEDHEADSHOT_TXD_STRING(sMemorialWall.pedRoadCaptainHeadShotID)
		ENDIF
	ENDIF
	
	// Enforcer
	IF sMemorialWall.pEnforcerID != INVALID_PLAYER_INDEX()
		IF sMemorialWall.pedEnforcerHeadShotID != NULL
			sMemorialWall.tlEnforcerHeadShot = GET_PEDHEADSHOT_TXD_STRING(sMemorialWall.pedEnforcerHeadShotID)
		ENDIF
	ENDIF
	
	// Sergeant
	IF sMemorialWall.pSergeantID != INVALID_PLAYER_INDEX()
		IF sMemorialWall.pedSergeantHeadShotID != NULL
			sMemorialWall.tlSergeantHeadShot = GET_PEDHEADSHOT_TXD_STRING(sMemorialWall.pedSergeantHeadShotID)
		ENDIF
	ENDIF
	
ENDPROC

PROC REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	// President
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlPresidentHeadShot)
		REQUEST_STREAMED_TEXTURE_DICT(sMemorialWall.tlPresidentHeadShot)
		sMemorialWall.iTexturesRequested++
		PRINTLN("REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS - tlPresidentHeadShot: ", sMemorialWall.tlPresidentHeadShot)
	ENDIF
	
	// Vice President
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlVPresidentHeadShot)
		REQUEST_STREAMED_TEXTURE_DICT(sMemorialWall.tlVPresidentHeadShot)
		sMemorialWall.iTexturesRequested++
		PRINTLN("REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS - tlVPresidentHeadShot: ", sMemorialWall.tlVPresidentHeadShot)
	ENDIF
	
	// Road Captain
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlRoadCaptainHeadShot)
		REQUEST_STREAMED_TEXTURE_DICT(sMemorialWall.tlRoadCaptainHeadShot)
		sMemorialWall.iTexturesRequested++
		PRINTLN("REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS - tlRoadCaptainHeadShot: ", sMemorialWall.tlRoadCaptainHeadShot)
	ENDIF
	
	// Enforcer
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlEnforcerHeadShot)
		REQUEST_STREAMED_TEXTURE_DICT(sMemorialWall.tlEnforcerHeadShot)
		sMemorialWall.iTexturesRequested++
		PRINTLN("REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS - tlEnforcerHeadShot: ", sMemorialWall.tlEnforcerHeadShot)
	ENDIF
	
	// Sergeant
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlSergeantHeadShot)
		REQUEST_STREAMED_TEXTURE_DICT(sMemorialWall.tlSergeantHeadShot)
		sMemorialWall.iTexturesRequested++
		PRINTLN("REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS - tlSergeantHeadShot: ", sMemorialWall.tlSergeantHeadShot)
	ENDIF
	
	PRINTLN("REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS - sMemorialWall.iTexturesRequested: ", sMemorialWall.iTexturesRequested)

ENDPROC

PROC HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	// President
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlPresidentHeadShot)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sMemorialWall.tlPresidentHeadShot)
			sMemorialWall.iTexturesLoaded++
			PRINTLN("HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED - tlPresidentHeadShot: ", sMemorialWall.tlPresidentHeadShot)
		ENDIF
	ENDIF
	
	// Vice President
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlVPresidentHeadShot)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sMemorialWall.tlVPresidentHeadShot)
			sMemorialWall.iTexturesLoaded++
			PRINTLN("HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED - tlVPresidentHeadShot: ", sMemorialWall.tlVPresidentHeadShot)
		ENDIF
	ENDIF
	
	// Road Captain
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlRoadCaptainHeadShot)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sMemorialWall.tlRoadCaptainHeadShot)
			sMemorialWall.iTexturesLoaded++
			PRINTLN("HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED - tlRoadCaptainHeadShot: ", sMemorialWall.tlRoadCaptainHeadShot)
		ENDIF
	ENDIF
	
	// Enforcer
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlEnforcerHeadShot)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sMemorialWall.tlEnforcerHeadShot)
			sMemorialWall.iTexturesLoaded++
			PRINTLN("HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED - tlEnforcerHeadShot: ", sMemorialWall.tlEnforcerHeadShot)
		ENDIF
	ENDIF
	
	// Sergeant
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlSergeantHeadShot)
		IF HAS_STREAMED_TEXTURE_DICT_LOADED(sMemorialWall.tlSergeantHeadShot)
			sMemorialWall.iTexturesLoaded++
			PRINTLN("HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED - tlSergeantHeadShot: ", sMemorialWall.tlSergeantHeadShot)
		ENDIF
	ENDIF
	
	PRINTLN("MAINTAIN_CLUBHOUSE_MEMORIAL_WALL - sMemorialWall.iTexturesLoaded: ", sMemorialWall.iTexturesLoaded)
	
ENDPROC

//// clubhouse mc memorial wall

PROC REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)

	// President
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_president")
		REGISTER_NAMED_RENDERTARGET("memorial_wall_president")
		IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_RT_MEMORIAL_PRESIDENT)
			LINK_NAMED_RENDERTARGET(BKR_PROP_RT_MEMORIAL_PRESIDENT)
			sMemorialWall.iPresidentRT = GET_NAMED_RENDERTARGET_RENDER_ID("memorial_wall_president")
			PRINTLN("REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS - iPresidentRT: ", sMemorialWall.iPresidentRT)
		ENDIF
	ENDIF
	
	// Vice President
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_vice_president")
		REGISTER_NAMED_RENDERTARGET("memorial_wall_vice_president")
		IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_RT_MEMORIAL_VICE_PRES)
			LINK_NAMED_RENDERTARGET(BKR_PROP_RT_MEMORIAL_VICE_PRES)
			sMemorialWall.iVPresidentRT = GET_NAMED_RENDERTARGET_RENDER_ID("memorial_wall_vice_president")
			PRINTLN("REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS - iVPresidentRT: ", sMemorialWall.iVPresidentRT)
		ENDIF
	ENDIF
	
	// Road Captain
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_active_01")
		REGISTER_NAMED_RENDERTARGET("memorial_wall_active_01")
		IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_RT_MEMORIAL_ACTIVE_01)
			LINK_NAMED_RENDERTARGET(BKR_PROP_RT_MEMORIAL_ACTIVE_01)
			sMemorialWall.iRoadCaptainRT = GET_NAMED_RENDERTARGET_RENDER_ID("memorial_wall_active_01")
			PRINTLN("REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS - iRoadCaptainRT: ", sMemorialWall.iRoadCaptainRT)
		ENDIF
	ENDIF
	
	// Enforcer
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_active_02")
		REGISTER_NAMED_RENDERTARGET("memorial_wall_active_02")
		IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_RT_MEMORIAL_ACTIVE_02)
			LINK_NAMED_RENDERTARGET(BKR_PROP_RT_MEMORIAL_ACTIVE_02)
			sMemorialWall.iEnforcerRT = GET_NAMED_RENDERTARGET_RENDER_ID("memorial_wall_active_02")
			PRINTLN("REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS - iEnforcerRT: ", sMemorialWall.iEnforcerRT)
		ENDIF
	ENDIF
	
	// Sergeant
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_active_03")
		REGISTER_NAMED_RENDERTARGET("memorial_wall_active_03")
		IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_RT_MEMORIAL_ACTIVE_03)
			LINK_NAMED_RENDERTARGET(BKR_PROP_RT_MEMORIAL_ACTIVE_03)
			sMemorialWall.iSergeantRT = GET_NAMED_RENDERTARGET_RENDER_ID("memorial_wall_active_03")
			PRINTLN("REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS - iSergeantRT: ", sMemorialWall.iSergeantRT)
		ENDIF
	ENDIF
	
ENDPROC

PROC DRAW_CLUBHOUSE_MEMORIAL_WALL_PRESIDENT(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	SET_TEXT_RENDER_ID(sMemorialWall.iPresidentRT)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlPresidentHeadShot)
	AND GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sMemorialWall.pPresidentID)
		DRAW_SPRITE_NAMED_RENDERTARGET(sMemorialWall.tlPresidentHeadShot, sMemorialWall.tlPresidentHeadShot, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

PROC DRAW_CLUBHOUSE_MEMORIAL_WALL_VICE_PRESIDENT(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	SET_TEXT_RENDER_ID(sMemorialWall.iVPresidentRT)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlVPresidentHeadShot)
	AND GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(sMemorialWall.pVPresidentID, sMemorialWall.pPresidentID, FALSE)
		DRAW_SPRITE_NAMED_RENDERTARGET(sMemorialWall.tlVPresidentHeadShot, sMemorialWall.tlVPresidentHeadShot, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

PROC DRAW_CLUBHOUSE_MEMORIAL_WALL_ROAD_CAPTAIN(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	SET_TEXT_RENDER_ID(sMemorialWall.iRoadCaptainRT)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlRoadCaptainHeadShot)
	AND GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(sMemorialWall.pRoadCaptainID, sMemorialWall.pPresidentID, FALSE)
		DRAW_SPRITE_NAMED_RENDERTARGET(sMemorialWall.tlRoadCaptainHeadShot, sMemorialWall.tlRoadCaptainHeadShot, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

PROC DRAW_CLUBHOUSE_MEMORIAL_WALL_SERGEANT(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	SET_TEXT_RENDER_ID(sMemorialWall.iSergeantRT)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlSergeantHeadShot)
	AND GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(sMemorialWall.pSergeantID, sMemorialWall.pPresidentID, FALSE)
		DRAW_SPRITE_NAMED_RENDERTARGET(sMemorialWall.tlSergeantHeadShot, sMemorialWall.tlSergeantHeadShot, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

PROC DRAW_CLUBHOUSE_MEMORIAL_WALL_ENFORCER(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	SET_TEXT_RENDER_ID(sMemorialWall.iEnforcerRT)
	SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sMemorialWall.tlEnforcerHeadShot)
	AND GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(sMemorialWall.pEnforcerID, sMemorialWall.pPresidentID, FALSE)
		DRAW_SPRITE_NAMED_RENDERTARGET(sMemorialWall.tlEnforcerHeadShot, sMemorialWall.tlEnforcerHeadShot, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
ENDPROC

FUNC BOOL DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall)
	
	// New Gang Members
	IF sMemorialWall.iTotalGangMembers != GET_NUMBER_OF_PLAYERS_IN_PLAYERS_GANG(sMemorialWall.pPresidentID)
		PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - MEMBER TOTAL - Old Total: ", sMemorialWall.iTotalGangMembers, " New Total: ", GET_NUMBER_OF_PLAYERS_IN_PLAYERS_GANG(sMemorialWall.pPresidentID))
		RETURN TRUE
	ENDIF
	
	IF sMemorialWall.pVPresidentID != INVALID_PLAYER_INDEX()
		IF GB_GET_PLAYER_GANG_ROLE(sMemorialWall.pVPresidentID) != GR_VP
			PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - Vice President Has Changed Role")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF sMemorialWall.pRoadCaptainID != INVALID_PLAYER_INDEX()
		IF GB_GET_PLAYER_GANG_ROLE(sMemorialWall.pRoadCaptainID) != GR_ROAD_CAPTAIN
			PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - Road Captain Has Changed Role")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF sMemorialWall.pSergeantID != INVALID_PLAYER_INDEX()
		IF GB_GET_PLAYER_GANG_ROLE(sMemorialWall.pSergeantID) != GR_SERGEANT_AT_ARMS
			PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - Sergeant Has Changed Role")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF sMemorialWall.pEnforcerID != INVALID_PLAYER_INDEX()
		IF GB_GET_PLAYER_GANG_ROLE(sMemorialWall.pEnforcerID) != GR_ENFORCER
			PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - Enforcer Has Changed Role")
			RETURN TRUE
		ENDIF
	ENDIF
	
	REPEAT GB_MAX_GANG_GOONS sMemorialWall.iParticipant
		IF sMemorialWall.pPresidentID != INVALID_PLAYER_INDEX()
	        PLAYER_INDEX pGangMember = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMemorialWall.pPresidentID)].sMagnateGangBossData.GangMembers[sMemorialWall.iParticipant]
			IF pGangMember != INVALID_PLAYER_INDEX()
				SWITCH GB_GET_PLAYER_GANG_ROLE(pGangMember)
					CASE GR_VP
						IF sMemorialWall.pVPresidentID != pGangMember
							PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - New Vice President")
							RETURN TRUE
						ENDIF
					BREAK
					CASE GR_ROAD_CAPTAIN
						IF sMemorialWall.pRoadCaptainID != pGangMember
							PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - New Road Captain")
							RETURN TRUE
						ENDIF
					BREAK
					CASE GR_SERGEANT_AT_ARMS
						IF sMemorialWall.pSergeantID != pGangMember
							PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - New Sergeant")
							RETURN TRUE
						ENDIF
					BREAK
					CASE GR_ENFORCER
						IF sMemorialWall.pEnforcerID != pGangMember
							PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - *UPDATE NEEDED* - New Enforcer")
							RETURN TRUE
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
    ENDREPEAT
	
	IF (g_bMemorialWallUpdate)
		PRINTLN("MEMORIAL WALL - DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING - Apperance Changed")	
		g_bMemorialWallUpdate = FALSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RELEASE_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS()
	
	IF IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_president")   
		RELEASE_NAMED_RENDERTARGET("memorial_wall_president")
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_vice_president")   
		RELEASE_NAMED_RENDERTARGET("memorial_wall_vice_president")
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_active_01")   
		RELEASE_NAMED_RENDERTARGET("memorial_wall_active_01")
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_active_02")   
		RELEASE_NAMED_RENDERTARGET("memorial_wall_active_02")
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("memorial_wall_active_03")   
		RELEASE_NAMED_RENDERTARGET("memorial_wall_active_03")
	ENDIF
	
ENDPROC

PROC MAINTAIN_CLUBHOUSE_MEMORIAL_WALL(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall, INT iCurrentProperty)
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		SWITCH sMemorialWall.eWallState
			CASE CHM_MEMORIAL_WALL_STATE_INIT
				
				DEFAULT_CLUBHOUSE_MEMORIAL_WALL_DATA(sMemorialWall)
				
				GET_CLUBHOUSE_MEMORIAL_WALL_BIKER_GANG_ROLES(sMemorialWall)
				
				IF GET_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_IDS(sMemorialWall)
					
					GET_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS(sMemorialWall)
					
					REQUEST_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS(sMemorialWall)
					
					HAS_CLUBHOUSE_MEMORIAL_WALL_HEADSHOTS_LOADED(sMemorialWall)
					
					// Continue if Headshots are Requested & Loaded
					IF sMemorialWall.iTexturesRequested = sMemorialWall.iTexturesLoaded
						sMemorialWall.eWallState = CHM_MEMORIAL_WALL_STATE_LINK_RT
					ENDIF
				ENDIF
				
			BREAK
			CASE CHM_MEMORIAL_WALL_STATE_LINK_RT
				
				IF sMemorialWall.iPresidentRT   = -1
				OR sMemorialWall.iVPresidentRT  = -1
				OR sMemorialWall.iRoadCaptainRT = -1
				OR sMemorialWall.iSergeantRT    = -1
				OR sMemorialWall.iEnforcerRT    = -1
					REGISTER_AND_LINK_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS(sMemorialWall)
				ENDIF
				sMemorialWall.eWallState = CHM_MEMORIAL_WALL_STATE_UPDATE_SCALEFORM
				
			BREAK
			CASE CHM_MEMORIAL_WALL_STATE_UPDATE_SCALEFORM
			
				IF g_b_RefreshBikerWall
					sMemorialWall.eWallState = CHM_MEMORIAL_WALL_STATE_INIT
					g_b_RefreshBikerWall = FALSE
					PRINTLN("MAINTAIN_CLUBHOUSE_MEMORIAL_WALL - new headshots have arrive. Refresh Wall. ")
					BREAK
				ENDIF
			
				IF DOES_CLUBHOUSE_MEMORIAL_WALL_NEED_UPDATING(sMemorialWall)
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
						//update PedHeadshots. 
						BROADCAST_GANG_STATUS_CHANGED(ALL_PLAYERS(), TRUE)
					ENDIF
					
					sMemorialWall.eWallState = CHM_MEMORIAL_WALL_STATE_INIT
				ENDIF
				
				
				
			BREAK
		ENDSWITCH
		
		IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sMemorialWall.pClubhouseOwner)
			DRAW_CLUBHOUSE_MEMORIAL_WALL_PRESIDENT(sMemorialWall)
			DRAW_CLUBHOUSE_MEMORIAL_WALL_VICE_PRESIDENT(sMemorialWall)
			DRAW_CLUBHOUSE_MEMORIAL_WALL_ROAD_CAPTAIN(sMemorialWall)
			DRAW_CLUBHOUSE_MEMORIAL_WALL_ENFORCER(sMemorialWall)
			DRAW_CLUBHOUSE_MEMORIAL_WALL_SERGEANT(sMemorialWall)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_CLUBHOUSE_MEMORIAL_WALL(CLUBHOUSE_MEMORIAL_WALL_STRUCT& sMemorialWall, BOOL bReleaseRT = TRUE)
	
	IF bReleaseRT
		RELEASE_CLUBHOUSE_MEMORIAL_WALL_RENDER_TARGETS()
		sMemorialWall.iPresidentRT				= -1
		sMemorialWall.iVPresidentRT				= -1
		sMemorialWall.iRoadCaptainRT			= -1
		sMemorialWall.iSergeantRT				= -1
		sMemorialWall.iEnforcerRT				= -1
	ENDIF
	DEFAULT_CLUBHOUSE_MEMORIAL_WALL_DATA(sMemorialWall)
	
	sMemorialWall.eWallState = CHM_MEMORIAL_WALL_STATE_INIT
	
ENDPROC

PROC MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR(CLUBHOUSE_LAPTOP_MONITOR_STRUCT& sLaptopMonitor, INT iCurrentProperty)
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		SWITCH sLaptopMonitor.eLaptopState
			CASE CHM_LAPTOP_MONITOR_STATE_INIT
				
				GAMER_HANDLE sPropertyOwnerGH
				GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), sPropertyOwnerGH)
				IF IS_GAMER_HANDLE_VALID(sPropertyOwnerGH)
					IF NETWORK_IS_GAMER_IN_MY_SESSION(sPropertyOwnerGH)
						PLAYER_INDEX pPlayer 
						pPlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sPropertyOwnerGH)
						
						IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pPlayer, TRUE)
							PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - LOCAL PLAYER IS MEMBER OF A BIKER GANG")
							
							IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(pPlayer)
								PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - LOCAL PLAYER BOSS OF BIKER GANG, USING CREW EMBLEM")
								sLaptopMonitor.pOwnerID = pPlayer
							ELSE
								PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - LOCAL PLAYER ONLY MEMBER OF BIKER GANG, GETTING BOSS EMBLEM")
								sLaptopMonitor.pOwnerID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
							ENDIF
						ELSE
							PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - LOCAL PLAYER NOT IN BIKER GANG, USING CREW EMBLEM")
							sLaptopMonitor.pOwnerID = pPlayer
						ENDIF
						
						IF sLaptopMonitor.pOwnerID = PLAYER_ID()
							PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - LOCAL PLAYER IS THE CLUBHOUSE OWNER")
						ELSE
							PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - LOCAL PLAYER IS *NOT THE CLUBHOUSE OWNER")
						ENDIF
						
					ENDIF
					
					IF NETWORK_CLAN_PLAYER_GET_DESC(sLaptopMonitor.memberInfo, SIZE_OF(sLaptopMonitor.memberInfo), sPropertyOwnerGH)
						sLaptopMonitor.iCrewID = sLaptopMonitor.memberInfo.Id
					ENDIF
				ENDIF
				
				REQUEST_STREAMED_TEXTURE_DICT("prop_screen_biker_laptop")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("prop_screen_biker_laptop")
					sLaptopMonitor.tlCrewTxd = "prop_screen_biker_laptop"
					sLaptopMonitor.eLaptopState = CHM_LAPTOP_MONITOR_STATE_LINK_RT
					PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - tlCrewTxd: ", sLaptopMonitor.tlCrewTxd, " is NULL - Displaying default background")
				ENDIF
				
			BREAK
			CASE CHM_LAPTOP_MONITOR_STATE_LINK_RT
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED("prop_clubhouse_laptop_01a")
					REGISTER_NAMED_RENDERTARGET("prop_clubhouse_laptop_01a")
					IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_CLUBHOUSE_LAPTOP_01A)
						LINK_NAMED_RENDERTARGET(BKR_PROP_CLUBHOUSE_LAPTOP_01A)
						IF sLaptopMonitor.iRenderTargetID = -1
							sLaptopMonitor.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("prop_clubhouse_laptop_01a")
						ENDIF
					ENDIF
				ENDIF
				
				PRINTLN("MAINTAIN_CLUBHOUSE_LAPTOP_MONITOR - Changing to state: CHM_LAPTOP_MONITOR_STATE_UPDATE_SCALEFORM - iRenderTargetID: ", sLaptopMonitor.iRenderTargetID)
				sLaptopMonitor.eLaptopState = CHM_LAPTOP_MONITOR_STATE_UPDATE_SCALEFORM
			BREAK
			CASE CHM_LAPTOP_MONITOR_STATE_UPDATE_SCALEFORM
				SET_TEXT_RENDER_ID(sLaptopMonitor.iRenderTargetID)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				
				DRAW_SPRITE_NAMED_RENDERTARGET(sLaptopMonitor.tlCrewTxd, sLaptopMonitor.tlCrewTxd, 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
				
				RESET_SCRIPT_GFX_ALIGN()
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC CLEANUP_CLUBHOUSE_LAPTOP_MONITOR(CLUBHOUSE_LAPTOP_MONITOR_STRUCT& sLaptopMonitor, BOOL bReleaseRT = TRUE)
	IF bReleaseRT
		IF IS_NAMED_RENDERTARGET_REGISTERED("prop_clubhouse_laptop_01a")   
			RELEASE_NAMED_RENDERTARGET("prop_clubhouse_laptop_01a")
		ENDIF
		sLaptopMonitor.iRenderTargetID = -1
	ENDIF
	sLaptopMonitor.eLaptopState = CHM_LAPTOP_MONITOR_STATE_INIT
	NETWORK_CLAN_RELEASE_EMBLEM(sLaptopMonitor.iCrewID)
	IF NOT IS_STRING_NULL_OR_EMPTY(sLaptopMonitor.tlCrewTxd)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sLaptopMonitor.tlCrewTxd)
	ENDIF
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("prop_screen_biker_laptop")
ENDPROC

FUNC PLAYER_INDEX GET_CLUBHOUSE_PROPERTY_OWNER()
	
	PLAYER_INDEX pOwner
	GAMER_HANDLE sPropertyOwnerGH
	GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), sPropertyOwnerGH)
	IF IS_GAMER_HANDLE_VALID(sPropertyOwnerGH)
		IF NETWORK_IS_GAMER_IN_MY_SESSION(sPropertyOwnerGH)
			pOwner = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sPropertyOwnerGH)
		ENDIF
	ENDIF
	
	RETURN pOwner
ENDFUNC

//// clubhouse mc name

PROC REGISTER_AND_LINK_CLUBHOUSE_MC_NAME_RENDER_TARGET(CLUBHOUSE_MC_NAME_STRUCT& sMCName)

	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("clubname_blackboard_01a")
		REGISTER_NAMED_RENDERTARGET("clubname_blackboard_01a")
		IF NOT IS_NAMED_RENDERTARGET_LINKED(BKR_PROP_CLUBHOUSE_BLACKBOARD_01A)
			LINK_NAMED_RENDERTARGET(BKR_PROP_CLUBHOUSE_BLACKBOARD_01A)
			IF sMCName.iRenderTargetID = -1
				sMCName.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("clubname_blackboard_01a")
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - REGISTER_AND_LINK_CLUBHOUSE_MC_NAME_RENDER_TARGET - Render Target ID: ", sMCName.iRenderTargetID)

ENDPROC

PROC UPDATE_CLUBHOUSE_MC_FONT_AND_COLOUR(CLUBHOUSE_MC_NAME_STRUCT& sMCName)
	
	IF sMCName.pOwnerID = PLAYER_ID()
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFont = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_FONT)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFontColour = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_COLOUR)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].bClubhouseHideName = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_SINAGEHIDE)
	ENDIF
			
	CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - UPDATE_CLUBHOUSE_MC_FONT_AND_COLOUR - Current Clubhouse MC Font: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFont, " & Font Colour: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFontColour, " & Hide Status: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].bClubhouseHideName)
	
ENDPROC

FUNC BOOL PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME(CLUBHOUSE_MC_NAME_STRUCT& sMCName)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sMCName.sNameScaleform)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Scaleform movie 'CLUBHOUSE_NAME' has loaded")
		BEGIN_SCALEFORM_MOVIE_METHOD(sMCName.sNameScaleform, "SET_CLUBHOUSE_NAME")	
		
		// Dont show name for restricted accounts
		IF sMCName.pOwnerID != INVALID_PLAYER_INDEX()
		AND DOES_ENTITY_EXIST(GET_PLAYER_PED(sMCName.pOwnerID))
		
			IF sMCName.pOwnerID = PLAYER_ID()
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Local player not in biker gang")
				sMCName.sName = GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlClubhouseName)
				IF IS_STRING_NULL_OR_EMPTY(sMCName.sName)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Clubhouse Name = NULL, setting to gang Name")
					sMCName.sName = GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName)
				ENDIF
			ELSE
			
				IF NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(sMCName.pOwnerID)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Clubhouse owner not member of a biker gang")
					sMCName.sName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID)
					IF IS_STRING_NULL_OR_EMPTY(sMCName.sName)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Clubhouse owner name = NULL, setting to Org Name")
						sMCName.sName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME  - Clubhouse owner is member of a biker gang")
					
					IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sMCName.pOwnerID)
						sMCName.sName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID)
						IF IS_STRING_NULL_OR_EMPTY(sMCName.sName)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME  - Owner Org name = NULL, setting to Clubhouse Name")
							sMCName.sName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID)
						ENDIF
					ELSE
						sMCName.sName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID)
						IF IS_STRING_NULL_OR_EMPTY(sMCName.sName)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME  - Owner MC name = NULL, setting to Org Name")
							sMCName.sName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF sMCName.bBlankMCName
				IF NOT IS_STRING_NULL_OR_EMPTY(sMCName.sName)
					GB_SET_ORGANIZATION_NAME("",GB_ORGANISATION_NAME_TYPE_CLUBHOUSE, FALSE)
					GB_SET_MC_CLUBHOUSE_NAME("", FALSE)
					sMCName.iMCNameHash = GET_HASH_KEY(GET_DEFAULT_BIKER_GANG_NAME())
				ENDIF
			ENDIF
			#ENDIF
			
			
			// Name
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sMCName.sName)
			sMCName.iMCNameHash = GET_HASH_KEY(sMCName.sName)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Setting Display Name: ", sMCName.sName)
			
			// Colour
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFontColour)
			sMCName.iCurrentFontColour = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFontColour
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Setting Display Name Font Colour: ", sMCName.iCurrentFontColour)	
			
			//Hide Status 
			sMCName.iHideMCName = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].bClubhouseHideName
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Setting Hide Status: ", sMCName.iHideMCName)	
			
			// Font
			IF IS_LANGUAGE_NON_ROMANIC()
			OR GET_CURRENT_LANGUAGE() = LANGUAGE_POLISH
			OR GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
				sMCName.iCurrentFont = -1
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Language does not support custom fonts - Setting font to -1 (B* 2816403)")
			ELSE
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFont)
				sMCName.iCurrentFont = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFont
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME - Setting Display Name Font: ", sMCName.iCurrentFont)
			ENDIF
		ENDIF
		
		END_SCALEFORM_MOVIE_METHOD()
		SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sMCName.sNameScaleform, TRUE)
		RETURN TRUE
	ELSE
		sMCName.sNameScaleform = REQUEST_SCALEFORM_MOVIE("CLUBHOUSE_NAME")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_CLUBHOUSE_MC_NAME(CLUBHOUSE_MC_NAME_STRUCT& sMCName)
	
	IF sMCName.iHideMCName = 0
		SET_TEXT_RENDER_ID(sMCName.iRenderTargetID)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		DRAW_SCALEFORM_MOVIE(sMCName.sNameScaleform, 0.0975, 0.105, 0.235, 0.350, 255, 255, 255, 255)  //sMCName.fCentreX, sMCName.fCentreY, sMCName.fWidth, sMCName.fHeight, 255, 255, 255, 255)
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT sMCName.bBlankMCName
	#ENDIF
		
		IF sMCName.pOwnerID != INVALID_PLAYER_INDEX()
		AND DOES_ENTITY_EXIST(GET_PLAYER_PED(sMCName.pOwnerID))
			
			IF sMCName.pOwnerID = PLAYER_ID()
				//if owner of clubhouse you always use the clubhouse name
				IF NOT IS_STRING_NULL_OR_EMPTY(GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlClubhouseName))
					sMCName.iMCTempHash = GET_HASH_KEY(GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlClubhouseName))
				ELSE
					sMCName.iMCTempHash = GET_HASH_KEY(GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName))
				ENDIF
			ELSE
				IF NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(sMCName.pOwnerID)
					IF NOT IS_STRING_NULL_OR_EMPTY(GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID))
						sMCName.iMCTempHash = GET_HASH_KEY(GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID))
					ELSE
						sMCName.iMCTempHash = GET_HASH_KEY(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID))
					ENDIF
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID))
						IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sMCName.pOwnerID)
							sMCName.iMCTempHash = GET_HASH_KEY(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID))
						ELSE
							sMCName.iMCTempHash = GET_HASH_KEY(GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID))
						ENDIF
					ELSE
						sMCName.iMCTempHash = GET_HASH_KEY(GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID))
					ENDIF
				ENDIF
			ENDIF
			
			// Name Updates
			IF (sMCName.iMCTempHash != sMCName.iMCNameHash)
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - iMCTempHash: ", sMCName.iMCTempHash)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - iMCNameHash: ", sMCName.iMCNameHash)
				
				IF sMCName.pOwnerID = PLAYER_ID()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - My display name has changed (I am Clubhouse owner)")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - Owner - (New) Gang Name: ", GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName))
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - Owner - (New) Clubhouse Name: ", GB_TO_STRING(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlClubhouseName))
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - Clubhouse owners display name has changed")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - Remote - (New) Gang Name: ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sMCName.pOwnerID))
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - Remote - (New) Clubhouse Name: ", GB_GET_PLAYER_MC_CLUBHOUSE_NAME_AS_A_STRING(sMCName.pOwnerID))
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DRAW_CLUBHOUSE_MC_NAME - Changing State to: CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM")
				
				sMCName.eNameStates = CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM
				sMCName.bUpdateScaleform = TRUE
				
			ENDIF
			
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(sMCName.sName)
			sMCName.eNameStates = CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM
			sMCName.bUpdateScaleform = TRUE
		ENDIF
	ENDIF
	#ENDIF	
	
ENDPROC

FUNC BOOL DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING(CLUBHOUSE_MC_NAME_STRUCT& sMCName)
	
	// Font Updates
	IF (sMCName.iCurrentFont != GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFont)
		IF NOT IS_LANGUAGE_NON_ROMANIC()
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_POLISH
		AND GET_CURRENT_LANGUAGE() != LANGUAGE_RUSSIAN
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING - Current Font: ", sMCName.iCurrentFont, " New Font: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFont)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING - Clubhouse Owners Font has Changed - Changing State to: CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM")
			RETURN TRUE
		ENDIF
	ENDIF
	
	// Font Colour Updates
	IF (sMCName.iCurrentFontColour != GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFontColour)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING - Current Font Colour: ", sMCName.iCurrentFontColour, " New Font Colour: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].iClubhouseNameFontColour)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING - Clubhouse Owners Font Colour has Changed - Changing State to: CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM")
		RETURN TRUE
	ENDIF
	
	// Hide Status
	IF sMCName.iHideMCName != GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].bClubhouseHideName
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING - Current Hide Status: ", sMCName.iHideMCName, " New Hide Status: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sMCName.pOwnerID)].bClubhouseHideName)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLUBHOUSE_MC_NAME(CLUBHOUSE_MC_NAME_STRUCT& sMCName, INT iCurrentProperty)

	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		SWITCH sMCName.eNameStates
			CASE CLUBHOUSE_MC_NAME_STATE_LINK_RT
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - CLUBHOUSE_MC_NAME_STATE_LINK_RT")
				REQUEST_SCALEFORM_MOVIE("CLUBHOUSE_NAME")
				
				sMCName.pOwnerID = GET_CLUBHOUSE_PROPERTY_OWNER()
				REGISTER_AND_LINK_CLUBHOUSE_MC_NAME_RENDER_TARGET(sMCName)
				
				IF sMCName.pOwnerID = PLAYER_ID()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - I am the Clubhouse Property Owner - My ID: ", NATIVE_TO_INT(sMCName.pOwnerID))
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - I am Not the Clubhouse Property Owner - Owner ID: ", NATIVE_TO_INT(sMCName.pOwnerID))
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - Changing State to: CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM")
				sMCName.eNameStates = CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM
				
			BREAK
			CASE CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM
				
				IF sMCName.bUpdateScaleform
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM")
					
					UPDATE_CLUBHOUSE_MC_FONT_AND_COLOUR(sMCName)
					
					IF PROCESS_CLUBHOUSE_MC_SCALEFORM_NAME(sMCName)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MC_NAME - Changing State to: CLUBHOUSE_MC_NAME_STATE_DRAW_NAME")
						sMCName.eNameStates = CLUBHOUSE_MC_NAME_STATE_DRAW_NAME
						sMCName.bUpdateScaleform = TRUE
					ENDIF
					
				ENDIF
			BREAK
			CASE CLUBHOUSE_MC_NAME_STATE_DRAW_NAME
				
				DRAW_CLUBHOUSE_MC_NAME(sMCName)
				
				IF DOES_CLUBHOUSE_MC_NAME_NEED_UPDATING(sMCName)
					sMCName.eNameStates = CLUBHOUSE_MC_NAME_STATE_UPDATE_SCALEFORM
					sMCName.bUpdateScaleform = TRUE
				ENDIF
				
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEANUP_CLUBHOUSE_MC_NAME(CLUBHOUSE_MC_NAME_STRUCT& sMCName, BOOL bReleaseRT = TRUE)
	IF HAS_SCALEFORM_MOVIE_LOADED(sMCName.sNameScaleform)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sMCName.sNameScaleform)
	ENDIF
	IF bReleaseRT
		IF IS_NAMED_RENDERTARGET_REGISTERED("clubname_blackboard_01a")   
			RELEASE_NAMED_RENDERTARGET("clubname_blackboard_01a")   
		ENDIF
		sMCName.iRenderTargetID = -1
	ENDIF
	sMCName.eNameStates = CLUBHOUSE_MC_NAME_STATE_LINK_RT
ENDPROC

/// PURPOSE:
///    If player owns a clubhouse it will return TRUE and vCoords will be set to that clubhouse location
/// PARAMS:
///    playerID - 
///    vCoords - 
/// RETURNS:
///    
FUNC BOOL GET_PLAYERS_CLUBHOUSE_POSITION(PLAYER_INDEX playerID, VECTOR &vCoords)
	IF GlobalPlayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE] > -1
		vCoords = mpProperties[GlobalPlayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE]].vBlipLocation[0]
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///// PURPOSE:
/////    Checks if given zone should block gaining of wanted level
///// PARAMS:
/////    iZone - 0 to 5 where 0 is owned clubhouse, 1-5 are owned factories
/////    vZoneCenter - 
///// RETURNS:
/////    
//FUNC BOOL _SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE(INT iZone, VECTOR &vZoneCenter)
//	BOOL bShouldSuppress
//	
//	IF NETWORK_IS_ACTIVITY_SESSION()
//		RETURN FALSE
//	ENDIF
//	
//	PLAYER_INDEX playerOwner
//	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
//		playerOwner = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
//	ELSE
//		// Not blocking if not part of MC
//		RETURN FALSE
//	ENDIF
//	
//	IF iZone = 0
//		// Zone 0 is clubhouse. If we own it or our boss owns it - block gaining.
//		IF GlobalPlayerBD_FM[NATIVE_TO_INT(playerOwner)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE] > -1
//			vZoneCenter = mpProperties[GlobalPlayerBD_FM[NATIVE_TO_INT(playerOwner)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_CLUBHOUSE]].entrance[0].vEntranceMarkerLoc
//			bShouldSuppress = TRUE
//		ENDIF
//	ELIF iZone > 0 AND iZone < ciMAX_OWNED_FACTORIES + 1
//		// Zones 1 to 1+n where n is max number of factories that can be owned - are factories.
//		FACTORY_ID eFactoryID = GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iZone - 1)
//		IF DOES_PLAYER_OWN_FACTORY(playerOwner, eFactoryID)
//			GET_FACTORY_ENTRY_LOCATE(GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eFactoryID), vZoneCenter)
//			bShouldSuppress = TRUE
//		ENDIF
//	ENDIF
//	
//	RETURN bShouldSuppress
//ENDFUNC

///// PURPOSE:
/////    Disable no wanted leve gain zones around clubhouses and factories
///// PARAMS:
/////    bDisable - 
//PROC SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES(BOOL bDisable)
//	IF bDisable
//		IF NOT g_DisableNoWantedLevelGainZones
//			g_DisableNoWantedLevelGainZones = TRUE
//			#IF IS_DEBUG_BUILD
//				PRINTLN("SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES - Disabling!")
//			#ENDIF
//		ENDIF
//	ELSE
//		IF g_DisableNoWantedLevelGainZones
//			g_DisableNoWantedLevelGainZones = FALSE
//			#IF IS_DEBUG_BUILD
//				PRINTLN("SET_DISABLE_NO_WANTED_LEVEL_GAIN_ZONES - Enabling!")
//			#ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

///// PURPOSE:
/////    Players around owned clubhouses and factories can't gain wanted level.
///// PARAMS:
/////    localData - 
//PROC MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES(NO_WANTED_LEVEL_GAIN_ZONES_DATA &localData)
//
//	#IF IS_DEBUG_BUILD
//	IF NOT localData.db_bCheckedCMD
//		IF GET_COMMANDLINE_PARAM_EXISTS("sc_DrawBikerNoWantedLevelGainZones")
//			localData.db_bDrawZones = TRUE
//		ENDIF
//		localData.db_bCheckedCMD = TRUE
//	ENDIF
//	#ENDIF
//
//	IF g_DisableNoWantedLevelGainZones
//		#IF IS_DEBUG_BUILD
//			IF GET_FRAME_COUNT() % 360 = 0
//				PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - Not running because g_DisableNoWantedLevelGainZones is TRUE")
//			ENDIF
//		#ENDIF
//		
//		IF localData.iActiveZonesBS > 0
//			localData.iActiveZonesBS = 0
//		ENDIF
//		
//		IF localData.iMaintainThisWantedLevel > -1
//			localData.iMaintainThisWantedLevel = -1
//		ENDIF
//		
//		EXIT
//	ENDIF
//	
//	VECTOR vCoords
//	BOOL bShouldSuppress = _SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE(localData.iZoneStagger, vCoords)
//	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
//	
//	vCoords.Z = 0.0
//	vPlayerCoords.Z = 0.0
//	
//	IF NOT IS_BIT_SET(localData.iActiveZonesBS, localData.iZoneStagger)
//		IF bShouldSuppress
//		AND VDIST(vPlayerCoords, vCoords) <= 50.0
//			IF localData.iActiveZonesBS = 0
//				localData.iMaintainThisWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
//			ENDIF
//			SET_BIT(localData.iActiveZonesBS, localData.iZoneStagger)
//			
//			#IF IS_DEBUG_BUILD
//				PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - We got close to zone ", localData.iZoneStagger, " and wanted level gaining should be suppressed around it.")
//			#ENDIF
//		ENDIF
//	ELSE
//		IF (NOT bShouldSuppress)
//		OR (VDIST(vPlayerCoords, vCoords) > 50.1)
//			CLEAR_BIT(localData.iActiveZonesBS, localData.iZoneStagger)
//			
//			IF localData.iActiveZonesBS = 0
//				localData.iMaintainThisWantedLevel = -1
//			ENDIF
//			
//			#IF IS_DEBUG_BUILD
//				PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - We got away from zone ", localData.iZoneStagger, " or it's not supposed to block wanted level gaining.")
//			#ENDIF
//		ENDIF
//	ENDIF
//	
//	localData.iZoneStagger = (localData.iZoneStagger + 1) % 6
//	
//	IF localData.iActiveZonesBS > 0
//		IF localData.iMaintainThisWantedLevel > -1
//			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > localData.iMaintainThisWantedLevel
//				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), localData.iMaintainThisWantedLevel)
//				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//				#IF IS_DEBUG_BUILD
//					PRINTLN("MAINTAIN_NO_WANTED_LEVEL_GAIN_ZONES - Player gained wanted level, setting it back to ", localData.iMaintainThisWantedLevel)
//				#ENDIF
//			ELIF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < localData.iMaintainThisWantedLevel
//				// Allow max wanted level to drop so if players lose it while being in the zone they can't gain it again.
//				localData.iMaintainThisWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		IF localData.db_bDrawZones
//			INT i
//			REPEAT 6 i
//				_SHOULD_WANTED_LEVEL_GAINING_BE_SUPPRESSED_AROUND_THIS_ZONE(i, vCoords)
//				//DRAW_DEBUG_SPHERE(vCoords, 50.0, 191, 96, 215, )
//				DRAW_MARKER(MARKER_CYLINDER, vCoords, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, <<50.0 * 2.0, 50.0 * 2.0, 10.0>>, PICK_INT(IS_BIT_SET(localData.iActiveZonesBS, i), 255, 140), 120, 120, 128)
//			ENDREPEAT
//		ENDIF
//	#ENDIF
//ENDPROC

/// PURPOSE:
///    Display help text when player don't own the clubhouse gun vault
FUNC BOOL SHOULD_DISPLAY_HT_FOR_DISABLED_GUN_VAULT(INT iCurrentProperty) 
	
	VECTOR vCoordsToCheck
	
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		vCoordsToCheck = <<1122.7124, -3162.8662, -37.8705>>
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		vCoordsToCheck = <<1008.2341, -3171.6279, -39.9071>>
	ENDIF
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCoordsToCheck, <<0.8, 0.5, 1.5>>)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_HELP_MESSAGE_ON_SCREEN()
				PRINT_HELP_FOREVER("CH_GUN_DIS")
			ENDIF
			
			RETURN TRUE
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC MP_PROP_OFFSET_STRUCT GET_CLUBHOUSE_BARMAN_LOCATION(INT iCurrentProperty)
	MP_PROP_OFFSET_STRUCT Pos

	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		Pos.vLoc = <<1120.9756, -3144.7769, -38.225>>
		Pos.vRot = <<0.0, 0.0, -90.0>>
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		Pos.vLoc = <<998.624, -3170.376, -35.2273>>
		Pos.vRot = <<0.0, 0.0, 0.0>>
	ENDIF
	
	RETURN Pos
ENDFUNC

//// clubhouse mc pinboard

PROC REGISTER_AND_LINK_CLUBHOUSE_MISSION_WALL_RENDER_TARGET(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu,INT iCurrentProperty)

	IF NOT IS_NAMED_RENDERTARGET_REGISTERED("clubhouse_Plan_01a")
		REGISTER_NAMED_RENDERTARGET("clubhouse_Plan_01a")
		
		
		MODEL_NAMES	eInteriorModel = DUMMY_MODEL_FOR_SCRIPT
		IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
			eInteriorModel = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_rt_clubhouse_plan_01a"))
		ELIF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
			eInteriorModel = INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_rt_clubhouse_plan_01a"))	//INT_TO_ENUM(MODEL_NAMES, HASH("bkr_prop_biker_scriptrt_wall"))
		ELSE
			eInteriorModel = DUMMY_MODEL_FOR_SCRIPT
			CASSERTLN(DEBUG_AMBIENT, "MAINTAIN_CLUBHOUSE_MISSION_WALL - REGISTER_AND_LINK_CLUBHOUSE_MISSION_WALL_RENDER_TARGET unknown peoperty ", iCurrentProperty)
			EXIT
		ENDIF
		
		IF NOT IS_NAMED_RENDERTARGET_LINKED(eInteriorModel)
			LINK_NAMED_RENDERTARGET(eInteriorModel)
			IF sClubHouseMissionMenu.iRenderTargetID = -1
				sClubHouseMissionMenu.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("clubhouse_Plan_01a")
			ENDIF
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - REGISTER_AND_LINK_CLUBHOUSE_MISSION_WALL_RENDER_TARGET - Render Target ID: ", sClubHouseMissionMenu.iRenderTargetID)

ENDPROC

//PROC UPDATE_CLUBHOUSE_MC_FONT_AND_COLOUR(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu)
//	
//	IF sClubHouseMissionMenu.pOwnerID = PLAYER_ID()
//		GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].iClubhouseNameFont = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_FONT)
//		GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].iClubhouseNameFontColour = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_COLOUR)
//	ENDIF
//					
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - UPDATE_CLUBHOUSE_MC_FONT_AND_COLOUR - Current Clubhouse MC Font: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].iClubhouseNameFont, " & Font Colour: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].iClubhouseNameFontColour)
//	
//ENDPROC

ENUM CLUBHOUSE_MC_STAT_ENUM
	//MC INFO
	 CHM_SDESC_Favourite_Formation
	,CHM_SDESC_Ride_in_Formation_Time
	,CHM_SDESC_Favourite_Jukebox_Playlist
	,CHM_SDESC_Favourite_Outfit_Style
	,CHM_SDESC_Members_Marked_For_Death

	//MC COMBAT
	,CHM_SDESC_MC_Kills
	,CHM_SDESC_MC_Deaths
	,CHM_SDESC_Rival_President_Kills
	,CHM_SDESC_Rival_CEO_and_VIP_Kills
	,CHM_SDESC_Road_Rash_Kills

	//MC ACTIVITIES
	,CHM_SDESC_Clubhouse_Contracts_Completed
	,CHM_SDESC_Clubhouse_Contract_Earnings
	,CHM_SDESC_Club_Work_Completed
	,CHM_SDESC_Club_Challenges_Completed
	,CHM_SDESC_Member_Challenges_Completed
	
	,NUM_CLUBHOUSE_MC_STATS
ENDENUM
ENUM CLUBHOUSE_MC_STAT_TYPE_ENUM
	 CHM_STYPE_STRING
	,CHM_STYPE_TIME
	,CHM_STYPE_INT
	,CHM_STYPE_CASH
	
	,NUM_CLUBHOUSE_MC_STAT_TYPES
ENDENUM


FUNC BOOL GET_STAT_DESCRIPTION_AND_VALUE(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu, CLUBHOUSE_MC_STAT_ENUM eStat, TEXT_LABEL &tlStatDesc, TEXT_LABEL &tlStatValue, INT &iStatValue, CLUBHOUSE_MC_STAT_TYPE_ENUM &eType)
	eType = CHM_STYPE_STRING
	tlStatValue = ""
	iStatValue = 0
	
	INT iSlot
	SWITCH eStat
		//MC INFO
		CASE CHM_SDESC_Favourite_Formation				//url:bugstar:3021480
			tlStatDesc	 = "CHM_SDESC_0"
			
			tlStatValue = BIK_GET_FORMATION_TEXT_LABEL(BIK_GET_FAVOURITE_FORMATION(sClubHouseMissionMenu.pOwnerID))
			IF IS_STRING_NULL_OR_EMPTY(tlStatValue)
			OR ARE_STRINGS_EQUAL(tlStatValue, "NULL")
				tlStatValue ="PI_BIK_8_3_O"
			ENDIF
			eType = CHM_STYPE_STRING
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Ride_in_Formation_Time
			tlStatDesc	 = "CHM_SDESC_1"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iFormationtime0 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME0)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iFormationtime1 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME1)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iFormationtime2 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME2)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iFormationtime3 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME3)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iFormationtime0 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME0)
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iFormationtime1 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME1)
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iFormationtime2 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME2)
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iFormationtime3 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME3)
			ENDIF
			
			eType = CHM_STYPE_TIME
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Favourite_Jukebox_Playlist		//url:bugstar:3021488
			tlStatDesc	 = "CHM_SDESC_2"
			
			INT iPlaylistID
			iPlaylistID = GET_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID(sClubHouseMissionMenu.pOwnerID)
			tlStatValue = GET_CLUBHOUSE_JB_PLAYLIST_TEXT_LABEL_FROM_ID(iPlaylistID)
			eType = CHM_STYPE_STRING
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Favourite_Outfit_Style
			tlStatDesc	 = "CHM_SDESC_3"
			
			tlStatValue = BIK_GET_PLAYER_FAVOURITE_OUTFIT_NAME(sClubHouseMissionMenu.pOwnerID)
			IF IS_STRING_NULL_OR_EMPTY(tlStatValue)
			OR ARE_STRINGS_EQUAL(tlStatValue, "NULL")
				tlStatValue ="PI_BIK_8_3_O"
			ENDIF
			eType = CHM_STYPE_STRING
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Members_Marked_For_Death
			tlStatDesc	 = "CHM_SDESC_4"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iMembersmarkedfordeath 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERSMARKEDFORDEATH)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iMembersmarkedfordeath 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERSMARKEDFORDEATH)
			ENDIF
			
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		
		//MC COMBAT										//url:bugstar:3021502
		CASE CHM_SDESC_MC_Kills
			tlStatDesc	 = "CHM_SDESC_5"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iMckills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MCKILLS)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iMckills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MCKILLS)
			ENDIF
			
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_MC_Deaths
			tlStatDesc	 = "CHM_SDESC_6"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iMcdeaths 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MCDEATHS)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iMcdeaths 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MCDEATHS)
			ENDIF
			
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Rival_President_Kills
			tlStatDesc	 = "CHM_SDESC_7"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iRivalpresidentkills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALPRESIDENTKILLS)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iRivalpresidentkills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALPRESIDENTKILLS)
			ENDIF
			
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Rival_CEO_and_VIP_Kills
			tlStatDesc	 = "CHM_SDESC_8"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iRivalceoandvipkills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALCEOANDVIPKILLS)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iRivalceoandvipkills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALCEOANDVIPKILLS)
			ENDIF
			
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Road_Rash_Kills
			tlStatDesc	 = "CHM_SDESC_9"
			
			iStatValue = 0
			IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(sClubHouseMissionMenu.pOwnerID)
				REPEAT NUM_NETWORK_PLAYERS iSlot
					PLAYER_INDEX pSlot
					pSlot = INT_TO_PLAYERINDEX(iSlot)
					IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(pSlot, TRUE)
					AND (GB_GET_THIS_PLAYER_GANG_BOSS(pSlot) = sClubHouseMissionMenu.pOwnerID)
						iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(pSlot)].sClubhouseMissionWallStats.iMeleekills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MELEEKILLS)
					ENDIF
				ENDREPEAT
			ELSE
				iStatValue += GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iMeleekills 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MELEEKILLS)
			ENDIF
			
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		
		//MC ACTIVITIES
		CASE CHM_SDESC_Clubhouse_Contracts_Completed
			tlStatDesc	 = "CHM_SDESC_10"
			
			iStatValue = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iClubhousecontractscomplete 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTSCOMPLETE)
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Clubhouse_Contract_Earnings
			tlStatDesc	 = "CHM_SDESC_11"
			
			iStatValue = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iClubhousecontractearnings 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTEARNINGS)
			eType = CHM_STYPE_CASH
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Club_Work_Completed
			tlStatDesc	 = "CHM_SDESC_12"
			
			iStatValue = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iClubworkcompleted 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBWORKCOMPLETED)
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Club_Challenges_Completed
			tlStatDesc	 = "CHM_SDESC_13"
			
			iStatValue = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iClubchallengescompleted 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBCHALLENGESCOMPLETED)
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		CASE CHM_SDESC_Member_Challenges_Completed
			tlStatDesc	 = "CHM_SDESC_14"
			
			iStatValue = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].sClubhouseMissionWallStats.iMemberchallengescompleted 	//GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERCHALLENGESCOMPLETED)
			eType = CHM_STYPE_INT
			RETURN TRUE
		BREAK
		
		DEFAULT
			tlStatDesc	 = "CHM_SDESC_"
			tlStatDesc	+= ENUM_TO_INT(eStat)
			
			
			CASSERTLN(DEBUG_SAFEHOUSE, "GET_STAT_DESCRIPTION_AND_VALUE unknown stat ", tlStatDesc)
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu)
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sClubHouseMissionMenu.sScaleform)
		INT i
		REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
			CLUBHOUSE_MISSION_ENUM eCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[i])
			IF eCHMission != CLUBHOUSE_MISSION_invalid
				VECTOR vLoc = GET_CHM_ITEM_LOCATION(eCHMission)
				
				CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - SET_MISSION(",
						GET_CHM_ITEM_NAME(eCHMission), " ",
						GET_CHM_ITEM_DESCRIPTION(eCHMission), " ",
						GET_CHM_ITEM_TEXTURE(eCHMission),
						" <<", vLoc.x, ", ", vLoc.y, ">>)")
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sClubHouseMissionMenu.sScaleform, "SET_MISSION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)													//The index of the mission to set. The first mission is at index zero.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_CHM_ITEM_NAME(eCHMission))					//Text label of the mission title.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_CHM_ITEM_DESCRIPTION(eCHMission))			//Text label of the mission description.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_PLAYER_NAME(GET_CHM_ITEM_TEXTURE(eCHMission))			//TXD reference for the mission image.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vLoc.x)											//[optional] Map x position of the mission in world coordinates
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(vLoc.y)											//[optional] Map y position of the mission in world coordinates
				END_SCALEFORM_MOVIE_METHOD()
				sClubHouseMissionMenu.eCHMissions[i] = eCHMission
				
				INT iFailCondition = 0
				IF IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH(sClubHouseMissionMenu.pOwnerID, eCHMission, iFailCondition)
//					BEGIN_SCALEFORM_MOVIE_METHOD(sClubHouseMissionMenu.sScaleform, "HIDE_MISSION")
//						#IF IS_DEBUG_BUILD
//						IF iFailCondition = 2
//							INT iReason = GB_GET_BOSS_MISSION_UNAVAILABLE_REASON(PLAYER_ID(), GB_RETURN_FMMC_TYPE_FROM_CHM_ITEM(eCHMission))
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - HIDE_MISSION(", i, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\"), iFailCondition:", iFailCondition, ", reason: ", iReason)
//						ELSE
//						#ENDIF
//							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - HIDE_MISSION(", i, " \"", GET_STRING_FROM_TEXT_FILE(GET_CHM_ITEM_NAME(eCHMission)), "\"), iFailCondition:", iFailCondition)
//						#IF IS_DEBUG_BUILD
//						ENDIF
//						#ENDIF
//						
//						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
//					END_SCALEFORM_MOVIE_METHOD()
					sClubHouseMissionMenu.bMissionAvailable[i] = FALSE
				ELSE
					sClubHouseMissionMenu.bMissionAvailable[i] = TRUE
				ENDIF
			ELSE
				BEGIN_SCALEFORM_MOVIE_METHOD(sClubHouseMissionMenu.sScaleform, "HIDE_MISSION")
					CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - SET_MISSION(", i, " CLUBHOUSE_MISSION_invalid)")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(i)
				END_SCALEFORM_MOVIE_METHOD()
				sClubHouseMissionMenu.bMissionAvailable[i] = FALSE

			ENDIF
		ENDREPEAT
		
		GB_MC_UPDATE_FAVOURITE_FORMATION()
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime0				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME0)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime1				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME1)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime2				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME2)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iFormationtime3				= GET_MP_INT_CHARACTER_STAT(MP_STAT_FORMATIONTIME3)
		SET_PLAYERS_FAVOURITE_PLAYLIST(GET_LOCAL_PLAYERS_FAVOURITE_JUKEBOX_PLAYLIST_ID())
//		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iXXX_Four					= GET_MP_INT_CHARACTER_STAT(MP_STAT_XXX_Four)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMembersmarkedfordeath		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERSMARKEDFORDEATH)
		
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMckills		 				= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCKILLS)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMcdeaths					= GET_MP_INT_CHARACTER_STAT(MP_STAT_MCDEATHS)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iRivalpresidentkills			= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALPRESIDENTKILLS)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iRivalceoandvipkills	 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_RIVALCEOANDVIPKILLS)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMeleekills			 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_MELEEKILLS)
		
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubhousecontractscomplete	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTSCOMPLETE)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubhousecontractearnings	= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBHOUSECONTRACTEARNINGS)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubworkcompleted			= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBWORKCOMPLETED)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iClubchallengescompleted		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CLUBCHALLENGESCOMPLETED)
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sClubhouseMissionWallStats.iMemberchallengescompleted	= GET_MP_INT_CHARACTER_STAT(MP_STAT_MEMBERCHALLENGESCOMPLETED)
		
		CLUBHOUSE_MC_STAT_ENUM eStat
		REPEAT NUM_CLUBHOUSE_MC_STATS eStat
			TEXT_LABEL tlStatDesc = "", tlStatValue = ""
			INT iStatValue = 0
			CLUBHOUSE_MC_STAT_TYPE_ENUM eType = NUM_CLUBHOUSE_MC_STAT_TYPES
			IF GET_STAT_DESCRIPTION_AND_VALUE(sClubHouseMissionMenu, eStat, tlStatDesc, tlStatValue, iStatValue, eType)
				CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - SET_STAT(tlStatDesc:",
						tlStatDesc, " tlStatValue:", tlStatValue, " iStatValue:", iStatValue, " eType:", eType, ")")
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sClubHouseMissionMenu.sScaleform, "SET_STAT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(eStat))							//The index of the stat to set. The first stat is at index zero.
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlStatDesc)									//Text label for the description of the stat.
					SWITCH eType																		//Raw string of the stat value.
						CASE CHM_STYPE_STRING
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tlStatValue)
						BREAK
						CASE CHM_STYPE_TIME
							INT iStatDay, iStatHour, iStatMin, iStatSec
							iStatDay = (iStatValue/(24*60*60))
							iStatValue -= (iStatDay*24*60*60)
							
							iStatHour = (iStatValue/(60*60))
							iStatValue -= (iStatHour*60*60)
							
							iStatMin = (iStatValue/(60))
							iStatValue -= (iStatMin*60)
							
							iStatSec = iStatValue
							
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING("CHM_VTIME")	//~1~d ~1~h ~1~m ~1~s
								ADD_TEXT_COMPONENT_INTEGER(iStatDay)
								ADD_TEXT_COMPONENT_INTEGER(iStatHour)
								ADD_TEXT_COMPONENT_INTEGER(iStatMin)
								ADD_TEXT_COMPONENT_INTEGER(iStatSec)
							END_TEXT_COMMAND_SCALEFORM_STRING()
						BREAK
						CASE CHM_STYPE_INT
							SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iStatValue)
						BREAK
						CASE CHM_STYPE_CASH
							BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PM_CASH")		//$~1~
								ADD_TEXT_COMPONENT_INTEGER(iStatValue)
							END_TEXT_COMMAND_SCALEFORM_STRING()
						BREAK
						
					ENDSWITCH
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDREPEAT
		
		IF !GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.bClubhouseMissionWallActive
			BEGIN_SCALEFORM_MOVIE_METHOD(sClubHouseMissionMenu.sScaleform, "SET_SELECTED_MISSION")
				CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - SET_SELECTED_MISSION(", -1, ") - !bClubhouseMissionWallActive")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
			END_SCALEFORM_MOVIE_METHOD()
		ELSE
			BEGIN_SCALEFORM_MOVIE_METHOD(sClubHouseMissionMenu.sScaleform, "SET_SELECTED_MISSION")
				IF (sClubHouseMissionMenu.iMenuCurrentRow = 0)
					sClubHouseMissionMenu.iCurrentMission = sClubHouseMissionMenu.iMenuCurrentColumn
				ENDIF
				
				IF sClubHouseMissionMenu.pOwnerID = PLAYER_ID()
					GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iHighlightedMission = sClubHouseMissionMenu.iCurrentMission
				ENDIF
				INT iCurrentMission = GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iHighlightedMission
				
				CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL - SET_SELECTED_MISSION(", iCurrentMission, ")")
				
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iCurrentMission)					//Shows the selected highlight of the specified mission and hides any other mission selected highlights.
																						//This is a zero-indexed value so passing a param of 0 will show the highlight on the first mission.
				
				sClubHouseMissionMenu.iCurrentMission = iCurrentMission
				sClubHouseMissionMenu.iHighlightedMission = iCurrentMission
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sClubHouseMissionMenu.sScaleform, TRUE)
		bUpdateControls = TRUE
		
		g_bClubhouseMissionWallUpdate = FALSE
		bUpdateControls = FALSE
		RETURN TRUE
	ELSE
		sClubHouseMissionMenu.sScaleform = REQUEST_SCALEFORM_MOVIE("BIKER_MISSION_WALL")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_CLUBHOUSE_MISSION_WALL(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu)
	
	SET_TEXT_RENDER_ID(sClubHouseMissionMenu.iRenderTargetID)
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
	DRAW_SCALEFORM_MOVIE(sClubHouseMissionMenu.sScaleform,
			sClubHouseMissionMenu.fCentreX, sClubHouseMissionMenu.fCentreY,
			sClubHouseMissionMenu.fWidth, sClubHouseMissionMenu.fHeight,
			255, 255, 255, 255)
	SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	
	IF sClubHouseMissionMenu.pOwnerID != INVALID_PLAYER_INDEX()
	AND DOES_ENTITY_EXIST(GET_PLAYER_PED(sClubHouseMissionMenu.pOwnerID))
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_BikerForceNewContracts")
			IF sClubHouseMissionMenu.iToggleClubhouseMissionWall != g_iToggleClubhouseMissionWall
				sClubHouseMissionMenu.iToggleClubhouseMissionWall = g_iToggleClubhouseMissionWall
				sClubHouseMissionMenu.bDebugUpdateMenu = TRUE
				
				INT iPlayerBS
				SET_BIT(iPlayerBS, NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID))
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_CLUBHOUSE_MISSION_WALL - iToggleClubhouseMissionWall:", sClubHouseMissionMenu.iToggleClubhouseMissionWall, " for pOwnerID:", NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID))
				SWITCH sClubHouseMissionMenu.iToggleClubhouseMissionWall
					CASE 0
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] = ENUM_TO_INT(CLUBHOUSE_MISSION_SHUTTLE)		//-	By the Pound (Chicken Factory) - variation 2
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] = ENUM_TO_INT(CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY)		//-	Weapon of Choice
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] = ENUM_TO_INT(CLUBHOUSE_MISSION_ROOFTOP_CONTAINER)			//-	Guns for Hire (Vanilla Unicorn) - variation 2
					BREAK
					CASE 1
					GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] = ENUM_TO_INT(CLUBHOUSE_MISSION_SHUTTLE)		//-	By the Pound (Chicken Factory) - variation 2
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] = ENUM_TO_INT(CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY)		//-	Weapon of Choice
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] = ENUM_TO_INT(CLUBHOUSE_MISSION_ROOFTOP_CONTAINER)			//-	Guns for Hire (Vanilla Unicorn) - variation 2
					BREAK
					CASE 2
					GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] = ENUM_TO_INT(CLUBHOUSE_MISSION_SHUTTLE)		//-	By the Pound (Chicken Factory) - variation 2
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] = ENUM_TO_INT(CLUBHOUSE_MISSION_DEATHBIKE_DELIVERY)		//-	Weapon of Choice
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] = ENUM_TO_INT(CLUBHOUSE_MISSION_ROOFTOP_CONTAINER)			//-	Guns for Hire (Vanilla Unicorn) - variation 2
					BREAK
					DEFAULT
						
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_BikerFlowPlayVariations")
			IF sClubHouseMissionMenu.iToggleClubhouseMissionWall != g_iToggleClubhouseMissionWall
				sClubHouseMissionMenu.iToggleClubhouseMissionWall = g_iToggleClubhouseMissionWall
				sClubHouseMissionMenu.bDebugUpdateMenu = TRUE
				
				INT iPlayerBS
				SET_BIT(iPlayerBS, NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID))
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DRAW_CLUBHOUSE_MISSION_WALL - iToggleClubhouseMissionWall:", sClubHouseMissionMenu.iToggleClubhouseMissionWall, " for pOwnerID:", NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID))
				SWITCH sClubHouseMissionMenu.iToggleClubhouseMissionWall
					CASE 0
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] = ENUM_TO_INT(CLUBHOUSE_MISSION_DEAL_GONE_WRONG)		//-	By the Pound (Chicken Factory) - variation 2
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] = ENUM_TO_INT(CLUBHOUSE_MISSION_CONTRACT_KILLING)		//-	Weapon of Choice
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] = ENUM_TO_INT(CLUBHOUSE_MISSION_LAST_RESPECTS)			//-	Guns for Hire (Vanilla Unicorn) - variation 2
					BREAK
					CASE 1
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] = ENUM_TO_INT(CLUBHOUSE_MISSION_FREE_PRISONER)			//-	Jailbreak
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] = ENUM_TO_INT(CLUBHOUSE_MISSION_SHUTTLE)				//-	Outrider
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] = ENUM_TO_INT(CLUBHOUSE_MISSION_RESCUE_CONTACT)		//-	POW (O’ Neil farm)
					BREAK
					CASE 2
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[0] = ENUM_TO_INT(CLUBHOUSE_MISSION_UNLOAD_WEAPONS)		//-	Gunrunning
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[1] = ENUM_TO_INT(CLUBHOUSE_MISSION_DESTROY_VANS)			//-	Fragile Goods
						GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[2] = ENUM_TO_INT(CLUBHOUSE_MISSION_BURN_ASSETS)			//-	Torched (Textile City) - variation 0
					BREAK
					DEFAULT
						
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
		#ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu)
	#IF IS_DEBUG_BUILD
	IF sClubHouseMissionMenu.bDebugUpdateMenu
		sClubHouseMissionMenu.bDebugUpdateMenu = FALSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - sClubHouseMissionMenu.bDebugUpdateMenu = true")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF g_bClubhouseMissionWallUpdate
		CDEBUG1LN(DEBUG_SAFEHOUSE, GET_PLAYER_NAME(PLAYER_ID()), " MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - g_bClubhouseMissionWallUpdate = true")
		INITIALISE_CHM_MISSION_LIST(sClubHouseMissionMenu)
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(), FALSE, SIMPLE_INTERIOR_TYPE_WAREHOUSE)
	AND NOT IS_PROPERTY_OFFICE(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	AND NOT IS_PROPERTY_CLUBHOUSE(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	)
	OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
		PRINTLN("MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - FALSE - Player in or entering/exiting property.")
		RETURN FALSE
	ENDIF
	
	// Item Updates
	IF NOT IS_BIT_SET(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_MISSION_CHANGED_AWAIT_CAM))
		IF (sClubHouseMissionMenu.pOwnerID = PLAYER_ID())
		AND (sClubHouseMissionMenu.iMenuCurrentRow = 0)
		AND (sClubHouseMissionMenu.iCurrentMission != sClubHouseMissionMenu.iMenuCurrentColumn)
			IF DOES_CAM_EXIST(sClubHouseMissionMenu.ciCam)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - Current Mission: ", sClubHouseMissionMenu.iCurrentMission, " New Mission: ", sClubHouseMissionMenu.iMenuCurrentColumn, " awaiting cam change...")
				SET_BIT(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_MISSION_CHANGED_AWAIT_CAM))
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - Current Mission: ", sClubHouseMissionMenu.iCurrentMission, " New Mission: ", sClubHouseMissionMenu.iMenuCurrentColumn, " (no cam)")
				CLEAR_BIT(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_MISSION_CHANGED_AWAIT_CAM))
				CLEAR_BIT(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_CAM_CHANGED_UPDATE_MOVIE))
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_CAM_CHANGED_UPDATE_MOVIE))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - Current Mission: ", sClubHouseMissionMenu.iCurrentMission, " New Mission: ", sClubHouseMissionMenu.iMenuCurrentColumn, " ...cam past midway point.")
			SET_BIT(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_MISSION_CHANGED_AWAIT_CAM))
			CLEAR_BIT(sClubHouseMissionMenu.iUpdatingMissionSelectionBitset, ENUM_TO_INT(eCHMBITSET_CAM_CHANGED_UPDATE_MOVIE))
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iHighlightedMission = GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iHighlightedMission
	IF sClubHouseMissionMenu.iHighlightedMission != iHighlightedMission
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - Highlighted Mission: ", sClubHouseMissionMenu.iHighlightedMission, " iHighlightedMission: ", iHighlightedMission)
		
		sClubHouseMissionMenu.iHighlightedMission = iHighlightedMission
		RETURN TRUE
	ENDIF
	
	INT i, iInvalidCount = 0
	REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
	
		IF NOT IS_BIT_SET(GlobalServerBD_FM.ClubhouseMissionData.iCHMissionInvalidBS, i)
		AND NOT IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissionInvalidBS, i)
			IF GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i] != GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[i]
				GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[i] = GlobalServerBD_FM.ClubhouseMissionData.iCHMissions[i]
			ENDIF
		ENDIF
		
		CLUBHOUSE_MISSION_ENUM eCHMission = INT_TO_ENUM(CLUBHOUSE_MISSION_ENUM, GlobalPlayerBD_FM[NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID)].ClubhouseMissionData.iCHMissions[i])
		IF sClubHouseMissionMenu.eCHMissions[i] != eCHMission
			CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - Mission: ", i, " eCHMissions:", sClubHouseMissionMenu.eCHMissions[i], " != eCHMission:", eCHMission)
			RETURN TRUE
		ENDIF
		
		INT iFailCondition= 0
		BOOL bUnavailable = IS_CH_MISSION_MENU_OPTION_UNAVAILABLE_FOR_LAUNCH(sClubHouseMissionMenu.pOwnerID, eCHMission, iFailCondition)
		IF sClubHouseMissionMenu.bMissionAvailable[i] = bUnavailable
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - Mission: ", i, " available:", GET_STRING_FROM_BOOL(sClubHouseMissionMenu.bMissionAvailable[i]), " = bUnavailable:", GET_STRING_FROM_BOOL(bUnavailable), "(iFailCondition:", iFailCondition, ")")
			RETURN TRUE
		ENDIF
		
		IF eCHMission != CLUBHOUSE_MISSION_invalid
			iInvalidCount++
		ENDIF
	ENDREPEAT
	IF iInvalidCount = iCONST_NUM_CLUBHOUSE_MISSIONS-1
		CASSERTLN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING - iInvalidCount:", iInvalidCount)
		INITIALISE_CHM_MISSION_LIST(sClubHouseMissionMenu)
		
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF sClubHouseMissionMenu.bDebugInvalidateAll
		REPEAT iCONST_NUM_CLUBHOUSE_MISSIONS i
			GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].ClubhouseMissionData.iCHMissions[i] = -1
			BROADCAST_CLUBHOUSE_MISSION_MISSION_LAUNCHED(i)
		ENDREPEAT
		
		sClubHouseMissionMenu.bDebugInvalidateAll = FALSE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_CLUBHOUSE_MISSION_WALL(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu, INT iCurrentProperty)

	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		SWITCH sClubHouseMissionMenu.eWallStates
			CASE CLUBHOUSE_MISSION_WALL_STATE_LINK_RT
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - CLUBHOUSE_MISSION_WALL_STATE_LINK_RT")
			
				REQUEST_SCALEFORM_MOVIE("BIKER_MISSION_WALL")
				
				sClubHouseMissionMenu.pOwnerID = GET_CLUBHOUSE_PROPERTY_OWNER()
				REGISTER_AND_LINK_CLUBHOUSE_MISSION_WALL_RENDER_TARGET(sClubHouseMissionMenu, iCurrentProperty)
				
				IF sClubHouseMissionMenu.pOwnerID = PLAYER_ID()
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - I am the Clubhouse Property Owner - My ID: ", NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID))
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - I am Not the Clubhouse Property Owner - Owner ID: ", NATIVE_TO_INT(sClubHouseMissionMenu.pOwnerID))
				ENDIF
				
				CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - Changing State to: CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM")
				sClubHouseMissionMenu.eWallStates = CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM
			BREAK
			CASE CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM
				CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM")
				
				IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE)
				AND PROCESS_CLUBHOUSE_CLUBHOUSE_MISSION_WALL(sClubHouseMissionMenu)
					CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - Changing State to: CLUBHOUSE_MISSION_WALL_STATE_DRAW_WALL")
					sClubHouseMissionMenu.eWallStates = CLUBHOUSE_MISSION_WALL_STATE_DRAW_WALL
					DRAW_CLUBHOUSE_MISSION_WALL(sClubHouseMissionMenu)
				ENDIF
			BREAK
			CASE CLUBHOUSE_MISSION_WALL_STATE_DRAW_WALL
				DRAW_CLUBHOUSE_MISSION_WALL(sClubHouseMissionMenu)
				
				IF DOES_CLUBHOUSE_MISSION_WALL_NEED_UPDATING(sClubHouseMissionMenu)
					CDEBUG3LN(DEBUG_SAFEHOUSE, "MAINTAIN_CLUBHOUSE_MISSION_WALL - Clubhouse Owners Mission has Changed - Changing State to: CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM")
					sClubHouseMissionMenu.eWallStates = CLUBHOUSE_MISSION_WALL_STATE_UPDATE_SCALEFORM
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEANUP_CLUBHOUSE_MISSION_WALL(CLUBHOUSE_MISSION_MENU_STRUCT& sClubHouseMissionMenu, INT iCurrentProperty, BOOL bReleaseRT = TRUE)
	UNUSED_PARAMETER(iCurrentProperty)
	
	CLEAR_CLUBHOUSE_MISSION_FAIL_HELP()
	
	IF HAS_SCALEFORM_MOVIE_LOADED(sClubHouseMissionMenu.sScaleform)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sClubHouseMissionMenu.sScaleform)
	ENDIF
	IF bReleaseRT
		IF IS_NAMED_RENDERTARGET_REGISTERED("clubhouse_Plan_01a")
			RELEASE_NAMED_RENDERTARGET("clubhouse_Plan_01a")
		ENDIF
		sClubHouseMissionMenu.iRenderTargetID = -1
	ENDIF
	IF NOT IS_STRING_NULL_OR_EMPTY(sClubHouseMissionMenu.tlPrintedHelp)
	AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sClubHouseMissionMenu.tlPrintedHelp)
		CLEAR_HELP()
		sClubHouseMissionMenu.tlPrintedHelp = ""
	ENDIF
	sClubHouseMissionMenu.eWallStates = CLUBHOUSE_MISSION_WALL_STATE_LINK_RT
ENDPROC

/// PURPOSE:
///   Gets the vector used to place a blip in the clubhouse interior that indicates the position of the laptop
FUNC VECTOR GET_BLIP_COORDS_FOR_CLBH_LAPTOP(INT iCurrentProperty)
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN <<1117.0757, -3160.7412, -36.9992>>
	ENDIF
	
	RETURN <<1007.1451, -3169.9517, -39.0053>>
ENDFUNC

FUNC VECTOR GET_BLIP_COORDS_FOR_CLBH_BOARD(INT iCurrentProperty)
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN <<1111.0071, -3145.6238, -36.9992>>
	ENDIF
	
	RETURN <<1011.2646, -3150.8464, -39.9071>>
ENDFUNC

FUNC VECTOR GET_BLIP_COORDS_FOR_CLBH_BAR(INT iCurrentProperty)
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN <<1121.2280, -3144.6479, -36.9992>>
	ENDIF
	RETURN <<998.4948, -3170.1240, -34.0653>>
ENDFUNC

//Creates the malcom ped used in the clubhouse tutorial
PROC CREATE_MALC_PED(PED_INDEX &malcPed, INT iCurrentProperty)
	
	IF DOES_ENTITY_EXIST(malcPed)
		EXIT
	ENDIF
	
	REQUEST_ANIM_DICT("anim@amb@clubhouse@tutorial@bkr_tut_ig1@")
			
	IF NOT HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@tutorial@bkr_tut_ig1@")
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_MALC_PED: NOT HAS_ANIM_DICT_LOADED")
		EXIT
	ENDIF
		
	REQUEST_MODEL(IG_MALC)
	IF NOT HAS_MODEL_LOADED(IG_MALC)
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_MALC_PED: NOT HAS_MODEL_LOADED(IG_MALC)")
		EXIT
	ENDIF
	
	MP_PROP_OFFSET_STRUCT malcPedLocation = GET_MALC_LOCATION_FOR_CLUBHOUSE(iCurrentProperty, 0)
	
	malcPed = CREATE_PED(PEDTYPE_MISSION, IG_MALC, malcPedLocation.vLoc, malcPedLocation.vRot.z, FALSE)

	SET_MODEL_AS_NO_LONGER_NEEDED(IG_MALC)
	SET_ENTITY_CAN_BE_DAMAGED(malcPed, FALSE)
	SET_FORCE_FOOTSTEP_UPDATE(malcPed, TRUE)
ENDPROC

PROC CREATE_MECHANIC_PED_FOR_CUTSCENE(PED_INDEX &mechPed, INT iCurrentProperty)
	
	IF DOES_ENTITY_EXIST(mechPed)
		EXIT
	ENDIF
	
	REQUEST_ANIM_DICT("anim@amb@clubhouse@tutorial@bkr_tut_ig3@")
			
	IF NOT HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@tutorial@bkr_tut_ig3@")
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_MECHANIC_PED_FOR_CUTSCENE: NOT HAS_ANIM_DICT_LOADED")
		EXIT
	ENDIF
		
	REQUEST_MODEL(S_M_Y_XMech_02)
	IF NOT HAS_MODEL_LOADED(S_M_Y_XMech_02)
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_MECHANIC_PED_FOR_CUTSCENE: NOT HAS_MODEL_LOADED(S_M_Y_XMech_02)")
		EXIT
	ENDIF
	
	MP_PROP_OFFSET_STRUCT mechPedLocation = GET_PED_LOCATION_FOR_CLBH_SCENE(iCurrentProperty, 0)
	
	mechPed = CREATE_PED(PEDTYPE_MISSION, S_M_Y_XMech_02, mechPedLocation.vLoc, mechPedLocation.vRot.z, FALSE)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_MECHANIC_PED_FOR_CUTSCENE: created ped at: ", mechPedLocation.vLoc)

	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_XMech_02)
	SET_ENTITY_CAN_BE_DAMAGED(mechPed, FALSE)
	
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_HEAD, 1,1)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_BERD, 0,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_HAIR, 1,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_TORSO, 0,1)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_LEG, 1,1)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_HAND, 0,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_FEET, 0,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_TEETH, 0,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_SPECIAL, 0,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_SPECIAL2, 1,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_DECL, 1,0)
	SET_PED_COMPONENT_VARIATION(mechPed, PED_COMP_JBIB, 0,0)
	
	IF DOES_ENTITY_EXIST(mechPed)
	AND NOT IS_ENTITY_DEAD(mechPed)
		ANIMATION_FLAGS eFlags = AF_LOOPING
		TASK_PLAY_ANIM(mechPed, "anim@amb@clubhouse@tutorial@bkr_tut_ig3@", "machinic_loop_mechandplayer", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, eFlags)
	ENDIF
ENDPROC

PROC CREATE_MALC_CUTSCENE_BIKE(VEHICLE_INDEX &malcBike, INT iCurrentProperty)
	
	IF DOES_ENTITY_EXIST(malcBike)
		EXIT
	ENDIF
		
	REQUEST_MODEL(DOUBLE)
	IF NOT HAS_MODEL_LOADED(DOUBLE)
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_MALC_CUTSCENE_BIKE: NOT HAS_MODEL_LOADED(BATI)")
		EXIT
	ENDIF
	
	VECTOR vCoords = <<1104.2094, -3156.1160, -38.5186>>
	FLOAT fHeading = 268.9416
	
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iCurrentProperty) = PROPERTY_CLUBHOUSE_7_BASE_B
		vCoords = <<1006.8077, -3156.8828, -39.9076>>
		fHeading = 123.8971
	ENDIF
	
	malcBike = CREATE_VEHICLE(DOUBLE, vCoords, fHeading, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(DOUBLE)
	SET_ENTITY_CAN_BE_DAMAGED(malcBike, FALSE)
	SET_VEHICLE_COLOURS(malcBike, 38, 38)
	
ENDPROC
	
PROC CREATE_BARTENDER_CLONE_FOR_CSCENE(PED_INDEX &ped_Bartender, INT iCurrentProperty)

	IF DOES_ENTITY_EXIST(ped_Bartender)
		EXIT
	ENDIF
	
	REQUEST_ANIM_DICT("anim@amb@clubhouse@bar@drink@base")
			
	IF NOT HAS_ANIM_DICT_LOADED("anim@amb@clubhouse@bar@drink@base")
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_BARTENDER_CLONE_FOR_CSCENE: NOT HAS_ANIM_DICT_LOADED")
		EXIT
	ENDIF
		
	REQUEST_MODEL(MP_F_CHBAR_01)
	IF NOT HAS_MODEL_LOADED(MP_F_CHBAR_01)
		CWARNINGLN(DEBUG_SAFEHOUSE, "CREATE_MALC_PED: NOT HAS_MODEL_LOADED(MP_F_CHBAR_01)")
		EXIT
	ENDIF
	
	MP_PROP_OFFSET_STRUCT PedLocation = GET_PED_LOCATION_FOR_CLBH_SCENE(iCurrentProperty, 3)
	
	ped_Bartender = CREATE_PED(PEDTYPE_MISSION, MP_F_CHBAR_01, PedLocation.vLoc, PedLocation.vRot.z, FALSE)

	SET_MODEL_AS_NO_LONGER_NEEDED(MP_F_CHBAR_01)
	SET_ENTITY_CAN_BE_DAMAGED(ped_Bartender, FALSE)
	
	IF DOES_ENTITY_EXIST(ped_Bartender)
	AND NOT IS_ENTITY_DEAD(ped_Bartender)
		ANIMATION_FLAGS eFlags = AF_LOOPING | AF_TURN_OFF_COLLISION
		TASK_PLAY_ANIM(ped_Bartender, "anim@amb@clubhouse@bar@drink@base", "idle_a_bartender", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, DEFAULT, eFlags)
	ENDIF
ENDPROC

PROC CREATE_PLAYER_CLONE_FOR_CSCENE(PED_INDEX &ped_Clone, INT iCurrentProperty)

	IF DOES_ENTITY_EXIST(ped_Clone)
		EXIT
	ENDIF
	
	MP_PROP_OFFSET_STRUCT PedLocation = GET_PED_LOCATION_FOR_CLBH_SCENE(iCurrentProperty, 4)
	
	IF NOT IS_PLAYER_FEMALE()
		ped_Clone = CREATE_PED(PEDTYPE_CIVMALE, GET_ENTITY_MODEL(GET_PLAYER_PED(PLAYER_ID())), PedLocation.vLoc, PedLocation.vRot.z, FALSE, FALSE)
	ELSE
		ped_Clone = CREATE_PED(PEDTYPE_CIVFEMALE, GET_ENTITY_MODEL(GET_PLAYER_PED(PLAYER_ID())), PedLocation.vLoc, PedLocation.vRot.z, FALSE, FALSE)
	ENDIF
	
	CLONE_PED_TO_TARGET(PLAYER_PED_ID(), ped_Clone)
ENDPROC	

#IF FEATURE_DLC_1_2022
FUNC BOOL RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION(GENERIC_TRANSACTION_STATE &eResult, INT &iTransactionID, INT iEarnAmount)

	// Cleanup previous attempts
	IF eResult != TRANSACTION_STATE_DEFAULT
	AND eResult != TRANSACTION_STATE_PENDING
		eResult = TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		CASE TRANSACTION_STATE_DEFAULT
			IF NOT NETWORK_REQUEST_CASH_TRANSACTION(iTransactionID, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_EARN, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_EARN_CLUBHOUSE_DUFFLE_BAG, iEarnAmount, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)						
				CASSERTLN(DEBUG_AMBIENT, "[CLUBHOUSE_DUFFEL] RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION fail to cash consume transaction")
				DELETE_CASH_TRANSACTION(iTransactionID)
				eResult = TRANSACTION_STATE_FAILED
				RETURN TRUE
			ENDIF
			
			eResult = TRANSACTION_STATE_PENDING
			CPRINTLN(DEBUG_AMBIENT, "[CLUBHOUSE_DUFFEL] RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION request cash transaction - for iEarnAmount: ", iEarnAmount)
		BREAK
		CASE TRANSACTION_STATE_PENDING
			IF IS_CASH_TRANSACTION_COMPLETE(iTransactionID)
				IF GET_CASH_TRANSACTION_STATUS(iTransactionID) = CASH_TRANSACTION_STATUS_SUCCESS
					PRINTLN("[CLUBHOUSE_DUFFEL] RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION - Transaction ", iTransactionID, " successful!")
					
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(iTransactionID))
					
					NETWORK_EARN_BIKER(iEarnAmount)
					
					DELETE_CASH_TRANSACTION(iTransactionID)
					eResult = TRANSACTION_STATE_SUCCESS
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[CLUBHOUSE_DUFFEL] RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION - Transaction ", iTransactionID, " failed!")
					
					DELETE_CASH_TRANSACTION(iTransactionID)
					eResult = TRANSACTION_STATE_FAILED
					RETURN FALSE
				ENDIF
				
				iTransactionID = -1
			ELSE
				PRINTLN("[CLUBHOUSE_DUFFEL] RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION - Transaction ", iTransactionID, " not complete!")
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != TRANSACTION_STATE_PENDING)
ENDFUNC

FUNC BOOL GIVE_PLAYER_CLUBHOUSE_PASSIVE_CASH(GENERIC_TRANSACTION_STATE &eResult, INT &iTransactionID, INT iEarnAmount)
	IF USE_SERVER_TRANSACTIONS()
		RETURN RUN_CLUBHOUSE_COLLECT_PASSIVE_INCOME_TRANSACTION(eResult, iTransactionID, iEarnAmount)
	ENDIF
	
	PRINTLN("[CLUBHOUSE_DUFFEL] GIVE_PLAYER_CLUBHOUSE_PASSIVE_CASH - Console - Success - iEarnAmount: ", iEarnAmount)
	eResult = TRANSACTION_STATE_SUCCESS
	NETWORK_EARN_BIKER(iEarnAmount)
	RETURN TRUE
ENDFUNC

PROC ADD_CASH_TO_CLUBHOUSE_DUFFEL_BAG(INT iCashToAdd)
	INT iCashTotal = (GET_MP_INT_CHARACTER_STAT(MP_STAT_BIKER_BAR_RESUPPLY_CASH) + iCashToAdd)
	
	IF iCashTotal > g_sMPTunables.iBIKER_PASSIVE_INCOME_BAG_LIMIT
		SCRIPT_ASSERT("ADD_CASH_TO_CLUBHOUSE_DUFFEL_BAG - Bag is already full!")
		EXIT
	ENDIF
	
	PRINTLN("[CLUBHOUSE_DUFFEL] ADD_CASH_TO_CLUBHOUSE_DUFFEL_BAG - New toatl: ", iCashTotal, " after adding: ", iCashToAdd)
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iClubhouseDuffelBagCashTotal = iCashTotal
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BIKER_BAR_RESUPPLY_CASH, iCashTotal)
ENDPROC

PROC EMPTY_CLUBHOUSE_DUFFEL_BAG()	
	PRINTLN("[CLUBHOUSE_DUFFEL] EMPTY_CLUBHOUSE_DUFFEL_BAG - New toatl: 0 after removing: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iClubhouseDuffelBagCashTotal)	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iClubhouseDuffelBagCashTotal = 0
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BIKER_BAR_RESUPPLY_CASH, 0)
ENDPROC

FUNC BOOL IS_ANY_CASH_IN_PLAYERS_CLUBHOUSE_DUFFEL_BAG(PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(player)].propertyDetails.iClubhouseDuffelBagCashTotal > 0
ENDFUNC

FUNC INT GET_CASH_IN_PLAYERS_CLUBHOUSE_DUFFEL_BAG(PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		RETURN 0
	ENDIF
	
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(player)].propertyDetails.iClubhouseDuffelBagCashTotal
ENDFUNC

FUNC BOOL IS_PLAYERS_CLUBHOUSE_DUFFEL_BAG_FULL(PLAYER_INDEX player)
	IF player = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(player)].propertyDetails.iClubhouseDuffelBagCashTotal >= g_sMPTunables.iBIKER_PASSIVE_INCOME_BAG_LIMIT
ENDFUNC

FUNC BOOL IS_ANY_CASH_IN_CLUBHOUSE_DUFFEL_BAG()
	RETURN IS_ANY_CASH_IN_PLAYERS_CLUBHOUSE_DUFFEL_BAG(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_CLUBHOUSE_DUFFEL_BAG_FULL()
	RETURN IS_PLAYERS_CLUBHOUSE_DUFFEL_BAG_FULL(PLAYER_ID())
ENDFUNC

FUNC STRING GET_BIKER_PASSIVE_INCOME_SINDY_TEXT()
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
		CASE 0  RETURN "BBR_ADD_TEXT_1"
		CASE 1	RETURN "BBR_ADD_TEXT_2"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC MAINTAIN_BIKER_PASSIVE_INCOME_SINDY_TEXT()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		EXIT
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		EXIT
	ENDIF
	
	IF g_bShouldSendTextFromSindy
		IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_SINDY,  GET_BIKER_PASSIVE_INCOME_SINDY_TEXT(), TXTMSG_UNLOCKED, DEFAULT, DEFAULT, DEFAULT, DEFAULT,DEFAULT, DEFAULT, CANNOT_CALL_SENDER)
            g_bShouldSendTextFromSindy = FALSE
			IF IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID())
				PRINT_TICKER("BBR_ADD_FULL_I")			
			ELSE
				PRINT_TICKER_WITH_INT("BBR_ADD_FULL_O", g_sMPTunables.iBIKER_PASSIVE_INCOME_BAG_LIMIT)
			ENDIF
        ENDIF	
	ENDIF
ENDPROC

PROC MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT()

	IF NOT DOES_PLAYER_OWN_CLUBHOUSE(PLAYER_ID())
		EXIT
	ENDIF
	
	MAINTAIN_BIKER_PASSIVE_INCOME_SINDY_TEXT()
	
	IF NOT g_bMaintainBikerPassiveIncomeChecks
		EXIT
	ENDIF
		
	INT iDaysLeft = GET_PACKED_STAT_INT(PACKED_MP_INT_BIKER_PASSIVE_INCOME_DAYS_LEFT)
	
	//FAILSAFE
	IF iDaysLeft > g_sMPTunables.iBIKER_PASSIVE_INCOME_DAYS_TO_ADD
		iDaysLeft = g_sMPTunables.iBIKER_PASSIVE_INCOME_DAYS_TO_ADD
		SET_PACKED_STAT_INT(PACKED_MP_INT_BIKER_PASSIVE_INCOME_DAYS_LEFT, iDaysLeft)
	ENDIF
	
	IF NOT (iDaysLeft >0)
		PRINTLN("[CLUBHOUSE][PASSIVE INCOME] MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT - Player has no payouts - no longer checking passive income payouts")
		g_bMaintainBikerPassiveIncomeChecks = FALSE
		EXIT
	ENDIF
	
	INT iTimeLastResupplyCompleted = GET_MP_INT_CHARACTER_STAT(MP_STAT_BIKER_BAR_RESUPPLY_CD_POSIX)
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	
	INT iDaysPassed = (iCurrentTime - iTimeLastResupplyCompleted)/GAME_DAY_TIME_IN_SECONDS
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ResupplyIncomeInterval")
		iDaysPassed = (iCurrentTime - iTimeLastResupplyCompleted)/GET_COMMANDLINE_PARAM_INT("sc_ResupplyIncomeInterval")
	ENDIF
	#ENDIF
	
	INT iDaysAlreadyRewarded = g_sMPTunables.iBIKER_PASSIVE_INCOME_DAYS_TO_ADD - iDaysLeft
	
	IF iDaysAlreadyRewarded < iDaysPassed
		PRINTLN("[CLUBHOUSE][PASSIVE INCOME] MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT - iDaysLeft: ", iDaysLeft)
		PRINTLN("[CLUBHOUSE][PASSIVE INCOME] MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT - iDaysPassed: ", iDaysPassed)
		PRINTLN("[CLUBHOUSE][PASSIVE INCOME] MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT - iDaysAlreadyRewarded: ", iDaysAlreadyRewarded)
		INT iDaysToRewardMultiplier = iDaysPassed - iDaysAlreadyRewarded
		IF iDaysToRewardMultiplier > g_sMPTunables.iBIKER_PASSIVE_INCOME_DAYS_TO_ADD
			iDaysToRewardMultiplier = g_sMPTunables.iBIKER_PASSIVE_INCOME_DAYS_TO_ADD
		ENDIF
		PRINTLN("[CLUBHOUSE][PASSIVE INCOME] MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT - iDaysToRewardMultiplier: ", iDaysToRewardMultiplier)
		INT iCashToAdd = iDaysToRewardMultiplier * g_sMPTunables.iBIKER_PASSIVE_INCOME_DAILY_ADD
		
		IF GET_CASH_IN_PLAYERS_CLUBHOUSE_DUFFEL_BAG(PLAYER_ID()) + iCashToAdd > g_sMPTunables.iBIKER_PASSIVE_INCOME_BAG_LIMIT
			iCashToAdd = g_sMPTunables.iBIKER_PASSIVE_INCOME_BAG_LIMIT - GET_CASH_IN_PLAYERS_CLUBHOUSE_DUFFEL_BAG(PLAYER_ID())	
			//SINDY SHOULD SEND A TEXT MESSAGE HERE IF TOO MUCH MONEY IS IN THE BAG	
			g_bShouldSendTextFromSindy = TRUE
		ENDIF
		
		PRINTLN("[CLUBHOUSE][PASSIVE INCOME] MAINTAIN_BIKER_PASSIVE_INCOME_PAYOUT - Adding cash to Duffel Bag ($",iCashToAdd,") Payouts left: ", iDaysLeft-iDaysToRewardMultiplier)
		//ANOTHER FAILSAFE
		INT iNewDaysLeft = iDaysLeft-iDaysToRewardMultiplier
		IF iNewDaysLeft < 0
			iNewDaysLeft = 0
		ENDIF
		SET_PACKED_STAT_INT(PACKED_MP_INT_BIKER_PASSIVE_INCOME_DAYS_LEFT, iNewDaysLeft)
		
		IF iCashToAdd>0
			ADD_CASH_TO_CLUBHOUSE_DUFFEL_BAG(iCashToAdd)
			IF CAN_CALL_STAT_SAVE(STAT_SAVETYPE_END_SHOPPING, FALSE)
				REQUEST_SAVE(SSR_REASON_BIKER_PASSIVE_INCOME_UPDATE, STAT_SAVETYPE_END_SHOPPING)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF


