//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arena_turret_public.sch																	//
// Description: Global api for managing the drive-in turrets in the Arena.   								//
//				All methods are prefixed with ARENA_TURRET_ then follow usual naming conventions.		    //
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		29/08/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "script_maths.sch"
USING "script_network.sch"
USING "turret_cam_public.sch"

/// PURPOSE:
///    Wrapper for turret manager state change to keep a log trail.
///    Not really public but needs to be here to avoid circular includes.
PROC _TURRET_MANAGER_SET_LOCK_STATE(TURRET_MANAGER_LOCK_STATE eLockState)
	CDEBUG1LN(DEBUG_NET_TURRET, " _TURRET_MANAGER_SET_LOCK_STATE ", ENUM_TO_INT(g_eTurretManagerState), " --> ", ENUM_TO_INT(eLockState))
	g_eTurretManagerState = eLockState
ENDPROC

/// PURPOSE:
///    Returns TRUE if the local response data is out of date.
///    Not really public but needs to be here to avoid circular includes.
FUNC BOOL TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
	IF g_iTurretLockRequestUid <> TLRI_NONE
	AND g_iTurretLockRequestUid = g_iTurretLockResponseUid
	AND g_eTurretManagerState <> TLLS_UNLOCKING
	AND g_eTurretManagerState <> TLLS_LOCKING
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

DEBUGONLY PROC PRINT_PLAYER_TURRET_OCC(INT iPlayerIndex)
	TURRET_OCCUPANCY_DATA data = GlobalServerBD.turrets.players[iPlayerIndex]
	CDEBUG1LN(DEBUG_NET_TURRET, "    Player = ", iPlayerIndex, " Group = ", ENUM_TO_INT(data.groupType), "  InstanceId = ", data.iInstanceId,  "  TurretId = ", data.iTurretId)
ENDPROC

DEBUGONLY PROC PRINT_TURRET_OCC(TURRET_OCCUPANCY_DATA data)
	CDEBUG1LN(DEBUG_NET_TURRET, "    Group = ", ENUM_TO_INT(data.groupType), "  InstanceId = ", data.iInstanceId,  "  TurretId = ", data.iTurretId)
ENDPROC

DEBUGONLY PROC PRINT_TURRET_RQST(INT& in_iArray[])
	CDEBUG1LN(DEBUG_NET_TURRET, "    Group = ", in_iArray[ci_TURRET_RQST_GRP], "  InstanceId = ", in_iArray[ci_TURRET_RQST_INST], "  TurretId = ", in_iArray[ci_TURRET_RQST_TURRET], "  Uid = ", in_iArray[ci_TURRET_RQST_UID])
	CDEBUG1LN(DEBUG_NET_TURRET, "    Search = ", in_iArray[ci_TURRET_RQST_SEARCH], "  SettingsBs = ", in_iArray[ci_TURRET_RQST_BS])
ENDPROC

/// PURPOSE:
///    Print net time for turret manager.
DEBUGONLY PROC PRINT_TURRET_TIME()
	CDEBUG1LN(DEBUG_NET_TURRET, "    Network time = ", NATIVE_TO_INT(GET_NETWORK_TIME()))
ENDPROC

FUNC TURRET_GROUP_TYPE TURRET_MANAGER_GET_GROUP(PLAYER_INDEX player)
	IF player = PLAYER_ID()
	AND NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
		RETURN g_turretLockResponse.groupType
	ENDIF
	
	RETURN GlobalServerBD.turrets.players[NATIVE_TO_INT(player)].groupType
ENDFUNC

FUNC INT TURRET_MANAGER_GET_TURRET_ID(PLAYER_INDEX player)
	IF player = PLAYER_ID()
	AND NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
		RETURN g_turretLockResponse.iTurretId
	ENDIF
	
	RETURN GlobalServerBD.turrets.players[NATIVE_TO_INT(player)].iTurretId
ENDFUNC

FUNC INT TURRET_MANAGER_GET_INSTANCE_ID(PLAYER_INDEX player)
	IF player = PLAYER_ID()
	AND NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
		RETURN g_turretLockResponse.iInstanceId
	ENDIF
	
	RETURN GlobalServerBD.turrets.players[NATIVE_TO_INT(player)].iInstanceId
ENDFUNC

FUNC BOOL IS_PLAYER_USING_ANY_TURRET(PLAYER_INDEX player)
	RETURN TURRET_MANAGER_GET_GROUP(player) <> TGT_NONE
ENDFUNC

/// PURPOSE:
///    Checks whether a running turret_script_cam
///    was launched with an Id matching the last 
///    turret lock request.
/// RETURNS:
///  	TRUE if Ids match (turret_cam_script is valid).  
FUNC BOOL TURRET_MANAGER_CURRENT_CAM_MATCHES_LOCK()
	RETURN g_iTurretScriptLaunchUid = g_iTurretLockRequestUid
	AND NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
ENDFUNC

/// PURPOSE:
///    Returns true if the turret script is ready to be launched via
///    TURRET_MANAGER_LAUNCH_CAM_NOW.
///    
///    This function will return FALSE if a turret_cam_script that matches the 
///    last lock request from the turret_manager is already running.
///    See TURRET_MANAGER_CURRENT_CAM_MATCHES_LOCK() 
FUNC BOOL TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(ci_TURRET_SCRIPT_HASH) > 0
	OR NETWORK_IS_SCRIPT_ACTIVE("turret_cam_script", -1, TRUE)
		IF NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
		AND g_iTurretScriptLaunchUid = g_iTurretLockRequestUid
			RETURN FALSE // Already running correct instance
		ELSE
			CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY - Killing existing turret. Expected Uid = ", g_iTurretLockRequestUid, " current Uid = ", g_iTurretScriptLaunchUid)
			KILL_TURRET_CAM_SCRIPT()
			RETURN FALSE
		ENDIF		
	ELSE
		IF TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
			CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY - Lock request data invalid - ongoing request? no request set up?")
			RETURN FALSE
		ENDIF
		REQUEST_SCRIPT_WITH_NAME_HASH(ci_TURRET_SCRIPT_HASH)
		IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(ci_TURRET_SCRIPT_HASH)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Call after TURRET_MANAGER_IS_CAM_LAUNCH_NOW_READY return true.
///    This function does not verify whether anything has loaded.
PROC TURRET_MANAGER_LAUNCH_CAM_NOW(TURRET_CAM_ARGS& ref_args)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_LAUNCH_CAM_NOW - Launching turret with Uid = ", g_iTurretLockRequestUid)
	g_bKillTurretScript = FALSE
	START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(ci_TURRET_SCRIPT_HASH, ref_args, SIZE_OF(ref_args), DEFAULT_STACK_SIZE)			
	SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(ci_TURRET_SCRIPT_HASH)
	g_iTurretScriptLaunchUid = g_iTurretLockRequestUid
	g_sArena_Telemetry_data.m_controlTurret = 1
	PRINTLN("[ARENA][TEL] - used a turret - ", g_sArena_Telemetry_data.m_controlTurret)
ENDPROC

/// PURPOSE:
///    Launches a turret_cam_script. If an instance is already running it whether it 
///    was launched for the last turret lock request. If so then the script is left
///    running and this returns TRUE. If not then the existing script 
///    is shutdown and a new one is launched. This will return FALSE untill the 
///    new instance has been launched, at which point it will return TRUE.
///    
///    You can continue to call it even after the inital return of TRUE.
///    
/// RETURNS:
///    TRUE while the script is launched & running.
FUNC BOOL TURRET_MANAGER_LAUNCH_TURRET_CAM(TURRET_CAM_ARGS& ref_args)
	// if running with wrong credentials...	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(ci_TURRET_SCRIPT_HASH) > 0
	OR NETWORK_IS_SCRIPT_ACTIVE("turret_cam_script", -1, TRUE)
		IF NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
		AND g_iTurretScriptLaunchUid = g_iTurretLockRequestUid
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_LAUNCH_TURRET_CAM - Killing existing turret. Expected Uid = ", g_iTurretLockRequestUid, " current Uid = ", g_iTurretScriptLaunchUid)
			KILL_TURRET_CAM_SCRIPT()
			RETURN FALSE
		ENDIF		
	ELSE
		IF TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
			CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_LAUNCH_TURRET_CAM - Lock request data invalid - ongoing request? no request set up?")
			RETURN FALSE
		ENDIF
		REQUEST_SCRIPT_WITH_NAME_HASH(ci_TURRET_SCRIPT_HASH)
		IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(ci_TURRET_SCRIPT_HASH)
			CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_LAUNCH_TURRET_CAM - Launching turret with Uid = ", g_iTurretLockRequestUid)
			g_bKillTurretScript = FALSE
			START_NEW_SCRIPT_WITH_NAME_HASH_AND_ARGS(ci_TURRET_SCRIPT_HASH, ref_args, SIZE_OF(ref_args), DEFAULT_STACK_SIZE)			
			SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(ci_TURRET_SCRIPT_HASH)
			g_iTurretScriptLaunchUid = g_iTurretLockRequestUid
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns true if the turret manager has started unlocking
///    or is in the unlocked state.
///    
///    e.g. will return TRUE after a call to TURRET_MANAGER_UNLOCK()
/// RETURNS:
///    TRUE if unlocking or unlocked.
FUNC BOOL TURRET_MANAGER_IS_UNLOCKED()
	RETURN g_eTurretManagerState = TLLS_UNLOCKING
		OR g_eTurretManagerState = TLLS_UNLOCKED
ENDFUNC

/// PURPOSE:
///    Kills turret script.
///    Tells turret launcher system that the turret in use can be now used by other players.
/// PARAMS:
///    NOTE: The following checks are done against the last REQUEST (i.e. it will work on, and cancel, ongoing requests).
///    eGroup 	 	- Only kick the player if they're using the specified group. Leave as default to ignore this.
///    iInstanceId 	- Only kick the player if they're using the specified instance. Leave as default to ignore this.
///    iTurretId  	- Only kick the player if they're using the turret id. Leave as default to ignore this.
/// RETURNS:
///    TRUE if the turret was unlocked (e.g. FALSE if the turret was already unlocked or another turret type is running/requested)
FUNC BOOL TURRET_MANAGER_UNLOCK(TURRET_GROUP_TYPE eGroup = TGT_NONE, INT iInstanceId = -1, INT iTurretId = -1)
	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_UNLOCK")
	
	IF eGroup <> TGT_NONE
	AND g_turretLockRequest.groupType <> eGroup
		CDEBUG1LN(DEBUG_NET_TURRET, "   Group mismatch: g_turretLockRequest.groupType = ", g_turretLockRequest.groupType, " eGroup = ", eGroup)
		RETURN FALSE
	ENDIF
	
	IF iInstanceId <> -1
	AND g_turretLockRequest.iInstanceId <> iInstanceId
		CDEBUG1LN(DEBUG_NET_TURRET, "   Group mismatch: g_turretLockRequest.iInstanceId = ", g_turretLockRequest.iInstanceId, " iInstanceId = ", iInstanceId)
		RETURN FALSE
	ENDIF
	
	IF iTurretId <> -1
	AND g_turretLockRequest.iTurretId <> iTurretId
		CDEBUG1LN(DEBUG_NET_TURRET, "   Group mismatch: g_turretLockRequest.iTurretId = ", g_turretLockRequest.iTurretId, " iTurretId = ", iTurretId)
		RETURN FALSE
	ENDIF
	
	IF TURRET_MANAGER_IS_UNLOCKED()
		CDEBUG1LN(DEBUG_NET_TURRET, "   TURRET_MANAGER_IS_UNLOCKED = TRUE")
		RETURN FALSE
	ENDIF
	
	KILL_TURRET_CAM_SCRIPT()
	
	_TURRET_MANAGER_SET_LOCK_STATE(TLLS_UNLOCKING)
	g_turretLockRequest.groupType = TGT_NONE
	g_turretLockRequest.iInstanceId = -1
	g_turretLockRequest.iTurretId = -1
	g_eTurretLockSearchType = TLST_THIS_TURRET_ONLY
	g_iTurretLockBs = 0
	g_iTurretLockRequestUid = INT_TO_ENUM(TURRET_LOCK_REQUEST_ID, NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	CDEBUG1LN(DEBUG_NET_TURRET, "   uid = ", g_iTurretLockRequestUid)
	
	g_bNewTurretLockRequest = TRUE
	
	RETURN TRUE
ENDFUNC
	
/// PURPOSE:
///    Request use of a particular turret. It's essential that TURRET_MANAGER_UNLOCK is called after calling this
///    function when you are done using the turret slot.
///    
///    NOTE: 	The turret Id assigned to this player may differ from iTurretId depending on the param "eSearchType". 
///    			Please check TURRET_MANAGER_GET_TURRET_ID(PLAYER_ID()) to confirm assigned index.
/// PARAMS:
///    eType 		- Requested turret group type.
///    iInstanceId 	- Requested turret group InstanceId. 
///    iTurretId 	- Requested turret Id (see eSearchType)
///    eSearchType 	- Search modifier (see TURRET_LOCK_SEARCH_TYPE)
///    bKickOnFail 	- Kick the player from their current turret if the request fails?
/// RETURNS:
///    A requestId which should be passed into TURRET_MANAGER_GET_LOCK_STATUS.
FUNC TURRET_LOCK_REQUEST_ID TURRET_MANAGER_CREATE_LOCK_REQUEST(TURRET_GROUP_TYPE eType, INT iInstanceId, INT iTurretId, TURRET_LOCK_SEARCH_TYPE eSearchType = TLST_THIS_TURRET_ONLY, BOOL bKickOnFail = TRUE)
	TURRET_OCCUPANCY_DATA request 
	request.groupType = eType
	request.iInstanceId = iInstanceId
	request.iTurretId = iTurretId
	
	_TURRET_MANAGER_SET_LOCK_STATE(TLLS_LOCKING)
	g_turretLockRequest = request
	g_eTurretLockSearchType = eSearchType
	g_iTurretLockRequestUid = INT_TO_ENUM(TURRET_LOCK_REQUEST_ID, NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	
	g_iTurretLockBs = 0
	ENABLE_BIT(g_iTurretLockBs, ci_TURRET_LOCK_BS_DO_NOT_KICK, NOT bKickOnFail)
	
	g_bNewTurretLockRequest = TRUE

	CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_CREATE_LOCK_REQUEST Uid: ", ENUM_TO_INT(g_iTurretLockRequestUid))
	RETURN g_iTurretLockRequestUid 
ENDFUNC

/// PURPOSE:
///    Call every frame to check on the status of a turret lock request created with 
///    "TURRET_MANAGER_CREATE_LOCK_REQUEST".
///    Once this has returned TLR_SUCCESS you can see the up-to-date server response with:
///    TURRET_MANAGER_GET_ | TURRET_ID | GROUP | INSTANCE_ID | DATA
///    
///    Continues to return a valid result after first non-pending result.
///    
/// PARAMS:
///    eRequestId - Id returned from TURRET_MANAGER_CREATE_LOCK_REQUEST 
/// RETURNS:
///    See "TURRET_LOCK_RESPONSE" in "turret_manager_def.sch"
FUNC TURRET_LOCK_RESPONSE TURRET_MANAGER_GET_LOCK_STATUS(TURRET_LOCK_REQUEST_ID eRequestId)
	// If the request id matches the current ongoing request...
	IF g_iTurretLockRequestUid = eRequestId
		IF g_eTurretManagerState = TLLS_LOCKED
			IF g_iTurretLockRequestUid = g_iTurretLockResponseUid
				RETURN TLR_SUCCESS
			ELSE
				CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_REQUEST_LOCK (",ENUM_TO_INT(eRequestId),") - FAILED : REQUEST-RESPONSE MISMATCH")
				RETURN TLR_FAILED
			ENDIF
		ELIF g_eTurretManagerState = TLLS_UNLOCKED
			CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_REQUEST_LOCK (",ENUM_TO_INT(eRequestId),") - FAILED : TIMEOUT")
			RETURN TLR_FAILED	
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_NET_TURRET, "TURRET_MANAGER_REQUEST_LOCK (",ENUM_TO_INT(eRequestId),") - FAILED : OVERWRITTEN WITH NEW REQUEST (",ENUM_TO_INT(eRequestId),")")
		RETURN TLR_FAILED
	ENDIF
	
	RETURN TLR_PENDING
ENDFUNC

FUNC TURRET_OCCUPANCY_DATA TURRET_MANAGER_GET_DATA(PLAYER_INDEX player)
	IF player = PLAYER_ID()
	AND NOT TURRET_MANAGER_IS_LOCAL_DATA_DIRTY()
		RETURN g_turretLockResponse
	ENDIF
	
	RETURN GlobalServerBD.turrets.players[NATIVE_TO_INT(player)]
ENDFUNC	

