///    Prototype common sp to mp transition functions....
///    TODO Comment and tidy.

USING "globals.sch"

// Network Headers 
USING "net_include.sch"

// Char select / hot spawn / transition.
USING "selector_public.sch"

// For MP Mission Checks: eg: IS_PLAYER_ON_ANY_MP_MISSION()
USING "net_mission.sch"

CONST_INT TRANSITION_DEFAULT_CAM_FoV 60

FUNC BOOL CAN_PLAYER_SWAP_TEAM()

	RETURN FALSE

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
//		IF NETWORK_CAN_LEAVE_PED_BEHIND() = FALSE
//			NET_NL()NET_PRINT(" CAN_PLAYER_SWAP_TEAM: NETWORK_CAN_LEAVE_PED_BEHIND with ALIVE Ped = FALSE ")
//		ENDIF
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			RETURN(TRUE)
		ENDIF
	ELSE
//		IF NETWORK_CAN_LEAVE_PED_BEHIND()
//			NET_NL()NET_PRINT(" CAN_PLAYER_SWAP_TEAM: NETWORK_CAN_LEAVE_PED_BEHIND with DEAD Ped = FALSE ")
//		ENDIF
		RETURN(TRUE)
		
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC RESET_TEAM_SWAP_DATA()	
	g_MPCharData.iNewTeam = -1
	g_MPCharData.iSwapTeamState = SWAP_TEAM_STATE_INIT
	g_MPCharData.bPerformingTeamSwap = FALSE
	g_MPCharData.bPlayerWasDead = FALSE
	NET_PRINT("RESET_TEAM_SWAP_DATA() - called") NET_NL()
ENDPROC
PROC COPY_SKY_CAM_DATA_TO_TRANSITION_DATA()
	IF DOES_CAM_EXIST(g_SkyCamData.camSky)
		g_TransitionData.vSkyCamPos = GET_CAM_COORD(g_SkyCamData.camSky)
		g_TransitionData.vSkyCamRot = GET_CAM_ROT(g_SkyCamData.camSky)
		g_TransitionData.fSkyCamFov = GET_CAM_FOV(g_SkyCamData.camSky)					
	ENDIF
ENDPROC
PROC SET_SKY_CAM_FROM_TRANSITION_DATA()
	NET_PRINT_THREAD_ID() NET_PRINT("SET_SKY_CAM_FROM_TRANSITION_DATA - called") NET_NL()
	IF DOES_CAM_EXIST(g_SkyCamData.camSky)
		NET_PRINT_THREAD_ID() NET_PRINT("SET_SKY_CAM_FROM_TRANSITION_DATA - camera exists") NET_NL()
		SET_CAM_COORD(g_SkyCamData.camSky, g_TransitionData.vSkyCamPos)
		SET_CAM_ROT(g_SkyCamData.camSky, g_TransitionData.vSkyCamRot)
		SET_CAM_FOV(g_SkyCamData.camSky, g_TransitionData.fSkyCamFov)
	ENDIF	
ENDPROC

//FUNC BOOL START_TRANSITION_CAM()
//	HIDE_HUD_AND_RADAR_THIS_FRAME()
//	REQUEST_SCRIPT("TransitionCam")
//	IF HAS_SCRIPT_LOADED("TransitionCam")			
//		IF GET_NUMBER_OF_THREADS_RUNNING_THIS_SCRIPT( "TransitionCam" ) = 0
//			COPY_SKY_CAM_DATA_TO_TRANSITION_DATA()
//			START_NEW_SCRIPT( "TransitionCam", MINI_STACK_SIZE )
//			SET_SCRIPT_AS_NO_LONGER_NEEDED("TransitionCam")
//		ELSE
//			NET_PRINT("START_TRANSITION_CAM - script was already running") NET_NL()
//		ENDIF
//		RETURN(TRUE)
//	ELSE
//		PRINTSTRING("START_TRANSITION_CAM - loading script...")
//		PRINTNL()
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC



PROC CLEANUP_TRANSITION_CAM()
	#IF IS_DEBUG_BUILD
		IF DOES_CAM_EXIST(g_TransitionData.TransitionCam)
			NET_PRINT("CLEANUP_TRANSITION_CAM - camera exist") NET_NL()
		ELSE
			NET_PRINT("CLEANUP_TRANSITION_CAM - camera does not exist") NET_NL()	
		ENDIF
	#ENDIF
	g_TransitionData.TransitionCam = NULL
ENDPROC


PROC GET_SKY_CAM_INFO_ABOVE_PLAYER(VECTOR &vPosIn, VECTOR &vRotIn, PED_INDEX WhichPlayer)	
	
	
	// Get a point 10m Behind player and 500m abouve.
	CONST_FLOAT fDisBehind  10.0	
	CONST_FLOAT fDisUp  500.0	
	VECTOR vPos
	VECTOR vForward
	VECTOR vCamRot
	
	
	IF DOES_ENTITY_EXIST(WhichPlayer)
		IF IS_PED_INJURED(WhichPlayer)
			vPos = /*WITH DEADCHECK=FALSE*/ GET_ENTITY_COORDS(WhichPlayer, FALSE)
			vForward = << -SIN(0.0), COS(0.0), 0 >>
		ELSE
			vPos = GET_ENTITY_COORDS(WhichPlayer)				
			vForward = << -SIN(GET_ENTITY_HEADING(WhichPlayer)), COS(GET_ENTITY_HEADING(WhichPlayer)), 0 >>
		ENDIF
	ELSE
		vPos = GET_FINAL_RENDERED_CAM_COORD()
		vForward = << -SIN(0.0), COS(0.0), 0 >>
	ENDIF
	
	NORMALISE_VECTOR(vForward)	
	vPos -= ( (vForward * <<fDisBehind,fDisBehind,0.0>>))		
							
	vPos.z += fDisUp
	
	vPosIn = vPos
	
	/*IF IS_PED_INJURED(PLAYER_PED_ID())
		vRotIn = <<-87.5, 0.0, vCamRot.z>>)
	ELSE
		vRotIn = <<-90,0, GET_ENTITY_HEADING(PLAYER_PED_ID()) - 180 >>
	ENDIF*/
	vCamRot = GET_FINAL_RENDERED_CAM_ROT()
	vRotIn = <<-87.5, 0.0, vCamRot.z>>
	
	NET_PRINT("GET_SKY_CAM_INFO_ABOVE_PLAYER - successfully called") NET_NL()
	
ENDPROC

PROC SET_CAM_AS_SKY_CAM(CAMERA_INDEX inCam)
	g_SkyCamData.camSky = inCam
ENDPROC


FUNC BOOL CREATE_SKY_CAM(PED_INDEX WhichPlayer, BOOL bGetCoordsAbovePlayer = TRUE)
	
//	CAMERA_INDEX temp_cam
//	SELECTOR_CAM_STRUCT blankStruct
//	
//	// reset sky cam data
//	IF DOES_CAM_EXIST(g_SkyCamData.camSky)		
//		temp_cam = g_SkyCamData.camSky
//	ENDIF	
//	g_SkyCamData = blankStruct
//	g_SkyCamData.camSky = temp_cam
	
	NET_PRINT_THREAD_ID() NET_PRINT("CREATE_SKY_CAM- called") NET_NL()
	
	// create the 
	IF DOES_CAM_EXIST(g_SkyCamData.camSky)
		DESTROY_CAM(g_SkyCamData.camSky)
	ENDIF
	g_SkyCamData.camSky = CREATE_CAM( "DEFAULT_SCRIPTED_CAMERA" )
	
	SET_CAM_AFFECTS_AIMING(g_SkyCamData.camSky, FALSE)

	
	IF (bGetCoordsAbovePlayer)
		VECTOR vPos = <<0.0,0.0,0.0>>
		VECTOR vRot	= <<0.0,0.0,0.0>>		
		GET_SKY_CAM_INFO_ABOVE_PLAYER(vPos, vRot, WhichPlayer )	
		SET_CAM_COORD( g_SkyCamData.camSky, vPos   )
		SET_CAM_ROT( g_SkyCamData.camSky, vRot )
		SET_CAM_FOV( g_SkyCamData.camSky, TRANSITION_DEFAULT_CAM_FoV)
		
	ENDIF
	
	SET_CAM_ACTIVE(g_SkyCamData.camSky, TRUE)
	
	RETURN TRUE
	
ENDFUNC
//
//FUNC BOOL PROCCESS_SKY_CAM_TO_PLAYER_INTERP()			
//	RETURN NOT RUN_CAM_SPLINE_FROM_SKY_TO_PLAYER( g_SkyCamData )	
//ENDFUNC

FUNC BOOL UPDATE_TEAM_SWAP()

	SWITCH g_MPCharData.iSwapTeamState
		CASE SWAP_TEAM_STATE_INIT				
			IF IS_PED_INJURED(PLAYER_PED_ID())
		
				IF NOT IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADING_IN()	
				AND NOT IS_SCREEN_FADED_OUT()						
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					NET_PRINT("UPDATE_TEAM_SWAP - screen is fading") NET_NL()							
				ELSE
					IF IS_SCREEN_FADED_OUT()	
						// player is dead and screen is faced out	
						//END_RESPAWN_PREVIEW_CAMERA()
						//SET_LOCAL_PLAYER_TEAM(g_MPCharData.iNewTeam)
						//NET_PRINT("UPDATE_TEAM_SWAP - faded out and dead so swapping team and returning TRUE") NET_NL()				
						RETURN(TRUE)
					ENDIF
				ENDIF
			ELSE
				RETURN(TRUE)
//				IF START_TRANSITION_CAM()
//					//END_RESPAWN_PREVIEW_CAMERA()
//					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_NO_COLLISION | NSPC_CLEAR_TASKS)
//					CREATE_SKY_CAM(FALSE)
//					SET_SKY_CAM_FROM_TRANSITION_DATA()
//					NET_PRINT_TIME() NET_PRINT("SpToMpTrans: created sky cam from transition data") NET_NL()
//					g_MPCharData.iSwapTeamState = SWAP_TEAM_STATE_CAMERA_SWOOP_UP						
//					REQUEST_SCRIPT("TransitionCam")
//				ENDIF
			ENDIF
		BREAK
//		CASE SWAP_TEAM_STATE_CAMERA_SWOOP_UP
//			HIDE_HUD_AND_RADAR_THIS_FRAME()			
//			IF NOT RUN_CAM_SPLINE_FROM_PLAYER_TO_SKY_CAM(g_SkyCamData, 1.5 )
//				NET_PRINT("UPDATE_TEAM_SWAP - SWOOP UP - FINISHED") NET_NL()				
//				RETURN(TRUE)
//			ELSE
//				NET_PRINT("UPDATE_TEAM_SWAP - SWOOP UP - RUNNING ") NET_NL()	
//			ENDIF	
//		BREAK					
	ENDSWITCH
		
	RETURN(FALSE)
				
ENDFUNC

/// PURPOSE: TODO Check if we have online privileges and are not already in mp.
FUNC BOOL CAN_START_MP()

	RETURN NOT NETWORK_IS_GAME_IN_PROGRESS()

ENDFUNC

PROC SETUP_TEAM_NUMBERS_FOR_GAME_MODE()


ENDPROC

PROC RESET_TELL_HUD_TO_PREPARE_FOR_A_TRANSITION()
	g_TransitionData.bHasHudQuitByCancelling = TRUE
ENDPROC

PROC TELL_HUD_TO_PREPARE_FOR_A_TRANSITION()
	g_TransitionData.bHasHudQuitByCancelling = FALSE
ENDPROC

FUNC BOOL IS_HUD_DOING_A_TRANSITION()
	RETURN NOT(g_TransitionData.bHasHudQuitByCancelling)
ENDFUNC



