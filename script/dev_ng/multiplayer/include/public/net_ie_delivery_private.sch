USING "globals.sch"
USING "net_include.sch"


USING "net_ie_dropoff_public.sch"
USING "gb_vehicle_export_freemode_header.sch"
USING "net_simple_interior.sch"

// Some delivery locations need crates present in freemode that are used to load vehicles
PROC IE_DELIVERY_GET_DROPOFF_CRATE_DATA(INT iCrateID, VECTOR &vCoords, FLOAT &fHeading, MODEL_NAMES &eModel)
	SWITCH iCrateID
		CASE 0
			vCoords = <<889.3146, -2919.0200, 4.9006>>
			fHeading = 270.0
			eModel = PROP_CONTAINER_LD
		BREAK
		CASE 1
			vCoords = <<937.1108, -2920.2805, 4.9021>>
			fHeading = 270.0
			eModel = PROP_CONTAINER_LD
		BREAK
		CASE 2
			vCoords = <<980.5549, -2920.3389, 4.9021>>
			fHeading = 270.0
			eModel = PROP_CONTAINER_LD
		BREAK
		CASE 3
			vCoords = <<1099.6295, -2920.1765, 4.9021>>
			fHeading = 270.0
			eModel = PROP_CONTAINER_LD
		BREAK
		CASE 4
			vCoords = <<1157.9714, -2919.9954, 4.9021>>
			fHeading = 270.0
			eModel = PROP_CONTAINER_LD
		BREAK
		CASE 5
			vCoords = <<1171.1613, -2970.1692, 4.9021>>
			fHeading = 90.0
			eModel = PROP_CONTAINER_LD
		BREAK
	ENDSWITCH
ENDPROC

// Tese are only used for cutscene purposes
ENUM IE_DELIVERY_DROPOFF_CUTSCENE_TYPE
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_INVALID,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CARPARK,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE_ALT,
	IE_DELIVERY_DORPOFF_CUTSCENE_TYPE_CARPARK_ALT,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD_GARAGE,
	IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
ENDENUM

FUNC IE_DELIVERY_DROPOFF_CUTSCENE_TYPE IE_DELIVERY_GET_DROPOFF_CUTSCENE_TYPE_FROM_DROPOFF_ID(IE_DROPOFF eDropOff)
	SWITCH eDropOff
		CASE IE_DROPOFF_DOCK_1
		CASE IE_DROPOFF_DOCK_2
		CASE IE_DROPOFF_DOCK_3
		CASE IE_DROPOFF_DOCK_4
		CASE IE_DROPOFF_DOCK_5
		CASE IE_DROPOFF_DOCK_6
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CRANE
			
		CASE IE_DROPOFF_GANGSTER_1
		CASE IE_DROPOFF_POLICE_STATION_4
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE
			
		CASE IE_DROPOFF_POLICE_STATION_2
		CASE IE_DROPOFF_POLICE_STATION_5
		CASE IE_DROPOFF_POLICE_STATION_7
		CASE IE_DROPOFF_POLICE_STATION_8
		CASE IE_DROPOFF_POLICE_STATION_9
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_CARPARK
			
		//CASE IE_DROPOFF_RAMP_TEST
		CASE IE_DROPOFF_PRIVATE_BUYER_5
		CASE IE_DROPOFF_SHOWROOM_2
		CASE IE_DROPOFF_SHOWROOM_3
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_RAMP
			
		CASE IE_DROPOFF_PRIVATE_BUYER_1
		CASE IE_DROPOFF_PRIVATE_BUYER_2
		CASE IE_DROPOFF_PRIVATE_BUYER_3
		CASE IE_DROPOFF_PRIVATE_BUYER_4
		
		CASE IE_DROPOFF_SHOWROOM_1
		CASE IE_DROPOFF_SHOWROOM_4
		CASE IE_DROPOFF_SHOWROOM_5
		CASE IE_DROPOFF_SHOWROOM_6
		CASE IE_DROPOFF_SHOWROOM_7
		CASE IE_DROPOFF_SHOWROOM_8
		
		CASE IE_DROPOFF_GANGSTER_4
		CASE IE_DROPOFF_GANGSTER_5
		CASE IE_DROPOFF_GANGSTER_6
		CASE IE_DROPOFF_GANGSTER_7
		CASE IE_DROPOFF_GANGSTER_8
		
		CASE IE_DROPOFF_WAREHOUSE_1
		CASE IE_DROPOFF_WAREHOUSE_2
		CASE IE_DROPOFF_WAREHOUSE_3
		CASE IE_DROPOFF_WAREHOUSE_4
		CASE IE_DROPOFF_WAREHOUSE_5
		CASE IE_DROPOFF_WAREHOUSE_6
		CASE IE_DROPOFF_WAREHOUSE_7
		CASE IE_DROPOFF_WAREHOUSE_8
		CASE IE_DROPOFF_WAREHOUSE_9
		CASE IE_DROPOFF_WAREHOUSE_10
		
		CASE IE_DROPOFF_POLICE_STATION_1
		CASE IE_DROPOFF_POLICE_STATION_3
		CASE IE_DROPOFF_POLICE_STATION_6
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GARAGE_ALT
			
		CASE IE_DROPOFF_SCRAPYARD_1
		CASE IE_DROPOFF_SCRAPYARD_3
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD_GARAGE
			
		CASE IE_DROPOFF_SCRAPYARD_2
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_SCRAPYARD
			
		CASE IE_DROPOFF_GANGSTER_2
		CASE IE_DROPOFF_GANGSTER_3
			RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_GOON_PICKUP
	ENDSWITCH
	
	RETURN IE_DELIVERY_DROPOFF_CUTSCENE_TYPE_INVALID
ENDFUNC

/// PURPOSE:
///    If TRUE dropoff will use faster version of the cutscene. This is so I can introduce this selectively.
///    (see url:bugstar:3212471)
/// PARAMS:
///    eDropoffID - 
/// RETURNS:
///    
FUNC BOOL IE_DROPOFF_HAS_INCREASED_DELIVERY_URGENCY(IE_DROPOFF eDropoffID)
	SWITCH eDropoffID
		CASE IE_DROPOFF_PRIVATE_BUYER_1
		CASE IE_DROPOFF_PRIVATE_BUYER_2
		CASE IE_DROPOFF_PRIVATE_BUYER_3
		CASE IE_DROPOFF_PRIVATE_BUYER_4
		
		CASE IE_DROPOFF_SHOWROOM_1
		CASE IE_DROPOFF_SHOWROOM_4
		CASE IE_DROPOFF_SHOWROOM_5
		CASE IE_DROPOFF_SHOWROOM_6
		CASE IE_DROPOFF_SHOWROOM_7
		CASE IE_DROPOFF_SHOWROOM_8

		CASE IE_DROPOFF_GANGSTER_1
		CASE IE_DROPOFF_GANGSTER_4
		CASE IE_DROPOFF_GANGSTER_5
		CASE IE_DROPOFF_GANGSTER_6
		CASE IE_DROPOFF_GANGSTER_7
		CASE IE_DROPOFF_GANGSTER_8
		
		CASE IE_DROPOFF_WAREHOUSE_1
		CASE IE_DROPOFF_WAREHOUSE_2
		CASE IE_DROPOFF_WAREHOUSE_3
		CASE IE_DROPOFF_WAREHOUSE_4
		CASE IE_DROPOFF_WAREHOUSE_5
		CASE IE_DROPOFF_WAREHOUSE_6
		CASE IE_DROPOFF_WAREHOUSE_7
		CASE IE_DROPOFF_WAREHOUSE_8
		CASE IE_DROPOFF_WAREHOUSE_9
		CASE IE_DROPOFF_WAREHOUSE_10
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC IE_DELIVERY_MAINTAIN_FREEMODE_DATA(IE_DELIVERY_FREEMODE_DATA &data)
	IF IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_INITIALISED)
		EXIT
	ENDIF

	IF NETWORK_IS_ACTIVITY_SESSION()
	OR NETWORK_IS_IN_TUTORIAL_SESSION()
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Not doing anything as in tutorial or activity session.")
		#ENDIF
		SET_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_INITIALISED)
	ENDIF
	
	INT i
	
	PLAYER_INDEX pCurrent = INT_TO_PLAYERINDEX(data.iCratePlayerStagger)
	
	IF IS_NET_PLAYER_OK(pCurrent)
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(pCurrent)
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(pCurrent) = FMMC_TYPE_VEHICLE_EXPORT_SELL
			//#IF IS_DEBUG_BUILD
			//	PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Player is on sell missionm let's check their dropoffs ", GET_PLAYER_NAME(pCurrent))
			//#ENDIF
		
			REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
				// If that player has dock dropoff active
				IF ENUM_TO_INT(GlobalPlayerBD_FM_3[data.iCratePlayerStagger].sMagnateGangBossData.eSelectedSellDropoffs[i]) >= ENUM_TO_INT(IE_DROPOFF_DOCK_1)
				AND ENUM_TO_INT(GlobalPlayerBD_FM_3[data.iCratePlayerStagger].sMagnateGangBossData.eSelectedSellDropoffs[i]) <= ENUM_TO_INT(IE_DROPOFF_DOCK_6)
					SET_BIT(data.iShouldCrateBeActiveTempBS, ENUM_TO_INT(GlobalPlayerBD_FM_3[data.iCratePlayerStagger].sMagnateGangBossData.eSelectedSellDropoffs[i]))
					//#IF IS_DEBUG_BUILD
					//	PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Dropoff id ", i, " is at docks!")
					//#ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	VECTOR vCrateCoords, vModelMin, vModelMax
	FLOAT fCrateHeading
	MODEL_NAMES eBaseCrateModel, eCrateModel
	
	IF data.iCratePlayerStagger = NUM_NETWORK_REAL_PLAYERS() - 1
	AND (data.iShouldCrateBeActiveTempBS > 0 OR data.iShouldCrateBeActiveBS > 0) 
		// We made a full cycle
		
		REPEAT IE_DELIVERY_DROP_OFF_CRATES_COUNT i
			IE_DELIVERY_GET_DROPOFF_CRATE_DATA(i, vCrateCoords, fCrateHeading, eCrateModel)
			FLOAT fDist = VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vCrateCoords)
			
			IF IS_BIT_SET(data.iShouldCrateBeActiveBS, i)
				IF (NOT IS_BIT_SET(data.iShouldCrateBeActiveTempBS, i)
				OR fDist > g_IEDeliveryData.fDistanceForCratesCreation + 10.0)
				AND NOT IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_DONT_MODIFY_CRATES)
					CLEAR_BIT(data.iShouldCrateBeActiveBS, i)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Crate ID ", i, " shouldn't be active.")
					#ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(data.iShouldCrateBeActiveTempBS, i)
				AND fDist < g_IEDeliveryData.fDistanceForCratesCreation
					SET_BIT(data.iShouldCrateBeActiveBS, i)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Crate ID ", i, " is close enough and is active for some players, should be acitve.")
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		data.iShouldCrateBeActiveTempBS = 0
	ENDIF
		
	data.iCratePlayerStagger = (data.iCratePlayerStagger + 1) % NUM_NETWORK_REAL_PLAYERS()
	
	// If there are some crates that we need to delete or create
	IF data.iShouldCrateBeActiveBS != data.iIsCrateActiveBS
		
		IE_DELIVERY_GET_DROPOFF_CRATE_DATA(0, vCrateCoords, fCrateHeading, eBaseCrateModel) // assume all crates use the same model
		
		REPEAT IE_DELIVERY_DROP_OFF_CRATES_COUNT i
		
			IE_DELIVERY_GET_DROPOFF_CRATE_DATA(i, vCrateCoords, fCrateHeading, eCrateModel)
		
			IF IS_BIT_SET(data.iShouldCrateBeActiveBS, i)
			AND NOT IS_BIT_SET(data.iCreateCrateBS, i)
				// All conditions for creating the crate are met
				SET_BIT(data.iCreateCrateBS, i)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Will create crate ", i)
				#ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(data.iShouldCrateBeActiveBS, i)
			AND IS_BIT_SET(data.iCreateCrateBS, i)
				// All conditions for removing the crate
				CLEAR_BIT(data.iCreateCrateBS, i)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Will delete crate ", i)
				#ENDIF
			ENDIF
					
			IF IS_BIT_SET(data.iCreateCrateBS, i)
			AND NOT IS_BIT_SET(data.iIsCrateActiveBS, i)
				IF NOT IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_DATA_REQUESTED)
					REQUEST_MODEL(eBaseCrateModel)
					SET_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_DATA_REQUESTED)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Requested the model.")
					#ENDIF
				ENDIF
				
				IF HAS_MODEL_LOADED(eBaseCrateModel)
					GET_MODEL_DIMENSIONS(eCrateModel, vModelMin, vModelMax)
					FLOAT fModelRadius = VDIST(vModelMin, vModelMax) * 0.5
					
					//#IF IS_DEBUG_BUILD
					//DRAW_DEBUG_BOX(vCrateCoords - <<fModelRadius, fModelRadius, fModelRadius>>, vCrateCoords + <<fModelRadius, fModelRadius, fModelRadius>>, 255, 0, 0, 128)
					//#ENDIF
					
					IF NOT IS_AREA_OCCUPIED(vCrateCoords - <<fModelRadius, fModelRadius, fModelRadius>>, vCrateCoords + <<fModelRadius, fModelRadius, fModelRadius>>, FALSE, TRUE, TRUE, FALSE, FALSE)
						data.objCrates[i] = CREATE_OBJECT_NO_OFFSET(eBaseCrateModel, vCrateCoords, FALSE)
						IF NOT IS_ENTITY_DEAD(data.objCrates[i])
							SET_ENTITY_HEADING(data.objCrates[i], fCrateHeading)
							FREEZE_ENTITY_POSITION(data.objCrates[i], TRUE)
						ENDIF
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Created crate ID ", i)
						#ENDIF
						
						SET_BIT(data.iIsCrateActiveBS, i)
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - There's something blocking the way for creation of crate ", i)
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Waiting for model to load.")
					#ENDIF
				ENDIF
			
			ELIF NOT IS_BIT_SET(data.iCreateCrateBS, i)
			AND IS_BIT_SET(data.iIsCrateActiveBS, i)
				IF DOES_ENTITY_EXIST(data.objCrates[i])
				AND data.fCurrentCrateAlpha[i] < 0.1
					DELETE_OBJECT(data.objCrates[i])
					CLEAR_BIT(data.iIsCrateActiveBS, i)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Removed crate ID ", i)
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
	ELSE
		IF IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_DATA_REQUESTED)
			IE_DELIVERY_GET_DROPOFF_CRATE_DATA(0, vCrateCoords, fCrateHeading, eBaseCrateModel) // assume all crates use the same model
			SET_MODEL_AS_NO_LONGER_NEEDED(eBaseCrateModel)
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_DATA_REQUESTED)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Cleaning up the model.")
			#ENDIF
		ENDIF
	ENDIF
	
	IF data.iIsCrateActiveBS > 0
		REPEAT IE_DELIVERY_DROP_OFF_CRATES_COUNT i
			IF DOES_ENTITY_EXIST(data.objCrates[i])
				BOOL bShouldFadeIn = IS_BIT_SET(data.iCreateCrateBS, i) AND data.fCurrentCrateAlpha[i] != 1.0
				BOOL bShouldFadeOut = NOT IS_BIT_SET(data.iCreateCrateBS, i) AND data.fCurrentCrateAlpha[i] > 0.0
				
				IF bShouldFadeIn
					FLOAT fTimeFactor = TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), data.timeLastFrameTimeForFade)) / 1000.0
					IF data.fCurrentCrateAlpha[i] < 1.0
						data.fCurrentCrateAlpha[i] = FMIN(1.0, data.fCurrentCrateAlpha[i] + fTimeFactor * 0.7)
					ELSE
						CLEAR_BIT(data.iCrateFadingIn, i)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Set alpha (fade in) of crate ", i, " - ", data.fCurrentCrateAlpha[i])
					#ENDIF
					
					SET_ENTITY_ALPHA(data.objCrates[i], ROUND(data.fCurrentCrateAlpha[i] * 255.0), FALSE)
				
				ELIF bShouldFadeOut
					FLOAT fTimeFactor = TO_FLOAT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), data.timeLastFrameTimeForFade)) / 1000.0
					IF data.fCurrentCrateAlpha[i] > 0.0
						data.fCurrentCrateAlpha[i] = FMAX(0.0, data.fCurrentCrateAlpha[i] - fTimeFactor * 0.7)
					ELSE
						CLEAR_BIT(data.iCrateFadingOut, i)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_FREEMODE_DATA - Set alpha (fade out) of crate ", i, " - ", data.fCurrentCrateAlpha[i])
					#ENDIF
						
					SET_ENTITY_ALPHA(data.objCrates[i], ROUND(data.fCurrentCrateAlpha[i] * 255.0), FALSE)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	data.timeLastFrameTimeForFade = GET_NETWORK_TIME()
ENDPROC

FUNC INT IE_DELIVERY_GET_INSTANCE_OF_CUTSCENE_SCRIPT_TO_LAUNCH()
	// To make sure that everyone in the car is running the same instance, use driver's ID as script instance
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAYER_INDEX playerDriver = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
	
		IF playerDriver != INVALID_PLAYER_INDEX()
			INT iInstance = NATIVE_TO_INT(playerDriver)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_GET_INSTANCE_OF_CUTSCENE_SCRIPT_TO_LAUNCH - Player driver is ", GET_PLAYER_NAME(playerDriver), " and the instance will be ", iInstance)
			#ENDIF
			
			RETURN iInstance
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_GET_INSTANCE_OF_CUTSCENE_SCRIPT_TO_LAUNCH - Driver is not ok, returning instance -1.")
	#ENDIF
	
	RETURN -1
ENDFUNC

PROC IE_DELIVERY_CLEANUP_FREEMODE_DATA(IE_DELIVERY_FREEMODE_DATA &data)
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_CLEANUP_FREEMODE_DATA - Doing the cleanup of crates")
	#ENDIF
	UNUSED_PARAMETER(data)	
	
	INT i
	REPEAT COUNT_OF(data.objCrates) i
		IF DOES_ENTITY_EXIST(data.objCrates[i])
			DELETE_OBJECT(data.objCrates[i])
		ENDIF
	ENDREPEAT
	
	IF IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_DATA_REQUESTED)
		VECTOR vFoo
		FLOAT fFoo
		MODEL_NAMES eBaseCrateModel
		IE_DELIVERY_GET_DROPOFF_CRATE_DATA(0, vFoo, fFoo, eBaseCrateModel) // assume all crates use the same model
		SET_MODEL_AS_NO_LONGER_NEEDED(eBaseCrateModel)
		CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_DATA_REQUESTED)
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_CLEANUP_FREEMODE_DATA - Cleaning up the model.")
		#ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR(VEHICLE_INDEX vehDelivered, PLAYER_INDEX pDeliverer, IE_DROPOFF eDropOff = IE_DROPOFF_INVALID, INT iVehValue = 0)
	//BOOL bFoo
	//VEHICLE_INDEX vehDelivered = IE_DELIVERY_GET_VEHICLE_PLAYER_IS_DELIVERING(PLAYER_ID(), bFoo, bFoo)
	
	IF vehDelivered != NULL
	AND DOES_ENTITY_EXIST(vehDelivered)
	AND NOT IS_ENTITY_DEAD(vehDelivered)
		//IF IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(vehDelivered) = PLAYER_ID()
		IF IS_NET_PLAYER_OK(pDeliverer)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - We are in charge of doing this delivery so we're gonna send the broadcast about delivering.")
			#ENDIF
			
			PLAYER_INDEX playerOriginalOwner = GB_GET_OWNER_OF_CONTRABAND(vehDelivered)
			
			IF playerOriginalOwner != INVALID_PLAYER_INDEX()
				
				INT iExportVehIndex = GB_GET_INDEX_OF_CONTRABAND(vehDelivered)
				MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehDelivered)
				
				IF iExportVehIndex != -1
					IF eDropoff != IE_DROPOFF_INVALID
						// Get the delivery reward value.
						INT iDeliveryRewardValue = iVehValue
						
						IE_VEHICLE_ENUM eIEVeh = GB_GET_EXPORT_VEHICLE_ENUM(vehDelivered)
						
						INT iRepairCost
						IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(eDropOff) // Repair cost is charged any time someone delivers for store in a warehouse
							iRepairCost = g_iVehicleExportStealMissionRepairCost
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - iRepairCost is ", iRepairCost)
							#ENDIF
						ENDIF
						
						IF eIEVeh != IE_VEH_INVALID
							g_IEDeliveryData.bTransactionPending = TRUE
							BROADCAST_PLAYER_DELIVERED_VEHICLE(pDeliverer, playerOriginalOwner, vehicleModel, iExportVehIndex, eIEVeh, iDeliveryRewardValue, iRepairCost, eDropOff)
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - IE_VEHICLE_ENUM is invalid!")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - DropoffID is invalid!")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - GB_GET_INDEX_OF_CONTRABAND returned -1!")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - GB_GET_OWNER_OF_CONTRABAND returned INVALID_PLAYER_INDEX!")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - Player deliverer not ok.")
			#ENDIF
		ENDIF
		//ELSE
		//	#IF IS_DEBUG_BUILD
		//		PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - We're not in charge!")
		//	#ENDIF
		//ENDIF	
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_LOCAL_PLAYER_DELIVERED_CAR - Player is not in an export vehicle - or it's dead or doesnt exist!")
		#ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_SEND_BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE(VEHICLE_INDEX vehDelivered, PLAYER_INDEX pDeliverer)
	IF vehDelivered != NULL
	AND DOES_ENTITY_EXIST(vehDelivered)
	AND NOT IS_ENTITY_DEAD(vehDelivered)
		
		PLAYER_INDEX playerOriginalOwner = GB_GET_OWNER_OF_CONTRABAND(vehDelivered)
		IF playerOriginalOwner != INVALID_PLAYER_INDEX()
				
			INT iExportVehIndex = GB_GET_INDEX_OF_CONTRABAND(vehDelivered)
			MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehDelivered)
				
			IF iExportVehIndex != -1
				BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE(playerOriginalOwner, pDeliverer, vehicleModel, iExportVehIndex)
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE - iExportVehIndex is -1")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE - playerOriginalOwner is invalid.")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_SEND_BROADCAST_REQUEST_SERVER_PERMISSION_TO_CLEANUP_EXPORT_VEHICLE - Player is not in an export vehicle - or it's dead or doesnt exist!")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IE_DELIVERY_IS_SCRIPT_RUNNING()
	RETURN (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("GB_IE_DELIVERY_CUTSCENE")) > 0)
ENDFUNC

PROC IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT(VECTOR vStartPoint, IE_DROPOFF eDropoffID)
	IF NOT g_IEDeliveryData.bLaunchingScript
	AND NOT IE_DELIVERY_IS_SCRIPT_RUNNING()
		//g_IEDeliveryData.bStartCutscene = FALSE
		g_IEDeliveryData.bLaunchingScript = TRUE
		g_IEDeliveryData.vLaunchPoint = vStartPoint
		g_IEDeliveryData.eDropoffIDForLaunch = eDropoffID
		
		REQUEST_SCRIPT("GB_IE_DELIVERY_CUTSCENE")
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Will launch at location ", vStartPoint, " for drop off: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(eDropoffID))
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Not launching, g_IEDeliveryData.bLaunchingScript: ", g_IEDeliveryData.bLaunchingScript, ", IE_DELIVERY_IS_SCRIPT_RUNNING(): ", IE_DELIVERY_IS_SCRIPT_RUNNING())
		#ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_CLEAN_GLOBAL_PLAYER_BD()
	CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_RUNNING_DELIVERY_SCRIPT)
	CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_PLAYING_DELIVERY_CUTSCENE)
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.eRunningDeliveryScriptForDropoffID = IE_DROPOFF_INVALID
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_CLEAN_GLOBAL_PLAYER_BD - Flags have been cleared.")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Maintains a bitset of active dropoffs for player in the car.
///    This is so others can see which dropoffs are active for this player in the current car.
/// PARAMS:
///    data - 
PROC IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS(IE_DELIVERY_FREEMODE_DATA &data)
	g_IEDeliveryData.vehCurrentlyDelivering = IE_DELIVERY_GET_VEHICLE_PLAYER_IS_DELIVERING(PLAYER_ID(), g_IEDeliveryData.bDeliveryViaCargobob, g_IEDeliveryData.bDeliveryViaTowtruck)
	
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
		IF IE_DELIVERY_IS_THIS_VEHICLE_ALREADY_BEING_DELIVERED(g_IEDeliveryData.vehCurrentlyDelivering)
			g_IEDeliveryData.vehCurrentlyDelivering = NULL
		ENDIF
	ENDIF
	
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
	
		BOOL bDoDistanceCheck
		INT iArray, iBit
		IE_DROPOFF eDropoffID = INT_TO_ENUM(IE_DROPOFF, data.iDropoffIDStagger)
		IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(eDropoffID, iArray, iBit)
		PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
		FLOAT fDist = VDIST(GET_PLAYER_COORDS(PLAYER_ID()), IE_DELIVERY_GET_DROPOFF_COORDS(eDropoffID))
		
		// If we are the main deliverer we decide if this dropoff is active
		IF pDeliverer = PLAYER_ID()
			IF IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(eDropoffID)
				SET_BIT(data.iActiveDropoffsTemp[iArray], iBit)
				//#IF IS_DEBUG_BUILD
				//	PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Setting iActiveDropoffsTemp ", iArray, " - ", iBit)
				//#ENDIF
				bDoDistanceCheck = TRUE
			ENDIF
		ELIF pDeliverer != INVALID_PLAYER_INDEX()
			IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
				bDoDistanceCheck = TRUE
			ENDIF
		ENDIF
		
		IF bDoDistanceCheck
			// Maintain check for the closest dropoff while we're at it
			IF fDist < data.fClosestActiveDropoffDistance
				data.fClosestActiveDropoffDistance = fDist
				data.eTempClosestActiveDropoffID = eDropoffID
			ENDIF
		ENDIF
		
		// Checks for closest dropoff
		IF fDist < data.fClosestDropoffDistance
			data.fClosestDropoffDistance = fDist
			data.eTempClosestDropoffID = eDropoffID
		ENDIF
	ENDIF
	
	IF data.iDropoffIDStagger = ENUM_TO_INT(IE_DROPOFF_COUNT) - 1
		// Made a full cycle, copy data to BD
		INT i
		#IF IS_DEBUG_BUILD
		BOOL bPrintFullDebug
		#ENDIF
		
		REPEAT COUNT_OF(data.iActiveDropoffsTemp) i
			IF data.iActiveDropoffsTemp[i] != GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iActiveDropoffs[i]
				GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iActiveDropoffs[i] = data.iActiveDropoffsTemp[i]
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Updating iActiveDropoffs in Player BD, index: ", i, " new value: ", data.iActiveDropoffsTemp[i])
					bPrintFullDebug = TRUE
				#ENDIF
			ELSE
				//PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - data.iActiveDropoffsTemp[i]: ", data.iActiveDropoffsTemp[i])
				//PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iActiveDropoffs[i]: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iActiveDropoffs[i])
			ENDIF
			
			data.iActiveDropoffsTemp[i] = 0
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
		IF bPrintFullDebug
			INT iArray, iBit
			REPEAT IE_DROPOFF_COUNT i
				IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(INT_TO_ENUM(IE_DROPOFF, i), iArray, iBit)
				IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Dropoff ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(INT_TO_ENUM(IE_DROPOFF, i)), " is active.")
				ELSE
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Dropoff ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(INT_TO_ENUM(IE_DROPOFF, i)), " is not active.")
				ENDIF
			ENDREPEAT
		ENDIF
		#ENDIF
		
		// Copying closest active dropoff
		IF data.eTempClosestActiveDropoffID != IE_DROPOFF_INVALID
			IF g_IEDeliveryData.eClosestActiveDropoffID != data.eTempClosestActiveDropoffID
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Got new closest active dropoff: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(data.eTempClosestActiveDropoffID))
				#ENDIF
			
				g_IEDeliveryData.eClosestActiveDropoffID = data.eTempClosestActiveDropoffID
			ENDIF
		ELSE
			IF g_IEDeliveryData.eClosestActiveDropoffID != IE_DROPOFF_INVALID
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - No close active dropoffs!")
				#ENDIF
				
				g_IEDeliveryData.eClosestActiveDropoffID = IE_DROPOFF_INVALID
			ENDIF
		ENDIF
		
		data.eTempClosestActiveDropoffID = IE_DROPOFF_INVALID
		data.fClosestActiveDropoffDistance = 9999.9
		
		IF g_IEDeliveryData.eClosestDropoffID != data.eTempClosestDropoffID
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - New closest dropoff: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(data.eTempClosestDropoffID))
			#ENDIF
			
			g_IEDeliveryData.eClosestDropoffID = data.eTempClosestDropoffID
		ENDIF
		
		data.eTempClosestDropoffID = IE_DROPOFF_INVALID
		data.fClosestDropoffDistance = 9999.9
	ENDIF
	
	// This is so spectators can fade out when we're about to do the delivery
	IF g_IEDeliveryData.eClosestActiveDropoffID != IE_DROPOFF_INVALID
		VECTOR vCoord = IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestActiveDropoffID)
		IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vCoord) < 7.0
			IF NOT IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
				SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Setting bit: BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF")
				#ENDIF
			ENDIF
		ELIF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vCoord) > 9.0
			IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
				CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Clearing bit: BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF (1)")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
			CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS - Clearing bit: BS_IE_DELIVERY_GLOBAL_PLAYER_BD_NEARBY_ACTIVE_DROPOFF (2)")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   Interacts with the door system to open a door or gate.
/// PARAMS:
///    iDoorHash - Hash of the door prop.
FUNC BOOL IE_DELIVERY_OPEN_DOOR(INT iDoorHash, FLOAT openRatio = 1.0, BOOL holdOpen = TRUE, BOOL flushState = FALSE)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
		IF NOT NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
			NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_OPEN_DOOR - Dont have control of door, requesting.")
		ELSE
			DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_UNLOCKED, FALSE, flushState)
			DOOR_SYSTEM_SET_HOLD_OPEN(iDoorHash, holdOpen)
			DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, openRatio, FALSE, flushState)
			
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_OPEN_DOOR - Can't open door, as it's not registered with system.")
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Interacts with the door system to close a door or gate.
/// PARAMS:
///    iDoorHash - Hash of the door prop.
FUNC BOOL IE_DELIVERY_CLOSE_DOOR(INT iDoorHash, FLOAT openRatio = 0.0, BOOL flushState = FALSE)

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iDoorHash)
		IF NOT NETWORK_HAS_CONTROL_OF_DOOR(iDoorHash)
			NETWORK_REQUEST_CONTROL_OF_DOOR(iDoorHash)
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_CLOSE_DOOR - Dont have control of door, requesting.")
		ELSE
			DOOR_SYSTEM_SET_HOLD_OPEN(iDoorHash, FALSE)
			DOOR_SYSTEM_SET_OPEN_RATIO(iDoorHash, openRatio, FALSE, flushState)
			DOOR_SYSTEM_SET_DOOR_STATE(iDoorHash, DOORSTATE_LOCKED, FALSE, flushState)
			
			RETURN TRUE
		ENDIF
	ELSE
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_OPEN_DOOR - Can't close door, as it's not registered with system.")
	ENDIF
	
	RETURN FALSE
ENDFUNC	

FUNC BLIP_SPRITE IE_DELIVERY_GET_DROPOFF_BLIP_SPRITE(IE_DROPOFF eDropoffID)
	IF IE_IS_DROPOFF_A_POLICE_STATION(eDropoffID)
		RETURN RADAR_TRACE_POLICE_STATION_DROPOFF
	ELIF IE_IS_DROPOFF_A_SCRAPYARD(eDropoffID)
		RETURN RADAR_TRACE_JUNKYARD
	ENDIF
	
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC FLOAT IE_DELIVERY_GET_DROPOFF_BLIP_SCALE(IE_DROPOFF eDropoffID)
	IF IE_IS_DROPOFF_A_POLICE_STATION(eDropoffID)
	OR IE_IS_DROPOFF_A_SCRAPYARD(eDropoffID)
		RETURN IE_DELIVERY_BLIP_SCALE
	ENDIF
	
	RETURN BLIP_SIZE_NETWORK_COORD
ENDFUNC

PROC IE_DELIVERY_MAINTAIN_BLIP_FOR_DROPOFF_LOCATION(IE_DROPOFF eDropoffID, IE_DELIVERY_FREEMODE_DATA &data)

	INT iArray, iBit
	IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(eDropoffID, iArray, iBit)

	BOOL bDropoffIsActive

	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
		PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
		
		IF pDeliverer != INVALID_PLAYER_INDEX()
			IF pDeliverer = PLAYER_ID()
				bDropoffIsActive = IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(eDropoffID)
			ELSE
				IF GB_ARE_PLAYERS_IN_SAME_GANG(pDeliverer, PLAYER_ID())
					IF IS_BIT_SET(GLobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
						bDropoffIsActive = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bDropoffIsActive
			BOOL bOriginalSeller = AM_I_ORIGINAL_SELLER_OF_EXPORT_VEHICLE(g_IEDeliveryData.vehCurrentlyDelivering)
			
			IF NOT IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(eDropoffID) OR bOriginalSeller // Warehouse dropoff for off mission players will be handled by simple interior blips
				IF NOT DOES_BLIP_EXIST(data.blipDropoffs[eDropoffID])
					VECTOR vBlipCoords
					vBlipCoords = IE_DELIVERY_GET_DROPOFF_COORDS(eDropoffID)
					
					IF NOT IS_VECTOR_ZERO(vBlipCoords)
						data.blipDropoffs[eDropoffID] = CREATE_BLIP_FOR_COORD(vBlipCoords, FALSE)
						
						BLIP_SPRITE eSprite = IE_DELIVERY_GET_DROPOFF_BLIP_SPRITE(eDropoffID)
						IF eSprite != RADAR_TRACE_INVALID
							SET_BLIP_SPRITE(data.blipDropoffs[eDropoffID], eSprite)
						ELSE
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(data.blipDropoffs[eDropoffID], HUD_COLOUR_YELLOW)
						ENDIF
						
						SET_BLIP_SCALE(data.blipDropoffs[eDropoffID], IE_DELIVERY_GET_DROPOFF_BLIP_SCALE(eDropoffID))
						SET_BLIP_PRIORITY(data.blipDropoffs[eDropoffID], BLIPPRIORITY_HIGH)
						SET_BLIP_FLASH_TIMER(data.blipDropoffs[eDropoffID], IE_DELIVERY_BLIP_FLASH_TIME)
						SET_BLIP_COLOUR(data.blipDropoffs[eDropoffID], BLIP_COLOUR_YELLOW)
						SET_BLIP_AS_SHORT_RANGE(data.blipDropoffs[eDropoffID], FALSE)
						
						IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(eDropoffID)
							INT iFoo1, iFoo2
							BLIP_INDEX blip
							MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY(GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(eDropoffID)), data.blipDropoffs[eDropoffID], blip, iFoo1, iFoo2, SIMPLE_INTERIOR_PRIMARY_BLIP)
						ELSE
							SET_BLIP_NAME_FROM_TEXT_FILE(data.blipDropoffs[eDropoffID], IE_DELIVERY_GET_DROPOFF_BLIP_NAME(eDropoffID))
						ENDIF
						
						IF bOriginalSeller
							SET_BLIP_ROUTE(data.blipDropoffs[eDropoffID], TRUE)
						ELSE
							BOOL bSetRoute = TRUE
							
							IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
								IF DOES_PLAYER_OWN_AN_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
									IF _CAN_PLAYER_TAKE_THIS_CAR_TO_THEIR_IE_GARAGE_OFF_MISSION(GET_SIMPLE_INTERIOR_ID_FROM_IMPORT_EXPORT_GARAGE_ID(GET_PLAYERS_OWNED_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS())) #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										bSetRoute = FALSE // Route will be set by warehouse and that one take priority over anything we set here
									ENDIF
								ENDIF
							ENDIF
							
							SET_BLIP_ROUTE(data.blipDropoffs[eDropoffID], bSetRoute)
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY] MAINTAIN_IE_DELIVERY_BLIP_FOR_DROPOFF_LOCATION - Created blip for drop off: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(eDropoffID))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bDropoffIsActive
		IF DOES_BLIP_EXIST(data.blipDropoffs[eDropoffID])
			REMOVE_BLIP(data.blipDropoffs[eDropoffID])
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] MAINTAIN_IE_DELIVERY_BLIP_FOR_DROPOFF_LOCATION - Removed blip for drop off: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(eDropoffID))
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC IE_DELIVERY_FORCE_UPDATE_ALL_BLIPS(IE_DELIVERY_FREEMODE_DATA &data, BOOL bCleanupCurrentBlips = FALSE)
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_FORCE_UPDATE_ALL_BLIPS - Called")
	#ENDIF
	
	INT i
	REPEAT IE_DROPOFF_COUNT i
		IF bCleanupCurrentBlips
			IF DOES_BLIP_EXIST(data.blipDropoffs[i])
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_FORCE_UPDATE_ALL_BLIPS - Removing blip ", i, " because bCleanupCurrentBlips is TRUE.")
				#ENDIF
				SET_BLIP_ROUTE(data.blipDropoffs[i], FALSE)
				REMOVE_BLIP(data.blipDropoffs[i])
			ENDIF
		ENDIF
		IE_DELIVERY_MAINTAIN_BLIP_FOR_DROPOFF_LOCATION(INT_TO_ENUM(IE_DROPOFF, i), data)
	ENDREPEAT
ENDPROC


/*
PROC IE_DELIVERY_MAINTAIN_MOST_LUCRATIVE_BLIP(IE_DROPOFF eDropOff, IE_DELIVERY_FREEMODE_DATA &data)

	IF IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(eDropOff)
		
		IF IE_DELIVERY_IS_DROP_OFF_MOST_LUCRATIVE(eDropOff)
		
			IF DOES_BLIP_EXIST(data.blipDropoffs[eDropOff]) AND NOT DOES_BLIP_HAVE_GPS_ROUTE(data.blipDropoffs[eDropOff])
			
				SET_BLIP_ROUTE(data.blipDropoffs[eDropOff], TRUE)
				
				// Set blip as long range.
				SET_BLIP_AS_SHORT_RANGE(data.blipDropoffs[eDropOff], FALSE)
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_MOST_LUCRATIVE_BLIP - Route set for most lucrative drop off: ", eDropOff)
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
*/

/*
FUNC BOOL IE_DELIVERY_IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_DELIVERED_VEHICLE(VEHICLE_INDEX vehIndex)
	IF IS_ENTITY_DEAD(vehIndex)
		RETURN FALSE
	ENDIF

	PLAYER_INDEX playerIndex
	PED_INDEX pedIndex

	INT iPlayerCount
	
	FLOAT fMinDistance = 15.0
	
	VECTOR vVehCoords = GET_ENTITY_COORDS(vehIndex)
	VECTOR vPedCoords

	REPEAT NUM_NETWORK_PLAYERS iPlayerCount
		playerIndex = INT_TO_PLAYERINDEX(iPlayerCount)
		
		IF IS_NET_PLAYER_OK(playerIndex)
			pedIndex = GET_PLAYER_PED(playerIndex)
			vPedCoords = GET_ENTITY_COORDS(pedIndex)
			
			// Only check players who are within 10 units of the vehicle.
			IF VDIST(vPedCoords, vVehCoords) <= fMinDistance
				// Consider entering the vehicle as being in the vehicle.
				IF IS_PED_IN_VEHICLE(pedIndex, vehIndex, TRUE) 
				AND NOT IS_PED_SITTING_IN_VEHICLE(pedIndex, vehIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][CUTSCENE] IS_ANYONE_TRYING_TO_GET_IN_OR_OUT_OF_DELIVERED_VEHICLE - Player: ", GET_PLAYER_NAME(PLAYER_ID()), " is trying to get in/out of the vehicle.")
					#ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC
*/

PROC IE_DELIVERY_MAINTAIN_DELIVERY_CORONA(IE_DELIVERY_FREEMODE_DATA &data)
	UNUSED_PARAMETER(data)
	
	IF g_IEDeliveryData.eClosestActiveDropoffID = IE_DROPOFF_INVALID
		EXIT
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR g_IEDeliveryData.bDontDrawCoronas
		EXIT
	ENDIF
	
	INT iArray, iBit
	IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(g_IEDeliveryData.eClosestActiveDropoffID, iArray, iBit)

	// Check if this dropoff is active on the deliverer side - this is to sync showing the coronas
	BOOL bDropoffIsActive

	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
		PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
		
		IF pDeliverer != INVALID_PLAYER_INDEX()
			IF pDeliverer = PLAYER_ID()
				bDropoffIsActive = IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(g_IEDeliveryData.eClosestActiveDropoffID)
			ELSE
				IF GB_ARE_PLAYERS_IN_SAME_GANG(pDeliverer, PLAYER_ID())
					IF IS_BIT_SET(GLobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
						bDropoffIsActive = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestActiveDropoffID)) < 200.0
		IF NOT bDropoffIsActive // IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(g_IEDeliveryData.eClosestActiveDropoffID #IF IS_DEBUG_BUILD , FALSE #ENDIF)
			#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 120 = 9
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_DELIVERY_CORONAS - Not drawing, player cant make delivery to this dropoff.")
				ENDIF
			#ENDIF
			EXIT
		ENDIF
		
		VECTOR vCoronaLocation = IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestActiveDropoffID)
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
		FLOAT fCoronaRadius = IE_DELIVERY_GET_DROPOFF_CORONA_RADIUS()
	
		#IF IS_DEBUG_BUILD
		IF NOT IS_VECTOR_ZERO(data.d_vCoronaCoords[ENUM_TO_INT(g_IEDeliveryData.eClosestActiveDropoffID)])
		AND data.d_fCoronaRadius > 0.0
			fCoronaRadius = data.d_fCoronaRadius
			vCoronaLocation = data.d_vCoronaCoords[ENUM_TO_INT(g_IEDeliveryData.eClosestActiveDropoffID)]
		ENDIF
		#ENDIF
		
		//IF g_IEDeliveryData.vehCurrentlyDelivering != NULL AND DOES_ENTITY_EXIST(g_IEDeliveryData.vehCurrentlyDelivering)
		IF NOT IS_ENTITY_AT_COORD(g_IEDeliveryData.vehCurrentlyDelivering, vCoronaLocation, IE_DELIVERY_GET_DROPOFF_LOCATE_SIZE())
		OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			DRAW_MARKER(MARKER_CYLINDER, vCoronaLocation, <<0, 0, 0>>, <<0, 0, 0>>, <<fCoronaRadius, fCoronaRadius, 2.5 >>, iR, iG, iB, 150)
		ENDIF
		//ENDIF
	ENDIF
ENDPROC

FUNC BOOL _STOP_IE_DELIVERY_AND_SHOW_WARNING_IF_PLAYER_HAS_WANTED_LEVEL(IE_DELIVERY_FREEMODE_DATA &data)
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		IF NOT HAS_NET_TIMER_STARTED(data.timerWantedLevelWarning)
			PRINT_HELP("VEH_DPO_WNTED_1")
			START_NET_TIMER(data.timerWantedLevelWarning)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] _STOP_IE_DELIVERY_AND_SHOW_WARNING_IF_PLAYER_HAS_WANTED_LEVEL - Player tries to deliver with wanted level, showing help.")
			#ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(data.timerWantedLevelWarning, 7000)
				RESET_NET_TIMER(data.timerWantedLevelWarning)
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] _STOP_IE_DELIVERY_AND_SHOW_WARNING_IF_PLAYER_HAS_WANTED_LEVEL - Oh god they're still here trying to deliver with wanted level, reset timer.")
				#ENDIF
			ENDIF
		ENDIF
		
		RETURN TRUE
	ELSE
		IF HAS_NET_TIMER_STARTED(data.timerWantedLevelWarning)
			RESET_NET_TIMER(data.timerWantedLevelWarning)
		ENDIF
		
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEH_DPO_WNTED_1")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC _RESET_WANTED_LEVEL_WARNING(IE_DELIVERY_FREEMODE_DATA &data)
	IF HAS_NET_TIMER_STARTED(data.timerWantedLevelWarning)
		RESET_NET_TIMER(data.timerWantedLevelWarning)
	ENDIF
ENDPROC

PROC IE_DELIVERY_MAINTAIN_LAUNCHING_CUTSCENE_SCRIPT_FOR_DROPOFF_LOCATION(IE_DROPOFF eDropoffID, IE_DELIVERY_FREEMODE_DATA &data)
	UNUSED_PARAMETER(data)
	
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
	
		PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
		
		IF pDeliverer = PLAYER_ID()
			IF eDropoffID != IE_DROPOFF_INVALID
			AND IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(eDropoffID #IF IS_DEBUG_BUILD , TRUE #ENDIF)
				VECTOR vStartPoint = IE_DELIVERY_GET_DROPOFF_COORDS(eDropoffID)
				
				IF NOT g_IEDeliveryData.bDeliveryViaCargobob
				AND NOT g_IEDeliveryData.bDeliveryViaTowtruck
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(g_IEDeliveryData.vehCurrentlyDelivering)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(g_IEDeliveryData.vehCurrentlyDelivering)
					ENDIF
				ENDIF
				
				IF VDIST(GET_ENTITY_COORDS(g_IEDeliveryData.vehCurrentlyDelivering), vStartPoint) < IE_DELIVERY_SCRIPT_LAUNCH_DISTANCE
					IF NOT g_IEDeliveryData.bLaunchingScript
						IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT(vStartPoint, eDropoffID)
					ENDIF
				ENDIF
				
				IF g_IEDeliveryData.bDeliveryViaCargobob
					// CARGOBOB
					IF IS_ENTITY_AT_COORD(g_IEDeliveryData.vehCurrentlyDelivering, vStartPoint, IE_DELIVERY_GET_DROPOFF_LOCATE_SIZE_FOR_CARGOBOB())
						IF NOT _STOP_IE_DELIVERY_AND_SHOW_WARNING_IF_PLAYER_HAS_WANTED_LEVEL(data)
							IF NOT g_IEDeliveryData.bStartCutscene
								//BROADCAST_START_DELIVERY_CUTSCENE(eDropoffID)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Setting g_IEDeliveryData.bStartCutscene to TRUE (1)")
								#ENDIF
								g_IEDeliveryData.bStartCutscene = TRUE
								
								/*
								// TODO maybe implement this somehow
								IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
									VEHICLE_INDEX vehCargobob = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									IF NOT IS_ENTITY_DEAD(vehCargobob)
									AND NETWORK_HAS_CONTROL_OF_ENTITY(vehCargobob)
										SET_CARGOBOB_FORCE_DONT_DETACH_VEHICLE(vehCargobob, TRUE)
									ENDIF
								ENDIF
								*/
							ENDIF
						ENDIF
					ELSE
						_RESET_WANTED_LEVEL_WARNING(data)
					ENDIF
				ELSE 
					IF g_IEDeliveryData.bDeliveryViaTowtruck
						// TOWTRUCK
						IF NOT g_IEDeliveryData.bHaltVehicleAndLaunchCutscene
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF IS_ENTITY_AT_COORD(g_IEDeliveryData.vehCurrentlyDelivering, vStartPoint, IE_DELIVERY_GET_DROPOFF_LOCATE_SIZE())
									OR IS_ENTITY_AT_COORD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vStartPoint, IE_DELIVERY_GET_DROPOFF_LOCATE_SIZE())
										IF NOT _STOP_IE_DELIVERY_AND_SHOW_WARNING_IF_PLAYER_HAS_WANTED_LEVEL(data)
											g_IEDeliveryData.bHaltVehicleAndLaunchCutscene = TRUE
										ENDIF
									ELSE
										_RESET_WANTED_LEVEL_WARNING(data)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						// NORMAL
						IF NOT g_IEDeliveryData.bHaltVehicleAndLaunchCutscene
							IF IS_ENTITY_AT_COORD(g_IEDeliveryData.vehCurrentlyDelivering, vStartPoint, IE_DELIVERY_GET_DROPOFF_LOCATE_SIZE())
								IF NOT _STOP_IE_DELIVERY_AND_SHOW_WARNING_IF_PLAYER_HAS_WANTED_LEVEL(data)
									g_IEDeliveryData.bHaltVehicleAndLaunchCutscene = TRUE
								ENDIF
							ELSE
								_RESET_WANTED_LEVEL_WARNING(data)
							ENDIF
						ENDIF
					ENDIF
					
					IF g_IEDeliveryData.bHaltVehicleAndLaunchCutscene
						VEHICLE_INDEX vehToHalt
						
						IF g_IEDeliveryData.bDeliveryViaTowtruck
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehToHalt = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								ENDIF
							ENDIF
						ELSE
							vehToHalt = g_IEDeliveryData.vehCurrentlyDelivering
						ENDIF
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehToHalt)
							//SET_ENTITY_PROOFS(vehToHalt, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
							
							FLOAT fHaltDist = 3.5
							FLOAT fHaltThreshold = 1.0
							
							IF IE_DROPOFF_HAS_INCREASED_DELIVERY_URGENCY(eDropoffID)
								fHaltDist = 3.0
								fHaltThreshold = 3.0
							ENDIF
							
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehToHalt, fHaltDist, DEFAULT, fHaltThreshold)
								IF NOT g_IEDeliveryData.bStartCutscene
									
									#IF IS_DEBUG_BUILD
										PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Setting g_IEDeliveryData.bStartCutscene to TRUE (2)")
									#ENDIF
									g_IEDeliveryData.bStartCutscene = TRUE
									
									//BROADCAST_START_DELIVERY_CUTSCENE(eDropoffID) // This will launch cutscene for everyone who is running the script
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF pDeliverer != INVALID_PLAYER_INDEX()
			// Listen for player deliverer starting delivery script
			IF NOT g_IEDeliveryData.bLaunchingScript
			AND NOT IE_DELIVERY_IS_SCRIPT_RUNNING()
				IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_RUNNING_DELIVERY_SCRIPT)
				AND GlobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.eRunningDeliveryScriptForDropoffID != IE_DROPOFF_INVALID
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Player deliverer started script, we're gonna start it too!")
					#ENDIF
				
					VECTOR vStartPoint = IE_DELIVERY_GET_DROPOFF_COORDS(GlobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.eRunningDeliveryScriptForDropoffID)
					IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT(vStartPoint, GlobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.eRunningDeliveryScriptForDropoffID)
				ENDIF
			ELSE
				/*
				IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(pDeliverer)].IEDeliveryBD.iBS, BS_IE_DELIVERY_GLOBAL_PLAYER_BD_PLAYING_DELIVERY_CUTSCENE)
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Setting g_IEDeliveryData.bStartCutscene to TRUE (3)")
					#ENDIF
					g_IEDeliveryData.bStartCutscene = TRUE
				ENDIF
				*/
			ENDIF
		ENDIF
		
		IF g_IEDeliveryData.bLaunchingScript
		AND NOT IE_DELIVERY_IS_SCRIPT_RUNNING()
			IF HAS_SCRIPT_LOADED("GB_IE_DELIVERY_CUTSCENE")
			
				IE_DELIVERY_SCRIPT_LAUNCH_DATA launchData
				launchData.iInstance = IE_DELIVERY_GET_INSTANCE_OF_CUTSCENE_SCRIPT_TO_LAUNCH()
				
				IF launchData.iInstance > -1
					launchData.vLaunchPoint = g_IEDeliveryData.vLaunchPoint
					launchData.eDropoffID = g_IEDeliveryData.eDropoffIDForLaunch
					launchData.pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
					
					IF NOT NETWORK_IS_SCRIPT_ACTIVE("GB_IE_DELIVERY_CUTSCENE", launchData.iInstance, TRUE)
						START_NEW_SCRIPT_WITH_ARGS("GB_IE_DELIVERY_CUTSCENE", launchData, SIZE_OF(launchData), IE_DELIVERY_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("GB_IE_DELIVERY_CUTSCENE")
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Started GB_IE_DELIVERY_CUTSCENE.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Trying to start GB_IE_DELIVERY_CUTSCENE even though it's already running.")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - Trying to start GB_IE_DELIVERY_CUTSCENE but the instance to launch is invalid. Probably something wrong with the driver.")
					#ENDIF
				ENDIF
				
			ENDIF
		ELSE
			IF g_IEDeliveryData.bLaunchingScript
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - bLaunchingScript is TRUE but script is not running (yet).")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF g_IEDeliveryData.bHaltVehicleAndLaunchCutscene
			g_IEDeliveryData.bHaltVehicleAndLaunchCutscene = FALSE
		ENDIF
		/*
		IF g_IEDeliveryData.bStartCutscene
			g_IEDeliveryData.bStartCutscene = FALSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_LAUNCH_DELIVERY_SCRIPT - bStartCutscene is TRUE but we're not doing any deliveries, clearing.")
			#ENDIF
		ENDIF
		*/
	ENDIF
ENDPROC

PROC MAINTAIN_MOC_TRAFFIC_REDUCTION_ZONES()	

	IF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CORONA()
	
		VECTOR vBlockCoords 
		GET_ARMORY_TRUCK_ENTRY_LOCATE(SIMPLE_INTERIOR_ARMORY_TRUCK_1, vBlockCoords)

		IF NOT IS_BIT_SET(g_IETrafficData.iBS, IE_BS_MOC_TRAFFIC_REDUCTION_ACTIVE)
		AND (g_IETrafficData.iTrafficZoneIndexMOC = -1 OR NOT DOES_TRAFFIC_REDUCTION_ZONE_EXIST(g_IETrafficData.iTrafficZoneIndexMOC))
			
			// Locally assign a traffic reduction zone.
			ADD_TRAFFIC_REDUCTION_ZONE(g_IETrafficData.iTrafficZoneIndexMOC, vBlockCoords)
			CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_MOC_TRAFFIC_REDUCTION_ZONES - Adding traffic reduction zone vBlockCoords = ", vBlockCoords, " index: ", g_IETrafficData.iTrafficZoneIndexMOC)
			
			SET_BIT(g_IETrafficData.iBS, IE_BS_MOC_TRAFFIC_REDUCTION_ACTIVE)
		ENDIF
		
	ELIF g_IETrafficData.iTrafficZoneIndexMOC != -1

		IF IS_BIT_SET(g_IETrafficData.iBS, IE_BS_MOC_TRAFFIC_REDUCTION_ACTIVE)
		AND DOES_TRAFFIC_REDUCTION_ZONE_EXIST(g_IETrafficData.iTrafficZoneIndexMOC)

			REMOVE_TRAFFIC_REDUCTION_ZONE(g_IETrafficData.iTrafficZoneIndexMOC)
			CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_MOC_TRAFFIC_REDUCTION_ZONES  - Removing traffic reduction zone from zone ", g_IETrafficData.iTrafficZoneIndexMOC)
				
			CLEAR_BIT(g_IETrafficData.iBS, IE_BS_MOC_TRAFFIC_REDUCTION_ACTIVE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stagger each drop off and check distance between local player and drop off. 
///    If player is within drop offs immediate area then reduce traffic around this 
///    drop off.
PROC MAINTAIN_TRAFFIC_REDUCTION_ZONES(IE_DELIVERY_FREEMODE_DATA &data)

	IF NETWORK_IS_PARTICIPANT_ACTIVE(NETWORK_GET_PARTICIPANT_INDEX(PLAYER_ID()))
		IF GB_IS_PLAYER_IN_EXPORT_VEHICLE(PLAYER_ID())
			VECTOR vDropoff = IE_DELIVERY_GET_DROPOFF_COORDS(INT_TO_ENUM(IE_DROPOFF, data.iDropoffIDStagger))
			VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			FLOAT fTempDist = VDIST(vDropoff, vPlayerCoords)
			
			IF fTempDist <= g_IETrafficData.fDistance

				IF NOT IS_BIT_SET(g_IETrafficData.iBS, IE_BS_TRAFFIC_REDUCTION_ACTIVE)
				AND NOT DOES_TRAFFIC_REDUCTION_ZONE_EXIST(g_IETrafficData.iTrafficZoneIndex)
					g_IETrafficData.iReducedDropoffZone = data.iDropoffIDStagger
					
					// Locally assign a traffic reduction zone.
					ADD_TRAFFIC_REDUCTION_ZONE(g_IETrafficData.iTrafficZoneIndex, vDropoff)
					CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_TRAFFIC_REDUCTION_ZONES - Adding traffic reduction zone to drop off ", data.iDropoffIDStagger)
					
					SET_BIT(g_IETrafficData.iBS, IE_BS_TRAFFIC_REDUCTION_ACTIVE)
				ELSE
					CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_TRAFFIC_REDUCTION_ZONES - Traffic reduction zone already exists for drop off ", data.iDropoffIDStagger)
				ENDIF
			ENDIF		
		ENDIF
	ENDIF

	IF g_IETrafficData.iReducedDropoffZone = data.iDropoffIDStagger
	AND IS_BIT_SET(g_IETrafficData.iBS, IE_BS_TRAFFIC_REDUCTION_ACTIVE)
		VECTOR vDropoff = IE_DELIVERY_GET_DROPOFF_COORDS(INT_TO_ENUM(IE_DROPOFF, g_IETrafficData.iReducedDropoffZone))
		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		FLOAT fTempDist = VDIST(vDropoff, vPlayerCoords)
	
		IF fTempDist > g_IETrafficData.fDistance
			REMOVE_TRAFFIC_REDUCTION_ZONE(g_IETrafficData.iTrafficZoneIndex)
			CPRINTLN(DEBUG_AMBIENT, "MAINTAIN_TRAFFIC_REDUCTION_ZONES - Removing traffic reduction zone from drop off ", data.iDropoffIDStagger)
			
			CLEAR_BIT(g_IETrafficData.iBS, IE_BS_TRAFFIC_REDUCTION_ACTIVE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IE_DELIVERY_IS_OFF_MISSION_HELP_BEING_DISPLAYED()
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_5")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_6")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_7")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_OFF_HELP_8")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IE_DELIVERY_IS_DROPOFF_HELP_BEING_DISPLAYED()
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_DPO_HELP_1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_DPO_HELP_2")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_DPO_HELP_3")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("VEX_DPO_HELP_4")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// This is handled by flow but leaving it here for now
PROC IE_DELIVERY_MAINTAIN_OFF_MISSION_HELP(IE_DELIVERY_FREEMODE_DATA &data)
	BOOL bCleanHelp

	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		bCleanHelp = TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_GOON_LAST_FRAME)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			SET_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_GOON_LAST_FRAME)
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_OFF_MISSION_HELP - Clearing BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME because we are now member of a gang.")
			#ENDIF
			bCleanHelp = TRUE
		ENDIF
	ELSE
		IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_GOON_LAST_FRAME)
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_OFF_MISSION_HELP - Clearing BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME because we are now longer member of a gang.")
			#ENDIF
			bCleanHelp = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_BOSS_LAST_FRAME)
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			SET_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_BOSS_LAST_FRAME)
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_OFF_MISSION_HELP - Clearing BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME because we are now boss.")
			#ENDIF
			bCleanHelp = TRUE
		ENDIF
	ELSE
		IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_BOSS_LAST_FRAME)
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_OFF_MISSION_HELP - Clearing BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME becasue we are now longer boss.")
			#ENDIF
			bCleanHelp = TRUE
		ENDIF
	ENDIF
	
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
	AND NOT bCleanHelp
		IF HAS_NET_TIMER_STARTED(data.timerShowOffMissionHelp)
			IF HAS_NET_TIMER_EXPIRED(data.timerShowOffMissionHelp, 1500)
				
				TEXT_LABEL_15 txtHelp
				
				// We got into a vehicle off mission, show some help as to what we can do with it
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
					PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
					
					IF pDeliverer != INVALID_PLAYER_INDEX()
					AND GB_ARE_PLAYERS_IN_SAME_GANG(pDeliverer, PLAYER_ID())
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
							txtHelp = "VEX_OFF_HELP_6" // take it to scrapyard
						ELSE
							IF DOES_PLAYER_OWN_AN_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS())
								BOOL bCanTakeToWarehouse = NOT IS_BIT_SET(GB_GET_EXPORT_VEHICLE_BITSET(g_IEDeliveryData.vehCurrentlyDelivering), CONTRABAND_DECORATOR_BS_PREVENT_DELIVERY_TO_RIVAL_WAREHOUSE) AND CAN_EARN_FROM_STEALING_VEHICLE_CARGO()
								BOOL bBossOwnsIt = NOT IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(GET_IE_DROPOFF_FROM_IMPORT_EXPORT_WAREHOUSE(GET_PLAYERS_OWNED_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS())))
								
								IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
									IF NOT bCanTakeToWarehouse
										txtHelp = "VEX_OFF_HELP_8" // too hot to deliver to your warehouse
									ELSE
										IF bBossOwnsIt
											txtHelp = "VEX_OFF_HELP_5" // You already own it, take it to police station
										ELSE
											txtHelp = "VEX_OFF_HELP_3" // Take it to your warehouse or police station
										ENDIF
									ENDIF
								ELSE
									IF NOT bCanTakeToWarehouse
										txtHelp = "VEX_OFF_HELP_7" // too hot to deliver to your Boss warehouse
									ELSE
										IF bBossOwnsIt
											txtHelp = "VEX_OFF_HELP_4" // Your Boss already owns it, take it to police station
										ELSE
											txtHelp = "VEX_OFF_HELP_2" // Tae it to your boss' warehouse or police station
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering) = PLAYER_ID()
						// Not in gang = solo player so only show help when we're driving
						txtHelp = "VEX_OFF_HELP_1" // Take it to police station
					ENDIF
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(txtHelp)
					PRINT_HELP(txtHelp, IE_DELIVERY_OFF_MISSION_HELP_TIME)
					SET_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_OFF_MISSION_HELP_SHOWN)
					RESET_NET_TIMER(data.timerShowOffMissionHelp)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IE_DELIVERY_IS_OFF_MISSION_HELP_BEING_DISPLAYED()
			CLEAR_HELP(TRUE)
			RESET_NET_TIMER(data.timerShowOffMissionHelp)
		ENDIF
		
		IF IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_OFF_MISSION_HELP_SHOWN)
			CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_OFF_MISSION_HELP_SHOWN)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL _SHOULD_SHOW_DROPOFF_HELP()
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF

	IF IE_DELIVERY_ARE_DROPOFFS_DISABLED_BY_BOSS()
	OR IE_DELIVERY_ARE_DROPOFFS_DISABLED_LOCALLY()
		RETURN FALSE
	ENDIF
	
	IF IE_IS_DROPOFF_A_PLAYER_OWNED_WAREHOUSE(g_IEDeliveryData.eClosestDropoffID)
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG() AND NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
			IF DOES_PLAYER_OWN_IE_GARAGE(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(g_IEDeliveryData.eClosestDropoffID))
				IF NOT IE_DELIVERY_CAN_LOCAL_PLAYER_USE_THIS_DROPOFF_IN_CURRENT_VEHICLE(g_IEDeliveryData.eClosestDropoffID)
					IF IS_ENTITY_AT_COORD(g_IEDeliveryData.vehCurrentlyDelivering, IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestDropoffID), <<4.5, 3.0, 4.5>>)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ELIF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			IF DOES_PLAYER_OWN_IE_GARAGE(PLAYER_ID(), GET_IMPORT_EXPORT_WAREHOUSE_FROM_IE_DROPOFF(g_IEDeliveryData.eClosestDropoffID))
				IF IS_ENTITY_AT_COORD(g_IEDeliveryData.vehCurrentlyDelivering, IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestDropoffID), <<4.5, 3.0, 4.5>>)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Check if player driven in delivery zone of their boss warehouse.
///    If they cant deliver show help with a little delay.
///    After some time have passed and they still are hanging around the delivery zone show the help again.
///    Reset everythign if they move far enough.
/// PARAMS:
///    data - 
PROC IE_DELIVERY_MAINTAIN_HELP(IE_DELIVERY_FREEMODE_DATA &data)
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
	
		IF IE_DELIVERY_IS_OFF_MISSION_HELP_BEING_DISPLAYED()
		OR NOT IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_OFF_MISSION_HELP_SHOWN)
			// Wait for off mission help to finish displaying
			IF HAS_NET_TIMER_STARTED(data.timerShowHelp)
			OR HAS_NET_TIMER_STARTED(data.timerHelpDelay)
				RESET_NET_TIMER(data.timerShowHelp)
				RESET_NET_TIMER(data.timerHelpDelay)
			ENDIF
			EXIT
		ENDIF
	
		IF NOT AM_I_ORIGINAL_SELLER_OF_EXPORT_VEHICLE(g_IEDeliveryData.vehCurrentlyDelivering)
			IF NOT HAS_NET_TIMER_STARTED(data.timerHelpDelay)
				IF _SHOULD_SHOW_DROPOFF_HELP()
					START_NET_TIMER(data.timerHelpDelay)
				ENDIF
			ELSE
				IF NOT HAS_NET_TIMER_STARTED(data.timerShowHelp)
					IF HAS_NET_TIMER_EXPIRED(data.timerHelpDelay, 500)
						IF NOT GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
							PRINT_HELP("VEX_DPO_HELP_4", 5000) // become vip to store
						ELSE
							IF NOT IS_BIT_SET(GB_GET_EXPORT_VEHICLE_BITSET(g_IEDeliveryData.vehCurrentlyDelivering), CONTRABAND_DECORATOR_BS_PREVENT_DELIVERY_TO_RIVAL_WAREHOUSE)
							AND CAN_EARN_FROM_STEALING_VEHICLE_CARGO()
								IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
									PRINT_HELP("VEX_DPO_HELP_3", 5000) // sorry boss already stored in your warehouse
								ELSE
									PRINT_HELP("VEX_DPO_HELP_2", 5000) // sorry already stored in your boss' warehouse
								ENDIF
							ELSE
								PRINT_HELP("VEX_DPO_HELP_1", 5000) // cant take this to the warehouse
							ENDIF
						ENDIF
						START_NET_TIMER(data.timerShowHelp)
					ENDIF
				ELSE
					// Help has been shown, wait some time before showing it again or go back to initial state if players move far enough.
					IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), IE_DELIVERY_GET_DROPOFF_COORDS(g_IEDeliveryData.eClosestDropoffID)) > 30.0
						RESET_NET_TIMER(data.timerShowHelp)
						RESET_NET_TIMER(data.timerHelpDelay)
					ELIF HAS_NET_TIMER_EXPIRED(data.timerShowHelp, 15000)
						IF _SHOULD_SHOW_DROPOFF_HELP()
							RESET_NET_TIMER(data.timerShowHelp)
						ELSE
							RESET_NET_TIMER(data.timerShowHelp)
							RESET_NET_TIMER(data.timerHelpDelay)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IE_DELIVERY_IS_DROPOFF_HELP_BEING_DISPLAYED()
			CLEAR_HELP(TRUE)
			
			IF HAS_NET_TIMER_STARTED(data.timerShowHelp)
			OR HAS_NET_TIMER_STARTED(data.timerHelpDelay)
				RESET_NET_TIMER(data.timerShowHelp)
				RESET_NET_TIMER(data.timerHelpDelay)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VEHICLE_INDEX GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF(IE_DROPOFF eDropoffID)

	VECTOR vCoords = IE_DELIVERY_GET_DROPOFF_COORDS(eDropoffID)
	FLOAT fRadius = 4.0
	
	RETURN GET_CLOSEST_VEHICLE(vCoords, fRadius, DUMMY_MODEL_FOR_SCRIPT,
								VEHICLE_SEARCH_FLAG_RETURN_LAW_ENFORCER_VEHICLES 
								| VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES 
								| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_GROUP_MEMBERS 
								| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_PLAYER                                                           
								| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED                                   
								| VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_WITH_PEDS_ENTERING_OR_EXITING                                         
								| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK
								| VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL    
								| VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_NON_DEFAULT_TASK)
ENDFUNC

PROC MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES(IE_DELIVERY_FREEMODE_DATA &data)
	
	IF NOT HAS_NET_TIMER_STARTED(g_IETrafficData.blockedDropoffTimer)
		START_NET_TIMER(g_IETrafficData.blockedDropoffTimer)
		CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - Starting net timer.")
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(g_IETrafficData.blockedDropoffTimer, IE_BLOCKED_DROPOFF_TIME_TO_CHECK)
	
		INT iArray, iBit
		
		IE_DROPOFF eDropoffID = INT_TO_ENUM(IE_DROPOFF, data.iDropoffIDStagger)
		IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(eDropoffID, iArray, iBit)
		
		// Stagger through each drop off, checking if each is active locally.
		IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.iActiveDropoffs[iArray], iBit)
			CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - Drop off ", data.iDropoffIDStagger, " is active.")
			
			VEHICLE_INDEX vehIndex = GET_VEHICLE_BLOCKING_VEHICLE_DROP_OFF(eDropoffID)
			
			IF DOES_ENTITY_EXIST(vehIndex)
				IF NOT IS_VEHICLE_DRIVEABLE(vehIndex)
					CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - Initial tPrevTimeStamp[", data.iDropoffIDStagger, "] = ", NATIVE_TO_INT(g_IETrafficData.tPrevTimeStamp[data.iDropoffIDStagger]))
					
					IF g_IETrafficData.tPrevTimeStamp[data.iDropoffIDStagger] = INT_TO_NATIVE(TIME_DATATYPE, 0)
						// Capture initial time vehicle was checked in drop off zone.
						g_IETrafficData.tPrevTimeStamp[data.iDropoffIDStagger] = GET_NETWORK_TIME()
						CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - Captured tPrevTimeStamp[", data.iDropoffIDStagger, "] = ", NATIVE_TO_INT(g_IETrafficData.tPrevTimeStamp[data.iDropoffIDStagger]))
					ENDIF

					g_IETrafficData.tCurrentTimeStamp = GET_NETWORK_TIME()
					CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - tCurrentTimeStamp[", data.iDropoffIDStagger, "] = ", NATIVE_TO_INT(g_IETrafficData.tCurrentTimeStamp))
					
					IF ABSI(GET_TIME_DIFFERENCE(g_IETrafficData.tPrevTimeStamp[data.iDropoffIDStagger], g_IETrafficData.tCurrentTimeStamp)) > IE_BLOCKED_DROPOFF_TIME_LIMIT
					
						CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - Clearing area.")
						CLEAR_AREA_LEAVE_VEHICLE_HEALTH(IE_DELIVERY_GET_DROPOFF_COORDS(eDropoffID), 8.0, FALSE, FALSE, FALSE, TRUE)
						
						// Clear time stamps.
						g_IETrafficData.tPrevTimeStamp[data.iDropoffIDStagger] = INT_TO_NATIVE(TIME_DATATYPE, 0)
						g_IETrafficData.tCurrentTimeStamp = INT_TO_NATIVE(TIME_DATATYPE, 0)
						
						IF NOT IS_BIT_SET(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
							SET_BIT(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
						ENDIF
					ENDIF
				ELSE					
					IF NOT IS_BIT_SET(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
						SET_BIT(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
					SET_BIT(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
		CDEBUG1LN(DEBUG_MISSION, "MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES - Resetting time to check active drop offs.")
		RESET_NET_TIMER(g_IETrafficData.blockedDropoffTimer)
	
		CLEAR_BIT(g_IETrafficData.iBS, IE_BS_RESET_DROPOFF_AREA_TIMER)
	ENDIF
ENDPROC

FUNC STRING GET_OFFICE_PA_REPAIR_MESSAGE(BOOL bFree = FALSE)
	STRING sMessage
	BOOL bMale = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_MALE_PA)
	
	IF bFree
		IF bMale
			sMessage = "VEH_WHS_RPF_M"
		ELSE
			sMessage = "VEH_WHS_RPF_F"
		ENDIF
	ELSE
		INT i = GET_RANDOM_INT_IN_RANGE(0, 3)
		
		SWITCH i
			CASE 0
				IF bMale
					sMessage = "VEH_WHS_RP_M0"
				ELSE
					sMessage = "VEH_WHS_RP_F0"
				ENDIF
			BREAK
			
			CASE 1
				IF bMale
					sMessage = "VEH_WHS_RP_M1"
				ELSE
					sMessage = "VEH_WHS_RP_F1"
				ENDIF
			BREAK
			
			CASE 2
				IF bMale
					sMessage = "VEH_WHS_RP_M2"
				ELSE
					sMessage = "VEH_WHS_RP_F2"
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sMessage
ENDFUNC

PROC IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS()
	INT iRepairCost = g_IEDeliveryData.iRepairCostForBoss
	
	IF iRepairCost > 0
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - We have a cost in queue, amount: ", iRepairCost)
		#ENDIF
		
		IF NOT NETWORK_IS_IN_MP_CUTSCENE()
		AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			// Once we can show the ticker give it a second so it doesn't happen right after screen fade etc.
			IF HAS_NET_TIMER_EXPIRED(g_IEDeliveryData.timerRepairTicker, 1000)
			AND NOT HAS_NET_TIMER_STARTED(g_IEDeliveryData.timerRepairCostText)
				IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) >= iRepairCost
				OR NETWORK_CAN_SPEND_MONEY(iRepairCost, FALSE, TRUE, FALSE) //Use OR to catch cash values over SCRIPT_MAX_INT32.
				
					IF g_IEDeliveryData.eModelVehRepaired != DUMMY_MODEL_FOR_SCRIPT
						// Show help
						IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3_DONE_REPAIR_COST_INCURRED_HELP)
							TEXT_LABEL_63 tempModel = GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_IEDeliveryData.eModelVehRepaired))
							PRINT_HELP_WITH_LITERAL_STRING("VEH_IE_RP_HELP", tempModel, HUD_COLOUR_PURE_WHITE, 10000)
							
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Help shown, setting BI_FM_GANG_BOSS_HELP_3_DONE_REPAIR_COST_INCURRED_HELP")
							#ENDIF
							
							SET_BIT(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3_DONE_REPAIR_COST_INCURRED_HELP)
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Not showing help because BI_FM_GANG_BOSS_HELP_3_DONE_REPAIR_COST_INCURRED_HELP is set.")
							#ENDIF
						ENDIF
						g_IEDeliveryData.eModelVehRepaired = DUMMY_MODEL_FOR_SCRIPT
					ENDIF
				
					IF g_IEDeliveryData.pWhoDidDeliveryForRepair = PLAYER_ID()
						PRINT_TICKER_WITH_INT("VEH_WHS_RP_A", iRepairCost)
						
						// We're done, ticker is enough.
						g_IEDeliveryData.iRepairCostForBoss = 0
						RESET_NET_TIMER(g_IEDeliveryData.timerRepairCostText)
						RESET_NET_TIMER(g_IEDeliveryData.timerRepairTicker)
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("VEH_WHS_RP_B", g_IEDeliveryData.pWhoDidDeliveryForRepair, iRepairCost)
						
						REINIT_NET_TIMER(g_IEDeliveryData.timerRepairCostText)
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Will send a text about someone crashing the car.")
						#ENDIF
					ENDIF
					
					IF USE_SERVER_TRANSACTIONS()
						INT iTransactionID
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_IMPORT_EXPORT_REPAIR, iRepairCost, iTransactionID, FALSE, TRUE)
					ELSE
						NETWORK_SPENT_IMPORT_EXPORT_REPAIR(iRepairCost, FALSE, TRUE)
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Cost deducted.")
					#ENDIF
				ELSE
					// Coulnd't afford the repair, send a text saying it's ok, it's on the house.
					IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_OFFICE_PA_CHAR(TRUE), GET_OFFICE_PA_REPAIR_MESSAGE(TRUE), TXTMSG_LOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED) 
						#IF IS_DEBUG_BUILD
							PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Text sent")
						#ENDIF
					ENDIF
					
					g_IEDeliveryData.iRepairCostForBoss = 0
					RESET_NET_TIMER(g_IEDeliveryData.timerRepairCostText)
					RESET_NET_TIMER(g_IEDeliveryData.timerRepairTicker)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Couldn't afford the repair, resetting data.")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Waiting for timersRepairTicker.")
				#ENDIF
			ENDIF
		ELSE
			REINIT_NET_TIMER(g_IEDeliveryData.timerRepairTicker)
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Gotta wait for when it's ok to charge the player.")
			#ENDIF
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_IEDeliveryData.timerRepairCostText)
	AND HAS_NET_TIMER_EXPIRED(g_IEDeliveryData.timerRepairCostText, 2000)
		TEXT_LABEL_15 tl15Temp
		tl15Temp += g_IEDeliveryData.iRepairCostForBoss
		
		IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(
				GET_OFFICE_PA_CHAR(TRUE), 
				GET_OFFICE_PA_REPAIR_MESSAGE(), 
				TXTMSG_UNLOCKED, 
				tl15Temp, 
				-1,
				"",
				STRING_COMPONENT, 
				TXTMSG_NOT_CRITICAL, 
				TXTMSG_AUTO_UNLOCK_AFTER_READ, 
				NO_REPLY_REQUIRED, 
				DEFAULT, 
				DEFAULT, 
				DEFAULT, 
				DEFAULT
				
				
		)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][REPAIR_COST] IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS - Text about someone doing the delivery sent, with the cost: ", g_IEDeliveryData.iRepairCostForBoss)
			#ENDIF
		ENDIF
		
		g_IEDeliveryData.iRepairCostForBoss = 0
		RESET_NET_TIMER(g_IEDeliveryData.timerRepairCostText)
		RESET_NET_TIMER(g_IEDeliveryData.timerRepairTicker)
	ENDIF
ENDPROC

FUNC QUEUED_VEHICLE_EXPORT_REWARD IE_DELIVERY_REWARDS_QUEUE_POP()
	QUEUED_VEHICLE_EXPORT_REWARD poppedReward = g_IEDeliveryData.rewardDataQueue[0]
	
	IF g_IEDeliveryData.iRewardDataQueueLength > 0
		g_IEDeliveryData.iRewardDataQueueLength -= 1
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[IE_DELIVERY] IE_DELIVERY_REQARDS_QUEUE_POP - No rewards to pop!")
		#ENDIF
	ENDIF
	
	IF g_IEDeliveryData.iRewardDataQueueLength > 0
		INT i
		REPEAT g_IEDeliveryData.iRewardDataQueueLength i
			g_IEDeliveryData.rewardDataQueue[i] = g_IEDeliveryData.rewardDataQueue[i+1]
		ENDREPEAT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[IE_DELIVERY] IE_DELIVERY_REQARDS_QUEUE_POP - Popped reward of value ", poppedReward.iCashEarned)
	#ENDIF
	
	RETURN poppedReward
ENDFUNC

/// PURPOSE:
///    Monitors the queue and if something appears on it consumes it.
///    
PROC IE_DELIVERY_MAINTAIN_REWARD_QUEUE(IE_DELIVERY_FREEMODE_DATA &data)
	IF g_IEDeliveryData.iRewardDataQueueLength = 0
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_IN_MP_CUTSCENE()
	AND IS_SCREEN_FADED_IN()
		IF NOT HAS_NET_TIMER_STARTED(data.timerBetweenRewards)
		OR HAS_NET_TIMER_EXPIRED(data.timerBetweenRewards, 5000)
			QUEUED_VEHICLE_EXPORT_REWARD sReward = IE_DELIVERY_REWARDS_QUEUE_POP()
			
			// Award the RP if we have a greater value than 0
			IF sReward.iRPEarned > 0
				NEXT_RP_ADDITION_SHOW_RANKBAR()
				GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "", XPTYPE_SKILL, XPCATEGORY_COLLECT_CHECKPOINT, sReward.iRPEarned)
			ENDIF
			
			// Award the cash if we have a greater value than 0
			IF sReward.iCashEarned > 0
				INT iBossId1, iBossId2
				PLAYER_INDEX playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF playerBoss != INVALID_PLAYER_INDEX()
					GB_GET_GANG_BOSS_UUID_INTS(playerBoss, iBossId1, iBossId2)
				ENDIF
				
				BOOL bNotInAGang = !GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				
				IF USE_SERVER_TRANSACTIONS()
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_REWARD_QUEUE - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_FROM_VEHICLE_EXPORT, $", sReward.iCashEarned, ", iScriptTransactionIndex, FALSE, TRUE)")
					INT iScriptTransactionIndex
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_FROM_VEHICLE_EXPORT, sReward.iCashEarned, iScriptTransactionIndex, FALSE, TRUE)
					g_cashTransactionData[iScriptTransactionIndex].cashInfo.iGangUUIDA = iBossId1
					g_cashTransactionData[iScriptTransactionIndex].cashInfo.iGangUUIDB = iBossId2
					g_cashTransactionData[iScriptTransactionIndex].cashInfo.bNotInAGang = bNotInAGang
				ELSE
					PRINTLN("[IE_DELIVERY] IE_DELIVERY_MAINTAIN_REWARD_QUEUE - NETWORK_EARN_FROM_VEHICLE_EXPORT = $", sReward.iCashEarned)
					NETWORK_EARN_FROM_VEHICLE_EXPORT(sReward.iCashEarned, iBossId1, iBossId2)  
				ENDIF
				
				SET_LAST_JOB_DATA(LAST_JOB_NONE, sReward.iCashEarned)
			ENDIF
			
			IF g_IEDeliveryData.iRewardDataQueueLength > 0
				REINIT_NET_TIMER(data.timerBetweenRewards)
			ELSE
				RESET_NET_TIMER(data.timerBetweenRewards)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_DROPOFF_HIDDEN_MODEL_DETAILS(IE_DROPOFF eDropoff, INT iIndex, VECTOR &vPosition, FLOAT &fRadius, MODEL_NAMES &eModel)
	SWITCH eDropoff
		CASE IE_DROPOFF_GANGSTER_1
			SWITCH iIndex
				CASE 0
					vPosition = <<-2557.7136, 1913.2067, 167.8837>>
					fRadius = 4.0
					eModel = PROP_LRGGATE_01C_L 
					RETURN TRUE
				BREAK
				CASE 1
					vPosition = <<-2557.7136, 1913.2067, 167.8837>>
					fRadius = 4.0
					eModel = PROP_LRGGATE_01C_R
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IE_DROPOFF_GANGSTER_5	
			SWITCH iIndex 
				CASE 0
					vPosition = <<-1800.2578, 473.1927, 132.6913>>
					fRadius = 4.0
					eModel = PROP_LRGGATE_01C_L 
					RETURN TRUE
				BREAK
				CASE 1
					vPosition = <<-1800.2578, 473.1927, 132.6913>>
					fRadius = 4.0
					eModel = PROP_LRGGATE_01C_R
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE IE_DROPOFF_GANGSTER_6
			SWITCH iIndex
				CASE 0
					vPosition = <<205.8799, 773.8717, 204.5877>>
					fRadius = 4.0
					eModel = PROP_LRGGATE_01C_L 
					RETURN TRUE
				BREAK
				CASE 1
					vPosition = <<205.8799, 773.8717, 204.5877>>
					fRadius = 4.0
					eModel = PROP_LRGGATE_01C_R
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC IE_DELIVERY_MAINTAIN_MODEL_VISIBILITY_NEAR_DROPOFFS(IE_DELIVERY_FREEMODE_DATA &data)
	VECTOR vCoords
	FLOAT fRadius
	MODEL_NAMES eModel
	
	IE_DROPOFF eDropoff = INT_TO_ENUM(IE_DROPOFF, data.iDropoffIDStagger)
	
	IF NOT GET_DROPOFF_HIDDEN_MODEL_DETAILS(eDropoff, 0, vCoords, fRadius, eModel)
		EXIT
	ENDIF
	
	INT iArray, iBit, iIndex
	
	vCoords = IE_DELIVERY_GET_DROPOFF_COORDS(INT_TO_ENUM(IE_DROPOFF, eDropoff))
	IE_DROPOFF_GET_BITSET_ARRAY_AND_INDEX(eDropoff, iArray, iBit)
		
	IF NOT IS_BIT_SET(g_IEDeliveryData.iDropoffModelHideSetUpBS[iArray], iBit)
		IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vCoords) < 200
			WHILE(GET_DROPOFF_HIDDEN_MODEL_DETAILS(eDropoff, iIndex, vCoords, fRadius, eModel))
				CREATE_MODEL_HIDE(vCoords, fRadius, eModel, FALSE)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_AMBIENT, "[IE_DELIVERY] IE_DELIVERY_MAINTAIN_MODEL_VISIBILITY_NEAR_DROPOFFS - Created model hide for model ", GET_MODEL_NAME_FOR_DEBUG(eModel), " for drop off ", eDropoff, " at coords ", vCoords, " and radius ", fRadius)
				#ENDIF
				iIndex++
			ENDWHILE
			
			SET_BIT(g_IEDeliveryData.iDropoffModelHideSetUpBS[iArray], iBit)
		ENDIF
	ELSE
		IF VDIST(GET_PLAYER_COORDS(PLAYER_ID()), vCoords) > 255
			WHILE(GET_DROPOFF_HIDDEN_MODEL_DETAILS(eDropoff, iIndex, vCoords, fRadius, eModel))
				REMOVE_MODEL_HIDE(vCoords, fRadius, eModel, FALSE)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_AMBIENT, "[IE_DELIVERY] IE_DELIVERY_MAINTAIN_MODEL_VISIBILITY_NEAR_DROPOFFS - Created model hide for model ", GET_MODEL_NAME_FOR_DEBUG(eModel), " for drop off ", eDropoff, " at coords ", vCoords, " and radius ", fRadius)
				#ENDIF
				iIndex++
			ENDWHILE
			
			CLEAR_BIT(g_IEDeliveryData.iDropoffModelHideSetUpBS[iArray], iBit)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC _PRINT_STATE_OF_IE_DELIVERY_SERVER()
	PRINTLN("[IE_DELIVERY][SERVER] _PRINT_STATE_OF_IE_DELIVERY_SERVER")
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		PLAYER_INDEX pCurrent = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(pCurrent)
			IF GlobalServerBD.IEDeliveryBD.sVehCleanup[i].iVehicleIndex > -1
			AND NOT GlobalServerBD.IEDeliveryBD.sVehCleanup[i].bSentCleanupEvent
				PRINTLN("[IE_DELIVERY][SERVER] _PRINT_STATE_OF_IE_DELIVERY_SERVER - Player ", GET_PLAYER_NAME(pCurrent), " is delivering vehicle model ", ENUM_TO_INT(GlobalServerBD.IEDeliveryBD.sVehCleanup[i].vehicleModel), " owned by ", GET_PLAYER_NAME(GlobalServerBD.IEDeliveryBD.sVehCleanup[i].playerVehicleOwner))
			ELSE
				PRINTLN("[IE_DELIVERY][SERVER] _PRINT_STATE_OF_IE_DELIVERY_SERVER - Player ", GET_PLAYER_NAME(pCurrent), " is not doing any deliveries.")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC MAINTAIN_IE_DELIVERY_SERVER(IE_DELIVERY_FREEMODE_DATA &data)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	// Check if there are any cleanup events that need to be sent
	IF GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].iVehicleIndex > -1
	AND NOT GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].bSentCleanupEvent
		BOOL bSendCleanupBroadcast
		PLAYER_INDEX pCurrent = INT_TO_PLAYERINDEX(data.iServerPlayerStagger)
		
		IF NOT IS_NET_PLAYER_OK(pCurrent)
			bSendCleanupBroadcast = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][SERVER] MAINTAIN_IE_DELIVERY_SERVER - Will send the broadcast because player (", data.iServerPlayerStagger ,") is not ok.")
			#ENDIF
		ENDIF
		
		INT iTimeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].timeDelivered)
		IF iTimeDiff > 6000
			// Player didn't send cleanup broadcast in 6 seconds after they said they delivered... Do it for them
			bSendCleanupBroadcast = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][SERVER] MAINTAIN_IE_DELIVERY_SERVER - Will send the broadcast because player (", data.iServerPlayerStagger ,") hasn't done it in 6s")
				PRINTLN("[IE_DELIVERY][SERVER] MAINTAIN_IE_DELIVERY_SERVER - They delivered at ", NATIVE_TO_INT(GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].timeDelivered))
				PRINTLN("[IE_DELIVERY][SERVER] MAINTAIN_IE_DELIVERY_SERVER - Current network time ", NATIVE_TO_INT(GET_NETWORK_TIME()))
				PRINTLN("[IE_DELIVERY][SERVER] MAINTAIN_IE_DELIVERY_SERVER - Diff ", iTimeDiff)
			#ENDIF
		ENDIF
		
		IF bSendCleanupBroadcast
			BROADCAST_OK_TO_CLEANUP_EXPORT_VEHICLE(
				GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].playerVehicleOwner,
				GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].vehicleModel,
				GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].iVehicleIndex,
				TRUE
			)
			
			//GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].iVehicleIndex = -1
			GlobalServerBD.IEDeliveryBD.sVehCleanup[data.iServerPlayerStagger].bSentCleanupEvent = TRUE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[IE_DELIVERY][SERVER] MAINTAIN_IE_DELIVERY_SERVER - OK. Sent!")
				_PRINT_STATE_OF_IE_DELIVERY_SERVER()
			#ENDIF
		ENDIF
	ENDIF
	
	data.iServerPlayerStagger = (data.iServerPlayerStagger + 1) % NUM_NETWORK_PLAYERS
ENDPROC

FUNC BOOL IE_DELIVERY_DOES_SERVER_ACKNOWLEDGE_OUR_DELIVERY(VEHICLE_INDEX vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
	AND NOT IS_ENTITY_DEAD(vehIndex)
		IF GB_IS_EXPORT_ENTITY(vehIndex)
			PLAYER_INDEX playerOriginalOwner = GB_GET_OWNER_OF_CONTRABAND(vehIndex)
			INT iExportVehIndex = GB_GET_INDEX_OF_CONTRABAND(vehIndex)
			MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehIndex)
			
			IF GlobalServerBD.IEDeliveryBD.sVehCleanup[NATIVE_TO_INT(PLAYER_ID())].playerVehicleOwner = playerOriginalOwner
			AND GlobalServerBD.IEDeliveryBD.sVehCleanup[NATIVE_TO_INT(PLAYER_ID())].vehicleModel = vehicleModel
			AND GlobalServerBD.IEDeliveryBD.sVehCleanup[NATIVE_TO_INT(PLAYER_ID())].iVehicleIndex = iExportVehIndex
			AND GlobalServerBD.IEDeliveryBD.sVehCleanup[NATIVE_TO_INT(PLAYER_ID())].bSentCleanupEvent = FALSE
				RETURN TRUE
			ENDIF
		ELSE
			// No server authorised deliveries for non export vehicles
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ENTRY POINT IN FREEMODE
PROC MAINTAIN_IE_DELIVERY(IE_DELIVERY_FREEMODE_DATA &data)

	IF MPGlobals.sFreemodeCache.bNetworkIsActivitySession
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		IE_DELIVERY_MAINTAIN_FREEMODE_DATA(data)
		
		IE_DELIVERY_MAINTAIN_CHECKS_FOR_ACTIVE_DROPOFFS(data)
		IE_DELIVERY_MAINTAIN_DELIVERY_CORONA(data)
		IE_DELIVERY_MAINTAIN_LAUNCHING_CUTSCENE_SCRIPT_FOR_DROPOFF_LOCATION(g_IEDeliveryData.eClosestActiveDropoffID, data)
		IE_DELIVERY_MAINTAIN_BLIP_FOR_DROPOFF_LOCATION(INT_TO_ENUM(IE_DROPOFF, data.iDropoffIDStagger), data)	
	
		IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
			IF NOT IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			
				IF GB_GET_OWNER_OF_CONTRABAND(g_IEDeliveryData.vehCurrentlyDelivering) != GB_GET_LOCAL_PLAYER_GANG_BOSS()
					START_NET_TIMER(data.timerShowOffMissionHelp)
				ENDIF
				
				IE_DELIVERY_FORCE_UPDATE_ALL_BLIPS(data, TRUE)
				FORCE_SIMPLE_INTERIOR_BLIPS_UPDATE(TRUE)
				SET_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			ENDIF
		ELSE
			IF IS_BIT_SET(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
				RESET_NET_TIMER(data.timerShowOffMissionHelp)
				IE_DELIVERY_FORCE_UPDATE_ALL_BLIPS(data, TRUE)
				FORCE_SIMPLE_INTERIOR_BLIPS_UPDATE(TRUE)
				CLEAR_BIT(data.iBS, BS_IE_DELIVERY_FREEMODE_WAS_DELIVERING_LAST_FRAME)
			ENDIF
		ENDIF
		
		IE_DELIVERY_MAINTAIN_MODEL_VISIBILITY_NEAR_DROPOFFS(data)
		IE_DELIVERY_MAINTAIN_HELP(data)
	ENDIF
		
	IE_DELIVERY_MAINTAIN_OFF_MISSION_HELP(data)
	IE_DELIVERY_MAINTAIN_BOSS_REPAIR_COSTS()
	IE_DELIVERY_MAINTAIN_REWARD_QUEUE(data)
	
	IF NOT IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		MAINTAIN_TRAFFIC_REDUCTION_ZONES(data)
		MAINTAIN_MOC_TRAFFIC_REDUCTION_ZONES()
		MAINTAIN_CLEAR_DROP_OFF_AREA_OF_UNDRIVEABLE_VEHICLES(data)
	ENDIF
	
	MAINTAIN_IE_DELIVERY_SERVER(data)

	data.iDropoffIDStagger = (data.iDropoffIDStagger + 1) % ENUM_TO_INT(IE_DROPOFF_COUNT)
ENDPROC

PROC CLEANUP_IE_DELIVERY(IE_DELIVERY_FREEMODE_DATA &data)
	IE_DELIVERY_CLEANUP_FREEMODE_DATA(data)
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_IE_DELIVERY_DEBUG_WIDGETS(IE_DELIVERY_FREEMODE_DATA &data)
	UNUSED_PARAMETER(data)
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehicleExportIgnoreVehicleCheckOnDelivery")
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.db_bAllowAllVehicles = TRUE
		PRINTLN("[IE_DELIVERY] CREATE_IE_DELIVERY_DEBUG_WIDGETS - Setting GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.db_bAllowAllVehicles to TRUE")
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehicleExportForceCleanupOnDelivery")
		g_IEDeliveryData.d_bForceCleanup = TRUE
		PRINTLN("[IE_DELIVERY] CREATE_IE_DELIVERY_DEBUG_WIDGETS - Setting g_IEDeliveryData.d_bForceCleanup to TRUE")
	ENDIF
	
	data.d_fCoronaRadius = 4.0
	
	/*
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehicleExportDropoffsAcceptAllClasses")
		g_IEDeliveryData.d_bDropoffsAcceptAllClasses = TRUE
		PRINTLN("[IE_DELIVERY] CREATE_IE_DELIVERY_DEBUG_WIDGETS - Setting g_IEDeliveryData.d_bDropoffsAcceptAllClasses to TRUE")
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_VehicleExportDropoffsAlwaysAvailable")
		g_IEDeliveryData.d_bDropoffsAlwaysAvailable = TRUE
		PRINTLN("[IE_DELIVERY] CREATE_IE_DELIVERY_DEBUG_WIDGETS - Setting g_IEDeliveryData.d_bDropoffsAlwaysAvailable to TRUE")
	ENDIF
	*/
	
	START_WIDGET_GROUP("IE Delivery")
		TEXT_LABEL_63 strTitle //, strPropTitle
		INT i//, j
			
		ADD_WIDGET_BOOL("All vehicles can be delivered everywhere", GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].IEDeliveryBD.db_bAllowAllVehicles)
		ADD_WIDGET_BOOL("Force vehicle cleanup on delivery", g_IEDeliveryData.d_bForceCleanup)
		ADD_WIDGET_BOOL("Draw debug", g_IEDeliveryData.d_bDrawDebug)
		ADD_WIDGET_BOOL("Force cargobob variation", g_IEDeliveryData.d_bForceCargobobVariation)
		ADD_WIDGET_BOOL("Clear repair cost help flag", data.d_bResetRepairHelpFlag)
		//ADD_WIDGET_BOOL("Force towtruck variation", g_IEDeliveryData.d_bForceTowtruckVariation)
		ADD_WIDGET_BOOL("Reset IE warehouse intro cutscene", data.d_bResetIEWarehouseIntro)
		
		START_WIDGET_GROUP("Test car")
			data.d_iTestCarOwner = NATIVE_TO_INT(PLAYER_ID())
			//ADD_WIDGET_INT_SLIDER("Dropoff to assign", data.d_iTestCarDropoffID, 0, ENUM_TO_INT(IE_DROPOFF_COUNT), 1)
			ADD_WIDGET_INT_SLIDER("Index to assign", data.d_iTestCarIndex, 0, MAX_NUM_VEHICLE_EXPORT_ENTITIES, 1)
			ADD_WIDGET_INT_SLIDER("Player owner", data.d_iTestCarOwner, 0, NUM_NETWORK_PLAYERS, 1)
			ADD_WIDGET_INT_SLIDER("Vehicle enum", data.d_iTestCarEnum, 0, ENUM_TO_INT(IE_VEHICLE_COUNT), 1)
			//ADD_WIDGET_BOOL("Car being sold", data.d_bTestCarBeingSold)
			ADD_WIDGET_BOOL("Set on current car", data.d_bSetOnCurrentCar)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Activate dropoffs")
			ADD_WIDGET_BOOL("Activate gangster", data.d_bActivateGangster)
			ADD_WIDGET_BOOL("Activate showroom", data.d_bActivateShowroom)
			ADD_WIDGET_BOOL("Activate private", data.d_bActivatePrivate)
			ADD_WIDGET_BOOL("Activate warehouse", data.d_bActivateWarehouse)
			ADD_WIDGET_INT_SLIDER("Car count", data.d_iActivateCount, 0, MAX_NUM_VEHICLE_EXPORT_ENTITIES, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Repair costs test")
			ADD_WIDGET_INT_SLIDER("Player who caused repair", data.d_iPlayerCausedRepair, 0, NUM_NETWORK_PLAYERS, 1)
			ADD_WIDGET_INT_SLIDER("Amount to pay", data.d_iRepairToPay, 0, 8000, 1)
			ADD_WIDGET_BOOL("PAY UP", data.d_bSetRepairCost)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Reward queue")
			ADD_WIDGET_INT_SLIDER("Cash", data.d_iRewardCash, 0, 100000, 100)
			ADD_WIDGET_INT_SLIDER("RP", data.d_iRewardRP, 0, 1000000, 10)
			ADD_WIDGET_BOOL("Reward!", data.d_bQueueReward)
		STOP_WIDGET_GROUP()
		
		//ADD_WIDGET_BOOL("Every dropoff accepts every class", g_IEDeliveryData.d_bDropoffsAcceptAllClasses)
		//ADD_WIDGET_BOOL("All dropoffs always available", g_IEDeliveryData.d_bDropoffsAlwaysAvailable)
		//ADD_WIDGET_BOOL("Use availability snapshot", g_IEDeliveryData.bOverrideAvailabilityWithSnapshot)
		
		REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
			strTitle = "Dropoff "
			strTitle += i
			ADD_WIDGET_INT_READ_ONLY(strTitle, data.d_iSelectedDropoffs[i])
		ENDREPEAT
		
		REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
			strTitle = "Dropoff to activate in slot "
			strTitle += i
			ADD_WIDGET_INT_SLIDER(strTitle, data.d_iDropoffsToActivate[i], 0, ENUM_TO_INT(IE_DROPOFF_COUNT), 1)
			ADD_WIDGET_BOOL("Activate", data.d_bActivateSelectedDropoff[i])
		ENDREPEAT
		
		ADD_WIDGET_FLOAT_SLIDER("Corona radius", data.d_fCoronaRadius, 0.1, 100.0, 0.01)
		
		START_WIDGET_GROUP("Dropoffs")
			REPEAT IE_DROPOFF_COUNT i
				strTitle = IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(INT_TO_ENUM(IE_DROPOFF, i))
				//strTitle += " [Accepts: "
				//strTitle += IE_DELIVERY_GET_DROPOFF_VEHICLE_CLASS_FOR_DEBUG(INT_TO_ENUM(IE_DROPOFF, i))
				//strTitle += "]"
				START_WIDGET_GROUP(strTitle)
					//ADD_WIDGET_INT_READ_ONLY("Is available?", data.d_iDropoffAvailabilities[i])
					ADD_WIDGET_BOOL("Do warp", data.d_bDoWarp[i])
					data.d_vCoronaCoords[i] = IE_DELIVERY_GET_DROPOFF_COORDS(INT_TO_ENUM(IE_DROPOFF, i))
					ADD_WIDGET_VECTOR_SLIDER("Locate", data.d_vCoronaCoords[i], -9999.9, 9999.9, 0.01)
					//ADD_WIDGET_INT_READ_ONLY("Accepts all?", data.d_iAcceptsAll[i])
					//ADD_WIDGET_INT_READ_ONLY("Accepted veh class", data.d_iAcceptedVehClass[i])
					
					/*
					START_WIDGET_GROUP("Props")
						j = 0
						WHILE IE_DELIVERY_GET_DROPOFF_PROP_DATA(INT_TO_ENUM(IE_DROPOFF, i), j, ePropModel, vPropPos, vPropRot)
							strPropTitle = "["
							strPropTitle += j
							strPropTitle += "]"
							
							//IF DOES_ENTITY_EXIST(data.objProps[i][j])
								data.d_vPropsPos[i][j] = vPropPos
								data.d_vPropsRot[i][j] = vPropRot
							//ENDIF
							
							START_WIDGET_GROUP(strPropTitle)
								ADD_WIDGET_VECTOR_SLIDER("Coords", data.d_vPropsPos[i][j], -9999.9, 9999.9, 0.01)
								ADD_WIDGET_VECTOR_SLIDER("Rot", data.d_vPropsRot[i][j], 0.0, 360.0, 0.1)
							STOP_WIDGET_GROUP()
							j += 1
						ENDWHILE
					STOP_WIDGET_GROUP()
					*/
					
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_IE_DELIVERY_DEBUG_WIDGETS(IE_DELIVERY_FREEMODE_DATA &data)
	
	IF data.d_bSetOnCurrentCar
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX _playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			SET_ENTITY_AS_MISSION_ENTITY(_playerVeh)
			GB_SET_VEHICLE_AS_CONTRABAND(_playerVeh, data.d_iTestCarIndex, GBCT_EXPORT, INT_TO_ENUM(IE_VEHICLE_ENUM, data.d_iTestCarEnum), DEFAULT ,data.d_iTestCarOwner)
			//GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eSelectedSellDropoffs[data.d_iTestCarIndex] = INT_TO_ENUM(IE_DROPOFF, data.d_iTestCarDropoffID)
		ENDIF
		data.d_bSetOnCurrentCar = FALSE
	ENDIF
	
	IF data.d_bDoWarp[data.d_eDropoffStagger]
		VECTOR vWarpCoord = <<0.0, 0.0, 0.0>>
		vWarpCoord = IE_DELIVERY_GET_DROPOFF_COORDS(data.d_eDropoffStagger)
		IF NET_WARP_TO_COORD(vWarpCoord, 0.0, TRUE)
			data.d_bDoWarp[data.d_eDropoffStagger] = FALSE
		ENDIF
	ENDIF
	
	INT  i
	REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
		IF data.d_bActivateSelectedDropoff[i]
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eSelectedSellDropoffs[i] = INT_TO_ENUM(IE_DROPOFF, data.d_iDropoffsToActivate[i])
			data.d_bActivateSelectedDropoff[i] = FALSE
		ENDIF
		data.d_iSelectedDropoffs[i] = ENUM_TO_INT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eSelectedSellDropoffs[i])
	ENDREPEAT
	
	IF data.d_bActivateGangster
		IE_DELIVERY_ACTIVATE_DROPOFF_OF_TYPE_FOR_GANG(VEHICLE_EXPORT_DROPOFF_BUYER_GANGSTER, data.d_iActivateCount)
		data.d_bActivateGangster = FALSE
	ENDIF
	
	IF data.d_bActivateShowroom
		IE_DELIVERY_ACTIVATE_DROPOFF_OF_TYPE_FOR_GANG(VEHICLE_EXPORT_DROPOFF_BUYER_SHOWROOM, data.d_iActivateCount)
		data.d_bActivateShowroom = FALSE
	ENDIF
	
	IF data.d_bActivatePrivate
		IE_DELIVERY_ACTIVATE_DROPOFF_OF_TYPE_FOR_GANG(VEHICLE_EXPORT_DROPOFF_BUYER_PRIVATE, data.d_iActivateCount)
		data.d_bActivatePrivate = FALSE
	ENDIF
	
	IF data.d_bActivateWarehouse
		IE_DELIVERY_ACTIVATE_DROPOFF_OF_TYPE_FOR_GANG(VEHICLE_EXPORT_DROPOFF_GB_WAREHOUSE, data.d_iActivateCount)
		data.d_bActivateWarehouse = FALSE
	ENDIF
	
	// Remote launch, boss has to copy dropoff data from launching player
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AllowRemoteMissionLaunch")
	AND GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
			INT j
			PLAYER_INDEX playerGoon
			REPEAT GB_GET_NUM_GOONS_IN_LOCAL_GANG() i
				playerGoon = GB_GET_GANG_GOON_AT_INDEX(PLAYER_ID(), i)
				IF playerGoon != INVALID_PLAYER_INDEX()
				AND GlobalplayerBD_FM_3[NATIVE_TO_INT(playerGoon)].sMagnateGangBossData.db_bBossShouldCopySelectedDropoffsFromMe
					REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES j
						GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eSelectedSellDropoffs[j] = GlobalplayerBD_FM_3[NATIVE_TO_INT(playerGoon)].sMagnateGangBossData.eSelectedSellDropoffs[j]
					ENDREPEAT
					
					PRINTLN("[IE_DELIVERY] MAINTAIN_IE_DELIVERY_DEBUG_WIDGETS - We are boss and we just copied selected dropoffs from ", GET_PLAYER_NAME(playerGoon))
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ELSE
			IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.db_bBossShouldCopySelectedDropoffsFromMe
				BOOL bAllCopied = TRUE
				PLAYER_INDEX playerBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				REPEAT MAX_NUM_VEHICLE_EXPORT_ENTITIES i
					IF GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.eSelectedSellDropoffs[i] != GlobalplayerBD_FM_3[NATIVE_TO_INT(playerBoss)].sMagnateGangBossData.eSelectedSellDropoffs[i]
						bAllCopied = FALSE
						BREAKLOOP
					ENDIF
				ENDREPEAT
				
				IF bAllCopied
					GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.db_bBossShouldCopySelectedDropoffsFromMe = FALSE
					PRINTLN("[IE_DELIVERY] MAINTAIN_IE_DELIVERY_DEBUG_WIDGETS - We are goon and are boss just copied selected dropoffs from us.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	/*
	INT i
	MODEL_NAMES ePropModel
	VECTOR vPropPos, vPropRot
	
	WHILE IE_DELIVERY_GET_DROPOFF_PROP_DATA(data.d_eDropoffStagger, i, ePropModel, vPropPos, vPropRot)
		IF DOES_ENTITY_EXIST(data.objProps[data.d_eDropoffStagger][i])
			IF NOT ARE_VECTORS_ALMOST_EQUAL(data.d_vPropsPos[data.d_eDropoffStagger][i], GET_ENTITY_COORDS(data.objProps[data.d_eDropoffStagger][i]), 0.001)
				SET_ENTITY_COORDS(data.objProps[data.d_eDropoffStagger][i], data.d_vPropsPos[data.d_eDropoffStagger][i])
			ENDIF
			
			IF NOT ARE_VECTORS_ALMOST_EQUAL(data.d_vPropsRot[data.d_eDropoffStagger][i], GET_ENTITY_ROTATION(data.objProps[data.d_eDropoffStagger][i]), 0.001)
				SET_ENTITY_ROTATION(data.objProps[data.d_eDropoffStagger][i], data.d_vPropsRot[data.d_eDropoffStagger][i])
			ENDIF
		ENDIF
		i += 1
	ENDWHILE
	*/
	
	// Drawing debug stuff
	IF g_IEDeliveryData.d_bDrawDebug
		/*
		IF NOT IS_TRANSITION_ACTIVE()
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		ENDIF
		*/
		
		FLOAT fSpacing = 0.016
		
		SET_TEXT_SCALE(0.0000, 0.3)
		SET_TEXT_COLOUR(255,0,0,255)
		DISPLAY_TEXT_WITH_TWO_LITERAL_STRINGS(0.02, 0.02 + fSpacing * 0, "TWOSTRINGS", "Closest dropoff: ", IE_DELIVERY_GET_DROPOFF_NAME_FOR_DEBUG(g_IEDeliveryData.eClosestActiveDropoffID))
		
		IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
			PLAYER_INDEX pDeliverer = IE_DELIVERY_GET_PLAYER_IN_CHARGE_OF_DELIVERY_OF_THIS_CAR(g_IEDeliveryData.vehCurrentlyDelivering)
			SET_TEXT_SCALE(0.0000, 0.3)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_TWO_LITERAL_STRINGS(0.02, 0.02 + fSpacing * 1, "TWOSTRINGS", "Currently delivering: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(g_IEDeliveryData.vehCurrentlyDelivering)))
			SET_TEXT_SCALE(0.0000, 0.3)
			SET_TEXT_COLOUR(255,0,0,255)
			DISPLAY_TEXT_WITH_TWO_LITERAL_STRINGS(0.02, 0.02 + fSpacing * 2, "TWOSTRINGS", "Player in charge: ", GET_PLAYER_NAME(pDeliverer))
		ENDIF
	ENDIF
	
	IF data.d_bSetRepairCost
		g_IEDeliveryData.pWhoDidDeliveryForRepair = INT_TO_PLAYERINDEX(data.d_iPlayerCausedRepair)
		g_IEDeliveryData.iRepairCostForBoss = data.d_iRepairToPay
		REINIT_NET_TIMER(g_IEDeliveryData.timerRepairTicker)
		data.d_bSetRepairCost = FALSE
	ENDIF
	
	IF data.d_bQueueReward
		QUEUED_VEHICLE_EXPORT_REWARD tmpReward
		tmpReward.iCashEarned = data.d_iRewardCash
		tmpReward.iRPEarned = data.d_iRewardRP
		IE_DELIVERY_QUEUE_REWARD(tmpReward)
		data.d_bQueueReward = FALSE
	ENDIF
	
	IF data.d_bResetRepairHelpFlag
		CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet3, BI_FM_GANG_BOSS_HELP_3_DONE_REPAIR_COST_INCURRED_HELP)
		data.d_bResetRepairHelpFlag = FALSE
	ENDIF
	
	IF data.d_bResetIEWarehouseIntro
		INT iStatInt
		iStatInt = g_MP_STAT_FM_CUT_DONE[GET_SLOT_NUMBER(-1)]
		CLEAR_BIT(iStatInt, biFmCut_IE_IntroCut)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_FM_CUT_DONE, iStatInt)
		data.d_bResetIEWarehouseIntro = FALSE
	ENDIF
	
	data.d_eDropoffStagger = INT_TO_ENUM(IE_DROPOFF, (ENUM_TO_INT(data.d_eDropoffStagger) + 1) % ENUM_TO_INT(IE_DROPOFF_COUNT))
ENDPROC
#ENDIF

