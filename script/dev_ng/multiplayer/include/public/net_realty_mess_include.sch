USING "mp_globals_fm.sch"
USING "transition_saving.sch"

// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	net_realty_mess_include.sch
//		DESCRIPTION		:	Functions to track mess-creating activities
//
// *****************************************************************************************
// *****************************************************************************************
FUNC INT GET_PROPERTY_MESS_INDEX(INT iPropertySlot)
	IF iPropertySlot = PROPERTY_OWNED_SLOT_APT_7
		RETURN 18
	ELIF iPropertySlot = PROPERTY_OWNED_SLOT_APT_8
		RETURN 19
	ELIF iPropertySlot = PROPERTY_OWNED_SLOT_APT_9
		RETURN 20
	ELIF iPropertySlot = PROPERTY_OWNED_SLOT_APT_10
		RETURN 21
	ENDIF
	IF iPropertySlot >= PROPERTY_OWNED_SLOT_OFFICE_0 //MAX_OWNED_PROPERTIES
	AND iPropertySlot != YACHT_PROPERTY_OWNED_SLOT
		IF iPropertySlot <= PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY
			RETURN iPropertySlot-PROPERTY_OWNED_SLOT_OFFICE_0
		ENDIF
	ENDIF
	RETURN -1
ENDFUNC


PROC INCREMENT_MP_APT_SMOKE_VARIABLE(INT iPropertySlot)
	SWITCH iPropertySlot //MAX_OWNED_PROPERTIES
		CASE 0
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked++
			PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked)
			EXIT
		BREAK
		CASE 1
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond++
			PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond)	
			EXIT
		BREAK
		CASE 2
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3++
			PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3)
			EXIT
		BREAK
		CASE 3
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked4++
			PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked4)	
			EXIT
		BREAK
		CASE 4
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked5++
			PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked5)	
			EXIT
		BREAK
		CASE YACHT_PROPERTY_OWNED_SLOT
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedYacht++
			PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedYacht)	
			EXIT
		BREAK
	ENDSWITCH
	INT iIndex = GET_PROPERTY_MESS_INDEX(iPropertySlot)
	IF iIndex != -1
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesSmoked[iIndex]++
		PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesSmoked[iIndex])
	ENDIF
ENDPROC



PROC INCREMENT_MP_APT_SMOKE_COUNTER()
	
	PLAYER_INDEX piOwner = g_ownerOfPropertyIAmIn
	//this data gets cleared when player enters property. its only used to hold data when transition into property
//	IF globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
//		IF IS_GAMER_HANDLE_VALID(globalPropertyEntryData.ownerHandle)
//			IF NETWORK_IS_GAMER_IN_MY_SESSION(globalPropertyEntryData.ownerHandle)
//				piOwner = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
//				PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): globalPropertyEntryData.ownerID was invalid, got piOwner from globalPropertyEntryData.ownerHandle")
//			ENDIF
//		ENDIF
//	ELSE
//		piOwner = globalPropertyEntryData.ownerID
//	ENDIF	
	INT i
	IF NATIVE_TO_INT(piOwner) > -1
	AND piOwner != INVALID_PLAYER_INDEX()
		REPEAT MAX_OWNED_PROPERTIES i
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iOwnedProperty[i]
				IF GET_PROPERTY_MESS_INDEX(i) != -1
					IF piOwner = PLAYER_ID()
						PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): Player is owner, incrementing info to local save struct")
						INCREMENT_MP_APT_SMOKE_VARIABLE(i)
						EXIT
					ELSE
						IF IS_NET_PLAYER_OK(piOwner,FALSE)
							PRINTLN("INCREMENT_MP_APT_SMOKE_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
							BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),1,0,0,i)
							// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
							EXIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
//		IF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(0)
//			IF piOwner != INVALID_PLAYER_INDEX()
//				// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesSmoked = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),1,0,0,0,0,0,0,0,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(1)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesSmokedSecond = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,1,0,0,0,0,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(2)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesSmoked3 = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,0,0,0,1,0,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC

PROC INCREMENT_MP_APT_DRINK_VARIABLE(INT iPropertySlot, INT iIncrementAmount = 1)
	SWITCH iPropertySlot
		CASE 0
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank + iIncrementAmount
			PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank)		
		BREAK
		CASE 1
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond + iIncrementAmount
			PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond)	
		BREAK
		CASE 2
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3 = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3 + iIncrementAmount
			PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3)	
		BREAK
		CASE 3
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4 = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4 + iIncrementAmount
			PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4)	
		BREAK
		CASE 4
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank5 = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank5 + iIncrementAmount
			PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank5)	
		BREAK
		CASE YACHT_PROPERTY_OWNED_SLOT
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankYacht = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankYacht + iIncrementAmount
			PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankYacht)	
		BREAK
	ENDSWITCH
	INT iIndex = GET_PROPERTY_MESS_INDEX(iPropertySlot)
	IF iIndex != -1
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesDrank[iIndex] = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesDrank[iIndex] + iIncrementAmount
		PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iArrayNumTimesDrank(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesDrank[iIndex])
	ENDIF
ENDPROC

PROC INCREMENT_MP_APT_DRINK_COUNTER()

	PLAYER_INDEX piOwner = g_ownerOfPropertyIAmIn
	
	//this data gets cleared when player enters property
//	IF globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
//		IF IS_GAMER_HANDLE_VALID(globalPropertyEntryData.ownerHandle)
//			IF NETWORK_IS_GAMER_IN_MY_SESSION(globalPropertyEntryData.ownerHandle)
//				piOwner = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
//				PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): globalPropertyEntryData.ownerID was invalid, got piOwner from globalPropertyEntryData.ownerHandle")
//			ENDIF
//		ENDIF
//	ELSE
//		piOwner = globalPropertyEntryData.ownerID
//	ENDIF
	INT i
	IF NATIVE_TO_INT(piOwner) > -1
	AND piOwner != INVALID_PLAYER_INDEX()
		REPEAT MAX_OWNED_PROPERTIES i
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iOwnedProperty[i]
				IF GET_PROPERTY_MESS_INDEX(i) != -1
					IF piOwner = PLAYER_ID()
						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
						INCREMENT_MP_APT_DRINK_VARIABLE(i)
						EXIT
					ELSE
						IF IS_NET_PLAYER_OK(piOwner,FALSE)
							PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
							BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,1,0,i)
							// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
							EXIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
//	IF NATIVE_TO_INT(piOwner) > -1
//		IF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(0)
//			IF piOwner != INVALID_PLAYER_INDEX()
//				// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,1,0,0,0,0,0,0,0)
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(1)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrankSecond = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,0,1,0,0,0,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(2)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank3 = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,0,0,0,0,1,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(3)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesDrank4 = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,0,0,0,0,0,0,0,1,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
	
	
ENDPROC

PROC INCREMENT_MP_APT_STRIPPER_VARIABLE(INT iPropertySlot)
	SWITCH iPropertySlot
		CASE 0
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers++
			PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesStrippers(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers)		
		BREAK
		CASE 1
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond++
			PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesStrippers(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond)	
		BREAK
		CASE 2
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3++
			PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesStrippers(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3)	
		BREAK
		CASE 3
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers4++
			PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesStrippers(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers4)	
		BREAK
		CASE 4
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers5++
			PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesStrippers(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers5)	
		BREAK
		CASE YACHT_PROPERTY_OWNED_SLOT
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersYacht++
			PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersYacht)	
		BREAK
	ENDSWITCH
	INT iIndex = GET_PROPERTY_MESS_INDEX(iPropertySlot)
	IF iIndex != -1
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesStripper[iIndex]++
		PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): iNumOfTimesSmoked(",iPropertySlot,") = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesStripper[iIndex])
	ENDIF
ENDPROC

PROC INCREMENT_MP_APT_STRIPPER_COUNTER()

	PLAYER_INDEX piOwner = g_ownerOfPropertyIAmIn
	
	INT i
	IF NATIVE_TO_INT(piOwner) > -1
	AND piOwner != INVALID_PLAYER_INDEX()
		REPEAT MAX_OWNED_PROPERTIES i
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iOwnedProperty[i]
				IF GET_PROPERTY_MESS_INDEX(i) != -1
					IF piOwner = PLAYER_ID()
						PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): Player is owner, incrementing info to local save struct")
						INCREMENT_MP_APT_STRIPPER_VARIABLE(i)
						EXIT
					ELSE
						IF IS_NET_PLAYER_OK(piOwner,FALSE)
							PRINTLN("INCREMENT_MP_APT_STRIPPER_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
							BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,1,i)
							// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
							EXIT
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	
//	//this data gets cleared when player enters property
//	IF NATIVE_TO_INT(piOwner) > -1
//		IF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(0)
//			IF piOwner != INVALID_PLAYER_INDEX()
//				// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesStrippers = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,1,0,0,0,0,0,0)
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(1)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesStrippersSecond = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,0,0,1,0,0,0)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(piOwner)].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(2)
//			IF piOwner != INVALID_PLAYER_INDEX()
//			// If all players get this info from the broadcast data, this would be easier, but I don't know how to do that...
//				IF piOwner = PLAYER_ID()
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is owner, incrementing info to local save struct")
//					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3++
//					PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): iNumOfTimesStrippers3 = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3)
//				ELSE
//					IF IS_NET_PLAYER_OK(piOwner,FALSE)
//						PRINTLN("INCREMENT_MP_APT_DRINK_COUNTER(): Player is NOT owner, incrementing info via broadcast data")
//						BROADCAST_PROPERTY_MESS_EVENT(SPECIFIC_PLAYER(piOwner),0,0,0,0,0,0,0,0,1)
//						// Increment iNumOfTimesDrank and send to property owner's save struct via broadcast data?
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC
