//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simple_interior_biker_wh.sch															//
// Description: This is implementation of biker illicit factories as simple interior.						//
//				As such this does not expose any public functions.											//
//				All functions to manipulate simple interiors are in net_simple_interior.					//
// Written by:  Neil, Tymon																					//
// Date:  		11/07/2016																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_gang_boss.sch"
USING "MP_globals_simple_interior.sch"
USING "net_simple_interior_base.sch"
USING "net_realty_new.sch"
USING "net_realty_biker_warehouse.sch"
USING "net_simple_interior_async_grid_warp.sch"
USING "net_simple_interior_cs_header_include.sch"

CONST_INT BS_SIMPLE_INTERIOR_FACTORY_STARTED_ENTERING 			0
CONST_INT BS_SIMPLE_INTERIOR_FACTORY_WANTED_LEVEL_CLEARED		1

CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS 	0 // We entered warehouse as boss (also owner)
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_GOON 	1 // We entered warehouse as goon
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_OWNER 	2 // We entered warehouse as owner (but not boss)

CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_PRESIDENT		10
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_VICE_PRESIDENT	11
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_ROAD_CAPTAIN	12
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_SERGEANT		13
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_PROSPECT		14
CONST_INT SIMPLE_INTERIOR_FACTORY_ACCESS_BS_ENFORCER		15

//Blip data
CONST_INT MAX_FACTORY_BLIPS	1

FUNC BOOL DOES_FACTORY_USE_EXTERIOR_SCRIPT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC _INIT_GENERIC_FACTORY_METH(SIMPLE_INTERIOR_DETAILS &details)
	details.strInteriorChildScript = "AM_MP_BIKER_WAREHOUSE"
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-13.148, -4.120, 0.840>>
	details.exitData.vLocateCoords2[0] = <<-10.838, -4.120, 3.758>>
	details.exitData.fLocateWidths[0] = 3.611
	details.exitData.fHeadings[0] = 90.0
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
ENDPROC

PROC _INIT_GENERIC_FACTORY_WEED(SIMPLE_INTERIOR_DETAILS &details)
	details.strInteriorChildScript = "AM_MP_BIKER_WAREHOUSE"
	// Exit data
	details.exitData.vLocateCoords1[0] = <<17.635, 13.228, 1.148>>
	details.exitData.vLocateCoords2[0] = <<14.985, 13.228, -1.725>>
	details.exitData.fLocateWidths[0] = 2.130
	details.exitData.fHeadings[0] = 270.0
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
ENDPROC

PROC _INIT_GENERIC_FACTORY_CRACK(SIMPLE_INTERIOR_DETAILS &details)
	details.strInteriorChildScript = "AM_MP_BIKER_WAREHOUSE"
	// Exit data
	details.exitData.vLocateCoords1[0] = <<-6.445, 9.430, 0.785>>
	details.exitData.vLocateCoords2[0] = <<-3.475, 9.430, -1.470>>
	details.exitData.fLocateWidths[0] = 3.060
	details.exitData.fHeadings[0] = 0.0
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
ENDPROC

PROC _INIT_GENERIC_FACTORY_MONEY(SIMPLE_INTERIOR_DETAILS &details)
	details.strInteriorChildScript = "AM_MP_BIKER_WAREHOUSE"
	// Exit data
	details.exitData.vLocateCoords1[0] = <<13.540, -2.890, 0.356>>
	details.exitData.vLocateCoords2[0] = <<13.540, -0.530, -3.000>>
	details.exitData.fLocateWidths[0] = 2.548
	details.exitData.fHeadings[0] = 180.0
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
ENDPROC

PROC _INIT_GENERIC_FACTORY_DOCUMENTS(SIMPLE_INTERIOR_DETAILS &details)
	details.strInteriorChildScript = "AM_MP_BIKER_WAREHOUSE"
	// Exit data
	details.exitData.vLocateCoords1[0] = <<9.752, 0.021, 0.793>>
	details.exitData.vLocateCoords2[0] = <<7.958, 0.021, -1.85>>
	details.exitData.fLocateWidths[0] = 2.130
	details.exitData.fHeadings[0] = 270.0
	
	details.entryAnim.strOnEnterSoundNames[0] = "PUSH"
	details.entryAnim.strOnEnterSoundNames[1] = "LIMIT"
	details.entryAnim.fOnEnterSoundPhases[0] = 0.271
	details.entryAnim.fOnEnterSoundPhases[1] = 0.411
	details.entryAnim.strOnEnterPhaseSoundSet = "GTAO_EXEC_WH_DOOR_GENERIC_SOUNDS"
ENDPROC

PROC GET_FACTORY_INTERIOR_TYPE_AND_POSITION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			vPosition = <<1009.5, -3196.6, -38.5>>
			fHeading = 0.0
			txtType = "bkr_biker_dlc_int_ware01"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			vPosition = <<1049.6, -3196.6, -38.5>>
			fHeading = 0.0
			txtType = "bkr_biker_dlc_int_ware02"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			vPosition = <<1093.6, -3196.6, -38.5>>
			fHeading = 0.0
			txtType = "bkr_biker_dlc_int_ware03"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			vPosition = <<1124.6, -3196.6, -38.5>>
			fHeading = 0.0
			txtType = "bkr_biker_dlc_int_ware04"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			vPosition = <<1165.0, -3196.6, -38.2>>
			fHeading = 0.0
			txtType = "bkr_biker_dlc_int_ware05"
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_FACTORY_LOAD_SECONDARY_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER_SET()
		SET_INTERIOR_FLOOR_INDEX(GET_FLOOR_INDEX_FOR_VEHICLE_ENTRY_AS_PASSENGER())
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_DETAILS_BS GET_FACTORY_PROPERTIES_BS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_DETAILS_BS structReturn
	
	//BS1
	
	//BS2
	SET_BIT(structReturn.iBS[1], BS2_SIMPLE_INTERIOR_DETAILS_PARENT_INT_SCRIPT_DOES_LOAD_SCENE)
	
	RETURN structReturn
ENDFUNC

PROC GET_FACTORY_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(bSMPLIntPreCache)
	details.sProperties = GET_FACTORY_PROPERTIES_BS(eSimpleInteriorID)
	
	SWITCH eSimpleInteriorID
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_1 // pass 1
			_INIT_GENERIC_FACTORY_METH(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<50.8801, 6336.8755, 32.1390>>
			details.entryAnim.cameraRot = <<-0.7937, 0.0000, -63.3392>>
			details.entryAnim.cameraFov = 40.1256
			details.entryAnim.syncScenePos = <<53.457, 6337.362, 30.641>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -150.840>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<59.6936, 6359.3013, 36.3307>>
			details.entryAnim.establishingCameraRot = <<-1.8386, 0.0000, 163.8716>>
			details.entryAnim.fEstablishingCameraFov = 42.6393
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1 // pass 1
			_INIT_GENERIC_FACTORY_WEED(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<416.6734, 6524.7129, 28.1782>>
			details.entryAnim.cameraRot = <<0.8228, 0.0000, 172.1154>>
			details.entryAnim.cameraFov = 30.8437
			details.entryAnim.syncScenePos = <<415.990, 6520.941, 26.755>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 87.480>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<435.9453, 6546.2085, 36.2606>>
			details.entryAnim.establishingCameraRot = <<-7.4123, -0.0000, 132.5485>>
			details.entryAnim.fEstablishingCameraFov = 45.7557
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1 // pass 1
			_INIT_GENERIC_FACTORY_CRACK(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<49.1174, 6487.8560, 32.0173>>
			details.entryAnim.cameraRot = <<-2.8312, 0.0000, -130.0002>>
			details.entryAnim.cameraFov = 33.4542
			details.entryAnim.syncScenePos = <<51.260, 6485.603, 30.428>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 136.440>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<67.5147, 6496.1543, 37.7476>>
			details.entryAnim.establishingCameraRot = <<-6.3943, 0.0000, 136.1353>>
			details.entryAnim.fEstablishingCameraFov = 44.9118
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1 // pass 1
			_INIT_GENERIC_FACTORY_MONEY(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-415.9254, 6173.2739, 32.2690>>
			details.entryAnim.cameraRot = <<-3.4101, -0.0000, -137.4814>>
			details.entryAnim.cameraFov = 32.1246
			details.entryAnim.syncScenePos = <<-414.329, 6171.476, 30.681>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 136.440>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-415.8688, 6186.6938, 36.3816>>
			details.entryAnim.establishingCameraRot = <<-7.4606, 0.0000, -177.6217>>
			details.entryAnim.fEstablishingCameraFov = 46.3689
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1 // pass 1
			_INIT_GENERIC_FACTORY_DOCUMENTS(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-165.6212, 6335.8062, 32.2612>>
			details.entryAnim.cameraRot = <<-2.5946, 0.0000, -137.9730>>
			details.entryAnim.cameraFov = 39.2599
			details.entryAnim.syncScenePos = <<-164.324, 6334.336, 30.630>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 136.440>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-164.5620, 6348.6855, 35.2535>>
			details.entryAnim.establishingCameraRot = <<-6.5189, -0.0000, 178.7005>>
			details.entryAnim.fEstablishingCameraFov = 45.7449
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_2 // pass 1
			_INIT_GENERIC_FACTORY_METH(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1457.5922, -1650.8224, 67.6540>>
			details.entryAnim.cameraRot = <<-3.3824, -0.0000, 113.9872>>
			details.entryAnim.cameraFov = 37.8183
			details.entryAnim.syncScenePos = <<1455.009, -1652.797, 66.112>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 22.320>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<1459.1654, -1638.3335, 70.7996>>
			details.entryAnim.establishingCameraRot = <<-6.9346, -0.0000, 158.3002>>
			details.entryAnim.fEstablishingCameraFov = 47.2229
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2 // pass 1
			_INIT_GENERIC_FACTORY_WEED(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<105.3636, 174.3149, 105.0667>>
			details.entryAnim.cameraRot = <<-1.6998, -0.0000, 68.1162>>
			details.entryAnim.cameraFov = 29.4730
			details.entryAnim.syncScenePos = <<102.675, 176.192, 103.716>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -18.720>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<113.2985, 159.4081, 111.8330>>
			details.entryAnim.establishingCameraRot = <<-2.2889, 0.0155, 12.6091>>
			details.entryAnim.fEstablishingCameraFov = 59.9445
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2 // pass 1
			_INIT_GENERIC_FACTORY_CRACK(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-1459.9408, -378.7752, 39.5714>>
			details.entryAnim.cameraRot = <<-2.4580, -0.0180, 135.8305>>
			details.entryAnim.cameraFov = 16.3865
			details.entryAnim.syncScenePos = <<-1463.153, -381.244, 37.881>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 45.360>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-1450.2316, -383.0981, 43.3350>>
			details.entryAnim.establishingCameraRot = <<-8.0395, -0.0000, 86.8152>>
			details.entryAnim.fEstablishingCameraFov = 50.0000
			details.entryAnim.fCamShake = 0.15
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2 // pass 1
			_INIT_GENERIC_FACTORY_MONEY(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-1174.6447, -1383.4298, 5.4953>>
			details.entryAnim.cameraRot = <<-4.6114, -0.0000, -56.2161>>
			details.entryAnim.cameraFov = 27.2213
			details.entryAnim.syncScenePos = <<-1170.636, -1381.263, 3.971>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -146.520>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-1192.7997, -1378.1884, 12.4331>>
			details.entryAnim.establishingCameraRot = <<-2.6181, -0.0000, -91.6741>>
			details.entryAnim.fEstablishingCameraFov = 54.9572
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2 // pass 1
			_INIT_GENERIC_FACTORY_DOCUMENTS(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<302.1353, -754.5077, 29.8027>>
			details.entryAnim.cameraRot = <<-1.6792, -0.0393, 148.3184>>
			details.entryAnim.cameraFov = 16.2107
			details.entryAnim.syncScenePos = <<298.479, -759.137, 28.393>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 71.640>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<326.6581, -788.2607, 43.2081>>
			details.entryAnim.establishingCameraRot = <<1.1232, -0.0000, 45.8093>>
			details.entryAnim.fEstablishingCameraFov = 60.7032
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_3 // pass 1
			_INIT_GENERIC_FACTORY_METH(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<205.1200, 2463.7224, 56.3782>>
			details.entryAnim.cameraRot = <<-0.1022, 0.0000, 112.1192>>
			details.entryAnim.cameraFov = 35.4509
			details.entryAnim.syncScenePos = <<201.659, 2462.738, 54.911>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 21.240>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<219.5281, 2451.2712, 61.5476>>
			details.entryAnim.establishingCameraRot = <<-3.9251, -0.0000, 55.1454>>
			details.entryAnim.fEstablishingCameraFov = 54.4082
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3 // pass 1
			_INIT_GENERIC_FACTORY_WEED(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<2850.7344, 4444.5693, 49.1442>>
			details.entryAnim.cameraRot = <<-2.0858, 0.0143, 18.4628>>
			details.entryAnim.cameraFov = 13.3380
			details.entryAnim.syncScenePos = <<2849.022, 4450.232, 47.513>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -71.640>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<2841.4365, 4433.7847, 53.9355>>
			details.entryAnim.establishingCameraRot = <<-7.5539, -0.0693, -33.7803>>
			details.entryAnim.fEstablishingCameraFov = 54.4386
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3 // pass 1
			_INIT_GENERIC_FACTORY_CRACK(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<377.3802, 3587.1262, 33.7180>>
			details.entryAnim.cameraRot = <<-0.7974, -0.0177, -104.6024>>
			details.entryAnim.cameraFov = 7.7303
			details.entryAnim.fCamShake = 0.1
			details.entryAnim.syncScenePos = <<387.316, 3584.326, 32.292>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 168.120>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<376.6257, 3595.8706, 38.3613>>
			details.entryAnim.establishingCameraRot = <<-9.8226, -0.0000, -153.9853>>
			details.entryAnim.fEstablishingCameraFov = 57.3618
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3 // pass 1
			_INIT_GENERIC_FACTORY_MONEY(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<643.5591, 2786.0149, 42.6034>>
			details.entryAnim.cameraRot = <<-2.6917, 0.0192, 89.5651>>
			details.entryAnim.cameraFov = 16.9015
			details.entryAnim.syncScenePos = <<636.700, 2786.340, 41.007>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 5.400>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<647.7792, 2771.4409, 47.9368>>
			details.entryAnim.establishingCameraRot = <<-7.6509, 0.0000, 30.7713>>
			details.entryAnim.fEstablishingCameraFov = 46.6686
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3 // pass 1
			_INIT_GENERIC_FACTORY_DOCUMENTS(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1658.3204, 4846.6294, 42.6674>>
			details.entryAnim.cameraRot = <<-4.8756, -0.0000, 15.9561>>
			details.entryAnim.cameraFov = 18.0966
			details.entryAnim.syncScenePos = <<1656.189, 4851.706, 40.982>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -80.640>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<1666.5372, 4863.8452, 45.2812>>
			details.entryAnim.establishingCameraRot = <<-1.6989, 0.0463, 137.9170>>
			details.entryAnim.fEstablishingCameraFov = 54.4030
		BREAK
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_4 // pass 1
			_INIT_GENERIC_FACTORY_METH(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<1181.6787, -3117.1072, 6.4817>>
			details.entryAnim.cameraRot = <<-1.1632, -0.0000, -0.0485>>
			details.entryAnim.cameraFov = 36.1378
			details.entryAnim.syncScenePos = <<1182.073, -3113.992, 5.028>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -89.280>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<1168.4919, -3096.3433, 13.8698>>
			details.entryAnim.establishingCameraRot = <<-9.5100, -0.0000, -138.0643>>
			details.entryAnim.fEstablishingCameraFov = 56.6217
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4 // pass 1
			_INIT_GENERIC_FACTORY_WEED(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<139.9231, -2467.3250, 7.1163>>
			details.entryAnim.cameraRot = <<-5.3337, -0.0000, 143.5151>>
			details.entryAnim.cameraFov = 27.4251
			details.entryAnim.syncScenePos = <<136.286, -2472.120, 5.100>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 54.720>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<149.1082, -2470.1746, 9.8266>>
			details.entryAnim.establishingCameraRot = <<-6.4047, -0.0392, 93.1629>>
			details.entryAnim.fEstablishingCameraFov = 55.1036
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4 // pass 1
			_INIT_GENERIC_FACTORY_CRACK(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_r"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-252.4981, -2587.9854, 6.6929>>
			details.entryAnim.cameraRot = <<-5.4360, 0.0000, 173.3827>>
			details.entryAnim.cameraFov = 36.7304
			details.entryAnim.syncScenePos = <<-252.278, -2591.029, 5.001>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, 93.600>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-268.2111, -2573.3992, 10.5836>>
			details.entryAnim.establishingCameraRot = <<-5.2092, 0.0000, -121.5472>>
			details.entryAnim.fEstablishingCameraFov = 56.0837
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4 // pass 1
			_INIT_GENERIC_FACTORY_MONEY(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<671.8454, -2670.0000, 6.6823>>
			details.entryAnim.cameraRot = <<-0.1395, -0.0000, -1.6591>>
			details.entryAnim.cameraFov = 39.8350
			details.entryAnim.syncScenePos = <<672.404, -2667.674, 5.293>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -86.400>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<655.1423, -2667.5669, 10.4512>>
			details.entryAnim.establishingCameraRot = <<-3.5482, -0.0000, -89.9837>>
			details.entryAnim.fEstablishingCameraFov = 56.0817
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4 // pass 1
			_INIT_GENERIC_FACTORY_DOCUMENTS(details)
			details.entryAnim.dictionary = "anim@apt_trans@hinge_l"
			details.entryAnim.clip = "ext_player"
			details.entryAnim.cameraPos = <<-331.7401, -2781.0002, 8.1884>>
			details.entryAnim.cameraRot = <<-53.2973, -0.0000, -18.3202>>
			details.entryAnim.cameraFov = 43.6043
			details.entryAnim.syncScenePos = <<-330.313, -2779.079, 4.332>>
			details.entryAnim.syncSceneRot = <<0.0, 0.0, -86.400>>
			details.entryAnim.startingPhase = 0.200
			details.entryAnim.endingPhase = 0.460
			details.entryAnim.establishingCameraPos = <<-351.9045, -2762.3547, 7.6078>>
			details.entryAnim.establishingCameraRot = <<3.1248, -0.0000, -125.7847>>
			details.entryAnim.fEstablishingCameraFov = 55.3918
		BREAK
		//════════════════════════════════════════════════════════════════════
	ENDSWITCH	
ENDPROC

FUNC STRING GET_FACTORY_NAME(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			RETURN "BWH_METH"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			RETURN "BWH_WEED"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			RETURN "BWH_CRACK"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			RETURN "BWH_MONEY"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			RETURN "BWH_DOCUMENTS"
		BREAK
	ENDSWITCH	
	
	RETURN "MISSING"
ENDFUNC

FUNC STRING GET_FACTORY_GAME_TEXT(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_GAME_TEXT eTextID)
	SWITCH eTextID
		CASE SIGT_PIM_INVITE_TOOLTIP			RETURN "PIM_HINVPBWH"	BREAK
		CASE SIGT_PIM_INVITE_MENU_LABEL			RETURN "PIM_INVBWH"		BREAK
		CASE SIGT_PIM_INVITE_MENU_TITLE			RETURN "PIM_TITLEBWH"	BREAK
		CASE SIGT_PROPERTY_INVITE_TICKER		RETURN "PIM_INVABWH"	BREAK
		CASE SIGT_PROPERTY_INVITE_ALL_TICKER	RETURN ""				BREAK
		CASE SIGT_PROPERTY_INVITE_RECIPT
			SWITCH GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
				CASE FACTORY_TYPE_METH 			RETURN "CELL_BWHINV_1" BREAK
				CASE FACTORY_TYPE_CRACK 		RETURN "CELL_BWHINV_3" BREAK
				CASE FACTORY_TYPE_WEED 			RETURN "CELL_BWHINV_2" BREAK
				CASE FACTORY_TYPE_FAKE_IDS 		RETURN "CELL_BWHINV_5" BREAK
				CASE FACTORY_TYPE_FAKE_MONEY 	RETURN "CELL_BWHINV_4" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC BOOL SHOULD_FACTORY_SHOW_PIM_INVITE_OPTION(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &tlReturn, BOOL bReturnMenuLabel, BOOL &bSelectable)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(tlReturn)
	
	bSelectable = TRUE
	
	IF bReturnMenuLabel
		tlReturn = GET_FACTORY_GAME_TEXT(eSimpleInteriorID, SIGT_PIM_INVITE_MENU_LABEL)
	ELSE
		tlReturn = "ciPI_TYPE_INVITE_TO_SIMPLE_INTERIOR - FACTORY"
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR_OF_TYPE(SIMPLE_INTERIOR_TYPE_FACTORY)
	AND (IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS) OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_OWNER)) // Are we in interior as owner of it?
	AND GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
	AND NOT GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_FACTORY_INSIDE_BOUNDING_BOX(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bbox  , BOOL bUseSecondaryInteriorDetails )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			bbox.vInsideBBoxMin = <<992.9876, -3203.4023, -40.4781>>
			bbox.vInsideBBoxMax = <<1019.8875, -3192.7283, -32.2875>>
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			bbox.vInsideBBoxMin = <<1029.689, -3209.573, -40.485>>
			bbox.vInsideBBoxMax = <<1071.916, -3179.999, -33.487>>
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			bbox.vInsideBBoxMin = <<1084.857, -3200.893, -40.493>>
			bbox.vInsideBBoxMax = <<1104.715, -3182.607, -35.253>>
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			bbox.vInsideBBoxMin = <<1114.766, -3204.229, -41.563>>
			bbox.vInsideBBoxMax = <<1145.767, -3191.791, -32.698>>
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			bbox.vInsideBBoxMin = <<1155.0, -3200.515, -40.050>>
			bbox.vInsideBBoxMax = <<1176.790, -3189.532, -34.785>>
		BREAK
	ENDSWITCH	
ENDPROC

PROC GET_FACTORY_ENTRY_LOCATE_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT &iR, INT &iG, INT &iB, INT &iA)
	UNUSED_PARAMETER(eSimpleInteriorID)
	GET_HUD_COLOUR(HUD_COLOUR_BLUE,iR, iG, iB, iA)
ENDPROC

FUNC BOOL SHOULD_THIS_FACTORY_CORONA_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF IS_DEBUG_BUILD
	UNUSED_PARAMETER(bPrintReason)
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_FACTORY_ALLOW_ENTRY_IN_A_TUTORIAL_SESSION(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF SHOULD_ALLOW_BIKER_BUSINESS_ENTRY_DURING_MISSION()
		RETURN TRUE
	ENDIF
	
	RETURN IS_PLAYER_ON_MP_INTRO()
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)
	UNUSED_PARAMETER(eSimpleInteriorID)
	
	IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
	AND NOT GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN - Returning TRUE, IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY() = ", IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY(), " and player is not on resupply mission")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN - Returning TRUE, NETWORK_IS_ACTIVITY_SESSION() is TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.sGlobalController.iBS, SIMPLE_INTERIOR_CONTROL_BS_BLOCK_BIKER_WH_ENTRY)
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("[SIMPLE_INTERIOR][SMPL_H] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN TRUE SIMPLE_INTERIOR_CONTROL_BS_BLOCK_BIKER_WH_ENTRY is true")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	BOOL bMustHaveAccess = FALSE // when on resupply mission we must have access to this
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
	AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_ALLOW_BIKER_BUSINESS_ENTRY_DURING_MISSION()
	#ENDIF
		IF GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID())
			IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_BUY
				INT iPlayerID = NATIVE_TO_INT(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
				
				IF iPlayerID != -1
					ILLICIT_GOODS_MISSION_DATA missionData = GlobalplayerBD_FM_3[iPlayerID].sMagnateGangBossData.illicitGoodsMissionData
					IF GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID) != missionData.eMissionFactory
						#IF IS_DEBUG_BUILD
							IF bPrintReason
								CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN - Returning TRUE, We are on a buy mission and this isnt the business we're running it for.")
							ENDIF
						#ENDIF
						RETURN TRUE
					ELSE
						bMustHaveAccess = TRUE
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bPrintReason
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN - Returning TRUE, We are on biker mission.")
					ENDIF
				#ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT CAN_PLAYER_USE_PROPERTY(TRUE #IF IS_DEBUG_BUILD , bPrintReason #ENDIF)
	AND NOT bMustHaveAccess	
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN - Returning TRUE, CAN_PLAYER_USE_PROPERTY is FALSE and we dont have to have access.")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	BOOL bTempPrintReason = FALSE
	#IF IS_DEBUG_BUILD
	bTempPrintReason = bPrintReason
	#ENDIF
	
	IF IS_PLAYER_BLOCKED_FROM_PROPERTY(bTempPrintReason, PROPERTY_FACTORY)
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
	AND NOT IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT SHOULD_ALLOW_BIKER_BUSINESS_ENTRY_DURING_MISSION()
	#ENDIF
		#IF IS_DEBUG_BUILD
			IF bPrintReason
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN - Returning TRUE, IS_PLAYER_BLOCKED_FROM_PROPERTY = TRUE")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_ARMORY_AIRCRAFT_PILOT_ENTERING()
	OR HAS_PLAYER_STARTED_AVENGER_SEAT_SHUFFLE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN TRUE player is entering avenger from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_HACKER_TRUCK_PILOT_ENTERING()
	OR IS_PLAYER_STARTED_MOVE_TO_HACKER_TRUCK_FROM_CAB(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][FACTORY] SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN TRUE player is entering hacker truck from pilot seat")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_FACTORY(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID)
	UNUSED_PARAMETER(iEntranceID)
	
//	IF g_sMPTunables.bexec_disable_warehouse_entry  
//		RETURN FALSE
//	ENDIF
	
	FACTORY_ID eFactorID = GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
	
	IF playerID != INVALID_PLAYER_INDEX()
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(playerID)
			RETURN DOES_PLAYER_OWN_FACTORY(GB_GET_THIS_PLAYER_GANG_BOSS(playerID), eFactorID)
		ELSE
			IF DOES_PLAYER_OWN_FACTORY(playerID, eFactorID)
			AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_ENTER_FACTORY_IN_VEHICLE(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(eModel)
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_USE_FACTORY_ENTRY_IN_CURRENT_VEHICLE(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
	UNUSED_PARAMETER(bReturnSetExitCoords)
	UNUSED_PARAMETER(data)
	UNUSED_PARAMETER(details)
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Reurns true if we're on a mission for the biker businesses or the weapons bsiness
FUNC BOOL IS_PLAYER_ON_ANY_BUSINESS_RESUPPLY_MISSION()
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
	AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_BUY
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GUNRUNNING_BUY
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC STRING GET_FACTORY_REASON_FOR_BLOCKED_ENTRY(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)
	UNUSED_PARAMETER(iEntranceUsed)
	
	IF SHOULD_BLOCK_SIMPLE_INTERIOR_ENTRY_FOR_ON_CALL()
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_FACTORY_METH_1
			CASE SIMPLE_INTERIOR_FACTORY_METH_2
			CASE SIMPLE_INTERIOR_FACTORY_METH_3
			CASE SIMPLE_INTERIOR_FACTORY_METH_4
				RETURN "SI_ENTR_BLCK5A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_WEED_1
			CASE SIMPLE_INTERIOR_FACTORY_WEED_2
			CASE SIMPLE_INTERIOR_FACTORY_WEED_3
			CASE SIMPLE_INTERIOR_FACTORY_WEED_4
				RETURN "SI_ENTR_BLCK6A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
				RETURN "SI_ENTR_BLCK7A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
				RETURN "SI_ENTR_BLCK8A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
				RETURN "SI_ENTR_BLCK9A"
			BREAK
		ENDSWITCH
	ENDIF
	
	IF DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
			IF NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
					IF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
						// You're a CEO
						RETURN "BWH_MC_BLOCK_EXEb"
					ELSE
						// You're a VIP
						RETURN "BWH_MC_BLOCK_EXEa"
					ENDIF
				ELSE
					IF DOES_PLAYER_OWN_OFFICE(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()))
						// You're an Associate
						RETURN "BWH_MC_BLOCK_EXEd"
					ELSE
						// You're a Bodyguard
						RETURN "BWH_MC_BLOCK_EXEc"
					ENDIF
				ENDIF
			ELSE
				IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
				AND NOT DOES_PLAYER_OWN_FACTORY(GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID()), GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
					// You're a [role] (and can't access your factory)
					txtExtraString = GB_GET_PLAYER_BIKER_ROLE_TEXT_LABEL(PLAYER_ID())
					RETURN "BWH_MC_BLOCK_BKRa"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_ON_ANY_BUSINESS_RESUPPLY_MISSION()
		PLAYER_INDEX pBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
		
		IF pBoss != INVALID_PLAYER_INDEX()
			ILLICIT_GOODS_MISSION_DATA missionData = GlobalplayerBD_FM_3[NATIVE_TO_INT(pBoss)].sMagnateGangBossData.illicitGoodsMissionData
			
			IF GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID) != missionData.eMissionFactory
				IF HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(pBoss, missionData.eMissionFactory)
					RETURN "BWH_MC_BLOCK_BKRc"
				ELSE
					RETURN "BWH_MC_BLOCK_BKRb"
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF (DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	OR DOES_PLAYER_OWN_FACTORY(GB_GET_LOCAL_PLAYER_GANG_BOSS(), GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)))
		IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
			RETURN "JUG_BLOCK_BUSI"
		ENDIF
		
		IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
			// You cannot enter whilst delivering a bounty.
			RETURN "BOUNTY_PROP_BLCK"
		ENDIF
	ENDIF
	
	IF IS_NPC_IN_VEHICLE()
		// You cannot enter whilst an NPC is in the vehicle.
		RETURN "NPC_BLOCK"
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_FACTORY_REASON_FOR_BLOCKED_EXIT(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iExitUsed)
	UNUSED_PARAMETER(txtExtraString)
	UNUSED_PARAMETER(iExitUsed)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_BLOCKED_EXITING_PROPERTY_FOR_INTRO()
		RETURN "BWH_EXIT_BLCK"
	ENDIF
	#ENDIF
	
	IF SHOULD_JOB_ENTRY_TYPE_BLOCK_SIMPLE_INTERIOR_TRANSITION()
		SWITCH eSimpleInteriorID
			CASE SIMPLE_INTERIOR_FACTORY_METH_1
			CASE SIMPLE_INTERIOR_FACTORY_METH_2
			CASE SIMPLE_INTERIOR_FACTORY_METH_3
			CASE SIMPLE_INTERIOR_FACTORY_METH_4
				RETURN "SI_EXIT_BLCK5A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_WEED_1
			CASE SIMPLE_INTERIOR_FACTORY_WEED_2
			CASE SIMPLE_INTERIOR_FACTORY_WEED_3
			CASE SIMPLE_INTERIOR_FACTORY_WEED_4
				RETURN "SI_EXIT_BLCK6A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
			CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
				RETURN "SI_EXIT_BLCK7A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
			CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
				RETURN "SI_EXIT_BLCK8A"
			BREAK
			
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
			CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
				RETURN "SI_EXIT_BLCK9A"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC BOOL CAN_PLAYER_INVITE_OTHERS_TO_FACTORY(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerID)
	AND DOES_PLAYER_OWN_FACTORY(playerID, GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		// Block invites when on a resupply/sell missions
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
			IF GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID())
				RETURN FALSE
			ENDIF
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_BE_INVITED_TO_FACTORY(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(playerID, FALSE)
	AND DOES_PLAYER_OWN_FACTORY(GB_GET_THIS_PLAYER_GANG_BOSS(playerID), GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerID))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_LOCAL_PLAYER_BE_INVITED_TO_FACTORY_BY_PLAYER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(pInvitingPlayer)
	RETURN TRUE
ENDFUNC

PROC GET_FACTORY_ENTRANCE_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			txtTitle = "BIKER_WH_1"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			txtTitle = "BIKER_WH_2"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			txtTitle = "BIKER_WH_3"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			txtTitle = "BIKER_WH_4"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			txtTitle = "BIKER_WH_5"
		BREAK
		DEFAULT
			txtTitle = "MP_WHOUSE_EXIT"
		BREAK
	ENDSWITCH
	
	//txtTitle = GET_FACTORY_NAME_FROM_ID(GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
	
	strOptions[0] = "BWH_ENTRM_ALONE"
	eventOptions[0] = SI_EVENT_ENTRY_MENU_ENTER_ALONE
	strOptions[1] = "BWH_ENTRM_GANG"
	eventOptions[1] = SI_EVENT_ENTRY_MENU_ENTER_ALONE
	iOptionsCount = 2
ENDPROC

PROC _ASSIGN_PLACES_TO_BIKER_GANG_MEMBERS_ACCORDING_TO_RANK(PLAYER_INDEX &players[], INT iNumPlayers, PLAYER_INDEX &places[])
	GB_GANG_ROLE currentRole
	INT iCurrentPlace
	
	PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
	PLAYER_INDEX currentPlayer
	
	INT iRole, iPlayer
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR][AUTOWARP][FACTORY] _ASSIGN_PLACES_TO_BIKER_GANG_MEMBERS_ACCORDING_TO_RANK - Let's do this! We have ", iNumPlayers, " players to check.")
	#ENDIF
	
	REPEAT COUNT_OF(places) iPlayer
		places[iPlayer] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	REPEAT COUNT_OF(players) iPlayer
		// Check if boss is in the list
		IF playerBoss != INVALID_PLAYER_INDEX()
		AND players[iPlayer] = playerBoss
			places[iNumPlayers-1] = playerBoss
			iCurrentPlace += 1
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][AUTOWARP][FACTORY] _ASSIGN_PLACES_TO_BIKER_GANG_MEMBERS_ACCORDING_TO_RANK - Assigning place ", iNumPlayers-1, " to ", GET_PLAYER_NAME(playerBoss))
			#ENDIF
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	// Distribute roles
	REPEAT COUNT_OF(GB_GANG_ROLE) iRole
		IF iRole = COUNT_OF(GB_GANG_ROLE) - 1
			currentRole = GR_NONE
		ELSE
			currentRole = INT_TO_ENUM(GB_GANG_ROLE, iRole)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			STRING strRoleName
			SWITCH currentRole
				CASE GR_VP strRoleName = "GR_VP" BREAK
				CASE GR_ROAD_CAPTAIN strRoleName = "GR_ROAD_CAPTAIN" BREAK
				CASE GR_SERGEANT_AT_ARMS strRoleName = "GR_SERGEANT_AT_ARMS" BREAK
				CASE GR_ENFORCER strRoleName = "GR_ENFORCER" BREAK
				CASE GR_NONE strRoleName = "GR_NONE" BREAK
			ENDSWITCH
			
			PRINTLN("[SIMPLE_INTERIOR][AUTOWARP][FACTORY] _ASSIGN_PLACES_TO_BIKER_GANG_MEMBERS_ACCORDING_TO_RANK - Checking if anyone in gang is a ", strRoleName)
		#ENDIF
		
		REPEAT iNumPlayers iPlayer
			currentPlayer = players[iPlayer]
			
			IF currentPlayer != playerBoss
			AND GB_GET_PLAYER_GANG_ROLE(currentPlayer) = currentRole
				
				places[(iNumPlayers - 1) - iCurrentPlace] = currentPlayer
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][AUTOWARP][FACTORY] _ASSIGN_PLACES_TO_BIKER_GANG_MEMBERS_ACCORDING_TO_RANK - Assigning place ", ((iNumPlayers - 1) - iCurrentPlace), " to ", GET_PLAYER_NAME(currentPlayer))
				#ENDIF
				
				iCurrentPlace += 1
				
				IF iCurrentPlace = COUNT_OF(places) - 1
					BREAKLOOP
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iCurrentPlace = COUNT_OF(places) - 1
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL _GET_FACTORY_ENTER_ALL_SPAWN_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iSlot, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			SWITCH iSlot
				CASE 0
				    vSpawnPoint = <<-11.5218, -1.9631, 1.1069>>
				    fSpawnHeading = 358.8873
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-12.2195, -2.5881, 1.1069>>
				    fSpawnHeading = 353.2567
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-10.5988, -2.5735, 1.1069>>
				    fSpawnHeading = 9.3732
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-11.3124, -3.2012, 1.1069>>
				    fSpawnHeading = 3.7415
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-11.7697, -3.8721, 1.1069>>
				    fSpawnHeading = 353.1597
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-10.5363, -3.6855, 1.1069>>
				    fSpawnHeading = 15.0139
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-12.0202, -4.5505, 1.1069>>
				    fSpawnHeading = 353.1581
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-10.2822, -4.4854, 1.1069>>
				    fSpawnHeading = 9.3249
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			SWITCH iSlot
				CASE 0
				    vSpawnPoint = <<13.1410, 11.7766, -1.6643>>
				    fSpawnHeading = 180.2234
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<13.9980, 12.4968, -1.6639>>
				    fSpawnHeading = 178.8630
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<12.2629, 12.4233, -1.6640>>
				    fSpawnHeading = 178.9821
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<14.4146, 13.2847, -1.6634>>
				    fSpawnHeading = 170.3274
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<11.7417, 13.1440, -1.6642>>
				    fSpawnHeading = 189.7531
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<12.9265, 13.8608, -1.6636>>
				    fSpawnHeading = 179.7817
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<14.8522, 14.0400, -1.6630>>
				    fSpawnHeading = 173.3929
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<11.1221, 13.9626, -1.6646>>
				    fSpawnHeading = 185.4507
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			SWITCH iSlot
				CASE 0
				    vSpawnPoint = <<-4.8895, 5.0156, -1.4935>>
				    fSpawnHeading = 182.1600
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-4.3308, 5.9236, -1.4935>>
				    fSpawnHeading = 176.0453
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-5.5707, 5.8857, -1.4935>>
				    fSpawnHeading = 184.1912
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-3.9784, 6.5925, -1.4935>>
				    fSpawnHeading = 182.1447
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-5.8405, 6.6951, -1.4935>>
				    fSpawnHeading = 192.3965
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-3.6199, 7.2219, -1.4935>>
				    fSpawnHeading = 167.2676
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-6.3328, 7.4983, -1.4935>>
				    fSpawnHeading = 182.1600
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-4.9199, 7.6431, -1.4935>>
				    fSpawnHeading = 182.1600
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			SWITCH iSlot
				CASE 0
				    vSpawnPoint = <<10.9879, 0.4951, -2.1607>>
				    fSpawnHeading = 88.6978
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<11.6602, -0.0383, -2.1607>>
				    fSpawnHeading = 79.9876
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<11.8115, 1.7468, -2.9008>>
				    fSpawnHeading = 104.8679
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<12.9205, -1.2554, -2.1607>>
				    fSpawnHeading = 77.4126
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<12.5179, 2.3425, -2.8972>>
				    fSpawnHeading = 97.8991
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<13.4517, -1.9224, -2.1607>>
				    fSpawnHeading = 84.0452
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<12.4200, -0.3584, -2.1607>>
				    fSpawnHeading = 85.2243
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<13.5294, 1.8442, -2.8952>>
				    fSpawnHeading = 107.9368
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			SWITCH iSlot
				CASE 0
				    vSpawnPoint = <<5.9895, 0.0969, -1.8080>>
				    fSpawnHeading = 89.0200
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<6.3651, -0.6306, -1.8080>>
				    fSpawnHeading = 77.0064
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<6.4493, 0.9109, -1.8080>>
				    fSpawnHeading = 101.4561
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<6.9224, -1.1592, -1.8080>>
				    fSpawnHeading = 79.5705
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<7.0930, 1.5454, -1.8080>>
				    fSpawnHeading = 98.9787
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<7.9274, -0.9612, -1.8080>>
				    fSpawnHeading = 75.0502
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<7.9625, 1.1995, -1.8080>>
				    fSpawnHeading = 106.0817
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<8.0869, 0.1787, -1.8080>>
				    fSpawnHeading = 91.2861
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_FACTORY_ASYNC_SPAWN_GRID_INSIDE_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			RETURN GRID_SPAWN_LOCATION_FACTORY_CRACK_INSIDE
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			RETURN GRID_SPAWN_LOCATION_FACTORY_METH_INSIDE
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			RETURN GRID_SPAWN_LOCATION_FACTORY_WEED_INSIDE
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			RETURN GRID_SPAWN_LOCATION_FACTORY_FAKEID_INSIDE
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			RETURN GRID_SPAWN_LOCATION_FACTORY_CASH_INSIDE
		BREAK
	ENDSWITCH
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

FUNC VECTOR GET_FACTORY_TELEPORT_IN_SVM_COORD(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

PROC PROCESS_FACTORY_ENTRANCE_MENU(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)
	#IF IS_DEBUG_BUILD
		PRINTLN("[SIMPLE_INTERIOR][FACTORY] PROCESS_FACTORY_ENTRANCE_MENU - Running this...")
	#ENDIF
	
	IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
	AND CAN_PLAYER_INVITE_OTHERS_TO_FACTORY(PLAYER_ID(), eSimpleInteriorID)
		VECTOR vEntryLocate, vGoonPos
		GET_FACTORY_ENTRY_LOCATE(eSimpleInteriorID, vEntryLocate)
		vEntryLocate.Z = 0.0
		
		PLAYER_INDEX playerGoon
		INT iPlayersCount, i
			
		REPEAT GB_GET_NUM_GOONS_IN_LOCAL_GANG() i
			playerGoon = GB_GET_GANG_GOON_AT_INDEX(PLAYER_ID(), i)
			
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
			AND IS_NET_PLAYER_OK(playerGoon)
			AND NOT IS_PED_WEARING_JUGGERNAUT_SUIT(GET_PLAYER_PED(playerGoon))
			AND NOT GB_IS_GLOBAL_CLIENT_BIT1_SET(playerGoon, eGB_GLOBAL_CLIENT_BITSET_1_RIVAL_BUSINESS_PLAYER)
			AND NOT IS_PLAYER_AN_ANIMAL(playerGoon)
				vGoonPos = GET_PLAYER_COORDS(playerGoon)
				vGoonPos.Z = 0.0
				
				IF VDIST(vGoonPos, vEntryLocate) <= GET_SIMPLE_INTERIOR_INVITE_RADIUS(eSimpleInteriorID)
					iPlayersCount += 1
				ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][FACTORY] PROCESS_FACTORY_ENTRANCE_MENU - Gang members around count: ", iPlayersCount)
		#ENDIF
		
		IF iPlayersCount = 0
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
		ELSE
			SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, TRUE)
		ENDIF
		
		IF WAS_SIMPLE_INTERIOR_MENU_OPTION_ACCEPTED(1, menu)
			IF iPlayersCount > 0
				BROADCAST_PLAYERS_WARP_INSIDE_SIMPLE_INTERIOR(PLAYER_ID(), eSimpleInteriorID  , <<0,0,0>>  )
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[SIMPLE_INTERIOR][FACTORY] PROCESS_FACTORY_ENTRANCE_MENU - Not sending broadcast as there's no goons around entrance.")
				#ENDIF
			ENDIF
			
			g_SimpleInteriorData.eCurrentInteriorID = SIMPLE_INTERIOR_INVALID // We need to set this manually so DO_SIMPLE_INTERIOR_AUTOWARP will work, otherwise it will return TRUE straight away as it checks if local player is in the interior, it's a bit of a hack but a really small, harmless one.
					
			SET_SIMPLE_INTERIOR_EVENT_AUTOWARP_ACTIVE(TRUE, eSimpleInteriorID)
			g_SimpleInteriorData.bEventAutowarpOverrideCoords = FALSE
			g_SimpleInteriorData.bEventAutowarpSetInInterior = TRUE
		ENDIF
	ELSE
		SET_SIMPLE_INTERIOR_MENU_OPTION_VISIBILITY(menu, 1, FALSE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_FACTORY(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	IF g_SimpleInteriorData.iAccessBS = 0
		// How did we get here? Probably script relaunched somehow, kick us out
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
	AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
		// No access to factories if you're CEO/VIP/Bodyguard/Associate
		RETURN TRUE
	ENDIF
	
	// Kick out players who entered as goons and stopped being goons while inside.
	// Even if they own a warehouse they need to reenter their own after they stopped being a goon.
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_GOON)
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// If we walked in our own business as boss or owner but quit and got hired as a goon - kick us out
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// For warehouses, since there are no invites, not being able to walk in is equivalent to not being supposed to be inside
	RETURN NOT CAN_PLAYER_ENTER_FACTORY(PLAYER_ID(), eSimpleInteriorID, 0)
ENDFUNC

/*
[PI_BIK_MCP]
Motorcycle Club President

[PI_BIK_MCV]
Motorcycle Club Vice President

[PI_BIK_MCR]
Motorcycle Club Road Captain

[PI_BIK_MCS]
Motorcycle Club Sergeant-at-Arms

[PI_BIK_MCM]
Motorcycle Club Prospect

[PI_BIK_MCE]
Motorcycle Club Enforcer
*/

FUNC STRING GET_FACTORY_KICK_OUT_REASON(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	UNUSED_PARAMETER(txtPlayerNameString)
	
	IF g_SimpleInteriorData.bExitAllTriggeredByOwner
		g_SimpleInteriorData.bExitAllTriggeredByOwner = FALSE
		RETURN "MP_FACTORY_KICKe" // Owner has requested all to leave Business.
	ENDIF
	
	IF g_SimpleInteriorData.iAccessBS = 0
		RETURN "MP_FACTORY_KICKc" // You don't have access to this Business
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_GOON)
		IF NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			/*
			// Depending on which role we had in MC show the reason for kicking out
			IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_VICE_PRESIDENT)
				txtExtraString = "PI_BIK_MCV"
			ELIF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_ROAD_CAPTAIN)
				txtExtraString = "PI_BIK_MCR"
			ELIF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_SERGEANT)
				txtExtraString = "PI_BIK_MCS"
			ELIF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_ENFORCER)
				txtExtraString = "PI_BIK_MCE"
			ELSE
				txtExtraString = "PI_BIK_MCM"
			ENDIF
			*/
			RETURN "MP_FACTORY_KICKb" // You're not longer a member of a MC club.
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS)
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
		OR (GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE) AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE))
			RETURN "MP_FACTORY_KICKa" // You're not longer a Motorcycle Club President
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS)
	OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_OWNER)
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
			txtExtraString = GB_GET_PLAYER_BIKER_ROLE_TEXT_LABEL(PLAYER_ID())
			RETURN "MP_FACTORY_KICKd" // Your Business is unavailable when working as a [ROLE] for an MC.
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC
 
PROC SET_FACTORY_ACCESS_BS(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iInvitingPlayer)
	
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX()
	globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS = 0
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS)
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_PRESIDENT)
		ELSE
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_GOON)
			SWITCH GB_GET_PLAYER_GANG_ROLE(PLAYER_ID())
				CASE GR_NONE				SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_PROSPECT) BREAK
				CASE GR_VP					SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_VICE_PRESIDENT) BREAK
				CASE GR_ROAD_CAPTAIN		SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_ROAD_CAPTAIN) BREAK
				CASE GR_SERGEANT_AT_ARMS	SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_SERGEANT) BREAK
				CASE GR_ENFORCER			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_ENFORCER) BREAK
			ENDSWITCH
		ENDIF
		
		// Create flags for everyone who potentially we would like to join in the warehouse
		PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
		PLAYER_INDEX playerGoon
		
		IF playerBoss != INVALID_PLAYER_INDEX() 
		AND playerBoss != PLAYER_ID() 
			SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerBoss))
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - We want to join boss: ", GET_PLAYER_NAME(playerBoss), " (", NATIVE_TO_INT(playerBoss), ")")
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - Check if there are any goons to join, how many of them are: ", GB_GET_NUM_GOONS_IN_LOCAL_GANG())
		#ENDIF
		
		/*
		INT i
		REPEAT GB_GET_NUM_GOONS_IN_LOCAL_GANG() i
			playerGoon = GB_GET_GANG_GOON_AT_INDEX(playerBoss, i)
		
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
				SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerGoon))
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - We want to join goon: ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ")")
				#ENDIF
			ENDIF
		ENDREPEAT
		*/
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			playerGoon = INT_TO_PLAYERINDEX(i)
			
			IF playerGoon != INVALID_PLAYER_INDEX()
			AND playerGoon != PLAYER_ID()
			AND playerGoon != playerBoss
				IF IS_NET_PLAYER_OK(playerGoon)
					IF GB_IS_PLAYER_MEMBER_OF_THIS_BIKER_GANG(playerGoon, playerBoss)
						SET_BIT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS, NATIVE_TO_INT(playerGoon))
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - We want to join goon: ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ")")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - Player ", GET_PLAYER_NAME(playerGoon), " (", NATIVE_TO_INT(playerGoon), ") isn't part of our gang.")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - Player ", NATIVE_TO_INT(playerGoon), " is not ok.")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF playerGoon = playerBoss
						CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - Skipping boss.")
					ENDIF
					
					CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - Skipping us or invalid player index (", NATIVE_TO_INT(playerGoon), ")")
				#ENDIF
			ENDIF
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - Players we should join BS: ", globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iPlayersToJoinBS)
		#ENDIF
	ELSE
		IF DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] SET_FACTORY_ACCESS_BS - We are entering this factory as in owner.")
			#ENDIF
			SET_BIT(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_OWNER)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL GET_FACTORY_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle  , BOOL bUseSecondaryInteriorDetails = FALSE )
	UNUSED_PARAMETER(bUseSecondaryInteriorDetails)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(bSpawnInVehicle)

	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-11.5018, -3.7710, 1.1068>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-11.1996, -4.8818, 1.1068>>
				    fSpawnHeading = 0.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-11.9667, 0.9038, 0.1068>>
				    fSpawnHeading = 271.4400
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-8.3604, 0.5366, -1.4932>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-6.5717, 0.5698, -1.4932>>
				    fSpawnHeading = 270.3600
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-6.5812, -1.5540, -1.4932>>
				    fSpawnHeading = 266.0400
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-4.9359, 0.5994, -1.4932>>
				    fSpawnHeading = 267.8400
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-5.4576, -2.2556, -1.4932>>
				    fSpawnHeading = 267.1200
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-2.1296, -3.4199, -1.4932>>
				    fSpawnHeading = 271.4400
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-3.3931, 0.6641, -1.4932>>
				    fSpawnHeading = 268.2000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-1.6493, 0.6008, -1.4932>>
				    fSpawnHeading = 271.0800
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<-0.5912, -3.4470, -1.4932>>
				    fSpawnHeading = 269.2800
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<-0.1370, 0.7095, -1.4932>>
				    fSpawnHeading = 268.9200
				    RETURN TRUE
				BREAK
			ENDSWITCH				
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<15.8730, 13.3074, -1.6635>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<15.7094, 14.8108, -1.6626>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<15.2938, 11.9883, -1.6642>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<14.5515, 13.0491, -1.6634>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<14.4570, 14.3352, -1.6628>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<13.9866, 11.9121, -1.6643>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<12.7440, 13.7371, -1.6637>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<12.6460, 12.4270, -1.6638>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<12.5011, 11.0618, -1.6648>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<11.5200, 14.1372, -1.6645>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<11.4950, 12.7761, -1.6645>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<11.2906, 11.1682, -1.6647>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH			
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<-4.8052, 8.3174, -1.4935>>
				    fSpawnHeading = 180.0000
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<-5.0101, 7.2205, -1.4935>>
				    fSpawnHeading = 178.2258
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<-4.9868, 5.7422, -1.4935>>
				    fSpawnHeading = 181.7104
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<-5.0342, 3.6167, -1.4935>>
				    fSpawnHeading = 255.7617
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<-4.1292, 3.5083, -1.4935>>
				    fSpawnHeading = 260.0012
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<-5.9457, 2.7358, -1.4935>>
				    fSpawnHeading = 183.2430
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<-5.9900, 0.7227, -1.4935>>
				    fSpawnHeading = 226.1079
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<-5.9446, -1.4463, -1.4935>>
				    fSpawnHeading = 271.2191
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<-4.1721, -1.4917, -1.4935>>
				    fSpawnHeading = 268.1750
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<-1.9153, -1.5291, -1.4935>>
				    fSpawnHeading = 268.2025
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-1.8743, 3.0684, -1.4935>>
				    fSpawnHeading = 264.3273
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<13.2156, -1.9326, -2.1607>>
				    fSpawnHeading = 357.8796
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<12.1537, -0.2056, -2.1607>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<13.3557, 1.7976, -2.8962>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<12.0094, 1.9473, -2.8996>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<10.7483, -0.1853, -2.1607>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<10.3102, 1.7913, -2.9005>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<9.3082, -0.1772, -2.1607>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<5.1066, 1.8057, -2.9004>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<8.5474, 2.2549, -2.8977>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<7.9846, -0.1001, -2.1607>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<6.8645, 0.0144, -2.1607>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 11
				    vSpawnPoint = <<5.4856, -0.0378, -2.1607>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 12
				    vSpawnPoint = <<3.1495, 1.7048, -2.9010>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH			
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			SWITCH iSpawnPoint
				CASE 0
				    vSpawnPoint = <<8.1375, 0.0410, -1.8080>>
				    fSpawnHeading = 90.5126
				    RETURN TRUE
				BREAK
				CASE 1
				    vSpawnPoint = <<7.4933, -1.6084, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 2
				    vSpawnPoint = <<7.8275, 1.4553, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 3
				    vSpawnPoint = <<6.1877, -1.5391, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 4
				    vSpawnPoint = <<6.6771, 1.4951, -1.8080>>
				    fSpawnHeading = 87.6196
				    RETURN TRUE
				BREAK
				CASE 5
				    vSpawnPoint = <<4.8105, 1.9807, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 6
				    vSpawnPoint = <<3.0282, -1.5840, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 7
				    vSpawnPoint = <<2.5529, 1.9456, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 8
				    vSpawnPoint = <<0.3383, 2.2065, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 9
				    vSpawnPoint = <<0.8707, -2.1997, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
				CASE 10
				    vSpawnPoint = <<-1.0348, 2.1890, -1.8080>>
				    fSpawnHeading = 90.0000
				    RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS GET_FACTORY_CUSTOM_INTERIOR_WARP_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS eParams
	RETURN eParams
ENDFUNC


FUNC BOOL GET_FACTORY_OUTSIDE_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
	UNUSED_PARAMETER(bSpawnInVehicle)
	SWITCH eSimpleInteriorID
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<52.3880, 6339.6826, 30.3747>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<50.1165, 6337.8135, 30.3840>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<54.0991, 6340.2085, 30.3764>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<51.8118, 6341.7832, 30.2274>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<49.7982, 6340.1450, 30.2268>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<47.4055, 6338.7554, 30.2268>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<47.6714, 6340.6216, 30.2282>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<49.0308, 6342.4697, 30.2290>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<51.3454, 6344.1646, 30.2295>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<44.6926, 6338.9736, 30.2282>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<46.1017, 6335.0874, 30.3673>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<44.0519, 6336.8765, 30.2269>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<48.0841, 6336.1460, 30.3725>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<39.6178, 6338.0117, 30.2306>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<44.4416, 6342.6157, 30.2314>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<45.2101, 6345.0391, 30.2329>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<34.4321, 6336.6206, 30.2325>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<48.1278, 6344.5889, 30.2312>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<41.9482, 6340.1011, 30.2309>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<44.3521, 6333.9849, 30.3641>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<41.7460, 6336.9834, 30.2284>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<36.4916, 6335.4063, 30.2301>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<34.1285, 6334.0835, 30.2302>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<38.9609, 6341.6431, 30.2340>>
					fSpawnHeading = 29.8800
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<47.4673, 6351.2676, 30.2368>>
					fSpawnHeading = 29.3628
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<42.0324, 6348.3335, 30.2377>>
					fSpawnHeading = 31.1542
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<42.5469, 6353.7471, 30.2398>>
					fSpawnHeading = 31.3772
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<44.7897, 6349.8628, 30.2372>>
					fSpawnHeading = 29.4159
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<39.3478, 6346.8848, 30.2382>>
					fSpawnHeading = 27.0972
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<40.0027, 6349.9302, 30.2398>>
					fSpawnHeading = 30.9198
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<42.4919, 6351.1226, 30.2397>>
					fSpawnHeading = 24.4507
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<44.7628, 6353.0190, 30.2398>>
					fSpawnHeading = 29.3117
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<418.0156, 6520.5854, 26.7163>>
					fSpawnHeading = 262.7964
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<417.1376, 6518.7583, 26.6879>>
					fSpawnHeading = 263.9654
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<417.9884, 6523.1240, 26.7372>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<419.6446, 6523.7095, 26.7372>>
					fSpawnHeading = 263.9875
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<416.5636, 6516.7383, 26.6779>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<417.7757, 6525.2188, 26.7372>>
					fSpawnHeading = 264.3846
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<417.9008, 6527.2847, 26.7276>>
					fSpawnHeading = 261.9809
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<416.3594, 6514.4063, 26.7113>>
					fSpawnHeading = 260.7525
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<419.2861, 6525.8906, 26.7306>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<416.8679, 6513.0601, 26.7013>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<418.0652, 6512.1226, 26.6896>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<416.2570, 6508.1362, 26.7295>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<431.1201, 6526.1196, 26.7967>>
					fSpawnHeading = 255.1485
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<418.6536, 6532.1743, 26.7355>>
					fSpawnHeading = 262.3797
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<418.7130, 6510.0635, 26.7268>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<431.0089, 6513.5483, 27.2204>>
					fSpawnHeading = 244.2593
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<432.3376, 6515.7090, 27.3426>>
					fSpawnHeading = 257.6375
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<419.1826, 6507.2871, 26.7564>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<430.5198, 6527.8955, 26.7518>>
					fSpawnHeading = 262.7752
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<421.3478, 6504.8086, 26.7940>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<421.5699, 6508.4053, 26.7834>>
					fSpawnHeading = 264.1678
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<433.3055, 6526.4678, 26.8483>>
					fSpawnHeading = 256.7643
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<418.1842, 6529.8174, 26.7083>>
					fSpawnHeading = 264.4763
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<430.1514, 6516.1694, 27.2395>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<419.2933, 6534.4771, 26.7172>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<423.4145, 6506.7998, 26.7148>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<419.1189, 6536.7764, 26.7062>>
					fSpawnHeading = 266.4000
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<433.2409, 6511.6206, 27.3255>>
					fSpawnHeading = 255.6953
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<429.1526, 6524.4312, 26.7881>>
					fSpawnHeading = 251.0102
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<431.9196, 6523.9292, 26.9157>>
					fSpawnHeading = 254.5240
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<430.4885, 6511.4116, 27.1360>>
					fSpawnHeading = 266.0406
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<429.3159, 6510.0195, 26.9990>>
					fSpawnHeading = 265.8243
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<52.8118, 6487.2690, 30.4277>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<54.1165, 6486.1128, 30.4375>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<51.2639, 6489.2886, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<55.2892, 6484.6538, 30.4396>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<49.5078, 6490.4287, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<56.8959, 6483.3662, 30.4337>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<47.6963, 6491.7715, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<57.9225, 6481.8506, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<46.8138, 6493.4858, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<59.4133, 6480.6621, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<45.5240, 6495.4414, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<60.7123, 6479.0361, 30.4253>>
					fSpawnHeading = 314.8000
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<64.8422, 6477.9917, 30.4036>>
					fSpawnHeading = 222.6826
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<62.6797, 6477.9824, 30.4253>>
					fSpawnHeading = 224.6725
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<62.4855, 6475.4448, 30.4253>>
					fSpawnHeading = 224.7985
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<66.4212, 6476.0186, 30.4253>>
					fSpawnHeading = 224.5524
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<64.1202, 6473.0068, 30.4253>>
					fSpawnHeading = 224.6458
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<59.7233, 6473.5010, 30.4253>>
					fSpawnHeading = 222.6271
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<57.7166, 6471.5757, 30.4253>>
					fSpawnHeading = 226.4074
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<58.7126, 6469.3682, 30.4253>>
					fSpawnHeading = 222.7943
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<61.6103, 6471.2056, 30.4253>>
					fSpawnHeading = 225.0653
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<55.6494, 6469.5015, 30.4253>>
					fSpawnHeading = 226.0485
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<53.1460, 6466.7197, 30.4253>>
					fSpawnHeading = 224.9646
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<56.6075, 6466.8013, 30.4253>>
					fSpawnHeading = 225.9164
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<55.9495, 6459.0947, 30.3096>>
					fSpawnHeading = 228.6592
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<52.8952, 6459.5337, 30.3907>>
					fSpawnHeading = 222.7136
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<54.2541, 6464.7407, 30.4253>>
					fSpawnHeading = 223.3415
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<52.6959, 6462.0996, 30.4253>>
					fSpawnHeading = 220.9931
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<49.7192, 6462.7891, 30.4253>>
					fSpawnHeading = 222.8229
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<55.9560, 6461.7515, 30.3767>>
					fSpawnHeading = 222.0971
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<58.3004, 6461.1455, 30.3021>>
					fSpawnHeading = 223.8293
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<57.7972, 6464.8481, 30.3991>>
					fSpawnHeading = 225.3850
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<-412.4298, 6173.2163, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<-413.8248, 6175.0703, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<-410.8973, 6171.7603, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<-411.1946, 6174.4844, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<-412.5322, 6176.3979, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<-409.6077, 6172.7627, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<-409.3319, 6174.6328, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<-411.1761, 6176.6177, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<-406.7141, 6168.1016, 30.4883>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<-408.5953, 6170.1294, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<-404.8738, 6170.2480, 30.4822>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<-403.1390, 6168.0771, 30.5356>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<-401.1759, 6164.8672, 30.4927>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<-404.1890, 6165.7119, 30.5369>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<-414.0832, 6181.1353, 30.4741>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<-412.0959, 6179.1323, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<-411.8097, 6181.1406, 30.4755>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<-412.8681, 6182.4917, 30.4661>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<-410.0663, 6179.7847, 30.4782>>
					fSpawnHeading = 311.3212
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<-397.1739, 6162.2192, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<-407.1871, 6172.5723, 30.4782>>
					fSpawnHeading = 315.3600
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<-396.5909, 6156.8091, 30.4781>>
					fSpawnHeading = 314.7660
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<-398.2342, 6158.7642, 30.4782>>
					fSpawnHeading = 314.7355
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<-395.8160, 6159.3438, 30.4782>>
					fSpawnHeading = 315.2863
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<-402.5583, 6170.3638, 30.5126>>
					fSpawnHeading = 314.6432
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<-399.8426, 6166.3647, 30.4975>>
					fSpawnHeading = 314.5009
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<-402.4862, 6172.5420, 30.5286>>
					fSpawnHeading = 316.1572
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<-398.1165, 6166.7261, 30.4913>>
					fSpawnHeading = 317.5430
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<-399.0388, 6168.9028, 30.5240>>
					fSpawnHeading = 316.4660
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<-400.4666, 6170.7310, 30.5366>>
					fSpawnHeading = 315.4282
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<-398.9958, 6171.1108, 30.5404>>
					fSpawnHeading = 315.2344
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<-397.4542, 6169.5688, 30.5168>>
					fSpawnHeading = 316.3151
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<-162.7138, 6335.4224, 30.5808>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<-161.2702, 6334.0381, 30.5808>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<-164.1573, 6336.8066, 30.5626>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<-159.8267, 6332.6538, 30.5808>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<-166.2008, 6338.7910, 30.5626>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<-158.3832, 6331.2695, 30.5808>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<-168.2722, 6340.8813, 30.5716>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<-156.9397, 6329.8853, 30.5808>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<-169.9879, 6342.4590, 30.5709>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<-155.4961, 6328.5010, 30.5770>>
					fSpawnHeading = 316.2000
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<-153.8149, 6326.9722, 30.5826>>
					fSpawnHeading = 312.3233
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<-152.4129, 6325.5576, 30.5920>>
					fSpawnHeading = 316.8614
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<-151.1120, 6324.2188, 30.5903>>
					fSpawnHeading = 313.0286
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<-158.1136, 6333.0815, 30.5808>>
					fSpawnHeading = 314.6399
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<-156.4287, 6331.4663, 30.5786>>
					fSpawnHeading = 313.9750
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<-153.7700, 6328.6934, 30.5647>>
					fSpawnHeading = 314.0734
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<-152.0941, 6327.2710, 30.5874>>
					fSpawnHeading = 314.2164
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<-168.2417, 6342.5391, 30.5417>>
					fSpawnHeading = 314.5307
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<-170.3239, 6344.0850, 30.4945>>
					fSpawnHeading = 314.5820
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<-164.5036, 6339.2261, 30.5626>>
					fSpawnHeading = 314.6957
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<-150.6437, 6325.6318, 30.5976>>
					fSpawnHeading = 314.7346
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<-149.4043, 6322.1553, 30.5716>>
					fSpawnHeading = 318.0889
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<-147.9739, 6321.7354, 30.5777>>
					fSpawnHeading = 314.7053
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<-151.1501, 6346.4644, 30.4964>>
					fSpawnHeading = 314.7262
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<-149.6130, 6344.9404, 30.4950>>
					fSpawnHeading = 314.7279
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<-148.0372, 6343.4966, 30.4983>>
					fSpawnHeading = 317.5560
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<-149.0959, 6347.4063, 30.5813>>
					fSpawnHeading = 314.9833
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<-147.2622, 6345.8052, 30.5732>>
					fSpawnHeading = 314.9387
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<-145.3534, 6344.0059, 30.5673>>
					fSpawnHeading = 321.8870
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<-145.9914, 6347.8535, 30.4908>>
					fSpawnHeading = 315.0452
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<-143.9613, 6345.5649, 30.4904>>
					fSpawnHeading = 319.5480
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<-147.8938, 6350.0288, 30.4906>>
					fSpawnHeading = 317.3596
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<1452.7185, -1652.5444, 65.2531>>
					fSpawnHeading = 98.6400
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<1451.3326, -1652.8336, 64.9894>>
					fSpawnHeading = 98.6400
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<1449.8157, -1653.0917, 64.8421>>
					fSpawnHeading = 98.8630
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<1452.2804, -1651.0598, 65.1814>>
					fSpawnHeading = 10.2482
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<1451.0249, -1651.3706, 64.9410>>
					fSpawnHeading = 12.1956
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<1456.2262, -1650.3683, 65.9188>>
					fSpawnHeading = 18.2891
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<1456.0203, -1649.1068, 65.9818>>
					fSpawnHeading = 18.7175
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<1454.0175, -1650.2985, 65.5237>>
					fSpawnHeading = 15.4597
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<1447.5638, -1650.1991, 64.2262>>
					fSpawnHeading = 71.9062
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<1446.0696, -1651.0072, 63.9286>>
					fSpawnHeading = 66.5926
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<1446.2316, -1649.5006, 63.9114>>
					fSpawnHeading = 43.6699
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<1446.4379, -1647.7697, 63.9483>>
					fSpawnHeading = 37.6348
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<1457.1556, -1645.7904, 66.3903>>
					fSpawnHeading = 23.0193
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<1458.7860, -1645.7250, 66.5759>>
					fSpawnHeading = 20.6023
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<1442.8823, -1651.7251, 63.3199>>
					fSpawnHeading = 83.5109
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<1446.1055, -1646.1302, 63.8255>>
					fSpawnHeading = 8.6400
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<1445.7734, -1644.3842, 63.7196>>
					fSpawnHeading = 15.5516
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<1458.6008, -1644.5585, 66.6900>>
					fSpawnHeading = 22.6359
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<1440.6805, -1652.5479, 62.9604>>
					fSpawnHeading = 83.0081
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<1459.5315, -1640.5502, 67.0141>>
					fSpawnHeading = 25.0443
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<1457.6359, -1641.7124, 66.5973>>
					fSpawnHeading = 22.3606
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<1457.7731, -1639.6628, 66.7683>>
					fSpawnHeading = 24.7135
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<1459.7672, -1638.2806, 67.2359>>
					fSpawnHeading = 22.7005
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<1455.6122, -1640.7814, 66.2886>>
					fSpawnHeading = 27.0112
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<1461.8242, -1639.3231, 67.5361>>
					fSpawnHeading = 24.0778
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<1461.5769, -1641.6654, 67.3225>>
					fSpawnHeading = 27.3296
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<1448.7515, -1637.3088, 64.8495>>
					fSpawnHeading = 17.9649
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<1446.1721, -1638.3405, 63.9024>>
					fSpawnHeading = 25.5326
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<1446.7759, -1636.3187, 64.1259>>
					fSpawnHeading = 20.7213
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<1443.1543, -1638.1781, 62.7794>>
					fSpawnHeading = 21.0717
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<1443.6226, -1636.0566, 62.9685>>
					fSpawnHeading = 21.0154
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<1440.2510, -1636.5936, 61.9508>>
					fSpawnHeading = 19.6392
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<101.5573, 173.0707, 103.5805>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<99.9892, 172.3697, 103.5692>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<102.7886, 171.4459, 103.5708>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<98.2435, 171.3624, 103.5405>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<104.9318, 170.9161, 103.5910>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<100.6276, 169.9560, 103.5367>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<96.9907, 173.0015, 103.5679>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<103.1269, 169.1342, 103.5354>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<95.1155, 171.9109, 103.5497>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<105.8590, 167.5653, 103.5429>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<99.1158, 167.4283, 103.5265>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<96.9644, 170.1569, 103.5273>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<103.0710, 166.3546, 103.5148>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<95.0259, 168.7767, 103.5041>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<106.9227, 165.0326, 103.5053>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<93.1757, 171.0423, 103.5259>>
					fSpawnHeading = 160.9200
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<108.6100, 169.3952, 103.6018>>
					fSpawnHeading = 159.1532
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<109.1702, 167.1929, 103.5540>>
					fSpawnHeading = 161.1016
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<112.4778, 166.7766, 103.5676>>
					fSpawnHeading = 157.8632
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<114.8762, 165.8928, 103.5475>>
					fSpawnHeading = 157.9706
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<113.3156, 164.4082, 103.5190>>
					fSpawnHeading = 159.3387
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<110.8967, 165.1313, 103.5308>>
					fSpawnHeading = 157.2200
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<115.8458, 163.4454, 103.5133>>
					fSpawnHeading = 163.3644
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<117.1773, 165.1345, 103.5553>>
					fSpawnHeading = 160.3933
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<105.6983, 160.9647, 103.5570>>
					fSpawnHeading = 157.5239
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<106.9749, 159.3869, 103.5674>>
					fSpawnHeading = 156.8706
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<106.3755, 162.8573, 103.5192>>
					fSpawnHeading = 158.1008
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<103.5319, 160.5350, 103.5865>>
					fSpawnHeading = 154.9452
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<104.8679, 158.5220, 103.6038>>
					fSpawnHeading = 154.5520
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<102.2848, 158.1931, 103.6371>>
					fSpawnHeading = 156.7726
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<106.0865, 156.3622, 103.6289>>
					fSpawnHeading = 156.8518
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<103.6108, 155.3809, 103.6755>>
					fSpawnHeading = 154.5688
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<-1461.3162, -383.0153, 37.7268>>
					fSpawnHeading = 225.3600
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<-1463.0905, -385.0027, 37.7036>>
					fSpawnHeading = 225.3600
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<-1459.5386, -381.5134, 37.7274>>
					fSpawnHeading = 228.1590
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<-1464.8990, -386.8547, 37.6880>>
					fSpawnHeading = 214.9657
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<-1457.1495, -379.1012, 37.7661>>
					fSpawnHeading = 235.8638
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<-1467.1108, -388.6677, 37.6901>>
					fSpawnHeading = 215.2533
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<-1459.7130, -385.0891, 37.5678>>
					fSpawnHeading = 230.9921
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<-1461.6987, -386.8681, 37.5623>>
					fSpawnHeading = 225.3600
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<-1457.5929, -383.0095, 37.4900>>
					fSpawnHeading = 237.4360
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<-1463.3572, -388.8349, 37.5195>>
					fSpawnHeading = 216.8518
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<-1455.4445, -380.7431, 37.6032>>
					fSpawnHeading = 235.9487
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<-1465.9160, -390.8907, 37.5321>>
					fSpawnHeading = 211.4971
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<-1457.4329, -386.4548, 37.3415>>
					fSpawnHeading = 218.5163
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<-1459.8727, -388.7091, 37.2991>>
					fSpawnHeading = 225.3600
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<-1454.9542, -383.9600, 37.4050>>
					fSpawnHeading = 239.7925
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<-1463.2766, -391.4753, 37.2504>>
					fSpawnHeading = 214.9716
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<-1452.9032, -382.2585, 37.4333>>
					fSpawnHeading = 237.7314
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<-1453.0367, -380.0698, 37.5405>>
					fSpawnHeading = 253.6929
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<-1451.6577, -378.0233, 37.5819>>
					fSpawnHeading = 269.3310
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<-1449.1438, -378.8390, 37.4439>>
					fSpawnHeading = 274.8042
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<-1447.0380, -377.6040, 37.4190>>
					fSpawnHeading = 268.6497
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<-1444.7014, -377.2707, 37.3457>>
					fSpawnHeading = 273.1105
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<-1450.9470, -381.1693, 37.4067>>
					fSpawnHeading = 272.5679
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<-1448.9479, -381.5771, 37.2669>>
					fSpawnHeading = 276.7779
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<-1446.9623, -380.8906, 37.2120>>
					fSpawnHeading = 270.8792
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<-1444.1615, -379.8249, 37.2053>>
					fSpawnHeading = 276.6370
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<-1469.5153, -391.0861, 37.6714>>
					fSpawnHeading = 137.0645
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<-1471.1643, -392.9117, 37.6507>>
					fSpawnHeading = 139.6607
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<-1473.0573, -394.7888, 37.4064>>
					fSpawnHeading = 133.1182
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<-1466.6475, -393.5308, 37.3348>>
					fSpawnHeading = 138.4943
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<-1468.7009, -395.0844, 37.3885>>
					fSpawnHeading = 139.4162
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<-1471.2952, -396.2152, 37.2593>>
					fSpawnHeading = 141.8664
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<-1171.8049, -1379.7600, 3.9672>>
					fSpawnHeading = 28.8000
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<-1173.8381, -1380.1514, 3.9449>>
					fSpawnHeading = 34.8391
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<-1175.0405, -1381.7244, 3.9085>>
					fSpawnHeading = 34.0448
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<-1176.4154, -1380.9800, 3.8931>>
					fSpawnHeading = 35.2419
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<-1177.8647, -1379.6335, 3.8829>>
					fSpawnHeading = 37.6581
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<-1173.2845, -1377.5790, 3.9650>>
					fSpawnHeading = 31.6187
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<-1175.5381, -1378.4746, 3.9343>>
					fSpawnHeading = 36.0438
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<-1177.2937, -1383.3969, 3.8535>>
					fSpawnHeading = 35.2433
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<-1179.2518, -1381.5870, 3.8390>>
					fSpawnHeading = 28.8000
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<-1181.6676, -1382.9659, 3.7598>>
					fSpawnHeading = 32.4180
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<-1174.4043, -1375.1339, 3.9656>>
					fSpawnHeading = 28.8000
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<-1176.5933, -1376.3735, 3.9386>>
					fSpawnHeading = 35.2438
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<-1178.5952, -1377.7871, 3.8900>>
					fSpawnHeading = 28.8000
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<-1181.0061, -1378.9824, 3.8361>>
					fSpawnHeading = 46.9055
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<-1183.3859, -1380.7770, 3.7525>>
					fSpawnHeading = 45.7085
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<-1175.8496, -1372.5419, 3.9618>>
					fSpawnHeading = 34.0330
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<-1178.0365, -1374.1194, 3.9377>>
					fSpawnHeading = 36.0408
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<-1180.4052, -1375.3499, 3.8842>>
					fSpawnHeading = 50.9507
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<-1183.3258, -1378.0282, 3.7992>>
					fSpawnHeading = 48.1209
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<-1185.8840, -1376.6923, 3.7520>>
					fSpawnHeading = 25.1088
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<-1188.0238, -1377.7202, 3.6766>>
					fSpawnHeading = 27.3524
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<-1186.9401, -1375.0372, 3.7478>>
					fSpawnHeading = 25.1048
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<-1188.9371, -1375.9404, 3.6766>>
					fSpawnHeading = 25.3466
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<-1183.0876, -1375.3632, 3.8374>>
					fSpawnHeading = 77.3209
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<-1186.3021, -1380.4865, 3.6784>>
					fSpawnHeading = 35.8389
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<-1184.5077, -1383.3792, 3.6760>>
					fSpawnHeading = 41.7869
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<-1176.6271, -1389.7535, 3.7946>>
					fSpawnHeading = 178.9759
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<-1178.7598, -1387.7458, 3.7692>>
					fSpawnHeading = 169.5914
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<-1174.5060, -1387.7423, 3.8554>>
					fSpawnHeading = 177.9477
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<-1176.6792, -1386.1193, 3.8363>>
					fSpawnHeading = 176.9071
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<-1179.8574, -1384.3727, 3.7852>>
					fSpawnHeading = 31.5586
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<-1182.7021, -1385.3102, 3.6949>>
					fSpawnHeading = 32.5507
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<300.8729, -760.0483, 28.3163>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<301.6440, -761.9394, 28.3147>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<302.4963, -759.5029, 28.3146>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<303.1685, -761.8504, 28.3129>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<303.9147, -758.1395, 28.3135>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<304.0247, -759.9157, 28.3126>>
					fSpawnHeading = 252.2312
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<301.4713, -768.2782, 28.3135>>
					fSpawnHeading = 251.8442
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<299.8669, -768.9089, 28.3140>>
					fSpawnHeading = 251.4297
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<305.3434, -753.3583, 28.3635>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<300.4586, -770.4727, 28.3138>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<305.8705, -751.4041, 28.3331>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<299.1807, -771.0784, 28.3140>>
					fSpawnHeading = 249.8400
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<304.6257, -755.8135, 28.3591>>
					fSpawnHeading = 250.0491
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<305.9816, -754.7924, 28.3666>>
					fSpawnHeading = 252.1873
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<305.3565, -757.1442, 28.3391>>
					fSpawnHeading = 252.1794
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<299.5625, -772.7739, 28.3129>>
					fSpawnHeading = 250.2665
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<298.1895, -773.2670, 28.3143>>
					fSpawnHeading = 250.9776
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<306.5619, -749.6814, 28.3606>>
					fSpawnHeading = 249.6350
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<296.4447, -776.8649, 28.3150>>
					fSpawnHeading = 252.7087
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<298.3273, -775.4174, 28.3133>>
					fSpawnHeading = 252.8287
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<296.6829, -779.3920, 28.3137>>
					fSpawnHeading = 252.3917
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<305.3011, -746.4330, 28.3706>>
					fSpawnHeading = 249.7762
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<306.2274, -743.6250, 28.3274>>
					fSpawnHeading = 250.7082
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<307.8594, -745.6110, 28.3304>>
					fSpawnHeading = 250.6613
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<331.7643, -776.8928, 28.2729>>
					fSpawnHeading = 250.5341
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<333.6688, -777.0743, 28.2705>>
					fSpawnHeading = 253.4692
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<332.6280, -774.4262, 28.2728>>
					fSpawnHeading = 250.8592
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<333.5020, -771.8655, 28.2728>>
					fSpawnHeading = 250.0925
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<334.3142, -769.5931, 28.2727>>
					fSpawnHeading = 251.0598
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<334.5899, -774.5726, 28.2704>>
					fSpawnHeading = 252.1623
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<335.3888, -771.9819, 28.3202>>
					fSpawnHeading = 252.3170
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<336.2341, -769.4737, 28.3241>>
					fSpawnHeading = 252.4850
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<202.4384, 2460.5598, 54.7008>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<200.3708, 2459.5686, 54.6941>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<204.7798, 2461.4360, 54.7382>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<197.7557, 2458.8074, 54.6885>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<209.0087, 2462.0645, 54.9950>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<195.4143, 2457.9312, 54.6845>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<203.0973, 2458.1167, 54.9177>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<200.5146, 2457.3582, 54.8638>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<205.6361, 2459.0344, 55.0299>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<197.6831, 2456.4976, 54.8015>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<207.8435, 2459.9148, 55.1618>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<193.8811, 2455.4937, 54.7618>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<204.2549, 2455.9072, 55.4142>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<201.4144, 2454.8870, 55.3609>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<206.5325, 2456.7534, 55.4914>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<198.5998, 2454.4338, 55.1941>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<189.0531, 2456.2178, 54.7092>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<191.8886, 2457.3240, 54.7090>>
					fSpawnHeading = 200.5200
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<205.7044, 2454.3455, 55.8587>>
					fSpawnHeading = 199.8020
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<208.7682, 2448.1240, 56.7242>>
					fSpawnHeading = 176.6295
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<208.8698, 2445.9407, 57.0429>>
					fSpawnHeading = 183.8303
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<208.9210, 2443.5483, 57.4307>>
					fSpawnHeading = 179.1214
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<210.8099, 2446.0769, 57.0055>>
					fSpawnHeading = 179.0752
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<210.5056, 2443.5793, 57.3852>>
					fSpawnHeading = 183.8073
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<210.0111, 2441.0833, 57.7527>>
					fSpawnHeading = 182.8844
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<221.9835, 2454.8267, 55.5680>>
					fSpawnHeading = 220.7026
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<223.9979, 2453.9063, 55.5964>>
					fSpawnHeading = 227.0621
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<225.4343, 2451.7673, 55.7987>>
					fSpawnHeading = 222.8667
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<220.6236, 2451.9204, 55.8987>>
					fSpawnHeading = 225.7103
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<222.4546, 2451.0691, 55.9536>>
					fSpawnHeading = 218.0795
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<221.9027, 2447.9817, 56.3041>>
					fSpawnHeading = 217.9536
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<224.5778, 2449.2268, 56.1253>>
					fSpawnHeading = 219.4924
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<2847.0112, 4449.6055, 47.5172>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<2846.2197, 4451.6738, 47.5096>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<2847.6211, 4447.3804, 47.5290>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<2845.6160, 4453.8213, 47.5014>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<2848.1943, 4445.3770, 47.5537>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<2844.9060, 4457.2524, 47.4885>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<2849.1777, 4442.1816, 47.5871>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<2844.7034, 4448.7573, 47.5378>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<2843.8250, 4451.3745, 47.5253>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<2845.3855, 4445.7285, 47.5674>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<2843.1406, 4454.1313, 47.5126>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<2846.3740, 4443.3687, 47.5935>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<2842.4263, 4456.4443, 47.5007>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<2847.0564, 4441.0967, 47.6161>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<2842.0955, 4447.4697, 47.6063>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<2841.0254, 4450.2705, 47.5853>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<2842.6960, 4444.9790, 47.6291>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<2840.1470, 4452.8525, 47.5646>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<2843.5439, 4442.5444, 47.6490>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<2839.5708, 4456.1079, 47.5255>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<2844.4165, 4439.5303, 47.6648>>
					fSpawnHeading = 106.5600
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<2831.7793, 4448.9492, 47.5968>>
					fSpawnHeading = 105.5845
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<2829.4160, 4450.4116, 47.5575>>
					fSpawnHeading = 105.9704
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<2827.8447, 4448.2720, 47.5520>>
					fSpawnHeading = 104.7238
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<2832.6821, 4445.7544, 47.6294>>
					fSpawnHeading = 108.1589
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<2830.6321, 4446.0649, 47.6127>>
					fSpawnHeading = 103.9698
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<2827.9944, 4445.0469, 47.5886>>
					fSpawnHeading = 106.2889
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<2833.5562, 4442.9590, 47.6539>>
					fSpawnHeading = 106.5517
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<2832.1812, 4441.6631, 47.6564>>
					fSpawnHeading = 107.0118
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<2829.9927, 4442.2529, 47.6317>>
					fSpawnHeading = 105.3059
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<2834.3186, 4440.6011, 47.6694>>
					fSpawnHeading = 104.0414
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<2831.1025, 4451.9385, 47.5644>>
					fSpawnHeading = 102.0362
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<387.7582, 3586.3020, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<389.9273, 3586.0181, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<385.6169, 3586.8301, 32.2922>>
					fSpawnHeading = 349.6432
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<392.1504, 3585.5520, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<382.9557, 3587.4890, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<395.2194, 3585.0696, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<379.7780, 3587.2498, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<388.1639, 3588.2671, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<390.6552, 3588.2063, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<385.3867, 3589.3289, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<393.0392, 3587.8396, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<382.7778, 3589.2771, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<395.8860, 3587.4500, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<380.7386, 3589.3772, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<399.8047, 3585.2661, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<397.9850, 3586.3530, 32.2922>>
					fSpawnHeading = 349.6487
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<382.2252, 3591.3364, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<393.1277, 3589.6753, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<377.7317, 3589.6973, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<397.7258, 3584.7195, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<377.1271, 3587.7136, 32.2922>>
					fSpawnHeading = 348.8400
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<370.6880, 3590.4458, 32.3428>>
					fSpawnHeading = 80.5106
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<368.9077, 3591.2227, 32.3838>>
					fSpawnHeading = 79.9337
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<366.2635, 3591.0620, 32.3747>>
					fSpawnHeading = 79.9504
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<368.8419, 3589.0500, 32.3662>>
					fSpawnHeading = 98.6671
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<366.4835, 3589.1135, 32.3813>>
					fSpawnHeading = 99.6186
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<367.3917, 3587.0667, 32.3844>>
					fSpawnHeading = 103.9887
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<399.0821, 3582.8306, 32.2922>>
					fSpawnHeading = 274.5721
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<397.9359, 3580.6184, 32.2922>>
					fSpawnHeading = 270.0849
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<399.5165, 3578.1614, 32.2922>>
					fSpawnHeading = 277.0565
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<397.8492, 3576.0811, 32.2922>>
					fSpawnHeading = 271.5672
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<400.1925, 3574.5994, 32.2922>>
					fSpawnHeading = 268.6003
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<636.7584, 2783.9006, 41.0148>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<638.9577, 2784.1306, 41.0030>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<635.1780, 2783.4351, 41.0234>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<641.8049, 2784.3726, 40.9789>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<636.0968, 2781.4229, 41.0241>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<638.8647, 2782.2505, 41.0040>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<641.2390, 2782.8125, 40.9850>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<642.4758, 2780.2344, 40.9586>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<635.9471, 2778.6875, 41.0091>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<638.3111, 2778.3667, 40.9891>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<639.8527, 2779.9014, 40.9842>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<642.0136, 2778.2632, 40.9502>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<636.3226, 2775.7134, 40.9935>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<638.4607, 2775.8386, 40.9691>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<640.7181, 2776.2500, 40.9585>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<643.4160, 2776.1787, 40.9373>>
					fSpawnHeading = 183.6000
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<642.2759, 2774.2808, 40.9994>>
					fSpawnHeading = 183.1321
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<637.4537, 2773.9592, 40.9926>>
					fSpawnHeading = 181.1902
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<656.7316, 2779.7605, 40.0253>>
					fSpawnHeading = 253.9082
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<658.9734, 2779.8499, 39.8517>>
					fSpawnHeading = 247.4577
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<661.2719, 2778.2942, 39.7406>>
					fSpawnHeading = 255.4666
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<657.0626, 2782.8896, 40.0158>>
					fSpawnHeading = 257.8912
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<659.4348, 2783.2190, 39.9377>>
					fSpawnHeading = 255.5625
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<661.8536, 2781.6013, 39.7951>>
					fSpawnHeading = 256.4716
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<655.9114, 2776.7207, 40.1675>>
					fSpawnHeading = 246.7690
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<658.2940, 2775.6553, 39.9362>>
					fSpawnHeading = 250.3359
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<649.8104, 2758.1133, 40.9581>>
					fSpawnHeading = 202.1911
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<646.8809, 2756.0715, 40.9894>>
					fSpawnHeading = 202.1923
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<649.9106, 2755.4998, 40.9632>>
					fSpawnHeading = 207.6483
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<647.5277, 2754.0090, 40.9975>>
					fSpawnHeading = 205.1322
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<650.2255, 2753.0000, 40.9791>>
					fSpawnHeading = 202.3729
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<648.0262, 2751.4441, 41.0162>>
					fSpawnHeading = 202.3873
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<1658.1498, 4851.8130, 40.9708>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<1658.2759, 4849.6509, 40.9736>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<1657.7581, 4853.2603, 40.9753>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<1658.9893, 4847.9023, 40.9602>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<1657.3250, 4856.1152, 40.9769>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<1658.9271, 4846.2437, 40.9731>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<1658.2393, 4854.7388, 40.9618>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<1659.0740, 4844.2788, 40.9916>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<1656.9421, 4859.1548, 40.9766>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<1660.0034, 4843.2251, 41.0006>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<1656.7218, 4861.2012, 40.9912>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<1659.1724, 4841.8623, 41.0267>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<1656.6008, 4864.0107, 40.9918>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<1660.0692, 4840.6611, 41.0395>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<1655.7034, 4865.6646, 40.9903>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<1659.6428, 4838.2466, 41.0387>>
					fSpawnHeading = 278.6400
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<1660.1400, 4832.9961, 41.0229>>
					fSpawnHeading = 275.2459
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<1659.7942, 4835.7520, 41.0141>>
					fSpawnHeading = 278.0196
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<1661.9414, 4833.7764, 41.1119>>
					fSpawnHeading = 277.7381
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<1661.5492, 4836.4346, 41.0244>>
					fSpawnHeading = 274.7514
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<1661.4890, 4838.7588, 41.0411>>
					fSpawnHeading = 278.8962
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<1655.3151, 4867.2715, 41.0049>>
					fSpawnHeading = 273.6084
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<1657.1460, 4867.4609, 41.0091>>
					fSpawnHeading = 273.1697
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<1677.7770, 4856.4692, 41.0667>>
					fSpawnHeading = 280.4979
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<1679.3457, 4856.7578, 41.0701>>
					fSpawnHeading = 279.4770
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<1678.1720, 4853.4082, 41.0605>>
					fSpawnHeading = 278.9321
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<1679.7213, 4853.6387, 41.0629>>
					fSpawnHeading = 279.0437
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<1678.8356, 4851.4995, 41.0594>>
					fSpawnHeading = 279.2171
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<1680.6602, 4851.4912, 41.0620>>
					fSpawnHeading = 275.0026
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<1679.0885, 4848.1743, 41.0677>>
					fSpawnHeading = 277.5333
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<1681.1964, 4848.4141, 41.1096>>
					fSpawnHeading = 277.1605
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<1679.2844, 4845.8818, 41.0752>>
					fSpawnHeading = 277.2397
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<1180.0082, -3113.9414, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<1180.3280, -3115.9104, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<1180.1664, -3111.9028, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<1178.8307, -3112.8477, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<1180.2092, -3117.8694, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<1179.1328, -3118.8381, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<1178.5935, -3116.6780, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<1178.3228, -3114.9678, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<1179.4896, -3110.0662, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<1177.1792, -3118.6201, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<1176.9032, -3116.3516, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<1175.8611, -3117.5105, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<1178.6616, -3108.7195, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<1173.3188, -3101.8167, 4.8710>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<1177.5500, -3111.4370, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<1173.2094, -3105.2827, 4.8668>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<1175.9191, -3106.4863, 5.0280>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<1173.2261, -3103.6631, 4.8676>>
					fSpawnHeading = 89.6400
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<1159.6451, -3121.6357, 4.9002>>
					fSpawnHeading = 97.9578
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<1157.7162, -3121.7283, 4.9002>>
					fSpawnHeading = 97.0644
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<1158.5684, -3123.6917, 4.8998>>
					fSpawnHeading = 122.4240
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<1157.5101, -3125.9746, 4.8998>>
					fSpawnHeading = 107.9816
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<1155.8958, -3123.9521, 4.8998>>
					fSpawnHeading = 105.6486
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<1154.7740, -3126.9988, 4.8998>>
					fSpawnHeading = 103.6211
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<1153.3929, -3124.7776, 4.8998>>
					fSpawnHeading = 101.0198
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<1158.2881, -3102.8779, 4.9902>>
					fSpawnHeading = 77.9229
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<1158.5170, -3100.2991, 4.9546>>
					fSpawnHeading = 79.1152
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<1156.1711, -3103.6616, 4.9685>>
					fSpawnHeading = 75.0904
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<1156.3712, -3100.1926, 4.9431>>
					fSpawnHeading = 74.7444
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<1153.7767, -3103.0740, 4.9603>>
					fSpawnHeading = 73.6500
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<1153.4404, -3099.4587, 4.8484>>
					fSpawnHeading = 75.6482
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<1151.6317, -3100.8479, 4.8632>>
					fSpawnHeading = 79.6286
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			SWITCH iSpawnPoint
				CASE 0
		            vSpawnPoint = <<138.0663, -2473.5037, 5.0000>>
		            fSpawnHeading = 234.3600
		            RETURN TRUE
		        BREAK
				CASE 1
					vSpawnPoint = <<136.3854, -2475.2454, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<139.1002, -2471.5854, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<138.6979, -2476.1013, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<140.4441, -2473.8120, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<137.2626, -2478.6067, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<141.9690, -2473.2188, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<140.0273, -2478.0518, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<141.3555, -2475.9922, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<139.7461, -2480.4663, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<142.5122, -2478.5981, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<135.1875, -2480.3855, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<143.2373, -2476.1521, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<132.9111, -2480.1458, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<134.4167, -2478.1096, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<140.5661, -2468.9004, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<142.0223, -2482.4106, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<145.7824, -2475.5723, 5.0000>>
					fSpawnHeading = 234.3600
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<145.0857, -2481.1458, 5.0000>>
					fSpawnHeading = 241.0637
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<149.7415, -2483.4270, 4.9946>>
					fSpawnHeading = 233.5104
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<151.0793, -2481.5125, 4.9918>>
					fSpawnHeading = 237.4624
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<150.2780, -2485.4160, 4.9934>>
					fSpawnHeading = 231.6367
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<151.9404, -2483.2317, 4.9899>>
					fSpawnHeading = 236.0939
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<152.8993, -2479.9565, 4.9879>>
					fSpawnHeading = 237.5987
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<133.5043, -2488.9224, 5.0000>>
					fSpawnHeading = 188.3722
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<129.2173, -2487.8945, 5.0000>>
					fSpawnHeading = 190.1083
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<131.1706, -2489.5662, 5.0000>>
					fSpawnHeading = 190.6198
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<128.6361, -2490.3357, 5.0000>>
					fSpawnHeading = 189.1775
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<131.2855, -2486.5398, 5.0000>>
					fSpawnHeading = 178.5667
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<164.5286, -2503.1067, 4.9877>>
					fSpawnHeading = 210.2661
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<166.7715, -2502.8213, 4.9877>>
					fSpawnHeading = 222.8792
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<166.6136, -2500.6565, 4.9877>>
					fSpawnHeading = 224.8715
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<-254.7741, -2591.2373, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<-254.2472, -2589.1731, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<-255.9361, -2589.7996, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<-256.4702, -2591.9253, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<-255.5893, -2587.7700, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<-257.3896, -2588.7129, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<-254.1175, -2586.8376, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<-258.2581, -2591.5681, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<-257.0626, -2586.2024, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<-254.7318, -2583.7014, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<-256.1104, -2581.6621, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<-256.0175, -2578.7498, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<-260.2751, -2592.1135, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<-259.7165, -2588.4631, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<-260.1514, -2584.9094, 5.0006>>
					fSpawnHeading = 87.0548
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<-258.7704, -2582.3511, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<-259.4825, -2577.2759, 5.0006>>
					fSpawnHeading = 85.0350
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<-256.7663, -2576.3308, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<-262.3983, -2590.6543, 5.0006>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<-262.6156, -2586.5264, 5.0006>>
					fSpawnHeading = 89.4713
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<-262.3634, -2579.5923, 5.0006>>
					fSpawnHeading = 86.8199
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<-261.7182, -2574.8611, 5.0006>>
					fSpawnHeading = 86.2061
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<-267.5555, -2577.9304, 5.0006>>
					fSpawnHeading = 83.4103
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<-267.5865, -2581.3816, 5.0006>>
					fSpawnHeading = 87.6697
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<-267.6170, -2584.8901, 5.0006>>
					fSpawnHeading = 87.6113
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<-269.2955, -2579.7805, 5.0006>>
					fSpawnHeading = 85.1179
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<-269.4418, -2583.5278, 5.0006>>
					fSpawnHeading = 91.6965
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<-269.4679, -2587.3049, 5.0006>>
					fSpawnHeading = 85.5674
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<-267.6761, -2588.5771, 5.0006>>
					fSpawnHeading = 93.8343
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<-267.8046, -2592.2075, 5.0006>>
					fSpawnHeading = 87.5751
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<-269.5222, -2591.3660, 5.0006>>
					fSpawnHeading = 81.9741
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<-268.9472, -2576.6003, 5.0006>>
					fSpawnHeading = 75.5813
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			SWITCH iSpawnPoint
				CASE 0
					vSpawnPoint = <<669.7007, -2667.4734, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 1
					vSpawnPoint = <<669.2310, -2665.4839, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 2
					vSpawnPoint = <<669.0834, -2669.4011, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 3
					vSpawnPoint = <<668.0314, -2663.9448, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 4
					vSpawnPoint = <<667.9921, -2671.4863, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 5
					vSpawnPoint = <<666.2173, -2670.2532, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 6
					vSpawnPoint = <<666.6612, -2667.4443, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 7
					vSpawnPoint = <<665.7930, -2664.5662, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 8
					vSpawnPoint = <<661.1834, -2676.7456, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 9
					vSpawnPoint = <<666.8834, -2661.5078, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 10
					vSpawnPoint = <<668.1364, -2674.1563, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 11
					vSpawnPoint = <<665.6526, -2659.5574, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 12
					vSpawnPoint = <<663.9605, -2667.4214, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 13
					vSpawnPoint = <<662.5350, -2664.5090, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 14
					vSpawnPoint = <<664.0994, -2672.0190, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 15
					vSpawnPoint = <<664.0226, -2661.6252, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 16
					vSpawnPoint = <<664.3822, -2675.4182, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 17
					vSpawnPoint = <<662.1806, -2658.6191, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 18
					vSpawnPoint = <<660.0225, -2667.8733, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 19
					vSpawnPoint = <<660.0655, -2660.8867, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 20
					vSpawnPoint = <<661.6735, -2670.7854, 5.0812>>
					fSpawnHeading = 91.0800
					RETURN TRUE
				BREAK
				CASE 21
					vSpawnPoint = <<656.7050, -2676.6733, 5.0812>>
					fSpawnHeading = 100.8788
					RETURN TRUE
				BREAK
				CASE 22
					vSpawnPoint = <<656.0792, -2673.9600, 5.0812>>
					fSpawnHeading = 99.2975
					RETURN TRUE
				BREAK
				CASE 23
					vSpawnPoint = <<655.5352, -2670.8987, 5.0812>>
					fSpawnHeading = 99.1658
					RETURN TRUE
				BREAK
				CASE 24
					vSpawnPoint = <<653.8568, -2676.4343, 5.0812>>
					fSpawnHeading = 97.2009
					RETURN TRUE
				BREAK
				CASE 25
					vSpawnPoint = <<653.2253, -2672.9873, 5.0812>>
					fSpawnHeading = 97.3717
					RETURN TRUE
				BREAK
				CASE 26
					vSpawnPoint = <<652.6971, -2669.8225, 5.0813>>
					fSpawnHeading = 97.5774
					RETURN TRUE
				BREAK
				CASE 27
					vSpawnPoint = <<654.7110, -2667.6570, 5.0812>>
					fSpawnHeading = 95.4195
					RETURN TRUE
				BREAK
				CASE 28
					vSpawnPoint = <<650.9651, -2662.2874, 5.0812>>
					fSpawnHeading = 83.2365
					RETURN TRUE
				BREAK
				CASE 29
					vSpawnPoint = <<649.9758, -2659.7100, 5.0812>>
					fSpawnHeading = 86.6450
					RETURN TRUE
				BREAK
				CASE 30
					vSpawnPoint = <<648.4595, -2662.9434, 5.0812>>
					fSpawnHeading = 83.3265
					RETURN TRUE
				BREAK
				CASE 31
					vSpawnPoint = <<647.6133, -2660.0991, 5.0812>>
					fSpawnHeading = 83.2693
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
		    SWITCH iSpawnPoint
		        CASE 0
		            vSpawnPoint = <<-334.7377, -2778.9688, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 1
		            vSpawnPoint = <<-332.9008, -2777.1187, 4.1404>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 2
		            vSpawnPoint = <<-332.8945, -2780.2756, 4.1404>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 3
		            vSpawnPoint = <<-335.8614, -2781.0002, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 4
		            vSpawnPoint = <<-332.6457, -2783.3877, 4.1419>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 5
		            vSpawnPoint = <<-332.4783, -2773.8413, 4.1427>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 6
		            vSpawnPoint = <<-336.0079, -2776.7393, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 7
		            vSpawnPoint = <<-337.6867, -2778.8811, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 8
		            vSpawnPoint = <<-334.9099, -2773.8020, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 9
		            vSpawnPoint = <<-335.4647, -2783.4446, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 10
		            vSpawnPoint = <<-337.2510, -2774.9919, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 11
		            vSpawnPoint = <<-337.6528, -2782.3647, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 12
		            vSpawnPoint = <<-334.2122, -2771.2732, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 13
		            vSpawnPoint = <<-335.3730, -2786.2329, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 14
		            vSpawnPoint = <<-339.5327, -2779.6787, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 15
		            vSpawnPoint = <<-340.4634, -2776.1567, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 16
		            vSpawnPoint = <<-349.4742, -2775.4648, 4.2003>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 17
		            vSpawnPoint = <<-338.7712, -2771.9490, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 18
		            vSpawnPoint = <<-348.5268, -2773.7664, 4.2003>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 19
		            vSpawnPoint = <<-338.1402, -2768.3640, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 20
		            vSpawnPoint = <<-338.4236, -2786.4333, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 21
		            vSpawnPoint = <<-335.0887, -2768.6594, 4.0002>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 22
		            vSpawnPoint = <<-347.0583, -2771.7302, 4.2003>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 23
		            vSpawnPoint = <<-332.9932, -2787.7168, 4.1432>>
		            fSpawnHeading = 89.6400
		            RETURN TRUE
		        BREAK
		        CASE 24
		            vSpawnPoint = <<-342.6335, -2762.5144, 4.6528>>
		            fSpawnHeading = 49.6019
		            RETURN TRUE
		        BREAK
		        CASE 25
		            vSpawnPoint = <<-343.9058, -2764.6228, 4.5421>>
		            fSpawnHeading = 51.5582
		            RETURN TRUE
		        BREAK
		        CASE 26
		            vSpawnPoint = <<-346.0114, -2766.9387, 4.5172>>
		            fSpawnHeading = 47.8237
		            RETURN TRUE
		        BREAK
		        CASE 27
		            vSpawnPoint = <<-347.4135, -2765.1406, 4.9457>>
		            fSpawnHeading = 44.1680
		            RETURN TRUE
		        BREAK
		        CASE 28
		            vSpawnPoint = <<-345.9332, -2762.9285, 5.0394>>
		            fSpawnHeading = 44.4418
		            RETURN TRUE
		        BREAK
		        CASE 29
		            vSpawnPoint = <<-344.9213, -2760.7815, 5.0370>>
		            fSpawnHeading = 44.0803
		            RETURN TRUE
		        BREAK
		        CASE 30
		            vSpawnPoint = <<-349.1890, -2763.7935, 5.0415>>
		            fSpawnHeading = 43.7607
		            RETURN TRUE
		        BREAK
		        CASE 31
		            vSpawnPoint = <<-347.8954, -2761.3315, 5.0404>>
		            fSpawnHeading = 43.9496
		            RETURN TRUE
		        BREAK
		    ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC ASYNC_GRID_SPAWN_LOCATIONS GET_FACTORY_ASYNC_SPAWN_GRID_LOCATION(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
	
		CASE SIMPLE_INTERIOR_FACTORY_METH_1			RETURN GRID_SPAWN_LOCATION_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1		RETURN GRID_SPAWN_LOCATION_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1			RETURN GRID_SPAWN_LOCATION_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1	RETURN GRID_SPAWN_LOCATION_FACTORY_FAKEID_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1		RETURN GRID_SPAWN_LOCATION_FACTORY_CASH_1
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_2			RETURN GRID_SPAWN_LOCATION_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2		RETURN GRID_SPAWN_LOCATION_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2			RETURN GRID_SPAWN_LOCATION_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2	RETURN GRID_SPAWN_LOCATION_FACTORY_FAKEID_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2		RETURN GRID_SPAWN_LOCATION_FACTORY_CASH_2
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_3			RETURN GRID_SPAWN_LOCATION_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3		RETURN GRID_SPAWN_LOCATION_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3			RETURN GRID_SPAWN_LOCATION_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3	RETURN GRID_SPAWN_LOCATION_FACTORY_FAKEID_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3		RETURN GRID_SPAWN_LOCATION_FACTORY_CASH_3
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_4			RETURN GRID_SPAWN_LOCATION_FACTORY_METH_4
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4		RETURN GRID_SPAWN_LOCATION_FACTORY_CRACK_4
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4			RETURN GRID_SPAWN_LOCATION_FACTORY_WEED_4
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4	RETURN GRID_SPAWN_LOCATION_FACTORY_FAKEID_4
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4		RETURN GRID_SPAWN_LOCATION_FACTORY_CASH_4
		
	ENDSWITCH
	
	RETURN GRID_SPAWN_LOCATION_INVALID
ENDFUNC

PROC GET_FACTORY_EXIT_MENU_TITLE_AND_OPTIONS(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iCurrentExit)
	
	IF COUNT_OF(strOptions) != SIMPLE_INTERIOR_MENU_MAX_OPTIONS
		CASSERTLN(DEBUG_PROPERTY, "GET_CLUBHOUSE_EXIT_MENU_TITLE_AND_OPTIONS - options array has wrong size.")
		EXIT
	ENDIF
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			txtTitle = "BIKER_WH_1"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			txtTitle = "BIKER_WH_2"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			txtTitle = "BIKER_WH_3"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			txtTitle = "BIKER_WH_4"
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			txtTitle = "BIKER_WH_5"
		BREAK
		DEFAULT
			txtTitle = "MP_WHOUSE_EXIT"
		BREAK
	ENDSWITCH
	
	iOptionsCount = 2
	strOptions[0] = "BIKER_WH_EXITa"
	strOptions[1] = "BIKER_WH_EXITb"
ENDPROC

FUNC BOOL CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_FACTORY(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitId)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iExitId)
	RETURN IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_OWNER) OR IS_BIT_SET(g_SimpleInteriorData.iAccessBS, SIMPLE_INTERIOR_FACTORY_ACCESS_BS_BOSS)
ENDFUNC

FUNC INT GET_FACTORY_MAX_BLIPS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN MAX_FACTORY_BLIPS
ENDFUNC
   
FUNC BOOL SHOULD_FACTORY_BE_BLIPPED_THIS_FRAME(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	//FACTORY_ID eFactorID = GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)

	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN() = eSimpleInteriorID
		RETURN FALSE
	ENDIF
	
	IF CAN_PLAYER_ENTER_FACTORY(PLAYER_ID(), eSimpleInteriorID, 0)
		IF NOT SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN(eSimpleInteriorID)
			RETURN TRUE
		ENDIF
	ENDIF
	
	/*
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
			IF DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), eFactorID)
				RETURN TRUE
			ENDIF
		ELSE
			IF DOES_PLAYER_OWN_FACTORY(GB_GET_LOCAL_PLAYER_GANG_BOSS(), eFactorID)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF DOES_PLAYER_OWN_FACTORY(PLAYER_ID(), eFactorID)
		AND NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	*/
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_FACTORY_BLIP_SHORT_RANGE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	/*
	IF NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE // set as long range by default 2952492
	*/
	
	IF GB_IS_PLAYER_ON_SMUGGLER_MISSION(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	RETURN TRUE // always true url:bugstar:3047654
ENDFUNC

FUNC BLIP_SPRITE GET_FACTORY_BLIP_SPRITE(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			RETURN RADAR_TRACE_PRODUCTION_METH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			RETURN RADAR_TRACE_PRODUCTION_WEED
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			RETURN RADAR_TRACE_PRODUCTION_CRACK
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			RETURN RADAR_TRACE_PRODUCTION_MONEY
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			RETURN RADAR_TRACE_PRODUCTION_FAKE_ID
		BREAK
	ENDSWITCH
	
	RETURN RADAR_TRACE_WAREHOUSE
ENDFUNC

FUNC VECTOR GET_FACTORY_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	VECTOR vBlipCoords
	GET_FACTORY_ENTRY_LOCATE(eSimpleInteriorID, vBlipCoords)
	RETURN vBlipCoords
ENDFUNC

FUNC BLIP_PRIORITY GET_FACTORY_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	RETURN BLIPPRIORITY_MED
ENDFUNC

PROC MAINTAIN_FACTORY_BLIP_EXTRA_FUNCTIONALITY(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)

	UNUSED_PARAMETER(iBlipType)
	UNUSED_PARAMETER(iBlipNameHash)
	UNUSED_PARAMETER(OverlayBlipIndex)
	UNUSED_PARAMETER(iBlipIndex)
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BIKER_WH_1B")
			EXIT
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BIKER_WH_2B")
			EXIT
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BIKER_WH_3B")
			EXIT
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BIKER_WH_4B")
			EXIT
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BIKER_WH_5B")
			EXIT
		BREAK
	ENDSWITCH	
	
	SET_BLIP_NAME_FROM_TEXT_FILE(blipIndex, "BIKER_WH_1B")
	
ENDPROC

FUNC INT GET_FACTORY_BLIP_COLOUR(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iBlipIndex)
	
	//url:bugstar:2817398 - Exec1 - Colour offices and warehouse blips the Org colour when you are a Bodyguard and change the access to your own offices (warehouse access is already blocked)
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
	ELIF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
		PLAYER_INDEX GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		RETURN GET_BLIP_COLOUR_FROM_HUD_COLOUR(GB_GET_PLAYER_GANG_HUD_COLOUR(GangLeaderID))
	ENDIF
	
	RETURN BLIP_COLOUR_WHITE
ENDFUNC

FUNC VECTOR GET_FACTORY_MAP_MIDPOINT(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID)
		CASE FACTORY_ID_METH_1 		RETURN <<22.90, 6307.98, 0.0>>
		CASE FACTORY_ID_WEED_1 		RETURN <<410.88, 6515.83, 0.0>>
		CASE FACTORY_ID_CRACK_1 	RETURN <<31.28, 6466.30, 0.0>>
		CASE FACTORY_ID_FAKE_CASH_1 RETURN <<-417.89, 6152.75, 0.0>>
		CASE FACTORY_ID_FAKE_ID_1 	RETURN <<-167.33, 6326.04, 0.0>>
		
		CASE FACTORY_ID_METH_2 		RETURN <<1447.56, -1666.12, 0.0>>
		CASE FACTORY_ID_WEED_2	 	RETURN <<121.81, 187.86, 0.0>>
		CASE FACTORY_ID_CRACK_2 	RETURN <<-1477.83, -379.04, 0.0>>
		CASE FACTORY_ID_FAKE_CASH_2 RETURN <<-1159.31, -1381.69, 0.0>>
		CASE FACTORY_ID_FAKE_ID_2 	RETURN <<268.15, -726.74, 0.0>>
		
		CASE FACTORY_ID_METH_3 		RETURN <<195.23, 2467.11, 0.0>>
		CASE FACTORY_ID_WEED_3 		RETURN <<2861.52, 4453.81, 0.0>>
		CASE FACTORY_ID_CRACK_3 	RETURN <<385.99, 3575.13, 0.0>>
		CASE FACTORY_ID_FAKE_CASH_3 RETURN <<628.56, 2798.91, 0.0>>
		CASE FACTORY_ID_FAKE_ID_3 	RETURN <<1652.13, 4843.88, 0.0>>
		
		CASE FACTORY_ID_METH_4 		RETURN <<1212.35, -3145.12, 0.0>>
		CASE FACTORY_ID_WEED_4 		RETURN <<129.28, -2469.72, 0.0>>
		CASE FACTORY_ID_CRACK_4 	RETURN <<-227.57, -2586.04, 0.0>>
		CASE FACTORY_ID_FAKE_CASH_4 RETURN <<678.49, -2680.50, 0.0>>
		CASE FACTORY_ID_FAKE_ID_4 	RETURN <<-306.09, -2780.97, 0.0>>
	ENDSWITCH
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

// CUTSCENE STUFF

/*
PROC _INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(SIMPLE_CUTSCENE &cutscene, FACTORY_TYPE eFactoryType, FACTORY_ID eFactoryID, BOOL bForBoss)
	
ENDPROC

FUNC BOOL HAS_FACTORY_CUTSCENE_STARTED(FACTORY_ID eFactoryID)
	BOOL bPlay
	FACTORY_TYPE eFactoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	
	IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
		IF NOT HAS_FIRST_BIKER_BUSINESS_CUTSCENE_BEEN_DONE(eFactoryType)
			SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.simpleCutscene)
			_INIT_FIRST_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, TRUE)
			SIMPLE_CUTSCENE_START(g_SimpleInteriorData.simpleCutscene)
			bPlay = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][FACTORY] HAS_FACTORY_CUTSCENE_STARTED - Starting first business simple cutscene for boss.")
			#ENDIF
		ELSE
			IF NOT HAS_SECOND_BIKER_BUSINESS_CUTSCENE_BEEN_DONE(eFactoryType)
				IF HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(eFactoryID)
					SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.simpleCutscene)
					_INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, TRUE)
					SIMPLE_CUTSCENE_START(g_SimpleInteriorData.simpleCutscene)
					bPlay = TRUE
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][FACTORY] HAS_FACTORY_CUTSCENE_STARTED - Starting second business simple cutscene for boss.")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// If we're a goon check if our boss is watching any of the cutscenes for the given interior
		PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
		IF playerBoss != INVALID_PLAYER_INDEX()
			IF IS_PLAYER_IN_SIMPLE_CUTSCENE(playerBoss)
				SWITCH eFactoryType
					CASE FACTORY_TYPE_WEED
						IF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_weed_1")
							_INIT_FIRST_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ELIF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_weed_2")
							_INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ENDIF
					BREAK
					CASE FACTORY_TYPE_METH
						IF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_meth_1")
							_INIT_FIRST_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ELIF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_meth_2")
							_INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ENDIF
					BREAK
					CASE FACTORY_TYPE_CRACK
						IF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_crack_1")
							_INIT_FIRST_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ELIF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_crack_2")
							_INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ENDIF
					BREAK
					CASE FACTORY_TYPE_FAKE_IDS
						IF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_fake_id_1")
							_INIT_FIRST_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ELIF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_fake_id_2")
							_INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ENDIF
					BREAK
					CASE FACTORY_TYPE_FAKE_MONEY
						IF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_fake_money_1")
							_INIT_FIRST_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ELIF IS_PLAYER_IN_THIS_SIMPLE_CUTSCENE(playerBoss, "bwh_intro_fake_money_2")
							_INIT_SECOND_SIMPLE_CUTSCENE_FOR_FACTORY(g_SimpleInteriorData.simpleCutscene, eFactoryType, eFactoryID, FALSE)
							bPlay = TRUE
						ENDIF
					BREAK
				ENDSWITCH
				
				IF bPlay
					SIMPLE_CUTSCENE_START(g_SimpleInteriorData.simpleCutscene)
					#IF IS_DEBUG_BUILD
						PRINTLN("[SIMPLE_INTERIOR][FACTORY] HAS_FACTORY_CUTSCENE_STARTED - Starting business simple cutscene for goon.")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bPlay
ENDFUNC
*/
PROC MAINTAIN_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(details)
	UNUSED_PARAMETER(eState)
	UNUSED_PARAMETER(bExtScriptMustRun)
	
	/*
	IF eState = ENTRANCE_STATE_START_ENTERING
	AND NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_STARTED_ENTERING)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][FACTORY] MAINTAIN_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS - Player is entering the factory.")
		#ENDIF
	
		INT iCutsceneHash = WHICH_FACTORY_INTRO_CUTSCENE_SHOULD_BE_PLAYED(GET_FACTORY_ID_FROM_SIMPLE_INTERIOR_ID(eSimpleInteriorID))
		IF iCutsceneHash != 0
			g_SimpleInteriorData.iSimpleCutsceneHashToStart = iCutsceneHash
			#IF IS_DEBUG_BUILD
				PRINTLN("[SIMPLE_INTERIOR][FACTORY] MAINTAIN_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS - WHICH_FACTORY_INTRO_CUTSCENE_SHOULD_BE_PLAYED returned a hash, will start a cutscene.")
			#ENDIF
		ENDIF
		
		SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_STARTED_ENTERING)
	ENDIF
	
	IF eState = ENTRANCE_STATE_IDLE
	AND IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_STARTED_ENTERING)
		CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_STARTED_ENTERING)
		#IF IS_DEBUG_BUILD
			PRINTLN("[SIMPLE_INTERIOR][FACTORY] MAINTAIN_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS - Back to idle, clearing BS_SIMPLE_INTERIOR_FACTORY_STARTED_ENTERING.")
		#ENDIF
	ENDIF
	*/
ENDPROC

PROC RESET_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC MAINTAIN_FACTORY_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	IF NOT IS_BIT_SET(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_WANTED_LEVEL_CLEARED)
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_PROPERTY, "[SIMPLE_INTERIOR][FACTORY] MAINTAIN_FACTORY_INSIDE_GAMEPLAY_MODIFIERS - Cleared wanted level")
			#ENDIF
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
		SET_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_WANTED_LEVEL_CLEARED)
	ENDIF
ENDPROC

PROC RESET_FACTORY_INSIDE_GAMEPLAY_MODIFIERS(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	CLEAR_BIT(details.iSimpleInteriorInstanceBS, BS_SIMPLE_INTERIOR_FACTORY_WANTED_LEVEL_CLEARED)
ENDPROC

FUNC BOOL DOES_FACTORY_BLOCK_WEAPONS(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_FACTORY_RESTRICT_PLAYER_MOVEMENT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN TRUE
ENDFUNC

PROC GET_FACTORY_CAR_GENS_BLOCK_AREA(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)
	IF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_WEED_1
		vMin = <<416.2334, 6526.3066, 26.1780>>
		vMax = <<425.1550, 6537.5146, 30.0163>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_CRACK_1
		vMin = <<58.1605, 6463.7407, 29.8964>>
		vMax = <<71.6159, 6473.8823, 33.6967>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_MONEY_1
		vMin = <<-420.1546, 6147.2603, 29.8735>>
		vMax = <<-397.0880, 6185.6274, 34.9226>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_MONEY_2
		vMin = <<-1181.3087, -1391.5046, 3.4970>>
		vMax = <<-1168.1514, -1370.6084, 8.0470>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		vMin = <<297.8819, -706.9392, 27.3973>>
		vMax = <<321.2971, -689.0288, 32.2224>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_METH_3
		vMin = <<202.1808, 2454.8960, 54.6059>>
		vMax = <<212.2262, 2464.7080, 59.9501>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_MONEY_3
		vMin = <<640.6338, 2771.8181, 40.4589>>
		vMax = <<653.1158, 2794.8391, 45.5267>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		vMin = <<1658.0909, 4851.4922, 40.5298>>
		vMax = <<1665.2662, 4864.5977, 44.0774>>
		bDoBlockCarGen = TRUE
		EXIT
	ELIF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
		vMin = <<-341.0086, -2789.8259, 3.5734>>
		vMax = <<-331.4757, -2772.2981, 7.5039>>
		bDoBlockCarGen = TRUE
		EXIT
	ENDIF
	
	bDoBlockCarGen = FALSE
ENDPROC

FUNC BOOL GET_FACTORY_OUTSIDE_MODEL_HIDE(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
	fActivationRadius = 200
	bSurviveMapReload = FALSE
	
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
			SWITCH iIndex
				CASE 0
					vPos = <<375.9739, 3586.1746, 32.3049>>
					fRadius = 4.0
					eModel = PROP_RUB_CHASSIS_01
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
			SWITCH iIndex
				CASE 0
					vPos = <<2876.7529, 4457.7275, 47.5327>>
					fRadius = 4.0
					eModel = PROP_WOODPILE_01C
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		/* Disabled for B*4051170
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
			SWITCH iIndex
				CASE 0
					vPos = <<144.5453, -2486.4292, 6.1636>>
					fRadius = 4.0
					eModel = PROP_FNCLINK_04GATE1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
			SWITCH iIndex
				CASE 0
					vPos = <<-181.1744, 6315.7236, 30.4581>>
					fRadius = 2.0
					eModel = PROP_DUMPSTER_02A
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		*/
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC GET_FACTORY_SCENARIO_PED_REMOVAL_SPHERE(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
	IF eSimpleInteriorID = SIMPLE_INTERIOR_FACTORY_WEED_4
		// TODO: maybe add a check for if you own this property (but then some other player might spawn peds)
		// or maybe if anyone in session owns it then remove them?
		bDoRemoveScenarioPeds = TRUE
		vCoords = <<135.4647, -2474.1729, 5.0000>>
		fRadius = 5.0
		EXIT
	ENDIF
	bDoRemoveScenarioPeds = FALSE
ENDPROC

FUNC BOOL SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FACTORY(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(interiorOwner)
	UNUSED_PARAMETER(bOwnerDrivingOut)
	RETURN FALSE
ENDFUNC

FUNC INT GET_FACTORY_ENTRANCES_COUNT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN 1
ENDFUNC

FUNC INT GET_FACTORY_INVITE_ENTRY_POINT(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	RETURN -1
ENDFUNC

PROC GET_FACTORY_ENTRANCE_DATA(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)
	SWITCH iEntranceID
		CASE 0
			GET_FACTORY_ENTRY_LOCATE(eSimpleInteriorID, data.vPosition)
			
			data.vLocatePos1 = <<1, 1, 2>>
			
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_DRAWS_LOCATE)
			SET_BIT(data.iSettingsBS, SIMPLE_INTERIOR_ENTRANCE_SETTING_BS_ON_FOOT_ONLY)
			
			GET_FACTORY_ENTRY_LOCATE_COLOUR(eSimpleInteriorID, data.iLocateColourR, data.iLocateColourG, data.iLocateColourB, data.iLocateColourA)
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FACTORY_ENTRANCE_SCENE_DETAILS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(iEntranceID)
	UNUSED_PARAMETER(details)
ENDPROC

PROC MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_FACTORY_ACCESS(SIMPLE_INTERIOR_DETAILS &details)
	UNUSED_PARAMETER(details)
ENDPROC		

FUNC SIMPLE_INTERIORS GET_OWNED_SMPL_INT_OF_TYPE_FACTORY(PLAYER_INDEX playerID, INT iSlot, INT iFactoryType)
	UNUSED_PARAMETER(iSlot)
	
	FACTORY_TYPE eFactoryType = INT_TO_ENUM(FACTORY_TYPE, iFactoryType)
	
	IF eFactoryType != FACTORY_TYPE_INVALID
		FACTORY_ID eFactory = GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(playerID, eFactoryType)
		IF eFactory != FACTORY_ID_INVALID			
			RETURN GET_SIMPLE_INTERIOR_ID_FROM_FACTORY_ID(eFactory)
		ENDIF
	ENDIF
	
	RETURN SIMPLE_INTERIOR_INVALID
ENDFUNC

PROC FACTORY_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)
	UNUSED_PARAMETER(eSimpleInteriorID)
	UNUSED_PARAMETER(veh)
	UNUSED_PARAMETER(bSetOnGround)
ENDPROC

PROC DO_FACTORY_PRE_EXIT_CLEANUP(SIMPLE_INTERIORS eSimpleInteriorID)
	UNUSED_PARAMETER(eSimpleInteriorID)
	g_SimpleInteriorData.bForceCCTVCleanup = TRUE
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Below is the function that builds the interface.
// Because look up table needs to be build on every call as an optimisation
// the function retrieves a pointer to one function at a time so that one call = one lookup
// Functions are grouped thematically.

PROC BUILD_FACTORY_LOOK_UP_TABLE(SIMPLE_INTERIOR_INTERFACE &interface, SIMPLE_INTERIOR_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_FACTORY_USE_EXTERIOR_SCRIPT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION
			interface.getSimpleInteriorInteriorTypeAndPosition = &GET_FACTORY_INTERIOR_TYPE_AND_POSITION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS
			interface.getSimpleInteriorDetails = &GET_FACTORY_DETAILS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_DETAILS_BS
			interface.getSimpleInteriorDetailsPropertiesBS = &GET_FACTORY_PROPERTIES_BS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_NAME
			interface.simpleInteriorFP_ReturnS_ParamID = &GET_FACTORY_NAME
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
			interface.getSimpleInteriorInsideBoundingBox = &GET_FACTORY_INSIDE_BOUNDING_BOX
		BREAK
		CASE E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
			interface.getOwnedSimpleInteriorOfType = &GET_OWNED_SMPL_INT_OF_TYPE_FACTORY
		BREAK
		CASE E_SIMPLE_INTERIOR_LOAD_SECONDARY_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamID = &SHOULD_FACTORY_LOAD_SECONDARY_INTERIOR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_GAME_TEXT
			interface.simpleInteriorFP_ReturnS_ParamID_gameTextID = &GET_FACTORY_GAME_TEXT
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_SHOW_PIM_INVITE_OPTION
			interface.simpleInteriorFP_ReturnB_ParamID_tl63_AndBoolx2 = &SHOULD_FACTORY_SHOW_PIM_INVITE_OPTION
		BREAK
		//════════════════════════════════════════════════════════════════════
		
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCES_COUNT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_FACTORY_ENTRANCES_COUNT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_DATA
			interface.getSimpleInteriorEntranceData = &GET_FACTORY_ENTRANCE_DATA
		BREAK				
		CASE E_GET_SIMPLE_INTERIOR_INVITE_ENTRY_POINT
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_FACTORY_INVITE_ENTRY_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS
			interface.getSimpleInteriorEntranceSceneDetails = &GET_FACTORY_ENTRANCE_SCENE_DETAILS
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_CORONA_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_FACTORY_CORONA_BE_HIDDEN
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_ALLOW_ENTRY_IN_A_TUTORIAL_SESSION
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_FACTORY_ALLOW_ENTRY_IN_A_TUTORIAL_SESSION
		BREAK
		CASE E_SHOULD_THIS_SIMPLE_INTERIOR_LOCATE_BE_HIDDEN
			interface.simpleInteriorFP_ReturnB_ParamIDAndDebugB = &SHOULD_THIS_FACTORY_LOCATE_BE_HIDDEN
		BREAK
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR_IN_VEHICLE
			interface.simpleInteriorFP_ReturnB_ParamPIDAndModel = &CAN_PLAYER_ENTER_FACTORY_IN_VEHICLE
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_CAN_PLAYER_ENTER_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPID_ID_AndInt = &CAN_PLAYER_ENTER_FACTORY
		BREAK		
		CASE E_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE
			interface.canPlayerUseEntryInCurrentVeh = &CAN_PLAYER_USE_FACTORY_ENTRY_IN_CURRENT_VEHICLE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_ENTRY
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_FACTORY_REASON_FOR_BLOCKED_ENTRY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_REASON_FOR_BLOCKED_EXIT
			interface.simpleInteriorFP_ReturnS_ParamID_TL63_AndInt = &GET_FACTORY_REASON_FOR_BLOCKED_EXIT
		BREAK
		CASE E_CAN_PLAYER_INVITE_OTHERS_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_INVITE_OTHERS_TO_FACTORY
		BREAK
		CASE E_CAN_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamPIDAndID = &CAN_PLAYER_BE_INVITED_TO_FACTORY
		BREAK
		CASE E_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER
			interface.intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer = &CAN_LOCAL_PLAYER_BE_INVITED_TO_FACTORY_BY_PLAYER
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ENTRANCE_MENU_TITLE_AND_OPTIONS
			interface.simpleInteriorFPProc_ParamIDAndCustomMenu = &GET_FACTORY_ENTRANCE_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_PROCESS_SIMPLE_INTERIOR_ENTRANCE_MENU
			interface.simpleInteriorFPProc_ParamIDAndEMenu = &PROCESS_FACTORY_ENTRANCE_MENU
		BREAK
		CASE E_SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_LOCAL_PLAYER_BE_KICKED_OUT_FROM_FACTORY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_KICK_OUT_REASON
			interface.getSimpleInteriorKickOutReason = &GET_FACTORY_KICK_OUT_REASON
		BREAK
		CASE E_SET_SIMPLE_INTERIOR_ACCESS_BS
			interface.setSimpleInteriorAccessBS = &SET_FACTORY_ACCESS_BS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_SPAWN_POINT
			interface.getSimpleInteriorSpawnPoint = &GET_FACTORY_SPAWN_POINT
		BREAK
		CASE E_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS
			interface.getSimpleInteriorCustomInteriorWarpParams = &GET_FACTORY_CUSTOM_INTERIOR_WARP_PARAMS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT
			interface.getSimpleInteriorOutsideSpawnPoint = &GET_FACTORY_OUTSIDE_SPAWN_POINT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_FACTORY_ASYNC_SPAWN_GRID_LOCATION
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_ASYNC_SPAWN_GRID_INSIDE_LOCATION
			interface.simpleInteriorFP_ReturnEGW_ParamID = &GET_FACTORY_ASYNC_SPAWN_GRID_INSIDE_LOCATION
		BREAK
		CASE E_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS
			interface.maintainLeaveentryVehIfNoAccess = &MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_FACTORY_ACCESS
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_TELE_IN_SVM_COORD
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_FACTORY_TELEPORT_IN_SVM_COORD
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS
			interface.getSimpleInteriorExitMenuTitleAndOptions = &GET_FACTORY_EXIT_MENU_TITLE_AND_OPTIONS
		BREAK
		CASE E_CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_SIMPLE_INTERIOR
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &CAN_LOCAL_PLAYER_TRIGGER_EXIT_ALL_FOR_FACTORY
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_GET_SIMPLE_INTERIOR_MAX_BLIPS
			interface.simpleInteriorFP_ReturnI_ParamID = &GET_FACTORY_MAX_BLIPS
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_BE_BLIPPED_THIS_FRAME
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &SHOULD_FACTORY_BE_BLIPPED_THIS_FRAME
		BREAK
		CASE E_IS_SIMPLE_INTERIOR_BLIP_SHORT_RANGE
			interface.simpleInteriorFP_ReturnB_ParamIDAndInt = &IS_FACTORY_BLIP_SHORT_RANGE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_SPRITE
			interface.simpleInteriorFP_ReturnEBS_ParamIDAndInt = &GET_FACTORY_BLIP_SPRITE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COORDS
			interface.simpleInteriorFP_ReturnV_ParamIDAndInt = &GET_FACTORY_BLIP_COORDS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY
			interface.maintainSimpleInteriorBlipExtraFunctionality = &MAINTAIN_FACTORY_BLIP_EXTRA_FUNCTIONALITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_PRIORITY
			interface.getSimpleInteriorBlipPriority	= &GET_FACTORY_BLIP_PRIORITY
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_BLIP_COLOUR
			interface.simpleInteriorFP_ReturnI_ParamIDAndInt = &GET_FACTORY_BLIP_COLOUR
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_MAP_MIDPOINT
			interface.simpleInteriorFP_ReturnV_ParamID = &GET_FACTORY_MAP_MIDPOINT
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.maintainSimpleInteriorOutsideGameplayModifiers = &MAINTAIN_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_FACTORY_OUTSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_MAINTAIN_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &MAINTAIN_FACTORY_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		CASE E_RESET_SIMPLE_INTERIOR_INSIDE_GAMEPLAY_MODIFIERS
			interface.simpleInteriorFPProc_ParamDetailsAndID = &RESET_FACTORY_INSIDE_GAMEPLAY_MODIFIERS
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_DOES_SIMPLE_INTERIOR_BLOCK_WEAPONS
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_FACTORY_BLOCK_WEAPONS
		BREAK
		CASE E_DOES_SIMPLE_INTERIOR_RESTRICT_PLAYER_MOVEMENT
			interface.simpleInteriorFP_ReturnB_ParamID = &DOES_FACTORY_RESTRICT_PLAYER_MOVEMENT
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA
			interface.getSimpleInteriorCarGensBlockArea = &GET_FACTORY_CAR_GENS_BLOCK_AREA
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE
			interface.getSimpleInteriorOutsideModelHide = &GET_FACTORY_OUTSIDE_MODEL_HIDE
		BREAK
		CASE E_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE
			interface.getSimpleInteriorScenarioPedRemovalSphere = &GET_FACTORY_SCENARIO_PED_REMOVAL_SPHERE
		BREAK
		CASE E_SHOULD_SIMPLE_INTERIOR_TRIGGER_EXIT_IN_CAR
			interface.simpleInteriorFP_ReturnB_ParamID_PID_AndB = &SHOULD_TRIGGER_EXIT_IN_CAR_FOR_FACTORY
		BREAK
		//════════════════════════════════════════════════════════════════════
		CASE E_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
			interface.DoCustomActionForStateWarpToSpawnPoint = &FACTORY_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT
		BREAK
		CASE E_DO_SIMPLE_INTERIOR_PRE_EXIT_CLEANUP
			interface.simpleInteriorFPProc_ParamID = &DO_FACTORY_PRE_EXIT_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

