//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_NIGHTCLUB.sch																					//
// Description: Functions to implement from net_peds_base.sch and look up tables to return NIGHTCLUB ped data.			//
// Written by:  Jan Mencfel																								//
// Date:  		06/04/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_DLC_1_2022
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"

FUNC VECTOR _GET_NIGHTCLUB_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN << 0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT _GET_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0.0
ENDFUNC

PROC GET_NIGHTCLUB_VIP_OUTFIT(PEDS_DATA &Data)
	SWITCH g_iVIPOutfit
		CASE 0 
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_VIP_1)	
			Data.iPackedDrawable[0]								= 983050
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 1
			Data.iPackedTexture[2] 								= 0			
		BREAK
		CASE 1	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_VIP_2)		
			Data.iPackedDrawable[0]								= 0
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 83886081
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 2	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_VIP_1)			
			Data.iPackedDrawable[0]								= 218169344
			Data.iPackedDrawable[1]								= 5
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 1
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 3	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_VIP_3)			
			Data.iPackedDrawable[0]								= 16777216
			Data.iPackedDrawable[1]								= 1
			Data.iPackedDrawable[2]								= 1
			Data.iPackedTexture[0] 								= 33554433
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 4	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_VIP_4)			
			Data.iPackedDrawable[0]								= 84082689
			Data.iPackedDrawable[1]								= 3
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 16777216
			Data.iPackedTexture[1] 								= 1
			Data.iPackedTexture[2] 								= 0			
			BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_TROUBLEMAKER_0_OUTFIT(PEDS_DATA &Data)
	PRINTLN("GET_NIGHTCLUB_TROUBLEMAKER_0_OUTFIT g_iVIPOutfit = ", g_iVIPOutfit)
	SWITCH g_iVIPOutfit
		CASE 0 
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_1)	
			Data.iPackedDrawable[0]								= 100663296
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 33554432
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
		BREAK
		CASE 1	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_1)		
			Data.iPackedDrawable[0]								= 65537
			Data.iPackedDrawable[1]								= 1
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 33554432
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 2	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_1)			
			Data.iPackedDrawable[0]								= 33685506
			Data.iPackedDrawable[1]								= 2
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 3	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_1)			
			Data.iPackedDrawable[0]								= 84017155
			Data.iPackedDrawable[1]								= 3
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 4	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_1)			
			Data.iPackedDrawable[0]								= 67174405
			Data.iPackedDrawable[1]								= 1
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 16777216
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_TROUBLEMAKER_1_OUTFIT(PEDS_DATA &Data)
	PRINTLN("GET_NIGHTCLUB_TROUBLEMAKER_1_OUTFIT g_iVIPOutfit = ", g_iVIPOutfit)
	SWITCH g_iVIPOutfit
		CASE 0 
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_2)	
			Data.iPackedDrawable[0]								= 100663296
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 33554432
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
		BREAK
		CASE 1	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_2)		
			Data.iPackedDrawable[0]								= 16842753
			Data.iPackedDrawable[1]								= 1
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 33554432
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 2	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_2)			
			Data.iPackedDrawable[0]								= 33685506
			Data.iPackedDrawable[1]								= 2
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 3	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_2)			
			Data.iPackedDrawable[0]								= 84017155
			Data.iPackedDrawable[1]								= 3
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 4	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_2)			
			Data.iPackedDrawable[0]								= 67174405
			Data.iPackedDrawable[1]								= 1
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 16777216
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_TROUBLEMAKER_2_OUTFIT(PEDS_DATA &Data)
	PRINTLN("GET_NIGHTCLUB_TROUBLEMAKER_2_OUTFIT g_iVIPOutfit = ", g_iVIPOutfit)
	SWITCH g_iVIPOutfit
		CASE 0 
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_3)	
			Data.iPackedDrawable[0]								= 100663296
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 33554432
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
		BREAK
		CASE 1	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_3)		
			Data.iPackedDrawable[0]								= 0
			Data.iPackedDrawable[1]								= 0
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 83886081
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 2	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_3)			
			Data.iPackedDrawable[0]								= 33685506
			Data.iPackedDrawable[1]								= 2
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 3	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_3)			
			Data.iPackedDrawable[0]								= 84017155
			Data.iPackedDrawable[1]								= 3
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 0
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
		CASE 4	
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_NIGHTCLUB_MALE_3)			
			Data.iPackedDrawable[0]								= 67174405
			Data.iPackedDrawable[1]								= 1
			Data.iPackedDrawable[2]								= 0
			Data.iPackedTexture[0] 								= 16777216
			Data.iPackedTexture[1] 								= 0
			Data.iPackedTexture[2] 								= 0			
			BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_SPECIAL_PED_ACTIVITY_AND_POSITION(PEDS_DATA &Data)
	PRINTLN("GET_NIGHTCLUB_SPECIAL_PED_ACTIVITY_AND_POSITION g_iPedLayout = ", g_iPedLayout)
	SWITCH g_iPedLayout
		//not spawning ped
		CASE -1
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
		BREAK
//===EJECTING TROUBLEMAKER===
		//DRUNK IN LOADING BAY
		CASE 0
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK)
			Data.vPosition 				=  <<-1633.309, -2996.884, -78.1125>>
			Data.vRotation 				= <<0.0000, 0.0000, 109.800>>
		BREAK
		//VOMITING PED	
		CASE 1
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_VOMIT)
			Data.vPosition 				= <<-1608.5745, -3015.3684, -79.6875>>
			Data.vRotation 				= <<0.0000, 0.0000, -111.500>>
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
		BREAK	
		//DRUNK IN BACK OF DANCEFLOOR
		CASE 2
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET)
			Data.vPosition 				= <<-1587.359, -3007.812, -79>>
			Data.vRotation 				= <<0.0000, 0.0000, -92.000>>
		BREAK
		//ARGUE WITH BOUNCER
		CASE 3
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_ARGUE_WITH_BOUNCER)
			Data.vPosition 				= <<-1580.7953, -3016.3579, -80.0059>>
			Data.vRotation 				= <<0.0000, 0.0000, 266.1042>>
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
		BREAK
//===PASSED-OUT VIP===
		//Location 1 = VIP Section:
		CASE 4
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION)
			Data.vPosition 				= <<-1601.631, -3002.895, -76.25>>
			Data.vRotation 				= <<0.0000, 0.0000, -90.0>>
		BREAK
		//Location 2 = Office Bed
		CASE 5
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_PASSED_OUT_OFFICE_BED)
			Data.vPosition 				= <<-1613.653, -3019.959, -75.2125>>
			Data.vRotation 				= <<0.0000, 0.0000, 113.500>>
		BREAK
		//Location 3 = Upstairs Bar:
		CASE 6
			Data.iActivity 				= ENUM_TO_INT(PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR)
			Data.vPosition 				= <<-1581.498, -3006.177, -76.025>>
			Data.vRotation 				= <<0.0000, 0.0000, 153.750>>
		BREAK
	ENDSWITCH
	
	IF g_iPedLayout < 4
		SWITCH g_iNightClubClientele
			CASE 0 	GET_NIGHTCLUB_TROUBLEMAKER_0_OUTFIT(Data)			BREAK
			CASE 1	GET_NIGHTCLUB_TROUBLEMAKER_1_OUTFIT(Data)			BREAK
			CASE 2	GET_NIGHTCLUB_TROUBLEMAKER_2_OUTFIT(Data)			BREAK
		ENDSWITCH
	ELSE
		GET_NIGHTCLUB_VIP_OUTFIT(Data)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_NIGHTCLUB_PED_DATA_LAYOUT_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iLevel 										= 2	
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_NIGHTCLUB_SPECIAL_PED_ACTIVITY_AND_POSITION(Data)
					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RESETTABLE_PED)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC


//PROC _SET_NIGHTCLUB_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
//	UNUSED_PARAMETER(ePedModel)
//	UNUSED_PARAMETER(iLayout)
//	SWITCH iPed		
//		CASE 0
//			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
//		BREAK
//	ENDSWITCH 
//ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_NIGHTCLUB_PED_SCRIPT_LAUNCH()
PRINTLN("_SHOULD_NIGHTCLUB_PED_SCRIPT_LAUNCH")
	RETURN g_iPedLayout != -1
ENDFUNC

FUNC BOOL _IS_NIGHTCLUB_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_NIGHTCLUB_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)
UNUSED_PARAMETER(iLayout)
	
	
	PRINTLN("_SET_NIGHTCLUB_PED_DATA   LAYOUT SMALL NIGHTCLUB 0, PED: ", iPed)	
	_SET_NIGHTCLUB_PED_DATA_LAYOUT_0(Data, iPed, bSetPedArea)
	
		
ENDPROC

FUNC INT _GET_NIGHTCLUB_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0
ENDFUNC

FUNC INT _GET_NIGHTCLUB_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ePedLocation)
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_NIGHTCLUB_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_NIGHTCLUB_SERVER_PED_LAYOUT_TOTAL()
	RETURN 1
ENDFUNC

FUNC INT _GET_NIGHTCLUB_SERVER_PED_LAYOUT()	
	IF g_iPedLayout != -1
		RETURN g_iPedLayout
	ENDIF
	RETURN 0
ENDFUNC

FUNC INT _GET_NIGHTCLUB_SERVER_PED_LEVEL()
	RETURN 2  // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

PROC _SET_NIGHTCLUB_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_NIGHTCLUB_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_NIGHTCLUB_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_NIGHTCLUB_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_NIGHTCLUB_NETWORK_PED_TOTAL(ServerBD)
	ENDIF	

	
	PRINTLN("[AM_MP_PEDS] _SET_NIGHTCLUB_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_NIGHTCLUB_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_NIGHTCLUB_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_NIGHTCLUB_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_NIGHTCLUB_PARENT_PROPERTY(PLAYER_INDEX playerID)
PRINTLN("[AM_MP_PEDS] _IS_PLAYER_IN_NIGHTCLUB_PARENT_PROPERTY")
	RETURN IS_PLAYER_IN_NIGHTCLUB(playerID)
ENDFUNC

FUNC BOOL _HAS_NIGHTCLUB_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_NIGHTCLUB_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	SWITCH g_iPedLayout
		CASE 4
		CASE 5
		CASE 6
			SET_FACIAL_IDLE_ANIM_OVERRIDE(PedID, "mood_sleeping_1")
		BREAK
	ENDSWITCH
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_NIGHTCLUB_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
	
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_NIGHTCLUB_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC BOOL _CAN_NIGHTCLUB_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH eSpeech)
	UNUSED_PARAMETER(eSpeech)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF g_iPedLayout != 3
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
	
	// Specific conditions
	SWITCH iPed
		CASE -1
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
FUNC PED_SPEECH _GET_NIGHTCLUB_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	UNUSED_PARAMETER(iPed)
	PED_SPEECH eSpeech = PED_SPH_INVALID	
	
	SWITCH g_iPedLayout
		CASE 3			
			SWITCH iSpeech
				CASE 0	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN eSpeech
ENDFUNC

PROC _GET_NIGHTCLUB_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	
	RESET_PED_CONVO_DATA(convoData)
	
	INT iRandSpeech
	
	SWITCH iPed
		CASE 0
			convoData.sCharacterVoice = "A_M_Y_Hipster_01_White_FULL_01"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_LOITER
					PRINTLN("_GET_NIGHTCLUB_PED_CONVO_DATA")
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRandSpeech
						CASE 0	convoData.sRootName = "GENERIC_INSULT_HIGH"	BREAK
						CASE 1	convoData.sRootName = "GENERIC_INSULT_MED"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC PED_SPEECH _GET_NIGHTCLUB_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE 0			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_NIGHTCLUB_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_NIGHTCLUB_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_NIGHTCLUB_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_NIGHTCLUB_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_NIGHTCLUB_STANDING_SMOKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_NIGHTCLUB_TROUBLEMAKER_VOMIT_ANIM(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= INSTANT_BLEND_IN
	pedAnimData.fBlendOutDelta 			= INSTANT_BLEND_OUT

	
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode@throw_up_toilet@male@"
			pedAnimData.sAnimClip 		= "vomit_idle"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode@throw_up_toilet@male@"
			pedAnimData.sAnimClip 		= "vomit"
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC _GET_NIGHTCLUB_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	UNUSED_PARAMETER(iClip)
	RESET_PED_ANIM_DATA(pedAnimData)
	SWITCH eActivity					
		CASE PED_ACT_NIGHTCLUB_VOMIT
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sum2_vomit_trigger"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_sum2", "scr_sum2_club_vomit", "", "", <<-0.02, 0.13, 0.0>>, <<90.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			GET_NIGHTCLUB_TROUBLEMAKER_VOMIT_ANIM(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK
			pedAnimData.sAnimDict 		= "amb@world_human_bum_standing@drunk@base"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET
			pedAnimData.sAnimDict 		= "move_m@drunk@VERYDRUNK_IDLES@"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "fidget_01"
		BREAK
		CASE PED_ACT_NIGHTCLUB_ARGUE_WITH_BOUNCER
			pedAnimData.sAnimDict 		= "anim@amb@casino@brawl@fights@argue@"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "arguement_loop_mp_m_brawler_01"
		BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION
			pedAnimData.sAnimDict 		= "misscarsteal4asleep" 
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			
			pedAnimData.sAnimClip 		= "franklin_asleep"
		BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_OFFICE_BED
			pedAnimData.sAnimDict 		= "anim@mp_bedmid@right_var_01"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)			
			 
			pedAnimData.sAnimClip 		= "f_sleep_r_loop_bighouse"
		BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR
			pedAnimData.sAnimDict 		= "amb@world_human_stupor@male_looking_right@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
						
			pedAnimData.sAnimClip 		= "base"
		BREAK
			
	ENDSWITCH
ENDPROC

FUNC INT _GET_NIGHTCLUB_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

PROC _NIGHTCLUB_UPDATE(SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)
UNUSED_PARAMETER(ServerBD)
	IF IS_PEDS_BIT_SET(LocalData.PedData[0].iBS, BS_PED_DATA_LAUNCH_CUSTOM_TASK)
	AND g_iPedLayout != -1
		CLEAR_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_LAUNCH_CUSTOM_TASK)
		
		PRINTLN("[TROUBLEMAKER][NIGHTCLUB_UPDATE] SHOULD SPAWN PED!  SETTING BS_PED_DATA_FADE_IN_PED")
		RESET_PED_DATA(LocalData.PedData[0])
		PED_LOCATIONS ePedLocation
		_SET_NIGHTCLUB_PED_DATA(ePedLocation, ServerBD, LocalData.PedData[0], 0, ServerBD.iLayout)
		CLEAR_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_FADE_PED)
		CLEAR_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_FADE_PED_COMPLETE)
		CLEAR_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_SKIP_PED)
		SET_PEDS_BIT(LocalData.PedData[0].iBS, BS_PED_DATA_FADE_IN_PED)		
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_NIGHTCLUB_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_NIGHTCLUB_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_NIGHTCLUB_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_NIGHTCLUB_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_NIGHTCLUB_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_NIGHTCLUB_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_NIGHTCLUB_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_NIGHTCLUB_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_NIGHTCLUB_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_NIGHTCLUB_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_NIGHTCLUB_PED_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_NIGHTCLUB_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_NIGHTCLUB_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_NIGHTCLUB_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_NIGHTCLUB_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_NIGHTCLUB_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_NIGHTCLUB_NETWORK_PED_PROPERTIES
		BREAK
//		CASE E_SET_PED_PROP_INDEXES
//			interface.setPedPropIndexes = &_SET_NIGHTCLUB_PED_PROP_INDEXES
//		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_NIGHTCLUB_PED_BEEN_CREATED
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_NIGHTCLUB_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_NIGHTCLUB_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_NIGHTCLUB_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_NIGHTCLUB_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_NIGHTCLUB_PED_CONVO_DATA
		BREAK
		CASE E_LOCATION_SPECIFIC_UPDATE
			interface.locationSpecificUpdate = &_NIGHTCLUB_UPDATE
		BREAK
				
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
