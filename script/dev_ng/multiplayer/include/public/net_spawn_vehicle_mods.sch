//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_spawn_vehicle_mods.sch																	//
// Description: Public header which contains helper methods for applying vehicle mods at spawn time.		//
// Written by:  Aidan Temple																				//
// Date:  		31/07/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_gang_boss_common.sch"

PROC SET_SPAWN_VEHICLE_MODS(VEHICLE_INDEX vehIndex)
	
	IF NOT DOES_ENTITY_EXIST(vehIndex)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Vehicle index is invalid, unable to set vehicle mods.") NET_NL()
		#ENDIF
		EXIT
	ENDIF
	
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(vehIndex)
	
	IF NOT IS_MODEL_VALID(vehModel)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Vehicle model is invalid, unable to set vehicle mods.") NET_NL()
		#ENDIF
		EXIT
	ENDIF
	
	IF (vehModel = HOWARD)
		IF GB_GET_SMUGGLER_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = SMV_STUNT_PILOT
		
			SET_VEHICLE_COLOURS(vehIndex, 111, 64)
			
			IF GET_NUM_MOD_KITS(vehIndex) > 0
				SET_VEHICLE_MOD_KIT(vehIndex, 0)
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Setting mod kits on active vehicle model ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehIndex)) NET_NL()
				#ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_LIVERY) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_LIVERY, 9)				
				ENDIF
				
				TOGGLE_VEHICLE_MOD(vehIndex, MOD_TOGGLE_TYRE_SMOKE, FALSE)
				
				TYRE_SMOKE_COLOUR_ENUM eSmokeColour = TYRE_SMOKE_BLUE
				INT iRand = GET_RANDOM_INT_IN_RANGE(0, 4)

				SWITCH iRand
					CASE 0
						eSmokeColour = TYRE_SMOKE_BLUE
					BREAK
					CASE 1
						eSmokeColour = TYRE_SMOKE_RED
					BREAK
					CASE 2
						eSmokeColour = TYRE_SMOKE_YELLOW
					BREAK
					CASE 3
						eSmokeColour = TYRE_SMOKE_BUSINESS_GREEN
					BREAK
				ENDSWITCH
				
				SET_AIRCRAFT_COUNTERMEASURE_TYPE(vehIndex, DEFAULT, TRUE, eSmokeColour)
			ENDIF
		ENDIF
	ENDIF
	
	IF (vehModel = MOGUL)
		IF GB_GET_SMUGGLER_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = SMV_ESCORT
			
			SET_VEHICLE_COLOURS(vehIndex, 24, 29)
			
			// Ensure vehicle has mod kits available before setting mods on it.
			IF GET_NUM_MOD_KITS(vehIndex) > 0
				SET_VEHICLE_MOD_KIT(vehIndex, 0)
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Setting mod kits on active vehicle model ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehIndex)) NET_NL()
				#ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_CHASSIS) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_CHASSIS, 0)	
				ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_ROOF) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_ROOF, 0)
				ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_EXHAUST) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_EXHAUST, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF (vehModel = MOLOTOK)
		IF GB_GET_SMUGGLER_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = SMV_CARGO_PLANE
			
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 4)
			
			SWITCH iRand
				CASE 0
					SET_VEHICLE_COLOURS(vehIndex, 117, 35)
				
					IF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
						SET_VEHICLE_LIVERY(vehIndex, 9)
					ENDIF
				BREAK
				CASE 1
					SET_VEHICLE_COLOURS(vehIndex, 117, 117)
				
					IF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
						SET_VEHICLE_LIVERY(vehIndex, 0)
					ENDIF
				BREAK
				CASE 2
					SET_VEHICLE_COLOURS(vehIndex, 117, 70)
				
					IF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
						SET_VEHICLE_LIVERY(vehIndex, 3)
					ENDIF
				BREAK
				CASE 3
					SET_VEHICLE_COLOURS(vehIndex, 3, 3)
				
					IF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
						SET_VEHICLE_LIVERY(vehIndex, 7)
					ENDIF
				BREAK
			ENDSWITCH
			
			IF GET_NUM_MOD_KITS(vehIndex) > 0
				SET_VEHICLE_MOD_KIT(vehIndex, 0)
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Setting mod kits on active vehicle model ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehIndex)) NET_NL()	
				#ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_ROOF) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_ROOF, 0)
				ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_BUMPER_F) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_BUMPER_F, 1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF (vehModel = NOKOTA)
		IF GB_GET_SMUGGLER_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = SMV_DOGFIGHT

			IF GET_VEHICLE_LIVERY_COUNT(vehIndex) > 0
				INT iRand = GET_RANDOM_INT_IN_RANGE(0, 4)				
			
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Setting livery to ") NET_PRINT_INT(iRand) NET_PRINT(" for vehicle model ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehIndex)) NET_NL()
				#ENDIF
			
				SWITCH iRand
					CASE 0
						SET_VEHICLE_LIVERY(vehIndex, 0)
					BREAK
					CASE 1
						SET_VEHICLE_LIVERY(vehIndex, 3)
					BREAK
					CASE 2
						SET_VEHICLE_LIVERY(vehIndex, 4)
					BREAK
					CASE 3
						SET_VEHICLE_LIVERY(vehIndex, 9)
					BREAK
				ENDSWITCH
			ENDIF

			SET_VEHICLE_COLOURS(vehIndex, 117, 35)
			#IF IS_DEBUG_BUILD
			NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Setting vehicle colours for vehicle model ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehIndex)) NET_NL()
			#ENDIF
			
			IF GET_NUM_MOD_KITS(vehIndex) > 0
				SET_VEHICLE_MOD_KIT(vehIndex, 0)
				
				#IF IS_DEBUG_BUILD
				NET_PRINT("[Spawning] SET_SPAWN_VEHICLE_MODS - Setting mod kits on active vehicle model ") NET_PRINT(GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(vehIndex)) NET_NL()
				#ENDIF

				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_CHASSIS) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_CHASSIS, 0)
				ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_ROOF) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_ROOF, 0)
				ENDIF
				
				IF GET_NUM_VEHICLE_MODS(vehIndex, MOD_BUMPER_F) > 0
					SET_VEHICLE_MOD(vehIndex, MOD_BUMPER_F, 1)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
