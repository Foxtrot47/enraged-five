///////////////////////////////////////////////////////////////////////////////////
// Name:        net_property_sections.sch                                      	 //
// Description: Swap interiors definitions										 //
// Written By:   Aleksej Leskin                                    				 //
///////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "model_enums.sch"

STRUCT ANCHOR_POINT_STRUCT
	VECTOR 		vPosition
	MODEL_NAMES mModel
ENDSTRUCT

STRUCT PROPERTY_SECTION_STRUCT
	ENTITY_INDEX				lightRig
	MODEL_NAMES					mSection
	VECTOR 						vPosition
	MODEL_NAMES					mAnchor
	INT							iSectionType
	INT							iSectionID
ENDSTRUCT
