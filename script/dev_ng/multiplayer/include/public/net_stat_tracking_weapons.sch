
//|=======================================================================================|
//|                 Author:  Brenda Carey				 Date: 05/07/2011                 |
//|=======================================================================================|
//| 																					  |
//|                           CNC STAT TRACKING WEAPONS    	                              |
//|                a) Where the guts of the weapon tracking takes place			     	  |
//|                             								        				  |
//| 																					  |
//|=======================================================================================|




USING "globals.sch"
USING "commands_script.sch"

USING "net_stat_info.sch"




//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN WORKING PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////






//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



PROC TRACK_AMMO_GROUPING()

	//PURCHASED AMMP
	INT AddupPistolPurchase
 
	AddupPistolPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_FM_AMMO_BOUGHT) 
	AddupPistolPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_FM_AMMO_BOUGHT) 
	AddupPistolPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL50_FM_AMMO_BOUGHT) 
	AddupPistolPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_FM_AMMO_BOUGHT ) 
	
	IF AddupPistolPurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_PISTOLS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_PISTOLS, AddupPistolPurchase)
	ENDIF
	
	INT AddupSMGPurchase

	AddupSMGPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_FM_AMMO_BOUGHT) 
	AddupSMGPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_FM_AMMO_BOUGHT) 
	//AddupSMGPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_AMMO_BOUGHT) 
	//AddupSMGPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTMG_AMMO_BOUGHT) 
	IF AddupSMGPurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_SMGS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_SMGS, AddupSMGPurchase)
	ENDIF
	INT AddupSniperPurchase

	AddupSniperPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_FM_AMMO_BOUGHT) 
	AddupSniperPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_FM_AMMO_BOUGHT) 
	IF AddupSniperPurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_SNIPERS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_SNIPERS, AddupSniperPurchase)
	ENDIF
	INT AddupShotgunPurchase

	
	AddupShotgunPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLPUP_FM_AMMO_BOUGHT) 
	AddupShotgunPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_FM_AMMO_BOUGHT) 
	AddupShotgunPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_FM_AMMO_BOUGHT) 
	IF AddupShotgunPurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_SHOTGUNS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_SHOTGUNS, AddupShotgunPurchase)
	ENDIF
ENDPROC

PROC TRACK_AMMO_GROUPING2()
INT AddupRiflePurchase

	
	AddupRiflePurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_BOUGHT)
	AddupRiflePurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_FM_AMMO_BOUGHT) 
	AddupRiflePurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_FM_AMMO_BOUGHT) 

	
	IF AddupRiflePurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_RIFLES)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_RIFLES, AddupRiflePurchase)
	ENDIF
	INT AddupHeavyPurchase

	
	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_GRNLAUNCH_FM_AMMO_BOUGHT)
	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUNS_FM_AMMO_BOUGHT) 
	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_FM_AMMO_BOUGHT) 
	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_RPG_FM_AMMO_BOUGHT) 

	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_FM_AMMO_BOUGHT)
	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_FM_AMMO_BOUGHT)
	AddupHeavyPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTMG_FM_AMMO_BOUGHT )
	IF AddupHeavyPurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_HEAVY)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_HEAVY, AddupHeavyPurchase)
	ENDIF
	INT AddupThrownPurchase

	
	AddupThrownPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT)
	AddupThrownPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_FM_AMMO_BOUGHT) 
	AddupThrownPurchase += GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT)
	IF AddupThrownPurchase!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_THROWN)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_PURCHASED_THROWN, AddupThrownPurchase)
	ENDIF
ENDPROC


PROC TRACK_AMMO_USED()
//USED AMMO
	INT AddupPistolUsed
	AddupPistolUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_SHOTS) 
	AddupPistolUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_SHOTS) 
	AddupPistolUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL50_SHOTS) 
	AddupPistolUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_SHOTS ) 
	IF AddupPistolUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_PISTOLS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_PISTOLS, AddupPistolUsed)
	ENDIF
	INT AddupSMGUsed
	AddupSMGUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_SHOTS) 
	AddupSMGUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_SHOTS) 
	AddupSMGUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_SHOTS) 
	AddupSMGUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTMG_SHOTS ) 
	IF AddupSMGUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_SMGS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_SMGS, AddupSMGUsed)
	ENDIF
	INT AddupSniperUsed
	AddupSniperUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_SHOTS) 
	AddupSniperUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_SHOTS) 
	IF AddupSniperUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_SNIPERS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_SNIPERS, AddupSniperUsed)
	ENDIF
	

ENDPROC

PROC TRACK_AMMO_USED2()
	INT AddupShotgunUsed
	AddupShotgunUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_PUMP_SHOTS) 
	AddupShotgunUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLPUP_SHOTS) 
	AddupShotgunUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_SHOTS) 
	AddupShotgunUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_SHOTS)
	
	IF AddupShotgunUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_SHOTGUNS)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_SHOTGUNS, AddupShotgunUsed)
	ENDIF
	
	INT AddupRifleUsed
	AddupRifleUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_SHOTS)
	AddupRifleUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_SHOTS) 
	AddupRifleUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_SHOTS) 
	IF AddupRifleUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_RIFLES)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_RIFLES, AddupRifleUsed)
	ENDIF
	INT AddupHeavyUsed
	AddupHeavyUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_GRNLAUNCH_SHOTS)
	AddupHeavyUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUNS_SHOTS) 
	AddupHeavyUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_SHOTS) 
	AddupHeavyUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_RPG_SHOTS) 

	AddupHeavyUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_SHOTS) 
	IF AddupHeavyUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_HEAVY)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_HEAVY, AddupHeavyUsed)
	ENDIF
	
	
	INT AddupThrownUsed
	AddupThrownUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_SHOTS)
	AddupThrownUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_SHOTS) 
	AddupThrownUsed += GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_SHOTS) 
	IF AddupThrownUsed!=GET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_THROWN)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_AMMO_USED_THROWN, AddupThrownUsed)
	ENDIF
ENDPROC














PROC GIVE_PLAYER_WEAPONS_OF_EACH_TYPE()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTSHOTGUN, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, INFINITE_AMMO)
		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_KNIFE, INFINITE_AMMO)
	ENDIF


ENDPROC

