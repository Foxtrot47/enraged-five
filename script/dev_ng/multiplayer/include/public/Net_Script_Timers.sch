USING "commands_misc.sch"
USING "commands_network.sch"
USING "MP_globals_script_timers.sch"

/// PURPOSE:
///    Get the game time according to constraints commonly found in the NET_TIMER implementation.
FUNC TIME_DATATYPE _GET_GAME_TIME(BOOL bForceNonNetTimer = FALSE, BOOL bUseMoreAccurateTimer = FALSE)
	IF NETWORK_IS_GAME_IN_PROGRESS()  
	AND NOT bForceNonNetTimer
		IF NOT bUseMoreAccurateTimer
			RETURN GET_NETWORK_TIME() 
		ELSE
			RETURN GET_NETWORK_TIME_ACCURATE() 
		ENDIF
	ENDIF
	
	RETURN INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
ENDFUNC

/// PURPOSE:
///    Used to initialise the timer to the current time
/// PARAMS:
///    MainTimer -  Timer struct
PROC START_NET_TIMER(SCRIPT_TIMER& MainTimer, BOOL bForceNonNetTimer = FALSE, BOOL bUseMoreAccurateTimer = FALSE)
	IF MainTimer.bInitialisedTimer = FALSE
		IF NETWORK_IS_GAME_IN_PROGRESS()  
		AND NOT bForceNonNetTimer
			IF NOT bUseMoreAccurateTimer
				MainTimer.Timer = GET_NETWORK_TIME() 
			ELSE
				MainTimer.Timer = GET_NETWORK_TIME_ACCURATE() 
			ENDIF
		ELSE
			MainTimer.Timer = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
		ENDIF
		MainTimer.bInitialisedTimer = TRUE
	ENDIF
ENDPROC 

/// PURPOSE:
///    Used to initialise the timer to the current time
/// PARAMS:
///    MainTimer - Timer struct
///    iOffsetMs - 
PROC START_NET_TIMER_RETROACTIVELY(SCRIPT_TIMER& MainTimer, INT iOffsetMs, BOOL bForceNonNetTimer = FALSE, BOOL bUseMoreAccurateTimer = FALSE)
	IF MainTimer.bInitialisedTimer = FALSE
		MainTimer.Timer = GET_TIME_OFFSET(_GET_GAME_TIME(bForceNonNetTimer, bUseMoreAccurateTimer), IMIN(-iOffsetMs, 0))
		MainTimer.bInitialisedTimer = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Used to set the value of timer0 to the same value as timer1 (i.e. copying a timer value to another timer).
/// PARAMS:
///    Timer0 - timer to start.
///    Time - timer to copy start value from.
PROC SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(SCRIPT_TIMER& Timer0, TIME_DATATYPE Time)
	
	Timer0.Timer = Time
	Timer0.bInitialisedTimer = TRUE
	
ENDPROC
/// PURPOSE:
///   Used when you want to set time value once per frame and just reference it from there
/// PARAMS:
///    MainTimer -  Timer struct
PROC REINIT_NET_TIMER(SCRIPT_TIMER& MainTimer, BOOL bForceNonNetTimer = FALSE, BOOL bUseMoreAccurateTimer = FALSE)
	IF NETWORK_IS_GAME_IN_PROGRESS()  
	AND NOT bForceNonNetTimer
		IF NOT bUseMoreAccurateTimer
			MainTimer.Timer = GET_NETWORK_TIME() 
		ELSE
			MainTimer.Timer = GET_NETWORK_TIME_ACCURATE() 
		ENDIF
	ELSE
		MainTimer.Timer = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
	ENDIF
	MainTimer.bInitialisedTimer = TRUE
ENDPROC 

/// PURPOSE:
///    Resets the timers initialised state, so it can be reused.
/// PARAMS:
///    MainTimer - The struct for the timer. 
PROC RESET_NET_TIMER(SCRIPT_TIMER& MainTimer)
	MainTimer.bInitialisedTimer = FALSE
ENDPROC

FUNC BOOL HAS_NET_TIMER_STARTED(SCRIPT_TIMER& MainTimer)
	RETURN MainTimer.bInitialisedTimer 
ENDFUNC  

 
/// PURPOSE:
///    Returns true when the timer has passed but only for one frame. The timer will start running again after returning true for one frame.
///    Has to be called the whole time you're running the timer. 
/// PARAMS:
///    MainTimer - Timer struct
///    TimeToExpire - The time you want it to pass in Milliseconds, -1 will return true.
/// RETURNS:
///    
FUNC BOOL HAS_NET_TIMER_EXPIRED_ONE_FRAME(SCRIPT_TIMER& MainTimer, INT TimeToExpire, BOOL bForceNonNetTimer = FALSE)

	IF TimeToExpire = -1
		RETURN TRUE
	ENDIF

	START_NET_TIMER(MainTimer, bForceNonNetTimer)
	IF NETWORK_IS_GAME_IN_PROGRESS()  
	AND NOT bForceNonNetTimer
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MainTimer.Timer)) >= TimeToExpire)
			RESET_NET_TIMER(MainTimer)
			RETURN TRUE
		ENDIF
	ELSE
		IF (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), MainTimer.Timer)) >= TimeToExpire)
			RESET_NET_TIMER(MainTimer)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
 
 /// PURPOSE:
///    Returns true when the timer has passed. Will always return true for this timer until RESET_SCRIPT_NET_TIMER has been called.
///    Has to be called the whole time you're running the timer. 
 /// PARAMS:
 ///    MainTimer - Timer struct
 ///    TimeToExpire - The time you want it to pass in Milliseconds, -1 will return true.
 /// RETURNS:
 ///    
FUNC BOOL HAS_NET_TIMER_EXPIRED(SCRIPT_TIMER& MainTimer, INT TimeToExpire, BOOL bForceNonNetTimer = FALSE)

	IF TimeToExpire = -1
		RETURN TRUE
	ENDIF
	
	START_NET_TIMER(MainTimer,bForceNonNetTimer)
	IF NETWORK_IS_GAME_IN_PROGRESS()  
	AND NOT bForceNonNetTimer
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MainTimer.Timer)) >= TimeToExpire)
			RETURN TRUE
		ENDIF
	ELSE
		IF (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), MainTimer.Timer)) >= TimeToExpire)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true when the timer has passed. Will always return true for this timer until RESET_SCRIPT_NET_TIMER has been called.
///    Has to be called the whole time you're running the timer. 
///    Doesn't call START_NET_TIMER. This is so Clients can check if a Sever timer has expired.
/// PARAMS:
///    MainTimer - Timer struct
///    TimeToExpire - The time you want it to pass in Milliseconds, -1 will return true.
/// RETURNS:
///    
FUNC BOOL HAS_NET_TIMER_EXPIRED_READ_ONLY(SCRIPT_TIMER MainTimer, INT TimeToExpire, BOOL bForceNonNetTimer = FALSE)

	IF TimeToExpire = -1
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
	AND NOT bForceNonNetTimer
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MainTimer.Timer)) >= TimeToExpire)
			RETURN TRUE
		ENDIF
	ELSE
		IF (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), MainTimer.Timer)) >= TimeToExpire)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    READ ONLY. Returns true when the timer has passed. Adds HAS_NET_TIMER_STARTED so that it returns false if it hasn't started yet.
///    Doesn't call START_NET_TIMER. This is so Clients can check if a Server timer has expired.
/// PARAMS:
///    MainTimer - Timer struct
///    TimeToExpire - The time you want it to pass in Milliseconds, -1 will return true.
/// RETURNS:
///    
FUNC BOOL HAS_NET_TIMER_STARTED_AND_EXPIRED(SCRIPT_TIMER MainTimer, INT TimeToExpire, BOOL bForceNonNetTimer = FALSE)
	IF NOT HAS_NET_TIMER_STARTED(MainTimer)
		RETURN FALSE
	ENDIF
	
	IF TimeToExpire = -1
		RETURN TRUE
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS() 
	AND NOT bForceNonNetTimer
		IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MainTimer.Timer)) >= TimeToExpire)
			RETURN TRUE
		ENDIF
	ELSE
		IF (ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), MainTimer.Timer)) >= TimeToExpire)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_NET_TIMER_DIFFERENCE(SCRIPT_TIMER& TimerOne, SCRIPT_TIMER& TimerTwo)
	RETURN GET_TIME_DIFFERENCE(TimerOne.Timer, TimerTwo.Timer)
ENDFUNC

FUNC INT GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(SCRIPT_TIMER& TimerOne, BOOL bForceNonNetTimer = FALSE, BOOL bUseMoreAccurateTimer = FALSE)
	IF NETWORK_IS_GAME_IN_PROGRESS()  
	AND NOT bForceNonNetTimer
		IF NOT bUseMoreAccurateTimer
			RETURN (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), TimerOne.Timer))
		ELSE
			RETURN (GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE() , TimerOne.Timer))
		ENDIF
	ENDIF
	RETURN (GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), TimerOne.Timer))
ENDFUNC

FUNC INT GET_ABS_NET_TIMER_DIFFERENCE(SCRIPT_TIMER& TimerOne, SCRIPT_TIMER& TimerTwo)
	RETURN ABSI(GET_TIME_DIFFERENCE(TimerOne.Timer, TimerTwo.Timer))
ENDFUNC

FUNC INT GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(SCRIPT_TIMER& TimerOne, BOOL bForceNonNetTimer = FALSE)
	IF NETWORK_IS_GAME_IN_PROGRESS() 
	AND NOT bForceNonNetTimer
		RETURN ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), TimerOne.Timer))
	ENDIF
	RETURN ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), TimerOne.Timer))
ENDFUNC

FUNC SCRIPT_TIMER GET_NET_TIMER_OFFSET(SCRIPT_TIMER& TimerOne, INT Offset)
	SCRIPT_TIMER Result
	Result.Timer = GET_TIME_OFFSET(TimerOne.Timer, Offset)
	RETURN Result 
ENDFUNC

/// PURPOSE:
///    Checks if two timers are equal to each other.
/// PARAMS:
///    TimerOne - Timer struct 
///    TimerTwo - Timer struct 
/// RETURNS:
///    
FUNC BOOL IS_NET_TIMER_EQUAL_TO(SCRIPT_TIMER& TimerOne, SCRIPT_TIMER& TimerTwo)

	IF IS_TIME_EQUAL_TO(TimerOne.Timer, TimerTwo.Timer) 
		RETURN TRUE
	ENDIF
	RETURN FALSE
		
ENDFUNC

/// PURPOSE:
///    Checks if timer one is greater than timer two
/// PARAMS:
///    TimerOne -  Timer struct
///    TimerTwo -  Timer struct
/// RETURNS:
///    
FUNC BOOL IS_NET_TIMER_MORE_THAN(SCRIPT_TIMER& TimerOne, SCRIPT_TIMER& TimerTwo)

	IF IS_TIME_MORE_THAN(TimerOne.Timer, TimerTwo.Timer) 
		RETURN TRUE
	ENDIF
	RETURN FALSE
		
ENDFUNC

/// PURPOSE:
///    Checks if timer one is less than timer two
/// PARAMS:
///    TimerOne -  Timer struct
///    TimerTwo -  Timer struct
/// RETURNS:
///    
FUNC BOOL IS_NET_TIMER_LESS_THAN(SCRIPT_TIMER& TimerOne, SCRIPT_TIMER& TimerTwo)

	IF IS_TIME_LESS_THAN(TimerOne.Timer, TimerTwo.Timer) 
		RETURN TRUE
	ENDIF
	RETURN FALSE
		
ENDFUNC

/// PURPOSE:
///    Returns the timer as a string in format h:mm:ss.ms
/// PARAMS:
///    MainTimer -  Timer struct
/// RETURNS:
///    
FUNC STRING GET_NET_TIMER_STRING(SCRIPT_TIMER& MainTimer)
	RETURN GET_TIME_AS_STRING(MainTimer.Timer)
ENDFUNC





/// <summary>
/// Give the ability to use PauseThisFrame() more than once per timer. 
/// </summary>
PROC NET_TIMER_PAUSE_RESET(SCRIPT_TIMER &temp)	
	temp.bInitialisedTimer = FALSE
ENDPROC

/// <summary>
/// Gives control to pause the timer each frame. Can only be used once and can be reused after calling ResetPause()
/// </summary>
/// <param name="useNetTimer"></param>
/// <param name="useAccurateTimer"></param>
PROC NET_TIMER_PAUSE_THIS_FRAME(SCRIPT_TIMER& MainTimer, SCRIPT_TIMER &temp, bool bForceNonNetTimer = FALSE, bool useAccurateTimer = false)

	IF NETWORK_IS_GAME_IN_PROGRESS()  
	AND NOT bForceNonNetTimer
		IF NOT useAccurateTimer

			IF temp.bInitialisedTimer = FALSE
				temp.Timer = GET_NETWORK_TIME()
				temp.bInitialisedTimer = true
			ENDIF
			
			MainTimer.Timer = GET_TIME_OFFSET(MainTimer.Timer, ABSI(GET_TIME_DIFFERENCE(temp.Timer,GET_NETWORK_TIME())))
			
			/*
			PRINTLN("[PAUSETIME] MainTimer.Timer = ", NATIVE_TO_INT(MainTimer.Timer))
			PRINTLN("[PAUSETIME] temp.Timer = ", NATIVE_TO_INT(temp.Timer))
			PRINTLN("[PAUSETIME] Diff = ", GET_TIME_DIFFERENCE(temp.Timer,GET_NETWORK_TIME()))
			PRINTLN("[PAUSETIME] Offset = ", NATIVE_TO_INT(GET_TIME_OFFSET(MainTimer.Timer, GET_TIME_DIFFERENCE(temp.Timer,GET_NETWORK_TIME()))))
			PRINTLN("[PAUSETIME] Result = ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MainTimer))
			PRINTLN("[PAUSETIME]")
			*/
			
			temp.Timer = GET_NETWORK_TIME()
			
			
			
		ELSE

			MainTimer.Timer = GET_TIME_OFFSET(MainTimer.Timer, GET_TIME_DIFFERENCE(temp.Timer,GET_NETWORK_TIME_ACCURATE()))
			temp.Timer = GET_NETWORK_TIME_ACCURATE()

		ENDIF
	ELSE
	
		MainTimer.Timer = GET_TIME_OFFSET(MainTimer.Timer, GET_TIME_DIFFERENCE(temp.Timer, INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())))
		temp.Timer = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())

	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the time remaining using the given the total time to expire of the given timer
FUNC INT GET_TIME_USED_BY_SCRIPT_TIMER_IN_SECONDS(SCRIPT_TIMER scriptTimer, BOOL bForceNonNetTimer = FALSE)
	INT iTimeRemaining
	
	TIME_DATATYPE iCurrentTime = GET_NETWORK_TIME()
	
	IF bForceNonNetTimer
		iCurrentTime = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
	ENDIF
	
	iTimeRemaining = ABSI(GET_TIME_DIFFERENCE(iCurrentTime, scriptTimer.Timer))		
	iTimeRemaining = (iTimeRemaining / 1000)
	
	RETURN iTimeRemaining
ENDFUNC

/// PURPOSE:
///    Returns the time remaining using the given the total time to expire of the given timer
FUNC INT GET_TIME_USED_BY_SCRIPT_TIMER_IN_MILLISECONDS(SCRIPT_TIMER scriptTimer, BOOL bForceNonNetTimer = FALSE)
	INT iTimeRemaining
	
	TIME_DATATYPE iCurrentTime = GET_NETWORK_TIME()
	
	IF bForceNonNetTimer
		iCurrentTime = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
	ENDIF
	
	iTimeRemaining = ABSI(GET_TIME_DIFFERENCE(iCurrentTime, scriptTimer.Timer))
	
	RETURN iTimeRemaining
ENDFUNC

/// PURPOSE:
///    Returns the time remaining using the given the total time to expire of the given timer
FUNC INT GET_TIME_REMAINING_FROM_SCRIPT_TIMER_IN_SECONDS(SCRIPT_TIMER scriptTimer, INT iTimeToExpire, BOOL bForceNonNetTimer = FALSE)
	INT iTimeRemaining
	
	TIME_DATATYPE iCurrentTime = GET_NETWORK_TIME()
	
	IF bForceNonNetTimer
		iCurrentTime = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
	ENDIF
	
	iTimeRemaining = ABSI(iTimeToExpire - (ABSI(GET_TIME_DIFFERENCE(iCurrentTime, scriptTimer.Timer))))		
	iTimeRemaining = (iTimeRemaining / 1000)
	
	RETURN iTimeRemaining
ENDFUNC

/// PURPOSE:
///    Returns the time remaining using the given the total time to expire of the given timer
FUNC INT GET_TIME_REMAINING_FROM_SCRIPT_TIMER_IN_MILLISECONDS(SCRIPT_TIMER scriptTimer, INT iTimeToExpire, BOOL bForceNonNetTimer = FALSE)
	INT iTimeRemaining
	
	TIME_DATATYPE iCurrentTime = GET_NETWORK_TIME()
	
	IF bForceNonNetTimer
		iCurrentTime = INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())
	ENDIF
	
	iTimeRemaining = ABSI(iTimeToExpire - (ABSI(GET_TIME_DIFFERENCE(iCurrentTime, scriptTimer.Timer))))
	
	RETURN iTimeRemaining
ENDFUNC




