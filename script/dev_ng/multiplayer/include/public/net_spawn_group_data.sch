
USING "mp_globals_new_features_tu.sch"

FUNC INT GET_NUMBER_OF_AREAS_FOR_GROUP_WARP_LOCATION(GROUP_WARP_LOCATION eGroupWarpLocation)
	SWITCH eGroupWarpLocation
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_1
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_2
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_3 
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_4
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_5
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_6
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_7
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_8
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_9
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_10
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_11
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_12		
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_1
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_2
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_3
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_4
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_6
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_7
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_8
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_9
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_10
		#IF FEATURE_CASINO_HEIST
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_PALETO_BAY
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_GRAPESEED
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_DAVIS
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_WEST_VINEWOOD
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_ROCKFORD_HILLS
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_LA_MESA
		#ENDIF
		
			RETURN 1
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_NUMBER_OF_POSITIONS_FOR_GROUP_WARP_LOCATION(GROUP_WARP_LOCATION eGroupWarpLocation)
	SWITCH eGroupWarpLocation
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_1
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_2
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_3 
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_4
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_5
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_6
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_7
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_8
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_9
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_10
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_11
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_12
			RETURN 8
		BREAK	
	
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_1
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_2
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_3
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_4
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_6
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_7
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_8
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_9
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_10
			RETURN 8
		BREAK 
		
		#IF FEATURE_CASINO_HEIST
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_PALETO_BAY
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_GRAPESEED
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_DAVIS
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_WEST_VINEWOOD
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_ROCKFORD_HILLS
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_LA_MESA
			RETURN 8
		BREAK
		#ENDIF
		
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(GROUP_WARP_LOCATION eGroupWarpLocation, INT iArea, INT iPosition, VECTOR &vPosition, FLOAT &fHeading)
	SWITCH eGroupWarpLocation
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_1
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<246.0, -1818.488, 27.176>>
							fHeading = 49.4419
						BREAK
						CASE 1
							vPosition = <<247.1122, -1817.5349, 26.1894>>
							fHeading = 49.4419
						BREAK
						CASE 2
							vPosition = <<248.1923, -1820.4270, 26.2858>>
							fHeading = 49.4419
						BREAK
						CASE 3
							vPosition = <<249.1652, -1819.2887, 26.1983>>
							fHeading = 49.4419
						BREAK
						CASE 4
							vPosition = <<250.2453, -1822.1807, 26.2947>>
							fHeading = 49.4419
						BREAK
						CASE 5
							vPosition = <<251.2181, -1821.0424, 26.2072>>
							fHeading = 49.4419
						BREAK
						CASE 6
							vPosition = <<252.2982, -1823.9344, 26.3036>>
							fHeading = 49.4419
						BREAK
						CASE 7
							vPosition = <<253.2710, -1822.7960, 26.2161>>
							fHeading = 49.4419
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_2
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<-1453.8585, -927.6298, 10.0884>>
							fHeading = 230.4346
						BREAK
						CASE 1
							vPosition = <<-1454.8143, -928.7858, 9.4687>>
							fHeading = 230.4346
						BREAK
						CASE 2
							vPosition = <<-1455.9403, -925.9097, 9.4687>>
							fHeading = 230.4346
						BREAK
						CASE 3
							vPosition = <<-1456.8958, -927.0660, 9.4687>>
							fHeading = 230.4346
						BREAK
						CASE 4
							vPosition = <<-1458.0217, -924.1899, 9.4687>>
							fHeading = 230.4346
						BREAK
						CASE 5
							vPosition = <<-1458.9772, -925.3463, 9.4687>>
							fHeading = 230.4346
						BREAK
						CASE 6
							vPosition = <<-1460.1031, -922.4701, 9.4687>>
							fHeading = 230.4346
						BREAK
						CASE 7
							vPosition = <<-1461.0586, -923.6265, 9.4687>>
							fHeading = 230.4346
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_3
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<40.8092, -1043.4675, 29.5242>>
							fHeading = 249.6747
						BREAK
						CASE 1
							vPosition = <<39.7319, -1044.6670, 28.9278>>
							fHeading = 249.6747
						BREAK
						CASE 2
							vPosition = <<38.2673, -1042.5249, 28.9278>>
							fHeading = 249.6747
						BREAK
						CASE 3
							vPosition = <<37.0711, -1043.6814, 28.9278>>
							fHeading = 249.6747
						BREAK
						CASE 4
							vPosition = <<35.6229, -1041.5454, 28.9278>>
							fHeading = 249.6747
						BREAK
						CASE 5
							vPosition = <<34.4384, -1042.7063, 28.9278>>
							fHeading = 249.6747
						BREAK
						CASE 6
							vPosition = <<32.9831, -1040.5676, 28.9278>>
							fHeading = 249.6747
						BREAK
						CASE 7
							vPosition = <<31.7987, -1041.7285, 28.9278>>
							fHeading = 249.6747
						BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK		
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_4
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<34.5021, 2797.1919, 57.8782>>
							fHeading = 143.5105
						BREAK
						CASE 1
							vPosition = <<33.6427, 2798.5527, 57.2584>>
							fHeading = 143.5105
						BREAK
						CASE 2
							vPosition = <<36.1079, 2799.3630, 57.2584>>
							fHeading = 143.5105
						BREAK
						CASE 3
							vPosition = <<35.3301, 2800.8340, 57.2584>>
							fHeading = 143.5105
						BREAK
						CASE 4
							vPosition = <<37.7849, 2801.6304, 57.2584>>
							fHeading = 143.5105
						BREAK
						CASE 5
							vPosition = <<36.9997, 2803.0911, 57.2584>>
							fHeading = 143.5105
						BREAK
						CASE 6
							vPosition = <<39.4589, 2803.8936, 57.2584>>
							fHeading = 143.5105
						BREAK
						CASE 7
							vPosition = <<38.6737, 2805.3542, 57.2584>>
							fHeading = 143.5105
						BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK		
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_5
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<-335.4050, 6067.1846, 31.2537>>
							fHeading = 224.6883
						BREAK
						CASE 1
							vPosition = <<-336.8762, 6066.5498, 30.6337>>
							fHeading = 224.6883
						BREAK
						CASE 2
							vPosition = <<-337.2989, 6069.1099, 30.6337>>
							fHeading = 224.6883
						BREAK
						CASE 3
							vPosition = <<-338.8717, 6068.5669, 30.6337>>
							fHeading = 224.6883
						BREAK
						CASE 4
							vPosition = <<-339.2821, 6071.1147, 30.6337>>
							fHeading = 224.6883
						BREAK
						CASE 5
							vPosition = <<-340.8460, 6070.5630, 30.6337>>
							fHeading = 224.6883
						BREAK
						CASE 6
							vPosition = <<-341.2617, 6073.1162, 30.6337>>
							fHeading = 224.6883
						BREAK
						CASE 7
							vPosition = <<-342.8257, 6072.5645, 30.6337>>
							fHeading = 224.6883
						BREAK				
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_6
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<1732.3295, 3736.7896, 33.8380>>
							fHeading = 320.9990
						BREAK
						CASE 1
							vPosition = <<1733.1382, 3735.3789, 33.1527>>
							fHeading = 320.9990
						BREAK
						CASE 2
							vPosition = <<1730.6399, 3734.6775, 33.1527>>
							fHeading = 320.9990
						BREAK
						CASE 3
							vPosition = <<1731.3525, 3733.1738, 33.1527>>
							fHeading = 320.9990
						BREAK
						CASE 4
							vPosition = <<1728.8652, 3732.4858, 33.1527>>
							fHeading = 320.9990
						BREAK
						CASE 5
							vPosition = <<1729.5857, 3730.9919, 33.1527>>
							fHeading = 320.9990
						BREAK
						CASE 6
							vPosition = <<1727.0936, 3730.2981, 33.1527>>
							fHeading = 320.9990
						BREAK
						CASE 7
							vPosition = <<1727.8141, 3728.8044, 33.1527>>
							fHeading = 320.9990
						BREAK				
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK			
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_7
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<958.3943, -1448.0645, 31.1388>>
							fHeading = 268.7474
						BREAK
						CASE 1
							vPosition = <<957.7778, -1449.5636, 30.5182>>
							fHeading = 268.7474
						BREAK
						CASE 2
							vPosition = <<955.6937, -1448.0176, 30.5182>>
							fHeading = 268.7474
						BREAK
						CASE 3
							vPosition = <<954.9410, -1449.5016, 30.5182>>
							fHeading = 268.7474
						BREAK
						CASE 4
							vPosition = <<952.8743, -1447.9559, 30.5182>>
							fHeading = 268.7474
						BREAK
						CASE 5
							vPosition = <<952.1342, -1449.4402, 30.5182>>
							fHeading = 268.7474
						BREAK
						CASE 6
							vPosition = <<950.0600, -1447.8944, 30.5182>>
							fHeading = 268.7474
						BREAK
						CASE 7
							vPosition = <<949.3199, -1449.3787, 30.5182>>
							fHeading = 268.7474
						BREAK						
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK			
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_8
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<206.2954, 290.0851, 105.5912>>
							fHeading = 246.5687
						BREAK
						CASE 1
							vPosition = <<205.1621, 288.9384, 104.9802>>
							fHeading = 246.5687
						BREAK
						CASE 2
							vPosition = <<203.8157, 291.1568, 104.9802>>
							fHeading = 246.5687
						BREAK
						CASE 3
							vPosition = <<202.5586, 290.0668, 104.9802>>
							fHeading = 246.5687
						BREAK
						CASE 4
							vPosition = <<201.2283, 292.2782, 104.9802>>
							fHeading = 246.5687
						BREAK
						CASE 5
							vPosition = <<199.9826, 291.1832, 104.9802>>
							fHeading = 246.5687
						BREAK
						CASE 6
							vPosition = <<198.6454, 293.3976, 104.9802>>
							fHeading = 246.5687
						BREAK
						CASE 7
							vPosition = <<197.3998, 292.3026, 104.9802>>
							fHeading = 246.5687
						BREAK				
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK		
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_9
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<-46.3335, -193.8786, 52.1672>>
							fHeading = 71.0923
						BREAK
						CASE 1
							vPosition = <<-45.3014, -192.6459, 51.5514>>
							fHeading = 71.0923
						BREAK
						CASE 2
							vPosition = <<-43.7843, -194.7511, 51.5514>>
							fHeading = 71.0923
						BREAK
						CASE 3
							vPosition = <<-42.6170, -193.5654, 51.5514>>
							fHeading = 71.0923
						BREAK
						CASE 4
							vPosition = <<-41.1164, -195.6649, 51.5514>>
							fHeading = 71.0923
						BREAK
						CASE 5
							vPosition = <<-39.9610, -194.4751, 51.5514>>
							fHeading = 71.0923
						BREAK
						CASE 6
							vPosition = <<-38.4533, -196.5771, 51.5514>>
							fHeading = 71.0923
						BREAK
						CASE 7
							vPosition = <<-37.2979, -195.3873, 51.5514>>
							fHeading = 71.0923
						BREAK			
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK		
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_10
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition = <<2490.2622, 4116.5747, 38.1571>>
							fHeading = 245.7572
						BREAK
						CASE 1
							vPosition = <<2489.1074, 4115.4429, 37.5213>>
							fHeading = 245.7572
						BREAK
						CASE 2
							vPosition = <<2487.7925, 4117.6797, 37.5213>>
							fHeading = 245.7572
						BREAK
						CASE 3
							vPosition = <<2486.5200, 4116.6079, 37.5213>>
							fHeading = 245.7572
						BREAK
						CASE 4
							vPosition = <<2485.2212, 4118.8379, 37.5213>>
							fHeading = 245.7572
						BREAK
						CASE 5
							vPosition = <<2483.9602, 4117.7607, 37.5213>>
							fHeading = 245.7572
						BREAK
						CASE 6
							vPosition = <<2482.6545, 4119.9937, 37.5213>>
							fHeading = 245.7572
						BREAK
						CASE 7
							vPosition = <<2481.3933, 4118.9165, 37.5213>>
							fHeading = 245.7572
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK	
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_11
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition 	= <<-12.5726, 6399.3857, 31.4490>>
							fHeading 	= 224.0049
						BREAK
						CASE 1
							vPosition 	= <<-14.0549, 6398.7617, 30.8238>>
							fHeading 	= 224.0049
						BREAK
						CASE 2
							vPosition 	= <<-14.4471, 6401.3267, 30.8238>>
							fHeading 	= 224.0049
						BREAK
						CASE 3
							vPosition 	= <<-16.0262, 6400.8027, 30.8238>>
							fHeading 	= 224.0049
						BREAK
						CASE 4
							vPosition 	= <<-16.4062, 6403.3550, 30.8238>>
							fHeading 	= 224.0049
						BREAK
						CASE 5
							vPosition 	= <<-17.9766, 6402.8218, 30.8238>>
							fHeading 	= 224.0049
						BREAK
						CASE 6
							vPosition 	= <<-18.3618, 6405.3799, 30.8238>>
							fHeading 	= 224.0049
						BREAK
						CASE 7
							vPosition 	= <<-19.9323, 6404.8467, 30.8238>>
							fHeading 	= 224.0049
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK			
		CASE GROUP_WARP_LOCATION_OUTSIDE_BIKER_CLUBHOUSE_12
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0
							vPosition 	= <<-1129.1245, -1561.9915, 4.3772>>
							fHeading 	= 215.0789
						BREAK
						CASE 1
							vPosition 	= <<-1130.6869, -1562.3765, 3.7575>>
							fHeading 	= 215.0789
						BREAK
						CASE 2
							vPosition 	= <<-1130.6764, -1559.7815, 3.7575>>
							fHeading 	= 215.0789
						BREAK
						CASE 3
							vPosition 	= <<-1132.3176, -1560.0543, 3.7575>>
							fHeading	= 215.0789
						BREAK
						CASE 4
							vPosition 	= <<-1132.2970, -1557.4738, 3.7575>>
							fHeading 	= 215.0789
						BREAK
						CASE 5
							vPosition 	= <<-1133.9312, -1557.7568, 3.7575>>
							fHeading 	= 215.0789
						BREAK
						CASE 6
							vPosition 	= <<-1133.9148, -1555.1700, 3.7575>>
							fHeading 	= 215.0789
						BREAK
						CASE 7
							vPosition 	= <<-1135.5490, -1555.4531, 3.7575>>
							fHeading 	= 215.0789
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK		
		
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_1
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<1267.6649, 2830.8057, 47.4572>> 	fHeading = 134.9998		BREAK
						CASE 1 vPosition = <<1271.2261, 2830.1042, 47.6760>> 	fHeading = 134.9998		BREAK
						CASE 2 vPosition = <<1267.5693, 2834.4199, 47.5533>> 	fHeading = 134.9998		BREAK
						CASE 3 vPosition = <<1271.3423, 2833.8105, 47.8892>> 	fHeading = 134.9998		BREAK
						CASE 4 vPosition = <<1273.6177, 2827.4812, 47.5246>> 	fHeading = 134.9998		BREAK
						CASE 5 vPosition = <<1265.3097, 2837.4272, 47.0981>> 	fHeading = 134.9998		BREAK
						CASE 6 vPosition = <<1274.5265, 2830.9797, 48.0164>> 	fHeading = 134.9998		BREAK
						CASE 7 vPosition = <<1269.0804, 2838.4282, 47.7671>> 	fHeading = 134.9998		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_2
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<42.1820, 2626.1321, 82.6721>> 		fHeading = 304.1995		BREAK
						CASE 1 vPosition = <<38.6526, 2627.2324, 83.2000>> 		fHeading = 304.1995		BREAK
						CASE 2 vPosition = <<41.6485, 2622.5764, 83.0436>> 		fHeading = 304.1995		BREAK
						CASE 3 vPosition = <<38.1687, 2623.6458, 83.7750>> 		fHeading = 304.1995		BREAK
						CASE 4 vPosition = <<37.5899, 2620.4617, 84.1639>> 		fHeading = 304.1990		BREAK
						CASE 5 vPosition = <<35.1463, 2624.0388, 84.1691>> 		fHeading = 304.1990		BREAK
						CASE 6 vPosition = <<35.4574, 2627.6355, 83.5183>> 		fHeading = 304.1990		BREAK
						CASE 7 vPosition = <<40.6398, 2619.2061, 83.6920>> 		fHeading = 304.1990		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_3
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<2745.9468, 3904.5554, 43.0911>> 	fHeading = 113.5998		BREAK
						CASE 1 vPosition = <<2749.3333, 3902.7651, 43.2914>> 	fHeading = 113.5998		BREAK
						CASE 2 vPosition = <<2746.9087, 3908.0693, 42.9858>> 	fHeading = 113.5998		BREAK
						CASE 3 vPosition = <<2750.3599, 3906.5439, 43.4507>> 	fHeading = 113.5998		BREAK
						CASE 4 vPosition = <<2753.4243, 3905.2563, 43.8769>> 	fHeading = 113.5998		BREAK
						CASE 5 vPosition = <<2750.9636, 3910.0347, 43.5315>> 	fHeading = 113.5998		BREAK
						CASE 6 vPosition = <<2752.2632, 3901.5173, 43.6095>> 	fHeading = 113.5998		BREAK
						CASE 7 vPosition = <<2747.7859, 3911.4685, 42.7866>> 	fHeading = 113.5998		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_4
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<3378.4102, 5511.5366, 21.7523>> 	fHeading = 80.5999		BREAK
						CASE 1 vPosition = <<3380.8420, 5508.0244, 22.6706>> 	fHeading = 80.5999		BREAK
						CASE 2 vPosition = <<3382.0757, 5513.7583, 22.4883>> 	fHeading = 80.5999		BREAK
						CASE 3 vPosition = <<3384.7629, 5509.9668, 23.6256>> 	fHeading = 80.5999		BREAK
						CASE 4 vPosition = <<3386.0569, 5513.4614, 23.4869>> 	fHeading = 80.5999		BREAK
						CASE 5 vPosition = <<3384.5984, 5506.3721, 23.7205>> 	fHeading = 80.5999		BREAK
						CASE 6 vPosition = <<3387.3948, 5503.7861, 24.5215>> 	fHeading = 80.5999		BREAK
						CASE 7 vPosition = <<3389.8672, 5514.8599, 24.0015>> 	fHeading = 80.5999		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_6
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<27.4749, 6822.3643, 14.0960>> 		fHeading = 246.9995		BREAK
						CASE 1 vPosition = <<26.7026, 6826.8135, 13.7357>> 		fHeading = 246.9995		BREAK
						CASE 2 vPosition = <<23.8422, 6819.7241, 14.0775>> 		fHeading = 246.9995		BREAK
						CASE 3 vPosition = <<22.9392, 6824.0044, 14.0986>> 		fHeading = 246.9995		BREAK
						CASE 4 vPosition = <<20.6394, 6820.8413, 14.1446>> 		fHeading = 246.9995		BREAK
						CASE 5 vPosition = <<23.4866, 6827.6060, 13.9491>> 		fHeading = 246.9995		BREAK
						CASE 6 vPosition = <<17.5391, 6819.0767, 14.2563>> 		fHeading = 246.9995		BREAK
						CASE 7 vPosition = <<22.5429, 6830.8423, 14.0572>> 		fHeading = 246.9995		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_7
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<-2229.1792, 2391.0881, 12.2090>> 	fHeading = 186.1996		BREAK
						CASE 1 vPosition = <<-2225.1016, 2393.5789, 12.0766>> 	fHeading = 186.1996		BREAK
						CASE 2 vPosition = <<-2233.6565, 2392.8694, 11.5378>> 	fHeading = 186.1996		BREAK
						CASE 3 vPosition = <<-2229.7209, 2396.2083, 11.3838>> 	fHeading = 186.1996		BREAK
						CASE 4 vPosition = <<-2226.1086, 2397.3342, 11.3832>> 	fHeading = 186.1996		BREAK
						CASE 5 vPosition = <<-2233.3745, 2396.5327, 11.0581>> 	fHeading = 186.1996		BREAK
						CASE 6 vPosition = <<-2236.3806, 2398.0474, 10.8486>> 	fHeading = 186.1996		BREAK
						CASE 7 vPosition = <<-2223.5977, 2399.2473, 11.1047>> 	fHeading = 186.1996		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_8
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<10.3912, 3348.8340, 37.2910>>		fHeading = 304.7997		BREAK
						CASE 1 vPosition = <<5.2901, 3350.4917, 38.4910>> 		fHeading = 304.7997 	BREAK
						CASE 2 vPosition = <<9.1670, 3343.2419, 38.1345>> 		fHeading = 304.7997 	BREAK
						CASE 3 vPosition = <<4.2154, 3344.8635, 39.1750>> 		fHeading = 304.7997 	BREAK
						CASE 4 vPosition = <<7.2217, 3339.4124, 39.3085>> 		fHeading = 304.7997		BREAK
						CASE 5 vPosition = <<13.1333, 3341.3752, 37.2188>> 		fHeading = 304.7997		BREAK
						CASE 6 vPosition = <<11.1768, 3337.5898, 38.1951>> 		fHeading = 304.7997		BREAK
						CASE 7 vPosition = <<8.6955, 3334.5442, 39.2529>> 		fHeading = 304.7997		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_9
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<2097.4536, 1764.2627, 101.8216>> 	fHeading = 289.7997		BREAK
						CASE 1 vPosition = <<2093.6658, 1767.6890, 101.7281>> 	fHeading = 289.7997		BREAK
						CASE 2 vPosition = <<2095.5906, 1758.7000, 102.0331>> 	fHeading = 289.7997		BREAK
						CASE 3 vPosition = <<2091.2388, 1762.2964, 102.3197>> 	fHeading = 289.7997		BREAK
						CASE 4 vPosition = <<2094.4387, 1753.6438, 102.2372>> 	fHeading = 289.7997		BREAK
						CASE 5 vPosition = <<2091.5039, 1756.5571, 102.6361>> 	fHeading = 289.7997		BREAK
						CASE 6 vPosition = <<2091.6528, 1750.4814, 102.6860>> 	fHeading = 289.7997		BREAK
						CASE 7 vPosition = <<2088.7791, 1766.4427, 102.1104>> 	fHeading = 289.7997		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_DEFUNCT_BASE_10
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0 vPosition = <<1860.3470, 262.1885, 162.2995>> 	fHeading = 148.2000		BREAK
						CASE 1 vPosition = <<1863.9475, 262.5565, 162.2570>> 	fHeading = 148.2000		BREAK
						CASE 2 vPosition = <<1859.0883, 265.3675, 162.2173>> 	fHeading = 148.2000		BREAK
						CASE 3 vPosition = <<1862.6290, 265.8549, 162.5956>> 	fHeading = 148.2000		BREAK
						CASE 4 vPosition = <<1865.5327, 266.6531, 162.7503>> 	fHeading = 148.2000		BREAK
						CASE 5 vPosition = <<1862.0459, 269.0594, 162.9132>> 	fHeading = 148.2000		BREAK
						CASE 6 vPosition = <<1868.7479, 265.6188, 162.6499>> 	fHeading = 148.2000		BREAK
						CASE 7 vPosition = <<1860.1130, 271.9998, 162.9077>> 	fHeading = 148.2000		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		#IF FEATURE_CASINO_HEIST
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_PALETO_BAY
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0	vPosition 	= <<-237.3731, 6229.0815, 30.5009>>	fHeading 	= 44.4450	BREAK
						CASE 1	vPosition 	= <<-236.8922, 6230.5283, 30.4999>>	fHeading 	= 44.4450	BREAK
						CASE 2	vPosition 	= <<-239.0041, 6229.0483, 30.5037>>	fHeading 	= 44.4450	BREAK
						CASE 3	vPosition 	= <<-235.3235, 6231.2998, 30.4971>>	fHeading 	= 44.4450	BREAK
						CASE 4	vPosition 	= <<-239.3641, 6227.6919, 30.5044>>	fHeading 	= 44.4450	BREAK
						CASE 5	vPosition 	= <<-236.0484, 6233.0664, 30.4981>>	fHeading 	= 44.4450	BREAK
						CASE 6	vPosition 	= <<-240.4026, 6230.9863, 30.5021>>	fHeading 	= 44.4450	BREAK
						CASE 7	vPosition 	= <<-239.9078, 6232.3184, 30.4989>>	fHeading 	= 44.4450	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_GRAPESEED
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0	vPosition 	= <<1719.3866, 4759.8828, 41.0502>>	fHeading 	= 180.4900	BREAK
						CASE 1	vPosition 	= <<1718.2644, 4758.9624, 40.9693>>	fHeading 	= 180.4900	BREAK
						CASE 2	vPosition 	= <<1720.8857, 4759.0601, 40.9956>>	fHeading 	= 180.4900	BREAK
						CASE 3	vPosition 	= <<1717.2003, 4759.9619, 41.0509>>	fHeading 	= 180.4900	BREAK
						CASE 4	vPosition 	= <<1722.0001, 4760.0029, 41.0461>>	fHeading 	= 180.4900	BREAK
						CASE 5	vPosition 	= <<1716.1064, 4759.0278, 41.0499>>	fHeading 	= 180.4900	BREAK
						CASE 6	vPosition 	= <<1718.8926, 4755.0776, 40.9545>>	fHeading 	= 180.4900	BREAK
						CASE 7	vPosition 	= <<1717.3116, 4755.7085, 40.9704>>	fHeading 	= 180.4900	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_DAVIS
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0	vPosition 	= <<-104.8722, -1778.3311, 28.5017>>	fHeading 	= 321.1200	BREAK
						CASE 1	vPosition 	= <<-103.7559, -1778.8312, 28.4544>>	fHeading 	= 321.1200	BREAK
						CASE 2	vPosition 	= <<-105.5397, -1777.2885, 28.5496>>	fHeading 	= 321.1200	BREAK
						CASE 3	vPosition 	= <<-102.8682, -1779.9331, 28.3980>>	fHeading 	= 321.1200	BREAK
						CASE 4	vPosition 	= <<-106.8902, -1776.7936, 28.6040>>	fHeading 	= 321.1200	BREAK
						CASE 5	vPosition 	= <<-103.5376, -1777.3862, 28.4859>>	fHeading 	= 321.1200	BREAK
						CASE 6	vPosition 	= <<-102.2100, -1778.4575, 28.4162>>	fHeading 	= 321.1200	BREAK
						CASE 7	vPosition 	= <<-105.5613, -1776.0249, 28.5836>>	fHeading 	= 321.1200	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_WEST_VINEWOOD
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0	vPosition 	= <<-617.9044, 284.8121, 80.6844>>	fHeading 	= 174.6000	BREAK
						CASE 1	vPosition 	= <<-619.4274, 285.5440, 80.6583>>	fHeading 	= 174.6000	BREAK
						CASE 2	vPosition 	= <<-616.4741, 285.3420, 80.7093>>	fHeading 	= 174.6000	BREAK
						CASE 3	vPosition 	= <<-621.0435, 285.3562, 80.6340>>	fHeading 	= 174.6000	BREAK
						CASE 4	vPosition 	= <<-615.1828, 284.3087, 80.7321>>	fHeading 	= 174.6000	BREAK
						CASE 5	vPosition 	= <<-622.7971, 285.4996, 80.6135>>	fHeading 	= 151.1470	BREAK
						CASE 6	vPosition 	= <<-617.7243, 282.4009, 80.6865>>	fHeading 	= 174.6000	BREAK
						CASE 7	vPosition 	= <<-619.9250, 282.2562, 80.6254>>	fHeading 	= 174.6000	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_ROCKFORD_HILLS
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0	vPosition 	= <<-1287.6573, -277.1252, 37.6529>>	fHeading 	= 29.5200	BREAK
						CASE 1	vPosition 	= <<-1285.9908, -277.1624, 37.5792>>	fHeading 	= 29.5200	BREAK
						CASE 2	vPosition 	= <<-1288.5918, -278.7067, 37.6207>>	fHeading 	= 29.5200	BREAK
						CASE 3	vPosition 	= <<-1284.6030, -276.2882, 37.5412>>	fHeading 	= 29.5200	BREAK
						CASE 4	vPosition 	= <<-1290.0999, -279.0659, 37.6665>>	fHeading 	= 29.5200	BREAK
						CASE 5	vPosition 	= <<-1284.2897, -274.7838, 37.5909>>	fHeading 	= 29.5200	BREAK
						CASE 6	vPosition 	= <<-1288.9452, -275.6217, 37.7717>>	fHeading 	= 29.5200	BREAK
						CASE 7	vPosition 	= <<-1287.5261, -275.3952, 37.7199>>	fHeading 	= 29.5200	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE GROUP_WARP_LOCATION_OUTSIDE_ARCADE_LA_MESA
			SWITCH iArea
				CASE 0
					SWITCH iPosition
						CASE 0	vPosition 	= <<726.2093, -822.5177, 23.8400>>	fHeading 	= 90.7000	BREAK
						CASE 1	vPosition 	= <<725.2628, -821.1919, 23.8098>>	fHeading 	= 90.7000	BREAK
						CASE 2	vPosition 	= <<725.2170, -824.1523, 23.7991>>	fHeading 	= 90.7000	BREAK
						CASE 3	vPosition 	= <<726.0624, -819.7414, 23.8434>>	fHeading 	= 90.7000	BREAK
						CASE 4	vPosition 	= <<726.1193, -825.5561, 23.8245>>	fHeading 	= 90.7000	BREAK
						CASE 5	vPosition 	= <<725.0648, -818.4267, 23.8110>>	fHeading 	= 90.7000	BREAK
						CASE 6	vPosition 	= <<721.7838, -824.0378, 23.6809>>	fHeading 	= 90.7000	BREAK
						CASE 7	vPosition 	= <<722.4434, -822.1356, 23.7059>>	fHeading 	= 90.7000	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		#ENDIF
		
	ENDSWITCH
ENDPROC


