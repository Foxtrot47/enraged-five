USING "mp_globals_fm.sch"

FUNC MODEL_NAMES GET_HIRED_CREW_PED_MODEL(CASINO_HEIST_DRIVERS theDriver = CASINO_HEIST_DRIVER__NONE,CASINO_HEIST_HACKERS theHacker = CASINO_HEIST_HACKER__NONE,CASINO_HEIST_WEAPON_EXPERTS theGunner = CASINO_HEIST_WEAPON_EXPERT__NONE)
	SWITCH theDriver
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ 		RETURN hc_driver
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ	RETURN hc_driver
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH			RETURN u_m_m_edtoh
		CASE CASINO_HEIST_DRIVER__ZACH				RETURN S_M_Y_XMech_02
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT	RETURN mp_m_weapexp_01
	ENDSWITCH
	SWITCH theHacker
		CASE CASINO_HEIST_HACKER__RICKIE_LUKENS		RETURN hc_hacker
		CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ	RETURN hc_hacker
		CASE CASINO_HEIST_HACKER__YOHAN				RETURN INT_TO_ENUM(MODEL_NAMES,HASH("s_m_y_waretech_01"))
		CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN	RETURN ig_money
		CASE CASINO_HEIST_HACKER__PAIGE_HARRIS		RETURN ig_paige
	ENDSWITCH
	SWITCH theGunner
		CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI	RETURN hc_gunman
		CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA	RETURN hc_gunman
		CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE		RETURN INT_TO_ENUM(MODEL_NAMES,HASH("u_m_y_smugmech_01"))
		CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT	RETURN mp_m_weapexp_01
		CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY	RETURN hc_gunman
	ENDSWITCH
	SCRIPT_ASSERT("GET_HIRED_CREW_PED_MODEL: no valid character?? See Conor")
	PRINTLN("GET_HIRED_CREW_PED_MODEL: no valid character?? See Conor")
	RETURN G_M_Y_LOST_01
ENDFUNC

PROC SETUP_HIRED_CREW_PED(PED_INDEX thePed,CASINO_HEIST_DRIVERS theDriver = CASINO_HEIST_DRIVER__NONE,CASINO_HEIST_HACKERS theHacker = CASINO_HEIST_HACKER__NONE,CASINO_HEIST_WEAPON_EXPERTS theGunner = CASINO_HEIST_WEAPON_EXPERT__NONE)
	SWITCH theDriver
		CASE CASINO_HEIST_DRIVER__KARIM_DENZ
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,4,2)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,3,2)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_FEET,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL2,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_DECL,1,0)
		BREAK
		CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,2,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,2,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,2,2)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,2,2)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_FEET,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL2,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_DECL,1,0)
		BREAK
		CASE CASINO_HEIST_DRIVER__EDDIE_TOH
		BREAK
		CASE CASINO_HEIST_DRIVER__ZACH
		BREAK
		CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT
		BREAK
	ENDSWITCH
	SWITCH theHacker
		CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,2,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,3,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,3,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,3,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAND,2,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,0,0)
			SET_PED_PROP_INDEX(thePed,ANCHOR_EYES,0,0)
			SET_PED_PROP_INDEX(thePed,ANCHOR_EARS,0,0)
		BREAK
		CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAND,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,0,0)
		BREAK
		CASE CASINO_HEIST_HACKER__YOHAN
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,5,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,5,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,5,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,4,0)
		BREAK
		CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
		CASE CASINO_HEIST_HACKER__PAIGE_HARRIS
		BREAK
	ENDSWITCH
	SWITCH theGunner
		CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_BERD,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,6,2)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,6,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_FEET,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL2,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_DECL,1,0)
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,4,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_BERD,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,6,1)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,6,1)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_FEET,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL2,0,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_DECL,1,0)
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE
		CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT
		
		BREAK
		CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY
			SET_PED_DEFAULT_COMPONENT_VARIATION(thePed)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HEAD,5,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_BERD,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_HAIR,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_TORSO,6,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_LEG,6,2)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_FEET,1,0)
			SET_PED_COMPONENT_VARIATION(thePed,PED_COMP_SPECIAL,0,0)
		BREAK
	ENDSWITCH
	
	IF theDriver = CASINO_HEIST_DRIVER__NONE
	AND theHacker = CASINO_HEIST_HACKER__NONE
	AND theGunner = CASINO_HEIST_WEAPON_EXPERT__NONE
		SCRIPT_ASSERT("SETUP_HIRED_CREW_PED: neither driver or hacker?? See Conor")
		PRINTLN("SETUP_HIRED_CREW_PED: neither driver or hacker?? See Conor")
	ENDIF
ENDPROC
