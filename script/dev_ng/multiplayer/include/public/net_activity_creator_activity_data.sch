USING "net_activity_creator_components.sch"
USING "net_realty_biker_warehouse.sch"
USING "net_simple_interior.sch"

PROC SETUP_LEAN_ACT(ACTIVITY_MAIN &ActivityMain)
	
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	
	// Fix for B*3042318, overriding blend ratio and play time.
	ActivityMain.approachComponentStruct.approachTaskStruct.MoveBlendRatio = 0.5
	ActivityMain.approachComponentStruct.approachTaskStruct.Time = 6500
	
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

	ActivityMain.iEndAnimID = 3
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_LEAN_ACT_2(ACTIVITY_MAIN &ActivityMain)
	
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 240.0

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	
	// Fix for B*3042318, overriding blend ratio and play time.
	ActivityMain.approachComponentStruct.approachTaskStruct.MoveBlendRatio = 0.5
	ActivityMain.approachComponentStruct.approachTaskStruct.Time = 6500
	
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

	ActivityMain.iEndAnimID = 3
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC GET_COKE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	IF bMale
		SWITCH iVar
			CASE 0
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
			BREAK
			CASE 1
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
				pedData.pcsComponents[0].NewDrawableNumber = 1
				pedData.pcsComponents[0].NewTextureNumber = 2
				pedData.pcsComponents[2].NewTextureNumber = 1
				pedData.pcsComponents[3].NewTextureNumber = 1
				pedData.pcsComponents[4].NewTextureNumber = 1
				pedData.pcsComponents[6].NewTextureNumber = 1
			BREAK
			
			CASE 2
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
				pedData.pcsComponents[0].NewDrawableNumber = 2
				pedData.pcsComponents[0].NewTextureNumber = 2
				pedData.pcsComponents[3].NewTextureNumber = 2
				pedData.pcsComponents[4].NewTextureNumber = 2
				pedData.pcsComponents[6].NewTextureNumber = 2
			BREAK
			
			CASE 3
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
				
				pedData.pcsComponents[2].NewTextureNumber = 2
				
			BREAK
			
		ENDSWITCH
	ELSE
		SWITCH iVar
			CASE 0
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01")) //  caucasian, while underware, purple hat
				pedData.pcsComponents[2].NewTextureNumber = 2
			BREAK
			CASE 1
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01")) // black, black underware, green hat
				pedData.pcsComponents[0].NewDrawableNumber = 1
				
				pedData.pcsComponents[3].NewTextureNumber = 3
				pedData.pcsComponents[4].NewTextureNumber = 3
				pedData.pcsComponents[6].NewTextureNumber = 1
			BREAK
			
			CASE 2
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01")) // caucasian, black and white underware, green hat
				pedData.pcsComponents[0].NewDrawableNumber = 2
				
				pedData.pcsComponents[3].NewTextureNumber = 4
				pedData.pcsComponents[4].NewTextureNumber = 5
				pedData.pcsComponents[6].NewTextureNumber = 2
			BREAK
			
			CASE 3
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01")) // black, white and black underware, purple hat
				pedData.pcsComponents[0].NewDrawableNumber = 1
				pedData.pcsComponents[2].NewTextureNumber = 2
				
				pedData.pcsComponents[3].NewTextureNumber = 3
				pedData.pcsComponents[4].NewTextureNumber = 2
				pedData.pcsComponents[6].NewTextureNumber = 1
			BREAK
			
			CASE 4
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01")) // black, white underware, blue hat
				pedData.pcsComponents[0].NewDrawableNumber = 1
				pedData.pcsComponents[2].NewTextureNumber = 1
				
				pedData.pcsComponents[3].NewTextureNumber = 2
				pedData.pcsComponents[4].NewTextureNumber = 2
				pedData.pcsComponents[6].NewTextureNumber = 1
			BREAK
			
			CASE 5
				pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01")) // caucasian, black underware, blue hat
				pedData.pcsComponents[0].NewDrawableNumber = 2
				pedData.pcsComponents[2].NewTextureNumber = 1
				
				pedData.pcsComponents[3].NewTextureNumber = 5
				pedData.pcsComponents[4].NewTextureNumber = 5
				pedData.pcsComponents[6].NewTextureNumber = 2
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC GET_AUTOSHOP_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	iVar = iVar
	bMale = bMale
	
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	pedData.pcsComponents[0].NewDrawableNumber = 1
	pedData.pcsComponents[0].NewTextureNumber = 2
	
	pedData.pcsComponents[2].NewDrawableNumber = 1
	pedData.pcsComponents[2].NewTextureNumber = 2
	
	pedData.pcsComponents[3].NewDrawableNumber = 1
	pedData.pcsComponents[3].NewTextureNumber  = 1
	
	pedData.pcsComponents[4].NewTextureNumber  = 3
	
	pedData.pcsComponents[PED_COMP_JBIB].NewDrawableNumber  = 2
			
ENDPROC

PROC GET_DEFAULT_PED(ACTIVITY_MAIN &activityMain, PED_DATA &pedData, INT iPedNumber)
	
	
	pedData = activityMain.pedData[iPedNumber]
			
ENDPROC


PROC GET_SMASH_METH_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_meth_01"))
			
			pedData.ppsProps[1].iPropIndex = 0
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_COOK_METH_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewTextureNumber = 2
			pedData.pcsComponents[2].NewDrawableNumber = 1
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewDrawableNumber = 1
		BREAK

	ENDSWITCH
ENDPROC


PROC GET_SECURITY_WEED_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 4
			pedData.pcsComponents[3].NewDrawableNumber = 5
			pedData.pcsComponents[4].NewDrawableNumber = 3
			
			pedData.pcsComponents[8].NewDrawableNumber = 2
			pedData.pcsComponents[9].NewDrawableNumber = 1
			
			
			pedData.ppsProps[1].iPropIndex = 0
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_MONEY_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 2
			pedData.pcsComponents[3].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewDrawableNumber = 2
						
			pedData.ppsProps[1].iPropIndex = 0
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_CFID_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 3
			pedData.pcsComponents[3].NewDrawableNumber = 3
			pedData.pcsComponents[4].NewDrawableNumber = 3
			pedData.pcsComponents[8].NewDrawableNumber = 1
						
			pedData.ppsProps[1].iPropIndex = 1
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_COKE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 3
			pedData.pcsComponents[2].NewDrawableNumber = 1
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[4].NewDrawableNumber = 1
			pedData.pcsComponents[6].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewDrawableNumber = 1
			
			
			pedData.ppsProps[1].iPropIndex = 0
		BREAK

	ENDSWITCH
ENDPROC


PROC GET_SECURITY_METH_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 2
			pedData.pcsComponents[2].NewDrawableNumber = 2
			pedData.pcsComponents[3].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewDrawableNumber = 2
			
			
			
			pedData.ppsProps[1].iPropIndex = 1
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_BUNKER_PED_1(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
		CASE -1
			
			//Hack for url:bugstar:3602394 - Bunker Property - When remote player purchased the Security Upgrade, a guard was standing at the door with an invisible gun
			IF iVar = -1
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GET_SECURITY_BUNKER_PED_1 - iVar = -1 was passed in, default to use the correct visual setup. ")
			ENDIF
			
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapWork_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 8
			pedData.pcsComponents[0].NewTextureNumber = 0
			
			pedData.pcsComponents[2].NewDrawableNumber = 2
			pedData.pcsComponents[2].NewTextureNumber = 0
			
			pedData.pcsComponents[3].NewDrawableNumber = 8
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 6
			pedData.pcsComponents[4].NewTextureNumber = 0
			
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 5
			
			
			pedData.weaponType = WEAPONTYPE_ASSAULTRIFLE
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_BUNKER_PED_1_NO_GUN(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
		CASE -1
			
			//Hack for url:bugstar:3602394 - Bunker Property - When remote player purchased the Security Upgrade, a guard was standing at the door with an invisible gun
			IF iVar = -1
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GET_SECURITY_BUNKER_PED_1 - iVar = -1 was passed in, default to use the correct visual setup. ")
			ENDIF
			
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapWork_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 8
			pedData.pcsComponents[0].NewTextureNumber = 0
			
			pedData.pcsComponents[2].NewDrawableNumber = 2
			pedData.pcsComponents[2].NewTextureNumber = 0
			
			pedData.pcsComponents[3].NewDrawableNumber = 8
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 6
			pedData.pcsComponents[4].NewTextureNumber = 0
			
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 5
			
			pedData.weaponType = WEAPONTYPE_UNARMED
		BREAK

	ENDSWITCH
ENDPROC
PROC GET_SECURITY_PED_DEFUNCT_BASE_GUARD_1(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
		CASE -1
			
			//Hack for url:bugstar:3602394 - Bunker Property - When remote player purchased the Security Upgrade, a guard was standing at the door with an invisible gun
			IF iVar = -1
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GET_SECURITY_BUNKER_PED_1 - iVar = -1 was passed in, default to use the correct visual setup. ")
			ENDIF
			
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_HighSec_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_HEAD].NewTextureNumber = 0
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_TORSO].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_JBIB].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_JBIB].NewTextureNumber = 2
			
			
			pedData.weaponType = WEAPONTYPE_UNARMED
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_PED_DEFUNCT_BASE_GUARD_2(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
		CASE -1
			
			//Hack for url:bugstar:3602394 - Bunker Property - When remote player purchased the Security Upgrade, a guard was standing at the door with an invisible gun
			IF iVar = -1
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GET_SECURITY_BUNKER_PED_1 - iVar = -1 was passed in, default to use the correct visual setup. ")
			ENDIF
			
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_HighSec_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_HEAD].NewTextureNumber = 0
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_TORSO].NewTextureNumber = 2
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 2
			
			pedData.pcsComponents[PED_COMP_JBIB].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_JBIB].NewTextureNumber = 1
			
			pedData.weaponType = WEAPONTYPE_UNARMED
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_PED_DEFUNCT_BASE_GUARD_3(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
		CASE -1
			
			//Hack for url:bugstar:3602394 - Bunker Property - When remote player purchased the Security Upgrade, a guard was standing at the door with an invisible gun
			IF iVar = -1
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GET_SECURITY_BUNKER_PED_1 - iVar = -1 was passed in, default to use the correct visual setup. ")
			ENDIF
			
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_HighSec_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_HEAD].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_TORSO].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_JBIB].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_JBIB].NewTextureNumber = 0
			
			pedData.weaponType = WEAPONTYPE_UNARMED
		BREAK

	ENDSWITCH
ENDPROC

PROC GET_SECURITY_PED_DEFUNCT_BASE_GUARD_4(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
		CASE -1
			
			//Hack for url:bugstar:3602394 - Bunker Property - When remote player purchased the Security Upgrade, a guard was standing at the door with an invisible gun
			IF iVar = -1
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "GET_SECURITY_BUNKER_PED_1 - iVar = -1 was passed in, default to use the correct visual setup. ")
			ENDIF
			
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_M_M_HighSec_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_HEAD].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_TORSO].NewTextureNumber = 0
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 0
			
			pedData.pcsComponents[PED_COMP_JBIB].NewDrawableNumber = 0
			pedData.pcsComponents[PED_COMP_JBIB].NewTextureNumber = 2
			
			pedData.weaponType = WEAPONTYPE_DLC_SPECIALCARBINE
		BREAK

	ENDSWITCH
ENDPROC


PROC GET_SECURITY_BUNKER_PED_2(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("MP_M_WeapWork_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 9
			pedData.pcsComponents[PED_COMP_HAIR].NewDrawableNumber = 2
					
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 8
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 5
			pedData.pcsComponents[PED_COMP_HAND].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 5
			
			pedData.weaponType = WEAPONTYPE_PUMPSHOTGUN
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_A_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 4
					
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 4
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 2 //Use this when at the shooting range
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_A_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 4
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 4
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 2 //Use this when at the shooting range
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_B_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0 //Use this when at the shooting range
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_B_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 2
			
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0 //Use this when at the shooting range
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_C_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 5
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 4
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 5
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 3
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_C_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 1
			
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 4
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 1
			
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 3
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_D_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 1
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_D_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 4
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 3
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 4
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 3
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 1
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_E_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 5
			pedData.pcsComponents[PED_COMP_TORSO].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 5
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 5
			pedData.pcsComponents[PED_COMP_TORSO].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 5
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 3
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_E_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 3
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 3
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 3
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 3
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_F_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 1
						
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 4
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 1
						
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 4
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_F_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 2
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 1
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_G_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 5
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 4
						
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 2
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 5
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 4
						
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
			
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 2
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 2
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_G_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 5
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 2
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 5
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 6
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 2
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 1
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 3
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 2
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 2
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_H_MANUFACTURING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 1
			
			pedData.pcsComponents[PED_COMP_HAIR].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 4
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))			
			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 1
			
			pedData.pcsComponents[PED_COMP_HAIR].NewDrawableNumber = 1
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 4
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_GUNRUNNING_PED_H_RESEARCHING(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	UNUSED_PARAMETER(bMale)
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 1
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 2
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 3
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weapwork_01"))

			pedData.pcsComponents[PED_COMP_HEAD].NewDrawableNumber = 1
			
			pedData.pcsComponents[PED_COMP_TORSO].NewDrawableNumber = 7
			
			pedData.pcsComponents[PED_COMP_LEG].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_LEG].NewTextureNumber = 2
					
			pedData.pcsComponents[PED_COMP_SPECIAL].NewDrawableNumber = 4
			pedData.pcsComponents[PED_COMP_SPECIAL].NewTextureNumber = 2
			
			pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 3
			pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_WEIGH_METH_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.ppsProps[1].iPropIndex = 0
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.ppsProps[1].iPropIndex = 0
			
		BREAK		
		
	ENDSWITCH
ENDPROC

PROC GET_PED_SET_INSPECT_SPRAY_WEED_DEFAULT_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[9].NewDrawableNumber = 1
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_weed_01"))
			pedData.pcsComponents[2].NewDrawableNumber = 1
			
		BREAK	
	ENDSWITCH
ENDPROC

PROC GET_PED_SET_INSPECT_SPRAY_WEED_UPGRADE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
//			pedData.pcsComponents[2].NewTextureNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[4].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewTextureNumber = 1
			
			
			
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_PED_SET_SORTING_WEED_DEFAULT_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 3
			pedData.pcsComponents[0].NewTextureNumber = 0
			pedData.pcsComponents[3].NewDrawableNumber = 4
			pedData.pcsComponents[3].NewTextureNumber = 0
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewTextureNumber = 0
			pedData.pcsComponents[5].NewDrawableNumber = 1
			pedData.pcsComponents[5].NewTextureNumber = 0
			pedData.pcsComponents[8].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewTextureNumber = 0
		
			pedData.pcsComponents[9].NewDrawableNumber = 1
			
			
			pedData.ppsProps[0].iPropIndex = 1
			pedData.ppsProps[0].iTexIndex = 0
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 2
			
			pedData.pcsComponents[3].NewDrawableNumber = 3
			
			
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewTextureNumber = 1
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
			pedData.pcsComponents[9].NewDrawableNumber = 1
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_PED_SET_SORTING_WEED_UPGRADE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewTextureNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 2
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
			pedData.pcsComponents[4].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
			
			
			
			pedData.ppsProps[0].iPropIndex = 0
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
			
			pedData.pcsComponents[9].NewDrawableNumber = 1
			
		BREAK	
	ENDSWITCH
ENDPROC

PROC GET_INSPECT_SPRAY_WEED_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewTextureNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 2
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
			pedData.pcsComponents[4].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
			
		BREAK
		CASE 3
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 2
			
			pedData.pcsComponents[3].NewDrawableNumber = 3
			
			
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
		BREAK
		CASE 4
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_weed_01"))
			pedData.pcsComponents[2].NewDrawableNumber = 1
			
		BREAK		
		
	ENDSWITCH
	
	
ENDPROC

PROC GET_SORTING_WEED_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 3
			pedData.pcsComponents[3].NewDrawableNumber = 4
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[8].NewDrawableNumber = 1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_weed_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[2].NewTextureNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[4].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewTextureNumber = 1
			
		BREAK
		
	ENDSWITCH
	
	
ENDPROC

PROC GET_FORGE_PHOTO_MODEL_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			
			pedData.pcsComponents[0].NewTextureNumber = 1
			
			pedData.pcsComponents[2].NewDrawableNumber= 1
			pedData.pcsComponents[3].NewDrawableNumber= 1
			pedData.pcsComponents[4].NewDrawableNumber= 1
			
			pedData.pcsComponents[8].NewDrawableNumber= 1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			
			pedData.pcsComponents[0].NewTextureNumber = 1
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[2].NewDrawableNumber= 2
			pedData.pcsComponents[3].NewDrawableNumber= 2
			pedData.pcsComponents[4].NewDrawableNumber= 3
			
			pedData.pcsComponents[8].NewDrawableNumber= 1
			
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 2
			
			pedData.pcsComponents[2].NewDrawableNumber= 4
			pedData.pcsComponents[3].NewDrawableNumber= 3
			pedData.pcsComponents[4].NewDrawableNumber= 4
			
		BREAK
		
		CASE 3
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[2].NewDrawableNumber= 3
			pedData.pcsComponents[3].NewDrawableNumber= 2
			pedData.pcsComponents[3].NewTextureNumber= 1
			pedData.pcsComponents[4].NewDrawableNumber= 2
			
			pedData.pcsComponents[8].NewDrawableNumber= 1
			
		BREAK
		
		
	ENDSWITCH
	
	
ENDPROC

PROC GET_FORGE_PHOTOGRAPHER_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))

			pedData.pcsComponents[3].NewDrawableNumber= 1
			pedData.pcsComponents[4].NewDrawableNumber= 1
			pedData.pcsComponents[4].NewTextureNumber= 1
			pedData.pcsComponents[5].NewDrawableNumber= 1
						
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FAKE_MONEY_MACHINE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))

			pedData.pcsComponents[2].NewDrawableNumber= 2
			pedData.pcsComponents[5].NewTextureNumber= 2
			pedData.pcsComponents[11].NewDrawableNumber= 1		
			
			pedData.ppsProps[0].iPropIndex = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FAKE_MONEY_CHOP_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))

			pedData.pcsComponents[0].NewDrawableNumber= 1
			pedData.pcsComponents[0].NewTextureNumber= 0
			pedData.pcsComponents[2].NewDrawableNumber= 2
			pedData.pcsComponents[3].NewDrawableNumber= 1
			pedData.pcsComponents[4].NewDrawableNumber= 1
			pedData.pcsComponents[5].NewTextureNumber= 2
			pedData.pcsComponents[11].NewDrawableNumber= 1	
			
			pedData.ppsProps[0].iPropIndex = 1
			pedData.ppsProps[0].iTexIndex = 0
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_PED_SET_FORGE_DEFAULT_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 2
			
			pedData.pcsComponents[3].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
			pedData.ppsProps[1].iPropIndex = 2
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			pedData.pcsComponents[0].NewTextureNumber = 1
	
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
			pedData.ppsProps[1].iPropIndex = 2
			
		BREAK
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
			
			pedData.ppsProps[1].iPropIndex = 0

		BREAK
	ENDSWITCH
ENDPROC

PROC GET_PED_SET_FORGE_UPGRADE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[5].NewDrawableNumber = 1		
			
			pedData.ppsProps[1].iPropIndex = 2
		BREAK
		
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 4
			
			pedData.pcsComponents[3].NewDrawableNumber = 4
			pedData.pcsComponents[4].NewDrawableNumber = 4
			pedData.pcsComponents[5].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewDrawableNumber = 2
			
			pedData.ppsProps[1].iPropIndex = 3
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 3
			pedData.pcsComponents[2].NewDrawableNumber = 5
			pedData.pcsComponents[3].NewDrawableNumber = 4
			pedData.pcsComponents[4].NewDrawableNumber = 5
			pedData.pcsComponents[8].NewDrawableNumber = 2

		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FORGE_ID_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 2
			
			pedData.pcsComponents[3].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			pedData.pcsComponents[0].NewTextureNumber = 1
	
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
			pedData.pcsComponents[5].NewDrawableNumber = 1
			
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_forgery_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[5].NewDrawableNumber = 1
		BREAK
		
		CASE 3
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_M_Indian_01"))
			
			pedData.pcsComponents[0].NewTextureNumber = 1
			
			pedData.pcsComponents[4].NewTextureNumber = 1
		BREAK
		
		CASE 4
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_forgery_01"))
			
			pedData.pcsComponents[8].NewDrawableNumber = 1

		BREAK
		
		CASE 5
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_F_M_EastSA_02"))
			
			pedData.pcsComponents[0].NewTextureNumber = 2
			
			pedData.pcsComponents[2].NewDrawableNumber = 1
			pedData.pcsComponents[2].NewTextureNumber = 2
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[3].NewTextureNumber = 2
			
			pedData.pcsComponents[4].NewTextureNumber = 4
			
			pedData.pcsComponents[8].NewTextureNumber = 1

		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC GET_PACK_MONITOR_COKE_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 5
			
			pedData.pcsComponents[2].NewDrawableNumber = 2
			pedData.pcsComponents[3].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewTextureNumber = 1
			
			pedData.pcsComponents[6].NewDrawableNumber = 2
			pedData.pcsComponents[6].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 2
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 4
			
			pedData.pcsComponents[2].NewDrawableNumber = 2
			pedData.pcsComponents[3].NewDrawableNumber = 2
			pedData.pcsComponents[4].NewDrawableNumber = 2
			
			
			pedData.pcsComponents[6].NewDrawableNumber = 2
			
			
			pedData.pcsComponents[8].NewDrawableNumber = 2
			
		BREAK
		
	ENDSWITCH
	
	
ENDPROC

PROC GET_MONITOR_METH_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
		
			pedData.pcsComponents[0].NewTextureNumber = 1
			
			pedData.pcsComponents[2].NewDrawableNumber = 1
			
			pedData.pcsComponents[7].NewDrawableNumber = 1
			
			pedData.ppsProps[1].iPropIndex = 0
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewDrawableNumber = 1
			pedData.ppsProps[1].iPropIndex = 0
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
		BREAK
		
		
	ENDSWITCH
	
	
ENDPROC

PROC GET_MONITOR_METH_PED_DEFUALT(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 0
			pedData.pcsComponents[0].NewTextureNumber = 1
			
			pedData.pcsComponents[2].NewDrawableNumber = 1
			pedData.pcsComponents[2].NewTextureNumber = 0
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewTextureNumber = 0
		BREAK
		
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			pedData.pcsComponents[0].NewTextureNumber = 0
			
			pedData.pcsComponents[2].NewDrawableNumber = 0
			pedData.pcsComponents[2].NewTextureNumber = 0
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewTextureNumber = 0
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			pedData.pcsComponents[0].NewTextureNumber = 0
			
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[3].NewTextureNumber = 1
			
			pedData.pcsComponents[8].NewDrawableNumber = 1
			pedData.pcsComponents[8].NewTextureNumber = 0
		BREAK

	ENDSWITCH
	
	
ENDPROC


PROC GET_MONITOR_MONEY_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	iVar = iVar
//	SWITCH iVar
//		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("s_m_m_latHandy_01"))
			
			pedData.pcsComponents[0].NewTextureNumber = 1
			pedData.pcsComponents[2].NewTextureNumber = 1
			pedData.pcsComponents[4].NewTextureNumber = 2

//		BREAK
				
//	ENDSWITCH
	
	
ENDPROC

PROC GET_PRINTING_CUTTING_MONEY_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
			
			pedData.pcsComponents[0].NewDrawableNumber = 1
			pedData.pcsComponents[0].NewTextureNumber = 0
			pedData.pcsComponents[3].NewDrawableNumber = 1
			pedData.pcsComponents[4].NewDrawableNumber = 1
			
			pedData.pcsComponents[11].NewDrawableNumber = 1
//			pedData.pcsComponents[12].NewTextureNumber = 1
//			pedData.pcsComponents[12].NewDrawableNumber = 1
			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
			pedData.pcsComponents[11].NewDrawableNumber = 1
		
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			pedData.pcsComponents[0].NewTextureNumber = 0
			pedData.pcsComponents[11].NewDrawableNumber = 1
		BREAK
		
		
	ENDSWITCH
	
	
ENDPROC

PROC GET_COUNTING_DRYING_MONEY_PED(PED_DATA &pedData, INT iVar, BOOL bMale = FALSE)
	bMale = bMale
	SWITCH iVar
		CASE 0
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_counterfeit_01"))
			
			
			pedData.pcsComponents[3].NewDrawableNumber = 1

			
		BREAK
		CASE 1
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_counterfeit_01"))
			pedData.pcsComponents[0].NewDrawableNumber = 1
			
			pedData.pcsComponents[2].NewDrawableNumber = 1
			
			pedData.pcsComponents[4].NewDrawableNumber = 1
		
		BREAK
		
		CASE 2
			pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("S_F_Y_Sweatshop_01"))
			pedData.pcsComponents[0].NewTextureNumber = 1
					
			pedData.pcsComponents[2].NewDrawableNumber = 1
			pedData.pcsComponents[2].NewTextureNumber = 2
			
			pedData.pcsComponents[3].NewTextureNumber = 1
			pedData.pcsComponents[4].NewTextureNumber = 1
		BREAK
		
		
	ENDSWITCH
	
	
ENDPROC

PROC SETUP_COKE_UNPACK_CUT_LEFT(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

////	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01"))
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.bIsLocalScene = TRUE
//	
////	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
//
//	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
//	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 2
//	ActivityMain.pedData[0].pcsComponents[2].NewTextureNumber = 1
//	ActivityMain.pedData[0].pcsComponents[3].NewTextureNumber = 1
//	ActivityMain.pedData[0].pcsComponents[4].NewTextureNumber = 1
//	ActivityMain.pedData[0].pcsComponents[6].NewTextureNumber = 1
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_bakingsoda_o")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02")) 
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02"))
//	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02"))
//	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02"))
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "cokecutting"
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_cs_credit_card"))
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_cs_credit_card"))
	#ENDIF
	
//	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullscoop_01a"))
//	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullsieve_01a"))
	
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_COKE_UNPACK_CUT(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

//	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_f_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.activityCheck.bUpdateActivityPhysics = TRUE
	ActivityMain.activityCheck.fCapsuleRadius = 0.15
	
	// Unpacking Ped var 1
//	ActivityMain.pedData[1].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
//	ActivityMain.pedData[1].pcsComponents[0].NewDrawableNumber = 2
//	ActivityMain.pedData[1].pcsComponents[0].NewTextureNumber = 2
//	ActivityMain.pedData[1].pcsComponents[3].NewTextureNumber = 2
//	ActivityMain.pedData[1].pcsComponents[4].NewTextureNumber = 2
//	ActivityMain.pedData[1].pcsComponents[6].NewTextureNumber = 2
	
//	ActivityMain.pedData[1].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
//	ActivityMain.pedData[1].pcsComponents[2].NewTextureNumber = 2
	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	ActivityMain.bRandomiseAnimStartClip = TRUE

	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_bakingsoda_o")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk_o")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02")) 
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02")) 
//	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02"))
//	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02")) 
//	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_metalbowl_02")) 
//	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02")) 
//	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_02")) 

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "cokecutting"

	
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_cs_credit_card"))
	ActivityMain.objectModelName[10] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_cs_credit_card"))
	
	ActivityMain.objectModelName[11] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullscoop_01a"))
//	ActivityMain.objectModelName[12] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullsieve_01a"))
	
	ActivityMain.objectModelName[13] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullscoop_01a"))
	ActivityMain.objectModelName[14] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullmetalbowl_02"))
	
	ActivityMain.objectModelName[15] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_box_01a"))
	ActivityMain.objectModelName[16] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_box_01a"))
	
	ActivityMain.objectModelName[17] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_box_01a"))
	ActivityMain.objectModelName[18] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_box_01a"))
	#ENDIF

	
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_PACK_COKE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.bIsLocalScene = TRUE
		
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullmetalbowl_02")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_mold_01a")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_press_01aa")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullscoop_01a")) 
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_cutblock_01")) 
	#ENDIF
	
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_PACK_COKE_HIGH(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.bIsLocalScene = TRUE
		
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullscoop_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_doll")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_boxedDoll")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollCast")) 
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollmould")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_fullmetalbowl_02")) 
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollCast")) 
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollCast")) 
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollCast")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_press_01b")) 
	
	ActivityMain.objectModelName[10] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollboxfolded")) 
	ActivityMain.objectModelName[11] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollboxfolded")) 
	ActivityMain.objectModelName[12] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_dollboxfolded")) 
	#ENDIF
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_NOTE_COUNTING_V1(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.RUN_AUDIO_EFFECT_LOGIC 										= &runSpecificMoneyCounterAudio

	ActivityMain.pedData[0].mnPedModel = S_F_M_Sweatshop_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_clubhouse_chair_01")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_wrapped_01")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_unsorted_01")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_tin_01")) 
	
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_wrapped_01")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_unsorted_01")) 
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_tin_cash_01a")) 
	#ENDIF
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_NOTE_DRYING(ACTIVITY_MAIN &ActivityMain)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_pokerbucket")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "cokecutting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_unsorted_01")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_money_unsorted_01")) 
	#ENDIF
	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC


PROC SETUP_NOTE_CUTTING_BILLS(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"


	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_papercutter")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneypage")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneypage")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneypage")) 
//	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneypage")) 
	
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_table")) 
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystack_01a")) 
//	
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystrip")) 
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystrip")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystrip")) 
	ActivityMain.objectModelName[10] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystrip")) 
	ActivityMain.objectModelName[11] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystrip")) 
//	
	ActivityMain.objectModelName[12] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_singlestack_01a")) 
	#ENDIF
	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_NOTE_CUTTING_BILLS_MACHINE_ONLY(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

//	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"


	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_papercutter")) 

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_table")) 
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_cutter_moneystack_01a")) 
	#ENDIF

	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_SMOKING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

//	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ng_proc_cigarette01a")) 
	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_SMOKING_JOINT(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

//	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_papercutter")) 
	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SET_WAREHOUSE_MECHANIC_PED_DATA(ACTIVITY_MAIN &ActivityMain)
	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 	
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 2
	
	ActivityMain.pedData[0].pcsComponents[2].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[2].NewTextureNumber = 2
	
	ActivityMain.pedData[0].pcsComponents[3].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[3].NewTextureNumber  = 1
	
	ActivityMain.pedData[0].pcsComponents[4].NewTextureNumber  = 3
	
	ActivityMain.pedData[0].pcsComponents[PED_COMP_JBIB].NewDrawableNumber  = 2
ENDPROC

PROC SETUP_MECHANIC_STAND_1(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE


	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
//	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_papercutter")) 
	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_MECHANIC_STAND_CAR(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

//	ActivityMain.pedData[0].mnPedModel = S_M_M_DockWork_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("TAILGATER")) 
	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_MECHANIC_JANITOR(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("TAILGATER")) 

ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	ActivityMain.pedData[0].vPos = <<969.3016, -2999.4626, -40.6471>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.1876>>

	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC



PROC SETUP_SCENARIO_HANGING_OUT(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("TAILGATER")) 

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	ActivityMain.pedData[0].vPos = <<965.6154, -3002.2148, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 147.9137>>

	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_SCENARIO_SMOKING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = FALSE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("TAILGATER")) 

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

	
	ActivityMain.iEndAnimID = 4
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_SCENARIO_HAMMERING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC

PROC SETUP_SCENARIO_WELDING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 	
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.pedData[0].pcsComponents[2].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[2].NewTextureNumber = 2
	ActivityMain.pedData[0].pcsComponents[3].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[3].NewTextureNumber  = 1
	ActivityMain.pedData[0].pcsComponents[4].NewTextureNumber  = 3
	ActivityMain.pedData[0].pcsComponents[PED_COMP_JBIB].NewDrawableNumber  = 2
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC

PROC SETUP_SCENARIO_DRILLING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC

PROC SETUP_SCENARIO_MAP(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC

PROC SETUP_SCENARIO_MOBILE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC

PROC SETUP_SCENARIO_COFFEE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC

PROC SETUP_MECHANIC_ONBACK(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_SCENARIO_ANIM

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("G_F_Y_Vagos_01")) 
	
	
	
	ActivityMain.pedData[0].vPos = <<965.4486, -3003.4309, -40.6349>>
	ActivityMain.pedData[0].vRot = <<0, 0, 351.1694>>

ENDPROC


PROC SETUP_BUNKER_LATHE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<902.481, -3228.718, -99.289>>
	ActivityMain.pedData[0].vRot = <<0, 0, 81.250>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	

	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_lathe_01a")) 
	
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_lathe_01a")) 
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_NONSCENARIO_COFFEE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<902.481, -3228.718, -99.289>>
	ActivityMain.pedData[0].vRot = <<0, 0, 81.250>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	

	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_lathe_01a")) 
//	
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_lathe_01a")) 
////	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_LATHE_HI(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<902.481, -3228.718, -99.289>>
	ActivityMain.pedData[0].vRot = <<0, 0, 81.250>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
	
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_lathe_01b")) 
		
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_lathe_01a")) 
	#ENDIF
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC
PROC SETUP_BUNKER_LATHE_HI_RES(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<902.481, -3228.718, -99.289>>
	ActivityMain.pedData[0].vRot = <<0, 0, 81.250>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	

	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_2stackcrate_01a")) 

	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_lathe_01c")) 
	
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_lathe_01a")) 
	#ENDIF
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_SPEED_DRILL(ACTIVITY_MAIN &ActivityMain, BOOL bCrate = FALSE)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<893.334, -3226.037, -99.247>>
	ActivityMain.pedData[0].vRot = <<0, 0, 180.000>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	IF bCrate = FALSE
		IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended < 50
			ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a"))
		ELSE
			ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 
		ENDIF
		
		#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
		IF GET_FACTORY_PRODUCT_TOTAL_AS_PERCENTAGE(GET_BUNKER_PLAYER_IS_IN(PLAYER_ID()), globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) < 50
			ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a"))
		ELSE
			ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 	
		ENDIF
		#ENDIF

//		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 
//		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 
	ELSE
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a")) 
		#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a")) 
		#ENDIF
	ENDIF
	 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_BUNKER_SPEED_DRILL: UPGRADE_ID_EQUIPMENT = FALSE, using gr_prop_gr_speeddrill_01a")
	
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_speeddrill_01a"))
	
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_drill_01a")) 
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC
//PROC SETUP_BUNKER_SPEED_DRILL_CRATE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
//
//	// Activity should be triggered by player prompt triggered by angled area
//	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
//	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
//	// Generic help text
//	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
//	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
//		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""
//
//	// Enter input logic
//	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
//
//	// Is player in correct location
//	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
//
//
//	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
//
//	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
//
//	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
//	ActivityMain.pedData[0].vPos = <<893.334, -3226.037, -99.247>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 180.000>>
//	ActivityMain.bIsLocalScene = TRUE
//	
//	ActivityMain.animState[0].tlStateName = "AwakeState"
//	
//	ActivityMain.bRandomiseAnimStartClip = TRUE
//	
//	ActivityMain.iEndAnimID = 10
//	ActivityMain.iStartAnimID = 0
//
//	
//	
//	
//	 
//	
//	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_BUNKER_SPEED_DRILL: UPGRADE_ID_EQUIPMENT = FALSE, using gr_prop_gr_speeddrill_01a")
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_speeddrill_01a"))
//	
//	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_drill_01a")) 
//
//
//	ActivityMain.bPropVisibilitySwap = FALSE
//
//ENDPROC

PROC SETUP_BUNKER_SPEED_DRILL_HI(ACTIVITY_MAIN &ActivityMain, BOOL bCrate = FALSE)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<893.334, -3226.037, -99.247>>
	ActivityMain.pedData[0].vRot = <<0, 0, 180.000>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	
	IF bCrate = FALSE
		IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended < 50
			ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a"))
		ELSE
			ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 
		ENDIF
		#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
		IF GET_FACTORY_PRODUCT_TOTAL_AS_PERCENTAGE(GET_BUNKER_PLAYER_IS_IN(PLAYER_ID()), globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) < 50
			ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a"))
		ELSE
			ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 	
		ENDIF
		#ENDIF
	ELSE
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a")) 
		#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a")) 
		#ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_BUNKER_SPEED_DRILL: UPGRADE_ID_EQUIPMENT = TRUE, using gr_prop_gr_speeddrill_01b")
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_speeddrill_01b"))
		
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_drill_01a")) 
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_SPEED_DRILL_RES_HI(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<893.334, -3226.037, -99.247>>
	ActivityMain.pedData[0].vRot = <<0, 0, 180.000>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_drillcage_01a")) 
	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_drillcrate_01a")) 
	
	 
	
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_BUNKER_SPEED_DRILL: UPGRADE_ID_EQUIPMENT = TRUE, using gr_prop_gr_speeddrill_01c")
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_speeddrill_01c"))
		
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_drill_01a")) 
	#ENDIF
	
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_VERTICAL_MILL(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<902.525, -3214.994, -99.230>>
	ActivityMain.pedData[0].vRot = <<0, 0, -10.000>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_millcrate_01a")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_millcrate_01a")) 
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended < 50
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_millcrate_01a"))
	ELSE
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_millcage_01a")) 
	ENDIF

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	IF GET_FACTORY_PRODUCT_TOTAL_AS_PERCENTAGE(GET_BUNKER_PLAYER_IS_IN(PLAYER_ID()), globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) < 50
		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_millcrate_01a"))
	ELSE
		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_millcage_01a")) 	
	ENDIF
	

	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_BUNKER_VERTICAL_MILL: UPGRADE_ID_EQUIPMENT = FALSE, using gr_prop_gr_vertmill_01a")
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_vertmill_01a"))  
	
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_mill_01a")) 
	#ENDIF
	
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_VERTICAL_MILL_HI(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_counterfeit_01"))
	ActivityMain.pedData[0].vPos = <<902.525, -3214.994, -99.230>>
	ActivityMain.pedData[0].vRot = <<0, 0, -10.000>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iMaterialsTotalBlended < 50
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_millcrate_01a"))
	ELSE
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_millcage_01a")) 
	ENDIF
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	IF GET_FACTORY_PRODUCT_TOTAL_AS_PERCENTAGE(GET_BUNKER_PLAYER_IS_IN(PLAYER_ID()), globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner) < 50
		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_3s_millcrate_01a"))
	ELSE
		ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_millcage_01a")) 	
	ENDIF

	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_BUNKER_VERTICAL_MILL: UPGRADE_ID_EQUIPMENT = TRUE, using gr_prop_gr_vertmill_01b")
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_vertmill_01b")) 
		
	
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_part_mill_01a")) 
	#ENDIF
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_BLUE_PRINTS(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_mug_04")) 



	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_ASSEMBLE_GUNS(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	


	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_LOAD_CLIPS(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	


	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_SHOOTING_RANGE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0
	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_notepad_02"))
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_assaultrifle_mag1"))
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_assaultrifle"))
	#ENDIF
	
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_SHOOTING_RANGE_FULL(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_notepad_02"))
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_assaultrifle_mag1"))
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_assaultrifle"))
	#ENDIF
//	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_magspile_01a"))
	
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_BUNKER_SUPERVISING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a"))
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_CHASSIS_REPAIR(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	


	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_STENOGRAPHER_PAPER(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_laptop_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01"))
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("v_ilev_chair02_ped"))
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_STENOGRAPHER_COMPUTER(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_GR_Chair02_ped")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_laptop_01c"))
	#ENDIF
	

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_RESEARCH_COMPUTER(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
	UNUSED_PARAMETER(activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	activityCheck.bLocalScript = ActivityMain.bIsLocalScene
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_GR_Chair02_ped")) 
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xm_prop_x17_corp_offchair")) 
	ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC
PROC SETUP_BUNKER_SECURITY_CAM_MONITOR(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
	UNUSED_PARAMETER(activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	activityCheck.bLocalScript = ActivityMain.bIsLocalScene
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0
	ActivityMain.bHeadTracking = TRUE
	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xm_prop_x17_corp_offchair")) 
//	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
//		ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("xm_prop_x17_corp_offchair")) 
//	ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_WHILEBOARD(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	ActivityMain.eActivity 																= ACT_CREATOR_BUNKER_WHILEBOARD


	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_ASSEMBLE_GUN(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_GR_Chair02_ped")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_carbineriflemk2"))
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_carbineriflemk2_mag1"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_LOAD_CLIP(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY

	// Enter input logic


	// Is player in correct location



	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_bulletscrate_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_crate_mag_01a"))
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_gr_crate_mag_01a"))
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("gr_prop_GR_Chair02_ped"))
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_assaultriflemk2_mag1"))
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_ar_carbineriflemk2_mag1"))
	#ENDIF
	
	ActivityMain.bPropVisibilitySwap = FALSE

ENDPROC

PROC SETUP_BUNKER_TURRET_SEAT(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_BUNKER_TURRET_HELP_TEXT
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_IAA_TURRET_HELP_TEXT
	ENDIF
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
//	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"


	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_BUNKER_TURRET
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_DEFUNCT_BASE_TURRET
	ENDIF
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 1.0000

	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_highendchair_gr_01a"))
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair_02"))
	ENDIF
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	IF ActivityMain.eActivity = ACT_CREATOR_IAA_TURRET_SEAT
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_GR_Console_01"))
	ELSE
		ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
	ENDIF
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_TURRET_SEAT
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC
			
PROC SETUP_BUNKER_TURRET_SEAT_LEFT(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_BUNKER_TURRET_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_DEFUNCT_BASE_TURRET_HELP_TEXT
	ENDIF
	
	//ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"
	

	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_BUNKER_TURRET
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_DEFUNCT_BASE_TURRET
	ENDIF
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 1.0000

	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_highendchair_gr_01a"))
	IF IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair_02"))
	ENDIF
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	IF ActivityMain.eActivity = ACT_CREATOR_IAA_TURRET_SEAT
	OR IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_IAABASE_FINALE)
		ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_GR_Console_01"))
	ELSE
		ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
	ENDIF
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_TURRET_SEAT_LEFT
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC

PROC SETUP_OSPREY_TURRET_SEAT(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_AVENGER
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_OSPREY_TURRET_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
//	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"


	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_OSPREY_TURRET
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 1.0000

	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_GR_Console_01"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC

PROC SETUP_HACKER_TRUCK_SEAT_1(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR Activity)
			
	// vSceneRot Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_DRONE_STATION
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HACKER_TRUCK_SEAT_1_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
//	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"


	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_HACKER_TRUCK_SEAT_1
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<-1422.454590,-3011.960449,-80.249947>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<-1420.889648,-3012.029297,-77.749947>> 
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.250000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 1.250000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= 57.3881
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vDymension.z	= 63.4207
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.3
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Control_Seat"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Control_Console"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

	ActivityMain.eActivity 																= ACT_CREATOR_HACKER_TRUCK_SEAT_1
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC

PROC SETUP_HACKER_TRUCK_SEAT_2(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_MISSILE_TURRET_STATION
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HACKER_TRUCK_SEAT_2_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
//	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"


	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_HACKER_TRUCK_SEAT_2
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		=  <<-1419.761841,-3010.001465,-80.249947>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		=  <<-1421.312988,-3009.952881,-77.749947>> 
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.000000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 1.000000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= 269.7835
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vDymension.z	= 269.7835
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.3
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Control_Seat"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("ba_Prop_Battle_Control_Console"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

	ActivityMain.eActivity 																= ACT_CREATOR_HACKER_TRUCK_SEAT_2
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC

#IF FEATURE_HEIST_ISLAND
PROC SETUP_SUBMARINE_REMOTE_MISSILE_SEAT_1(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_MISSILE_TURRET_STATION
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_SUBMARINE_SEAT_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
//	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"


	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_HACKER_TRUCK_SEAT_1
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1559.606201,386.689117,-50.685444>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1558.005371,386.653229,-48.685440>> 
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vAngledAreaA 		= <<1559.578979,387.542145,-50.685368>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vAngledAreaB 		= <<1557.988037,387.493195,-48.685364>>
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 0.75
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 0.75
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= 24.9220
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vDymension.z	= 130.6346
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.3
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("h4_int_lev_sub_chair_02"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Console_01a"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

	ActivityMain.eActivity 																= ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC

PROC SETUP_SUBMARINE_REMOTE_MISSILE_SEAT_2(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_MISSILE_TURRET_STATION
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_SUBMARINE_SEAT_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
//	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"


	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_HACKER_TRUCK_SEAT_2
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1557.953979,388.409485,-50.685425>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1559.570068,388.440460,-48.685429>> 
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vAngledAreaA 		= <<1559.487427,389.331360,-50.685501>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vAngledAreaB 		= <<1557.971680,389.306274,-48.685501>> 
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 0.9
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 0.75
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= 24.9220
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].vDymension.z	= 130.6346
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.3
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("h4_int_lev_sub_chair_02"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("h4_Prop_h4_Console_01a"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

	ActivityMain.eActivity 																= ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
	
	ActivityMain.bCanUseTurrets															= TRUE
ENDPROC
#ENDIF

PROC RUN_AUDIO_LOGIC_FOR_FROSTED_GLASS(ACTIVITY_ANIM_SEQUENCE &activityAnimSequence)
	IF IS_BIT_SET(activityAnimSequence.iBSGlassFrost, ciBS_FROSTED_GLASS_ACT_HAS_ACTIVATED)
	AND NOT IS_BIT_SET(activityAnimSequence.iBSGlassFrost, ciBS_FROSTED_GLASS_ACT_HAS_PLAYED_AUDIO)		
		SET_BIT(activityAnimSequence.iBSGlassFrost, ciBS_FROSTED_GLASS_ACT_HAS_PLAYED_AUDIO)
		IF g_SimpleInteriorData.bToggleFrostedGlass
			PLAY_SOUND_FROM_COORD(-1, "Activate_Privacy_Glass", <<367.317, 4846.754, -58.448>>, "dlc_xm_facility_ambient_sounds")
		ELSE 
			PLAY_SOUND_FROM_COORD(-1, "Deactivate_Privacy_Glass", <<367.317, 4846.754, -58.448>>, "dlc_xm_facility_ambient_sounds")	
		ENDIF
	
	ENDIF
ENDPROC

PROC SETUP_RADIO_ACTIVITY(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_GLASS_FROST
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
	IF g_SimpleInteriorData.bToggleFrostedGlass
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPFROST_PMT2" // Press ~INPUT_CONTEXT~ to unfrost Privacy Glass.
	ELSE
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPFROST_PMT" // Press ~INPUT_CONTEXT~ to frost Privacy Glass.
	ENDIF

	ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &RUN_AUDIO_LOGIC_FOR_FROSTED_GLASS
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= -1
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.35
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.iStartAnimID 															= 0
	ActivityMain.iEndAnimID 															= 1
	// Handle Audio
	//ActivityMain.loadForActivity.sAudioBankName 										= "dlc_xm_facility_ambient_sounds"//"dlc_xm_facility_ambient_sounds"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
	
//	// Activity Props
//	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
//	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
//	#ENDIF
//	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT
	
ENDPROC

PROC RUN_AUDIO_LOGIC_FOR_FROSTED_GLASS_QUARTERS(ACTIVITY_ANIM_SEQUENCE &activityAnimSequence)
	IF IS_BIT_SET(activityAnimSequence.iBSGlassFrost, ciBS_FROSTED_GLASS_ACT_HAS_ACTIVATED)
	AND NOT IS_BIT_SET(activityAnimSequence.iBSGlassFrost, ciBS_FROSTED_GLASS_ACT_HAS_PLAYED_AUDIO)		
		SET_BIT(activityAnimSequence.iBSGlassFrost, ciBS_FROSTED_GLASS_ACT_HAS_PLAYED_AUDIO)
		IF g_SimpleInteriorData.bQuartersFrostedGlass
			PLAY_SOUND_FROM_COORD(-1, "Activate_Privacy_Glass", <<372.1150, 4827.5039, -58.470>>, "dlc_xm_facility_ambient_sounds")
		ELSE 
			PLAY_SOUND_FROM_COORD(-1, "Deactivate_Privacy_Glass", <<372.1150, 4827.5039, -58.470>>, "dlc_xm_facility_ambient_sounds")	
		ENDIF
	
	ENDIF
ENDPROC

PROC SETUP_RADIO_ACTIVITY2(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_GLASS_FROST
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
//	
	IF g_SimpleInteriorData.bQuartersFrostedGlass
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPFROST_PMT2" // Press ~INPUT_CONTEXT~ to unfrost Privacy Glass.
	ELSE
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPFROST_PMT" // Press ~INPUT_CONTEXT~ to frost Privacy Glass.
	ENDIF

	ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &RUN_AUDIO_LOGIC_FOR_FROSTED_GLASS_QUARTERS
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= -1
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.iStartAnimID 															= 0
	ActivityMain.iEndAnimID 															= 1
	// Handle Audio
	//ActivityMain.loadForActivity.sAudioBankName 										= "dlc_xm_facility_ambient_sounds"//"dlc_xm_facility_ambient_sounds"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
	
//	// Activity Props
//	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
//	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
//	#ENDIF
//	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT
	
ENDPROC

PROC SETUP_RADIO_ACTIVITY3(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_GLASS_FROST
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_FROSTLIGHT
//	
	IF g_SimpleInteriorData.bNightclubFrostedGlass
		IF IS_PLAYERS_NIGHTCLUB_OFFICE_LIGHTS_DIM(PLAYER_ID())
		//	Press ~INPUT_CONTEXT~ to deactivate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn on Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FROSTLIGHT_PMT2"
		ELSE
		//	Press ~INPUT_CONTEXT~ to deactivate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn off Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FROSTLIGHT_PMT4"
		ENDIF
	ELSE
		IF IS_PLAYERS_NIGHTCLUB_OFFICE_LIGHTS_DIM(PLAYER_ID())
		//	Press ~INPUT_CONTEXT~ to activate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn on Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText		= "FROSTLIGHT_PMT"
		ELSE
		//	Press ~INPUT_CONTEXT~ to activate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn off Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FROSTLIGHT_PMT3"
		ENDIF
	ENDIF

	ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &RUN_AUDIO_LOGIC_FOR_FROSTED_GLASS_NIGHTCLUB
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= 270
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.iStartAnimID 															= 0
	ActivityMain.iEndAnimID 															= 1
	// Handle Audio
	//ActivityMain.loadForActivity.sAudioBankName 										= "dlc_xm_facility_ambient_sounds"//"dlc_xm_facility_ambient_sounds"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
	
//	// Activity Props
//	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
//	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
//	#ENDIF
//	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT
	
ENDPROC

PROC SETUP_RADIO_ACTIVITY4(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_GLASS_FROST
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_FROSTLIGHT
	
	IF g_SimpleInteriorData.bNightclubFrostedGlass
		IF IS_PLAYERS_NIGHTCLUB_OFFICE_LIGHTS_DIM(PLAYER_ID())
		//	Press ~INPUT_CONTEXT~ to deactivate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn on Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FROSTLIGHT_PMT2"
		ELSE
		//	Press ~INPUT_CONTEXT~ to deactivate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn off Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FROSTLIGHT_PMT4"
		ENDIF
	ELSE
		IF IS_PLAYERS_NIGHTCLUB_OFFICE_LIGHTS_DIM(PLAYER_ID())
		//	Press ~INPUT_CONTEXT~ to activate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn on Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText		= "FROSTLIGHT_PMT"
		ELSE
		//	Press ~INPUT_CONTEXT~ to activate Privacy Glass.~n~Press ~INPUT_CONTEXT~ to turn off Lights.
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FROSTLIGHT_PMT3"
		ENDIF
	ENDIF

	ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &RUN_AUDIO_LOGIC_FOR_OFFICE_LIGHT_NIGHTCLUB
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC				= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT_SECONDARY

	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z	= 270
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.iStartAnimID 															= 0
	ActivityMain.iEndAnimID 															= 1
	// Handle Audio
	//ActivityMain.loadForActivity.sAudioBankName 										= "dlc_xm_facility_ambient_sounds"//"dlc_xm_facility_ambient_sounds"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
	
//	// Activity Props
//	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
//	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
//	#ENDIF
//	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT
	
ENDPROC

PROC SETUP_LAPTOP_TRIGGER_ACTIVITY(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_LAPTOP_TRIGGER
	 
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "BROWSEINPUTTRIG" // Press ~INPUT_CONTEXT~ to browse the internet on this computer.
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC				= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT
	
	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.iStartAnimID 															= 0
	ActivityMain.iEndAnimID 															= 2
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
//	// Activity Props
//	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
//	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
//	#ENDIF
//	ActivityMain.bPropVisibilitySwap 													= FALSE

//	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT
	
ENDPROC
			
PROC SETUP_OSPREY_TURRET_SEAT_LEFT(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity Trigger
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK_AVENGER
	
	// Generic Help Text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_OSPREY_TURRET_HELP_TEXT
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
	
	//ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "MPJAC_SIT"
	

	// Handle Input
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL_OSPREY_TURRET
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT

	// Area Checks
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[1].fWidth 			= 1.0000

	// Ped Tasking: Approach position using ped task go straight to coord. Could use 
	// navMesh instead here, if a nav mesh exists.
	ActivityMain.bShouldPlayerApproachActivity 											= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.2
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	// Asset Retrieval
	ActivityMain.bActivityServerProp 													= TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty 														= activityCheck.iPropertyID

	// Handle Animation
	ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.iEndAnimID 															= 3
	ActivityMain.iStartAnimID 															= 0
	
	// Handle Audio
//	ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
//	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
	// Activity Props
	ActivityMain.objectModelName[0] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_avengerchair"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] 													= INT_TO_ENUM(MODEL_NAMES, HASH("xm_Prop_GR_Console_01"))
	#ENDIF
	ActivityMain.bPropVisibilitySwap 													= FALSE

	ActivityMain.eActivity 																= ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
	
	ActivityMain.bCanUseTurrets															= TRUE
	
ENDPROC

PROC SETUP_NEW_WHEAT_GRASS_ENUM(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)//, VECTOR vScenePos, VECTOR vSceneRot)
				
	// Activity should be triggered by player prompt triggered by angled area
		ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
		// Generic help text
		ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
			ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHEAT"


		// Enter input logic
		ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
			ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
			ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

		// Is player in correct location
		ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1009.1285, -3166.8862, -35.0576>>
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1009.2790, -3165.9983, -33.0576>>
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

		// Approach position using ped task go straight to coord(could use navMesh instead here)
		ActivityMain.bShouldPlayerApproachActivity 									= TRUE
		ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
		ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
		ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

		ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
		ActivityMain.iCurrentProperty = activityCheck.iPropertyID

		ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
		
		ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHEATGRASS"
		ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
		
		
		ActivityMain.objectModelName[0] = p_w_grass_gls_s
		#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
//		ActivityMain.objectModelName[1] = p_cs_lighter_01
		#ENDIF
		ActivityMain.iEndAnimID = 1
		ActivityMain.iStartAnimID = 0

		ActivityMain.bPropVisibilitySwap = FALSE
		ActivityMain.bBlockRecording = TRUE

		ActivityMain.eActivity = NEW_WHEAT_GRASS_ENUM
		
		IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
			ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 240.0
		ENDIF
ENDPROC

PROC SETUP_AUTO_SHOP_WHEAT_GRASS(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "SA_WHEAT"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 		= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType	= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction	= INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1009.1285, -3166.8862, -35.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1009.2790, -3165.9983, -33.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHEATGRASS"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= TRUE
	
	ActivityMain.objectModelName[0] = p_w_grass_gls_s
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.bPropVisibilitySwap = FALSE
	ActivityMain.bBlockRecording = TRUE
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 90.0
	
	ActivityMain.eActivity = ACT_CREATOR_AUTO_SHOP_WHEAT_GRASS
ENDPROC

PROC SETUP_AUTO_SHOP_WHEAT_GRASS_2(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "SA_WHEAT"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 		= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType	= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction	= INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1009.1285, -3166.8862, -35.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1009.2790, -3165.9983, -33.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHEATGRASS"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= TRUE
	
	ActivityMain.objectModelName[0] = p_w_grass_gls_s
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.bPropVisibilitySwap = FALSE
	ActivityMain.bBlockRecording = TRUE
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = -90.0
	
	ActivityMain.eActivity = ACT_CREATOR_AUTO_SHOP_WHEAT_GRASS_2
ENDPROC

#IF FEATURE_FIXER
PROC SETUP_FIXER_HQ_GREEN_DRINK_JUNK(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FHQ_DRINK_JUNK"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 		= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType	= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction	= INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1009.1285, -3166.8862, -35.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1009.2790, -3165.9983, -33.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHEATGRASS"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= TRUE
	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("sf_p_sf_grass_gls_s_01a"))
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.bPropVisibilitySwap = FALSE
	ActivityMain.bBlockRecording = TRUE
				
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(275.811493,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 180.0	
	
	ActivityMain.eActivity = ACT_CREATOR_FIXER_HQ_GREEN_DRINK_JUNK
ENDPROC

PROC SETUP_FIXER_HQ_NEW_BONG(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACITIVITY: ACT_CREATOR_FIXER_HQ_NEW_BONG")
				
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BONG2"

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1006.6099, -3164.7461, -35.0921>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1006.7067, -3165.6909, -33.2738>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_USE_BONG"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
	
	ActivityMain.objectModelName[0] = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_bong_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = p_cs_lighter_01
	#ENDIF
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE

	ActivityMain.bBlockRecording = TRUE
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(227.348694,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 90.0
	
	ActivityMain.eActivity = ACT_CREATOR_FIXER_HQ_NEW_BONG
ENDPROC

PROC SETUP_FIXER_HQ_PINK_DRINK(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 		= "FHQ_DRINK_LIMEY"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 		= &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType	= FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction	= INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1009.1285, -3166.8862, -35.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1009.2790, -3165.9983, -33.0576>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHEATGRASS"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= TRUE
	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("sf_p_sf_grass_gls_s_02a"))
	
	ActivityMain.iEndAnimID = 1 
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.bPropVisibilitySwap = FALSE
	ActivityMain.bBlockRecording = TRUE
	
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(275.811493,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 180.0	
	
	ActivityMain.eActivity = ACT_CREATOR_FIXER_HQ_PINK_DRINK
ENDPROC
#ENDIF

PROC SETUP_BUNKER_RUM(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	
			
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_RUM"
	
	IF IS_PLAYER_DEFUNCT_BASE_LOUNGE_3_PURCHASED(g_ownerOfBasePropertyIAmIn)
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_GIN"
	ENDIF
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
	
	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.5208, -3146.4619, -38.0705>>
	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 182.0111>>

		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

		//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.1

	//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_03_S"))
	IF IS_PLAYER_DEFUNCT_BASE_LOUNGE_3_PURCHASED(g_ownerOfBasePropertyIAmIn)
		ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_02_S"))
	ENDIF
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = ex_p_ex_tumbler_03_empty
	IF IS_PLAYER_DEFUNCT_BASE_LOUNGE_3_PURCHASED(g_ownerOfBasePropertyIAmIn)
		ActivityMain.objectModelName[1] = ex_p_ex_tumbler_02_empty
	ENDIF
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE

	ActivityMain.eActivity = ACT_CREATOR_DEFUNCT_RUM
ENDPROC

PROC SETUP_BUNKER_WINE(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WINE"

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_WINE"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= FALSE
	
	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.5208, -3146.4619, -38.0705>>
	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 182.0111>>

		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

		//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.1

	//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

	
	ActivityMain.objectModelName[0] = prop_wine_bot_01
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = prop_wine_glass
	ActivityMain.objectModelName[2] = P_WINE_GLASS_S
	ActivityMain.objectModelName[3] = P_POUR_WINE_S
	

	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = TRUE
	ActivityMain.bObjectHiddenNotInUse[1] = FALSE
	ActivityMain.bObjectHiddenInUse[2] = FALSE
	ActivityMain.bObjectHiddenNotInUse[2] = TRUE
	ActivityMain.bObjectHiddenInUse[3] = FALSE
	ActivityMain.bObjectHiddenNotInUse[3] = TRUE
	
	#ENDIF
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0

	ActivityMain.eActivity = NEW_WINE_DRINK_ENUM
				
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 40.0
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(3.960510,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 253.9605
	#ENDIF
	ENDIF
ENDPROC

PROC SETUP_NIGHT_CLUB_RUM(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	
			
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_RUM"
	
	IF IS_PLAYER_NIGHTCLUB_STYLE_1_PURCHASED(GET_OWNER_OF_THIS_NIGHTCLUB())
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_VODKA"
	ELIF IS_PLAYER_NIGHTCLUB_STYLE_2_PURCHASED(GET_OWNER_OF_THIS_NIGHTCLUB())
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"
	ENDIF
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= FALSE
	
	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.5208, -3146.4619, -38.0705>>
	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 182.0111>>

		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

		//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.1

	//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_01_S"))
	IF IS_PLAYER_NIGHTCLUB_STYLE_1_PURCHASED(GET_OWNER_OF_THIS_NIGHTCLUB())
		ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_02_S"))
	ELIF IS_PLAYER_NIGHTCLUB_STYLE_2_PURCHASED(GET_OWNER_OF_THIS_NIGHTCLUB())
		ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_03_S"))
	ENDIF
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = ex_p_ex_tumbler_01_s
	ActivityMain.objectModelName[2] = ex_p_ex_tumbler_01_empty
	IF IS_PLAYER_NIGHTCLUB_STYLE_1_PURCHASED(GET_OWNER_OF_THIS_NIGHTCLUB())
		ActivityMain.objectModelName[1] = ex_p_ex_tumbler_02_s
		ActivityMain.objectModelName[2] = ex_p_ex_tumbler_02_empty
	ELIF IS_PLAYER_NIGHTCLUB_STYLE_2_PURCHASED(GET_OWNER_OF_THIS_NIGHTCLUB())
		ActivityMain.objectModelName[1] = ex_p_ex_tumbler_03_s
		ActivityMain.objectModelName[2] = ex_p_ex_tumbler_03_empty
	ENDIF

	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0

	ActivityMain.eActivity = ACT_CREATOR_NIGHT_CLUB_RUM
ENDPROC

PROC SETUP_NIGHT_CLUB_BEER(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1117.5385, -3158.6226, -38.062759>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1116.695654, -3159.302490,-36.562756>> 
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= FALSE

	ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACT_CREATOR_NIGHT_CLUB_BEER: ActivityMain.objectModelName[0] = prop_cs_beer_bot_01")
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
				
	ActivityMain.eActivity = ACT_CREATOR_NIGHT_CLUB_BEER
ENDPROC

PROC SETUP_ARENA_GARAGE_BEER(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"
		
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
		
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1117.5385, -3158.6226, -38.062759>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1116.695654, -3159.302490,-36.562756>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_FRANKLIN_DRINK_BEER"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
	
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACT_CREATOR_ARENA_GARAGE_BEER: ActivityMain.objectModelName[0] = prop_cs_beer_bot_01")
	
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.bPropVisibilitySwap = FALSE
	
	ActivityMain.eActivity = ACT_CREATOR_ARENA_GARAGE_BEER
ENDPROC

PROC SETUP_ARENA_GARAGE_RUM(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_RUM"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_03_S"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = ex_p_ex_tumbler_03_s
	ActivityMain.objectModelName[2] = ex_p_ex_tumbler_03_empty
	
	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.eActivity = ACT_CREATOR_NIGHT_CLUB_RUM
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 240.0
	ENDIF
ENDPROC

PROC SETUP_ARENA_GARAGE_WHISKEY(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = p_whiskey_bottle_s
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = EX_P_EX_TUMBLER_01_S
	ActivityMain.objectModelName[2] = EX_P_EX_TUMBLER_01_EMPTY
	
	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.eActivity = ACT_CREATOR_ARENA_GARAGE_WHISKEY
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 320.0
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())	
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(92.605988,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 342.6060
	#ENDIF
	ENDIF
ENDPROC

PROC SETUP_ARENA_GARAGE_WHISKEY_2(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Same as SETUP_ARENA_GARAGE_WHISKEY but with different heading

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = p_whiskey_bottle_s
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = EX_P_EX_TUMBLER_01_S
	ActivityMain.objectModelName[2] = EX_P_EX_TUMBLER_01_EMPTY
	
	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.eActivity = ACT_CREATOR_ARENA_GARAGE_WHISKEY_2
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	#IF FEATURE_TUNER
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 90.0
	#ENDIF
	ENDIF
ENDPROC

PROC SETUP_NIGHT_CLUB_VIP_WHISKEY(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
	
			
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity 									= TRUE
	ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= FALSE
	
	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.5208, -3146.4619, -38.0705>>
	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 182.0111>>

		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

		//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.1

	//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_03_S"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = ex_p_ex_tumbler_03_s
	ActivityMain.objectModelName[2] = ex_p_ex_tumbler_03_empty
	
	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
					
	#IF FEATURE_FIXER
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(175.405090,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 65.4051
	ENDIF
	#ENDIF

	ActivityMain.eActivity = ACT_CREATOR_NIGHT_CLUB_VIP_WHISKEY
ENDPROC

PROC SETUP_CASINO_BEER(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
	
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1117.5385, -3158.6226, -38.062759>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1116.695654, -3159.302490,-36.562756>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_FRANKLIN_DRINK_BEER"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
	
	ActivityMain.iEndAnimID = 2
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.bPropVisibilitySwap = FALSE
	
	ActivityMain.eActivity = ACT_CREATOR_CASINO_BEER
ENDPROC

PROC SETUP_CASINO_VODKA(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_VODKA"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
		
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_02_S"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = ex_p_ex_tumbler_02_s
	ActivityMain.objectModelName[2] = ex_p_ex_tumbler_02_empty
	
	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.eActivity = ACT_CREATOR_CASINO_VODKA
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 115.0
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())	
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(92.605988,  GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())) // World value at Hawick location 342.6060
	#ENDIF
	ENDIF
ENDPROC

PROC SETUP_CASINO_VODKA_2(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT activityCheck)
	// Same as SETUP_CASINO_VODKA() but with altered heading 

	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.SHOULD_ACTIVITY_LAUNCH = &AMBIENT_AREA_TRIGGER_CHECK
	
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC = &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC = &CLEAR_HELP_TEXT_GENERIC
	
	ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_VODKA"
	
	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC = &HAS_PLAYER_JUST_PRESSED_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType = FRONTEND_CONTROL
	ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
		
	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC = &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	// Approach position using ped task go straight to coord(could use navMesh instead here)
	ActivityMain.bShouldPlayerApproachActivity = TRUE
	ActivityMain.RUN_APPROACH_COMPONENT = &RUN_APPROACH_PED_TASK_COMPONENT
	ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.07500
	ActivityMain.approachComponentStruct.APPROACH_PED_TASK = &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
	
	ActivityMain.bActivityServerProp = TRUE
	ActivityMain.GET_ASSETS_FOR_ACTIVITY = &GRAB_ASSETS_FROM_SERVER
	ActivityMain.iCurrentProperty = activityCheck.iPropertyID
	
	ActivityMain.RUN_ANIM_SEQUENCE = &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
	ActivityMain.loadForActivity.sAudioBankName = "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
	ActivityMain.loadForActivity.bAudioBankLoadOverNetwork = FALSE
	
	ActivityMain.objectModelName[0] = INT_TO_ENUM( MODEL_NAMES, HASH("ba_Prop_Battle_Decanter_02_S"))
	
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] = ex_p_ex_tumbler_02_s
	ActivityMain.objectModelName[2] = ex_p_ex_tumbler_02_empty
	
	ActivityMain.bPropVisibilitySwap = TRUE
	ActivityMain.bObjectHiddenInUse[0] = FALSE
	ActivityMain.bObjectHiddenNotInUse[0] = FALSE
	ActivityMain.bObjectHiddenInUse[1] = FALSE
	ActivityMain.bObjectHiddenNotInUse[1] = TRUE
	ActivityMain.bObjectHiddenInUse[2] = TRUE
	ActivityMain.bObjectHiddenNotInUse[2] = FALSE
	#ENDIF
	
	ActivityMain.iEndAnimID = 1
	ActivityMain.iStartAnimID = 0
	
	ActivityMain.eActivity = ACT_CREATOR_CASINO_VODKA_2
	
	IF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	#IF FEATURE_TUNER
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vDymension.z = 90
	#ENDIF
	ENDIF
ENDPROC

PROC SETUP_WEED_INSPECT_HIGH(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
	
	ActivityMain.bRandomiseAnimStartClip = TRUE

	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

//	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("mp_m_weed_01")) 	
	ActivityMain.pedData[0].mnPedModel = S_M_M_Migrant_01 	
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	

	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_penclipboard")) 
	#ENDIF
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC
//
//
//PROC SETUP_WEED_INSPECT_CROUCH(ACTIVITY_MAIN &ActivityMain, VECTOR vScenePos, VECTOR vSceneRot)
//	// Activity should be triggered by player prompt triggered by angled area
//	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
//	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
//	// Generic help text
//	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
//	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
//		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""
//
//	// Enter input logic
//	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
//
//	// Is player in correct location
//	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
//
//
//	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
//
//	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
//
//	ActivityMain.pedData[0].mnPedModel = S_M_M_Migrant_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
//	ActivityMain.bIsLocalScene = TRUE
//	
//	
//	
//	ActivityMain.animState[0].tlStateName = "AwakeState"
//	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation = vSceneRot 
//																				  
//		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "weed_crouch_base_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[0].clipTypeDefData.bBaseVarClip = TRUE
//	
//		//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//
//	//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_idle_01_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//		
//		//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//	
//
//	//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_idle_02_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//				
//		//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//		
//		
//	//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_spraying_01_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//		//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//		
//	//ActivityMain.animState[0].activityAnimSequence[4].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[4].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_spraying_02_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[4].clipTypeDefData.bIdleVarClip = TRUE
//				
//		//ActivityMain.animState[0].activityAnimSequence[4].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//		
////	ActivityMain.animState[0].activityAnimSequence[5].scenePosition    = vScenePos
////	ActivityMain.animState[0].activityAnimSequence[5].sceneOrientation = vSceneRot
////
////		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
////		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_spraying_03_inspector"
////		ActivityMain.animState[0].activityAnimSequence[5].clipTypeDefData.bIdleVarClip = TRUE
////				
////		ActivityMain.animState[0].activityAnimSequence[5].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
////		
////	ActivityMain.animState[0].activityAnimSequence[5].scenePosition    = vScenePos
////	ActivityMain.animState[0].activityAnimSequence[5].sceneOrientation = vSceneRot
////
////		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
////		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_idle_02_inspector"
////		ActivityMain.animState[0].activityAnimSequence[5].clipTypeDefData.bIdleVarClip = TRUE
//				
//		ActivityMain.animState[0].activityAnimSequence[5].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//	
//	ActivityMain.iEndAnimID = 8
//	ActivityMain.iStartAnimID = 0
//
//	
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	
////	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_bakingsoda")) 
////	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
////	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 
//
//
//	ActivityMain.bPropVisibilitySwap = FALSE
//ENDPROC
//
PROC SETUP_WEED_SPRAY_CROUCH(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_M_Migrant_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	ActivityMain.bRandomiseAnimStartClip = TRUE	
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_spray_01a")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_WEED_SPRAY_STAND(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_M_Migrant_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_spray_01a")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC
//
PROC SETUP_WEED_NOSPRAY_CROUCH(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_M_Migrant_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_spray_01a")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC


PROC SETUP_WEED_LEFT_SORTING_SEATED(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_Y_Dockwork_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_dry_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
		ActivityMain.animState[1].tlStateName = "weedInspecting"

	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_dry_01a")) 
	
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_leaf_01a")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bag_01a")) 
	
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02a")) 
	
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bag_pile_01a")) 
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_weed_chair_01a")) 
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bucket_open_01a")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	#ENDIF
	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC
//
PROC SETUP_WEED_RIGHT_SORTING_SEATED(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_Y_Dockwork_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

	ActivityMain.bRandomiseAnimStartClip = TRUE		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_dry_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_dry_01a")) 
	
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_leaf_01a")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bag_01a")) 
	
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02a")) 
	
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bag_pile_01a")) 
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_chair_01a")) 
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bucket_open_01a")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC
//

PROC SETUP_WEED_SORTING_SEATED_FULL(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_Y_Dockwork_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_dry_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_dry_01a")) 
	
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_leaf_01a")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bag_01a")) 
	
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02a")) 
	
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bag_pile_01a")) 
	
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02a")) 
	
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[10] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[11] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[12] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02b")) 
	ActivityMain.objectModelName[13] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bud_02a")) 
	ActivityMain.objectModelName[14] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_leaf_01a")) 
	ActivityMain.objectModelName[15] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bucket_open_01a")) 
	ActivityMain.objectModelName[16] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_bucket_open_01a")) 
	ActivityMain.objectModelName[17] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_chair_01a")) 
	ActivityMain.objectModelName[18] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_chair_01a"))
	#ENDIF
								
	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_METH_CLEANING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_Y_Dockwork_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	

	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("p_loose_rag_01_s")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_biker_tool_broom"))  
	#ENDIF
								
	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC


PROC SETUP_CFID_DESK_ID(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_M_CntryBar_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"

		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("close_inspection_ruler")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_scalpel_02a")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_singledriverl")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_singledriverl")) 
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_singledriverl")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_singledriverl")) 
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_singledriverl")) 
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_off_chair_04_s")) 
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_foiltipper")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_ruler_01a")) 
	#ENDIF


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_CFID_PHOTO_PHOTOGRAPHER(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = A_M_Y_MEXTHUG_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.bRandomiseAnimStartClip = TRUE	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
			
		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_ing_camera_01")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_CFID_DESK_DOCS(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = A_M_Y_MEXTHUG_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_openpassport")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_bundlepassports")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_singlepassport")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_magnifyingglass")) 
//	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_cs_keyboard_01")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_off_chair_04_s")) 
	#ENDIF


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC


PROC SETUP_METH_COOKING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.bIsLocalScene = TRUE


	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 2
	ActivityMain.pedData[0].pcsComponents[2].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[3].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[8].NewDrawableNumber = 1
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_sacid")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_ammonia")) 
	
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#ENDIF


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_METH_MONITORING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	


	
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.pedData[0].pcsComponents[2].NewDrawableNumber = 1
	ActivityMain.pedData[0].pcsComponents[8].NewDrawableNumber = 1
	
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 
	#ENDIF

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC
 
PROC SETUP_INSPECTOR_HIGH(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	#ENDIF
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_SIT(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0
	ActivityMain.bRandomiseAnimStartClip = TRUE
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_npc_phone")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_STAND(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0
	ActivityMain.bRandomiseAnimStartClip = TRUE
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_npc_phone")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_METH_SMASH(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0
	ActivityMain.bRandomiseAnimStartClip = TRUE
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("w_me_hammer")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"

	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_Meth_SmashedTray_01_frag_")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_Meth_Tray_02a")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_Meth_Tray_02a")) 
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_Meth_SmashedTray_01_frag_")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_Meth_SmashedTray_01_frag_"))
	
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_bigbag_04a")) 
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_bigbag_03a")) 
	
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[10] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[11] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[12] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[13] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[14] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 

	ActivityMain.objectModelName[15] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_scoop_01a")) 
	ActivityMain.objectModelName[16] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_scale_01")) 
	ActivityMain.objectModelName[17] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_penclipboard")) 
	ActivityMain.objectModelName[18] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	
	ActivityMain.objectModelName[19] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_Prop_Meth_Tray_02a")) 
	#ENDIF
	
	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_METH_WEIGHT(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_scoop_01a")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"

	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_scale_01")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_penclipboard")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	
	ActivityMain.objectModelName[6] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_bigbag_04a")) 
	ActivityMain.objectModelName[7] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_bigbag_03a")) 
	
	ActivityMain.objectModelName[8] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[9] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[10] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[11] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[12] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[13] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02")) 
	ActivityMain.objectModelName[14] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_meth_openbag_02"))
	#ENDIF
	ActivityMain.bRandomiseAnimStartClip = TRUE

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_SIT_CHAIR(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_npc_phone")) 
	
	IF ActivityMain.iInterior != 0
		IF ActivityMain.iInterior = HASH("bkr_biker_dlc_int_ware02") // weed
	//			ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_chair_01a")) 
		ELSE
		ENDIF
	ENDIF
	
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_scale_01")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 

	ActivityMain.bRandomiseAnimStartClip = TRUE
	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_SECURITY_SIT_CHAIR(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	ActivityMain.bRandomiseAnimStartClip = TRUE
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_npc_phone")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_scale_01")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_MONEY_MACHINE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.bRandomiseAnimStartClip = TRUE
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_npc_phone")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_scale_01")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_IDLE_SIT_SLOUCH(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_meth_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewDrawableNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	activityCheck.bLocalScript = ActivityMain.bIsLocalScene
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
	ActivityMain.bRandomiseAnimStartClip = TRUE
//	IF NOT IS_STRING_NULL_OR_EMPTY(ActivityMain.strInterior)
//		IF ARE_STRINGS_EQUAL(ActivityMain.strInterior, "bkr_biker_dlc_int_ware02") // weed
			ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_chair_01a")) 
//		ELSE
//			
//		ENDIF
//	ENDIF
//

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_INSPECTOR_LOW(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
		// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	

	
	ActivityMain.iEndAnimID = 10
	ActivityMain.iStartAnimID = 0

	
	
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("prop_pencil_01")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_fakeid_clipboard_01a")) 
	#ENDIF
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 

	ActivityMain.bRandomiseAnimStartClip = TRUE
	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC


//PROC SETUP_CFID_LAZY_WORKER(ACTIVITY_MAIN &ActivityMain, VECTOR vScenePos, VECTOR vSceneRot)
//		// Activity should be triggered by player prompt triggered by angled area
//	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
//	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
//	// Generic help text
//	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
//	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
//		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""
//
//	// Enter input logic
//	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
//
//	// Is player in correct location
//	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
//
//
//	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
//
//	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
//
//	ActivityMain.pedData[0].mnPedModel = A_M_Y_MEXTHUG_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
//	ActivityMain.bIsLocalScene = TRUE
//	
//	
//	
//	ActivityMain.animState[0].tlStateName = "AwakeState"
//	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation = vSceneRot 
//																				  
//		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "leg_smacking_lazyworker"
//		//ActivityMain.animState[0].activityAnimSequence[0].clipTypeDefData.bBaseVarClip = TRUE
//	
//		//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//
//	//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "look_around_lazyworker"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//		
//		//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//	
//
//	//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "look_around_v1_lazyworker"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//				
//		//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//		
//	//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimClip = "lookaround_phoneless_lazyworker"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//		//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	//ActivityMain.animState[0].activityAnimSequence[4].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[4].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimClip = "phone_search_lazyworker"
//		//ActivityMain.animState[0].activityAnimSequence[4].clipTypeDefData.bIdleVarClip = TRUE
//				
//		//ActivityMain.animState[0].activityAnimSequence[4].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	ActivityMain.animState[0].activityAnimSequence[5].scenePosition    = vScenePos
//	ActivityMain.animState[0].activityAnimSequence[5].sceneOrientation = vSceneRot
//
//		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimClip = "smokers_cough_lazyworker"
//		ActivityMain.animState[0].activityAnimSequence[5].clipTypeDefData.bIdleVarClip = TRUE
//				
//		ActivityMain.animState[0].activityAnimSequence[5].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	ActivityMain.animState[0].activityAnimSequence[6].scenePosition    = vScenePos
//	ActivityMain.animState[0].activityAnimSequence[6].sceneOrientation = vSceneRot
//
//		ActivityMain.animState[0].activityAnimSequence[6].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
//		ActivityMain.animState[0].activityAnimSequence[6].activitySyncScenePed[0].sAnimClip = "tired_search_lazyworker"
//		ActivityMain.animState[0].activityAnimSequence[6].clipTypeDefData.bIdleVarClip = TRUE
//				
//		ActivityMain.animState[0].activityAnimSequence[6].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//	
////	ActivityMain.animState[0].activityAnimSequence[7].scenePosition    = vScenePos
////	ActivityMain.animState[0].activityAnimSequence[7].sceneOrientation = vSceneRot
////
////		ActivityMain.animState[0].activityAnimSequence[7].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
////		ActivityMain.animState[0].activityAnimSequence[7].activitySyncScenePed[0].sAnimClip = "look_around_v2_idartist"
////		ActivityMain.animState[0].activityAnimSequence[7].clipTypeDefData.bIdleVarClip = TRUE
////				
////		ActivityMain.animState[0].activityAnimSequence[7].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
////		
////		
////	ActivityMain.animState[0].activityAnimSequence[8].scenePosition    = vScenePos
////	ActivityMain.animState[0].activityAnimSequence[8].sceneOrientation = vSceneRot
////
////		ActivityMain.animState[0].activityAnimSequence[8].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
////		ActivityMain.animState[0].activityAnimSequence[8].activitySyncScenePed[0].sAnimClip = "simple_inspect_idartist"
////		ActivityMain.animState[0].activityAnimSequence[8].clipTypeDefData.bIdleVarClip = TRUE
////				
////		ActivityMain.animState[0].activityAnimSequence[8].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
////		
////	ActivityMain.animState[0].activityAnimSequence[9].scenePosition    = vScenePos
////	ActivityMain.animState[0].activityAnimSequence[9].sceneOrientation = vSceneRot
////
////		ActivityMain.animState[0].activityAnimSequence[9].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfid@cfid_desk_no_work_bgen_chair_no_work@"
////		ActivityMain.animState[0].activityAnimSequence[9].activitySyncScenePed[0].sAnimClip = "simple_inspect_v1_idartist"
////		ActivityMain.animState[0].activityAnimSequence[9].clipTypeDefData.bIdleVarClip = TRUE
////				
////		ActivityMain.animState[0].activityAnimSequence[9].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//	
//	ActivityMain.iEndAnimID = 10
//	ActivityMain.iStartAnimID = 0
//
//	
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	
////	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_bakingsoda")) 
////	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
////	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 
//
//
//	ActivityMain.bPropVisibilitySwap = FALSE
//ENDPROC

//PROC SETUP_WEED_STANDING_SPRAY(ACTIVITY_MAIN &ActivityMain, VECTOR vScenePos, VECTOR vSceneRot)
//	// Activity should be triggered by player prompt triggered by angled area
//	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
//	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
//	// Generic help text
//	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
//	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
//		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""
//
//	// Enter input logic
//	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
//		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
//
//	// Is player in correct location
//	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
//	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000
//
//
//	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
//
//	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
//
//	ActivityMain.pedData[0].mnPedModel = S_M_M_Migrant_01
//	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
//	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
//	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
//	ActivityMain.bIsLocalScene = TRUE
//	
//	
//	
//	ActivityMain.animState[0].tlStateName = "AwakeState"
//	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation = vSceneRot 
//																				  
//		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_stand_base_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[0].clipTypeDefData.bBaseVarClip = TRUE
//	
//		//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//
//	//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_stand_idle_01_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//		
//		//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//	
//
//	//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_stand_kneeling_01_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//				
//		//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//		
//	//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_stand_spraying_01_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
//		//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	//ActivityMain.animState[0].activityAnimSequence[4].scenePosition    = vScenePos
//	//ActivityMain.animState[0].activityAnimSequence[4].sceneOrientation = vSceneRot
//
//		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_stand_spraying_02_inspector"
//		//ActivityMain.animState[0].activityAnimSequence[4].clipTypeDefData.bIdleVarClip = TRUE
//				
//		//ActivityMain.animState[0].activityAnimSequence[4].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	ActivityMain.animState[0].activityAnimSequence[5].scenePosition    = vScenePos
//	ActivityMain.animState[0].activityAnimSequence[5].sceneOrientation = vSceneRot
//
////		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
////		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimClip = "weed_spraybottle_crouch_spraying_03_inspector"
////		ActivityMain.animState[0].activityAnimSequence[5].clipTypeDefData.bIdleVarClip = TRUE
////				
////		ActivityMain.animState[0].activityAnimSequence[5].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	
//	ActivityMain.iEndAnimID = 8
//	ActivityMain.iStartAnimID = 0
//
//	
//	ActivityMain.animState[1].tlStateName = "weedInspecting"
//	
////	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_bakingsoda")) 
////	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
////	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 
//
//
//	ActivityMain.bPropVisibilitySwap = FALSE
//ENDPROC

PROC SETUP_WEED_STANDING_INSPECTING(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("mp_m_weed_01")) 	
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
	ActivityMain.bIsLocalScene = TRUE
	
	
	
	ActivityMain.animState[0].tlStateName = "AwakeState"
	//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = vScenePos
	//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation = vSceneRot 
																				  
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
		//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "weed_stand_base_inspector"
		//ActivityMain.animState[0].activityAnimSequence[0].clipTypeDefData.bBaseVarClip = TRUE
	
		//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC

	//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = vScenePos
	//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation = vSceneRot

		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
		//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_idle_01_inspector"
		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
		
		//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
	

	//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = vScenePos
	//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation = vSceneRot

		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
		//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_idle_02_inspector"
		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
				
		//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
		
		
	//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = vScenePos
	//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation = vSceneRot

		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
		//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_idle_03_inspector"
		//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
		//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
		
	//ActivityMain.animState[0].activityAnimSequence[4].scenePosition    = vScenePos
	//ActivityMain.animState[0].activityAnimSequence[4].sceneOrientation = vSceneRot

		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
		//ActivityMain.animState[0].activityAnimSequence[4].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_idle_04_inspector"
		//ActivityMain.animState[0].activityAnimSequence[4].clipTypeDefData.bIdleVarClip = TRUE
				
		//ActivityMain.animState[0].activityAnimSequence[4].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
		
//	ActivityMain.animState[0].activityAnimSequence[5].scenePosition    = vScenePos
//	ActivityMain.animState[0].activityAnimSequence[5].sceneOrientation = vSceneRot
//
//		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		ActivityMain.animState[0].activityAnimSequence[5].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_idle_05_inspector"
//		ActivityMain.animState[0].activityAnimSequence[5].clipTypeDefData.bIdleVarClip = TRUE
//				
//		ActivityMain.animState[0].activityAnimSequence[5].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	ActivityMain.animState[0].activityAnimSequence[6].scenePosition    = vScenePos
//	ActivityMain.animState[0].activityAnimSequence[6].sceneOrientation = vSceneRot
//
//		ActivityMain.animState[0].activityAnimSequence[6].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		ActivityMain.animState[0].activityAnimSequence[6].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_kneeling_01_inspector"
//		ActivityMain.animState[0].activityAnimSequence[6].clipTypeDefData.bIdleVarClip = TRUE
//				
//		ActivityMain.animState[0].activityAnimSequence[6].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
//		
//	ActivityMain.animState[0].activityAnimSequence[7].scenePosition    = vScenePos
//	ActivityMain.animState[0].activityAnimSequence[7].sceneOrientation = vSceneRot
//
//		ActivityMain.animState[0].activityAnimSequence[7].activitySyncScenePed[0].sAnimDict = "anim@amb@business@weed@weed_inspecting_lo_med_hi@"
//		ActivityMain.animState[0].activityAnimSequence[7].activitySyncScenePed[0].sAnimClip = "weed_stand_checkingleaves_kneeling_02_inspector"
//		ActivityMain.animState[0].activityAnimSequence[7].clipTypeDefData.bIdleVarClip = TRUE
//				
//		ActivityMain.animState[0].activityAnimSequence[7].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
		
	
	ActivityMain.iEndAnimID = 8
	ActivityMain.iStartAnimID = 0

	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.animState[1].tlStateName = "weedInspecting"
	#ENDIF
//	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_weed_spray_01a")) 
//	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powderedmilk")) 
//	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_coke_powder_01")) 


	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_COUNTERFIT_MONEY_OVERSEE(ACTIVITY_MAIN &ActivityMain)//, VECTOR vScenePos, VECTOR vSceneRot)
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
//	ActivityMain.iCurrentProperty = activityCheck.iPropertyID

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE

	ActivityMain.pedData[0].mnPedModel = S_M_Y_Construct_01
	ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
	ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
	ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1

	ActivityMain.bIsLocalScene = TRUE
	
	ActivityMain.objectModelName[0] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_scrunched_moneypage")) 
	#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
	ActivityMain.objectModelName[1] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_scrunched_moneypage")) 
	ActivityMain.objectModelName[2] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_scrunched_moneypage")) 
	ActivityMain.objectModelName[3] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_scrunched_moneypage")) 
	ActivityMain.objectModelName[4] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_scrunched_moneypage")) 
	ActivityMain.objectModelName[5] =  INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("bkr_prop_scrunched_moneypage")) 
	#ENDIF
	
	
	ActivityMain.iEndAnimID = 7
	ActivityMain.iStartAnimID = 0

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC
PROC INITALISE_GENERIC_FUNCTION_POINTERS(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)
	ActivityMain.RUN_AUDIO_EFFECT_LOGIC = &runGenericAudioEffect
	
	// Activity should be triggered by player prompt triggered by angled area
	ActivityMain.bShouldPlayerApproachActivity 									= FALSE
	ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
	// Generic help text
	ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
	ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
		ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = ""

	// Enter input logic
	ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
		ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

	// Is player in correct location
	ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
	ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

	ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER

	ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE


	ActivityMain.bIsLocalScene = FALSE
	activityCheck.bLocalScript = ActivityMain.bIsLocalScene
	ActivityMain.bRandomiseAnimStartClip = FALSE

	ActivityMain.bPropVisibilitySwap = FALSE
ENDPROC

PROC SETUP_AN_ACTIVITY(ACTIVITY_MAIN &ActivityMain, CHECK_CREATOR_ACTIVITIES_STRUCT &activityCheck)
	INITALISE_GENERIC_FUNCTION_POINTERS(ActivityMain, activityCheck)
	
	IF IS_PROPERTY_CLUBHOUSE(activityCheck.iPropertyID, PROPERTY_CLUBHOUSE_1_BASE_A)
		SWITCH ActivityMain.eActivity
//			CASE ACT_CREATOR_DEBUG_EXT
//				// Activity should be triggered by player prompt triggered by angled area
//				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
//					
//					// Generic help text (could use context intentions instead)
//					ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
//					ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
//						ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"
//					
//					// Enter input logic
//					ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
//						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
//						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
//					
//					// Is player in correct location
//					ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
//						ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<-1498.489380,-381.656525,39.526276>>
//						ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<-1497.262207,-383.182312,41.831360>> 
//						ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1
//					
//					// Approach position using ped task go straight to coord(could use navMesh instead here)
//					ActivityMain.bShouldPlayerApproachActivity 									= TRUE
//					ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
//						ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.05
//						ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
//						
//						
//			
//				
//				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
//				ActivityMain.iCurrentProperty = activityCheck.iPropertyID
//				
//				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
//				
//					//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<-1496.3059, -380.2702, 39.4698>>
//					//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation = << 0.000, -0.000, -149.400 >>
//	//				//ActivityMain.animState[0].activityAnimSequence[0].originalScenePosition    = //ActivityMain.animState[0].activityAnimSequence[0].scenePosition    
//	//				//ActivityMain.animState[0].activityAnimSequence[0].originalSceneOrientation = //ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation 
//					
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip  = "drink_whisky_stage1"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()
//					
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
//				
//					//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//				
//	//				ActivityMain.approachComponentStruct.entryAnim = //ActivityMain.animState[0].activityAnimSequence[0]
//				
//			
//				ActivityMain.iEndAnimID = 0
//				
//				ActivityMain.bPropVisibilitySwap = FALSE
//
//				
//				ActivityMain.eActivity = ACT_CREATOR_DEBUG_EXT
//				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACTIVITY: ACT_CREATOR_DEBUG_EXT")
//			BREAK
			CASE ACT_CREATOR_DEBUG_PHONE

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<-1025.4208, -218.4773, 36.9267>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<-1024.5679, -218.8955, 38.7449>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	//			ActivityMain.approachComponentStruct.entryAnim = //ActivityMain.animState[0].activityAnimSequence[0]


				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

//				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<-1024.0013, -216.9689, 36.9361>>
//				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, 0.0000, -204.3925>>
//
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "drink_whisky_stage1"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()
//
//
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"
//
//
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
//					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"
//
//				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//
//				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<0.0000, 0.0000, 0.0000>>
//				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, 0.0000>>
//				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//
//				//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = <<0.0000, 0.0000, 0.0000>>
//				//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation    = <<0.0000, 0.0000, 0.0000>>
//				//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
//
//				//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = <<0.0000, 0.0000, 0.0000>>
//				//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation    = <<0.0000, 0.0000, 0.0000>>
//				//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.iEndAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_DEBUG_PHONE
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACTIVITY: ACT_CREATOR_DEBUG_PHONE")
			BREAK
			

			CASE ACT_CREATOR_BEER_1

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1122.7527, -3145.8926, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.7418, -3144.9429, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.bActivityServerProp = TRUE

				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.9164, -3145.2771, -36.9628>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, 0.0000, 45.2224>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<1121.9164, -3145.2771, -36.9628>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<-0.0000, 0.0000, 45.2224>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[1].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[1].sAnimClip = "New text widget"


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[2].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[2].sAnimClip = "New text widget"

				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_1

				BREAK
				
			
			CASE ACT_CREATOR_BEER_3

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1117.5385, -3158.6226, -38.062759>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1116.695654, -3159.302490,-36.562756>> 
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.bActivityServerProp = TRUE
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1117.3972, -3159.3083, -36.9605>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, -184.9679>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<1117.3972, -3159.3083, -36.9605>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation   = <<0.0000, 0.0000, -184.9679>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"




				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "ACT_CREATOR_BEER_3: ActivityMain.objectModelName[0] = prop_cs_beer_bot_01")
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_3

			BREAK
			
			CASE ACT_CREATOR_WHISKEY_VARA_1

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.bActivityServerProp = TRUE
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
				
				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.5208, -3146.4619, -38.0705>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 182.0111>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"


					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.1

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				
				ActivityMain.objectModelName[0] = p_whiskey_bottle_s
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] = hei_prop_heist_tumbler_empty
				#ENDIF
				ActivityMain.iEndAnimID = 1
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_WHISKEY_VARA_1

			BREAK
			
			CASE YACHT_LEAN_SLOT_1_ENUM

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.iEndAnimID = 3
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = YACHT_LEAN_SLOT_1_ENUM

			BREAK
			
			CASE YACHT_LEAN_SLOT_2_ENUM

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1110.776611,-3158.927979,-38.062767>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1112.200317,-3158.918213,-36.312767>> 
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.iEndAnimID = 3
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = YACHT_LEAN_SLOT_2_ENUM
				
			BREAK
			
			CASE YACHT_LEAN_SLOT_3_ENUM

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1110.627319,-3154.963379,-38.062767>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.898926,-3154.949707,-36.312767>> 
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1111.4205, -3154.9983, -38.0618>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, 0.0000, 293.4249>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@amb@yacht@rail@standing@male@variant_01@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<1111.4205, -3154.9983, -38.0618>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<-0.0000, 0.0000, 293.4249>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "anim@amb@yacht@rail@standing@male@variant_01@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "base"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()

				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &REPEAT_ANIM_SEQUENCE
				//ActivityMain.animState[0].activityAnimSequence[1].HAS_PLAYER_TRIGGERED_INPUT[0]					  = &HAS_PLAYER_JUST_PRESSED_CONTROL
				//ActivityMain.animState[0].activityAnimSequence[1].sequenceTriggerInput[0].controlType			  = PLAYER_CONTROL
				//ActivityMain.animState[0].activityAnimSequence[1].sequenceTriggerInput[0].controlAction			  = INPUT_CONTEXT
				//ActivityMain.animState[0].activityAnimSequence[1].sequenceTriggerInput[0].iNextStage			  = 2
				//ActivityMain.animState[0].activityAnimSequence[1].helpTextStruct.helpText			=  "MPYACHT_UNLEAN"
				//ActivityMain.animState[0].activityAnimSequence[1].SHOW_PLAYER_ENTER_HELP_TEXT_PROC			=  &SHOW_HELP_TEXT_GENERIC
				//ActivityMain.animState[0].activityAnimSequence[1].CLEAR_PLAYER_HELP_TEXT_PROC			=  &CLEAR_HELP_TEXT_GENERIC

				//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = <<1111.4205, -3154.9983, -38.0618>>
				//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation    = <<-0.0000, 0.0000, 293.4249>>

					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "anim@amb@yacht@rail@standing@male@variant_01@"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "exit"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()

				//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.iEndAnimID = 3
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = YACHT_LEAN_SLOT_3_ENUM

			BREAK
			
			CASE NEW_BONG_ENUM
				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 												= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 			= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText 				= "SA_BONG2"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 			= &HAS_PLAYER_JUST_PRESSED_CONTROL
				ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   		= FRONTEND_CONTROL
				ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction 		= INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA		= <<1124.237549,-3143.697510,-38.062782>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1123.250122,-3143.683838,-36.562782>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 											= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_USE_BONG"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    	 				= <<1008.6459, -3166.2043, -35.0826>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    				= <<-0.0000, -0.0000, -126.2192>>

				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict 	= "anim@safehouse@bong"
				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip 	= "bong_stage1"
				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex 	= PLAYER_PED_ID()

				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@bong"
				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "bong_stage1_bong"

				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@bong"
				//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "bong_stage1_lighter"
					
				//ActivityMain.animState[0].activityAnimSequence[0].fWeedShotPhase 						 = 0.3

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					 = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_bong_01
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] = p_cs_lighter_01
				#ENDIF
				ActivityMain.iEndAnimID = 1
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE
				
				ActivityMain.bBlockRecording = TRUE

				ActivityMain.eActivity = NEW_BONG_ENUM
				
			BREAK
			
			DEFAULT
				ActivityMain.eActivity = NULL_ACTIVITY
			BREAK
			
		ENDSWITCH
	ENDIF
	
	IF IS_PROPERTY_CLUBHOUSE(activityCheck.iPropertyID, PROPERTY_CLUBHOUSE_7_BASE_B)	
	
	
		CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACTIVITY: PROPERTY_CLUBHOUSE_7_BASE_B")
		SWITCH ActivityMain.eActivity
			CASE ACT_CREATOR_BEER_1

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<999.2708, -3168.6106, -35.0920>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<998.3392, -3168.4253, -33.2738>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.bActivityServerProp = TRUE
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
				
				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<998.8856, -3169.3147, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 140.8918>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"


					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<998.8856, -3169.3147, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, 140.8918>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[1].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[1].sAnimClip = "New text widget"


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[2].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[2].sAnimClip = "New text widget"

				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_1

			BREAK
			
			CASE NEW_BONG_ENUM
				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BONG2"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1009.1285, -3166.8862, -35.0576>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1009.2790, -3165.9983, -33.0576>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
				
				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_USE_BONG"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    	 = <<1008.6459, -3166.2043, -35.0826>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, -0.0000, -126.2192>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@bong"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "bong_stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@bong"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "bong_stage1_bong"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@bong"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "bong_stage1_lighter"
					
					//ActivityMain.animState[0].activityAnimSequence[0].fWeedShotPhase = 0.3

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_bong_01
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] = p_cs_lighter_01
				#ENDIF
				ActivityMain.iEndAnimID = 1
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE
				ActivityMain.bBlockRecording = TRUE

				ActivityMain.eActivity = NEW_BONG_ENUM

			BREAK
			

			CASE ACT_CREATOR_WHISKEY_VARA_1

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1010.690674,-3167.417480,-35.082565>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1011.755432,-3167.497070,-33.332565>> 
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1011.2612, -3167.2778, -35.0826>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, 0.0000, -187.8426>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = p_whiskey_bottle_s
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] = hei_prop_heist_tumbler_empty
				#ENDIF
				ActivityMain.iEndAnimID = 1
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE



				ActivityMain.eActivity = ACT_CREATOR_WHISKEY_VARA_1


			BREAK

			
			
			
			CASE ACT_CREATOR_BEER_2

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<997.4754, -3167.1558, -35.0920>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<997.6340, -3166.2192, -33.2738>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
					
				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<997.1848, -3166.6592, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 56.4838>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5
					
				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<997.1848, -3166.6592, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, 56.4838>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"



				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_2

			BREAK
			
			CASE ACT_CREATOR_BEER_3

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<997.944580,-3166.966064,-34.843430>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<997.828125,-3166.106934,-33.322479>> 
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<998.2509, -3165.1360, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, -122.5595>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<998.2509, -3165.1360, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, -122.5595>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"



				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_3

			BREAK

			DEFAULT
				ActivityMain.eActivity = NULL_ACTIVITY
			BREAK
		ENDSWITCH
	ENDIF
	CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "activityCheck.iPropertyID = ", activityCheck.iPropertyID)
//	IF activityCheck.iPropertyID = -1 // If we're just using the creator, all the script to preview any activity
//	#IF FEATURE_GUNRUNNING
//	OR activityCheck.iPropertyID = -2
//	
//	#ENDIF
		SWITCH ActivityMain.eActivity
		
			
			
			CASE ACT_CREATOR_BUNKER_WHISKY
				
				// Activity Trigger
				ActivityMain.SHOULD_ACTIVITY_LAUNCH													= &AMBIENT_AREA_TRIGGER_CHECK
				
				// Generic Help Text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC			= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC					= &CLEAR_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText				= "SA_WHSKY"
				
				// Handle Input
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC				= &HAS_PLAYER_JUST_PRESSED_CONTROL
				ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType		= FRONTEND_CONTROL
				ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction		= INPUT_CONTEXT
				
				// Area Checks
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000

				// Ped Tasking: Approach position using ped task go straight to coord. Could use 
				// navMesh instead here, if a nav mesh exists.
				ActivityMain.bShouldPlayerApproachActivity 											= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT													= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius 				= 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK								= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
				
				// Asset Retrieval
				ActivityMain.bActivityServerProp 													= TRUE
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 												= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID
				
				// Handle Animation
				ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
				ActivityMain.iEndAnimID 															= 1
				ActivityMain.iStartAnimID 															= 0
				
				// Handle Audio
				ActivityMain.loadForActivity.sAudioBankName 										= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				// Activity Props
				ActivityMain.objectModelName[0] 													= p_whiskey_bottle_s
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] 													= p_tumbler_01_s
				#ENDIF
				ActivityMain.bPropVisibilitySwap 													= FALSE
				
				ActivityMain.eActivity																= ACT_CREATOR_BUNKER_WHISKY
			BREAK
			
			CASE ACT_CREATOR_BUNKER_SECURITY_1
				// Activity Trigger
				ActivityMain.bShouldPlayerApproachActivity											= FALSE
				ActivityMain.SHOULD_ACTIVITY_LAUNCH													= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
				
				// Area Check
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
				
				// Asset Retrieval 
				ActivityMain.GET_ASSETS_FOR_ACTIVITY												= &GRAB_ASSETS_FROM_SERVER
				
				// Handle Animation
				ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
				ActivityMain.iEndAnimID																= 1
				ActivityMain.iStartAnimID															= 0
				
				// Activity Props
				ActivityMain.pedData[0].mnPedModel													= INT_TO_ENUM(MODEL_NAMES, HASH("mp_m_cocaine_01"))
				ActivityMain.bPropVisibilitySwap													= FALSE

			BREAK
			
			CASE ACT_CREATOR_BUNKER_SECURITY_2
				// Activity Trigger
				ActivityMain.bShouldPlayerApproachActivity											= FALSE
				ActivityMain.SHOULD_ACTIVITY_LAUNCH													= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
				
				// Area Check
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
				
				// Asset Retrieval 
				ActivityMain.GET_ASSETS_FOR_ACTIVITY												= &GRAB_ASSETS_FROM_SERVER
				
				// Handle Animation
				ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
				ActivityMain.iEndAnimID																= 1
				ActivityMain.iStartAnimID															= 0
				
				// Activity Props
				ActivityMain.pedData[0].mnPedModel													= INT_TO_ENUM(MODEL_NAMES, HASH("mp_g_m_pros_01"))
				ActivityMain.bPropVisibilitySwap													= FALSE
			BREAK
			CASE ACT_CREATOR_SECURITY_STAND_WITH_WEAPON
				// Activity Trigger
				ActivityMain.bShouldPlayerApproachActivity											= FALSE
				ActivityMain.SHOULD_ACTIVITY_LAUNCH													= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
				
				// Area Check
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 					= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA 		= <<1121.2943, -3146.8977, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB 		= <<1122.2415, -3146.9692, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth 			= 1.0000
				
				// Asset Retrieval 
				ActivityMain.GET_ASSETS_FOR_ACTIVITY												= &GRAB_ASSETS_FROM_SERVER
				
				// Handle Animation
				ActivityMain.RUN_ANIM_SEQUENCE														= &RUN_ANIM_LOCAL_SYNC_SCENE_SEQUENCE
				ActivityMain.iEndAnimID																= 1
				ActivityMain.iStartAnimID															= 3
				
				// Activity Props
				ActivityMain.pedData[0].mnPedModel													= INT_TO_ENUM(MODEL_NAMES, HASH("mp_g_m_pros_01"))
				ActivityMain.bPropVisibilitySwap													= FALSE
				ActivityMain.bIsLocalScene = TRUE
				ActivityMain.bHeadTracking = TRUE
				activityCheck.bLocalScript = ActivityMain.bIsLocalScene
			BREAK

			CASE NEW_BONG_ENUM
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACITIVITY: NEW_BONG_ENUM")
				
				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BONG2"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1006.6099, -3164.7461, -35.0921>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1006.7067, -3165.6909, -33.2738>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.bActivityServerProp = TRUE
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_USE_BONG"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1006.9877, -3165.1594, -35.0826>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, -262.4273>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@bong"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "bong_stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@bong"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "bong_stage1_bong"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@bong"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "bong_stage1_lighter"

					//ActivityMain.animState[0].activityAnimSequence[0].fWeedShotPhase = 0.1

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = PROP_BONG_01
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] = p_cs_lighter_01
				#ENDIF
				ActivityMain.iEndAnimID = 1
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.bBlockRecording = TRUE
				
				ActivityMain.eActivity = NEW_BONG_ENUM

			BREAK
			CASE ACT_CREATOR_BEER_3

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<997.944580,-3166.966064,-34.843430>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<997.828125,-3166.106934,-33.322479>> 
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork						= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<998.2509, -3165.1360, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, -122.5595>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<998.2509, -3165.1360, -33.9803>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, -122.5595>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"



				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_3

			BREAK
			
			
			CASE ACT_CREATOR_DEBUG_EXT
				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
					
					// Generic help text (could use context intentions instead)
					ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
						ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"
					
					// Enter input logic
					ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT
					
					// Is player in correct location
					ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
						ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<-1498.489380,-381.656525,39.526276>>
						ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<-1497.262207,-383.182312,41.831360>> 
						ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1
					
					// Approach position using ped task go straight to coord(could use navMesh instead here)
					ActivityMain.bShouldPlayerApproachActivity 									= TRUE
					ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
						ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.05
						ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
						
						
			
				
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID
				
				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
				
					//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<-1496.3059, -380.2702, 39.4698>>
					//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation = << 0.000, -0.000, -149.400 >>
	//				//ActivityMain.animState[0].activityAnimSequence[0].originalScenePosition    = //ActivityMain.animState[0].activityAnimSequence[0].scenePosition    
	//				//ActivityMain.animState[0].activityAnimSequence[0].originalSceneOrientation = //ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation 
					
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip  = "drink_whisky_stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()
					
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
				
					//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				
	//				ActivityMain.approachComponentStruct.entryAnim = //ActivityMain.animState[0].activityAnimSequence[0]
				
			
				ActivityMain.iEndAnimID = 0
				
				ActivityMain.bPropVisibilitySwap = FALSE

				
				ActivityMain.eActivity = ACT_CREATOR_DEBUG_EXT
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACTIVITY: ACT_CREATOR_DEBUG_EXT")
			BREAK
			CASE ACT_CREATOR_DEBUG_PHONE

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<-1025.4208, -218.4773, 36.9267>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<-1024.5679, -218.8955, 38.7449>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

	//			ActivityMain.approachComponentStruct.entryAnim = //ActivityMain.animState[0].activityAnimSequence[0]


				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<-1024.0013, -216.9689, 36.9361>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, 0.0000, -204.3925>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "drink_whisky_stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<0.0000, 0.0000, 0.0000>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, 0.0000>>
				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = <<0.0000, 0.0000, 0.0000>>
				//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation    = <<0.0000, 0.0000, 0.0000>>
				//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = <<0.0000, 0.0000, 0.0000>>
				//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation    = <<0.0000, 0.0000, 0.0000>>
				//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.iEndAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_DEBUG_PHONE
				CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "SETUP_AN_ACTIVITY: ACT_CREATOR_DEBUG_PHONE")
			BREAK
			

			CASE ACT_CREATOR_BEER_1

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1122.7527, -3145.8926, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.7418, -3144.9429, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.bActivityServerProp = TRUE

				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.9164, -3145.2771, -36.9628>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<-0.0000, 0.0000, 45.2224>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[2].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[2].sAnimClip = "New text widget"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5
					
				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<1121.9164, -3145.2771, -36.9628>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<-0.0000, 0.0000, 45.2224>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[1].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[1].sAnimClip = "New text widget"


					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[2].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[2].sAnimClip = "New text widget"

				//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = <<1121.9164, -3145.2771, -36.9628>>
				//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation    = <<-0.0000, 0.0000, 45.2224>>

					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "New text widget"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()



					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncSceneEntity[0].sAnimClip = "New text widget"


					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncSceneEntity[1].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncSceneEntity[1].sAnimClip = "New text widget"


					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncSceneEntity[2].sAnimDict = "mp_safehousebeer@"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncSceneEntity[2].sAnimClip = "New text widget"

				//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ



				//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
				ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
				ActivityMain.iEndAnimID = 2
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BEER_1
				
			BREAK
				
			CASE ACT_CREATOR_BEER_2

					// Activity should be triggered by player prompt triggered by angled area
					ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
					// Generic help text
					ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
						ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_BEER"

					// Enter input logic
					ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
						ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

					// Is player in correct location
					ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
					ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1115.7933, -3154.9929, -38.0723>>
					ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1116.7424, -3155.0291, -36.2540>>
					ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

					// Approach position using ped task go straight to coord(could use navMesh instead here)
					ActivityMain.bShouldPlayerApproachActivity 									= TRUE
					ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
					ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
					ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

					ActivityMain.bActivityServerProp = TRUE
					ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
					ActivityMain.iCurrentProperty = activityCheck.iPropertyID
					
					ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_FRANKLIN_DRINK_BEER"
					ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
					
					ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

					//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1116.2676, -3154.3071, -36.9605>>
					//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, -37.9559>>

						//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
						//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "enter"
						//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


						//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
						//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "enter_bottle"

						//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.5

					//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

					//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<1116.2676, -3154.3071, -36.9605>>
					//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, -37.9559>>

						//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "mp_safehousebeer@"
						//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "exit_1"
						//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


						//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimDict = "mp_safehousebeer@"
						//ActivityMain.animState[0].activityAnimSequence[1].activitySyncSceneEntity[0].sAnimClip = "exit_1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ
					ActivityMain.objectModelName[0] = prop_cs_beer_bot_01
					ActivityMain.iEndAnimID = 2
					ActivityMain.iStartAnimID = 0

					ActivityMain.bPropVisibilitySwap = FALSE

					ActivityMain.eActivity = ACT_CREATOR_BEER_2

			BREAK
			
			
			CASE ACT_CREATOR_WHISKEY_VARA_1

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &AMBIENT_AREA_TRIGGER_CHECK
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "SA_WHSKY"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1121.2943, -3146.8977, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1122.2415, -3146.9692, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000

				// Approach position using ped task go straight to coord(could use navMesh instead here)
				ActivityMain.bShouldPlayerApproachActivity 									= TRUE
				ActivityMain.RUN_APPROACH_COMPONENT											= &RUN_APPROACH_PED_TASK_COMPONENT
				ActivityMain.approachComponentStruct.approachTaskStruct.TargetRadius = 0.0500
				ActivityMain.approachComponentStruct.APPROACH_PED_TASK						= &APPROACH_PED_TASK_GO_STRAIGHT_TO_COORD

				ActivityMain.bActivityServerProp = TRUE
				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE
				ActivityMain.loadForActivity.sAudioBankName 								= "SAFEHOUSE_MICHAEL_DRINK_WHISKEY"
				ActivityMain.loadForActivity.bAudioBankLoadOverNetwork								= TRUE
				
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1121.5208, -3146.4619, -38.0705>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 182.0111>>

					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "DRINK_WHISKY_Stage1"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].PedIndex = PLAYER_PED_ID()


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[0].sAnimClip = "drink_whisky_stage1_bottle"


					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimDict = "anim@safehouse@whisky"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncSceneEntity[1].sAnimClip = "drink_whisky_stage1_tumbler"

					//ActivityMain.animState[0].activityAnimSequence[0].fDrunkShotPhase = 0.1

				//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &INCREMENT_TO_NEXT_SEQ

				
				ActivityMain.objectModelName[0] = p_whiskey_bottle_s
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.objectModelName[1] = hei_prop_heist_tumbler_empty
				#ENDIF
				ActivityMain.iEndAnimID = 1
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_WHISKEY_VARA_1

			BREAK
			
			
			
			CASE ACT_CREATOR_BAR

				// Activity should be triggered by player prompt triggered by angled area
				ActivityMain.bShouldPlayerApproachActivity 									= FALSE
				ActivityMain.SHOULD_ACTIVITY_LAUNCH 										= &LAUNCH_AMBIENT_ACTIVITY_STRAIGHT_AWAY
				// Generic help text
				ActivityMain.shouldActivityLaunchStruct.SHOW_PLAYER_ENTER_HELP_TEXT_PROC 	= &SHOW_HELP_TEXT_GENERIC
				ActivityMain.shouldActivityLaunchStruct.CLEAR_ENTER_HELP_TEXT_PROC			= &CLEAR_HELP_TEXT_GENERIC
					ActivityMain.shouldActivityLaunchStruct.enterHelpTextStruct.helpText = "MPYACHT_LEAN"

				// Enter input logic
				ActivityMain.shouldActivityLaunchStruct.HAS_PLAYER_TRIGGERED_INPUT_FUNC 	= &HAS_PLAYER_JUST_PRESSED_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlType   = FRONTEND_CONTROL
					ActivityMain.shouldActivityLaunchStruct.playerTriggerInputStruct.controlAction = INPUT_CONTEXT

				// Is player in correct location
				ActivityMain.shouldActivityLaunchStruct.IS_PLAYER_IN_POSITION_FUNC 			= &ANGLED_AREA_CHECK
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaA = <<1111.7524, -3160.4717, -38.0723>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].vAngledAreaB = <<1111.7400, -3159.5220, -36.2540>>
				ActivityMain.shouldActivityLaunchStruct.isPlayerInPositionStruct[0].fWidth = 1.0000


				ActivityMain.GET_ASSETS_FOR_ACTIVITY 										= &GRAB_ASSETS_FROM_SERVER
				ActivityMain.iCurrentProperty = activityCheck.iPropertyID

				ActivityMain.RUN_ANIM_SEQUENCE												= &RUN_ANIM_NETWORK_SYNC_SCENE_SEQUENCE

				ActivityMain.pedData[0].mnPedModel = MP_F_BOATSTAFF_01
				ActivityMain.pedData[0].vPos = <<1010.1063, -3199.7590, -39.9931>>
				ActivityMain.pedData[0].vRot = <<0, 0, 83.4291>>
				ActivityMain.pedData[0].pcsComponents[0].NewTextureNumber = 1
				ActivityMain.bIsLocalScene = FALSE
				
				ActivityMain.pedData[0].sAnimDict = "anim@mini@yacht@bar@drink@IDLE_A"
				ActivityMain.pedData[0].sAnimClip = "IDLE_A_BARTENDER"
				
				
				ActivityMain.animState[0].tlStateName = "AwakeState"
				//ActivityMain.animState[0].activityAnimSequence[0].scenePosition    = <<1010.1063, -3199.7590, -39.9931>>
				//ActivityMain.animState[0].activityAnimSequence[0].sceneOrientation    = <<0.0000, 0.0000, 288.9752>>
																							  
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfm@cfm_machine_no_work@"
					//ActivityMain.animState[0].activityAnimSequence[0].activitySyncScenePed[0].sAnimClip = "hanging_out_operator"
					//ActivityMain.animState[0].activityAnimSequence[0].clipTypeDefData.bBaseVarClip = TRUE
				
					//ActivityMain.animState[0].activityAnimSequence[0].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC

				//ActivityMain.animState[0].activityAnimSequence[1].scenePosition    = <<1010.1063, -3199.7590, -39.9931>>
				//ActivityMain.animState[0].activityAnimSequence[1].sceneOrientation    = <<0.0000, 0.0000, 288.9752>>

					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfm@cfm_machine_no_work@"
					//ActivityMain.animState[0].activityAnimSequence[1].activitySyncScenePed[0].sAnimClip = "inspecting_hands_operator"
					//ActivityMain.animState[0].activityAnimSequence[1].clipTypeDefData.bIdleVarClip = TRUE
					
					//ActivityMain.animState[0].activityAnimSequence[1].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
				

				//ActivityMain.animState[0].activityAnimSequence[2].scenePosition    = <<1010.1063, -3199.7590, -39.9931>>
				//ActivityMain.animState[0].activityAnimSequence[2].sceneOrientation    = <<0.0000, 0.0000, 288.9752>>

					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfm@cfm_machine_no_work@"
					//ActivityMain.animState[0].activityAnimSequence[2].activitySyncScenePed[0].sAnimClip = "smokers_cough_operator"
					//ActivityMain.animState[0].activityAnimSequence[2].clipTypeDefData.bIdleVarClip = TRUE
							
					//ActivityMain.animState[0].activityAnimSequence[2].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
					
					
				//ActivityMain.animState[0].activityAnimSequence[3].scenePosition    = <<1010.1063, -3199.7590, -39.9931>>
				//ActivityMain.animState[0].activityAnimSequence[3].sceneOrientation    = <<0.0000, 0.0000, 288.9752>>

					//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimDict = "anim@amb@business@cfm@cfm_machine_no_work@"
					//ActivityMain.animState[0].activityAnimSequence[3].activitySyncScenePed[0].sAnimClip = "smokers_cough_V1_operator"
					//ActivityMain.animState[0].activityAnimSequence[3].clipTypeDefData.bIdleVarClip = TRUE
							
					//ActivityMain.animState[0].activityAnimSequence[3].RUN_NEXT_ANIM_SEQ					  = &USE_TYPE_DEF_LOGIC
				#IF NOT DEFINED(ACTIVITY_STRUCT_VARIABLE_SIZE)
				ActivityMain.animState[1].tlStateName = "SleepState"
				#ENDIF
				ActivityMain.iEndAnimID = 5
				ActivityMain.iStartAnimID = 0

				ActivityMain.bPropVisibilitySwap = FALSE

				ActivityMain.eActivity = ACT_CREATOR_BAR

			BREAK
			
			CASE YACHT_LEAN_SLOT_1_ENUM
				SETUP_LEAN_ACT(ActivityMain)//, << 1088.416, -3195.236, -39.650 >>, << 0.000, -0.000, 0.000 >>)
			BREAK
			
			CASE YACHT_LEAN_SLOT_2_ENUM
				SETUP_LEAN_ACT_2(ActivityMain)//, << 1088.416, -3195.236, -39.650 >>, << 0.000, -0.000, 240.000 >>)
			BREAK
			
			CASE ACT_CREATOR_CUT_COKE_LEFT
				SETUP_COKE_UNPACK_CUT_LEFT(ActivityMain)//, << 1088.416, -3195.236, -39.650 >>, << 0.000, -0.000, 0.000 >>)
			BREAK
			
			CASE ACT_CREATOR_CUT_COKE
				SETUP_COKE_UNPACK_CUT(ActivityMain)//, <<1090.9454, -3195.3660, -39.6327>>, <<-0.0000, -0.0000, -355.1360>>)
			BREAK
			
			CASE ACT_CREATOR_PACK_COKE	
				SETUP_PACK_COKE(ActivityMain)//, <<1090.9454, -3195.3660, -39.6327>>, <<-0.0000, -0.0000, -355.1360>>)
			BREAK
			
			CASE ACT_CREATOR_PACK_COKE_HIGH
				SETUP_PACK_COKE_HIGH(ActivityMain)//, <<1090.9454, -3195.3660, -39.6327>>, <<-0.0000, -0.0000, -355.1360>>)
			BREAK
			
			CASE ACT_CREATOR_COUNTERFIT_MONEY_OVERSEE
				SETUP_COUNTERFIT_MONEY_OVERSEE(ActivityMain)//, << 1131.000, -3198.206, -40.729 >>, << 0.000, -0.000, -177.840 >>)
			BREAK
			
			CASE ACT_CREATOR_NOTE_COUNTING_V1
				SETUP_NOTE_COUNTING_V1(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_NOTE_DRYING
				SETUP_NOTE_DRYING(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_NOTE_CUTTING_BILLS
				SETUP_NOTE_CUTTING_BILLS(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_WEED_INSPECT_HIGH
				SETUP_WEED_INSPECT_HIGH(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
//		
			CASE ACT_CREATOR_WEED_INSPECT_STAND
				SETUP_WEED_STANDING_INSPECTING(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_WEED_SPRAY_CROUCH
				SETUP_WEED_SPRAY_CROUCH(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_WEED_NOSPRAY_CROUCH
				SETUP_WEED_NOSPRAY_CROUCH(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK	
			CASE ACT_CREATOR_WEED_SPRAY_STAND
				SETUP_WEED_SPRAY_STAND(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_WEED_SORTING_LEFT
				SETUP_WEED_LEFT_SORTING_SEATED(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
			
			CASE ACT_CREATOR_WEED_SORTING_RIGHT
				SETUP_WEED_RIGHT_SORTING_SEATED(ActivityMain)//, activityCheck.vScenePos, activityCheck.vSceneRot)
			BREAK
		
			CASE ACT_CREATOR_CFID_DESK_ID
				SETUP_CFID_DESK_ID(ActivityMain)//, << 1164.277, -3197.024, -39.188 >>, << 0.000, 0.000, 100.608 >>)
			BREAK
			
			CASE ACT_CREATOR_CFID_PHOTO_PHOTOGRAPHER
				SETUP_CFID_PHOTO_PHOTOGRAPHER(ActivityMain)//, << 1163.408, -3191.304, -39.025 >>, << 0.000, -0.000, -97.920 >>)
			BREAK
			
			CASE ACT_CREATOR_CFID_DESK_DOCS
				SETUP_CFID_DESK_DOCS(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_METH_COOKING
				SETUP_METH_COOKING(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_METH_MONITORING
				SETUP_METH_MONITORING(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_FACTORY_INSPECTOR_MED
				SETUP_INSPECTOR_LOW(ActivityMain)
			BREAK
			CASE ACT_CREATOR_FACTORY_INSPECTOR_HIGH
				SETUP_INSPECTOR_HIGH(ActivityMain)
			BREAK
//			CASE ACT_CREATOR_CFID_LAZY_WORKER
//				SETUP_CFID_LAZY_WORKER(ActivityMain, << 1158.722, -3196.496, -39.990 >>, << 0.000, 0.000, 83.880 >>)
//			BREAK

			CASE ACT_CREATOR_IDLE_SIT
				SETUP_IDLE_SIT(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_IDLE_STAND
				SETUP_IDLE_STAND(ActivityMain)
			BREAK

			CASE ACT_CREATOR_METH_SMASH
				SETUP_METH_SMASH(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_METH_WEIGHT
				SETUP_METH_WEIGHT(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_IDLE_SIT_CHAIR
				SETUP_IDLE_SIT_CHAIR(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_SECURITY_SIT
			CASE ACT_CREATOR_SECURITY_STAND_ARM_CROSS
			CASE ACT_CREATOR_DEFUNCT_BASE_SECURITY_WITHOUT_WEAPON
				SETUP_SECURITY_SIT_CHAIR(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_WEED_SORTING_FULL
				SETUP_WEED_SORTING_SEATED_FULL(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_METH_CLEANING
				SETUP_METH_CLEANING(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_IDLE_MONEY_MACHINE
				SETUP_IDLE_MONEY_MACHINE(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_IDLE_SIT_SLOUCH
				SETUP_IDLE_SIT_SLOUCH(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_CUTTING_BILLS_MACHINE_ONLY
				SETUP_NOTE_CUTTING_BILLS_MACHINE_ONLY(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_IDLE_SMOKING
				SETUP_IDLE_SMOKING(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_IDLE_SMOKING_JOINT
				SETUP_IDLE_SMOKING_JOINT(ActivityMain)
			BREAK
			CASE ACT_CREATOR_MECHANIC_1
			CASE ACT_CREATOR_MECHANIC_HAMMER
			CASE ACT_CREATOR_FIXING_CAR_PED
			CASE ACT_CREATOR_FIXING_CAR_PLAYER
				SETUP_MECHANIC_STAND_1(ActivityMain)
			BREAK
			CASE ACT_CREATOR_MECHANIC_2
				SETUP_MECHANIC_STAND_1(ActivityMain)
			BREAK
			CASE ACT_CREATOR_MECHANIC_CAR
				SETUP_MECHANIC_STAND_CAR(ActivityMain)
			BREAK
			CASE ACT_CREATOR_JANITOR
				SETUP_MECHANIC_JANITOR(ActivityMain)
			BREAK
			
			CASE ACT_CREATOR_SCENARIO_HANGING_OUT
				SETUP_SCENARIO_HANGING_OUT(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_SMOKING
				SETUP_SCENARIO_SMOKING(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_HAMMERING
				SETUP_SCENARIO_HAMMERING(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_WELDING
				SETUP_SCENARIO_WELDING(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_DRILL
				SETUP_SCENARIO_DRILLING(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_MAP
				SETUP_SCENARIO_MAP(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_MOBILE
				SETUP_SCENARIO_MOBILE(ActivityMain)
			BREAK
			CASE ACT_CREATOR_SCENARIO_COFFEE
				SETUP_SCENARIO_COFFEE(ActivityMain)
			BREAK
			CASE ACT_CREATOR_MECHANIC_ONBACK
				SETUP_MECHANIC_ONBACK(ActivityMain)
			BREAK 
			CASE ACT_CREATOR_BUNKER_LATHE
				SETUP_BUNKER_LATHE(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_LATHE_HI
				SETUP_BUNKER_LATHE_HI(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_LATHE_HI_RES
				SETUP_BUNKER_LATHE_HI_RES(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_SPEED_DRILL
				SETUP_BUNKER_SPEED_DRILL(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_SPEED_DRILL_CRATE
				SETUP_BUNKER_SPEED_DRILL(ActivityMain, TRUE)
			BREAK
			CASE ACT_CREATOR_BUNKER_SPEED_DRILL_HI
				SETUP_BUNKER_SPEED_DRILL_HI(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_SPEED_DRILL_HI_CRATE
				SETUP_BUNKER_SPEED_DRILL_HI(ActivityMain, TRUE)
			BREAK
			CASE ACT_CREATOR_BUNKER_VERTICAL_MILL
				SETUP_BUNKER_VERTICAL_MILL(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_VERTICAL_MILL_HI
				SETUP_BUNKER_VERTICAL_MILL_HI(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_BLUEPRINTS
				SETUP_BUNKER_BLUE_PRINTS(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_LOAD_CLIPS
				SETUP_BUNKER_LOAD_CLIPS(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_SHOOTING_RANGE
				SETUP_BUNKER_SHOOTING_RANGE(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_SHOOTING_RANGE_FULL
				SETUP_BUNKER_SHOOTING_RANGE_FULL(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_SUPERVISING
				SETUP_BUNKER_BUNKER_SUPERVISING(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_CHASSIS_REPAIR
				SETUP_BUNKER_CHASSIS_REPAIR(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_STENOGRAPHER_PAPER
				SETUP_BUNKER_STENOGRAPHER_PAPER(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_STENOGRAPHER_COMPUTER
				SETUP_BUNKER_STENOGRAPHER_COMPUTER(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_WHILEBOARD
				SETUP_BUNKER_WHILEBOARD(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_ASSEMBLE_GUN
				SETUP_BUNKER_ASSEMBLE_GUN(ActivityMain)
			BREAK
			CASE ACT_CREATOR_BUNKER_LOAD_CLIP
				SETUP_BUNKER_LOAD_CLIP(ActivityMain)
			BREAK
			CASE ACT_CREATOR_TURRET_SEAT
			CASE ACT_CREATOR_IAA_TURRET_SEAT
				SETUP_BUNKER_TURRET_SEAT(ActivityMain, activityCheck)
			BREAK 
			CASE ACT_CREATOR_TURRET_SEAT_LEFT
				SETUP_BUNKER_TURRET_SEAT_LEFT(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_BUNKER_RESEARCH_COMPUTER
				SETUP_BUNKER_RESEARCH_COMPUTER(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_DEFUNCT_BASE_SECURITY_CAM_MONITOR
				SETUP_BUNKER_SECURITY_CAM_MONITOR(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_OSPREY_TURRET_SEAT
				SETUP_OSPREY_TURRET_SEAT(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_OSPREY_TURRET_SEAT_LEFT
				SETUP_OSPREY_TURRET_SEAT_LEFT(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_HACKER_TRUCK_SEAT_1
				SETUP_HACKER_TRUCK_SEAT_1(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_HACKER_TRUCK_SEAT_2
				SETUP_HACKER_TRUCK_SEAT_2(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_NONSCENARIO_COFFEE
				SETUP_NONSCENARIO_COFFEE(ActivityMain)
			BREAK
			CASE NEW_WHEAT_GRASS_ENUM
				SETUP_NEW_WHEAT_GRASS_ENUM(ActivityMain, activityCheck)
			BREAK			
			CASE ACT_CREATOR_AUTO_SHOP_WHEAT_GRASS
				SETUP_AUTO_SHOP_WHEAT_GRASS(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_AUTO_SHOP_WHEAT_GRASS_2
				SETUP_AUTO_SHOP_WHEAT_GRASS_2(ActivityMain, activityCheck)
			BREAK
			#IF FEATURE_FIXER
			CASE ACT_CREATOR_FIXER_HQ_GREEN_DRINK_JUNK
				SETUP_FIXER_HQ_GREEN_DRINK_JUNK(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_FIXER_HQ_NEW_BONG
				SETUP_FIXER_HQ_NEW_BONG(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_FIXER_HQ_PINK_DRINK
				SETUP_FIXER_HQ_PINK_DRINK(ActivityMain, activityCheck)
			BREAK
			#ENDIF
//			CASE ACT_CREATOR_BUNKER_WEAPON_TEST
//				SETUP_BUNKER_WEAPON_TEST(ActivityMain)
//			BREAK
			CASE ACT_CREATOR_DEFUNCT_RUM
				SETUP_BUNKER_RUM(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_NIGHT_CLUB_RUM
				SETUP_NIGHT_CLUB_RUM(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_NIGHT_CLUB_VIP_WHISKEY
				SETUP_NIGHT_CLUB_VIP_WHISKEY(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_NIGHT_CLUB_BEER
				SETUP_NIGHT_CLUB_BEER(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_ARENA_GARAGE_BEER
				SETUP_ARENA_GARAGE_BEER(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_ARENA_GARAGE_RUM
				SETUP_ARENA_GARAGE_RUM(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_ARENA_GARAGE_WHISKEY
				SETUP_ARENA_GARAGE_WHISKEY(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_ARENA_GARAGE_WHISKEY_2
				SETUP_ARENA_GARAGE_WHISKEY_2(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_CASINO_BEER
				SETUP_CASINO_BEER(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_CASINO_VODKA
				SETUP_CASINO_VODKA(ActivityMain, activityCheck)
			BREAK
			
			CASE ACT_CREATOR_CASINO_VODKA_2
				SETUP_CASINO_VODKA_2(ActivityMain, activityCheck)
			BREAK
			
			CASE NEW_WINE_DRINK_ENUM
				SETUP_BUNKER_WINE(ActivityMain, activityCheck)
			BREAK
			CASE RADIO_ENUM_1
				SETUP_RADIO_ACTIVITY(ActivityMain, activityCheck)
			BREAK
			CASE RADIO_ENUM_2
				SETUP_RADIO_ACTIVITY2(ActivityMain, activityCheck)
			BREAK
			CASE RADIO_ENUM_3
				SETUP_RADIO_ACTIVITY3(ActivityMain, activityCheck)
			BREAK
			CASE RADIO_ENUM_4
				SETUP_RADIO_ACTIVITY4(ActivityMain, activityCheck)
			BREAK
			CASE ACT_LAPTOP_TRIGGER
				SETUP_LAPTOP_TRIGGER_ACTIVITY(ActivityMain, activityCheck)
			BREAK
			#IF FEATURE_HEIST_ISLAND
			CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_1
				SETUP_SUBMARINE_REMOTE_MISSILE_SEAT_1(ActivityMain, activityCheck)
			BREAK
			CASE ACT_CREATOR_SUBMARINE_REMOTE_MISSILE_SEAT_2
				SETUP_SUBMARINE_REMOTE_MISSILE_SEAT_2(ActivityMain, activityCheck)
			BREAK
			#ENDIF
			DEFAULT 
				ActivityMain.eActivity = NULL_ACTIVITY
			BREAK
		ENDSWITCH
//	ENDIF
	activityCheck.bLocalScript = ActivityMain.bIsLocalScene
ENDPROC
