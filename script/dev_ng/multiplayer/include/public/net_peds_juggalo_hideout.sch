//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_JUGGALO_HIDEOUT.sch																			//
// Description: Functions to implement from net_peds_base.sch and look up tables to return JUGGALO HIDEOUT ped data.	//
// Written by:  Jan Mencfel																								//
// Date:  		26/08/22																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_DLC_1_2022
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"

FUNC VECTOR _GET_JUGGALO_HIDEOUT_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN <<585.000000, -2605.000000, -50.000000>>  //freemode entrance at 599.602539,-408.358459,25.102179
ENDFUNC

FUNC FLOAT _GET_JUGGALO_HIDEOUT_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0.0
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//SINGLE LEVEL JUGGALO_HIDEOUT
PROC _SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2602.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SMOKING_WEED_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2608.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_1(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2602.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2608.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2603.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2605.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2607.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<582.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<582.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<584.8041, -2605.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_2(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2602.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2608.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2603.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2605.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2607.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<582.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<582.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<584.8041, -2605.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

PROC _SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_3(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_CLIPBOARD)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2602.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_NOTEPAD)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_WAREHOUSE_STAND_TEXTING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<578.8041, -2608.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2603.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2605.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<580.8041, -2607.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_VIP_BAR_DRINKING_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<582.8041, -2604.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_VIP_BAR_DRINKING_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<582.8041, -2606.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_JUGGALO_HIDEOUT_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<584.8041, -2605.3201, -49.6470>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
ENDPROC

//PROC _SET_JUGGALO_HIDEOUT_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
//	UNUSED_PARAMETER(ePedModel)
//	UNUSED_PARAMETER(iLayout)
//	SWITCH iPed		
//		CASE 1
//			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
//		BREAK
//	ENDSWITCH 
//ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_JUGGALO_HIDEOUT_PED_SCRIPT_LAUNCH()
	RETURN IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_JUGGALO_HIDEOUT_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_JUGGALO_HIDEOUT_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	SWITCH iLayout
		CASE 0
			_SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_0(Data, iPed, bSetPedArea)
		BREAK		
		CASE 1
			_SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_1(Data, iPed, bSetPedArea)
		BREAK	
		CASE 2
			_SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_2(Data, iPed, bSetPedArea)
		BREAK	
		CASE 3
			_SET_JUGGALO_HIDEOUT_PED_DATA_LAYOUT_3(Data, iPed, bSetPedArea)
		BREAK	
	ENDSWITCH		
ENDPROC

FUNC INT _GET_JUGGALO_HIDEOUT_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ServerBD)
UNUSED_PARAMETER(ePedLocation)
	RETURN 0
ENDFUNC

FUNC INT _GET_JUGGALO_HIDEOUT_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
UNUSED_PARAMETER(ePedLocation)
UNUSED_PARAMETER(ServerBD)
	//if completed less than 2 missions
	//4 peds
	//else 10 peds
	
	IF ServerBD.iLayout = 0
		RETURN 4
	ENDIF
	
	RETURN 10
ENDFUNC

FUNC INT _GET_JUGGALO_HIDEOUT_SERVER_PED_LAYOUT_TOTAL()
	RETURN 4
ENDFUNC

FUNC INT _GET_JUGGALO_HIDEOUT_SERVER_PED_LAYOUT()	
	//if completed less than 2 missions
	//layout 0
	//else layouts 1, 2, 3
	RETURN GET_RANDOM_INT_IN_RANGE(0, _GET_JUGGALO_HIDEOUT_SERVER_PED_LAYOUT_TOTAL())
ENDFUNC

FUNC INT _GET_JUGGALO_HIDEOUT_SERVER_PED_LEVEL()
	RETURN 2  // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

FUNC BOOL _IS_PLAYER_IN_JUGGALO_HIDEOUT_PARENT_PROPERTY(PLAYER_INDEX playerID)
	RETURN IS_PLAYER_IN_JUGGALO_HIDEOUT_PROPERTY(playerID)
ENDFUNC

FUNC BOOL _HAS_JUGGALO_HIDEOUT_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_JUGGALO_HIDEOUT_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_JUGGALO_HIDEOUT_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
	
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_JUGGALO_HIDEOUT_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_JUGGALO_HIDEOUT_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_JUGGALO_HIDEOUT_SITTING_DRINKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
			pedAnimData.sAnimClip 		= "idle_d"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
			pedAnimData.sAnimClip 		= "idle_e"
		BREAK
		CASE 6
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
			pedAnimData.sAnimClip 		= "idle_f"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_JUGGALO_HIDEOUT_STANDING_SMOKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_JUGGALO_HIDEOUT_STANDING_SMOKING_F_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC


//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC _GET_JUGGALO_HIDEOUT_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	UNUSED_PARAMETER(iClip)
	RESET_PED_ANIM_DATA(pedAnimData)
	SWITCH eActivity
		CASE PED_ACT_AMBIENT_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@MALE_C@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_AMBIENT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE PED_ACT_LEANING_TEXTING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_TEXTING_IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "AMB_WORLD_HUMAN_LEANING_FEMALE_WALL_BACK_TEXTING_IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_SMOKING_IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_SMOKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_SMOKING_IDLE_A"
		BREAK
		CASE PED_ACT_STANDING_SMOKING_M_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_JUGGALO_HIDEOUT_STANDING_SMOKING_M_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_STANDING_SMOKING_F_01
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_JUGGALO_HIDEOUT_STANDING_SMOKING_F_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_HANGOUT_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@MALE_C@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_HANGOUT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE PED_ACT_SMOKING_WEED_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@MALE@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_SMOKING_WEED_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@FEMALE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_DRINKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "AMB_WORLD_HUMAN_DRINKING_BEER_MALE_BASE"
		BREAK
		CASE PED_ACT_DRINKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "AMB_WORLD_HUMAN_DRINKING_BEER_FEMALE_BASE"
		BREAK
		CASE PED_ACT_SITTING_DRINKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_A"			BREAK
				CASE 1	pedAnimData.sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_B"			BREAK
				CASE 2	pedAnimData.sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_C"			BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_SITTING_DRINKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_FEMALE_IDLE_B"		BREAK
				CASE 1	pedAnimData.sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_FEMALE_IDLE_C"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_HANGOUT_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "BAR_HANGOUT_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "BAR_DRINK_MALE_A_BASE"			BREAK
				CASE 1	pedAnimData.sAnimClip = "BAR_DRINK_MALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "BAR_DRINK_MALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "BAR_DRINK_MALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "BAR_DRINK_MALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "BAR_DRINK_MALE_B_BASE"			BREAK
				CASE 1	pedAnimData.sAnimClip = "BAR_DRINK_MALE_B_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "BAR_DRINK_MALE_B_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "BAR_DRINK_MALE_B_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "BAR_DRINK_MALE_B_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_03
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "BAR_DRINK_MALE_C_BASE"			BREAK
				CASE 1	pedAnimData.sAnimClip = "BAR_DRINK_MALE_C_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "BAR_DRINK_MALE_C_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "BAR_DRINK_MALE_C_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "BAR_DRINK_MALE_C_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_B_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_B_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_BASE"	BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_B_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_BASE"	BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_B_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_DRINK_CUP_FEMALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_B_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_BASE"	BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_B_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "STAND_CELL_PHONE_FEMALE_A_BASE"	BREAK
				CASE 1	pedAnimData.sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_A"		BREAK
				CASE 2	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_B"		BREAK
				CASE 3	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_C"		BREAK
				CASE 4	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_D"		BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_MALE_A_BASE"		BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_BASE"	BREAK
				CASE 1	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
				CASE 2	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
				CASE 3	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
				CASE 4	pedAnimData.sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_LEAN_PNONE
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@male@wall@back@mobile@idle_a"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "idle_a"	
		BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_TEXTING
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@female@wall@back@texting@idle_a"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
		
			pedAnimData.sAnimClip 		= "idle_a"	
		BREAK
		CASE PED_ACT_AUTO_SHOP_MECHANIC_TEXTING
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
		
			pedAnimData.sAnimClip 		= "idle_a"	
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT
			pedAnimData.sAnimDict 		= "amb@medic@standing@kneel@idle_a"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT
			pedAnimData.sAnimDict 		= "ANIM@AMB@INSPECT@CROUCH@MALE_A@IDLES"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK
			pedAnimData.sAnimDict 		= "amb@world_human_bum_standing@drunk@base"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET
			pedAnimData.sAnimDict 		= "move_m@drunk@VERYDRUNK_IDLES@"
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "fidget_01"
		BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR
			pedAnimData.sAnimDict 		= "amb@world_human_stupor@male_looking_right@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
						
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION
			pedAnimData.sAnimDict 		= "misscarsteal4asleep" 
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			
			pedAnimData.sAnimClip 		= "franklin_asleep"
		BREAK
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR
		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B
			pedAnimData.sAnimDict		= "amb@prop_human_seat_bar@male@elbows_on_bar@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M						
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			

			SWITCH iClip
				CASE 0
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@base"
					pedAnimData.sAnimClip 		= "base"
				BREAK
				CASE 1
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
					pedAnimData.sAnimClip 		= "idle_a"
				BREAK
				CASE 2
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
					pedAnimData.sAnimClip 		= "idle_b"
				BREAK
				CASE 3
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_a"
					pedAnimData.sAnimClip 		= "idle_c"
				BREAK
				CASE 4
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
					pedAnimData.sAnimClip 		= "idle_d"
				BREAK
				CASE 5
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
					pedAnimData.sAnimClip 		= "idle_e"
				BREAK
				CASE 6
					pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink_beer@male@idle_b"
					pedAnimData.sAnimClip 		= "idle_f"
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M						
			pedAnimData.sAnimDict		= "amb@world_human_drinking@beer@male@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK		
		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F						
			pedAnimData.sAnimDict		= "amb@prop_human_seat_chair_drink_beer@female@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_LEAN_DRINK						
			pedAnimData.sAnimDict		= "amb@world_human_leaning@male@wall@back@beer@base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)

			SWITCH iClip
				CASE 0
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
					pedAnimData.sAnimClip 		= "BASE"
				BREAK
				CASE 1
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_A"
				BREAK
				CASE 2
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_B"
				BREAK
				CASE 3
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_C"
				BREAK
			ENDSWITCH
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)		
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F						
			pedAnimData.sAnimDict		= "amb@world_human_drinking@beer@female@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F						
			pedAnimData.sAnimDict		= "amb@world_human_leaning@female@wall@back@mobile@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F						
			pedAnimData.sAnimDict		= "amb@world_human_smoking@female@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
	
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)		
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_F
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@BASE"
					pedAnimData.sAnimClip 		= "BASE"
				BREAK
				CASE 1
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_A"
				BREAK
				CASE 2
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_B"
				BREAK
				CASE 3
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_C"
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_M
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@BASE"
					pedAnimData.sAnimClip 		= "BASE"
				BREAK
				CASE 1
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_A"
				BREAK
				CASE 2
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_B"
				BREAK
				CASE 3
					pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
					pedAnimData.sAnimClip 		= "IDLE_C"
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_WAREHOUSE_CLIPBOARD
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)	
			pedAnimData.sAnimClip = "WORLD_HUMAN_CLIPBOARD_FACILITY"
		BREAK
		CASE PED_ACT_WAREHOUSE_NOTEPAD
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@base"
					pedAnimData.sAnimClip 		= "base"
				BREAK
				CASE 1
					pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@idle_a"
					pedAnimData.sAnimClip 		= "idle_a"
				BREAK
				CASE 2
					pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@idle_a"
					pedAnimData.sAnimClip 		= "idle_c"
				BREAK
		//		CASE 3  //ped looks at an absent wristwatch in this one, so we need to skip it
		//			pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@idle_a"
		//			pedAnimData.sAnimClip 		= "idle_b"
		//		BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH
ENDPROC

FUNC INT _GET_JUGGALO_HIDEOUT_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

PROC _JUGGALO_HIDEOUT_UPDATE(SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)

	#IF IS_DEBUG_BUILD
	IF g_bDebugHideInteriorPedsRunUpdate
		g_bDebugHideInteriorPedsRunUpdate = FALSE
		INT i
		IF g_bDebugHideInteriorPeds 
			REPEAT _GET_JUGGALO_HIDEOUT_LOCAL_PED_TOTAL(ServerBD) i
				IF IS_ENTITY_ALIVE(LocalData.PedData[i].PedID)
					SET_ENTITY_VISIBLE(LocalData.PedData[i].PedID, FALSE)
				ENDIF
			ENDREPEAT
		ELSE
			REPEAT _GET_JUGGALO_HIDEOUT_LOCAL_PED_TOTAL(ServerBD) i
				IF DOES_ENTITY_EXIST(LocalData.PedData[i].PedID)
					SET_ENTITY_VISIBLE(LocalData.PedData[i].PedID, TRUE)
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	#ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

FUNC STRING GET_JUGGALO_ACTIVITY_NAME(PED_ACTIVITIES eActivity)
	SWITCH eActivity
		CASE PED_ACT_AMBIENT_M_01                       RETURN "PED_ACT_AMBIENT_M_01"

		CASE PED_ACT_AMBIENT_F_01                      RETURN "PED_ACT_AMBIENT_F_01"

		CASE PED_ACT_LEANING_TEXTING_M_01                      RETURN "PED_ACT_LEANING_TEXTING_M_01"

		CASE PED_ACT_LEANING_TEXTING_F_01                      RETURN "PED_ACT_LEANING_TEXTING_F_01"

		CASE PED_ACT_LEANING_SMOKING_M_01                      RETURN "PED_ACT_LEANING_SMOKING_M_01"

		CASE PED_ACT_LEANING_SMOKING_F_01                      RETURN "PED_ACT_LEANING_SMOKING_F_01"

		CASE PED_ACT_STANDING_SMOKING_M_01                      RETURN "PED_ACT_STANDING_SMOKING_M_01"

		CASE PED_ACT_STANDING_SMOKING_F_01                      RETURN "PED_ACT_STANDING_SMOKING_F_01"

		CASE PED_ACT_HANGOUT_M_01                      RETURN "PED_ACT_HANGOUT_M_01"

		CASE PED_ACT_HANGOUT_F_01                      RETURN "PED_ACT_HANGOUT_F_01"

		CASE PED_ACT_SMOKING_WEED_M_01                      RETURN "PED_ACT_SMOKING_WEED_M_01"

		CASE PED_ACT_SMOKING_WEED_F_01                      RETURN "PED_ACT_SMOKING_WEED_F_01"

		CASE PED_ACT_ON_PHONE_M_01                      RETURN "PED_ACT_ON_PHONE_M_01"

		CASE PED_ACT_ON_PHONE_F_01                      RETURN "PED_ACT_ON_PHONE_F_01"

		CASE PED_ACT_DRINKING_M_01                      RETURN "PED_ACT_DRINKING_M_01"

		CASE PED_ACT_DRINKING_F_01                      RETURN "PED_ACT_DRINKING_F_01"

		CASE PED_ACT_SITTING_DRINKING_M_01                      RETURN "PED_ACT_SITTING_DRINKING_M_01"

		CASE PED_ACT_SITTING_DRINKING_F_01                      RETURN "PED_ACT_SITTING_DRINKING_F_01"

		CASE PED_ACT_VIP_BAR_HANGOUT_M_01                      RETURN "PED_ACT_VIP_BAR_HANGOUT_M_01"

		CASE PED_ACT_VIP_BAR_DRINKING_M_01                      RETURN "PED_ACT_VIP_BAR_DRINKING_M_01"

		CASE PED_ACT_VIP_BAR_DRINKING_M_02                      RETURN "PED_ACT_VIP_BAR_DRINKING_M_02"

		CASE PED_ACT_VIP_BAR_DRINKING_M_03                      RETURN "PED_ACT_VIP_BAR_DRINKING_M_03"

		CASE PED_ACT_VIP_BAR_DRINKING_F_01                      RETURN "PED_ACT_VIP_BAR_DRINKING_F_01"

		CASE PED_ACT_VIP_BAR_DRINKING_F_02                      RETURN "PED_ACT_VIP_BAR_DRINKING_F_02"

		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01                      RETURN "PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01"

		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02                      RETURN "PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02"

		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01                      RETURN "PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01"

		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01                      RETURN "PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01"

		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01                      RETURN "PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01"

		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01                      RETURN "PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01"

		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02                      RETURN "PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02"

		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01                      RETURN "PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01"

		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01                      RETURN "PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01"

		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02                      RETURN "PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02"

		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01                      RETURN "PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01"

		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01                      RETURN "PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01"

		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01                      RETURN "PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01"

		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01                      RETURN "PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01"

		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02                      RETURN "PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02"

		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01                      RETURN "PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01"

		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01                      RETURN "PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01"

		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02                      RETURN "PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02"

		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01                      RETURN "PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01"

		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01                      RETURN "PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01"

		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01                      RETURN "PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01"

		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01                      RETURN "PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01"

		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01                      RETURN "PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01"

		CASE PED_ACT_AUTO_SHOP_MECHANIC_LEAN_PNONE                      RETURN "PED_ACT_AUTO_SHOP_MECHANIC_LEAN_PNONE"

		CASE PED_ACT_AUTO_SHOP_SESSANTA_TEXTING                      RETURN "PED_ACT_AUTO_SHOP_SESSANTA_TEXTING"

		CASE PED_ACT_AUTO_SHOP_MECHANIC_TEXTING                      RETURN "PED_ACT_AUTO_SHOP_MECHANIC_TEXTING"

		CASE PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT                      RETURN "PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT"

		CASE PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT                      RETURN "PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT"

		CASE PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK                      RETURN "PED_ACT_NIGHTCLUB_STADING_BUM_DRUNK"

		CASE PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET                      RETURN "PED_ACT_NIGHTCLUB_STADING_DRUNK_FIDGET"

		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR                      RETURN "PED_ACT_NIGHTCLUB_PASSED_OUT_UPSTAIRS_BAR"

		CASE PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION                      RETURN "PED_ACT_NIGHTCLUB_PASSED_OUT_VIP_SECTION"

		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR                      RETURN "PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR"

		CASE PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B                      RETURN "PED_ACT_CLUBHOUSE_ELBOWS_ON_BAR_B"

		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M                      RETURN "PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_M"

		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M                      RETURN "PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_M"

		CASE PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F                      RETURN "PED_ACT_CLUBHOUSE_SEAT_CHAIR_DRINK_F"

		CASE PED_ACT_CLUBHOUSE_LEAN_DRINK                      RETURN "PED_ACT_CLUBHOUSE_LEAN_DRINK"

		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M                      RETURN "PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_M"

		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F                      RETURN "PED_ACT_CLUBHOUSE_WORLD_HUMAN_DRINKING_F"

		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F                      RETURN "PED_ACT_CLUBHOUSE_WORLD_HUMAN_LEANING_PHONE_F"

		CASE PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F                      RETURN "PED_ACT_CLUBHOUSE_WORLD_HUMAN_SMOKING_F"

		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_F                      RETURN "PED_ACT_WAREHOUSE_STAND_TEXTING_F"

		CASE PED_ACT_WAREHOUSE_STAND_TEXTING_M                      RETURN "PED_ACT_WAREHOUSE_STAND_TEXTING_M"

		CASE PED_ACT_WAREHOUSE_CLIPBOARD                      RETURN "PED_ACT_WAREHOUSE_CLIPBOARD"

		CASE PED_ACT_WAREHOUSE_NOTEPAD                      RETURN "PED_ACT_WAREHOUSE_NOTEPAD"
	ENDSWITCH
	
	 RETURN "PED_ACT_INVALID"
ENDFUNC

PROC DEBUG_PRINT_PED_ACTS_WITH_CORRESPONDING_NUMBERS()
	INT i
	PRINTLN("[AM_MP_PEDS]  DEBUG_PRINT_PED_ACTS_WITH_CORRESPONDING_NUMBERS")
	PED_ANIM_DATA eData
	REPEAT PED_ACT_TOTAL i
		RESET_PED_ANIM_DATA(eData)
		PED_ACTIVITIES eActivity = INT_TO_ENUM(PED_ACTIVITIES, i)
		_GET_JUGGALO_HIDEOUT_PED_ANIM_DATA(eActivity, eData)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(eData.sAnimDict)
		
			PRINTLN("[AM_MP_PEDS] ", GET_JUGGALO_ACTIVITY_NAME(eActivity), " = ", i)
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

PROC _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_JUGGALO_HIDEOUT_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_JUGGALO_HIDEOUT_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_JUGGALO_HIDEOUT_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_JUGGALO_HIDEOUT_NETWORK_PED_TOTAL(ServerBD)
		PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - PLAYER IS THE HOST OF THE SCRIPT")
	ELSE
		PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - PLAYER IS NOT THE HOST OF THE SCRIPT")
	ENDIF
		
	g_iPedLayout = ServerBD.iLayout		
	
	PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - g_iPedLayout: ", g_iPedLayout)
	PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_JUGGALO_HIDEOUT_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_PED_ACTS_WITH_CORRESPONDING_NUMBERS()
	#ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_JUGGALO_HIDEOUT_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_JUGGALO_HIDEOUT_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_JUGGALO_HIDEOUT_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_JUGGALO_HIDEOUT_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_JUGGALO_HIDEOUT_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_JUGGALO_HIDEOUT_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_JUGGALO_HIDEOUT_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_JUGGALO_HIDEOUT_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_JUGGALO_HIDEOUT_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_JUGGALO_HIDEOUT_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_JUGGALO_HIDEOUT_PED_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_JUGGALO_HIDEOUT_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_JUGGALO_HIDEOUT_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_JUGGALO_HIDEOUT_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_JUGGALO_HIDEOUT_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_JUGGALO_HIDEOUT_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_JUGGALO_HIDEOUT_NETWORK_PED_PROPERTIES
		BREAK
//		CASE E_SET_PED_PROP_INDEXES
//			interface.setPedPropIndexes = &_SET_JUGGALO_HIDEOUT_PED_PROP_INDEXES
//		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_JUGGALO_HIDEOUT_PED_BEEN_CREATED
		BREAK
		
		//UPDATE FUNCTION
		CASE E_LOCATION_SPECIFIC_UPDATE
			interface.locationSpecificUpdate = &_JUGGALO_HIDEOUT_UPDATE
		BREAK
						
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
