USING "rage_builtins.sch" 
USING "globals.sch"
USING "shop_private.sch"
USING "gunclub_shop_private.sch"

#IF IS_DEBUG_BUILD
STRUCT HEARTBEAT_PLAYER_DEBUG_DATA
	
	// weapon
	TEXT_WIDGET_ID GenKeyWidget
	INT iWeaponType
	TEXT_WIDGET_ID WeaponNameWidget
	INT iGunclubType
	INT iVariant
	INT iWeaponCost
	
	// pv
	INT iPVInvKey
	INT iPVVehicleKey
	INT iPVVehicleCost
	
ENDSTRUCT

#ENDIF

STRUCT ILLEGAL_DLC_DATA
	TIME_DATATYPE UpdateTimer
	BOOL bIsInVehicle
ENDSTRUCT

STRUCT HEARTBEAT_DATA
	TIME_DATATYPE UpdateTimer
	BOOL bIsInPV
	WEAPON_TYPE WeaponType
ENDSTRUCT

FUNC BOOL IS_WEAPON_TYPE_SHAREABLE(WEAPON_TYPE eWeapon)
	SWITCH eWeapon
		CASE WEAPONTYPE_DLC_GUSENBERG
		CASE WEAPONTYPE_DLC_MUSKET
		CASE WEAPONTYPE_DLC_FIREWORK
		CASE WEAPONTYPE_DLC_HATCHET
		CASE WEAPONTYPE_DLC_SNOWBALL
			RETURN(FALSE)
		BREAK
	ENDSWITCH
	RETURN(TRUE)
ENDFUNC

PROC PROCESS_GUNS_HEARTBEAT_CHECKS( #IF IS_DEBUG_BUILD  HEARTBEAT_PLAYER_DEBUG_DATA &PlayerDebugData #ENDIF )
	
	// guns
	TEXT_LABEL_63 strGeneratedKey
	WEAPON_TYPE eWeaponType
	TEXT_LABEL_15 strWeaponName
	GUNCLUB_ITEM_TYPE_ENUM eGunclubType
	INT iVariant

	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eWeaponType, FALSE)
	IF NOT (eWeaponType = WEAPONTYPE_INVALID)
	AND NOT (eWeaponType = WEAPONTYPE_UNARMED)
	AND NOT (eWeaponType = WEAPONTYPE_DLC_SNOWBALL)
	
		IF NOT IS_WEAPON_TYPE_SHAREABLE(eWeaponType)
			
			strWeaponName = GET_WEAPON_NAME(eWeaponType)
		
			GET_GUNCLUB_ITEM_TYPE_AND_VARIANT_FROM_WEAPON_TYPE(eWeaponType, eGunclubType, iVariant)
			GENERATE_KEY_FOR_SHOP_CATALOGUE(strGeneratedKey, strWeaponName, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_GUN, ENUM_TO_INT(eGunclubType), iVariant, GET_WEAPON_ALT_NUM_FOR_CATALOG_KEY(eWeaponType))
			
			INT iItemKey = GET_HASH_KEY(strGeneratedKey)
			INT iCost = NET_GAMESERVER_GET_PRICE(iItemKey, CATEGORY_WEAPON, 1)

			IF NET_GAMESERVER_CATALOG_ITEM_KEY_IS_VALID(iItemKey)
			
				PRINTLN("[heartbeat] PROCESS_GUNS_HEARTBEAT_CHECKS_FOR_PLAYER - weapon key for player is ", strGeneratedKey, ", iItemKey = ", iItemKey, ", iCost = ", iCost )
							
				// update values in the widget
				#IF IS_DEBUG_BUILD
					PlayerDebugData.iWeaponType = ENUM_TO_INT(eWeaponType)
					SET_CONTENTS_OF_TEXT_WIDGET(PlayerDebugData.GenKeyWidget, strGeneratedKey)
					SET_CONTENTS_OF_TEXT_WIDGET(PlayerDebugData.WeaponNameWidget, strWeaponName)
					PlayerDebugData.iGunclubType = ENUM_TO_INT(eGunclubType)
					PlayerDebugData.iVariant = iVariant
					PlayerDebugData.iWeaponCost = iCost
				#ENDIF
			
				// iItemID - Catalog key used when purchasing weapon
				// value - Quantity, in this case always 1
				// price - Cost of the weapon from the catalog
				NET_GAMESERVER_ADD_CATALOG_ITEM_TO_HEARTBEAT(iItemKey, 1, iCost)
			ENDIF
		ENDIF
		
	ENDIF


ENDPROC

FUNC BOOL WAS_VEHICLE_IN_SINGLE_PLAYER(VEHICLE_INDEX VehicleID)		
	INT iDecor
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(VehicleID, "MPBitset")
				iDecor = DECOR_GET_INT(VehicleID, "MPBitset")
				IF IS_BIT_SET(iDecor, MP_DECORATOR_BS_SINGLE_PLAYER_VEHICLE)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_PERSONAL_VEHICLE_HEARTBEAT_CHECKS( #IF IS_DEBUG_BUILD HEARTBEAT_PLAYER_DEBUG_DATA &PlayerDebugData #ENDIF )

	// check the remote vehicle listed for player
	IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())		
		IF DECOR_EXIST_ON(PERSONAL_VEHICLE_ID(),"Player_Vehicle")	
		AND DECOR_EXIST_ON(PERSONAL_VEHICLE_ID(), "PV_Slot")
			IF (DECOR_GET_INT(PERSONAL_VEHICLE_ID(), "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
				
				// Get the correct item id based the current personal vehicle
				INT iItemKey = GET_VEHICLE_KEY_FOR_CATALOGUE(GET_ENTITY_MODEL(PERSONAL_VEHICLE_ID()))				
				// Grab the catalog key for the current vehicle
				INT iInventoryKey = GET_VEHICLE_INVENTORY_KEY_FOR_CATALOGUE(DECOR_GET_INT(PERSONAL_VEHICLE_ID(), "PV_Slot"))
				// Retrieve the cost from the server
				INT iCost = NET_GAMESERVER_GET_PRICE(iItemKey, CATEGORY_VEHICLE, 1)
				
				PRINTLN("[heartbeat] PROCESS_PERSONAL_VEHICLE_HEARTBEAT_CHECKS - iInventoryKey = ", iInventoryKey, ", iItemKey = ", iItemKey, " iCost = ", iCost )
				
				// update values in the widget
				#IF IS_DEBUG_BUILD
					PlayerDebugData.iPVInvKey = iInventoryKey
					PlayerDebugData.iPVVehicleKey = iItemKey
					PlayerDebugData.iPVVehicleCost = iCost
				#ENDIF				
				
				// iItemID - Catalog key used when purchasing vehicle
				// value - Quantity, in this case always 1
				// price - Cost of the weapon from the catalog
				NET_GAMESERVER_ADD_CATALOG_ITEM_TO_HEARTBEAT(iItemKey, 1, iCost)
			ENDIF
		ENDIF
	ENDIF

ENDPROC


#IF IS_DEBUG_BUILD
PROC ADD_WIDGETS_FOR_HEARTBEAT(HEARTBEAT_DATA &HeartbeatData , HEARTBEAT_PLAYER_DEBUG_DATA &PlayerDebugData)
	
	START_WIDGET_GROUP("Heartbeat")
		
		START_WIDGET_GROUP("Weapons")
			ADD_WIDGET_INT_SLIDER("iWeaponType", PlayerDebugData.iWeaponType, LOWEST_INT, HIGHEST_INT, 1)
			PlayerDebugData.GenKeyWidget = ADD_TEXT_WIDGET("Generated Key")
			PlayerDebugData.WeaponNameWidget = ADD_TEXT_WIDGET("Weapon Name")
			ADD_WIDGET_INT_SLIDER("iGunclubType", PlayerDebugData.iGunclubType, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iVariant", PlayerDebugData.iVariant, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iWeaponCost", PlayerDebugData.iWeaponCost, LOWEST_INT, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Vehicles")
			ADD_WIDGET_INT_SLIDER("iPVInvKey", PlayerDebugData.iPVInvKey, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iPVVehicleKey", PlayerDebugData.iPVVehicleKey, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iPVVehicleCost", PlayerDebugData.iPVVehicleCost, LOWEST_INT, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("Data")
			ADD_WIDGET_BOOL("bIsInPV", HeartbeatData.bIsInPV)
		STOP_WIDGET_GROUP()

	STOP_WIDGET_GROUP()
	
ENDPROC
#ENDIF


PROC PROCESS_HEARTBEAT_CHECKS(HEARTBEAT_DATA &HeartbeatData #IF IS_DEBUG_BUILD , HEARTBEAT_PLAYER_DEBUG_DATA &PlayerDebugData #ENDIF )

	

	IF USE_SERVER_TRANSACTIONS()
	AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
	
		// If the inventory heartbeat is turned off, exit early.
		IF g_sMPTunables.bDisableInventoryHeartbeat
			EXIT
		ENDIF
	
		// timer update
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), HeartbeatData.UpdateTimer) > g_sMPTunables.iInventoryHeartbeatTime)
			
			PRINTLN("[heartbeat] PROCESS_HEARTBEAT_CHECKS - timer check")
			
			PROCESS_GUNS_HEARTBEAT_CHECKS( #IF IS_DEBUG_BUILD PlayerDebugData #ENDIF )
			PROCESS_PERSONAL_VEHICLE_HEARTBEAT_CHECKS( #IF IS_DEBUG_BUILD PlayerDebugData #ENDIF )
			
			HeartbeatData.UpdateTimer = GET_NETWORK_TIME()
			
		ELSE
		
			// If have enabled the tunable heartbeat time only we should not send this data when the content changes.
			IF g_sMPTunables.bInventoryHeartbeatTimeOnly
				EXIT
			ENDIF
		
			// has just got into car?
			IF DOES_ENTITY_EXIST(PERSONAL_VEHICLE_ID())
			AND NOT IS_ENTITY_DEAD(PERSONAL_VEHICLE_ID())
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PERSONAL_VEHICLE_ID())
				IF NOT (HeartbeatData.bIsInPV)
					PRINTLN("[heartbeat] PROCESS_HEARTBEAT_CHECKS - player has entered PV")
					PROCESS_PERSONAL_VEHICLE_HEARTBEAT_CHECKS( #IF IS_DEBUG_BUILD PlayerDebugData #ENDIF )
					HeartbeatData.bIsInPV = TRUE
				ENDIF
			ELSE
				IF (HeartbeatData.bIsInPV)
					HeartbeatData.bIsInPV = FALSE	
				ENDIF
			ENDIF
			
			// has just changed weapon
			WEAPON_TYPE WeaponType
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WeaponType, FALSE)
			IF NOT (WeaponType = HeartbeatData.WeaponType)
				PRINTLN("[heartbeat] PROCESS_HEARTBEAT_CHECKS - player has changed weapons")
				PROCESS_GUNS_HEARTBEAT_CHECKS( #IF IS_DEBUG_BUILD PlayerDebugData #ENDIF )	
				HeartbeatData.WeaponType = WeaponType 
			ENDIF
		
		
		ENDIF
	
		
		
	ENDIF

ENDPROC


