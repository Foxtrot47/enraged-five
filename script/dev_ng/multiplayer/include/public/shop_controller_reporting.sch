USING "commands_script.sch"
USING "commands_netshopping.sch"
USING "commands_network.sch"
USING "net_simple_interior_common.sch"
USING "fm_in_corona_header.sch"

ENUM SC_REPORT_TRANSACTION_STATE
	BGR_IDLE,
	BGR_RUN_TRANSACTION,
	BGR_SEND_SIGNAL
ENDENUM

ENUM SC_REPORT_REASON
	SC_RR_GMODE,
	SC_RR_INVISIBILITY, 
	SC_RR_RAGDOLL,
	SC_RR_SERVICE,
	SC_RR_COUNT
ENDENUM

ENUM SC_REPORT_STATUS
	SC_RS_NOT_DETECTED,
	SC_RS_DETECTED,
	SC_RS_REPORTING,
	SC_RS_REPORTED
ENDENUM

STRUCT SC_REPORTING_DATA
	INT iTransactionId = -1
	BOOL bReportTransactionFinished = FALSE
	SC_REPORT_REASON eReportReason
	SC_REPORT_TRANSACTION_STATE eState = BGR_IDLE
	SC_REPORT_STATUS eReportStatus[SC_RR_COUNT]
ENDSTRUCT

DEBUGONLY FUNC STRING GET_DEBUG_REPORT_REASON_STRING(SC_REPORT_REASON eReason)
	SWITCH eReason
		CASE SC_RR_GMODE		RETURN "SC_RR_GMODE"		BREAK
		CASE SC_RR_INVISIBILITY	RETURN "SC_RR_INVISIBILITY"	BREAK
		CASE SC_RR_RAGDOLL		RETURN "SC_RR_RAGDOLL"		BREAK
		CASE SC_RR_SERVICE		RETURN "SC_RR_SERVICE"		BREAK
	ENDSWITCH
	
	RETURN "Unknown exploit"
ENDFUNC

FUNC TRANSACTION_SERVICES GET_BG_REPORT_SERVICE(SC_REPORT_REASON eReason)
	INT iItemID
	SWITCH eReason
		CASE SC_RR_GMODE		iItemID = HASH("SERVICE_EARN_JBONUS_NO_DEATH")	BREAK
		CASE SC_RR_INVISIBILITY	iItemID = HASH("SERVICE_EARN_JBONUS_NOT_SEEN")	BREAK
		CASE SC_RR_RAGDOLL		iItemID = HASH("SERVICE_EARN_JBONUS_NO_FALL")	BREAK
		CASE SC_RR_SERVICE		iItemID = HASH("SERVICE_EARN_JBONUS_SE")		BREAK
	ENDSWITCH
	
	RETURN INT_TO_ENUM(TRANSACTION_SERVICES, iItemID)
ENDFUNC

/// PURPOSE:
///    trigger an exploit report for a given exploit
PROC SUBMIT_EXPLOIT_REPORT(SC_REPORTING_DATA &data, SC_REPORT_REASON eReason)
	IF data.eReportStatus[eReason] = SC_RS_NOT_DETECTED
		data.eReportStatus[eReason] = SC_RS_DETECTED
		PRINTLN("[SC_REPORT] SUBMIT_EXPLOIT_REPORT - ", GET_DEBUG_REPORT_REASON_STRING(eReason), " reporting...")
	ELSE
		PRINTLN("[SC_REPORT] SUBMIT_EXPLOIT_REPORT - ", GET_DEBUG_REPORT_REASON_STRING(eReason), " is already reported")
	ENDIF
ENDPROC

/// PURPOSE:
///    PC ONLY
///    Only runs when we have detected an exploit and have activley reported the local player via a transaction
///    Added here to avoid altering existing transaction handling
PROC MONITOR_SHOP_TRANSACTION_EVENTS(SC_REPORTING_DATA &data)
	
	IF data.iTransactionId = -1
	OR data.bReportTransactionFinished
		EXIT
	ENDIF
	
	INT iCount
	STRUCT_SHOP_TRANSACTION_EVENT Event
	
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		IF GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount) = EVENT_NETWORK_SHOP_TRANSACTION
			IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Event, SIZE_OF(Event))
				
				IF Event.m_Id = data.iTransactionId
				AND Event.m_Id != -1
					PRINTLN("[SC_REPORT] MONITOR_SHOP_TRANSACTION_EVENTS - Transaction finished with ID: ", data.iTransactionId)
					
					#IF IS_DEBUG_BUILD
						IF Event.m_ResultCode = 0
							PRINTLN("[SC_REPORT] MONITOR_SHOP_TRANSACTION_EVENTS - Transaction success!")									
						ELSE
							PRINTLN("[SC_REPORT] MONITOR_SHOP_TRANSACTION_EVENTS - Transaction failed!")
						ENDIF
					#ENDIF
					
					data.bReportTransactionFinished = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Returns if any explot has been reported
FUNC SC_REPORT_REASON GET_EXPLOIT_READY_TO_REPORT(SC_REPORTING_DATA &data)
	INT i
	
	REPEAT SC_RR_COUNT i
		IF data.eReportStatus[i] = SC_RS_DETECTED
			RETURN INT_TO_ENUM(SC_REPORT_REASON, i)
		ENDIF
	ENDREPEAT
	
	RETURN SC_RR_COUNT
ENDFUNC

/// PURPOSE:
///    Runs check for godmode exploits
PROC MAINTAIN_GODMODE_CHECKS(SC_REPORTING_DATA &data)

	IF NETWORK_IS_ACTIVITY_SESSION()
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
	OR NETWORK_IS_ENTITY_FADING(PLAYER_PED_ID())
	OR IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	OR NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_PLAYER_IN_CORONA()
	OR IS_PLAYER_USING_RC_PERSONAL_VEHICLE(PLAYER_ID())
	OR g_FreemodeDeliveryData.bDeliveryScriptTriggeredSceneLoad
		EXIT
	ENDIF

	IF IS_PED_SHOOTING(PLAYER_PED_ID())
		IF NOT GET_ENTITY_CAN_BE_DAMAGED(PLAYER_PED_ID())
			SUBMIT_EXPLOIT_REPORT(data, SC_RR_GMODE)
		ENDIF
		
		IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
			SUBMIT_EXPLOIT_REPORT(data, SC_RR_INVISIBILITY)
		ENDIF
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks for any unexpected transaction results that indicates something other than script started a transaction
PROC MAINTAIN_TRANSACTION_VALIDITY_CHECKS(SC_REPORTING_DATA &data)
	IF data.eReportStatus[SC_RR_SERVICE] = SC_RS_NOT_DETECTED
		IF g_sUnknownCashTransactionEventData.iFrameCount != 0
		AND g_sUnknownCashTransactionEventData.iEventCode = 0
		AND g_sUnknownCashTransactionEventData.iEventAction = ENUM_TO_INT(NET_SHOP_ACTION_EARN)
			SUBMIT_EXPLOIT_REPORT(data, SC_RR_SERVICE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    PC only
///    Runs exploit checks & reprots any detections via transactions
/// PARAMS:
///    data - primary struct
PROC MAINTAIN_EXPLOIT_TRANSACTIONS(SC_REPORTING_DATA &data)
	
	IF NOT USE_SERVER_TRANSACTIONS()
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR !g_sCURRENT_UGC_STATUS.g_bUgcGlobalsBlockLoaded
		EXIT
	ENDIF
	
	MAINTAIN_GODMODE_CHECKS(data)
	MAINTAIN_TRANSACTION_VALIDITY_CHECKS(data)
	
	SWITCH data.eState
		
		CASE BGR_IDLE
			data.eReportReason = GET_EXPLOIT_READY_TO_REPORT(data)
			IF data.eReportReason != SC_RR_COUNT
				PRINTLN("[SC_REPORT]RUN_EXPLOIT_TRANSACTIONS - Begin report on exploit = ", GET_DEBUG_REPORT_REASON_STRING(data.eReportReason))
				data.eState = BGR_SEND_SIGNAL
			ENDIF
		BREAK
		
		CASE BGR_SEND_SIGNAL
			IF NET_GAMESERVER_BEGIN_SERVICE(data.iTransactionId, CATEGORY_SERVICE_WITH_THRESHOLD, GET_BG_REPORT_SERVICE(data.eReportReason), NET_SHOP_ACTION_BONUS, 0)
				PRINTLN("[SC_REPORT] RUN_EXPLOIT_TRANSACTIONS - NET_GAMESERVER_BEGIN_SERVICE returned true. Transaction ID = ", data.iTransactionId)
				IF NET_GAMESERVER_CHECKOUT_START(data.iTransactionId)
					PRINTLN("[SC_REPORT] RUN_EXPLOIT_TRANSACTIONS - NET_GAMESERVER_CHECKOUT_START returned true. Moving on to BGR_RUN_TRANSACTION")
					data.eState = BGR_RUN_TRANSACTION
					data.eReportStatus[data.eReportReason] = SC_RS_REPORTING
				ENDIF
			ELSE
				PRINTLN("[SC_REPORT] RUN_EXPLOIT_TRANSACTIONS - NET_GAMESERVER_CHECKOUT_START returned false.")
			ENDIF
		BREAK
		
		CASE BGR_RUN_TRANSACTION
			MONITOR_SHOP_TRANSACTION_EVENTS(data)
			
			IF data.bReportTransactionFinished
			AND NET_GAMESERVER_END_SERVICE(data.iTransactionId)
				PRINTLN("[SC_REPORT] RUN_EXPLOIT_TRANSACTIONS - Done NET_GAMESERVER_END_SERVICE.")
				data.iTransactionId 					= -1
				data.eState 							= BGR_IDLE
				data.eReportStatus[data.eReportReason] 	= SC_RS_REPORTED
				data.eReportReason						= SC_RR_COUNT
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC
