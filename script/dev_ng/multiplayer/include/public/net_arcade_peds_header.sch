//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_ARCADE_PEDS_HEADER.sch																				//
// Description: Header file containing functionality for the Arcade peds.												//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		05/09/19																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "net_wait_zero.sch"
USING "ped_component_packing.sch"

#IF IS_DEBUG_BUILD
USING "net_spawn_debug.sch"
#ENDIF

#IF FEATURE_CASINO_HEIST
USING "net_arcade_peds_vars.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ DEBUG ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING ARCADE_ACTIVITY_TO_STRING(ARCADE_ACTIVITY eActivity)
	SWITCH eActivity
		CASE AA_AMBIENT									RETURN "AA_AMBIENT"
		CASE AA_HANGOUT_WITHDRINK_M_01A					RETURN "AA_HANGOUT_WITHDRINK_M_01A"		
		CASE AA_HANGOUT_WITHDRINK_M_01B					RETURN "AA_HANGOUT_WITHDRINK_M_01B"		
		CASE AA_HANGOUT_F_3A							RETURN "AA_HANGOUT_F_3A"				
		CASE AA_HANGOUT_F_3B							RETURN "AA_HANGOUT_F_3B"				
		CASE AA_HANGOUT_CONVO_F_2A						RETURN "AA_HANGOUT_CONVO_F_2A"			
		CASE AA_HANGOUT_WITHDRINK_F_01B					RETURN "AA_HANGOUT_WITHDRINK_F_01B"		
		CASE AA_JIMMY_PRE_CABINET_SETUP					RETURN "AA_JIMMY_PRE_CABINET_SETUP"
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP				RETURN "AA_JIMMY_PRE_SCOPE_OUT_SETUP"
		CASE AA_WENDY_PRE_CABINET_SETUP					RETURN "AA_WENDY_PRE_CABINET_SETUP"
		CASE AA_WENDY_PRE_SCOPE_OUT_SETUP				RETURN "AA_WENDY_PRE_SCOPE_OUT_SETUP"
		CASE AA_JIMMY_SOFA_WATCHING_TV					RETURN "AA_JIMMY_SOFA_WATCHING_TV"
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV			RETURN "AA_JIMMY_SOFA_DRINKING_WATCHING_TV"
		CASE AA_JIMMY_STANDING_DRINKING					RETURN "AA_JIMMY_STANDING_DRINKING"
		CASE AA_JIMMY_STANDING_PHONE_CALL				RETURN "AA_JIMMY_STANDING_PHONE_CALL"
		CASE AA_JIMMY_STANDING_TEXTING					RETURN "AA_JIMMY_STANDING_TEXTING"
		CASE AA_JIMMY_LYING_DOWN_SMOKING				RETURN "AA_JIMMY_LYING_DOWN_SMOKING"
		CASE AA_JIMMY_PHONE_LEANING						RETURN "AA_JIMMY_PHONE_LEANING"
		CASE AA_HEIST_DRIVER_INSPECTING					RETURN "AA_HEIST_DRIVER_INSPECTING"
		CASE AA_HEIST_DRIVER_IDLE						RETURN "AA_HEIST_DRIVER_IDLE"
		CASE AA_HEIST_DRIVER_CLIPBOARD					RETURN "AA_HEIST_DRIVER_CLIPBOARD"
		CASE AA_HEIST_HACKER_COMPUTER					RETURN "AA_HEIST_HACKER_COMPUTER"
		CASE AA_HEIST_HACKER_COFFEE						RETURN "AA_HEIST_HACKER_COFFEE"
		CASE AA_HEIST_HACKER_TABLET						RETURN "AA_HEIST_HACKER_TABLET"
		CASE AA_HEIST_WE_SAT_WITH_GUN					RETURN "AA_HEIST_WE_SAT_WITH_GUN"
		CASE AA_HEIST_WE_TESTING_GUN					RETURN "AA_HEIST_WE_TESTING_GUN"
		CASE AA_HEIST_WE_IDLE_PHONE						RETURN "AA_HEIST_WE_IDLE_PHONE"
		CASE AA_ELBOWS_ON_BAR_M							RETURN "AA_ELBOWS_ON_BAR_M"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED MODELS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC MODEL_NAMES GET_ARCADE_PED_MODEL(INT iPed, BOOL bFemale, CASINO_HEIST_DRIVERS eDriver, CASINO_HEIST_HACKERS eHacker, CASINO_HEIST_WEAPON_EXPERTS eWeaponExpert, INT iPedModelVariation)
	
	// Unique peds
	SWITCH iPed
		CASE 0	RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ig_jimmydisanto2"))
		CASE 1	RETURN INT_TO_ENUM(MODEL_NAMES, GET_HASH_KEY("ig_wendy"))
		CASE 2	
			SWITCH eDriver
				CASE CASINO_HEIST_DRIVER__KARIM_DENZ 			RETURN hc_driver
				CASE CASINO_HEIST_DRIVER__TALIANA_MARTINEZ		RETURN hc_driver
				CASE CASINO_HEIST_DRIVER__EDDIE_TOH				RETURN u_m_m_edtoh
				CASE CASINO_HEIST_DRIVER__ZACH					RETURN S_M_Y_XMech_02
				CASE CASINO_HEIST_DRIVER__WEAPONS_EXPERT		RETURN mp_m_weapexp_01
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH eHacker
				CASE CASINO_HEIST_HACKER__RICKIE_LUKENS			RETURN hc_hacker
				CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ		RETURN hc_hacker
				CASE CASINO_HEIST_HACKER__YOHAN					RETURN INT_TO_ENUM(MODEL_NAMES,HASH("s_m_y_waretech_01"))
				CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN		RETURN ig_money
				CASE CASINO_HEIST_HACKER__PAIGE_HARRIS			RETURN ig_paige
			ENDSWITCH
		BREAK
		CASE 4
			SWITCH eWeaponExpert
				CASE CASINO_HEIST_WEAPON_EXPERT__KARL_ABOLAJI	RETURN hc_gunman
				CASE CASINO_HEIST_WEAPON_EXPERT__GUSTAVO_MOTA	RETURN hc_gunman
				CASE CASINO_HEIST_WEAPON_EXPERT__CHARLIE		RETURN INT_TO_ENUM(MODEL_NAMES,HASH("u_m_y_smugmech_01"))
				CASE CASINO_HEIST_WEAPON_EXPERT__WEAPONS_EXPERT	RETURN mp_m_weapexp_01
				CASE CASINO_HEIST_WEAPON_EXPERT__PACKIE_MCREARY	RETURN hc_gunman
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// Generic peds
	IF (bFemale)
		SWITCH iPedModelVariation
			CASE 0	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Hipster_02"))
			CASE 1	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Hipster_04"))
			CASE 2	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_F_Y_Skater_01"))
		ENDSWITCH
	ELSE
		SWITCH iPedModelVariation
			CASE 0	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_BeachVesp_01"))
			CASE 1	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Skater_01"))
			CASE 2	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Skater_02"))
			CASE 3	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Hipster_01"))
			CASE 4	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_StWhi_02"))
		ENDSWITCH
	ENDIF
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_Hipster_01"))
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PROP MODELS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL DOES_ARCADE_ACTIVITY_HAVE_PROP(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HANGOUT_WITHDRINK_M_01A
		CASE AA_HANGOUT_WITHDRINK_M_01B
		CASE AA_HANGOUT_WITHDRINK_F_01B
		CASE AA_JIMMY_PRE_CABINET_SETUP
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
		CASE AA_WENDY_PRE_CABINET_SETUP
		CASE AA_WENDY_PRE_SCOPE_OUT_SETUP
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
		CASE AA_JIMMY_STANDING_DRINKING
		CASE AA_JIMMY_STANDING_PHONE_CALL
		CASE AA_JIMMY_STANDING_TEXTING
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_JIMMY_PHONE_LEANING
		CASE AA_HEIST_DRIVER_INSPECTING
		CASE AA_HEIST_DRIVER_IDLE
		CASE AA_HEIST_DRIVER_CLIPBOARD
		CASE AA_HEIST_HACKER_COMPUTER
		CASE AA_HEIST_HACKER_COFFEE
		CASE AA_HEIST_HACKER_TABLET
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
		CASE AA_HEIST_WE_IDLE_PHONE
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_ARCADE_PROP_FOR_ACTIVITY(PED_DATA &Data, INT iObject)
	
	SWITCH iObject
		CASE 0
			SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
				CASE AA_HANGOUT_WITHDRINK_M_01A
				CASE AA_HANGOUT_WITHDRINK_M_01B
				CASE AA_HANGOUT_WITHDRINK_F_01B
				CASE AA_JIMMY_STANDING_DRINKING
				CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
					RETURN PROP_AMB_BEER_BOTTLE
					
				CASE AA_JIMMY_PRE_CABINET_SETUP
				CASE AA_JIMMY_LYING_DOWN_SMOKING
					RETURN PROP_CS_CIGGY_01
				
				CASE AA_HEIST_HACKER_TABLET
					RETURN PROP_CS_TABLET
					
				CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
				CASE AA_HEIST_WE_IDLE_PHONE
				CASE AA_WENDY_PRE_CABINET_SETUP
				CASE AA_JIMMY_STANDING_PHONE_CALL
				CASE AA_JIMMY_STANDING_TEXTING
				CASE AA_JIMMY_PHONE_LEANING
				CASE AA_HEIST_DRIVER_IDLE
					RETURN PROP_AMB_PHONE
					
				CASE AA_WENDY_PRE_SCOPE_OUT_SETUP
				CASE AA_HEIST_DRIVER_CLIPBOARD
					RETURN P_CS_CLIPBOARD
					
				CASE AA_HEIST_HACKER_COFFEE
					RETURN P_AMB_COFFEECUP_01
				
				CASE AA_HEIST_WE_SAT_WITH_GUN
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("W_AR_CARBINERIFLE"))
					
				CASE AA_HEIST_WE_TESTING_GUN
					RETURN INT_TO_ENUM(MODEL_NAMES, HASH("W_AR_ASSAULTRIFLE"))
				
				CASE AA_HEIST_DRIVER_INSPECTING
					RETURN PROP_PENCIL_01
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
				CASE AA_HEIST_DRIVER_INSPECTING
					RETURN BKR_PROP_FAKEID_CLIPBOARD_01A
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC PED_BONETAG GET_BONE_FOR_ARCADE_ACTIVITY_PROP(PED_DATA &Data, INT iObject)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HANGOUT_WITHDRINK_M_01B
		CASE AA_HANGOUT_WITHDRINK_F_01B
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
		CASE AA_JIMMY_STANDING_DRINKING
		CASE AA_HEIST_HACKER_COFFEE
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
		CASE AA_JIMMY_PRE_CABINET_SETUP
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_WENDY_PRE_CABINET_SETUP
		CASE AA_JIMMY_STANDING_PHONE_CALL
		CASE AA_JIMMY_STANDING_TEXTING
		CASE AA_JIMMY_PHONE_LEANING
		CASE AA_HEIST_DRIVER_IDLE
			RETURN BONETAG_PH_R_HAND
			
		CASE AA_HANGOUT_WITHDRINK_M_01A
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
		CASE AA_HEIST_WE_IDLE_PHONE
		CASE AA_HEIST_HACKER_TABLET
		CASE AA_WENDY_PRE_SCOPE_OUT_SETUP
		CASE AA_HEIST_DRIVER_CLIPBOARD
			RETURN BONETAG_PH_L_HAND
		
		CASE AA_HEIST_DRIVER_INSPECTING
			SWITCH iObject
				CASE 0	RETURN BONETAG_PH_R_HAND
				CASE 1	RETURN BONETAG_PH_L_HAND
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN BONETAG_PH_R_HAND
ENDFUNC

FUNC VECTOR GET_BONE_OFFSET_POSITION_TO_ATTACH_TO(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)	
		CASE AA_JIMMY_PRE_CABINET_SETUP
			RETURN <<0.005, 0.0175, 0.02>>
			
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
			RETURN <<-0.03, 0.0, -0.02>>
			
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			RETURN <<0.02, -0.02, -0.04>>
			
		CASE AA_JIMMY_STANDING_DRINKING
			RETURN <<0.02, -0.02, -0.08>>
			
		CASE AA_JIMMY_STANDING_TEXTING
		CASE AA_HEIST_DRIVER_IDLE
			RETURN <<0.015, -0.012, -0.02>>
			
		CASE AA_JIMMY_LYING_DOWN_SMOKING
			RETURN <<0.01, 0.023, 0.02>>
		
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
			RETURN <<0.13, 0.075, 0.0>>
		
		CASE AA_HEIST_WE_IDLE_PHONE
			RETURN <<0.07, 0.05, 0.0>>
		
		CASE AA_HEIST_HACKER_TABLET
			RETURN <<0.0, 0.0, 0.0>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_BONE_OFFSET_ROTATION_TO_ATTACH_TO(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
			RETURN <<0.0, -90.0, 0.0>>
			
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			RETURN <<10.0, 0.0, 0.0>>
			
		CASE AA_JIMMY_STANDING_DRINKING
			RETURN <<-2.0, -5.0, 0.0>>
			
		CASE AA_JIMMY_STANDING_TEXTING
		CASE AA_HEIST_DRIVER_IDLE
			RETURN <<0.0, 10.0, 0.0>>
		
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
			RETURN <<270.0, 20.0, 10.0>>
		
		CASE AA_HEIST_WE_IDLE_PHONE
			RETURN <<270.0, 0.0, 0.0>>
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ ANIMATION DICTIONARIES ╞════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL DOES_ARCADE_ACTIVITY_HAVE_REDUCED_CLIPS(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_ELBOWS_ON_BAR_M
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARCADE_ANIM_MULTI_STAGED(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_AMBIENT	
		CASE AA_JIMMY_PRE_CABINET_SETUP
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
		CASE AA_WENDY_PRE_CABINET_SETUP
		CASE AA_WENDY_PRE_SCOPE_OUT_SETUP
		CASE AA_JIMMY_SOFA_WATCHING_TV
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
		CASE AA_JIMMY_STANDING_DRINKING
		CASE AA_JIMMY_STANDING_PHONE_CALL
		CASE AA_JIMMY_STANDING_TEXTING
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_JIMMY_PHONE_LEANING
		CASE AA_HEIST_DRIVER_INSPECTING
		CASE AA_HEIST_DRIVER_IDLE
		CASE AA_HEIST_DRIVER_CLIPBOARD
		CASE AA_HEIST_HACKER_COMPUTER
		CASE AA_HEIST_HACKER_COFFEE
		CASE AA_HEIST_HACKER_TABLET
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
		CASE AA_HEIST_WE_IDLE_PHONE
		CASE AA_ELBOWS_ON_BAR_M
			RETURN FALSE
	ENDSWITCH	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_ARCADE_ANIM_DICTIONARY_FOR_ACTIVITY()
	RETURN "ANIM_HEIST@ARCADE_COMBINED@"
ENDFUNC

FUNC STRING GET_ARCADE_PROP_ANIM_CLIP_FOR_ACTIVITY(PED_DATA &Data, INT iClip = 0)
	
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HEIST_WE_SAT_WITH_GUN		
			RETURN "ANIM@AMB@RANGE@ASSEMBLE_GUNS@Base_W_AR_CarbineRifleMK2"
		CASE AA_HEIST_WE_TESTING_GUN		
			RETURN "ANIM@AMB@RANGE@WEAPON_TEST@Weapon_Test_Busrt_01_W_AR_AssaultRifleMK2"
		CASE AA_JIMMY_LYING_DOWN_SMOKING
			SWITCH iClip
				CASE 0	RETURN "JIMMY@_SMOKING_BASE_P_CS_Ciggy_01b_S"
				CASE 1	RETURN "JIMMY@_SMOKING_IDLE_01_P_CS_Ciggy_01b_S"
				CASE 2	RETURN "JIMMY@_SMOKING_IDLE_02_P_CS_Ciggy_01b_S"
				CASE 3	RETURN "JIMMY@_SMOKING_IDLE_03_P_CS_Ciggy_01b_S"
			ENDSWITCH
		BREAK
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			SWITCH iClip
				CASE 0	RETURN "JIMMY@_DRINKING@_BEER_BASE"
				CASE 1	RETURN "JIMMY@_DRINKING@_BEER_IDLE_A"
				CASE 2	RETURN "JIMMY@_DRINKING@_BEER_IDLE_B"
				CASE 3	RETURN "JIMMY@_DRINKING@_BEER_IDLE_C"
				CASE 4	RETURN "JIMMY@_DRINKING@_BEER_IDLE_D"
				CASE 5	RETURN "JIMMY@_DRINKING@_BEER_IDLE_E"
				CASE 6	RETURN "JIMMY@_DRINKING@_BEER_IDLE_F"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC STRING GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY_BASE(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HANGOUT_WITHDRINK_M_01A					RETURN "PED_MALE@_STAND_WITHDRINK@_01A@_BASE_BASE"
		CASE AA_HANGOUT_WITHDRINK_M_01B					RETURN "PED_MALE@_STAND_WITHDRINK@_01B@_BASE_BASE"
		CASE AA_HANGOUT_F_3A							RETURN "PED_FEMALE@_STAND@_03A@_BASE_BASE"
		CASE AA_HANGOUT_F_3B							RETURN "PED_FEMALE@_STAND@_03B@_BASE_BASE"
		CASE AA_HANGOUT_CONVO_F_2A						RETURN "PED_FEMALE@_STAND@_02A@_BASE_BASE"		
		CASE AA_HANGOUT_WITHDRINK_F_01B					RETURN "PED_FEMALE@_STAND_WITHDRINK@_01B@_BASE_BASE"
		CASE AA_ELBOWS_ON_BAR_M							RETURN "AMB@PROP_HUMAN_SEAT_BAR@MALE@ELBOWS_ON_BAR@BASE"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC STRING GET_ARCADE_ANIM_CLIP_FOR_ACTIVITY(PED_DATA &Data, INT iClip = 0)
	
	// General
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_WENDY_PRE_CABINET_SETUP					RETURN "world_human_stand_mobile@_female@_standing@_call@_idle_a"
		CASE AA_WENDY_PRE_SCOPE_OUT_SETUP				RETURN "world_human_clipboard@_male@_idle_d"
		CASE AA_JIMMY_PRE_CABINET_SETUP					RETURN "WORLD_HUMAN_AA_SMOKE@_MALE@_IDLE_A"
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP				RETURN "world_human_seat_wall_tablet@_male@_idle_a"
		CASE AA_JIMMY_SOFA_WATCHING_TV					RETURN "SWITCH@_MICHAEL@_ON_SOFA_BASE_JIMMY"
		CASE AA_JIMMY_STANDING_DRINKING					RETURN "PED_MALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_A"
		CASE AA_JIMMY_STANDING_PHONE_CALL				RETURN "male@_standing@_call@_idle_a"
		CASE AA_JIMMY_STANDING_TEXTING					RETURN "WORLD_HUMAN_STAND_MOBILE@_MALE@_TEXT@_idle_a"
		CASE AA_JIMMY_PHONE_LEANING						RETURN "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@MOBILE@IDLE_A"
		CASE AA_HEIST_DRIVER_INSPECTING					RETURN "Inspecting_HIGH_Idle_01_inspector"
		CASE AA_HEIST_DRIVER_IDLE						RETURN "WORLD_HUMAN_STAND_MOBILE@_MALE@_TEXT@_idle_a"
		CASE AA_HEIST_DRIVER_CLIPBOARD					RETURN "AMB@WORLD_HUMAN_CLIPBOARD@MALE@IDLE_A_IDLE_A"
		CASE AA_HEIST_HACKER_COMPUTER					RETURN "prop_human_seat_computer@_male@_idle_a"
		CASE AA_HEIST_HACKER_COFFEE						RETURN "AMB@WORLD_HUMAN_LEANING@MALE@COFFEE@IDLE_A_IDLE_A"
		CASE AA_HEIST_HACKER_TABLET						RETURN "world_human_seat_wall_tablet@_male@_idle_a"
		CASE AA_HEIST_WE_SAT_WITH_GUN					RETURN "ANIM@AMB@RANGE@ASSEMBLE_GUNS@Base_AMY_Skater_01"
		CASE AA_HEIST_WE_TESTING_GUN					RETURN "Weapon_Test_Busrt_01_AMY_Skater_01"
		CASE AA_HEIST_WE_IDLE_PHONE						RETURN "stand_phone_lookaround_nowork"
		CASE AA_ELBOWS_ON_BAR_M							RETURN "AMB@PROP_HUMAN_SEAT_BAR@MALE@ELBOWS_ON_BAR@IDLE_A"
		CASE AA_JIMMY_LYING_DOWN_SMOKING
			SWITCH iClip
				CASE 0	RETURN "JIMMY@_SMOKING_BASE"
				CASE 1	RETURN "JIMMY@_SMOKING_IDLE_01"
				CASE 2	RETURN "JIMMY@_SMOKING_IDLE_02"
				CASE 3	RETURN "JIMMY@_SMOKING_IDLE_03"
			ENDSWITCH
		BREAK
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			SWITCH iClip
				CASE 0	RETURN "JIMMY@_DRINKING@_BASE"
				CASE 1	RETURN "JIMMY@_DRINKING@_IDLE_A"
				CASE 2	RETURN "JIMMY@_DRINKING@_IDLE_B"
				CASE 3	RETURN "JIMMY@_DRINKING@_IDLE_C"
				CASE 4	RETURN "JIMMY@_DRINKING@_IDLE_D"
				CASE 5	RETURN "JIMMY@_DRINKING@_IDLE_E"
				CASE 6	RETURN "JIMMY@_DRINKING@_IDLE_F"
			ENDSWITCH
		BREAK
		CASE AA_HANGOUT_WITHDRINK_M_01A
			SWITCH iClip
				CASE 0	RETURN "PED_MALE@_STAND_WITHDRINK@_01A@_IDLES_IDLE_A"
				CASE 1	RETURN "PED_MALE@_STAND_WITHDRINK@_01A@_IDLES_IDLE_B"
				CASE 2	RETURN "PED_MALE@_STAND_WITHDRINK@_01A@_IDLES_IDLE_C"
				CASE 3	RETURN "PED_MALE@_STAND_WITHDRINK@_01A@_IDLES_IDLE_D"
			ENDSWITCH
		BREAK
		CASE AA_HANGOUT_WITHDRINK_M_01B
			SWITCH iClip
				CASE 0	RETURN "PED_MALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_A"
				CASE 1	RETURN "PED_MALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_B"
				CASE 2	RETURN "PED_MALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_C"
				CASE 3	RETURN "PED_MALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_D"
			ENDSWITCH
		BREAK
		CASE AA_HANGOUT_F_3A
			SWITCH iClip
				CASE 0	RETURN "PED_FEMALE@_STAND@_03A@_IDLES_IDLE_A"
				CASE 1	RETURN "PED_FEMALE@_STAND@_03A@_IDLES_IDLE_B"
				CASE 2	RETURN "PED_FEMALE@_STAND@_03A@_IDLES_IDLE_C"
				CASE 3	RETURN "PED_FEMALE@_STAND@_03A@_IDLES_IDLE_D"
			ENDSWITCH
		BREAK
		CASE AA_HANGOUT_F_3B
			SWITCH iClip
				CASE 0	RETURN "PED_FEMALE@_STAND@_03B@_IDLES_IDLE_A"
				CASE 1	RETURN "PED_FEMALE@_STAND@_03B@_IDLES_IDLE_B"
				CASE 2	RETURN "PED_FEMALE@_STAND@_03B@_IDLES_IDLE_C"
				CASE 3	RETURN "PED_FEMALE@_STAND@_03B@_IDLES_IDLE_D"
			ENDSWITCH
		BREAK
		CASE AA_HANGOUT_CONVO_F_2A
			SWITCH iClip
				CASE 0	RETURN "PED_FEMALE@_STAND@_02A@_IDLES_CONVO_IDLE_A"
				CASE 1	RETURN "PED_FEMALE@_STAND@_02A@_IDLES_CONVO_IDLE_B"
				CASE 2	RETURN "PED_FEMALE@_STAND@_02A@_IDLES_CONVO_IDLE_C"
				CASE 3	RETURN "PED_FEMALE@_STAND@_02A@_IDLES_CONVO_IDLE_D"
			ENDSWITCH
		BREAK
		CASE AA_HANGOUT_WITHDRINK_F_01B
			SWITCH iClip
				CASE 0	RETURN "PED_FEMALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_A"
				CASE 1	RETURN "PED_FEMALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_B"
				CASE 2	RETURN "PED_FEMALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_C"
				CASE 3	RETURN "PED_FEMALE@_STAND_WITHDRINK@_01B@_IDLES_IDLE_D"
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// Gender dependant
	IF (Data.bFemale)
		SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
			CASE AA_AMBIENT	RETURN "amb_world_human_hang_out_street_female_hold_arm_idle_b"
		ENDSWITCH		
	ELSE
		SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
			CASE AA_AMBIENT	RETURN "amb_world_human_hang_out_street_male_c_base"
		ENDSWITCH		
	ENDIF
	
	RETURN "amb_world_human_hang_out_street_female_hold_arm_idle_b"
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ ANIMATION CLIPS ╞════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

FUNC BOOL SHOULD_ARCADE_PROP_BE_ATTACHED_FOR_ACTIVITY(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_ARCADE_ACTIVITY_USE_SYCNED_SCENE(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARCADE_ACTIVITY_SYCNED_SCENE_LOOPED(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_HEIST_WE_TESTING_GUN
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ARCADE_ACTIVITY_NEED_EXPANDED_CAPSULE(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_HEIST_HACKER_TABLET
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARCADE_ACTIVITY_SEATED(PED_DATA &Data)
	SWITCH INT_TO_ENUM(ARCADE_ACTIVITY, Data.iActivity)
		CASE AA_JIMMY_PRE_SCOPE_OUT_SETUP
		CASE AA_JIMMY_SOFA_WATCHING_TV
		CASE AA_JIMMY_SOFA_DRINKING_WATCHING_TV
		CASE AA_JIMMY_LYING_DOWN_SMOKING
		CASE AA_HEIST_HACKER_COMPUTER
		CASE AA_HEIST_HACKER_TABLET
		CASE AA_HEIST_WE_SAT_WITH_GUN
		CASE AA_ELBOWS_ON_BAR_M
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC GET_INITIAL_DATA_FOR_PED(SERVER_BROADCAST_DATA ServerBD, PED_DATA &Data, INT iID, BOOL bCabinetsSetupMissionComplete, BOOL bScopeOutSetupMissionComplete, CASINO_HEIST_DRIVERS eHeistDriver, CASINO_HEIST_HACKERS eHeistHacker, CASINO_HEIST_WEAPON_EXPERTS eHeistWeaponExpert)
	SWITCH ServerBD.iLayout
		CASE 0
			SWITCH iID
				CASE 0	// Jimmy
					IF NOT bCabinetsSetupMissionComplete
						Data.vPosition = <<2732.426, -387.6855, -49.02532>>
						Data.fHeading = 17.280
						Data.bFemale = FALSE
						Data.iActivity = ENUM_TO_INT(AA_JIMMY_PRE_CABINET_SETUP)
						Data.iLevel = 0
					ELIF NOT bScopeOutSetupMissionComplete
						Data.vPosition = <<2724.772, -383.1500, -48.9625>>
						Data.fHeading = -90.0
						Data.bFemale = FALSE
						Data.iActivity = ENUM_TO_INT(AA_JIMMY_PRE_SCOPE_OUT_SETUP)
						Data.iLevel = 0
					ELSE
						SWITCH ServerBD.iJimmySpawnLocation
							CASE 0	// Arcade - Sofa - Watching TV
								Data.vPosition = <<2737.947, -381.5125, -48.56255>>
								Data.fHeading = 1.250
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_SOFA_WATCHING_TV)
								Data.iLevel = 0
							BREAK
							CASE 1	// Arcade - Sofa - Drinking and Watching TV
								Data.vPosition = <<2738.195, -380.763, -49.430>>
								Data.fHeading = 95.0
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_SOFA_DRINKING_WATCHING_TV)
								Data.iLevel = 0
							BREAK
							CASE 2	// Arcade - Standing - Drinking
								Data.vPosition = <<2723.006, -389.2750, -49.0089>>
								Data.fHeading = -74.500
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_DRINKING)
								Data.iLevel = 0
							BREAK
							CASE 3	// Arcade - Standing - Phone Call
								Data.vPosition = <<2735.208, -380.530, -49.345>>
								Data.fHeading = 38.160
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_PHONE_CALL)
								Data.iLevel = 0
							BREAK
							CASE 4	// Arcade - Standing - Texting
								Data.vPosition = <<2722.576, -382.3125, -49.0050>>
								Data.fHeading = -43.0
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_TEXTING)
								Data.iLevel = 0
							BREAK
							CASE 5	// Arcade - Lying down behind bar - Smoking
								Data.vPosition = <<2728.902, -389.2875, -49.990>>
								Data.fHeading = 88.75
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_LYING_DOWN_SMOKING)
								Data.iLevel = 0
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE 1	// Wendy
					IF NOT bCabinetsSetupMissionComplete
						Data.vPosition = <<2723.778, -384.875, -49.02532>>
						Data.fHeading = -57.0
						Data.bFemale = TRUE
						Data.iActivity = ENUM_TO_INT(AA_WENDY_PRE_CABINET_SETUP)
						Data.iLevel = 0
					ELIF NOT bScopeOutSetupMissionComplete
						Data.vPosition = <<2731.507, -381.1375, -49.02532>>
						Data.fHeading = 141.750
						Data.bFemale = TRUE
						Data.iActivity = ENUM_TO_INT(AA_WENDY_PRE_SCOPE_OUT_SETUP)
						Data.iLevel = 0
					ELSE
						// Wendy is spawned in the bartender script after setup complete.
						Data.bSkipPed = TRUE
					ENDIF
				BREAK
				CASE 2	// Heist Driver
					IF bCabinetsSetupMissionComplete
					AND bScopeOutSetupMissionComplete
					AND eHeistDriver != CASINO_HEIST_DRIVER__NONE
						SWITCH ServerBD.iHeistDriverSpawnLocation
							CASE 0	// Inspecting
								Data.vPosition = <<2710.603, -354.328, -56.162>>
								Data.fHeading = 104.500
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_DRIVER_INSPECTING)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 1	// Idle
								Data.vPosition = <<2710.709, -355.79, -56.162>>
								Data.fHeading = -128.000
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_DRIVER_IDLE)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 2	// Leaning Phone Call
								Data.vPosition = <<2709.916, -357.1025, -55.16993>>
								Data.fHeading = -90.000
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_PHONE_LEANING)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 3	// Clipboard - Needs getaway vehicle spawned
								Data.vPosition = <<2694.033, -358.24, -55.18243>>
								Data.fHeading = -68.500
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_DRIVER_CLIPBOARD)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
						ENDSWITCH
					ELSE
						Data.bSkipPed = TRUE
					ENDIF
				BREAK
				CASE 3	// Heist Hacker
					IF bCabinetsSetupMissionComplete
					AND bScopeOutSetupMissionComplete
					AND eHeistHacker != CASINO_HEIST_HACKER__NONE
						SWITCH ServerBD.iHeistHackerSpawnLocation
							CASE 0	// On Computer
								Data.vPosition = <<2712.357, -352.9247, -55.65743>>
								Data.fHeading = 0.0
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_HACKER_COMPUTER)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 1	// Texting on Phone
								Data.vPosition = <<2711.475, -352.9872, -55.19493>>
								Data.fHeading = -19.250
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_TEXTING)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 2	// Drinking Coffee
								Data.vPosition = <<2713.429, -352.94, -55.19493>>
								Data.fHeading = 180.0
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_HACKER_COFFEE)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 3	// Sitting on ledge playing with tablet
								Data.vPosition = <<2716.49, -359.0622, -54.59493>>
								Data.fHeading = 180.0
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_HACKER_TABLET)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
						ENDSWITCH
					ELSE
						Data.bSkipPed = TRUE
					ENDIF
				BREAK
				CASE 4	// Weapon Expert
					IF bCabinetsSetupMissionComplete
					AND bScopeOutSetupMissionComplete
					AND eHeistWeaponExpert != CASINO_HEIST_WEAPON_EXPERT__NONE
						SWITCH ServerBD.iHeistWeaponExpertSpawnLocation
							CASE 0	// Sat with Gun
								Data.vPosition = <<2712.863, -356.7349, -55.82305>>
								Data.fHeading = -150.500
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_WE_SAT_WITH_GUN)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 1	// Testing Gun
								Data.vPosition = <<2714.122, -355.6775, -56.15743>>
								Data.fHeading = -99.250
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_WE_TESTING_GUN)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
							CASE 2	// Idle on Phone
								Data.vPosition = <<2715.202, -357.2525, -55.20743>>
								Data.fHeading = 90.000
								Data.bFemale = FALSE
								Data.iActivity = ENUM_TO_INT(AA_HEIST_WE_IDLE_PHONE)
								Data.iLevel = 0
								Data.bBasementPed = TRUE
							BREAK
						ENDSWITCH
					ELSE
						Data.bSkipPed = TRUE
					ENDIF
				BREAK
				CASE 5
					Data.vPosition = <<2725.3936, -382.9775, -49.9124>>
					Data.fHeading = 34.9000
					Data.bFemale = FALSE
					Data.iPackedDrawable = 4608
					Data.iPackedTexture = 13314
					Data.iActivity = ENUM_TO_INT(AA_HANGOUT_WITHDRINK_M_01B)
					Data.iLevel = 1
					Data.iPedModelVariation = 0
				BREAK
				CASE 6
					Data.vPosition = <<2724.9746, -382.3050, -49.9999>>
					Data.fHeading = 217.6650
					Data.bFemale = TRUE
					Data.iPackedDrawable = 65
					Data.iPackedTexture = 4608
					Data.iActivity = ENUM_TO_INT(AA_HANGOUT_CONVO_F_2A)
					Data.iLevel = 1
					Data.iPedModelVariation = 1
				BREAK
				CASE 7
					Data.vPosition = <<2729.8884, -388.9200, -49.3124>>
					Data.fHeading = 300.0000
					Data.bFemale = FALSE
					Data.iPackedDrawable = 37377
					Data.iPackedTexture = 33587265
					Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_PHONE_CALL)
					Data.iLevel = 2
					Data.iPedModelVariation = 1
				BREAK
				CASE 8
					Data.vPosition = <<2725.3784, -386.4875, -49.2124>>
					Data.fHeading = 90.0000
					Data.bFemale = FALSE
					Data.iPackedDrawable = 16781313
					Data.iPackedTexture = 268444160
					Data.iActivity = ENUM_TO_INT(AA_ELBOWS_ON_BAR_M)
					Data.iLevel = 2
					Data.iPedModelVariation = 2
				BREAK
				CASE 9
					Data.vPosition = <<2734.2324, -387.2575, -49.3799>>
					Data.fHeading = 134.5650
					Data.bFemale = FALSE
					Data.iPackedDrawable = 4160
					Data.iPackedTexture = 16781826
					Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_TEXTING)
					Data.iLevel = 3
					Data.iPedModelVariation = 3
					
					IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
						Data.bSkipPed = TRUE
					ENDIF
				BREAK
				CASE 10
					Data.vPosition = <<2733.6694, -387.7975, -49.3624>>
					Data.fHeading = -47.5750
					Data.bFemale = FALSE
					Data.iPackedDrawable = 134218241
					Data.iPackedTexture = 33559553
					Data.iActivity = ENUM_TO_INT(AA_JIMMY_STANDING_TEXTING)
					Data.iLevel = 3
					Data.iPedModelVariation = 2
					
					IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
						Data.bSkipPed = TRUE
					ENDIF
				BREAK
				CASE 11
					Data.vPosition = <<2725.3135, -387.1650, -49.9525>>
					Data.fHeading = 38.6400
					Data.bFemale = TRUE
					Data.iPackedDrawable = 4673
					Data.iPackedTexture = 9281
					Data.iActivity = ENUM_TO_INT(AA_HANGOUT_F_3A)
					Data.iLevel = 4
					Data.iPedModelVariation = 0
				BREAK
				CASE 12
					Data.vPosition = <<2728.1426, -387.5425, -49.1974>>
					Data.fHeading = 180.0000
					Data.bFemale = FALSE
					Data.iPackedDrawable = 1
					Data.iPackedTexture = 8704
					Data.iActivity = ENUM_TO_INT(AA_ELBOWS_ON_BAR_M)
					Data.iLevel = 4
					Data.iPedModelVariation = 0
				BREAK
				CASE 13
					Data.vPosition = <<2736.2815, -374.2325, -49.9999>>
					Data.fHeading = 6.4800
					Data.bFemale = TRUE
					Data.iPackedDrawable = 16777216
					Data.iPackedTexture = 4673
					Data.iActivity = ENUM_TO_INT(AA_HANGOUT_WITHDRINK_F_01B)
					Data.iLevel = 5
					Data.iPedModelVariation = 2
				BREAK
				CASE 14
					Data.vPosition = <<2736.2366, -373.4775, -50.0024>>
					Data.fHeading = 190.1650
					Data.bFemale = FALSE
					Data.iPackedDrawable = 16777857
					Data.iPackedTexture = 8194
					Data.iActivity = ENUM_TO_INT(AA_AMBIENT)
					Data.iLevel = 5
					Data.iPedModelVariation = 3
				BREAK
				CASE 15
					Data.vPosition = <<2736.4185, -379.0775, -49.3649>>
					Data.fHeading = 100.6400
					Data.bFemale = FALSE
					Data.iPackedDrawable = 65
					Data.iPackedTexture = 6721
					Data.iActivity = ENUM_TO_INT(AA_HANGOUT_WITHDRINK_M_01A)
					Data.iLevel = 6
					Data.iPedModelVariation = 4
				BREAK
				CASE 16
					Data.vPosition = <<2735.6836, -379.1575, -49.3649>>
					Data.fHeading = 280.6300
					Data.bFemale = TRUE
					Data.iPackedDrawable = 4097
					Data.iPackedTexture = 9281
					Data.iActivity = ENUM_TO_INT(AA_HANGOUT_F_3B)
					Data.iLevel = 6
					Data.iPedModelVariation = 1
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF  //FEATURE_CASINO_HEIST
