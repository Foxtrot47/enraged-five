//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_race_to_point.sch																		//
// Description: Controls the  [CS_IMPROMPTU]  functionality. 													//
// Written by:  Ryan Baker																					//
// Date: 23/08/2012																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
//USING "building_control_public.sch"
USING "menu_public.sch"
USING "shop_public.sch"

USING "net_events.sch"
USING "net_comms_public.sch"
USING "net_mission_joblist.sch"

USING "net_mission.sch"
USING "fm_unlocks_header.sch"

CONST_INT IMPROMPTU_DM_INVITE_RESPONSE_TIME	30000

FUNC BOOL IS_ONE_ON_ONE_VEH_DM()
	RETURN g_sImpromptuVars.bVehicleDM
ENDFUNC

PROC SET_ONE_ON_ONE_VEH_DM()
	IF g_sImpromptuVars.bVehicleDM = FALSE
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SET_ONE_ON_ONE_VEH_DM ") 
		g_sImpromptuVars.bVehicleDM = TRUE
	ENDIF
ENDPROC

//FUNC BOOL AM_I_IN_SUITABLE_VEH_FOR_ONE_ON_ONE()
//	RETURN IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI)
//ENDFUNC

//FUNC BOOL IS_RIVAL_IN_SUITABLE_VEH_FOR_ONE_ON_ONE()
//	RETURN IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_RIVAL_IN_A_HELI)
//ENDFUNC

PROC SET_UP_VEHICLE_DM_VARS()
	IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI)
		IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_RIVAL_IN_A_HELI)
			SET_ONE_ON_ONE_VEH_DM()
		ELSE
			PRINTLN("    ----->    [CS_IMPROMPTU], SET_UP_VEHICLE_DM_VARS, (FALSE) IS_RIVAL_IN_SUITABLE_VEH_FOR_ONE_ON_ONE ", NATIVE_TO_INT(g_sImpromptuVars.playerImpromptuRival))
		ENDIF
	ELSE
		PRINTLN("    ----->    [CS_IMPROMPTU], SET_UP_VEHICLE_DM_VARS, (FALSE) IS_PLAYER_IN_A_ONE_ONE_ONE_VEHICLE", NATIVE_TO_INT(g_sImpromptuVars.playerImpromptuRival))
	ENDIF
ENDPROC

FUNC BOOL HAS_IMPROMPTU_TIMER_EXPIRED()
	IF HAS_NET_TIMER_STARTED(g_sImpromptuVars.InviteTimer)
			IF g_sImpromptuVars.iBossDmType != BOSS_COOPERATIVE
			IF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, IMPROMPTU_DM_INVITE_RESPONSE_TIME)
			
				PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - HAS_NET_TIMER_EXPIRED") 
			
				RETURN TRUE
			ENDIF
		ELSE
			IF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, 60000)
			
				PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - HAS_NET_TIMER_EXPIRED - BOSS_COOPERATIVE") 
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



FUNC INT GET_IMPROMPTU_INVITE_TIMEOUT()
	IF g_sImpromptuVars.iBossDmType = BOSS_COOPERATIVE
		RETURN 60000
	ENDIF
	
	RETURN 15000
ENDFUNC
//PURPOSE: Checks to see if the player should end the  [CS_IMPROMPTU] 
FUNC BOOL SHOULD_END_BOSSVBOSS_DM(BOOL bDoAlreadyRunningCheck = TRUE)

	// need to replace with something else
//	// Check if our challenger has respawned
//	IF bDoChallengerAliveCheck
//		IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerThatSentImpromptuInvite, TRUE)
//		
//			PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - IS_NET_PLAYER_OK ") 
//		
//			RETURN FALSE
//		ENDIF
//	ENDIF

	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - NETWORK_IS_IN_TUTORIAL_SESSION") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - IS_PLAYER_ON_ANY_FM_MISSION") 
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - IS_FM_MISSION_LAUNCH_IN_PROGRESS") 
		RETURN FALSE
	ENDIF
	
	IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_KILL_LIST)
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - FMMC_TYPE_KILL_LIST") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - MPAM_TYPE_TIME_TRIAL") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - MPAM_TYPE_TIME_TRIAL") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - MPAM_TYPE_CINEMA") 
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)") 
		RETURN FALSE
	ENDIF
	
	IF bDoAlreadyRunningCheck = TRUE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("gb_deathmatch")) > 0
			PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - bDoAlreadyRunningCheck") 
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
	OR IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PLAYER_ID())
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SHOULD_END_BOSSVBOSS_DM - IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR ") 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Checks to see if the player should end the  [CS_IMPROMPTU] 
FUNC BOOL SAFE_TO_LAUNCH_BOSSVBOSS_DM()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("gb_deathmatch")) > 0
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - ALREADY RUNNING") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - IS_PLAYER_ON_ANY_FM_MISSION") 
		RETURN FALSE
	ENDIF
		
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - IS_PLAYER_ON_MP_AMBIENT_SCRIPT - MPAM_TYPE_CINEMA") 
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - IS_FM_MISSION_LAUNCH_IN_PROGRESS") 
		RETURN FALSE
	ENDIF
			
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - BROWSING SHOP") 
		RETURN FALSE
	ENDIF
	
//	IF IS_CUSTOM_MENU_ON_SCREEN()
//		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - CUSTOM MENU ON SCREEN") 
//		RETURN FALSE
//	ENDIF
	
		IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_PENNED_IN	
			//-- Actively taking part in Penned In
			PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - FMMC_TYPE_PENNED_IN") 
			RETURN FALSE
		ENDIF
		
		IF g_sImpromptuVars.playerThatSentImpromptuInvite != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(g_sImpromptuVars.playerThatSentImpromptuInvite)
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(g_sImpromptuVars.playerThatSentImpromptuInvite, MPAM_TYPE_TIME_TRIAL)
				PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - IS_PLAYER_ON_MP_AMBIENT_SCRIPT(g_sImpromptuVars.playerThatSentImpromptuInvite, MPAM_TYPE_TIME_TRIAL)") 
			ENDIF
		ENDIF
	 //FEATURE_NEW_AMBIENT_EVENTS
	
	IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
	OR IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PLAYER_ID())
		PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_BOSSVBOSS_DM - IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR ") 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

 // FEATURE_GANG_BOSS

//PURPOSE: Checks to see if the player should end the  [CS_IMPROMPTU] 
FUNC BOOL SHOULD_END_IMPROMPTU_DM(BOOL bDoAlreadyRunningCheck = TRUE)

	// need to replace with something else
//	// Check if our challenger has respawned
//	IF bDoChallengerAliveCheck
//		IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerThatSentImpromptuInvite, TRUE)
//		
//			PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - IS_NET_PLAYER_OK ") 
//		
//			RETURN FALSE
//		ENDIF
//	ENDIF

	IF NETWORK_IS_IN_TUTORIAL_SESSION()
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - NETWORK_IS_IN_TUTORIAL_SESSION") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - IS_PLAYER_ON_ANY_FM_MISSION") 
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - IS_FM_MISSION_LAUNCH_IN_PROGRESS") 
		RETURN FALSE
	ENDIF
	
	IF IS_FM_PLAYER_CRITICAL_TO_THIS_FM_EVENT(PLAYER_ID(), FMMC_TYPE_KILL_LIST)
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - FMMC_TYPE_KILL_LIST") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - MPAM_TYPE_TIME_TRIAL") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - MPAM_TYPE_TIME_TRIAL") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - MPAM_TYPE_CINEMA") 
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)
		PRINTLN("     ----->    [CS_IMPROMPTU]  - IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, ciFM_EVENT_USING_KILL_LIST_VEHICLE)") 
		RETURN FALSE
	ENDIF
	
	IF bDoAlreadyRunningCheck = TRUE
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Impromptu_DM_Controler")) > 0
			PRINTLN("     ----->    [CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - bDoAlreadyRunningCheck") 
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
	OR IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PLAYER_ID())
		PRINTLN("     ----->   	[CS_IMPROMPTU]  - SHOULD_END_IMPROMPTU_DM - IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR ") 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Checks to see if the player should end the  [CS_IMPROMPTU] 
FUNC BOOL SAFE_TO_LAUNCH_IMPROMPTU_DM()
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Impromptu_DM_Controler")) > 0
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - ALREADY RUNNING") 
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - IS_PLAYER_ON_ANY_FM_MISSION") 
		RETURN FALSE
	ENDIF
		
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA)
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - IS_PLAYER_ON_MP_AMBIENT_SCRIPT - MPAM_TYPE_CINEMA") 
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - IS_FM_MISSION_LAUNCH_IN_PROGRESS") 
		RETURN FALSE
	ENDIF
			
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - BROWSING SHOP") 
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - CUSTOM MENU ON SCREEN") 
		RETURN FALSE
	ENDIF
	
		IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_PENNED_IN	
			//-- Actively taking part in Penned In
			PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - FMMC_TYPE_PENNED_IN") 
			RETURN FALSE
		ENDIF
		
		IF g_sImpromptuVars.playerThatSentImpromptuInvite != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(g_sImpromptuVars.playerThatSentImpromptuInvite)
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(g_sImpromptuVars.playerThatSentImpromptuInvite, MPAM_TYPE_TIME_TRIAL)
				PRINTLN("     ----->    [CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - IS_PLAYER_ON_MP_AMBIENT_SCRIPT(g_sImpromptuVars.playerThatSentImpromptuInvite, MPAM_TYPE_TIME_TRIAL)") 
			ENDIF
		ENDIF
	 //FEATURE_NEW_AMBIENT_EVENTS
	
	IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
	OR IS_PLAYER_DOING_SIMPLE_INTERIOR_AUTOWARP(PLAYER_ID())
		PRINTLN("     ----->   	[CS_IMPROMPTU]  - SAFE_TO_LAUNCH_IMPROMPTU_DM - IS_LOCAL_PLAYER_IN_SIMPLE_INTERIOR ") 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns the next free  [CS_IMPROMPTU]  instance
FUNC INT GET_FREE_IMPROMPTU_DN_INSTANCE()
//	
//	INT i
//	REPEAT NUM_NETWORK_PLAYERS i
//		IF NOT NETWORK_IS_SCRIPT_ACTIVE("FM_Impromptu_DM_Controler", i)
//			PRINTLN("     ----->    [CS_IMPROMPTU]  - GET_FREE_IMPROMPTU_DN_INSTANCE - FOUND    i = ") NET_PRINT_INT(i) 
//			RETURN i
//		ENDIF
//	ENDREPEAT
//	
//	PRINTLN("     ----->    [CS_IMPROMPTU]  - GET_FREE_IMPROMPTU_DN_INSTANCE - FAILED    i = ") NET_PRINT_INT(i) 
//	NET_SCRIPT_ASSERT("GET_FREE_IMPROMPTU_DN_INSTANCE - Couldn't find free instance")
//	RETURN -1
	RETURN NATIVE_TO_INT(PLAYER_ID())
ENDFUNC

PROC RESET_INVITE_TIMER()
	IF HAS_NET_TIMER_STARTED(g_sImpromptuVars.InviteTimer)
		PRINTLN("RESET_INVITE_TIMER")
		RESET_NET_TIMER(g_sImpromptuVars.InviteTimer)
	ENDIF
ENDPROC

//PURPOSE: Controls the player choosing to launch a  [CS_IMPROMPTU] 
PROC PROCESS_START_IMPROMPTU_DM()
	// INVITE SENT
	IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST_AWAITING_REPLY)
		
		IF NOT HAS_NET_TIMER_STARTED(g_sImpromptuVars.InviteTimer)
			PRINTLN(" ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - START_INVITE_TIMER")
			START_NET_TIMER(g_sImpromptuVars.InviteTimer)
		ELSE
			// DECLINED
			IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_REQUEST_REJECTED)
			OR (HAS_NET_TIMER_STARTED(g_sImpromptuVars.InviteTimer) AND HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, (IMPROMPTU_DM_INVITE_RESPONSE_TIME + 5000)))
			OR IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS)
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET( g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS)
						PRINTLN("[CS_IMPROMPTU]  REQUEST_REJECTED, ciIMPROMPTU_KILL_ALL_REQUESTS")
					ELIF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_REQUEST_REJECTED)
						PRINTLN("[CS_IMPROMPTU]  REQUEST_REJECTED, ciIMPROMPTU_REQUEST_REJECTED")
					ELIF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, (IMPROMPTU_DM_INVITE_RESPONSE_TIME + 5000))
						PRINTLN("[CS_IMPROMPTU]  REQUEST_REJECTED, HAS_NET_TIMER_EXPIRED")
					ENDIF
					#ENDIF
					PRINT_HELP("IDM_INV_TXTN")
					RESET_IMPROMPTU_GLOBALS()
					SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)

					PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - REQUEST_REJECTED ") NET_PRINT_INT(0) 
					EXIT
				ENDIF
			ELSE
				// ACCEPTED
				IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_REQUEST_ACCEPTED)
					
					SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU)
					SET_KILLSTRIP_AUTO_RESPAWN_TIME(0)
								
					IF SAFE_TO_LAUNCH_IMPROMPTU_DM()
						IF g_sImpromptuVars.iInvitedToInstance > -1
						AND g_sImpromptuVars.iInvitedToInstance < NUM_NETWORK_PLAYERS
							SET_UP_VEHICLE_DM_VARS()
							g_sImpromptuVars.iLaunchStage = 0
							PRINTLN(" ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH - iLaunchStage = 0  iInstanceId = ") NET_PRINT_INT(g_sImpromptuVars.iInvitedToInstance) 
						ELSE
							PRINTLN("----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH FAILED - INVALID INSTANCE  iInstanceId = ") NET_PRINT_INT(g_sImpromptuVars.iInvitedToInstance) 
						ENDIF
					ELSE
						PRINTLN(" ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - NOT SAFE TO LAUNCH  <-----     ") 
					ENDIF
					CLEAR_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_REQUEST_ACCEPTED)
					SET_IMPROMPTU_SPAWN_GLOBAL(TRUE)
					
				
				ENDIF
			ENDIF
		ENDIF
		
	// LOCAL PLAYER INVITED
	ELIF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST)
		IF SAFE_TO_LAUNCH_IMPROMPTU_DM()
			INT iFreeInstance = GET_FREE_IMPROMPTU_DN_INSTANCE()
			IF iFreeInstance > -1
			AND iFreeInstance < NUM_NETWORK_PLAYERS
				g_sImpromptuVars.iInviteStage = 0
				g_sImpromptuVars.iInvitedToInstance = iFreeInstance
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH - iLaunchStage = 0  iInstanceId = ", g_sImpromptuVars.iInvitedToInstance) 
			ELSE
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH FAILED - INVALID INSTANCE  iInstanceId = ", g_sImpromptuVars.iInvitedToInstance) 
			ENDIF
		ELSE
			PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - NOT SAFE TO LAUNCH  <-----     ") 
		ENDIF
		CLEAR_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST)
	ENDIF

ENDPROC

//PURPOSE: Controls receiveing and dealing with an invite from a  [CS_IMPROMPTU] 
PROC PROCESS_IMPROMPTU_DM_INVITE()

	//Check if  [CS_IMPROMPTU]  has been launched
	IF g_sImpromptuVars.iInviteStage <> -1
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Impromptu_DM_Controler")) > 0
			g_sImpromptuVars.iInviteStage = -1
			PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_IMPROMPTU_DM_INVITE - SCRIPT LAUNCHED OR ABOUT TO - RESET  <-----     ") 
			EXIT
		ENDIF
	ENDIF
	
	//Control Accept Invite Stages
	SWITCH g_sImpromptuVars.iInviteStage
		
		//Control having received an invite
		CASE -1
			IF g_sImpromptuVars.iInvitedToInstance <> -1
			AND g_sImpromptuVars.iLaunchStage = -1
				IF SHOULD_END_IMPROMPTU_DM()
					CLEAR_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS)
					PRINTLN("g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS")
					RESET_INVITE_TIMER()

// KEITH 20/6/13: Switch to Joblist Invites					
//					Delete_Text_Messages_With_This_Label("IDM_INV_TXT")
					g_sImpromptuVars.iInviteStage = 0
					PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_IMPROMPTU_DM_INVITE - INVITE RECEIVED - iInviteStage = 0  <-----     ") 
				ELSE
					g_sImpromptuVars.iInvitedToInstance = -1
					g_sImpromptuVars.iInviteStage = -1
					RESET_INVITE_TIMER()
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_ONE_ON_ONE_DM)
					PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_IMPROMPTU_DM_INVITE - INVITE RECEIVED - CAN'T DO  [CS_IMPROMPTU]  - iInviteStage = -1  <-----     ") 
				ENDIF
			ENDIF
		BREAK
		
		//Control having received an invite
		CASE 0
			INT iTempBitSet, iFee
			FLOAT fFee
			SET_BIT(iTempBitSet, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE) // KGM 17/6/13: doesn't get delayed by recent invite
			PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_IMPROMPTU_DM_INVITE - TEXT MESSAGE INVITE FROM ") NET_PRINT(GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite)) 
// KEITH 20/6/13: Switch to Joblist Invites					
//			IF Request_MP_Comms_Txtmsg_With_Components_From_Player(g_sImpromptuVars.playerThatSentImpromptuInvite, "IDM_INV_TXT", GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite), TRUE, NO_INT_SUBSTRING_COMPONENT_VALUE, "", "", TRUE)//, iTempBitSet)	Request_MP_Comms_Txtmsg_From_Player
// KEITH 12/7/13: Now need to pass in 'wager' value for display on invite					
//			Store_Basic_Invite_Details_For_Joblist_From_Player(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_IMPROMPTU_DM, "")
			fFee = (TO_FLOAT(ONE_ON_ONE_ENTRY_FEE) * g_sMPTunables.fImpromptuDmEntryMultiplier)
			iFee = ROUND(fFee)
			iFee = MULTIPLY_CASH_BY_TUNABLE(iFee)
			Store_Basic_Invite_Details_For_Joblist_From_Player_With_Params(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_IMPROMPTU_DM, "", "", TRUE, iFee)
			g_sImpromptuVars.iInviteStage = 1
			PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_IMPROMPTU_DM_INVITE - TEXT MESSAGE RECEIVED - iInviteStage = 1  <-----     ") 
			PRINTLN(" ----->    [CS_IMPROMPTU]  - CASE 0 - START_INVITE_TIMER")
			START_NET_TIMER(g_sImpromptuVars.InviteTimer)
//			ENDIF
		BREAK
		
		//Wait for Response
		CASE 1
			//Check for Timeout
			IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS)		
				Cancel_Basic_Invite_From_Player(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_IMPROMPTU_DM)
				g_sImpromptuVars.iInvitedToInstance = -1
				g_sImpromptuVars.iInviteStage = -1
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_IMPROMPTU_DM_INVITE - CLEANUP - ciIMPROMPTU_KILL_ALL_REQUESTS<-----     ") 
				RESET_INVITE_TIMER()
				IF g_sImpromptuVars.playerThatSentImpromptuInvite <> INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(g_sImpromptuVars.playerThatSentImpromptuInvite, FALSE)
					REPLY_TO_IMPROMPTU_CHALLENGE(g_sImpromptuVars.playerThatSentImpromptuInvite, FALSE, -1, FALSE, GlobalServerBD.iLaunchPosix)
				ENDIF
				g_sImpromptuVars.iBitSet = 0
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_ONE_ON_ONE_DM)
				
				EXIT
			ELSE
				IF NOT SHOULD_END_IMPROMPTU_DM(TRUE)
				OR HAS_IMPROMPTU_TIMER_EXPIRED()
					PRINTLN("SHOULD_END_IMPROMPTU_DM, ciIMPROMPTU_KILL_ALL_REQUESTS")
					SET_BIT( g_sImpromptuVars.iBitSet, ciIMPROMPTU_KILL_ALL_REQUESTS)
				ENDIF
			ENDIF
			
			// Check for Response
			IF (Has_Player_Accepted_Basic_Invite_From_Player(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_IMPROMPTU_DM))
				SET_UP_VEHICLE_DM_VARS()
				REPLY_TO_IMPROMPTU_CHALLENGE(g_sImpromptuVars.playerThatSentImpromptuInvite, TRUE, g_sImpromptuVars.iInvitedToInstance, IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI), GlobalServerBD.iLaunchPosix)
				Kill_This_MP_Txtmsg_Comms("IDM_INV_TXT")
				g_sImpromptuVars.iLaunchStage = 0
				g_sImpromptuVars.iBitSet = 0
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - INVITE ACCEPTED - START LAUNCH - iInvitedToInstance = ") NET_PRINT_INT(g_sImpromptuVars.iInvitedToInstance) 
				g_sImpromptuVars.iInviteStage = -1
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - INVITE ACCEPTED - START LAUNCH - iInviteStage = -1  <-----     ") 
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_START_IMPROMPTU_DM - INVITE ACCEPTED - START LAUNCH - iLaunchStage = 0  <-----     ") 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Launches the  [CS_IMPROMPTU]  script
PROC PROCESS_LAUNCH_IMPROMPTU_DM()
	MP_MISSION_DATA missionData
	SWITCH g_sImpromptuVars.iLaunchStage
		//Launch  [CS_IMPROMPTU] 
		CASE 0
			missionData.mdID.idMission 	= eFM_IMPROMPTU_DM
			missionData.iInstanceId 	= g_sImpromptuVars.iInvitedToInstance
			IF IS_ONE_ON_ONE_VEH_DM()
				PRINTLN(" -->  [CS_IMPROMPTU], IS_ONE_ON_ONE_VEH_DM() = TRUE ")
				SET_BIT(missionData.iBitSet, ciIMPROMPTU_DOG_FIGHT)
			ELSE
				PRINTLN(" -->  [CS_IMPROMPTU], IS_ONE_ON_ONE_VEH_DM() = FALSE ")
			ENDIF
			PRINTLN(" -->  [CS_IMPROMPTU]  will launch with instance ID ",missionData.iInstanceId)
		   	IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
				//BROADCAST_JOIN_THIS_AMBIENT_MP_MISSION_EVENT(MPGlobalsAmbience.R2Pdata.missionData, SPECIFIC_PLAYER(PLAYER_ID()))
				g_sImpromptuVars.iLaunchStage++
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_LAUNCH_IMPROMPTU_DM - LAUNCHED - iLaunchStage = 1  <-----     ") 
			ENDIF
		BREAK
		
		//Check if  [CS_IMPROMPTU]  has Started
		CASE 1
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Impromptu_DM_Controler")) > 0
//				RESET_IMPROMPTU_GLOBALS()
				PRINTLN("     ----->    [CS_IMPROMPTU]  - PROCESS_LAUNCH_IMPROMPTU_DM - STARTED - iLaunchStage = -1  <-----     ") 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_PLAYER_IN_UNSUITABLE_PROPERTY()

	IF IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(),FALSE)
		IF NOT IS_PLAYER_IN_CLUBHOUSE(PLAYER_ID())
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC


PROC REJECT_BOSSVBOSS_INVITE(INT iRejectReason = BOSS_V_BOSS_ACTIVELY_REJECT)
	PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - REJECT_BOSSVBOSS_INVITE Called...")
	 GB_DELETE_INVITE_FROM_PLAYER(g_sImpromptuVars.playerThatSentImpromptuInvite)
	//	Cancel_Basic_Invite_From_Player(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_GB_BOSS_DEATHMATCH)
	g_sImpromptuVars.iBVBInvitedToInstance = -1
	g_sImpromptuVars.iBVBInviteStage = -1
	PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - CLEANUP - ciBOSSVBOSS_KILL_ALL_REQUESTS<-----     ") 
	RESET_INVITE_TIMER()
	RESET_NET_TIMER(g_sImpromptuVars.timeBVBInvHelp)
	
	IF g_sImpromptuVars.iBossDmType != BOSS_COOPERATIVE
	//Clear gang boss mission launch stage
	//GB_CLEAR_MISSION_LAUNCH_PROGRESS()
	ENDIF
	
	REPLY_TO_BOSSVBOSS_CHALLENGE(g_sImpromptuVars.playerThatSentImpromptuInvite, FALSE, -1, FALSE, iRejectReason)
	g_sImpromptuVars.iBitSet = 0
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_ONE_ON_ONE_DM)
ENDPROC

//PURPOSE: Controls receiveing and dealing with an invite from a BOSS V BOSS DM
PROC PROCESS_BOSSVBOSS_DM_INVITE()

	//Check if  [CS_IMPROMPTU]  has been launched
	IF g_sImpromptuVars.iBVBInviteStage <> -1
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("GB_DEATHMATCH")) > 0
			g_sImpromptuVars.iBVBInviteStage = -1
			PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - SCRIPT LAUNCHED OR ABOUT TO - RESET  <-----     ") 
			EXIT
		ENDIF
	ENDIF
	
	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
	//	PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE EXIT AS GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG / GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION") 
		EXIT
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(),TRUE)

		IF g_sImpromptuVars.iBVBInviteStage <> -1
			PRINTLN("[BOSSVBOSS] [CS_IMPROMPTU] [PROCESS_BOSSVBOSS_DM_INVITE] Rejecting invite as GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION")
			REJECT_BOSSVBOSS_INVITE(BOSS_V_BOSS_UNABLE_TO_TAKE_PART)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 100 = 0
			IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [PROCESS_BOSSVBOSS_DM_INVITE] Exit as GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION....")
			ELSE
				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [PROCESS_BOSSVBOSS_DM_INVITE] Exit as GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION - There is an active request! From Player ", GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite))
			ENDIF
		ENDIF
		#ENDIF
		
		EXIT

	ENDIF
	
	IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
		IF g_sImpromptuVars.iBVBInviteStage <> -1
			PRINTLN("[BOSSVBOSS] [CS_IMPROMPTU] [PROCESS_BOSSVBOSS_DM_INVITE] Rejecting invite as FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT")
			REJECT_BOSSVBOSS_INVITE(BOSS_V_BOSS_UNABLE_TO_TAKE_PART)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 100 = 0
			IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [PROCESS_BOSSVBOSS_DM_INVITE] Exit as FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT....")
			ELSE
				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [PROCESS_BOSSVBOSS_DM_INVITE] Exit as FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT - There is an active request! From Player ", GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite))
			ENDIF
		ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	IF GET_FRAME_COUNT() % 100 = 0
		IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
			PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] [BOSSVBOSS_EXTRA_DEBUG] [PROCESS_BOSSVBOSS_DM_INVITE] There is an active request! From Player ", GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite), " iBVBLaunchStage = ", g_sImpromptuVars.iBVBLaunchStage, " iBVBInviteStage = ", g_sImpromptuVars.iBVBInviteStage, " iBitSet = ", g_sImpromptuVars.iBitSet)
		ENDIF
	ENDIF
	
	//Control Accept Invite Stages
	SWITCH g_sImpromptuVars.iBVBInviteStage
		
		//Control having received an invite
		CASE -1
			IF g_sImpromptuVars.iBVBInvitedToInstance <> -1
			AND g_sImpromptuVars.iBVBLaunchStage = -1
				IF SHOULD_END_BOSSVBOSS_DM()
					CLEAR_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
					PRINTLN("g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS")
					RESET_INVITE_TIMER()
					RESET_NET_TIMER(g_sImpromptuVars.timeBVBInvHelp)
				//	g_sImpromptuVars.iBVBInviteStage = 0
					PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - INVITE RECEIVED - SHOULD_END_BOSSVBOSS_DM  <-----     ") 
				ELSE
					g_sImpromptuVars.iBVBInvitedToInstance = -1
					g_sImpromptuVars.iBVBInviteStage = -1
					RESET_INVITE_TIMER()
					RESET_NET_TIMER(g_sImpromptuVars.timeBVBInvHelp)
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_ONE_ON_ONE_DM)
					PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - INVITE RECEIVED - CAN'T DO  [CS_IMPROMPTU]  - iBVBInviteStage = -1  <-----     ") 
				ENDIF
			ENDIF
		BREAK
		
		//Control having received an invite
		CASE 0
			INT iTempBitSet, iFee
			FLOAT fFee
			SET_BIT(iTempBitSet, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE) // KGM 17/6/13: doesn't get delayed by recent invite
			PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - TEXT MESSAGE INVITE FROM ") NET_PRINT(GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite)) 

			fFee = (TO_FLOAT(ONE_ON_ONE_ENTRY_FEE) * g_sMPTunables.fImpromptuDmEntryMultiplier)
			iFee = ROUND(fFee)
			iFee = MULTIPLY_CASH_BY_TUNABLE(iFee)
			
			
			// Save out a BVB Invite
			STRUCT_GANG_INVITE_DATA sInviteData
			BOSS_AGENCY_INVITE_TYPE eType 
			eType = BOSS_APP_INVITE_TYPE_BOSS_V_BOSS
			
			IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_JOUST
				eType = BOSS_APP_INVITE_TYPE_BIKER_V_BIKER_JOUST
			ELIF g_sImpromptuVars.iBossDmType = BOSS_COOPERATIVE
				eType = BOSS_APP_INVITE_TYPE_BIKER_COOP
			ENDIF
			GB_STORE_NEW_INVITE_DETAILS(g_sImpromptuVars.playerThatSentImpromptuInvite, eType,GIT_DEBUG,sInviteData)
			
			
	//		Store_Basic_Invite_Details_For_Joblist_From_Player_With_Params(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_GB_BOSS_DEATHMATCH, "", "", TRUE)
			//SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_DO_BVB_INVITE_HELP)
			g_sImpromptuVars.iBVBInviteStage = 1
			PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - TEXT MESSAGE RECEIVED - iBVBInviteStage = 1  <-----     ") 
			PRINTLN(" ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - CASE 0 - START_INVITE_TIMER")
			START_NET_TIMER(g_sImpromptuVars.InviteTimer)
//			ENDIF
		BREAK
		
		//Wait for Response
		CASE 1
			//Check for Timeout
			IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACCEPT_BVB_INVITE_HELP)	
				IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)		
					PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - Reject invite as ciBOSSVBOSS_KILL_ALL_REQUESTS..")
					REJECT_BOSSVBOSS_INVITE()
					EXIT
				ELSE
					IF NOT SHOULD_END_BOSSVBOSS_DM(TRUE)
						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU] SHOULD_END_BOSSVBOSS_DM, ciBOSSVBOSS_KILL_ALL_REQUESTS - SHOULD_END_BOSSVBOSS_DM")
						SET_BIT( g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
					ELIF IS_PLAYER_IN_UNSUITABLE_PROPERTY()
						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU] SHOULD_END_BOSSVBOSS_DM, ciBOSSVBOSS_KILL_ALL_REQUESTS - IS_PLAYER_IN_MP_PROPERTY")
						SET_BIT( g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
					ELIF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, GET_IMPROMPTU_INVITE_TIMEOUT())
						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU] SHOULD_END_BOSSVBOSS_DM, ciBOSSVBOSS_KILL_ALL_REQUESTS - HAS_NET_TIMER_EXPIRED")
						SET_BIT( g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
					ELIF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_INVITE_HELP_TIMEOUT)
						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU] SHOULD_END_BOSSVBOSS_DM, ciBOSSVBOSS_KILL_ALL_REQUESTS - as ciBOSSVBOSS_BVB_INVITE_HELP_TIMEOUT")
						SET_BIT( g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
					ENDIF
				ENDIF
			ENDIF
			
			// Check for Response
	//		IF (Has_Player_Accepted_Basic_Invite_From_Player(g_sImpromptuVars.playerThatSentImpromptuInvite, FMMC_TYPE_GB_BOSS_DEATHMATCH))
			IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACCEPT_BVB_INVITE_HELP)	
				IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
					SET_UP_VEHICLE_DM_VARS()
				ENDIF
				REPLY_TO_BOSSVBOSS_CHALLENGE(g_sImpromptuVars.playerThatSentImpromptuInvite, TRUE, g_sImpromptuVars.iBVBInvitedToInstance, IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI))
				
				
				IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_JOUST
					GB_SET_PLAYER_LAUNCHING_GANG_BOSS_MISSION(FMMC_TYPE_BIKER_JOUST)
				ELIF g_sImpromptuVars.iBossDmType = BOSS_COOPERATIVE
					//GB_SET_PLAYER_LAUNCHING_GANG_BOSS_MISSION(g_sImpromptuVars.iFmmcType)
					GB_SET_PLAYER_ALLIED_BOSS(g_sImpromptuVars.playerThatSentImpromptuInvite)
				ELSE
					GB_SET_PLAYER_LAUNCHING_GANG_BOSS_MISSION(FMMC_TYPE_GB_BOSS_DEATHMATCH)
				ENDIF
				IF g_sImpromptuVars.iBossDmType != BOSS_COOPERATIVE
					GB_SET_LOCAL_PLAYER_MISSION_HOST(g_sImpromptuVars.playerThatSentImpromptuInvite)
					GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
				ENDIF
					
			//	GB_GET_MY_GANG_MEMBERS_TO_JOIN_BOSSVBOSS_DM(g_sImpromptuVars.iBVBInvitedToInstance)
				GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM)
				Kill_This_MP_Txtmsg_Comms("IDM_INV_TXT")
				g_sImpromptuVars.iBVBLaunchStage = 0
				g_sImpromptuVars.iBitSet = 0
				RESET_NET_TIMER(g_sImpromptuVars.timeBVBInvHelp)
				RESET_NET_TIMER(g_sImpromptuVars.InviteTimer)
				PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - INVITE ACCEPTED - START LAUNCH - iBVBInvitedToInstance = ") NET_PRINT_INT(g_sImpromptuVars.iBVBInvitedToInstance) 
				g_sImpromptuVars.iBVBInviteStage = -1
				PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - INVITE ACCEPTED - START LAUNCH - iBVBInviteStage = -1  <-----     ") 
				PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_BOSSVBOSS_DM_INVITE - INVITE ACCEPTED - START LAUNCH - iBVBLaunchStage = 0  <-----     ") 
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_MY_BOSS_ACCEPTING_BOSSVBOSS_DM()
	IF NOT GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
			IF g_sImpromptuVars.iBVBInvitedToInstance <> -1
			AND IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_ACCEPTED)
				g_sImpromptuVars.iBVBLaunchStage = 0
				g_sImpromptuVars.iBitSet = 0
				
				#IF IS_DEBUG_BUILD
					PLAYER_INDEX playerMyBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
					PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_MY_BOSS_ACCEPTING_BOSSVBOSS_DM MY BOSS ", GET_PLAYER_NAME(playerMyBoss) , "ACCEPTED, WILL JOIN INSTANCE ", g_sImpromptuVars.iBVBInvitedToInstance) 
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


 // FEATURE_BIKER
//PURPOSE: Controls the player choosing to launch a Boss V Boss DM
PROC PROCESS_START_BOSSVBOSS_DM()
	// INVITE SENT
	TEXT_LABEL_15 tl15Ticker
	
	
	IF GET_FRAME_COUNT() % 100 = 0
		IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
			PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] [BOSSVBOSS_EXTRA_DEBUG] [PROCESS_START_BOSSVBOSS_DM] There is an active request! From Player ", GET_PLAYER_NAME(g_sImpromptuVars.playerThatSentImpromptuInvite), " iBVBLaunchStage = ", g_sImpromptuVars.iBVBLaunchStage, " iBVBInviteStage = ", g_sImpromptuVars.iBVBInviteStage, " iBitSet = ", g_sImpromptuVars.iBitSet)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST_AWAITING_REPLY)
		IF GET_FRAME_COUNT() % 100 = 0
			IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
				PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] [BOSSVBOSS_EXTRA_DEBUG] [PROCESS_START_BOSSVBOSS_DM] I'm awaiting reply!")
			ENDIF
		ENDIF
	
		//-- Ticker saying we've invited someone
		IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_INVITE_DONE_SEND_TICKER)
			IF NETWORK_IS_PLAYER_ACTIVE(g_sImpromptuVars.playerImpromptuRival)
				SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_INVITE_DONE_SEND_TICKER)
				
				
				IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
					IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
						PRINT_TICKER_WITH_PLAYER_NAME("BVB_TICKS", g_sImpromptuVars.playerImpromptuRival)
					ELSE
						PRINT_TICKER_WITH_PLAYER_NAME("BKVBK_TICKS", g_sImpromptuVars.playerImpromptuRival)
					ENDIF
				ELIF g_sImpromptuVars.iBossDmType = BOSSVBOSS_JOUST
				
					PRINT_TICKER_WITH_PLAYER_NAME("JST_TICKS", g_sImpromptuVars.playerImpromptuRival)
				ELIF g_sImpromptuVars.iBossDmType = BOSS_COOPERATIVE
					tl15Ticker = "BKR_INVRESC"
					TEXT_LABEL_15 subText = GET_BIKER_INVITE_FEED_TITLE()
					PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING(tl15Ticker, g_sImpromptuVars.playerImpromptuRival,subText)
				ENDIF
				PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] DONE SEND INVITE TICKER")
			ENDIF
			
			SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
		ENDIF	

		IF g_sImpromptuVars.iBVBLaunchStage < 0
			IF NETWORK_IS_PLAYER_ACTIVE(g_sImpromptuVars.playerImpromptuRival)
				IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_ACCEPTED)
					IF NATIVE_TO_INT(g_sImpromptuVars.playerImpromptuRival) != -1
						IF GlobalplayerBD_FM[NATIVE_TO_INT(g_sImpromptuVars.playerImpromptuRival)].iCurrentMissionType != FMMC_TYPE_GB_BOSS_DEATHMATCH
							IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
							OR g_sImpromptuVars.iBossDmType = BOSSVBOSS_JOUST
								IF NOT Is_This_The_Current_Objective_Text("BVB_INVITW")
									INT iTempGangID = GET_GANG_ID_FOR_PLAYER(g_sImpromptuVars.playerImpromptuRival)
									IF iTempGangID > -1
										HUD_COLOURS hcTempGang = GET_HUD_COLOUR_FOR_GANG_ID(iTempGangID)
										Print_Objective_Text_With_Coloured_Player_Name("BVB_INVITW", g_sImpromptuVars.playerImpromptuRival, hcTempGang) 
									ELSE
										PRINTLN(" ----->     [dsw] [BOSSVBOSS] CAN't DO OBJECTIVE TEXT BECAUSE iTempGangID = ", iTempGangID)
										IF IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PPAF_GB_BOSS_V_BOSS_DM)
											CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_REJECTED)
				
							IF IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PPAF_GB_BOSS_V_BOSS_DM)
								CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
							ENDIF
							PRINTLN(" ----->     [dsw] [BOSSVBOSS] SET ciBOSSVBOSS_REQUEST_REJECTED AS RIVAL BOSS ALREADY ON FMMC_TYPE_GB_BOSS_DEATHMATCH")
						ENDIF
					ENDIF
				ENDIF
							
			ELSE
				SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_REJECTED)
				
				IF IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PPAF_GB_BOSS_V_BOSS_DM)
					CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
				ENDIF
				PRINTLN(" ----->     [dsw] [BOSSVBOSS] SET ciBOSSVBOSS_REQUEST_REJECTED AS RIVAL BOSS NO LONGER ACTIVE")
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_sImpromptuVars.InviteTimer)
			PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM - START_INVITE_TIMER")
			START_NET_TIMER(g_sImpromptuVars.InviteTimer)
		ELSE
			// ACCEPTED
			IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_ACCEPTED)
				
				SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU)
				SET_KILLSTRIP_AUTO_RESPAWN_TIME(0)
							
				IF SAFE_TO_LAUNCH_BOSSVBOSS_DM()
					IF g_sImpromptuVars.iBVBInvitedToInstance > -1
					AND g_sImpromptuVars.iBVBInvitedToInstance < NUM_NETWORK_PLAYERS
						IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
							SET_UP_VEHICLE_DM_VARS()
						ENDIF
						g_sImpromptuVars.iBVBLaunchStage = 0
						PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH - iBVBLaunchStage = 0  iInstanceId = ") NET_PRINT_INT(g_sImpromptuVars.iBVBInvitedToInstance) 
					ELSE
						PRINTLN("----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH FAILED - INVALID INSTANCE  iInstanceId = ") NET_PRINT_INT(g_sImpromptuVars.iBVBInvitedToInstance) 
					ENDIF
					
					IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
						GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM)
						g_sGb_Telemetry_data.iBossVsBossInvitesAccepted++
						PRINTLN(" ----->     [dsw] [BOSSVBOSS][CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM g_sGb_Telemetry_data.iBossVsBossInvitesAccepted = ", g_sGb_Telemetry_data.iBossVsBossInvitesAccepted)
					ENDIF
			//		GB_GET_MY_GANG_MEMBERS_TO_JOIN_BOSSVBOSS_DM(g_sImpromptuVars.iBVBInvitedToInstance)
				ELSE
					PRINTLN(" ----->     [dsw] [BOSSVBOSS][CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM - NOT SAFE TO LAUNCH 2 <-----     ") 
				ENDIF
				CLEAR_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_ACCEPTED)
				IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
					SET_IMPROMPTU_SPAWN_GLOBAL(TRUE)
				ENDIF
				Clear_Any_Objective_Text_From_This_Script()
				CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
			// DECLINED
			ELIF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_REJECTED)
			OR IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
			//	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					#IF IS_DEBUG_BUILD
					IF IS_BIT_SET( g_sImpromptuVars.iBitSet, ciBOSSVBOSS_KILL_ALL_REQUESTS)
						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU]  REQUEST_REJECTED, ciBOSSVBOSS_KILL_ALL_REQUESTS")
					ELIF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_REQUEST_REJECTED)
						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU]  REQUEST_REJECTED, ciBOSSVBOSS_REQUEST_REJECTED")
//					ELIF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, (IMPROMPTU_DM_INVITE_RESPONSE_TIME + 5000))
//						PRINTLN("[dsw] [BOSSVBOSS] [CS_IMPROMPTU]  REQUEST_REJECTED, HAS_NET_TIMER_EXPIRED")
					ENDIF
					#ENDIF
				//	PRINT_HELP("BVB_INVR") // Your Boss Vs. Boss deathmatch invite was declined.
					//-- Ticker saying invite was rejected
					IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_INVITE_DONE_REJECT_TICKER)
						IF NETWORK_IS_PLAYER_ACTIVE(g_sImpromptuVars.playerImpromptuRival)
						AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
						OR GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
						
							SET_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_INVITE_DONE_REJECT_TICKER)
							
							IF g_sImpromptuVars.iBossDmType = BOSSVBOSS_DM
								IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(PLAYER_ID())
									PRINT_TICKER_WITH_PLAYER_NAME("BVB_TICKR", g_sImpromptuVars.playerImpromptuRival)
								ELSE
									IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_UNABLE_TO_TAKE_PART)
										PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] DONE REJECT INVITE TICKER - ciBOSSVBOSS_BVB_UNABLE_TO_TAKE_PART")
										PRINT_TICKER_WITH_PLAYER_NAME("BKVBK_TICKU", g_sImpromptuVars.playerImpromptuRival)
									ELSE
										PRINT_TICKER_WITH_PLAYER_NAME("BKVBK_TICKR", g_sImpromptuVars.playerImpromptuRival)
									ENDIF
								ENDIF
							ELIF g_sImpromptuVars.iBossDmType = BOSSVBOSS_JOUST
								IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_BVB_UNABLE_TO_TAKE_PART)
									PRINT_TICKER_WITH_PLAYER_NAME("JST_TICKR", g_sImpromptuVars.playerImpromptuRival)
									PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] DONE REJECT INVITE TICKER - ciBOSSVBOSS_BVB_UNABLE_TO_TAKE_PART - Joust")
								ELSE	
									PRINT_TICKER_WITH_PLAYER_NAME("JST_TICKRU", g_sImpromptuVars.playerImpromptuRival)
								ENDIF
							ELIF g_sImpromptuVars.iBossDmType = BOSS_COOPERATIVE
								tl15Ticker = "BKR_DECRESC"
								TEXT_LABEL_15 subText = GET_BIKER_INVITE_FEED_TITLE()
								PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING(tl15Ticker, g_sImpromptuVars.playerImpromptuRival,subText)
							ENDIF
							 // FEATURE_BIKER
							
							PRINTLN(" ----->     [dsw] [BOSSVBOSS] [CS_IMPROMPTU] DONE REJECT INVITE TICKER")
						ENDIF
					ENDIF
		
					//Clear gang boss mission launch stage
					IF g_sImpromptuVars.iBossDmType != BOSS_COOPERATIVE
						GB_PUBLIC_CLEAR_MISSION_LAUNCH_PROGRESS()
					ENDIF
		
					RESET_IMPROMPTU_GLOBALS()
					SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
					
					Clear_Any_Objective_Text_From_This_Script()
					CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
					PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM - REQUEST_REJECTED ") NET_PRINT_INT(0) 
					EXIT
			//	ENDIF
			ELIF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.InviteTimer, 60000)
				IF IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PPAF_GB_BOSS_V_BOSS_DM)
					PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM InviteTimer expired and recieved no reply")
					CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
				ENDIF
			ENDIF
		ENDIF

	// LOCAL PLAYER INVITED
	ELIF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
		IF SAFE_TO_LAUNCH_BOSSVBOSS_DM()
			INT iFreeInstance = GET_FREE_IMPROMPTU_DN_INSTANCE()
			IF iFreeInstance > -1
			AND iFreeInstance < NUM_NETWORK_PLAYERS
				g_sImpromptuVars.iBVBInviteStage = 0
				g_sImpromptuVars.iBVBInvitedToInstance = iFreeInstance
				PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH - iBVBInviteStage = 0  iInstanceId = ", g_sImpromptuVars.iBVBInvitedToInstance) 
			ELSE
				PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM -  [CS_IMPROMPTU]  TRIGGERED - START LAUNCH FAILED - INVALID INSTANCE  iInstanceId = ", g_sImpromptuVars.iBVBInvitedToInstance) 
			ENDIF
		ELSE
			REJECT_BOSSVBOSS_INVITE(BOSS_V_BOSS_UNABLE_TO_TAKE_PART)
			PRINTLN("     ----->    [dsw] [BOSSVBOSS] [CS_IMPROMPTU]  - PROCESS_START_BOSSVBOSS_DM - NOT SAFE TO LAUNCH 1 <-----     ") 
		ENDIF
		CLEAR_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
	ENDIF

ENDPROC

PROC PROCESS_LAUNCH_BOSSVBOSS_DM()
	//MP_MISSION_DATA missionData
	SWITCH g_sImpromptuVars.iBVBLaunchStage
		//Launch  [CS_IMPROMPTU] 
		CASE 0
//			missionData.mdID.idMission 	= eAM_BOSS_VS_BOSS_DM
//			missionData.iInstanceId 	= g_sImpromptuVars.iBVBInvitedToInstance
////			IF IS_ONE_ON_ONE_VEH_DM()
////				PRINTLN(" --> [dsw] [CS_IMPROMPTU], IS_ONE_ON_ONE_VEH_DM() = TRUE ")
////				SET_BIT(missionData.iBitSet, ciIMPROMPTU_DOG_FIGHT)
////			ELSE
////				PRINTLN(" -->  [CS_IMPROMPTU], IS_ONE_ON_ONE_VEH_DM() = FALSE ")
////			ENDIF
//			PRINTLN(" -->  [dsw] [CS_IMPROMPTU] [PROCESS_LAUNCH_BOSSVBOSS_DM] will launch with instance ID ",missionData.iInstanceId)
//		   	IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
//				g_sImpromptuVars.iBVBLaunchStage++
//				PRINTLN("     ----->    [dsw] [CS_IMPROMPTU] [PROCESS_LAUNCH_BOSSVBOSS_DM] - LAUNCHED - iBVBLaunchStage = 1  <-----     ") 
//			ENDIF
			IF IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST_AWAITING_REPLY)
				IF g_sImpromptuVars.iBossDmType != BOSS_COOPERATIVE
					IF GB_GET_LOCAL_PLAYER_MISSION_HOST() = INVALID_PLAYER_INDEX()
						GB_SET_LOCAL_PLAYER_MISSION_HOST(PLAYER_ID())
					ENDIF
					GB_SET_MISSION_LAUNCH_STAGE(eGB_MISSION_STAGE_PREPARE)
				ENDIF
				CLEAR_BIT(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST_AWAITING_REPLY)
				PRINTLN("     ----->    [dsw] [CS_IMPROMPTU] [PROCESS_LAUNCH_BOSSVBOSS_DM] - LAUNCHED - Clear ciBOSSVBOSS_ACTIVE_REQUEST_AWAITING_REPLY")
			ENDIF
			g_sImpromptuVars.iBVBLaunchStage++
			PRINTLN("     ----->    [dsw] [CS_IMPROMPTU] [PROCESS_LAUNCH_BOSSVBOSS_DM] - LAUNCHED - iBVBLaunchStage = 1  <-----     ")
		BREAK
		
		//Check if  [CS_IMPROMPTU]  has Started
		CASE 1
//			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FM_Impromptu_DM_Controler")) > 0
//				PRINTLN("     ----->    [dsw] [CS_IMPROMPTU] [PROCESS_LAUNCH_BOSSVBOSS_DM] - STARTED - iBVBLaunchStage = -1  <-----     ") 
//			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC
 // FEATURE_GANG_BOSS

//Maintain player in a Heli Flag
PROC MAINTAIN_PLAYER_IN_A_HELI()
	//If a DM request is not under way
	IF NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST)
	AND NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_ACTIVE_REQUEST_AWAITING_REPLY)
	AND NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST_AWAITING_REPLY)
	AND NOT IS_BIT_SET(g_sImpromptuVars.iBitSet, ciBOSSVBOSS_ACTIVE_REQUEST)
		//Check every 1.1 seconds
		IF HAS_NET_TIMER_EXPIRED(g_sImpromptuVars.HeliCheckTimer, 2500)
			
			
			//IPlayer ok
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				
				#IF IS_DEBUG_BUILD
				BOOL bInHeli = IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI)
				#ENDIF
				//Clear the Heli flag
				CLEAR_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI)
				//Check to see if they are in a helicopter
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					VEHICLE_INDEX viVehicleIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(viVehicleIndex)
						
						IF IS_MODEL_VEH_DM_MODEL(GET_ENTITY_MODEL(viVehicleIndex))
							PRINTLN("[CS_IMPROMPTU] MAINTAIN_PLAYER_IN_A_HELI SET_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI)")
							SET_BIT(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI)
							g_mnImpromptuVehicle =  GET_ENTITY_MODEL(viVehicleIndex)
							REINIT_NET_TIMER(g_sImpromptuVars.HeliCheckTimer)
							#IF IS_DEBUG_BUILD
							IF NOT bInHeli
								PRINTLN("[CS_IMPROMPTU] MAINTAIN_PLAYER_IN_A_HELI - g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI")
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Launches the  [CS_IMPROMPTU]  script
PROC MAINTAIN_STARTING_IMPROMPTU_DM()
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_MP_PASSIVE_MODE_ENABLED()
	AND NOT IS_PLAYER_IN_UNSUITABLE_PROPERTY()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
	AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), FALSE)

		MAINTAIN_PLAYER_IN_A_HELI()
		PROCESS_START_IMPROMPTU_DM()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			PROCESS_IMPROMPTU_DM_INVITE()
			PROCESS_LAUNCH_IMPROMPTU_DM()
		ENDIF
	ELSE
		IF IS_PLAYER_IN_UNSUITABLE_PROPERTY()
			PROCESS_IMPROMPTU_DM_INVITE()
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_STARTING_BOSSVBOSS_DM()
	IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
	AND NOT IS_MP_PASSIVE_MODE_ENABLED()
	AND NOT IS_PLAYER_IN_UNSUITABLE_PROPERTY()
	AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), FALSE)
//		#IF IS_DEBUG_BUILD
//		IF GET_FRAME_COUNT() % 100 = 0
//			PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [MAINTAIN_STARTING_BOSSVBOSS_DM] Trying to launch....")
//		ENDIF
//		#ENDIF
		
		MAINTAIN_PLAYER_IN_A_HELI()
		PROCESS_START_BOSSVBOSS_DM()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			PROCESS_BOSSVBOSS_DM_INVITE()
			PROCESS_LAUNCH_BOSSVBOSS_DM()
			PROCESS_MY_BOSS_ACCEPTING_BOSSVBOSS_DM()
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD
//		IF GET_FRAME_COUNT() % 100 = 0
//			IF IS_PLAYER_ON_BOSSVBOSS_DM()
//				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [MAINTAIN_STARTING_BOSSVBOSS_DM] failing - IS_PLAYER_ON_BOSSVBOSS_DM")
//			ELIF IS_MP_PASSIVE_MODE_ENABLED()
//				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [MAINTAIN_STARTING_BOSSVBOSS_DM] failing - IS_MP_PASSIVE_MODE_ENABLED")
//			ELIF IS_PLAYER_IN_UNSUITABLE_PROPERTY()
//				PRINTLN("[BOSSVBOSS_EXTRA_DEBUG] [MAINTAIN_STARTING_BOSSVBOSS_DM] failing - IS_PLAYER_IN_MP_PROPERTY")
//			ENDIF
//		ENDIF
//		#ENDIF
		// Still maintain the invite while in a property so we can clean up and send the rejection
		IF IS_PLAYER_IN_UNSUITABLE_PROPERTY()
			PROCESS_BOSSVBOSS_DM_INVITE()
		ENDIF
	ENDIF
ENDPROC
 // #IF FEATURE_GANG_BOSS
// 


