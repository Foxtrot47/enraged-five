USING "net_simple_interior.sch"

/// PURPOSE:
///    
/// PARAMS:
///    cutscene - 
///    eSimpleInteriorForCutscene (optional) - simple interior that the cutscene is played in, all coords will be relative
PROC SIMPLE_CUTSCENE_CREATE(SIMPLE_CUTSCENE &cutscene, STRING strName, SIMPLE_INTERIORS eSimpleInteriorForCutscene = SIMPLE_INTERIOR_INVALID)
	cutscene.strName = strName
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_CREATE - Created.")
	#ENDIF
	
	IF eSimpleInteriorForCutscene != SIMPLE_INTERIOR_INVALID
		cutscene.bUseRelativePos = TRUE
		TEXT_LABEL_63 txtFoo
		
		BOOL bSecondaryDetails = FALSE
		
		IF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		AND GET_INTERIOR_FLOOR_INDEX() = 0
			bSecondaryDetails = TRUE
		ENDIF
		
		GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION(eSimpleInteriorForCutscene, txtFoo, cutscene.vBasePos, cutscene.fBaseHeading  , bSecondaryDetails )
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_CUTSCENE, "[SIMPLE_CUTSCENE] SIMPLE_CUTSCENE_CREATE - With simple interior: ", GET_SIMPLE_INTERIOR_DEBUG_NAME(eSimpleInteriorForCutscene))
		#ENDIF
	ENDIF
ENDPROC
