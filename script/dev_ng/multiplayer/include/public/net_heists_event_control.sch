/* ============================================================================================= *\
		MISSION NAME	:	net_heists_event_control.sch
		AUTHOR			:	Alastair Costley - 2013/12/10
		DESCRIPTION		:	Sends and processes event commands for the heist planning board.
\* ============================================================================================= */

USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
//USING "context_control_public.sch"
//USING "net_ambience.sch"
USING "MP_Scaleform_Functions.sch"
//USING "fmmc_corona_controller.sch"
USING "mp_globals_fm.sch"
USING "mp_globals_common_definitions.sch"


//USING "net_heists_common.sch"
USING "net_heists_public.sch"

/* ============================================================================================= *\
		GLOBAL STRUCTS FOR EVENTS
\* ============================================================================================= */


// Update the heist roles (E.G. driver, gunner, etc)
STRUCT m_sEventHeistUpdateRoles
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for changing roles.
	HEIST_ROLES	ePlayerRoles[MAX_HEIST_PLAYER_SLOTS]
ENDSTRUCT

// Update the board with the initial heist configuration as defined in the cloud.
STRUCT m_sEventHeistSetupInitData
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT 	iHeistTotalPay
	INT 	iHeistTotalCosts
	INT 	iHeistTotalRules
	INT 	iHeistTotalTeams
	INT 	iUniqueSessionHash0
	INT 	iUniqueSessionHash1
	INT		iPlayerOrder[MAX_HEIST_PLAYER_SLOTS]
ENDSTRUCT

// Update the clients with the latest cut data.
STRUCT m_sEventHeistUpdateCuts
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for initial setup.
	INT 	iHeistCutPercent[MAX_HEIST_PLAYER_SLOTS+1] // +1 for the fake player pot.
	INT 	iUniqueSessionHash0
	INT 	iUniqueSessionHash1
ENDSTRUCT

// Update the clients with the latest heist data.
//STRUCT m_sEventHeistUpdateOutfits
//	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
//	// Add custom data for initial setup.
//	INT 	iHeistOutfits[MAX_HEIST_PLAYER_SLOTS]
//ENDSTRUCT

// Update the clients with the player's heist status.
STRUCT m_sEventHeistUpdateMemberStatus
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for initial setup.
	INT		iHeistPlanningStatus
ENDSTRUCT

// Update a single heist member's role.
STRUCT m_sEventHeistUpdateRoleSingleton
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for changing roles.
	INT			index
	HEIST_ROLES	ePlayerRole
	INT 		iTargetPlayer
ENDSTRUCT

// Update a single heist member's status.
STRUCT m_sEventHeistUpdateCutSingleton
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for changing cuts.
	INT			index
	INT			iHeistCutModifier
	INT			iCutTotal
	INT 		iUniqueSessionHash0
	INT 		iUniqueSessionHash1
ENDSTRUCT

// Update a single heist member's status.
STRUCT m_sEventHeistUpdateCutSingletonTutorial
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for changing cuts.
	INT			index
	INT			iHeistCutModifier
	INT			iCutTotal
	INT			iSource
	INT 		iUniqueSessionHash0
	INT 		iUniqueSessionHash1
ENDSTRUCT

// Update a single heist member's outfit.
//STRUCT m_sEventHeistUpdateOutfitSingleton
//	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
//	// Add custom data for changing roles.
//	INT			index
//	INT			iOutfit
//ENDSTRUCT

STRUCT m_sEventHeistRemoveMember
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for removing a member.
	INT			index
ENDSTRUCT

STRUCT m_sEventHeistRequestMembers
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for the download request target.
	INT			iHeistLeaderIndex
ENDSTRUCT

STRUCT m_sEventHeistDownloadSavedMember
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Custom data that contains the saved names of the previous participants of a heist pre-planning mission.
	INT 			iMissionID
	INT				iMemberNumber
	TEXT_LABEL_63 	tlMemberUID
ENDSTRUCT

STRUCT m_sEventHeistAbandonPlanning
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data containing the heist leader ID for people to check against. If they are in the leader's apt, then abandon the active heist.
	INT			iHeistLeaderIndex
ENDSTRUCT

STRUCT m_sEventHeistMedalDetails
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data containing the heist leader ID for people to check against. If they are in the leader's apt, then abandon the active heist.
	HEIST_MEDAL_DATA sPlayerMedals[MAX_HEIST_PLANNING_ROWS] // 28 INTs, still less than maximum of 44.
ENDSTRUCT

// Update the host with heist saved cache data.
STRUCT m_sEventHeistReSyncHost
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	// Add custom data for initial setup.
	HEIST_ROLES	ePlayerRoles	[MAX_HEIST_PLAYER_SLOTS]
	INT 		iHeistCutPercent[MAX_HEIST_PLAYER_SLOTS]
//	INT 		iHeistOutfits	[MAX_HEIST_PLAYER_SLOTS]
ENDSTRUCT


/* ============================================================================================= *\
		BROADCAST
\* ============================================================================================= */

/// PURPOSE:
///    Broadcast the request for the initial heist planning board setup.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA(INT iHeistTotalPay, INT iHeistTotalRules, INT iHeistTotalTeams, INT& iPlayerOrder[])//, INT& iSelectedOutfit[])

	// Store the Broadcast data
	m_sEventHeistSetupInitData theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_SETUP_INIT_DATA
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	GET_THIS_SESSION_UNIQUE_EVENT_IDS(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
	theEventData.iHeistTotalPay				= iHeistTotalPay
	theEventData.iHeistTotalRules			= iHeistTotalRules
	theEventData.iHeistTotalTeams			= iHeistTotalTeams
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		theEventData.iPlayerOrder[index] = iPlayerOrder[index]
//		theEventData.iSelectedOutfit[index] = iSelectedOutfit[index]
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update the player heist roles on the server.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLES(HEIST_ROLES &ePlayerRoles[])

	// Store the Broadcast data
	m_sEventHeistUpdateRoles theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_ROLES
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		theEventData.ePlayerRoles[index]	= ePlayerRoles[index]
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLES - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update the player heist roles on the server.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUTS(INT &iHeistCutPercent[])

	// Store the Broadcast data
	m_sEventHeistUpdateCuts theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_CUTS
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	GET_THIS_SESSION_UNIQUE_EVENT_IDS(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
	
	INT index
	FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
		theEventData.iHeistCutPercent[index] = iHeistCutPercent[index]
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUTS - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update the player heist outfits on the server.
/// RETURNS:
///    BOOL - TRUE if successful.
//FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS(INT &iHeistOutfits[])
//
//	// Store the Broadcast data
//	m_sEventHeistUpdateOutfits theEventData
//	
//	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_OUTFITS
//	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
//	
//	INT index
//	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//		theEventData.iHeistOutfits[index] = iHeistOutfits[index]
//	ENDFOR
//	
//	// Broadcast the event to the mission control server only
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS - Sent By: ", GET_THIS_SCRIPT_NAME())
//	#ENDIF
//	
//	// Everything OK
//	RETURN TRUE
//
//ENDFUNC


/// PURPOSE:
///    Broadcast the request to update the player heist roles on the server.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_STATUS(INT &iHeistPlanningStatus)

	// Store the Broadcast data
	m_sEventHeistUpdateMemberStatus theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_STATUS
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
			theEventData.iHeistPlanningStatus = iHeistPlanningStatus
		ENDIF
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_STATUS - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update a specific player heist role.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON(INT &index, HEIST_ROLES &ePlayerRole, INT &iTargetPlayer)

	// Store the Broadcast data
	m_sEventHeistUpdateRoleSingleton theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	theEventData.index						= index
	theEventData.ePlayerRole				= ePlayerRole
	theEventdata.iTargetPlayer				= iTargetPlayer
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update a specific player's cut percentage by a modifier value.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(INT index, INT &iHeistCutModifier, INT &iCutTotal)

	// Store the Broadcast data
	m_sEventHeistUpdateCutSingleton theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	GET_THIS_SESSION_UNIQUE_EVENT_IDS(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
	theEventData.index						= index
	theEventData.iHeistCutModifier			= iHeistCutModifier
	theEventData.iCutTotal					= iCutTotal // Not used.
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update a specific player's cut percentage by a modifier value (tutorial only).
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL(INT index, INT &iHeistCutModifier, INT &iCutTotal, INT iSource)

	// Store the Broadcast data
	m_sEventHeistUpdateCutSingletonTutorial theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	GET_THIS_SESSION_UNIQUE_EVENT_IDS(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
	theEventData.index						= index
	theEventData.iHeistCutModifier			= iHeistCutModifier
	theEventData.iCutTotal					= iCutTotal // Not used.
	theEventData.iSource					= iSource
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Sent By: ", GET_THIS_SCRIPT_NAME())
		PRINTLN("[AMEC][HEIST_EVENTS] ...  index:				", index)
		PRINTLN("[AMEC][HEIST_EVENTS] ...  iHeistCutModifier:	", iHeistCutModifier)
		PRINTLN("[AMEC][HEIST_EVENTS] ...  iCutTotal:			", iCutTotal)
		PRINTLN("[AMEC][HEIST_EVENTS] ...  iSource:				", iSource)
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast the request to update a specific player heist outfit.
/// RETURNS:
///    BOOL - TRUE if successful.
//FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON(INT &index, INT &iPlayerOutfit)
//
//	// Store the Broadcast data
//	m_sEventHeistUpdateOutfitSingleton theEventData
//	
//	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON
//	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
//	theEventData.index						= index
//	theEventData.iOutfit					= iPlayerOutfit
//	
//	// Broadcast the event to the mission control server only
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Sent By: ", GET_THIS_SCRIPT_NAME())
//	#ENDIF
//	
//	// Everything OK
//	RETURN TRUE
//
//ENDFUNC


/// PURPOSE:
///    Broadcast the request for the server to force sync all clients with fresh data. Not all of the data passed in is valid, and
///    that invalid data is filtered out at the process end and only the actual data is used. this is to keep 1 event for globa syncing.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA(INT iHeistTotalPay, INT iHeistTotalRules, INT iHeistTotalTeams, INT &iPlayerOrder[])

	// Store the Broadcast data
	m_sEventHeistSetupInitData theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	GET_THIS_SESSION_UNIQUE_EVENT_IDS(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
	theEventData.iHeistTotalPay				= iHeistTotalPay
	theEventData.iHeistTotalRules			= iHeistTotalRules
	theEventData.iHeistTotalTeams			= iHeistTotalTeams
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		theEventData.iPlayerOrder[index] = iPlayerOrder[index]
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Trigger a heist member removal event. This is used when a NON LEADER heist member leaves the planning board.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_REMOVE_MEMBER(INT &iPlayerIndex)

	// Store the Broadcast data
	m_sEventHeistRemoveMember theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_REMOVE_MEMBER
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	theEventData.index						= iPlayerIndex
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_REMOVE_MEMBER - MemberID: ",iPlayerIndex,", Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Request a heist leader to commence sending specific heist details.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCAST_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS(INT iLeaderParticipantIndex)

	// Store the Broadcast data
	m_sEventHeistRequestMembers theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	theEventData.iHeistLeaderIndex			= iLeaderParticipantIndex
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS - Sent By: ", GET_THIS_SCRIPT_NAME())
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS - Leader index: ", iLeaderParticipantIndex)
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Download a specific missions's participant list from a heist owner.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCAST_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS(	PLAYER_INDEX targetPlayerIndex, INT iMissionIndex,
																			INT iMemberNumber, 	TEXT_LABEL_63 tlMemberName)

	// Store the Broadcast data
	m_sEventHeistDownloadSavedMember theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	theEventData.iMissionID					= iMissionIndex
	theEventData.iMemberNumber				= iMemberNumber
	theEventData.tlMemberUID				= tlMemberName
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), SPECIFIC_PLAYER(targetPlayerIndex))
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS - Sent By: ", GET_PLAYER_NAME(PLAYER_ID()))
		PRINTLN("[AMEC][HEIST_EVENTS]  ... iMissionIndex: 	", iMissionIndex)
		PRINTLN("[AMEC][HEIST_EVENTS]  ... iMemberNumber: 	", iMemberNumber)
		PRINTLN("[AMEC][HEIST_EVENTS]  ... tlMemberName: 	", tlMemberName)
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Send local stats data to all finale board connected members.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA(HEIST_MEDAL_DATA& sPlayerMedals[])

	// Store the Broadcast data
	m_sEventHeistMedalDetails theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_MEDAL_DATA
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
		theEventData.sPlayerMedals[index].iPlayerOne 	= sPlayerMedals[index].iPlayerOne
		theEventData.sPlayerMedals[index].iPlayerTwo 	= sPlayerMedals[index].iPlayerTwo
		theEventData.sPlayerMedals[index].iPlayerThree 	= sPlayerMedals[index].iPlayerThree
		theEventData.sPlayerMedals[index].iPlayerFour 	= sPlayerMedals[index].iPlayerFour
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA - Sent By: ", GET_PLAYER_NAME(PLAYER_ID()))

		FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
			PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA - ROW[",index,"] - PlayerOne(hash): ", sPlayerMedals[index].iPlayerOne)
			PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA - ROW[",index,"] - PlayerTwo(hash): ", sPlayerMedals[index].iPlayerTwo)
			PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA - ROW[",index,"] - PlayerThree(hash): ", sPlayerMedals[index].iPlayerThree)
			PRINTLN("[AMEC][HEIST_EVENTS] - BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA - ROW[",index,"] - PlayerFour(hash): ", sPlayerMedals[index].iPlayerFour)
		ENDFOR
		
		#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Broadcast a re-sync event for the script host. This is for restoring packed data when on a heist finale.
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCST_SCRIPT_EVENT_HEIST_RESYNC_HOST(HEIST_ROLES &ePlayerRoles[], INT &iHeistCutPercent[]) //, INT &iHeistOutfits[])

	// Store the Broadcast data
	m_sEventHeistReSyncHost theEventData
	
	theEventData.Details.Type				= SCRIPT_EVENT_HEIST_RESYNC_HOST
	theEventData.Details.FromPlayerIndex	= PLAYER_ID()
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		theEventData.ePlayerRoles[index] = ePlayerRoles[index]
		theEventData.iHeistCutPercent[index] = iHeistCutPercent[index]
//		theEventData.iHeistOutfits[index] = iHeistOutfits[index]
	ENDFOR
	
	// Broadcast the event to the mission control server only
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, theEventData, SIZE_OF(theEventData), ALL_PLAYERS())
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - BROADCST_SCRIPT_EVENT_HEIST_RESYNC_HOST - Sent By: ", GET_THIS_SCRIPT_NAME())
	#ENDIF
	
	// Everything OK
	RETURN TRUE

ENDFUNC






/* ============================================================================================= *\
		PROCESS (RECEIVE)
\* ============================================================================================= */


/// PURPOSE:
///    Process the requested initial setup for the heist planning board.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA - I am NOT host, exiting event update.")
		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistSetupInitData theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA(): NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)")
		#ENDIF
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA ...  iHeistTotalPay:		",theEventData.iHeistTotalPay)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA ...  iHeistTotalCosts:	",theEventData.iHeistTotalCosts)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA ...  iHeistTotalRules:	",theEventData.iHeistTotalRules)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA ...  iHeistTotalTeams:	",theEventData.iHeistTotalTeams)
	#ENDIF

	// TODO: Get the actual values for these...
	GlobalServerBD_HeistPlanning.iHeistTotalPay 	= theEventData.iHeistTotalPay
	GlobalServerBD_HeistPlanning.iHeistSetupCosts 	= theEventData.iHeistTotalCosts
	GlobalServerBD_HeistPlanning.iHeistTotalRules	= theEventData.iHeistTotalRules
	GlobalServerBD_HeistPlanning.iHeistTotalTeams	= theEventData.iHeistTotalTeams
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		GlobalServerBD_HeistPlanning.iPlayerOrder[index] = theEventData.iPlayerOrder[index]
//		GlobalServerBD_HeistPlanning.iSelectedOutfit[index] = INVALID_HEIST_DATA
		
		IF theEventData.iPlayerOrder[index] != -1
			GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_UNASSIGNED
		ELSE
			GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_INVALID
		ENDIF
	ENDFOR
	
	// Set the leader of the heist from the player order data. The leader is always 0.
	// The leader starts with 100% of the cut for the heist.
	GlobalServerBD_HeistPlanning.LeaderIndex = INT_TO_PLAYERINDEX(theEventData.iPlayerOrder[0])

ENDPROC


/// PURPOSE:
///    Process the requested update to the heist roles (host only)
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLES(INT paramEventID)
		
	// Retrieve the data associated with the event.
	m_sEventHeistUpdateRoles theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLES(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLES - I am host, processing event update...")
		#ENDIF
		
		INT index
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLES - Global[",index,"] = eventData[",index,"]: ", ENUM_TO_INT(theEventData.ePlayerRoles[index]))
			GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index] = theEventData.ePlayerRoles[index]
		ENDFOR

	ENDIF
	
	PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLES - Updating teamSelected broadcast data for local player.")
	
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
			IF ENUM_TO_INT(theEventData.ePlayerRoles[index]) < -1
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = -1
			ELSE
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ENUM_TO_INT(theEventData.ePlayerRoles[index])
			ENDIF
			PRINTLN("PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLES GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
		ENDIF
	ENDFOR
	
ENDPROC


/// PURPOSE:
///    Process the requested initial setup for the heist planning board.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUTS(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUTS - I am NOT host, exiting event update.")
//		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUTS - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistUpdateCuts theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUTS(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUTS(): NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)")
		#ENDIF
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUTS - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
	#ENDIF
	
	INT index
	FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
		GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index] = theEventData.iHeistCutPercent[index]
	ENDFOR

ENDPROC


/// PURPOSE:
///    Process the requested full set of heist outfit data.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
//PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS(INT paramEventID)
//
//	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())		
//		EXIT // If you are not host, you have no use for this data.
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS - I am host, processing event update...")
//	#ENDIF
//		
//	// Retrieve the data associated with the event.
//	m_sEventHeistUpdateOutfits theEventData
//	
//	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
//		#IF IS_DEBUG_BUILD
//			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	// Debugging code for validation purposes.
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
//	#ENDIF
//	
//	INT index
//	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//		GlobalServerBD_HeistPlanning.iSelectedOutfit[index] = theEventData.iHeistOutfits[index]
//		// Debugging code for validation purposes.
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS - Outfit[",index,"] = event.iHeistOutfits[",index,"]: ", theEventData.iHeistOutfits[index])
//		#ENDIF
//	ENDFOR
//
//ENDPROC


/// PURPOSE:
///    Process the requested update to the heist player status.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_STATUS(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_STATUS - I am NOT host, exiting event update.")
//		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_STATUS - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistUpdateMemberStatus theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_STATUS(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_STATUS - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
	#ENDIF
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
			GlobalServerBD_HeistPlanning.iHeistPlanningState[index] = theEventData.iHeistPlanningStatus
		ENDIF
	ENDFOR

ENDPROC


/// PURPOSE:
///    Process the requested update to the heist player status.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON(INT paramEventID)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Session CLIENT processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistUpdateRoleSingleton theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - New Role: ", ENUM_TO_INT(theEventData.ePlayerRole))
	#ENDIF
	
	// Update the map once the event has been processed.
	g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapAll = TRUE
	
	BOOL bUpdatedTeam = FALSE
	
	// check player id
	
	INT iEventDataIndex = theEventData.index
	
	IF iEventDataIndex < 0
	OR iEventDataIndex >= COUNT_OF(GlobalServerBD_HeistPlanning.iPlayerOrder)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON [VALIDATE] - invalid value in theEventData.index, expected value between 0 and (MAX_HEIST_PLAYER_SLOTS-1): ", iEventDataIndex)
		SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON [VALIDATE] - invalid value in theEventData.index")
		EXIT
	ENDIF
	
	IF GlobalServerBD_HeistPlanning.iPlayerOrder[iEventDataIndex] = NATIVE_TO_INT(PLAYER_ID())
		IF ENUM_TO_INT(theEventData.ePlayerRole) < -1
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = -1
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Event targeted at local player, setting iTeamChosen to: -1 - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = -1")
			#ENDIF
			bUpdatedTeam = TRUE
		ELSE
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ENUM_TO_INT(theEventData.ePlayerRole)
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Event targeted at local player, setting iTeamChosen to: ", ENUM_TO_INT(theEventData.ePlayerRole), " - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			#ENDIF
			bUpdatedTeam = TRUE
		ENDIF
	ENDIF
	
	IF NOT bUpdatedTeam
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - bUpdatedTeam = FALSE, falling back to direct breadcast data set for iTargetPlayer: ", theEventData.iTargetPlayer)
		#ENDIF
		
		IF theEventData.iTargetPlayer = NATIVE_TO_INT(PLAYER_ID())
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Event re-targeted at local player, setting iTeamChosen to: ", ENUM_TO_INT(theEventData.ePlayerRole), " - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			GlobalplayerBD_FM[theEventData.iTargetPlayer].sClientCoronaData.iTeamChosen = ENUM_TO_INT(theEventData.ePlayerRole)
		ENDIF
	
	ENDIF
	
	IF (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		// Set the value of the singleton using the index pointer.
		GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[iEventDataIndex] = theEventData.ePlayerRole
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON - Event targeted at local player, setting iTeamChosen to: ", ENUM_TO_INT(theEventData.ePlayerRole))
		#ENDIF
	ENDIF	

ENDPROC


/// PURPOSE:
///    Process the requested update to the heist player status.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - I am NOT host, exiting event update.")
//		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistUpdateCutSingleton theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(): IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)")
		#ENDIF
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - ModifierAmount: 	", theEventData.iHeistCutModifier)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Target: 			", theEventData.index)
	#ENDIF
	
	INT iMinimumCut
	
	IF GlobalServerBD_HeistPlanning.iPlayerOrder[theEventData.index] = g_HeistSharedClient.iCurrentPropertyOwnerIndex
		iMinimumCut = g_sMPTUNABLES.iLeader_Min_Heist_Finale_Take_percentage
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Minimum cut percent for leader: ", iMinimumCut)
	ELSE
		iMinimumCut = g_sMPTUNABLES.iMember_Min_Heist_Finale_Take_percentage
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Minimum cut percent for member: ", iMinimumCut)
	ENDIF
	
	INT iModifier = theEventData.iHeistCutModifier
	
	IF iModifier < 0 	
		// DECREMENT TARGET, INCREASE POT.
		INT iTargetCut = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index]
		IF iTargetCut < ABSI(iModifier)
			iModifier = iTargetCut
		ENDIF
		
		IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] < iMinimumCut
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Cannot decrease target cut! Target already less cut than allowed minimum! Target cut: ", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index], ", Minimum: ", iMinimumCut)
			#ENDIF
			
			// Because the target has somehow ended up with less than the minimum, calculate how much under they are and add it back onto the target.
			INT iRem
			iRem = iMinimumCut - GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index]
			GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Added missing remainder: ", iRem, ", to target total: ", (GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = iMinimumCut
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Cannot decrease target cut, target already at minimum: ", iMinimumCut)
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Decrease target cut. iModifier: ", iModifier)
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - DECREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] <= (100 - ABSI(iModifier))
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] + ABSI(iModifier)
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] - ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - DECREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
		ENDIF
		
	ELSE 	
		// INCREMENT TARGET, DECREASE POT.
		INT iPotCut = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS]
		IF iPotCut < ABSI(iModifier)
			iModifier = iPotCut
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Increase target cut. iModifier: ", iModifier)
		#ENDIF
	
		IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] > g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Cannot increase target cut! Already greater cut than allowed maximum! Target cut: ", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index], ", Maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
			// Because the target has somehow ended up with more than the maximum, calculate how much over they are and add it back onto the pot.
			INT iRem
			iRem = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] - g_sMPTUNABLES.imax_heist_cut_amount
			GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Added remainder: ", iRem, ", to pot total: ", (GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - Cannot increase target cut, already at maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
		ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - INCREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] >= ABSI(iModifier)
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] - ABSI(iModifier)
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] + ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON - INCREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
		ENDIF
		
	ENDIF
		

ENDPROC


/// PURPOSE:
///    Process the requested update to the heist player status (tutorial only).
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - I am NOT host, exiting event update.")
//		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistUpdateCutSingletonTutorial theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL(): IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)")
		#ENDIF
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
		PRINTLN("[AMEC][HEIST_EVENTS] ...  iHeistCutModifier: 	", theEventData.iHeistCutModifier)
		PRINTLN("[AMEC][HEIST_EVENTS] ...  Target: 				", theEventData.index)
		PRINTLN("[AMEC][HEIST_EVENTS] ...  iCutTotal: 			", theEventData.iCutTotal)
		PRINTLN("[AMEC][HEIST_EVENTS] ...  iSource: 			", theEventData.iSource)
	#ENDIF
	
	INT iModifier = theEventData.iHeistCutModifier
	
	IF iModifier < 0 	
		// DECREMENT TARGET, INCREASE SOURCE.
		INT iTargetCut = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index]
		IF iTargetCut < ABSI(iModifier)
			iModifier = iTargetCut
		ENDIF
		
		IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] > g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Cannot increase target cut! Source already greater cut than allowed maximum! Target cut: ", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource], ", Maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
			// Because the source has somehow ended up with more than the maximum, calculate how much over they are and add it back onto the target.
			INT iRem
			iRem = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] - g_sMPTUNABLES.imax_heist_cut_amount
			GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Added remainder: ", iRem, ", to target total: ", (GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] = g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Cannot increase target cut, source already at maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Decrease target cut. iModifier: ", iModifier)
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - DECREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] <= (100 - ABSI(iModifier))
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] + ABSI(iModifier)
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] - ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - DECREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
		ENDIF
		
	ELSE 	
		// INCREMENT TARGET, DECREASE SOURCE.
		INT iSourceCut = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource]
		IF iSourceCut < ABSI(iModifier)
			iModifier = iSourceCut
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Increase target cut. iModifier: ", iModifier)
		#ENDIF
	
		IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] > g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Cannot increase target cut! Already greater cut than allowed maximum! Target cut: ", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index], ", Maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
			// Because the target has somehow ended up with more than the maximum, calculate how much over they are and add it back onto the pot.
			INT iRem
			iRem = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] - g_sMPTUNABLES.imax_heist_cut_amount
			GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Added remainder: ", iRem, ", to source total: ", (GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - Cannot increase target cut, already at maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
		ELSE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - INCREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] >= ABSI(iModifier)
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource] - ABSI(iModifier)
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index] + ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL - INCREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[theEventData.index])
			#ENDIF
			
		ENDIF
		
	ENDIF
		

ENDPROC


/// PURPOSE:
///    Process the requested update to the heist player outfit.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
//PROC PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON(INT paramEventID)
//
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Session CLIENT processing event update...")
//	#ENDIF
//		
//	// Retrieve the data associated with the event.
//	m_sEventHeistUpdateOutfitSingleton theEventData
//	
//	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
//		#IF IS_DEBUG_BUILD
//			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	// Debugging code for validation purposes.
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
//	#ENDIF
//	
//	STRING sPlayerName = GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[theEventData.index]))
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Looking for [",theEventData.index,"] in iPlayerOrder array.")
//	#ENDIF
//	
//	// Check player id
//	IF GlobalServerBD_HeistPlanning.iPlayerOrder[theEventData.index] = NATIVE_TO_INT(PLAYER_ID())
//		IF theEventData.iOutfit <= INVALID_HEIST_DATA
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Updating outfit for ",sPlayerName," from ",theEventData.iOutfit," to ",INVALID_HEIST_DATA)
//			g_FMMC_STRUCT.iHeistOutfit = INVALID_HEIST_DATA
//		ELSE
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Updating outfit for ",sPlayerName," from ",g_FMMC_STRUCT.iHeistOutfit," to ",theEventData.iOutfit)
//			g_FMMC_STRUCT.iHeistOutfit = theEventData.iOutfit
//		ENDIF
//		
//		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON - Broadcasting selected outfit for PED clone. Outfit: ", theEventData.iOutfit)
//		BROADCAST_CORONA_PLAYER_OUTFIT(theEventData.iOutfit, TRUE, TRUE)
//	ENDIF
//	
//	IF (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		// Set the value of the singleton using the index pointer.
//		GlobalServerBD_HeistPlanning.iSelectedOutfit[theEventData.index] = theEventData.iOutfit
//	ENDIF	
//
//ENDPROC


/// PURPOSE:
///    Process the requested data sync.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - I am NOT host, exiting event update.")
//		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistSetupInitData theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA(): IS_THIS_EVENT_SESSION_UNIQUE_ID_OK(theEventData.iUniqueSessionHash0, theEventData.iUniqueSessionHash1)")
		#ENDIF
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
	#ENDIF

	// TODO: Get the actual values for these...
	
	IF theEventData.iHeistTotalPay != INVALID_HEIST_DATA
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - ... PAY VARIATION DETECTED! Event-TotalPay: ", theEventData.iHeistTotalPay, " Global-TotalPay: ", GlobalServerBD_HeistPlanning.iHeistTotalPay)
		#ENDIF
		GlobalServerBD_HeistPlanning.iHeistTotalPay 	= theEventData.iHeistTotalPay
	ENDIF
	
	IF theEventData.iHeistTotalRules != INVALID_HEIST_DATA
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - ... RULE VARIATION DETECTED! Event-TotalRules: ", theEventData.iHeistTotalRules, " Global-TotalRules: ", GlobalServerBD_HeistPlanning.iHeistTotalRules)
		#ENDIF
		GlobalServerBD_HeistPlanning.iHeistTotalRules	= theEventData.iHeistTotalRules
	ENDIF
	
	IF theEventData.iHeistTotalTeams != INVALID_HEIST_DATA
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - ... TEAM VARIATION DETECTED! Event-TotalTeams: ", theEventData.iHeistTotalTeams, " Global-TotalTeams: ", GlobalServerBD_HeistPlanning.iHeistTotalTeams)
		#ENDIF
		GlobalServerBD_HeistPlanning.iHeistTotalTeams	= theEventData.iHeistTotalTeams
	ENDIF
	
	// Hardcoded to always check 0 as 0 is always the leader. This will not change. Also, there will never be
	// a valid update where playerOrder 0 = -1 as that would mean there are no members in the heist (not even a leader).
	IF theEventData.iPlayerOrder[0] != INVALID_HEIST_DATA
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA - ... PLAYER ORDER VARIATION DETECTED!")
		#ENDIF
	
		INT index
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			GlobalServerBD_HeistPlanning.iPlayerOrder[index] = theEventData.iPlayerOrder[index]
		ENDFOR
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Process the requested data sync.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_REMOVE_MEMBER(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_REMOVE_MEMBER - I am NOT host, exiting event update.")
//		#ENDIF
		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_REMOVE_MEMBER - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistRemoveMember theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_REMOVE_MEMBER(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_REMOVE_MEMBER - Sent By: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
	#ENDIF

	INT iPlayerIndex = theEventData.index
	INT index
	
	FOR index = 1 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF index >= iPlayerIndex
			IF index = (MAX_HEIST_PLAYER_SLOTS-1)
				// The end of the array needs to be set to nothing, consdering that everything before has been shifted left.
				GlobalServerBD_HeistPlanning.iPlayerOrder[index] = -1
				GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_INVALID
				GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index] = 0
			ELSE
				// Shift all array entries (from target onwards) to the left by one.
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_SCRIPT_EVENT_HEIST_REMOVE_MEMBER - iPlayerOder[",index,"]: ", GlobalServerBD_HeistPlanning.iPlayerOrder[index], " = iPlayerOrder[",index,"+1]: ", GlobalServerBD_HeistPlanning.iPlayerOrder[index+1])
				GlobalServerBD_HeistPlanning.iPlayerOrder[index] = GlobalServerBD_HeistPlanning.iPlayerOrder[index+1]
				GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index] = GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index+1]
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC


PROC PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS(INT paramEventID)

	// Retrieve the data associated with the event.
	m_sEventHeistRequestMembers theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS - Checking to see if event was directed at me. Details:")
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS ...	My PartID: ", PARTICIPANT_ID_TO_INT(), ". TargetID: ",theEventData.iHeistLeaderIndex)
	#ENDIF
	
	IF (PARTICIPANT_ID_TO_INT() = theEventData.iHeistLeaderIndex)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS - Local participant index matches event data.")
		#ENDIF
		
		// If we get to this point, I must be running a heast at the moment. Someone has requested a download, so firstly
		// we need to grab them so that we know where to send the heist saved member data.
		
		SET_HEIST_START_SENDING_MEMBER_DATA(TRUE)
		SET_BIT(g_HeistSharedClient.iMemberDataUploadTargets, NATIVE_TO_INT(theEventData.Details.FromPlayerIndex))
		g_HeistSharedClient.iMemberDataUploadProgress = 0
		g_HeistSharedClient.iMemberDataUploadSubProgress = 0
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS - Set target bit at: ", NATIVE_TO_INT(theEventData.Details.FromPlayerIndex))
		#ENDIF

	ENDIF

ENDPROC


PROC PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS(INT paramEventID)

	// Retrieve the data associated with the event.
	m_sEventHeistDownloadSavedMember theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS - FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS - Received mission data pack from heist leader. Details:")
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS ... Leader Index: 	", NATIVE_TO_INT(theEventData.Details.FromPlayerIndex))
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS ... MissionID: 		", theEventData.iMissionID)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS ... iMemberNumber: 	", theEventData.iMemberNumber)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS ... tlMemberUID: 	", theEventData.tlMemberUID)
	#ENDIF
	
	IF theEventData.iMissionID < 0
	OR theEventData.iMissionID >= COUNT_OF(g_HeistSharedClient.tlMissionPlayerUID)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS [VALIDATE] - invalid value in theEventData.iMissionID, expected value between 0 and (MAX_HEIST_PLANNING_ROWS-1), actual value: ", theEventData.iMissionID)
		SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS [VALIDATE] - invalid value in theEventData.iMissionID")
		EXIT
	ENDIF
	
	IF theEventData.iMemberNumber < 0
	OR theEventData.iMemberNumber >= COUNT_OF(g_HeistSharedClient.tlMissionPlayerUID[])
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS [VALIDATE] - invalid value in theEventData.iMemberNumber, expected value between 0 and (MAX_HEIST_PLAYERS_PER_SETUP-1), actual value: ", theEventData.iMemberNumber)
		SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS [VALIDATE] - invalid value in theEventData.iMemberNumber")
		EXIT
	ENDIF
	
	// This event is a specific event, meaning that it's only ever received from a player who directed it here.
	// Now, save the data locally. We're not going to do anything with it here, and instead, wait for the heist pre-planning
	// script to request the data when it requires.
	
	IF NOT IS_BIT_SET(g_HeistSharedClient.iMemberDataBitset, theEventData.iMissionID)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS - Recieved member data for missionID: ",theEventData.iMissionID)
		#ENDIF
		SET_BIT(g_HeistSharedClient.iMemberDataBitset, theEventData.iMissionID)
	ENDIF
	
	INT iMemberPointer = GET_BIT_RANGE_FOR_MISSION_MEMBERS(theEventData.iMissionID) + theEventData.iMemberNumber
	
	IF NOT IS_BIT_SET(g_HeistSharedClient.iMemberSubDataBitset, iMemberPointer)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_PRE_PLAN_DOWNLOAD_MISSION_MEMBERS - Setting member: ",iMemberPointer,"'s data as AVAILABLE.")
		#ENDIF
		SET_BIT(g_HeistSharedClient.iMemberSubDataBitset, iMemberPointer)
	ENDIF
	
	g_HeistSharedClient.tlMissionPlayerUID[theEventData.iMissionID][theEventData.iMemberNumber] = theEventData.tlMemberUID

ENDPROC


/// PURPOSE:
///    process leader's stats data for pre-planning medal data.
PROC PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA(INT paramEventID)

	// Retrieve the data associated with the event.
	m_sEventHeistMedalDetails theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - My name hash: ", GET_HASH_KEY(GET_PLAYER_NAME(PLAYER_ID())))
	
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - Heist setup [",index,"] player [0] has medal value: ", theEventData.sPlayerMedals[index].iPlayerOne)
		g_HeistPlanningClient.sMedalData[index].iPlayerOne = theEventData.sPlayerMedals[index].iPlayerOne
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - Heist setup [",index,"] player [1] has medal value: ", theEventData.sPlayerMedals[index].iPlayerTwo)
		g_HeistPlanningClient.sMedalData[index].iPlayerTwo = theEventData.sPlayerMedals[index].iPlayerTwo
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - Heist setup [",index,"] player [2] has medal value: ", theEventData.sPlayerMedals[index].iPlayerThree)
		g_HeistPlanningClient.sMedalData[index].iPlayerThree = theEventData.sPlayerMedals[index].iPlayerThree
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - Heist setup [",index,"] player [3] has medal value: ", theEventData.sPlayerMedals[index].iPlayerFour)
		g_HeistPlanningClient.sMedalData[index].iPlayerFour = theEventData.sPlayerMedals[index].iPlayerFour
	ENDFOR
	
	// Refresh our cards
	g_HeistPlanningClient.bPlayerCardsInSync = FALSE
	PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_MEDAL_DATA - Set g_HeistPlanningClient.bPlayerCardsInSync = FALSE")
ENDPROC


/// PURPOSE:
///    Process the re-sync data. This is to allow heist members who aren't the leader to see the leader's data in the leader's apartment during a heist finale mission.
/// PARAMS:
///    paramEventID - INT - EventID from the NET_PROCESS_EVENTS header.
PROC PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())		
		EXIT // If you are not host, you have no use for this data.
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST - I am host, processing event update...")
	#ENDIF
		
	// Retrieve the data associated with the event.
	m_sEventHeistReSyncHost theEventData
	
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST(): FAILED TO RETRIEVE EVENT DATA! Tell Alastair.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST - Requested by: ", GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))
	#ENDIF
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index] = theEventData.ePlayerRoles[index]
		GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index] = theEventData.iHeistCutPercent[index]
//		GlobalServerBD_HeistPlanning.iSelectedOutfit[index] = theEventData.iHeistOutfits[index]
		// Debugging code for validation purposes.
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST - Restoring cached heist data for player: ", index)
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST ... Role ID:		", ENUM_TO_INT(theEventData.ePlayerRoles[index]))
			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST ... Cut Percent:	", theEventData.iHeistCutPercent[index])
//			PRINTLN("[AMEC][HEIST_EVENTS] - PROCESS_SCRIPT_EVENT_HEIST_RESYNC_HOST ... Outfit ID:	", theEventData.iHeistOutfits[index])
		#ENDIF
	ENDFOR

ENDPROC






// End of file.


