//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	net_cinematic_cam_behaviour.sch								//
//		AUTHOR			:	Tom Turner													//
//		DESCRIPTION		:	Camera behaviours used in net_cinematic_cam_sequencer.sch   //
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"


// Flags for defining cameras enabled behaviours
ENUM CINEMATIC_CAM_BEHAVIOUR_TYPE
	CCB_LERP
	,CCB_SHAKE
	,CCB_ORBIT_COORD
	,CCB_ORBIT_ENTITY
	,CCB_LOOK_AT_ENTITY
	,CCB_LOOK_AT_COORD
	,CCB_LERP_FOV
ENDENUM

DEBUGONLY FUNC STRING DEBUG_GET_CINEMATIC_CAM_BEHAVIOUR_TYPE_AS_STRING(CINEMATIC_CAM_BEHAVIOUR_TYPE eEnum)
	SWITCH eEnum
		CASE CCB_LERP			RETURN	"CCB_LERP"
		CASE CCB_SHAKE			RETURN	"CCB_SHAKE"
		CASE CCB_ORBIT_COORD	RETURN	"CCB_ORBIT_COORD"
		CASE CCB_ORBIT_ENTITY	RETURN	"CCB_ORBIT_ENTITY"
		CASE CCB_LOOK_AT_ENTITY	RETURN	"CCB_LOOK_AT_ENTITY"
		CASE CCB_LOOK_AT_COORD	RETURN	"CCB_LOOK_AT_COORD"
		CASE CCB_LERP_FOV		RETURN	"CCB_LERP_FOV"
	ENDSWITCH

	ASSERTLN("DEBUG_GET_CINEMATIC_CAM_BEHAVIOUR_TYPE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE: 
///    Contains data needed for updating cinematic ciCamera sBehaviour.
///    Should only be manipulated through sBehaviour setters
///    and preset sBehaviour functions
STRUCT CINEMATIC_CAM_BEHAVIOUR_STRUCT

	// Flags defining enabled behaviours
	INT iCamBehaviourBS
	
	// Basic
	VECTOR vStartPos
	VECTOR vStartRot
	FLOAT fStartFOV = 65.0
	FLOAT fWeighting = 100.0
	
	// Lerp
	VECTOR vEndPos
	VECTOR vEndRot
	INT iLerpDuration
	INT iLerpDelay = -1
	TIME_DATATYPE iLerpDelayStartTime
	CAMERA_GRAPH_TYPE eLerpGraph
	
	// FOV lerp
	INT iFOVLerpDurationMS
	FLOAT fEndFOV = 65.0
	FLOAT fFOVLerpProgress
	
	// Duration
	INT iCameraDuration = -1
	TIME_DATATYPE iCamStartTime
	
	// Look At
	ENTITY_INDEX eiLookAtTarget
	VECTOR vLookAtOffset
	VECTOR vLookAtCoord
	
	// Shake
	TEXT_LABEL_63 strCamShakeType
	FLOAT fCamShakeIntensity
	
	// Orbit
	VECTOR vOrbitCentre
	VECTOR vOrbitOffset
	ENTITY_INDEX eiOrbitTarget
	FLOAT fOrbitSpeed
	FLOAT fDistanceFromCentre
	FLOAT fStartTheta
	FLOAT fTheta
	INT iOrbitDirection
	
ENDSTRUCT

DEBUGONLY PROC CINEMATIC_CAM_BEHAVIOUR_DEBUG__PRINT(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, STRING strPrefix)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT --------------------------")	
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iCamBehaviourBS: ", sBehaviour.iCamBehaviourBS)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vStartPos: ", sBehaviour.vStartPos)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vStartRot: ", sBehaviour.vStartRot)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fStartFOV: ", sBehaviour.fStartFOV)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vEndPos: ", sBehaviour.vEndPos)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vEndRot: ", sBehaviour.vEndRot)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iLerpDuration: ", sBehaviour.iLerpDuration)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iLerpDelay: ", sBehaviour.iLerpDelay)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iLerpDelayStartTime: ", NATIVE_TO_INT(sBehaviour.iLerpDelayStartTime))
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - eLerpGraph: ", sBehaviour.eLerpGraph)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iFOVLerpDurationMS: ", sBehaviour.iFOVLerpDurationMS)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fEndFOV: ", sBehaviour.fEndFOV)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fFOVLerpProgress: ", sBehaviour.fFOVLerpProgress)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iCameraDuration: ", sBehaviour.iCameraDuration)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iCamStartTime: ", NATIVE_TO_INT(sBehaviour.iCamStartTime))
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - eiLookAtTarget: ", NATIVE_TO_INT(sBehaviour.eiLookAtTarget))
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vLookAtOffset: ", sBehaviour.vLookAtOffset)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vLookAtCoord: ", sBehaviour.vLookAtCoord)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - strCamShakeType: ", sBehaviour.strCamShakeType)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fCamShakeIntensity: ", sBehaviour.fCamShakeIntensity)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vOrbitCentre: ", sBehaviour.vOrbitCentre)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - vOrbitOffset: ", sBehaviour.vOrbitOffset)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - eiOrbitTarget: ", NATIVE_TO_INT(sBehaviour.eiOrbitTarget))
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fOrbitSpeed: ", sBehaviour.fOrbitSpeed)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fDistanceFromCentre: ", sBehaviour.fDistanceFromCentre)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fStartTheta: ", sBehaviour.fStartTheta)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - fTheta: ", sBehaviour.fStartTheta)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT - iOrbitDirection: ", sBehaviour.iOrbitDirection)
	PRINTLN(strPrefix, "[CINEMATIC_CAM_BEHAVIOUR_DEBUG] PRINT --------------------------")
ENDPROC

PROC CINEMATIC_CAM_BEHAVIOUR__CLEAR(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	CINEMATIC_CAM_BEHAVIOUR_STRUCT sEmpty
	COPY_SCRIPT_STRUCT(sBehaviour, sEmpty, SIZE_OF(sEmpty))
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡    BEHAVIOUR INIT    ╞════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Ensures the given field of view is not 0 or less.
///    Will default FOV to 65 otherwise.
/// PARAMS:
///    FOV - field of view
PROC VALIDATE_CAM_FOV(FLOAT& FOV)
	
	IF FOV <= 0
		CWARNINGLN(DEBUG_PROPERTY, "[CINEMATIC_CAM_BEHAVIOUR] VALIDATE_CAM_FOV - FOV <= 0 setting to default 65")
		FOV = 65
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets up the ciCamera wait sBehaviour if enabled
/// PARAMS:
///    sBehaviour - sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_DURATION_BEHAVIOUR(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	IF sBehaviour.iCameraDuration > 0
		sBehaviour.iCamStartTime = GET_NETWORK_TIME()
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets up look at sBehaviour if enabled. 
///    Will tell the ciCamera to look at a target or
///    if not enabled stop looking at previour target
/// PARAMS:
///    ciCamera - ciCamera to use
///    sBehaviour - sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_LOOKAT_BEHAVIOUR(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	// If enabled
	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_LOOK_AT_ENTITY)
		POINT_CAM_AT_ENTITY(ciCamera, sBehaviour.eiLookAtTarget, sBehaviour.vLookAtOffset)
	ELIF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_LOOK_AT_COORD)
		POINT_CAM_AT_COORD(ciCamera, sBehaviour.vLookAtCoord)
	ELSE
		//-- Stop looking at previous behaviours target
		STOP_CAM_POINTING(ciCamera)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets up the ciCamera shake sBehaviour of enabled
/// PARAMS:
///    ciCamera - ciCamera to use
///    sBehaviour - sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_SHAKE_BEHAVIOUR(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_SHAKE)
		SHAKE_CAM(ciCamera, sBehaviour.strCamShakeType, sBehaviour.fCamShakeIntensity)
	ELSE
		STOP_CAM_SHAKING(ciCamera, TRUE)	
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets up lerp sBehaviour if enabled
/// PARAMS:
///    ciCamera - ciCamera to use
///    sBehaviour - sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_LERP_BEHAVIOUR(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP)
		
		//-- Make ciCamera lerp with given params
		SET_CAM_PARAMS(ciCamera, sBehaviour.vEndPos, sBehaviour.vEndRot,sBehaviour.fStartFOV, sBehaviour.iLerpDuration, sBehaviour.eLerpGraph)	

	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets up orbit coord sBehaviour if enabled
/// PARAMS:
///    ciCamera - ciCamera to use
///    sBehaviour - sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_ORBIT_COORD_BEHAVIOUR(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_COORD)
		
		//-- Reset angle
		sBehaviour.fTheta = sBehaviour.fStartTheta
		
		//-- Radial to cartesian
		FLOAT fX = COS(sBehaviour.fTheta) * sBehaviour.fDistanceFromCentre
		FLOAT fY = SIN(sBehaviour.fTheta) * sBehaviour.fDistanceFromCentre
		
		//-- Ofset from orbit centre
		VECTOR orbitPos = <<fX, fY, 0.0>>		
		VECTOR newCamPos = sBehaviour.vOrbitCentre + orbitPos
		
		//-- Put cam in starting orbit pos
		SET_CAM_PARAMS(ciCamera, newCamPos, sBehaviour.vStartRot, sBehaviour.fStartFOV)
		
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets up orbit entity sBehaviour if enabled
/// PARAMS:
///    ciCamera - ciCamera to use
///    sBehaviour - sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_ORBIT_ENTITY_BEHAVIOUR(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)

	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_ENTITY)
		//-- Reset angle
		sBehaviour.fTheta = sBehaviour.fStartTheta
		
		//-- Radial to cartesian
		FLOAT fX = COS(sBehaviour.fTheta) * sBehaviour.fDistanceFromCentre
		FLOAT fY = SIN(sBehaviour.fTheta) * sBehaviour.fDistanceFromCentre
		
		//-- Ofset from orbit centre
		VECTOR orbitPos = <<fX, fY, 0.0>>		
		VECTOR newCamPos = GET_ENTITY_COORDS(sBehaviour.eiOrbitTarget) + orbitPos + sBehaviour.vOrbitOffset
		
		//-- Put cam in starting orbit pos
		SET_CAM_PARAMS(ciCamera, newCamPos, sBehaviour.vStartRot, sBehaviour.fStartFOV)		
	ENDIF
	
ENDPROC


PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_FOV_LERP_BEHAVIOUR(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP_FOV)
		sBehaviour.fFOVLerpProgress = 0.0
		SET_CAM_FOV(ciCamera, sBehaviour.fStartFOV)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Initialises any enabled behaviours
/// PARAMS:
///    ciCamera - ciCamera the sBehaviour will be added to
///    sBehaviour - the sBehaviour to init
PROC _CINEMATIC_CAM_BEHAVIOUR__INIT_ENABLED_BEHAVIOURS(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
		
	_CINEMATIC_CAM_BEHAVIOUR__INIT_DURATION_BEHAVIOUR(sBehaviour)
	_CINEMATIC_CAM_BEHAVIOUR__INIT_FOV_LERP_BEHAVIOUR(ciCamera, sBehaviour)
	_CINEMATIC_CAM_BEHAVIOUR__INIT_LOOKAT_BEHAVIOUR(ciCamera, sBehaviour)
	_CINEMATIC_CAM_BEHAVIOUR__INIT_SHAKE_BEHAVIOUR(ciCamera, sBehaviour)
	_CINEMATIC_CAM_BEHAVIOUR__INIT_ORBIT_COORD_BEHAVIOUR(ciCamera, sBehaviour)
	_CINEMATIC_CAM_BEHAVIOUR__INIT_ORBIT_ENTITY_BEHAVIOUR(ciCamera, sBehaviour)
	_CINEMATIC_CAM_BEHAVIOUR__INIT_LERP_BEHAVIOUR(ciCamera, sBehaviour)
	
ENDPROC


/// PURPOSE:
///    Starts the given sBehaviour on the given ciCamera
/// PARAMS:
///    ciCamera - ciCamera to start the sBehaviour on
///    sBehaviour - sBehaviour to start
PROC CINEMATIC_CAM_BEHAVIOUR__START(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	// Validate FOV and enable ciCamera
	VALIDATE_CAM_FOV(sBehaviour.fStartFOV)
	
	// Recreate the camera to clear any behaviours its currently performing
	DESTROY_CAM(ciCamera)
	ciCamera = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, sBehaviour.vStartPos, sBehaviour.vStartRot, sBehaviour.fStartFOV)
	SET_CAM_ACTIVE(ciCamera, TRUE)
	
	// Init any enabled behaviours
	_CINEMATIC_CAM_BEHAVIOUR__INIT_ENABLED_BEHAVIOURS(ciCamera, sBehaviour)
	
ENDPROC


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡    BEHAVIOUR ADDERS    ╞══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Adds a look at sBehaviour for tracking an entity
/// PARAMS:
///    sBehaviour - cam to add sBehaviour to
///    eiLookAtEntity - the entity to track
PROC CB_ADD_LOOK_AT_ENTITY(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, ENTITY_INDEX& eiLookAtEntity, VECTOR vLookAtOffset)
	
	// Only enable if entity isn't null
	IF eiLookAtEntity != NULL
	
		//-- Enable look at
		SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LOOK_AT_ENTITY)
		
		//-- Can only have one look at type
		CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LOOK_AT_COORD)
		sBehaviour.eiLookAtTarget = eiLookAtEntity
		sBehaviour.vLookAtOffset = vLookAtOffset
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Adds a look at sBehaviour for tracking an coordinate
/// PARAMS:
///    sBehaviour - cam to add sBehaviour to
///    vLookAtCoord - the coordinate to track
PROC CB_ADD_LOOK_AT_COORD(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, VECTOR vLookAtCoord)
	
	// Enable look at coord
	SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LOOK_AT_COORD)
	
	// Can only have one look at type
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LOOK_AT_ENTITY)
	sBehaviour.vLookAtCoord = vLookAtCoord
	
ENDPROC

PROC CB_ADD_FOV_LERP(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, FLOAT fStartFOV, FLOAT fEndFOV, INT iLerpDurationMS)
	SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP_FOV)
	sBehaviour.fStartFOV = fStartFOV
	sBehaviour.fEndFOV = fEndFov
	sBehaviour.iFOVLerpDurationMS = iLerpDurationMS
ENDPROC

/// PURPOSE:
///    Ensures direction is normalised
/// PARAMS:
///    iDirection - the unvalidated left or right direction
/// RETURNS:
///    1 for right, -1 for left
FUNC INT _CINEMATIC_CAM_BEHAVIOUR__GET_VALID_ORBIT_DIRECTION(INT iDirection)
	
	RETURN PICK_INT(iDirection >= 0, 1, -1)
	
ENDFUNC


/// PURPOSE:
///    Adds an orbiting behavior around a coordinate
/// PARAMS:
///    sBehaviour - cam to add sBehaviour to
///    vOrigin - coordinate to orbit
///    fDistance - distance to orbit from origin
///    fSpeed - speed at which to orbit
///    iCameraDuration - how long the sBehaviour will last
PROC CB_ADD_ORBIT_COORD(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, VECTOR vOrigin, FLOAT fDistance, FLOAT fSpeed, INT iCameraDuration, INT iDirection, FLOAT fStartAngle = 0.0)
	
	// Enable orbit
	SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_COORD)
	
	// Cannot lerp or orbit entity while orbiting coord
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP)
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_ENTITY)
	
	// Set orbit relevant values
	sBehaviour.vOrbitCentre = vOrigin
	sBehaviour.fDistanceFromCentre = fDistance
	sBehaviour.fOrbitSpeed = fSpeed
	sBehaviour.iCameraDuration = iCameraDuration
	sBehaviour.iOrbitDirection = _CINEMATIC_CAM_BEHAVIOUR__GET_VALID_ORBIT_DIRECTION(iDirection)
	sBehaviour.fStartTheta = fStartAngle
	
ENDPROC


PROC CB_ADD_ORBIT_ENTITY(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, ENTITY_INDEX eiTargetEntity, FLOAT fDistance, FLOAT fSpeed, INT iCameraDuration, INT iDirection, FLOAT fStartAngle, VECTOR vOffset)
	
	// Enable orbit
	SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_ENTITY)
	
	// Cannot lerp or orbit coord while orbiting entity
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP)
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_COORD)
	
	// Set orbit relevant values
	sBehaviour.eiOrbitTarget = eiTargetEntity
	sBehaviour.fDistanceFromCentre = fDistance
	sBehaviour.fOrbitSpeed = fSpeed
	sBehaviour.iCameraDuration = iCameraDuration
	sBehaviour.vOrbitOffset = vOffset
	sBehaviour.iOrbitDirection = _CINEMATIC_CAM_BEHAVIOUR__GET_VALID_ORBIT_DIRECTION(iDirection)
	sBehaviour.fStartTheta = fStartAngle
	
ENDPROC


/// PURPOSE:
///    Adds ciCamera shake sBehaviour
/// PARAMS:
///    sBehaviour - cam to add sBehaviour to
///    fShakeIntensity - shake intensity
///    strShakeType - what type of shake to use
PROC CB_ADD_CAMERA_SHAKE(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, FLOAT fShakeIntensity, STRING strShakeType)
	
	// Enable shake
	SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_SHAKE)
	
	// Set shake relevant values
	sBehaviour.fCamShakeIntensity = fShakeIntensity
	sBehaviour.strCamShakeType = strShakeType
	
ENDPROC

/// PURPOSE:
///    Sets the random weighting for the camera that determines how likely it is to be picked.
///    This will only work if the cinematic cam sequence this behaviour is used in is setup to use random weightings
/// PARAMS:
///    sBehaviour - cam to add sBehaviour to
///    fWeighting - weighting for it to be picked >= 0, <= 100
PROC CB_ADD_CAMERA_RANDOM_WEIGHTING(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, FLOAT fWeighting)
	sBehaviour.fWeighting = CLAMP(fWeighting, 0.0, 100.0)
ENDPROC


/// PURPOSE:
///    Adds a ciCamera lerp sBehaviour between two positions and rotations
/// PARAMS:
///    sBehaviour - sBehaviour to use
///    vPosFrom - starting position
///    vPosTo - end position
///    vRotFrom - start rotation
///    vRotTo - end rotation
///    iLerpDuration - how long the lerp lasts
///    eGraphType - easing type
PROC CB_ADD_CAMERA_LERP(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, VECTOR vPosFrom, VECTOR vRotFrom, VECTOR vPosTo,  VECTOR vRotTo,
INT iLerpDuration, CAMERA_GRAPH_TYPE eGraphType = GRAPH_TYPE_LINEAR, INT iHoldTime = -1)
	
	// Enable lerping
	SET_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP)
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_COORD)
	
	// Set lerp relevant values
	sBehaviour.vStartPos = vPosFrom
	sBehaviour.vEndPos = vPosTo
	
	sBehaviour.vStartRot = vRotFrom
	sBehaviour.vEndRot = vRotTo
	
	sBehaviour.iLerpDuration = iLerpDuration
	sBehaviour.eLerpGraph = eGraphType
	sBehaviour.iLerpDelay = iHoldTime
	
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡    BEHAVIOUR UPDATERS    ╞════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _CINEMATIC_CAM_BEHAVIOUR__HAS_SET_DURATION(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	RETURN sBehaviour.iCameraDuration > -1
ENDFUNC

/// PURPOSE:
///    Updates the ciCamera duration, checking if the duration is over
/// PARAMS:
///    sBehaviour - behviour to check the duration of
/// RETURNS:
///    FALSE if not finished or no duration set, TRUE if finished
FUNC BOOL _CINEMATIC_CAM_BEHAVIOUR__UPDATE_DURATION(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	// Exit if the ciCamera has no duration
	IF NOT _CINEMATIC_CAM_BEHAVIOUR__HAS_SET_DURATION(sBehaviour)
		//-- Return false so the current sBehaviour doesn't end due to no duration
		RETURN FALSE
	ENDIF
	
	RETURN ABSI(GET_TIME_DIFFERENCE(sBehaviour.iCamStartTime, GET_NETWORK_TIME())) > sBehaviour.iCameraDuration
	
ENDFUNC


/// PURPOSE:
///    Updates the end delay of a lerp if it has been enabled
/// PARAMS:
///    sBehaviour - sBehaviour to check the delay of
FUNC BOOL _CINEMATIC_CAM_BEHAVIOUR__UPDATE_LERP_DELAY(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)
	
	// If no end delay has been set, mark as finished
	IF sBehaviour.iLerpDelay <= 0
		RETURN TRUE
	ENDIF
	
	// If lerp end delay is over
	RETURN ABSI(GET_TIME_DIFFERENCE(sBehaviour.iLerpDelayStartTime,	GET_NETWORK_TIME())) > sBehaviour.iLerpDelay
ENDFUNC


/// PURPOSE:
///    Updates the lerp sBehaviour checking for completeion
/// PARAMS:
///    sBehaviour - sBehaviour to check the lerp of
///    ciCamera - the lerping ciCamera
/// RETURNS:
///    TRUE if not enabled or finished, FALSE if in progress
FUNC BOOL _CINEMATIC_CAM_BEHAVIOUR__UPDATE_LERP_POS(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, CAMERA_INDEX &ciCamera)
	
	// Exit if this behaviouur isn't enabled or has finished
	IF NOT IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP) 
		RETURN TRUE
	ENDIF	
	
	// Wait for interpolation to end
	IF NOT IS_CAM_INTERPOLATING(ciCamera)
		// Wait for lerp hold time if it exists
		RETURN _CINEMATIC_CAM_BEHAVIOUR__UPDATE_LERP_DELAY(sBehaviour)
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL _CINEMATIC_CAM_BEHAVIOUR__UPDATE_FOV_LERP(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, CAMERA_INDEX &ciCamera)
	
	IF NOT IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_LERP_FOV) 
		RETURN TRUE
	ENDIF	
		
	sBehaviour.fFOVLerpProgress += GET_FRAME_TIME()
	FLOAT fTime = MAP(sBehaviour.fFOVLerpProgress, 0.0, TO_FLOAT(sBehaviour.iFOVLerpDurationMS / 1000), 0.0, 1.0)
	PRINTLN("[CINEMATIC_CAM_BEHAVIOUR] UPDATE_FOV_LERP - fTime: ", fTime)
	
	IF fTime >= 1.0
		PRINTLN("[CINEMATIC_CAM_BEHAVIOUR] UPDATE_FOV_LERP - Finished")
		RETURN TRUE
	ENDIF
	
	FLOAT fCurrentFOV = LERP_FLOAT(sBehaviour.fStartFOV, sBehaviour.fEndFOV, fTime)
	SET_CAM_FOV(ciCamera, fCurrentFOV)
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Orbits the ciCamera around a coordinate
/// PARAMS:
///    sBehaviour - sBehaviour to update theta progress
///    ciCamera - ciCamera to orbit
///    vOrbitCentre - cordinate to orbit around
PROC _CINEMATIC_CAM_BEHAVIOUR__UPDATE_ORBIT_AROUND_COORD(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, CAMERA_INDEX &ciCamera, VECTOR vOrbitCentre)

	// Increment theta by speed
	sBehaviour.fTheta += GET_FRAME_TIME() * sBehaviour.fOrbitSpeed * sBehaviour.iOrbitDirection
	
	// Radial to cartesian
	FLOAT fX = COS(sBehaviour.fTheta) * sBehaviour.fDistanceFromCentre
	FLOAT fY = SIN(sBehaviour.fTheta) * sBehaviour.fDistanceFromCentre
	
	// Offset from origin
	VECTOR orbitPos = <<fX, fY, 0.0>>		
	VECTOR newCamPos = vOrbitCentre + orbitPos
	
	// Update ciCamera position
	SET_CAM_COORD(ciCamera, newCamPos)

ENDPROC


/// PURPOSE:
///    Updates the ciCamera orbit sBehaviour. 
///    Updates for orbiting coord or entity
/// PARAMS:
///    sBehaviour - sBehaviour to update theta progress
///    ciCamera - ciCamera to orbit
FUNC BOOL _CINEMATIC_CAM_BEHAVIOUR__UPDATE_CAM_ORBIT(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, CAMERA_INDEX &ciCamera)
	
	VECTOR vOrbitCentre
	
	// Get centre coord according to orbit type
	IF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_COORD)
		vOrbitCentre = sBehaviour.vOrbitCentre
	ELIF IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, CCB_ORBIT_ENTITY)
		vOrbitCentre = GET_ENTITY_COORDS(sBehaviour.eiOrbitTarget) + sBehaviour.vOrbitOffset
	ELSE
		RETURN TRUE
	ENDIF
	
	_CINEMATIC_CAM_BEHAVIOUR__UPDATE_ORBIT_AROUND_COORD(sBehaviour, ciCamera, vOrbitCentre)
	RETURN _CINEMATIC_CAM_BEHAVIOUR__UPDATE_DURATION(sBehaviour) // Use duration for camera
	
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡    PUBLIC API     ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL CINEMATIC_CAM_BEHAVIOUR__HAS_BEHAVIOUR_TYPE_ENABLED(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, CINEMATIC_CAM_BEHAVIOUR_TYPE eBehaviour)
	
	RETURN IS_BIT_SET_ENUM(sBehaviour.iCamBehaviourBS, eBehaviour)
	
ENDFUNC

PROC CINEMATIC_CAM_BEHAVIOUR__DISABLE_BEHAVIOUR_TYPE(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, CINEMATIC_CAM_BEHAVIOUR_TYPE eBehaviour)
	
	CLEAR_BIT_ENUM(sBehaviour.iCamBehaviourBS, eBehaviour)
	
ENDPROC

/// PURPOSE:
///    Sets up a cam sBehaviour with the basic parameters a cam sBehaviour should have.
///    Must at use this before adding behaviours
/// PARAMS:
///    sBehaviour - the cam sBehaviour to set up
///    vStartPos - position of the ciCamera
///    vStartRot - rotation of the ciCamera
///    fStartFOV - ciCamera fov
PROC CINEMATIC_CAM_BEHAVIOUR__INIT(CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour, VECTOR vStartPos, VECTOR vStartRot, FLOAT fStartFOV, INT iDurationMS = -1)
	
	// Clear all sBehaviour flags
	sBehaviour.iCamBehaviourBS = 0
	
	// Set required variables
	sBehaviour.vStartPos = vStartPos
	sBehaviour.vStartRot = vStartRot
	sBehaviour.fStartFOV = fStartFOV
	sBehaviour.iCameraDuration = iDurationMS
	
ENDPROC

/// PURPOSE:
///    Updates the ciCamera with the given sBehaviour.
/// PARAMS:
///    ciCamera - the ciCamera to update
///    sBehaviour - the sBehaviour to use
/// RETURNS:
///    True if the sBehaviour has finished
FUNC BOOL CINEMATIC_CAM_BEHAVIOUR__MAINTAIN(CAMERA_INDEX &ciCamera, CINEMATIC_CAM_BEHAVIOUR_STRUCT &sBehaviour)	
	
	// If duration is over, sBehaviour is finished regardless of other behaviours progress
	IF _CINEMATIC_CAM_BEHAVIOUR__UPDATE_DURATION(sBehaviour)
		RETURN TRUE
	ENDIF
	
	// If all behaviours finished
	IF _CINEMATIC_CAM_BEHAVIOUR__UPDATE_CAM_ORBIT(sBehaviour, ciCamera)	
	AND _CINEMATIC_CAM_BEHAVIOUR__UPDATE_LERP_POS(sBehaviour, ciCamera)
	AND _CINEMATIC_CAM_BEHAVIOUR__UPDATE_FOV_LERP(sBehaviour, ciCamera)	
		IF _CINEMATIC_CAM_BEHAVIOUR__HAS_SET_DURATION(sBehaviour)
			RETURN _CINEMATIC_CAM_BEHAVIOUR__UPDATE_DURATION(sBehaviour)
		ENDIF
		
		RETURN TRUE 
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

