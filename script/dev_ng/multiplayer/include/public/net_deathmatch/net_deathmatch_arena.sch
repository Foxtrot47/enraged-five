USING "net_deathmatch_include.sch"

PROC CLEAR_DESTROYED_ARENA_VEHICLES(SHARED_DM_VARIABLES &dmVarsPassed)

	IF NOT bLocalPlayerOK
		EXIT //Only delete your old vehicle after you've respawned in the spectate box
	ENDIF
	
	IF ( NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) AND NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_TRIANGLE_WARP) )
	OR ( DOES_ENTITY_EXIST(dmVarsPassed.viArenaVehToCleanup) AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed))
		
		IF IS_ENTITY_ALIVE(dmVarsPassed.viArenaVehToCleanup)
			IF VDIST2(vLocalPlayerPosition, GET_ENTITY_COORDS(dmVarsPassed.viArenaVehToCleanup)) < 50
				PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES - Too close to clean up! ")
				EXIT
			ENDIF
		ENDIF
	
		VEHICLE_INDEX vehDelete
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			vehDelete = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)	
			PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES, GET_VEHICLE_PED_IS_IN ")
		ELSE
			IF DOES_ENTITY_EXIST(dmVarsPassed.viArenaVehToCleanup)
				vehDelete = dmVarsPassed.viArenaVehToCleanup	
				PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES, dmVarsPassed.viArenaVehToCleanup ")
			ELSE
				vehDelete = GET_PLAYERS_LAST_VEHICLE()	
				PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES, GET_PLAYERS_LAST_VEHICLE ")
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(vehDelete)
			PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES, NETWORK_ENTITY_GET_OBJECT_ID = ", NETWORK_ENTITY_GET_OBJECT_ID(vehDelete))
		ENDIF
		#ENDIF
		
		
		IF DOES_ENTITY_EXIST(vehDelete)
			IF IS_VEHICLE_EMPTY(vehDelete, TRUE, DEFAULT, DEFAULT, TRUE, FALSE)
				PED_INDEX piPotentialDriver = GET_PED_IN_VEHICLE_SEAT(vehDelete, VS_DRIVER)
				
				IF DOES_ENTITY_EXIST(piPotentialDriver)
					PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES - Exiting due to piPotentialDriver")
					
					IF TAKE_CONTROL_OF_ENTITY(piPotentialDriver)
						SET_ENTITY_AS_MISSION_ENTITY(piPotentialDriver, FALSE, TRUE)
						DELETE_PED(piPotentialDriver)
					ENDIF
					
					EXIT
				ENDIF
				
				IF TAKE_CONTROL_OF_ENTITY(vehDelete)
					PRINTLN("CLEAR_DESTROYED_ARENA_VEHICLES, DELETE_STORED_VEHICLE, DELETE_VEHICLE ", NETWORK_ENTITY_GET_OBJECT_ID(vehDelete))
					SET_ENTITY_AS_MISSION_ENTITY(vehDelete, FALSE, TRUE)
					DELETE_VEHICLE(vehDelete)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			dmVarsPassed.viArenaVehToCleanup = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
		ENDIF
	ENDIF

ENDPROC

PROC HOST_SET_ARENA_AUDIO_SCORE(ServerBroadcastData &serverBDpassed)
	IF NOT CONTENT_IS_USING_ARENA()
	OR NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	IF serverBDpassed.iArenaAudioScore = -1
		serverBDpassed.iArenaAudioScore = GET_RANDOM_INT_IN_RANGE(1, 9)
		PRINTLN("[HOST_SET_ARENA_AUDIO_SCORE] - Setting Audio Score for DM to iArenaAudioScore: ", serverBDpassed.iArenaAudioScore)
	ENDIF
ENDPROC

PROC PROCESS_PLAY_ARENA_AUDIO_SCORE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	
	IF g_bMissionEnding
	OR g_bCelebrationScreenIsActive	
		PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - Exitting because g_bMissionEnding: ", g_bMissionEnding, " g_bCelebrationScreenIsActive: ", g_bCelebrationScreenIsActive)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO)
		PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - Exitting because 30s Countdown: ", IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO))
		EXIT
	ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
		IF serverBDpassed.iArenaAudioScore > -1
			IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_ARENA_AUDIO_TRACK_PLAYED)
				START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
				SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
				TEXT_LABEL_15 tl15_track = "MC_AW_MUSIC_"
				tl15_track += serverBDpassed.iArenaAudioScore
				TRIGGER_MUSIC_EVENT(tl15_track)
			
				SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_ARENA_AUDIO_TRACK_PLAYED)
				
				PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - iArenaAudioScore: ", serverBDpassed.iArenaAudioScore, " Playing: ", tl15_track)
			ENDIF
		ELSE
			PRINTLN("[PROCESS_PLAY_ARENA_AUDIO_SCORE] - Waiting for iArenaAudioScore to be > -1")
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Pass the Bomb
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC FLOAT GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS(ServerBroadcastData &serverBDpassed)

	IF NOT HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
		RETURN 9999.0
	ENDIF
	
	FLOAT fCurDifference = TO_FLOAT((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDPassed.tdTaggedExplosionTimer)/1000))
	FLOAT fMaxTime = TO_FLOAT(GET_DM_TAG_EXPLOSION_TIMER_VALUE(g_FMMC_STRUCT.iTagExplosionTimer[serverBDpassed.iTagBombsDetonated]))
	
	FLOAT fReturn = fMaxTime - fCurDifference

	PRINTLN("[PTB] GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS - Returning ", fReturn)
	RETURN fReturn
ENDFUNC

PROC HOST_PROCESS_SET_TAGGED_PLAYER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT iPart, BOOL bPassed)

	INT iPreviousCarrier = serverBDPassed.iPartTagged
	
	serverBDPassed.iPartTagged = iPart
	serverBDPassed.iPartLastTagged = serverBDPassed.iPartTagged
	
	IF bPassed
		IF NOT HAS_NET_TIMER_STARTED(serverBDpassed.iLastPassTimeStamp)
		OR HAS_NET_TIMER_EXPIRED(serverBDpassed.iLastPassTimeStamp, 10000)
			REINIT_NET_TIMER(serverBDpassed.iLastPassTimeStamp)
			serverBDpassed.iRecentPasses = 0
			PRINTLN("[PTB][PTBAnnouncer] Restarting recent pass timer & pass count!")
		ENDIF
		
		serverBDpassed.iRecentPasses++
		PRINTLN("[PTB][PTBAnnouncer] Upping iRecentPasses to ", serverBDpassed.iRecentPasses)
		
		serverBDpassed.iPTB_Passes[iPreviousCarrier]++
		PRINTLN("[PASSTHEBOMB] HOST_PROCESS_SET_TAGGED_PLAYER || Increasing iPTB_Passes to ", serverBDpassed.iPTB_Passes[iPreviousCarrier], " for participant ", iPreviousCarrier)
		
		IF NOT IS_TEAM_DEATHMATCH()
			IF serverBDpassed.iRecentPasses >= 3
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_MultipleBombPasses)
			ELIF GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS(serverBDPassed) <= 9.0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_PassedJustInTime)
			ELSE
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_PassedTheBomb)
			ENDIF
		ENDIF
	ENDIF
	
	//If we're clearing the tagged player, keep this variable the same so that it gets passed to someone on the same team as who had it last
	IF IS_TEAM_DEATHMATCH()
		IF iPart > -1
			serverBDPassed.iPartTaggedTeam = playerBDPassed[iPart].iTeam
			PRINTLN("[PASSTHEBOMB] HOST_PROCESS_SET_TAGGED_PLAYER - Setting iPartTaggedTeam to ", serverBDPassed.iPartTaggedTeam)
		ELSE
			PRINTLN("[PASSTHEBOMB] HOST_PROCESS_SET_TAGGED_PLAYER - Leaving iPartTaggedTeam as ", serverBDPassed.iPartTaggedTeam)
		ENDIF
	ENDIF
	
	PRINTLN("[PASSTHEBOMB][PTBMAJOR] HOST_PROCESS_SET_TAGGED_PLAYER - Giving the bomb to participant ", iPart, " || ", GET_PARTICIPANT_NAME(iPart))
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC SET_TAGGED_PLAYER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT iPart, BOOL bPassedOver = FALSE)
	UNUSED_PARAMETER(serverBDPassed)
	UNUSED_PARAMETER(playerBDPassed)
	
	IF serverBDPassed.iPartTagged != iPart
		BROADCAST_DM_PASS_THE_BOMB_TAGGED(iPart, -1, bPassedOver)
	ELSE
		PRINTLN("[PASSTHEBOMB] SET_TAGGED_PLAYER - Passing bomb to part who already has bomb!")
	ENDIF
ENDPROC

PROC CLEAR_TAGGED_PLAYER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	PRINTLN("[PASSTHEBOMB] CLEAR_TAGGED_PLAYER - Clearing bomb carrier!")
	HOST_PROCESS_SET_TAGGED_PLAYER(serverBDpassed, playerBDPassed, -1, FALSE)
ENDPROC

FUNC INT GET_TAG_EXPLOSION_TIME(ServerBroadcastData &serverBDpassed)

	IF serverBDpassed.iTagBombsDetonated < NUM_NETWORK_PLAYERS
		RETURN g_FMMC_STRUCT.iTagExplosionTimer[serverBDpassed.iTagBombsDetonated]
	ENDIF
	
	RETURN DM_TAG_EXPLOSION_TIMER_30
ENDFUNC

FUNC BOOL HAS_TAG_BOMB_TIMER_EXPIRED(ServerBroadcastData &serverBDpassed)
	IF HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
		RETURN HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBDPassed.tdTaggedExplosionTimer, GET_DM_TAG_EXPLOSION_TIMER_VALUE(GET_TAG_EXPLOSION_TIME(serverBDpassed)) * 1000)
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL HAS_TAG_BOMB_TIMER_NEARLY_EXPIRED(ServerBroadcastData &serverBDpassed)
	IF HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
		RETURN HAS_NET_TIMER_EXPIRED_READ_ONLY(serverBDPassed.tdTaggedExplosionTimer, (GET_DM_TAG_EXPLOSION_TIMER_VALUE(GET_TAG_EXPLOSION_TIME(serverBDpassed)) * 1000) - 500)
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL IS_PARTICIPANT_VALID_TO_BE_TAGGED(INT iParticipant, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	UNUSED_PARAMETER(serverBDpassed)
	
	IF iParticipant = -1
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		PRINTLN("[PASSTHEBOMB SPAM] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning FALSE || Participant ", iParticipant, " is not active")
		RETURN FALSE
	ENDIF
	
	IF playerBDPassed[iParticipant].eClientLogicStage != CLIENT_MISSION_STAGE_FIGHTING
		PRINTLN("[PASSTHEBOMB SPAM] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning FALSE || Participant ", iParticipant, " is not at CLIENT_MISSION_STAGE_FIGHTING")
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
	PED_INDEX piPed = GET_PLAYER_PED(piPlayer)
	
	IF IS_PARTICIPANT_A_SPECTATOR(playerBDPassed, iParticipant)
	OR IS_PLAYER_SPECTATOR(playerBDPassed, piPlayer, iParticipant)
		PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning FALSE || Participant ", iParticipant, " is some form of spectator || ", GET_PLAYER_NAME(piPlayer))
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_DEAD(piPlayer)
		PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning FALSE || Participant ", iParticipant, " is dead || ", GET_PLAYER_NAME(piPlayer))
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(piPlayer)
		PRINTLN("[PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning FALSE || Participant ", iParticipant, " isn't okay || ", GET_PLAYER_NAME(piPlayer))
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(piPed)
	AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_USING(piPed)) = RCBANDITO
		PRINTLN("[PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning FALSE || Participant ", iParticipant, " is in an RCBANDITO || ", GET_PLAYER_NAME(piPlayer))
		RETURN FALSE
	ENDIF
	
	PRINTLN("[PASSTHEBOMB SPAM] IS_PARTICIPANT_VALID_TO_BE_TAGGED - Returning TRUE!! || Participant ", iParticipant, " is valid || ", GET_PLAYER_NAME(piPlayer))
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED(INT iParticipant, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], BOOL bIgnoreTeamCheck = FALSE)
	
	IF NOT IS_PARTICIPANT_VALID_TO_BE_TAGGED(iParticipant, serverBDpassed, playerBDPassed)
		PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN SPAM] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Returning FALSE || Participant ", iParticipant, " is returning FALSE in IS_PARTICIPANT_VALID_TO_BE_TAGGED")
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
	PED_INDEX piPed = GET_PLAYER_PED(piPlayer)
	
	IF iParticipant = serverBDPassed.iPartLastTagged
		PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Returning FALSE || Participant ", iParticipant, " was the last person to be tagged || ", GET_PLAYER_NAME(piPlayer))
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ALIVE(piPed)
		IF ABSF(GET_ENTITY_SPEED(piPed)) > 5.0
			IF GET_RANDOM_FLOAT_IN_RANGE(0.0, 100.0) > 65.0
				PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Returning FALSE || Participant ", iParticipant, " is possibly valid to be tagged but they're off the hook due to not being stationary and getting lucky || ", GET_PLAYER_NAME(piPlayer))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_TEAM_DEATHMATCH()
	AND serverBDPassed.iPartTaggedTeam > -1
	AND serverBDPassed.iPartTaggedTeam != playerBDPassed[iParticipant].iTeam
		IF NOT bIgnoreTeamCheck
			PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN SPAM] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Returning FALSE || Participant ", iParticipant, " is on the enemy team to the current owner which would be a bit unfair || ", GET_PLAYER_NAME(piPlayer))
			RETURN FALSE
		ELSE
			PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN SPAM] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Ignoring the fact that ", GET_PLAYER_NAME(piPlayer), " is on the enemy team due to bIgnoreTeamCheck")
		ENDIF
	ENDIF
	
	PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Returning TRUE || Participant ", iParticipant, " is valid  || ", GET_PLAYER_NAME(piPlayer))
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PART_THE_ONLY_VALID_PART_TO_BE_TAGGED(INT iPart, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBD[])
	INT i
	FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
	
		IF i = iPart
			RELOOP
		ENDIF
		
		IF IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED(i, serverBDPassed, playerBD, TRUE) // Passing TRUE in here - we only want this function to prevent tagging if they're literally the ONLY player left, not if they're the only player on their team
			PRINTLN("IS_PART_THE_ONLY_VALID_PART_TO_BE_TAGGED - Participant ", iPart, " is also valid")
			RETURN FALSE
		ENDIF
	ENDFOR
	
	PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PART_THE_ONLY_VALID_PART_TO_BE_TAGGED - Participant ", iPart, " is the only valid participant!! || ", GET_PARTICIPANT_NAME(iPart))
	RETURN TRUE
ENDFUNC

FUNC INT GET_NUMBER_OF_VALID_PARTS_TO_BE_TAGGED(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBD[])
	INT i
	INT iValidParts = 0
	
	FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
		IF IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED(i, serverBDpassed, playerBD, TRUE)
			iValidParts++
			PRINTLN("[PASSTHEBOMB SPAM][PASSTHEBOMB REASSIGN SPAM] GET_NUMBER_OF_VALID_PARTS_TO_BE_TAGGED - Participant ", i, " is valid")
		ENDIF
	ENDFOR
	
	PRINTLN("[PASSTHEBOMB SPAM][PASSTHEBOMB REASSIGN] GET_NUMBER_OF_VALID_PARTS_TO_BE_TAGGED - Returning ", iValidParts)
	RETURN iValidParts
ENDFUNC

FUNC BOOL RANDOMLY_REASSIGN_TAGGED_PLAYER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBD[])
	
	INT iNumberOfValidParts = GET_NUMBER_OF_VALID_PARTS_TO_BE_TAGGED(serverBDpassed, playerBD)
	
	INT i
	FOR i = 0 TO 3
		INT iRandomiseTaggedPlayer = GET_RANDOM_INT_IN_RANGE(0, NUM_NETWORK_PLAYERS)
		PRINTLN("[ML][PASSTHEBOMB][PASSTHEBOMB REASSIGN SPAM] RANDOMLY_REASSIGN_TAGGED_PLAYER - Trying to assign to part ", iRandomiseTaggedPlayer)
		
		IF IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED(iRandomiseTaggedPlayer, serverBDpassed, playerBD)
		AND iNumberOfValidParts > 1 // If they're the last remaining valid player, don't give them the bomb - they've won!
		
			SET_TAGGED_PLAYER(serverBDpassed, playerBD, iRandomiseTaggedPlayer)
			PRINTLN("[ML][PASSTHEBOMB][PASSTHEBOMB REASSIGN] RANDOMLY_REASSIGN_TAGGED_PLAYER - Finally selected Participant Index: ", serverBDPassed.iPartTagged)
			RETURN TRUE
		ELSE
			PRINTLN("[ML][PASSTHEBOMB][PASSTHEBOMB REASSIGN SPAM] RANDOMLY_REASSIGN_TAGGED_PLAYER - Attempted to assign to Participant Index: ", iRandomiseTaggedPlayer, " but they weren't valid/active. || Last Tagged Participant Index : ", serverBDPassed.iPartLastTagged)
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_SCRIPT_EVENT_DM_PASS_THE_BOMB_TAGGED(INT iEventID, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	SCRIPT_EVENT_DATA_DM_PASS_THE_BOMB_TAGGED EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
	
		PRINTLN("[PASSTHEBOMB][PTBMAJOR] Received a 'pass bomb' event! Passing to participant: ", serverBDPassed.iPartTagged)
		
		IF HAS_TAG_BOMB_TIMER_EXPIRED(serverBDpassed)
			PRINTLN("[PASSTHEBOMB][PTBMAJOR] The bomb is already due to explode! Not passing - exiting now")
			EXIT
		ENDIF
		
		IF EventData.iParticipantToTag > -1
			IF IS_TEAM_DEATHMATCH()	
			
				PRINTLN("[PASSTHEBOMB] PROCESS_SCRIPT_EVENT_DM_PASS_THE_BOMB_TAGGED - serverBDPassed.iPartTaggedTeam: ", serverBDPassed.iPartTaggedTeam, " || playerBDPassed[EventData.iParticipantToTag].iTeam: ", playerBDPassed[EventData.iParticipantToTag].iTeam)
				
				IF EventData.iParticipantToTag > -1
				AND serverBDPassed.iPartTagged > -1
					IF playerBDPassed[serverBDPassed.iPartTagged].iTeam = playerBDPassed[EventData.iParticipantToTag].iTeam
						PRINTLN("[PASSTHEBOMB] PROCESS_SCRIPT_EVENT_DM_PASS_THE_BOMB_TAGGED || Exiting early because the tagger and tag-ee are on the same team")
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT EventData.bPassedOver
			CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_PassTheBomb_BombTimer50Seconds)
			CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_PassTheBomb_BombTimer40Seconds)
			CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_PassTheBomb_BombTimer30Seconds)
			CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_PassTheBomb_BombTimer10Seconds)
			CLEAR_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCKED(g_iAA_PlaySound_PassTheBomb_BombTimer10SecondsLastBomb)
			
			PRINTLN("[PASSTHEBOMB][PTBAnnouncer] Clearing locks on timers")
		ENDIF
		
		IF bIsLocalPlayerHost
			HOST_PROCESS_SET_TAGGED_PLAYER(serverBDpassed, playerBDPassed, EventData.iParticipantToTag, EventData.bPassedOver)
		ENDIF
		
		IF HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)
			PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("DM_PASS_BOMB", INT_TO_PLAYERINDEX(EventData.iParticipantToTag), "")
		ENDIF
		
		PLAY_ARENA_CROWD_CHEER_AUDIO() 
		PRINTLN("[CROWD CHEER] Hot Bomb - Bomb passed to another player")
		
	ENDIF
ENDPROC

PROC PROCESS_PASS_THE_BOMB_OVERHEAD(ServerBroadcastData &serverBDpassed)
	INT iPlayer
	FOR iPlayer = 0 TO (NUM_NETWORK_PLAYERS - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			IF serverBDPassed.iPartTagged = iPlayer 
				SET_BIT(GlobalplayerBD[iPlayer].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))
				SET_OVERHEAD_ACTIVE(DISPLAYOVERHEADS_DISPLAY_INVENTORY, TRUE, INT_TO_PLAYERINDEX(iPlayer))
			ELSE
				CLEAR_BIT(GlobalplayerBD[iPlayer].iMyOverHeadInventory, ENUM_TO_INT(MP_TAG_PACKAGE_LARGE))
			ENDIF
		ENDIF
	ENDFOR		
ENDPROC

FUNC BOOL PASS_THE_BOMB_TIME_CRITICAL(ServerBroadcastData &serverBDpassed)
		
	FLOAT fCurrentTime = TO_FLOAT((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDPassed.tdTaggedExplosionTimer)/1000))
	FLOAT fCriticalLimit = TO_FLOAT((GET_DM_TAG_EXPLOSION_TIMER_VALUE(g_FMMC_STRUCT.iTagExplosionTimer[serverBDpassed.iTagBombsDetonated])) - 10)
	
	PRINTLN("fCurrentTime = ", fCurrentTime)
	PRINTLN("fCriticalLimit ", fCriticalLimit)
	
	IF fCurrentTime < fCriticalLimit
		RETURN FALSE
	ELSE	
		RETURN TRUE
	ENDIF

ENDFUNC


FUNC BOOL CAN_SHOW_BOMB_TIMER()
	
	IF IS_RP_BAR_ACTIVE_IN_ARENA_BOX()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_PASS_THE_BOMB_HUD_STATE(PASS_THE_BOMB_HUD_STATE eNewState)
	ePassTheBombHUDState = eNewState
	PRINTLN("[PASSTHEBOMBHUD] Setting HUD state to ", eNewState)
ENDPROC

FUNC BOOL SHOULD_DRAW_PTB_HUD(ServerBroadcastData &serverBDpassed, PED_INDEX piPlayerPed, BOOL bDisableTimer)

	IF NOT HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - Not started the timer!")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_DEAD_OR_DYING(piPlayerPed)
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - Player is dead")
		RETURN FALSE
	ENDIF
	
	IF bDisableTimer
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - bDisableTimer")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - Skyswoop is up")
		RETURN FALSE
	ENDIF
	
	IF NOT GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - Renderphase is paused")
		RETURN FALSE
	ENDIF
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appinternet")) > 0
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - Internet app is active")
		RETURN FALSE
	ENDIF
	
	IF IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		PRINTLN("[PTB] SHOULD_DRAW_PTB_HUD - IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE")
		RETURN FALSE
	ENDIF
	
	PRINTLN("[HIDDENPRINT] PTB Hud is okay to be drawn")
	RETURN TRUE
ENDFUNC

PROC PROCESS_PASS_THE_BOMB_HUD(ServerBroadcastData &serverBDpassed, PlayerBroadcastData& playerBDPassed[], BOOL bDisableTimer)
	
	FLOAT fPercentage
	FLOAT fNumerator
	FLOAT fDenominator
	INT iHudColour
	INT iHudColour2
	
	IF ePassTheBombHUDState != PASS_THE_BOMB_HUD_OFF
	AND ePassTheBombHUDState != PASS_THE_BOMB_HUD_LOADING
		IF NOT SHOULD_DRAW_PTB_HUD(serverBDPassed, LocalPlayerPed, bDisableTimer)
			PRINTLN("[PASSTHEBOMBHUD] Exiting due to SHOULD_DRAW_PTB_HUD")
			EXIT
		ENDIF
	ENDIF
	
	SWITCH ePassTheBombHUDState
		CASE PASS_THE_BOMB_HUD_OFF
			PRINTLN("[PASSTHEBOMBHUD] Starting up the PASS_THE_BOMB HUD!")
			SET_PASS_THE_BOMB_HUD_STATE(PASS_THE_BOMB_HUD_LOADING)
		BREAK
		
		CASE PASS_THE_BOMB_HUD_LOADING
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_PASS_THE_BOMB_HUD_STATE(PASS_THE_BOMB_HUD_SETUP)
			ELSE
				PRINTLN("[PASSTHEBOMBHUD] Requesting scaleform!")
				sfiPPGenericMovie = REQUEST_SCALEFORM_MOVIE("POWER_PLAY_GENERIC")
			ENDIF
		BREAK
		
		CASE PASS_THE_BOMB_HUD_SETUP
			
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
				//INT iHudColour
				iHudColour = ENUM_TO_INT(HUD_COLOUR_GREEN)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour)
				PRINTLN("[PASSTHEBOMBHUD] - PROCESS_PASS_THE_BOMB_HUD - Adding hud colour: ", iHudColour)
			END_SCALEFORM_MOVIE_METHOD()
				
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_TEAM")
				iHudColour2 = ENUM_TO_INT(HUD_COLOUR_RED)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iHudColour2)
				PRINTLN("[PASSTHEBOMBHUD] - PROCESS_PASS_THE_BOMB_HUD - Adding hud colour: ", iHudColour)
			END_SCALEFORM_MOVIE_METHOD()	
				
				
			BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "ADD_ICON")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(4)
				PRINTLN("[PASSTHEBOMBHUD] - PROCESS_PASS_THE_BOMB_HUD - Adding icon : ", 4) // 4 = Bomb icon
			END_SCALEFORM_MOVIE_METHOD()
			
			SET_PASS_THE_BOMB_HUD_STATE(PASS_THE_BOMB_HUD_RUNNING)
		BREAK
		
		CASE PASS_THE_BOMB_HUD_RUNNING
			IF playerBDpassed[iLocalPart].iGameState = GAME_STATE_RUNNING
			AND GET_SERVER_GAME_STATE(serverBDpassed) = GAME_STATE_RUNNING
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sfiPPGenericMovie, "SET_ICON_TIMER")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)	// Setting meter on the first icon
			
				IF HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)			
					fNumerator = TO_FLOAT((GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDPassed.tdTaggedExplosionTimer)/1000))
					fDenominator = TO_FLOAT(GET_DM_TAG_EXPLOSION_TIMER_VALUE(g_FMMC_STRUCT.iTagExplosionTimer[serverBDpassed.iTagBombsDetonated]))
					fPercentage = 1 - (fNumerator / fDenominator)	// Starts with "100 - " To empty the timer rather than fill it
					
					IF NOT PASS_THE_BOMB_TIME_CRITICAL(serverBDpassed)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fPercentage)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
					ELSE
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fPercentage)
					ENDIF
				ENDIF
				END_SCALEFORM_MOVIE_METHOD()	
				
				IF CAN_SHOW_BOMB_TIMER()
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfiPPGenericMovie, 255, 255, 255, 255)
					DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_RANKBAR)
				ENDIF
			ELSE
				SET_PASS_THE_BOMB_HUD_STATE(PASS_THE_BOMB_HUD_CLEANUP)
			ENDIF
		BREAK
		
		CASE PASS_THE_BOMB_HUD_CLEANUP
			IF HAS_SCALEFORM_MOVIE_LOADED(sfiPPGenericMovie)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfiPPGenericMovie)
			ENDIF				
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC PROCESS_PASSTHEBOMB_PASSING(INT iPartToCheck, VEHICLE_INDEX viMyVeh, SHARED_DM_VARIABLES &dmVarsPassed)
	
	PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPartToCheck)
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
	PED_INDEX piPedToCheck = GET_PLAYER_PED(piPlayer)
			
	VEHICLE_INDEX viTheirVeh
	
	IF IS_ENTITY_ALIVE(LocalPlayerPed)
		IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			viMyVeh = dmVarsPassed.viPTBVeh
			PRINTLN("[PASSTHEBOMB] PROCESS_PASSTHEBOMB_PASSING - Using dmVarsPassed.viPTBVeh")
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(piPedToCheck)
		IF IS_PED_IN_ANY_VEHICLE(piPedToCheck)
			viTheirVeh = GET_VEHICLE_PED_IS_USING(piPedToCheck)
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(viMyVeh)
	AND IS_ENTITY_ALIVE(viTheirVeh)
		IF IS_ENTITY_TOUCHING_ENTITY(viMyVeh, viTheirVeh)
			IF PASS_THE_BOMB__CAN_BE_TAGGED()
				BROADCAST_DM_PASS_THE_BOMB_TAGGED(iPartToCheck, iLocalPart, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_START_PTB_TIMER(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])

	UNUSED_PARAMETER(dmVarsPassed)

	IF playerBDPassed[serverBDPassed.iPartTagged].eClientLogicStage <> CLIENT_MISSION_STAGE_FIGHTING
		PRINTLN("[ML][PASSTHEBOMB] SHOULD_START_PTB_TIMER - Returning FALSE! Waiting for CLIENT_MISSION_STAGE_FIGHTING. Current: ", playerBDPassed[serverBDPassed.iPartTagged].eClientLogicStage)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_PASSTHEBOMB_HOST(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])
	
	IF serverBDPassed.iPartTagged = -1
	
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
			PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Waiting for SERV_BITSET2_FINISHED_COUNTDOWN")
			EXIT
		ENDIF
	
		IF RANDOMLY_REASSIGN_TAGGED_PLAYER(serverBDpassed, playerBDPassed)
			
			IF HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
			AND HAS_TAG_BOMB_TIMER_EXPIRED(serverBDpassed)
				serverBDPassed.iTagBombsDetonated++
				PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Tagged player has exploded and timer is up! Upping iTagBombsDetonated to ", serverBDPassed.iTagBombsDetonated)
			ENDIF
	
			RESET_NET_TIMER(serverBDPassed.tdTaggedExplosionTimer)
		ELSE
			PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Didn't find a new player to tag just yet")
		ENDIF
	ELSE
		
		IF NOT HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
			IF SHOULD_START_PTB_TIMER(serverBDPassed, dmVarsPassed, playerBDPassed)
				REINIT_NET_TIMER(serverBDPassed.tdTaggedExplosionTimer)
				PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Starting timer!")
				
				IF NOT IS_TEAM_DEATHMATCH()
					INT iTime = GET_DM_TAG_EXPLOSION_TIMER_VALUE(g_FMMC_STRUCT.iTagExplosionTimer[serverBDpassed.iTagBombsDetonated])
					PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - This timer will last for: ", iTime)
					
					IF iTime = 50
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_BombTimer50Seconds, TRUE)
						PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Playing 50 seconds line!")
					ELIF iTime = 40
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_BombTimer40Seconds, TRUE)
						PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Playing 40 seconds line!")
					ELIF iTime = 30
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_BombTimer30Seconds, TRUE)
						PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Playing 30 seconds line!")
					ELIF iTime = 10
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_BombTimer10Seconds, TRUE)
						PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Playing 10 seconds line!")
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("hotbombdebug")
					DRAW_DEBUG_TEXT_2D("Not starting timer", <<0.5, 0.25, 0.5>>)
				ENDIF
				PRINTLN("[ML][PASSTHEBOMB] PROCESS_PASSTHEBOMB_HOST - Not starting timer!")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("hotbombdebug")
				DRAW_DEBUG_TEXT_2D("Timer is running", <<0.05, 0.15, 0.5>>)
			ENDIF
			#ENDIF
		ENDIF
				
		// IF THE PLAYER IS INVALID OR DEAD, DUE TO EITHER THE TIMEBOMB OR OTHER PLAYERS, SELECT A NEW PLAYER TO BE TAGGED
		IF NOT IS_PARTICIPANT_VALID_TO_BE_TAGGED(serverBDPassed.iPartTagged, serverBDPassed, playerBDPassed)
			PRINTLN("[ML][PASSTHEBOMB][PTBMAJOR] PROCESS_PASSTHEBOMB_HOST - Clearing tagged player because they are no longer valid!")
			CLEAR_TAGGED_PLAYER(serverBDPassed, playerBDPassed)	// Set no player tagged
		ENDIF
		
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("hotbombdebug")
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
			PRINTLN("[ML][PASSTHEBOMB][PTBMAJOR] PROCESS_PASSTHEBOMB_HOST - Clearing tagged player because debug key was pressed!")
			CLEAR_TAGGED_PLAYER(serverBDPassed, playerBDPassed)	// Set no player tagged
		ENDIF
	ENDIF
	#ENDIF
	
ENDPROC

FUNC STRING GET_ENEMY_TEAM_COLOUR_AS_STRING(PlayerBroadcastData& playerBDPassed[])
	STRING sEnemyTeamColour = "~r~"
		
	IF IS_TEAM_DEATHMATCH()
		SWITCH playerBDPassed[iLocalPart].iTeam
			CASE 0
				sEnemyTeamColour = "~p~"
			BREAK
			CASE 1
				sEnemyTeamColour = "~o~"
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN sEnemyTeamColour
ENDFUNC

FUNC HUD_COLOURS GET_MY_TEAM_COLOUR_AS_HUDCOLOUR(PlayerBroadcastData& playerBDPassed[])
		
	IF IS_TEAM_DEATHMATCH()
		SWITCH playerBDPassed[iLocalPart].iTeam
			CASE 0
				RETURN HUD_COLOUR_ORANGE
			BREAK
			CASE 1
				RETURN HUD_COLOUR_PURPLE
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN HUD_COLOUR_RED
ENDFUNC

FUNC HUD_COLOURS GET_ENEMY_TEAM_COLOUR_AS_HUDCOLOUR(PlayerBroadcastData& playerBDPassed[])
		
	IF IS_TEAM_DEATHMATCH()
		SWITCH playerBDPassed[iLocalPart].iTeam
			CASE 0
				RETURN HUD_COLOUR_PURPLE
			BREAK
			CASE 1
				RETURN HUD_COLOUR_ORANGE
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN HUD_COLOUR_RED
ENDFUNC

PROC INITIALISE_BOMB_CARRIER_MODE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PED_INDEX piPlayerPed, VEHICLE_INDEX veh)
	
	IF NOT ANIMPOSTFX_IS_RUNNING("CrossLine")
	AND IS_ENTITY_ALIVE(piPlayerPed)
	AND HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
	AND NOT IS_HEAVY_FILTER_ARENA_LIGHTING()
		ANIMPOSTFX_PLAY("CrossLine", 0, TRUE)
	ENDIF

	IF IS_ENTITY_ALIVE(veh)
	AND IS_ENTITY_ALIVE(piPlayerPed)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		SET_VEHICLE_HANDLING_OVERRIDE(veh, HANDLING_SPORTS_CAR)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, 1.0)
		
		SET_VEHICLE_MOD_KIT(veh, 0)
		PRINTLN("[TMS][Nitro] SET_VEHICLE_MOD_KIT")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_GiveBombCarrierNitro)
								
			INT iModLevel = g_FMMC_STRUCT.iPassTheBomb_CarrierNitroLevel
			
			IF iModLevel > 2
				iModLevel = 2
			ENDIF
			
			SET_VEHICLE_MOD(veh, MOD_ENGINEBAY2, iModLevel)
			
			IF g_FMMC_STRUCT.iPassTheBomb_CarrierNitroLevel >= 3
				SET_OVERRIDE_NITROUS_LEVEL(veh, TRUE, 2.5, 1.1, 4.0)
			ENDIF
			
			PRINTLN("[PASSTHEBOMB][Nitro] Giving myself nitro boosters of level ", g_FMMC_STRUCT.iPassTheBomb_CarrierNitroLevel)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(piPlayerPed)
		PLAY_SOUND_FRONTEND(-1, "Bomb_Collected", "DLC_AW_PTB_Sounds", FALSE)
		PRINTLN("[PASSTHEBOMBSND] - PROCESS_PASSTHEBOMB_CLIENT - Starting Bomb_Collected sound!")
	ENDIF
	
	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "ARENA_GOT_BOMB")
	PRINTLN("[PASSTHEBOMB] Setting up new 'ARENA_GOT_BOMB' big message!")
	
	PRINT_HELP("PTB_GBMB")
	
	SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_BOMB_CARRIER_MODE_STARTED)
ENDPROC

PROC INITIALISE_NON_BOMB_CARRIER_MODE(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData& playerBDPassed[], PED_INDEX piPlayerPed, VEHICLE_INDEX veh, BOOL bIsInit = FALSE)
	IF ANIMPOSTFX_IS_RUNNING("CrossLine")
		PRINTLN("[PASSTHEBOMB] Stopping 'CrossLine' VFX")
		ANIMPOSTFX_STOP("CrossLine")
	ENDIF
	
	IF NOT bIsInit
		IF NOT IS_PED_INJURED(piPlayerPed)
			PLAY_SOUND_FRONTEND(-1, "Bomb_Passed", "DLC_AW_PTB_Sounds", FALSE)
			PRINTLN("[PASSTHEBOMBSND] - PROCESS_PASSTHEBOMB_CLIENT - Starting Bomb_Passed sound!")
		ELSE
			PRINTLN("[PASSTHEBOMBSND] - PROCESS_PASSTHEBOMB_CLIENT - Not playing Bomb_Passed sound since I'm dead")
		ENDIF
	ENDIF

	IF IS_ENTITY_ALIVE(veh)
	AND IS_ENTITY_ALIVE(piPlayerPed)
	AND NETWORK_HAS_CONTROL_OF_ENTITY(veh)
	
		SET_VEHICLE_HANDLING_OVERRIDE(veh, HANDLING_AVERAGE)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(LocalPlayer, g_FMMC_STRUCT.fPassTheBomb_DragForNonCarriers)
		
		SET_VEHICLE_MOD_KIT(veh, 0)
		
		PRINTLN("[PASSTHEBOMB][Nitro] SET_VEHICLE_MOD_KIT")
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_GiveBombCarrierNitro)
			SET_VEHICLE_MOD(veh, MOD_ENGINEBAY2, dmVarsPassed.iPTBCachedNitroModLevel)
			SET_ABILITY_BAR_VISIBILITY(FALSE)
			SET_OVERRIDE_NITROUS_LEVEL(veh, FALSE)
			
			PRINTLN("[PASSTHEBOMB][Nitro] Taking away tagged player boosters! Setting mod level to ", dmVarsPassed.iPTBCachedNitroModLevel)
		ENDIF
	ELSE
		PRINTLN("[PASSTHEBOMB][Nitro] in the ELSE!")
	ENDIF
	
	playerBDPassed[iLocalPart].iPTB_TimeSpentCarryingBomb = dmVarsPassed.iTimeCarryingBomb
	PRINTLN("[PTB](2) Updating iPTB_TimeSpentCarryingBomb to ", dmVarsPassed.iTimeCarryingBomb)
	
	CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_BOMB_CARRIER_MODE_STARTED)
ENDPROC	

PROC PASSTHEBOMB_INIT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData& playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, PED_INDEX piPlayerPed, VEHICLE_INDEX veh)
	IF dmVarsPassed.iPTBCachedNitroModLevel = -99
	AND DOES_ENTITY_EXIST(veh)
		dmVarsPassed.iPTBCachedNitroModLevel = GET_VEHICLE_MOD(veh, MOD_ENGINEBAY2)
		PRINTLN("[PTB][Nitro] Caching iPTBCachedNitroModLevel as ", dmVarsPassed.iPTBCachedNitroModLevel)
	ENDIF
	
	IF serverBDPassed.iPartTagged = iLocalPart
		INITIALISE_BOMB_CARRIER_MODE(serverBDPassed, dmVarsPassed, piPlayerPed, veh)
	ELSE
		INITIALISE_NON_BOMB_CARRIER_MODE(dmVarsPassed, playerBDPassed, piPlayerPed, veh, TRUE)
	ENDIF
ENDPROC

PROC PROCESS_PASSTHEBOMB_CLIENT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData& playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	BOOL bDisableTimer = FALSE
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
	AND NOT HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
		PRINTLN("[PTB][PASSTHEBOMB SPAM] Exiting client processing due to SERV_BITSET2_FINISHED_COUNTDOWN")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF dmVarsPassed.debugDmVars.bDisablePTBTimer
		bDisableTimer = TRUE
	ENDIF
	
	IF bDisableTimer
		DRAW_DEBUG_TEXT_2D("Disabling PTB Functionality due to widget", <<0.15, 0.85, 0.5>>)
		PRINTLN("[PTB][PASSTHEBOMB SPAM] Disabling PTB Functionality due to widget")
	ENDIF
	#ENDIF
	
	PED_INDEX piPlayerPed = LocalPlayerPed
	VEHICLE_INDEX veh
	
	IF IS_ENTITY_ALIVE(piPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(piPlayerPed)
			veh = GET_VEHICLE_PED_IS_USING(piPlayerPed)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_PASSTHEBOMB_INITIALISED)
		PASSTHEBOMB_INIT(serverBDPassed, playerBDPassed, dmVarsPassed, piPlayerPed, veh)
		
		PRINTLN("[TMS][PASSTHEBOMB][PTB] Initialised Pass the Bomb!")
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_PASSTHEBOMB_INITIALISED)
	ENDIF
	
	dmVarsPassed.viPTBVeh = veh
	PROCESS_PASS_THE_BOMB_OVERHEAD(serverBDpassed)
	
	IF serverBDPassed.iPartTagged = iLocalPart	//If I'm the tagged player...
		
		PRINTLN("[PASSTHEBOMB SPAM] I AM TAGGED")
		dmVarsPassed.iTimeCarryingBomb += ROUND(GET_FRAME_TIME() * 1000)
		g_sArena_Telemetry_data.m_bombTime = dmVarsPassed.iTimeCarryingBomb
		PRINTLN("[ARENA][TEL] - Bomb Time - ", g_sArena_Telemetry_data.m_bombTime)
		STRING sTaggedLabel = "PTB_TGD_PTB"
		
		IF NOT Has_This_MP_Objective_Text_With_User_Created_String_Been_Received(sTaggedLabel, GET_ENEMY_TEAM_COLOUR_AS_STRING(playerBDPassed))
		AND NOT IS_PARTICIPANT_A_SPECTATOR(playerBDPassed, iLocalPart)
			Print_Objective_Text_with_user_created_string(sTaggedLabel, GET_ENEMY_TEAM_COLOUR_AS_STRING(playerBDPassed))
			dmVarsPassed.iTagBomb_PrevObjectiveTextPartIndex = -1
			PRINTLN("[TMS][PTBMAJOR] Setting iTagBomb_PrevObjectiveTextPartIndex to ", dmVarsPassed.iTagBomb_PrevObjectiveTextPartIndex)
		ENDIF
	
		IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_BOMB_CARRIER_MODE_STARTED)
			INITIALISE_BOMB_CARRIER_MODE(serverBDpassed, dmVarsPassed, piPlayerPed, veh)
		ENDIF
		
		IF NOT bDisableTimer
			IF HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
				IF HAS_TAG_BOMB_TIMER_EXPIRED(serverBDpassed)
				AND NOT IS_PED_DEAD_OR_DYING(piPlayerPed)
				AND NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_EXPLODED_FROM_BEING_TAGGED)
					
					SET_ENTITY_HEALTH(piPlayerPed, 0, piPlayerPed)
					
					ADD_EXPLOSION(GET_ENTITY_COORDS(piPlayerPed), EXP_TAG_HI_OCTANE, 1.0, TRUE, FALSE, 1.0, TRUE)
					IF IS_PED_IN_ANY_VEHICLE(piPlayerPed)
						NETWORK_EXPLODE_VEHICLE(veh, FALSE)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(veh)
					ENDIF
					
					SEND_PASS_THE_BOMB_ELIMINATED_TICKER()
					
					playerBDPassed[iLocalPart].iPTB_TimeSpentCarryingBomb = dmVarsPassed.iTimeCarryingBomb
					PRINTLN("[PTB](1) Updating iPTB_TimeSpentCarryingBomb to ", dmVarsPassed.iTimeCarryingBomb)
					
					IF NOT IS_TEAM_DEATHMATCH()
						PRINTLN("[PASSTHEBOMB][PBAnnouncer] Playing g_iAA_PlaySound_PassTheBomb_Elimination")
						BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_Elimination, FALSE, DEFAULT, DEFAULT, TRUE, TRUE)
					ENDIF
					
					g_b_WastedShard_Eliminated_Not_Suicide = TRUE
					PRINTLN("[PASSTHEBOMB][PTBMAJOR] PROCESS_PASSTHEBOMB_CLIENT - Setting g_b_WastedShard_Eliminated_Not_Suicide to TRUE!") 
					SET_BIT(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_EXPLODED_FROM_BEING_TAGGED)
					
					PRINTLN("[PASSTHEBOMB][PTBMAJOR] PROCESS_PASSTHEBOMB_CLIENT - EXPLOSION!")
				ELSE
					g_b_WastedShard_Eliminated_Not_Suicide = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_NET_TIMER_STARTED(g_stPTBCooldownTimer)
			REINIT_NET_TIMER(g_stPTBCooldownTimer)
			PRINTLN("[PASSTHEBOMB][PTBMAJOR] - PROCESS_PASSTHEBOMB_CLIENT - Starting cooldown timer cos I don't have the bomb any more!")
		ENDIF
		
		PRINTLN("[TMSSND] dmVarsPassed.iTagBombCountdownSndID = ", dmVarsPassed.iTagBombCountdownSndID)
		PRINTLN("[TMSSND] HAS_SOUND_FINISHED(dmVarsPassed.iTagBombCountdownSndID) ")
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableBombSounds)
		AND bLocalPlayerOK
		AND HAS_NET_TIMER_STARTED(serverBDPassed.tdTaggedExplosionTimer)
		
			IF dmVarsPassed.iTagBombCountdownSndID = -1
				
				dmVarsPassed.iTagBombCountdownSndID = GET_SOUND_ID()
				
				IF dmVarsPassed.iTagBombCountdownSndID > -1
					PLAY_SOUND_FRONTEND(dmVarsPassed.iTagBombCountdownSndID, "Bomb_Countdown", "DLC_AW_PTB_Sounds", TRUE)
					PRINTLN("[PASSTHEBOMBSND] - PROCESS_PASSTHEBOMB_CLIENT - Starting Countdown sound! (1)")
				ELSE
					PRINTLN("[PASSTHEBOMBSND] - PROCESS_PASSTHEBOMB_CLIENT - iTagBombCountdownSndID is still -1")
				ENDIF
				
			ELSE
				IF HAS_SOUND_FINISHED(dmVarsPassed.iTagBombCountdownSndID)
					PLAY_SOUND_FRONTEND(dmVarsPassed.iTagBombCountdownSndID, "Bomb_Countdown", "DLC_AW_PTB_Sounds", TRUE)
					PRINTLN("[PASSTHEBOMBSND] - PROCESS_PASSTHEBOMB_CLIENT - Starting Countdown sound! (2)")
				ENDIF
				
				IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDPassed.tdTaggedExplosionTimer) > 0
					INT iMaxTime = GET_DM_TAG_EXPLOSION_TIMER_VALUE(GET_TAG_EXPLOSION_TIME(serverBDpassed)) * 1000
					FLOAT fIntensity = TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDPassed.tdTaggedExplosionTimer)) / TO_FLOAT(iMaxTime)
					
					PRINTLN("[PASSTHEBOMB SPAM][SND SPAM] - PROCESS_PASSTHEBOMB_CLIENT - Setting countdown intensity to ", fIntensity)
					SET_VARIABLE_ON_SOUND(dmVarsPassed.iTagBombCountdownSndID, "Ctrl", fIntensity)
				ENDIF
				
				PRINTLN("[TMSSND] in the else")
			ENDIF
		ELSE
			PRINTLN("[TMSSND] Stopping dmVarsPassed.iTagBombCountdownSndID")
			END_DM_SOUND(dmVarsPassed.iTagBombCountdownSndID)
		ENDIF
		
	ELSE
		
		//I'm not the tagged player - display different objective text and clean up VFX etc
		IF NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
			
			IF serverBDPassed.iPartTagged > -1
			AND dmVarsPassed.iTagBomb_PrevObjectiveTextPartIndex != serverBDPassed.iPartTagged
			AND NOT IS_PARTICIPANT_A_SPECTATOR(playerBDPassed, iLocalPart)
			
				IF playerBDPassed[serverBDPassed.iPartTagged].iTeam = playerBDPassed[iLocalPart].iTeam
				AND IS_TEAM_DEATHMATCH()
					//I'm on the same team as the bomb carrier!
					IF NOT Has_This_MP_Objective_Text_With_Player_Name_Been_Received("PTB_HLPPLYR", INT_TO_PLAYERINDEX(serverBDPassed.iPartTagged))
						STRING tl15 = "PTB_HLPPLYR_0"
						IF playerBDPassed[serverBDPassed.iPartTagged].iTeam = 1
							tl15 = "PTB_HLPPLYR_1"
						ENDIF
						Print_Objective_Text_With_Coloured_Player_Name(tl15, INT_TO_PLAYERINDEX(serverBDPassed.iPartTagged), GET_MY_TEAM_COLOUR_AS_HUDCOLOUR(playerBDPassed))
					ENDIF
				ELSE
					//The bomb carrier is an enemy!
					IF NOT Has_This_MP_Objective_Text_With_Player_Name_Been_Received("PTB_AVDPLYR", INT_TO_PLAYERINDEX(serverBDPassed.iPartTagged))
						Print_Objective_Text_With_Coloured_Player_Name("PTB_AVDPLYR", INT_TO_PLAYERINDEX(serverBDPassed.iPartTagged), GET_ENEMY_TEAM_COLOUR_AS_HUDCOLOUR(playerBDPassed))
					ENDIF
				ENDIF
				
				dmVarsPassed.iTagBomb_PrevObjectiveTextPartIndex = serverBDPassed.iPartTagged
				PRINTLN("[TMS][BMBFB][PTBMAJOR] Setting iTagBomb_PrevObjectiveTextPartIndex to ", dmVarsPassed.iTagBomb_PrevObjectiveTextPartIndex)
			ENDIF
		ELSE
			IF Has_This_MP_Objective_Text_Been_Received("PTB_HLPPLYR")
			OR Has_This_MP_Objective_Text_Been_Received("PTB_AVDPLYR")
			OR Has_This_MP_Objective_Text_Been_Received("I_AM_TAGGED")
				Clear_Any_Objective_Text_From_This_Script()
			ENDIF
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(g_stPTBCooldownTimer)
			RESET_NET_TIMER(g_stPTBCooldownTimer)
			PRINTLN("[PASSTHEBOMB][PTBMAJOR] - PROCESS_PASSTHEBOMB_CLIENT - Resetting cooldown timer!")
		ENDIF
		
		IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_BOMB_CARRIER_MODE_STARTED)
			INITIALISE_NON_BOMB_CARRIER_MODE(dmVarsPassed, playerBDPassed, piPlayerPed, veh)
		ENDIF
		
		IF IS_ENTITY_ALIVE(veh)
			PRINTLN("[Nitro] GET_VEHICLE_MOD: ", GET_VEHICLE_MOD(veh, MOD_ENGINEBAY2))
		ENDIF
			
		PRINTLN("[TMSSND] Stopping dmVarsPassed.iTagBombCountdownSndID")
		END_DM_SOUND(dmVarsPassed.iTagBombCountdownSndID)
	ENDIF
	
	// Display timer as long as the local player isn't dead
	PROCESS_PASS_THE_BOMB_HUD(serverBDpassed, playerBDPassed, bDisableTimer)
	
	IF IS_PED_DEAD_OR_DYING(piPlayerPed)
	OR IS_PARTICIPANT_A_SPECTATOR(playerBDPassed, iLocalPart)
		Clear_Any_Objective_Text_From_This_Script()
	ENDIF
	
	g_b_PTB_Timer_Up = HAS_TAG_BOMB_TIMER_NEARLY_EXPIRED(serverBDPassed)

	// Set the tagged player's blip to be a bomb sprite
	INT i = 0
	FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
			PED_INDEX piPed = GET_PLAYER_PED(piPlayer)
			
			BLIP_INDEX biBlip = GET_BLIP_FROM_ENTITY(piPed)
			
			IF serverBDPassed.iPartTagged = iLocalPart
			AND piPed != piPlayerPed
				IF IS_PARTICIPANT_VALID_TO_BE_TAGGED(i, serverBDPassed, playerBDPassed)
					PROCESS_PASSTHEBOMB_PASSING(i, veh, dmVarsPassed)
				ENDIF
			ENDIF
			
			IF biBlip != NULL
			
				IF dmVarsPassed.bsPrevBS = RADAR_TRACE_INVALID
				AND piPed != piPlayerPed
					dmVarsPassed.bsPrevBS = GET_BLIP_SPRITE(biBlip)
					PRINTLN("[PTB] Setting bsPrevBS to ", dmVarsPassed.bsPrevBS)
				ENDIF
	
				IF serverBDPassed.iPartTagged = i
				AND serverBDPassed.iPartTagged != iLocalPart
				AND GET_BLIP_SPRITE(biBlip) != RADAR_TRACE_PICKUP_BOMB
					SET_BLIP_SPRITE(biBlip, RADAR_TRACE_PICKUP_BOMB)
					
					IF IS_TEAM_DEATHMATCH()
						IF playerBDPassed[i].iTeam = 0
							SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_ORANGE)
						ELSE
							SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_PURPLE)
						ENDIF
					ELSE
						SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_RED)
					ENDIF
					
					SET_BLIP_NAME_TO_PLAYER_NAME(biBlip, piPlayer)
					SET_BLIP_SCALE(biBlip, 1.5)
					SHOW_HEADING_INDICATOR_ON_BLIP(biBlip, FALSE)
					SET_BLIP_PRIORITY(biBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
				ENDIF
				
				IF serverBDPassed.iPartTagged != i
					IF GET_BLIP_SPRITE(biBlip) = RADAR_TRACE_PICKUP_BOMB
						SET_BLIP_SPRITE(biBlip, dmVarsPassed.bsPrevBS)
						SET_BLIP_SCALE(biBlip, 1.0)
						
						IF IS_TEAM_DEATHMATCH()
							IF playerBDPassed[i].iTeam = 0
								SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_ORANGE)
							ELSE
								SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_PURPLE)
							ENDIF
						ELSE
							SET_BLIP_COLOUR(biBlip, BLIP_COLOUR_RED)
						ENDIF
						
						SHOW_HEADING_INDICATOR_ON_BLIP(biBlip, TRUE)
						SET_BLIP_PRIORITY(biBlip, BLIP_PRIORITY_HIGHEST_SPECIAL_LOW)
						PRINTLN("[PTB] Resetting blip for participant ", i, " since they are no longer the bomb carrier")
					ENDIF
				ENDIF
				
				SHOW_HEIGHT_ON_BLIP(biBlip, TRUE)
				SET_BLIP_SHORT_HEIGHT_THRESHOLD(biBlip, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

PROC PROCESS_PTB_ANNOUNCER_CLIPS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT& iPlayersLeft[], INT& iPlayersMax[])
	
	UNUSED_PARAMETER(playerBDPassed)
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	//ANNOUNCER
	IF IS_TEAM_DEATHMATCH()
		IF iPlayersLeft[0] = 1
		AND iPlayersMax[0] > 1
			IF GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS(serverBDpassed) <= 10.0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LTS_BombTimer10SecondsLastBomb, TRUE)
			ELSE
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LTS_Team1LastAlive, TRUE)
			ENDIF
		ENDIF
		
		IF iPlayersLeft[1] = 1
		AND iPlayersMax[1] > 1
			IF GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS(serverBDpassed) <= 10.0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LTS_BombTimer10SecondsLastBomb, TRUE)
			ELSE
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LTS_Team2LastAlive, TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCK_SET(g_iAA_PlaySound_PassTheBomb_LTS_Team1FirstBlood)
		AND NOT IS_ARENA_ANNOUNCER_BS_MODE_SPECIFIC_LOCK_SET(g_iAA_PlaySound_PassTheBomb_LTS_Team2FirstBlood)
			IF iPlayersLeft[0] < iPlayersMax[0]
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LTS_Team1FirstBlood, TRUE, FALSE, FALSE, TRUE)
			ENDIF
			IF iPlayersLeft[1] < iPlayersMax[1]
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LTS_Team2FirstBlood, TRUE, FALSE, FALSE, TRUE)
			ENDIF
		ENDIF
	ELSE
		IF serverBDpassed.iNumAlivePlayers = 2
		AND iPlayersMax[0] > 2
			IF GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS(serverBDpassed) <= 10.0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_BombTimer10SecondsLastBomb, TRUE)
			ELSE
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_LastTwoPlayers, TRUE)
			ENDIF
		ELSE
			IF GET_REMAINING_PTB_TIMER_TIME_IN_SECONDS(serverBDpassed) <= 10.0
				BROADCAST_SET_ARENA_ANNOUNCER_SOUND_BS(ci_Arena_Announcer_DataTypeForEvent_ModeSpecific, g_iAA_PlaySound_PassTheBomb_BombTimer10Seconds, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROC_DEATHMATCH_ARENA_ANNOUNCER_TRACKING(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	IF NOT g_bMissionClientGameStateRunning
	OR (NOT IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED() AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR())
		EXIT
	ENDIF
	UNUSED_PARAMETER(dmVarsPassed)
	
	IF HAS_DM_STARTED(serverBDPassed)	
		IF IS_ONLY_30_SECONDS_LEFT(serverBDpassed)
		AND NOT IS_ARENA_ANNOUNCER_BS_GENERAL_LOCK_SET(g_iAA_PlaySound_Generic_RoundAlmostOver)	
			SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_RoundAlmostOver, TRUE)
		ENDIF
		
	
		INT iTeamLoop
		INT iPart
		INT iPlayersLeft[FMMC_MAX_TEAMS]
		INT iPlayersMax[FMMC_MAX_TEAMS]
		
		INT iTeamsLeft = 0
		INT iTeamCountedBS = 0
		IF NOT HAS_SERVER_FINISHED(serverBDPassed)
			FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
					PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
					IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
					AND NOT IS_PLAYER_SCTV(piPlayer)
					AND playerBDPassed[iPart].eClientLogicStage >= CLIENT_MISSION_STAGE_FIGHTING
						IF playerBDPassed[iPart].iTeam > -1 AND playerBDPassed[iPart].iTeam < FMMC_MAX_TEAMS
							IF NOT IS_BIT_SET(playerBDPassed[iPart].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
							AND NOT IS_BIT_SET(playerBDPassed[iPart].iClientBitSet, CLIENT_BITSET_SENT_TO_SPECTATOR)
							AND NOT IS_PLAYER_USING_ACTIVE_ROAMING_SPECTATOR_MODE(piPlayer)
								iPlayersLeft[playerBDPassed[iPart].iTeam]++
								
								IF NOT IS_BIT_SET(iTeamCountedBS, playerBDPassed[iPart].iTeam)
								AND IS_TEAM_DEATHMATCH()
									iTeamsLeft++
									SET_BIT(iTeamCountedBS, playerBDPassed[iPart].iTeam)
								ENDIF
							ENDIF
							iPlayersMax[playerBDPassed[iPart].iTeam]++
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			FOR iTeamLoop = 0 TO serverBDpassed.iNumberOfTeams - 1
				
				PRINTLN("[PROC_DEATHMATCH_ARENA_ANNOUNCER_TRACKING] - iTeamLoop: ", iTeamLoop, " iPlayersMax: ", iPlayersMax[iTeamLoop], " iPlayersLeft: ", iPlayersLeft[iTeamLoop])
				
				IF iPlayersLeft[iTeamLoop] = iPlayersMax[iTeamLoop]/2
				AND iPlayersMax[iTeamLoop]/2 > 1
				AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] > 1
					SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_HalfPlayerRemaining, TRUE)
				ENDIF
				IF iPlayersLeft[iTeamLoop] = 1
				AND iPlayersMax[iTeamLoop] > 1
				AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] > 1
					SET_ARENA_ANNOUNCER_BS_GENERAL(g_iAA_PlaySound_Generic_LastPlayerRemaining, TRUE)
				ENDIF
				
				IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)				
					IF iPlayersMax[iTeamLoop] != iPlayersLeft[iTeamLoop]
					AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] > 1
					AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] != 0
						IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
							SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_Carnage_FirstBlood, TRUE)
						ELSE
							SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_Carnage_LTS_Team1Player1stDeath+iTeamLoop, TRUE)
						ENDIF
					ENDIF				
					
					IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
						IF iPlayersLeft[iTeamLoop] = 1
						AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] > 1
						AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] != 0
						AND iPlayersMax[iTeamLoop] > 1
							SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_Carnage_LTS_Team1LastAlive+iTeamLoop, TRUE)
						ENDIF
					ELSE
						IF iPlayersLeft[iTeamLoop] = 2
						AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] > 2
						AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] != 0
						AND iPlayersMax[iTeamLoop] > 2
							SET_ARENA_ANNOUNCER_BS_MODE_SPECIFIC(g_iAA_PlaySound_Carnage_Last2PlayersRemaining, TRUE)					
						ENDIF					
					ENDIF
				ENDIF
			ENDFOR			
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
				PROCESS_PTB_ANNOUNCER_CLIPS(serverBDpassed, playerBDPassed, iPlayersLeft, iPlayersMax)
			ENDIF
		ENDIF
		
		PROCESS_SHARED_ARENA_ANNOUNCER_TRACKING(dmVarsPassed.td_ArenaWarsBigAir, DEFAULT, iTeamsLeft)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Arena interior
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_ARENA_INTERIOR()
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		ENABLE_STADIUM_PROBES_THIS_FRAME(TRUE)
	ENDIF
ENDPROC





