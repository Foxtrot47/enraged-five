USING "net_deathmatch_impromptu.sch"
USING "net_deathmatch_KotH.sch"
USING "net_deathmatch_spawning.sch"
USING "net_deathmatch_arena.sch"
USING "net_deathmatch_pickups.sch"

PROC PRINT_FIRST_STRIKE_TICKER(SHARED_DM_VARIABLES &dmVarsPassed, INT iFirstKiller, BOOL bLocal)
	IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_FIRST_STRIKE_TICKER)
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		AND NOT CONTENT_IS_USING_ARENA()
		AND NOT IS_KING_OF_THE_HILL()
			// "First Kill!"
			IF bLocal
				PRINT_TICKER( "DM_TICK7")
			ELSE
				// "~a~~HUD_COLOUR_WHITE~ killed first!"
				PRINT_TICKER_WITH_PLAYER_NAME( "DM_TICK8", INT_TO_PLAYERINDEX(iFirstKiller))
			ENDIF
			SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_FIRST_STRIKE_TICKER)
		ELSE
			SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_FIRST_STRIKE_TICKER)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_FIRST_KILL_EVENT(SHARED_DM_VARIABLES &dmVarsPassed, INT iEventID)
	SCRIPT_EVENT_DATA_FIRST_KILL Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))	
		IF Event.iFirstKillPlayer = NATIVE_TO_INT(LocalPlayer)
			PRINT_FIRST_STRIKE_TICKER(dmVarsPassed, Event.iFirstKillPlayer, TRUE)
		ELSE
			PRINT_FIRST_STRIKE_TICKER(dmVarsPassed, Event.iFirstKillPlayer, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DM_FINISHED_EVENT(INT iEventID, SHARED_DM_VARIABLES &dmVarsPassed)
	SCRIPT_EVENT_DATA_DM_FINISH Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		PRINTLN("[RESPAWN_STATE_CHANGE] PROCESS_DM_FINISHED_EVENT ")
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_FINISH_EVENT_RECEIVED)
	ENDIF
ENDPROC

PROC PROCESS_REWARD_CASH_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_DM_REWARD_CASH Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))	
		IF Event.Details.FromPlayerIndex = LocalPlayer
			// "You collected the $~1~ reward from ~a~."
			PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("DM_B2", INT_TO_PLAYERINDEX(event.iDeadPlayer), Event.iCashCollected)
		ELSE
			// "~a~~HUD_COLOUR_WHITE~ collected the $~1~ reward from you."
			IF Event.iDeadPlayer = NATIVE_TO_INT(LocalPlayer)
				PRINT_TICKER_WITH_PLAYER_NAME_AND_INT("DM_B3", Event.Details.FromPlayerIndex, Event.iCashCollected)
				RESET_BOUNTY()				
			ELSE
				// " ~a~~HUD_COLOUR_WHITE~ collected the $~1~ reward from ~a~. "
				PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_INTS("DM_B1", Event.Details.FromPlayerIndex, INT_TO_PLAYERINDEX(event.iDeadPlayer), Event.iCashCollected)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_BULLSHARK_BIG_MESSAGE(PLAYER_INDEX playerId)
	SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_GENERIC_MIDSIZED, playerId, -1, "STRAP_BULL_C", "STRAP_BULL", GET_BIG_MESSAGE_COLOUR_FOR_PLAYER(playerId))
ENDPROC

PROC PROCESS_BULLSHARK_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_BULLSHARK_COLLECTED Event
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))	
		IF Event.Details.FromPlayerIndex <> LocalPlayer
			DO_BULLSHARK_BIG_MESSAGE(Event.Details.FromPlayerIndex)
			CLEANUP_DM_BRU_BOX()
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE(INT iEventID)
	PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Processing...")
	
	SCRIPT_EVENT_DATA_FMMC_ALPHA_CHANGE EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_ALPHA_CHANGE
						
			PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - iAlphaPlayer: ", EventData.iAlphaPlayer, " iVehicleAlpha", EventData.iAlphaVehicle, " iPart: ", EventData.iPart, " iNetVehIndex: ", EventData.iNetVehIndex)
			
			IF EventData.iPart > -1
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(EventData.iPart))
					PED_INDEX piPed =  GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(EventData.iPart)))
			
					IF NOT IS_PED_INJURED(piPed)
						IF EventData.iAlphaPlayer > -1	
							IF EventData.iAlphaPlayer = 255
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha ")
								RESET_ENTITY_ALPHA(piPed)
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha ")
								SET_ENTITY_ALPHA(piPed, EventData.iAlphaPlayer, FALSE)
							ENDIF
						ENDIF
						
						IF EventData.iAlphaVehicle > -1
							IF EventData.iAlphaVehicle = 255		
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha ")
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting Alpha veh")
									RESET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed))
								ENDIF
								IF EventData.iNetVehIndex > 0
									NETWORK_INDEX netVehID = INT_TO_NATIVE(NETWORK_INDEX, EventData.iNetVehIndex)
									IF NETWORK_DOES_NETWORK_ID_EXIST(netVehID)
										VEHICLE_INDEX vehToChange = NET_TO_VEH(netVehID)
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Resetting alpha veh (passed in)")
										RESET_ENTITY_ALPHA(vehToChange)
									ELSE
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Net id does not exist...")
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha ")							
								IF IS_PED_IN_ANY_VEHICLE(piPed)
								AND DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(piPed))
									PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha veh")
									SET_ENTITY_ALPHA(GET_VEHICLE_PED_IS_IN(piPed), EventData.iAlphaVehicle, FALSE)
								ENDIF
								IF EventData.iNetVehIndex > 0
									NETWORK_INDEX netVehID = INT_TO_NATIVE(NETWORK_INDEX, EventData.iNetVehIndex)
									IF NETWORK_DOES_NETWORK_ID_EXIST(netVehID)
										VEHICLE_INDEX vehToChange = NET_TO_VEH(netVehID)
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Setting alpha veh (passed in)")
										SET_ENTITY_ALPHA(vehToChange, EventData.iAlphaVehicle, FALSE)
									ELSE
										PRINTLN("[LM][PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE] - Net id does not exist...")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC PROCESS_EVENT_NETWORK_CONNECTION_TIMEOUT(SHARED_DM_VARIABLES &dmVarsPassed, INT iCount)
	STRUCT_NETWORK_CONNECTION_TIMEOUT sEventdata
	GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventdata, SIZE_OF(sEventdata))
	PLAYER_INDEX playerId = INT_TO_NATIVE(PLAYER_INDEX, sEventdata.m_PlayerIndex)
	IF NETWORK_IS_PLAYER_ACTIVE(playerId)
		IF playerId = NETWORK_GET_HOST_PLAYER_INDEX()
			IF NOT dmVarsPassed.sSplitSession.bRaceWaitForNetworkSessionEventForHostTimeout
				dmVarsPassed.sSplitSession.bRaceWaitForNetworkSessionEventForHostTimeout = TRUE
				PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - PROCESS_EVENT_NETWORK_CONNECTION_TIMEOUT = [connection timeout] - received from session host player, set bRaceWaitForNetworkSessionEventForHostTimeout = TRUE")
			ENDIF
		ENDIF
	ENDIF
	IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.sSplitSession.stRaceNetworkConnectionTimeoutTimer)
		START_NET_TIMER(dmVarsPassed.sSplitSession.stRaceNetworkConnectionTimeoutTimer)
		dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent = TO_FLOAT(NETWORK_GET_TOTAL_NUM_PLAYERS())
		dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent -= 1
		PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - PROCESS_EVENT_NETWORK_CONNECTION_TIMEOUT = [connection timeout] - started timer stRaceNetworkConnectionTimeoutTimer")
		PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - PROCESS_EVENT_NETWORK_CONNECTION_TIMEOUT = [connection timeout] - NETWORK_GET_TOTAL_NUM_PLAYERS() (minus local player) on first event received = ", dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent)
	ENDIF
	dmVarsPassed.sSplitSession.fRaceNetworkConnectionTimeoutTally++
	PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - PROCESS_EVENT_NETWORK_CONNECTION_TIMEOUT = [connection timeout] - fRaceNetworkConnectionTimeoutTally = ", dmVarsPassed.sSplitSession.fRaceNetworkConnectionTimeoutTally)
ENDPROC

PROC RESET_RACE_NETWORK_CONNECTION_TIMEOUT_CHECKS_TIMER_DATA(SHARED_DM_VARIABLES &dmVarsPassed)
	dmVarsPassed.sSplitSession.fRaceNetworkConnectionTimeoutTally = 0
	dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent = 0
	SCRIPT_TIMER tempTimer
	dmVarsPassed.sSplitSession.stRaceNetworkConnectionTimeoutTimer = tempTimer
	PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - RESET_RACE_NETWORK_CONNECTION_TIMEOUT_CHECKS_TIMER_DATA = [connection timeout] - stRaceNetworkConnectionTimeoutTimer reset")
ENDPROC

FUNC FLOAT RACE_GET_NUM_RECEIVED_TIMEOUT_EVENTS_AS_PERCENTAGE_OF_SESSION_SIZE_ON_FIRST_EVENT(SHARED_DM_VARIABLES &dmVarsPassed)
	PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - RACE_GET_NUM_RECEIVED_TIMEOUT_EVENTS_AS_PERCENTAGE_OF_SESSION_SIZE_ON_FIRST_EVENT dmVarsPassed.sSplitSession.fRaceNetworkConnectionTimeoutTally = ", dmVarsPassed.sSplitSession.fRaceNetworkConnectionTimeoutTally)
	PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - RACE_GET_NUM_RECEIVED_TIMEOUT_EVENTS_AS_PERCENTAGE_OF_SESSION_SIZE_ON_FIRST_EVENT dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent = ", dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent)
	RETURN (dmVarsPassed.sSplitSession.fRaceNetworkConnectionTimeoutTally/dmVarsPassed.sSplitSession.fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent)*100
ENDFUNC

PROC MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS(SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF HAS_NET_TIMER_STARTED(dmVarsPassed.sSplitSession.stRaceNetworkConnectionTimeoutTimer)
		IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sSplitSession.stRaceNetworkConnectionTimeoutTimer, 1000)
			PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS = [connection timeout] - stRaceNetworkConnectionTimeoutTimer expired (25000)")
			IF NETWORK_GET_TOTAL_NUM_PLAYERS() <= 1
				PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS = [connection timeout] - NETWORK_GET_TOTAL_NUM_PLAYERS() <= 1")
				FLOAT fPercentagewithTimeoutEvents = RACE_GET_NUM_RECEIVED_TIMEOUT_EVENTS_AS_PERCENTAGE_OF_SESSION_SIZE_ON_FIRST_EVENT(dmVarsPassed)
				PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] - MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS = [connection timeout] - fPercentagewithTimeoutEvents = ", fPercentagewithTimeoutEvents)
				IF fPercentagewithTimeoutEvents >= 90.0
					IF NOT dmVarsPassed.sSplitSession.bSplitSession
						PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] MAINTAIN_RACE_EVENT_NETWORK_CONNECTION_TIMEOUTS - success ")
						dmVarsPassed.sSplitSession.bSplitSession = TRUE
					ENDIF
				ENDIF
			ENDIF
			RESET_RACE_NETWORK_CONNECTION_TIMEOUT_CHECKS_TIMER_DATA(dmVarsPassed)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_EVENT_NETWORK_SESSION_EVENT(SHARED_DM_VARIABLES &dmVarsPassed, INT iCount)
	
	PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] PROCESS_EVENT_NETWORK_SESSION_EVENT - received NETWORK SESSION EVENT:")
	
	STRUCT_SESSION_EVENT structSessionEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, structSessionEventData, SIZE_OF(structSessionEventData))
		
		PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] PROCESS_EVENT_NETWORK_SESSION_EVENT - structSessionEventData.nEventParam", structSessionEventData.nEventParam)
		
		IF structSessionEventData.nEventID = EVENT_SESSION_MIGRATE_END
		
			PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] PROCESS_EVENT_NETWORK_SESSION_EVENT - [VEH_EXPORT] - PROCESS_NET_EVENTS = [connection timeout] - SESSION_EVENT_TYPE = EVENT_SESSION_MIGRATE_END")
			PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] PROCESS_EVENT_NETWORK_SESSION_EVENT - [VEH_EXPORT] - PROCESS_NET_EVENTS = [connection timeout] - structSessionEventData.nEventParam = ", structSessionEventData.nEventParam)
			
			IF structSessionEventData.nEventParam = 1
				
				IF dmVarsPassed.sSplitSession.bRaceWaitForNetworkSessionEventForHostTimeout
					PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION]  - PROCESS_NET_EVENTS = [connection timeout] - dmVarsPassed.bRaceWaitForNetworkSessionEventForHostTimeout = TRUE, calling SET_EXPORT_ENTITY_ON_FIRE_FOR_SESSION_SPLIT_IF_REQUIRED")
					IF NOT dmVarsPassed.sSplitSession.bSplitSession
						PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] PROCESS_EVENT_NETWORK_SESSION_EVENT - success ")
						dmVarsPassed.sSplitSession.bSplitSession = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			IF dmVarsPassed.sSplitSession.bRaceWaitForNetworkSessionEventForHostTimeout
				dmVarsPassed.sSplitSession.bRaceWaitForNetworkSessionEventForHostTimeout = FALSE
				PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION]  - PROCESS_NET_EVENTS = [connection timeout] - set dmVarsPassed.bRaceWaitForNetworkSessionEventForHostTimeout = FALSE")
			ENDIF
			
		ENDIF
	
	#IF IS_DEBUG_BUILD
	ELSE
		SCRIPT_ASSERT("PROCESS_EVENT_NETWORK_SESSION_EVENT - could not retrieve data.")
	#ENDIF // #IF IS_DEBUG_BUILD
	ENDIF
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Damage Event
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC INT GET_PLAYER_MODIFIERS__SCORING_KILL(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, TARGET_FLOATING_SCORE &sTargetFloatingScores[], ServerBroadcastData &serverBDPassed, ServerBroadcastData_Leaderboard &serverBDPassedLDB, INT iCurrentModset, PlayerBroadcastData &playerBDPassed[])

	INT iTotalScore
	
	WEAPON_TYPE wtWeapon = INT_TO_ENUM(WEAPON_TYPE, sDamageEvent.WeaponUsed)

	PED_INDEX victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sDamageEvent.VictimIndex)
	PLAYER_INDEX victimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(victimPed)
	INT iVictimTeam = GET_PLAYER_TEAM(victimPlayerID)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Invalid modset")
		RETURN iTotalScore
	ENDIF
	
	IF NOT sDamageEvent.VictimDestroyed
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Victim Not Destroyed. No Score.")
		RETURN iTotalScore
	ENDIF
	
	// Kill Score
	iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iKill
	PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Base Kill. Total Score: ", iTotalScore)
	
	// Melee Bonus
	IF sDamageEvent.IsWithMeleeWeapon
		iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierScoring.iMelee
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Melee Kill. Total Score: ", iTotalScore)
	ELSE
		
		// Headshot Bonus
		IF sDamageEvent.IsHeadShot
			iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iHeadshot
			PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Headshot Kill. Total Score: ", iTotalScore)
		ENDIF
		
		// Vehicle Bonus
		IF IS_ENTITY_DEAD(victimPed)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(victimPed)
			OR wtWeapon = WEAPONTYPE_RUNOVERBYVEHICLE
				iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iVehicle
				PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Vehicle Kill. Total Score: ", iTotalScore)
			ENDIF
		ENDIF
		
		// Specific Weapon Group Bonus
		iTotalScore += GET_PLAYER_MODIFIERS_SCORING_WEAPON_GROUP_SCORE(iCurrentModset, wtWeapon)
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Specific Weapon Group Kill. Weapon: ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(wtWeapon), " Total Score: ", iTotalScore)
		
	ENDIF
	
	// First Place
	IF IS_PLAYER_WINNING_OUTRIGHT(victimPlayerID, serverBDPassed, serverBDPassedLDB, playerBDPassed)
		iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iPositionFirst
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - First Place Kill. Team: ", iVictimTeam, " Total Score: ", iTotalScore)
	ENDIF
	
	// Last Place
	IF IS_PLAYER_LOSING_OUTRIGHT(victimPlayerID, serverBDPassed, serverBDPassedLDB, playerBDPassed)
		iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iPositionLast
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Last Place Kill. Team: ", iVictimTeam, " Total Score: ", iTotalScore)
	ENDIF
	
	// Specific Team Bonus
	IF iVictimTeam >= 0 AND iVictimTeam <= g_FMMC_STRUCT.iMaxNumberOfTeams
		iTotalScore += g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].sModifierScoring.iSpecificTeam[iVictimTeam]
		PRINTLN("[SCR_MODS] GET_PLAYER_MODIFIERS__SCORING_KILL - Specific Team Kill. Team: ", iVictimTeam, " Total Score: ", iTotalScore)
	ENDIF

	PROCESS_CAP_INDIVIDUAL_TDM_SCORE_AT_ZERO(iTotalScore)
	PROCESS_SCORING_MODIFIERS_ADD_FLOATING_SCORE(sTargetFloatingScores, iTotalScore)
	
	RETURN iTotalScore
	
ENDFUNC

PROC PROCESS_DM_GIVE_ASSIST_AWARD(PED_INDEX victimPed)
	
	INT iAssistedDamage
	
	IF NETWORK_GET_ASSISTED_DAMAGE_OF_ENTITY(LocalPlayer, victimPed, iAssistedDamage)
			
		PRINTLN("MP_AWARD_FM_DM_STOLENKILL, iAssistedDamage = ", iAssistedDamage)
		
		IF iAssistedDamage <= MIN_STOLEN_DAMAGE
			
			PRINTLN("MP_AWARD_FM_DM_STOLENKILL, ASSIST AWARD ")
			
			IF SHOULD_PROGRESS_DM_AWARDS()
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_STOLENKILL, 1)
			ENDIF
			
		ENDIF
		
	ENDIF 

ENDPROC

PROC PROCESS_DM_GIVE_BOUNTY_CASH(INT iKillerPlayerID, INT iVictimPlayerID)

	IF NOT HAS_PLAYER_COLLECTED_DM_BOUNTY(iKillerPlayerID)
		
		INT iCashBounty = GlobalplayerBD_FM[iVictimPlayerID].iPlayerBounty
		PRINTLN("[CS_BOUNTY] BEFORE iCashBounty = ", iCashBounty)
		iCashBounty = MULTIPLY_CASH_BY_TUNABLE(iCashBounty)
		
		IF iCashBounty > 0
		
			FLOAT fBounty = TO_FLOAT(iCashBounty)
			iCashBounty =  ROUND(fBounty)
			
			IF USE_SERVER_TRANSACTIONS()
				INT iTransactionID
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_DEATHMATCH_BOUNTY, iCashBounty, iTransactionID)
			ELSE
				NETWORK_EARN_FROM_PICKUP(iCashBounty)
				GIVE_LOCAL_PLAYER_CASH(iCashBounty)
			ENDIF
			
			// Send event
			BROADCAST_DM_REWARD_CASH(iVictimplayerID, iCashBounty)
			PRINTLN("[CS_BOUNTY] SUCCESS iCashBounty = ", iCashBounty)
			SET_BIT(GlobalplayerBD_FM[iKillerplayerID].iDeathmatchBitset, COLLECTED_BOUNTY_THIS_DM)
		
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_DM_BOSSVBOSS(PLAYER_INDEX victimPlayerID)

	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		IF GB_IS_PLAYER_BOSS_OF_A_GANG(victimPlayerID)
			PRINTLN("[BOSSVBOSS] KILL EVENT! I KILLED BOSS ", GET_PLAYER_NAME(victimPlayerID))
			GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
		ELSE
			IF IS_PLAYER_ON_BIKER_DM()
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(victimPlayerID)
					PRINTLN("[BOSSVBOSS] KILL EVENT - BIKER DM! I KILLED PLAYER ", GET_PLAYER_NAME(victimPlayerID))
					GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL IS_VICTIM_ON_KILLERS_TEAM(PLAYER_INDEX killerPlayerID, PLAYER_INDEX victimPlayerID)

	IF GET_PLAYER_TEAM(killerPlayerID) = -1
	OR GET_PLAYER_TEAM(victimPlayerID) = -1
		RETURN FALSE
	ENDIF
	
	IF GET_PLAYER_TEAM(killerPlayerID) = GET_PLAYER_TEAM(victimPlayerID) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_PROCESSED_EVENT_FOR_THIS_PLAYER_BITSET(INT iVictimPlayerId)
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[iVictimplayerID].iVehDeathmatchKillBitSet, iVictimPlayerId)
			PRINTLN("[CS_VEH_DM] SET_PROCESSED_EVENT_FOR_THIS_PLAYER_BITSET, iVictimplayerID = ", iVictimplayerID)
			SET_BIT(GlobalplayerBD_FM[iVictimplayerID].iVehDeathmatchKillBitSet, iVictimPlayerId)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OK_TO_CHECK_EVENT_FOR_DEAD_PLAYER(INT iVictimPlayerId)
	IF IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
	AND NOT IS_ANY_SPAWN_VEHICLE_MODIFIER_SET_FOR_DM()
		IF IS_BIT_SET(GlobalplayerBD_FM[iVictimplayerID].iVehDeathmatchKillBitSet, iVictimPlayerId)
			PRINTLN("OK_TO_CHECK_EVENT_FOR_DEAD_PLAYER - Returning FALSE")
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC PROCESS_LAST_PLAYER_I_DAMAGED(INT iVictimPlayerID, PED_INDEX &killerPed, PLAYER_INDEX &killerPlayerID, INT &iKillerPlayerID)
		
	PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_PLAYER_I_DAMAGED_CHECK - Checking for last player I damaged.")

	// If the victim is the last player I damaged, then attribute the kill to me
	IF iVictimPlayerID = g_iLastPlayerIDamaged
	
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_PLAYER_I_DAMAGED_CHECK - Local player was the last player to damage the victim. Victim ID: ", g_iLastPlayerIDamaged, ". Checking if within time limit.")

		// If within time limit
		IF (GET_GAME_TIMER() - g_iLastPlayerIDamagedTimeStamp) < ciLastDamagerTimeLimit
		
			PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_PLAYER_I_DAMAGED_CHECK - Within time limit. Local player was the last damager of the victim. Attributing kill to local player.")
			MPGlobals.g_KillStrip.piOverRideKillerPlayer = LocalPlayer
			
			iKillerPlayerID = NATIVE_TO_INT(LocalPlayer)
			killerPlayerID = LocalPlayer
			killerPed = LocalPlayerPed

		ELSE
			PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_PLAYER_I_DAMAGED_CHECK - Time limit expired.")
		ENDIF
	
	ELSE
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_PLAYER_I_DAMAGED_CHECK - I was not the last damager. Last Player I Damaged: ", g_iLastPlayerIDamaged)
	ENDIF
	
ENDPROC

PROC PROCESS_LAST_DAMAGER(PED_INDEX &killerPed, PLAYER_INDEX &killerPlayerID, INT &iKillerPlayerID)

	PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_DAMAGER_CHECK - Checking for last damager.")
	
	// If the local player has a last damager
	IF g_iLastDamagerPlayer != -1
	
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_DAMAGER_CHECK - Local player has a last damager. Last Damager ID: ", g_iLastDamagerPlayer, ". Checking if within time limit.")

		// If within time limit
		IF (GET_GAME_TIMER() - g_iLastDamagerTimeStamp) < ciLastDamagerTimeLimit
		
			PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_DAMAGER_CHECK - Within time limit. Local player was killed by Player ", g_iLastDamagerPlayer, ". Attributing kill to Player ", g_iLastDamagerPlayer)
			MPGlobals.g_KillStrip.piOverRideKillerPlayer = INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer)
			
			iKillerPlayerID = g_iLastDamagerPlayer
			killerPlayerID = INT_TO_NATIVE(PLAYER_INDEX, iKillerPlayerID)
			killerPed = GET_PLAYER_PED(killerPlayerID)

		ELSE
			PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_DAMAGER_CHECK - Time limit expired.")
		ENDIF
		
	ELSE
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_LAST_DAMAGER_CHECK - No last damager.")
	ENDIF

ENDPROC

FUNC BOOL CHECK_HEADSHOT(PED_INDEX pedID)
	
	IF NOT DOES_ENTITY_EXIST(pedID)
		RETURN FALSE
	ENDIF
	
	PED_BONETAG ePedBonetag = BONETAG_NULL
	
	IF NOT GET_PED_LAST_DAMAGE_BONE(pedID, ePedBonetag)	
		PRINTLN("CHECK_HEADSHOT - invalid bone")
		RETURN FALSE
	ENDIF

	RETURN ePedBonetag = BONETAG_HEAD OR ePedBonetag = BONETAG_NECK
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLAYER_MODIFIER_HELP_TEXT_ATTACKER(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, INT iVictimCurrentModSet, PED_INDEX VictimPed)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iVictimCurrentModSet)
		EXIT
	ENDIF
		
	IF NOT sDamageEvent.VictimDestroyed
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iVictimCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockHeadshots)
		AND CHECK_HEADSHOT(VictimPed)
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_NO_HEADSHOTS)
		ENDIF
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iVictimCurrentModSet].iHealthScale > ciDM_HEALTH_MODIFIER_DEFAULT * 2
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_HIGH_HEALTH)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_PLAYER_MODIFIER_HELP_TEXT_VICTIM(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, INT iVictimCurrentModSet, PED_INDEX VictimPed)
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iVictimCurrentModSet)
		EXIT
	ENDIF
	
	IF NOT sDamageEvent.VictimDestroyed
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iVictimCurrentModSet].iModifierSetBS, ciDM_ModifierBS_BlockHeadshots)
		AND CHECK_HEADSHOT(VictimPed)
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_NO_HEADSHOTS_VIC)
		ENDIF
		IF g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iVictimCurrentModSet].iHealthScale > ciDM_HEALTH_MODIFIER_DEFAULT * 2
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_HIGH_HEALTH_VIC)
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_NETWORK_KILL_EVENT(
		STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, 
		SHARED_DM_VARIABLES &dmVarsPassed, 
		ServerBroadcastData &serverBDPassed, 
		ServerBroadcastData_Leaderboard &serverBDPassedLDB, 
		PlayerBroadcastData &playerBDPassed[], 
		PLAYER_INDEX killerPlayerID, 
		INT iKillerPlayerID, 
		PED_INDEX victimPed, 
		PLAYER_INDEX victimPlayerID, 
		INT iVictimPlayerID
	)
	
	IF g_bCompKillTickers
	AND NOT g_bNoDeathTickers
		
		// If local player is the killer
		IF (killerPlayerID = LocalPlayer)
			
			// If local player is not the victim
			IF (victimPlayerID != LocalPlayer)
				
				// Player got a kill
				PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_KILL_EVENT - Player got a kill")
				
				IF NOT IS_VICTIM_ON_KILLERS_TEAM(killerPlayerID, victimPlayerID)
				
					SET_PROCESSED_EVENT_FOR_THIS_PLAYER_BITSET(iVictimPlayerID)
				
					IF IS_BIT_SET(GlobalplayerBD_FM[iVictimPlayerID].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)
					
						// Player killed enemy
						PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_KILL_EVENT - Player killed an enemy player!")
						
						IF NOT IS_KING_OF_THE_HILL()
						 
							IF NOT IS_THIS_LEGACY_DM_CONTENT()
							AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
							AND NOT IS_PLAYER_ON_BIKER_DM()
							AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
								
								INT iCurrentModset = GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed)
								INT iScore = GET_PLAYER_MODIFIERS__SCORING_KILL(sDamageEvent, dmVarsPassed.sTargetFloatingScores, serverBDPassed, serverBDPassedLDB, iCurrentModset, playerBDPassed)
								
								INCREMENT_DEATHMATCH_SCORE(iVictimPlayerID, iScore)
								PROCESS_CUSTOM_DM_ON_KILL(sDamageEvent, dmVarsPassed.sTargetFloatingScores, playerBDPassed, dmVarsPassed.sPlayerModifierData)
								
							ELSE
								INCREMENT_DEATHMATCH_SCORE(iVictimPlayerID)
							ENDIF
							
						ENDIF
						
						DO_DEATHMATCH_FIRST_KILLER(iKillerPlayerID)
						PROCESS_DM_GIVE_ASSIST_AWARD(victimPed)
						PROCESS_DM_GIVE_BOUNTY_CASH(iKillerPlayerID, iVictimPlayerID)
						PROCESS_DM_BOSSVBOSS(victimPlayerID)
						
					ENDIF
				
				ELSE
				
					// Player killed friendly
					PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_KILL_EVENT - Player killed another player on their team!")
					INT iCurrentModset = GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed)
					INT iScore = GET_PLAYER_MODIFIERS__SCORING_FRIENDLY_KILL(dmVarsPassed.sTargetFloatingScores, iCurrentModset)
					INCREMENT_DEATHMATCH_SCORE(iVictimPlayerID, iScore)
					
					IF iScore > 0
						SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_FRIENDLY_SCORE)
						DO_DEATHMATCH_FIRST_KILLER(iKillerPlayerID)
					ENDIF
					
					IF IS_CURRENT_MODIFIER_SET_VALID(iCurrentModset)
					AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModset].iModifierSetBS, ciDM_ModifierBS_OnKillAwardFriendlyFire)
						PROCESS_CUSTOM_DM_ON_KILL(sDamageEvent, dmVarsPassed.sTargetFloatingScores, playerBDPassed, dmVarsPassed.sPlayerModifierData)
					ENDIF
				
				ENDIF
		
			ELSE
			
				INT iBoundsIndex = GET_CURRENT_DM_BOUNDS_INDEX(dmVarsPassed.sPlayerModifierData)
				
				// If the player was not killed by the bounds
				IF NOT WAS_PLAYER_KILLED_BY_DEATHMATCH_BOUNDS(dmVarsPassed.sDeathmatchBoundsData, iBoundsIndex, victimPed)

					// Player commit suicide
					PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_KILL_EVENT - Player commit suicide")
					INT iCurrentModset = GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed)
					INT iScore = GET_PLAYER_MODIFIERS__SCORING_SUICIDE(dmVarsPassed.sTargetFloatingScores, iCurrentModSet)
					INCREMENT_DEATHMATCH_SCORE(iVictimPlayerID, iScore)
					
					IF iScore > 0
						SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_SUICIDE_SCORE)
					ENDIF
					
				ENDIF
				
			ENDIF
	
		ELSE
		
			// If local player is the victim
			IF (victimPlayerID = LocalPlayer)
				
				// Player died
				PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_KILL_EVENT - Player died")

			ENDIF
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_NETWORK_HIT_EVENT(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[], PLAYER_INDEX killerPlayerID, PLAYER_INDEX victimPlayerID, INT iVictimPlayerID)

	IF g_bCompKillTickers
	AND NOT g_bNoDeathTickers
		
		// If local player is the damager
		IF (killerPlayerID = LocalPlayer)
			
			// If local player is not the victim
			IF (victimPlayerID != LocalPlayer)
				
				// Player inflicted damaged
				PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_HIT_EVENT - Player inflicted damage on another player!")
				
				IF NOT IS_VICTIM_ON_KILLERS_TEAM(killerPlayerID, victimPlayerID)
				
					IF IS_BIT_SET(GlobalplayerBD_FM[iVictimplayerID].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)
					
						// Player damaged enemy
						PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_HIT_EVENT - Player damaged an enemy player!")
						
						IF NOT IS_KING_OF_THE_HILL()
						 
							IF NOT IS_THIS_LEGACY_DM_CONTENT()
							AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
							AND NOT IS_PLAYER_ON_BIKER_DM()
							AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
								PROCESS_CUSTOM_DM_ON_HIT(sDamageEvent, dmVarsPassed.sTargetFloatingScores, playerBDPassed, dmVarsPassed.sPlayerModifierData)
							ENDIF
							
						ENDIF
					ENDIF
				
				ELSE
				
					// Player damaged friendly
					PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_HIT_EVENT - Player damaged another player on their team!")
					IF IS_CURRENT_MODIFIER_SET_VALID(GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed))
					AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed)].iModifierSetBS, ciDM_ModifierBS_OnHitAwardFriendlyFire)
						PROCESS_CUSTOM_DM_ON_HIT(sDamageEvent, dmVarsPassed.sTargetFloatingScores, playerBDPassed, dmVarsPassed.sPlayerModifierData)
					ENDIF
				
				ENDIF
		
			ELSE

				// Player damaged themself
				PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_HIT_EVENT - Player inflicted damage on themself!")
				
			ENDIF
	
		ELSE
		
			// If local player is the victim
			IF (victimPlayerID = LocalPlayer)
				
				// Player damaged
				PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_HIT_EVENT - Player damaged by an enemy player!")

			ENDIF
			
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_NETWORK_DAMAGE_ENTITY_EVENT(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData &serverBDPassed, ServerBroadcastData_Leaderboard &serverBDPassedLDB, PlayerBroadcastData &playerBDPassed[])

	PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_DAMAGE_ENTITY_EVENT - Processing network damage event")
	
	PED_INDEX victimPed, killerPed
	PLAYER_INDEX victimPlayerID = INVALID_PLAYER_INDEX()
	PLAYER_INDEX killerPlayerID = INVALID_PLAYER_INDEX()
	INT iVictimPlayerID, iKillerPlayerID
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_DAMAGE_ENTITY_EVENT - sDamageEvent.VictimDestroyed = ", PICK_STRING(sDamageEvent.VictimDestroyed, "TRUE", "FALSE"))
	#ENDIF
	
	// Victim checks
	IF DOES_ENTITY_EXIST(sDamageEvent.VictimIndex)
		IF IS_ENTITY_A_PED(sDamageEvent.VictimIndex)
			victimPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sDamageEvent.VictimIndex)
			IF IS_PED_A_PLAYER(victimPed)
				victimPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(victimPed)
			ENDIF
		ENDIF
	ENDIF
	
	IF victimPlayerID = INVALID_PLAYER_INDEX()
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_DAMAGE_ENTITY_EVENT - Invalid victimPlayerID")
		EXIT
	ENDIF
	
	iVictimPlayerID = NATIVE_TO_INT(victimPlayerID)
	
	IF NOT DOES_ENTITY_EXIST(sDamageEvent.DamagerIndex)
	OR NATIVE_TO_INT(sDamageEvent.DamagerIndex) = -1
		
		INT iWeaponUsed = sDamageEvent.WeaponUsed
		
		// Treat Unknown Deaths as Suicide
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_TreatUnknownDeathsAsSuicide)
			IF iWeaponUsed = HASH("WEAPON_FALL")
			OR iWeaponUsed = HASH("WEAPON_DROWNING")
			OR iWeaponUsed = HASH("WEAPON_DROWNING_IN_VEHICLE")
			OR iWeaponUsed = HASH("WEAPON_INVALID")
				sDamageEvent.DamagerIndex = sDamageEvent.VictimIndex
			ENDIF	
		ENDIF
		
	ENDIF
	
	// Killer checks
	IF DOES_ENTITY_EXIST(sDamageEvent.DamagerIndex)
		IF IS_ENTITY_A_PED(sDamageEvent.DamagerIndex)
			killerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(sDamageEvent.DamagerIndex)
			IF IS_PED_A_PLAYER(killerPed)
				killerPlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)	
			ENDIF
		ENDIF
	ENDIF
		
	// Last damager checks
	IF sDamageEvent.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_FALL)
	OR sDamageEvent.WeaponUsed = ENUM_TO_INT(WEAPONTYPE_EXPLOSION)
		
		// If the player is not victim and not the killer, check if they were the last damager of the victim
		IF victimPlayerID != LocalPlayer
		AND killerPlayerID != LocalPlayer
			
			IF bLocalPlayerPedOK
				PROCESS_LAST_PLAYER_I_DAMAGED(iVictimPlayerID, killerPed, killerPlayerID, iKillerPlayerID)
			ENDIF
		ENDIF
		
		// If the player is the victim, and if they are the killer (i.e. suicide), or if there is no killer, check if there was a last damager
		IF victimPlayerID = LocalPlayer
		
			IF killerPlayerID = LocalPlayer
			OR killerPlayerID = INVALID_PLAYER_INDEX()
				PROCESS_LAST_DAMAGER(killerPed, killerPlayerID, iKillerPlayerID)
			ENDIF
			
		ENDIF

	ENDIF
	
	IF killerPlayerID = INVALID_PLAYER_INDEX()
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_DAMAGE_ENTITY_EVENT - Invalid killerPlayerID")
		EXIT
	ENDIF
	
	iKillerPlayerID = NATIVE_TO_INT(killerPlayerID)
	
	IF iVictimPlayerID != -1
		IF killerPlayerID = LocalPlayer
			PROCESS_PLAYER_MODIFIER_HELP_TEXT_ATTACKER(sDamageEvent, playerBDPassed[iVictimPlayerID].iCurrentModSet, victimPed)
		ELSE
			PROCESS_PLAYER_MODIFIER_HELP_TEXT_VICTIM(sDamageEvent, playerBDPassed[iVictimPlayerID].iCurrentModSet, victimPed)
		ENDIF
	ENDIF
	
	IF OK_TO_CHECK_EVENT_FOR_DEAD_PLAYER(iVictimPlayerID)
	
		// Hit event
		PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_DAMAGE_ENTITY_EVENT - Processing network hit event")
		PROCESS_NETWORK_HIT_EVENT(sDamageEvent, dmVarsPassed, playerBDPassed, killerPlayerID, victimPlayerID, iVictimPlayerID)
		
		IF sDamageEvent.VictimDestroyed
		
			// Kill event
			PRINTLN("[DM_DAMAGE_EVENT] PROCESS_NETWORK_DAMAGE_ENTITY_EVENT - Processing network kill event")
			PROCESS_NETWORK_KILL_EVENT(sDamageEvent, dmVarsPassed, serverBDPassed, serverBDPassedLDB, playerBDPassed, killerPlayerID, iKillerPlayerID, victimPed, victimPlayerID, iVictimPlayerID)	
		
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROCESS_EVENT_NETWORK_DAMAGE_ENTITY(SHARED_DM_VARIABLES &dmVarsPassed, INT iCount, ServerBroadcastData &serverBDPassed, ServerBroadcastData_Leaderboard &serverBDPassedLDB, PlayerBroadcastData &playerBDPassed[])
	
	STRUCT_ENTITY_DAMAGE_EVENT EventData
	
	PRINTLN("[DM_DAMAGE_EVENT] - PROCESS_EVENT_NETWORK_DAMAGE_ENTITY")
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	
		PRINTLN("PROCESS_EVENT_NETWORK_DAMAGE_ENTITY - Network damage entity event received")
		PROCESS_NETWORK_DAMAGE_ENTITY_EVENT(EventData, dmVarsPassed, serverBDPassed, serverBDPassedLDB, playerBDPassed)
		
	ENDIF
	
ENDPROC

PROC PROCESS_SCRIPT_EVENT_DM_PLAYER_BLIP(INT iCount)

	SCRIPT_EVENT_DATA_DM_PLAYER_BLIP EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	
		IF NOT NETWORK_IS_PLAYER_ACTIVE(EventData.Details.FromPlayerIndex)
			PRINTLN("[DM_BLIP_EVENT] PROCESS_SCRIPT_EVENT_DM_PLAYER_BLIP - Player not active!")
			EXIT
		ENDIF
		
		PRINTLN("[DM_BLIP_EVENT] PROCESS_SCRIPT_EVENT_DM_PLAYER_BLIP - Script deathmatch player blip event received!")
		PRINTLN("[DM_BLIP_EVENT] PROCESS_SCRIPT_EVENT_DM_PLAYER_BLIP - Blipping player ", NATIVE_TO_INT(EventData.Details.FromPlayerIndex), " | Show blip: ", EventData.ShowBlip)
		
		FORCE_BLIP_PLAYER(EventData.Details.FromPlayerIndex, EventData.ShowBlip, EventData.ShowBlip)
	
	ENDIF

ENDPROC

PROC PROCESS_SCRIPT_EVENT_DM_GIVE_TEAM_LIVES(INT iCount, ServerBroadcastData &serverBDPassed)

	SCRIPT_EVENT_DATA_DM_GIVE_TEAM_LIVES EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	
		IF EventData.iTeam < 0 OR EventData.iTeam >= FMMC_MAX_TEAMS
			PRINTLN("[DM_ONKILL_EVENT] PROCESS_SCRIPT_EVENT_DM_GIVE_TEAM_LIVES - Invalid team! Team: ", EventData.iTeam)
			EXIT
		ENDIF
		
		IF NOT bIsLocalPlayerHost
			PRINTLN("[DM_ONKILL_EVENT] PROCESS_SCRIPT_EVENT_DM_GIVE_TEAM_LIVES - Player is not host of this script!")
			EXIT
		ENDIF
		
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_SCRIPT_EVENT_DM_GIVE_TEAM_LIVES - Script deathmatch give team lives event received!")
		
		// Prevents the 'Team Lives' bottom right HUD from displaying incorrectly when a team with shared lives is given an additional life
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
		
			INT iRemainingLives = serverBDpassed.iTeamLives[EventData.iTeam] + serverBDpassed.iTeamExtraLives[EventData.iTeam] - serverBDPassed.iTeamDeaths[EventData.iTeam]
			
			IF iRemainingLives < 0
				EventData.iExtraLives += ABSI(iRemainingLives)
			ENDIF
			
		ENDIF
		
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_SCRIPT_EVENT_DM_GIVE_TEAM_LIVES - Giving team ", EventData.iTeam, " extra lives: ", EventData.iExtraLives)
		serverBDPassed.iTeamExtraLives[EventData.iTeam] += EventData.iExtraLives
	
	ENDIF

ENDPROC

PROC PROCESS_SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS(INT iCount, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])
	
	SCRIPT_EVENT_DATA_DM_PLAYER_OUT_OF_BOUNDS EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, EventData, SIZE_OF(EventData))
	
		IF EventData.Details.FromPlayerIndex = INVALID_PLAYER_INDEX()
			PRINTLN("[DM_BOUNDS_EVENT] PROCESS_SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS - Invalid player!")
			EXIT
		ENDIF
		
		IF NOT NETWORK_IS_PLAYER_ACTIVE(EventData.Details.FromPlayerIndex)
			PRINTLN("[DM_BOUNDS_EVENT] PROCESS_SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS - Player is no longer active!")
			EXIT
		ENDIF
		
		INT iCurrentModset = playerBDPassed[NATIVE_TO_INT(EventData.Details.FromPlayerIndex)].iCurrentModSet
		IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet) 
			PRINTLN("[BM_BOUNDS_EVENT] PROCESS_SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS - Invalid modset! Modset: ", iCurrentModSet)
			EXIT
		ENDIF
		
		INT iScore = GET_PLAYER_MODIFIERS__SCORING_OUT_OF_BOUNDS(dmVarsPassed.sTargetFloatingScores, iCurrentModset)
		INCREMENT_DEATHMATCH_SCORE(DEFAULT, iScore)
		IF iScore > 0
			SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_OOB_SCORE)
		ENDIF
	ENDIF
	
ENDPROC

// KING OF THE HILL //
PROC PROCESS_KOTH_PLAYER_GOT_A_KILL_EVENT(INT iEventID, KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo, PlayerBroadcastData &playerBDPassed[])
	SCRIPT_EVENT_DATA_KOTH_PLAYER_GOT_A_KILL EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
	
		IF NOT bIsLocalPlayerHost
		OR EventData.iKillerParticipant = -1
		OR EventData.iVictimParticipant = -1
			EXIT
		ENDIF
		
		IF playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo.iCurrentHillBS > 0
			
			INT iTeamToUse = playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo.iCachedKOTHTeam
			
			IF iTeamToUse = -1
				PRINTLN("[KOTH_HOST] PROCESS_KOTH_PLAYER_GOT_A_KILL_EVENT - Participant ", EventData.iKillerParticipant, " has an invalid team to use")
			ENDIF
			
			INT iArea
			FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
				IF IS_CAPTURING_KOTH_HILL(playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo, iArea)
				AND sKOTH_HostInfo.sHills[iArea].iTeamHolding = iTeamToUse
					BROADCAST_KOTH_GIVE_SCORE(iArea, EventData.vVictimCoords, ciKotH_POINTS_BONUS_ON_KILL, FALSE)
					PRINTLN("[KOTH_HOST] PROCESS_KOTH_PLAYER_GOT_A_KILL_EVENT - Giving a point to participant/team ", iTeamToUse, " | vVictimCoords: ", EventData.vVictimCoords)
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_KOTH_PLAYER_EXPLODED_A_VEHICLE(INT iEventID, KING_OF_THE_HILL_RUNTIME_LOCAL_STRUCT& sKOTH_LocalInfo, PlayerBroadcastData &playerBDPassed[])
	SCRIPT_EVENT_DATA_KOTH_PLAYER_EXPLODED_A_VEHICLE EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		
		IF EventData.iKillerParticipant = iLocalPart
			playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo.fKOTHScore -= TO_FLOAT(ciKotH_POINTS_LOST_FOR_KILLING_UNATTENDED_HILL)
			playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo.fKOTHScore = CLAMP(playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo.fKOTHScore, 0.0, 9999.0)
			PRINTLN("[KOTH] PROCESS_KOTH_PLAYER_EXPLODED_A_VEHICLE - Removing score from myself!")
			
			PRINTLN("[KOTH] PROCESS_KOTH_PLAYER_EXPLODED_A_VEHICLE - Adding number at ", EventData.vVehicleCoords)
			TARGET_ADD_FLOATING_SCORE(-ciKotH_POINTS_LOST_FOR_KILLING_UNATTENDED_HILL, EventData.vVehicleCoords, HUD_COLOUR_RED, sKOTH_LocalInfo.sFloatingScore)
		ENDIF
		
		//INT iTeamToUse = playerBDPassed[EventData.iKillerParticipant].sKOTH_PlayerInfo.iCachedKOTHTeam
	ENDIF
ENDPROC
// // // // // // // // // //

PROC PROCESS_FMMC_SHUNT_POWERUP(INT iEventID, INT iLocalTeam)
	
	SCRIPT_EVENT_DATA_FMMC_SHUNT_POWERUP EventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_SHUNT_POWERUP

			IF EventData.details.FromPlayerIndex = LocalPlayer
				EXIT
			ENDIF
			
			PED_INDEX damagerPed = GET_PLAYER_PED(EventData.details.FromPlayerIndex)
			
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				EXIT
			ENDIF
			IF NOT IS_PED_IN_ANY_VEHICLE(damagerPed)
				EXIT
			ENDIF
			
			VEHICLE_INDEX vIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)			
			VEHICLE_INDEX damagerVeh = GET_VEHICLE_PED_IS_IN(damagerPed)
			
			VECTOR vDir = GET_ENTITY_COORDS(vIndex) - GET_ENTITY_COORDS(damagerVeh)
			vDir = NORMALISE_VECTOR(vDir)

			FLOAT fFactor = EventData.fSpeedFactor
			FLOAT iForce = g_FMMC_STRUCT.fMaxPushSmash * fFactor
			FLOAT iForceV = g_FMMC_STRUCT.fMaxPushVSmash * fFactor
			IF iForce < g_FMMC_STRUCT.fMinPushSmash
				iForce = g_FMMC_STRUCT.fMinPushSmash
			ENDIF
			IF iForceV < g_FMMC_STRUCT.fMinPushVSmash
				iForceV = g_FMMC_STRUCT.fMinPushVSmash
			ENDIF
			vDir.X = vDir.X*iForce
			vDir.Y = vDir.Y*iForce
			vDir.Z = iForceV
			PRINTLN("PROCESS_FMMC_SHUNT_POWERUP - Force to apply: ", vDir)
			//FORCE
			SET_ENTITY_INVINCIBLE(vIndex, TRUE)
			APPLY_FORCE_TO_ENTITY(vIndex, APPLY_TYPE_EXTERNAL_IMPULSE, vDir, <<0,0,0>>, 0, FALSE, FALSE, TRUE)
			SET_ENTITY_INVINCIBLE(vIndex, FALSE)
			
			//DAMAGE
			IF GET_PLAYER_TEAM(EventData.details.FromPlayerIndex) = -1
			OR GET_PLAYER_TEAM(EventData.details.FromPlayerIndex) != iLocalTeam
				FLOAT fDamage = g_FMMC_STRUCT.iMaxDamageSmash*fFactor
				IF fDamage < g_FMMC_STRUCT.iMinDamageSmash
					fDamage = TO_FLOAT(g_FMMC_STRUCT.iMinDamageSmash)
				ENDIF
				
				FLOAT fHealth = GET_VEHICLE_BODY_HEALTH(vIndex) - fDamage
				IF fHealth < 0.0
					fHealth = 0.0
				ENDIF
				
				SET_VEHICLE_BODY_HEALTH(vIndex, fHealth)
				PRINTLN("PROCESS_FMMC_SHUNT_POWERUP - Vehicle body damage dealt: ", fHealth)
			ENDIF
			
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 130, 256)
			SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", fFactor + 0.1)
			
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_DEATHMATCH_EVENTS(ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDPassedLDB, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	INT iEventID
	EVENT_NAMES ThisScriptEvent
	
	STRUCT_EVENT_COMMON_DETAILS Details
	STRUCT_PLAYER_SCRIPT_EVENTS Data
	STRUCT_PICKUP_EVENT PickupData
	STRUCT_PICKUP_RESPAWN_EVENT pickupEventData
	TEXT_LABEL_15 labelTemp
	INT nThread = 0
	PARTICIPANT_INDEX participantIndex

	IF HAS_DM_STARTED(serverBDpassed)
	
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
			
			PRINTLN("[DM_DAMAGE_EVENT] - Processing DM event now")
			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
						
			SWITCH ThisScriptEvent			
			
				CASE EVENT_NETWORK_SCRIPT_EVENT	
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))	
					IF IS_NET_PLAYER_OK(Details.FromPlayerIndex, FALSE)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(Details.FromPlayerIndex)
							SWITCH Details.Type
								CASE SCRIPT_EVENT_FIRST_KILL
									IF g_sBlockedEvents.bSCRIPT_EVENT_FIRST_KILL_BLOCKED
										BREAK
									ENDIF
									PROCESS_FIRST_KILL_EVENT(dmVarsPassed, iEventID)
								BREAK
								CASE SCRIPT_EVENT_DM_REWARD_CASH
									IF g_sBlockedEvents.bSCRIPT_EVENT_DM_REWARD_CASH_BLOCKED
										BREAK
									ENDIF
									PROCESS_REWARD_CASH_EVENT(iEventID)
								BREAK
								CASE SCRIPT_EVENT_DM_FINISHED
									IF g_sBlockedEvents.bSCRIPT_EVENT_DM_FINISHED_BLOCKED
										BREAK
									ENDIF
									PROCESS_DM_FINISHED_EVENT(iEventID, dmVarsPassed)
								BREAK
								CASE SCRIPT_EVENT_DM_BULLSHARK
									IF g_sBlockedEvents.bSCRIPT_EVENT_DM_BULLSHARK_BLOCKED
										BREAK
									ENDIF
									PROCESS_BULLSHARK_EVENT(iEventID)
								BREAK
								CASE SCRIPT_EVENT_DM_PASS_THE_BOMB_TAGGED
//									IF g_sBlockedEvents.bSCRIPT_EVENT_papap
//										BREAK
//									ENDIF
									PROCESS_SCRIPT_EVENT_DM_PASS_THE_BOMB_TAGGED(iEventID, serverBDpassed, playerBDpassed)
								BREAK
								CASE SCRIPT_EVENT_DM_PLAYER_BLIP
									PROCESS_SCRIPT_EVENT_DM_PLAYER_BLIP(iEventID)
								BREAK
								CASE SCRIPT_EVENT_DM_GIVE_TEAM_LIVES
									PROCESS_SCRIPT_EVENT_DM_GIVE_TEAM_LIVES(iEventID, serverBDpassed)	
								BREAK
								CASE SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS
									PROCESS_SCRIPT_EVENT_DM_PLAYER_OUT_OF_BOUNDS(iEventID, dmVarsPassed, playerBDPassed)	
								BREAK
								
								CASE SCRIPT_EVENT_FMMC_ARENA_ARTICULATED_JOINT
									PRINTLN("PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ARENA_ARTICULATED_JOINT, processing event...")
									PROCESS_SCRIPT_EVENT_FMMC_ARENA_ARTICULATED_JOINT(iEventID, serverFMMCPassed.niDynoProps)
								BREAK
								
								CASE SCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED
									PRINTLN("PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED, processing event...")
									PROCESS_SCRIPT_EVENT_FMMC_ARENA_TRAP_ACTIVATED(iEventID, serverBDpassed.sTrapInfo_Host, dmVarsPassed.sTrapInfo_Local)
								BREAK
								CASE SCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED
									PRINTLN("PROCESS_EVENTS - SCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED, processing event...")
									PROCESS_SCRIPT_EVENT_FMMC_ARENA_TRAP_DEACTIVATED(iEventID, serverBDpassed.sTrapInfo_Host, dmVarsPassed.sTrapInfo_Local)
								BREAK
								CASE SCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED
									PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED, processing event...")
									PROCESS_SCRIPT_EVENT_FMMC_ARENA_LANDMINE_TOUCHED(iEventID, serverBDpassed.sTrapInfo_Host)
								BREAK
								
								CASE SCRIPT_EVENT_KOTH_FLASH_AREA
									PROCESS_KOTH_FLASH_AREA_EVENT(iEventID, serverBDpassed.sKOTH_HostInfo, playerBDpassed[iLocalPart].sKOTH_PlayerInfo, dmVarsPassed.sKOTH_LocalInfo)
								BREAK
								CASE SCRIPT_EVENT_KOTH_SET_CAPTURE_DIR
									PROCESS_KOTH_SET_CAPTURE_DIR_EVENT(iEventID, serverBDpassed.sKOTH_HostInfo, playerBDpassed[iLocalPart].sKOTH_PlayerInfo, dmVarsPassed.sKOTH_LocalInfo)
								BREAK
								CASE SCRIPT_EVENT_KOTH_GIVE_SCORE
									PROCESS_KOTH_GIVE_SCORE_EVENT(iEventID, dmVarsPassed.sKOTH_LocalInfo, playerBDpassed[iLocalPart].sKOTH_PlayerInfo, serverBDpassed.sKOTH_HostInfo, GET_TARGET_SCORE(serverBDPassed, -1))
								BREAK
								CASE SCRIPT_EVENT_KOTH_PLAYER_GOT_A_KILL
									PROCESS_KOTH_PLAYER_GOT_A_KILL_EVENT(iEventID, serverBDpassed.sKOTH_HostInfo, playerBDpassed)
								BREAK
								CASE SCRIPT_EVENT_KOTH_PLAYER_EXPLODED_AN_UNATTENDED_VEHICLE_HILL
									PROCESS_KOTH_PLAYER_EXPLODED_A_VEHICLE(iEventID, dmVarsPassed.sKOTH_LocalInfo, playerBDpassed)
								BREAK
								
								#IF IS_DEBUG_BUILD
								CASE SCRIPT_EVENT_JOB_INTRO
									PROCESS_JOB_INTRO_EVENT(iEventID)
								BREAK
								#ENDIF
								
								CASE SCRIPT_EVENT_FMMC_ALPHA_CHANGE
									IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_CHANGE_ALPHA_BLOCKED
										BREAK
									ENDIF
									PRINTLN("[RCC MISSION]  - mission Controller - PROCESS_EVENTS - bSCRIPT_EVENT_FMMC_CHANGE_ALPHA_BLOCKED, processing event...")
									PROCESS_SCRIPT_EVENT_FMMC_ALPHA_CHANGE(iEventID)
								BREAK	
								#IF IS_DEBUG_BUILD
								CASE SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS
									PROCESS_SCRIPT_EVENT_DM_PAUSE_ALL_TIMERS(iEventID)
								BREAK
								#ENDIF
								
								CASE SCRIPT_EVENT_FMMC_SHUNT_POWERUP
									PROCESS_FMMC_SHUNT_POWERUP(iEventId, playerBDPassed[iLocalPart].iTeam)
								BREAK
								
								CASE SCRIPT_EVENT_FMMC_POWERUP_SOUND
									IF g_sBlockedEvents.bSCRIPT_EVENT_FMMC_POWERUP_SOUND
										BREAK
									ENDIF
									PROCESS_SCRIPT_EVENT_FMMC_POWERUP_SOUND(iEventID)
								BREAK
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK

				CASE EVENT_NETWORK_PLAYER_JOIN_SCRIPT
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Data, SIZE_OF(Data))
					REPEAT Data.NumThreads nThread
						IF (Data.Threads[nThread] = GET_ID_OF_THIS_THREAD())
							GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Data, SIZE_OF(Data))	
							IF IS_NET_PLAYER_OK(Data.PlayerIndex)
								participantIndex = NETWORK_GET_PARTICIPANT_INDEX(Data.PlayerIndex)
								IF IS_BIT_SET(playerBDPassed[NATIVE_TO_INT(participantIndex)].iClientBitSet, CLIENT_BITSET_JOINED_LATE)
									ADD_TICKER_PLAYER_JOINED_MODE(Data.PlayerIndex, "DM_T_JOINED")
								ENDIF
							ENDIF		
						ENDIF
					ENDREPEAT
				BREAK
				
				CASE EVENT_NETWORK_PICKUP_RESPAWNED
					IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, pickupEventData, SIZE_OF(pickupEventData))
					
						ADD_BLIP_FOR_RESPAWNED_PICKUPS(dmVarsPassed.Pickup, dmVarsPassed.blipPickups, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
						
						PLAY_POWERUPS_SOUND_FROM_PICKUP(pickupEventData.PickupIndex, "Powerup_Block_Spawn")
					ENDIF			
				BREAK
				
				// Weapon pickup tickers
				CASE EVENT_NETWORK_PLAYER_COLLECTED_PICKUP
					IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, PickupData, SIZE_OF(PickupData))

						IF IS_NET_PLAYER_OK(PickupData.PlayerIndex)
						AND NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
							
							BOOL bLocalPlayer
							bLocalPlayer = (PickupData.PlayerIndex = LocalPlayer)
							
							INT iPickupLoop, iPickupCollected
							iPickupCollected = -1
							FOR iPickupLoop = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons - 1
								
								IF iPickupLoop >= FMMC_MAX_WEAPONS
									BREAKLOOP
								ENDIF
								
								IF PickupData.PickupIndex != dmVarsPassed.Pickup[iPickupLoop]
									RELOOP
								ENDIF
								
								iPickupCollected = iPickupLoop
								
								IF PickupData.PickupType = PICKUP_CUSTOM_SCRIPT
									PROCESS_CUSTOM_PICKUP_EVENT(iPickupCollected)
								ELSE
									PROCESS_POWERUP_PICKUP_EVENT(dmVarsPassed.sPowerUps, iPickupCollected, bLocalPlayer)
								ENDIF
								PRINTLN("iPickupCollected: ", iPickupCollected)
								BREAKLOOP
							ENDFOR
														
							IF bLocalPlayer
								IF NETWORK_IS_PLAYER_A_PARTICIPANT(PickupData.PlayerIndex)
									BOOL bAltTicker
		//							PRINTLN("NETWORK_IS_PLAYER_A_PARTICIPANT")
									IF iPickupCollected != -1
										IF dmVarsPassed.sPowerUps.ePowerUpJustCollected != POWERUP_NONE
											labelTemp = GET_TEXT_LABEL_FOR_POWERUP_TYPE(ENUM_TO_INT(dmVarsPassed.sPowerUps.ePowerUpJustCollected))
											bAltTicker = TRUE
										ELSE
											labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType, PickupData.PickupAmount, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickupCollected].iVehicleWeaponPickupType)
										ENDIF
									ELSE
										labelTemp = GET_CORRECT_TICKER_STRING(PickupData.PickupType, PickupData.PickupAmount)
									ENDIF
									
									IF NOT IS_STRING_NULL_OR_EMPTY(labelTemp)
										IF bAltTicker
											PRINT_TICKER_WITH_STRING("PWRUP_PICK", labelTemp)
										ELSE
											PRINT_WEAPON_TICKER(labelTemp)
										ENDIF
										
									ENDIF
									
									IF dmVarsPassed.bCollectedHealth = FALSE
										IF PickupData.PickupType = PICKUP_HEALTH_STANDARD  
										OR PickupData.PickupType = PICKUP_VEHICLE_HEALTH_STANDARD 
										OR PickupData.PickupType = PICKUP_VEHICLE_HEALTH_STANDARD_LOW_GLOW
											PRINTLN("[CS_OBJ] bCollectedHealth = TRUE ")
											dmVarsPassed.bCollectedHealth = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
							
						ENDIF
						
						REMOVE_BLIP_FOR_COLLECTED_PICKUP(dmVarsPassed.Pickup, dmVarsPassed.blipPickups, g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
						
						PLAY_POWERUPS_SOUND_FROM_PICKUP(pickupEventData.PickupIndex, "Powerup_Block_Despawn")
					ENDIF
				BREAK	
				
				CASE EVENT_NETWORK_REMOVED_FROM_SESSION_DUE_TO_STALL
					IF NOT dmVarsPassed.sSplitSession.bSplitSession
						PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] EVENT_NETWORK_REMOVED_FROM_SESSION_DUE_TO_STALL ")
						dmVarsPassed.sSplitSession.bSplitSession = TRUE
					ENDIF
				BREAK
				
				CASE EVENT_NETWORK_REMOVED_FROM_SESSION_DUE_TO_COMPLAINTS
					IF NOT dmVarsPassed.sSplitSession.bSplitSession
						PRINTLN("[NET_DEATHMATCH] [SPLIT_SESSION] EVENT_NETWORK_REMOVED_FROM_SESSION_DUE_TO_COMPLAINTS ")
						dmVarsPassed.sSplitSession.bSplitSession = TRUE
					ENDIF
				BREAK
				
				CASE EVENT_NETWORK_CONNECTION_TIMEOUT
					PROCESS_EVENT_NETWORK_CONNECTION_TIMEOUT(dmVarsPassed, iEventID)
				BREAK
				
				CASE EVENT_NETWORK_SESSION_EVENT
					PROCESS_EVENT_NETWORK_SESSION_EVENT(dmVarsPassed, iEventID)
				BREAK
				
				CASE EVENT_NETWORK_DAMAGE_ENTITY
					PROCESS_EVENT_NETWORK_DAMAGE_ENTITY(dmVarsPassed, iEventID, serverBDPassed, serverBDPassedLDB, playerBDPassed)
				BREAK
				
			ENDSWITCH
		ENDREPEAT
	
	// DEBUG ONLY EVENTS
	#IF IS_DEBUG_BUILD
	
	ELSE
		
		REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iEventID
	
			ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iEventID)
						
			SWITCH ThisScriptEvent			
			
				CASE EVENT_NETWORK_SCRIPT_EVENT	
					GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Details, SIZE_OF(Details))	
					IF IS_NET_PLAYER_OK(Details.FromPlayerIndex, FALSE)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(Details.FromPlayerIndex)
							SWITCH Details.Type
								
								#IF IS_DEBUG_BUILD
								CASE SCRIPT_EVENT_JOB_INTRO
									PROCESS_JOB_INTRO_EVENT(iEventID)
								BREAK
								#ENDIF
								
							ENDSWITCH
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDREPEAT
		
	#ENDIF
	
	ENDIF
ENDPROC
