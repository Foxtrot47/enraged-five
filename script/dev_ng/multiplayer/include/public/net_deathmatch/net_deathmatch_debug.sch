USING "net_deathmatch_utility.sch"

#IF IS_DEBUG_BUILD

PROC PROCESS_PAUSED_TIMERS(ServerBroadcastData &serverBDpassed)

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_9, KEYBOARD_MODIFIER_ALT, "Pause Mission Timers")
	OR bWdPauseGameTimerRAG != bWdPauseGameTimer
		IF NOT bWdPauseGameTimer
			bWdPauseGameTimer = TRUE
			bWdPauseGameTimerRAG = TRUE
		ELIF bWdPauseGameTimer
			bWdPauseGameTimer = FALSE
			bWdPauseGameTimerRAG = FALSE
		ENDIF			
		BROADCAST_FMMC_PAUSE_ALL_TIMERS(bWdPauseGameTimer)
	ENDIF
	
	IF bWdPauseGameTimer
		FLOAT fTextSize = 0.55
		TEXT_LABEL_63 sText
		SET_TEXT_SCALE(fTextSize, fTextSize)
		SET_TEXT_COLOUR(75, 75, 170, 200)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
			sText = "### Game Timer Paused ###"
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			sText = "### Bomb Timer Paused ###"
		ENDIF
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.4, 0.15, "STRING", sText)
	ENDIF
	
	IF NOT bWdPauseGameTimer
		RESET_NET_TIMER(tdPauseTimer1)
	ELIF bWdPauseGameTimer				
		PRINTLN("[LM][PROCESS_PAUSED_TIMERS] - Pausing Mission Timers.")
		//PAUSE
		IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			NET_TIMER_PAUSE_THIS_FRAME(serverBDpassed.tdTaggedExplosionTimer, tdPauseTimer1)
		ELSE
			NET_TIMER_PAUSE_THIS_FRAME(serverBDpassed.timeServerMainDeathmatchTimer, tdPauseTimer1)
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_8, KEYBOARD_MODIFIER_ALT, "Reduce Mission Timers")
		serverBDpassed.timeServerMainDeathmatchTimer.Timer = GET_TIME_OFFSET(serverBDpassed.timeServerMainDeathmatchTimer.Timer, -30000)
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_DM_PAUSE_ALL_TIMERS(INT iEventID)
	SCRIPT_EVENT_DATA_FMMC_PAUSE_ALL_TIMERS EventData
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		IF EventData.Details.Type = SCRIPT_EVENT_FMMC_PAUSE_ALL_TIMERS
			IF EventData.Details.FromPlayerIndex != LocalPlayer
				bWdPauseGameTimer = EventData.bPauseToggle
				bWdPauseGameTimerRAG = EventData.bPauseToggle
			ENDIF
			PRINTLN("[PROCESS_SCRIPT_EVENT_RACE_PAUSE_ALL_TIMERS] - Player ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), "  EventData.bPauseToggle: ",  EventData.bPauseToggle)
		ENDIF
	ENDIF
ENDPROC

PROC START_PRINTING_USEFUL_DM_STUFF(ServerBroadcastData &serverBDPassed)//, SHARED_DM_VARIABLES &dmVarsPassed)
	PRINTLN("___________________________START_PRINTING_USEFUL_DM_STUFF__________________________________", g_FMMC_STRUCT.tl63MissionName)
	PRINTLN("DM_START_TIME  >>> ", NATIVE_TO_INT(GET_NETWORK_TIME()))
	PRINTLN("MY_NAME_IS ", GET_PLAYER_NAME(LocalPlayer))
	PRINTLN(" DM_INSTANCE ", g_i_BrucieInstanceDm)
	IF bIsLocalPlayerHost
		PRINTLN(" I_AM_DM_HOST ", NETWORK_PLAYER_ID_TO_INT())
	ELSE
		PRINTLN(" I_AM_DM_CLIENT ", NETWORK_PLAYER_ID_TO_INT())
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		PRINTLN("[CS_RCK] ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order ")
	ENDIF
	
	IF IS_CURRENT_MISSION_UGC_NON_ROCKSTAR()
		PRINTLN("[CS_RCK] IS_CURRENT_MISSION_UGC_NON_ROCKSTAR ")
	ENDIF
	IF IS_CURRENT_MISSION_ROCKSTAR_VERIFIED()
		PRINTLN("[CS_RCK] IS_CURRENT_MISSION_ROCKSTAR_VERIFIED ")
	ENDIF
	IF IS_CURRENT_MISSION_ROCKSTAR_CREATED()
		PRINTLN("[CS_RCK] IS_CURRENT_MISSION_ROCKSTAR_CREATED ")
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		PRINTLN("[CS_RCK] CONTENT_IS_USING_ARENA() ")
	ENDIF
	
	PRINTLN("HOST_IS_CALLED ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())) )  
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
		PRINTLN("IS_PLAYER_ON_IMPROMPTU_DM")
		IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
			PRINTLN("IS_PLAYER_ON_IMPROMPTU_DM, vehicleDM")
		ENDIF
	ELSE
		IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
			IF serverBDpassed.iTeamBalancing = DM_TEAM_BALANCING_OFF
				PRINTLN("DM_TEAM_BALANCING_OFF")
			ELSE
				PRINTLN("DM_TEAM_BALANCING_ON")
			ENDIF
			PRINTLN("TEAM_DEATHMATCH")		
		ELIF IS_PLAYER_ON_BOSSVBOSS_DM() 
			PRINTLN("IS_PLAYER_ON_BOSSVBOSS_DM")
		ELSE
			IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
				PRINTLN("IS_THIS_VEHICLE_DEATHMATCH")
			ELSE
				PRINTLN("FFA_DEATHMATCH")
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints = ",g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints)
	PRINTLN("serverBDpassed.iNumDMPlayers ", serverBDpassed.iNumDMPlayers)
	PRINTLN("NETWORK_GET_NUM_PARTICIPANTS()", NETWORK_GET_NUM_PARTICIPANTS()) 
	
	IF IS_JOB_FORCED_WEAPON_ONLY()
		PRINTLN("[CS_WEAPON] - IS_JOB_FORCED_WEAPON_ONLY ")
	ELIF IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
		PRINTLN("[CS_WEAPON] - IS_JOB_FORCED_WEAPON_PLUS_PICKUPS ")
	ELSE
		PRINTLN("[CS_WEAPON] - IS_JOB_FORCED_OWNED_PLUS_PICKUPS ")
	ENDIF
	
	PRINTLN("g_FMMC_STRUCT.iAdversaryModeType: ", g_FMMC_STRUCT.iAdversaryModeType)
	
//	DEBUG_PRINT_DEATHMATCH_LOBBY_CHOICES(serverBDPassed)
	DEBUG_PRINT_PLAYERS()
	
	PRINTLN("______________________________________________________________________________________________")
ENDPROC

FUNC BOOL HAS_DEBUG_BEEN_USED_IN_THIS_DM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_S_PASS)
	
		PRINTLN("HAS_DEBUG_BEEN_USED_IN_THIS_DM, DEBUG_BITSET_S_PASS")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_F_FAIL)
	
		PRINTLN("HAS_DEBUG_BEEN_USED_IN_THIS_DM, DEBUG_BITSET_F_FAIL")
		RETURN TRUE
	ENDIF
	
	IF playerBDPassed[iLocalPart].bPressedS = TRUE
		PRINTLN("HAS_DEBUG_BEEN_USED_IN_THIS_DM, bPressedS")
		RETURN TRUE
	ENDIF
	
	IF playerBDPassed[iLocalPart].bPressedF = TRUE
		PRINTLN("HAS_DEBUG_BEEN_USED_IN_THIS_DM, bPressedF")
		RETURN TRUE
	ENDIF
	
	// if debug warped
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ONE_PLAYER_DM_DEBUG()

	RETURN GlobalServerBD_DM.bOnePlayerDeathmatch
ENDFUNC

FUNC BOOL HAS_LOCAL_PLAYER_DEBUG_ENDED(SHARED_DM_VARIABLES &dmVarsPassed)

	IF dmVarsPassed.debugDmVars.bPassTriggered = TRUE
	OR dmVarsPassed.debugDmVars.bFailTriggered = TRUE 
	
		PRINTLN("HAS_LOCAL_PLAYER_DEBUG_ENDED  ")
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC INITIALISE_DEBUG_LBD_TEAMS(SHARED_DM_VARIABLES &dmVarsPassed)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		dmVarsPassed.debugDmVars.iPlyerTeam[i] = 17
	ENDREPEAT	
ENDPROC

PROC CREATE_DM_WIDGETS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	TEXT_LABEL_63 dummy = ""
	TEXT_LABEL_63 tl63 = ""
	
	INT i = 0
			

	widgetID = START_WIDGET_GROUP("     DEATHMATCH Widgets")	

	IF widgetID != NULL
		IF DOES_WIDGET_GROUP_EXIST(widgetID)
			CLEAR_CURRENT_WIDGET_GROUP(widgetID)
		ENDIF
		SET_CURRENT_WIDGET_GROUP(widgetID)
	ENDIF
	
		START_WIDGET_GROUP("           ARENA")
			ADD_WIDGET_BOOL("bKeepLocalRacing", bKeepLocalRacing)	
			ADD_WIDGET_BOOL("bSplitSessionDuringJob", bSplitSessionDuringJob)
			
			ADD_WIDGET_BOOL("bSplitSession", dmVarsPassed.sSplitSession.bSplitSession)
			
			ADD_WIDGET_INT_SLIDER("iVehicleRemovalCap", serverBDpassed.iVehicleRemovalCap, 1, NUM_NETWORK_PLAYERS, 1)
			ADD_WIDGET_BOOL("Pause Timers", bWdPauseGameTimerRAG)
			
			ADD_WIDGET_INT_READ_ONLY(" g_FMMC_STRUCT_ENTITIES.iNumberOfProps", g_FMMC_STRUCT_ENTITIES.iNumberOfProps)

//			ADD_WIDGET_INT_SLIDER("iExtraTime", iExtraTime, -1000000, 1000000, 1)	
//			ADD_WIDGET_INT_SLIDER("iPlayerLives", playerBDPassed[iLocalPart].iPlayerLives, 0, 100, 1)

			CREATE_FMMC_ARENA_WIDGETS()
		STOP_WIDGET_GROUP()	
	
		ADD_WIDGET_BOOL("bFreezeCelebration ", bFreezeCelebration)
		ADD_WIDGET_BOOL("bHideDMHud", dmVarsPassed.debugDmVars.bHideDMHud)	
		ADD_WIDGET_BOOL("MPGlobalsScoreHud.ProgressHud_ForceReset", MPGlobalsScoreHud.ProgressHud_ForceReset)			
		START_WIDGET_GROUP("   Pickups")
			ADD_WIDGET_INT_READ_ONLY(" iPickupsMade", dmVarsPassed.iPickupsMade)
			ADD_WIDGET_INT_READ_ONLY(" iNumberOfWeapons", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("   ELO")
			ADD_WIDGET_INT_READ_ONLY(" NumDMPlayed", dmVarsPassed.iNumberDmStartedStat)
			ADD_WIDGET_INT_READ_ONLY(" TeamNumPlayers", serverBDpassed.iNumberOfTeams)
			ADD_WIDGET_INT_READ_ONLY(" FFANumPlayers", serverBDpassed.iNumDmStarters)
			ADD_WIDGET_INT_READ_ONLY(" iCurrentELO", playerBDPassed[iLocalPart].iCurrentELO)
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("   ENDVOTE")
//			ADD_WIDGET_BOOL("bDebugRefresh", dmVarsPassed.nextJobStruct.dbgNJVS.bDebugRefresh)	
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes0", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_ZERO])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes1", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_ONE])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes2", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_TWO])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes3", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_THREE])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes4", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_FOUR])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes5", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_FIVE])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes6", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_SIX])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes7", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_SEVEN])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes8", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_EIGHT])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes9", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_NINE])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes10", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_TEN])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes11", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_ELEVEN])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes12rep", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_REPLAY])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes13ref", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_REFRESH])
			ADD_WIDGET_INT_READ_ONLY(" iJobVotes14ext", g_sMC_serverBDEndJob.iJobVotes[JOB_PANEL_EXIT])
			
			START_WIDGET_GROUP("   HOST")
				ADD_WIDGET_INT_READ_ONLY(" iNumberOnJob", g_sMC_serverBDEndJob.iNumberOnJob)
				ADD_WIDGET_INT_READ_ONLY(" iHighestPlayerRank", g_sMC_serverBDEndJob.iHighestPlayerRank)
				ADD_WIDGET_INT_READ_ONLY(" iAverageRank", g_sMC_serverBDEndJob.iAverageRank)
				ADD_WIDGET_INT_READ_ONLY(" iJobVoteWinner", g_sMC_serverBDEndJob.iJobVoteWinner)
				ADD_WIDGET_INT_READ_ONLY(" iSecondryJobVoteWinner", g_sMC_serverBDEndJob.iSecondryJobVoteWinner)
				ADD_WIDGET_INT_READ_ONLY(" iRandomProgress", g_sMC_serverBDEndJob.iRandomProgress)
//				ADD_WIDGET_INT_READ_ONLY(" iStoredRandom", g_sMC_serverBDEndJob.iStoredRandom)
				ADD_WIDGET_INT_READ_ONLY(" iVoteTotal", g_sMC_serverBDEndJob.iVoteTotal)
				ADD_WIDGET_INT_READ_ONLY(" iFound", g_sMC_serverBDEndJob.iFound)
				ADD_WIDGET_INT_READ_ONLY(" iServerLogicProgress", g_sMC_serverBDEndJob.iServerLogicProgress)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Perks")
			ADD_WIDGET_INT_SLIDER("g_iDeathStreak", g_iDeathStreak, 0, 9999, 1)	
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("g_i_CoronaHighestRankPlayer")
			ADD_WIDGET_INT_READ_ONLY("g_i_CoronaHighestRankPlayer", g_i_CoronaHighestRankPlayer)	
		STOP_WIDGET_GROUP()	
	
	
		START_WIDGET_GROUP("Zoom")
			ADD_WIDGET_BOOL("bDisableZoom", dmVarsPassed.debugDmVars.bDisableZoom)
//			ADD_WIDGET_FLOAT_SLIDER("fFurthestTargetDistTemp", dmVarsPassed.fFurthestTargetDistTemp , -1, 100.00, 0.001) 
			ADD_WIDGET_FLOAT_SLIDER("fFurthestTargetDist", dmVarsPassed.fFurthestTargetDist , -1, 1000.00, 0.001) 
			ADD_WIDGET_FLOAT_SLIDER("fOldFurthestTargetDist", dmVarsPassed.fOldFurthestTargetDist , -1, 1000.00, 0.001) 
//			ADD_WIDGET_FLOAT_SLIDER("fFurthestWeaponDist", dmVarsPassed.fFurthestWeaponDist , -1, 100.00, 0.001) 
//			ADD_WIDGET_FLOAT_SLIDER("fFurthestWeaponDistTemp", dmVarsPassed.fFurthestWeaponDistTemp , -1, 100.00, 0.001) 
			
			ADD_WIDGET_INT_READ_ONLY("iNumPickCreated ", serverBDpassed.iNumPickCreated)
			//ADD_WIDGET_INT_READ_ONLY("iWeaponIterator ", dmVarsPassed.iWeaponIterator)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("1407518")
			ADD_WIDGET_BOOL("g_b_SpamRankOutput", g_b_SpamRankOutput)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("VEH dm")
			ADD_WIDGET_INT_READ_ONLY("g_iLastDamagerPlayer ", g_iLastDamagerPlayer)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Bounty cash on me")
			ADD_WIDGET_INT_READ_ONLY("BountyCash ", GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iPlayerBounty)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("LeaderBoard")
			ADD_WIDGET_BOOL("bSpewLeaderboardInfo", bSpewLeaderboardInfo)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Damage")
			ADD_WIDGET_FLOAT_READ_ONLY("g_f_MyDamageTaken ", g_f_MyDamageTaken)
			ADD_WIDGET_FLOAT_READ_ONLY("g_f_MyDamageDealt ", g_f_MyDamageDealt)
			ADD_WIDGET_FLOAT_READ_ONLY("fDamageTaken ", playerBDPassed[iLocalPart].fDamageTaken)
			ADD_WIDGET_FLOAT_READ_ONLY("fDamageDealt ", playerBDPassed[iLocalPart].fDamageDealt)
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP(" SCORE globals ")
			ADD_WIDGET_INT_READ_ONLY(" g_iMyDMScore ", g_iMyDMScore)
			ADD_WIDGET_INT_READ_ONLY(" g_iMyAmountOfKills ", g_iMyAmountOfKills)
			ADD_WIDGET_INT_READ_ONLY(" g_iMyAmountOfDeaths ", g_iMyAmountOfDeaths)
			ADD_WIDGET_INT_READ_ONLY(" g_iMyAmountOfSuicides ", g_iMyAmountOfSuicides)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("ThumbCount")		
			ADD_WIDGET_INT_READ_ONLY(" iTotalLikes", g_TransitionSessionNonResetVars.stThumbVars.iTotalLikes)
			ADD_WIDGET_INT_READ_ONLY(" iTotalDislikes", g_TransitionSessionNonResetVars.stThumbVars.iTotalDislikes)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Match Winner ")
			ADD_WIDGET_INT_READ_ONLY("g_iLastKillerId ", g_iLastKillerId)
//			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.playerMatchWinner ", serverBDpassed.playerMatchWinner)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Cash/XP Tracker ")
			START_WIDGET_GROUP("XP Gained Breakdown")
				ADD_WIDGET_INT_READ_ONLY("XP AWARDED DURING ", dmVarsPassed.debugDmVars.iXPDuring)
				ADD_WIDGET_INT_READ_ONLY("XP AWARD END ", dmVarsPassed.debugDmVars.iXPEnd)
				ADD_WIDGET_INT_READ_ONLY("XP AWARD TOTAL ", dmVarsPassed.debugDmVars.iXPTotal)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Cash Break down")
				ADD_WIDGET_INT_READ_ONLY("CASH AWARD DURING ", dmVarsPassed.debugDmVars.iCashDuring)
				ADD_WIDGET_INT_READ_ONLY("CASH AWARD END ", dmVarsPassed.debugDmVars.iCashEnd)
				ADD_WIDGET_INT_READ_ONLY("CASH AWARD TOTAL ", dmVarsPassed.debugDmVars.iCashTotal)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("CASH")
			ADD_WIDGET_INT_READ_ONLY(" g_iMyJobCash",g_iMyJobCash)
			ADD_WIDGET_BOOL("Ignore 4 player Min", g_b_Ignore_PlayerCount_JobCash)
			ADD_WIDGET_INT_SLIDER("g_i_DM_CashToDrop", g_i_DM_CashToDrop, 0, 50000, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("  SCROLL")
			ADD_WIDGET_BOOL("bNeverTimeOut", dmVarsPassed.debugDmVars.bNeverTimeOut)
			ADD_WIDGET_BOOL("bScrollSpew", dmVarsPassed.debugDmVars.bScrollSpew)
			ADD_WIDGET_INT_READ_ONLY("dmVarsPassed.iPlayerSelection", dmVarsPassed.iPlayerSelection )
			ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("    Debug Lbd ")
//			ADD_WIDGET_INT_SLIDER("TEST_MAX_ROW", TEST_MAX_ROW, 0, 9999, 1)
			ADD_WIDGET_BOOL("bDrawBIGLeaderboard", dmVarsPassed.debugDmVars.bDrawBIGLeaderboard)
			ADD_WIDGET_BOOL("Disable Pass the Bomb Timer", dmVarsPassed.debugDmVars.bDisablePTBTimer)
			ADD_WIDGET_BOOL("bNeverTimeOut", dmVarsPassed.debugDmVars.bNeverTimeOut)
			ADD_WIDGET_INT_SLIDER("iNumColumns", dmVarsPassed.debugDmVars.iNumColumns, 0, 6, 1)
			ADD_WIDGET_BOOL("bSameTeam", dmVarsPassed.debugDmVars.bSameTeam)
			ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_LeaderboardRowCounter", g_i_LeaderboardRowCounter, -100, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_LeaderboardShift", g_i_LeaderboardShift, -100, 9999, 1)
			
			START_WIDGET_GROUP("TEAMS ")
				REPEAT NUM_NETWORK_PLAYERS i
					tl63 = "iPlyerTeam["
					tl63 += i
					tl63 += "]"
					ADD_WIDGET_INT_SLIDER(tl63 , dmVarsPassed.debugDmVars.iPlyerTeam[i], 0, 17, 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_SLIDER("iNumTeams", dmVarsPassed.debugDmVars.iNumTeams, 1, 8, 1)
			
			START_WIDGET_GROUP(" Team Counts")
				ADD_WIDGET_INT_SLIDER("iTeam1WinCount", dmVarsPassed.debugDmVars.iTeamWinCount, 0, 16, 1)
				ADD_WIDGET_INT_SLIDER("iTeam2Count", dmVarsPassed.debugDmVars.iTeam2Count, 0, 16, 1)
				ADD_WIDGET_INT_SLIDER("iTeam3Count", dmVarsPassed.debugDmVars.iTeam3Count, 0, 16, 1)
				ADD_WIDGET_INT_SLIDER("iTeam4Count", dmVarsPassed.debugDmVars.iTeam4Count, 0, 16, 1)
				ADD_WIDGET_INT_SLIDER("iTeam5Count", dmVarsPassed.debugDmVars.iTeam5Count, 0, 16, 1)
				ADD_WIDGET_INT_SLIDER("iTeam6Count", dmVarsPassed.debugDmVars.iTeam6Count, 0, 16, 1)
				ADD_WIDGET_INT_SLIDER("iTeam7Count", dmVarsPassed.debugDmVars.iTeam7Count, 0, 16, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP(" Team Order")
				ADD_WIDGET_INT_SLIDER("i1stTeam", dmVarsPassed.debugDmVars.iWinningTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i2ndTeam", dmVarsPassed.debugDmVars.iSecondTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i3rdTeam", dmVarsPassed.debugDmVars.iThirdTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i4thTeam", dmVarsPassed.debugDmVars.iFourthTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i5thTeam", dmVarsPassed.debugDmVars.iFifthTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i6thTeam", dmVarsPassed.debugDmVars.iSixthTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i7thTeam", dmVarsPassed.debugDmVars.iSeventhTeam, 0, 7, 1)
				ADD_WIDGET_INT_SLIDER("i8iLosingTeam", dmVarsPassed.debugDmVars.iLosingTeam, 0, 7, 1)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP(" variables")
				ADD_WIDGET_INT_SLIDER("iTeamScore1", dmVarsPassed.debugDmVars.iTeamScore1, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore2", dmVarsPassed.debugDmVars.iTeamScore2, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore3", dmVarsPassed.debugDmVars.iTeamScore3, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore4", dmVarsPassed.debugDmVars.iTeamScore4, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore5", dmVarsPassed.debugDmVars.iTeamScore5, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore6", dmVarsPassed.debugDmVars.iTeamScore6, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore7", dmVarsPassed.debugDmVars.iTeamScore7, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("iTeamScore8", dmVarsPassed.debugDmVars.iTeamScore8, 0, 100000, 1)
				ADD_WIDGET_FLOAT_SLIDER("fRatio", dmVarsPassed.debugDmVars.fRatio , -1, 100.00, 0.001) 
				ADD_WIDGET_INT_SLIDER("iVar1", dmVarsPassed.debugDmVars.iVar1, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar2", dmVarsPassed.debugDmVars.iVar2, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar3", dmVarsPassed.debugDmVars.iVar3, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar4", dmVarsPassed.debugDmVars.iVar4, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar5", dmVarsPassed.debugDmVars.iVar5, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar6", dmVarsPassed.debugDmVars.iVar6, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iBadgeRank", dmVarsPassed.debugDmVars.iBadgeRank, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iRank", dmVarsPassed.debugDmVars.iRank, 0, 9999999, 1)
				ADD_WIDGET_FLOAT_SLIDER("fKDRatio",  dmVarsPassed.debugDmVars.fKDRatio, 0, 100.0, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP(" DM Blips")
			ADD_WIDGET_BOOL("PowerPlayBlip ", dmVarsPassed.bBlipPower)
			ADD_WIDGET_BOOL("DeathStreakBlip ", dmVarsPassed.bBlipDeath)		
			ADD_WIDGET_BOOL("AllBlipsOn ", dmVarsPassed.bBlipAll)
			ADD_WIDGET_BOOL("MyBlipFlash ", dmVarsPassed.bBlipFlash)
			ADD_WIDGET_BOOL("KillStreakBlip ", dmVarsPassed.bBlipStreak)
			ADD_WIDGET_BOOL("bBlipPerk ", dmVarsPassed.bBlipPerk)
			ADD_WIDGET_BOOL("bBlipStart ", dmVarsPassed.bBlipStart)
			ADD_WIDGET_BOOL("bBlipEnd ", dmVarsPassed.bBlipEnd)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP(" OBJECTIVES")	
			ADD_WIDGET_INT_SLIDER("iObjective", g_iDMObjective, -1, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_iMyDMObjectiveOutcome", g_iMyDMObjectiveOutcome, -1, 9999, 1)
			ADD_WIDGET_INT_SLIDER("g_i_Headshots", g_i_Headshots, 0, 9999, 1)
			ADD_WIDGET_BOOL("bCollectedHealth ", dmVarsPassed.bCollectedHealth)
			ADD_WIDGET_BOOL("g_b_OverrideObjective ", g_b_OverrideObjective)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Global")	
			ADD_WIDGET_BOOL("g_bFM_ON_VEH_DEATHMATCH", g_bFM_ON_VEH_DEATHMATCH)
			ADD_WIDGET_BOOL("g_bFM_ON_TEAM_DEATHMATCH", g_bFM_ON_TEAM_DEATHMATCH)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Power Play")	
			ADD_WIDGET_INT_READ_ONLY("g_i_PowerPlayer", g_i_PowerPlayer)
//			ADD_WIDGET_INT_READ_ONLY("g_e_Chosen_Perk", ENUM_TO_INT(g_e_Chosen_Perk))
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Brucie Box")	
			ADD_WIDGET_BOOL("bDebugBrucieBox(host)", dmVarsPassed.debugDmVars.bDebugBrucieBox)
			ADD_WIDGET_BOOL("bMakeBrucieBox", dmVarsPassed.bMakeBrucieBox)
			ADD_WIDGET_INT_READ_ONLY("iBrucieProgress", serverBDpassed.iBrucieProgress)
			ADD_WIDGET_INT_READ_ONLY("iNumberOfWeapons", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
			ADD_WIDGET_INT_READ_ONLY("g_i_BrucieInstanceDm", g_i_BrucieInstanceDm)
		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP(" DPAD SIZES")
//			ADD_WIDGET_FLOAT_SLIDER("SDPAD_X", SDPAD_X , 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("SDPAD_Y", SDPAD_Y , 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("SDPAD_H", SDPAD_H , 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("SDPAD_W", SDPAD_W , 0, 2.0, 0.001)
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("        NEW COLUMNS")	
		
//			START_WIDGET_GROUP(" PERCENT")
//				START_WIDGET_GROUP(" THUMB")
////					ADD_WIDGET_FLOAT_SLIDER("TVOTEP_H ", TVOTEP_H  , 0, 2.0, 0.001)
////					ADD_WIDGET_FLOAT_SLIDER("TVOTEP_W ", TVOTEP_W  , 0, 2.0, 0.001)
////					ADD_WIDGET_FLOAT_SLIDER("TVOTEP_X ", TVOTEP_X  , 0, 2.0, 0.001)
////					ADD_WIDGET_FLOAT_SLIDER("TVOTEP_Y ", TVOTEP_Y  , 0, 2.0, 0.001)
////					ADD_WIDGET_FLOAT_SLIDER("TVOTEP_ROT", TVOTEP_ROT , 0, 2.0, 0.001)
//				STOP_WIDGET_GROUP()
//				
////				ADD_WIDGET_FLOAT_SLIDER("PERCENT_X", PERCENT_X , 0, 2.0, 0.001)
////				ADD_WIDGET_FLOAT_SLIDER("PERCENT_Y", PERCENT_Y , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
						
//			START_WIDGET_GROUP(" ROCK")
////				ADD_WIDGET_FLOAT_SLIDER("ROCKSTAR_BADGE_X", ROCKSTAR_BADGE_X , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("ROCKSTAR_BADGE_Y", ROCKSTAR_BADGE_Y , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("ROCKSTAR_BADGE_SCALE", ROCKSTAR_BADGE_SCALE , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP(" SCROLL")
//				ADD_WIDGET_FLOAT_SLIDER("SCROLL_X", SCROLL_X , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("SCROLL_Y", SCROLL_Y , 0, 2.0, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_X", SCROLLARR_X , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_Y", SCROLLARR_Y , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_W", SCROLLARR_W , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("SCROLLARR_H", SCROLLARR_H , 0, 2.0, 0.001)
				
//				ADD_WIDGET_FLOAT_SLIDER("COUNT_LONG__RECT_X", COUNT_LONG__RECT_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COUNT_RECT_Y", COUNT_RECT_Y, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COUNT_LONG__RECT_W", COUNT_LONG__RECT_W, 0, 1.0, 0.001)
				
				ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
				ADD_WIDGET_INT_SLIDER("g_i_LeaderboardRowCounter", g_i_LeaderboardRowCounter, -100, 9999, 1)
				ADD_WIDGET_INT_SLIDER("g_i_LeaderboardShift", g_i_LeaderboardShift, -100, 9999, 1)
			STOP_WIDGET_GROUP()

			ADD_WIDGET_BOOL("bNeverTimeOut", dmVarsPassed.debugDmVars.bNeverTimeOut)
			ADD_WIDGET_INT_SLIDER("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected, -100, 9999, 1)
			ADD_WIDGET_BOOL("bDrawBIGLeaderboard", dmVarsPassed.debugDmVars.bDrawBIGLeaderboard)
			ADD_WIDGET_BOOL("bDebugDrawDDSTexture", dmVarsPassed.debugDmVars.bDebugDrawDDSTexture)
			ADD_WIDGET_BOOL("bWidthsGuide", dmVarsPassed.debugDmVars.bWidthsGuide)
			ADD_WIDGET_INT_SLIDER("iNumColumns", dmVarsPassed.debugDmVars.iNumColumns, 0, 6, 1)
			ADD_WIDGET_BOOL("g_b_ShowBestLap", g_b_ShowBestLap)
//			ADD_WIDGET_FLOAT_SLIDER("fRankScale", fRankScale , 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("fCrewScale", fCrewScale , 0, 2.0, 0.001)
			ADD_WIDGET_BOOL("bSameTeam", dmVarsPassed.debugDmVars.bSameTeam)
			
//			
//			ADD_WIDGET_FLOAT_SLIDER("COLUM_TITLE_X", COLUM_TITLE_X , 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("COLUM_TITLE_Y", COLUM_TITLE_Y , 0, 2.0, 0.001)
//			
//			ADD_WIDGET_FLOAT_SLIDER("TEAM_BIG_PLAYER_NAMES_X", TEAM_BIG_PLAYER_NAMES_X, 0, 1.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("YOUR_TEAM_SCORE_X", YOUR_TEAM_SCORE_X, 0, 1.0, 0.001)
			
//			START_WIDGET_GROUP(" ICONS")
//				ADD_WIDGET_FLOAT_SLIDER("BLOB_X", BLOB_X , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("BLOB_Y", BLOB_Y , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("BLOB_W", BLOB_W , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("BLOB_H", BLOB_H , 0, 2.0, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("VOICE_H ", VOICE_H , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("VOICE_W ", VOICE_W , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("VOICE_X ", VOICE_X , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("VOICE_Y ", VOICE_Y , 0, 2.0, 0.001)
//				
//			STOP_WIDGET_GROUP()
			
//			START_WIDGET_GROUP(" RECT")
//			
//				ADD_WIDGET_FLOAT_SLIDER("RED_X", RED_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("RED_W", RED_W, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_X", PLAYER_NAME_RECT_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_W", PLAYER_NAME_RECT_W, 0, 1.0, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("VAR_RECT_X", VAR_RECT_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("VAR_RECT_W", VAR_RECT_W, 0, 1.0, 0.001)
//				
//				// team bars & white lines
//				ADD_WIDGET_FLOAT_SLIDER("TEAM_RECT_X",  		TEAM_RECT_X, 		0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("TEAM_RECT_W",  		TEAM_RECT_W, 		0, 1.0, 0.001)
////				ADD_WIDGET_FLOAT_SLIDER("TWO_PIXEL_HEIGHT", 	TWO_PIXEL_HEIGHT, 	0, 1.0, 0.001)
////				ADD_WIDGET_FLOAT_SLIDER("WHITE_TEAM_RECT_Y", 	WHITE_TEAM_RECT_Y, 	0, 1.0, 0.001)
//				
//			STOP_WIDGET_GROUP()
//			
//			START_WIDGET_GROUP(" Crew")
////				ADD_WIDGET_FLOAT_SLIDER("CREW_X", CREW_X , 0, 2.0, 0.001)
////				ADD_WIDGET_FLOAT_SLIDER("CREW_Y", CREW_Y , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("VARIABLE_BASE_CREW", VARIABLE_BASE_CREW, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("fCrewOffset", fCrewOffset , -2, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
//		
			START_WIDGET_GROUP(" wraps")	
//				START_WIDGET_GROUP("6 col")
//					ADD_WIDGET_FLOAT_SLIDER("f6wrap1EndX", f6wrap1EndX , -1, 2.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("f6wrap2EndX", f6wrap2EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f6wrap3EndX", f6wrap3EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f6wrap4EndX", f6wrap4EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f6wrap5EndX", f6wrap5EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f6wrap6EndX", f6wrap6EndX , -1, 2.0, 0.001)  
//				STOP_WIDGET_GROUP()
//
//				START_WIDGET_GROUP("5 col")
//					ADD_WIDGET_FLOAT_SLIDER("f5wrap1EndX", f5wrap1EndX, -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f5wrap2EndX", f5wrap2EndX, -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f5wrap3EndX", f5wrap3EndX, -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f5wrap4EndX", f5wrap4EndX, -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f5wrap5EndX", f5wrap5EndX, -1, 2.0, 0.001)  
//				STOP_WIDGET_GROUP()
//
//				START_WIDGET_GROUP("4 col")
//					ADD_WIDGET_FLOAT_SLIDER("f4wrap1EndX", f4wrap1EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f4wrap2EndX", f4wrap2EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f4wrap3EndX", f4wrap3EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f4wrap4EndX", f4wrap4EndX , -1, 2.0, 0.001)   
//				STOP_WIDGET_GROUP()
//
//				START_WIDGET_GROUP("3 col")
//					ADD_WIDGET_FLOAT_SLIDER("f3wrap1EndX", f3wrap1EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f3wrap2EndX", f3wrap2EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f3wrap3EndX", f3wrap3EndX , -1, 2.0, 0.001)   
//				STOP_WIDGET_GROUP()
//
//				START_WIDGET_GROUP("2 col")
//					ADD_WIDGET_FLOAT_SLIDER("f2wrap1EndX", f2wrap1EndX , -1, 2.0, 0.001)     
//					ADD_WIDGET_FLOAT_SLIDER("f2wrap2EndX", f2wrap2EndX , -1, 2.0, 0.001)   
//				STOP_WIDGET_GROUP()
//
//				START_WIDGET_GROUP("1 col")
//					ADD_WIDGET_FLOAT_SLIDER("f1wrap1EndX", f1wrap1EndX , -1, 2.0, 0.001) 
//				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP(" variables")
				ADD_WIDGET_FLOAT_SLIDER("fRatio", dmVarsPassed.debugDmVars.fRatio , -1, 100.00, 0.001) 
				ADD_WIDGET_INT_SLIDER("iVar1", dmVarsPassed.debugDmVars.iVar1, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar2", dmVarsPassed.debugDmVars.iVar2, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar3", dmVarsPassed.debugDmVars.iVar3, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar4", dmVarsPassed.debugDmVars.iVar4, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar5", dmVarsPassed.debugDmVars.iVar5, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iVar6", dmVarsPassed.debugDmVars.iVar6, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iBadgeRank", dmVarsPassed.debugDmVars.iBadgeRank, 0, 9999999, 1)
				ADD_WIDGET_INT_SLIDER("iRank", dmVarsPassed.debugDmVars.iRank, 0, 9999999, 1)
				
//				ADD_WIDGET_FLOAT_SLIDER("TEAM_TOTAL_X", TEAM_TOTAL_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("TEAM_TOTAL_Y", TEAM_TOTAL_Y, 0, 1.0, 0.001)
			STOP_WIDGET_GROUP()
		
//			START_WIDGET_GROUP("6 COLUMNS")
//				ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_1 		",  SIX_COLUMN_1 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_2 		",  SIX_COLUMN_2 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_3 		",  SIX_COLUMN_3 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_4 		",  SIX_COLUMN_4 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_5 		",  SIX_COLUMN_5 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" SIX_COLUMN_6 		",  SIX_COLUMN_6 		 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()	
//			
//			START_WIDGET_GROUP("5 COLUMNS")
//				ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_1  	",  FIVE_COLUMN_1 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_2  	",  FIVE_COLUMN_2 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_3  	",  FIVE_COLUMN_3 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_4  	",  FIVE_COLUMN_4 		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("FIVE_COLUMN_5  	",  FIVE_COLUMN_5 		 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()	
//			
//			START_WIDGET_GROUP("4 COLUMNS")
//				ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_1 	",   FOUR_COLUMN_1 	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_2 	",   FOUR_COLUMN_2 	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_3 	",   FOUR_COLUMN_3 	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("  FOUR_COLUMN_4 	",   FOUR_COLUMN_4 	 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()	
//			
//			START_WIDGET_GROUP("3 COLUMNS")
//				ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_1 	",   THREE_COLUMN_1 	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_2 	",   THREE_COLUMN_2 	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("   THREE_COLUMN_3 	",   THREE_COLUMN_3 	 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()	
//			
//			START_WIDGET_GROUP("2 COLUMNS")
//				ADD_WIDGET_FLOAT_SLIDER("   TWO_COLUMN_1 	",   TWO_COLUMN_1 	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("   TWO_COLUMN_2 	",   TWO_COLUMN_2 	 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()	
//			
//			START_WIDGET_GROUP("1 COLUMN")
//				ADD_WIDGET_FLOAT_SLIDER("   ONE_COLUMN_1 	",   ONE_COLUMN_1 	 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()	
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("     Team Balancing ")
//			ADD_WIDGET_INT_READ_ONLY(" iBalanceProgress", serverBDpassed.iBalanceProgress )
//			ADD_WIDGET_INT_READ_ONLY(" iFindLargestProgress", serverBDpassed.iFindLargestProgress )
			ADD_WIDGET_INT_READ_ONLY(" iCurrentDMTeam", serverBDpassed.iCurrentDMTeam )
			ADD_WIDGET_BOOL("bTeamSizeSorted ", serverBDpassed.bTeamSizeSorted)
		STOP_WIDGET_GROUP()
		
		ADD_WIDGET_BOOL("g_bFM_ON_TEAM_DEATHMATCH ", g_bFM_ON_TEAM_DEATHMATCH)
	
		ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iParticipantsTurnToSpawn", serverBDpassed.iParticipantsTurnToSpawn )	
		ADD_WIDGET_INT_READ_ONLY("dmVarsPassed.iPlayerSelection", dmVarsPassed.iPlayerSelection )	
//		ADD_WIDGET_INT_READ_ONLY("g_i_ActualLeaderboardPlayerSelected", g_i_ActualLeaderboardPlayerSelected )	
		
		ADD_WIDGET_BOOL("bDebugUnlockVeh", dmVarsPassed.debugDmVars.bDebugUnlockVeh)	
		ADD_WIDGET_INT_READ_ONLY("iVehicleRespawnTime", g_FMMC_STRUCT_ENTITIES.iVehicleRespawnTime )
		ADD_WIDGET_INT_READ_ONLY("iRestartProgress", dmVarsPassed.iRestartProgress )
		ADD_WIDGET_INT_READ_ONLY("iLateTeamProgress", dmVarsPassed.iLateTeamProgress )
		ADD_WIDGET_BOOL("g_b_PlayAgain", g_b_PlayAgain)
		ADD_WIDGET_BOOL("g_b_RestartButtons", g_b_RestartButtons)
	
		START_WIDGET_GROUP("  THUMBS")
			
			ADD_WIDGET_BOOL("bDebugDrawThumbs", dmVarsPassed.debugDmVars.bDebugDrawThumbs)
			ADD_WIDGET_BOOL("bDebugHideThumbRect", dmVarsPassed.debugDmVars.bDebugHideThumbRect)
			
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_rect_width", THUMB_rect_width, 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_rect_height", THUMB_rect_height, 0, 2.0, 0.001)
//			
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_X", THUMB_X, 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_Y", THUMB_Y, 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_W", THUMB_W, 0, 2.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_H", THUMB_H, 0, 2.0, 0.001)
//			ADD_WIDGET_INT_SLIDER("THUMB_R", THUMB_R, 0, 9999, 1)
//			ADD_WIDGET_INT_SLIDER("THUMB_G", THUMB_G, 0, 9999, 1)
//			ADD_WIDGET_INT_SLIDER("THUMB_B", THUMB_B, 0, 9999, 1)
//			ADD_WIDGET_INT_SLIDER("THUMB_A", THUMB_A, 0, 9999, 1)
//			ADD_WIDGET_FLOAT_SLIDER("THUMB_Rot", THUMB_Rot, 0, 2.0, 0.001)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("  1738975")
//			ADD_WIDGET_INT_READ_ONLY( "iTeamKills0", serverBDpassed.iTeamKills[0])
//			ADD_WIDGET_INT_READ_ONLY( "iTeamKills1", serverBDpassed.iTeamKills[1])
//			ADD_WIDGET_INT_READ_ONLY( "iTeamKills2", serverBDpassed.iTeamKills[2])
//			ADD_WIDGET_INT_READ_ONLY( "iTeamKills3", serverBDpassed.iTeamKills[3])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScore0", serverBDpassed.iTeamScore[0])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScore1", serverBDpassed.iTeamScore[1])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScore2", serverBDpassed.iTeamScore[2])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScore3", serverBDpassed.iTeamScore[3])
			
			ADD_WIDGET_INT_READ_ONLY( "iTeamScoreForLBD0", serverBDpassed.iTeamScoreForLBD[0])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScoreForLBD1", serverBDpassed.iTeamScoreForLBD[1])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScoreForLBD2", serverBDpassed.iTeamScoreForLBD[2])
			ADD_WIDGET_INT_READ_ONLY( "iTeamScoreForLBD3", serverBDpassed.iTeamScoreForLBD[3])
			
			ADD_WIDGET_INT_READ_ONLY( "iTeamDeaths0", serverBDpassed.iTeamDeaths[0])
			ADD_WIDGET_INT_READ_ONLY( "iTeamDeaths1", serverBDpassed.iTeamDeaths[1])
			ADD_WIDGET_INT_READ_ONLY( "iTeamDeaths2", serverBDpassed.iTeamDeaths[2])
			ADD_WIDGET_INT_READ_ONLY( "iTeamDeaths3", serverBDpassed.iTeamDeaths[3])
			
			ADD_WIDGET_FLOAT_READ_ONLY( "iTeamRatio0", serverBDpassed.fTeamRatio[0])
			ADD_WIDGET_FLOAT_READ_ONLY( "iTeamRatio1", serverBDpassed.fTeamRatio[1])
			ADD_WIDGET_FLOAT_READ_ONLY( "iTeamRatio2", serverBDpassed.fTeamRatio[2])
			ADD_WIDGET_FLOAT_READ_ONLY( "iTeamRatio3", serverBDpassed.fTeamRatio[3])
			
			ADD_WIDGET_INT_READ_ONLY( "iTeamXP0", serverBDpassed.iTeamXP[0])
			ADD_WIDGET_INT_READ_ONLY( "iTeamXP1", serverBDpassed.iTeamXP[1])
			ADD_WIDGET_INT_READ_ONLY( "iTeamXP2", serverBDpassed.iTeamXP[2])
			ADD_WIDGET_INT_READ_ONLY( "iTeamXP3", serverBDpassed.iTeamXP[3])
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("  TEAM_DEATHMATCH")
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iTeamWithHighestScore", serverBDpassed.iTeamWithHighestScore)
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iBestPlayerWinningTeam", serverBDpassed.iBestPlayerWinningTeam )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iDeathmatchType", serverBDpassed.iDeathmatchType )
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iNumberOfTeams ", serverBDpassed.iNumberOfTeams)
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNumActiveTeams", serverBDpassed.iNumActiveTeams )
			ADD_WIDGET_INT_READ_ONLY("TEAM 0 ", serverBDpassed.iNumberOfLBPlayers[0])
			ADD_WIDGET_INT_READ_ONLY("TEAM 1 ", serverBDpassed.iNumberOfLBPlayers[1])
			ADD_WIDGET_INT_READ_ONLY("TEAM 2 ", serverBDpassed.iNumberOfLBPlayers[2])
			ADD_WIDGET_INT_READ_ONLY("TEAM 3 ", serverBDpassed.iNumberOfLBPlayers[3])	
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iWinningTeam ", serverBDpassed.iWinningTeam)
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iSecondTeam ", serverBDpassed.iSecondTeam)
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iThirdTeam ", serverBDpassed.iThirdTeam)
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iLosingTeam ", serverBDpassed.iLosingTeam)
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iNumWinners ", serverBDpassed.iNumWinners)
			
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iTeam2Count ", serverBDpassed.iTeam2Count)
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iTeam3Count ", serverBDpassed.iTeam3Count)
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iTeam4Count ", serverBDpassed.iTeam4Count)
			
//			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iFindWinningTeamStage ", serverBDpassed.iFindWinningTeamStage)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP(" FMMC DEATHMATCH")  
		
			ADD_WIDGET_BOOL("g_b_On_Deathmatch", g_b_On_Deathmatch)
			ADD_WIDGET_BOOL("g_b_AddToCashScore", g_b_AddToCashScore)
			ADD_WIDGET_INT_READ_ONLY("g_i_DM_CashToAdd", g_i_DM_CashToAdd)
		
			ADD_WIDGET_BOOL("bLoadedMissionRatingDetails", dmVarsPassed.bLoadedMissionRatingDetails)
			//ADD_WIDGET_INT_SLIDER("iQuit", g_FMMC_Current_Mission_Cloud_Data.iQuit, 0, 9999, 1)
			//ADD_WIDGET_INT_SLIDER("iPlayed", g_FMMC_Current_Mission_Cloud_Data.iPlayed, 0, 9999, 1)
			//ADD_WIDGET_BOOL("bBookMarked", g_FMMC_Current_Mission_Cloud_Data.bBookMarked)
			//ADD_WIDGET_INT_SLIDER("iRating", g_FMMC_Current_Mission_Cloud_Data.iRating, 0, 1, 1)
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("           ROUTE_A")
			START_WIDGET_GROUP("TEAM_BARS")
				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_RECT_X 	",  tfTEAM_RECT_X 	 	, 0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_RECT_W 	",  tfTEAM_RECT_W 	 	, 0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_WEE_RECT_W ",  tfTEAM_WEE_RECT_W 	, 0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_WEE_RECT_X ",  tfTEAM_WEE_RECT_X 	, 0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_POS_X ",  tfTEAM_POS_X 	, 0, 2.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_NAME_X ",  tfTEAM_NAME_X 	, 0, 2.0, 0.001)
				

//				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_BIG_RECT_W ",  tfTEAM_BIG_RECT_W 	, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" tfTEAM_BIG_RECT_X ",  tfTEAM_BIG_RECT_X 	, 0, 2.0, 0.001)
			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("      4")
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_FOUR   ",  	VAR_FOUR_FOUR    , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_THREE  ",  	VAR_FOUR_THREE   , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_TWO	",  	VAR_FOUR_TWO	 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_ONE	",  	VAR_FOUR_ONE	 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("      3")
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_THREE_THREE ",  	VAR_THREE_THREE  , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_THREE_TWO   ",  	VAR_THREE_TWO    , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_THREE_ONE   ",  	VAR_THREE_ONE    , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("      2")
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_TWO_TWO		",  VAR_TWO_TWO		 , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" VAR_TWO_ONE		",  VAR_TWO_ONE		 , 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
			
		
		START_WIDGET_GROUP("       BIG Leaderboard")
		
			//ADD_WIDGET_BOOL("bHideRaceUI", dmVarsPassed.debugDmVars.bHideRaceUI)
			ADD_WIDGET_BOOL("bDrawBIGLeaderboard", dmVarsPassed.debugDmVars.bDrawBIGLeaderboard)
			ADD_WIDGET_BOOL("bFakeBIGLeaderboard", dmVarsPassed.debugDmVars.bFakeBIGLeaderboard)
			ADD_WIDGET_BOOL("bDebugDrawDDSTexture", dmVarsPassed.debugDmVars.bDebugDrawDDSTexture)
			ADD_WIDGET_BOOL("bNeverTimeOut", dmVarsPassed.debugDmVars.bNeverTimeOut)
			
//			START_WIDGET_GROUP("       		NEW TEAM")
//				ADD_WIDGET_FLOAT_SLIDER("TEAM_BIG_PLAYER_NAMES_X", TEAM_BIG_PLAYER_NAMES_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("YOUR_TEAM_SCORE_X", YOUR_TEAM_SCORE_X, 0, 1.0, 0.001)
				
				
//			STOP_WIDGET_GROUP()
		
//			START_WIDGET_GROUP("      ICONS")	
				
//				START_WIDGET_GROUP("      VARS")
//					START_WIDGET_GROUP("      5")
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FIVE_FIVE 	",  VAR_FIVE_FIVE 	 , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FIVE_FOUR 	",  VAR_FIVE_FOUR 	 , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FIVE_THREE 	",  VAR_FIVE_THREE 	 , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FIVE_TWO  	",  VAR_FIVE_TWO  	 , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FIVE_ONE  	",  VAR_FIVE_ONE  	 , 0, 2.0, 0.001)
//					STOP_WIDGET_GROUP()
//					START_WIDGET_GROUP("      4")
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_FOUR   ",  	VAR_FOUR_FOUR    , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_THREE  ",  	VAR_FOUR_THREE   , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_TWO	",  	VAR_FOUR_TWO	 , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_FOUR_ONE	",  	VAR_FOUR_ONE	 , 0, 2.0, 0.001)
//					STOP_WIDGET_GROUP()
//					START_WIDGET_GROUP("      3")
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_THREE_THREE ",  	VAR_THREE_THREE  , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_THREE_TWO   ",  	VAR_THREE_TWO    , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_THREE_ONE   ",  	VAR_THREE_ONE    , 0, 2.0, 0.001)
//					STOP_WIDGET_GROUP()
//					START_WIDGET_GROUP("      2")
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_TWO_TWO		",  VAR_TWO_TWO		 , 0, 2.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER(" VAR_TWO_ONE		",  VAR_TWO_ONE		 , 0, 2.0, 0.001)
//					STOP_WIDGET_GROUP()
//				STOP_WIDGET_GROUP()
		
				// LEADERBOARD
//				ADD_WIDGET_FLOAT_SLIDER("LBD_TEXT_X", LBD_TEXT_X, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("LBD_TEXT_Y", LBD_TEXT_Y, 0, 2.0, 0.001)
//			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("RECTS")
				
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_LONG__RECT_X", COLUMN_LONG__RECT_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_LONG__RECT_W", COLUMN_LONG__RECT_W, 0, 1.0, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_RECT_Y", COLUMN_RECT_Y, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("RECT_HEIGHT_CONSTANT", RECT_HEIGHT_CONSTANT, 0, 1.0, 0.001)
//			
//				ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_X", PLAYER_NAME_RECT_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_W", PLAYER_NAME_RECT_W, 0, 1.0, 0.001)
//				
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_HEADING_RECT_X", COLUMN_HEADING_RECT_X, 0, 1.0, 0.001)	  				
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_HEADING_RECT_Y", COLUMN_HEADING_RECT_Y, 0, 1.0, 0.001)	  				
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_HEADING_RECT_W", COLUMN_HEADING_RECT_W, 0, 1.0, 0.001)	  							  						  					  	
//				
//				ADD_WIDGET_FLOAT_SLIDER("RECT_Y_OFFSET", RECT_Y_OFFSET, 0, 1.0, 0.001)	
//				
//				START_WIDGET_GROUP("RED")
//					ADD_WIDGET_FLOAT_SLIDER("RED_X", RED_X, 0, 1.0, 0.001)
//					ADD_WIDGET_FLOAT_SLIDER("RED_W", RED_W, 0, 1.0, 0.001)
//				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("WHITE")
		
//					START_WIDGET_GROUP("CAPS")
//					
//						START_WIDGET_GROUP("DM")
//					
//							ADD_WIDGET_FLOAT_SLIDER("COLUMN_CAPS_Y_ALL", COLUMN_CAPS_Y_ALL, 0, 1.0, 0.001)
//							ADD_WIDGET_FLOAT_SLIDER("COLUMN_CAP_H_ALL", COLUMN_CAP_H_ALL, 0, 1.0, 0.001)
//						
//							ADD_WIDGET_FLOAT_SLIDER("COLUMN_CAP1_X", COLUMN_CAP1_X, 0, 1.0, 0.001)
//							ADD_WIDGET_FLOAT_SLIDER("COLUMN_CAP1_W", COLUMN_CAP1_W, 0, 1.0, 0.001)
//							
//						STOP_WIDGET_GROUP()
//										
//					STOP_WIDGET_GROUP()
				
//					START_WIDGET_GROUP("All")
//					 				
//						ADD_WIDGET_FLOAT_SLIDER("COLUMN_HEADING_RECT_Y", COLUMN_HEADING_RECT_Y, 0, 1.0, 0.001)	  				
//
//						ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_X", PLAYER_NAME_RECT_X, 0, 1.0, 0.001)
//						ADD_WIDGET_FLOAT_SLIDER("PLAYER_NAME_RECT_W", PLAYER_NAME_RECT_W, 0, 1.0, 0.001)
//					
//					STOP_WIDGET_GROUP()
				
					
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			
//			START_WIDGET_GROUP("Deathmatch")
//			
//			STOP_WIDGET_GROUP()
//			START_WIDGET_GROUP("Team Deathmatch")
//			
//			STOP_WIDGET_GROUP()
			
//			START_WIDGET_GROUP("Sprites")
//				ADD_WIDGET_FLOAT_SLIDER("DDS_X", DDS_X, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("DDS_Y", DDS_Y, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("DDS_W", DDS_W, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("DDS_H", DDS_H, 0, 2.0, 0.001)
////				ADD_WIDGET_INT_SLIDER("DDS_a", DDS_a, 0, 255, 1)
//			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Content populate")
			
			
//				ADD_WIDGET_FLOAT_SLIDER(" PLAYER_XP_EARNED_X",  PLAYER_XP_EARNED_X , 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER(" PLAYER_CASH_EARNED_X",  PLAYER_CASH_EARNED_X , 0, 2.0, 0.001)
		
			
//				ADD_WIDGET_FLOAT_SLIDER("POSITION_POS_X", POSITION_POS_X, 0, 1.0, 0.001)
				
//				ADD_WIDGET_FLOAT_SLIDER("BIG_PLAYER_NAMES_X", BIG_PLAYER_NAMES_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_VARIABLE_3", COLUMN_VARIABLE_3, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("CARS_POS_X", CARS_POS_X, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_VARIABLE_5", COLUMN_VARIABLE_5, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("RACE_BEST_LAP_POS", RACE_BEST_LAP_POS, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_VARIABLE_7", COLUMN_VARIABLE_7, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("TIME_POS_X", TIME_POS_X, 0, 1.0, 0.001)
////				ADD_WIDGET_FLOAT_SLIDER("COLUMN_VARIABLE_9", COLUMN_VARIABLE_9, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("COLUMN_VARIABLE_10", COLUMN_VARIABLE_10, 0, 1.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("VARIABLE_BASE_Y", VARIABLE_BASE_Y, 0, 1.0, 0.001)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Top right position")
//				ADD_WIDGET_FLOAT_SLIDER("LARGE_POS_X", LARGE_POS_X, 0, 1.0, 0.001)	   							
//				ADD_WIDGET_FLOAT_SLIDER("LARGE_POS_Y", LARGE_POS_Y, 0, 1.0, 0.001)	   		
//				ADD_WIDGET_FLOAT_SLIDER("LARGE_POS_X_OFFSET", LARGE_POS_X_OFFSET, 0, 1.0, 0.001)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Rank badge")
//				ADD_WIDGET_FLOAT_SLIDER("RANK_BADGE_X", RANK_BADGE_X, 0, 1.0, 0.001)	  						
//				ADD_WIDGET_FLOAT_SLIDER("RANK_BADGE_Y", RANK_BADGE_Y, 0, 1.0, 0.001)	  						
	//			ADD_WIDGET_FLOAT_SLIDER("RANK_BADGE_W", RANK_BADGE_W, 0, 1.0, 0.001)	  						
	//			ADD_WIDGET_FLOAT_SLIDER("RANK_BADGE_H", RANK_BADGE_H, 0, 1.0, 0.001)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Team scores")
//				ADD_WIDGET_FLOAT_SLIDER("YOUR_TEAM_SCORE_X", YOUR_TEAM_SCORE_X, 0, 1.0, 0.001)	  						
	//			ADD_WIDGET_FLOAT_SLIDER("ENEMY_TEAM_SCORE_X", ENEMY_TEAM_SCORE_X, 0, 1.0, 0.001)	  						
//				ADD_WIDGET_FLOAT_SLIDER("TEAM_SCORE_Y", TEAM_SCORE_Y, 0, 1.0, 0.001)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("DPAD Leaderboard")
			ADD_WIDGET_BOOL("bNeverTimeOut", dmVarsPassed.debugDmVars.bNeverTimeOut)
			ADD_WIDGET_BOOL("bDrawDpadLeaderboard", dmVarsPassed.debugDmVars.bDrawDpadLeaderboard)
			ADD_WIDGET_BOOL("bFakeDpadLeaderboard", dmVarsPassed.debugDmVars.bFakeDpadLeaderboard)
			ADD_WIDGET_BOOL("bDrawDpadDDS", dmVarsPassed.debugDmVars.bDrawDpadDDS)
			
//			START_WIDGET_GROUP("DDS")
//				ADD_WIDGET_FLOAT_SLIDER("DPAD_DDS_X", DPAD_DDS_X, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("DPAD_DDS_Y", DPAD_DDS_Y, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("DPAD_DDS_W", DPAD_DDS_W, 0, 2.0, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("DPAD_DDS_H", DPAD_DDS_H, 0, 2.0, 0.001)
//				ADD_WIDGET_INT_SLIDER("DPAD_DDS_A", DPAD_DDS_A, 0, 255, 1)
//			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("Debug Populate")
				ADD_WIDGET_INT_SLIDER("Positions ", dmVarsPassed.debugDmVars.iDebugPos, 0, 100, 1)
				ADD_WIDGET_INT_SLIDER("Cash ", dmVarsPassed.debugDmVars.iDebugCash, 0, 100000, 1)
				ADD_WIDGET_INT_SLIDER("Kills ", dmVarsPassed.debugDmVars.iDebugKills, 0, 100, 1)
				ADD_WIDGET_BOOL("bCash", dmVarsPassed.debugDmVars.bCash)
			STOP_WIDGET_GROUP()	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Respawn ")
			ADD_WIDGET_INT_READ_ONLY("Netspawn respawn state", GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iRespawnState)
//			ADD_WIDGET_INT_READ_ONLY( "iRespawnProgress", dmVarsPassed.iRespawnProgress )
//			ADD_WIDGET_BOOL("g_bDeathmatchRespawnTimeUp", g_bDeathmatchRespawnTimeUp)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Local Bools")
			ADD_WIDGET_BOOL("bDontEndOnePlayer", dmVarsPassed.debugDmVars.bDontEndOnePlayer)
			ADD_WIDGET_BOOL("g_bDeathmatchFinished", g_bDeathmatchFinished)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("           ServerBDDeathmatch ")
		
		
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iTeamWithHighestScore", serverBDpassed.iTeamWithHighestScore )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iMVP", serverBDpassed.iMVP )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNumActiveTeams", serverBDpassed.iNumActiveTeams )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNumDMPlayers", serverBDpassed.iNumDMPlayers )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNumActivePlayers", serverBDpassed.iNumActivePlayers )
			ADD_WIDGET_INT_READ_ONLY( "serverBDPassed.iTagSetting", serverBDPassed.iTagSetting )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iDeathmatchType", serverBDpassed.iDeathmatchType )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iServerGameState", serverBDpassed.iServerGameState )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iServerBitSet", serverBDpassed.iServerBitSet )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iParticipantsTurnToSpawn", serverBDpassed.iParticipantsTurnToSpawn )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iMissionId", serverBDpassed.iMissionId )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNumVehCreated", serverBDpassed.iNumVehCreated )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNumberOfTeams", serverBDpassed.iNumberOfTeams )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iWinningTeam", serverBDpassed.iWinningTeam )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iSecondTeam", serverBDpassed.iSecondTeam )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iLosingTeam", serverBDpassed.iLosingTeam )
			ADD_WIDGET_INT_READ_ONLY( "serverBDpassed.iNextMission", serverBDpassed.iNextMission )			
			ADD_WIDGET_INT_READ_ONLY( "iNumVehCreated", serverBDpassed.iNumVehCreated )
			ADD_WIDGET_INT_READ_ONLY( "iServerGameState", serverBDpassed.iServerGameState )
			ADD_WIDGET_INT_READ_ONLY( "iNumDMPlayers", serverBDpassed.iNumDMPlayers )
			ADD_WIDGET_INT_READ_ONLY("Winning Team ", serverBDpassed.iWinningTeam)
			ADD_WIDGET_INT_READ_ONLY("Team in 2nd ", serverBDpassed.iSecondTeam)
			ADD_WIDGET_INT_READ_ONLY("Number of winners ", serverBDpassed.iNumWinners )
			ADD_WIDGET_INT_READ_ONLY("Next Mission", serverBDpassed.iNextMission)	
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iFinishTime ", serverBDpassed.iFinishTime )
			ADD_WIDGET_INT_READ_ONLY("serverBDpassed.iNumSpectators ", serverBDpassed.iNumSpectators )
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Globals")
		
			START_WIDGET_GROUP("Globals ")
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iDuration", GlobalServerBD_DM.iDuration )
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iTarget		", GlobalServerBD_DM.iTarget		 )
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iSpawnTime	", GlobalServerBD_DM.iSpawnTime	)
//				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iAim	", GlobalServerBD_DM.iAim	)
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iBlips", GlobalServerBD_DM.iBlips)
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iTags	", GlobalServerBD_DM.iTags	)
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iVoice	", GlobalServerBD_DM.iVoice	 )
				ADD_WIDGET_INT_READ_ONLY( "	GlobalServerBD_DM.iWeaponRespawnTime ", 	GlobalServerBD_DM.iWeaponRespawnTime  )
				ADD_WIDGET_INT_READ_ONLY( "GlobalServerBD_DM.iVehicleRespawnTime", GlobalServerBD_DM.iVehicleRespawnTime )
				ADD_WIDGET_BOOL("GlobalServerBD_DM.bIsTeamDM", GlobalServerBD_DM.bIsTeamDM)
			STOP_WIDGET_GROUP()			
		
			
		STOP_WIDGET_GROUP()
		
		i = 0
		
		START_WIDGET_GROUP( " Leaderboard Data") 
			FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
				dummy = "LB "
				dummy += i
				START_WIDGET_GROUP( dummy )
					ADD_WIDGET_INT_READ_ONLY("iParticipant", serverBDpassedLDB.leaderBoard[i].iParticipant)
//					ADD_WIDGET_INT_READ_ONLY("playerID", NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[i].playerID))
					ADD_WIDGET_INT_READ_ONLY("iTeam", serverBDpassedLDB.leaderBoard[i].iTeam)
					ADD_WIDGET_INT_READ_ONLY("iTeamScore", serverBDpassedLDB.leaderBoard[i].iTeamScore)					
					ADD_WIDGET_INT_READ_ONLY("iKills", serverBDpassedLDB.leaderBoard[i].iKills)						
					ADD_WIDGET_INT_READ_ONLY("iDeaths", serverBDpassedLDB.leaderBoard[i].iDeaths)
					ADD_WIDGET_INT_READ_ONLY("iRank", serverBDpassedLDB.leaderBoard[i].iRank)
					ADD_WIDGET_INT_READ_ONLY("iScore", serverBDpassedLDB.leaderBoard[i].iScore)
				STOP_WIDGET_GROUP()
			ENDFOR
			
			START_WIDGET_GROUP("ServerLBD") 
				INT iPlayers = 0
				REPEAT NUM_NETWORK_PLAYERS iPlayers
					IF (serverBDpassedLDB.leaderBoard[iPlayers].playerID <> INVALID_PLAYER_INDEX())
						ADD_WIDGET_STRING(GET_PLAYER_NAME(serverBDpassedLDB.leaderBoard[iPlayers].playerID))

						ADD_WIDGET_INT_READ_ONLY("i = ", i)
						ADD_WIDGET_INT_READ_ONLY("iKills = ", serverBDpassedLDB.leaderBoard[iPlayers].iKills)
						ADD_WIDGET_INT_READ_ONLY("iScore = ", serverBDpassedLDB.leaderBoard[iPlayers].iScore)
						ADD_WIDGET_INT_READ_ONLY("iDeaths = ", serverBDpassedLDB.leaderBoard[iPlayers].iDeaths)
						
						ADD_WIDGET_INT_READ_ONLY("iJobXP = ", serverBDpassedLDB.leaderBoard[iPlayers].iJobXP)
						ADD_WIDGET_INT_READ_ONLY("iJobCash = ", serverBDpassedLDB.leaderBoard[iPlayers].iJobCash)
						ADD_WIDGET_INT_READ_ONLY("iBets = ", serverBDpassedLDB.leaderBoard[iPlayers].iBets)
						
						ADD_WIDGET_FLOAT_READ_ONLY("KD_RATIo = ", serverBDpassedLDB.leaderBoard[iPlayers].fKDRatio)
						ADD_WIDGET_INT_READ_ONLY("iRank = ", serverBDpassedLDB.leaderBoard[iPlayers].iRank)
						ADD_WIDGET_INT_READ_ONLY("iBadgeRank = ", serverBDpassedLDB.leaderBoard[iPlayers].iBadgeRank)
						ADD_WIDGET_INT_READ_ONLY("iTeam = ", serverBDpassedLDB.leaderBoard[iPlayers].iTeam)
					ENDIF
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Client BD")  
			INT iPlayer = 0
			REPEAT NUM_NETWORK_PLAYERS iPlayer
				tl63 = "Player "
				tl63 += iPlayer
				IF iPlayer = iLocalPart
					tl63 += "*"
				ENDIF
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_BOOL("pressedTimeSkip", playerBDPassed[iPlayer].bFinishedDeathmatch)
//					ADD_WIDGET_INT_READ_ONLY("iFinishingPos", playerBDPassed[iPlayer].iFinishingPos )
					ADD_WIDGET_INT_READ_ONLY("iInitialFadeProgress", playerBDPassed[iPlayer].iInitialFadeProgress )
					ADD_WIDGET_INT_READ_ONLY("iGameState", playerBDPassed[iPlayer].iGameState )
					ADD_WIDGET_INT_READ_ONLY("iScore", playerBDPassed[iPlayer].iScore )

					ADD_WIDGET_INT_SLIDER("iKills", playerBDPassed[iPlayer].iKills, 0, 100, 1 )
					ADD_WIDGET_INT_SLIDER("iCurrentXP", playerBDPassed[iPlayer].iCurrentXP, 0, HIGHEST_INT, 1 )
					ADD_WIDGET_INT_SLIDER("iHeadshots", playerBDPassed[iPlayer].iHeadshots, 0, 100, 1 )
					ADD_WIDGET_INT_SLIDER("iDeaths", playerBDPassed[iPlayer].iDeaths, 0, 100, 1 )
					ADD_WIDGET_FLOAT_SLIDER("fRatio", playerBDPassed[iPlayer].fRatio, 0, 100, 0.1)
					ADD_WIDGET_BOOL("bPressedF", playerBDPassed[iPlayer].bPressedF)
					ADD_WIDGET_BOOL("bPressedS", playerBDPassed[iPlayer].bPressedS)
					ADD_WIDGET_BOOL("pressedTimeSkip", playerBDPassed[iPlayer].pressedTimeSkip)
//					ADD_WIDGET_INT_SLIDER("iDeathStreak", playerBDPassed[iPlayer].iDeathStreak, 0, 100, 1 )
					ADD_WIDGET_INT_READ_ONLY("iJobXP", playerBDPassed[iPlayer].iJobXP )
					ADD_WIDGET_INT_READ_ONLY("iJobCash", playerBDPassed[iPlayer].iJobCash )
					ADD_WIDGET_INT_SLIDER("iKillStreak", GlobalplayerBD_FM[(iPlayer)].iKillStreak , 0, 100, 1 )
					ADD_WIDGET_INT_READ_ONLY("iCurrentELO", playerBDPassed[iPlayer].iCurrentELO )
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("SHARED_DM_VARIABLES") 
		
			ADD_WIDGET_INT_SLIDER("dmVarsPassed.iDamage ", dmVarsPassed.iDamage, 0, 200, 5)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("CUTSCENE WIDGETS")
			ADD_WIDGET_BOOL("FORCE CAMERA TYPE ON", bDeathmatchIntroCut)
			ADD_WIDGET_INT_SLIDER("CAMERA TYPE TO USE ", iDeatchmatchIntroCut , 0, 9, 1)
			ADD_WIDGET_BOOL("IGNORE SAFETY CHECKS ", bDeathmatchIntroCutIgnoreSafe)
		STOP_WIDGET_GROUP()
		
		#IF SCRIPT_PROFILER_ACTIVE
			CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF
		
	STOP_WIDGET_GROUP()	
	

ENDPROC

// Debug print anything useful before the deathmatch ends
PROC PRINT_DEATHMATCH_DETAILS_BEFORE_LEADERBOARD_STAGE(ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	PRINTLN("START PRINT_DEATHMATCH_DETAILS_BEFORE_LEADERBOARD_STAGE -------------------------------- ", g_FMMC_STRUCT.tl63MissionName)
	
	PRINTLN("HOST_IS_CALLED                     = ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(NETWORK_GET_HOST_OF_THIS_SCRIPT())))  
	PRINTLN("iNumDMPlayers                      = ", serverBDpassed.iNumDMPlayers)
	PRINTLN("iNumActivePlayers                  = ", serverBDpassed.iNumActivePlayers)
	PRINTLN("TARGET                            = ", serverBDPassed.iTarget)
	
	PRINTLN("iWinningTeam                       = ", serverBDpassed.iWinningTeam)
	PRINTLN("iSecondTeam                        = ", serverBDpassed.iSecondTeam)
	PRINTLN("iThirdTeam                         = ", serverBDpassed.iThirdTeam)
	PRINTLN("iFourthTeam                        = ", serverBDpassed.iFourthTeam)
	PRINTLN("iLosingTeam                        = ", serverBDpassed.iLosingTeam)
	PRINTLN("serverBDpassed.iBestTeamOrder[0]   = ", serverBDpassed.iBestTeamOrder[0])
	PRINTLN("serverBDpassed.iBestTeamOrder[1]   = ", serverBDpassed.iBestTeamOrder[1])
	PRINTLN("serverBDpassed.iBestTeamOrder[2]   = ", serverBDpassed.iBestTeamOrder[2])
	PRINTLN("serverBDpassed.iBestTeamOrder[3]   = ", serverBDpassed.iBestTeamOrder[3])
	PRINTLN("iNumberOfTeams                     = ", serverBDpassed.iNumberOfTeams)
	PRINTLN("iNumActiveTeams                    = ", serverBDpassed.iNumActiveTeams)			

	
	PRINTLN("iNumWinners                        = ", serverBDpassed.iNumWinners)
	PRINTLN("iTeam2Count                        = ", serverBDpassed.iTeam2Count)
	PRINTLN("iTeam3Count                        = ", serverBDpassed.iTeam3Count)
	PRINTLN("iTeam4Count                        = ", serverBDpassed.iTeam4Count)
	
	INT i
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF (serverBDpassedLDB.leaderBoard[i].playerID <> INVALID_PLAYER_INDEX())
			PRINTLN(" [LBD_NAMES] i = ", i, "  Name = ", GET_PLAYER_NAME(serverBDpassedLDB.leaderBoard[i].playerID))
		ENDIF
		PRINTLN(" [LBD_NAMES] serverBDpassed i = ", i, "  tl63_ParticipantNames = ", serverBDpassed.tl63_ParticipantNames[i])
	ENDREPEAT
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF (serverBDpassedLDB.leaderBoard[i].playerID <> INVALID_PLAYER_INDEX())
			PRINTNL()
			PRINTLN("-------------------------------- -------------------------------- --------------------------------  ")
			PRINTLN("NAME                               = ", GET_PLAYER_NAME(serverBDpassedLDB.leaderBoard[i].playerID))

			PRINTLN("i                                  = ", i)
			PRINTLN("iKills                             = ", serverBDpassedLDB.leaderBoard[i].iKills)
			PRINTLN("iScore                             = ", serverBDpassedLDB.leaderBoard[i].iScore)
			PRINTLN("iFinishedAtTime                    = ", serverBDpassedLDB.leaderBoard[i].iFinishTime)
			PRINTLN("iDeaths                            = ", serverBDpassedLDB.leaderBoard[i].iDeaths)
			
			PRINTLN("iArenaPoints                       = ", serverBDpassedLDB.leaderBoard[i].iArenaPoints)
			PRINTLN("iJobXP                           	= ", serverBDpassedLDB.leaderBoard[i].iJobXP)
			PRINTLN("iJobCash                           = ", serverBDpassedLDB.leaderBoard[i].iJobCash)
			PRINTLN("iBets                           	= ", serverBDpassedLDB.leaderBoard[i].iBets)
			
			PRINTLN("bFinishedDeathmatch                = ", IS_BIT_SET(serverBDpassedLDB.leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)) //serverBDpassedLDB.leaderBoard[i].bFinished) 
			PRINTLN("bEnteredDM                         = ", IS_BIT_SET(serverBDpassedLDB.leaderBoard[i].iLbdBitSet, ciLBD_BIT_STARTED)) //serverBDpassedLDB.leaderBoard[i].bStarted)
			
			PRINTLN("KD_RATIO                           = ", serverBDpassedLDB.leaderBoard[i].fKDRatio)
			PRINTLN("iRank                              = ", serverBDpassedLDB.leaderBoard[i].iRank)
			PRINTLN("iBadgeRank                         = ", serverBDpassedLDB.leaderBoard[i].iBadgeRank)
			PRINTLN("iTeam                              = ", serverBDpassedLDB.leaderBoard[i].iTeam)
			PRINTLN("-------------------------------- -------------------------------- --------------------------------  ")
			PRINTNL()
		ELSE
			PRINTLN("INVALID 		= ", i)
		ENDIF
	ENDREPEAT
	
	PRINTLN("END PRINT_DEATHMATCH_DETAILS_BEFORE_LEADERBOARD_STAGE -------------------------------- ")
ENDPROC

PROC INCREMENT_SCORE_DEBUG()
	g_iMyDMScore++
	PRINTLN("INCREMENT_SCORE_DEBUG ", g_iMyDMScore)
ENDPROC

PROC DECREMENT_SCORE_DEBUG()
	IF g_iMyDMScore > 0
		g_iMyDMScore--
		PRINTLN("DECREMENT_SCORE_DEBUG ", g_iMyDMScore)
	ENDIF
ENDPROC

//PURPOSE: Process someone requesting to pass or fail the mission
PROC PROCESS_DEATHMATCH_DEBUG_KEY_PRESSES(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	// Someone pressed S 
	IF playerBDPassed[iLocalPart].bPressedS = TRUE
		dmVarsPassed.debugDmVars.bPassTriggered = TRUE				
	ENDIF
	// Someone pressed F
	IF playerBDPassed[iLocalPart].bPressedF = TRUE
		dmVarsPassed.debugDmVars.bFailTriggered = TRUE
	ENDIF 

	// Triggering the pass fail 
	IF dmVarsPassed.debugDmVars.bPassTriggered = FALSE
	AND dmVarsPassed.debugDmVars.bFailTriggered = FALSE
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass") 
			NET_PRINT_TIME() NET_PRINT_STRINGS("---------->     IS_DEBUG_KEY_JUST_PRESSED KEY_S    <---------- ", tl31ScriptName) NET_NL()
			playerBDPassed[iLocalPart].bPressedS = TRUE
			dmVarsPassed.debugDmVars.bPassTriggered = TRUE
				IF IS_PLAYER_ON_BIKER_DM()
					TRIGGER_MUSIC_EVENT("BIKER_MP_MUSIC_FAIL")
					PRINTLN("[DM_AUDIO] BIKER_MP_MUSIC_FAIL ")
				ENDIF
		ENDIF
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail") 
			NET_PRINT_TIME() NET_PRINT_STRINGS("---------->     IS_DEBUG_KEY_JUST_PRESSED KEY_F    <---------- ", tl31ScriptName) NET_NL()
			playerBDPassed[iLocalPart].bPressedF = TRUE
			dmVarsPassed.debugDmVars.bFailTriggered = TRUE
		ENDIF
	ENDIF
	
	// Check clients for debug time skip
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "Trigger Server Launch")
		NET_PRINT_TIME() NET_PRINT_STRINGS("---------->     IS_DEBUG_KEY_JUST_PRESSED KEY_0    <---------- ", tl31ScriptName) NET_NL()
		playerBDPassed[iLocalPart].pressedTimeSkip = TRUE
	ENDIF
	
	// Increment score via debug
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_ADD, KEYBOARD_MODIFIER_NONE, "Debug Increment Kills")
		IF dmVarsPassed.debugDmVars.iIncrementDelayDebug < GET_GAME_TIMER()
			dmVarsPassed.debugDmVars.iIncrementDelayDebug = (GET_GAME_TIMER() + 250)
	
			NET_PRINT_TIME() NET_PRINT_STRINGS("---------->     IS_DEBUG_KEY_JUST_PRESSED SCORE++    <---------- ", tl31ScriptName) NET_NL()
			IF dmVarsPassed.debugDmVars.bDebugSwitch = FALSE
			
				PRINT_TICKER_WITH_PLAYER_NAME("DM_CHEAT_K", LocalPlayer)
			
				IF playerBDPassed[iLocalPart].iScore < GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam) 
					dmVarsPassed.debugDmVars.bDebugSwitch = TRUE
					INCREMENT_SCORE_DEBUG()
					INCREMENT_KILLS()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		dmVarsPassed.debugDmVars.bDebugSwitch = FALSE
	ENDIF
	
	// Decrement score via debug
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SUBTRACT, KEYBOARD_MODIFIER_NONE, "Debug Decrement Kills")
		IF dmVarsPassed.debugDmVars.iIncrementDelayDebug < GET_GAME_TIMER()
			dmVarsPassed.debugDmVars.iIncrementDelayDebug = (GET_GAME_TIMER() + 250)
	
			NET_PRINT_TIME() NET_PRINT_STRINGS("---------->     IS_DEBUG_KEY_JUST_PRESSED SCORE--    <---------- ", tl31ScriptName) NET_NL()
			IF dmVarsPassed.debugDmVars.bDebugSwitch = FALSE
				IF playerBDPassed[iLocalPart].iScore < GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam) 
					dmVarsPassed.debugDmVars.bDebugSwitch = TRUE
					DECREMENT_SCORE_DEBUG()
					DECREMENT_KILLS()
				ENDIF
			ENDIF
		ENDIF
	ELSE
		dmVarsPassed.debugDmVars.bDebugSwitch = FALSE
	ENDIF
ENDPROC

FUNC INT GET_TIME_VALUE_TO_CHOP_OFF_DEBUG(ServerBroadcastData &serverBDpassed)
	SWITCH serverBDPassed.iDuration
		CASE DM_DURATION_5	RETURN 	270000	BREAK 
		CASE DM_DURATION_10	RETURN	570000	BREAK 
		CASE DM_DURATION_15	RETURN	870000	BREAK 
		CASE DM_DURATION_20	RETURN	1170000	BREAK 
		CASE DM_DURATION_30	RETURN	1770000	BREAK
		CASE DM_DURATION_45	RETURN	2670000	BREAK
		CASE DM_DURATION_60	RETURN	3570000	BREAK
		DEFAULT
		#IF IS_DEBUG_BUILD 
			SCRIPT_ASSERT("Deathmatch: GET_TIME_VALUE_TO_CHOP_OFF_DEBUG") 
		#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 570000
ENDFUNC

PROC HOLD_NUM_7_TO_PRINT_CLIENT_STAGE(PlayerBroadcastData &playerBDPassed[])
	IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD7, KEYBOARD_MODIFIER_NONE, "Print Client Stage")
	
		SWITCH GET_CLIENT_MISSION_STAGE(playerBDPassed)
			CASE CLIENT_MISSION_STAGE_INIT	
				NET_PRINT("CLIENT_MISSION_STAGE_INIT")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING
				NET_PRINT("CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_HOLDING	
				NET_PRINT("CLIENT_MISSION_STAGE_HOLDING")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SETUP	
				NET_PRINT("CLIENT_MISSION_STAGE_SETUP")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_FIGHTING
				NET_PRINT("CLIENT_MISSION_STAGE_FIGHTING")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_DEAD	
				NET_PRINT("CLIENT_MISSION_STAGE_DEAD")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_RESPAWN	
				NET_PRINT("CLIENT_MISSION_STAGE_RESPAWN")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
				NET_PRINT("CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_CELEBRATION
				NET_PRINT("CLIENT_MISSION_STAGE_CELEBRATION")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_LBD_CAM
				NET_PRINT("CLIENT_MISSION_STAGE_LBD_CAM")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SOCIAL_LBD
				NET_PRINT("CLIENT_MISSION_STAGE_SOCIAL_LBD")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_RESULTS
				NET_PRINT("CLIENT_MISSION_STAGE_RESULTS")NET_NL()	
			BREAK
			CASE CLIENT_MISSION_STAGE_LEADERBOARD
				NET_PRINT("CLIENT_MISSION_STAGE_LEADERBOARD")NET_NL()	
			BREAK
			CASE CLIENT_MISSION_STAGE_PICK
				NET_PRINT("CLIENT_MISSION_STAGE_PICK")NET_NL()	
			BREAK
			CASE CLIENT_MISSION_STAGE_SCTV
				NET_PRINT("CLIENT_MISSION_STAGE_SCTV")NET_NL()	
			BREAK
			CASE CLIENT_MISSION_STAGE_END	
				NET_PRINT("CLIENT_MISSION_STAGE_END")NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SPECTATOR	
				NET_PRINT("CLIENT_MISSION_STAGE_SPECTATOR")NET_NL()
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC HOLD_NUM_8_TO_PRINT_SERVER_STATE(ServerBroadcastData &serverBDpassed)
	IF bIsLocalPlayerHost
		IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD8, KEYBOARD_MODIFIER_NONE, "8 Server State")
			SWITCH GET_SERVER_GAME_STATE(serverBDpassed)
				CASE GAME_STATE_INI 	
					NET_PRINT("GAME_STATE_INI")NET_NL()
				BREAK
				CASE GAME_STATE_RUNNING	
					NET_PRINT("GAME_STATE_RUNNING")NET_NL()
				BREAK
				CASE GAME_STATE_LEAVE
					NET_PRINT("GAME_STATE_LEAVE")NET_NL()
				BREAK
				CASE GAME_STATE_END
					NET_PRINT("GAME_STATE_END")NET_NL()
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

// The server handles the mission timers
PROC SERVER_DEBUG_TIME_DOWN(ServerBroadcastData &serverBDpassed)

	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_DEBUG_TIME_DOWN run on a client, host only ")
	IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
		// Debug time down to 30 seconds if someone presses timeskips
		IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_TIME_SKIP)
			IF NOT IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_DEBUGGED_TIME)
												
				SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(serverBDpassed.timeServerMainDeathmatchTimer, 
													GET_TIME_OFFSET(serverBDpassed.timeServerMainDeathmatchTimer.Timer, - GET_TIME_VALUE_TO_CHOP_OFF_DEBUG(serverBDpassed)))
				
				SET_BIT(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_DEBUGGED_TIME)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DEBUG_HIDE_HUD(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_O, KEYBOARD_MODIFIER_NONE, "")
		IF dmVarsPassed.debugDmVars.bHideDMHud = FALSE
			dmVarsPassed.debugDmVars.bHideDMHud = TRUE
		ELSE
			dmVarsPassed.debugDmVars.bHideDMHud = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_DDS_MOCK_UP(LEADERBOARD_PLACEMENT_TOOLS& Placement, ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	UNUSED_PARAMETER(serverBDpassed)
	IF dmVarsPassed.debugDmVars.bDebugDrawDDSTexture = TRUE
		REQUEST_STREAMED_TEXTURE_DICT("MPOverview")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPOverview")
	
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].x = DDS_X
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].y = DDS_Y
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].w = DDS_W
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].h = DDS_H
//				Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].a = DDS_a

//				IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
//					DRAW_2D_SPRITE("MPOverview", "DMLeaderboard_PixelPerfect", Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS])
//				ELSE
//					DRAW_2D_SPRITE("MPOverview", "FreeforAll DMLeaderboard_PixelPerfect", Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS])
//				ENDIF		
			
			IF dmVarsPassed.debugDmVars.bWidthsGuide
				DRAW_2D_SPRITE("MPOverview", "GTARaceLeaderboard_GUIDE_PP", Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS])
			ELSE
				DRAW_2D_SPRITE("MPOverview", "GTARaceLeaderboard_PP", Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_DPAD_DDS_MOCK_UP(LEADERBOARD_PLACEMENT_TOOLS& Placement, ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	UNUSED_PARAMETER(serverBDpassed)
	IF dmVarsPassed.debugDmVars.bDrawDpadDDS = TRUE
		REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard")
	
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].x = DPAD_DDS_X
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].y = DPAD_DDS_Y
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].w = DPAD_DDS_W
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].h = DPAD_DDS_H
			Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS].a = DPAD_DDS_A
	
			DRAW_2D_SPRITE("MPLeaderboard", "DPad_Down_Deathmatch_PP", Placement.SpritePlacement[SPRITE_PLACEMENT_LB_DDS])		
		ENDIF
	ENDIF
ENDPROC

PROC DEBUG_DISPLAY_BIG_LEADERBOARD(LEADERBOARD_PLACEMENT_TOOLS& Placement, ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	INT i, iSlotCounter, iRow, iTeamRow		
	INT iPlayerTeam[NUM_NETWORK_PLAYERS]
	INT iTeamSlotCounter[NUM_NETWORK_PLAYERS]
	BOOL bDraw[MAX_NUM_DM_TEAMS]
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_3, KEYBOARD_MODIFIER_ALT, "DEBUG")
		IF dmVarsPassed.debugDmVars.bDrawBIGLeaderboard = TRUE
			PRINTLN("DEBUG_DISPLAY_BIG_LEADERBOARD, OFF ")
			dmVarsPassed.debugDmVars.bDrawBIGLeaderboard = FALSE
		ELSE
			PRINTLN("DEBUG_DISPLAY_BIG_LEADERBOARD, ON ")
			dmVarsPassed.debugDmVars.bDrawBIGLeaderboard = TRUE
		ENDIF
	ENDIF

	IF dmVarsPassed.debugDmVars.bDrawBIGLeaderboard = TRUE
	
		// Draw the leaderboard
		RENDER_LEADERBOARD(dmVarsPassed.lbdVars, Placement, GET_SUB_MODE(serverBDpassed), TRUE,	GET_LEADERBOARD_TITLE(GET_SUB_MODE(serverBDpassed), g_FMMC_STRUCT.tl63MissionName), dmVarsPassed.debugDmVars.iNumColumns, -1, TRUE)
		DRAW_DDS_MOCK_UP(Placement, serverBDpassed,dmVarsPassed)
		
		IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)								
			REPEAT NUM_NETWORK_PLAYERS i
			
				dmVarsPassed.debugDmVars.tl63_FakeParticipantNames[i] = GET_NAME_FOR_FAKE_LBD(i)
				iPlayerTeam[i] = dmVarsPassed.debugDmVars.iPlyerTeam[i]
				dmVarsPassed.lbdVars.tlRaceVeh = "DOMINATOR4_S"
				
				IF iPlayerTeam[i] < 17
	
					iRow = GET_ROW_TO_DRAW_PLAYERS(iPlayerTeam[i], dmVarsPassed.debugDmVars.iNumTeams, iTeamSlotCounter[iPlayerTeam[i]],
												dmVarsPassed.debugDmVars.iTeamWinCount, 
												dmVarsPassed.debugDmVars.iTeam2Count, 
												dmVarsPassed.debugDmVars.iTeam3Count, 
												dmVarsPassed.debugDmVars.iTeam4Count, 
												dmVarsPassed.debugDmVars.iTeam5Count, 
												dmVarsPassed.debugDmVars.iTeam6Count, 
												dmVarsPassed.debugDmVars.iTeam7Count,
												
												dmVarsPassed.debugDmVars.iWinningTeam,
												dmVarsPassed.debugDmVars.iSecondTeam,
												dmVarsPassed.debugDmVars.iThirdTeam,
												dmVarsPassed.debugDmVars.iFourthTeam,
												dmVarsPassed.debugDmVars.iFifthTeam,
												dmVarsPassed.debugDmVars.iSixthTeam,
												dmVarsPassed.debugDmVars.iSeventhTeam,
												dmVarsPassed.debugDmVars.iLosingTeam, FALSE, FALSE)
					// DEBUG*************							
					iTeamRow = GET_TEAM_BARS_ROW(iPlayerTeam[i], dmVarsPassed.debugDmVars.iNumTeams,
												dmVarsPassed.debugDmVars.iTeamWinCount, dmVarsPassed.debugDmVars.iTeam2Count, dmVarsPassed.debugDmVars.iTeam3Count, 
												dmVarsPassed.debugDmVars.iTeam4Count, dmVarsPassed.debugDmVars.iTeam5Count, dmVarsPassed.debugDmVars.iTeam6Count, dmVarsPassed.debugDmVars.iTeam7Count, 0,
												dmVarsPassed.debugDmVars.iWinningTeam, dmVarsPassed.debugDmVars.iSecondTeam, dmVarsPassed.debugDmVars.iThirdTeam, 
												dmVarsPassed.debugDmVars.iFourthTeam, dmVarsPassed.debugDmVars.iFifthTeam, dmVarsPassed.debugDmVars.iSixthTeam, dmVarsPassed.debugDmVars.iSeventhTeam, dmVarsPassed.debugDmVars.iLosingTeam, FALSE)
					
					TRACK_LBD_ROW_FOR_SCROLL(dmVarsPassed.lbdVars, dmVarsPassed.debugDmVars.iNumTeams, serverBDpassed.iNumDMPlayers)
					
					POPULATE_COLUMN_LBD(dmVarsPassed.lbdVars,
										Placement,
										GET_SUB_MODE(serverBDpassed),
										LBD_VOTE_NOT_USED,
										dmVarsPassed.debugDmVars.tl63_FakeParticipantNames,
										dmVarsPassed.debugDmVars.iNumColumns,
										dmVarsPassed.debugDmVars.fKDRatio,
										dmVarsPassed.debugDmVars.iVar1,											//1 DEBUG
										dmVarsPassed.debugDmVars.iVar2,											//2
										dmVarsPassed.debugDmVars.iVar3,// car									//3
										dmVarsPassed.debugDmVars.iVar4,											//4
										dmVarsPassed.debugDmVars.iVar5,											//5
										dmVarsPassed.debugDmVars.iVar6,// winningx								//6
										0,
										0,
										iRow,
										dmVarsPassed.debugDmVars.iRank,
										i,
										dmVarsPassed.debugDmVars.iRank,
										INT_TO_PLAYERINDEX(i),
										TRUE,
										bDraw[iPlayerTeam[i]],
										dmVarsPassed.debugDmVars.iTeamScore1,
										iTeamRow)
													
					iTeamSlotCounter[iPlayerTeam[i]] ++		
				
				ENDIF
			ENDREPEAT
		ELSE
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
	
				dmVarsPassed.debugDmVars.tl63_FakeParticipantNames[i] = GET_NAME_FOR_FAKE_LBD(i)
				dmVarsPassed.lbdVars.tlRaceVeh = "DOMINATOR4_S"
		
				POPULATE_COLUMN_LBD(dmVarsPassed.lbdVars,
									Placement,
									GET_SUB_MODE(serverBDpassed),
									LBD_VOTE_NOT_USED,
									dmVarsPassed.debugDmVars.tl63_FakeParticipantNames,
									dmVarsPassed.debugDmVars.iNumColumns,
									dmVarsPassed.debugDmVars.fRatio,
									dmVarsPassed.debugDmVars.iVar1,											//1 DEBUG
									dmVarsPassed.debugDmVars.iVar2,											//2
									dmVarsPassed.debugDmVars.iVar3,// car									//3
									dmVarsPassed.debugDmVars.iVar4,											//4
									dmVarsPassed.debugDmVars.iVar5,											//5
									dmVarsPassed.debugDmVars.iVar6,// winningx								//6
									0,
									0,
									iSlotCounter,
									dmVarsPassed.debugDmVars.iRank,
									i,
									dmVarsPassed.debugDmVars.iBadgeRank,
									INT_TO_PLAYERINDEX(i),
									FALSE,
									bDraw[0],
									-1,
									iTeamRow)
									
				iSlotCounter++		
			ENDREPEAT
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DM_DEBUG_DISPLAY(SHARED_DM_VARIABLES &dmVarsPassed)

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_D, KEYBOARD_MODIFIER_CTRL_SHIFT, "NO")
		IF g_debugDisplayState = DEBUG_DISP_OFF
			g_debugDisplayState = DEBUG_DISP_LBD
			PRINTLN("g_debugDisplayState = DEBUG_DISP_LBD")
		ELSE
			g_debugDisplayState = DEBUG_DISP_OFF
			PRINTLN("g_debugDisplayState = DEBUG_DISP_OFF")
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_Z, KEYBOARD_MODIFIER_CTRL_SHIFT, "NO")
		g_debugDisplayState = DEBUG_DISP_REPLAY
		g_SkipCelebAndLbd = TRUE
	ENDIF
	
	SWITCH g_debugDisplayState 
	
		CASE DEBUG_DISP_OFF
			IF dmVarsPassed.debugDmVars.bRenderServerLeaderBoard
				dmVarsPassed.debugDmVars.bRenderServerLeaderBoard = FALSE
				bSpewLeaderboardInfo = FALSE
				bShowPlaylistLbdDebug = FALSE
			ENDIF
		BREAK
		
		CASE DEBUG_DISP_LBD
			IF NOT dmVarsPassed.debugDmVars.bRenderServerLeaderBoard
				dmVarsPassed.debugDmVars.bRenderServerLeaderBoard = TRUE
				bSpewLeaderboardInfo = TRUE
				IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
					bShowPlaylistLbdDebug = TRUE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	

ENDPROC

PROC DEBUG_RENDER_SERVER_LEADERBOARD(ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	INT i
	FLOAT fDrawX, fDrawY
	INT iRows
	INT r,g,b,a
	INT iCol
	
	// Debug starting positions
	FLOAT fWindowX = 0.318
	FLOAT fWindowY = 0.472

	// Black box values
	FLOAT fBlackX = 0.370
	FLOAT fBlackY = 0.654
	FLOAT fBlackW = 0.315
	FLOAT fBlackH = 0.551
	
	FLOAT colW = 0.38
	
	// Shrink the box by the number of empty rows
	INT emptyRows = NUM_NETWORK_PLAYERS - (serverBDpassed.iNumActivePlayers + 1)
	FLOAT emptySpace = g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(emptyRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	fWindowY += emptySpace
	fBlackY += emptySpace*0.5
	fBlackH -= emptySpace
	
	// draw black box
	DRAW_RECT(fBlackX, fBlackY, fBlackW, fBlackH, 0, 0, 0, 128)
	
	// HEADINGS ///////////////////////////////////////////////////////////////////////
	
	// draw title
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Server Leader Board", HUD_COLOUR_WHITE)
	
	// player ID title
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "PlyId", HUD_COLOUR_YELLOW)	
	iCol += 1
	
	// part id title
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "PrtId", HUD_COLOUR_PINK)		
	iCol += 1
	
	// score
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_ORANGE, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Score", HUD_COLOUR_ORANGE)		
	iCol += 1	
	
	// finish time
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_GREEN, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "iTime", HUD_COLOUR_GREEN)		
	iCol += 1	
	
	// Team
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_RED, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Team", HUD_COLOUR_RED)		
	iCol += 1	
	
	// Team Score
	fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol))
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "TmScore", HUD_COLOUR_WHITE)		
	iCol += 1	
	
	iRows += 1	
	
	// POPULATE /////////////////////////////////////////////////////////////////////////////
	
	PLAYER_INDEX playerId
	
	BOOL bTeamMode = IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	HUD_COLOURS playerHudColour = HUD_COLOUR_BLUE
	
	REPEAT COUNT_OF(serverBDpassedLDB.leaderBoard) i
	
		playerId = serverBDpassedLDB.leaderBoard[i].playerID
		IF IS_NET_PLAYER_OK(playerId, FALSE)
		AND serverBDpassedLDB.leaderBoard[i].iParticipant != -1
		
			fDrawX = fWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
			fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
					
			// player name
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)			
			IF IS_NET_PLAYER_OK(playerID, FALSE)
				IF bTeamMode
					playerHudColour = GET_PLAYER_HUD_COLOUR(playerID)
				ENDIF
				DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", GET_PLAYER_NAME(playerID), playerHudColour)
			ELSE
				DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "invalid", HUD_COLOUR_GREY)
			ENDIF
					
			// other details
			iCol = 0
			GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)	
			
			// player id
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", NATIVE_TO_INT(playerId))
			iCol += 1
			
			// participant id
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", serverBDpassedLDB.leaderBoard[i].iParticipant)
			iCol += 1
			
			// score
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_ORANGE, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", serverBDpassedLDB.leaderBoard[i].iScore)
			iCol += 1
			
			// finish time
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_GREEN, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", serverBDpassedLDB.leaderBoard[i].iFinishTime)
			iCol += 1
			
			// team
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_RED, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", serverBDpassedLDB.leaderBoard[i].iTeam)
			iCol += 1
			
			// team score
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX + (g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX + ((g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX * colW) * iCol)), fDrawY, "NUMBR", GET_TEAM_DM_SCORE(serverBDpassed, serverBDpassedLDB.leaderBoard[i].iTeam))
			iCol += 1
			
			iRows += 1
		ENDIF

	ENDREPEAT
ENDPROC

#ENDIF
