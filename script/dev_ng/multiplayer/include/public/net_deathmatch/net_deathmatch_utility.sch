USING "net_deathmatch_using.sch"

// -----------------------------------	STAGE FUNCTIONS

PROC PRINT_CLIENT_LOGIC_STAGE(CLIENT_MISSION_STAGE localstage)
	SWITCH localstage		
		CASE CLIENT_MISSION_STAGE_INIT 					NET_PRINT("CLIENT_MISSION_STAGE_INIT  ")				BREAK
		CASE CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING 	NET_PRINT("CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING  ")	BREAK
		CASE CLIENT_MISSION_STAGE_HOLDING 				NET_PRINT("CLIENT_MISSION_STAGE_HOLDING  ")				BREAK
		CASE CLIENT_MISSION_STAGE_SETUP 				NET_PRINT("CLIENT_MISSION_STAGE_SETUP  ")				BREAK
		CASE CLIENT_MISSION_STAGE_FIGHTING				NET_PRINT("CLIENT_MISSION_STAGE_FIGHTING   ")			BREAK
		CASE CLIENT_MISSION_STAGE_DEAD					NET_PRINT("CLIENT_MISSION_STAGE_DEAD  ")				BREAK
		CASE CLIENT_MISSION_STAGE_RESPAWN				NET_PRINT("CLIENT_MISSION_STAGE_RESPAWN  ")				BREAK
		CASE CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE		NET_PRINT("CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE  ")		BREAK
		CASE CLIENT_MISSION_STAGE_CONOR_LBD				NET_PRINT("CLIENT_MISSION_STAGE_CONOR_LBD  ")			BREAK
		CASE CLIENT_MISSION_STAGE_CELEBRATION			NET_PRINT("CLIENT_MISSION_STAGE_CELEBRATION  ")			BREAK
		CASE CLIENT_MISSION_STAGE_LBD_CAM				NET_PRINT("CLIENT_MISSION_STAGE_LBD_CAM  ")				BREAK
		CASE CLIENT_MISSION_STAGE_SOCIAL_LBD			NET_PRINT("CLIENT_MISSION_STAGE_SOCIAL_LBD  ")			BREAK
		CASE CLIENT_MISSION_STAGE_RESULTS				NET_PRINT("CLIENT_MISSION_STAGE_RESULTS  ")				BREAK
		CASE CLIENT_MISSION_STAGE_LEADERBOARD			NET_PRINT("CLIENT_MISSION_STAGE_LEADERBOARD  ")			BREAK
		CASE CLIENT_MISSION_STAGE_PICK					NET_PRINT("CLIENT_MISSION_STAGE_PICK  ")				BREAK
		CASE CLIENT_MISSION_STAGE_SCTV					NET_PRINT("CLIENT_MISSION_STAGE_SCTV  ")				BREAK
		CASE CLIENT_MISSION_STAGE_SPECTATOR				NET_PRINT("CLIENT_MISSION_STAGE_SPECTATOR  ")			BREAK
		CASE CLIENT_MISSION_STAGE_END					NET_PRINT("CLIENT_MISSION_STAGE_END  ")					BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_CLIENT_GAME_STATE(INT iLocalState)
	SWITCH iLocalState
		CASE GAME_STATE_INI 	 	NET_PRINT("GAME_STATE_INI  ")		 	BREAK
		CASE GAME_STATE_RUNNING		NET_PRINT("GAME_STATE_RUNNING  ")		BREAK
		CASE GAME_STATE_LEAVE		NET_PRINT("GAME_STATE_LEAVE  ")			BREAK
		CASE GAME_STATE_END			NET_PRINT("GAME_STATE_END  ")			BREAK
	ENDSWITCH
ENDPROC

PROC PRINT_SERVER_GAME_STATE(INT iLocalServerState)
	SWITCH iLocalServerState
		CASE GAME_STATE_INI 	 	NET_PRINT("GAME_STATE_INI  ")		 	BREAK
		CASE GAME_STATE_RUNNING		NET_PRINT("GAME_STATE_RUNNING  ")		BREAK
		CASE GAME_STATE_LEAVE		NET_PRINT("GAME_STATE_LEAVE  ")			BREAK
		CASE GAME_STATE_END			NET_PRINT("GAME_STATE_END  ")			BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Gets the local players client mission stage.
FUNC CLIENT_MISSION_STAGE GET_CLIENT_MISSION_STAGE(PlayerBroadcastData &playerBDPassed[])

	RETURN playerBDPassed[iLocalPart].eClientLogicStage
ENDFUNC

/// PURPOSE:
///    Sets the client mission stage to stage eStageToGoTo.
PROC GOTO_CLIENT_MISSION_STAGE(PlayerBroadcastData &playerBDPassed[], CLIENT_MISSION_STAGE eStageToGoTo)
	#IF IS_DEBUG_BUILD
		SWITCH eStageToGoTo
			CASE CLIENT_MISSION_STAGE_INIT
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_INIT  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_HOLDING
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_HOLDING  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SETUP
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_SETUP  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_FIGHTING
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_FIGHTING  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_DEAD
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_DEAD  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_RESPAWN
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_RESPAWN  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_CONOR_LBD
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_CONOR_LBD  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_CELEBRATION
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_CELEBRATION  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_LBD_CAM
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_LBD_CAM  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SOCIAL_LBD
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_SOCIAL_LBD  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_RESULTS
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_RESULTS  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_LEADERBOARD
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_LEADERBOARD  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_PICK
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_PICK  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SCTV
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_SCTV  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_SPECTATOR
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_SPECTATOR  ", tl31ScriptName) NET_NL()
			BREAK
			CASE CLIENT_MISSION_STAGE_END
				NET_PRINT("Previous Mission Stage = ")
				PRINT_CLIENT_LOGIC_STAGE(GET_CLIENT_MISSION_STAGE(playerBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_CLIENT_MISSION_STAGE  >>> CLIENT_MISSION_STAGE_END  ", tl31ScriptName) NET_NL()
			BREAK
			DEFAULT
				SCRIPT_ASSERT("GOTO_CLIENT_MISSION_STAGE Invalid option net_deathmatch")
			BREAK
		ENDSWITCH
	#ENDIF

	playerBDPassed[iLocalPart].eClientLogicStage = eStageToGoTo
ENDPROC

/// PURPOSE:
///    Helper function to get a clients game state
FUNC INT GET_CLIENT_GAME_STATE(PlayerBroadcastData &playerBDPassed[], INT iPlayer)

	RETURN playerBDPassed[iPlayer].iGameState
ENDFUNC

PROC SET_CLIENT_GAME_STATE(PlayerBroadcastData &playerBDPassed[], INT iState)
	#IF IS_DEBUG_BUILD
		SWITCH iState
			CASE GAME_STATE_INI 	
				NET_PRINT("Previous Game State = ")
				PRINT_CLIENT_GAME_STATE(GET_CLIENT_GAME_STATE(playerBDPassed, iLocalPart))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> GAME_STATE_INI  ",tl31ScriptName) NET_NL()
			BREAK
			CASE GAME_STATE_RUNNING	
				NET_PRINT("Previous Game State = ")
				PRINT_CLIENT_GAME_STATE(GET_CLIENT_GAME_STATE(playerBDPassed, iLocalPart))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> GAME_STATE_RUNNING  ",tl31ScriptName) NET_NL()
			BREAK
			CASE GAME_STATE_LEAVE
				NET_PRINT("Previous Game State = ")
				PRINT_CLIENT_GAME_STATE(GET_CLIENT_GAME_STATE(playerBDPassed, iLocalPart))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> GAME_STATE_LEAVE  ",tl31ScriptName) NET_NL()
			BREAK
			CASE GAME_STATE_END		
				NET_PRINT("Previous Game State = ")
				PRINT_CLIENT_GAME_STATE(GET_CLIENT_GAME_STATE(playerBDPassed, iLocalPart))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_CLIENT_GAME_STATE  >>> GAME_STATE_END  ",tl31ScriptName) NET_NL()
			BREAK
			DEFAULT
				SCRIPT_ASSERT("SET_CLIENT_GAME_STATE Invalid option DEATHMATCH")
			BREAK
		ENDSWITCH
	#ENDIF

	playerBDPassed[iLocalPart].iGameState = iState
ENDPROC

/// PURPOSE:
///    Helper function to get the servers game/mission state
FUNC INT GET_SERVER_GAME_STATE(ServerBroadcastData &serverBDPassed)

	RETURN serverBDPassed.iServerGameState
ENDFUNC	

PROC SET_SERVER_GAME_STATE(ServerBroadcastData &serverBDPassed, INT iStage)

	#IF IS_DEBUG_BUILD
		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SET_SERVER_GAME_STATE Called on client by mistake")
	
		SWITCH iStage
			CASE GAME_STATE_INI
				NET_PRINT("Previous Server State = ")
				PRINT_SERVER_GAME_STATE(GET_SERVER_GAME_STATE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> GAME_STATE_INI  ", tl31ScriptName) NET_NL()
			BREAK
			CASE GAME_STATE_RUNNING
				NET_PRINT("Previous Server State = ")
				PRINT_SERVER_GAME_STATE(GET_SERVER_GAME_STATE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> GAME_STATE_RUNNING  ", tl31ScriptName) NET_NL()
			BREAK
			CASE GAME_STATE_LEAVE
				NET_PRINT("Previous Server State = ")
				PRINT_SERVER_GAME_STATE(GET_SERVER_GAME_STATE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> GAME_STATE_LEAVE  ", tl31ScriptName) NET_NL()
			BREAK
			CASE GAME_STATE_END
				NET_PRINT("Previous Server State = ")
				PRINT_SERVER_GAME_STATE(GET_SERVER_GAME_STATE(serverBDPassed))
				NET_PRINT_TIME() NET_PRINT_STRINGS("SET_SERVER_GAME_STATE  >>> GAME_STATE_END  ", tl31ScriptName) NET_NL()
			BREAK
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("SET_SERVER_GAME_STATE Invalid option net_deathmatch")
				#ENDIF
			BREAK
		ENDSWITCH
	#ENDIF
	
	serverBDPassed.iServerGameState = iStage
ENDPROC

FUNC INT GET_LOAD_NEXT_MISSION_STAGE(PlayerBroadcastData &playerBDPassed[])

	RETURN playerBDPassed[iLocalPart].iLoadNextMissionProgress
ENDFUNC

PROC GOTO_LOAD_NEXT_MISSION_STAGE(PlayerBroadcastData &playerBDPassed[], INT iLoadStageGoto)
	#IF IS_DEBUG_BUILD
		SWITCH iLoadStageGoto
			CASE LOAD_MISSION_STAGE_SETUP 	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LOAD_NEXT_MISSION_STAGE  >>> LOAD_MISSION_STAGE_SETUP  ", tl31ScriptName) NET_NL() 	BREAK		
			CASE LOAD_MISSION_STAGE_LOAD 	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LOAD_NEXT_MISSION_STAGE  >>> LOAD_MISSION_STAGE_LOAD  ", tl31ScriptName) NET_NL() 		BREAK
			CASE LOAD_MISSION_STAGE_CHOOSE	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LOAD_NEXT_MISSION_STAGE  >>> LOAD_MISSION_STAGE_CHOOSE  ", tl31ScriptName) NET_NL() 	BREAK
			CASE LOAD_MISSION_STAGE_FINISH	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LOAD_NEXT_MISSION_STAGE  >>> LOAD_MISSION_STAGE_FINISH  ", tl31ScriptName) NET_NL() 	BREAK
		ENDSWITCH
	#ENDIF
	
	playerBDPassed[iLocalPart].iLoadNextMissionProgress = iLoadStageGoto
ENDPROC

FUNC BOOL HAS_SERVER_DONE_AT_LEAST_ONE_LBD_UPDATE(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_FINISHED_FIRST_UPDATE)
ENDFUNC


FUNC INT GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM()	
	IF IS_PLAYER_ON_BIKER_DM()
		RETURN g_sMPTunables.iBIKER_DEATHMATCH_LIVES                                               
	ENDIF
	RETURN g_sMPTunables.igb_bossvsbossdm_boss_lives
ENDFUNC	

FUNC BOOL HAS_DEATHMATCH_FINISHED(ServerBroadcastData &serverBDpassed) 
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_SERVER_SAYS_DM_ENDED)
ENDFUNC

FUNC BOOL HAS_DM_STARTED(ServerBroadcastData &serverBDPassed)

	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_DEATHMATCH_STARTED)
ENDFUNC

FUNC INT GET_INITIAL_STAGE(PlayerBroadcastData &playerBDPassed[])
	RETURN playerBDPassed[iLocalPart].iInitialFadeProgress 
ENDFUNC 

FUNC BOOL DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(PlayerBroadcastData &playerBDPassed[])
	RETURN IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Native Caching
// ##### Description: Helpers for caching natives to variables
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_CACHING_NETWORK_HOST()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		bIsLocalPlayerHost = NETWORK_IS_HOST_OF_THIS_SCRIPT()
	ENDIF
	
ENDPROC

PROC PROCESS_CACHING_LOCAL_PARTICIPANT_STATE(BOOL bPreGame = FALSE)
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF bPreGame
			iLocalPart = NATIVE_TO_INT(GET_PLAYER_INDEX())
		ELSE
			iLocalPart = PARTICIPANT_ID_TO_INT()
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_FAKE_MULTIPLAYER_MODE_SET()
		iLocalPart = 0
	ENDIF
	#ENDIF
	
	PRINTLN("PROCESS_CACHING_LOCAL_PARTICIPANT_STATE - iLocalPart: ", iLocalPart)
	
ENDPROC

PROC PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
	PLAYER_INDEX ThisLocalPlayer = PLAYER_ID()
	PED_INDEX ThisLocalPlayerPed = PLAYER_PED_ID()
	
	#IF IS_DEBUG_BUILD
		IF (ThisLocalPlayer != LocalPlayer)
			PRINTLN("PROCESS_CACHING_LOCAL_PLAYER_STATE - LocalPlayer has changed from ", NATIVE_TO_INT(LocalPlayer), " to ", NATIVE_TO_INT(ThisLocalPlayer))
		ENDIF
		IF (ThisLocalPlayerPed != LocalPlayerPed)
			PRINTLN("PROCESS_CACHING_LOCAL_PLAYER_STATE - LocalPlayerPed has changed from ", NATIVE_TO_INT(LocalPlayerPed), " to ", NATIVE_TO_INT(ThisLocalPlayerPed))
		ENDIF
	#ENDIF
	
	LocalPlayer = ThisLocalPlayer
	LocalPlayerPed = ThisLocalPlayerPed
	
	IF IS_NET_PLAYER_OK(LocalPlayer)
		bLocalPlayerOK = TRUE
		bLocalPlayerPedOK = NOT IS_PED_INJURED(LocalPlayerPed)
	ELSE
		bLocalPlayerOK = FALSE
		bLocalPlayerPedOK = FALSE
	ENDIF
	
	IF !bIsAnySpectator
		vLocalPlayerPosition = GET_ENTITY_COORDS(LocalPlayerPed, FALSE)
	ELSE
		vLocalPlayerPosition = <<0,0,0>>
	ENDIF
	
ENDPROC

PROC PROCESS_CACHING_LOCAL_PLAYER_SPECTATOR_STATE()

	IF IS_PLAYER_SCTV(LocalPlayer)
		bIsSCTV = TRUE
		bIsAnySpectator = TRUE
	ELSE
		bIsSCTV = FALSE
		bIsAnySpectator = IS_PLAYER_SPECTATING(LocalPlayer)
	ENDIF
	
	IF bIsAnySpectator
		PlayerPedToUse = GET_SPECTATOR_SELECTED_PED()
		IF IS_PED_ALLOWED_TO_BE_VIEWED_BY_SPEC_DATA(g_BossSpecData, PlayerPedToUse)
			IF IS_PED_A_PLAYER(PlayerPedToUse)
				PlayerToUse = NETWORK_GET_PLAYER_INDEX_FROM_PED(PlayerPedToUse)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerToUse)
					iPartToUse = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(PlayerToUse))
				ENDIF
				
				IF IS_NET_PLAYER_OK(PlayerToUse)
					bPlayerPedToUseOK = NOT IS_PED_INJURED(PlayerPedToUse)
				ELSE
					bPlayerPedToUseOK = FALSE
				ENDIF
				
			ENDIF
		ENDIF
	ELSE
		iPartToUse = iLocalPart
		PlayerToUse = LocalPlayer
		PlayerPedToUse = LocalPlayerPed
		bPlayerPedToUseOK = bLocalPlayerPedOK
	ENDIF
	
ENDPROC

PROC PROCESS_PRE_FRAME_LOCAL_PLAYER_CACHING()
	
	PROCESS_CACHING_NETWORK_HOST()
	
	PROCESS_CACHING_LOCAL_PLAYER_STATE()
	
	PROCESS_CACHING_LOCAL_PARTICIPANT_STATE()
	
	PROCESS_CACHING_LOCAL_PLAYER_SPECTATOR_STATE()
	
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: DM Type
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_THIS_TEAM_DEATHMATCH(ServerBroadcastData &serverBDPassed)
	IF serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_TDM
	OR serverBDPassed.iDeathmatchType = FMMC_KING_OF_THE_HILL_TEAM
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_FFA_DEATHMATCH(ServerBroadcastData &serverBDPassed)
	
	IF serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_FFA
	OR serverBDPassed.iDeathmatchType = FMMC_KING_OF_THE_HILL
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirty, ciOptionsBS30_EnableClassicDeathmatchFeatures)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_TDM_USING_FFA_SCORING()
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_FFAScoringInTDM) AND NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Leaderboard
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Return player index of player in position (0 for leader and so on)
FUNC PLAYER_INDEX GET_PLAYER_IN_LEADERBOARD_POS(ServerBroadcastData_Leaderboard &serverBDpassedLDB, INT iPos)

	#IF IS_DEBUG_BUILD
		IF iPos > NUM_NETWORK_PLAYERS
			SCRIPT_ASSERT("GET_PLAYER_IN_LEADERBOARD_POS() PROBLEM WITH iPos")
		ENDIF
	#ENDIF

	PLAYER_INDEX PlayerToGet
	
	// Added to fix 311089
	IF serverBDpassedLDB.leaderBoard[iPos].playerID <> INVALID_PLAYER_INDEX()
	
		PlayerToGet = serverBDpassedLDB.leaderBoard[iPos].playerID
	ENDIF
	
	RETURN PlayerToGet
ENDFUNC

FUNC INT GET_TEAM_RANK(ServerBroadcastData &serverBDpassed, INT iLocalTeam, INT iNumTeams)

	INT iTeamRank
		
	SWITCH iNumTeams
		CASE 4
			IF iLocalTeam = serverBDpassed.iWinningTeam		
				iTeamRank = 1
			ELIF iLocalTeam = serverBDpassed.iSecondTeam		
				iTeamRank = 2
			ELIF iLocalTeam = serverBDpassed.iThirdTeam	
				iTeamRank = 3
			ELIF iLocalTeam = serverBDpassed.iFourthTeam
				iTeamRank = 4
			ELIF iLocalTeam = serverBDpassed.iLosingTeam 	
				iTeamRank = 4
			ENDIF
		BREAK
		CASE 3
			IF iLocalTeam = serverBDpassed.iWinningTeam		
				iTeamRank = 1
			ELIF iLocalTeam = serverBDpassed.iSecondTeam		
				iTeamRank = 2
			ELIF iLocalTeam = serverBDpassed.iThirdTeam
				iTeamRank = 3	
			ELIF iLocalTeam = serverBDpassed.iLosingTeam 	
				iTeamRank = 3
			ENDIF
		BREAK
		CASE 2
			IF iLocalTeam = serverBDpassed.iWinningTeam		
				iTeamRank = 1
			ELIF iLocalTeam = serverBDpassed.iSecondTeam
				iTeamRank = 2
			ELIF iLocalTeam = serverBDpassed.iLosingTeam 	
				iTeamRank = 2
			ENDIF
		BREAK
		DEFAULT
			iTeamRank = 1
		BREAK
	ENDSWITCH	
		
	RETURN iTeamRank	
ENDFUNC

FUNC INT GET_LOCAL_PLAYER_LEADERBOARD_POSITION(ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	INT i, iRank

	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)		
		IF serverBDpassedLDB.leaderBoard[i].playerID = LocalPlayer
			iRank = serverBDpassedLDB.leaderBoard[i].iRank
			PRINTLN("GET_LOCAL_PLAYER_LEADERBOARD_POSITION GRAB iRank = ", iRank, " i = ", i)
			RETURN iRank
		#IF IS_DEBUG_BUILD
		ELSE
			IF serverBDpassedLDB.leaderBoard[i].playerID <> INVALID_PLAYER_INDEX()
				STRING sName = GET_PLAYER_NAME(serverBDpassedLDB.leaderBoard[i].playerID)
				PRINTLN("GET_LOCAL_PLAYER_LEADERBOARD_POSITION, i = ", NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[i].playerID), "   Name = ", sName, " rank = ", serverBDpassedLDB.leaderBoard[i].iRank)
			ENDIF
		#ENDIF
		
		ENDIF
	ENDFOR
	
	RETURN iRank
ENDFUNC

FUNC INT GET_PLAYER_FINISH_POS(ServerBroadcastData &serverBDpassed, INT iPlayer)
	INT iFinishPos = -1
	
	IF iPlayer <> -1
		iFinishPos = serverBDPassed.iFinishingPos[iPlayer]
	ENDIF
	
	RETURN iFinishPos	
ENDFUNC

FUNC LBD_SUB_MODE GET_SUB_MODE(ServerBroadcastData &serverBDpassed)

	IF g_FinalRoundsLbd
		RETURN SUB_FINAL_ROUNDS
	ENDIF
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
		RETURN SUB_DM_UPDATE
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN SUB_KART
	ENDIF

	IF IS_KING_OF_THE_HILL()
		RETURN SUB_KOTH
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN SUB_ARENA_PASSBOMB
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	AND CONTENT_IS_USING_ARENA()
		RETURN SUB_ARENA_CARNAGE
	ENDIF
	
	// UGC etc.
	IF CONTENT_IS_USING_ARENA() 
		RETURN SUB_ARENA_CARNAGE
	ENDIF

	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		RETURN SUB_DM_TEAM
	ENDIF
	
	RETURN SUB_DM_FFA
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Time
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INT GET_ROUND_DURATION(ServerBroadcastData &serverBDpassed)

	IF IS_PLAYER_ON_BIKER_DM()
		RETURN g_sMPTunables.iBIKER_DEATHMATCH_TIME_LIMIT   * 1000
	ENDIF
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		RETURN g_sMPTunables.igb_bossvsbossdm_time_limit
	ENDIF
	

	
	SWITCH serverBDPassed.iDuration
		CASE DM_DURATION_5	RETURN 	(1000*60*5)-1000	BREAK
		CASE DM_DURATION_10	RETURN	(1000*60*10)-1000	BREAK
		CASE DM_DURATION_15	RETURN	(1000*60*15)-1000	BREAK
		CASE DM_DURATION_20	RETURN	(1000*60*20)-1000	BREAK
		CASE DM_DURATION_30	RETURN	(1000*60*30)-1000	BREAK
		CASE DM_DURATION_45	RETURN	(1000*60*45)-1000	BREAK
		CASE DM_DURATION_60	RETURN	(1000*60*60)-1000	BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD 
				SCRIPT_ASSERT("Deathmatch: GET_ROUND_DURATION")
				PRINTLN("serverBDPassed.iDuration = ", serverBDPassed.iDuration)
			#ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (1000*60*10)-1000

ENDFUNC

FUNC BOOL HAS_TIME_RUN_OUT(ServerBroadcastData &serverBDpassed) 
	// When time limit over finish
	IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)		
		IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, GET_ROUND_DURATION(serverBDpassed))
			
//			PRINTLN("---------->     HAS_TIME_RUN_OUT (YES)    <---------- ") 
			
			RETURN TRUE	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SERVER_SETS_TIME_UP(ServerBroadcastData &serverBDpassed)
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TIMED_OUT)
		IF HAS_TIME_RUN_OUT(serverBDpassed)
			
			PRINTLN("[DM_HOST_END] SERVER_SETS_TIME_UP, SERV_BITSET_TIMED_OUT")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TIMED_OUT)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_DM_TIME_EXPIRED(ServerBroadcastData &serverBDpassed) 
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TIMED_OUT)	
ENDFUNC

FUNC INT GET_DM_TOTAL_TIME(ServerBroadcastData &serverBDpassed)
	RETURN GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBDpassed.timeServerMainDeathmatchTimer.Timer)
ENDFUNC

PROC SERVER_STORES_FINISH_TIME(ServerBroadcastData &serverBDpassed)
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		IF serverBDpassed.iFinishTime = -1
			serverBDpassed.iFinishTime = GET_DM_TOTAL_TIME(serverBDpassed)
			PRINTLN("SERVER_STORES_FINISH_TIME, =  ", serverBDpassed.iFinishTime)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_TIME_DIFF(INT iCurrentTime, INT iStartTime)

	RETURN (iCurrentTime - iStartTime)
ENDFUNC

PROC START_DM_TIMER(ServerBroadcastData &serverBDpassed)
	IF HAS_DM_STARTED(serverBDPassed)
		IF NOT HAS_NET_TIMER_STARTED(serverBDpassed.timeServerForFinishedLast)
			START_NET_TIMER(serverBDpassed.timeServerForFinishedLast)
		ENDIF
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_RemoveTimeLimitForDM)
			IF NOT HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
				START_NET_TIMER(serverBDpassed.timeServerMainDeathmatchTimer)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_HUD_TIME(ServerBroadcastData &serverBDpassed)

	RETURN GET_ROUND_DURATION(serverBDpassed) + 1000 - GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), serverBDpassed.timeServerMainDeathmatchTimer.Timer)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Spectator
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_PLAYER_SPECTATOR(PlayerBroadcastData &playerBDPassed[], PLAYER_INDEX playerId, INT iParticipant)

	IF playerId <> INVALID_PLAYER_INDEX()
		IF IS_PLAYER_SCTV(playerId)
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF iParticipant <> -1
		IF IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)
		
			RETURN TRUE
		ENDIF		
	ENDIF
	
	IF playerId <> INVALID_PLAYER_INDEX()
		IF DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerId)
		
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_SPECTATOR_ONLY(PlayerBroadcastData &playerBDPassed[])

	IF NETWORK_IS_SIGNED_ONLINE()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF iLocalPart <> -1
				IF IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)
				
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsSCTV
	
		RETURN TRUE
	ENDIF
	
	IF g_b_IsDMSpectator
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC STRING GET_PARTICIPANT_NAME(INT iPart)
	IF iPart > -1
	AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
		RETURN GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart)))
	ENDIF
	
	RETURN "INVALID"
ENDFUNC


FUNC BOOL IS_PARTICIPANT_A_SPECTATOR(PlayerBroadcastData &playerBDPassed[], INT iParticipant)
	IF IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
	OR IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)
	OR IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_SENT_TO_SPECTATOR)
		PRINTLN("[PASSTHEBOMB][PASSTHEBOMB REASSIGN] IS_PARTICIPANT_VALID_TO_BE_RANDOMLY_TAGGED - Returning FALSE || Participant ", iParticipant, " is some form of spectator || ", GET_PARTICIPANT_NAME(iParticipant))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC STOP_DM_ARENA_SCORE(SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
		TRIGGER_MUSIC_EVENT("MP_DM_STOP_TRACK")
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
		PRINTLN("[DM_AUDIO] STOP_DM_SCORE ")
	ENDIF	
ENDPROC

PROC DO_ROAMING_SPECTATOR_END_LOGIC(SHARED_DM_VARIABLES &dmVarsPassed)
	IF CONTENT_IS_USING_ARENA()
		IF IS_PLAYER_USING_ANY_TURRET(LocalPlayer)
			TURRET_MANAGER_UNLOCK()
		ENDIF
		STOP_DM_ARENA_SCORE(dmVarsPassed)
		IF SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS()
		AND DID_I_JOIN_MISSION_AS_SPECTATOR()
			PRINTLN("[LM][SPEC_SPEC][JIP] - Setting that we want to stay as a special spectator so we can double check.")
			SET_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS(TRUE)
		ENDIF
		PRINTLN("[LM][SPEC_SPEC][NET_DEATMATCH] DO_ROAMING_SPECTATOR_END_LOGIC ")
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_DM_USING_CORONA_VEHICLES()

	IF KOTH_USES_VEHICLES()
	AND KOTH_SHOULD_I_SPAWN_IN_VEHICLE()
		RETURN TRUE
	ENDIF
	
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[0].iTeamBitset, ciBS_TEAM_USES_CORONA_VEHICLE_LIST)
ENDFUNC

FUNC BOOL IS_ANY_SPAWN_VEHICLE_MODIFIER_SET_FOR_DM()

	INT i
	FOR i = 0 TO ciDM_MAX_MODIFIER_SETS - 1
		IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[i].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(INT iCurrentModSet, PLAYER_MODIFIER_DATA &sPlayerModifierData)
		
	IF iLocalPart = -1
		RETURN FALSE
	ENDIF
	
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		RETURN FALSE
	ENDIF
	
	IF sPlayerModifierData.mnRespawnVehicle = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF

	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_THIS_VEHICLE_DEATHMATCH(ServerBroadcastData &serverBDPassed)
	IF serverBDPassed.iDeathmatchType = FMMC_DM_TYPE_VEHICLE
	OR IS_THIS_DM_USING_CORONA_VEHICLES()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Ending
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()

	#IF IS_DEBUG_BUILD
		PRINTLN("DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - Calling DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN.")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF IS_SCREEN_FADING_OUT()
		PRINTLN("DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - IS_SCREEN_FADING_OUT = TRUE, exiting. ")
		EXIT
	ENDIF
	
	SET_NIGHTVISION(FALSE)
		
	THEFEED_PAUSE()
	PRINTLN("DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN - called THEFEED_FLUSH_QUEUE and THEFEED_PAUSE.")
	
	TOGGLE_RENDERPHASES(FALSE, TRUE)
							
ENDPROC


// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: End Conditions
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// Return true if target score met
FUNC BOOL HAS_ANYONE_REACHED_TARGET_SCORE(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TARGET_SCORE_REACHED)
ENDFUNC

FUNC BOOL DID_DM_END_WITHOUT_ENOUGH_PLAYERS(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ONE_PLAYER_KICK)
ENDFUNC

FUNC BOOL HAS_RIVAL_BOSS_BEEN_KILLED(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_KILLED)
ENDFUNC

FUNC BOOL HAS_GANG_BOSS_QUIT_BOSSVBOSS_DM(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
ENDFUNC

FUNC BOOL DO_ALL_ACTIVE_PARTICIPANTS_HAVE_A_FINISH_TIME(PlayerBroadcastData &playerBDPassed[])
	INT i
	
	FOR i = 0 TO NUM_NETWORK_PLAYERS - 1
		PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(i)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			IF playerBDPassed[i].iFinishedAtTime <= 0
				PRINTLN("DO_ALL_ACTIVE_PARTICIPANTS_HAVE_A_FINISH_TIME - Returning FALSE due to participant ", i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_ALL_OPPONENTS_BEEN_DEFEATED(ServerBroadcastData &serverBDpassed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
	OR IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)
		RETURN serverBDpassed.bAllPlayersHaveAFinishTime
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SERVER_FINISHED_SORTING_WINNING_TEAMS(ServerBroadcastData &serverBDpassed)

	IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	OR (IS_TDM_USING_FFA_SCORING() AND HAS_DEATHMATCH_FINISHED(serverBDpassed))
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_FINISHED_WINNING_TEAMS)
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_TEAM_SORT)
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_DEATHMATCH_FINISHED_LOCAL(SHARED_DM_VARIABLES &dmVarsPassed) 
	RETURN IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_FINISH_EVENT_RECEIVED)
ENDFUNC

FUNC BOOL WAS_IMPROMPTU_ABANDONED(ServerBroadcastData &serverBDpassed)

	IF serverBDpassed.playerAbandonedImpromptu <> INVALID_PLAYER_INDEX()
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
FUNC BOOL HAS_DM_DEBUG_ENDED(ServerBroadcastData &serverBDPassed)

	IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_S_PASS)

		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_F_FAIL)

		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL SERVER_CHECKS_HAS_DM_ENDED(ServerBroadcastData &serverBDpassed, BOOL bIgnoreTeamSortCheck #IF IS_DEBUG_BUILD , SHARED_DM_VARIABLES &dmVarsPassed #ENDIF )
	
	#IF IS_DEBUG_BUILD
	IF g_debugDisplayState = DEBUG_DISP_REPLAY
		PRINTLN("SERVER_CHECKS_HAS_DM_ENDED - End early due to debug restart CTRL+SHIFT+Z")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_KILL_ACTIVE_BOSS)
			IF bIsLocalPlayerHost
				IF GB_SHOULD_KILL_ACTIVE_BOSS_MISSION() 
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS -  SERVER_CHECKS_HAS_DM_ENDED - TRUE AS GB_SHOULD_KILL_ACTIVE_BOSS_MISSION  <----------     ") 
					SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_KILL_ACTIVE_BOSS)
					RETURN TRUE	
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS -  SERVER_CHECKS_HAS_DM_ENDED - TRUE AS SERV_BITSET2_KILL_ACTIVE_BOSS  <----------     ")
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bIgnoreTeamSortCheck = FALSE
		IF NOT HAS_SERVER_FINISHED_SORTING_WINNING_TEAMS(serverBDpassed)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Time up
	#IF IS_DEBUG_BUILD
	IF dmVarsPassed.debugDmVars.bNeverTimeOut = FALSE
	#ENDIF
	IF HAS_DM_TIME_EXPIRED(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - HAS_DM_TIME_EXPIRED()")
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
	
	IF HAS_RIVAL_BOSS_BEEN_KILLED(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - HAS_RIVAL_BOSS_BEEN_KILLED()")
		RETURN TRUE
	ENDIF
	
	IF HAS_GANG_BOSS_QUIT_BOSSVBOSS_DM(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - HAS_GANG_BOSS_QUIT_BOSSVBOSS_DM()")
		RETURN TRUE
	ENDIF
	
	// Target score reached
	IF HAS_ANYONE_REACHED_TARGET_SCORE(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - HAS_ANYONE_REACHED_TARGET_SCORE()")
		RETURN TRUE
	ENDIF
	
	// One player/team remains
	IF DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - DID_DM_END_WITHOUT_ENOUGH_PLAYERS()")
		RETURN TRUE
	ENDIF
	
	// Impromptu abandoned
	IF WAS_IMPROMPTU_ABANDONED(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - WAS_IMPROMPTU_ABANDONED()")
		RETURN TRUE
	ENDIF
	
	IF HAS_ALL_OPPONENTS_BEEN_DEFEATED(serverBDpassed)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - HAS_ALL_OPPONENTS_BEEN_DEFEATED()")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_FINISHED)
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - SERV_BITSET_EVERYONE_FINISHED()")
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ILLEGAL_UGC_DM)
		PRINTLN("---------->[DM_HOST_END] SHOULD_CLIENT_MOVE_TO_RESULTS, [SERV_BITSET_ILLEGAL_UGC_DM] ", tl31ScriptName) 
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_CHECKS_HAS_DM_ENDED] TRUE - SERV_BITSET_ILLEGAL_UGC_DM()")
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Debug ended
		IF HAS_DM_DEBUG_ENDED(serverBDpassed)
			PRINTLN("---------->[DM_HOST_END] SHOULD_CLIENT_MOVE_TO_RESULTS, [HAS_DM_DEBUG_ENDED] ", tl31ScriptName) 
			RETURN TRUE	
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ONLY_ONE_KILL_REMAINING(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ONE_KILL_LEFT)
ENDFUNC

FUNC BOOL IS_ONLY_30_SECONDS_LEFT(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_30_SECONDS_LEFT)
ENDFUNC

FUNC BOOL DOES_TEAM_HAVE_UNLIMITED_LIVES(ServerBroadcastData &serverBDpassed, INT iTeamToCheck)
	RETURN IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType) AND serverBDpassed.iTeamLives[iTeamToCheck] = 0
ENDFUNC

FUNC BOOL IS_LIVES_BASED_DEATHMATCH(ServerBroadcastData &serverBDpassed)
	RETURN serverBDpassed.iPlayerLives > 0 OR IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
ENDFUNC

FUNC INT GET_NUMBER_OF_DEATHS_FOR_PLAYER(PlayerBroadcastData &playerBDPassed[], ServerBroadcastData &serverBDpassed, PLAYER_INDEX piPlayer)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
		INT iTeam = GET_PLAYER_TEAM(piPlayer)
		IF iTeam != -1
			RETURN serverBDpassed.iTeamDeaths[iTeam]
		ENDIF
	ENDIF
	
	RETURN playerBDPassed[NATIVE_TO_INT(piPlayer)].iDeaths
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_SHARED_LIVES_FOR_TEAM(ServerBroadcastData &serverBDpassed, INT iTeam)

	IF DOES_TEAM_HAVE_UNLIMITED_LIVES(serverBDpassed, iTeam)
		RETURN 0
	ENDIF
	
	RETURN serverBDpassed.iTeamLives[iTeam] + serverBDpassed.iTeamExtraLives[iTeam]

ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_LIVES_FOR_PLAYER(PlayerBroadcastData &playerBDPassed[], ServerBroadcastData &serverBDpassed, PLAYER_INDEX piPlayer = NULL)
	
	IF piPlayer = NULL
		piPlayer = LocalPlayer
	ENDIF
	
	IF piPlayer = INVALID_PLAYER_INDEX()
		RETURN serverBDpassed.iPlayerLives
	ENDIF
	
	INT iTeam = GET_PLAYER_TEAM(piPlayer)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
	AND iTeam != -1
	AND iTeam < FMMC_MAX_TEAMS	
		IF DOES_TEAM_HAVE_UNLIMITED_LIVES(serverBDpassed, iTeam)
			RETURN 0
		ELIF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
			RETURN serverBDpassed.iTeamLives[iTeam] + serverBDpassed.iTeamExtraLives[iTeam]
		ELSE
			RETURN serverBDpassed.iTeamLives[iTeam] + playerBDPassed[NATIVE_TO_INT(piPlayer)].iExtraLives
		ENDIF
	ENDIF
	
	RETURN serverBDpassed.iPlayerLives + playerBDPassed[NATIVE_TO_INT(piPlayer)].iExtraLives
ENDFUNC

FUNC BOOL HAVE_LIVES_RUN_OUT_FOR_PLAYER(PlayerBroadcastData &playerBDPassed[], ServerBroadcastData &serverBDpassed, BOOL bForceSpectator, PLAYER_INDEX piPlayerIndex = NULL)
	
	INT iTeam = GET_PLAYER_TEAM(piPlayerIndex)
	IF iTeam != -1
	AND iTeam < FMMC_MAX_TEAMS
	AND DOES_TEAM_HAVE_UNLIMITED_LIVES(serverBDpassed, iTeam)
		RETURN FALSE
	ENDIF
	
	RETURN (GET_NUMBER_OF_DEATHS_FOR_PLAYER(playerBDPassed, serverBDpassed, piPlayerIndex) >= GET_MAX_NUMBER_OF_LIVES_FOR_PLAYER(playerBDPassed, serverBDpassed, piPlayerIndex) OR bForceSpectator)
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Winning/Position
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_SERVER_STORE_CACHE_SPECIFIC_PLAYER_POSITIONS(ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	serverBDpassed.playerWinning = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0)
	
	IF serverBDpassed.iNumActivePlayers > 0
		serverBDpassed.playerLosing = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, serverBDpassed.iNumActivePlayers-1)
	ENDIF
	
ENDPROC

PROC INITIALISE_TEAM_SORT_VARS(TEAM_SORT_STRUCT& sTeamSortVars)
	sTeamSortVars.iTeamScore 	= 0
	sTeamSortVars.iTeamDeaths	= 9999
	sTeamSortVars.fTeamRatio 	= -1
	sTeamSortVars.iTeamXP		= -1
	sTeamSortVars.iRemainingPlayers	= -1
ENDPROC

FUNC INT GET_NUMBER_OF_REMAINING_PLAYERS_ON_TEAM(INT iTeam, PlayerBroadcastData &playerBDPassed[])
	INT iRemainingPlayers
	INT i
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF playerBDPassed[i].iTeam != iTeam
			RELOOP
		ENDIF
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF iTeam > -1
			AND iTeam < FMMC_MAX_TEAMS				
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
				AND NOT IS_PLAYER_SCTV(piPlayer)					
					IF NOT IS_BIT_SET(playerBDPassed[i].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
						iRemainingPlayers++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("GET_NUMBER_OF_REMAINING_PLAYERS_ON_TEAM - Team ", iTeam, " has ", iRemainingPlayers, " remaining")
	RETURN iRemainingPlayers
ENDFUNC

FUNC BOOL IS_TEAM_BEST_TEAM(ServerBroadcastData &serverBDpassed, TEAM_SORT_STRUCT& sTeamSortVars, INT iTeam, PlayerBroadcastData &playerBDPassed[])

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
		IF (serverBDpassed.iTeamDeaths[iTeam] < sTeamSortVars.iTeamDeaths)
			RETURN TRUE
		ELIF (serverBDpassed.iTeamDeaths[iTeam] > sTeamSortVars.iTeamDeaths)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	AND NOT CONTENT_IS_USING_ARENA()
	AND IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)	
		INT iRemaining = GET_NUMBER_OF_REMAINING_PLAYERS_ON_TEAM(iTeam, playerBDPassed)
		IF iRemaining > sTeamSortVars.iRemainingPlayers
			PRINTLN("IS_TEAM_BEST_TEAM - TRUE - Team ", iTeam, " has ", iRemaining, " Current best team has: ", sTeamSortVars.iRemainingPlayers)
			RETURN TRUE
		ELSE
			PRINTLN("IS_TEAM_BEST_TEAM - FALSE - Team ", iTeam, " has ", iRemaining, " Current best team has: ", sTeamSortVars.iRemainingPlayers)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE()
		IF serverBDpassed.iTeamScorePercentage[iTeam] > sTeamSortVars.iTeamScore
			RETURN TRUE
		ELIF serverBDpassed.iTeamScorePercentage[iTeam] < sTeamSortVars.iTeamScore
			RETURN FALSE
		ENDIF
	ELSE
		IF serverBDpassed.iTeamScore[iTeam] > sTeamSortVars.iTeamScore
			RETURN TRUE
		ELIF serverBDpassed.iTeamScore[iTeam] < sTeamSortVars.iTeamScore
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF g_FMMC_STRUCT.sDMCustomSettings.iWinBiasTeam = iTeam
		RETURN TRUE
	ENDIF
	
	IF serverBDpassed.iTeamDeaths[iTeam] < sTeamSortVars.iTeamDeaths
		RETURN TRUE
	ELIF serverBDpassed.iTeamDeaths[iTeam] > sTeamSortVars.iTeamDeaths
		RETURN FALSE
	ENDIF
		
	IF serverBDpassed.fTeamRatio[iTeam] > sTeamSortVars.fTeamRatio
		RETURN TRUE
	ELIF serverBDpassed.fTeamRatio[iTeam] < sTeamSortVars.fTeamRatio
		RETURN FALSE
	ENDIF
												
	IF serverBDpassed.iTeamXP[iTeam] > sTeamSortVars.iTeamXP
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_TEAM_BEST_TEAM(ServerBroadcastData &serverBDpassed, TEAM_SORT_STRUCT& sTeamSortVars, INT iTeam, PlayerBroadcastData &playerBDPassed[])
	IF NOT DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE()
		sTeamSortVars.iTeamScore 	= serverBDpassed.iTeamScorePercentage[iTeam]
	ELSE
		sTeamSortVars.iTeamScore 	= serverBDpassed.iTeamScore[iTeam]
	ENDIF
	sTeamSortVars.iTeamDeaths	= serverBDpassed.iTeamDeaths[iTeam]
	sTeamSortVars.fTeamRatio 	= serverBDpassed.fTeamRatio[iTeam] 	
	sTeamSortVars.iTeamXP		= serverBDpassed.iTeamXP[iTeam]	
	sTeamSortVars.iRemainingPlayers = GET_NUMBER_OF_REMAINING_PLAYERS_ON_TEAM(iTeam, playerBDPassed)
	PRINTLN("SET_TEAM_BEST_TEAM - Setting best team struct from team ", iTeam)
ENDPROC

PROC SERVER_SORT_WINNING_TEAMS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[] #IF IS_DEBUG_BUILD , SHARED_DM_VARIABLES &dmVarsPassed #ENDIF )
	BOOL bCoronaReady = IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		bCoronaReady = TRUE
	ENDIF
	
	IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	OR NOT bCoronaReady 
		PRINTLN("[SERVER_END] SERVER_SORT_WINNING_TEAMS, EXIT 1 --- bCoronaReady: ", bCoronaReady, " IS_THIS_TEAM_DEATHMATCH: ", IS_THIS_TEAM_DEATHMATCH(serverBDpassed))
		EXIT
	ENDIF
	
	IF HAS_SERVER_FINISHED_SORTING_WINNING_TEAMS(serverBDpassed)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		AND NOT IS_CURRENT_MISSION_ARENA_UGC()
			PRINTLN("[SERVER_END] SERVER_SORT_WINNING_TEAMS, EXIT 2 - IS_CURRENT_MISSION_ARENA_UGC: ", IS_CURRENT_MISSION_ARENA_UGC(), " Last_Man_Standing_Style_Leaderboard_Order: ", IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order))
			EXIT
		ENDIF
	ENDIF
	 
	INT iTeamLoop = 0
	
	BOOL bAlreadyAddedToServerBD[MAX_NUM_DM_TEAMS]
	
	INT i
	REPEAT MAX_NUM_DM_TEAMS i
		bAlreadyAddedToServerBD[i] = FALSE
	ENDREPEAT

	INT indexToInsertAt = 0
	
	INT indexOfBestTeamFoundSoFar = -1
	TEAM_SORT_STRUCT sTeamSortVars

	FOR i = 0 TO (serverBDpassed.iNumberOfTeams - 1)
		indexOfBestTeamFoundSoFar = -1
		INITIALISE_TEAM_SORT_VARS(sTeamSortVars)
		
// Find smallest remaining valid iTeamPosition
		FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams - 1)
			IF NOT bAlreadyAddedToServerBD[iTeamLoop]
//				IF serverBDpassed.iTeamPosition[iTeamLoop] > 0
//					IF (serverBDpassed.iTeamPosition[iTeamLoop] < scoreOfBestTeamFoundSoFar)
					IF IS_TEAM_BEST_TEAM(serverBDpassed, sTeamSortVars, iTeamLoop, playerBDPassed)
						indexOfBestTeamFoundSoFar = iTeamLoop
						SET_TEAM_BEST_TEAM(serverBDpassed, sTeamSortVars, iTeamLoop, playerBDPassed)
						PRINTLN("SERVER_SORT_WINNING_TEAMS - indexOfBestTeamFoundSoFar: ", indexOfBestTeamFoundSoFar)
//						scoreOfBestTeamFoundSoFar = serverBDpassed.iTeamPosition[iTeamLoop]
					ENDIF
//				ENDIF
			ENDIF
		ENDFOR
		
// Add the best team found this time round to the server broadcast data
		IF indexOfBestTeamFoundSoFar <> -1
			IF indexToInsertAt < MAX_NUM_DM_TEAMS
				serverBDpassed.iBestTeamOrder[indexToInsertAt] = indexOfBestTeamFoundSoFar
				indexToInsertAt++
			ENDIF
			bAlreadyAddedToServerBD[indexOfBestTeamFoundSoFar] = TRUE
		ENDIF
	ENDFOR
	
//	Add all teams with invalid positions to the end of the server bd array?
	FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams - 1)
		IF NOT bAlreadyAddedToServerBD[iTeamLoop]
//			IF serverBDpassed.iTeamPosition[iTeamLoop] <= 0
				IF indexToInsertAt < MAX_NUM_DM_TEAMS
					serverBDpassed.iBestTeamOrder[indexToInsertAt] = iTeamLoop
					indexToInsertAt++
				ENDIF
//			ELSE
//				PRINTLN("[CS_DPAD] SERVER_SORT_WINNING_TEAMS - team ", iTeamLoop, " has position ", serverBDpassed.iTeamPosition[iTeamLoop])
//				SCRIPT_ASSERT("SERVER_SORT_WINNING_TEAMS - only expected to find invalid teams in the array at this stage")
//			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD6, KEYBOARD_MODIFIER_SHIFT, "")
		FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams - 1)
			//PRINTLN("[CS_DPAD] SERVER_SORT_WINNING_TEAMS - team ", iTeamLoop, " has position ", serverBDpassed.iTeamPosition[iTeamLoop])
			PRINTLN("[CS_DPAD] SERVER_SORT_WINNING_TEAMS - team ", iTeamLoop, " has iTeamScore ", serverBDpassed.iTeamScore[iTeamLoop])
			PRINTLN("[CS_DPAD] SERVER_SORT_WINNING_TEAMS - team ", iTeamLoop, " has iTeamDeaths ", serverBDpassed.iTeamDeaths[iTeamLoop])
			PRINTLN("[CS_DPAD] SERVER_SORT_WINNING_TEAMS - team ", iTeamLoop, " has fTeamRatio ", serverBDpassed.fTeamRatio[iTeamLoop])
			PRINTLN("[CS_DPAD] SERVER_SORT_WINNING_TEAMS - team ", iTeamLoop, " has iTeamXP ", serverBDpassed.iTeamXP[iTeamLoop])
		ENDFOR
	ENDIF
	#ENDIF
	
	// Ensure a full update before ending
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_TEAM_SORT)
		PRINTLN("[SERVER_END] SERVER_SORT_WINNING_TEAMS, SERV_BITSET_TEAM_SORT_CLOSED")
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TEAM_SORT_CLOSED)
	ELIF SERVER_CHECKS_HAS_DM_ENDED(serverBDpassed, TRUE   #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF )
		PRINTLN("[SERVER_END] SERVER_SORT_WINNING_TEAMS, SERV_BITSET_START_FINISHING_UP_TEAM_SORT")
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_TEAM_SORT)
	ENDIF
ENDPROC

FUNC BOOL CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER(ServerBroadcastData &serverBDpassed)
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] iLivesRemaining[0] = ", serverBDpassed.iLivesRemaining[0], " iLivesRemaining[1] = ", serverBDpassed.iLivesRemaining[1])
	IF HAS_DM_TIME_EXPIRED(serverBDpassed)
	
		IF serverBDpassed.iLivesRemaining[0] != 0
		AND serverBDpassed.iLivesRemaining[1] != 0
			IF serverBDpassed.iLivesRemaining[0] = serverBDpassed.iLivesRemaining[1]
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] TRUE AS SCORES DRAWN")
				RETURN TRUE
			ELIF serverBDpassed.iLivesRemaining[0] > serverBDpassed.iLivesRemaining[1]
				IF bIsLocalPlayerHost
					serverBDpassed.iBossvBossWinner = 0
					CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] I'M HOST SET serverBDpassed.iBossvBossWinner = ", serverBDpassed.iBossvBossWinner)
				ENDIF
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] FALSE AS TEAM 0 HAS MORE LIVES serverBDpassed.iBossvBossWinner = ", serverBDpassed.iBossvBossWinner)
				RETURN FALSE
			ELIF serverBDpassed.iLivesRemaining[0] < serverBDpassed.iLivesRemaining[1]
				IF bIsLocalPlayerHost
					serverBDpassed.iBossvBossWinner = 1
					CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] I'M HOST SET serverBDpassed.iBossvBossWinner = ", serverBDpassed.iBossvBossWinner)
				ENDIF
				
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] FALSE AS TEAM 1 HAS MORE LIVES serverBDpassed.iBossvBossWinner = ", serverBDpassed.iBossvBossWinner)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] [CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER] FALSE - FALLTHRU")
	
	RETURN FALSE
ENDFUNC
 // FEATURE_GANG_BOSS
PROC SERVER_ASSIGN_WINNING_TEAMS(ServerBroadcastData &serverBDpassed)

	#IF IS_DEBUG_BUILD
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_ASSIGN_WINNING_TEAMS called on client ")
	#ENDIF
	
	IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		EXIT
	ENDIF
	
	IF NOT HAS_DM_STARTED(serverBDpassed)
		EXIT
	ENDIF
	
	IF HAS_SERVER_FINISHED_SORTING_WINNING_TEAMS(serverBDpassed)
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		AND NOT IS_CURRENT_MISSION_ARENA_UGC()
			EXIT
		ENDIF
	ENDIF

	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)

//		PRINTLN("DRAW_DM_HUD SERVER_ASSIGN_WINNING_TEAMS ASSIGNING serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams, " serverBDpassed.iBestTeamOrder[0] = ",serverBDpassed.iBestTeamOrder[0], " serverBDpassed.iBestTeamOrder[1] = ", serverBDpassed.iBestTeamOrder[1])
		
		SWITCH serverBDpassed.iNumberOfTeams
			CASE 1	
				serverBDpassed.iWinningTeam = serverBDpassed.iBestTeamOrder[0]
			BREAK
			CASE 2
				serverBDpassed.iWinningTeam = serverBDpassed.iBestTeamOrder[0]
				serverBDpassed.iSecondTeam = serverBDpassed.iBestTeamOrder[1]
				serverBDpassed.iLosingTeam = serverBDpassed.iBestTeamOrder[1]
			BREAK
			CASE 3
				serverBDpassed.iWinningTeam = serverBDpassed.iBestTeamOrder[0]
				serverBDpassed.iSecondTeam = serverBDpassed.iBestTeamOrder[1]
				serverBDpassed.iThirdTeam = serverBDpassed.iBestTeamOrder[2]
				serverBDpassed.iLosingTeam = serverBDpassed.iBestTeamOrder[2]
			BREAK
			CASE 4
				serverBDpassed.iWinningTeam = serverBDpassed.iBestTeamOrder[0]
				serverBDpassed.iSecondTeam = serverBDpassed.iBestTeamOrder[1]
				serverBDpassed.iThirdTeam = serverBDpassed.iBestTeamOrder[2]
				serverBDpassed.iFourthTeam = serverBDpassed.iBestTeamOrder[3]
				serverBDpassed.iLosingTeam = serverBDpassed.iBestTeamOrder[3]
			BREAK
		ENDSWITCH

	ENDIF
	
		IF IS_PLAYER_ON_BOSSVBOSS_DM()
			IF serverBDpassed.iBossvBossWinner = -1
				IF HAS_DM_TIME_EXPIRED(serverBDpassed)
					//-- Server needs to set winner if timer expires before end shard gets displayed
					CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER(serverBDpassed)
				ENDIF
			ENDIF
		ENDIF
		
		IF serverBDpassed.iBossvBossWinner <> -1
			IF NOT HAS_GANG_BOSS_QUIT_BOSSVBOSS_DM(serverBDpassed)
				serverBDpassed.iWinningTeam = serverBDpassed.iBossvBossWinner
				CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] [SERVER_ASSIGN_WINNING_TEAMS] THINK THIS TEAM WON ", serverBDpassed.iWinningTeam )
			//	PLAYER_INDEX playerKilledBoss = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBDpassed.iBossvBossWinner))
			//	serverBDpassed.iWinningTeam = GET_PLAYER_TEAM(playerKilledBoss)
			//	CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] [SERVER_ASSIGN_WINNING_TEAMS] THINK THIS PLAYER ", GET_PLAYER_NAME(playerKilledBoss), " KILLED RIVAL BOSS, TEAM = ", GET_PLAYER_TEAM(playerKilledBoss))
			ELSE
				PLAYER_INDEX playerBossQuit = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBDpassed.iBossvBossWinner))
				serverBDpassed.iWinningTeam = GET_PLAYER_TEAM(playerBossQuit)
				CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] [SERVER_ASSIGN_WINNING_TEAMS] THINK THIS PLAYER ", GET_PLAYER_NAME(playerBossQuit), " BOSS QUIT, TEAM = ", GET_PLAYER_TEAM(playerBossQuit))
			ENDIF	
		ENDIF
	
	// Set the end bit
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TEAM_SORT_CLOSED)
		PRINTLN("[SERVER_END] SERVER_ASSIGN_WINNING_TEAMS, SERV_BITSET_FINISHED_WINNING_TEAMS")
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_FINISHED_WINNING_TEAMS)
	ENDIF
ENDPROC


FUNC INT GET_PLAYER_POSITION(ServerBroadcastData_Leaderboard &serverBDpassedLDB, PLAYER_INDEX playerId)

	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
	
		IF serverBDpassedLDB.leaderBoard[i].playerID = playerId
		
			RETURN (i +1) // No 0th Pos
		ENDIF	
	ENDREPEAT	
	
	#IF IS_DEBUG_BUILD
//	SCRIPT_ASSERT("GET_PLAYER_POSITION, RETURN 0 (invalid iParticipant?)")
	PRINTLN("GET_PLAYER_POSITION, RETURN 0 (invalid iParticipant?)")
	#ENDIF

	RETURN 0
ENDFUNC

FUNC INT GET_TEAM_IN_LEADERBOARD_POS(ServerBroadcastData_Leaderboard &serverBDpassedLDB, INT iPos)

	INT iTeam
	
	IF serverBDpassedLDB.leaderBoard[iPos].iTeam <> -1	

		iTeam = serverBDpassedLDB.leaderBoard[iPos].iTeam 
	ENDIF
	
	RETURN iTeam
ENDFUNC

FUNC INT GET_TEAM_POSITION(ServerBroadcastData &serverBDpassed, INT iTeam)
	RETURN serverBDpassed.iTeamPosition[iTeam]
ENDFUNC

FUNC BOOL HAS_SERVER_PICKED_WINNING_TEAM(ServerBroadcastData &serverBDpassed)

	#IF IS_DEBUG_BUILD
	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("HAS_SERVER_PICKED_WINNING_TEAM called on client")
	#ENDIF
	
	IF serverBDpassed.iWinningTeam > -1
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_WINNING(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		IF GET_PLAYER_TEAM(thisPlayer) = serverBDpassed.iWinningTeam

			RETURN TRUE
		ENDIF
	ELSE
		IF GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0) = thisPlayer
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_WINNING_OUTRIGHT(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB, PlayerBroadcastData &playerBDPassed[])
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		IF GET_PLAYER_TEAM(thisPlayer) = serverBDpassed.iWinningTeam
		AND serverBDpassed.iWinningTeam != -1
		AND serverBDpassed.iSecondTeam != -1
			IF NOT DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE()
				IF serverBDpassed.iTeamScorePercentage[serverBDpassed.iWinningTeam] > serverBDpassed.iTeamScorePercentage[serverBDpassed.iSecondTeam]
					RETURN TRUE
				ENDIF
			ELSE
				IF serverBDpassed.iTeamScore[serverBDpassed.iWinningTeam] > serverBDpassed.iTeamScore[serverBDpassed.iSecondTeam]
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0) = thisPlayer
			
			INT iPlayerToCheck = NATIVE_TO_INT(thisPlayer)
			INT iPlayerScore, iSecondScore
			// Player in 2nd
			PLAYER_INDEX playerSecond = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 1)
			INT iSecond = NATIVE_TO_INT(playerSecond)

			IF iPlayerToCheck != -1
				iPlayerScore = playerBDPassed[iPlayerToCheck].iScore
			ENDIF
			IF iSecond != -1
				iSecondScore = playerBDPassed[iSecond].iScore
			ENDIF

			IF iPlayerScore > iSecondScore
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_LOSING(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
	AND NOT IS_TDM_USING_FFA_SCORING()
		IF GET_PLAYER_TEAM(thisPlayer) = serverBDpassed.iLosingTeam

			RETURN TRUE
		ENDIF
	ELSE
		IF serverBDpassed.iNumAlivePlayers-1 >= 0
		AND GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, serverBDpassed.iNumAlivePlayers-1) = thisPlayer
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_SECOND_TO_LAST_TEAM(ServerBroadcastData &serverBDpassed)
	SWITCH serverBDpassed.iNumberOfTeams
		CASE 2
			RETURN serverBDpassed.iWinningTeam
		BREAK
		CASE 3
			RETURN serverBDpassed.iSecondTeam
		BREAK
		CASE 4
			RETURN serverBDpassed.iThirdTeam
		BREAK
	ENDSWITCH
	
	RETURN serverBDpassed.iWinningTeam
ENDFUNC

FUNC BOOL IS_PLAYER_LOSING_OUTRIGHT(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB, PlayerBroadcastData &playerBDPassed[])
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		
		IF GET_PLAYER_TEAM(thisPlayer) = serverBDpassed.iLosingTeam
		AND GET_SECOND_TO_LAST_TEAM(serverBDpassed) != TEAM_INVALID
		
			IF NOT DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE()
				IF serverBDpassed.iTeamScorePercentage[serverBDpassed.iLosingTeam] < serverBDpassed.iTeamScorePercentage[GET_SECOND_TO_LAST_TEAM(serverBDpassed)]
					RETURN TRUE
				ENDIF
			ELSE
				IF serverBDpassed.iTeamScore[serverBDpassed.iLosingTeam] < serverBDpassed.iTeamScore[GET_SECOND_TO_LAST_TEAM(serverBDpassed)]
					RETURN TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
		IF serverBDpassed.iNumAlivePlayers-1 >= 0
		AND GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, serverBDpassed.iNumAlivePlayers-1) = thisPlayer
			
			INT iPlayerToCheck = NATIVE_TO_INT(thisPlayer)
			INT iPlayerScore, iSecondLastScore
			
			IF serverBDpassed.iNumAlivePlayers-2 >= 0
				PLAYER_INDEX playerSecondLast = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, serverBDpassed.iNumAlivePlayers-2)
				INT iSecondLast = NATIVE_TO_INT(playerSecondLast)

				IF iPlayerToCheck != -1
					iPlayerScore = playerBDPassed[iPlayerToCheck].iScore
				ENDIF
				IF iSecondLast != -1
					iSecondLastScore = playerBDPassed[iSecondLast].iScore
				ENDIF

				IF iPlayerScore < iSecondLastScore
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DID_I_WIN(ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	// TDM
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		IF GET_PLAYER_TEAM(LocalPlayer) = serverBDpassed.iWinningTeam
	
			RETURN TRUE
		ENDIF
	ELSE
		// FFA
		IF GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0) = LocalPlayer
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Stats
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INT NUMBER_TIMES_STARTED_DM(SHARED_DM_VARIABLES &dmVarsPassed)

	RETURN dmVarsPassed.iNumberDmStartedStat
ENDFUNC

FUNC INT DEATHMATCH_GET_POSIX_FOR_PLAYSTATS(ServerBroadcastData &serverBDpassed)

	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		RETURN serverBDpassed.iMatchHistoryId
	ENDIF	

	RETURN g_MissionControllerserverBD_LB.iMatchTimeID
ENDFUNC

FUNC INT DEATHMATCH_GET_HASHMAC_FOR_PLAYSTATS(ServerBroadcastData &serverBDpassed) 

	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		RETURN serverBDpassed.iHashedMac
	ENDIF	

	RETURN g_MissionControllerserverBD_LB.iHashMAC
ENDFUNC

PROC END_PLAYSTATS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	IF !g_bNeedToCallMatchEndOnQuit
		PRINTLN("END_PLAYSTATS -  g_bNeedToCallMatchEndOnQuit was not set")
		EXIT
	ENDIF
	
	IF GET_INITIAL_STAGE(playerBDPassed) < INITIAL_FADE_STAGE_FADED_IN
		PRINTLN("END_PLAYSTATS - Not far enough into init process")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_END_STATS)

		INT iTeam = playerBDPassed[iLocalPart].iTeam
		// 2175983
		IF iTeam < 0
			iTeam = 0
		ENDIF
		
		INT iFinishPos
		IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		AND NOT IS_TDM_USING_FFA_SCORING()
			iFinishPos = GET_TEAM_RANK(serverBDpassed, playerBDPassed[iLocalPart].iTeam, serverBDpassed.iNumberOfTeams)
			PRINTLN("END_PLAYSTATS - GET_TEAM_RANK, iFinishPos = ", iFinishPos)
		ELSE
			IF CONTENT_IS_USING_ARENA()
				iFinishPos = GET_LOCAL_PLAYER_LEADERBOARD_POSITION(serverBDpassedLDB)
				PRINTLN("END_PLAYSTATS - GET_LOCAL_PLAYER_LEADERBOARD_POSITION, iFinishPos = ", iFinishPos)
			ELSE
				iFinishPos = GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT())
				PRINTLN("END_PLAYSTATS - GET_PLAYER_FINISH_POS, iFinishPos = ", iFinishPos)
			ENDIF
		ENDIF
		
		INT iDMResult = ciFMMC_END_OF_MISSION_STATUS_FAILED
		IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
		OR IS_PAUSE_MENU_REQUESTING_TRANSITION()
			iDMResult = ciFMMC_END_OF_MISSION_STATUS_CANCELLED
		ELIF iFinishPos = 1
			iDMResult = ciFMMC_END_OF_MISSION_STATUS_PASSED
		ENDIF
		PRINTLN("END_PLAYSTATS = iDMResult = ", iDMResult)
		
		MODEL_NAMES mnVehicleUsed = DUMMY_MODEL_FOR_SCRIPT
		
		IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
		OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, dmVarsPassed.sPlayerModifierData)
			mnVehicleUsed = g_mnMyRaceModel
		ENDIF

		DEAL_WITH_FM_MATCH_END(	g_FMMC_STRUCT.iMissionType , 
								DEATHMATCH_GET_POSIX_FOR_PLAYSTATS(serverBDpassed), 
								0, 
								iDMResult, 
								GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()), 
								playerBDPassed[iLocalPart].iKills, 
								playerBDPassed[iLocalPart].iDeaths, 
								playerBDPassed[iLocalPart].iSuicides, 
								dmVarsPassed.iHighestKillStreak, 
								iTeam,
								playerBDPassed[iLocalPart].iHeadshots,
								mnVehicleUsed,
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT,
								IS_THIS_TEAM_DEATHMATCH(serverBDpassed))
		PRINTLN("[NET_DEATHMATCH] END_PLAYSTATS done")
		
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_END_STATS)
	ELSE
		PRINTLN("[NET_DEATHMATCH] DM_BOOLS2_END_STATS is already set")
	ENDIF	
ENDPROC

PROC INITIALIZE_PLAYSTATS_FOR_ROUNDS()
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp = 0
		g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp = GET_PLAYER_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID()))
	ENDIF	
	IF g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel = 0
		g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel = GET_FM_RANK_FROM_XP_VALUE(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp)
	ENDIF
ENDPROC

PROC GRAB_NUM_TIMES_DM_STARTED_STAT(SHARED_DM_VARIABLES &dmVarsPassed)
	dmVarsPassed.iNumberDmStartedStat =  (GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_START) + 1) // Begins at 0
	PRINTLN("[CS_STAT] GRAB_NUM_TIMES_DM_STARTED_STAT,  ", dmVarsPassed.iNumberDmStartedStat)
ENDPROC

FUNC BOOL SHOULD_DEATHMATCH_BE_EXEMPT_FROM_BADSPORT()

	IF g_sMPTunables.bENABLE_NO_BADSPORT_ON_LOW_RATED_DMS = FALSE
		RETURN FALSE
	ENDIF
	
	IF IS_ROCKSTAR_CREATED_MISSION()
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		RETURN FALSE
	ENDIF
	
	IF g_FMMC_STRUCT.iRating != FMMC_NOT_YET_RATED
	AND g_FMMC_STRUCT.iRating >= g_sMPTunables.iNO_BADSPORT_ON_LOW_RATED_DMS_VOTE_THRESH
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC STARTED_DM_STAT()
	PRINTLN("[CS_STAT] STARTED_DM_STAT ")
//	INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_DM_START  , 1)
//	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START, INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY())
//	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_STARTED, 1)

	IF INCREMENT_STARTING_ACTIVITY_CHEAT_STAT_BY() > 0
		SET_JOB_ACTIVITY_PROFILE_SETTINGS(PSJA_DM_STARTED)
	ENDIF
	
	IF SHOULD_DEATHMATCH_BE_EXEMPT_FROM_BADSPORT()
		PRINTLN("[BADSPORT_EXEMPT] STARTED_DM_STAT - Setting g_bDeathmatchExemptFromBadsport")
		g_bDeathmatchExemptFromBadsport = TRUE
	ENDIF

	CHEAT_TRACKER_START_OF_DEATHMATCH()
ENDPROC

PROC AWARD_KILLSTREAK_STAT()

	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_KILLSTREAK, 1)")
	INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_KILLSTREAK, 1)
ENDPROC

PROC AWARD_MVP_STAT()

	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TDM_MVP, 1)")
	INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TDM_MVP, 1)
ENDPROC

PROC AWARD_TDM_WIN_STAT()

	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TDM_WINS, 1)")
	INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TDM_WINS, 1)
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_WON, 1)")
	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_WON, 1)
ENDPROC

PROC TDM_LOST_STAT()

	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_LOST, 1)")
	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TDEATHMATCH_LOST, 1)
ENDPROC

PROC APPLY_LOSS_STAT()
	
	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_LOST, 1)")
	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_LOST, 1)
ENDPROC

PROC APPLY_LOSS_STATS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_LOSS_STAT)
		APPLY_LOSS_STAT()
		IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
			TDM_LOST_STAT()
		ENDIF
		SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_LOSS_STAT)
	ENDIF
ENDPROC

PROC END_LEGITIMATELY_STAT(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	IF NETWORK_IS_SIGNED_ONLINE()  
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_END_LEGIT_STAT)
			PRINTLN("[CS_STAT] END_LEGITIMATELY_STAT: INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_DM_END  , 1)  ")
			INT IncrementBy = INCREMENT_ENDING_ACTIVITY_CHEAT_STAT_BY()
				
			IF SHOULD_PROGRESS_DM_AWARDS()
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_DM_END, 1)
			ENDIF
			INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END, IncrementBy)
			
			IF IncrementBy > 0
				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED, 1)
				IF IncrementBy = 1
					APPLY_LOSS_STATS(serverBDpassed, dmVarsPassed)
				ENDIF
			ENDIF
			
			SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_END_LEGIT_STAT)
		ENDIF
		
		IF IS_CURRENT_CONTENT_MADE_BY_A_FRIEND()
			REQUEST_SYSTEM_ACTIVITY_TYPE_PLAYED_FRIENDS_PUBLISHED_DM(GET_CURRENT_CONTENT_MADE_BY_A_FRIEND_NAME())
			TEXT_LABEL_63 EmptyTL = ""
			SET_CURRENT_CONTENT_MADE_BY_A_FRIEND(FALSE, EmptyTL)
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_WIN_STAT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	IF NOT DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
		PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_WON, 1)")
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_DEATHMATCH_WON, 1)
		IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
			PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_WINS, 1)")
			INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_WINS, 1)
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_TOP_THREE_STAT()

	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_TOP_3, 1)")
	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_TOP_3, 1)
ENDPROC

PROC APPLY_FINISHED_LAST_STAT()

	IF NOT SHOULD_PROGRESS_DM_AWARDS()
		EXIT
	ENDIF
	
	PRINTLN("[CS_STAT] INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_LAST, 1)")
	INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TIMES_FINISH_DM_LAST, 1)
ENDPROC

PROC APPLY_WIN_STATS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	APPLY_WIN_STAT(serverBDPassed, playerBDPassed)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		IF GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()) = 1
			AWARD_MVP_STAT()
		ENDIF			
		AWARD_TDM_WIN_STAT()
	ENDIF
ENDPROC

PROC APPLY_DM_END_STATS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_STATS_WRITTEN)
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM() 
			PRINTLN("IS_PLAYER_ON_BOSSVBOSS_DM")
			IF DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed)
				//1843597
				APPLY_WIN_STATS(serverBDpassed, playerBDPassed)
			ELSE
				IF DID_I_WIN(serverBDpassed, serverBDpassedLDB)
					APPLY_WIN_STATS(serverBDpassed, playerBDPassed)
				ELSE
					APPLY_LOSS_STATS(serverBDpassed, dmVarsPassed)
				ENDIF
				
				IF GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()) = NETWORK_GET_NUM_PARTICIPANTS()
					IF serverBDpassed.iNumDmStarters >= 2
						APPLY_FINISHED_LAST_STAT()
					ENDIF
				ENDIF
				
				IF GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()) < 4
					IF serverBDpassed.iNumDmStarters >= 4
						APPLY_TOP_THREE_STAT()
					ENDIF
				ENDIF
			ENDIF
		
		//Daily Objectives Stuff - MJM
			IF SHOULD_PROGRESS_DM_AWARDS()
				IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed) 
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_TEAM_DM)
				ELIF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_VEHICLE_DM)
				ELSE
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_SOLO_DM)
				ENDIF
			ELSE
				IF IS_KING_OF_THE_HILL()
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_KING_OF_THE_HILL)
				ENDIF
			ENDIF
			
			// SERIES
			IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_FEATURED_ADVERSARY_MODE_JOB(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_FEATURED_SERIES)
				PRINTLN("[MJL][DM][DAILY OBJECTIVE] This was in a Featured series", g_FMMC_STRUCT.iRootContentIDHash)
			ENDIF
			
			IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_PARTICIPATE_IN_ARENA_WAR_SERIES)
				PRINTLN("[MJL][DM][DAILY OBJECTIVE] This was in a Arena series", g_FMMC_STRUCT.iRootContentIDHash)
			ENDIF
						
			IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_CARNAGE)
				PRINTLN("[MJL][DM][DAILY OBJECTIVE] This was Arena Carnage")
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_JOB_HOT_BOMB)
				PRINTLN("[MJL][DM][DAILY OBJECTIVE] This was Arena Hot Bomb")
			ENDIF
		
		//Arena Wars
			IF IS_ARENA_WARS_JOB(TRUE)
				IF DID_I_WIN(serverBDpassed, serverBDpassedLDB)
					g_sArena_Telemetry_data.m_endreason = 1
					PRINTLN("[ARENA][TEL] - end reason - Won ", g_sArena_Telemetry_data.m_endreason)
					IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed) 
						g_sArena_Telemetry_data.m_winConditions = 1
						PRINTLN("[ARENA][TEL] - win condition - Team Finishing first. ", g_sArena_Telemetry_data.m_winConditions)
					ELSE
						g_sArena_Telemetry_data.m_winConditions = 2
						PRINTLN("[ARENA][TEL] - win condition - last player standing ", g_sArena_Telemetry_data.m_winConditions)
					ENDIF
				ELSE
					g_sArena_Telemetry_data.m_endreason = 0
					PRINTLN("[ARENA][TEL] - end reason - Lost ", g_sArena_Telemetry_data.m_endreason)
				ENDIF
				g_sArena_Telemetry_data.m_matchduration = serverBDpassed.iFinishTime
				PRINTLN("[ARENA][TEL] - DM g_sArena_Telemetry_data.m_matchduration - ", g_sArena_Telemetry_data.m_matchduration)
				g_sArena_Telemetry_data.m_finishPosition = GET_LOCAL_PLAYER_LEADERBOARD_POSITION(serverBDpassedLDB)
				PRINTLN("[ARENA][TEL] - DM g_sArena_Telemetry_data.m_finishPosition - ", g_sArena_Telemetry_data.m_finishPosition)
				g_sArena_Telemetry_data.m_publiccontentid = g_FMMC_STRUCT.iRootContentIDHash
				PRINTLN("[ARENA][TEL] - DM g_sArena_Telemetry_data.m_publiccontentid - ", g_sArena_Telemetry_data.m_publiccontentid)
				g_sArena_Telemetry_data.m_playingtime = playerBDPassed[iLocalPart].iFinishedAtTime
				PRINTLN("[ARENA][TEL] - DM g_sArena_Telemetry_data.m_playingtime - ", g_sArena_Telemetry_data.m_playingtime)
			ENDIF
		ELSE
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_COMPLETE_IMPROMPTU_DM)
		ENDIF
	
		END_LEGITIMATELY_STAT(serverBDpassed, dmVarsPassed)
		SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_STATS_WRITTEN)
	ENDIF
ENDPROC

PROC RESET_THE_STATS_THAT_NEED_RESETTING()
	PRINTLN("RESET_THE_STATS_THAT_NEED_RESETTING")
	RESET_MP_INT_CHARACTER_STAT(MP_STAT_DM_CURRENT_KILLS)
	RESET_MP_INT_CHARACTER_STAT(MP_STAT_DM_CURRENT_DEATHS)
ENDPROC

PROC STORE_LOCAL_PLAYERS_PERMANENT_DM_STATS(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	PRINTLN("----------> STORE_LOCAL_PLAYERS_PERMANENT_DM_STATS()  ", tl31ScriptName) 
	
	INT iAssists = GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_CURRENT_ASSISTS)
	
	IF SHOULD_PROGRESS_DM_AWARDS()
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DM_TOTAL_KILLS, 		playerBDPassed[iLocalPart].iKills)
		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_DM_TOTAL_ASSISTS, 	iAssists)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DM_TOTAL_DEATHS, 		playerBDPassed[iLocalPart].iDeaths)
	ENDIF
	PRINTLN("iAssists = ", iAssists)

	IF dmVarsPassed.iHighestKillStreak > GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_HIGHEST_KILLSTREAK )
	AND SHOULD_PROGRESS_DM_AWARDS()
		PRINTLN("Increasing skillstreak stat because player beat previous best ", dmVarsPassed.iHighestKillStreak)
		RESET_MP_INT_CHARACTER_STAT(MP_STAT_DM_HIGHEST_KILLSTREAK )
		INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_DM_HIGHEST_KILLSTREAK , dmVarsPassed.iHighestKillStreak)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: ELO
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL WAS_ELO_SUCCESSFUL(LEADERBOARDS_ENUM leaderboard)
	STORED_ELO_ENUM eloEnum
	eloEnum = GET_STORED_ELO_ENUM_FOR_LEADERBOARD(leaderboard)
	IF NOT storedELOData.bReadFinished[eloEnum]
		PRINTLN("WAS_ELO_SUCCESSFUL: ELO read not finished! stored value = ", storedELOData.iELO[eloEnum])
		SCRIPT_ASSERT("WAS_ELO_SUCCESSFUL: ELO read not finished!")
		RETURN FALSE
	ENDIF
	IF storedELOData.bReadFailed[eloEnum]
		PRINTLN("WAS_ELO_SUCCESSFUL: ELO read failed! stored value = ", storedELOData.iELO[eloEnum])
		SCRIPT_ASSERT("WAS_ELO_SUCCESSFUL: ELO read failed!")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_CURRENT_ELO_FOR_PARTICIPANT(PlayerBroadcastData &playerBDPassed[], INT iParticipant)

	INT iElo = playerBDPassed[iParticipant].iCurrentELO
	
	RETURN iElo
ENDFUNC

FUNC INT GET_CURRENT_ELO_FOR_LOCAL_PLAYER(PlayerBroadcastData &playerBDPassed[])

	INT iElo = playerBDPassed[iLocalPart].iCurrentELO

	RETURN iElo
ENDFUNC

FUNC INT GET_AVERAGE_ELO(PlayerBroadcastData &playerBDPassed[], INT iNumPlayersOnJob, BOOL bTeam)

	INT iPlayerElo
	INT iTotalElo
	INT iAverageElo
	INT iParticipant//, iTeam
	PLAYER_INDEX PlayerId
	
	PRINTLN(" [CS_ELO] GET_AVERAGE_ELO, iNumPlayersOnJob = ", iNumPlayersOnJob)

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			IF (PlayerId <> LocalPlayer)
				IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, PlayerId, iParticipant)
					IF NOT bTeam
					OR NOT ARE_PLAYERS_ON_SAME_TEAM(PlayerId, LocalPlayer)
						iPlayerElo = GET_CURRENT_ELO_FOR_PARTICIPANT(playerBDPassed, iParticipant)
						PRINTLN(" [CS_ELO] GET_AVERAGE_ELO, iPlayerElo =  ", iPlayerElo, " iParticipant = ", iParticipant)
						iPlayerElo = (iPlayerElo + ELO_BASE)
						PRINTLN(" [CS_ELO] GET_AVERAGE_ELO, +1200 iPlayerElo =  ", iPlayerElo, " iParticipant = ", iParticipant)
					
						iTotalElo += iPlayerElo
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	iAverageElo = (iTotalElo / (iNumPlayersOnJob -1))
	
	RETURN iAverageElo
ENDFUNC

PROC GET_OPPONENTS_AVERAGE_ELO(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_ELO_AVE_GRABBED)
		IF NOT bIsSCTV
			BOOL bTeam 			= IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
//			INT iTeam			= GET_PLAYER_TEAM(LocalPlayer)
			INT iNumPlayers
			IF bTeam
//				IF iTeam <> -1
//				AND iTeam < MAX_NUM_DM_TEAMS
//					iNumPlayers	= serverBDpassed.iNumberOfLBPlayers[iTeam]
//				ENDIF
				iNumPlayers		= serverBDpassed.iNumberOfTeams
			ELSE
				iNumPlayers		= serverBDpassed.iNumDmStarters
			ENDIF
			dmVarsPassed.iAverageOpponentELO = GET_AVERAGE_ELO(playerBDPassed, iNumPlayers, bTeam)
			PRINTLN("[CS_ELO] GET_OPPONENTS_AVERAGE_ELO = ", dmVarsPassed.iAverageOpponentELO)
			SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_ELO_AVE_GRABBED)
		ENDIF
	ENDIF
ENDPROC

PROC FINISH_JOB_ELO(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	BOOL bWon			= DID_I_WIN(serverBDpassed, serverBDpassedLDB)
	INT iCurrentELO 	= GET_CURRENT_ELO_FOR_LOCAL_PLAYER(playerBDPassed)
	INT iNumPlayed		= NUMBER_TIMES_STARTED_DM(dmVarsPassed)

	PREPARE_ELO_FOR_LBD_WRITE(playerBDPassed[iLocalPart].iELODiffForConor, iCurrentELO, dmVarsPassed.iAverageOpponentELO, iNumPlayed, bWon)	

	#IF IS_DEBUG_BUILD
		PRINTNL()

		PRINTLN("[CS_ELO] DM_END _____________________________________________")

		PRINTNL()	
	#ENDIF
ENDPROC

FUNC LEADERBOARDS_ENUM GET_LBD_ELO_FOR_MODE(ServerBroadcastData &serverBDpassed)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		
		RETURN LEADERBOARD_FREEMODE_ELO_TEAM_DEATHMATCH_OVERALL
	ELIF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	
		RETURN LEADERBOARD_FREEMODE_ELO_VEHICLE_DEATHMATCH_OVERALL
	ENDIF	

	RETURN LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: TDM
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INT GET_RIVAL_TEAM(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed)
	PRINTLN("DRAW_DM_HUD GET_RIVAL_TEAM GET_PLAYER_TEAM(thisPlayer) = ", GET_PLAYER_TEAM(thisPlayer), " serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams, " serverBDpassed.iWinningTeam = ", serverBDpassed.iWinningTeam, " serverBDpassed.iLosingTeam = ", serverBDpassed.iLosingTeam, " serverBDpassed.iSecondTeam = ",serverBDpassed.iSecondTeam)
	IF serverBDpassed.iNumberOfTeams = 2
		IF GET_PLAYER_TEAM(thisPlayer) = serverBDpassed.iWinningTeam
			RETURN serverBDpassed.iLosingTeam
		ELSE
			RETURN serverBDpassed.iWinningTeam
		ENDIF
	ELSE
		IF GET_PLAYER_TEAM(thisPlayer) = serverBDpassed.iWinningTeam
			RETURN serverBDpassed.iSecondTeam
		ELSE
			RETURN serverBDpassed.iWinningTeam
		ENDIF
	ENDIF	
		
	RETURN TEAM_INVALID
ENDFUNC

//Has to be done because we can't convert from text label to string, the functions don't work
FUNC STRING GET_LABEL_FOR_TEAM_NAME(INT iTeam)
	SWITCH iTeam
		CASE 1	RETURN "FMMC_TEAM_1"
		CASE 2	RETURN "FMMC_TEAM_2"
		CASE 3	RETURN "FMMC_TEAM_3"
		CASE 4	RETURN "FMMC_TEAM_4"
		CASE 5	RETURN "FMMC_TEAM_5"
		CASE 6	RETURN "FMMC_TEAM_6"
		CASE 7	RETURN "FMMC_TEAM_7"
		CASE 8	RETURN "FMMC_TEAM_8"
		CASE 9	RETURN "FMMC_TEAM_9"
		CASE 10	RETURN "FMMC_TEAM_10"
		CASE 11	RETURN "FMMC_TEAM_11"
		CASE 12	RETURN "FMMC_TEAM_12"
		CASE 13	RETURN "FMMC_TEAM_13"
		CASE 14	RETURN "FMMC_TEAM_14"
		CASE 15	RETURN "FMMC_TEAM_15"
		CASE 16	RETURN "FMMC_TEAM_16"
		CASE 17	RETURN "FMMC_TEAM_17"
		CASE 18	RETURN "FMMC_TEAM_18"
		CASE 19	RETURN "FMMC_TEAM_19"
		CASE 20	RETURN "FMMC_TEAM_20"
		CASE 21	RETURN "FMMC_TEAM_21"
		CASE 22	RETURN "FMMC_TEAM_22"
		CASE 23	RETURN "FMMC_TEAM_23"
		CASE 24	RETURN "FMMC_TEAM_24"
		CASE 25	RETURN "FMMC_TEAM_25"
		CASE 26	RETURN "FMMC_TEAM_26"
		CASE 27	RETURN "FMMC_TEAM_27"
		CASE 28	RETURN "FMMC_TEAM_28"
		CASE 29	RETURN "FMMC_TEAM_29"
		CASE 30	RETURN "FMMC_TEAM_30"
		CASE 31	RETURN "FMMC_TEAM_31"
		CASE 32	RETURN "FMMC_TEAM_32"
		CASE 33	RETURN "FMMC_TEAM_33"
		CASE 34	RETURN "FMMC_TEAM_34"
		CASE 35	RETURN "FMMC_TEAM_35"
		CASE 36	RETURN "FMMC_TEAM_36"
		CASE 37	RETURN "FMMC_TEAM_37"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC TEXT_LABEL_15 GET_TDM_NAME(ServerBroadcastData &serverBDpassed, INT iTeam, BOOL isEnemyTeam, BOOL bRelative = TRUE)//, BOOL bIsWinning)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		IF iTeam > -1		
			IF g_FMMC_STRUCT.iTeamNames[iTeam] > 0
				RETURN GET_CORONA_TEAM_NAME(iTeam)//GET_LABEL_FOR_TEAM_NAME(g_FMMC_STRUCT.iTeamNames[iTeam])
			ENDIF
		ENDIF
			
		IF NOT bRelative
		//OR serverBDpassed.iNumberOfTeams > 2
			TEXT_LABEL_15 tl15 = "TDM_TM_"
			tl15 += iTeam
			RETURN tl15
		ENDIF
	ENDIF
	TEXT_LABEL_15 tl15 = ""
	IF isEnemyTeam
		tl15 = "DM_MODE_TDM5"
	ELSE
		tl15 = "DM_MODE_TDM1"
	ENDIF	
	RETURN tl15
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Scoring
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL IS_TARGET_MATCH(ServerBroadcastData &serverBDpassed)
	
	IF IS_KING_OF_THE_HILL()
		RETURN serverBDPassed.iTarget > 0
	ELSE
		IF serverBDPassed.iTarget = DM_TARGET_KILLS_OFF
			RETURN FALSE
		ENDIF
		
		IF IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_TARGET_SCORE(ServerBroadcastData &serverBDpassed, INT iTeam)
	IF IS_TARGET_MATCH(serverBDpassed)
		
		IF NOT IS_THIS_LEGACY_DM_CONTENT()
		AND IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		AND IS_TEAM_VALID(iTeam)
			RETURN GET_TARGET_SCORE_VALUE(serverBDPassed.iTeamTarget[iTeam])
		ENDIF
		
		RETURN GET_TARGET_SCORE_VALUE(serverBDpassed.iTarget)
	ENDIF
	
	// If there is no target score, return a fake one so other logic will not break
	RETURN 996699
ENDFUNC

FUNC INT GET_JOB_DECIDER(ServerBroadcastData &serverBDpassed, INT iTeam)
	IF CONTENT_IS_USING_ARENA()
		IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			RETURN 12
		ENDIF
	ENDIF
	
	RETURN GET_TARGET_SCORE(serverBDpassed, iTeam)
ENDFUNC

/// PURPOSE:
///    Gets a team deathmatch score.
/// PARAMS:
///    iTeam - team to get score for.
/// RETURNS:
///    Team dm score.
FUNC INT GET_TEAM_DM_SCORE(ServerBroadcastData &serverBDpassed, INT iTeam, BOOL bForLTSLeaderboard = FALSE)

	INT iReturn = 0
	
	IF iTeam >= 0
	AND iTeam < MAX_NUM_DM_TEAMS
		IF bForLTSLeaderboard
			iReturn = serverBDpassed.iTeamScoreForLBD[iTeam]
		ELSE
			iReturn = serverBDpassed.iTeamScore[iTeam]
		ENDIF
	ENDIF
	
	IF iReturn > GET_TARGET_SCORE(serverBDpassed, iTeam)
		iReturn = GET_TARGET_SCORE(serverBDpassed, iTeam)
	ENDIF
	
	RETURN iReturn
ENDFUNC

FUNC INT GET_SCORE_FOR_POS(ServerBroadcastData_Leaderboard &serverBDpassedLDB, INT iPos)
	RETURN serverBDpassedLDB.leaderBoard[iPos].iScore
ENDFUNC

// Return local players score
FUNC INT GET_PLAYER_SCORE(ServerBroadcastData &serverBDpassed, PLAYER_INDEX thisPlayer, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	INT iReturn = 0
	IF NETWORK_IS_PLAYER_A_PARTICIPANT(thisPlayer)
		INT iPos = (GET_PLAYER_POSITION(serverBDpassedLDB, thisPlayer) -1)
		IF iPos <> -1	
		
			iReturn = serverBDpassedLDB.leaderBoard[iPos].iScore
		ENDIF
	ENDIF
	INT iTeam = GET_PLAYER_TEAM(thisPlayer)
	IF iReturn > GET_TARGET_SCORE(serverBDpassed, iTeam)
		iReturn = GET_TARGET_SCORE(serverBDpassed, iTeam)
	ENDIF
	
	RETURN iReturn
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Kill Streaks
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL IS_PLAYER_ON_KILL_STREAK()

	IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iKillStreak >= SMALL_STREAK
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_MED_STREAK_REWARD_FOR_TICKER()

	FLOAT fReward
	INT iReturn
	
	fReward = TO_FLOAT(MED_STREAK_KILLED_REWARD)
	fReward = (fReward * g_sMPTunables.cashMultiplier)
	iReturn = ROUND(fReward)
	
	RETURN iReturn
ENDFUNC

FUNC INT GET_BIG_STREAK_REWARD_FOR_TICKER()

	FLOAT fReward
	INT iReturn
	
	fReward = TO_FLOAT(MED_STREAK_KILLED_REWARD)
	fReward = (fReward * g_sMPTunables.cashMultiplier)
	iReturn = ROUND(fReward)
	
	RETURN iReturn
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Power Plays
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL MODE_HAS_POWER_PLAY()

	IF CONTENT_IS_USING_ARENA()
	OR IS_KING_OF_THE_HILL()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableDMPowerPlays)
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

PROC DO_BIG_MESSAGE_WITH_PLAYER(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, BIG_MESSAGE_TYPE bigMessage, PLAYER_INDEX playerId, INT iReward, STRING sStrap, BOOL bPowerPlay)
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		EXIT
	ENDIF
	
	IF NOT MODE_HAS_POWER_PLAY()
		EXIT
	ENDIF
	
	INT iPlayer = NATIVE_TO_INT(playerId)
	
//	BOOL bSameTeam = FALSE
//	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
//		bSameTeam = ARE_PLAYERS_ON_SAME_TEAM(playerId, LocalPlayer)
//	ENDIF
		
	IF bPowerPlay
		IF NOT IS_BIT_SET(dmVarsPassed.iPowerPlayBit, iPlayer)	
			IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_AWARDS)
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(bigMessage, playerId, -1, sStrap, "", GET_BIG_MESSAGE_COLOUR_FOR_PLAYER(playerId))
				PRINTLN("DO_BIG_MESSAGE_WITH_PLAYER Player = ", iPlayer)
				SET_BIT(dmVarsPassed.iPowerPlayBit, iPlayer)
			ENDIF
		ENDIF
	ELSE
		IF iReward = MED_STREAK_KILLED_REWARD
			IF NOT IS_BIT_SET(dmVarsPassed.iKillMedBountyBit, iPlayer)	
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_AWARDS)
					IF playerId = LocalPlayer
						SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(bigMessage, iReward, sStrap, "", HUD_COLOUR_WHITE)
					ELSE
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(bigMessage, playerId, iReward, sStrap, "", GET_BIG_MESSAGE_COLOUR_FOR_PLAYER(playerId))
					ENDIF
					PRINTLN("DO_BIG_MESSAGE_WITH_PLAYER Player = ", iPlayer, " iReward = ", iReward)
					SET_BIT(dmVarsPassed.iKillMedBountyBit, iPlayer)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(dmVarsPassed.iKillBigBountyBit, iPlayer)	
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_AWARDS)
					IF playerId = LocalPlayer
						SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(bigMessage, iReward, sStrap, "", HUD_COLOUR_WHITE)
					ELSE
						SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(bigMessage, playerId, iReward, sStrap, "", GET_BIG_MESSAGE_COLOUR_FOR_PLAYER(playerId))
					ENDIF
					PRINTLN("DO_BIG_MESSAGE_WITH_PLAYER Player = ", iPlayer, " iReward = ", iReward)
					SET_BIT(dmVarsPassed.iKillBigBountyBit, iPlayer)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_POWER_PLAYS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	UNUSED_PARAMETER(playerBDPassed)
	
	IF NOT MODE_HAS_POWER_PLAY()
		EXIT
	ENDIF

	INT iWinningPlayerScore, iRunnerUpScore, iDifference//, iPart
	FLOAT fWinnersScore, fRunnerUpScore, fThirtyPercentCalculation
	BOOL bThirtyPercentLead//, bPlayerQuit
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		IF serverBDpassed.playerPower <> INVALID_PLAYER_INDEX()
			serverBDpassed.playerPower = INVALID_PLAYER_INDEX()
			PRINTLN("[SERVER_POWER_PLAYS], RESET(HAS_DEATHMATCH_FINISHED) ")
		ENDIF
	ELSE
		
		// If the leading player has buggered off then exit for now
		IF NOT IS_NET_PLAYER_OK(serverBDpassed.playerWinning, FALSE)
			IF serverBDpassed.playerPower <> INVALID_PLAYER_INDEX()
				serverBDpassed.playerPowerStoredLast = serverBDpassed.playerPower
				serverBDpassed.playerPower = INVALID_PLAYER_INDEX()
				PRINTLN("[SERVER_POWER_PLAYS], RESET(inactive) ")
			ENDIF
			EXIT
		ELSE
			// Calculate Power Player
			iWinningPlayerScore 			= GET_SCORE_FOR_POS(serverBDpassedLDB, 0)
			iRunnerUpScore 					= GET_SCORE_FOR_POS(serverBDpassedLDB, 1)
			fWinnersScore 					= TO_FLOAT(iWinningPlayerScore)
			fRunnerUpScore					= TO_FLOAT(iRunnerUpScore)
			iDifference						= (iWinningPlayerScore - iRunnerUpScore)
			fThirtyPercentCalculation		= (fWinnersScore * 0.3)
			IF (fWinnersScore - fThirtyPercentCalculation) >= fRunnerUpScore
				bThirtyPercentLead = TRUE
			ELSE
				bThirtyPercentLead = FALSE
			ENDIF
			
			// Debug
			#IF IS_DEBUG_BUILD
				IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD6, KEYBOARD_MODIFIER_NONE, "")
					PRINTLN("[SERVER_POWER_PLAYS] playerWinning ", NATIVE_TO_INT(serverBDpassed.playerWinning))
					PRINTLN("[SERVER_POWER_PLAYS] playerPower ", NATIVE_TO_INT(serverBDpassed.playerPower))
					PRINTLN("[SERVER_POWER_PLAYS] iWinningPlayerScore ", iWinningPlayerScore)
					PRINTLN("[SERVER_POWER_PLAYS] iRunnerUpScore ", iRunnerUpScore)
					PRINTLN("[SERVER_POWER_PLAYS] iWinningPlayerScore ", iWinningPlayerScore)
					PRINTLN("[SERVER_POWER_PLAYS] iDifference ", iDifference)
					IF bThirtyPercentLead
						PRINTLN("[SERVER_POWER_PLAYS] bThirtyPercentLead TRUE ")
					ELSE
						PRINTLN("[SERVER_POWER_PLAYS] bThirtyPercentLead FALSE ")
					ENDIF
				ENDIF
			#ENDIF
			
			// Assgign power player
			IF iDifference >= POWER_PLAYER_LEAD
			AND bThirtyPercentLead
				IF serverBDpassed.playerPower = INVALID_PLAYER_INDEX()
					serverBDpassed.playerPower = serverBDpassed.playerWinning
					PRINTLN("[SERVER_POWER_PLAYS], NEW serverBDpassed.playerPower =  ", NATIVE_TO_INT(serverBDpassed.playerPower))
				ENDIF
			ELSE
				// Reset
				IF serverBDpassed.playerPower <> INVALID_PLAYER_INDEX()
					serverBDpassed.playerPowerStoredLast = serverBDpassed.playerPower
					serverBDpassed.playerPower = INVALID_PLAYER_INDEX()
					PRINTLN("[SERVER_POWER_PLAYS], RESET(diff) ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL AM_I_POWER_PLAYER(ServerBroadcastData &serverBDpassed)
	IF serverBDpassed.playerPower = LocalPlayer
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC DO_POWER_PLAYER_HELP(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_POWER_PLAYER_HLP)
		IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		OR NOT MODE_HAS_POWER_PLAY()

			SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_POWER_PLAYER_HLP)
		ELSE
			IF AM_I_POWER_PLAYER(serverBDpassed)
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_COLLECTED_POWER_PLAYER)  < 4
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("DM_PWR_HLP")
						SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_POWER_PLAYER_HLP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INCREMENT_POWER_PLAYER_STAT(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_POWER_PLAYER_STAT)
		IF AM_I_POWER_PLAYER(serverBDpassed)
			PRINTLN("INCREMENT_POWER_PLAYER_STAT yes")
			INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_DM_COLLECTED_POWER_PLAYER)
			SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_POWER_PLAYER_STAT)
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_POWER_PLAYS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	IF NOT MODE_HAS_POWER_PLAY()
		EXIT
	ENDIF

	IF serverBDpassed.playerPower = INVALID_PLAYER_INDEX()
		IF g_i_PowerPlayer <> -1
			CLEAR_BIT(dmVarsPassed.iPowerPlayBit, NATIVE_TO_INT(serverBDpassed.playerPowerStoredLast))

			IF IS_PLAYER_FORCE_BLIPPED(INT_TO_PLAYERINDEX(g_i_PowerPlayer)) 
				FORCE_BLIP_PLAYER(INT_TO_PLAYERINDEX(g_i_PowerPlayer), FALSE)
			ENDIF
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_POWER_PLAY)
			g_i_PowerPlayer = -1
			PRINTLN("CLIENT_POWER_PLAYS g_i_PowerPlayer INVALID_PLAYER_INDEX() ")
		ENDIF
	ELSE
		IF GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
			IF g_i_PowerPlayer = -1
				IF IS_NET_PLAYER_OK(serverBDpassed.playerPower)
					g_i_PowerPlayer = NATIVE_TO_INT(serverBDpassed.playerPower)
					PRINTLN("CLIENT_POWER_PLAYS g_i_PowerPlayer ", g_i_PowerPlayer)
					IF NOT IS_PLAYER_FORCE_BLIPPED(serverBDpassed.playerPower)
						FORCE_BLIP_PLAYER(serverBDpassed.playerPower, TRUE)
					ENDIF
					DO_BIG_MESSAGE_WITH_PLAYER(serverBDpassed, dmVarsPassed, BIG_MESSAGE_POWER_PLAY, serverBDpassed.playerPower, 0, "STRAP_POW", TRUE) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: VFX
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC DO_KILL_STREAK_BIG_MESSAGE()
	// "Kill Streak"
	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "BM_STREAK", "", HUD_COLOUR_WHITE)
ENDPROC

PROC DO_DEATH_STREAK_BIG_MESSAGE()
	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "BM_LOSER", "", HUD_COLOUR_RED)
ENDPROC

PROC START_EFFECT(SHARED_DM_VARIABLES &dmVarsPassed, INT iEffect)
	IF NOT IS_BIT_SET(dmVarsPassed.iFXBitset, iEffect)
		TRIGGER_FX_EFFECT(iEffect)	
		IF iEffect = FX_REASON_KILLSTREAK
			IF NOT IS_BIT_SET(dmVarsPassed.iFXBitset, FX_REASON_HAD_RAGE) // 1723205
				DO_KILL_STREAK_BIG_MESSAGE()
			ENDIF
		ELIF iEffect = FX_REASON_DEATHSTREAK
			DO_DEATH_STREAK_BIG_MESSAGE()
		ENDIF
		dmVarsPassed.iCurrentFX = iEffect
		SET_BIT(dmVarsPassed.iFXBitset, iEffect)
		PRINTLN(" [ DM_FX ] START_EFFECT iEffect = ", GET_FX_NAME(iEffect, FALSE))	
	ENDIF
ENDPROC

PROC CLEANUP_EFFECT(SHARED_DM_VARIABLES &dmVarsPassed, INT iEffect)
	IF IS_BIT_SET(dmVarsPassed.iFXBitset, iEffect)
		STOP_FX_EFFECT(iEffect)
		CLEAR_BIT(dmVarsPassed.iFXBitset, iEffect)
		PRINTLN(" [ DM_FX ] CLEANUP_EFFECT iEffect = ", GET_FX_NAME(iEffect, FALSE))
	ENDIF
ENDPROC

FUNC INT GET_CURRENT_ACTIVE_FX(SHARED_DM_VARIABLES &dmVarsPassed)
	RETURN dmVarsPassed.iCurrentFX
ENDFUNC

PROC CLEANUP_ALL_FX()
	STOP_FX_EFFECT(FX_REASON_RAGE)
	STOP_FX_EFFECT(FX_REASON_KILLSTREAK)
	STOP_FX_EFFECT(FX_REASON_POWER)
	STOP_FX_EFFECT(FX_REASON_DEATHSTREAK)
	PU_FORCE_END_RAGE()
ENDPROC

PROC CLEANUP_FX(SHARED_DM_VARIABLES &dmVarsPassed)
	CLEANUP_EFFECT(dmVarsPassed, FX_REASON_RAGE)
	CLEANUP_EFFECT(dmVarsPassed, FX_REASON_KILLSTREAK)
	CLEANUP_EFFECT(dmVarsPassed, FX_REASON_POWER)
	CLEANUP_EFFECT(dmVarsPassed, FX_REASON_DEATHSTREAK)
	CLEAR_BIT(dmVarsPassed.iFXBitset, FX_REASON_HAD_RAGE)
ENDPROC

PROC MANAGE_FX(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_noDMfx")
	#ENDIF
	
		EXIT
	ENDIF
	
	IF NOT MODE_HAS_POWER_PLAY()
		EXIT
	ENDIF
	
	INT iCurrentFXActive = GET_CURRENT_ACTIVE_FX(dmVarsPassed)

	IF IS_RAGE_ACTIVE()
		IF iCurrentFXActive <> FX_REASON_RAGE
			IF HAS_FX_OUTRO_FINISHED(iCurrentFXActive, FALSE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_KILLSTREAK)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_POWER)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_DEATHSTREAK)
				IF dmVarsPassed.iCurrentFX <> FX_REASON_RAGE
					dmVarsPassed.iCurrentFX = FX_REASON_RAGE 
				ENDIF
				SET_BIT(dmVarsPassed.iFXBitset, FX_REASON_HAD_RAGE)
				// Ryan's Brucie Box launches Rage
			ENDIF
		ENDIF
	ELIF AM_I_POWER_PLAYER(serverBDpassed)
		IF iCurrentFXActive <> FX_REASON_POWER
			IF HAS_FX_OUTRO_FINISHED(iCurrentFXActive, FALSE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_RAGE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_KILLSTREAK)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_DEATHSTREAK)
				START_EFFECT(dmVarsPassed, FX_REASON_POWER)
			ENDIF
		ENDIF
	ELIF IS_PLAYER_ON_KILL_STREAK()
		IF iCurrentFXActive <> FX_REASON_KILLSTREAK
			IF HAS_FX_OUTRO_FINISHED(iCurrentFXActive, FALSE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_RAGE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_POWER)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_DEATHSTREAK)
				START_EFFECT(dmVarsPassed, FX_REASON_KILLSTREAK)
			ENDIF
		ENDIF
	ELIF IS_PLAYER_ON_DEATH_STREAK()
	AND NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DEATH_STREAK)
			PRINTLN("NONBD_BITSET_DEATH_STREAK")
			SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DEATH_STREAK)
		ENDIF
		IF iCurrentFXActive <> FX_REASON_DEATHSTREAK
			IF HAS_FX_OUTRO_FINISHED(iCurrentFXActive, FALSE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_RAGE)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_KILLSTREAK)
				CLEANUP_EFFECT(dmVarsPassed, FX_REASON_POWER)
				START_EFFECT(dmVarsPassed, FX_REASON_DEATHSTREAK)
			ENDIF
		ENDIF
	ELSE
		IF iCurrentFXActive <> FX_REASON_CLEANUP
			IF HAS_FX_OUTRO_FINISHED(iCurrentFXActive, TRUE)
				CLEANUP_FX(dmVarsPassed)
				dmVarsPassed.iCurrentFX = FX_REASON_CLEANUP
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Idle
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC INT GET_IDLE_REWARD()

	FLOAT fReward
	INT iReturn
	
	fReward = TO_FLOAT(g_sMPTunables.iIdle_DM_Bounty)
	fReward = (fReward * g_sMPTunables.cashMultiplier)
	iReturn = ROUND(fReward)
	
	RETURN iReturn
ENDFUNC

FUNC BOOL AM_I_IDLE_PLAYER()
	IF IS_BIT_SET(g_i_IdlePlayerBit, iLocalPart)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Bounties
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL HAS_PLAYER_LOST_BOUNTY_CASH(SHARED_DM_VARIABLES &dmVarsPassed, INT iBountyReason)
	RETURN IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, iBountyReason)
ENDFUNC
PROC ASSIGN_CASH_BOUNTY(SHARED_DM_VARIABLES &dmVarsPassed, INT iBountyReason)

	INT iBountyCash

	SWITCH iBountyReason
		CASE BOUNTY_REASON_IDLE 	
			IF NOT IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_MED)
				IF NOT IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_BIG)
					IF NOT IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_IDLE)
						iBountyCash = GET_IDLE_REWARD()
						PRINTLN("[BOUNTY] ASSIGN_CASH_BOUNTY reason IDLE_KILLER_REWARD iBountyCash = ", iBountyCash, " g_sMPTunables.cashMultiplier = ", g_sMPTunables.cashMultiplier, " g_sMPTunables.iIdle_DM_Bounty = ", g_sMPTunables.iIdle_DM_Bounty)
						GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iPlayerBounty = iBountyCash	
						//IF iCurrentCash >= iBountyCash
						IF NETWORK_CAN_SPEND_MONEY(iBountyCash, FALSE, FALSE, FALSE)
							IF USE_SERVER_TRANSACTIONS()
								INT iTransactionID
								TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_BOUNTY_DM, iBountyCash, iTransactionID)
							ELSE
								NETWORK_SPENT_BOUNTY(iBountyCash)
							ENDIF
							
							PRINT_TICKER_WITH_INT("DM_IDLE_BNTY", iBountyCash)
						ENDIF
						SET_BIT(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_IDLE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE BOUNTY_REASON_KS_MED	
			IF NOT IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_BIG)
				IF NOT IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_MED)
					iBountyCash = MED_STREAK_KILLED_REWARD
					iBountyCash = MULTIPLY_CASH_BY_TUNABLE(iBountyCash)
					PRINTLN("[BOUNTY] ASSIGN_CASH_BOUNTY reason MED_STREAK_KILLED_REWARD ", iBountyCash)
					GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iPlayerBounty = iBountyCash
					SET_BIT(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_MED)
				ENDIF
			ENDIF
		BREAK
		CASE BOUNTY_REASON_KS_BIG	
			IF NOT IS_BIT_SET(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_BIG)
				iBountyCash = BIG_STREAK_KILLED_REWARD
				iBountyCash = MULTIPLY_CASH_BY_TUNABLE(iBountyCash)
				PRINTLN("[BOUNTY] ASSIGN_CASH_BOUNTY reason BIG_STREAK_KILLED_REWARD ", iBountyCash)
				GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iPlayerBounty = iBountyCash
				SET_BIT(dmVarsPassed.iBountyAssignedBitset, BOUNTY_REASON_KS_BIG)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_LOCAL_PART_HAVE_KILLSTREAK_BOUNTY(SHARED_DM_VARIABLES &dmVarsPassed)
	IF HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_KS_MED)
	OR HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_KS_BIG)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC HANDLE_KILL_STREAK_BOUNTY_MESSAGES(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], PLAYER_INDEX playerId, SHARED_DM_VARIABLES &dmVarsPassed)	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
	OR GET_CLIENT_MISSION_STAGE(playerBDPassed) >= CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
	OR NOT MODE_HAS_POWER_PLAY()
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		EXIT
	ELSE
		INT iPlayer = NATIVE_TO_INT(playerId)
		INT iLocalPlayer = NATIVE_TO_INT(LocalPlayer)
		IF IS_NET_PLAYER_OK(playerId, FALSE)
			INT iStreak 		= GlobalplayerBD_FM[iPlayer].iKillStreak
//			INT iLocalStreak 	= GlobalplayerBD_FM[iLocalPlayer].iKillStreak

			IF iStreak = MEDIUM_STREAK
				IF playerId = LocalPlayer
//					IF HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_KS_MED)
//						IF NOT IS_BIT_SET(dmVarsPassed.iKillMedBountyBit, iLocalPlayer)
//							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_MED_KS_MESSAGE)
//								// "Receiving double damage during this kill streak."
//								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_DOUBLE_DAMAGE, "", "STRAP_STRK3")
//								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_MED_KS_MESSAGE)
//							ENDIF		
//						ENDIF
//					ELSE
					IF NOT HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_KS_MED)
						// "$~1~ Bounty on your head"
						ASSIGN_CASH_BOUNTY(dmVarsPassed, BOUNTY_REASON_KS_MED)
						DO_BIG_MESSAGE_WITH_PLAYER(serverBDpassed, dmVarsPassed, BIG_MESSAGE_DOUBLE_DAMAGE, LocalPlayer, GET_MED_STREAK_REWARD_FOR_TICKER(), "STRAP_STRK2", FALSE)
					ENDIF
				ELSE
//					IF HAS_PLAYER_COLLECTED_DM_BOUNTY(iLocalPlayer)
//						IF NOT IS_BIT_SET(dmVarsPassed.iKillMedBountyBit, iLocalPlayer)
//							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_MED_KS_MESSAGE)
//								// "Double damage on kill streak player ~a~."
//								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_DOUBLE_DAMAGE, playerId, -1, "STRAP_STRK4")
//								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_MED_KS_MESSAGE)
//							ENDIF	
//						ENDIF
//					ELSE
					IF NOT HAS_PLAYER_COLLECTED_DM_BOUNTY(iLocalPlayer)
						// "$~1~ for killer of ~a~"
						//ASSIGN_CASH_BOUNTY(dmVarsPassed, BOUNTY_REASON_KS_MED)
						DO_BIG_MESSAGE_WITH_PLAYER(serverBDpassed, dmVarsPassed, BIG_MESSAGE_DOUBLE_DAMAGE, playerId, GET_MED_STREAK_REWARD_FOR_TICKER(), "STRAP_STRK1", FALSE)
					ENDIF
				ENDIF
			ELIF iStreak = BIG_STREAK
				IF playerId = LocalPlayer
//					IF HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_KS_BIG)
//						IF NOT IS_BIT_SET(dmVarsPassed.iKillBigBountyBit, iLocalPlayer)	
//							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_BIG_KS_MESSAGE)
//								// "Receiving double damage during this kill streak."
//								SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_DOUBLE_DAMAGE, "", "STRAP_STRK3")
//								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_BIG_KS_MESSAGE)
//							ENDIF
//						ENDIF
//					ELSE
					IF NOT HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_KS_BIG)
						// "$~1~ Bounty on your head"
						ASSIGN_CASH_BOUNTY(dmVarsPassed, BOUNTY_REASON_KS_BIG)
						DO_BIG_MESSAGE_WITH_PLAYER(serverBDpassed, dmVarsPassed, BIG_MESSAGE_DOUBLE_DAMAGE, LocalPlayer, GET_BIG_STREAK_REWARD_FOR_TICKER(), "STRAP_STRK2", FALSE)
					ENDIF
				ELSE
//					IF HAS_PLAYER_COLLECTED_DM_BOUNTY(iLocalPlayer)
//						IF NOT IS_BIT_SET(dmVarsPassed.iKillBigBountyBit, iLocalPlayer)	
//							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_BIG_KS_MESSAGE)
//								// "Double damage on kill streak player ~a~."
//								SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_DOUBLE_DAMAGE, playerId, -1, "STRAP_STRK4")
//								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_BIG_KS_MESSAGE)
//							ENDIF
//						ENDIF
//					ELSE
					IF NOT HAS_PLAYER_COLLECTED_DM_BOUNTY(iLocalPlayer)
						// "$~1~ for killer of ~a~"
						//ASSIGN_CASH_BOUNTY(dmVarsPassed, BOUNTY_REASON_KS_BIG)
						DO_BIG_MESSAGE_WITH_PLAYER(serverBDpassed, dmVarsPassed, BIG_MESSAGE_DOUBLE_DAMAGE, playerId, GET_BIG_STREAK_REWARD_FOR_TICKER(), "STRAP_STRK1", FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_BOUNTY()
	// Reset bounty cash
	GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iPlayerBounty = 0
	PRINTLN("[BOUNTY] RESET_BOUNTY ")
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Double Damage Taken/Dealt
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
FUNC BOOL SHOULD_PLAYER_RECEIVE_DOUBLE_DAMAGE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	IF NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		RETURN FALSE
	ENDIF

	IF AM_I_IDLE_PLAYER()
	
		RETURN TRUE
	ENDIF
	
	IF AM_I_POWER_PLAYER(serverBDpassed)
	
		RETURN TRUE
	ENDIF
	
	IF DOES_LOCAL_PART_HAVE_KILLSTREAK_BOUNTY(dmVarsPassed)
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_IDLE(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_IDLE)
		CLEAR_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_IDLE)
		PRINTLN("CLIENT_SETS_IDLE_BIT, CLEAR_BIT ")
	ENDIF
	IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeIdle)
		RESET_NET_TIMER(dmVarsPassed.timeIdle)
		PRINTLN("CLIENT_SETS_IDLE_BIT, RESET_NET_TIMER ")
	ENDIF
ENDPROC

PROC CLIENT_SET_IDLE_GLOBALS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, PLAYER_INDEX playerId, INT iParticipant)
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
	OR GET_CLIENT_MISSION_STAGE(playerBDPassed) >= CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
		CLEANUP_IDLE(playerBDPassed, dmVarsPassed)
	ELSE
		IF IS_BIT_SET(serverBDpassed.iIdleBitset, iParticipant)
			IF NOT IS_BIT_SET(g_i_IdlePlayerBit, iParticipant)
				PRINTLN("CLIENT_SET_IDLE_GLOBALS, SET_BIT", iParticipant)
				SET_BIT(g_i_IdlePlayerBit, iParticipant)
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_AWARDS)
					IF playerId = LocalPlayer
						IF HAS_PLAYER_LOST_BOUNTY_CASH(dmVarsPassed, BOUNTY_REASON_IDLE)
							// "You will receive double damage whilst idle."
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_DOUBLE_DAMAGE, "", "STRAP_IDLE3", HUD_COLOUR_RED)
						ELSE
							// "$~1~ Bounty on your head"
							SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_DOUBLE_DAMAGE, GET_IDLE_REWARD(), "STRAP_IDLE2", "", HUD_COLOUR_RED)
						ENDIF
					ELSE
						IF NOT HAS_PLAYER_COLLECTED_DM_BOUNTY(NATIVE_TO_INT(LocalPlayer))
//							// "Double damage on idle ~a~."
//							SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_DOUBLE_DAMAGE, playerId, -1, "STRAP_IDLE4")
//						ELSE
							// "$~1~ for killer of idle ~a~"
							SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_DOUBLE_DAMAGE, playerId, GET_IDLE_REWARD(), "STRAP_IDLE1", "", GET_BIG_MESSAGE_COLOUR_FOR_PLAYER(playerId))
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF IS_BIT_SET(g_i_IdlePlayerBit, iParticipant)
				PRINTLN("CLIENT_SET_IDLE_GLOBALS, CLEAR_BIT", iParticipant)
				CLEAR_BIT(g_i_IdlePlayerBit, iParticipant)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Might need to reset if idle player dies too
PROC CLIENT_SETS_IDLE_BIT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		EXIT
	ENDIF
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
	OR AM_I_POWER_PLAYER(serverBDpassed) // don't go idle if power player
	OR DOES_LOCAL_PART_HAVE_KILLSTREAK_BOUNTY(dmVarsPassed)
	OR NOT bLocalPlayerOK
		CLEANUP_IDLE(playerBDPassed, dmVarsPassed)
	ELSE
		IF IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL)
			CLEANUP_IDLE(playerBDPassed, dmVarsPassed)
		ELSE
			IF NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_IDLE)
				IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeIdle)
					PRINTLN("CLIENT_SETS_IDLE_BIT, START_NET_TIMER")
					START_NET_TIMER(dmVarsPassed.timeIdle)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeIdle, IDLE_TIMEOUT)	
						PRINTLN("CLIENT_SETS_IDLE_BIT, SET_BIT, CLIENT_BITSET_IDLE")
						SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_IDLE)
						
						ASSIGN_CASH_BOUNTY(dmVarsPassed, BOUNTY_REASON_IDLE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC APPLY_GIVING_DAMAGE(SHARED_DM_VARIABLES &dmVarsPassed, PLAYER_INDEX playerId, INT iDamageMultiplier)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_GIVEN)
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(playerId, (FREEMODE_PLAYER_WEAPON_DAMAGE * iDamageMultiplier))
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(playerId, (FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE * iDamageMultiplier))
		PRINTLN("[CS_DMG], APPLY_GIVING_DAMAGE, iDamageMultiplier = ", iDamageMultiplier)
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_GIVEN)
	ENDIF
ENDPROC

PROC CLEANUP_GIVING_DAMAGE(SHARED_DM_VARIABLES &dmVarsPassed, PLAYER_INDEX playerId)
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_GIVEN)
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(playerId, FREEMODE_PLAYER_WEAPON_DAMAGE)
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(playerId, FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE)
		PRINTLN("[CS_DMG], DOUBLE_DAMAGE, CLEANUP_GIVING_DAMAGE")
		CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_GIVEN)
	ENDIF
ENDPROC

PROC APPLY_RECEIVING_DAMAGE(SHARED_DM_VARIABLES &dmVarsPassed, PLAYER_INDEX playerId, INT iDamageMultiplier)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_RECEIVED)
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(playerId, (FREEMODE_PLAYER_WEAPON_DEFENSE * iDamageMultiplier))
		SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(playerId, (FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE * iDamageMultiplier))
		PRINTLN("[CS_DMG], DOUBLE_DAMAGE, APPLY_RECEIVING_DAMAGE, iDamageMultiplier = ", iDamageMultiplier)
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_RECEIVED)
	ENDIF
ENDPROC

PROC CLEANUP_RECEIVING_DOUBLE_DAMAGE(SHARED_DM_VARIABLES &dmVarsPassed, PLAYER_INDEX playerId)
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_RECEIVED)
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(playerId, FREEMODE_PLAYER_WEAPON_DEFENSE)
		SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(playerId, FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
		PRINTLN("[CS_DMG], DOUBLE_DAMAGE, CLEANUP_RECEIVING_DOUBLE_DAMAGE")
		CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_DOUBLE_DAMAGE_RECEIVED)
	ENDIF
ENDPROC

PROC CLIENT_MANAGES_DOUBLE_DAMAGE_RECEIVED(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF SHOULD_PLAYER_RECEIVE_DOUBLE_DAMAGE(serverBDpassed, dmVarsPassed)
		IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeDoubleDamage)
			APPLY_RECEIVING_DAMAGE(dmVarsPassed, LocalPlayer, DAMAGE_DOUBLE)
			START_NET_TIMER(dmVarsPassed.timeDoubleDamage) 
		ELSE
			IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeDoubleDamage, DOUBLE_DAMAGE_TIME)
				CLEANUP_RECEIVING_DOUBLE_DAMAGE(dmVarsPassed, LocalPlayer)
				RESET_NET_TIMER(dmVarsPassed.timeDoubleDamage)
			ENDIF
		ENDIF
	ELSE
		RESET_NET_TIMER(dmVarsPassed.timeDoubleDamage)
		CLEANUP_RECEIVING_DOUBLE_DAMAGE(dmVarsPassed, LocalPlayer)
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Blips
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ACTIVATE_ALL_BLIPS(SHARED_DM_VARIABLES &dmVarsPassed, INT iReason)
//	IF ARE_ALL_BLIPS_ON_FOR_PLAYER(dmVarsPassed) = FALSE
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ALL_BLIPS_ON)
		SWITCH iReason
//			CASE BLIP_REASON_POWER
//				PRINTLN("ACTIVATE_ALL_BLIPS,BLIP_REASON_POWER ")
//				dmVarsPassed.bBlipPower = TRUE		
//			BREAK
			
			CASE BLIP_REASON_DEATH
				PRINTLN("ACTIVATE_ALL_BLIPS,BLIP_REASON_DEATH ")
				dmVarsPassed.bBlipDeath = TRUE
			BREAK
			
			CASE BLIP_REASON_STREAK
				PRINTLN("ACTIVATE_ALL_BLIPS, BLIP_REASON_STREAK ")
				dmVarsPassed.bBlipStreak = TRUE
			BREAK
			
			CASE BLIP_REASON_PERK
				PRINTLN("ACTIVATE_ALL_BLIPS, BLIP_REASON_PERK ")
				dmVarsPassed.bBlipPerk = TRUE
			BREAK
			
			CASE BLIP_REASON_START
				PRINTLN("ACTIVATE_ALL_BLIPS, BLIP_REASON_START ")
				dmVarsPassed.bBlipStart = TRUE
			BREAK
			
			CASE BLIP_REASON_END
				PRINTLN("ACTIVATE_ALL_BLIPS, BLIP_REASON_END ")
				dmVarsPassed.bBlipEnd = TRUE
			BREAK
		ENDSWITCH
		SHOW_ALL_PLAYER_BLIPS(TRUE)
		dmVarsPassed.bBlipAll = TRUE
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ALL_BLIPS_ON)
		PRINTLN("ACTIVATE_ALL_BLIPS TRUE ", iReason)
	ENDIF
ENDPROC

PROC DEACTIVATE_ALL_BLIPS(SHARED_DM_VARIABLES &dmVarsPassed)
//	IF ARE_ALL_BLIPS_ON_FOR_PLAYER(dmVarsPassed) NONBD_BITSET_ALL_BLIPS_ON
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ALL_BLIPS_ON)
		dmVarsPassed.bBlipPower = FALSE
		dmVarsPassed.bBlipDeath = FALSE
		dmVarsPassed.bBlipStreak = FALSE
		dmVarsPassed.bBlipStart = FALSE
		dmVarsPassed.bBlipEnd = FALSE
		SHOW_ALL_PLAYER_BLIPS(FALSE)
		dmVarsPassed.bBlipAll = FALSE
		dmVarsPassed.bBlipPerk = FALSE
		PRINTLN("DEACTIVATE_ALL_BLIPS ")
		CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ALL_BLIPS_ON)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SHOW_ALL_BLIPS_END_OF_DM(ServerBroadcastData &serverBDpassed)

	IF NOT MODE_HAS_POWER_PLAY()
		RETURN FALSE
	ENDIF

	IF IS_ONLY_ONE_KILL_REMAINING(serverBDpassed)
	
		RETURN TRUE
	ENDIF
	
	IF IS_ONLY_30_SECONDS_LEFT(serverBDpassed)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLIENT_MANAGES_BLIPS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	OR NOT MODE_HAS_POWER_PLAY()
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		EXIT
	ENDIF
	
	IF NOT HAS_DM_STARTED(serverBDpassed)
		SHOW_ALL_PLAYER_BLIPS(TRUE)
	ELSE
		IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeBlipsFirst5)
			START_NET_TIMER(dmVarsPassed.timeBlipsFirst5)
		ELSE
			IF NOT HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeBlipsFirst5, BLIPS_ON_START_DM_DURATION)
				ACTIVATE_ALL_BLIPS(dmVarsPassed, BLIP_REASON_START)
			ELSE
				IF SHOULD_SHOW_ALL_BLIPS_END_OF_DM(serverBDpassed)
					ACTIVATE_ALL_BLIPS(dmVarsPassed, BLIP_REASON_END)
				ELSE
					IF dmVarsPassed.bBlipStart = TRUE
						DEACTIVATE_ALL_BLIPS(dmVarsPassed)
					ELSE
					
	//					IF AM_I_POWER_PLAYER(serverBDpassed)
	//						ACTIVATE_ALL_BLIPS(dmVarsPassed, BLIP_REASON_POWER)
	//					ELIF DOES_LOCAL_PART_HAVE_KILLSTREAK_BOUNTY(dmVarsPassed)
	//						ACTIVATE_ALL_BLIPS(dmVarsPassed, BLIP_REASON_STREAK)
	//					ELSE
							IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timePerk) //1252613
	//						AND NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeBlipsDeathStreak)
								DEACTIVATE_ALL_BLIPS(dmVarsPassed)
							ENDIF
	//					ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC DM_BLIP_TYPE GET_DM_PLAYER_BLIP_TYPE(PlayerBroadcastData &playerBDPassed[])
	RETURN playerBDPassed[iLocalPart].eBlipType
ENDFUNC

PROC SET_DM_PLAYER_BLIP_TYPE(PlayerBroadcastData &playerBDPassed[], DM_BLIP_TYPE eBlipType)
	playerBDPassed[iLocalPart].eBlipType = eBlipType
ENDPROC

FUNC INT GET_DM_PLAYER_CURRENT_MODSET(PlayerBroadcastData &playerBDPassed[])
	RETURN playerBDPassed[iLocalPart].iCurrentModSet
ENDFUNC

PROC SET_DM_PLAYER_CURRENT_MODSET(PlayerBroadcastData &playerBDPassed[], INT iCurrentModset)
	playerBDPassed[iLocalPart].iCurrentModSet = iCurrentModset
ENDPROC

PROC CACHE_DM_PLAYER_CURRENT_MODSET(SHARED_DM_VARIABLES &dmVarsPassed, INT iCurrentModset)
	dmVarsPassed.iCachedModset = iCurrentModset
ENDPROC

PROC PROCESS_DM_HIDE_MY_PLAYER_BLIP_FROM_ENEMY(BOOL bHide)

	INT iTeam

	IF IS_TEAM_DEATHMATCH()
		REPEAT g_FMMC_STRUCT.iMaxNumberOfTeams iTeam
			IF GET_PLAYER_TEAM(LocalPlayer) != iTeam
				HIDE_MY_PLAYER_BLIP_FROM_TEAM(bHide, iTeam)
			ENDIF
		ENDREPEAT
	ELSE
		HIDE_MY_PLAYER_BLIP(bHide)
	ENDIF
	
ENDPROC

// Adapted from PROCESS_PLAYER_SHOW_BLIP_IF_IDLE in fm_mission_controller_players.sch
PROC PROCESS_PLAYER_SHOW_BLIP_IF_CAMPING(INT iCurrentModset)

	PED_INDEX pedLocalPlayer = LocalPlayerPed
		
	IF NOT HAS_NET_TIMER_STARTED(tCampTimer)
	
		vLastCachedPosition = GET_ENTITY_COORDS(pedLocalPlayer)
		iCampBlipExitRetry = 0
		
		SET_MY_PLAYER_BLIP_AS_IDLE(FALSE)
		START_NET_TIMER(tCampTimer)
		
	ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tCampTimer) > (500 * iCampBlipExitRetry)	
		
		INT iCampRadius = PICK_INT(IS_PED_IN_ANY_VEHICLE(pedLocalPlayer, TRUE), ciDM_DEFAULT_CAMP_RADIUS_VEHICLE, ciDM_DEFAULT_CAMP_RADIUS_FOOT)
		INT iCampTimer = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierBlips.iCampTimer
		
		// Check every 500ms if the player is out of camping range, if so reset and cache a new position
		IF VDIST(GET_ENTITY_COORDS(pedLocalPlayer), vLastCachedPosition) > iCampRadius
		
			RESET_NET_TIMER(tCampTimer)
		
		// Otherwise, if the player is still within the camp radius when the timer has expired, blip them
		ELIF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(tCampTimer) > GET_DM_CAMP_TIMER_FROM_SELECTION(iCampTimer)
			
			SET_MY_PLAYER_BLIP_AS_IDLE(TRUE)
			
		ENDIF
		
		iCampBlipExitRetry++
	
	ENDIF
	
ENDPROC

PROC PROCESS_CUSTOM_DM_BLIPS(PlayerBroadcastData &playerBDPassed[])

	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	INT iCurrentModset = playerBDPassed[iLocalPart].iCurrentModSet
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet)
		PRINTLN("[DM_BLIP_EVENT] PROCESS_CUSTOM_DM_BLIPS - Invalid modset! Modset: ", iCurrentModSet)
		EXIT
	ENDIF
	
	IF IS_PLAYER_DEAD(LocalPlayer)
		SET_DM_PLAYER_BLIP_TYPE(playerBDPassed, DM_BLIP_TYPE__DEFAULT)
		EXIT
	ENDIF
	
	DM_BLIP_TYPE eBlipType = g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[iCurrentModSet].sModifierBlips.eBlipType
	PRINTLN("[DM_BLIP_EVENT] PROCESS_CUSTOM_DM_BLIPS - eBlipType: ", eBlipType)

	SWITCH eBlipType
	
		CASE DM_BLIP_TYPE__DEFAULT
		BREAK
	
		CASE DM_BLIP_TYPE__HIDE
			
			// Only process on change
			IF GET_DM_PLAYER_BLIP_TYPE(playerBDPassed) != eBlipType
				PROCESS_DM_HIDE_MY_PLAYER_BLIP_FROM_ENEMY(TRUE)
				BROADCAST_DM_FORCE_PLAYER_BLIP(FALSE)
			ENDIF
			
		BREAK
		
		CASE DM_BLIP_TYPE__FORCE_ON
			
			// Only process on change
			IF GET_DM_PLAYER_BLIP_TYPE(playerBDPassed) != eBlipType
				PROCESS_DM_HIDE_MY_PLAYER_BLIP_FROM_ENEMY(FALSE)
				BROADCAST_DM_FORCE_PLAYER_BLIP(TRUE)
			ENDIF
			
		BREAK
		
		CASE DM_BLIP_TYPE__CAMPERS
		CASE DM_BLIP_TYPE__ONLY_CAMPERS
			
			// Only process on change
			IF GET_DM_PLAYER_BLIP_TYPE(playerBDPassed) != eBlipType
			
				IF eBlipType = DM_BLIP_TYPE__CAMPERS
					PROCESS_DM_HIDE_MY_PLAYER_BLIP_FROM_ENEMY(FALSE)
				ELIF eBlipType = DM_BLIP_TYPE__ONLY_CAMPERS
					PROCESS_DM_HIDE_MY_PLAYER_BLIP_FROM_ENEMY(TRUE)
				ENDIF
				
				BROADCAST_DM_FORCE_PLAYER_BLIP(FALSE)
				
			ENDIF
		
			// Process every frame
			PROCESS_PLAYER_SHOW_BLIP_IF_CAMPING(iCurrentModset)
			
		BREAK
	
	ENDSWITCH
	
	IF GET_DM_PLAYER_BLIP_TYPE(playerBDPassed) != eBlipType
		PRINTLN("[DM_BLIP_EVENT] PROCESS_CUSTOM_DM_BLIPS - Setting player eBlipType to: ", eBlipType)
		SET_DM_PLAYER_BLIP_TYPE(playerBDPassed, eBlipType)
	ENDIF
	
ENDPROC

FUNC INT  GET_ARENA_TDM_BLIP_COLOUR(INT iTeam)
	SWITCH iTeam
		CASE 0 RETURN BLIP_COLOUR_ORANGE
		CASE 1 RETURN BLIP_COLOUR_PURPLE
		CASE 2 RETURN BLIP_COLOUR_PINK
		CASE 3 RETURN BLIP_COLOUR_GREEN
	ENDSWITCH
	RETURN BLIPCOLOUR_NET_PLAYER1
ENDFUNC

PROC CLIENT_CUSTOM_TDM_BLIP_COLOURS(PlayerBroadcastData &playerBDPassed[])
	
	IF NOT IS_TEAM_DEATHMATCH()
		EXIT
	ENDIF
	
	IF NOT CONTENT_IS_USING_ARENA()
	AND NOT IS_KING_OF_THE_HILL()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_UseCustomTDMPlayerBlipColours)
		EXIT
	ENDIF
	
	INT iPlayer
	FOR iPlayer = 0 TO (NUM_NETWORK_PLAYERS - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			INT iBlipColour
			
			IF IS_KING_OF_THE_HILL()
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_UseCustomTDMPlayerBlipColours)
				IF playerBDPassed[iLocalPart].iTeam = playerBDPassed[iPlayer].iTeam
					iBlipColour = BLIP_COLOUR_BLUE
				ELSE
					iBlipColour = GET_BLIP_COLOUR_FROM_HUD_COLOUR(GET_HUD_COLOUR_FOR_ENEMY_TEAM(playerBDPassed[iLocalPart].iTeam, playerBDPassed[iPlayer].iTeam))
				ENDIF
			ENDIF
			
			IF CONTENT_IS_USING_ARENA()
			OR USE_KOTH_TEAM_COLOURS()
				iBlipColour = GET_ARENA_TDM_BLIP_COLOUR(playerBDPassed[iPlayer].iTeam)
			ENDIF
			
			SET_CUSTOM_BLIP_COLOUR_FOR_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer), iBlipColour, TRUE)
		ENDIF
	ENDFOR
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Perks
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC ACTIVATE_HIDE_BLIP_PERK(SHARED_DM_VARIABLES &dmVarsPassed, BOOL bHide)
	IF bHide
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_HIDE_BLIPS)
			PRINTLN("[CS_PERKS] ACTIVATE_HIDE_BLIP_PERK")
			HIDE_MY_PLAYER_BLIP(TRUE) 
			SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_HIDE_BLIPS)
		ENDIF
	ELSE
		IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_HIDE_BLIPS)
			HIDE_MY_PLAYER_BLIP(FALSE) 
			CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_HIDE_BLIPS) 
		ENDIF
	ENDIF
ENDPROC

PROC ACTIVATE_CHOSEN_PERK(SHARED_DM_VARIABLES &dmVarsPassed)
	SWITCH g_e_Chosen_Perk
	
		// all blips
		CASE KS_PP_SQUARE_ALL_BLIP
			PRINTLN("[CS_PERKS] ACTIVATE_CHOSEN_PERK, BLIP_REASON_PERK")
			ACTIVATE_ALL_BLIPS(dmVarsPassed, BLIP_REASON_PERK)
		BREAK
		
		// quad damage
		CASE KS_PP_TRIANGLE_QUAD
			PRINTLN("[CS_PERKS] ACTIVATE_CHOSEN_PERK, KS_PP_TRIANGLE_QUAD")
			APPLY_GIVING_DAMAGE(dmVarsPassed, LocalPlayer, DAMAGE_QUAD)
		BREAK
		
		// hide blip
		CASE KS_PP_CIRCLE_HIDE_BLIP
			PRINTLN("[CS_PERKS] ACTIVATE_CHOSEN_PERK, KS_PP_CIRCLE_HIDE_BLIP")
			ACTIVATE_HIDE_BLIP_PERK(dmVarsPassed, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEANUP_CHOSEN_PERK(SHARED_DM_VARIABLES &dmVarsPassed)
	SWITCH g_e_Chosen_Perk
	
		// all blips
		CASE KS_PP_SQUARE_ALL_BLIP
			PRINTLN("[CS_PERKS] CLEANUP_CHOSEN_PERK, START_NET_TIMER")
			DEACTIVATE_ALL_BLIPS(dmVarsPassed)
		BREAK
		
		// quad damage
		CASE KS_PP_TRIANGLE_QUAD
			PRINTLN("[CS_PERKS] CLEANUP_CHOSEN_PERK, KS_PP_TRIANGLE_QUAD")
			CLEANUP_GIVING_DAMAGE(dmVarsPassed, LocalPlayer)
		BREAK
		
		CASE KS_PP_CIRCLE_HIDE_BLIP
			PRINTLN("[CS_PERKS] CLEANUP_CHOSEN_PERK, KS_PP_CIRCLE_HIDE_BLIP")
			ACTIVATE_HIDE_BLIP_PERK(dmVarsPassed, FALSE)
		BREAK
	ENDSWITCH
ENDPROC

PROC RESET_PERK(SHARED_DM_VARIABLES &dmVarsPassed)
	RESET_NET_TIMER(dmVarsPassed.timePerk)
	g_e_Chosen_Perk = KS_PP_NOT_USE
	dmVarsPassed.iPerkBarProgress = 0
	PRINTLN("[CS_PERKS] RESET_PERK ")
ENDPROC

PROC CLIENT_HANDLE_DM_PERKS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	OR NOT MODE_HAS_POWER_PLAY()
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		EXIT
	ENDIF

	IF g_e_Chosen_Perk <> KS_PP_NOT_USE
		// Check they are alive otherwise net_spawn will override damage
		IF bLocalPlayerOK
			IF GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_FIGHTING
				IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timePerk)
					PRINTLN("[CS_PERKS] CLIENT_HANDLE_DM_PERKS, START_NET_TIMER")
					START_NET_TIMER(dmVarsPassed.timePerk)
				ELSE
					IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timePerk, PERK_TIMEOUT)
						PRINTLN("[CS_PERKS] CLIENT_HANDLE_DM_PERKS, TIMER ")
						CLEANUP_CHOSEN_PERK(dmVarsPassed)
						RESET_PERK(dmVarsPassed)
					ELSE
						// Do perk
						ACTIVATE_CHOSEN_PERK(dmVarsPassed)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(dmVarsPassed.timePerk)
			PRINTLN("[CS_PERKS] CLIENT_HANDLE_DM_PERKS, RESET_NET_TIMER ")
			RESET_PERK(dmVarsPassed)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Kills
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC INCREMENT_HEADSHOTS(PlayerBroadcastData &playerBDPassed[])
	playerBDPassed[iLocalPart].iHeadshots += DEATHMATCH_ONE_POINT
	PRINTLN("INCREMENT_HEADSHOTS ")
ENDPROC

PROC PROCESS_CUSTOM_DM_ON_KILL(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, TARGET_FLOATING_SCORE &sTargetFloatingScores[], PlayerBroadcastData &playerBDPassed[], PLAYER_MODIFIER_DATA &sPlayerModifierData)

	IF NOT NETWORK_IS_PLAYER_ACTIVE(LocalPlayer)
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_ON_KILL - Player is no longer active!")
		EXIT
	ENDIF

	INT iCurrentModset = GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed)
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet) 
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_ON_KILL - Invalid modset! Modset: ", iCurrentModSet)
		EXIT
	ENDIF
	
	PROCESS_CUSTOM_DM_GIVE_LIVES(playerBDPassed[iLocalPart].iExtraLives, iCurrentModset, sTargetFloatingScores)
	PROCESS_CUSTOM_DM_GIVE_HEALTH(sDamageEvent.Damage, playerBDPassed[iLocalPart].iCurrentModset, sTargetFloatingScores)
	PROCESS_CUSTOM_DM_GIVE_AMMO(sPlayerModifierData, playerBDPassed[iLocalPart].iCurrentModset, sTargetFloatingScores)
	PROCESS_CUSTOM_DM_GIVE_ARMOUR(iCurrentModset, sTargetFloatingScores)
	
ENDPROC

PROC PROCESS_CUSTOM_DM_ON_HIT(STRUCT_ENTITY_DAMAGE_EVENT &sDamageEvent, TARGET_FLOATING_SCORE &sTargetFloatingScores[], PlayerBroadcastData &playerBDPassed[], PLAYER_MODIFIER_DATA &sPlayerModifierData)

	IF NOT NETWORK_IS_PLAYER_ACTIVE(LocalPlayer)
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_ON_HIT - Player is no longer active!")
		EXIT
	ENDIF

	INT iCurrentModset = GET_DM_PLAYER_CURRENT_MODSET(playerBDPassed)
	IF NOT IS_CURRENT_MODIFIER_SET_VALID(iCurrentModSet) 
		PRINTLN("[DM_ONKILL_EVENT] PROCESS_CUSTOM_DM_ON_HIT - Invalid modset! Modset: ", iCurrentModSet)
		EXIT
	ENDIF
	
	PROCESS_CUSTOM_DM_GIVE_HEALTH(sDamageEvent.Damage, playerBDPassed[iLocalPart].iCurrentModset, sTargetFloatingScores, FALSE)
	PROCESS_CUSTOM_DM_GIVE_AMMO(sPlayerModifierData, playerBDPassed[iLocalPart].iCurrentModSet, sTargetFloatingScores, FALSE)
	PROCESS_CUSTOM_DM_GIVE_ARMOUR(playerBDPassed[iLocalPart].iCurrentModset, sTargetFloatingScores, FALSE)
	
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server Checks
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL HAS_SERVER_FINISHED(ServerBroadcastData &serverBDpassed)
	IF GET_SERVER_GAME_STATE(serverBDpassed) = GAME_STATE_END
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SERVER_CHECKED_EVERYONE_READY(ServerBroadcastData &serverBDPassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_READY)
ENDFUNC

FUNC BOOL SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(ServerBroadcastData &serverBDpassed)
	IF IS_THIS_A_ROUNDS_MISSION_FOR_CORONA()
	AND g_MissionControllerserverBD_LB.iHashMAC != 0
	AND g_MissionControllerserverBD_LB.iMatchTimeID != 0
		PRINTLN("SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2 - Already set up for the round!")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		IF PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBDpassed.iHashedMac, serverBDpassed.iMatchHistoryId)
			PRINTLN("SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2 - Playstats are now set up!")
			RETURN TRUE
		ENDIF
	ELSE	
		IF PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(g_MissionControllerserverBD_LB.iHashMAC, g_MissionControllerserverBD_LB.iMatchTimeID)
			PRINTLN("SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2 - Playstats are now set up!")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Vehicles
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL ARE_PLANE_WINGS_BUSTED(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
			IF NOT IS_ENTITY_DEAD(dmVarsPassed.dmVehIndex)
				IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex))
					IF NOT ARE_WINGS_OF_PLANE_INTACT(dmVarsPassed.dmVehIndex)					
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEH_IN_DEEP_WATER_FOR_TIME(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_ENTITY_IN_DEEP_WATER(dmVarsPassed.dmVehIndex, TRUE)
		IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeWaterCheck)
			START_NET_TIMER(dmVarsPassed.timeWaterCheck)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeWaterCheck, WATER_RESPAWN_TIME)
			
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeWaterCheck)
			RESET_NET_TIMER(dmVarsPassed.timeWaterCheck)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Rounds
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

FUNC BOOL SHOULD_DRAW_ROUNDS_LBD(ServerBroadcastData &serverBDPassed)
	RETURN IS_BIT_SET(serverBDPassed.iServerBitSet2, SERV_BITSET2_ROUNDS_LBD_DO)
ENDFUNC

FUNC BOOL SHOULD_I_JOIN_SMALLEST_TEAM(ServerBroadcastData &serverBDPassed, INT iLBPosition, BOOL bAlwaysSwap = FALSE)

	BOOL bJoin = FALSE
	
	INT iSmallestTeam = FIND_SMALLEST_TEAM_FOR_SWAP_TO_SMALLEST_TEAM_MISSION()
	INT iSmallestTeamPlayers = 0
	
	PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - iSmallestTeam = ", iSmallestTeam)
	
	IF iSmallestTeam != -1
		
		iSmallestTeamPlayers = g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iSmallestTeam]
		
		PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - iSmallestTeamPlayers = ",iSmallestTeamPlayers)
		
		INT iMyPosition = -1
		INT iLBPos = 0
		
		INT i = 0
		REPEAT COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) i
			PLAYER_INDEX tempPlayer = g_MissionControllerserverBD_LB.sleaderboard[i].playerID
			
			IF tempPlayer != INVALID_PLAYER_INDEX()
			AND g_MissionControllerserverBD_LB.sleaderboard[i].iTeam != -1
				PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - Leaderboard pos ",i," contains participant ",g_MissionControllerserverBD_LB.sleaderboard[i].iParticipant," from team ",g_MissionControllerserverBD_LB.sleaderboard[i].iTeam)
				IF g_MissionControllerserverBD_LB.sleaderboard[i].iTeam = serverBDpassed.iWinningTeam
					PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - w winning team ",serverBDpassed.iWinningTeam)
					IF tempPlayer = LocalPlayer
						iMyPosition = iLBPos
						PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - it's me, position in winning team is ",iMyPosition)
						i = COUNT_OF(g_MissionControllerserverBD_LB.sleaderboard) //Break out!
					ELSE
						PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - it's not me, position in winning team is ",iLBPos)
						iLBPos++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iMyPosition != -1
			IF iMyPosition < iSmallestTeamPlayers
				PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - My position on the winning team is high enough to join! iMyPosition (",iMyPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				bJoin = TRUE
			ELSE
				PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - My position on the winning team is too low: iMyPosition (",iMyPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
			ENDIF
		ELSE
			PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - I'm not on the winning team")
			IF iLBPos = 0 // No winning team, pick from who came top on the leaderboard
				PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - Nobody was on the winning team! (serverBDpassed.iWinningTeam = ",serverBDpassed.iWinningTeam,")")
				IF iLBPosition < iSmallestTeamPlayers
					PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - but my position on the leaderboard is high enough anyway! iLBPosition (",iLBPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
					bJoin = TRUE
				ELSE
					PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - My position on the winning team is too low: iLBPosition (",iLBPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				ENDIF
			ELIF bAlwaysSwap AND (iSmallestTeam != GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen)
				IF iLBPosition < iSmallestTeamPlayers
					PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - TRUE - bAlwaysSwap - but my position on the leaderboard is high enough anyway! iLBPosition (",iLBPosition,") < iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
					bJoin = TRUE
				ELSE
					PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - bAlwaysSwap - My position on the winning team is too low: iLBPosition (",iLBPosition,") >= iSmallestTeamPlayers (",iSmallestTeamPlayers,")")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bAlwaysSwap AND (iSmallestTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen)
		bJoin = FALSE
	ENDIF
	
	
	PRINTLN("[DM-Rounds] - SHOULD_I_JOIN_SMALLEST_TEAM - ***** Returning ",bJoin," *****")
	
	RETURN bJoin
	
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_TIMER_EXPIRED(SHARED_DM_VARIABLES &dmVarsPassed)	
	
	IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.tdForceEndTimer)
		START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
		
		REINIT_NET_TIMER(dmVarsPassed.tdForceEndTimer)
	ELSE
		IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(dmVarsPassed.tdForceEndTimer) > VIEW_LEADERBOARD_TIME
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS()

	#IF IS_DEBUG_BUILD
	PRINTLN("[NETCELEBRATION] [MSROUND] - RESET_ROUNDS_MISSION_CELEBRATION_GLOBALS")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartRp				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel			= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl		= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl		= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachEndLvl		= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints				= 0
	g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash					= 0
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Friendly Fire
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC PROCESS_FRIENDLY_FIRE_SETTING_FOR_TEAM(INT iTeam)
	
	IF iTeam = -1
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_FriendlyFireTeam0+iTeam)
		NETWORK_SET_FRIENDLY_FIRE_OPTION(TRUE)
		SET_PLAYER_MODIFIER_CONTEXT_HELP_TEXT_REQUIRED(ciPLAYER_MODIFIER_HELP_FRIENDLY_FIRE_ON)
		PRINTLN("PROCESS_FRIENDLY_FIRE_SETTING_FOR_TEAM - Enabling Friendly Fire")
	ELSE
		NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
		PRINTLN("PROCESS_FRIENDLY_FIRE_SETTING_FOR_TEAM - Disabling Friendly Fire")
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Misc Player
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PARACHUTES()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyFour, ciOptionsBS24_GiveParachute)
		IF NOT HAS_PED_GOT_WEAPON(LocalPlayerPed, GADGETTYPE_PARACHUTE)
			GIVE_WEAPON_TO_PED(LocalPlayerPed, GADGETTYPE_PARACHUTE, 100, DEFAULT, FALSE)
			EQUIP_STORED_MP_PARACHUTE(LocalPlayer)
			PRINTLN("[DM] Giving parachute to the player due to ciOptionsBS24_GiveParachute!")
		ENDIF
	ENDIF
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_DisableTakeOffParachutePack, TRUE)
	ENDIF
	
ENDPROC
