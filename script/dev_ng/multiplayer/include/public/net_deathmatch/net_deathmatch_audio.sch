USING "net_deathmatch_leaderboard.sch"

FUNC BOOL LOAD_DM_SOUNDS()
	BOOL bAllLoaded = TRUE
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AW_PTB_01")
			PRINTLN("[TMS] LOAD_SOUNDS - REQUEST_SCRIPT_AUDIO_BANK(DLC_AWXM2018/AW_PTB_01) loaded successfully.")
		ELSE
			PRINTLN("[TMS] LOAD_SOUNDS - REQUEST_SCRIPT_AUDIO_BANK(DLC_AWXM2018/AW_PTB_01) currently loading.")
			bAllLoaded = FALSE
		ENDIF
		
		START_AUDIO_SCENE("DLC_AW_PTB_General_Scene")
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/KOTH")
			PRINTLN("[TMS] LOAD_SOUNDS - REQUEST_SCRIPT_AUDIO_BANK(DLC_VINEWOOD/KOTH) loaded successfully.")
		ELSE
			PRINTLN("[TMS] LOAD_SOUNDS - REQUEST_SCRIPT_AUDIO_BANK(DLC_VINEWOOD/KOTH) currently loading.")
			bAllLoaded = FALSE
		ENDIF
		
		//START_AUDIO_SCENE("DLC_AW_PTB_General_Scene")
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		IF NOT LOAD_POWERUPS_AUDIO()
			PRINTLN("LOAD_SOUNDS - Powerup audio bank currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
		START_AUDIO_SCENE("Gokart_Adversary_In_Gameplay_Scene")
	ENDIF
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
		IF REQUEST_SCRIPT_AUDIO_BANK("DLC_AIRRACES/AIR_RACE_01")
			PRINTLN("[TMS] LOAD_SOUNDS - REQUEST_SCRIPT_AUDIO_BANK(DLC_AIRRACES/AIR_RACE_01) loaded successfully.")
		ELSE
			PRINTLN("[TMS] LOAD_SOUNDS - REQUEST_SCRIPT_AUDIO_BANK(DLC_AIRRACES/AIR_RACE_01) currently loading.")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	RETURN bAllLoaded
ENDFUNC

PROC END_DM_SOUND(INT& iSnd)
	IF iSnd > -1
		STOP_SOUND(iSnd)
		
		IF iSnd > -1
			RELEASE_SOUND_ID(iSnd)
			iSnd = -1
		ELSE
			PRINTLN("[TMS][TMSSND] iSnd is -1 now after stop sound")
		ENDIF
	ENDIF
ENDPROC

PROC UNLOAD_DM_SOUNDS(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
		
		END_DM_SOUND(dmVarsPassed.iTagBombCountdownSndID)

		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_AWXM2018/AW_PTB_01")
		PRINTLN("[TMS] UNLOAD_SOUNDS - Releasing DLC_AWXM2018/AW_PTB_01")
		
		STOP_AUDIO_SCENE("DLC_AW_PTB_General_Scene")
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_VINEWOOD/KOTH")
		PRINTLN("[TMS] UNLOAD_SOUNDS - Releasing DLC_VINEWOOD/KOTH")
	ENDIF
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
		STOP_AUDIO_SCENE("Gokart_Adversary_In_Gameplay_Scene")
	ENDIF
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_AIRRACES/AIR_RACE_01")
	ENDIF
ENDPROC

PROC MANAGE_AUDIO_MIX_GROUPS(SHARED_DM_VARIABLES &dmVarsPassed)
	// Mix groups
	IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timerAudio)
		START_NET_TIMER(dmVarsPassed.timerAudio)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timerAudio, 2500)
			SETUP_DM_AUDIO_GROUPS()
			RESET_NET_TIMER(dmVarsPassed.timerAudio)
		ENDIF
	ENDIF
ENDPROC

PROC MUTE_RADIOS(SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_MUTED_RADIOS)
		PRINTLN("[DM_AUDIO] MUTE_RADIOS ")
		START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
		SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_MUTED_RADIOS)
	ENDIF
ENDPROC

PROC UN_MUTE_RADIOS(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_MUTED_RADIOS)
		PRINTLN("[DM_AUDIO] UN_MUTE_RADIOS ")
		STOP_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
		SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE)
		CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_MUTED_RADIOS)
	ENDIF
ENDPROC

// =============================================== MUSIC ===============================================

FUNC BOOL SHOULD_DM_PLAY_MUSIC(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF g_FMMC_STRUCT.iNewAudioScore != -1
	AND NOT HAS_DEATHMATCH_FINISHED(serverBDPassed)
	AND HAS_DM_STARTED(serverBDPassed)
	AND NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO)
	AND NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_40_SECONDS_LEFT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_DM_MUSIC(INT iAudioTrack, INT iMoodType, SHARED_DM_VARIABLES &dmVarsPassed, BOOL bForced = FALSE, BOOL bPreventResetTimer = FALSE)
	// Is it playing?
	IF dmVarsPassed.iAudioTrack != iAudioTrack
	OR bForced
		PRINTLN("[LM][DmMusic][SET_DM_MUSIC] - iAudioTrack was: ", dmVarsPassed.iAudioTrack, " Setting to: ", iAudioTrack, " TrackName: ", GET_PUBLIC_CONTROLLER_AUDIO_TRACK_NAME(iAudioTrack))
		TRIGGER_MUSIC_EVENT(GET_PUBLIC_CONTROLLER_AUDIO_TRACK_NAME(iAudioTrack))
		dmVarsPassed.iAudioTrack = iAudioTrack
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_MID)		
	ENDIF
	
	// Change mood type.
	IF dmVarsPassed.iAudioMood != iMoodType
	OR bForced
		PRINTLN("[LM][DmMusic][SET_DM_MUSIC] - iAudioMood was: ", dmVarsPassed.iAudioMood, " Setting to: ", iMoodType, " MoodName: ", GET_PUBLIC_CONTROLLER_AUDIO_MOOD_NAME(iAudioTrack, iMoodType))
		TRIGGER_MUSIC_EVENT(GET_PUBLIC_CONTROLLER_AUDIO_MOOD_NAME(iAudioTrack, iMoodType))
		dmVarsPassed.iAudioMood = iMoodType
		IF bPreventResetTimer
			RESET_NET_TIMER(dmVarsPassed.tdMusicMoodChangeTimer)
			START_NET_TIMER(dmVarsPassed.tdMusicMoodChangeTimer)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAN_UP_MUSIC(SHARED_DM_VARIABLES &dmVarsPassed)
	PRINTLN("[LM][DmMusic][SET_DM_MUSIC] - CALLING CLEAN_UP_MUSIC")
	CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_MID)
	CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_START)
	TRIGGER_MUSIC_EVENT("GTA_ONLINE_STOP_SCORE")
ENDPROC

FUNC BOOL CAN_MUSIC_MOOD_CHANGE_YET(SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.tdMusicMoodChangeTimer)
	OR HAS_NET_TIMER_EXPIRED_READ_ONLY(dmVarsPassed.tdMusicMoodChangeTimer, dmVarsPassed.iMoodChangeTime)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PROCESS_DYNAMIC_MUSIC(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])
	
	PLAYER_INDEX PlayerID = PlayerToUse
	PED_INDEX pedPlayer = PlayerPedToUse
	VEHICLE_INDEX vehPlayer
	BOOL bBattling, bWanted, bNearlyDead, bInVeh
	
	IF GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_FIGHTING
		
		IF bPlayerPedToUseOK
			IF IS_PED_IN_ANY_VEHICLE(pedPlayer)				
				vehPlayer = GET_VEHICLE_PED_IS_IN(pedPlayer)
			ENDIF
		ENDIF
		
		bWanted = (GET_PLAYER_WANTED_LEVEL(PlayerID) > 0)		
		bInVeh = DOES_ENTITY_EXIST(vehPlayer)		
		IF NOT bInVeh
			dmVarsPassed.iMoodChangeTime = 2500
			bNearlyDead = (GET_ENTITY_HEALTH(pedPlayer) < 50)
			bBattling = GET_PLAYER_RECEIVED_BATTLE_EVENT_RECENTLY(PlayerID, 12500, FALSE)
		ELSE
			dmVarsPassed.iMoodChangeTime = 2500
			bNearlyDead = (GET_VEHICLE_BODY_HEALTH(vehPlayer) < 250)
			bBattling = GET_PLAYER_RECEIVED_BATTLE_EVENT_RECENTLY(PlayerID, 20000, FALSE)
		ENDIF
		
		IF CAN_MUSIC_MOOD_CHANGE_YET(dmVarsPassed)
			IF bInVeh
				IF bBattling
				OR bNearlyDead
				OR bWanted
					SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_VEHICLE_CHASE, dmVarsPassed)
				ELIF NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(dmVarsPassed.tdMusicMoodChangeTimer, 35000)
					SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_ACTION, dmVarsPassed, FALSE, TRUE)
				ELSE
					// If we've been inactive for the given time, go to suspense.
					SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_SUSPENSE, dmVarsPassed)
				ENDIF
			ELSE
				IF bWanted
				OR bNearlyDead
					SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_VEHICLE_CHASE, dmVarsPassed)
				ELIF bBattling				
					SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_ACTION, dmVarsPassed)
				ELSE
					SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_SUSPENSE, dmVarsPassed)
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_dmMusicDebug")
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("===== DM MUSIC DEBUG =====", "", vBlankDbg)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("g_FMMC_STRUCT.iNewAudioScore = ", "", vBlankDbg, g_FMMC_STRUCT.iNewAudioScore)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iAudioTrack = ", "", vBlankDbg, dmVarsPassed.iAudioTrack)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("iAudioMood = ", "", vBlankDbg, dmVarsPassed.iAudioMood)
			
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("NameTrack = ", GET_PUBLIC_CONTROLLER_AUDIO_TRACK_NAME(dmVarsPassed.iAudioTrack), vBlankDbg)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("NameMood = ", GET_PUBLIC_CONTROLLER_AUDIO_MOOD_NAME(dmVarsPassed.iAudioTrack, dmVarsPassed.iAudioMood), vBlankDbg)
			
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("bBattling = ", PICK_STRING(bBattling, "TRUE", "FALSE"), vBlankDbg)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("bNearlyDead = ", PICK_STRING(bNearlyDead, "TRUE", "FALSE"), vBlankDbg)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("bWanted = ", PICK_STRING(bWanted, "TRUE", "FALSE"), vBlankDbg)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("bInVeh = ", PICK_STRING(bInVeh, "TRUE", "FALSE"), vBlankDbg)
			PRINT_LUKE_ON_SCREEN_DEBUG_TEXT("===== DM MUSIC DEBUG =====", "", vBlankDbg)
			ENDIF
		#ENDIF
	ENDIF
ENDPROC

PROC START_INITIAL_DM_MUSIC(SHARED_DM_VARIABLES &dmVarsPassed)
	// Start Music
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_START)		
		PRINTLN("[LM][DmMusic][START_INITIAL_DM_MUSIC] ========= Starting DM Music! =========")
		PRINTLN("[LM][DmMusic][START_INITIAL_DM_MUSIC] g_FMMC_STRUCT.iNewAudioScore: ", g_FMMC_STRUCT.iNewAudioScore)
		START_AUDIO_SCENE("MP_POSITIONED_RADIO_MUTE_SCENE")
		SET_AUDIO_FLAG("AllowScoreAndRadio", FALSE)
		SET_DM_MUSIC(g_FMMC_STRUCT.iNewAudioScore, MUSIC_SUSPENSE, dmVarsPassed, TRUE)
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_START)
	ENDIF
ENDPROC

PROC PROCESS_MUSIC(SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	// Handled elsewhere...
	IF CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	
	// Cleanup and Should play?
	IF NOT SHOULD_DM_PLAY_MUSIC(serverBDpassed, dmVarsPassed)
		IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_MID)
		OR IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_PLAYING_MUSIC_START)
			CLEAN_UP_MUSIC(dmVarsPassed)
		ENDIF		
		EXIT
	ENDIF
	
	// Start Music
	START_INITIAL_DM_MUSIC(dmVarsPassed)
	
	// Process Dynamic Music Changes
	PROCESS_DYNAMIC_MUSIC(dmVarsPassed, playerBDPassed)
ENDPROC

// =============================================== MUSIC ===============================================

PROC START_DM_SCORE(SHARED_DM_VARIABLES &dmVarsPassed)
	//IF NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_STARTED_SCORE)
		IF NOT CONTENT_IS_USING_ARENA()
			IF g_FMMC_STRUCT.iNewAudioScore != -1
				START_INITIAL_DM_MUSIC(dmVarsPassed)
			ELIF IS_PLAYER_ON_BIKER_DM()
				TRIGGER_MUSIC_EVENT("BIKER_DM_START")
				PRINTLN("[DM_AUDIO] BIKER_DM_START ")
			ELSE
				TRIGGER_MUSIC_EVENT("MP_DM_START_ALL")
				PRINTLN("[DM_AUDIO] START_DM_SCORE ")
			ENDIF
		ENDIF
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_STARTED_SCORE)
	ENDIF
ENDPROC

PROC STOP_DM_SCORE(SHARED_DM_VARIABLES &dmVarsPassed)
	//IF NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
			IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_STARTED_SCORE)
				IF NOT IS_PLAYER_ON_BIKER_DM()
					TRIGGER_MUSIC_EVENT("MP_DM_STOP_TRACK")
				ENDIF
				SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
				PRINTLN("[DM_AUDIO] STOP_DM_SCORE ")
			ELSE
				PRINTLN("[DM_AUDIO] Not stopping DM score because NONBD_BITSET_STARTED_SCORE is not set")
			ENDIF
		ELSE
			PRINTLN("[DM_AUDIO] Not stopping DM score because NONBD_BITSET_KILLED_SCORE is already set")
		ENDIF
	//ENDIF
ENDPROC

PROC SET_RADIO_STATION_OVER_SCORE(BOOL bSet)
	IF g_SpawnData.bRadioStationOverScore != bSet
		g_SpawnData.bRadioStationOverScore = bSet
		PRINTLN("[DM_AUDIO] SET_RADIO_STATION_OVER_SCORE - g_SpawnData.bRadioStationOverScore is now ", PICK_STRING(bSet, "TRUE", "FALSE"))
	ENDIF
ENDPROC

PROC MAINTAIN_DM_SCORE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	// Don't change track again if one kill left audio playing
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ONE_KILL_AUDIO)
	OR g_FMMC_STRUCT.iNewAudioScore != -1
	OR CONTENT_IS_USING_ARENA()
	//OR NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		EXIT
	ENDIF
		
	// Fix for 3013245
	IF IS_PLAYER_ON_BIKER_DM()
		IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_40_SECONDS_LEFT)
			EXIT
		ENDIF
	ENDIF
	
	IF IS_ENTITY_ALIVE(LocalPlayerPed)
		IF GET_PLAYER_RADIO_STATION_INDEX() != 255
			SET_RADIO_STATION_OVER_SCORE(TRUE)
		ELSE
			SET_RADIO_STATION_OVER_SCORE(FALSE)
		ENDIF
	ENDIF
	
	// Exit if score ended
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
		EXIT
	ENDIF
	
	// Change track every 5 mins
	IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeMusicScore)
		IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeMusicScore, DM_AUDIO_RESTART_TIME)
			
			IF NOT IS_PLAYER_ON_BIKER_DM()
				TRIGGER_MUSIC_EVENT("MP_DM_START_ALL")
				RESET_NET_TIMER(dmVarsPassed.timeMusicScore)
				CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
				PRINTLN("[DM_AUDIO] MAINTAIN_DM_SCORE, TRIGGER_MUSIC_EVENT ")
			ENDIF
		ENDIF
	ELSE
		START_NET_TIMER(dmVarsPassed.timeMusicScore)
		PRINTLN("[DM_AUDIO] MAINTAIN_DM_SCORE, START_NET_TIMER ")
	ENDIF
	
	// Stop audio score 10 secs before countdown triggered
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_40_SECONDS_LEFT)
		STOP_DM_SCORE(dmVarsPassed)
	ENDIF
ENDPROC
 
// Countdown music ///////
PROC KILL_DM_NEARLY_FINISHED_AUDIO(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	//IF NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_AUDIO)
			IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO)
				IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
					IF NOT HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, GET_ROUND_DURATION(serverBDpassed))
						PRINTLN("[DM_AUDIO] KILL_DM_NEARLY_FINISHED_AUDIO")
						IF NOT CONTENT_IS_USING_ARENA()
							TRIGGER_MUSIC_EVENT("MP_DM_COUNTDOWN_KILL")
						ELSE
							TRIGGER_MUSIC_EVENT("AW_COUNTDOWN_30S_KILL")						
						ENDIF
						SET_RADIO_STATION_OVER_SCORE(FALSE)
						SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_AUDIO)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	//ENDIF
ENDPROC

PROC PROCESS_KART_BATTLE_ROUND_COUNTDOWN_AUDIO(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
	OR GET_HUD_TIME(serverBDpassed) > 31000
	OR GET_HUD_TIME(serverBDpassed) <= -1
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT)
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT)
		PLAY_SOUND_FRONTEND(-1, "30_Secs_Remaining", "Go_Kart_Death_Match_Soundset")
		PRINTLN("PROCESS_KART_BATTLE_ROUND_COUNTDOWN_AUDIO - 30_Secs_Remaining")
	ELIF GET_HUD_TIME(serverBDpassed) <= 21000
	AND NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT_2)
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT_2)
		PLAY_SOUND_FRONTEND(-1, "20_Secs_Remaining", "Go_Kart_Death_Match_Soundset")
		PRINTLN("PROCESS_KART_BATTLE_ROUND_COUNTDOWN_AUDIO - 20_Secs_Remaining")
	ELIF GET_HUD_TIME(serverBDpassed) <= (dmVarsPassed.iCountdownBeeps * 1000)
	AND NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
		PLAY_SOUND_FRONTEND(-1, "10_Secs_Countdown", "Go_Kart_Death_Match_Soundset")
		dmVarsPassed.iCountdownBeeps--
		PRINTLN("PROCESS_KART_BATTLE_ROUND_COUNTDOWN_AUDIO - 10_Secs_Countdown - Beeps remaining: ", dmVarsPassed.iCountdownBeeps)
	ENDIF
	
ENDPROC

PROC PLAY_DEATHMATCH_NEARLY_FINISHED_AUDIO(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	//IF NOT IS_ON_VEH_DEATHMATCH_GLOBAL_SET()
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO)
			IF IS_ONLY_ONE_KILL_REMAINING(serverBDpassed)
			AND NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_KILLED_SCORE)
				IF NOT HAS_SERVER_FINISHED(serverBDPassed)
				#IF IS_DEBUG_BUILD
				AND NOT HAS_LOCAL_PLAYER_DEBUG_ENDED(dmVarsPassed)
				#ENDIF
					IF GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
					OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
						IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ONE_KILL_AUDIO)							
							IF PREPARE_MUSIC_EVENT("MP_DM_LAST")
								IF TRIGGER_MUSIC_EVENT("MP_DM_LAST") // one kill remains audio
									PRINTLN("[DM_AUDIO] NONBD_BITSET_ONE_KILL_AUDIO - One Kill Remaining music event has been triggered")
									SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_ONE_KILL_AUDIO)
								ELSE
									PRINTLN("[DM_AUDIO] NONBD_BITSET_ONE_KILL_AUDIO - One Kill Remaining music event has ***not*** been triggered")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
					
				IF NOT HAS_SERVER_FINISHED(serverBDPassed)
				#IF IS_DEBUG_BUILD
				AND NOT HAS_LOCAL_PLAYER_DEBUG_ENDED(dmVarsPassed)
				#ENDIF
					IF GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
					OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					
						IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_35_SECONDS_LEFT)
							IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_PRE_COUNTDOWN_35_SEC_LEFT)
								IF TRIGGER_MUSIC_EVENT("AW_PRE_COUNTDOWN_STOP") // 35 secs left audio
									SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_PRE_COUNTDOWN_35_SEC_LEFT)
								ENDIF
							ENDIF
						ENDIF
				
						IF IS_ONLY_30_SECONDS_LEFT(serverBDpassed)					
						
							IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO)
								STRING musictoplay = "MP_DM_COUNTDOWN_30_SEC"
								
									IF IS_PLAYER_ON_BIKER_DM()
										musictoplay ="FM_COUNTDOWN_30S"
									ENDIF
									IF CONTENT_IS_USING_ARENA()
										musictoplay = "AW_COUNTDOWN_30S"
									ENDIF
								IF PREPARE_MUSIC_EVENT(musictoplay)									
									IF TRIGGER_MUSIC_EVENT(musictoplay) // 30 secs left audio
										PRINTLN("[DM_AUDIO] NONBD_BITSET_NEARLY_FINISHED_AUDIO - 30 seconds remaining music event has been triggered")
										SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_NEARLY_FINISHED_AUDIO)
									ELSE
										PRINTLN("[DM_AUDIO] FAIL - NONBD_BITSET_NEARLY_FINISHED_AUDIO - 30 seconds remaining music event has ***not*** been triggered")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[DM_AUDIO] - NONBD_BITSET_NEARLY_FINISHED_AUDIO is set - game is within final 30 seconds")
		ENDIF
	//ENDIF
ENDPROC
