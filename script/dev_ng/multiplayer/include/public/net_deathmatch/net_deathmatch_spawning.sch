USING "net_deathmatch_include.sch"

FUNC BOOL USING_TEAM_SPAWNS(SHARED_DM_VARIABLES &dmVarsPassed)

	RETURN IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_USING_TEAM_SPAWNS)
ENDFUNC

// Block team spawning if too many players in the teams 1621085
PROC COUNT_TEAMS_FOR_TEAM_SPAWNING(SHARED_DM_VARIABLES &dmVarsPassed)
	INT i, iPlayerTeam
	INT iTeamCounts[MAX_NUM_DM_TEAMS]
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))	
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))

			iPlayerTeam = GlobalplayerBD_FM[NATIVE_TO_INT(playerId)].sClientCoronaData.iTeamChosen

			IF iPlayerTeam <> -1
				IF (iPlayerTeam >= 0) AND (iPlayerTeam < MAX_NUM_DM_TEAMS)
					iTeamCounts[iPlayerTeam]++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_USING_TEAM_SPAWNS)
		IF iTeamCounts[0] <= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[0]
		AND iTeamCounts[1] <= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[1]
		AND iTeamCounts[2] <= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[2]
		AND iTeamCounts[3] <= g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[3]
			PRINTLN(" [CS_SPAWN] NONBD_BITSET_USING_TEAM_SPAWNS")
			SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_USING_TEAM_SPAWNS)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DM_SPAWN_POINT_IS_VALID(FMMCSpawnPointStruct& spawnPoint, INT i, INT iModset = -1)
	
	IF IS_CURRENT_MODIFIER_SET_VALID(iModset)
		IF IS_BIT_SET(spawnPoint.iModifierRestrictedBS, iModset)
			PRINTLN("[TMSSpawnRest] DM_SPAWN_POINT_IS_VALID - Spawn point ", i, " is restricted for modset ", iModset)
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen
	
	IF IS_TEAM_DEATHMATCH()
		IF IS_BIT_SET(spawnPoint.iTeamRestrictedBS, iTeam)
			PRINTLN("[TMSSpawnRest] DM_SPAWN_POINT_IS_VALID - Spawn point ", i, " is restricted for team ", iTeam)
			RETURN FALSE
		ENDIF
	ENDIF
	
	PRINTLN("[TMSSpawnRest] DM_SPAWN_POINT_IS_VALID - Spawn point ", i, " is valid")
	RETURN TRUE
	
ENDFUNC

PROC SET_UP_CUSTOM_SPAWN_POINTS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, BOOL bInitialSpawnDone)

	PRINTLN(" [CS_SPAWN] SET_UP_CUSTOM_SPAWN_POINTS______START")
	CLEAR_CUSTOM_SPAWN_POINTS() 

	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	
		// Do nothing, Neil's system will deal with it
		EXIT
		
	ELSE
	
		BOOL bIgnoreObstruction = (IS_NON_ARENA_KING_OF_THE_HILL() AND NOT bInitialSpawnDone)
		FLOAT fVehicleClearRadius = CUSTOM_SPAWN_VEHICLE_CLEARANCE_RADIUS
		
		IF bIgnoreObstruction
			fVehicleClearRadius = 1.0
		ENDIF
		
		//Set up the custom spawn 
		IF CONTENT_IS_USING_ARENA()
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DisableNavMeshSpawnFallback)
			PRINTLN("[CS_SPAWN] SET_UP_CUSTOM_SPAWN_POINTS - Using Arena call for USE_CUSTOM_SPAWN_POINTS")
			USE_CUSTOM_SPAWN_POINTS(TRUE, DEFAULT, DEFAULT, DEFAULT, 2.0, DEFAULT, 2.5, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ELSE
			USE_CUSTOM_SPAWN_POINTS(TRUE, g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints > (g_FMMC_STRUCT.iNumParticipants + 1) * 2, bIgnoreObstruction, DEFAULT, fVehicleClearRadius, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
		ENDIF
		
		INT i
		
		// Use team spawns TDM
		IF NOT bInitialSpawnDone
			IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
				COUNT_TEAMS_FOR_TEAM_SPAWNING(dmVarsPassed)
			ENDIF
		ENDIF	
		
		IF bInitialSpawnDone
			// Team Deathmatch spawn points might need some extra logic so keeping this separate.
			IF USING_TEAM_SPAWNS(dmVarsPassed)
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)
					PRINTLN(" [CS_SPAWN] USING_TEAM_SPAWNS, SET_UP_CUSTOM_SPAWN_POINTS,  i = ", i)
					
					IF DM_SPAWN_POINT_IS_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i], i, playerBDPassed[iLocalPart].iCurrentModSet)
						ADD_CUSTOM_SPAWN_POINT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead)
					ENDIF
				ENDFOR
			ELSE
				// FFA Deathmatch spawn points
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)					
					PRINTLN(" [CS_SPAWN] FFA, SET_UP_CUSTOM_SPAWN_POINTS, bUseTeamSpawnPoints, FALSE ")
					PRINTLN(" [CS_SPAWN] i = ", i)
					
					IF DM_SPAWN_POINT_IS_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i], i, playerBDPassed[iLocalPart].iCurrentModSet)
						ADD_CUSTOM_SPAWN_POINT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead)
					ENDIF
				ENDFOR
			ENDIF
		ELSE
			// Team Deathmatch spawn points
			IF USING_TEAM_SPAWNS(dmVarsPassed)
				INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].sClientCoronaData.iTeamChosen
				INT j
				FOR j = 0 TO (MAX_NUM_DM_TEAMS - 1)
					FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfTeamSpawnPoints[j] - 1)
						IF j = iTeam
							PRINTLN(" [CS_SPAWN] USING_TEAM_SPAWNS, SET_UP_CUSTOM_SPAWN_POINTS,  ", "i = ", i, " Team = ", j)
							ADD_CUSTOM_SPAWN_POINT(g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[j][i].vPos, g_FMMC_STRUCT_ENTITIES.sTDMSpawnPoints[j][i].fHead)
						ENDIF
					ENDFOR
				ENDFOR
			ELSE
				// FFA Deathmatch spawn points
				FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints - 1)					
					PRINTLN(" [CS_SPAWN] FFA, SET_UP_CUSTOM_SPAWN_POINTS, bUseTeamSpawnPoints, FALSE ")
					PRINTLN(" [CS_SPAWN] i = ", i)
					
					IF DM_SPAWN_POINT_IS_VALID(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i], i, playerBDPassed[iLocalPart].iCurrentModSet)
						ADD_CUSTOM_SPAWN_POINT(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos, g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].fHead)
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
	ENDIF	
	
	PRINTLN(" [CS_SPAWN] SET_UP_CUSTOM_SPAWN_POINTS______end")
	
ENDPROC

PROC CLEANUP_THE_VEHICLE(SHARED_DM_VARIABLES &dmVarsPassed)
	ENTITY_INDEX eToCleanup 
	
	IF DOES_ENTITY_EXIST(dmVarsPassed.vehicleToDelete)
		eToCleanup = GET_ENTITY_FROM_PED_OR_VEHICLE(dmVarsPassed.vehicleToDelete)
		IF DOES_ENTITY_EXIST(eToCleanup)
			IF NOT IS_ENTITY_DEAD(eToCleanup)
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(eToCleanup)
					PRINTLN("CLEANUP_THE_VEHICLE ")	
					SET_ENTITY_AS_NO_LONGER_NEEDED(eToCleanup)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC STRING GET_SPAWN_STATE_NAME(INT iState)
	STRING retVal = ""
	SWITCH iState
		CASE INITIAL_SPAWN__SETUP	retVal = "INITIAL_SPAWN__SETUP	 "	BREAK
		CASE INITIAL_SPAWN__GET_SPAWN_POSITION	retVal = "INITIAL_SPAWN__GET_SPAWN_POSITION	 "	BREAK
		CASE INITIAL_SPAWN__WARP_PLAYER			retVal = "INITIAL_SPAWN__WARP_PLAYER"			BREAK
		CASE INITIAL_SPAWN__CREATE_VEH			retVal = "INITIAL_SPAWN__CREATE_VEH"			BREAK
		CASE INITIAL_SPAWN__ENTER_VEH			retVal = "INITIAL_SPAWN__ENTER_VEH"				BREAK
		CASE INITIAL_SPAWN__RETURN_TRUE			retVal = "INITIAL_SPAWN__RETURN_TRUE"			BREAK
	ENDSWITCH
	RETURN retVal
ENDFUNC

PROC GO_TO_INITIAL_SPAWN_STATE(SHARED_DM_VARIABLES &dmVarsPassed, INT iState)

	#IF IS_DEBUG_BUILD
	NET_NL()
	NET_PRINT_STRINGS("GO_TO_INITIAL_SPAWN_STATE - Previous state	= ", GET_SPAWN_STATE_NAME(dmVarsPassed.iInitialPedSpawnState))	NET_NL()
	NET_PRINT_STRINGS("GO_TO_INITIAL_SPAWN_STATE - Next state	= ", GET_SPAWN_STATE_NAME(iState))	NET_NL()
	#ENDIF
	
	dmVarsPassed.iInitialPedSpawnState = iState
ENDPROC

FUNC INT GET_INITIAL_SPAWN_STAGE(SHARED_DM_VARIABLES &dmVarsPassed)

	RETURN dmVarsPassed.iInitialPedSpawnState
ENDFUNC

FUNC BOOL ARE_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM() 		
		PRINTLN("ARE_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN, IS_PLAYER_ON_IMPROMPTU_DM, RETURN(TRUE)")
		RETURN(TRUE)
	ENDIF	

	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerID, iParticipant)
			
				IF NOT IS_BIT_SET(serverBDpassed.iBS_ParticipantHasSpawned, iParticipant)
			
					IF NOT (playerId = LocalPlayer)
						IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(playerID))
							IF IS_PLAYER_VISIBLE_TO_SCRIPT(playerID)
								IF NOT (playerBDPassed[iParticipant].eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING)
									PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - player ", NATIVE_TO_INT(playerID), " NOT eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - player name ", GET_PLAYER_NAME(playerID))	
									RETURN(FALSE)
								ELSE
									PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - player ", NATIVE_TO_INT(playerID), " AND eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - player name ", GET_PLAYER_NAME(playerID))									
								ENDIF
							ELSE
								IF NOT (playerBDPassed[iParticipant].eClientLogicStage <= CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING)
									PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - player ", NATIVE_TO_INT(playerID), " is invisible but NOT eClientLogicStage <= CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - player name ", GET_PLAYER_NAME(playerID))	
									RETURN(FALSE)	
								ELSE
									PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - player ", NATIVE_TO_INT(playerID), " is invisible AND eClientLogicStage <= CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - player name ", GET_PLAYER_NAME(playerID))										
								ENDIF
							ENDIF
						ELSE
							PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - player ", NATIVE_TO_INT(playerID), " is dead - player name ", GET_PLAYER_NAME(playerID))
						ENDIF
					ELSE
						PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - is local player. PlayerID =", NATIVE_TO_INT(playerID), ", participant id = ", iParticipant, " playerBDPassed[iParticipant].eClientLogicStage = ", ENUM_TO_INT(playerBDPassed[iParticipant].eClientLogicStage))
					ENDIF
					
				ELSE
					
					// if the server says the player has spawned, need to also check they have become visible
					IF NOT (playerId = LocalPlayer)
						IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(playerID))
							IF NOT IS_PLAYER_VISIBLE_TO_SCRIPT(playerID)					
								NET_PRINT("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - BIT SET iBS_ParticipantHasSpawned - player ")
								NET_PRINT_INT(NATIVE_TO_INT(playerID))
								NET_PRINT(" has spawned, but not yet visible ")
								NET_PRINT(GET_PLAYER_NAME(playerID))
								NET_PRINT(" player coords = ")
								NET_PRINT_VECTOR(GET_PLAYER_COORDS(playerID))
								NET_NL()
								RETURN(FALSE)	
							ELSE
								// check they have moved on
								IF (playerBDPassed[iParticipant].eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING)
								
									// check they are where they are supposed to be
									VECTOR vPlayerPos = GET_PLAYER_COORDS(PlayerID)
									VECTOR vPlayerBroadcastPos = GlobalplayerBD[NATIVE_TO_INT(PlayerID)].vInitialSpawnCoords
									
									// ignore any z diff
									vPlayerPos.z = 0.0
									vPlayerBroadcastPos.z = 0.0
									
									IF (VDIST(vPlayerPos, vPlayerBroadcastPos) > 2.0)
									
										NET_PRINT("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - BIT SET iBS_ParticipantHasSpawned - player ")
										NET_PRINT_INT(NATIVE_TO_INT(playerID))
										NET_PRINT(" has spawned and is visible (and eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING), but position is different from bd pos ")
										NET_PRINT(GET_PLAYER_NAME(playerID))
										NET_PRINT(" player coords = ")
										NET_PRINT_VECTOR(GET_PLAYER_COORDS(playerID))
										NET_PRINT(" bd coords = ")
										NET_PRINT_VECTOR(GlobalplayerBD[NATIVE_TO_INT(PlayerID)].vInitialSpawnCoords)
										NET_NL()
										RETURN(FALSE)	
										
									ELSE
								
								
										// also need to check they have movede on
										NET_PRINT("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - BIT SET iBS_ParticipantHasSpawned - player ")
										NET_PRINT_INT(NATIVE_TO_INT(playerID))
										NET_PRINT(" has spawned and is visible (and eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING) ")
										NET_PRINT(GET_PLAYER_NAME(playerID))
										NET_PRINT(" player coords = ")
										NET_PRINT_VECTOR(GET_PLAYER_COORDS(playerID))
										NET_NL()
										
									ENDIF
								ELSE
									NET_PRINT("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - BIT SET iBS_ParticipantHasSpawned - player ")
									NET_PRINT_INT(NATIVE_TO_INT(playerID))
									NET_PRINT(" has spawned, is visible, but NOT bConsiderPlayerForVisibleChecks ")
									NET_PRINT(GET_PLAYER_NAME(playerID))
									NET_PRINT(" player coords = ")
									NET_PRINT_VECTOR(GET_PLAYER_COORDS(playerID))
									NET_NL()
									RETURN(FALSE)
								ENDIF
							ENDIF
						ELSE
							NET_PRINT("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - BIT SET iBS_ParticipantHasSpawned - player ")
							NET_PRINT_INT(NATIVE_TO_INT(playerID))
							NET_PRINT(" has spawned and is dead ")
							NET_PRINT(GET_PLAYER_NAME(playerID))
							NET_PRINT(" player coords = ")
							NET_PRINT_VECTOR(GET_PLAYER_COORDS(playerID))
							NET_NL()
						ENDIF
					ELSE
						NET_PRINT("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - BIT SET iBS_ParticipantHasSpawned - player ")
						NET_PRINT_INT(NATIVE_TO_INT(playerID))
						NET_PRINT(" has spawned and is local player ")
						NET_PRINT(GET_PLAYER_NAME(playerID))
						NET_PRINT(" player coords = ")
						NET_PRINT_VECTOR(GET_PLAYER_COORDS(playerID))
						NET_NL()						
					ENDIF
					

				ENDIF
				
			ELSE
				PRINTLN("AREA_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN - Player is spectator. PlayerID = ", NATIVE_TO_INT(playerID), ", participant id = ", iParticipant)
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(TRUE)
ENDFUNC

FUNC BOOL HAS_INITIAL_SPAWN_FINISHED(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])	
	
	IF (IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT g_sImpromptuVars.bVehicleDM)
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("HAS_INITIAL_SPAWN_FINISHED - EXIT  ")

		RETURN TRUE
	ENDIF
	
	PRINTLN("GET_INITIAL_SPAWN_STAGE(dmVarsPassed): ", GET_INITIAL_SPAWN_STAGE(dmVarsPassed))
	
	IF bLocalPlayerOK
	
		//Set up the custom spawn 
		IF (GET_INITIAL_SPAWN_STAGE(dmVarsPassed) = INITIAL_SPAWN__SETUP)
			SET_UP_CUSTOM_SPAWN_POINTS(serverBDpassed, playerBDPassed, dmVarsPassed, FALSE)
			GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__GET_SPAWN_POSITION)
		ENDIF
	
		// INTIAL SPAWN
		IF (GET_INITIAL_SPAWN_STAGE(dmVarsPassed) = INITIAL_SPAWN__GET_SPAWN_POSITION)
		
			// make sure this player is invisible
			IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					IF IS_PLAYER_VISIBLE_TO_SCRIPT(LocalPlayer)
						SET_ENTITY_VISIBLE(LocalPlayerPed, FALSE)
						PRINTLN("HAS_INITIAL_SPAWN_FINISHED - player was not invisible, doing that now.   ")
						SCRIPT_ASSERT("HAS_INITIAL_SPAWN_FINISHED - player was not invisible, doing that now.")
					ENDIF
				ENDIF	
			ENDIF
		
			// Has server given ticket?												
			IF serverBDpassed.iParticipantsTurnToSpawn = iLocalPart
			OR IS_PLAYER_ON_IMPROMPTU_DM()				
				IF ARE_ALL_PLAYERS_INVISIBLE_OR_HAVE_PASSED_INITIAL_SPAWN(serverBDpassed, playerBDPassed) OR (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),  dmVarsPassed.timeInitialSpawn)) > 30000)
					
					#IF IS_DEBUG_BUILD
						IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),  dmVarsPassed.timeInitialSpawn)) > INITIAL_SPAWN_TIMEOUT)
						AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
							NET_PRINT("HAS_INITIAL_SPAWN_FINISHED - participant hit timeout, why?") NET_NL()
							SCRIPT_ASSERT("HAS_INITIAL_SPAWN_FINISHED - participant hit timeout, why?")
						ENDIF
					#ENDIF
					
					PRINTLN("HAS_INITIAL_SPAWN_FINISHED - my turn to spawn participant id = ", iLocalPart)	
					
					//g_SpawnData.bInitialSpawnForDM = TRUE
					IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
					OR g_sImpromptuVars.bVehicleDM
					OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, dmVarsPassed.sPlayerModifierData)
						IF IS_PLAYER_ON_IMPROMPTU_DM()
							// check if player is already in vehicle of this type
							IF IS_PLAYER_IN_DM_VEH_MODEL()
								PRINTLN("HAS_INITIAL_SPAWN_FINISHED - impromptu, on time, vehicle dm, player already in suitable model")	
								GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__RETURN_TRUE)
							ELSE							
								IF HAS_GOT_SPAWN_LOCATION(dmVarsPassed.vInitialSpawnPos, dmVarsPassed.fInitialSpawnHeading, SPAWN_LOCATION_NEAR_CURRENT_POSITION, TRUE, FALSE, FALSE, FALSE, FALSE)
									PRINTLN("HAS_INITIAL_SPAWN_FINISHED - impromptu, on time, vehicle dm, dmVarsPassed.vInitialSpawnPos = ", dmVarsPassed.vInitialSpawnPos)						
									GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__CREATE_VEH)
								ENDIF
							ENDIF
						ELSE
							IF HAS_GOT_SPAWN_LOCATION(dmVarsPassed.vInitialSpawnPos, dmVarsPassed.fInitialSpawnHeading, SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, TRUE, FALSE)											
								PRINTLN("HAS_INITIAL_SPAWN_FINISHED - normal, on time, vehicle dm, dmVarsPassed.vInitialSpawnPos = ", dmVarsPassed.vInitialSpawnPos)
								
								SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE(playerBDPassed)
								
								GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__CREATE_VEH)	
							ENDIF
						ENDIF
					ELSE
						PRINTLN("HAS_INITIAL_SPAWN_FINISHED - normal, on time, on foot, going straight to warp player ")		
						GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__WARP_PLAYER)
					ENDIF
				ELSE
				
					PRINTLN("HAS_INITIAL_SPAWN_FINISHED - participant 0 is waiting for all players to be invisible, time ", ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),  dmVarsPassed.timeInitialSpawn)))		
				ENDIF
			ELSE													
				PRINTLN("HAS_INITIAL_SPAWN_FINISHED It's this participant's turn to spawn:  = ", serverBDpassed.iParticipantsTurnToSpawn)
			ENDIF

		ENDIF
		
		// VEHICLE DM CREATE
		IF (GET_INITIAL_SPAWN_STAGE(dmVarsPassed) = INITIAL_SPAWN__CREATE_VEH)
			IF REQUEST_LOAD_MODEL(GET_DM_VEHICLE_TO_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData))
				IF CAN_REGISTER_MISSION_VEHICLES(1)
					
					// Was -1. Likely due to debug, so this is a failsafe.
					IF g_iMyRaceModelChoice < 0
						PRINTLN("[DM][INITIAL_SPAWN__CREATE_VEH] - g_iMyRaceModelChoice was -1, setting to 0.")	
						g_iMyRaceModelChoice = 0
					ENDIF
					
//					IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
//						DELETE_VEHICLE(dmVarsPassed.dmVehIndex)
//						PRINTLN("[DM][VEH] Deleting dmVarsPassed.dmVehIndex (1)")
//					ENDIF
					
					IF CREATE_PLAYER_SPAWN_VEHICLE(playerBDPassed, dmVarsPassed.dmVehID, GET_DM_VEHICLE_TO_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData), dmVarsPassed.vInitialSpawnPos, dmVarsPassed.fInitialSpawnHeading)
						dmVarsPassed.dmVehIndex = NET_TO_VEH(dmVarsPassed.dmVehID)
						PRINTLN("[VEH] 1 Setting dmVarsPassed.dmVehIndex to ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex)))
						
						SET_VEHICLE_ENGINE_ON(dmVarsPassed.dmVehIndex,TRUE,TRUE)
						SET_ENTITY_HEALTH(dmVarsPassed.dmVehIndex, 1000)
						SET_VEHICLE_ENGINE_HEALTH(dmVarsPassed.dmVehIndex, 1000.0)
						SET_VEHICLE_PETROL_TANK_HEALTH(dmVarsPassed.dmVehIndex, 1000.0)
										
						SET_VEHICLE_ON_GROUND_PROPERLY(dmVarsPassed.dmVehIndex, 1.0)
						
	//					SET_VEHICLE_FIXED(dmVarsPassed.dmVehIndex)
						
						// (529132) Clear fire
						CLEAR_AREA(dmVarsPassed.vInitialSpawnPos, CLEAR_AREA_AROUND_NEW_VEH_AREA, FALSE)
						SET_ENTITY_LOAD_COLLISION_FLAG(dmVarsPassed.dmVehIndex, TRUE)
//						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(dmVarsPassed.dmVehIndex, TRUE) // 1199361

						dmVarsPassed.eFreeze = eFREEZE_STATE_YES
						
						SET_UP_SPAWN_VEHICLE_COLOURS(playerBDPassed, dmVarsPassed, GET_PLAYER_TEAM(LocalPlayer))
						
						SET_UP_VDM_VEHICLE(playerBDPassed, dmVarsPassed)
						
						STORE_VEHICLE_SPAWN_INFO_FROM_THIS_CAR(dmVarsPassed.dmVehIndex, GET_GAMER_HANDLE_PLAYER(LocalPlayer)) // must be called AFTER setting the vehicle colours
						
						// make sure player is unfrozen and has collision before we warp them into the vehicle, as we can't do it once they are inside. 						
						FREEZE_ENTITY_POSITION(LocalPlayerPed, FALSE)  
						SET_ENTITY_COLLISION(LocalPlayerPed, TRUE)	
						SET_VEHICLE_IS_STOLEN(dmVarsPassed.dmVehIndex, FALSE)
												
						//FREEZE_ENTITY_POSITION(dmVarsPassed.dmVehIndex, TRUE)
						
						//SET_VEHICLE_RADIO_ENABLED(dmVarsPassed.dmVehIndex, FALSE)
						//SET_RADIO_TO_STATION_NAME("OFF")
						SET_VEH_RADIO_STATION(dmVarsPassed.dmVehIndex, "OFF")
						
						IF NOT g_sImpromptuVars.bVehicleDM
							SET_ENTITY_VISIBLE(dmVarsPassed.dmVehIndex, FALSE)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_PlayerVehiclesExplosionResistant)
							PRINTLN("[DM][HAS_INITIAL_SPAWN_FINISHED] - Calling SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE on initial spawn.")
							SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE(TRUE)
						ENDIF
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Monster3IncreasedCrushDamage)
							PRINTLN("[DM][HAS_INITIAL_SPAWN_FINISHED] - ciOptionsBS22_Monster3IncreasedCrushDamage is set")
							
							IF IS_VEHICLE_A_MONSTER(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex))
								SET_INCREASE_WHEEL_CRUSH_DAMAGE(dmVarsPassed.dmVehIndex, TRUE)
								PRINTLN("[DM][HAS_INITIAL_SPAWN_FINISHED] - Using SET_INCREASE_WHEEL_CRUSH_DAMAGE")
							ENDIF
						ENDIF
						
						IF CONTENT_IS_USING_ARENA()
							APPLY_ARENA_SETTINGS_TO_PLAYER_VEH(dmVarsPassed.dmVehIndex, IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_USING_PERSONAL_VEH))
						ENDIF						
						
						SET_ENTITY_INVINCIBLE(dmVarsPassed.dmVehIndex, TRUE)
																	
						GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__ENTER_VEH)		
					ELSE
						PRINTLN("[INITIAL_SPAWN__CREATE_VEH] failing on CREATE_NET_VEHICLE")NET_NL()
					ENDIF
				ELSE
					PRINTLN("[INITIAL_SPAWN__CREATE_VEH] failing on CAN_REGISTER_MISSION_VEHICLES")NET_NL()
				ENDIF
			ELSE
				PRINTLN("[INITIAL_SPAWN__CREATE_VEH] failing to load ", GET_MODEL_NAME_FOR_DEBUG(GET_DM_VEHICLE_TO_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData)))
			ENDIF			
		ENDIF
		
		// VEHICLE DM ENTER
		IF (GET_INITIAL_SPAWN_STAGE(dmVarsPassed) = INITIAL_SPAWN__ENTER_VEH)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
				dmVarsPassed.dmVehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				PRINTLN("[VEH] 2 Setting dmVarsPassed.dmVehIndex to ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex)))
				
				IF IS_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehIndex)
					IF GET_PED_IN_VEHICLE_SEAT(dmVarsPassed.dmVehIndex) = LocalPlayerPed	

						SET_MODEL_AS_NO_LONGER_NEEDED(GET_DM_VEHICLE_TO_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData))
						GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__RETURN_TRUE)
					ELSE
						PRINTLN("HAS_INITIAL_VEH_SPAWN_FINISHED - VEH_INITIAL_SPAWN__ENTER_VEHICLE - Player not yet in vehicle seat")	
					ENDIF
				ELSE
					PRINTLN("HAS_INITIAL_VEH_SPAWN_FINISHED - VEH_INITIAL_SPAWN__ENTER_VEHICLE - Vehicle is not driveable, wtf?")	
				ENDIF
			ELSE
				PRINTLN("HAS_INITIAL_VEH_SPAWN_FINISHED - VEH_INITIAL_SPAWN__ENTER_VEHICLE - Ped is not in a vehicle yet.")	
				IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
					IF IS_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehIndex)
						IF IS_VEHICLE_SEAT_FREE(dmVarsPassed.dmVehIndex)
							PRINTLN(" VEH_INITIAL_SPAWN__ENTER_VEHICLE - Tasking player to enter vehicle...")								
							TASK_ENTER_VEHICLE(LocalPlayerPed, dmVarsPassed.dmVehIndex, -1, VS_DRIVER, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
							SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, FALSE) 
						ELSE
							PRINTLN("HAS_INITIAL_VEH_SPAWN_FINISHED - VEH_INITIAL_SPAWN__ENTER_VEHICLE - Driver seat not free wtf?")
						ENDIF
					ELSE
						PRINTLN("HAS_INITIAL_VEH_SPAWN_FINISHED - VEH_INITIAL_SPAWN__ENTER_VEHICLE - Vehicle not driveable, wtf?")	
					ENDIF
				ELSE
					PRINTLN("HAS_INITIAL_VEH_SPAWN_FINISHED - VEH_INITIAL_SPAWN__ENTER_VEHICLE - Vehicle doesn't exist, wtf?")						
					GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__CREATE_VEH)
				ENDIF
			ENDIF
		ENDIF
		
		// ON FOOT NORMAL DEATHMATCHES - vehicle dm and impromptu need not apply.
		IF (GET_INITIAL_SPAWN_STAGE(dmVarsPassed) = INITIAL_SPAWN__WARP_PLAYER)		
			PRINTLN("INITIAL_SPAWN__WARP_PLAYER, OK")		
			IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, TRUE, TRUE)	
				PRINTLN("INITIAL_SPAWN__WARP_PLAYER, HAS_INITIAL_SPAWN_FINISHED,  >>> RETURN TRUE ")
				GO_TO_INITIAL_SPAWN_STATE(dmVarsPassed, INITIAL_SPAWN__RETURN_TRUE)
			ELSE			
				PRINTLN("INITIAL_SPAWN__WARP_PLAYER, HAS_INITIAL_SPAWN_FINISHED waiting on  WARP_TO_SPAWN_LOCATION ")
			ENDIF	
		ENDIF
		
		// neilF - added a new stage return_true to keep all the flags that need set in one place
		IF (GET_INITIAL_SPAWN_STAGE(dmVarsPassed) = INITIAL_SPAWN__RETURN_TRUE)
		
			GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].bConsiderPlayerForVisibleChecks = TRUE
			GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].vInitialSpawnCoords = GET_PLAYER_COORDS(LocalPlayer)
			SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
			
		
			PRINTLN("INITIAL_SPAWN__RETURN_TRUE, OK")
			RETURN(TRUE)
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PLAYER_SETUP_AFTER_SPAWN(PlayerBroadcastData &playerBDPassed[], PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	IF NOT IS_THIS_LEGACY_DM_CONTENT()
		IF playerBDPassed[iLocalPart].iPendingModSet = -1
			APPLY_MOD_SET_TO_LOCAL_PLAYER(sPlayerModifierData, playerBDPassed[iLocalPart].iCurrentModSet)
			CLEAR_PENDING_MODIFIER_DATA(sPlayerModifierData, playerBDPassed[iLocalPart].iPendingModSet)
		ENDIF
	ENDIF
	
	PROCESS_FRIENDLY_FIRE_SETTING_FOR_TEAM(playerBDPassed[iLocalPart].iTeam)
	
ENDPROC

PROC PREPARE_PLAYER_AFTER_INITIAL_SPAWN(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	PRINTLN("PREPARE_PLAYER_AFTER_INITIAL_SPAWN	= ")
	MPGlobals.g_NetWarpReStarted = FALSE
//	IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
//		SET_ENTITY_VISIBLE(LocalPlayerPed, TRUE)
//	ENDIF
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		CLEAR_AREA(dmVarsPassed.vInitialSpawnPos, 200.0, TRUE, FALSE)
		
		IF NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()				
			REMOVE_ALL_PED_WEAPONS(LocalPlayerPed)
			PRINTLN("PREPARE_PLAYER_AFTER_INITIAL_SPAWN - IS_JOB_FORCED_WEAPON_ONLY, REMOVE_ALL_PED_WEAPONS ")
			SET_BLOCK_AMMO_GLOBAL()
		ENDIF
		
		PROCESS_PLAYER_SETUP_AFTER_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData)
		IF NOT IS_BIT_SET(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INVENTORY_GIVEN)
			DEAL_WITH_FORCED_WEAPON_AND_AMMO(TRUE, dmVarsPassed.iDlcWeaponBitSet, dmVarsPassed.wtWeaponToRemove)
		ENDIF
		// Reset up the spawn points so team players spawn on normal points when they die and respawn
		IF USING_TEAM_SPAWNS(dmVarsPassed)
			SET_UP_CUSTOM_SPAWN_POINTS(serverBDpassed, playerBDPassed, dmVarsPassed, TRUE)
		ENDIF	
	ENDIF
ENDPROC

PROC CHANGE_MY_RESPAWN_STATE()
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("[RESPAWN_STATE_CHANGE] CHANGE_MY_RESPAWN_STATE, SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN) ")
		g_b_SkipKillStrip = TRUE
		SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_CLIENTS_AT_SPAWN_STAGE(PlayerBroadcastData &playerBDPassed[])

	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))	
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
//			IF NOT IS_PLAYER_SCTV(playerId)
			IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iParticipant)
				IF playerBDPassed[iParticipant].iInitialFadeProgress < INITIAL_FADE_STAGE_FADED_OUT
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_MOVE_TO_RESPAWN_STAGE(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)	
	
		RETURN FALSE
	ENDIF	
	// Important
	IF GET_CLIENT_MISSION_STAGE(playerBDPassed) > CLIENT_MISSION_STAGE_RESPAWN
	
		RETURN FALSE
	ENDIF
	IF GET_CLIENT_MISSION_STAGE(playerBDPassed) <= CLIENT_MISSION_STAGE_SETUP
	
		RETURN FALSE
	ENDIF
	
	IF IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
		RETURN TRUE
	ENDIF
		
	// If Neil's spawning is active don't do a manual spawn
	IF IS_PLAYER_RESPAWNING(LocalPlayer)
		RETURN FALSE
	ENDIF

	IF IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_TRIANGLE_WARP)	
		PRINTLN("----------> SHOULD_MOVE_TO_RESPAWN_STAGE TRUE, NONBD_BITSET_TRIANGLE_WARP ", tl31ScriptName)
		
		RETURN TRUE
	ENDIF
	
	// RESPAWN WHEN ON FOOT
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	AND NOT IS_PLAYER_ON_IMPROMPTU_DM() // careful taking this out as player got stuck accepting Impromptu
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_Disable_Respawn_When_Out_Of_Veh)
	AND NOT IS_NON_ARENA_KING_OF_THE_HILL()
		IF bLocalPlayerOK 
			IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				IF IS_SCREEN_FADED_IN()
	
					PRINTLN("----------> SHOULD_MOVE_TO_RESPAWN_STAGE TRUE, OK_TO_AUTO_RESPAWN_WHEN_ON_FOOT")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF ARE_PLANE_WINGS_BUSTED(serverBDpassed, dmVarsPassed)
		PRINTLN("----------> SHOULD_MOVE_TO_RESPAWN_STAGE TRUE, ARE_PLANE_WINGS_BUSTED")
		
		RETURN TRUE
	ENDIF
	
	IF NOT IS_NON_ARENA_KING_OF_THE_HILL()
		IF IS_VEH_IN_DEEP_WATER_FOR_TIME(dmVarsPassed)
			PRINTLN("----------> SHOULD_MOVE_TO_RESPAWN_STAGE TRUE, IS_VEH_IN_DEEP_WATER_FOR_TIME")
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF bLocalPlayerOK // If player dies with the vehicle then let Neil's stuff respawn player
		IF NOT DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
	
				PRINTLN("[NET_DEATHMATCH] SHOULD_MOVE_TO_RESPAWN_STAGE, IS_PED_IN_ANY_VEHICLE ")
				dmVarsPassed.dmVehIndex = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
				PRINTLN("[VEH] 3 Setting dmVarsPassed.dmVehIndex to ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex)))
			ENDIF
		ELSE
			IF IS_PED_IN_VEHICLE(LocalPlayerPed, dmVarsPassed.dmVehIndex)
				IF IS_VEHICLE_FUCKED(dmVarsPassed.dmVehIndex) 
					IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeRespawn)
						IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeRespawn, VEH_DM_RESPAWN_DELAY)

							PRINTLN("[NET_DEATHMATCH] SHOULD_MOVE_TO_RESPAWN_STAGE TRUE, IS_VEHICLE_FUCKED ")
							RESET_NET_TIMER(dmVarsPassed.timeRespawn)
							
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("[NET_DEATHMATCH] SHOULD_MOVE_TO_RESPAWN_STAGE, START_NET_TIMER ")
						START_NET_TIMER(dmVarsPassed.timeRespawn)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_SCREEN_FADED_IN()
					PRINTLN("[NET_DEATHMATCH] SHOULD_MOVE_TO_RESPAWN_STAGE, CLEANUP_THE_VEHICLE ")
					PRINTLN("5342883, SHOULD_MOVE_TO_RESPAWN_STAGE, CLEANUP_THE_VEHICLE")
					CLEANUP_THE_VEHICLE(dmVarsPassed)
					
					IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(dmVarsPassed.dmVehIndex)
					ENDIF
					
					PRINTLN("[VEH] 4 Setting dmVarsPassed.dmVehIndex to NULL")
					dmVarsPassed.dmVehIndex = NULL
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MOVE_TO_RESPAWN_STAGE(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
		EXIT
	ENDIF

	IF GET_CLIENT_MISSION_STAGE(playerBDPassed) <> CLIENT_MISSION_STAGE_RESPAWN
		IF SHOULD_MOVE_TO_RESPAWN_STAGE(serverBDpassed, playerBDPassed, dmVarsPassed)
			SET_PLAYER_INVINCIBLE(LocalPlayer, TRUE)
			DEDUCT_DEATHMATCH_SCORE()
			GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iKillStreak = 0  

			GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_RESPAWN)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_RESPAWN_STATE(SHARED_DM_VARIABLES &dmVarsPassed)

	RETURN dmVarsPassed.iRaceRespawnState
ENDFUNC

FUNC STRING GET_DM_RESPAWN_STATE_NAME(INT iState)
	STRING retVal = ""
	SWITCH iState
		CASE DM_RESPAWN__FADE_OUT				retVal = "DM_RESPAWN__FADE_OUT         		"	BREAK
		CASE DM_RESPAWN__DELETE_LAST_VEHICLE	retVal = "DM_RESPAWN__DELETE_LAST_VEHICLE		"	BREAK
		CASE DM_RESPAWN__POSITION_PLAYER		retVal = "DM_RESPAWN__POSITION_PLAYER   		"	BREAK
		CASE DM_RESPAWN__FADE_IN				retVal = "DM_RESPAWN__FADE_IN          		"	BREAK
		CASE DM_RESPAWN__FINISH_UP			retVal = "DM_RESPAWN__FINISH_UP          		"	BREAK
	ENDSWITCH
	
	RETURN retVal
ENDFUNC

PROC GO_TO_DM_RESPAWN_STATE(SHARED_DM_VARIABLES &dmVarsPassed, INT iState)
	
	#IF IS_DEBUG_BUILD
	NET_NL()
	NET_PRINT_FRAME()
	NET_PRINT_STRINGS("GO_TO_DM_RESPAWN_STATE - Previous state	= ", GET_DM_RESPAWN_STATE_NAME(dmVarsPassed.iRaceRespawnState))	NET_NL()
	NET_PRINT_STRINGS("GO_TO_DM_RESPAWN_STATE - Next state	= ", GET_DM_RESPAWN_STATE_NAME(iState))	NET_NL()
	#ENDIF
	
	dmVarsPassed.iRaceRespawnState = iState
ENDPROC

PROC RESET_DM_RESPAWN_STATE(SHARED_DM_VARIABLES &dmVarsPassed)
	#IF IS_DEBUG_BUILD
	NET_PRINT("RESET_DM_RESPAWN_STATE - called")	NET_NL()
	#ENDIF
	dmVarsPassed.iRaceRespawnState = DM_RESPAWN__FADE_OUT
ENDPROC

PROC FADE_IN_FROM_RESPAWN(SHARED_DM_VARIABLES &dmVarsPassed)
	GIVE_PLAYER_HEALTH()
	dmVarsPassed.tempVeh = NULL					
	dmVarsPassed.vehicleToDelete = INT_TO_NATIVE(VEHICLE_INDEX, -1)
	
	GO_TO_DM_RESPAWN_STATE(dmVarsPassed, DM_RESPAWN__FADE_IN)
ENDPROC

FUNC BOOL PROCESS_PLAYER_RESPAWN(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])
		
	// Stop player from exiting vehicle before I get a chance to lock it
	//DISABLE_VEHICLE_EXIT_THIS_FRAME()
	DISABLE_ALL_CONTROLS_FOR_PLAYER_THIS_FRAME()
	
	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		PRINTLN("[LM][SPEC_SPEC][PROCESS_PLAYER_RESPAWN] - Returning False.")
		RETURN FALSE
	ENDIF
		
	IF GET_RESPAWN_STATE(dmVarsPassed) = DM_RESPAWN__FADE_OUT
		IF NOT IS_PED_BEING_JACKED(LocalPlayerPed)
			SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_PLAYER_RESPAWNING)  
			//DO_SCREEN_FADE_OUT(1000)
			DISABLE_SELECTOR()
			//SETUP_SPECIFIC_SPAWN_LOCATION(dmVarsPassed.vRespawnLoc, dmVarsPassed.fRespawnHeading)
			PRINTLN("PROCESS_PLAYER_RESPAWN, DM_RESPAWN__FADE_OUT")	
			
			IF NATIVE_TO_INT(LocalPlayer) > -1
				SET_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === DM SET_BIT(GlobalplayerBD[", NATIVE_TO_INT(LocalPlayer), "].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)")
				#ENDIF
			ENDIF
					
			//SET_SWOOP_CAMERA_TYPE_FOR_RESPAWN(SPAWN_LOCATION_AT_SPECIFIC_COORDS, GET_PLAYER_COORDS(LocalPlayer))
			
			g_SpawnData.bIsSwoopedUp = FALSE  // url:bugstar:5386688 - Arena - Arcade Race I - Camera sees under the map during transition after manually respawning

			GO_TO_DM_RESPAWN_STATE(dmVarsPassed, DM_RESPAWN__DELETE_LAST_VEHICLE)
		ENDIF
	ENDIF
		
	IF GET_RESPAWN_STATE(dmVarsPassed) = DM_RESPAWN__DELETE_LAST_VEHICLE	
		IF FadeOutForRespawn(500, TRUE)
			STORE_VEHICLE_TO_DELETE_FOR_AFTER_RESPAWN(dmVarsPassed)						
			HIDE_PLAYER_AND_VEHICLE_FOR_WARP()			
			GO_TO_DM_RESPAWN_STATE(dmVarsPassed, DM_RESPAWN__POSITION_PLAYER)
			dmVarsPassed.timeRespawnTimeout = GET_NETWORK_TIME()
			
//			IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
//				DELETE_VEHICLE(dmVarsPassed.dmVehIndex)
//				PRINTLN("[DM][VEH] Deleting dmVarsPassed.dmVehIndex (2)")
//			ENDIF
		ENDIF
	ENDIF

	IF GET_RESPAWN_STATE(dmVarsPassed) = DM_RESPAWN__POSITION_PLAYER	
		//IF NOT CAN_ANY_PLAYER_SEE_ME_AS_VISIBLE_TO_SCRIPT() // wait until invisible on all players screens
//		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeRespawnTimeout)>500)
		
		
			BOOL bMinTimePassed
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeRespawnTimeout) >500)
				bMinTimePassed = TRUE
			ELSE
				PRINTLN("[RSPWN] PROCESS_PLAYER_RESPAWN, waiting for CAN_ANY_PLAYER_SEE_ME_AS_VISIBLE_TO_SCRIPT - time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeRespawnTimeout))							
				bMinTimePassed = FALSE
			ENDIF	
			
			IF NOT IS_PLAYER_ON_IMPROMPTU_DM()		
			AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				// Stops players getting stuck
				IF bLocalPlayerOK
					IF NOT IS_PED_BEING_JACKED(LocalPlayerPed)
						IF IS_PLAYER_CONTROL_ON(LocalPlayer) 
							SET_PLAYER_CONTROL(LocalPlayer, FALSE)
						ENDIF
						
					
						
						IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, TRUE, FALSE, FALSE, FALSE, TRUE,DEFAULT,DEFAULT,DEFAULT,DEFAULT, bMinTimePassed)
							FADE_IN_FROM_RESPAWN(dmVarsPassed)
						ENDIF
					ENDIF
				ELSE
					FADE_IN_FROM_RESPAWN(dmVarsPassed)
				ENDIF
			ELSE
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AUTOMATIC, FALSE, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, DEFAULT, DEFAULT, bMinTimePassed)
					FADE_IN_FROM_RESPAWN(dmVarsPassed)
				ENDIF			
			ENDIF
//		ELSE
//			PRINTLN("DM_RESPAWN__POSITION_PLAYER, waiting for CAN_ANY_PLAYER_SEE_ME - time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeRespawnTimeout))
//		ENDIF
	ENDIF


	IF GET_RESPAWN_STATE(dmVarsPassed) = DM_RESPAWN__FADE_IN
		IF NOT IS_PLAYER_CONTROL_ON(LocalPlayer) 
			SET_PED_CAN_RAGDOLL(LocalPlayerPed, TRUE)
			SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF	
		
	  	IF HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN(dmVarsPassed, playerBDPassed)
		
			IF IS_NON_ARENA_KING_OF_THE_HILL()
				IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
					SET_ENTITY_AS_MISSION_ENTITY(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, FALSE, TRUE)
				ENDIF
			ENDIF
			
			GO_TO_DM_RESPAWN_STATE(dmVarsPassed, DM_RESPAWN__FINISH_UP)
		ENDIF
	ENDIF
	
	IF GET_RESPAWN_STATE(dmVarsPassed) = DM_RESPAWN__FINISH_UP
		IF FadeInAfterRespawn()
			REVEAL_PLAYER_AND_VEHICLE_FOR_WARP()	
			ENABLE_SELECTOR()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 			
			FREEZE_ENTITY_POSITION(dmVarsPassed.dmVehIndex , FALSE)
			
			IF IS_SPAWNING_IN_VEHICLE()
			AND (IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel))
				BoostAirVehiclePlayerIsIn()				
			ENDIF
			
			IF NATIVE_TO_INT(LocalPlayer) > -1
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== HUD === DM CLEAR_BIT(GlobalplayerBD[", NATIVE_TO_INT(LocalPlayer), "].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)")
				#ENDIF
			ENDIF
			
			CLEANUP_THE_VEHICLE(dmVarsPassed)
			PRINTLN("5342883, PROCESS_PLAYER_RESPAWN, CLEANUP_THE_VEHICLE")
			CLEAR_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_PLAYER_RESPAWNING) 
			
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			
			RETURN TRUE	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

