#IF NOT IS_DEBUG_BUILD
USING "net_deathmatch_utility.sch"
#ENDIF
#IF IS_DEBUG_BUILD
USING "net_deathmatch_debug.sch"
#ENDIF

PROC IncreaseParticipantsTurnToSpawn(ServerBroadcastData &serverBDpassed)
	
	INT iStart  = serverBDpassed.iParticipantsTurnToSpawn
	INT iThisParticipant	
	INT i
	PLAYER_INDEX PlayerID
	
	PRINTLN("IncreaseParticipantsTurnToSpawn - iStart = ", iStart) 
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		// get next participant number to test
		iThisParticipant = 	iStart + i + 1
		IF (iThisParticipant >= NUM_NETWORK_PLAYERS)
			iThisParticipant -= NUM_NETWORK_PLAYERS	
		ENDIF
						
		// is this a valid next participant? 
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iThisParticipant))
			PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iThisParticipant))	
			IF IS_NET_PLAYER_OK(PlayerId, FALSE)
				IF NOT IS_BIT_SET(serverBDpassed.iBS_ParticipantHasSpawned, iThisParticipant)	
					serverBDpassed.iParticipantsTurnToSpawn = iThisParticipant
					PRINTLN("IncreaseParticipantsTurnToSpawn - returning ", iThisParticipant) 
					EXIT
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	PRINTLN("IncreaseParticipantsTurnToSpawn - end.") 
	
ENDPROC

PROC SERVER_SPAWN_TICKETS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	#IF IS_DEBUG_BUILD
	
		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_SPAWN_TICKETS, dm called on client")
	#ENDIF
	
	IF HAS_SERVER_CHECKED_EVERYONE_READY(serverBDpassed)
	OR NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ALL_PLAYERS_ON_BETTING)

		EXIT
	ENDIF

	PLAYER_INDEX PlayerId

	IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBDpassed.iParticipantsTurnToSpawn))		
		PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(serverBDpassed.iParticipantsTurnToSpawn))		
		IF IS_NET_PLAYER_OK(PlayerId, FALSE)
			IF IS_PLAYER_SPECTATOR(playerBDPassed, PlayerId, serverBDpassed.iParticipantsTurnToSpawn)
				// Ignore spectators
				SET_BIT(serverBDpassed.iBS_ParticipantHasSpawned, serverBDpassed.iParticipantsTurnToSpawn)

				IncreaseParticipantsTurnToSpawn(serverBDpassed)
				PRINTLN("SERVER_SPAWN_TICKETS, IS_PLAYER_SPECTATOR, increased iParticipantsTurnToSpawn to ", serverBDpassed.iParticipantsTurnToSpawn)
			ELSE
				// Once they have spawned
//				IF playerBDPassed[serverBDpassed.iParticipantsTurnToSpawn].iInitialFadeProgress > INITIAL_FADE_STAGE_FADED_OUT
				IF playerBDPassed[serverBDpassed.iParticipantsTurnToSpawn].eClientLogicStage > CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING
					PRINTLN("SERVER_SPAWN_TICKETS, this participant spawned OK ", serverBDpassed.iParticipantsTurnToSpawn)
					SET_BIT(serverBDpassed.iBS_ParticipantHasSpawned, serverBDpassed.iParticipantsTurnToSpawn)
					IncreaseParticipantsTurnToSpawn(serverBDpassed)					
					PRINTNL()
					PRINTLN("SERVER_SPAWN_TICKETS, increasing iParticipantsTurnToSpawn to ", serverBDpassed.iParticipantsTurnToSpawn)
				#IF IS_DEBUG_BUILD	
				ELSE
					PRINTLN("SERVER_SPAWN_TICKETS, iParticipantsTurnToSpawn = ", serverBDpassed.iParticipantsTurnToSpawn, " is at iInitialFadeProgress stage ", playerBDPassed[serverBDpassed.iParticipantsTurnToSpawn].iInitialFadeProgress)
				#ENDIF
				ENDIF
			ENDIF
		ELSE
			// Not OK
			IncreaseParticipantsTurnToSpawn(serverBDpassed)
			PRINTLN("SERVER_SPAWN_TICKETS, IS_NET_PLAYER_OK = FALSE, increased iParticipantsTurnToSpawn to ", serverBDpassed.iParticipantsTurnToSpawn)
		ENDIF
	ELSE
		// Not active
		IncreaseParticipantsTurnToSpawn(serverBDpassed)
		PRINTLN("SERVER_SPAWN_TICKETS, NETWORK_IS_PARTICIPANT_ACTIVE = FALSE, increased iParticipantsTurnToSpawn to ", serverBDpassed.iParticipantsTurnToSpawn)
	ENDIF

	// Cap
	IF (serverBDpassed.iParticipantsTurnToSpawn >=  NUM_NETWORK_PLAYERS)
		serverBDpassed.iParticipantsTurnToSpawn = 0	
		PRINTLN("SERVER_SPAWN_TICKETS, resetting iParticipantsTurnToSpawn to 0")
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_DM_VEH_MODEL()
	VEHICLE_INDEX CarID
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		CarID = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
		IF GET_ENTITY_MODEL(CarID) = GET_DM_VEH_MODEL()
			RETURN(TRUE)
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC CHECK_VEHICLE_STUCK_TIMERS(ServerBroadcastData &serverBDpassed)

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisableAllStuckTimers)
		EXIT
	ENDIF
	
	IF g_bCelebrationScreenIsActive
	OR g_bMissionEnding
	OR NOT g_bMissionClientGameStateRunning
	OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		EXIT
	ENDIF
	
	VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
	
	IF DOES_ENTITY_EXIST(veh)
	AND IS_ENTITY_ALIVE(veh)
		
		BOOL bHaveControl = NETWORK_HAS_CONTROL_OF_ENTITY(veh)
		
		MODEL_NAMES model = GET_ENTITY_MODEL( veh )
		IF HAS_DM_STARTED(serverBDpassed)
			INT iStuckTimer = ci_DM_STUCK_TIME
			INT iRoofTimer = ci_DM_ROOF_TIME
			
			IF g_FMMC_STRUCT.iStuckTimeOut > 0
				iStuckTimer = g_FMMC_STRUCT.iStuckTimeOut
				iRoofTimer = g_FMMC_STRUCT.iStuckTimeOut
				PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - Using override Stuck TimeOut ",g_FMMC_STRUCT.iStuckTimeOut )
			ENDIF
			
			IF (IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_ROOF, iRoofTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_SIDE, iStuckTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_HUNG_UP, iStuckTimer)
			OR IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_JAMMED, iStuckTimer))
			AND NOT IS_ENTITY_ATTACHED_TO_ANY_OBJECT(veh)
			AND NOT IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(veh)
				IF NOT IS_THIS_MODEL_A_BOAT( model ) 
				OR ( HAS_COLLISION_LOADED_AROUND_ENTITY(veh) )
				AND NOT IS_ENTITY_IN_WATER( veh ) 
					IF bHaveControl					
						#IF IS_DEBUG_BUILD
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_ROOF, iRoofTimer)
								PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_ON_ROOF ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_ON_SIDE, iStuckTimer)
								PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_ON_SIDE ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_HUNG_UP, iStuckTimer)
								PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_HUNG_UP ")
							ENDIF
							IF IS_VEHICLE_STUCK_TIMER_UP( veh, VEH_STUCK_JAMMED, iStuckTimer)
								PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - VEH_STUCK_JAMMED ")
							ENDIF
						#ENDIF
						
						// So We're not Spamming..
						IF IS_VEHICLE_DRIVEABLE(veh)
							PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS Vehicle is still driveable")						
							
							IF model = TRFLAT
							OR model = TRAILERS
								SET_ENTITY_HEALTH( veh, 0 )
								SET_VEHICLE_ENGINE_HEALTH( veh, -1000 )
								SET_VEHICLE_PETROL_TANK_HEALTH( veh, -1000 )
								PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - TRAILER IS STUCK SETTING PETROL TANK HEALTH -1000")
							ENDIF									
								
							IF GET_VEHICLE_ENGINE_HEALTH( veh ) > -1000
								SET_VEHICLE_ENGINE_HEALTH( veh, -1000 )
								IF GET_ENTITY_HEALTH( veh ) > 100
									SET_ENTITY_HEALTH( veh, 100 )
								ENDIF
								SET_VEHICLE_PETROL_TANK_HEALTH( veh, -1000 )
								PRINTLN("[LM][DM] CHECK_VEHICLE_STUCK_TIMERS - VEHICLE IS STUCK SETTING HEALTH -1000")
								
								IF CONTENT_IS_USING_ARENA()
								AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Disable_Arena_Mode)
									PRINTLN("[DM] CHECK_VEHICLE_STUCK_TIMERS - ARENA MODE IS SET - VEHICLE IS STUCK SETTING BODY HEALTH -1000")
									SET_VEHICLE_BODY_HEALTH(veh, -1000)
								ENDIF
							ENDIF
							SET_VEHICLE_UNDRIVEABLE(veh, TRUE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(veh, FALSE)
							
							NETWORK_EXPLODE_VEHICLE(veh, TRUE, FALSE, iLocalPart)
						ENDIF
						
						IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(veh)
							NETWORK_INDEX vehIdToDelete
							vehIdToDelete = VEH_TO_NET(veh)						
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Roaming_Spectator_Force_Delete_Old_Veh)	
							AND NOT CONTENT_IS_USING_ARENA()
								PRINTLN("[LM][DM] CHECK_VEHICLE_STUCK_TIMERS - cleaning up the old Veh (Via; CLEANUP_NET_ID)")
								IF NETWORK_DOES_NETWORK_ID_EXIST(vehIdToDelete)
									CLEANUP_NET_ID(vehIdToDelete)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(veh)
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_FROZEN_VEHICLE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])

	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, dmVarsPassed.sPlayerModifierData)
	
		IF dmVarsPassed.eFreeze = eFREEZE_STATE_YES

			IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
				IF IS_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehIndex)
					
					FREEZE_ENTITY_POSITION(dmVarsPassed.dmVehIndex, TRUE)
					
					PRINTLN("MAINTAIN_FROZEN_VEHICLE, FREEZE_ENTITY_POSITION, TRUE 1 ")
					IF GET_INITIAL_STAGE(playerBDPassed) <= INITIAL_FADE_STAGE_FADE_IN
					AND NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_PLACED_VEH_ON_GROUND_PROP)
						IF SET_VEHICLE_ON_GROUND_PROPERLY(dmVarsPassed.dmVehIndex, 1.0)
							PRINTLN("MAINTAIN_FROZEN_VEHICLE, SET_VEHICLE_ON_GROUND_PROPERLY, TRUE 1 ")
							SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_PLACED_VEH_ON_GROUND_PROP)
						ELSE
							PRINTLN("MAINTAIN_FROZEN_VEHICLE, SET_VEHICLE_ON_GROUND_PROPERLY, FAILED 1")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
					IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), TRUE)
							PRINTLN("MAINTAIN_FROZEN_VEHICLE, FREEZE_ENTITY_POSITION, TRUE 2 ")
							IF GET_INITIAL_STAGE(playerBDPassed) <= INITIAL_FADE_STAGE_FADE_IN
							AND NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_PLACED_VEH_ON_GROUND_PROP)
								IF SET_VEHICLE_ON_GROUND_PROPERLY(dmVarsPassed.dmVehIndex, 1.0)
									PRINTLN("MAINTAIN_FROZEN_VEHICLE, SET_VEHICLE_ON_GROUND_PROPERLY, TRUE 2 ")
									SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_PLACED_VEH_ON_GROUND_PROP)
								ELSE
									PRINTLN("MAINTAIN_FROZEN_VEHICLE, SET_VEHICLE_ON_GROUND_PROPERLY, FAILED 2")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
		ELIF dmVarsPassed.eFreeze = eFREEZE_STATE_NO
		
			IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
				IF IS_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehIndex)
					FREEZE_ENTITY_POSITION(dmVarsPassed.dmVehIndex, FALSE)
					PRINTLN("MAINTAIN_FROZEN_VEHICLE, FREEZE_ENTITY_POSITION, FALSE 1 ")
					dmVarsPassed.eFreeze = eFREEZE_STATE_NONE
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
					IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
							FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
							PRINTLN("MAINTAIN_FROZEN_VEHICLE, FREEZE_ENTITY_POSITION, FALSE 2 ")
							dmVarsPassed.eFreeze = eFREEZE_STATE_NONE
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
//		ELSE
//			PRINTLN("MAINTAIN_FROZEN_VEHICLE, FREEZE_ENTITY_POSITION, DEAD ")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_USE_DEFAULT_CORONA_VEHICLE(INT iTeam)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DM_SpawnInCoronaVehicle)
		RETURN FALSE
	ENDIF
	
	RETURN g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam] != -1
	AND g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam] != -1
	AND g_mnMyRaceModel = DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_DM_VEHICLE_TO_SPAWN(PlayerBroadcastData &playerBDPassed[], PLAYER_MODIFIER_DATA &sPlayerModifierData)
	
	INT iLocalPlayerPart = iLocalPart
	
	INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].sClientCoronaData.iTeamChosen
	IF NOT IS_TEAM_DEATHMATCH()
		iTeam = 0
	ENDIF
	
	IF IS_FREE_FOR_ALL_DM_BEING_PLAYED_AS_TEAM_DM()
	AND g_FMMC_STRUCT.mnVehicleModel[iTeam] = DUMMY_MODEL_FOR_SCRIPT
		iTeam = 0
		PRINTLN("[DM][SPAWN VEH] - GET_DM_VEHICLE_TO_SPAWN - Setting iTeam to 0 because this content is meant to be free-for-all but we're playing it as a Team DM & our team would have nothing otherwise")
	ENDIF
	
	//If a player modifier has been set in PROCESS_PLAYER_MODIFIERS__RESPAWN_VEHICLE we'll now use that model for spawning in 
	IF sPlayerModifierData.mnRespawnVehicle != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[DM][SPAWN VEH][0] - GET_DM_VEHICLE_TO_SPAWN - mnVehicleModel is | ", GET_MODEL_NAME_FOR_DEBUG(sPlayerModifierData.mnRespawnVehicle))
		RETURN sPlayerModifierData.mnRespawnVehicle
	ENDIF
	
	IF IS_NON_ARENA_KING_OF_THE_HILL()
		IF KOTH_SHOULD_I_SPAWN_IN_VEHICLE(iTeam)
			PRINTLN("[DM][SPAWN VEH][1] - GET_DM_VEHICLE_TO_SPAWN - mnVehicleModel is | ", GET_MODEL_NAME_FOR_DEBUG(g_FMMC_STRUCT.mnVehicleModel[iTeam]))
			RETURN g_FMMC_STRUCT.mnVehicleModel[iTeam]
		ENDIF
	ENDIF
	
	IF playerBDPassed[iLocalPlayerPart].mnCoronaVehicle != DUMMY_MODEL_FOR_SCRIPT
		PRINTLN("[DM][SPAWN VEH] - GET_DM_VEHICLE_TO_SPAWN - Using corona vehicle | ", GET_MODEL_NAME_FOR_DEBUG(playerBDPassed[iLocalPlayerPart].mnCoronaVehicle))
		RETURN playerBDPassed[iLocalPlayerPart].mnCoronaVehicle
	ELIF SHOULD_USE_DEFAULT_CORONA_VEHICLE(iTeam)
		RETURN GET_VEHICLE_MODEL_FROM_CREATOR_SELECTION(g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeam], g_FMMC_STRUCT.iDefaultCommonVehicle[iTeam])
	ELSE
		PRINTLN("[DM][SPAWN VEH] - GET_DM_VEHICLE_TO_SPAWN - GET_DM_VEH_MODEL")
		RETURN GET_DM_VEH_MODEL()
	ENDIF
ENDFUNC

FUNC BOOL CREATE_PLAYER_SPAWN_VEHICLE(PlayerBroadcastData &playerBDPassed[], NETWORK_INDEX& niVeh, MODEL_NAMES mnVehToSpawn, VECTOR vSpawnPos, FLOAT fSpawnHeading)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciUSE_PERSONAL_VEHICLES_FOR_TEAM_VEHICLE)
	OR GET_CUSTOM_VEHICLE_MODEL_NAME(g_iMyRaceModelChoice) = DUMMY_MODEL_FOR_SCRIPT
	OR NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_USING_PERSONAL_VEH)
		RETURN CREATE_NET_VEHICLE(niVeh, mnVehToSpawn, vSpawnPos, fSpawnHeading, FALSE)
	ELSE 
		RETURN CREATE_MP_RACE_SAVED_VEHICLE(niVeh, vSpawnPos, fSpawnHeading, TRUE )
	ENDIF
ENDFUNC

PROC SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE(PlayerBroadcastData &playerBDPassed[])
	
	#IF IS_DEBUG_BUILD
	INT iTeamToUse = playerBDPassed[iLocalPart].iTeam
	IF playerBDPassed[iLocalPart].iTeam = -1
		iTeamToUse = 0
	ENDIF
	#ENDIF
	
	IF NOT IS_THIS_DM_USING_CORONA_VEHICLES()
		EXIT
	ENDIF
	
	playerBDPassed[iLocalPart].mnCoronaVehicle = g_mnCoronaMissionVehicle //g_mnMyRaceModel

	IF IS_MODEL_VALID(playerBDPassed[iLocalPart].mnCoronaVehicle)
		PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - Vehicle model selected: ", GET_MODEL_NAME_FOR_DEBUG(playerBDPassed[iLocalPart].mnCoronaVehicle))
	ELSE
		PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - Vehicle model selected: INVALID!!")
	ENDIF
	PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - g_FMMC_STRUCT.iDefaultCommonVehicleType[",iTeamToUse,"]: ",g_FMMC_STRUCT.iDefaultCommonVehicleType[iTeamToUse], " g_FMMC_STRUCT.iDefaultMissionVehicle: ", g_FMMC_STRUCT.iDefaultCommonVehicle[iTeamToUse])
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciUSE_PERSONAL_VEHICLES_FOR_TEAM_VEHICLE)
		//No personal vehicles for flow_play
		IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(LocalPlayer)
			playerBDPassed[iLocalPart].mnCoronaVehicle = g_mnMyRaceModel
			SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_USING_PERSONAL_VEH)
			PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - USING PERSONAL VEHICLE - ", GET_MODEL_NAME_FOR_DEBUG(g_mnMyRaceModel), " g_iMyRaceModelChoice: ", g_iMyRaceModelChoice)
			PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - IS_PLAYER_SELECTING_CUSTOM_VEHICLE is returning TRUE but we're not worried about personal vehicles for this play, so doing nothing")
		ELSE
			PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - IS_PLAYER_SELECTING_CUSTOM_VEHICLE is returning FALSE")
			
			IF CONTENT_IS_USING_ARENA()
				playerBDPassed[iLocalPart].mnCoronaVehicle = g_mnMyRaceModel
				IF IS_MODEL_VALID(playerBDPassed[iLocalPart].mnCoronaVehicle)
					PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - Arena Content, so using g_mnMyRaceModel - Vehicle model selected: ", GET_MODEL_NAME_FOR_DEBUG(playerBDPassed[iLocalPart].mnCoronaVehicle))
				ELSE
					PRINTLN("[DM][SPAWN VEH] SET_SPAWN_VEHICLE_TO_CORONA_VEHICLE - Arena Content, so using g_mnMyRaceModel - Vehicle model selected: INVALID!!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC GET_ARENA_TEAM_VEHICLE_COLOURS(INT iTeam, INT &iColour1, INT &iColour2, INT &iColour4)
	SWITCH iTeam
		CASE 0
			//Orange
			iColour1 = 124
			iColour2 = 41
			iColour4 = 124
		BREAK
		CASE 1
			//Purple
			iColour1 = 145
			iColour2 = 81
			iColour4 = 148
		BREAK
		CASE 2
			//Pink
			iColour1 = 137
			iColour2 = 136
			iColour4 = 136
		BREAK
		CASE 3
			//Green
			iColour1 = 53
			iColour2 = 55
			iColour4 = 55
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL IS_CREW_COLOUR()
	IF g_iMyRaceColorSelection = -1
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC SET_UP_ARENA_SPAWN_VEHICLE(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, INT iTeamOverride = -2)
	
	IF NOT CONTENT_IS_USING_ARENA()
		EXIT
	ENDIF
	
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(LocalPlayer)
	AND NOT IS_TEAM_DEATHMATCH()
		EXIT
	ENDIF
	
	INT iTeam = playerBDPassed[iLocalPart].iTeam
	IF iTeamOverride != -2
		PRINTLN("[SET_UP_ARENA_SPAWN_VEHICLE] - Using iTeamOverride: ", iTeamOverride)
		iTeam = iTeamOverride
	ENDIF
	
	IF IS_TEAM_DEATHMATCH()
		INT iColour1, iColour2, iColour4
		GET_ARENA_TEAM_VEHICLE_COLOURS(iTeam, iColour1, iColour2, iColour4)
		SET_VEHICLE_COLOURS(dmVarsPassed.dmVehIndex, iColour1, iColour2)
		SET_VEHICLE_EXTRA_COLOURS(dmVarsPassed.dmVehIndex, 0, iColour4)
		SET_TEAM_COLOUR_OVERRIDE_FLAG(TRUE)
		PRINTLN("[SET_UP_ARENA_SPAWN_VEHICLE] - Setting up Vehicle iColour1: ", iColour1, " iColour2: ", iColour2, " iColour4: ", iColour4)
	
	ELSE
		IF IS_CREW_COLOUR()			
			PRINTLN("[SET_UP_ARENA_SPAWN_VEHICLE] - GET_VEH_RGB_FOR_LBD, IS_CREW_COLOUR ")					
			INT iRed, iGreen, iBlue
			GET_PLAYERS_CREW_COLOUR(LocalPlayer, TRUE, iRed, iGreen, iBlue)
			
			SET_VEHICLE_CUSTOM_PRIMARY_COLOUR(dmVarsPassed.dmVehIndex, iRed, iGreen, iBlue)
			SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(dmVarsPassed.dmVehIndex, iRed, iGreen, iBlue)
	
		ELIF g_iMyRaceColorSelection = FM_RACE_COLOUR_DEFAULT
	
			INT iColour1, iColour2, iExtraColour1, iExtraColour2, iColour5, iColour6
			GET_DEFAULT_COLOUR_SELECTION_COLOURS(dmVarsPassed.dmVehIndex, iColour1, iColour2, iExtraColour1, iExtraColour2, iColour5, iColour6)
			PRINTLN("SET_UP_ARENA_SPAWN_VEHICLE - FM_RACE_COLOUR_DEFAULT, set to default colours:")
			PRINTLN("							iColour1: ", iColour1)
			PRINTLN("							iColour2: ", iColour2)
			PRINTLN("							iExtraColour1: ", iExtraColour1)
			PRINTLN("							iExtraColour2: ", iExtraColour2)
			PRINTLN("							iColour5: ", iColour5)
			PRINTLN("							iColour6: ", iColour6)
			
			SET_VEHICLE_COLOURS(dmVarsPassed.dmVehIndex, iColour1, iColour2)
			SET_VEHICLE_EXTRA_COLOURS(dmVarsPassed.dmVehIndex, iExtraColour1, iExtraColour2)
			SET_VEHICLE_EXTRA_COLOUR_5(dmVarsPassed.dmVehIndex, iColour5)
			SET_VEHICLE_EXTRA_COLOUR_6(dmVarsPassed.dmVehIndex, iColour6)
	
		ELSE
			SET_VEHICLE_COLOUR_FROM_LOBBY_SELECTION(dmVarsPassed.dmVehIndex)
		ENDIF
		
	ENDIF
ENDPROC

PROC SET_UP_SPAWN_VEHICLE_COLOURS(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, INT iTeamOverride = -2)
	
	IF g_iMyRaceColorSelection = FM_RACE_COLOUR_DEFAULT
		EXIT
	ENDIF
	
	IF CONTENT_IS_USING_ARENA()
		SET_UP_ARENA_SPAWN_VEHICLE(playerBDPassed, dmVarsPassed, iTeamOverride)
		EXIT
	ENDIF
	
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(LocalPlayer)
		EXIT
	ENDIF
	
	IF IS_CURRENT_MODIFIER_SET_VALID(playerBDPassed[iLocalPart].iCurrentModset)
	AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.sModifierSet[playerBDPassed[iLocalPart].iCurrentModset].iModifierSetBS, ciDM_ModifierBS_SpawnInVehicle)
		SET_DM_VEHICLE_COLOUR(dmVarsPassed.dmVehIndex)
		EXIT
	ENDIF
	
	SET_VEHICLE_COLOUR_FROM_LOBBY_SELECTION(dmVarsPassed.dmVehIndex)
	
ENDPROC

PROC SET_UP_VDM_VEHICLE(PlayerBroadcastData& playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	VEHICLE_INDEX viVeh
	IF IS_ENTITY_ALIVE(localPlayerPed)
	AND IS_PED_IN_ANY_VEHICLE(localPlayerPed)
		viVeh = GET_VEHICLE_PED_IS_IN(localPlayerPed)
	ENDIF
	
	IF IS_VEHICLE_FUCKED_MP(viVeh)
		EXIT
	ENDIF
	
	IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
		INT iTeam = playerBDPassed[iLocalPart].iTeam
		PRINTLN("SET_UP_VDM_VEHICLE - Called iTeam: ", iTeam)
		IF iTeam != -1
			SET_ENTITY_CAN_BE_DAMAGED_BY_RELATIONSHIP_GROUP(viVeh, FALSE, dmVarsPassed.rgFM_DEATHMATCH[iTeam])
		ENDIF
		
		SET_VEHICLE_DAMAGE_SCALES(viVeh, 1.0, 0.0, 1.0)
		
	ENDIF
ENDPROC

PROC SET_VEH_NEEDS_LOCKED(SHARED_DM_VARIABLES &dmVarsPassed)
	PRINTLN("SET_VEH_NEEDS_LOCKED()")
	dmVarsPassed.bLockVeh = FALSE	
ENDPROC

PROC STORE_VEHICLE_TO_DELETE_FOR_AFTER_RESPAWN(SHARED_DM_VARIABLES &dmVarsPassed)
	dmVarsPassed.vehicleToDelete = INT_TO_NATIVE(VEHICLE_INDEX, -1)	
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		dmVarsPassed.vehicleToDelete = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)	
	ELSE
		dmVarsPassed.vehicleToDelete = dmVarsPassed.storedLastVeh	
	ENDIF
	
	
ENDPROC

// Store the last vehicle used for use when respawning
PROC CACHE_PLAYER_LAST_VEHICLE(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDpassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	AND NOT DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, dmVarsPassed.sPlayerModifierData)
		EXIT
	ENDIF

	IF NOT DOES_ENTITY_EXIST(dmVarsPassed.storedLastVeh)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			dmVarsPassed.storedLastVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)	
		ELSE
			dmVarsPassed.storedLastVeh = GET_PLAYERS_LAST_VEHICLE()	
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])

	ENTITY_INDEX eVeh 
	
	IF DOES_ENTITY_EXIST(dmVarsPassed.tempVeh)
	
		IF NOT IS_VEHICLE_SEAT_FREE(dmVarsPassed.tempVeh)
			IF GET_PED_IN_VEHICLE_SEAT(dmVarsPassed.tempVeh, DEFAULT, TRUE) != LocalPlayerPed
				PRINTLN("[DM][HAS_INITIAL_SPAWN_FINISHED] - Exiting HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN early because we're currently looking at the wrong vehicle || SCRIPT_VEHICLE_", NETWORK_ENTITY_GET_OBJECT_ID(dmVarsPassed.tempVeh))
				dmVarsPassed.tempVeh = NULL
				
				RETURN FALSE
			ENDIF
		ENDIF
		
		IF CAN_REGISTER_MISSION_VEHICLES(1)
			eVeh = GET_ENTITY_FROM_PED_OR_VEHICLE(dmVarsPassed.tempVeh)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(eVeh)
				
				SET_ENTITY_AS_MISSION_ENTITY(eVeh, FALSE, TRUE)
		
				dmVarsPassed.dmVehIndex = dmVarsPassed.tempVeh
				PRINTLN("[VEH] 5 Setting dmVarsPassed.dmVehIndex to ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex)))
				
				IF IS_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehIndex)
					SET_VEHICLE_ENGINE_ON(dmVarsPassed.dmVehIndex, TRUE, TRUE)
					SET_ENTITY_HEALTH(dmVarsPassed.dmVehIndex, iRACE_VEH_ENGINE_HEALTH)
					SET_VEHICLE_ENGINE_HEALTH(dmVarsPassed.dmVehIndex, fRACE_VEH_ENGINE_HEALTH)
					SET_VEHICLE_PETROL_TANK_HEALTH(dmVarsPassed.dmVehIndex, fRACE_VEH_ENGINE_HEALTH)
					SET_VEHICLE_FIXED(dmVarsPassed.dmVehIndex)
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_PlayerVehiclesExplosionResistant)
						SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE(TRUE)
						PRINTLN("[DM][HAS_INITIAL_SPAWN_FINISHED] - Calling SET_DISABLE_VEHICLE_EXPLOSIONS_DAMAGE on respawn")
					ENDIF
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Monster3IncreasedCrushDamage)
						PRINTLN("[DM][HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN] - ciOptionsBS22_Monster3IncreasedCrushDamage is set")
						
						IF IS_VEHICLE_A_MONSTER(GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex))
							SET_INCREASE_WHEEL_CRUSH_DAMAGE(dmVarsPassed.dmVehIndex, TRUE)
							PRINTLN("[DM][HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN] - Calling SET_INCREASE_WHEEL_CRUSH_DAMAGE")
						ENDIF
					ENDIF
					
					IF CONTENT_IS_USING_ARENA()
						APPLY_ARENA_SETTINGS_TO_PLAYER_VEH(dmVarsPassed.dmVehIndex, IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_USING_PERSONAL_VEH))
					ENDIF
				ENDIF
						
				FREEZE_ENTITY_POSITION(eVeh, TRUE)
				
//				SET_VEHICLE_COLOUR_RACE(serverBDpassed, playerBDPassed, dmVarsPassed)
				
				IF NOT ((g_bFM_ON_VEH_DEATHMATCH) AND IS_VEHICLE_MODEL(dmVarsPassed.tempVeh, RHINO)) // added by Neil, dan wanted this for 1632406
					SET_NETWORK_VEHICLE_RESPOT_TIMER(NETWORK_GET_NETWORK_ID_FROM_ENTITY(eVeh), VEH_RESPOT_TIMER, DEFAULT, TRUE)	
				ENDIF
				
				SET_UP_SPAWN_VEHICLE_COLOURS(playerBDPassed, dmVarsPassed)
				
				SET_UP_VDM_VEHICLE(playerBDPassed, dmVarsPassed)

				CLEAR_AREA(GET_ENTITY_COORDS(eVeh), CLEAR_AREA_AROUND_NEW_VEH_AREA, FALSE)
	
				PRINTLN("HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN = TRUE")
				
				RETURN TRUE
			ELSE
				NETWORK_REQUEST_CONTROL_OF_ENTITY(eVeh)
			ENDIF			
		ELSE
			PRINTLN("stuck CAN_REGISTER_MISSION_VEHICLES")
		ENDIF
	ELSE
		IF bLocalPlayerOK
			IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
				PRINTLN("GET_VEHICLE_PED_IS_IN")
				dmVarsPassed.tempVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			ELSE
				PRINTLN("dmVarsPassed.tempVeh = dmVarsPassed.storedLastVeh")
				dmVarsPassed.tempVeh = dmVarsPassed.storedLastVeh
			ENDIF
		ELSE
			PRINTLN("HAS_VEHICLE_BEEN_PREPARED_AFTER_RESPAWN stuck IS_NET_PLAYER_OK")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_LOCAL_RESPAWN_VEHICLE_BOUNDS(INT iVeh)
	VECTOR vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	MODEL_NAMES vehModel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	VECTOR vStart, vEnd, vMax, vMin
	FLOAT fWidth
	GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
	IF IS_THIS_MODEL_A_HELI(vehModel)
		fWidth = vMax.x*0.55
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.5
		vMax.y = vMax.y*0.5
	ELSE
		fWidth = vMax.x*0.8
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.9
		vMax.y = vMax.y*0.9
	ENDIF	
	vMin.y -= fWidth // START_SHAPE_TEST_CAPSULE is extending the end/start points........................
	vMax.y += fWidth
	vStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMin)
	vEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMax)
	
	vSpawnPos.x = (vStart.x + vEnd.x)
	vSpawnPos.y = (vStart.y + vEnd.y)
	vSpawnPos.x /= 2
	vSpawnPos.y /= 2
	
	FLOAT length = GET_DISTANCE_BETWEEN_COORDS(vEnd, vStart, FALSE)
	FLOAT angle = GET_HEADING_FROM_VECTOR_2D(vEnd.x - vStart.x, vEnd.y - vStart.y)
	DRAW_MARKER(MARKER_BOXES, (<<vSpawnPos.x, vSpawnPos.y, vSpawnPos.z-0.2>>), (<<0.0, 0.0, 0.0>>), (<<0.0, 0.0, angle>>), (<<ABSF(fWidth*2), ABSF(length), 0.5>>), 255, 50, 50, 150)
	
ENDPROC

FUNC BOOL IS_SHAPETESTED_VEHICLE_AREA_SAFE(INT iVeh, SHARED_DM_VARIABLES &dmVarsPassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed)
	
	INT iHitSomething
	ENTITY_INDEX hitEntity
	VECTOR vNormal
	VECTOR vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	MODEL_NAMES vehModel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	VECTOR vStart, vEnd, vMax, vMin
	FLOAT fWidth
	GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
	IF IS_THIS_MODEL_A_HELI(vehModel)
		fWidth = vMax.x*0.55
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.5
		vMax.y = vMax.y*0.5
	ELSE
		fWidth = vMax.x*0.8
		vMin.x = 0	
		vMax.x = 0
		vMin.z = vMax.z*0.8
		vMax.z = vMax.z*0.8
		vMin.y = vMin.y*0.9
		vMax.y = vMax.y*0.9
	ENDIF	
	vStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMin)
	vEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMax)
	
	IF dmVarsPassed.iVehToShapeTest != iVeh
		PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] iVehToShapeTest != iVeh not shapetesting yet, wait out turn (iVeh: ", iVeh, " iVehToShapeTest: ", dmVarsPassed.iVehToShapeTest, ")")
		RETURN FALSE
	ENDIF
	
	/*
	vMin.y -= fWidth // START_SHAPE_TEST_CAPSULE is extending the end/start points........................
	vMax.y += fWidth
	VECTOR vdbgStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, <<vMin.x+fWidth, vMin.y, vMin.z>>)
	VECTOR vdbgEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, <<vMax.x+fWidth, vMax.y, vMax.z>>)
	DRAW_DEBUG_LINE(vstart, vend)
	DRAW_DEBUG_LINE(vdbgStart, vdbgEnd)
	*/
		
	dmVarsPassed.stiVehSafeStatus = GET_SHAPE_TEST_RESULT(dmVarsPassed.stiVehSafe, iHitSomething, vSpawnPos, vNormal, hitEntity)
	IF dmVarsPassed.stiVehSafeStatus = SHAPETEST_STATUS_RESULTS_READY
		PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] -stStatus: SHAPETEST_STATUS_RESULTS_READY")
		
		dmVarsPassed.iVehToShapeTest++
		
		IF NOT DOES_ENTITY_EXIST(hitEntity)
			PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] -stStatus: Entity Not Hit. Returning True - Next vehicle to test is iVeh: ", dmVarsPassed.iVehToShapeTest)
			RETURN TRUE
		ELSE
			PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] -stStatus: Entity Hit. Returning False Next vehicle to test is iVeh: ", dmVarsPassed.iVehToShapeTest)
			RETURN FALSE
		ENDIF
	ELIF dmVarsPassed.stiVehSafeStatus =  SHAPETEST_STATUS_NONEXISTENT
		PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_SHAPETESTED_VEHICLE_AREA_SAFE] - Shape Test vStart: ", vStart, "  vEnd: ", vEnd, " fWidth: ", fWidth)
		VEHICLE_INDEX viVeh
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niVehicle[iVeh])
			viVeh = NET_TO_VEH(serverFMMCPassed.niVehicle[iVeh])
		ENDIF
		dmVarsPassed.stiVehSafe = START_SHAPE_TEST_CAPSULE(vStart, vEnd, fWidth, SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED, viVeh, SCRIPT_SHAPETEST_OPTION_DEFAULT)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_RESPAWN_DM_VEH_NOW_SAFE(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, INT iVeh, SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niVehicle[iVeh])
		VEHICLE_INDEX viVeh
		viVeh = NET_TO_VEH(serverFMMCPassed.niVehicle[iVeh])
		
		IF NOT IS_ENTITY_ALIVE(viVeh)
			viVeh = NULL
		ENDIF
				
		IF NOT IS_SHAPETESTED_VEHICLE_AREA_SAFE(iVeh, dmVarsPassed, serverFMMCPassed)
			PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_RESPAWN_DM_VEH_NOW_SAFE] - iVeh: ", iVeh, " the area is occupied by something. Not Safe.")
			RETURN FALSE	
		ENDIF
	ENDIF
	
	PRINTLN("[LM][DM_ENTITY_RESPAWN][IS_RESPAWN_DM_VEH_NOW_SAFE] - iVeh: ", iVeh, " The area is now safe, returning true.")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL RESPAWN_DM_VEH(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, INT iVeh, ServerBroadcastData& serverBDPassed)

	VECTOR vSpawnPos = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].vPos
	FLOAT fSpawnHead = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead
	MODEL_NAMES vehModel = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].mn
	
	VECTOR vFoundSpawnPos
	FLOAT fFoundSpawnHeading
	SPAWN_SEARCH_PARAMS sSpawnSearchParams
	IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(vSpawnPos, 40, vFoundSpawnPos, fFoundSpawnHeading, sSpawnSearchParams)
		vSpawnPos = vFoundSpawnPos
		fSpawnHead = fFoundSpawnHeading
		PRINTLN("[DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Found a position: ", vFoundSpawnPos, " and a heading: ", fFoundSpawnHeading)
	ENDIF
	
	IF serverBDPassed.iSpawnAreaVeh != iVeh
	AND serverBDPassed.iSpawnAreaVeh > -1
		PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Another vehicle is currently respawning, so bailing on this one for now..")
		RETURN FALSE
	ENDIF
	
	IF serverBDPassed.iSpawnAreaVeh = -1
		serverBDPassed.iSpawnAreaVeh = iVeh
		PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Assigning Spawn Area Veh LINK.........")
	ENDIF
	
	IF NOT NETWORK_ENTITY_AREA_DOES_EXIST(serverBDPassed.iSpawnArea)
		VECTOR vStart, vEnd, vMax, vMin
		FLOAT fWidth
		GET_MODEL_DIMENSIONS(vehModel, vMin, vMax)
		IF IS_THIS_MODEL_A_HELI(vehModel)
			fWidth = vMax.x*0.5
			vMin.x = 0	
			vMax.x = 0
			vMin.z = vMin.z*0.75
			vMax.z = vMax.z*0.75
		ELSE
			fWidth = vMax.x*0.8
			vMin.x = 0	
			vMax.x = 0
			vMin.z = vMin.z*0.8
			vMax.z = vMax.z*0.8
		ENDIF
		vStart = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMin)
		vEnd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vSpawnPos, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fHead, vMax)		
		
		serverBDPassed.iSpawnArea = NETWORK_ADD_ENTITY_ANGLED_AREA(vStart, vEnd, fWidth)
		PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Assigning Spawn Area to Check. iSpawnArea: ", serverBDPassed.iSpawnArea)
		RETURN FALSE
	ELSE
		IF NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(serverBDPassed.iSpawnArea)
			IF NETWORK_ENTITY_AREA_IS_OCCUPIED(serverBDPassed.iSpawnArea)
				PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Area is occupied. Setting iVehicleRespawnGhostedBitset.")
				SET_BIT(serverBDPassed.iVehicleRespawnGhostedBitset, iVeh)
			ENDIF
		ELSE
			PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Waiting for network to respond.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	REQUEST_MODEL(vehModel)
	IF NOT HAS_MODEL_LOADED(vehModel)
		PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " || Waiting to load model ", GET_MODEL_NAME_FOR_DEBUG(vehModel))
		RETURN FALSE
	ENDIF
	
	PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iVeh: ", iVeh, " Creating/Respawning the vehicle now!")
	
	IF CREATE_NET_VEHICLE(serverFMMCPassed.niVehicle[iVeh], vehModel, vSpawnPos, fSpawnHead, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, SHOULD_VEHICLE_CREATION_IGNORE_GROUND_CHECK(iVeh, vSpawnPos, -1))
		VEHICLE_INDEX viVeh
		viVeh = NET_TO_VEH(serverFMMCPassed.niVehicle[iVeh])
		
		IF IS_VEHICLE_MODEL(viVeh, MULE)
			FMMC_SET_THIS_VEHICLE_EXTRAS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery)
		ELSE
			FMMC_SET_THIS_VEHICLE_EXTRAS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitSet)
		ENDIF
		
		FMMC_SET_THIS_VEHICLE_COLOURS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColour,g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iLivery, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iColourCombination, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour2, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iColour3, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].fEnvEff)
				
		SET_ENTITY_VISIBLE(viVeh, TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
		
		IF DOES_VEHICLE_HAVE_ANY_WEAPON_MODS(vehModel)
			SET_VEHICLE_WEAPON_MODS(viVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModTurret, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModArmour, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVehBitsetFive, iVeh, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModAirCounter, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModExhaust, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModBombBay, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iVModSpoiler)
		ENDIF
		
		SET_UP_FMMC_VEH(viVeh, iVeh)
		
		IF NETWORK_ENTITY_AREA_DOES_EXIST(serverBDPassed.iSpawnArea)	
			PRINTLN("[LM][DM_ENTITY_RESPAWN][RESPAWN_DM_VEH] - iSpawnArea: ", serverBDPassed.iSpawnArea, " Removing Spawn Area iVeh: ", iVeh)
			NETWORK_REMOVE_ENTITY_AREA(serverBDPassed.iSpawnArea)
		ENDIF
		serverBDPassed.iSpawnAreaVeh = -1
			
		IF IS_BIT_SET(serverBDPassed.iVehicleRespawnGhostedBitset, iVeh)
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(viVeh, FALSE)
			SET_ENTITY_ALPHA(viVeh, 125, FALSE)
			PRINTLN("RESPAWN_DM_VEH - Fading out veh ", iVeh, " || NetID: ", NETWORK_ENTITY_GET_OBJECT_ID(viVeh), " (1)")
			FREEZE_ENTITY_POSITION(viVeh, TRUE)
			SET_NETWORK_ID_CAN_MIGRATE(serverFMMCPassed.niVehicle[iVeh], FALSE)
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(vehModel)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_DM_VEH_TO_RESPAWN(ServerBroadcastData& serverBDPassed, INT iVeh, INT iReason)
	IF NOT IS_BIT_SET(serverBDPassed.iVehicleRespawnBitset, iVeh)
		PRINTLN("[LM][DM_ENTITY_RESPAWN][SET_DM_VEH_TO_RESPAWN] - Flagging iVeh: ", iVeh, " To respawn due to iReason: ", iReason)
		UNUSED_PARAMETER(iReason)
		SET_BIT(serverBDPassed.iVehicleRespawnBitset, iVeh)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DM_VEH_RESPAWN(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, ServerBroadcastData& serverBDPassed, INT iVeh)
	// Flagged to respawn vehicle
	IF IS_BIT_SET(serverBDPassed.iVehicleRespawnBitset, iVeh)
		PRINTLN("[SHOULD_DM_VEH_RESPAWN] - iVehicleRespawnBitset already set for veh ", iVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(serverBDPassed.iVehicleSpawnedBitset, iVeh)
	AND IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_DM_VEHICLES_CAN_RESPAWN)
		// Check if we should set Respawn Flag	
		NETWORK_INDEX niVeh = serverFMMCPassed.niVehicle[iVeh]
		IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
			VEHICLE_INDEX viVeh = NET_TO_VEH(niVeh)
			
			// Destroyed/Stuck/Undriveable
			IF NOT IS_VEHICLE_DRIVEABLE(viVeh)
			OR IS_ENTITY_DEAD(viVeh)
			OR (IS_ENTITY_SUBMERGED_IN_WATER(viVeh, FALSE, WATER_SUBMERGED_LEVEL) AND IS_THIS_MODEL_A_BIKE(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].mn))
			OR GET_ENTITY_HEALTH(viVeh) <= 0
				PRINTLN("[LM][url:bugstar:5632342][SHOULD_DM_VEH_RESPAWN] - iVeh: ", iVeh, " NetID: ", NETWORK_ENTITY_GET_OBJECT_ID(viVeh), " Vehicle is dead/undriveable.")
				SET_DM_VEH_TO_RESPAWN(serverBDPassed, iVeh, 0)
				RESET_NET_TIMER(serverBDPassed.td_VehCleanupTimer[iVeh])
				PRINTLN("SHOULD_DM_VEH_RESPAWN - Veh ", iVeh, " is dead or undrivable!")
			ELSE
				PRINTLN("[LM][url:bugstar:5632342][SHOULD_DM_VEH_RESPAWN] - iVeh: ", iVeh, " NetID: ", NETWORK_ENTITY_GET_OBJECT_ID(viVeh), " Vehicle is currently okay.")
			ENDIF			
		ELSE
			// Someone/Something cleaned this up.
			SET_DM_VEH_TO_RESPAWN(serverBDPassed, iVeh, 1)
			RESET_NET_TIMER(serverBDPassed.td_VehCleanupTimer[iVeh])
			PRINTLN("[SHOULD_DM_VEH_RESPAWN] - Veh ", iVeh, " doesn't exist")
		ENDIF
	ELSE
		PRINTLN("[SHOULD_DM_VEH_RESPAWN] - IS_BIT_SET(serverBDPassed.iVehicleSpawnedBitset, ", iVeh, ") = ", IS_BIT_SET(serverBDPassed.iVehicleSpawnedBitset, iVeh))
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iveh].iVehBitsetEight, ciFMMC_VEHICLE8_DM_VEHICLES_CAN_RESPAWN)
			PRINTLN("[SHOULD_DM_VEH_RESPAWN] - NOT ciFMMC_VEHICLE8_DM_VEHICLES_CAN_RESPAWN)")
		ENDIF
	ENDIF
	
	PRINTLN("SHOULD_DM_VEH_RESPAWN - Returning FALSE for veh ", iVeh)
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEANED_UP_DM_VEH(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, ServerBroadcastData& serverBDPassed, INT iVeh)
	
	IF HAS_NET_TIMER_STARTED(serverBDPassed.td_VehCleanupTimer[iVeh])
		IF HAS_NET_TIMER_EXPIRED(serverBDPassed.td_VehCleanupTimer[iVeh], 5000)	
			
		ELSE
			PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Waiting for timer on vehicle ", iVeh)
			RETURN FALSE
		ENDIF
	ELSE
		PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Starting Timer.")
		REINIT_NET_TIMER(serverBDPassed.td_VehCleanupTimer[iVeh])
		
		RETURN FALSE
	ENDIF
	
	NETWORK_INDEX niVeh = serverFMMCPassed.niVehicle[iVeh]	
	IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(niVeh)
		VEHICLE_INDEX viVeh = NET_TO_VEH(niVeh)
	
		IF IS_VEHICLE_EMPTY(viVeh, DEFAULT, DEFAULT, DEFAULT, TRUE)
			IF HAS_NET_TIMER_STARTED(serverBDPassed.td_VehCleanupTimer[iVeh])
				IF HAS_NET_TIMER_EXPIRED(serverBDPassed.td_VehCleanupTimer[iVeh], 5000)			
					IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(niVeh)
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(niVeh)
						PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Requesting control of Net ID netVehicleToCleanUp")
					ELSE										
						SET_NETWORK_ID_CAN_MIGRATE(niVeh, FALSE) // So we don't lose control, we're cleaning up this crap anyway.
						
						IF NOT IS_ENTITY_VISIBLE_TO_SCRIPT(viVeh)
							IF HAS_NET_TIMER_EXPIRED(serverBDPassed.td_VehCleanupTimer[iVeh], 10000)
								PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Deleting netVehicleToCleanUp")	
								IF DOES_ENTITY_EXIST(viVeh)
								AND IS_ENTITY_ON_FIRE(viVeh)
									STOP_ENTITY_FIRE(viVeh)
								ENDIF
								BROADCAST_FMMC_CLEAR_FIRE_EVENT(GET_ENTITY_COORDS(viVeh), 10.0)
								NETWORK_FADE_OUT_ENTITY(viVeh, TRUE, TRUE)
								SET_ENTITY_COLLISION(viVeh, FALSE)
								SET_ENTITY_VISIBLE(viVeh, FALSE)
								DELETE_NET_ID(niVeh)
								
							ELSE
								PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Waiting for Timer.")
							ENDIF
						ELSE
							PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - NETWORK_FADE_OUT_ENTITY netVehicleToCleanUp")
							IF DOES_ENTITY_EXIST(viVeh)
							AND IS_ENTITY_ON_FIRE(viVeh)
								STOP_ENTITY_FIRE(viVeh)
							ENDIF
							
							BROADCAST_FMMC_CLEAR_FIRE_EVENT(GET_ENTITY_COORDS(viVeh), 10.0)
							NETWORK_FADE_OUT_ENTITY(viVeh, TRUE, TRUE)
						ENDIF
					ENDIF			
				ELSE
					PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - Waiting for Timer, or we are respawning.")
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[LM][DM_ENTITY_RESPAWN][CLEANED_UP_DM_VEH] - iVeh: ", iVeh, " PROCESS_FORCE_DELETE_VEHICLE_INDEX - NETWORK_FADE_OUT_ENTITY Vehicle is not empty...")
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_VEHICLE_LOCK_STATE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_PLAYER_SPECTATOR(playerBDPassed, LocalPlayer, iLocalPart)
		EXIT
	ENDIF
	
	IF bLocalPlayerPedOK
	AND IS_ENTITY_ALIVE(dmVarsPassed.dmVehIndex)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		OR NOT CONTENT_IS_USING_ARENA()
			MAINTAIN_LOCK_VEH(dmVarsPassed.dmVehIndex, dmVarsPassed.bLockVeh, DEFAULT, TRUE)
		ELSE
			IF CONTENT_IS_USING_ARENA()
				PRINTLN("MAINTAIN_VEHICLE_LOCK_STATE - Outside of our Locked Vehicle Temporarily unlocking.")
				SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(dmVarsPassed.dmVehIndex, LocalPlayer, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_DM_VEHICLE_INITIALIZATIONS(PlayerBroadcastData& playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)			
	
	IF IS_BIT_SET(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_TRANSFORM_VEH_INIT_REQUESTED)
		CLEAR_BIT(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_TRANSFORM_VEH_INIT_REQUESTED)
		IF NOT IS_DM_PLAYER_DEAD_GLOBAL_SET()
			PRINTLN("MAINTAIN_DM_VEHICLE_INITIALIZATIONS - Setting up transform vehicle.")
			CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_VDM_SET_UP_VEH)
		ENDIF
	ENDIF

	IF IS_DM_PLAYER_DEAD_GLOBAL_SET()
	OR NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_VDM_SET_UP_VEH)
		IF IS_SCREEN_FADED_IN()
			SET_UP_SPAWN_VEHICLE_COLOURS(playerBDPassed, dmVarsPassed)
			SET_UP_VDM_VEHICLE(playerBDPassed, dmVarsPassed)
			
			SET_DEAD_IN_DM_GLOBAL(FALSE)
			SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_VDM_SET_UP_VEH)
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_BULLETPROOF_TYRES()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_Enable_Bulletproof_Tyres)
	AND IS_ENTITY_ALIVE(LocalPlayerPed)
		VEHICLE_INDEX viVeh

		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)

			viVeh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)

			IF DOES_ENTITY_EXIST(viVeh)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)

				IF GET_VEHICLE_TYRES_CAN_BURST(viVeh)
					PRINTLN("[MJL][PROCESS_BULLETPROOF_TYRES] Deathmatch - Setting player's vehicle tyres cannot burst. For vehicle ID: ", iLocalPart)
					SET_VEHICLE_TYRES_CAN_BURST(viVeh, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_ExplodeAtZeroBodyHealth)
	AND NOT g_bMissionEnding
	AND NOT g_bCelebrationScreenIsActive
		PED_INDEX playerPed = LocalPlayerPed
		VEHICLE_INDEX playerVeh
		IF bLocalPlayerPedOK
		AND DOES_ENTITY_EXIST(playerPed)
		AND IS_PED_IN_ANY_VEHICLE(playerPed)
		
			playerVeh = GET_PLAYERS_LAST_VEHICLE()
			
			IF NOT DOES_ENTITY_EXIST(playerVeh)
				EXIT
			ENDIF
			
			IF IS_ENTITY_DEAD(playerVeh)
			OR GET_VEHICLE_BODY_HEALTH(playerVeh) <= 0
			OR NOT IS_VEHICLE_DRIVEABLE(playerVeh)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(playerVeh)
					PRINTLN("PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH - Exploding Vehicle")
					IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
						SET_PLAYER_INVINCIBLE(LocalPlayer, FALSE)
						SET_ENTITY_PROOFS(playerPed, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					ENDIF
					
					SET_ENTITY_INVINCIBLE(playerVeh, FALSE)
					IF g_iLastDamagerPlayer != -1
						MPGlobals.g_KillStrip.piOverRideKillerPlayer = INT_TO_NATIVE(PLAYER_INDEX, g_iLastDamagerPlayer)
						PRINTLN("PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH - MPGlobals.g_KillStrip.piOverRideKillerPlayer updated to ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.piOverRideKillerPlayer))
						NETWORK_EXPLODE_VEHICLE(playerVeh, TRUE, FALSE, g_iLastDamagerPlayer)
						PRINTLN("PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH - Exploded using g_iLastDamagerPlayer")
					ELSE
						NETWORK_EXPLODE_VEHICLE(playerVeh, TRUE, TRUE)
						PRINTLN("PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH - Exploded using keep damage entity")
					ENDIF
				ELSE
					PRINTLN("PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH - No Control of Entity - Requesting Control.")
					NETWORK_REQUEST_CONTROL_OF_ENTITY(playerVeh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SET_VEHICLE_HANDBRAKE_STATE(BOOL bOn)
	VEHICLE_INDEX vehPlayer

	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)

		IF NETWORK_HAS_CONTROL_OF_ENTITY(vehPlayer)
			IF bOn 
				PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, ON")
			ELSE
				PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, OFF")
			ENDIF

			SET_VEHICLE_HANDBRAKE(vehPlayer, bOn)
			RETURN TRUE
		ELSE
			PRINTLN("LIMIT RACE CONROLS - [SET_VEHICLE_HANDBRAKE_ON] VEHICLE_HANDBRAKE, NETWORK_HAS_CONTROL_OF_ENTITY")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL OK_TO_DO_VEHICLE_BOOST()

	IF IS_BIT_SET(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
		RETURN FALSE
	ENDIF

	IF IS_RACE_PASSENGER()
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
	AND HAS_NET_TIMER_EXPIRED(g_FMMC_STRUCT.stBoostTimer, 1000)
		RESET_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
		RETURN FALSE
	ENDIF
	
	//RETURN TRUE
	// 2540514 - Always return false - stripping out this functionality for Lowrider 
	RETURN TRUE
ENDFUNC

PROC DO_VEH_BOOST()
	IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
		IF OK_TO_DO_VEHICLE_BOOST()
			PRINTLN("[JS] DO_VEH_BOOST - Waiting for input")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE) 
				PRINTLN(">>>>>>>>>>>> OK_TO_DO_VEHICLE_BOOST, DO_VEH_BOOST, START_VEHICLE_BOOST")
				START_VEHICLE_BOOST(0.5)
				ANIMPOSTFX_PLAY("RaceTurbo", 0, FALSE)
				SET_BIT(g_FMMC_STRUCT.bsRaceMission, ciDidSpeedBoost)
				INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_TURBO_STARTS_IN_RACE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(BOOL bBlockCam = TRUE, BOOL bAllowRev = FALSE, BOOL bAllowRadio = FALSE , BOOL bHandBrakeOn = TRUE, BOOL bAllowHydro = FALSE)
	DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
	IF bBlockCam
		DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
	    PRINTLN("LIMIT RACE CONROLS - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - DISABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)")
	ELSE
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_LOOK_RIGHT)
	ENDIF
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ROOF)	
	IF bAllowRadio
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
		PRINTLN("LIMIT RACE CONROLS - bAllowRadio")
	ENDIF
	IF bAllowRev
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)	
		PRINTLN("LIMIT RACE CONROLS - bAllowRev")
	ENDIF
	IF bAllowHydro
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_TOGGLE)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_DOWN)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_LEFT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_RIGHT)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UP)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HYDRAULICS_CONTROL_LR)
		PRINTLN("LIMIT RACE CONROLS - bAllowHydro")
	ENDIF
	
	ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

	SET_VEHICLE_HANDBRAKE_STATE(bHandBrakeOn)

ENDPROC

FUNC FLOAT CALCULATE_NEW_HEADING(FLOAT fCurrentHeading, FLOAT fheadingChange)
	FLOAT newHeading
	newHeading = fCurrentHeading + fheadingChange
	
	IF newHeading > 360
		newHeading -= 360.0
	ENDIF
	IF newHeading < 0
		newHeading += 360.0
	ENDIF
	
	RETURN newHeading
ENDFUNC

// (1383756)
PROC HANDLE_TANK_TURRETS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	FLOAT fHeading 
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		IF GET_ENTITY_MODEL(NET_TO_ENT(dmVarsPassed.dmVehID)) = RHINO
			IF IS_NET_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehID)
				IF bLocalPlayerOK
					fHeading = GET_ENTITY_HEADING(NET_TO_ENT(dmVarsPassed.dmVehID))
					SET_VEHICLE_TANK_TURRET_POSITION(NET_TO_VEH(dmVarsPassed.dmVehID), CALCULATE_NEW_HEADING(fHeading,180), TRUE)
					SET_ENTITY_HEALTH(NET_TO_VEH(dmVarsPassed.dmVehID),1000)
					PRINTLN("HANDLE_TANK_TURRETS, fHeading = ", fHeading)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_VEH_DM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], PLAYER_MODIFIER_DATA &sPlayerModifierData)
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, sPlayerModifierData)
		SET_PLAYER_CAN_BE_KNOCKED_OFF_VEHICLE(KNOCKOFFVEHICLE_HARD)
		SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, GET_DM_VEHICLE_TO_SPAWN(playerBDPassed, sPlayerModifierData), TRUE, FALSE)
	ENDIF
ENDPROC

PROC PROCESS_GENERAL_VDM()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_DisableGTARaceVehicleExit)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
	ENDIF
	
ENDPROC

