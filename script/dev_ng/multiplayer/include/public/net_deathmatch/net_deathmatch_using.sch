
USING "globals.sch"
USING "freemode_header.sch"
USING "screens_header.sch"
USING "net_spectator_cam.sch"
USING "net_common_functions.sch"
USING "fmmc_cloud_loader.sch"
USING "fmmc_mp_setup_mission.sch"
USING "Thumbs.sch"
USING "FMMC_HEADER.sch"
USING "Cheat_Handler.sch"
USING "FM_Playlist_Header.sch"
USING "net_bru_box.sch"
USING "FMMC_Restart_Header.sch"
USING "socialclub_leaderboard.sch"
USING "net_power_ups.sch"
USING "net_mission_control_public.sch"
USING "net_car_spikes.sch"
USING "net_car_rockets.sch"
USING "social_feed_controller.sch"
USING "commands_money.sch"
USING "net_interactions.sch"
USING "net_dm_objectives.sch"
USING "FMMC_next_job_screen.sch"
USING "net_celebration_screen.sch"
USING "net_elo.sch"
USING "net_cash_transactions.sch"
USING "net_gang_boss.sch"
USING "freemode_events_header.sch" // for HANDLE_MAP_EXITING_PLAYERS

USING "net_arena_wars_career.sch"
USING "net_ArenaAnnouncer_Header.sch"

USING "FMMC_PowerUps.sch"
USING "FMMC_Player_Modifiers.sch"

//Local Player State Caching
BOOL bIsLocalPlayerHost
INT iLocalPart
PLAYER_INDEX LocalPlayer
PED_INDEX LocalPlayerPed
BOOL bLocalPlayerOK
BOOL bLocalPlayerPedOK
VECTOR vLocalPlayerPosition
BOOL bIsSCTV
BOOL bIsAnySpectator

INT iPartToUse
PLAYER_INDEX PlayerToUse
PED_INDEX PlayerPedToUse
BOOL bPlayerPedToUseOK
 
CONST_INT TEAM_WON_REWARD_FINISH_POS			2
CONST_INT TEAM_LOST_REWARD_FINISH_POS			4
CONST_INT TEAM_2ND_PLACE_FINISH_POS				4
CONST_INT TEAM_3RD_PLACE_FINISH_POS				5
CONST_INT TEAM_4TH_PLACE_FINISH_POS				6

CONST_INT WINNER_BONUS							100
CONST_INT NUM_LOBBY_DM_SPAWN_POINTS				32
CONST_INT MIN_NUM_DM_PLAYERS					2
CONST_INT MIN_NUM_DM_TEAMS						2
CONST_INT POINTS_FOR_KILL						1
CONST_INT NUM_DM_PICKUPS						15
CONST_INT READY_UP_TIME	 						3000
CONST_INT ASSIST_RESET_TIME						30000
CONST_INT END_DEATHMATCH_TIME_LIMIT				60000
CONST_INT FLASH_TIME							3000
CONST_INT TEAM_DEATHMATCH_SCORES_TIED			3
CONST_INT IDLE_TIMEOUT							60000
CONST_INT DOUBLE_DAMAGE_TIME					60000
CONST_INT MINIMUM_TIME_BEFORE_ONE_PLAYER_KICK	10000
CONST_INT IMPROMPTU_DISTANCE_LIMIT				1000
CONST_INT IMPROMPTU_DISTANCE_WARNING			800
CONST_INT IMPROMPTU_DISTANCE_CLOSE				750
CONST_INT DEATH_STREAK_MIN						3
CONST_INT INITIAL_SPAWN_TIMEOUT					100000

// Game States
CONST_INT GAME_STATE_INI 						0
CONST_INT GAME_STATE_RUNNING					1
CONST_INT GAME_STATE_LEAVE						2
CONST_INT GAME_STATE_END						3

// Leaderboard
CONST_INT LEADERBOARD_STAGE_SCALEFORM			0	
CONST_INT LEADERBOARD_STAGE_MAKE_SAFE			1
CONST_INT LEADERBOARD_STAGE_CHALLENGES			2
CONST_INT LEADERBOARD_STAGE_AWARD				3
//////////////////////////////      
CONST_INT LOAD_MISSION_STAGE_SETUP				0
CONST_INT LOAD_MISSION_STAGE_LOAD				1
CONST_INT LOAD_MISSION_STAGE_CHOOSE				2
CONST_INT LOAD_MISSION_STAGE_FINISH				3
//////////////////////////////   

CONST_FLOAT LEADERBOARD_PLAYER_Y_SPACE 			0.1955
CONST_FLOAT LEADERBOARD_SELECTION_X_SPACE 		0.0895

// -----------------------------------		BITSETS 
////////////////////////////// Server			
CONST_INT SERV_BITSET_EVERYONE_READY				0
CONST_INT SERV_BITSET_START_READY_TIMER				1
CONST_INT SERV_BITSET_TEAMS_COUNTED					2
CONST_INT SERV_BITSET_MORE_PLAYERS_TIMER			3
CONST_INT SERV_BITSET_DEATHMATCH_STARTED			4
CONST_INT SERV_BITSET_TARGET_SCORE_REACHED			5
CONST_INT SERV_BITSET_EVERYONE_ON_LBD				6
CONST_INT SERV_BITSET_ONE_PLAYER_KICK				7
CONST_INT SERV_BITSET_START_FINISHING_UP_TEAM_SORT	8	
CONST_INT SERV_BITSET_TEAM_SORT_CLOSED				9
CONST_INT SERV_BITSET_FINISHED_WINNING_TEAMS		10	
CONST_INT SERV_BITSET_LBD_CLOSED					11
CONST_INT SERV_BITSET_START_FINISHING_UP_LBD_SORT	12
CONST_INT SERV_BITSET_SERVER_SAYS_DM_ENDED			13
CONST_INT SERV_BITSET_ONE_KILL_LEFT					14
CONST_INT SERV_BITSET_30_SECONDS_LEFT				15
CONST_INT SERV_BITSET_IMPROMPTU_CLOSE				16
CONST_INT SERV_BITSET_MADE_ENTITIES					17
CONST_INT SERV_BITSET_COUNTED_PICKUPS				18
CONST_INT SERV_BITSET_ALL_PLAYERS_ON_BETTING		19
CONST_INT SERV_BITSET_TIMED_OUT						20
CONST_INT SERV_BITSET_LBD_STARTING_FIRST_UPDATE		21
CONST_INT SERV_BITSET_LBD_FINISHED_FIRST_UPDATE		22
CONST_INT SERV_BITSET_TEAM_CPS_DONE					23
CONST_INT SERV_BITSET_GRABBED_IMP_COORD				24
CONST_INT SERV_BITSET_EVERYONE_FINISHED				25
CONST_INT SERV_BITSET_40_SECONDS_LEFT				26
CONST_INT SERV_BITSET_IMPROMPTU_OK_DISTANCE_CHECK	27
CONST_INT SERV_BITSET_ILLEGAL_UGC_DM				28
CONST_INT SERV_BITSET_STORED_FINISH_POSITIONS		29
CONST_INT SERV_BITSET_BOSSVBOSS_KILLED				30
CONST_INT SERV_BITSET_BOSSVBOSS_BOSS_QUIT			31

CONST_INT SERV_BITSET2_KILL_ACTIVE_BOSS				0
CONST_INT SERV_BITSET2_RAN_OUT_OF_LIVES				1
CONST_INT SERV_BITSET2_CELEB_VEH					2
CONST_INT SERV_BITSET2_STARTED_COUNTDOWN			3
CONST_INT SERV_BITSET2_FINISHED_COUNTDOWN			4
CONST_INT SERV_BITSET2_35_SECONDS_LEFT				5
CONST_INT SERV_BITSET2_TEAM_SCORES_WIPED			6
CONST_INT SERV_BITSET2_CORONA_READY					7
CONST_INT SERV_BITSET2_ROUNDS_LBD_DO				8
CONST_INT SERV_BITSET2_SHRINKING_BOUNDS_START		9
CONST_INT SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND		10

////////////////////////////// Client
CONST_INT CLIENT_BITSET_PINNED_INTERIOR					0
CONST_INT CLIENT_BITSET_GOT_TEAM						1
CONST_INT CLIENT_BITSET_SAFE_TO_RESTART					2
CONST_INT CLIENT_BITSET_JOINED_LATE						3
CONST_INT CLIENT_BITSET_IDLE							4	
CONST_INT CLIENT_BITSET_JOINED_AS_SPECTATOR				5	
CONST_INT CLIENT_BITSET_PLAYER_QUIT_JOB					6
CONST_INT CLIENT_BITSET_ENTERED_IMPROMPTU				7
CONST_INT CLIENT_BITSET_SENT_TO_SPECTATOR				8
CONST_INT CLIENT_BITSET_RAN_OUT_OF_LIVES				9
CONST_INT CLIENT_BITSET_USING_PERSONAL_VEH				10
CONST_INT CLIENT_BITSET_ROAMING_SPECTATOR				11
CONST_INT CLIENT_BITSET_EXPLODED_FROM_BEING_TAGGED		12
CONST_INT CLIENT_BITSET_ELIMINATED_FROM_FALLING_OUT_OF_VEH	13

////////////////////////////// NonBroadcast
CONST_INT NONBD_BITSET_SEEN_DPAD_LEADERBOARD	0
CONST_INT NONBD_BITSET_NEARLY_FINISHED_AUDIO	1
CONST_INT NONBD_BITSET_FINISHING_UP				2		
CONST_INT NONBD_BITSET_WRITE_LBD_STATS			3
CONST_INT NONBD_BITSET_STATS_WRITTEN			4
CONST_INT NONBD_BITSET_TRIANGLE_WARP			5	
CONST_INT NONBD_BITSET_DOUBLE_DAMAGE_RECEIVED	6
CONST_INT NONBD_BITSET_PLAYER_RESPAWNING		7	
CONST_INT NONBD_BITSET_DOUBLE_DAMAGE_GIVEN		8
CONST_INT NONBD_BITSET_HIDE_BLIPS				9
CONST_INT NONBD_BITSET_ALL_BLIPS_ON				10
CONST_INT NONBD_BITSET_THUMB_VOTE_SAVED			11
CONST_INT NONBD_BITSET_THUMBS_DONE				12			
CONST_INT NONBD_BITSET_START_SCORE_TIMER        13
CONST_INT NONBD_BITSET_KILLED_AUDIO				14
CONST_INT NONBD_BITSET_KILLED_SCORE				15
CONST_INT NONBD_BITSET_STARTED_SCORE			16
CONST_INT NONBD_BITSET_MOVED_TO_FIX_DEAD_PEOPLE	17	
CONST_INT NONBD_BITSET_POWER_PLAYER_STAT		18
CONST_INT NONBD_BITSET_SPECTATOR_CHAT			19
CONST_INT NONBD_BITSET_SET_ZOOM_ONCE			20
CONST_INT NONBD_BITSET_USING_TEAM_SPAWNS		21
CONST_INT NONBD_BITSET_ONE_KILL_AUDIO			22
CONST_INT NONBD_BITSET_READ_ELO					23
CONST_INT NONBD_BITSET_DO_ZOOM					24
CONST_INT NONBD_BITSET_MUTED_RADIOS				25
CONST_INT NONBD_BITSET_DEATH_STREAK				26
CONST_INT NONBD_BITSET_TARGET_PRIORITY			27
CONST_INT NONBD_BITSET_ELO_DEDUCTION_DONE		28
CONST_INT NONBD_BITSET_ELO_AVE_GRABBED			29
CONST_INT NONBD_BITSET_END_LEGIT_STAT			30
CONST_INT NONBD_BITSET_LOSS_STAT				31

// INT iDmBools2
CONST_INT DM_BOOLS2_OUTRO_DONE						0
CONST_INT DM_BOOLS2_OUTRO_OK_TO_DO					1
CONST_INT DM_BOOLS2_END_STATS						2
CONST_INT DM_BOOLS2_PLAYING_MUSIC_START				3
CONST_INT DM_BOOLS2_PLAYING_MUSIC_MID				4
CONST_INT DM_BOOLS2_PASSTHEBOMB_INITIALISED			5
CONST_INT DM_CELEB_GRABBED_VEH						6
CONST_INT DM_BOOL2_DISPLACEMENT_BLIP_SET			7
CONST_INT DM_BOOL2_HAS_AIRHORN_PLAYED				8
CONST_INT DM_BOOL2_DISPLAYED_INTRO_SHARD			9
CONST_INT DM_BOOL2_HALF_PLAYERS_DM					10
CONST_INT DM_BOOL2_ARENA_AUDIO_TRACK_PLAYED			11
CONST_INT DM_BOOL2_CLEAR_VEHICLE_MINES_DIABLE		12
CONST_INT DM_BOOL2_PRE_COUNTDOWN_35_SEC_LEFT		13
CONST_INT DM_BOOL2_SET_TIME_FOR_ARENA				14
CONST_INT DM_BOOL2_BOMB_CARRIER_MODE_STARTED		15
CONST_INT DM_BOOL2_PLACED_VEH_ON_GROUND_PROP		16
CONST_INT DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT	17
CONST_INT DM_BOOL2_DELAYED_PICKUPS_SPAWNED			18
CONST_INT DM_BOOL2_LAST_LIFE_SHARD_DONE				19
CONST_INT DM_BOOL2_VDM_SET_UP_VEH					20
CONST_INT DM_BOOL2_FRESH_POWERUP_TRANSFORM			21
CONST_INT DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT_2	22
CONST_INT DM_BOOL2_FINISH_EVENT_RECEIVED			23
CONST_INT DM_BOOL2_ROUNDS_DM_END_SET_UP				24
CONST_INT DM_BOOL2_ROUNDS_DM_CALL_SWAP_TEAM_LOGIC	25
CONST_INT DM_BOOL2_SAVED_ROUNDS_LBD_DATA			26
CONST_INT DM_BOOL2_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED	27

CONST_INT DM_BOOL3_DO_PLAYER_TOTALS_FOR_ROUNDS		0
CONST_INT WAS_NOT_THE_HOST							1
CONST_INT StoppedAllPostFxForSwitch					2

////////////////////////////// Text
CONST_INT TXT_BITSET_TOOK_LEAD_TICKER			0		
CONST_INT TXT_BITSET_LOST_LEAD_TICKER			1
CONST_INT TXT_BITSET_HELP						2
CONST_INT TXT_BITSET_READY						3
CONST_INT TXT_BITSET_FIRST_STRIKE_TICKER		4
CONST_INT TXT_BITSET_WINNING_KILL_TICKER		5
CONST_INT TXT_BITSET_BLIP_HELP					6
CONST_INT TXT_BITSET_VOICE_HELP					7
CONST_INT TXT_BITSET_TAG_HELP					8
CONST_INT TXT_BITSET_MVP_TICKER					9
CONST_INT TXT_BITSET_SUICIDE_VEH_DM				10
CONST_INT TXT_BITSET_POWER_PLAYER_HLP			11
CONST_INT TXT_BITSET_AIM_HELP					12
//FREE FREE FREE FREE FREE
CONST_INT TXT_BITSET_CHALLENGE_TIME				14
CONST_INT TXT_BITSET_SCORE_EXPLAIN				15
CONST_INT TXT_BITSET_LOSER						16		
CONST_INT TXT_BITSET_KOTH_VEH					17

////////////////////////////// Initial fade 
CONST_INT INITIAL_FADE_STAGE_START				0
CONST_INT INITIAL_FADE_STAGE_FADE_OUT 			1
CONST_INT INITIAL_FADE_STAGE_FADED_OUT		 	2
CONST_INT INITIAL_FADE_STAGE_OBJ				3
CONST_INT INITIAL_FADE_STAGE_FADE_IN 			4
CONST_INT INITIAL_CAMERA_TEAM_OVERVIEW			5
CONST_INT INITIAL_CAMERA_OVERVIEW				6
CONST_INT INITIAL_CAMERA_PLAYER_CUT				7
CONST_INT INITIAL_CAMERA_BLEND_OUT				8
CONST_INT INITIAL_FADE_STAGE_FADED_IN 			9
			
CONST_INT INITIAL_SPAWN__SETUP					0											
CONST_INT INITIAL_SPAWN__GET_SPAWN_POSITION		1
CONST_INT INITIAL_SPAWN__CREATE_VEH				2
CONST_INT INITIAL_SPAWN__ENTER_VEH				3
CONST_INT INITIAL_SPAWN__WARP_PLAYER			4
CONST_INT INITIAL_SPAWN__RETURN_TRUE			5

CONST_INT BVB_BS1_DONE_INTRO_HELP				0
CONST_INT BVB_BS1_DONE_OBJ						1
CONST_INT BVB_BS1_DONE_TELEMETRY				2
CONST_INT BVB_BS1_RESET_NO_SHUFFLE				3
CONST_INT BVB_BS1_DONE_INTRO_HELP2				4
CONST_INT BVB_BS1_RIVAL_BOSS_BLIP				5
CONST_INT BVB_BS1_DONE_CLEANUP					6
 // FEATURE_GANG_BOSS
CONST_INT BVB_BS1_CHECK_RIVAL_IS_BIKER			7
 // FEATURE_BIKER
////////////////////////////// Server Debug
#IF IS_DEBUG_BUILD
CONST_INT DEBUG_BITSET_TIME_SKIP			0
CONST_INT DEBUG_BITSET_DEBUGGED_TIME		1
CONST_INT DEBUG_BITSET_PRINT_TIME			2
CONST_INT DEBUG_BITSET_S_PASS				3
CONST_INT DEBUG_BITSET_F_FAIL				4
///////////////////////////////////////////

CONST_INT DAMAGE_BITSET_GRABBED_OLD_HEALTH	0
CONST_INT DAMAGE_BITSET_PRINT_TICKER		1

//For prints
TEXT_LABEL_31 tl31ScriptName				= "net_deathmatch.sch"

// For widgets
WIDGET_GROUP_ID widgetID

STRUCT DEBUG_DM_VARIABLES

	BOOL BdebugHelp

	BOOL bDebugUnlockVeh = FALSE
	
	BOOL bGrabbedFakeScore = FALSE
	INT iFakeScore

	BOOL bNeverTimeOut = FALSE
	BOOL bScrollSpew = FALSE
	BOOL bFakeDpadLeaderboard = FALSE
	BOOL bDrawDpadLeaderboard = FALSE
	BOOL bDrawDpadDDS = FALSE
	BOOL bPrintEndReason = FALSE
	
	// Dpad
	INT iDebugPos
	INT iDebugCash
	INT iDebugKills
	BOOL bCash
	
	BOOL bFakeBIGLeaderboard = FALSE
	BOOL bDrawBIGLeaderboard = FALSE
	BOOL bDebugBrucieBox
	
	BOOL bDebugDrawDDSTexture = FALSE
	BOOL bSameTeam = FALSE
	
	BOOL bDebugHideThumbRect = FALSE
	BOOL bDebugDrawThumbs = FALSE

	BOOL bHideDMHud = FALSE
	
	BOOL bRenderServerLeaderBoard
	
	INT iIncrementDelayDebug
	
	INT iSpawnCount = 0	
	INT iDebugSkipPickupCount = 0		// debug skip between pickups
	
	BOOL bPassTriggered
	BOOL bFailTriggered
	BOOL bDebugSwitch = FALSE
	BOOL bDontEndOnePlayer
	
	// NEW TEAM LBD
	INT iPlyerTeam[NUM_NETWORK_PLAYERS]
	INT iNumTeams = 8
	
	INT iWinningTeam = 0	
	INT iSecondTeam = 1
	INT iThirdTeam = 2
	INT iFourthTeam = 3
	INT iFifthTeam = 4
	INT iSixthTeam = 5
	INT iSeventhTeam = 6
	INT iLosingTeam = 7
	
	INT iTeamScore1 = 10
	INT iTeamScore2 = 20
	INT iTeamScore3 = 30
	INT iTeamScore4 = 40
	INT iTeamScore5 = 50
	INT iTeamScore6 = 60
	INT iTeamScore7 = 70
	INT iTeamScore8 = 80
	
	INT iDebugSlotCounter[MAX_NUM_DM_TEAMS]


	INT iRank = 10

	INT iScore
	INT iKills
	INT iDeaths
	FLOAT fKDRatio 
	INT iCash
	
	BOOL bWidthsGuide
	INT iNumColumns = 5
	FLOAT fRatio
	INT iVar1 =10 
	INT iVar2 = 20
	INT iVar3 = 30 
	INT iVar4 = 100
	INT iVar5 = 50
	INT iVar6 = 60
	INT iBadgeRank = 5
	
	INT iTeamWinCount = 2
	INT iTeam2Count = 2
	INT iTeam3Count = 2
	INT iTeam4Count = 2
	INT iTeam5Count = 2
	INT iTeam6Count = 2
	INT iTeam7Count = 2
	INT iTeamLoseCount  = 2
	
	INT iXPDuring 	= 0
	INT iXPEnd 		= 0
	INT iXPTotal	= 0
	INT iCashDuring = 0
	INT iCashEnd	= 0
	INT iCashTotal	= 0
	
	BOOL bDisableZoom
	
	TEXT_LABEL_63 tl63_FakeParticipantNames[NUM_NETWORK_PLAYERS]
	
	BOOL bDisablePTBTimer = FALSE
ENDSTRUCT

BOOL bSpewLeaderboardInfo = FALSE

BOOL bWdPauseGameTimer, bWdPauseGameTimerRAG
SCRIPT_TIMER tdPauseTimer1

BOOL bFreezeCelebration
BOOL bKeepLocalRacing 

#ENDIF

STRUCT TEAM_SORT_STRUCT
	INT 	iTeamScore
	INT 	iTeamDeaths
	FLOAT 	fTeamRatio 
	INT 	iTeamXP
	INT		iRemainingPlayers
ENDSTRUCT

CONST_INT LATE_TEAM_COUNT			0
CONST_INT LATE_TEAM_FIND_SMALLEST 	1
CONST_INT LATE_TEAM_ASSIGN			2	

CONST_INT DM_RESPAWN__FADE_OUT								0
CONST_INT DM_RESPAWN__DELETE_LAST_VEHICLE					1
CONST_INT DM_RESPAWN__POSITION_PLAYER						2	
CONST_INT DM_RESPAWN__FADE_IN								3
CONST_INT DM_RESPAWN__FINISH_UP								4

CONST_INT QUARTER_TIME 300000

CONST_INT MIN_STOLEN_DAMAGE 150

CONST_INT ciSYNC_RACE_COUNTDOWN_TIMER 3500

SCRIPT_TIMER timerExtraVehDamageForceHitting	// Me hitting someone
CONST_INT ciTimeExtraVehDamageToBeHittingAgain 1000

SCRIPT_TIMER tCampTimer
VECTOR vLastCachedPosition
INT iCampBlipExitRetry

ENUM DM_PROGRESS
	DM_WINNING 	= 0,
	DM_LOSING 	= 1,
	DM_DRAWING 	= 2,
	DM_NOT_USED = 3
ENDENUM

CONST_INT ILLEGAL_CHECK_UGC 		0
CONST_INT ILLEGAL_CHECK_SPAWNPOINTS 1
CONST_INT ILLEGAL_SET_BIT			2
CONST_INT ILLEGAL_EXIT				3

CONST_INT DM_AUDIO_RESTART_TIME 300000 //5 mINS

CONST_INT BRUCIE_BOX_TIMER 					60000
CONST_INT BRUCIE_STAGE_RESET_TIMER			0
CONST_INT BRUCIE_STAGE_GET_BOX_COORDS 		1
CONST_INT BRUCIE_STAGE_GET_SAFE_SPOT		2
CONST_INT BRUCIE_STAGE_BOX_WAIT_COLLECTION 	3

CONST_INT ci_DM_STUCK_TIME 20000
CONST_INT ci_DM_ROOF_TIME 12000

CONST_INT MIN_NUM_TEAM_SPAWNS 4

CONST_INT TEAM_1_SPAWN_POINTS 8
CONST_INT TEAM_2_SPAWN_POINTS 8
CONST_INT TEAM_3_SPAWN_POINTS 5
CONST_INT TEAM_4_SPAWN_POINTS 4

CONST_INT VEH_DM_OBJ_TIMER 	10000
CONST_INT VEH_DM_OBJ_TARGET 2

CONST_INT PERK_TIMEOUT 30000

CONST_INT BLIPS_ON_START_DM_DURATION	5000

CONST_INT DAMAGE_DOUBLE 2
CONST_INT DAMAGE_QUAD 	4

CONST_INT BLIP_REASON_POWER 	0
CONST_INT BLIP_REASON_DEATH 	1
CONST_INT BLIP_REASON_STREAK 	2
CONST_INT BLIP_REASON_PERK 		3
CONST_INT BLIP_REASON_START		4
CONST_INT BLIP_REASON_END		5

CONST_INT BOUNTY_REASON_IDLE 	0
CONST_INT BOUNTY_REASON_KS_MED	1
CONST_INT BOUNTY_REASON_KS_BIG	2

CONST_INT POWER_PLAYER_LEAD 3
CONST_FLOAT THIRTY_PERCENT	0.3


ENUM eFREEZE_STATE
	eFREEZE_STATE_NONE=0,
	eFREEZE_STATE_YES,
	eFREEZE_STATE_NO,
	eFREEZE_STATE_NOT_USED
ENDENUM

ENUM PASS_THE_BOMB_HUD_STATE
	PASS_THE_BOMB_HUD_OFF,
	PASS_THE_BOMB_HUD_LOADING,
	PASS_THE_BOMB_HUD_SETUP,
	PASS_THE_BOMB_HUD_RUNNING,
	PASS_THE_BOMB_HUD_CLEANUP
ENDENUM
PASS_THE_BOMB_HUD_STATE ePassTheBombHUDState

STRUCT DM_INTRO_COUNTDOWN_UI
	SCALEFORM_INDEX uiCountdown
	INT iBitFlags
	INT iFrameCount
	structTimer CountdownTimer
ENDSTRUCT

ENUM COUNTDOWN_UI_FLAGS_RACE
	CNTDWN_UI_SoundGo  		= BIT4,
	CNTDWN_UI2_Played_3 	= BIT0,
	CNTDWN_UI2_Played_2 	= BIT1,
	CNTDWN_UI2_Played_1 	= BIT2,
	CNTDWN_UI2_Played_Go	= BIT3
ENDENUM

STRUCT SPLIT_SESSION
	BOOL bSplitSession = FALSE
	SCRIPT_TIMER stRaceNetworkConnectionTimeoutTimer
	BOOL bRaceWaitForNetworkSessionEventForHostTimeout
	FLOAT fRaceNetworkConnectionTimeoutTally
	FLOAT fNumInSessionOnFirstRaceNetworkConnectionTimeoutEvent
ENDSTRUCT

//Celebration Screen
STRUCT CELEBRATION_STATS
	PLAYER_INDEX winningPlayer			//The index of the overall winner (or the top player from the winning team).
	INT iLocalPlayerCash				
	INT iLocalPlayerScore				//This is the score used to determine if this player beat a challenge for this mission. It may be a time value rather than score, but either can be put in here.
	INT iLocalPlayerKills
	INT iLocalPlayerDeaths
	INT iLocalPlayerHeadshots
	INT iLocalPlayerJobPoints			
	INT iLocalPlayerXP					
	INT iLocalPlayerBetWinnings			//How much money the player won/lost on bets
	INT iLocalPlayerPosition			//If in teams this is just the position of the player's team, otherwise it's the player's leaderboard position.	
	BOOL bIsDraw						//I don't know if you can draw a mission but there's a screen for it so just in case you can then this variable covers it.	
ENDSTRUCT

STRUCT SHARED_DM_VARIABLES
	SCRIPT_TIMER stCelebrationDelay
	TEXT_LABEL_63 tl23WinnerVehicle
	eFREEZE_STATE eFreeze = eFREEZE_STATE_NONE

	INT iDmBools2
	INT iDmBools3
	INT iSeaMine_HasExplodedBS
	
	INT iDlcWeaponBitSet
	INT iAverageOpponentELO
	INT iPickupsMade
	
	INT iPerkBarProgress

	WEAPON_TYPE wtWeaponToRemove = WEAPONTYPE_INVALID
	
	SCRIPT_TIMER stRewardPercentageTimer
	
	BOOL DID_PLAYER_CREATE_ACTIVITY // added by kevin to check rewards for player another users deathmatch and winning

//	INT iTagsBitSet
	SPECTATOR_DATA_STRUCT specData
	ELO_VARS eloVars
	
	INT iCurrentFX
	
	INT iBountyAssignedBitset
	
	INT iHighestPlayerScoreStored
	INT iHighestTeamScoreStored[MAX_NUM_DM_TEAMS]
	
	VECTOR vBruciePos	
	
	INT iSpectatorBit
	
	BOOL bSetupVehDmHealthBars
	
	INT iObjKillCounter
	BOOL bPassedObjective
	SCRIPT_TIMER timerTimedKill
	INT iTeamRowBit
	INT iTeamNameRowBit
	
	INT iWeaponIterator
	
	BOOL bCollectedHealth
	
	BOOL bWeaponsAndPropsCreated
	
	INT iFXBitset

//	INT iFurthestPickupIndex = -1
	INT iKillMedBountyBit
	INT iKillBigBountyBit
	INT iPowerPlayBit
	DPAD_VARS dpadVars
	LBD_VARS lbdVars
	SCALEFORM_INDEX scaleformDpadMovie
	
	INT iFacialProgress
	
	INT iXPToShowOnLeaderboard
	INT iXPAdditional
	
	INT iWeaponBitset
	
	INT iPlayerObjCounter
	INT iRandSeed
	
	BOOL bBlipPower = FALSE
	BOOL bBlipDeath = FALSE
	BOOL bBlipAll = FALSE
	BOOL bBlipFlash = FALSE
	BOOL bBlipStreak = FALSE
	BOOL bBlipPerk = FALSE
	BOOL bBlipStart = FALSE 
	BOOL bBlipEnd = FALSE
		
//	INT iBrucieProgress
	BOOL bMakeBrucieBox

	BOOL bLoadedMissionRatingDetails
	STRING stRockStarDeathMatchToLoad
	PED_WEAPONS_STRUCT structWeaponInfo

	FMMC_EOM_DETAILS                sEndOfMission
	PLAYER_CURRENT_STATUS_COUNTS    sPlayerCounts

	REL_GROUP_HASH rgFM_DEATHMATCH[NUM_NETWORK_PLAYERS]

	INT isearchingforVeh  = -1
	
	BOOL bLockVeh
	
	BOOL bFakeCameraSetup
	
	INT iNumberDmStartedStat
	
	SCRIPT_TIMER timeIdle
//	SCRIPT_TIMER tempTimer
	SCRIPT_TIMER timeRestartOptions
//	SCRIPT_TIMER timeAwardTimeout
	SCRIPT_TIMER timeBigMessageFailSafe
	SCRIPT_TIMER timeAwardFailSafe
	SCRIPT_TIMER timeCoronaFailSafe
	SCRIPT_TIMER timeDoubleDamage
	TIME_DATATYPE timeScrollDelay
	SCRIPT_TIMER timeForceWarp
	SCRIPT_TIMER timeLeaderboardTimeOut
	SCRIPT_TIMER timeBlipsDeathStreak
	SCRIPT_TIMER timeAudioFade
	SCRIPT_TIMER timeWaterCheck
	SCRIPT_TIMER timeRespawn
	SCRIPT_TIMER timePerk
	SCRIPT_TIMER timeBlipsFirst5
	SCRIPT_TIMER timeMusicScore
	SCRIPT_TIMER timeThumbSafe
//	SCRIPT_TIMER timeRespawnStateChangeDelay
	SCRIPT_TIMER timeJobBoxSafety
	SCRIPT_TIMER timeRespawnStuckSafety
	SCRIPT_TIMER timerAudio
	SCRIPT_TIMER lbdRankPredictionTimeout
	
	TIME_DATATYPE timeWarp
	TIME_DATATYPE timeInitialSpawn
	TIME_DATATYPE timeRespawnTimeout
	TIME_DATATYPE timeInitialLoadScene
	
	// initial spawn pos
	INT iInitialVehSpawnState
	VECTOR vInitialSpawnPos
	FLOAT fInitialSpawnHeading
	
	JOB_INTRO_CUT_DATA DMIntroData
	
	// VEH dm's
	INT iRaceRespawnState
	VEHICLE_INDEX vehicleToDelete
	VEHICLE_INDEX tempVeh
	VEHICLE_INDEX storedLastVeh
	VEHICLE_INDEX dmVehIndex
//	VECTOR vRespawnLoc
//	FLOAT fRespawnHeading
	
	// Lbd player chosen
	INT iPlayerSelection = -1
	INT iPlayerSelectionDelay
	
//	INT iTempDelayTimer

	// Late joining
	INT iLateTeam
	BOOL bSmallestTeam[MAX_NUM_DM_TEAMS]
	INT iTeamLateCount[MAX_NUM_DM_TEAMS]
	INT iLateTeamProgress
	
	// Props
	OBJECT_INDEX oiProps[FMMC_MAX_NUM_PROPS]
	OBJECT_INDEX oiPropsChildren[FMMC_MAX_NUM_PROP_CHILDREN]
	
	// Veh dm's
	INT iInitialPedSpawnState
//	VEHICLE_INDEX dmVehIndex
	NETWORK_INDEX dmVehID

	PICKUP_INDEX Pickup[NUM_FM_DM_PICKUPS]

	SCALEFORM_INDEX RespawnButton
//	Graeme - this didn't seem to be used any more
//	DISPLAYOVERHEADSTRUCT DisplayInfo
	
	// Thumb votes
	SCALEFORM_INDEX siThumb
//	SCALEFORM_THUMBS thumbStruct
	SCRIPT_TIMER thumbTimer
	SPRITE_PLACEMENT ScaleformSprite
	BOOL bScaleform
	
	INT iClientRefreshDpadValue

//	INT iCleanupProgress
	SAVE_OUT_UGC_PLAYER_DATA_VARS sSaveOutVars
	
	SCRIPT_TIMER timeRespawning
	SCRIPT_TIMER timeDpad
//	SCRIPT_TIMER timeMorePlayers
	SCRIPT_TIMER timeScoreboardRunning
	//SCRIPT_TIMER timeHelpText
	SCRIPT_TIMER timeVehRespawn

	INT iXPAtJobStart
	//INT iCashAtJobStart
	
	INT iTeamSpawnBitSet

//	INT iRespawnProgress
	INT iRestartProgress
//	INT iInitialFadeProgress
//	INT iLeaderboardProgress
	INT iDied
//	INT iRevengeBitset
	INT iDeathBitSet
	INT iAssistBitSet
	INT iTickerBitSet
	INT iTimeHlpTxtTimer
//	INT iWeatherRandom
	INT iIDiedBitset
	INT iPickupBitset
	// Assists
	INT iDamage
//	INT iAssistTimer[NUM_NETWORK_PLAYERS]
	INT iAssistDamage[NUM_NETWORK_PLAYERS]
	INT iMeFoeDiedBitSet
	//////////////////////////////   
	///     
	STRUCT_REL_GROUP_HASH sRGH
	
	NEXT_JOB_STRUCT nextJobStruct
	
	INT iKilledSameGuyBitset
	INT iHighestKillStreak
	
	INT iTeamAreaSwitch
	
	PARTICIPANT_INDEX localParticipant
	PARTICIPANT_INDEX participantKiller
	BLIP_INDEX blipPickups[NUM_FM_DM_PICKUPS]

	WEAPON_TYPE weaponType
	
//	TEXT_LABEL_23 tl63_ParticipantNames[NUM_NETWORK_PLAYERS]
 
	INT iNonBDBitset
	INT iTxtBitset
	SCRIPT_TIMER stSecondHTTimer
	
	INT iSavedRow[NUM_NETWORK_PLAYERS]
	INT iSavedSpectatorRow[NUM_NETWORK_PLAYERS]
	INT iSpecProgress
	
//	// Winning stage
//	BOOL bWinnersFound
//	BOOL bAlreadyUsedTeam[MAX_NUM_DM_TEAMS]
//	BOOL bNotBestTeam[MAX_NUM_DM_TEAMS]
//	INT iBestTeamOrder[MAX_NUM_DM_TEAMS]
//	INT iCurrentTeamToCheck
	
//	INT iInstance = -1
	
	//radar Zoom variables
//	FLOAT fFurthestTargetDistTemp
	FLOAT fFurthestTargetDist = 100.0
	FLOAT fOldFurthestTargetDist = 100.0
//	FLOAT fFurthestWeaponDist
//	FLOAT fFurthestWeaponDistTemp
//	FLOAT fFurthestPlayerDist
//	FLOAT fFurthestPlayerDistTemp
	
	//CDM 13/2/13
	SC_LEADERBOARD_CONTROL_STRUCT scLB_control
	SCALEFORM_INDEX	scLB_scaleformID
//	PLAYER_INDEX 	piImpromptuRival
	UGCStateUpdate_Data sUgcStatData
	BOOL bHaveFriendToSendTo
	#IF IS_DEBUG_BUILD
		//FLOAT fStoredDamageModifier = -1.0
		DEBUG_DM_VARIABLES debugDmVars
	#ENDIF	
	
	//Celebration screen.
	BOOL bCelebrationSummaryComplete
	CELEBRATION_SCREEN_DATA sCelebrationData
	CAMERA_INDEX camEndScreen
	
	INT iBrucieInstance = -1
	
	INT iBVBbitset
	INT iInitRemoteGhostBitset
	INT iSetRemotelyGhostedBitset
	
	GB_STRUCT_BOSS_END_UI gbBossEndUi
	 //FEATURE_GANG_BOSS
	
	INT iBlipRivalGangBitset
	BOOL bRivalBossIsBiker
	 // FEATURE_BIKER
	
	//Pass the Bomb
	INT iTagBombCountdownSndID = -1
	INT iTagBomb_PrevObjectiveTextPartIndex = -1
	INT iTimeCarryingBomb = 0
	BLIP_SPRITE bsPrevBS = RADAR_TRACE_INVALID
	INT iPTBCachedNitroModLevel = -99
	
	//Spectator
	SCRIPT_TIMER tdSpectatorStartTimer
	
	SCRIPT_TIMER tdMusicMoodChangeTimer
	INT iMoodChangeTime
	INT iAudioTrack
	INT iAudioMood
	
	SCRIPT_TIMER stHelptextTimer
	INT iHelptextBitset
	
	VEHICLE_INDEX viArenaVehToCleanup
	VEHICLE_INDEX viPTBVeh
	FMMC_ARENA_TRAPS_LOCAL_INFO sTrapInfo_Local
	
	ARENA_SOUNDS_STRUCT sArenaSoundsInfo
	
	SCRIPT_TIMER stCelebVehicle
	VEHICLE_INDEX winnerVehicle
	INT iWinningParticipantCopyForCelebrationScreen = -1
	
	//INTERIOR_INSTANCE_INDEX interiorArena
	ARENA_CONTESTANT_TURRET_CONTEXT arenaTurretContext
	
	FLOAT fVehicleWeaponShootingTime = 2.5
	FLOAT fVehicleWeaponTimeSinceLastfired = 2.0
	BOOL bRegenVehicleWeapons = FALSE
	INT iFakeAmmoRechargeSoundID = -1
	
	DM_INTRO_COUNTDOWN_UI cduiIntro
	BOOL bLocalHandBrakeOnFlag
	
	SPLIT_SESSION sSplitSession
	
	INT iZoneWaterCalmingQuadID[FMMC_MAX_NUM_ZONES]
	INT iWaterCalmingQuad[FMMC_MAX_WATER_CALMING_QUADS]
	INT iWaveDampingZoneBitset
	INT iLocalZoneBitset
	INT iLocalZoneCheckBitset
	INT iZoneStaggeredLoop
	
	SCRIPT_TIMER stExplosionZoneTimer[FMMC_MAX_NUM_ZONES]
	SCRIPT_TIMER td_ArenaWarsBigAir
	BLIP_INDEX biTrapCamBlip[FMMC_MAX_NUM_DYNOPROPS]
	INTERIOR_INSTANCE_INDEX iArenaInterior_VIPLoungeIndex
	SCRIPT_TIMER tdArenaSpawnTimer

	SHAPETEST_INDEX stiVehSafe
	SHAPETEST_STATUS stiVehSafeStatus
	INT iVehToShapeTest = 0
	INT iVehRespawnAlphaBS = 0
	
	KING_OF_THE_HILL_RUNTIME_LOCAL_STRUCT sKOTH_LocalInfo
	DEATHMATCH_BOUNDS_RUNTIME_LOCAL_STRUCT sDeathmatchBoundsData[FMMC_MAX_NUM_DM_BOUNDS]
	INT iPreviousBoundsIndex = -1
	DM_SHRINKING_BOUNDS_RUNTIME_LOCAL_STRUCT sShrinkingBoundsLocalRuntimeData[FMMC_MAX_NUM_DM_BOUNDS]
	DM_SHRINKING_BOUNDS_CONDITION_DATA sShrinkingBoundsConditionData
	
	INT iWorldPropIterator
	LEGACY_RUNTIME_WORLD_PROP_DATA sRuntimeWorldPropData
	
	INACTIVE_DETECTION_STRUCT sInactiveDetectionStruct
	
	FLOAT fTimeSpentCheckingIdle
	
	FMMC_POWERUP_STRUCT sPowerUps
	FMMC_VEHICLE_MACHINE_GUN sVehMG
	
	SCRIPT_TIMER tdPickupSpawnShardTimer
	INT iPickupSpawnShardSoundID = -1
	
	INT iCountdownBeeps = 11
	
	SCRIPT_TIMER tdLBDDelay
	SCRIPT_TIMER tdForceEndTimer
	SCRIPT_TIMER tdresultstimer
	SCRIPT_TIMER tdLeaderboardRoundTimer
	
	INT iMyPreviousScore
	DM_MODIFIER_POSITION eMyPreviousPosition
	
	TARGET_FLOATING_SCORE sTargetFloatingScores[TARGET_MAX_FS]
	
	PLAYER_MODIFIER_DATA sPlayerModifierData	
	
	INT iCachedModset = -1
	
ENDSTRUCT

SCALEFORM_INDEX sfiPPGenericMovie	// For PROCESS_PASS_THE_BOMB_HUD

// Local mission stages
ENUM CLIENT_MISSION_STAGE
	CLIENT_MISSION_STAGE_INIT=0,
	CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING,
	CLIENT_MISSION_STAGE_HOLDING,
	CLIENT_MISSION_STAGE_SETUP,
	CLIENT_MISSION_STAGE_FIGHTING,
	CLIENT_MISSION_STAGE_DEAD,
	CLIENT_MISSION_STAGE_RESPAWN,
	CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE,
	CLIENT_MISSION_STAGE_CONOR_LBD,
	CLIENT_MISSION_STAGE_CELEBRATION,
	CLIENT_MISSION_STAGE_LBD_CAM,
	CLIENT_MISSION_STAGE_SOCIAL_LBD,
	CLIENT_MISSION_STAGE_RESULTS,
	CLIENT_MISSION_STAGE_LEADERBOARD,
	CLIENT_MISSION_STAGE_PICK,
	CLIENT_MISSION_STAGE_SCTV,
	CLIENT_MISSION_STAGE_SPECTATOR,
	CLIENT_MISSION_STAGE_END
ENDENUM

// -----------------------------------	SERVER BROADCAST DATA ----------------------------------------------------------------------
STRUCT ServerBroadcastData
	CELEB_SERVER_DATA sCelebServer
	
	INT iMatchHistoryId
	INT iHashedMac

	INT iFinishingPos[NUM_NETWORK_PLAYERS]
	
	INT iPopulationHandle = -1

	INT iIllegalCheckProgress
	TEXT_LABEL_63 tl63_ParticipantNames[NUM_NETWORK_PLAYERS]

	INT iImpromptuWinner = -1
	
	INT iFixedCamera
	
	INT iUniqueId
	INT iNumDmStarters // Players coming through from corona
	
	INT iNumSpectators
	
	INT iHostRefreshDpadValue
	
	INT iFurthestPickupIndex = -1
	
	INT iTotalKills
	INT iTotalDpadKills
	
	INT iTeamWinCount, iTeam2Count, iTeam3Count, iTeam4Count
	
	INT iNumAlivePlayers	
	INT iNumAliveTeams
	
	INT iNumPickCreated
	
//	INT iThumbBitset
//	INT iThumbUpCount
//	INT iThumbDownCount

	
	INT iFinishTime = -1
	
	INT iTeamPosition[MAX_NUM_DM_TEAMS]
	INT iNumberOfLBPlayers[MAX_NUM_DM_TEAMS]
	
	PLAYER_INDEX playerMatchWinner 	
	PLAYER_INDEX playerDMWinner 	
	SERVER_EOM_VARS sServerFMMC_EOM
	
	//Used to work out centre point 
	VECTOR vTeamCentreStartPoint[MAX_NUM_DM_TEAMS]
	
	// Brucie box
	VECTOR vPosBrucie
	FLOAT fBrucieHeading
	INT iBrucieProgress
	BOOL bScriptLaunched
	
	INT iDeathmatchType = -1
	INT iVehDeathmatch 	= -1
	
	INT iTagSetting 	= -1
	INT iWeather    	= -1
	INT iPolice			= -1
	INT iDuration		= -1
	INT iTarget 		= -1
	INT iTeamTarget[FMMC_MAX_TEAMS]
	INT iStartWeapon 	= -1
	INT iHealthBar		= -1
	INT iTimeOfDay 		= -1
	INT iTraffic		= -1
	INT iAverageRank	= -1
	
	INT iIdleBitset
	
	// Team score totals
//	INT iTeamKills[MAX_NUM_DM_TEAMS]
	INT iTeamDeaths[MAX_NUM_DM_TEAMS]
	FLOAT fTeamRatio[MAX_NUM_DM_TEAMS]
	INT iTeamXP[MAX_NUM_DM_TEAMS]
 
	// Team balancing
//	INT iBalanceProgress
//	INT iFindLargestProgress
	INT iTempBalancedTeam[MAX_NUM_MC_PLAYERS]
	BOOL bNotBiggest[MAX_NUM_DM_TEAMS]
	BOOL bAlreadyUsed[MAX_NUM_DM_TEAMS]
	INT iTeamOrder[MAX_NUM_DM_TEAMS]
	INT iCurrentDMTeam
	INT iteamBalanceCount[MAX_NUM_DM_TEAMS]
	BOOL bTeamSizeSorted
	VECTOR vImpromptuCoord

	INT iServerGameState
	INT iServerBitSet		
	INT iServerBitSet2
	
	INT iTeamSpawnPoint[NUM_NETWORK_PLAYERS]
	
	INT iParticipantsTurnToSpawn = 0
	INT iBS_ParticipantHasSpawned = 0
	
	INT iMissionId // deathmatch id (map)
	
	SCRIPT_TIMER timeServerForFinishedLast
	SCRIPT_TIMER timeServerMainDeathmatchTimer
	SCRIPT_TIMER timeBrucieBox
	SCRIPT_TIMER tdPickupSpawnTimer

	INT iNumVehCreated // Num vehs made in creator
	INT iNumDMPlayers
	INT iNumActivePlayers
	INT iNumActiveTeams
	INT iBestPlayerWinningTeam
	INT iMVP
	
	INT iTeamWithHighestScore = -1
//	PLAYER_INDEX playerWithHighestScore 
	
	INT iNumberOfTeams
	INT iTeamBalancing
	INT iWinningTeam		= TEAM_INVALID
	INT iSecondTeam		 	= TEAM_INVALID
	INT iThirdTeam			= TEAM_INVALID
	INT iFourthTeam			= TEAM_INVALID
	INT iLosingTeam 		= TEAM_INVALID
	INT iNumWinners		
	
	PLAYER_INDEX playerWinning
	PLAYER_INDEX playerLosing
	PLAYER_INDEX playerPower 
	PLAYER_INDEX playerPowerStoredLast
	PLAYER_INDEX playerAbandonedImpromptu 
	
	INT iPlayerWhoLeftScore[MAX_NUM_DM_TEAMS]
	INT iPlayersLastScore[MAX_NUM_DM_TEAMS][NUM_NETWORK_PLAYERS] 
	INT iPlayerWhoLeftDeaths[MAX_NUM_DM_TEAMS]
	INT iPlayersLastDeaths[MAX_NUM_DM_TEAMS][NUM_NETWORK_PLAYERS] 
	FLOAT fPlayerWhoLeftRatio[MAX_NUM_DM_TEAMS]
	FLOAT fPlayersLastRatio[MAX_NUM_DM_TEAMS][NUM_NETWORK_PLAYERS] 
	INT iPlayerWhoLeftRP[MAX_NUM_DM_TEAMS]
	INT iPlayersLastRP[MAX_NUM_DM_TEAMS][NUM_NETWORK_PLAYERS] 
	
	INT iNumberOfStartingPlayers[MAX_NUM_DM_TEAMS]
	
	////////////////////////////// 	TDM
	INT iTeamScore[MAX_NUM_DM_TEAMS]
	INT iTeamScoreForLBD[MAX_NUM_DM_TEAMS]
	INT iTeamScorePercentage[MAX_NUM_DM_TEAMS]
//	TEAM_COUNT sTeamCount
	
	#IF IS_DEBUG_BUILD
	INT iServerDebugBitSet
	#ENDIF
	
//	// Winning stage
//	INT iFindWinningTeamStage
	INT iBestTeamOrder[MAX_NUM_DM_TEAMS]
	
	//For resetting the datta
	INT 	iNextMission		= -1
	INT 	iMissionVariation	= -1
	
	INT iBossvBossWinner = -1
	INT iLivesRemaining[2]
	INT iMissingKills[2]
	INT iTeamBossPlayer[2]
	INT iTeamBossPart[2]

	
	FMMC_ARENA_TRAPS_HOST_INFO sTrapInfo_Host
	
	INT iPartTagged	= -1	// Participant currently tagged
	INT iPartLastTagged = -1
	INT iRecentPasses = 0
	SCRIPT_TIMER iLastPassTimeStamp
	
	INT iPartTaggedTeam = -1 //Server-side cache of the team of the player who is tagged
	SCRIPT_TIMER tdTaggedExplosionTimer
	INT iTagBombsDetonated
	INT iPTB_Passes[NUM_NETWORK_PLAYERS]
	
	BOOL bAllPlayersHaveAFinishTime = FALSE
	NETWORK_INDEX vehWinner
	
	SCRIPT_TIMER timeServerCountdown
	
	ARENA_CONTESTANT_SERVER_DATA arenaContestantTurretServer
	INT iVehicleRemovalCap = 1
	
	INT iArenaAudioScore = -1
	
	INT iArena_Lighting = -1
	
	INT iVehicleSpawnedBitset
	INT iVehicleRespawnBitset
	INT iVehicleRespawnGhostedBitset
	SCRIPT_TIMER td_VehCleanupTimer[FMMC_MAX_VEHICLES]

	INT iSpawnArea = -1
	INT iSpawnAreaVeh = -1
		
	KING_OF_THE_HILL_RUNTIME_HOST_STRUCT sKOTH_HostInfo
	
	INT iPlayerLives = 0
	INT iTeamLives[FMMC_MAX_TEAMS]
	INT iTeamExtraLives[FMMC_MAX_TEAMS]
		
	CELEBRATION_STATS sCelebrationStats[NUM_NETWORK_PLAYERS]
	
	DM_SHRINKING_BOUNDS_SERVER_DATA sBoundsServerData[FMMC_MAX_NUM_DM_BOUNDS]
ENDSTRUCT

STRUCT ServerBroadcastData_Leaderboard
	THE_LEADERBOARD_STRUCT leaderboard[NUM_NETWORK_PLAYERS]
ENDSTRUCT

//SERVER_NEXT_JOB_VARS serverJobVoteVars

CONST_INT ci_SpectatorState_WAIT 				0
CONST_INT ci_SpectatorState_INIT	 			1
CONST_INT ci_SpectatorState_SET_TO_SPECTATOR 	2
CONST_INT ci_SpectatorState_MAINTAIN 			3
CONST_INT ci_SpectatorState_CLEANUP 			4
CONST_INT ci_SpectatorState_END					5

// -----------------------------------	CLIENT BROADCAST DATA ----------------------------------------------------------------------
STRUCT PlayerBroadcastData

	// Clients start here
	CLIENT_MISSION_STAGE eClientLogicStage = CLIENT_MISSION_STAGE_INIT
	
	BOOL bEnteredDM
	
	BOOL bFinishedDeathmatch
//	INT iFinishingPos = -1
	INT iInitialFadeProgress
	INT iLeaderboardProgress
	INT iLoadNextMissionProgress
	INT iGameState
	INT iCash
	INT iKills 
	INT iScore 
//	INT iAssists
	//INT iLastDamager
	INT iHeadshots
	INT iDeaths
	INT iSuicides
	INT iExtraLives
//	INT iDeathStreak
	FLOAT fRatio
	INT iCurrentXP
	INT iJobXP
	INT iJobCash
	INT iBets	
	INT iClientBitSet	
	INT iTeam
	INT iDataFileBitSet
	
	INT iCurrentELO
	INT iELODiffForConor
	
	INT iSpectatorState
	
	FLOAT fDamageTaken
	FLOAT fDamageDealt
	
	INT iPTB_TimeSpentCarryingBomb
	
//	PLAYER_INDEX PlayerThatKilledMeLast

	INT iMyRivalBossKills = 0
	INT iMyBossKillSelf = 0
	
	INT iFinishedAtTime = 0
	
	#IF IS_DEBUG_BUILD
		BOOL bPressedF					
		BOOL bPressedS
		BOOL pressedTimeSkip
	#ENDIF
	MODEL_NAMES mnCoronaVehicle
	INT iApEarned
	INT iDisplaySlot
	ARENA_POINTS sArenaPoints
	ARENA_WARS_SC_LDB_STATS sArenaSCLBStats
	
	KING_OF_THE_HILL_RUNTIME_PLAYER_STRUCT sKOTH_PlayerInfo
	
	FMMC_VEHICLE_MACHINE_GUN_PBD sScriptedMachineGun
		
	INT iCurrentModSet = -1
	INT iPendingModSet = -1
	
	CELEBRATION_STATS sCelebrationStats
	
	DM_BLIP_TYPE eBlipType
	
	DM_SHRINKING_BOUNDS_CLIENT_DATA sShrinkingBoundsData[FMMC_MAX_NUM_DM_BOUNDS]
	
ENDSTRUCT

//Player Modifiers
MODEL_NAMES mnPrevPlayerModifierRespawnVeh = DUMMY_MODEL_FOR_SCRIPT

