USING "net_deathmatch_vehicleDM.sch"

PROC STORE_CLIENT_KILL_DEATH_RATIO(PlayerBroadcastData &playerBDPassed[])
	FLOAT fDeaths	= TO_FLOAT(playerBDPassed[iLocalPart].iDeaths)
	FLOAT fKills 	= TO_FLOAT(playerBDPassed[iLocalPart].iKills)
//	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_STORED_RATIO) 
		playerBDPassed[iLocalPart].fRatio = GET_KD_RATIO(fDeaths, fKills)
//		PRINTLN(" STORE_CLIENT_KILL_DEATH_RATIO --- ", playerBDPassed[iLocalPart].fRatio)
//		SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_STORED_RATIO)
//	ENDIF
ENDPROC

FUNC INT GET_LEADERBOARD_STAGE(PlayerBroadcastData &playerBDPassed[])
	RETURN playerBDPassed[iLocalPart].iLeaderboardProgress 
ENDFUNC 

PROC GOTO_LEADERBOARD_STAGE(PlayerBroadcastData &playerBDPassed[], INT iLeaderboardStageGoto)
	#IF IS_DEBUG_BUILD
		SWITCH iLeaderboardStageGoto
			CASE LEADERBOARD_STAGE_SCALEFORM 	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_SCALEFORM  ", tl31ScriptName) NET_NL() 	BREAK
			CASE LEADERBOARD_STAGE_MAKE_SAFE	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_MAKE_SAFE  ", tl31ScriptName) NET_NL() 	BREAK
			//CASE LEADERBOARD_STAGE_THUMBS		NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_THUMBS  ", tl31ScriptName) NET_NL() 		BREAK	
//			CASE LEADERBOARD_STAGE_SHOW			NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_SHOW  ", tl31ScriptName) NET_NL() 		BREAK
			CASE LEADERBOARD_STAGE_CHALLENGES	NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_CHALLENGES  ", tl31ScriptName) NET_NL() 	BREAK
			CASE LEADERBOARD_STAGE_AWARD		NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_AWARD  ", tl31ScriptName) NET_NL() 	BREAK
			//CASE LEADERBOARD_STAGE_LBD_CAM		NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_LBD_CAM  ", tl31ScriptName) NET_NL() 	BREAK
			//CASE LEADERBOARD_STAGE_TIDY		NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_LEADERBOARD_STAGE  >>> LEADERBOARD_STAGE_TIDY  ", tl31ScriptName) NET_NL() 	BREAK
		ENDSWITCH
	#ENDIF
	
	playerBDPassed[iLocalPart].iLeaderboardProgress = iLeaderboardStageGoto
ENDPROC

FUNC INT GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(ServerBroadcastData_Leaderboard &serverBDpassedLDB, INT iteam)
	INT i = 0

	REPEAT COUNT_OF(serverBDpassedLDB.leaderBoard) i
		INT iParticipant = serverBDpassedLDB.leaderBoard[i].iParticipant
	
		IF iParticipant > -1
			IF serverBDpassedLDB.leaderBoard[i].iteam = iteam
// 			IF playerBDPassed[iParticipant].iTeam = iteam
				PRINTLN("[JOB POINT], GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM =  ", i, " iteam = ", iteam)
				RETURN i
			ELSE
				PRINTLN("[JOB POINT], GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM serverBDpassedLDB.leaderBoard[iParticipant].iteam =  ", serverBDpassedLDB.leaderBoard[i].iteam, " iteam = ", iteam)
			ENDIF
		ELSE
			PRINTLN("[JOB POINT], GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM RETURN iParticipant =  ", iParticipant)
		ENDIF
	ENDREPEAT
	PRINTLN("[JOB POINT], GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM RETURN 0  ")
	RETURN 0
ENDFUNC

FUNC BOOL IS_READY_FOR_LAST_ALIVE_LEADERBOARD(PlayerBroadcastData &playerBDPassed[])

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		PRINTLN("IS_READY_FOR_LAST_ALIVE_LEADERBOARD, Last Man Standing Leaderboard Enabled start________________________")
		INT iPart
		FOR iPart = 0 TO (NUM_NETWORK_PLAYERS - 1)
			PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iPart)				
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
			AND NOT IS_PLAYER_SCTV(NETWORK_GET_PLAYER_INDEX(piPart))
			AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(NETWORK_GET_PLAYER_INDEX(piPart))
				IF playerBDPassed[iPart].iFinishedAtTime = 0
					PRINTLN("IS_READY_FOR_LAST_ALIVE_LEADERBOARD, Part: ", iPart, " has not been assigned an ending time. Probably waiting for BD update. Returning false.")
					RETURN FALSE
				ELSE
					PRINTLN("IS_READY_FOR_LAST_ALIVE_LEADERBOARD, Part: ", iPart, " has been assigned an ending time.")
				ENDIF
			ELSE
				PRINTLN("IS_READY_FOR_LAST_ALIVE_LEADERBOARD, Part: ", iPart, " is not active so skipping them.")
			ENDIF
		ENDFOR
		PRINTLN("IS_READY_FOR_LAST_ALIVE_LEADERBOARD, Last Man Standing Leaderboard Enabled finished________________________")		
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC LBD_SUB_MODE GET_SUB_MODE_FOR_LBD_READ_WRITE(ServerBroadcastData &serverBDpassed)

	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		RETURN SUB_DM_TEAM
	ENDIF
	
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
	
		RETURN SUB_DM_VEH
	ENDIF
	
	RETURN SUB_DM_FFA
ENDFUNC

FUNC INT GET_SUBTYPE_FOR_SC(ServerBroadcastData &serverBDpassed)
	INT iSubMode = FMMC_DM_TYPE_NORMAL
	
	SWITCH GET_SUB_MODE_FOR_LBD_READ_WRITE(serverBDpassed)
		CASE SUB_DM_FFA
			IF IS_KING_OF_THE_HILL()
				iSubMode = FMMC_DM_TYPE_KOTH
			ELSE
				iSubMode = FMMC_DM_TYPE_NORMAL
			ENDIF
		BREAK
		CASE SUB_DM_TEAM
			IF IS_KING_OF_THE_HILL()
				iSubMode = FMMC_DM_TYPE_TEAM_KOTH
			ELSE
				iSubMode = FMMC_DM_TYPE_TEAM
			ENDIF
		BREAK
		CASE SUB_DM_VEH
			iSubMode = FMMC_DM_TYPE_VEHICLE
		BREAK
	ENDSWITCH
	
	RETURN iSubMode
ENDFUNC

FUNC BOOL HAS_SERVER_FINISHED_LBD_SORT(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_CLOSED)
ENDFUNC

FUNC BOOL SHOULD_POSITION_COMPARE_FINISH_TIME(ServerBroadcastData &serverBDpassed, PlayerBroadcastData& playerBDPassed[], INT iParticipant, INT iThisParticipant)
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	AND playerBDPassed[iParticipant].iTeam = playerBDPassed[iThisParticipant].iTeam
		
		INT iTimeDifference = ABSI(playerBDPassed[iParticipant].iFinishedAtTime - playerBDPassed[iThisParticipant].iFinishedAtTime)
		
		IF iTimeDifference < 2000
			#IF IS_DEBUG_BUILD
			IF (bSpewLeaderboardInfo)
				PRINTLN("[GET_POSITION_FOR_PARTICIPANT] SHOULD_POSITION_COMPARE_FINISH_TIME - Not comparing team mate finish times as they are too close to each other.")
				PRINTLN("[GET_POSITION_FOR_PARTICIPANT] SHOULD_POSITION_COMPARE_FINISH_TIME - iParticipant ", iParticipant, " iThisParticipant ", iThisParticipant)
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN playerBDPassed[iParticipant].iFinishedAtTime != playerBDPassed[iThisParticipant].iFinishedAtTime
	
ENDFUNC

FUNC INT GET_POSITION_FOR_PARTICIPANT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData& playerBDPassed[], INT iThisParticipant)
	INT iParticipant
	INT iPosition
	
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX ThisPlayerID
	PLAYER_INDEX PlayerID	
	#ENDIF
	
	BOOL bLastManStanding = IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	BOOL bPassTheBomb 
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		bPassTheBomb = TRUE
	ENDIF
	BOOL bKOTH = IS_KING_OF_THE_HILL()
	
	iPosition = 1 // participant has finished so return a valid position.	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
	
		#IF IS_DEBUG_BUILD
		IF (bSpewLeaderboardInfo)
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iThisParticipant)) 
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant)) 
					ThisPlayerID 	= NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iThisParticipant))
					PlayerID 	 	= NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))	
				ENDIF
			ENDIF
		ENDIF
		#ENDIF

		// check this participant took part in the race				
		IF (playerBDPassed[iParticipant].bEnteredDM)
		AND (playerBDPassed[iThisParticipant].bEnteredDM)
			IF NOT (iParticipant = iThisParticipant)

				IF (NOT IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB) AND IS_BIT_SET(playerBDPassed[iThisParticipant].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB))
				AND NOT bKOTH

					iPosition++	
					
					#IF IS_DEBUG_BUILD
						IF (bSpewLeaderboardInfo)	
							PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [QUIT] ThisPlayerID = ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
						ENDIF
					#ENDIF
					
				ELSE
					IF bLastManStanding 
					AND SHOULD_POSITION_COMPARE_FINISH_TIME(serverBDpassed, playerBDPassed, iParticipant, iThisParticipant)
						IF playerBDPassed[iParticipant].iFinishedAtTime > playerBDPassed[iThisParticipant].iFinishedAtTime
							iPosition++	
							
							#IF IS_DEBUG_BUILD
								IF (bSpewLeaderboardInfo)	
									PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iFinishedAtTime] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
									PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iFinishedAtTime] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].iFinishedAtTime)
									PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iFinishedAtTime] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].iFinishedAtTime)
								ENDIF
							#ENDIF
						ENDIF
					ELIF bLastManStanding
					AND NOT IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
					AND IS_BIT_SET(playerBDPassed[iThisParticipant].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
						iPosition++	
						
						#IF IS_DEBUG_BUILD
							IF (bSpewLeaderboardInfo)	
								PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [OutOfLives] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
								PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [OutOfLives] ", GET_PLAYER_NAME(ThisPlayerID), " is out of lives")
								PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [OutOfLives] ", GET_PLAYER_NAME(PlayerID), " is not out of lives")
							ENDIF
						#ENDIF
					ELSE 
						IF playerBDPassed[iParticipant].iScore > playerBDPassed[iThisParticipant].iScore
						AND NOT bPassTheBomb
							
							iPosition++	
							
							#IF IS_DEBUG_BUILD
								IF (bSpewLeaderboardInfo)	
									PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iScore] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
									PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iScore] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].iScore)
									PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iScore] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].iScore)
								ENDIF
							#ENDIF
						ELSE
							IF playerBDPassed[iParticipant].iScore = playerBDPassed[iThisParticipant].iScore // if they are on the same points, then order by deaths
							OR bPassTheBomb
								IF playerBDPassed[iParticipant].iDeaths < playerBDPassed[iThisParticipant].iDeaths
								
									iPosition++	
											
									#IF IS_DEBUG_BUILD
										IF (bSpewLeaderboardInfo)	
											PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iDeaths] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
											PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iDeaths] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].iDeaths)
											PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iDeaths] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].iDeaths)
										ENDIF
									#ENDIF
								// Same deaths order on ratio	
								ELIF playerBDPassed[iParticipant].iDeaths = playerBDPassed[iThisParticipant].iDeaths
									IF playerBDPassed[iParticipant].fRatio > playerBDPassed[iThisParticipant].fRatio     
									
										iPosition++
										
										#IF IS_DEBUG_BUILD
											IF (bSpewLeaderboardInfo)	
												PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fRatio] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
												PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fRatio] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].fRatio)
												PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fRatio] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].fRatio)
											ENDIF
										#ENDIF
									// Same ratio order on damage dealt	
									ELIF playerBDPassed[iParticipant].fRatio = playerBDPassed[iThisParticipant].fRatio
										IF playerBDPassed[iParticipant].fDamageDealt > playerBDPassed[iThisParticipant].fDamageDealt
											
											iPosition++
											
											#IF IS_DEBUG_BUILD
												IF (bSpewLeaderboardInfo)	
													PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fDamageDealt] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
													PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fDamageDealt] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].fDamageDealt)
													PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fDamageDealt] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].fDamageDealt)
												ENDIF
											#ENDIF
										// Same damage dealt order on least damage taken	
										ELIF playerBDPassed[iParticipant].fDamageDealt = playerBDPassed[iThisParticipant].fDamageDealt
											IF (playerBDPassed[iParticipant].fDamageTaken < playerBDPassed[iThisParticipant].fDamageTaken)
											
												iPosition++
												
												#IF IS_DEBUG_BUILD
													IF (bSpewLeaderboardInfo)	
														PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fDamageTaken] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
														PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fDamageTaken] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].fDamageTaken)
														PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [fDamageTaken] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].fDamageTaken)
													ENDIF
												#ENDIF
											// Same damage taken oreder on XP
											ELIF playerBDPassed[iParticipant].fDamageTaken = playerBDPassed[iThisParticipant].fDamageTaken
												IF playerBDPassed[iParticipant].iJobXP > playerBDPassed[iThisParticipant].iJobXP     		
												
													iPosition++
													
													#IF IS_DEBUG_BUILD
														IF (bSpewLeaderboardInfo)	
															PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iJobXP] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
															PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iJobXP] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].iJobXP)
															PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iJobXP] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].iJobXP)
														ENDIF
													#ENDIF
													
												// Same JobXP order on Cash	
												ELIF playerBDPassed[iParticipant].iJobXP = playerBDPassed[iThisParticipant].iJobXP
													IF (playerBDPassed[iParticipant].iJobCash > playerBDPassed[iThisParticipant].iJobCash)
													
														iPosition++
														
														#IF IS_DEBUG_BUILD
															IF (bSpewLeaderboardInfo)	
																PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iJobCash] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
																PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iJobCash] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].iJobCash)
																PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iJobCash] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].iJobCash)
															ENDIF
														#ENDIF
													// Same cash order on participant	
													ELIF (playerBDPassed[iParticipant].iJobCash = playerBDPassed[iThisParticipant].iJobCash)
														IF (playerBDPassed[iParticipant].iTeam < playerBDPassed[iThisParticipant].iTeam)
														
															iPosition++
																
															#IF IS_DEBUG_BUILD
																IF (bSpewLeaderboardInfo)	
																	PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iTeam] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
																	PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iTeam] ", GET_PLAYER_NAME(ThisPlayerID), " is ", playerBDPassed[iThisParticipant].iTeam)
																	PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [iTeam] ", GET_PLAYER_NAME(PlayerID), " is ", playerBDPassed[iParticipant].iTeam)
																ENDIF
															#ENDIF
															
														// Alphabetical sort	
														ELIF (playerBDPassed[iParticipant].iTeam = playerBDPassed[iThisParticipant].iTeam)
															IF NOT IS_STRING_NULL_OR_EMPTY(serverBDpassed.tl63_ParticipantNames[iParticipant])
															AND NOT IS_STRING_NULL_OR_EMPTY(serverBDpassed.tl63_ParticipantNames[iThisParticipant])
															AND COMPARE_STRINGS(serverBDpassed.tl63_ParticipantNames[iParticipant], serverBDpassed.tl63_ParticipantNames[iThisParticipant]) < 0
															
																iPosition++
																
																#IF IS_DEBUG_BUILD
																	IF (bSpewLeaderboardInfo)	
																		PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [alphabet] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
																	ENDIF
																#ENDIF
															// ID sort
															ELIF COMPARE_STRINGS(serverBDpassed.tl63_ParticipantNames[iParticipant], serverBDpassed.tl63_ParticipantNames[iThisParticipant]) = 0
																IF iParticipant < iThisParticipant	
																
																	iPosition++
																	
																	#IF IS_DEBUG_BUILD
																		IF (bSpewLeaderboardInfo)	
																			PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [ID sort] ThisPlayerID  ", GET_PLAYER_NAME(ThisPlayerID), " PlayerID = ", GET_PLAYER_NAME(PlayerID))
																			PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [ID sort] iThisParticipant = ", iThisParticipant) 
																			PRINTLN("[GET_POSITION_FOR_PARTICIPANT] [ID sort] iParticipant = ", iParticipant)
																		ENDIF
																	#ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
		
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN(iPosition)
ENDFUNC

FUNC BOOL IsParticipantGoodToInsertIntoLeaderboardPosition(ServerBroadcastData &serverBDpassed, THE_LEADERBOARD_STRUCT& leaderBoard[], PlayerBroadcastData &playerBDPassed[], INT iParticipant, INT iPosition, INT iPositionOfParticipant)
	BOOL bGoodToInsert = TRUE
	
	// 1303905
	IF leaderBoard[iPosition].iParticipant = -1

		RETURN FALSE
	ENDIF	
	
	INT iPositionOfCurrentPlayerInLeaderBoard = GET_POSITION_FOR_PARTICIPANT(serverBDpassed, playerBDPassed, leaderBoard[iPosition].iParticipant)
	
	#IF IS_DEBUG_BUILD
		IF (bSpewLeaderboardInfo)
			NET_PRINT("IsParticipantGoodToInsertIntoLeaderboardPosition - iPositionOfCurrentPlayerInLeaderBoard = ") NET_PRINT_INT(iPositionOfCurrentPlayerInLeaderBoard) NET_NL()
		ENDIF
	#ENDIF
	
	// if the current player in the leader board has a valid position
	IF (iPositionOfCurrentPlayerInLeaderBoard != 0)
			
		IF (iPositionOfParticipant = 0)
			// don't insert if this player has an invalid position
			bGoodToInsert = FALSE
			#IF IS_DEBUG_BUILD
				IF (bSpewLeaderboardInfo)	
					NET_PRINT("IsParticipantGoodToInsertIntoLeaderboardPosition - bGoodToInsert = FALSE 1 - invalid position ")  NET_NL()
				ENDIF
			#ENDIF
		ELSE
								
			IF (iPositionOfCurrentPlayerInLeaderBoard < iPosition+1)
			
				bGoodToInsert = FALSE
				
				#IF IS_DEBUG_BUILD
					IF (bSpewLeaderboardInfo)
						NET_PRINT("IsParticipantGoodToInsertIntoLeaderboardPosition - bGoodToInsert = FALSE - iPosition < iPosition+1  ")  NET_NL()
					ENDIF
				#ENDIF
				
			ELIF (iPositionOfCurrentPlayerInLeaderBoard = iPosition+1)
				IF leaderBoard[iPosition].iParticipant < iParticipant

					bGoodToInsert = FALSE
					#IF IS_DEBUG_BUILD
						IF (bSpewLeaderboardInfo)	
							NET_PRINT("IsParticipantGoodToInsertIntoLeaderboardPosition - bGoodToInsert = iParticipant < iParticipant  ")  NET_NL()
						ENDIF
					#ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ELSE
		// current player in leaderboard does not have a valid position, only dont insert if this player also has an invalid position.
		IF (iPositionOfParticipant = 0)
			IF leaderBoard[iPosition].iParticipant < iParticipant									
				bGoodToInsert = FALSE
				#IF IS_DEBUG_BUILD
					IF (bSpewLeaderboardInfo)	
						NET_PRINT("IsParticipantGoodToInsertIntoLeaderboardPosition - bGoodToInsert = iParticipant < iParticipant (invalid position) ")  NET_NL()
					ENDIF
				#ENDIF
			ENDIF		
		ENDIF
	ENDIF
	
	RETURN(bGoodToInsert)
ENDFUNC

FUNC BOOL SHOULD_LBD_RUN(ServerBroadcastData &serverBDpassed)

	// This is needed else the cash/RP does not get awarded
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_ON_LBD)
	
		RETURN TRUE
	ENDIF
	
	IF HAS_SERVER_FINISHED_LBD_SORT(serverBDpassed)
	
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_DEATHMATCH_READY_TO_CLOSE_LDB(ServerBroadcastData &serverBDpassed)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	AND NOT serverBDpassed.bAllPlayersHaveAFinishTime
		PRINTLN("IS_DEATHMATCH_READY_TO_CLOSE_LDB - not all active participants have a finish time yet")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///   (SERVER ONLY: Maintains the leaderboard data)
PROC SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT(THE_LEADERBOARD_STRUCT& leaderBoard[], ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT iThisParticipant #IF IS_DEBUG_BUILD, SHARED_DM_VARIABLES &dmVarsPassed #ENDIF )
				
	#IF IS_DEBUG_BUILD	NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT called on client, host only! ") #ENDIF		

	INT i
	THE_LEADERBOARD_STRUCT EmptyLeaderBoardStruct
	THE_LEADERBOARD_STRUCT StoredLeaderBoardStruct
	THE_LEADERBOARD_STRUCT TempLeaderBoardStruct
	THE_LEADERBOARD_STRUCT ThisParticipantLeaderBoardStruct

	INT iPositionOfThisParticipant
	
	// Team totals local
//	INT iTeamKills[MAX_NUM_DM_TEAMS]
//	INT iTeamDeaths[MAX_NUM_DM_TEAMS]
//	FLOAT fTeamRatio[MAX_NUM_DM_TEAMS]
//	INT iTeamXP[MAX_NUM_DM_TEAMS]
	
	INT iTotalKills
	INT iFFAKills[NUM_NETWORK_PLAYERS]
	INT iTempNumberOfLBPlayers[MAX_NUM_DM_TEAMS]
	BOOL bOneKillRemains = FALSE
	
	INT iTotalDpadKills
	INT iDpadKills[NUM_NETWORK_PLAYERS]
	
	BOOL bArenaMode, bPassBomb, bContinueLBDAfterEnd
	IF IS_ARENA_WARS_JOB(TRUE)  
		bArenaMode = TRUE
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			bPassBomb = TRUE
		ENDIF
		
		bContinueLBDAfterEnd = IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
							OR IS_CURRENT_MISSION_ARENA_UGC()
	ENDIF
	
	PRINTLN("[bug:5588022][SERVER_END] SHOULD_LBD_RUN: ", SHOULD_LBD_RUN(serverBDpassed), " bContinueLBDAfterEnd: ", bContinueLBDAfterEnd)
	
	IF SHOULD_LBD_RUN(serverBDpassed)	
	OR bContinueLBDAfterEnd
		IF HAS_SERVER_CHECKED_EVERYONE_READY(serverBDpassed)		
		
			// Starting update
			IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_STARTING_FIRST_UPDATE)
				IF iThisParticipant = 0
					PRINTLN("[SERVER_END] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT, SERV_BITSET_LBD_STARTING_FIRST_UPDATE")
					SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_STARTING_FIRST_UPDATE)
				ENDIF
			ENDIF
				
			// Ensure full update
			IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_LBD_SORT)
				IF SERVER_CHECKS_HAS_DM_ENDED(serverBDpassed, FALSE  #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF  )
					IF iThisParticipant = 0
						PRINTLN("[SERVER_END] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT, SERV_BITSET_START_FINISHING_UP_LBD_SORT")
						SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_LBD_SORT)
					ENDIF
				ENDIF
			ENDIF
		
			iPositionOfThisParticipant = GET_POSITION_FOR_PARTICIPANT(serverBDpassed, playerBDPassed, iThisParticipant)
			
			// remove this participant from leader board before we re-add him.
			BOOL bParticipantRemoved = FALSE
			REPEAT NUM_NETWORK_PLAYERS i 
			
//				IF leaderBoard[i].bInitialise = FALSE
				IF NOT IS_BIT_SET(leaderBoard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
					leaderBoard[i].playerID 		= INVALID_PLAYER_INDEX()
					leaderBoard[i].iParticipant 	= -1
//					leaderBoard[i].bInitialise = TRUE
					SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_INITIALISE)
				ENDIF
			
				// Team counts
				IF leaderBoard[i].iParticipant <> -1
					IF leaderBoard[i].iTeam <> -1
						iTempNumberOfLBPlayers[leaderBoard[i].iTeam]++
					ENDIF
					
					// For dpad lbd updating					
					iDpadKills[i] = 0
					iDpadKills[i] = (iDpadKills[i] + leaderBoard[i].iScore)
					iTotalDpadKills += iDpadKills[i]
					
					// Reset total kills
					IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
					OR IS_TDM_USING_FFA_SCORING()
						iFFAKills[i] = 0
						iFFAKills[i] = (iFFAKills[i] + playerBDPassed[leaderBoard[i].iParticipant].iScore)
						iTotalKills += iFFAKills[i]
						
						IF iFFAKills[i] = (GET_TARGET_SCORE(serverBDpassed, playerBDPassed[i].iTeam) - 1)
							PRINTLN("[1719773] iFFAKills = ", iFFAKills[i])
							PRINTLN("[1719773] GET_TARGET_SCORE = ", GET_TARGET_SCORE(serverBDpassed, playerBDPassed[i].iTeam))
							bOneKillRemains = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				// REMOVING PLAYERS
				IF NOT (bParticipantRemoved)
					IF (leaderBoard[i].iParticipant = iThisParticipant)
						ThisParticipantLeaderBoardStruct = leaderBoard[i]
						
						IF bArenaMode
						
							#IF IS_DEBUG_BUILD
								IF (bSpewLeaderboardInfo)														
									PRINTLN("[CS_LBD] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - removing participant ", iThisParticipant, " from leaderBoard array element ", i)
								ENDIF
							#ENDIF

							// check if this participant left mid dm (without legitimately finishing)
							IF ThisParticipantLeaderBoardStruct.iParticipant >= 0
							AND NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, ThisParticipantLeaderBoardStruct.iParticipant))
							AND (playerBDPassed[ThisParticipantLeaderBoardStruct.iParticipant].bEnteredDM)
							AND NOT (playerBDPassed[ThisParticipantLeaderBoardStruct.iParticipant].bFinishedDeathmatch)
							
								#IF IS_DEBUG_BUILD
									IF (bSpewLeaderboardInfo)														
										PRINTLN("[CS_LBD] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - setting ThisParticipantLeaderBoardStruct.iParticipant to -1 ", ThisParticipantLeaderBoardStruct.iParticipant)
									ENDIF
								#ENDIF
							
								ThisParticipantLeaderBoardStruct.iParticipant = -1
							ENDIF
	
						ENDIF

						leaderBoard[i] = EmptyLeaderBoardStruct
						bParticipantRemoved = TRUE
					ENDIF
				ELSE
					TempLeaderBoardStruct = leaderBoard[i]
					leaderBoard[i-1] = 	TempLeaderBoardStruct
					leaderBoard[i] = EmptyLeaderBoardStruct
				ENDIF
			ENDREPEAT
			
			// Team counts as serverBD
			serverBDpassed.iNumberOfLBPlayers[0] = iTempNumberOfLBPlayers[0]
			serverBDpassed.iNumberOfLBPlayers[1] = iTempNumberOfLBPlayers[1]
			serverBDpassed.iNumberOfLBPlayers[2] = iTempNumberOfLBPlayers[2]
			serverBDpassed.iNumberOfLBPlayers[3] = iTempNumberOfLBPlayers[3]
			
			// Team position
			INT iTeamPosition[MAX_NUM_DM_TEAMS]
			// Reset local team totals
			IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
				REPEAT MAX_NUM_DM_TEAMS i
//					iTeamKills[i] 	= 0
//					iTeamDeaths[i] 	= 0
//					fTeamRatio[i] 	= 0
//					iTeamXP[i] 		= 0
					iTeamPosition[i] = 0
				ENDREPEAT
			ENDIF
			
			BOOL bParticipantInserted = FALSE
			BOOL bGoodToInsert = FALSE
			
			REPEAT NUM_NETWORK_PLAYERS i 
				IF NOT (bParticipantInserted)
					IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iThisParticipant)) 
					OR (NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iThisParticipant)) AND (ThisParticipantLeaderBoardStruct.iParticipant > -1) )
						
						PLAYER_INDEX PlayerId
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, iThisParticipant)) 
							PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_NATIVE(PARTICIPANT_INDEX, iThisParticipant))
						ELSE
							PlayerId = ThisParticipantLeaderBoardStruct.playerID
						ENDIF
						
						// RE-ADDING PLAYERS
						IF playerBDPassed[iThisParticipant].bEnteredDM
							IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iThisParticipant)
							OR IS_BIT_SET(playerBDPassed[iThisParticipant].iClientBitSet, CLIENT_BITSET_ROAMING_SPECTATOR)
								IF IS_NET_PLAYER_OK(PlayerId, FALSE, FALSE)
								OR ( (playerID != INVALID_PLAYER_INDEX()) AND (playerBDPassed[iThisParticipant].bEnteredDM) AND (NOT bArenaMode) ) // added so that racers who have left the session but finished the race are still considered.
								
									#IF IS_DEBUG_BUILD
										IF (bSpewLeaderboardInfo)		
											PRINTLN("iPositionOfThisParticipant = ", iPositionOfThisParticipant)
											NET_PRINT("*************** SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT **************") NET_NL()
											NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - i = ") NET_PRINT_INT(i) NET_NL()
											NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - iThisParticipant = ") NET_PRINT_INT(iThisParticipant) NET_NL()
											NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - playerID = ") NET_PRINT_INT(NATIVE_TO_INT(playerID)) NET_NL()		
											NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - leaderBoard[i].playerID = ") NET_PRINT_INT(NATIVE_TO_INT(leaderBoard[i].playerID)) NET_NL()	
											NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - leaderBoard[i].iParticipant = ") NET_PRINT_INT(leaderBoard[i].iParticipant) NET_NL()									
										ENDIF
									#ENDIF
									
									IF (iPositionOfThisParticipant <= i+1)
									
										bGoodToInsert = TRUE																					
									
										// if there is already a someone there?
										IF (leaderBoard[i].playerID <> playerID) AND (leaderBoard[i].playerID <> INVALID_PLAYER_INDEX())
												
											#IF IS_DEBUG_BUILD
												IF (bSpewLeaderboardInfo)								
													NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - playerBDPassed[leaderBoard[i].iParticipant].iFinishTime = ") NET_PRINT_INT(playerBDPassed[leaderBoard[i].iParticipant].iScore) NET_NL()																
													NET_PRINT("SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT - iPositionOfThisParticipant = ") NET_PRINT_INT(iPositionOfThisParticipant) NET_NL()																																													
												ENDIF
											#ENDIF
											
											bGoodToInsert = IsParticipantGoodToInsertIntoLeaderboardPosition(serverBDpassed, leaderBoard, playerBDPassed, iThisParticipant, i, iPositionOfThisParticipant)
										ENDIF
										
										// are we still good to insert this person into the leader board?
										IF (bGoodToInsert)
																		
											serverBDpassed.sCelebrationStats[i] = playerBDPassed[iThisParticipant].sCelebrationStats
																										
											StoredLeaderBoardStruct 		= leaderBoard[i]										
											leaderBoard[i].playerID 		= PlayerId
											leaderBoard[i].iParticipant 	= iThisParticipant
											leaderBoard[i].iTeam 			= playerBDPassed[iThisParticipant].iTeam
											leaderBoard[i].iKills			= playerBDPassed[iThisParticipant].iKills
											
											IF bPassBomb
												leaderBoard[i].iScore 		= playerBDPassed[iThisParticipant].iPTB_TimeSpentCarryingBomb
											ELSE
												leaderBoard[i].iScore 		= playerBDPassed[iThisParticipant].iScore
											ENDIF												
											
											leaderBoard[i].iDeaths 			= playerBDPassed[iThisParticipant].iDeaths
											IF bArenaMode
												leaderBoard[i].iArenaPoints	= playerBDPassed[iThisParticipant].iApEarned
											ENDIF
											leaderBoard[i].iJobXP			= playerBDPassed[iThisParticipant].iJobXP

											leaderBoard[i].iJobCash			= playerBDPassed[iThisParticipant].iJobCash
											leaderBoard[i].iBets			= playerBDPassed[iThisParticipant].iBets
											leaderBoard[i].iBadgeRank 		= GlobalplayerBD_FM[NATIVE_TO_INT(PlayerId)].scoreData.iRank
											
											leaderBoard[i].fKDRatio		= playerBDPassed[iThisParticipant].fRatio 
//											leaderBoard[i].bFinished		= playerBDPassed[iThisParticipant].bFinishedDeathmatch
											IF playerBDPassed[iThisParticipant].bFinishedDeathmatch
												SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
											ELSE
												CLEAR_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_FINISHED)
											ENDIF
											IF playerBDPassed[iThisParticipant].bEnteredDM
												SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_STARTED)
											ELSE
												CLEAR_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_STARTED)
											ENDIF
//											leaderBoard[i].bStarted			= playerBDPassed[iThisParticipant].bEnteredDM
											leaderBoard[i].iRank 			= iPositionOfThisParticipant
											leaderBoard[i].iHeadshots 		= playerBDPassed[iThisParticipant].iHeadshots	
											IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)		
												leaderBoard[i].iTeamScore 	= serverBDpassed.iTeamScoreForLBD[leaderBoard[i].iTeam] 
											ENDIF
											leaderBoard[i].fDamageTaken 	= playerBDPassed[iThisParticipant].fDamageTaken	
											leaderBoard[i].fDamageDealt 	= playerBDPassed[iThisParticipant].fDamageDealt
											IF IS_BIT_SET(playerBDPassed[iThisParticipant].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)
												SET_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_QUIT)
											ELSE
												CLEAR_BIT(leaderBoard[i].iLbdBitSet, ciLBD_BIT_QUIT)
											ENDIF
											
											leaderBoard[i].mnVehModel		= playerBDPassed[iThisParticipant].mnCoronaVehicle
											
											leaderBoard[i].iFinishTime = playerBDPassed[iThisParticipant].iFinishedAtTime
//											leaderBoard[i].bQuit 			= IS_BIT_SET(playerBDPassed[iThisParticipant].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)

											bParticipantInserted 			= TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// shift everyone else in the leaderboard down
					TempLeaderBoardStruct = leaderBoard[i]
					leaderBoard[i] = StoredLeaderBoardStruct
					StoredLeaderBoardStruct = TempLeaderBoardStruct
				ENDIF
			ENDREPEAT
							
			IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
//				REPEAT NUM_NETWORK_PLAYERS i 
//					// Add team totals together
//					IF leaderBoard[i].iParticipant <> -1
//						iTeamKills[leaderBoard[i].iTeam] 	= (iTeamKills[leaderBoard[i].iTeam]		+ playerBDPassed[leaderBoard[i].iParticipant].iScore)
//						//PRINTLN("DM_TEST, iTeam =  ", leaderBoard[i].iTeam, " iTeamKills = ", iTeamKills[leaderBoard[i].iTeam], " iScore = ", playerBDPassed[leaderBoard[i].iParticipant].iScore)
//						iTeamDeaths[leaderBoard[i].iTeam] 	= (iTeamDeaths[leaderBoard[i].iTeam]	+ playerBDPassed[leaderBoard[i].iParticipant].iDeaths)
//						fTeamRatio[leaderBoard[i].iTeam] 	= (fTeamRatio[leaderBoard[i].iTeam] 	+ playerBDPassed[leaderBoard[i].iParticipant].fRatio)
//						iTeamXP[leaderBoard[i].iTeam] 		= (iTeamXP[leaderBoard[i].iTeam] 		+ playerBDPassed[leaderBoard[i].iParticipant].iCurrentXP)
//					ENDIF
//				ENDREPEAT
			
				// Assign server data totals
				REPEAT MAX_NUM_DM_TEAMS i
//					serverBDpassed.iTeamKills[i] 	= iTeamKills[i] 
//					serverBDpassed.iTeamDeaths[i] 	= iTeamDeaths[i] 
//					serverBDpassed.fTeamRatio[i] 	= fTeamRatio[i] 
//					serverBDpassed.iTeamXP[i] 		= iTeamXP[i] 
					
					IF serverBDpassed.iTeamScoreForLBD[i] = (GET_TARGET_SCORE(serverBDpassed, i) - 1)
						PRINTLN("[1719773] iTeamScore = ", serverBDpassed.iTeamScore[i])
						PRINTLN("[1719773] GET_TARGET_SCORE = ", GET_TARGET_SCORE(serverBDpassed, i))
						bOneKillRemains = TRUE
					ENDIF
					
					iTotalKills += serverBDpassed.iTeamScoreForLBD[i]

					#IF IS_DEBUG_BUILD
					IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "")
						PRINTLN(" i = ", i, " iTeamScore = ", serverBDpassed.iTeamScore[i])
	//					PRINTLN("serverBDpassed.iTeamCash[i]", serverBDpassed.iTeamCash[i])
	//					PRINTLN("serverBDpassed.iTeamKills[i]", serverBDpassed.iTeamKills[i])
						PRINTLN("serverBDpassed.iTeamDeaths[i]", serverBDpassed.iTeamDeaths[i])
						PRINTLN("serverBDpassed.fTeamRatio[i]", serverBDpassed.fTeamRatio[i])
						PRINTLN("serverBDpassed.iTeamXP[i]", serverBDpassed.iTeamXP[i])
					ENDIF
					#ENDIF

					iTeamPosition[i] = GET_TEAM_RANK(serverBDpassed, i, serverBDpassed.iNumberOfTeams)
					serverBDpassed.iTeamPosition[i] = iTeamPosition[i]
					
					#IF IS_DEBUG_BUILD
					IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD5, KEYBOARD_MODIFIER_NONE, "")
						PRINTLN(" i = ", i, " iTeamPosition = ", serverBDpassed.iTeamPosition[i])
					ENDIF
					#ENDIF
				ENDREPEAT			
			ENDIF
		ELSE
			PRINTLN("[bug:5588022][SERVER_END] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT, HAS_SERVER_CHECKED_EVERYONE_READY = FALSE")
		ENDIF
		
		// One kill remaining
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ONE_KILL_LEFT)	
			IF bOneKillRemains
				PRINTLN("[RESPAWN_STATE_CHANGE], [1719773] SERV_BITSET_ONE_KILL_LEFT")
				SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_ONE_KILL_LEFT)
			ENDIF
		ENDIF
		
		// 35 seconds left
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_35_SECONDS_LEFT)	
			IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
				IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, (GET_ROUND_DURATION(serverBDpassed) - PRE_NEARLY_FINISHED_AUDIO_TIME))
					PRINTLN("[DM_AUDIO] SERV_BITSET_30_SECONDS_LEFT")
					SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_35_SECONDS_LEFT)
				ENDIF
			ENDIF
		ENDIF
		
		// 30 seconds left
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_30_SECONDS_LEFT)	
			IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
				IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, (GET_ROUND_DURATION(serverBDpassed) - NEARLY_FINISHED_AUDIO_TIME))
					PRINTLN("[DM_AUDIO] SERV_BITSET_30_SECONDS_LEFT")
					SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_30_SECONDS_LEFT)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_40_SECONDS_LEFT)	
			IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
				IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, (GET_ROUND_DURATION(serverBDpassed) - 40000))
					PRINTLN("[DM_AUDIO] SERV_BITSET_40_SECONDS_LEFT")
					SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_40_SECONDS_LEFT)
				ENDIF
			ENDIF
		ENDIF

		// Total num kills in dm or TDM
		IF serverBDpassed.iTotalKills <> iTotalKills
			serverBDpassed.iTotalKills = iTotalKills
		ENDIF
		
		// Updating dpad lbd
		IF serverBDpassed.iTotalDpadKills <> iTotalDpadKills
			serverBDpassed.iTotalDpadKills = iTotalDpadKills
			serverBDpassed.iHostRefreshDpadValue++
		ENDIF
		
		// Server has done at least one full update // 1616556
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_FINISHED_FIRST_UPDATE)
			IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_STARTING_FIRST_UPDATE)
				IF iThisParticipant = (NETWORK_GET_MAX_NUM_PARTICIPANTS() -1)
					PRINTLN("[SERVER_END] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT, SERV_BITSET_LBD_FINISHED_FIRST_UPDATE")
					SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_FINISHED_FIRST_UPDATE)
				ENDIF
			ELSE
				PRINTLN("[bug:5588022][SERVER_END] SERV_BITSET_LBD_STARTING_FIRST_UPDATE = FALSE")
			ENDIF
		ELSE
			PRINTLN("[bug:5588022][SERVER_END] SERV_BITSET_LBD_FINISHED_FIRST_UPDATE = TRUE")
		ENDIF
		
		// Ensure full update since staggered
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_CLOSED)
			IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_LBD_SORT)
			AND IS_DEATHMATCH_READY_TO_CLOSE_LDB(serverBDpassed)
				PRINTLN("[bug:5588022][SERVER_END] SERV_BITSET_START_FINISHING_UP_LBD_SORT = TRUE")
				
				IF iThisParticipant = (NETWORK_GET_MAX_NUM_PARTICIPANTS() -1)
					PRINTLN("[SERVER_END] SERVER_MAINTAIN_DM_LEADERBOARD_FOR_PARTICIPANT, SERV_BITSET_LBD_CLOSED")
					SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_LBD_CLOSED)
				ENDIF	
			ELSE
				PRINTLN("[bug:5588022][SERVER_END] SERV_BITSET_START_FINISHING_UP_LBD_SORT = FALSE")
			ENDIF
		ELSE
			PRINTLN("[bug:5588022][SERVER_END] SERV_BITSET_LBD_CLOSED = TRUE")
		ENDIF
	ENDIF
ENDPROC

PROC WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(ServerBroadcastData &serverBDpassed, ELO_WRITE_STAGES eEloStage, ServerBroadcastData_Leaderboard &serverBDpassedLDB, INT iLB_ID = -1)
	INT iIncrement = GET_WRITE_VALUE(eEloStage, 1)
	PRINTLN("[CS_ELO] WRITE iIncrement = ", iIncrement)
	IF DID_I_WIN(serverBDpassed, serverBDpassedLDB)
		PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing col # 4 NUM_WIN ",1 )
		IF iLB_ID = -1
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_WINS,iIncrement,0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLB_ID,LB_INPUT_COL_NUM_WINS,1,0)
		ENDIF
		
		PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing col # 5 NUM_MATCHES ",1 )
		IF iLB_ID = -1
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES,iIncrement,0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLB_ID,LB_INPUT_COL_NUM_MATCHES,1,0)
		ENDIF
		
		PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing col # 6 MATCHES_LOST ",0 )
		IF iLB_ID = -1
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES_LOST,0,0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLB_ID,LB_INPUT_COL_NUM_MATCHES_LOST,0,0)
		ENDIF

	ELSE
		PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing col # 4 NUM_WIN ",0 )
		IF iLB_ID = -1
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_WINS,0,0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLB_ID,LB_INPUT_COL_NUM_WINS,0,0)
		ENDIF
		
		PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing col # 5 NUM_MATCHES ",1 )
		IF iLB_ID = -1
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES,iIncrement,0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLB_ID,LB_INPUT_COL_NUM_MATCHES,1,0)
		ENDIF
		
		PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing col # 6 MATCHES_LOST ",1 )
		IF iLB_ID = -1
			LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_NUM_MATCHES_LOST,iIncrement,0)
		ELSE
			WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLB_ID,LB_INPUT_COL_NUM_MATCHES_LOST,1,0)
		ENDIF
	ENDIF
ENDPROC

PROC WRITE_TO_ELO_LEADERBOARD(LBD_SUB_MODE eLbdSubMode, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ELO_WRITE_STAGES eEloStage, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	TEXT_LABEL_31 categoryNames[3]
	TEXT_LABEL_23 uniqueIdentifiers[3]
	//STORED_ELO_ENUM eloEnum
	
	INT iEloUpdate 
	IF eEloStage = ELO_WRITE_START
		iEloUpdate = ELO_DEDUCTION 
		PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD, ELO_WRITE_START, iEloUpdate =  ", iEloUpdate)
	ELSE
		PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD, iELODiffForConor =  ", playerBDPassed[iLocalPart].iELODiffForConor)
		IF IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_ELO_DEDUCTION_DONE)
			iEloUpdate = (playerBDPassed[iLocalPart].iELODiffForConor + ABSI(ELO_DEDUCTION))
			PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD, ELO_WRITE_END, BS_ELO_DEDUCTION_DONE, iEloUpdate =  ", iEloUpdate)
		ELSE
			iEloUpdate = playerBDPassed[iLocalPart].iELODiffForConor
			PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD, ELO_WRITE_END, iEloUpdate =  ", iEloUpdate)
		ENDIF
	ENDIF
	
	// Don't write if debug skipped
	#IF IS_DEBUG_BUILD
		IF HAS_DEBUG_BEEN_USED_IN_THIS_DM(serverBDpassed, playerBDPassed)
		
			PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD bypassed because player cheated")
		
			EXIT
		ENDIF
	#ENDIF
	
	// Don't write if spectating
	IF IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	
		PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD bypassed because IS_LOCAL_PLAYER_SPECTATOR_ONLY")
		
		EXIT
	ENDIF
	
	IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		TEXT_LABEL_31 groupHandle
		groupHandle = g_FMMC_STRUCT.tl31LoadedContentID
		SWITCH eLbdSubMode
		 
			CASE SUB_DM_FFA				
				categoryNames[0] = "Mission"
				uniqueIdentifiers[0] = groupHandle
				
				IF WAS_ELO_SUCCESSFUL(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL)
					categoryNames[0] = "SeasonId"
					uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
					
					IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
						//eloEnum = GET_STORED_ELO_ENUM_FOR_LEADERBOARD(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL)


						IF UPDATE_STORED_ELO(LEADERBOARD_FREEMODE_ELO_DEATHMATCH_OVERALL,iELOUpdate)
							LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iELOUpdate,0)
							PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing ELO update to LB ",iELOUpdate)
						ELSE
							LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,0,0)
							PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN: UPDATE_STORED_ELO failed not updating LB!")
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
								PRINTLN("[CS_ELO] iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
							ENDIF				
						#ENDIF
					
						// Write kills
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
						PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
								PRINTLN("[CS_ELO] iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
							ENDIF				
						#ENDIF
						
						// Write deaths			
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
						PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
						
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
								PRINTLN("[CS_ELO] iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
							ENDIF				
						#ENDIF
						
						// Write wins				
						WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB)
						
						// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_TOTAL_TIME_ELO_RACE,serverBDpassed.iFinishTime,0)
						
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_DEATHMATCH_OVERALL_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_DEATHMATCH_OVERALL_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_DEATHMATCH_OVERALL_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_DEATHMATCH_OVERALL_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
						NET_PRINT_STRINGS(" -<>- [CS_ELO] WRITE_TO_ELO_DEATHMATCH_OVERALL_LEADERBOARD: group handle",groupHandle) NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE SUB_DM_TEAM
			
				categoryNames[0] = "Mission"
				uniqueIdentifiers[0] = groupHandle
				
				IF WAS_ELO_SUCCESSFUL(LEADERBOARD_FREEMODE_ELO_TEAM_DEATHMATCH_OVERALL)
					categoryNames[0] = "SeasonId"
					uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
					IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_ELO_TEAM_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
						//eloEnum = GET_STORED_ELO_ENUM_FOR_LEADERBOARD(LEADERBOARD_FREEMODE_ELO_TEAM_DEATHMATCH_OVERALL)


						IF UPDATE_STORED_ELO(LEADERBOARD_FREEMODE_ELO_TEAM_DEATHMATCH_OVERALL,iELOUpdate)
							LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iELOUpdate,0)
							PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing ELO update to LB ",iELOUpdate)
						ELSE
							LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,0,0)
							PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN: UPDATE_STORED_ELO failed not updating LB!")
						ENDIF
						
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
								PRINTLN("[CS_ELO] iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
							ENDIF				
						#ENDIF
					
						// Write kills
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
						PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
								PRINTLN("[CS_ELO] iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
							ENDIF				
						#ENDIF
						
						// Write deaths			
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
						PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
						
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
								PRINTLN("iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
							ENDIF				
						#ENDIF
						
						// Write wins				
						WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB)
						
						// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_TOTAL_TIME_ELO_RACE,serverBDpassed.iFinishTime,0)
						
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
						NET_PRINT_STRINGS(" -<>- [CS_ELO] WRITE_TO_ELO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: group handle",groupHandle) NET_NL()
					ENDIF
				ENDIF
			BREAK
			
			CASE SUB_DM_VEH
				categoryNames[0] = "Mission"
				uniqueIdentifiers[0] = groupHandle
				
				IF WAS_ELO_SUCCESSFUL(LEADERBOARD_FREEMODE_ELO_VEHICLE_DEATHMATCH_OVERALL)
					categoryNames[0] = "SeasonId"
					uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
					IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_ELO_VEHICLE_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
						//eloEnum = GET_STORED_ELO_ENUM_FOR_LEADERBOARD(LEADERBOARD_FREEMODE_ELO_VEHICLE_DEATHMATCH_OVERALL)


						IF UPDATE_STORED_ELO(LEADERBOARD_FREEMODE_ELO_VEHICLE_DEATHMATCH_OVERALL,iELOUpdate)
							LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,iELOUpdate,0)
							PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing ELO update to LB ",iELOUpdate)
						ELSE
							LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE,0,0)
							PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN: UPDATE_STORED_ELO failed not updating LB!")
						ENDIF
					
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
								PRINTLN("[CS_ELO] iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
							ENDIF				
						#ENDIF
					
						// Write kills
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
						PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
								PRINTLN("[CS_ELO] iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
							ENDIF				
						#ENDIF
						
						// Write deaths			
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
						PRINTLN("[CS_ELO] LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
						
						#IF IS_DEBUG_BUILD
							IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
								PRINTLN("[CS_ELO] iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
//								SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
							ENDIF				
						#ENDIF
						
						// Write wins				
						WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB)
						
						// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
						LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_TOTAL_TIME_ELO_RACE,serverBDpassed.iFinishTime,0)
						
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
						NET_PRINT(" -<>- [CS_ELO] WRITE_TO_ELO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
						NET_PRINT_STRINGS(" -<>-[CS_ELO]  WRITE_TO_ELO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: group handle",groupHandle) NET_NL()
					ENDIF
				ENDIF
			BREAK
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("WRITE_TO_DEATHMATCH_LEADERBOARD, invalid race eLbdSubMode")
					PRINTLN("[CS_ELO] WRITE_TO_ELO_LEADERBOARD, invalid race eLbdSubMode")
				#ENDIF
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[CS_ELO] Mission name or creator ID is invalid not uploading data!") 
	ENDIF
ENDPROC

PROC GRAB_ELO_DATA(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		IF IS_THIS_A_RSTAR_ACTIVITY()
			IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_READ_ELO)
				IF GET_PLAYER_ELO_FOR_MODE(GET_LBD_ELO_FOR_MODE(serverBDpassed), dmVarsPassed.eloVars.iEloReadStage, dmVarsPassed.eloVars.iEloLoadStage, dmVarsPassed.eloVars.bEloSuccess, playerBDPassed[iLocalPart].iCurrentELO)

					PRINTNL()
					PRINTLN("[CS_ELO] DM_START _____________________________________________")
					PRINTNL()
					PRINTLN(" [CS_ELO] GRAB_ELO_DATA, SUCCESS")

					SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_READ_ELO)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_ELO_DEDUCTION_DONE)
					PRINTLN("[CS_ELO] SET_ELO_DEDUCTION_BIT ")
					WRITE_TO_ELO_LEADERBOARD(GET_SUB_MODE_FOR_LBD_READ_WRITE(serverBDpassed), serverBDpassed, playerBDPassed, dmVarsPassed, ELO_WRITE_START, serverBDpassedLDB)
					SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_ELO_DEDUCTION_DONE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//			Name: LEADERBOARD_FREEMODE_TEAM_DEATHMATCH_3
//			ID: 511
//			Num Cols: 7
//			Column Info: 
//		    *SCORE( AGG_SUM ) : COLUMN_ID_LB_SCORE - LBCOLUMNTYPE_INT64
//		    KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
//		    DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
//		    KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_DEATH_RATIO - LBCOLUMNTYPE_INT32
//		    CASH( AGG_SUM ) : COLUMN_ID_LB_CASH - LBCOLUMNTYPE_INT32
//		    NUM_WINS( AGG_SUM ) : COLUMN_ID_LB_NUM_WINS - LBCOLUMNTYPE_INT64
//		    NUM_MATCHES( AGG_SUM ) : COLUMN_ID_LB_NUM_MATCHES - LBCOLUMNTYPE_INT32

PROC WRITE_TO_DEATHMATCH_LEADERBOARD(LBD_SUB_MODE eLbdSubMode, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, INT iMissionId, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	TEXT_LABEL_31 categoryNames[3]
	TEXT_LABEL_23 uniqueIdentifiers[3]
	IF iMissionID <> 0
	ENDIF

	// Don't write if debug skipped
	#IF IS_DEBUG_BUILD
		IF HAS_DEBUG_BEEN_USED_IN_THIS_DM(serverBDpassed, playerBDPassed)
		
			PRINTLN("WRITE_TO_DM_LEADERBOARD bypassed because player cheated")
		
			EXIT
		ENDIF
	#ENDIF
	
	// Don't write if spectating
	IF IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("WRITE_TO_DM_LEADERBOARD bypassed because IS_LOCAL_PLAYER_SPECTATOR_ONLY")
		
		EXIT
	ENDIF
	
	INT iLbdId
	ELO_WRITE_STAGES eEloStage = ELO_NOT_USED
	
	PRINTLN("WRITE_TO_DM_LEADERBOARD ") 
	IF NOT ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		TEXT_LABEL_31 groupHandle
		groupHandle = g_FMMC_STRUCT.tl31LoadedContentID
		
		// Write ELO at the end
		WRITE_TO_ELO_LEADERBOARD(eLbdSubMode, serverBDpassed, playerBDPassed, dmVarsPassed, ELO_WRITE_END, serverBDpassedLDB)
		
		SWITCH eLbdSubMode
		 
			CASE SUB_DM_FFA
				//*SCORE_COLUMN( AGG_SUM ) : COLUMN_ID_LB_RATIO_SCORE - LBCOLUMNTYPE_DOUBLE
			   // KILLS( AGG_SUM ) : COLUMN_ID_LB_KILLS - LBCOLUMNTYPE_INT32
			    //DEATHS( AGG_SUM ) : COLUMN_ID_LB_DEATHS - LBCOLUMNTYPE_INT32
			    //KILL_DEATH_RATIO( AGG_LAST ) : COLUMN_ID_LB_RATIO - LBCOLUMNTYPE_DOUBLE
			   // CASH( AGG_SUM ) : COLUMN_ID_LB_CASH - LBCOLUMNTYPE_INT32
			   // NUM_WINS( AGG_SUM ) : COLUMN_ID_LB_NUM_WINS - LBCOLUMNTYPE_INT64
			   //NUM_MATCHES( AGG_SUM ) : COLUMN_ID_LB_NUM_MATCHES - LBCOLUMNTYPE_INT32
			    //NUM_LOSES( AGG_SUM ) : COLUMN_ID_LB_NUM_MATCHES_LOST - LBCOLUMNTYPE_INT32
				
				categoryNames[0] = "Mission"
				uniqueIdentifiers[0] = groupHandle
				iLbdId = ENUM_TO_INT(LEADERBOARD_FREEMODE_DEATHMATCH)
				
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_DEATHMATCH,uniqueIdentifiers,categoryNames,1,-1,TRUE)
				
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
				
					// Write kills
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId, LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
					
					// Write deaths			
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
					
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
							PRINTLN("iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
						ENDIF				
					#ENDIF
					
					// Write wins				
					WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB, iLbdId)
					
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_ID,ENUM_TO_INT(g_mnMyRaceModel),0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_VEHICLE_ID,ENUM_TO_INT(g_mnMyRaceModel),0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to vehicle id ",ENUM_TO_INT(g_mnMyRaceModel))
					
					//1061136 UN_COMMENT WHEN 1098154
					LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID, g_MissionControllerserverBD_LB.iHashMAC, g_MissionControllerserverBD_LB.iMatchTimeID)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to column 7 matchHistoryID: ",g_MissionControllerserverBD_LB.iMatchTimeID," Hashed MAC: " , g_MissionControllerserverBD_LB.iHashMAC)
					
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
	//				NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Cash: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iCash) NET_NL()
					NET_PRINT_STRINGS(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: group handle",groupHandle) NET_NL()
				ENDIF
				categoryNames[0] = "SeasonId"
				uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
				
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
				
					// Write kills
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
					
					// Write deaths			
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
					
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
							PRINTLN("iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
						ENDIF				
					#ENDIF
					
					// Write wins				
					WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB)
					
					// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME,serverBDpassed.iFinishTime,0)
					
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_OVERALL_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_OVERALL_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_OVERALL_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_OVERALL_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
	//				NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Cash: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iCash) NET_NL()
					NET_PRINT_STRINGS(" -<>- WRITE_TO_DEATHMATCH_OVERALL_LEADERBOARD: group handle",groupHandle) NET_NL()
				ENDIF
			BREAK
			
			CASE SUB_DM_TEAM
			
				categoryNames[0] = "Mission"
				uniqueIdentifiers[0] = groupHandle
				iLbdId = ENUM_TO_INT(LEADERBOARD_FREEMODE_TEAM_DEATHMATCH)
				
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_TEAM_DEATHMATCH,uniqueIdentifiers,categoryNames,1,-1,TRUE)
					
					// Write score
					//LEADERBOARDS_WRITE_ADD_COLUMN(0,serverBDpassed.iTeamScore[ GET_PLAYER_TEAM(LocalPlayer)] )
				
					// Write kills
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
					
					// Write deaths			
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 3 COL_DEATHS ",playerBDPassed[iLocalPart].iDeaths)
					
					// Wins
					WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB, iLbdId)
					
					//1061136 UN_COMMENT WHEN 1098154
					LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID, g_MissionControllerserverBD_LB.iHashMAC, g_MissionControllerserverBD_LB.iMatchTimeID)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 7 matchHistoryID: ",g_MissionControllerserverBD_LB.iMatchTimeID," Hashed MAC: " , g_MissionControllerserverBD_LB.iHashMAC)
					
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
	//				NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Cash: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iCash) NET_NL()
					NET_PRINT_STRINGS(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: group handle",groupHandle) NET_NL()
					//mpGlobals.g_bFlushLBWrites = TRUE
				ENDIF
				categoryNames[0] = "SeasonId"
				uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_TEAM_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
				
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
				
					// Write kills
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
					
					// Write deaths			
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
					
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
							PRINTLN("iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
						ENDIF				
					#ENDIF
					
					// Write wins				
					WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB)
					
					// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME,serverBDpassed.iFinishTime,0)
					
					NET_PRINT(" -<>- WRITE_TO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
	//				NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Cash: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iCash) NET_NL()
					NET_PRINT_STRINGS(" -<>- WRITE_TO_TEAM_DEATHMATCH_OVERALL_LEADERBOARD: group handle",groupHandle) NET_NL()
				ENDIF
			BREAK
			
			CASE SUB_DM_VEH
				categoryNames[0] = "Mission"
				uniqueIdentifiers[0] = groupHandle
				iLbdId = ENUM_TO_INT(LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH)
				
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH,uniqueIdentifiers,categoryNames,1,-1,TRUE)
					
					// Write score
					//LEADERBOARDS_WRITE_ADD_COLUMN(0,serverBDpassed.iTeamScore[ GET_PLAYER_TEAM(LocalPlayer)] )
				
					// Write kills
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to column 1 ",playerBDPassed[iLocalPart].iKills )
					
					// Write deaths			
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to column 2 ",playerBDPassed[iLocalPart].iDeaths)
					
					// Wins
					WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB, iLbdId)
					
					//LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_VEHICLE_ID,ENUM_TO_INT(g_mnMyRaceModel),0)
					WRITE_VALUE_TO_LEADERBOARD_USED_FOR_RANK_PREDICTION(iLbdId,LB_INPUT_COL_VEHICLE_ID,ENUM_TO_INT(g_mnMyRaceModel),0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to column 8 ",ENUM_TO_INT(g_mnMyRaceModel))
					
					//1061136 UN_COMMENT WHEN 1098154
					LEADERBOARDS_WRITE_ADD_COLUMN_LONG(LB_INPUT_COL_MATCH_ID ,g_MissionControllerserverBD_LB.iHashMAC, g_MissionControllerserverBD_LB.iMatchTimeID)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to column 8 matchHistoryID: ",g_MissionControllerserverBD_LB.iMatchTimeID," Hashed MAC: " , g_MissionControllerserverBD_LB.iHashMAC)
					
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
	//				NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Cash: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iCash) NET_NL()
					NET_PRINT_STRINGS(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: group handle",groupHandle) NET_NL()
					//mpGlobals.g_bFlushLBWrites = TRUE
				ENDIF
				categoryNames[0] = "SeasonId"
				uniqueIdentifiers[0] = g_sMPTunables.iEloSeason
				IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_VEHICLE_DEATHMATCH_OVERALL,uniqueIdentifiers,categoryNames,1)
				
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
				
					// Write kills
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_KILLS,playerBDPassed[iLocalPart].iKills,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 1 COL_KILLS ",playerBDPassed[iLocalPart].iKills )
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iKills) < 0
							PRINTLN("iKills = ", TO_FLOAT(playerBDPassed[iLocalPart].iKills))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iKills ")
						ENDIF				
					#ENDIF
					
					// Write deaths			
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_DEATHS,playerBDPassed[iLocalPart].iDeaths,0)
					PRINTLN("LEADERBOARDS_WRITE_ADD_COLUMN writing to col # 2 COL_DEATHS ",playerBDPassed[iLocalPart].ideaths )
					
					#IF IS_DEBUG_BUILD
						IF TO_FLOAT(playerBDPassed[iLocalPart].iDeaths) < 0
							PRINTLN("iDeaths = ", TO_FLOAT(playerBDPassed[iLocalPart].iDeaths))
							SCRIPT_ASSERT("Trying to upload a dodgy value to DM leaderboard iDeaths ")
						ENDIF				
					#ENDIF
					
					// Write wins				
					WRITE_DEATHMATCH_WIN_TO_LEADERBOARD(serverBDpassed, eEloStage, serverBDpassedLDB)
					
					// Write time //NEED TO ADD TRACKING FOR THIS REF# 12345232312
					LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_TOTAL_TIME,serverBDpassed.iFinishTime,0)
					
					NET_PRINT(" -<>- WRITE_TO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: eLbdSubMode: ") NET_PRINT_INT(ENUM_TO_INT(eLbdSubMode)) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: Kills: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iKills) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: Deaths: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iDeaths) NET_NL()
					NET_PRINT(" -<>- WRITE_TO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: Ratio: ") NET_PRINT_FLOAT(playerBDPassed[iLocalPart].fRatio) NET_NL()
	//				NET_PRINT(" -<>- WRITE_TO_DEATHMATCH_LEADERBOARD: Cash: ") NET_PRINT_INT(playerBDPassed[iLocalPart].iCash) NET_NL()
					NET_PRINT_STRINGS(" -<>- WRITE_TO_VEHICLE_DEATHMATCH_OVERALL_LEADERBOARD: group handle",groupHandle) NET_NL()
				ENDIF
			BREAK
			DEFAULT
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("WRITE_TO_DEATHMATCH_LEADERBOARD, invalid race eLbdSubMode")
				#ENDIF
			BREAK
		ENDSWITCH
	ELSE
		NET_PRINT("Mission name or creator ID is invalid not uploading data!") NET_NL()
	ENDIF
ENDPROC

FUNC INT GET_SPECIFIED_LBD_PLAYER(ServerBroadcastData_Leaderboard &serverBDpassedLDB, PLAYER_INDEX playerID)
	INT i
	INT iRow
	FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
		IF serverBDpassedLDB.leaderBoard[i].playerID = playerID
			iRow = i
		ENDIF
	ENDFOR
	
	RETURN iRow
ENDFUNC

FUNC INT GET_PLAYER_POS(ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	IF GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0) = LocalPlayer
		RETURN 1
	ENDIF
	IF GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 1) = LocalPlayer
		RETURN 2
	ENDIF
	IF GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 2) = LocalPlayer
		RETURN 3
	ENDIF
	
	RETURN -1
ENDFUNC

