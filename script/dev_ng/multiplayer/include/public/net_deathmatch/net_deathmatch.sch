//╔═════════════════════════════════════════════════════════════════════════════╗
//║		net_deathmatch.sch														║
//╚═════════════════════════════════════════════════════════════════════════════╝	

USING "net_deathmatch_event_processing.sch"

PROC SERVER_DOES_RESTART_STUFF(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	INT iparticipant
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		FMMC_EOM_SERVER_RESET_PLAYER_COUNTS(dmVarsPassed.sPlayerCounts)
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
			FMMC_EOM_SERVER_PLAYER_PROCESSING(dmVarsPassed.sPlayerCounts, iparticipant)
		ENDREPEAT
		FMMC_EOM_SERVER_END_OF_FRAME_PROCESSING(dmVarsPassed.sPlayerCounts, serverBDpassed.sServerFMMC_EOM, FMMC_TYPE_DEATHMATCH, 0, 0.5, IS_THIS_TEAM_DEATHMATCH(serverBDPassed))
		IF SHOULD_RUN_NEW_VOTING_SYSTEM(dmVarsPassed.sEndOfMission.iQuitProgress, serverBDpassed.sServerFMMC_EOM.iVoteStatus)
			SERVER_JOB_VOTE_PROCESSING(g_sMC_serverBDEndJob)
		ENDIF
	ENDIF
	
	IF IS_THIS_A_ROUNDS_MISSION()
	AND GET_CLIENT_MISSION_STAGE(playerBDPassed) >= CLIENT_MISSION_STAGE_FIGHTING
	AND GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_CONOR_LBD	
		INT iActivePlayers
		INT iTeamCount[FMMC_MAX_TEAMS], iPartLoop
		PLAYER_INDEX playerID
		
		PRINTLN("[PLAYER_LOOP] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND")
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartLoop
			//If they are active
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Get player ID
				playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))	
				IF playerID != INVALID_PLAYER_INDEX()
					INT iTeam = playerBDPassed[iPartLoop].iTeam
					//Not an array over run
					IF iTeam != -1
					AND iTeam < FMMC_MAX_TEAMS
					AND NOT IS_PLAYER_SCTV(playerID)
					AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(playerID)
						//Incrament the count of that team
						iTeamCount[iTeam]++
						PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND iTeamCount[", iTeam, "] = ", iTeamCount[iTeam])
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		INT iActiveTeams
		
		//Loop the number of teams
		REPEAT g_FMMC_STRUCT.iNumberOfTeams iPartLoop
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND -----------------")
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND iTeamCount[", iPartLoop, "] = ", iTeamCount[iPartLoop])
			PRINTLN("[TS] - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "] = ", g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop])
			
			IF iTeamCount[iPartLoop] > 0
				iActiveTeams++
			ENDIF
			
			iActivePlayers += iTeamCount[iPartLoop]
			
			IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
				//If the team count is less than the min
				IF g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop] > 0 // If the option is set.
					IF iTeamCount[iPartLoop] < g_FMMC_STRUCT.iNumPlayersPerTeam[iPartLoop]
						PRINTLN("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iTeamCount[", iPartLoop, "] < g_FMMC_STRUCT.iNumPlayersPerTeam[", iPartLoop, "] Setting SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND")					
						SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)				
					ENDIF
				ENDIF
			ENDIF
						
		ENDREPEAT
		
		IF iActivePlayers < g_FMMC_STRUCT.iMinNumParticipants
			PRINTLN("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() -  iActivePlayers: ", iActivePlayers, " Setting SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND")
			#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iActivePlayers: ", iActivePlayers, " Setting SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND")#ENDIF
			SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)	
		ENDIF
		
		IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
			IF iActiveTeams <= 1
				PRINTLN("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() -  iActiveTeams: ", iActiveTeams, " Setting SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND")
				#IF USE_FINAL_PRINTS PRINTLN_FINAL("[TS] * - ARE_TEAM_NUMBERS_TOO_LOW_FOR_NEXT_ROUND() - iActiveTeams: ", iActiveTeams, " Setting SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND")#ENDIF
				SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)				
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC STORE_JOB_CASH(ServerBroadcastData &serverBDPassed, PlayerBroadcastData &playerBDPassed[] #IF IS_DEBUG_BUILD , SHARED_DM_VARIABLES &dmVarsPassed #ENDIF )
//	IF HAS_DM_STARTED(serverBDPassed)
		IF NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
			IF playerBDPassed[iLocalPart].iJobCash <> g_iMyJobCash
				IF g_iMyJobCash > playerBDPassed[iLocalPart].iJobCash
					playerBDPassed[iLocalPart].iJobCash = g_iMyJobCash
					PRINTLN("STORE_JOB_CASH iJobCash = ", playerBDPassed[iLocalPart].iJobCash)
				ENDIF
			ENDIF
		
			// Add info to cash widgets
			#IF IS_DEBUG_BUILD
				IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
					IF dmVarsPassed.debugDmVars.iCashEnd <> (playerBDPassed[iLocalPart].iJobCash - dmVarsPassed.debugDmVars.iCashDuring)
						dmVarsPassed.debugDmVars.iCashEnd = (playerBDPassed[iLocalPart].iJobCash - dmVarsPassed.debugDmVars.iCashDuring)
					ENDIF
				ELSE
					IF dmVarsPassed.debugDmVars.iCashDuring <> playerBDPassed[iLocalPart].iJobCash 
						dmVarsPassed.debugDmVars.iCashDuring = playerBDPassed[iLocalPart].iJobCash
					ENDIF
				ENDIF
				IF dmVarsPassed.debugDmVars.iCashTotal <> playerBDPassed[iLocalPart].iJobCash
					dmVarsPassed.debugDmVars.iCashTotal = playerBDPassed[iLocalPart].iJobCash
				ENDIF
			#ENDIF
		ENDIF
//	ENDIF
ENDPROC

PROC STORE_STARTING_XP(SHARED_DM_VARIABLES &dmVarsPassed)
	dmVarsPassed.iXPAtJobStart = GET_PLAYER_XP(LocalPlayer)
	PRINTLN(" STORE_STARTING_XP  --- ", dmVarsPassed.iXPAtJobStart)
ENDPROC

PROC STORE_BETTING_WINNINGS(ServerBroadcastData &serverBDPassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDPassedLDB)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		PRINTLN("STORE_BETTING_WINNINGS = ", playerBDPassed[iLocalPart].iBets, "  iBestPlayerWinningTeam = ", serverBDpassed.iBestPlayerWinningTeam)
		playerBDPassed[iLocalPart].iBets = BROADCAST_BETTING_MISSION_FINISHED(INT_TO_PLAYERINDEX(serverBDpassed.iBestPlayerWinningTeam))
	ELSE
		PRINTLN("STORE_BETTING_WINNINGS = ", playerBDPassed[iLocalPart].iBets, "  iPlayer = ", NATIVE_TO_INT(GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0)))
		playerBDPassed[iLocalPart].iBets = BROADCAST_BETTING_MISSION_FINISHED(GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0))
	ENDIF
ENDPROC

// -----------------------------------		INITIAL FADE STAGE

PROC GOTO_INITIAL_STAGE(PlayerBroadcastData &playerBDPassed[], INT iInitialStageGoto)
	#IF IS_DEBUG_BUILD
		SWITCH iInitialStageGoto
			CASE INITIAL_FADE_STAGE_START 				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_FADE_STAGE_START  ", tl31ScriptName) NET_NL() 		BREAK
			CASE INITIAL_FADE_STAGE_FADE_OUT 			NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_FADE_STAGE_FADE_OUT  ", tl31ScriptName) NET_NL() 	BREAK
			CASE INITIAL_FADE_STAGE_FADED_OUT			NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_FADE_STAGE_FADED_OUT  ", tl31ScriptName) NET_NL() 	BREAK	
			CASE INITIAL_FADE_STAGE_OBJ					NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_FADE_STAGE_OBJ  ", tl31ScriptName) NET_NL() 		BREAK	
			CASE INITIAL_FADE_STAGE_FADE_IN 			NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_FADE_STAGE_FADE_IN  ", tl31ScriptName) NET_NL() 	BREAK		
			CASE INITIAL_FADE_STAGE_FADED_IN 			NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_FADE_STAGE_FADED_IN  ", tl31ScriptName) NET_NL() 	BREAK
			
			CASE INITIAL_CAMERA_TEAM_OVERVIEW 			NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_CAMERA_TEAM_OVERVIEW  ", tl31ScriptName) NET_NL() 	BREAK
			CASE INITIAL_CAMERA_OVERVIEW 				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_CAMERA_OVERVIEW  ", tl31ScriptName) NET_NL() 		BREAK
			CASE INITIAL_CAMERA_PLAYER_CUT 				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_CAMERA_PLAYER_CUT  ", tl31ScriptName) NET_NL() 		BREAK
			CASE INITIAL_CAMERA_BLEND_OUT 				NET_PRINT_TIME() NET_PRINT_STRINGS("GOTO_INITIAL_STAGE  >>> INITIAL_CAMERA_BLEND_OUT  ", tl31ScriptName) NET_NL() 		BREAK
		ENDSWITCH
	#ENDIF
	
	playerBDPassed[iLocalPart].iInitialFadeProgress = iInitialStageGoto
ENDPROC

PROC SET_DM_FINISHED_BOOL(PlayerBroadcastData &playerBDPassed[])
	IF playerBDPassed[iLocalPart].bFinishedDeathmatch = FALSE
		PRINTLN("SET_DM_FINISHED_BOOL ")
		playerBDPassed[iLocalPart].bFinishedDeathmatch = TRUE
	ENDIF
ENDPROC

///Determines whether the player the winning or losing postfx:
///Win:
///    - Player/team came first.
///Lose:
///    - Player/team didn't come first.
FUNC BOOL SHOULD_POSTFX_BE_WINNER_VERSION(PlayerBroadcastData &playerBDpassed[], ServerBroadcastData &serverBDpassed)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		IF GET_TEAM_RANK(serverBDpassed, playerBDPassed[iLocalPart].iTeam, serverBDpassed.iNumberOfTeams) = 1
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()) = 1
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS(ServerBroadcastData &serverBDPassed)
	IF serverBDpassed.iAverageRank = -1
		serverBDpassed.iAverageRank = GET_AVERAGE_RANK_OF_PLAYERS_IN_JOB(serverBDpassed.iNumDmStarters)
		PRINTLN("SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS, serverBDpassed.iNumDmStarters = ", serverBDpassed.iNumDmStarters)
		PRINTLN("SERVER_GRABS_AVERAGE_RANK_OF_DEATHMATCH_PLAYERS, serverBDpassed.iAverageRank = ", serverBDpassed.iAverageRank)
	ENDIF
ENDPROC

FUNC BOOL HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
//	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM TRUE 1")
		RETURN TRUE
	ENDIF	
	
	IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM, RETURN TRUE, IS_TEAM_DEATHMATCH ", IS_THIS_TEAM_DEATHMATCH(serverBDPassed))
		RETURN TRUE
	ENDIF

	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iParticipant)
//				IF serverBDpassed.iBalancedTeam[iParticipant] = -1
				IF GET_PLAYER_TEAM(playerId) = -1
				AND GET_PLAYER_TEAM(playerId) < MAX_NUM_DM_TEAMS
 					PRINTLN("Printing player teams in case it get stuck ", iParticipant, "( ", GET_PLAYER_TEAM(playerId), " ) ")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

//+-----------------------------------------------------------------------------+
//¦				LATE JOINING TDM												¦
//+-----------------------------------------------------------------------------+

FUNC BOOL HAS_CLIENT_JOINED_LATE(ServerBroadcastData &serverBDpassed)
	
	IF HAS_DM_STARTED(serverBDpassed)
	
		PRINTLN("HAS_DM_STARTED")
	
		RETURN TRUE
	ENDIF
	
//	IF DID_PLAYER_ENTER_CORONA_LATE(LocalPlayer)
//	
//		PRINTLN("DID_PLAYER_ENTER_CORONA_LATE")
//	
//		RETURN TRUE
//	ENDIF
	
//	IF HAS_TEAM_BALANCING_FINISHED(serverBDpassed)
//	
//		PRINTLN("HAS_TEAM_BALANCING_FINISHED")
//	
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_BIT_FOR_LATE_TDM_JOINERS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	IF HAS_CLIENT_JOINED_LATE(serverBDpassed)
		PRINTLN("SET_BIT_FOR_LATE_TDM_JOINERS ")
		SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_JOINED_LATE)
	ENDIF
ENDPROC

FUNC BOOL IS_LATE_JOINING_BIT_SET(PlayerBroadcastData &playerBDPassed[])

	RETURN IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_JOINED_LATE)
ENDFUNC

FUNC BOOL HAS_LATE_CLIENT_BEEN_GIVEN_TEAM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	INT iTeamToCheck
	INT iTeamLoop
	PARTICIPANT_INDEX tempPart
	INT iparticipant
	PLAYER_INDEX playerId
	
	//Ignore if impromptu
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		RETURN TRUE
	ENDIF
	
	// Ignore if TDM
	IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		PRINTLN("HAS_LATE_CLIENT_BEEN_GIVEN_TEAM BYPASSED, NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed) ")
		RETURN TRUE
	ENDIF
	
	// Ignore if player joined on time
	IF NOT IS_LATE_JOINING_BIT_SET(playerBDPassed)
		PRINTLN("HAS_LATE_CLIENT_BEEN_GIVEN_TEAM BYPASSED, NOT IS_LATE_JOINING_BIT_SET ")
		RETURN TRUE
	ENDIF
	
	SWITCH dmVarsPassed.iLateTeamProgress
	
		// Count the teams
		CASE LATE_TEAM_COUNT
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
				tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iparticipant))
//					IF NOT IS_PLAYER_SCTV(playerId)
					IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iParticipant)
						dmVarsPassed.iLateTeam = GET_PLAYER_TEAM(playerId)
						IF dmVarsPassed.iLateTeam <> -1
					
							dmVarsPassed.iTeamLateCount[dmVarsPassed.iLateTeam]++ 
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			PRINTLN("dmVarsPassed.iLateTeamProgress = LATE_TEAM_FIND_SMALLEST")
			dmVarsPassed.iLateTeamProgress = LATE_TEAM_FIND_SMALLEST
		BREAK
	
		// Find smallest
		CASE LATE_TEAM_FIND_SMALLEST
			// Get smallest team
			FOR iTeamToCheck = 0 TO (serverBDpassed.iNumberOfTeams -1)
				FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
					IF iTeamToCheck <> iTeamLoop
//						IF dmVarsPassed.iTeamLateCount[iTeamToCheck] <= dmVarsPassed.iTeamLateCount[iTeamLoop] 
						IF dmVarsPassed.iTeamLateCount[iTeamToCheck] < dmVarsPassed.iTeamLateCount[iTeamLoop] 
							dmVarsPassed.bSmallestTeam[iTeamToCheck] = TRUE
							PRINTLN("HAS_LATE_CLIENT_BEEN_GIVEN_TEAM, THIS_TEAM_IS_SMALLEST ", iTeamToCheck)
						// Put player on losing team if sizes match	
						ELIF dmVarsPassed.iTeamLateCount[iTeamToCheck] = dmVarsPassed.iTeamLateCount[iTeamLoop] 
							IF iTeamToCheck = serverBDpassed.iLosingTeam
								dmVarsPassed.bSmallestTeam[iTeamToCheck] = TRUE
								PRINTLN("HAS_LATE_CLIENT_BEEN_GIVEN_TEAM, PUT_PLAYER_ON_LOSING_TEAM ", iTeamToCheck)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDFOR
			
			PRINTLN("dmVarsPassed.iLateTeamProgress = LATE_TEAM_ASSIGN")
			dmVarsPassed.iLateTeamProgress = LATE_TEAM_ASSIGN
		BREAK
		
		// Give team based on smallest
		CASE LATE_TEAM_ASSIGN
			FOR iTeamLoop = 0 TO (serverBDpassed.iNumberOfTeams -1)
				IF dmVarsPassed.bSmallestTeam[iTeamLoop] = TRUE
					SET_LOCAL_PLAYER_TEAM(iTeamLoop)
					PRINTLN("HAS_LATE_CLIENT_BEEN_GIVEN_TEAM_RETURN_TRUE ", iTeamLoop)
					
					RETURN TRUE
				ENDIF
			ENDFOR
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC ASSIGN_LOCAL_PLAYER_TEAM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	PRINTLN("ASSIGN_LOCAL_PLAYER_TEAM CALLED")
	INT iMyPartID = iLocalPart

	IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	OR IS_LATE_JOINING_BIT_SET(playerBDPassed)
		IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			EXIT
		
		ENDIF
	ENDIF
	
	INT iMyGBD  = NATIVE_TO_INT(LocalPlayer)
	
	INT iTeam 
	
	IF NOT IS_BIT_SET(playerBDPassed[iMyPartID].iClientBitSet, CLIENT_BITSET_GOT_TEAM)
//		IF NOT bIsSCTV
		IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, LocalPlayer, iMyPartID)
			IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				iTeam = GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iTeamChosen
				SET_LOCAL_PLAYER_TEAM(iTeam)
				PRINTLN("ASSIGN_LOCAL_PLAYER_TEAM, corona iTeamChosen ", GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iTeamChosen)
				PRINTLN("ASSIGN_LOCAL_PLAYER_TEAM, SET_PED_CAN_BE_TARGETTED_BY_TEAM, false ", iTeam)
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeam, FALSE)
				SET_BIT(playerBDPassed[iMyPartID].iClientBitSet, CLIENT_BITSET_GOT_TEAM)
			ELSE
				//-- Boss V Boss DM
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					//-- Im a boss
				 	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM)
						//-- I started the Boss V Boss DM
						SET_LOCAL_PLAYER_TEAM(0)
						SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, 0, FALSE)
						SET_BIT(playerBDPassed[iMyPartID].iClientBitSet, CLIENT_BITSET_GOT_TEAM)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] I'M A BOSS AND eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM SET FOR ME - TEAM 0")
					ELIF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM)
						//-- I accepted the Boss V Boss DM
						SET_LOCAL_PLAYER_TEAM(1)
						SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, 1, FALSE)
						SET_BIT(playerBDPassed[iMyPartID].iClientBitSet, CLIENT_BITSET_GOT_TEAM)
						CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] I'M A BOSS AND eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM SET FOR ME - TEAM 1")
					ELSE
						// Don't know if I initiated / accepted!
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] I'M A BOSS BUT DON'T KNOW IF I INITIATED OR ACCEPTED BOSSVBOSS DM!")
							NET_SCRIPT_ASSERT("BOSS Vs BOSS - DIDN'T FIND TEAM FOR BOSS")
						#ENDIF
					ENDIF
				ELSE
					//-- I'm not a boss, did my boss initiate or accept?
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE)
						PLAYER_INDEX playerMyBoss =  GB_GET_LOCAL_PLAYER_GANG_BOSS()
						IF playerMyBoss != INVALID_PLAYER_INDEX()
							IF GB_IS_GLOBAL_CLIENT_BIT0_SET(playerMyBoss, eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM)
								//-- My Boss started the Boss V Boss DM
								SET_LOCAL_PLAYER_TEAM(0)
								SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, 0, FALSE)
								SET_BIT(playerBDPassed[iMyPartID].iClientBitSet, CLIENT_BITSET_GOT_TEAM)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] MY BOSS ", GET_PLAYER_NAME(playerMyBoss), " HAS eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM SET - TEAM 0")
							ELIF GB_IS_GLOBAL_CLIENT_BIT0_SET(playerMyBoss, eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM)
								//-- My Boss Accepted the boss V boss DM
								SET_LOCAL_PLAYER_TEAM(1)
								SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, 1, FALSE)
								SET_BIT(playerBDPassed[iMyPartID].iClientBitSet, CLIENT_BITSET_GOT_TEAM)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] MY BOSS ", GET_PLAYER_NAME(playerMyBoss), " HAS eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM SET - TEAM 1")
							ENDIF
						ELSE
							//-- Boss invalid
							#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] MY BOSS IS INVALID!")
								NET_SCRIPT_ASSERT("BOSS Vs BOSS TEAM - My Boss is invalid!")
							#ENDIF
						ENDIF
					ELSE
						//-- Not in a gang
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [ASSIGN_LOCAL_PLAYER_TEAM] I'M NOT IN A GANG!")
							NET_SCRIPT_ASSERT("BOSS Vs BOSS TEAM - Not in a gang!")
						#ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			 // FEATURE_GANG_BOSS
		ENDIF
	ENDIF
ENDPROC	

PROC MVP_BONUS_JOB_POINT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		INT iLocalTeam 		= playerBDPassed[iLocalPart].iTeam
		INT iLBDIndexWinner = GET_LEADERBOARD_INDEX_OF_TOP_PLAYER_ON_TEAM(serverBDpassedLDB, iLocalTeam)
		INT iWinningPlayer 	= NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[iLBDIndexWinner].playerID)
		
		INT iLocalPlayer 	= NETWORK_PLAYER_ID_TO_INT()
		IF iLocalPlayer <> -1
			PRINTLN("[JOB POINT], MVP_BONUS_JOB_POINT  iLBDIndexWinner  = ", iLBDIndexWinner, " iWinningPlayer = ", iWinningPlayer, " local Player = ", iLocalPlayer)
			GlobalplayerBD_FM[iLocalPlayer].sPlaylistVars.iMVPThisJob = iWinningPlayer
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL LOAD_DM_TEXT_BLOCK()
	REQUEST_ADDITIONAL_TEXT("DMATCH", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("FMMC", ODDJOB_TEXT_SLOT)
	
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("DMATCH", MISSION_TEXT_SLOT)
	AND HAS_THIS_ADDITIONAL_TEXT_LOADED("FMMC", ODDJOB_TEXT_SLOT)
		PRINTLN("[DM] LOAD_DM_TEXT_BLOCK || Text has loaded!")
		RETURN TRUE
	ENDIF
	
	PRINTLN("[DM] LOAD_DM_TEXT_BLOCK || Loading text...")
	RETURN FALSE
ENDFUNC

PROC CLEAR_PRINT_WITH_NUMBER(STRING sPrintToClear, INT iNumberToClear)
	BEGIN_TEXT_COMMAND_IS_MESSAGE_DISPLAYED(sPrintToClear)

	ADD_TEXT_COMPONENT_INTEGER(iNumberToClear)
	
	IF END_TEXT_COMMAND_IS_MESSAGE_DISPLAYED()
		CLEAR_THIS_PRINT(sPrintToClear)
	ENDIF
ENDPROC

PROC CLEAR_PRINT(STRING sPrintToClear)
	IF IS_THIS_PRINT_BEING_DISPLAYED(sPrintToClear)
		CLEAR_THIS_PRINT(sPrintToClear)
	ENDIF
ENDPROC

PROC CLEAR_HELP_IF_THIS_PRINT_DISPLAYED(STRING sHelpDisplayed)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpDisplayed)
		CLEAR_HELP()
	ENDIF
ENDPROC

PROC CLEAR_MISSION_HELP()
	CLEAR_HELP_IF_THIS_PRINT_DISPLAYED("DPAD_VIEW")
	CLEAR_HELP_IF_THIS_PRINT_DISPLAYED("DM_VOICE_TDM")
	CLEAR_HELP_IF_THIS_PRINT_DISPLAYED("DM_VOICE_FFA")
ENDPROC

PROC PROCESS_MISSION_TEXT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed, SHARED_DM_VARIABLES &dmVarsPassed)

	IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)

		SWITCH playerBDPassed.eClientLogicStage
		
			CASE CLIENT_MISSION_STAGE_INIT
			CASE CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING
			CASE CLIENT_MISSION_STAGE_HOLDING
			CASE CLIENT_MISSION_STAGE_SETUP
			
			
			BREAK
			
			CASE CLIENT_MISSION_STAGE_FIGHTING		
			
				IF IS_PLAYER_ON_IMPROMPTU_DM()
				
					IF IS_PLAYER_ABANDONING_IMPROMPTU(serverBDpassed)
						PRINT_HELP_FOREVER("DM_ONE_FAR")
					ELSE
						CLEAR_HELP_IF_THIS_PRINT_DISPLAYED("DM_ONE_FAR")
					ENDIF
				ELIF IS_PLAYER_ON_BOSSVBOSS_DM()
				ELIF IS_KING_OF_THE_HILL()
					IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_SCORE_EXPLAIN)
						STRING sScoreExplainText
						
						IF GET_KOTH_ACTIVE_HILL_COUNT(serverBDpassed.sKOTH_HostInfo, dmVarsPassed.sKOTH_LocalInfo) > 1
							IF IS_TEAM_DEATHMATCH()
								sScoreExplainText = "KOTH_HPTXT_TM"
							ELSE
								sScoreExplainText = "KOTH_HPTXT_FFA"
							ENDIF
						ELSE
							IF IS_TEAM_DEATHMATCH()
								sScoreExplainText = "KH_HPTXT_TM_S"
							ELSE
								sScoreExplainText = "KH_HPTXT_FFA_S"
							ENDIF
						ENDIF
						
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP(sScoreExplainText)
						ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sScoreExplainText)
							SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_SCORE_EXPLAIN)
							REINIT_NET_TIMER(dmVarsPassed.stSecondHTTimer)
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_KOTH_VEH)
						AND HAS_NET_TIMER_EXPIRED(dmVarsPassed.stSecondHTTimer, 10000)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							AND NOT IS_HELP_MESSAGE_FADING_OUT()
							
								INT iVehCheck
								BOOL bUsingVehHill
								
								FOR iVehCheck = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
									IF g_FMMC_STRUCT.sKotHData.sHillData[iVehCheck].iHillType = ciKOTH_HILL_TYPE__VEHICLE
										bUsingVehHill = TRUE
										BREAKLOOP
									ENDIF
								ENDFOR
								
								IF bUsingVehHill
									PRINT_HELP("KOTH_HPTXT_V")
								ELSE
									SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_KOTH_VEH)
								ENDIF
								
							ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTH_HPTXT_V")
								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_KOTH_VEH)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					// "As the Power Player you will see all enemy blips but the damage you receive will be doubled and your blip will appear larger to other players."
					DO_POWER_PLAYER_HELP(serverBDpassed, dmVarsPassed)					
				
					IF NUMBER_TIMES_STARTED_DM(dmVarsPassed) <= 3
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_DM_Disable_Default_HelpText)
					
						// "A kill is deducted for suicide in Vehicle Deathmatches."
						IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_SUICIDE_VEH_DM)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
									PRINT_HELP("DM_VEH_PEN")
								ENDIF
							ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_VEH_PEN")
								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_SUICIDE_VEH_DM)
							ENDIF
						ENDIF
						
						// "To quickly aim and fire at someone behind you, press R3 then L2 to target."
						IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
						OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed.iCurrentModSet, dmVarsPassed.sPlayerModifierData)
							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_AIM_HELP)
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("FM_DMTCH_LBEH")
								ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_DMTCH_LBEH")
									SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_AIM_HELP)
								ENDIF
							ENDIF
						ENDIF
					
						// "~s~Press ~DPAD_DOWN~ to view leaderboard."
						IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_HELP)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_SEEN_DPAD_LEADERBOARD)
									IF bLocalPlayerOK
										IF NOT IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)//INPUT_FRONTEND_DOWN)
											PRINT_HELP("DPAD_VIEW")
										ENDIF
									ENDIF
								ENDIF
							ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DPAD_VIEW")
								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_HELP)
							ENDIF
						ENDIF
						
						// (1006598)
						// "When enemies make a noise within your earshot they will be shown on the radar by a red blip. Be sure not to make too much sound when keeping out of enemies sight."
						IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_BLIP_HELP)	
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINTLN("PRINT_HELP(DM_BLP_HLP)")
									PRINT_HELP("DM_BLP_HLP")
								ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_BLP_HLP")
									SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_BLIP_HELP)
								ENDIF
							ENDIF
						ENDIF
						
						// (979130)
						IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_VOICE_HELP)	
						
							IF CONTENT_IS_USING_ARENA() 
								SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_VOICE_HELP)
							ELSE
						
								IF NETWORK_PLAYER_HAS_HEADSET(LocalPlayer)
									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
										IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
											IF IS_KING_OF_THE_HILL()
												// "In Team King of the Hill you can only chat with players in the same team."
												PRINTLN("PRINT_HELP(KOTH_VOICE_TM)")
												PRINT_HELP("KOTH_VOICE_TM")
											ELSE
												// "In Team Deathmatch you can only chat with players in the same team."
												PRINTLN("PRINT_HELP(DM_VOICE_TDM)")
												PRINT_HELP("DM_VOICE_TDM")
											ENDIF
										ELSE
											IF IS_KING_OF_THE_HILL()
												// "In King of the Hill you can chat with all players in the match."
												PRINTLN("PRINT_HELP(KOTH_VOICE_FFA)")
												PRINT_HELP("KOTH_VOICE_FFA")
											ELSE
												// "In Deathmatch you can only chat with nearby players."
												PRINTLN("PRINT_HELP(DM_VOICE_FFA)")
												PRINT_HELP("DM_VOICE_FFA")
											ENDIF
										ENDIF
									ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_VOICE_TDM")
										SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_VOICE_HELP)
									ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_VOICE_FFA")
										SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_VOICE_HELP)
									ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTH_VOICE_FFA")
										SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_VOICE_HELP)
									ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("KOTH_VOICE_TM")
										SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_VOICE_HELP)
									ENDIF
								ENDIF
								
							ENDIF
						ENDIF
						
						// "In deathmatch, each kill scores one point. A point is deducted for each suicide"
						IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_SCORE_EXPLAIN)
							IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)	
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("DM_SCORE_K")
								ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_SCORE_K")
									SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_SCORE_EXPLAIN)
								ENDIF
							ENDIF	
						ENDIF
						
						// "Setting Challenge"
						IF IS_PLAYLIST_SETTING_CHALLENGE_TIME()
							IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_CHALLENGE_TIME)	
								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
									PRINT_HELP("FMMC_PL_STCL",6000)
									SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_CHALLENGE_TIME)
								ENDIF
							ENDIF
						ENDIF
						
						// "When on a losing streak you can select a respawn perk to help you out."
						IF IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_LOSER)
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("DM_LOS_HLP")
							ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_LOS_HLP")
								CLEAR_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_LOSER)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK			
		ENDSWITCH
	ENDIF
ENDPROC

PROC TURN_SPINNER_OFF()
	IF BUSYSPINNER_IS_ON()
		//PRINTLN("TURN_SPINNER_OFF")
		BUSYSPINNER_OFF()
	ENDIF
ENDPROC

// Store clients highest kill streak this game
PROC STORE_LOCAL_HIGHEST_STREAK(SHARED_DM_VARIABLES &dmVarsPassed)
	IF GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iKillStreak > dmVarsPassed.iHighestKillStreak
		dmVarsPassed.iHighestKillStreak = GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iKillStreak
	ENDIF
ENDPROC

FUNC PED_BONETAG GET_THIS_PEDS_LAST_DAMAGED_BONE(PED_INDEX playerPed)
	PED_BONETAG damaged_bone
	IF GET_PED_LAST_DAMAGE_BONE(playerPed, damaged_bone) 
		RETURN damaged_bone
	ENDIF
	
	RETURN damaged_bone
ENDFUNC

FUNC BOOL ARE_ALL_CLIENTS_AT_BET_STAGE(PlayerBroadcastData &playerBDPassed[])

	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))	
			PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iParticipant)
				IF GET_PLAYER_CORONA_STATUS(playerId) < CORONA_STATUS_BETTING
				AND IS_THIS_PLAYER_IN_CORONA(playerId)
					PRINTLN("GET_PLAYER_CORONA_STATUS playerId ", NATIVE_TO_INT(playerId), " is at ", GET_PLAYER_CORONA_STATUS(playerId))
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC SERVER_SETS_EVERYONE_BETTING_BIT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ALL_PLAYERS_ON_BETTING)
		IF ARE_ALL_CLIENTS_AT_BET_STAGE(playerBDPassed)
			PRINTLN("SERV_BITSET_ALL_PLAYERS_ON_BETTING, ")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_ALL_PLAYERS_ON_BETTING)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_SERVER_COUNTED_FINISHING_TEAMS(ServerBroadcastData &serverBDpassed)
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TEAMS_COUNTED)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SERVER_CREATE_DM_ENTITIES(ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_MADE_ENTITIES)
		IF LOAD_AND_CREATE_ALL_FMMC_ENTITIES(serverFMMCPassed, dmVarsPassed.sRGH, FMMC_TYPE_DEATHMATCH)

			PRINTLN("SERVER_CREATE_DM_ENTITIES - All entities are now created!")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_MADE_ENTITIES)
			
			// Vehicles
			INT iVeh
			FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1
				IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niVehicle[iVeh])
					SET_BIT(serverBDPassed.iVehicleSpawnedBitset, iVeh)
					PRINTLN("[LM][DM_ENTITY_RESPAWN][SERVER_CREATE_DM_ENTITIES] - Successfully (inital) SPAWNED iVeh: ", iVeh)
				ENDIF
			ENDFOR
		ELSE
			PRINTLN("LOAD_AND_CREATE_ALL_FMMC_ENTITIES returning FALSE")
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_SETS_BIT_WHEN_TEAMS_COUNTED(ServerBroadcastData &serverBDpassed)

	#IF IS_DEBUG_BUILD
		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_SETS_BIT_WHEN_TEAMS_COUNTED() being called by machine that is not host! Host only command!")	
	#ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TEAMS_COUNTED)
		EXIT
	ENDIF
	
	IF HAS_DM_STARTED(serverBDpassed)
		IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		OR IS_TDM_USING_FFA_SCORING()
			PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], NOT IS_THIS_TEAM_DEATHMATCH, SERV_BITSET_TEAMS_COUNTED")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TEAMS_COUNTED)
		ELSE
			IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
				IF HAS_SERVER_FINISHED_SORTING_WINNING_TEAMS(serverBDpassed)
					IF HAS_SERVER_FINISHED_LBD_SORT(serverBDpassed)
						IF HAS_SERVER_PICKED_WINNING_TEAM(serverBDpassed)
							IF serverBDpassed.iWinningTeam <> -1
								serverBDpassed.iNumWinners = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iWinningTeam]
								PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], iNumWinners = ", serverBDpassed.iNumWinners)
							ENDIF
							IF serverBDpassed.iSecondTeam <> -1
								serverBDpassed.iTeam2Count = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iSecondTeam]
								PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], iTeam2Count = ",serverBDpassed.iTeam2Count)
							ENDIF
							IF serverBDpassed.iThirdTeam <> -1
								serverBDpassed.iTeam3Count = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iThirdTeam]
								PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], iThirdTeam = ", serverBDpassed.iTeam3Count)
							ENDIF
							IF serverBDpassed.iFourthTeam <> -1
								serverBDpassed.iTeam4Count = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iFourthTeam]
								PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], iFourthTeam = ", serverBDpassed.iTeam4Count)
							ENDIF
							PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], SERV_BITSET_TEAMS_COUNTED")
							SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TEAMS_COUNTED)
						ELSE
							PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], HAS_SERVER_PICKED_WINNING_TEAM = FALSE")
						ENDIF
					ELSE
						PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], HAS_SERVER_FINISHED_LBD_SORT = FALSE")
					ENDIF
				ELSE	
					PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], HAS_SERVER_FINISHED_SORTING_WINNING_TEAMS = FALSE")
				ENDIF
			ELSE
				IF serverBDpassed.iWinningTeam <> -1
					IF serverBDpassed.iNumWinners <> serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iWinningTeam]
						serverBDpassed.iNumWinners = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iWinningTeam]
						PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], [NOT_FINISHED] iNumWinners = ", serverBDpassed.iNumWinners)
					ENDIF
				ENDIF
				IF serverBDpassed.iSecondTeam <> -1
					IF serverBDpassed.iTeam2Count <> serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iSecondTeam]
						serverBDpassed.iTeam2Count = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iSecondTeam]
						PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], [NOT_FINISHED] iTeam2Count = ",serverBDpassed.iTeam2Count)
					ENDIF
				ENDIF
				IF serverBDpassed.iThirdTeam <> -1
					IF serverBDpassed.iTeam3Count <> serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iThirdTeam]
						serverBDpassed.iTeam3Count = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iThirdTeam]
						PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], [NOT_FINISHED] iThirdTeam = ", serverBDpassed.iTeam3Count)
					ENDIF
				ENDIF
				IF serverBDpassed.iFourthTeam <> -1
					IF serverBDpassed.iTeam4Count <> serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iFourthTeam]
						serverBDpassed.iTeam4Count = serverBDpassed.iNumberOfLBPlayers[serverBDpassed.iFourthTeam]
						PRINTLN("[SERVER_SETS_BIT_WHEN_TEAMS_COUNTED], [NOT_FINISHED] iFourthTeam = ", serverBDpassed.iTeam4Count)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_ONLY_ONE_TEAM_LEFT(ServerBroadcastData &serverBDpassed)

	IF serverBDpassed.iNumActiveTeams < MIN_NUM_DM_TEAMS
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_RESPAWN_SAFETY_TIMER(SHARED_DM_VARIABLES &dmVarsPassed)
	IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeRespawnStuckSafety)
		REINIT_NET_TIMER(dmVarsPassed.timeRespawnStuckSafety)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_CLIENT_MOVE_TO_RESULTS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)

	// Player is respawning
	IF IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_PLAYER_RESPAWNING)
	
		PRINTLN("SHOULD_CLIENT_MOVE_TO_RESULTS (FALSE) GET_RESPAWN_STATE = ", dmVarsPassed.iRaceRespawnState)
	
		RETURN FALSE
	ENDIF
	
	// 1538775 We want to wait until players have been sorted into spectators first
	IF GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_INIT
	
		RETURN FALSE
	ENDIF
	
	IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
	
		PRINTLN("SHOULD_CLIENT_MOVE_TO_RESULTS, DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE")

		RETURN TRUE
	ENDIF

	#IF IS_DEBUG_BUILD
	
		// Debug ended
		IF HAS_DM_DEBUG_ENDED(serverBDpassed)
			PRINTLN("----------> SHOULD_CLIENT_MOVE_TO_RESULTS, [HAS_DM_DEBUG_ENDED] ", tl31ScriptName) 
			
			RETURN TRUE	
		ENDIF

		IF bKeepLocalRacing = TRUE

			RETURN FALSE
		ENDIF

	#ENDIF
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		PRINTLN("SHOULD_CLIENT_MOVE_TO_RESULTS, HAS_DEATHMATCH_FINISHED")
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
			IF playerBDPassed[iLocalPart].iFinishedAtTime = 0
			AND HAS_DEATHMATCH_FINISHED(serverBDpassed)
				PRINTLN("[LM][FINISH_TIME] - (3) Assigning Finished at time: ", (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000)
				playerBDPassed[iLocalPart].iFinishedAtTime = (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000
			ENDIF
			
			RETURN IS_READY_FOR_LAST_ALIVE_LEADERBOARD(playerBDPassed)
		ENDIF
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLIENT_MOVES_TO_FIX_DEAD_WHEN_DM_OVER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_MOVED_TO_FIX_DEAD_PEOPLE)
		IF SHOULD_CLIENT_MOVE_TO_RESULTS(serverBDpassed, playerBDPassed, dmVarsPassed)
		AND GET_CLIENT_MISSION_STAGE(playerBDPassed) != CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
			IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
			AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				TRIGGER_CELEBRATION_PRE_LOAD(dmVarsPassed.sCelebrationData, TRUE)
			ENDIF
			
			CLEANUP_RESPAWN_SAFETY_TIMER(dmVarsPassed)
			DO_ROAMING_SPECTATOR_END_LOGIC(dmVarsPassed)
			
			GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE)
		ENDIF
	ENDIF
ENDPROC

// Ensure client variables all start at 0
PROC INITIALISE_CLIENT_BROADCAST_DATA(PlayerBroadcastData &playerBDPassed[])
	playerBDPassed[iLocalPart].iScore 					= 0
	playerBDPassed[iLocalPart].iKills 					= 0
//	playerBDPassed[iLocalPart].iAssists				= 0
	playerBDPassed[iLocalPart].iHeadshots				= 0
	playerBDPassed[iLocalPart].iDeaths					= 0
	playerBDPassed[iLocalPart].iSuicides				= 0
ENDPROC

PROC DOUBLE_XP_FINAL_KILL(ServerBroadcastData &serverBDpassed)
	IF IS_ONLY_ONE_KILL_REMAINING(serverBDpassed)
		IF g_b_DoubleXpFinalKill = FALSE
			PRINTLN("DOUBLE_XP_FINAL_KILL")
			g_b_DoubleXpFinalKill = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_SET_MATCH_WINNER(ServerBroadcastData &serverBDpassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed) 
	AND (HAS_ANYONE_REACHED_TARGET_SCORE(serverBDpassed) 
		OR CONTENT_IS_USING_ARENA()
		OR (IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES) AND serverBDpassed.bAllPlayersHaveAFinishTime))
	#IF IS_DEBUG_BUILD
	OR HAS_DM_DEBUG_ENDED(serverBDpassed)
	#ENDIF
		IF g_iLastKillerId <> -1
		
			PLAYER_INDEX playerId
			playerId = INT_TO_PLAYERINDEX(g_iLastKillerId)
			INT iTeam
			
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				iTeam = GET_PLAYER_TEAM(playerId)
				
				IF iTeam = serverBDpassed.iWinningTeam
					IF serverBDpassed.playerMatchWinner = INVALID_PLAYER_INDEX()
						serverBDpassed.playerMatchWinner = INT_TO_PLAYERINDEX(g_iLastKillerId)
						PRINTLN("SERVER_SET_MATCH_WINNER, MATCH_WINNER = ", NATIVE_TO_INT(serverBDpassed.playerMatchWinner))
					ENDIF
				ENDIF

			ELSE
				PRINTLN("SERVER_SET_MATCH_WINNER, iTeam =  ", iTeam, " serverBDpassed.iWinningTeam = ", serverBDpassed.iWinningTeam)
			ENDIF
		ELSE
			PRINTLN("SERVER_SET_MATCH_WINNER, g_iLastKillerId =  ", g_iLastKillerId)
		ENDIF
		
		// Added this new variable, stuff above is old 
		IF serverBDpassed.playerDMWinner = INVALID_PLAYER_INDEX()
			serverBDpassed.playerDMWinner = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0)
			PRINTLN("SERVER_SET_MATCH_WINNER, playerDMWinner =  ", GET_PLAYER_NAME(serverBDpassed.playerDMWinner))
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL INSUFFICIENT_PLAYERS(ServerBroadcastData &serverBDpassed)
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_OK_DISTANCE_CHECK)
			IF serverBDpassed.iNumActivePlayers = 1
			
				RETURN TRUE
			ENDIF
		ENDIF
	ELIF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		IF IS_ONLY_ONE_TEAM_LEFT(serverBDpassed)
			
			RETURN TRUE
		ENDIF
	ELSE 
		IF serverBDpassed.iNumActivePlayers = 1
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SERVER_SETS_ONE_PLAYER_LEFT_BIT(ServerBroadcastData &serverBDpassed #IF IS_DEBUG_BUILD , SHARED_DM_VARIABLES &dmVarsPassed #ENDIF)
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_ONE_PLAYER_KICK)
		IF HAS_DM_STARTED(serverBDpassed)
			IF INSUFFICIENT_PLAYERS(serverBDpassed)
				#IF IS_DEBUG_BUILD
				IF GlobalServerBD_DM.bOnePlayerDeathmatch = FALSE
				AND dmVarsPassed.debugDmVars.bDontEndOnePlayer = FALSE
				#ENDIF
					PRINTLN("[DM_HOST_END] ----------> SERVER_SETS_ONE_PLAYER_LEFT_BIT, [ONLY ONE PLAYER LEFT], SERV_BITSET_ONE_PLAYER_KICK ", tl31ScriptName)
					SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_ONE_PLAYER_KICK)
				#IF IS_DEBUG_BUILD
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_FFA
		EXIT
	ENDIF

	INT i
	FLOAT iTeam0Num, iTeam1Num, iTeam2Num, iTeam3Num
	PLAYER_INDEX thePlayerID
	PED_INDEX pedLocal

	FOR i = 0 TO serverBDpassed.iNumDmStarters
	
		thePlayerID = INT_TO_PLAYERINDEX(i)
		pedLocal = GET_PLAYER_PED(thePlayerID)
		
		IF DOES_ENTITY_EXIST(pedLocal)
			IF NOT IS_PED_DEAD_OR_DYING(pedLocal)
				IF playerBDPassed[i].iTeam = 0
					serverBDpassed.vTeamCentreStartPoint[0] += GET_ENTITY_COORDS(pedLocal)
					iTeam0Num++
					PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam0Num = ", iTeam0Num)
				ELIF playerBDPassed[i].iTeam = 1
					serverBDpassed.vTeamCentreStartPoint[1] += GET_ENTITY_COORDS(pedLocal)
					iTeam1Num++
					PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam1Num = ", iTeam1Num)
				ELIF playerBDPassed[i].iTeam = 2
					serverBDpassed.vTeamCentreStartPoint[2] += GET_ENTITY_COORDS(pedLocal)
					iTeam2Num++
					PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam2Num = ", iTeam2Num)
				ELIF playerBDPassed[i].iTeam = 3
					serverBDpassed.vTeamCentreStartPoint[3] += GET_ENTITY_COORDS(pedLocal)
					iTeam3Num++
					PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, iTeam3Num =", iTeam3Num)
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	serverBDpassed.vTeamCentreStartPoint[0] = (serverBDpassed.vTeamCentreStartPoint[0]/iTeam0Num)
	serverBDpassed.vTeamCentreStartPoint[1] = (serverBDpassed.vTeamCentreStartPoint[1]/iTeam1Num)
	serverBDpassed.vTeamCentreStartPoint[2] = (serverBDpassed.vTeamCentreStartPoint[2]/iTeam2Num)
	serverBDpassed.vTeamCentreStartPoint[3] = (serverBDpassed.vTeamCentreStartPoint[3]/iTeam3Num)
	PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[0] = ", serverBDpassed.vTeamCentreStartPoint[0])
	PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[1] = ", serverBDpassed.vTeamCentreStartPoint[1])
	PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[2] = ", serverBDpassed.vTeamCentreStartPoint[2])
	PRINTLN("SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS, serverBDpassed.vTeamCentreStartPoint[3] = ", serverBDpassed.vTeamCentreStartPoint[3])
	
ENDPROC

FUNC BOOL IS_TEAM_SCORE_HIGHER_THAN_EVERYONE(ServerBroadcastData &serverBDpassed,SHARED_DM_VARIABLES &dmVarsPassed,INT iteam)

	INT iTeam2

	REPEAT MAX_NUM_DM_TEAMS iTeam2
		IF serverBDpassed.iTeamScore[iTeam] <= dmVarsPassed.iHighestTeamScoreStored[iTeam2]
			RETURN FALSE
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_SERVER_STORED_FINISH_POSITIONS(ServerBroadcastData &serverBDPassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STORED_FINISH_POSITIONS)
ENDFUNC

PROC SERVER_STORES_FINISH_POSITIONS(ServerBroadcastData &serverBDPassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	IF (IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType) OR NOT IS_THIS_LEGACY_DM_CONTENT())
	AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_FINISHED)
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_STORED_FINISH_POSITIONS)
		IF HAS_SERVER_FINISHED_LBD_SORT(serverBDpassed)
		AND IS_READY_FOR_LAST_ALIVE_LEADERBOARD(playerBDPassed)
	
			INT i, iPlayer
			PLAYER_INDEX playerId
		
			REPEAT NUM_NETWORK_PLAYERS i
			
				IF serverBDpassedLDB.leaderBoard[i].playerID <> INVALID_PLAYER_INDEX()
				
					playerId = serverBDpassedLDB.leaderBoard[i].playerID
					iPlayer = NATIVE_TO_INT(playerId)
				
					serverBDPassed.iFinishingPos[iPlayer] = (i +1) // No 0th Pos
					
					PRINTLN("SERVER_STORES_FINISH_POSITIONS, iFinishingPos for  ", GET_PLAYER_NAME(playerId), " is iFinishingPos = ", serverBDPassed.iFinishingPos[iPlayer])
					
				ENDIF	
			ENDREPEAT	
		
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_STORED_FINISH_POSITIONS)
		ENDIF
	ENDIF
ENDPROC

PROC SERVER_PROCESSING(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed #IF IS_DEBUG_BUILD , BOOL bDebug #ENDIF , ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	#IF IS_DEBUG_BUILD
		NET_SCRIPT_HOST_ONLY_COMMAND_ASSERT("SERVER_PROCESSING called on client when it's a host only function")
	#ENDIF

	INT iParticipant
	INT iNumActivePlayers, iNumPlayers, iNumOffScriptButFinished, iNumActiveTeams, iTeam, iTeamBitset, iNumAlivePlayers, iNumAliveTeams
	INT iBestPlayerWinningTeam, iMVP
	INT iBestRank = 99
	INT iBestRankMVP = 99
	INT iTeamsAdded
	
	INT iNumStartingPlayers[MAX_NUM_DM_TEAMS]
	
	// Highest Score
//	INT iHighestPlayerScoreTemp = 0
//	INT ipartWithHighestScore = -1
	INT iTeamWithHigestScore = -1
	INT iBestTeamScoreTemp
	
	FLOAT fKOTH_TeamScores[MAX_NUM_DM_TEAMS]
	
	BOOL bEveryoneOnLbd = TRUE
	BOOL bEveryoneReady = TRUE
	BOOL bEveryoneFinished = TRUE
	INT iNumSpectators
	
	BOOL bLastManStanding = IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	INT iLoop
	
	// Set the corona ready flag 
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
		IF IS_CORONA_READY_TO_START_WITH_JOB()
			PRINTLN("SERVER_PROCESSING, SERV_BITSET2_CORONA_READY")
			SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
		ENDIF
	ENDIF
	
	//////////////////////////////        	TDM SCORES RESET
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
//	AND NOT SERVER_CHECKS_HAS_DM_ENDED(serverBDpassed, TRUE #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF)
		REPEAT MAX_NUM_DM_TEAMS iTeam
			serverBDpassed.iTeamScore[iTeam] 	= 0
			serverBDpassed.iTeamScoreForLBD[iTeam] 	= 0
			serverBDpassed.iTeamDeaths[iTeam]	= 0
			serverBDpassed.fTeamRatio[iTeam] 	= 0
			serverBDpassed.iTeamXP[iTeam] 		= 0
		ENDREPEAT
	ENDIF
	
	//Calculates Team spawn locations
	IF HAS_SERVER_CHECKED_EVERYONE_READY(serverBDpassed)
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TEAM_CPS_DONE)
			SERVER_CALCULATE_CENTRE_POINT_FOR_TEAMS(serverBDpassed, playerBDPassed)
			PRINTLN("INIT_SERVER_DATA, CENTRE POINTS SORTED")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TEAM_CPS_DONE)
		ENDIF
	ENDIF
		
	SERVER_COUNTS_PICKUPS(serverBDpassed, dmVarsPassed)
	
	INT iBossKills[2]
	INT iBossKillSelf[2]
	INT iOtherTeam

	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant	
	
		IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
			IF serverBDpassedLDB.leaderBoard[iParticipant].playerID <> INVALID_PLAYER_INDEX()
				// Grab best player winning team
				IF serverBDpassedLDB.leaderBoard[iParticipant].iTeam = serverBDpassed.iWinningTeam
					IF serverBDpassedLDB.leaderBoard[iParticipant].iRank < iBestRank
						iBestRank = serverBDpassedLDB.leaderBoard[iParticipant].iRank
						iBestPlayerWinningTeam = NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[iParticipant].playerID)
					ENDIF
				ENDIF
				// Grab MVP
				IF serverBDpassedLDB.leaderBoard[iParticipant].iRank < iBestRankMVP
					iBestRankMVP = serverBDpassedLDB.leaderBoard[iParticipant].iRank
					iMVP = NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[iParticipant].playerID)
				ENDIF
			ENDIF
		ENDIF
		
//		IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
//			IF serverBDpassedLDB.leaderBoard[iParticipant].playerID <> INVALID_PLAYER_INDEX()
//				COUNT_UP_DOWN_VOTES(serverBDpassedLDB.leaderBoard[iParticipant].playerID, serverBDpassed.iThumbBitset, serverBDpassed.iThumbUpCount, serverBDpassed.iThumbDownCount)
//			ENDIF
//		ENDIF
	
		// COUNT 1 Grab num folk who have finished	
		IF (serverBDpassedLDB.leaderBoard[iParticipant].playerID <> INVALID_PLAYER_INDEX())
			IF serverBDpassedLDB.leaderBoard[iParticipant].iParticipant <> -1
				IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(serverBDpassedLDB.leaderBoard[iParticipant].iParticipant))
					// Count off script
					//IF playerBDPassed[serverBDpassedLDB.leaderBoard[iParticipant].iParticipant].bFinishedDeathmatch
					//IF serverBDpassedLDB.leaderBoard[serverBDpassedLDB.leaderBoard[iParticipant].iParticipant].bStarted
//					IF serverBDpassedLDB.leaderBoard[iParticipant].bStarted
					IF IS_BIT_SET(serverBDpassedLDB.leaderBoard[iParticipant].iLbdBitSet, ciLBD_BIT_STARTED)
						iNumOffScriptButFinished++
					ENDIF
					//////////////////////////////        	TDM SCORES COUNT INACTIVE
					IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
						//	Store and reset players individual scores for those that left
						// -----------------------------------------------------------------------------------------------//
						PLAYER_INDEX playerId = serverBDpassedLDB.leaderBoard[iParticipant].playerID
						IF (playerId <> INVALID_PLAYER_INDEX())

							// Grab player team				
							iTeam = serverBDpassedLDB.leaderBoard[iParticipant].iTeam
							
							IF iTeam <> -1
							AND iTeam < MAX_NUM_DM_TEAMS
								IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iParticipant)
									IF serverBDpassed.iPlayersLastScore[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] <> 0
										// If a player leaves, store their score
										serverBDpassed.iPlayerWhoLeftScore[iTeam] += serverBDpassed.iPlayersLastScore[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant]
										// Rest their score now so that a new player in the same slot can contribute
										serverBDpassed.iPlayersLastScore[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] = 0
									ENDIF
									// Deaths
									IF serverBDpassed.iPlayersLastDeaths[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] <> 0
										serverBDpassed.iPlayerWhoLeftDeaths[iTeam] += serverBDpassed.iPlayersLastDeaths[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant]
										serverBDpassed.iPlayersLastDeaths[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] = 0
									ENDIF
									// Ratio
									IF serverBDpassed.fPlayersLastRatio[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] <> 0
										serverBDpassed.fPlayerWhoLeftRatio[iTeam] += serverBDpassed.fPlayersLastRatio[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant]
										serverBDpassed.fPlayersLastRatio[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] = 0
									ENDIF
									// RP
									IF serverBDpassed.iPlayersLastRP[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] <> 0
										// If a player leaves, store their score
										serverBDpassed.iPlayerWhoLeftRP[iTeam] += serverBDpassed.iPlayersLastRP[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant]
										// Rest their score now so that a new player in the same slot can contribute
										serverBDpassed.iPlayersLastRP[iTeam][serverBDpassedLDB.leaderBoard[iParticipant].iParticipant] = 0
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_NET_PLAYER_OK(PlayerId, FALSE)
			
				//////////////////////////////        	DM STARTED
				IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_DEATHMATCH_STARTED)
					IF playerBDPassed[iParticipant].eClientLogicStage >= CLIENT_MISSION_STAGE_FIGHTING	
					AND NOT IS_PLAYER_SCTV(PlayerId) // IMPORTANT
						SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_DEATHMATCH_STARTED)
						PRINTLN("[DEATHMATCH] SERVER_PROCESSING | Ready to start")
					ELSE
						PRINTLN("[DEATHMATCH] SERVER_PROCESSING | Participant ", iParticipant, " isn't ready to start called ", GET_PLAYER_NAME(PlayerId))
					ENDIF
				ENDIF
			
				IF IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iParticipant)
				
					iNumSpectators++
				ELSE
				
					// DEFINE_NAMES_ARRAY, grab names for lbd and gamer handles
					IF IS_STRING_NULL_OR_EMPTY(serverBDpassed.tl63_ParticipantNames[iparticipant])	
					AND NOT IS_STRING_NULL_OR_EMPTY(GET_PLAYER_NAME(PlayerId))
						serverBDpassed.tl63_ParticipantNames[iparticipant] = GET_PLAYER_NAME(PlayerId)
						PRINTLN(" [LBD_NAMES] DEFINE_NAMES_ARRAY,  iparticipant = ", iparticipant, " tl63_ParticipantNames = ", serverBDpassed.tl63_ParticipantNames[iparticipant])
					ENDIF
				
					// Abandoned Impromptu
					IF IS_PLAYER_ON_IMPROMPTU_DM()
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
						IF HAS_DM_STARTED(serverBDpassed)
							// Only do this stuff when the deathmatch is running
							IF playerBDPassed[iParticipant].eClientLogicStage >= CLIENT_MISSION_STAGE_FIGHTING
							AND playerBDPassed[iParticipant].eClientLogicStage < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
						
								serverBDpassed.tl63_ParticipantNames[iParticipant] = GET_PLAYER_NAME(PlayerId)
							ENDIF
						ENDIF
						
						IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
							IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
								IF OK_TO_CHECK_DISTANCE(serverBDpassed)
									IF serverBDpassed.playerAbandonedImpromptu = INVALID_PLAYER_INDEX()
										IF playerBDPassed[iParticipant].eClientLogicStage = CLIENT_MISSION_STAGE_FIGHTING
											IF HAS_PLAYER_ABANDONED_IMPROMPTU(serverBDpassed, playerId)
												serverBDpassed.playerAbandonedImpromptu = playerId
												PRINTLN("[CS_IMPROMPTU] SERVER_PROCESSING, serverBDpassed.playerAbandonedImpromptu = ", NATIVE_TO_INT(serverBDpassed.playerAbandonedImpromptu))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							// Phone quit
							IF serverBDpassed.playerAbandonedImpromptu = INVALID_PLAYER_INDEX()
								IF IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)
									serverBDpassed.playerAbandonedImpromptu = playerId		
									PRINTLN("[CS_IMPROMPTU] SERVER_PROCESSING, CLIENT_BITSET_PLAYER_QUIT_JOB playerAbandonedImpromptu = ", NATIVE_TO_INT(serverBDpassed.playerAbandonedImpromptu))			
								ENDIF
							ENDIF
						ENDIF
				
						// Impromptu results
						IF IS_PLAYER_ON_IMPROMPTU_DM()
							IF PlayerId <> LocalPlayer
								IF serverBDpassed.playerAbandonedImpromptu <> INVALID_PLAYER_INDEX()
									IF PlayerId <> serverBDpassed.playerAbandonedImpromptu
										PRINTLN("[CS_IMPROMPTU] SERVER_PROCESSING playerAbandonedImpromptu iImpromptuWinner = ", iParticipant)
										serverBDpassed.iImpromptuWinner = iParticipant
									ENDIF
								ELSE
									// Drawing
									IF playerBDPassed[iParticipant].iScore = playerBDPassed[iLocalPart].iScore
										IF serverBDpassed.iImpromptuWinner <> -1
											PRINTLN("[CS_IMPROMPTU] SERVER_PROCESSING DRAWING")
											serverBDpassed.iImpromptuWinner = -1
										ENDIF
									ELSE
										// Deciding who won
										IF playerBDPassed[iParticipant].iScore > playerBDPassed[iLocalPart].iScore
											IF serverBDpassed.iImpromptuWinner <> iParticipant
												PRINTLN("[CS_IMPROMPTU] SERVER_PROCESSING iImpromptuWinner = ", iParticipant)
												serverBDpassed.iImpromptuWinner = iParticipant
											ENDIF
										ELSE
											IF serverBDpassed.iImpromptuWinner <> iLocalPart
												PRINTLN("[CS_IMPROMPTU] SERVER_PROCESSING iImpromptuWinner = ", iLocalPart)
												serverBDpassed.iImpromptuWinner = iLocalPart
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELIF IS_PLAYER_ON_BOSSVBOSS_DM()
							IF serverBDpassed.iTeamBossPlayer[0] = -1
								IF GET_PLAYER_TEAM(PlayerId) = 0
									IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerId)
										serverBDpassed.iTeamBossPlayer[0] = NATIVE_TO_INT(PlayerId)
										serverBDpassed.iTeamBossPart[0] = iParticipant
										CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [CS_IMPROMPTU] [BOSSVBOSS] SERVER_PROCESSING THINK THIS PLAYER IS BOSS OF GANG 0 ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ENDIF
							ENDIF
							
							IF serverBDpassed.iTeamBossPlayer[1] = -1
								IF GET_PLAYER_TEAM(PlayerId) = 1
									IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerId)
										serverBDpassed.iTeamBossPlayer[1] = NATIVE_TO_INT(PlayerId)
										serverBDpassed.iTeamBossPart[1] = iParticipant
										CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [CS_IMPROMPTU] [BOSSVBOSS] SERVER_PROCESSING THINK THIS PLAYER IS BOSS OF GANG 1 ", GET_PLAYER_NAME(PlayerId))
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_KILLED)
//								IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PlayerId, eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
//									CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [CS_IMPROMPTU] [BOSSVBOSS] SERVER_PROCESSING SERVER THINKS THIS PLAYER KILLED RIVAL BOSS ", GET_PLAYER_NAME(PlayerId), " Team ", GET_PLAYER_TEAM(PlayerId), " part ", iParticipant)
//									IF serverBDpassed.iBossvBossWinner = -1
//										serverBDpassed.iBossvBossWinner = iParticipant
//									ENDIF
//									
//									SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_KILLED)
//								ENDIF
								IF playerBDPassed[iParticipant].iMyRivalBossKills !=0
									iOtherTeam = 1 - GET_PLAYER_TEAM(PlayerId)
									IF iOtherTeam < 2
										iBossKills[iOtherTeam]+= playerBDPassed[iParticipant].iMyRivalBossKills
									ENDIF
									
								ENDIF
								
								IF GB_IS_PLAYER_BOSS_OF_A_GANG(PlayerId)
								OR IS_PLAYER_ON_BIKER_DM()
									IF  GET_PLAYER_TEAM(PlayerId) > -1
										iBossKillSelf[ GET_PLAYER_TEAM(PlayerId)] += playerBDPassed[iParticipant].iMyBossKillSelf
									ENDIF
								ENDIF
							ENDIF 
							IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
								IF GB_IS_GLOBAL_CLIENT_BIT0_SET(PlayerId, eGB_GLOBAL_CLIENT_BITSET_0_BOSS_QUIT_BOSSVBOSS_DM)
									CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [CS_IMPROMPTU] [BOSSVBOSS] SERVER_PROCESSING SERVER THINKS THIS PLAYER'S BOSS QUIT ", GET_PLAYER_NAME(PlayerId), " Team ", GET_PLAYER_TEAM(PlayerId), " part ", iParticipant)
									IF serverBDpassed.iBossvBossWinner = -1
										serverBDpassed.iBossvBossWinner = iParticipant
									ENDIF
									
									SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
								ELSE 
									IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
										IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, 15000)
											IF NETWORK_GET_NUM_PARTICIPANTS() < 2
												
												CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [CS_IMPROMPTU] [BOSSVBOSS] SERVER_PROCESSING SERVER THINKS THIS PLAYER IS ONLY PLAYER LEFT ", GET_PLAYER_NAME(PlayerId), " Team ", GET_PLAYER_TEAM(PlayerId), " part ", iParticipant)
												IF serverBDpassed.iBossvBossWinner = -1
													serverBDpassed.iBossvBossWinner = iParticipant
												ENDIF
												
												SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
					
					// Everyone ready
					IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_READY)
						IF playerBDPassed[iParticipant].iInitialFadeProgress < INITIAL_FADE_STAGE_FADE_IN
							PRINTLN("ARE_ALL_CLIENTS_READY_TO_FIGHT iParticipant ", iParticipant, " is at ", playerBDPassed[iParticipant].iInitialFadeProgress)
							bEveryoneReady = FALSE
						ENDIF
					ENDIF
				
					// Check that all racers are on the leaderboard
					IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_ON_LBD)
						IF playerBDPassed[iParticipant].eClientLogicStage <> CLIENT_MISSION_STAGE_LEADERBOARD
							bEveryoneOnLbd = FALSE
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_FINISHED)
						IF playerBDPassed[iParticipant].eClientLogicStage < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
							bEveryoneFinished = FALSE
						ENDIF
					ENDIF
					
					// 2359747
					IF IS_PLAYER_ON_IMPROMPTU_DM()
					
						IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
							IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, 15000)
								IF NETWORK_GET_NUM_PARTICIPANTS() < 2
									
									bEveryoneFinished = TRUE
								ENDIF
							ENDIF
						ENDIF
//					#IF FEATURE_GANG_BOSS
//					ELSE
//						IF IS_PLAYER_ON_BOSSVBOSS_DM()
//							
//						ENDIF
//					#ENDIF
					ENDIF

				
					//////////////////////////////        	PLAYER COUNTS
					IF NOT IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)
						// Still playing
						iNumActivePlayers++
					ENDIF
					
					IF NOT IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
						// Still playing
						iNumAlivePlayers++
						
						IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)														
							IF NOT IS_BIT_SET(iTeamsAdded, playerBDPassed[iParticipant].iteam)
								iNumAliveTeams++
								PRINTLN("[TMS] SERVER_PROCESSING - Upping iNumAliveTeams to ", iNumAliveTeams, " and setting ", playerBDPassed[iParticipant].iteam, " on iTeamsAdded which is ", iTeamsAdded)
								SET_BIT(iTeamsAdded, playerBDPassed[iParticipant].iteam)
							ELSE
								PRINTLN("[TMS] SERVER_PROCESSING - ", playerBDPassed[iParticipant].iteam, " is set on iTeamsAdded which is ", iTeamsAdded)
							ENDIF
						ENDIF
					ENDIF
					
					// Count teams
					iTeam = GET_PLAYER_TEAM(PlayerId)
					IF iTeam <> -1 // don't count FFA teams 
					AND iTeam < MAX_NUM_DM_TEAMS
						IF NOT IS_BIT_SET(iTeamBitset, iTeam)
							iNumActiveTeams ++
							SET_BIT(iTeamBitset, iTeam)
						ENDIF
						
						iNumStartingPlayers[iTeam]++
					ENDIF
					
					//////////////////////////////        	TDM SCORES COUNT ACTIVE
					IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 

						iTeam = GET_PLAYER_TEAM(PlayerId)
						IF iTeam <> -1 // Prevent array overrun
						AND iTeam < MAX_NUM_DM_TEAMS

							IF bLastManStanding
								IF HAS_DM_TIME_EXPIRED(serverBDPassed) 
									IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_TEAM_SCORES_WIPED)
										// Clear out old time based scores
										REPEAT MAX_NUM_DM_TEAMS iLoop
											serverBDpassed.iTeamScore[iLoop] = 0
										ENDREPEAT
										SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_TEAM_SCORES_WIPED)	
										PRINTLN("SERVER_PROCESSING, SERV_BITSET2_TEAM_SCORES_WIPED ")
									ELSE
										// If the mode timed out then sort by kills since the finish time is the same for all
										serverBDpassed.iTeamScore[iTeam] += playerBDPassed[iParticipant].iScore
										serverBDpassed.iTeamScoreForLBD[iTeam] += playerBDPassed[iParticipant].iScore
										serverBDpassed.iPlayersLastScore[iTeam][iParticipant] = playerBDPassed[iParticipant].iScore
									ENDIF								
								ELSE
									IF GET_SUB_MODE(serverBDpassed) = SUB_KART
										serverBDpassed.iTeamScore[iTeam] += playerBDPassed[iParticipant].iScore
										serverBDpassed.iTeamScoreForLBD[iTeam] += playerBDPassed[iParticipant].iScore
										serverBDpassed.iPlayersLastScore[iTeam][iParticipant] = playerBDPassed[iParticipant].iScore
									ELSE
										// Store the team member's highest finish time
										IF playerBDPassed[iParticipant].iFinishedAtTime > serverBDpassed.iTeamScore[iTeam] 
											serverBDpassed.iTeamScore[iTeam] = playerBDPassed[iParticipant].iFinishedAtTime
										ENDIF
										serverBDpassed.iTeamScoreForLBD[iTeam] += playerBDPassed[iParticipant].iScore
									ENDIF
								ENDIF
							ELSE
								// Score
								// Increment team score with player's individual scores
								IF IS_KING_OF_THE_HILL()
									fKOTH_TeamScores[iTeam] += playerBDPassed[iParticipant].sKOTH_PlayerInfo.fKOTHScore
									PRINTLN("[KOTH_SCORE] fKOTH_TeamScores[", iTeam, "] is now ", fKOTH_TeamScores[iTeam])
								ELSE
									serverBDpassed.iTeamScore[iTeam] += playerBDPassed[iParticipant].iScore
									serverBDpassed.iTeamScoreForLBD[iTeam] += playerBDPassed[iParticipant].iScore
								ENDIF
								
								// Store the player's last score in case they leave
								serverBDpassed.iPlayersLastScore[iTeam][iParticipant] = playerBDPassed[iParticipant].iScore
							ENDIF
							
							// Deaths
							serverBDpassed.iTeamDeaths[iTeam] += playerBDPassed[iParticipant].iDeaths
							serverBDpassed.iPlayersLastDeaths[iTeam][iParticipant] = playerBDPassed[iParticipant].iDeaths
							
							// Ratio
							serverBDpassed.fTeamRatio[iTeam] += playerBDPassed[iParticipant].fRatio
							serverBDpassed.fPlayersLastRatio[iTeam][iParticipant] = playerBDPassed[iParticipant].fRatio
							
							// RP
							serverBDpassed.iTeamXP[iTeam] += playerBDPassed[iParticipant].iCurrentXP
							serverBDpassed.iPlayersLastRP[iTeam][iParticipant] = playerBDPassed[iParticipant].iCurrentXP
							
						ENDIF
					ENDIF
					
					//////////////////////////////        	TARGET SCORE FFA
					IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TARGET_SCORE_REACHED)
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						IF IS_TARGET_MATCH(serverBDpassed)
							IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
								iTeam = GET_PLAYER_TEAM(PlayerId)
								IF GET_TEAM_DM_SCORE(serverBDpassed, iTeam) >= GET_TARGET_SCORE(serverBDpassed, iTeam) 
									PRINTLN("[DM_HOST_END] TDM, Team ", iTeam, " has reached the target score | SERV_BITSET_TARGET_SCORE_REACHED ", GET_TARGET_SCORE(serverBDpassed, iTeam))
									SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TARGET_SCORE_REACHED)
									
								#IF IS_DEBUG_BUILD
								ELSE
									IF iTeam <> -1
										PRINTLN("[2280575] iTeam = ", iTeam)
										PRINTLN("[2280575] GET_TEAM_DM_SCORE = ", GET_TEAM_DM_SCORE(serverBDpassed, iTeam))
										PRINTLN("[2280575] GET_TARGET_SCORE = ", GET_TARGET_SCORE(serverBDpassed, iTeam))
									ENDIF				
								#ENDIF
								
								ENDIF				
							ELSE
								IF playerBDPassed[iParticipant].iScore >= GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iParticipant].iTeam) 
									PRINTLN("[DM_HOST_END] FFA, Participant ", iParticipant, " has reached the target score | SERV_BITSET_TARGET_SCORE_REACHED ", GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iParticipant].iTeam))
									SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TARGET_SCORE_REACHED)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					//////////////////////////////        	Idle checks
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
							IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
								CLEAR_BIT(serverBDpassed.iIdleBitset, iParticipant)
							ELSE
								IF IS_BIT_SET(playerBDPassed[iParticipant].iClientBitSet, CLIENT_BITSET_IDLE)
									IF NOT IS_BIT_SET(serverBDpassed.iIdleBitset, iParticipant)
										SET_BIT(serverBDpassed.iIdleBitset, iParticipant)
									ENDIF
								ELSE
									IF IS_BIT_SET(serverBDpassed.iIdleBitset, iParticipant)
										CLEAR_BIT(serverBDpassed.iIdleBitset, iParticipant)
									ENDIF
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
					
					//////////////////////////////        	Check for debug skip
					#IF IS_DEBUG_BUILD
					
						IF bDebug
					
							// Time skip
							IF NOT IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_TIME_SKIP)
								IF playerBDPassed[iParticipant].pressedTimeSkip = TRUE
									SET_BIT(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_TIME_SKIP)
								ENDIF
							ENDIF
						
							// Debug passing
							IF NOT IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_S_PASS)
								IF playerBDPassed[iParticipant].bPressedS = TRUE
		//							serverBDpassed.iPlayerWhoDebugEnded = NATIVE_TO_INT(PlayerId)
									SET_BIT(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_S_PASS)
										IF IS_PLAYER_ON_BIKER_DM()
											TRIGGER_MUSIC_EVENT("BIKER_MP_MUSIC_FAIL")
											PRINTLN("[DM_AUDIO] BIKER_MP_MUSIC_FAIL ")
										ENDIF
								ENDIF
							ENDIF
							
							// Debug failing
							IF NOT IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_F_FAIL)
								IF playerBDPassed[iParticipant].bPressedF = TRUE
		//							serverBDpassed.iPlayerWhoDebugEnded = NATIVE_TO_INT(PlayerId)
									SET_BIT(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_F_FAIL)
								ENDIF
							ENDIF
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			//-- HAs Boss v Boss DM Boss quit?
			IF IS_PLAYER_ON_BOSSVBOSS_DM()
				IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
					IF serverBDpassed.iTeamBossPart[0] = iParticipant
						SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
						serverBDpassed.iWinningTeam = 0
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     [BOSSVBOSS] -  SET SERV_BITSET_BOSSVBOSS_BOSS_QUIT AS TEAM 0 BOSS PART NO LONGER ACTIVE serverBDpassed.iWinningTeam = ", serverBDpassed.iWinningTeam)
					ELIF serverBDpassed.iTeamBossPart[1] = iParticipant
						SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_BOSS_QUIT)
						serverBDpassed.iWinningTeam = 1
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     [BOSSVBOSS] -  SET SERV_BITSET_BOSSVBOSS_BOSS_QUIT AS TEAM 1 BOSS PART NO LONGER ACTIVE serverBDpassed.iWinningTeam = ", serverBDpassed.iWinningTeam)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT	
	
	/// KOTH ///
	IF IS_KING_OF_THE_HILL()
		INT iKOTHTeam = 0
		
		FOR iKOTHTeam = 0 TO MAX_NUM_DM_TEAMS - 1
			serverBDpassed.iTeamScore[iKOTHTeam] = FLOOR(fKOTH_TeamScores[iKOTHTeam])
			PRINTLN("[KOTH_SCORES] Setting team score for team ", iKOTHTeam, " to ", serverBDpassed.iTeamScore[iKOTHTeam], " || ", fKOTH_TeamScores[iKOTHTeam])
			
			IF IS_TEAM_DEATHMATCH()
				IF NOT IS_BIT_SET(iTeamBitset, iKOTHTeam)
					IF NOT IS_BIT_SET(serverBDpassed.sKOTH_HostInfo.iEmptyTeamBS, iKOTHTeam)
						SET_BIT(serverBDpassed.sKOTH_HostInfo.iEmptyTeamBS, iKOTHTeam)
						PRINTLN("[KOTH][KOTH_HOST] Setting ", iKOTHTeam, " in iEmptyTeamBS")
					ENDIF
					
					serverBDpassed.iTeamScore[iKOTHTeam] = 0
				ELSE
					IF IS_BIT_SET(serverBDpassed.sKOTH_HostInfo.iEmptyTeamBS, iKOTHTeam)
						CLEAR_BIT(serverBDpassed.sKOTH_HostInfo.iEmptyTeamBS, iKOTHTeam)
						PRINTLN("[KOTH][KOTH_HOST] Clearing ", iKOTHTeam, " in iEmptyTeamBS")
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	//////////////////////////////        	PLAYER COUNT Tally

	// Active players
 	IF serverBDpassed.iNumActivePlayers <> iNumActivePlayers
		serverBDpassed.iNumActivePlayers = iNumActivePlayers
	ENDIF
	
	// Add together
	iNumPlayers = (serverBDpassed.iNumActivePlayers + iNumOffScriptButFinished)

	// Result
	IF serverBDpassed.iNumDMPlayers <> iNumPlayers
		serverBDpassed.iNumDMPlayers = iNumPlayers
	ENDIF
	
	// Starting Players On Team
	INT iTeamLoop = 0
	FOR iTeamLoop = 0 TO MAX_NUM_DM_TEAMS-1
		IF serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] <> iNumStartingPlayers[iTeamLoop]
		AND serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] = 0
			serverBDpassed.iNumberOfStartingPlayers[iTeamLoop] = iNumStartingPlayers[iTeamLoop]
			PRINTLN("[LM][DM][SERVER_PROCESSING] - iTeamLoop: ", iTeamLoop, " iNumStartingPlayers: ", iNumStartingPlayers[iTeamLoop])
		ENDIF
	ENDFOR
	
	// Count teams
	IF serverBDpassed.iNumActiveTeams <> iNumActiveTeams
		serverBDpassed.iNumActiveTeams = iNumActiveTeams
	ENDIF
	
	// Count spectators
	IF serverBDpassed.iNumSpectators <> iNumSpectators
		serverBDpassed.iNumSpectators = iNumSpectators
	ENDIF
	
	IF serverBDpassed.iNumAlivePlayers <> iNumAlivePlayers
		serverBDpassed.iNumAlivePlayers = iNumAlivePlayers
	ENDIF
	
	IF serverBDpassed.iNumAliveTeams <> iNumAliveTeams
		serverBDpassed.iNumAliveTeams = iNumAliveTeams
	ENDIF
	
	//////////////////////////////        	TDM SCORES COUNT TALLY
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
	
		// Best player in winning team
		IF serverBDpassed.iBestPlayerWinningTeam <> iBestPlayerWinningTeam
		
			serverBDpassed.iBestPlayerWinningTeam = iBestPlayerWinningTeam
		ENDIF
		
		IF serverBDpassed.iMVP <> iMVP
		
			serverBDpassed.iMVP = iMVP
			PRINTLN(" [MVP] serverBDpassed.iMVP = ", serverBDpassed.iMVP)
		ENDIF
	
		REPEAT MAX_NUM_DM_TEAMS iTeam
			// Add to team score for players who left
			IF serverBDpassed.iPlayerWhoLeftScore[iTeam] <> 0
			AND (NOT IS_KING_OF_THE_HILL() OR IS_BIT_SET(iTeamBitset, iTeam))
				serverBDpassed.iTeamScore[iTeam] += serverBDpassed.iPlayerWhoLeftScore[iTeam]
				serverBDpassed.iTeamScoreForLBD[iTeam] += serverBDpassed.iPlayerWhoLeftScore[iTeam]
			ENDIF
			// Deaths
			IF serverBDpassed.iPlayerWhoLeftDeaths[iTeam] <> 0
				serverBDpassed.iTeamDeaths[iTeam] += serverBDpassed.iPlayerWhoLeftDeaths[iTeam]
			ENDIF
			// Ratio
			IF serverBDpassed.fPlayerWhoLeftRatio[iTeam] <> 0
				serverBDpassed.fTeamRatio[iTeam] += serverBDpassed.fPlayerWhoLeftRatio[iTeam]
			ENDIF
			// RP
			IF serverBDpassed.iPlayerWhoLeftRP[iTeam] <> 0
				serverBDpassed.iTeamXP[iTeam] += serverBDpassed.iPlayerWhoLeftRP[iTeam]
			ENDIF
			
			// Highest score
			IF IS_TEAM_SCORE_HIGHER_THAN_EVERYONE(serverBDpassed,dmVarsPassed,iTeam)
			AND IS_BIT_SET(iTeamBitset, iTeam)
				IF serverBDpassed.iTeamScore[iTeam] > 0
					iTeamWithHigestScore = iTeam
				ENDIF
			ENDIF
			
			IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				//////////////////////////////        	TARGET SCORE TDM
				IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TARGET_SCORE_REACHED)
					IF IS_TARGET_MATCH(serverBDpassed)
						IF GET_TEAM_DM_SCORE(serverBDpassed, iTeam) >= GET_TARGET_SCORE(serverBDpassed, iTeam) 
							PRINTLN("GET_PLAYER_TEAM, SERV_BITSET_TARGET_SCORE_REACHED ")
							SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TARGET_SCORE_REACHED)
						ENDIF				
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		// Set team with highest score
		IF iTeamWithHigestScore <> -1
			IF dmVarsPassed.iHighestTeamScoreStored[iTeamWithHigestScore] < iBestTeamScoreTemp
				IF serverBDpassed.iTeamWithHighestScore <> iTeamWithHigestScore
					serverBDpassed.iTeamWithHighestScore = iTeamWithHigestScore
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
	AND HAS_DM_STARTED(serverBDPassed)
		PRINTLN("[LM][DM_LIVES][SERVER_PROCESSING] - Setting serverBDpassed.iNumAliveTeams = ", serverBDpassed.iNumAliveTeams, " serverBDpassed.iNumAlivePlayers: ", serverBDpassed.iNumAlivePlayers)	
		
		IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
			IF serverBDpassed.iNumAliveTeams <= 1
				IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)
					PRINTLN("[LM][DM_LIVES][SERVER_PROCESSING] - Setting SERV_BITSET2_RAN_OUT_OF_LIVES")
					SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)
				ENDIF
			ENDIF
		ELSE
			IF serverBDpassed.iNumAlivePlayers <= 1
			
			#IF IS_DEBUG_BUILD
			AND NOT IS_ONE_PLAYER_DM_DEBUG()
			AND NOT dmVarsPassed.debugDmVars.bDontEndOnePlayer
			#ENDIF
			
				IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)
					PRINTLN("[LM][DM_LIVES][SERVER_PROCESSING] - Setting SERV_BITSET2_RAN_OUT_OF_LIVES")
					SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	// Player with highest score
//	IF ipartWithHighestScore <> -1
//		PLAYER_INDEX playerHighestScore = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(ipartWithHighestScore))
//		IF serverBDpassed.playerWithHighestScore <> playerHighestScore
//			serverBDpassed.playerWithHighestScore = playerHighestScore
//		ENDIF
//	ENDIF
	
	SERVER_SET_MATCH_WINNER(serverBDPassed, serverBDpassedLDB)
	
	// Everyone on the lbd
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_ON_LBD)
		IF bEveryoneOnLbd = TRUE
			PRINTLN("SERVER_PROCESSING, SERV_BITSET_EVERYONE_ON_LBD")
			serverBDpassed.sServerFMMC_EOM.bAllowedToVote = TRUE
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_ON_LBD)
		ELSE
			PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, bEveryoneOnLbd = FALSE")
		ENDIF
	ELSE
		PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, SERV_BITSET_EVERYONE_ON_LBD = TRUE")
	ENDIF	
	
	// Everyone ready
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_READY)
		IF bEveryoneReady = TRUE
			PRINTLN("SERVER_PROCESSING, SERV_BITSET_EVERYONE_READY")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_READY)
		ELSE
			PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, bEveryoneReady = FALSE")
		ENDIF
	ELSE
		PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, SERV_BITSET_EVERYONE_READY = TRUE")
	ENDIF	
	
	// Server decides when dm has ended
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_SERVER_SAYS_DM_ENDED)
		IF SERVER_CHECKS_HAS_DM_ENDED(serverBDpassed, FALSE #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF )
			PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, SERVER_CHECKS_HAS_DM_ENDED = TRUE")
			IF HAS_SERVER_FINISHED_LBD_SORT(serverBDpassed)
				BROADCAST_SERVER_DM_FINISHED(TRUE)
				PRINTLN("[SERVER_END] SERVER_PROCESSING, SERV_BITSET_SERVER_SAYS_DM_ENDED")
				SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_SERVER_SAYS_DM_ENDED)
			ELSE
				PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, HAS_SERVER_FINISHED_LBD_SORT = FALSE")
			ENDIF
		ELSE
			PRINTLN("[bug:5588022][SERVER_END] SERVER_PROCESSING, SERVER_CHECKS_HAS_DM_ENDED = FALSE")
		ENDIF
	ENDIF
	
	SET_BIT_WHEN_PLAYERS_CLOSE_ENOUGH(serverBDpassed, playerBDPassed)
	
	SERVER_STORES_FINISH_POSITIONS(serverBDpassed, playerBDPassed, serverBDpassedLDB)
	
	// Time expired bit
	SERVER_SETS_TIME_UP(serverBDpassed)
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_FINISHED)
		IF bEveryoneFinished
			PRINTLN("[DM_HOST_END][SERVER_END] SERVER_PROCESSING, SERV_BITSET_EVERYONE_FINISHED")
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_FINISHED)
		ENDIF
	ENDIF
	
	INT iNewLivesRemaning
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_KILLED)
			iNewLivesRemaning = (GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM() - iBossKills[0] - serverBDpassed.iMissingKills[0]) // Number of times boss killed
			
			iNewLivesRemaning -= iBossKillSelf[0] // Number of times boss killed self
			IF iNewLivesRemaning > 0
			
				IF serverBDpassed.iLivesRemaining[0] != iNewLivesRemaning
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] UPDATING LIVES FOR TEAM 0....")
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM() = ", GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM())
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] iBossKills[0] = ", iBossKills[0])
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] iBossKillSelf[0] = ", iBossKillSelf[0])
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] serverBDpassed.iMissingKills[0] = ", serverBDpassed.iMissingKills[0])
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] iNewLivesRemaning = ", iNewLivesRemaning)
					
					IF serverBDpassed.iLivesRemaining[0] > iNewLivesRemaning 
						serverBDpassed.iLivesRemaining[0] = iNewLivesRemaning			
						CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] serverBDpassed.iLivesRemaining[0] UPDATED TO ", serverBDpassed.iLivesRemaining[0], " iBossKills[0] = ", iBossKills[0], " iBossKillSelf[0] = ", iBossKillSelf[0], " iMissingKills[0] = ", serverBDpassed.iMissingKills[0])
					ELSE
						serverBDpassed.iMissingKills[0] = iNewLivesRemaning - serverBDpassed.iLivesRemaining[0]
						CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] serverBDpassed.iLivesRemaining[0] serverBDpassed.iLivesRemaining[0] = ",serverBDpassed.iLivesRemaining[0], " < iNewLivesRemaning = ", iNewLivesRemaning, " SOME KILLS GON MISSING iMissingKills[0] = ", serverBDpassed.iMissingKills[0])
					ENDIF
				ENDIF
			ELSE
				SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_KILLED)
				IF serverBDpassed.iBossvBossWinner = -1
					serverBDpassed.iBossvBossWinner = 1
				ENDIF
				CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] SET SERV_BITSET_BOSSVBOSS_KILLED serverBDpassed.iBossvBossWinner = ", serverBDpassed.iBossvBossWinner)
			ENDIF
			
			iNewLivesRemaning = (GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM() - iBossKills[1] - serverBDpassed.iMissingKills[1]) // Number of times boss killed
			
			iNewLivesRemaning -= iBossKillSelf[1] // Number of times boss killed self
			IF iNewLivesRemaning > 0
				IF serverBDpassed.iLivesRemaining[1] != iNewLivesRemaning
					IF serverBDpassed.iLivesRemaining[1] > iNewLivesRemaning 
						serverBDpassed.iLivesRemaining[1] = iNewLivesRemaning
						CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] serverBDpassed.iLivesRemaining[1] UPDATED TO ", serverBDpassed.iLivesRemaining[1], " iBossKills[1] = ", iBossKills[1], " iBossKillSelf[1] = ", iBossKillSelf[1], " iMissingKills[1] = ", serverBDpassed.iMissingKills[1])
					ELSE
						serverBDpassed.iMissingKills[1] = iNewLivesRemaning - serverBDpassed.iLivesRemaining[1]
						CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] serverBDpassed.iLivesRemaining[1] serverBDpassed.iLivesRemaining[1] = ",serverBDpassed.iLivesRemaining[1], " < iNewLivesRemaning = ", iNewLivesRemaning, " SOME KILLS GON MISSING iMissingKills[1] = ", serverBDpassed.iMissingKills[1])
				
					ENDIF
				ENDIF
			ELSE
				SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_BOSSVBOSS_KILLED)
				IF serverBDpassed.iBossvBossWinner = -1
					serverBDpassed.iBossvBossWinner = 0
				ENDIF
				CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [SERVER_PROCESSING] SET SERV_BITSET_BOSSVBOSS_KILLED serverBDpassed.iBossvBossWinner = ")
			ENDIF
		ENDIF
	ENDIF
	 //FEATURE_GANG_BOSS
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		FOR iTeamLoop = 0 TO serverBDpassed.iNumberOfTeams-1
			FLOAT fCurrentTeamScore = TO_FLOAT(serverBDpassed.iTeamScore[iTeamLoop])
			FLOAT fPercent = fCurrentTeamScore/GET_TARGET_SCORE(serverBDpassed, iTeamLoop)
			serverBDpassed.iTeamScorePercentage[iTeamLoop] = ROUND(fPercent * 100)
			PRINTLN("[SERVER_PROCESSING] serverBDpassed.iTeamScorePercentage[",iTeamLoop,"]: ", serverBDpassed.iTeamScorePercentage[iTeamLoop])
		ENDFOR
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD4, KEYBOARD_MODIFIER_NONE, "")
		PRINTLN("  SERVER_PROCESSING  ")
		PRINTLN("  iNumPlayers = ", iNumPlayers)
		PRINTLN("  iNumOffScriptButFinished = ", iNumOffScriptButFinished)
		PRINTLN("  iNumActiveTeams = ", iNumActiveTeams)
	ENDIF
	#ENDIF
ENDPROC

// Temp to handle pesky Freemode cops
PROC HANDLE_DEATHMATCH_WANTED_RATINGS(ServerBroadcastData &serverBDpassed)
	PRINTLN("[HANDLE_DEATHMATCH_WANTED_RATINGS] ", tl31ScriptName)
	
	INT iMyTeam = GET_PLAYER_TEAM(LocalPlayer) 
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	AND iMyTeam > -1
	AND g_FMMC_STRUCT.iTeamWanted[iMyTeam] > 1
		IF bLocalPlayerOK
		AND GET_PLAYER_WANTED_LEVEL(LocalPlayer) != g_FMMC_STRUCT.iTeamWanted[iMyTeam]-1
			PRINTLN("[HANDLE_DEATHMATCH_WANTED_RATINGS] SET_PLAYER_WANTED_LEVEL: ",g_FMMC_STRUCT.iTeamWanted[iMyTeam]-1) 
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			SET_MAX_WANTED_LEVEL(g_FMMC_STRUCT.iTeamWanted[iMyTeam]-1)
			SET_PLAYER_WANTED_LEVEL(LocalPlayer,g_FMMC_STRUCT.iTeamWanted[iMyTeam]-1)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
		ELSE
			SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(LocalPlayer)
		ENDIF
	ELIF GET_MAX_WANTED_LEVEL() != 0
	OR GET_PLAYER_WANTED_LEVEL(LocalPlayer) > 0
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_MAX_WANTED_LEVEL(0)
		IF bLocalPlayerOK
			SET_PLAYER_WANTED_LEVEL(LocalPlayer, 0)
			SET_PLAYER_WANTED_LEVEL_NOW(LocalPlayer)
		ENDIF
	ENDIF
ENDPROC

///////
///// PURPOSE:
  ///    ///////
  /// PARAMS:
  ///    dmVarsPassed - 
  /// RETURNS:
  ///    
FUNC BOOL HAS_RESPAWN_SCALEFORM_LOADED(SHARED_DM_VARIABLES &dmVarsPassed)

	dmVarsPassed.RespawnButton = REQUEST_SCALEFORM_MOVIE("SAVING_FOOTER")
	
	IF HAS_SCALEFORM_MOVIE_LOADED(dmVarsPassed.RespawnButton)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC REFRESH_DM_SCALEFORM_BUTTON_SLOTS(LEADERBOARD_PLACEMENT_TOOLS& Placement)
	MPHUD_CLEAR_SCALEFORM_ELEMENT(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_INSTRUCTIONAL)], HUD_SCALEFORM_INSTRUCTIONAL)
	MPHUD_CLEAR_SCALEFORM_ELEMENT(Placement.ButtonMovie[GET_SCALEFORM_INDEX_FOR_HUD(HUD_SCALEFORM_LOADING_ICON)], HUD_SCALEFORM_LOADING_ICON)
	REFRESH_SCALEFORM_LOADING_ICON(Placement.ScaleformLoadingStruct)
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(Placement.LeaderboardScaleformStruct)
ENDPROC

PROC SERVER_COUNT_JOINING_PLAYERS(ServerBroadcastData &serverBDpassed)

	INT iBitField, i, iNumPlayers
	PLAYER_INDEX thePlayerID

	// Grab uniqueId
	serverBDpassed.iUniqueId = Get_UniqueID_For_This_Players_Mission(LocalPlayer)
	PRINTLN("SERVER_COUNT_JOINING_PLAYERS, serverBDpassed.iUniqueId = ", serverBDpassed.iUniqueId)
	// Grab num players joining from corona
	iBitField = Get_Bitfield_Of_Players_On_Or_Joining_Mission(serverBDpassed.iUniqueId)
	
	REPEAT NUM_NETWORK_PLAYERS i
	    thePlayerID = INT_TO_PLAYERINDEX(i)
	    IF (IS_NET_PLAYER_OK(thePlayerID, FALSE))
		    IF (IS_BIT_SET(iBitField, i))
		    	iNumPlayers++
		    ENDIF
		ENDIF
	ENDREPEAT
	
	serverBDpassed.iNumDmStarters = iNumPlayers
	PRINTLN("SERVER_COUNT_JOINING_PLAYERS, serverBDpassed.iNumDmStarters = ", serverBDpassed.iNumDmStarters)
ENDPROC

FUNC BOOL ARE_ANY_SPAWN_POINTS_IN_ILLEGAL_ZONES()

	INT i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfSpawnPoints i
		IF IS_PLACEMENT_IN_A_RESTRICTED_AREA(g_FMMC_STRUCT_ENTITIES.sPlacedSpawnPoint[i].vPos)

			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN TRUE
ENDFUNC

PROC SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS(ServerBroadcastData &serverBDpassed)
	SWITCH serverBDpassed.iIllegalCheckProgress
		CASE ILLEGAL_CHECK_UGC
			IF IS_CURRENT_MISSION_UGC_NON_ROCKSTAR()
				PRINTLN("SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS, ILLEGAL_CHECK_UGC, IS_CURRENT_MISSION_UGC_NON_ROCKSTAR >> ILLEGAL_CHECK_SPAWNPOINTS ")
				serverBDpassed.iIllegalCheckProgress = ILLEGAL_CHECK_SPAWNPOINTS
			ELSE
				PRINTLN("SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS, ILLEGAL_CHECK_UGC,  ILLEGAL_EXIT (IS_CURRENT_MISSION_UGC_NON_ROCKSTAR) ")
				serverBDpassed.iIllegalCheckProgress = ILLEGAL_EXIT
			ENDIF
		BREAK
		
		CASE ILLEGAL_CHECK_SPAWNPOINTS
			IF ARE_ANY_SPAWN_POINTS_IN_ILLEGAL_ZONES()
				PRINTLN("SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS, ILLEGAL_CHECK_SPAWNPOINTS, ARE_ANY_SPAWN_POINTS_IN_ILLEGAL_ZONES >> ILLEGAL_SET_BIT ")
				serverBDpassed.iIllegalCheckProgress = ILLEGAL_SET_BIT
			ELSE
				PRINTLN("SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS, ILLEGAL_CHECK_SPAWNPOINTS, ILLEGAL_EXIT (ARE_ANY_SPAWN_POINTS_IN_ILLEGAL_ZONES) ")
				serverBDpassed.iIllegalCheckProgress = ILLEGAL_EXIT
			ENDIF
		BREAK
		
		CASE ILLEGAL_SET_BIT
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_ILLEGAL_UGC_DM)
			PRINTLN("SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS, ILLEGAL_SET_BIT,  SERV_BITSET_ILLEGAL_UGC_DM >> 3 ")
			serverBDpassed.iIllegalCheckProgress = ILLEGAL_EXIT
		BREAK
		
		CASE ILLEGAL_EXIT
			EXIT
		BREAK
	  ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_USE_LOBBY_SET_LIVES()
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS29_ShowDMLivesOptionInCorona)
		PRINTLN("SHOULD_USE_LOBBY_SET_LIVES - Lives option not set")
		RETURN FALSE
	ENDIF
	
	IF IS_TEAM_DEATHMATCH()
	AND NOT DO_ALL_DM_TEAMS_HAVE_SAME_NUMBER_OF_LIVES_SET()
		PRINTLN("SHOULD_USE_LOBBY_SET_LIVES - Team Deathmatch with mismatching lives")
		RETURN FALSE
	ENDIF
	
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL SHOULD_USE_LOBBY_SET_SCORE()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyNine, ciOptionsBS20_Hide_Corona_Option_Target_Score)
		PRINTLN("SHOULD_USE_LOBBY_SET_SCORE - Score option not set")
		RETURN FALSE
	ENDIF
	
	IF IS_TEAM_DEATHMATCH()
	AND NOT DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE()
		PRINTLN("SHOULD_USE_LOBBY_SET_SCORE - Team Deathmatch with mismatching target scores")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL INIT_SERVER_DATA(ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed)
	PRINTLN("INIT_SERVER_DATA, DM")
	
	IF NOT SETUP_PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(serverBDpassed)
		PRINTLN("[INIT_SERVER] INIT_SERVER_DATA - Waiting for Play Stats Match History")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_A_ROUNDS_MISSION()
	AND g_sTransitionSessionData.sMissionRoundData.iNumberOfRoundsPlayed > 0
		IF IS_CORONA_READY_TO_START_WITH_JOB()
			PRINTLN("[7719312] - INIT_SERVER_DATA Pre copy g_sDM_SB_CoronaOptions.bIsTeamDM: ", g_sDM_SB_CoronaOptions.bIsTeamDM)
			PRINTLN("[7719312] - INIT_SERVER_DATA Pre copy GlobalServerBD_DM.bIsTeamDM: ", GlobalServerBD_DM.bIsTeamDM)
			GlobalServerBD_DM = g_sDM_SB_CoronaOptions
			PRINTLN("[7719312] - INIT_SERVER_DATA Post copy g_sDM_SB_CoronaOptions.bIsTeamDM: ", g_sDM_SB_CoronaOptions.bIsTeamDM)
			PRINTLN("[7719312] - INIT_SERVER_DATA Post copy GlobalServerBD_DM.bIsTeamDM: ", GlobalServerBD_DM.bIsTeamDM)
		ELSE
			PRINTLN("[7719312][INIT_SERVER] INIT_SERVER_DATA - Waiting for Corona to be ready")
			#IF IS_DEBUG_BUILD
			SET_TEXT_SCALE(0.6800, 0.6800)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_CENTRE(TRUE)
			SET_TEXT_DROPSHADOW(2, 0, 0, 0, 255)
			SET_TEXT_EDGE(0, 0, 0, 0, 255)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.0650, "STRING", "Waiting on corona to be ready!")
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
		
	SERVER_COUNT_JOINING_PLAYERS(serverBDpassed)
	
//	serverBDPassed.iLaunchPosix = GET_CLOUD_TIME_AS_INT() 
//	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iLaunchPosix = ", serverBDPassed.iLaunchPosix)
	
	INT i
	FOR i = 0 TO (ciMAX_CREATED_VEHICLES-1)
		IF IS_NET_VEHICLE_DRIVEABLE(serverFMMCPassed.niVehicle[i])

			serverBDpassed.iNumVehCreated ++
		ENDIF
	ENDFOR
	PRINTLN("INIT_SERVER_DATA, serverBDpassed.iNumVehCreated  ", serverBDpassed.iNumVehCreated)
	
	// Only do for TDM fool
	IF GlobalServerBD_DM.bIsTeamDM = TRUE
		serverBDpassed.iNumberOfTeams = g_sDM_SB_CoronaOptions.iNumberOfTeams 
		PRINTLN("INIT_SERVER_DATA, serverBDpassed.iNumberOfTeams = ", serverBDpassed.iNumberOfTeams)
	ENDIF
	
	// Cap number of teams (943588)
	// Check we don't need to cap based on balance settings
	IF serverBDpassed.iNumberOfTeams > serverBDpassed.iNumDmStarters
		serverBDpassed.iNumberOfTeams = serverBDpassed.iNumDmStarters
		PRINTLN("INIT_SERVER_DATA, serverBDpassed.iNumberOfTeams CAPPED_AND_EQUALS : ", serverBDpassed.iNumberOfTeams)
	ENDIF
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		serverBDpassed.iNumberOfTeams = 2
		PRINTLN("INIT_SERVER_DATA, serverBDpassed.iNumberOfTeams 2 AS IS_PLAYER_ON_BOSSVBOSS_DM")
		
		serverBDpassed.iLivesRemaining[0] = GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM()
		serverBDpassed.iLivesRemaining[1] = GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM()
		
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] INIT_SERVER_DATA, INIT iLivesRemaining AS IS_PLAYER_ON_BOSSVBOSS_DM GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM = ", GET_START_NUMBER_OF_LIVES_FOR_BOSS_V_BOSS_DM())
		
		serverBDpassed.iTeamBossPlayer[0] = -1		
		serverBDpassed.iTeamBossPart[0] = -1
		serverBDpassed.iTeamBossPlayer[1] = -1
		serverBDpassed.iTeamBossPart[1] = -1
	ENDIF
	serverBDpassed.iTeamBalancing = g_sDM_SB_CoronaOptions.iBalanceTeams
	PRINTLN("INIT_SERVER_DATA, serverBDpassed.iTeamBalancing = ", serverBDpassed.iTeamBalancing)
	
//	serverBDpassed.playerWithHighestScore = INVALID_PLAYER_INDEX()
	serverBDpassed.playerMatchWinner = INVALID_PLAYER_INDEX()
	serverBDpassed.playerDMWinner = INVALID_PLAYER_INDEX()
	serverBDpassed.playerPower = INVALID_PLAYER_INDEX() 
	serverBDpassed.playerWinning = INVALID_PLAYER_INDEX()
	serverBDpassed.playerAbandonedImpromptu = INVALID_PLAYER_INDEX()
	
	// Debug fake teams if testing one player
	#IF IS_DEBUG_BUILD
		IF IS_ONE_PLAYER_DM_DEBUG()
			PRINTLN("INIT_SERVER_DATA, serverBDpassed.iNumberOfTeams = 2 because IS_ONE_PLAYER_DM_DEBUG")
			serverBDpassed.iNumberOfTeams = 2
		ENDIF
	#ENDIF
	
	PRINTLN("[7719312] - INIT_SERVER_DATA - GlobalServerBD_DM.bIsTeamDM - ", GlobalServerBD_DM.bIsTeamDM)
	PRINTLN("[7719312] - INIT_SERVER_DATA - g_sDM_SB_CoronaOptions.bIsTeamDM - ", g_sDM_SB_CoronaOptions.bIsTeamDM)
	PRINTLN("[7719312] - INIT_SERVER_DATA - serverBDPassed.iDeathmatchType - ", serverBDPassed.iDeathmatchType)
	
	IF GlobalServerBD_DM.bIsTeamDM = TRUE
		PRINTLN("INIT_SERVER_DATA, serverBDpassed.bIsTeamDeathmatch = TRUE")
		
		IF IS_KING_OF_THE_HILL()
			serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_KOTH_TEAM
		ELSE
			serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_TDM
		ENDIF
		
	ELIF IS_PLAYER_ON_BOSSVBOSS_DM()
			PRINTLN("INIT_SERVER_DATA, serverBDpassed.bIsTeamDeathmatch = TRUE AS IS_PLAYER_ON_BOSSVBOSS_DM")
			
			IF IS_KING_OF_THE_HILL()
				serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_KOTH_TEAM
			ELSE
				serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_TDM
			ENDIF
	
	ELSE
		PRINTLN("INIT_SERVER_DATA, serverBDpassed.bIsTeamDeathmatch = FALSE")
		IF IS_KING_OF_THE_HILL()
			serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_KOTH
		ELSE
			serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_FFA
		ENDIF
		
		// Set this bit for non team modes
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_START_FINISHING_UP_TEAM_SORT)		
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_FINISHED_WINNING_TEAMS)
		SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_TEAM_SORT_CLOSED)		
	ENDIF
	
	PRINTLN("SPEIRS GlobalServerBD_DM.iVehicleDeathmatch", GlobalServerBD_DM.iVehicleDeathmatch)
	
	serverBDPassed.iFixedCamera = g_FMMC_STRUCT.iFixedCamera
	PRINTLN("serverBDPassed.iFixedCamera = ", serverBDPassed.iFixedCamera)
	
	serverBDPassed.iVehDeathmatch = GlobalServerBD_DM.iVehicleDeathmatch
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iVehDeathmatch", serverBDPassed.iVehDeathmatch)
	
	IF GlobalServerBD_DM.iTypeOfDeathmatch = FMMC_DM_TYPE_VEHICLE
		serverBDPassed.iDeathmatchType = GlobalServerBD_DM.iTypeOfDeathmatch
	ENDIF 
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iDeathmatchType", serverBDPassed.iDeathmatchType)
	
	serverBDPassed.iTagSetting = GlobalServerBD_DM.iTags
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iTagSetting", serverBDPassed.iTagSetting)
	
	serverBDPassed.iWeather = GlobalServerBD_DM.iWeather
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iWeather = ", serverBDPassed.iWeather)
	
	serverBDPassed.iPolice = GlobalServerBD_DM.iPolice
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		serverBDPassed.iPolice = POLICE_AMB_ON
		PRINTLN("INIT_SERVER_DATA, serverBDPassed.iPolice = POLICE_AMB_ON, in impromptu DM")
	ENDIF
	
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iPolice = ", serverBDPassed.iPolice)
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	
		serverBDPassed.iDuration = g_sImpromptuVars.iImpDuration
		PRINTLN("INIT_SERVER_DATA, [IMP_TIME] IS_PLAYER_ON_IMPROMPTU_DM... ")
	ELSE
		serverBDPassed.iDuration = GlobalServerBD_DM.iDuration
	ENDIF
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iDuration = ", serverBDPassed.iDuration)
	
	serverBDPassed.iTarget = GlobalServerBD_DM.iTarget
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iTarget = ", serverBDPassed.iTarget)
	IF IS_TEAM_DEATHMATCH()
		FOR i = 0 TO FMMC_MAX_TEAMS-1
			IF NOT SHOULD_USE_LOBBY_SET_SCORE()
				serverBDPassed.iTeamTarget[i] = g_FMMC_STRUCT.sDMCustomSettings.iTeamTargetScore[i]
				PRINTLN("INIT_SERVER_DATA, Setting from g_FMMC_STRUCT.sDMCustomSettings.iTeamTargetScore[",i,"] - serverBDpassed.iTeamTarget[",i,"] = ", serverBDpassed.iTeamTarget[i])
			ELSE
				serverBDPassed.iTeamTarget[i] = GlobalServerBD_DM.iTarget
				PRINTLN("INIT_SERVER_DATA, Setting from GlobalServerBD_DM.iTarget - serverBDpassed.iTeamTarget[",i,"] = ", serverBDpassed.iTeamTarget[i])
			ENDIF
		ENDFOR
	ENDIF
	
	serverBDPassed.iStartWeapon = GlobalServerBD_DM.iStartWeapon
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iStartWeapon = ", serverBDPassed.iStartWeapon)
	
	serverBDPassed.iHealthBar = GlobalServerBD_DM.iHealthBar
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iHealthBar = ", serverBDPassed.iHealthBar)
	
	serverBDPassed.iTimeOfDay = GlobalServerBD_DM.iTimeOfDay
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iTimeOfDay = ", serverBDPassed.iTimeOfDay)
	
	serverBDPassed.iTraffic = GlobalServerBD_DM.iTraffic
	PRINTLN("INIT_SERVER_DATA, serverBDPassed.iTraffic = ", serverBDPassed.iTraffic)
	
	IF IS_THIS_LEGACY_DM_CONTENT()
	OR IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
		serverBDPassed.iPlayerLives = g_FMMC_STRUCT.iPlayerLives
		PRINTLN("INIT_SERVER_DATA, serverBDPassed.iPlayerLives = ", serverBDPassed.iPlayerLives)
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
		IF NOT SHOULD_USE_LOBBY_SET_LIVES()
			IF IS_TEAM_DEATHMATCH()
				FOR i = 0 TO FMMC_MAX_TEAMS-1
					serverBDpassed.iTeamLives[i] = g_FMMC_STRUCT.sDMCustomSettings.iTeamLives[i]
					PRINTLN("INIT_SERVER_DATA, serverBDpassed.iTeamLives[",i,"] = ", serverBDpassed.iTeamLives[i])
				ENDFOR
			ELSE
				serverBDPassed.iPlayerLives = g_FMMC_STRUCT.sDMCustomSettings.iFFALives
				PRINTLN("INIT_SERVER_DATA, from g_FMMC_STRUCT.sDMCustomSettings.iFFALives serverBDPassed.iPlayerLives = ", serverBDPassed.iPlayerLives)
			ENDIF
		ELSE
			IF IS_TEAM_DEATHMATCH()
				FOR i = 0 TO FMMC_MAX_TEAMS-1
					serverBDpassed.iTeamLives[i] = g_FMMC_STRUCT.iPlayerLives
					PRINTLN("INIT_SERVER_DATA, serverBDpassed.iTeamLives[",i,"] = ", serverBDpassed.iTeamLives[i])
				ENDFOR
			ENDIF
		ENDIF
	ENDIF
	
	SERVER_CHECKS_FOR_ILLEGAL_UGC_DMS(serverBDPassed)
	
	INITIALISE_CELEBRATION_SCREEN_DATA_SERVER(serverBDpassed.sCelebServer)
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_DM_XP_TOP_THREE_MULTIPLIER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	RETURN (serverBDpassed.iNumDmStarters * playerBDPassed[iLocalPart].iKills)
ENDFUNC

// -----------------------------------		DEATHMATCH HUD

FUNC TEXT_LABEL_31 GET_PLAYER_NAME_FOR_HUD(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, BOOL bIsEnemy, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	TEXT_LABEL_31 sReturn
	IF bIsEnemy
		IF IS_PLAYER_WINNING(thisPlayer, serverBDpassed, serverBDpassedLDB)				
			IF serverBDpassedLDB.leaderBoard[1].iParticipant <> -1
				sReturn = Get_String_From_TextLabel(serverBDpassed.tl63_ParticipantNames[serverBDpassedLDB.leaderBoard[1].iParticipant])
			ENDIF
		ELSE				
			IF serverBDpassedLDB.leaderBoard[0].iParticipant <> -1	
				sReturn = Get_String_From_TextLabel(serverBDpassed.tl63_ParticipantNames[serverBDpassedLDB.leaderBoard[0].iParticipant])
			ENDIF
		ENDIF
	ELSE
		sReturn = GET_PLAYER_NAME(thisPlayer)
	ENDIF
	
	RETURN sReturn
	
ENDFUNC

FUNC TEXT_LABEL_31 GET_NAME_FOR_HUD(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, BOOL bIsEnemy, ServerBroadcastData_Leaderboard &serverBDpassedLDB, BOOL bPlayerNameInTDM = FALSE)
	TEXT_LABEL_31 sReturn
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
		IF bIsEnemy 
			IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerImpromptuRival, FALSE)
				sReturn = GET_PLAYER_NAME(g_sImpromptuVars.playerImpromptuRival)
			ENDIF			
		ELSE
			sReturn = GET_PLAYER_NAME(thisPlayer)
		ENDIF
	ELIF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	AND NOT bPlayerNameInTDM
		IF bIsEnemy
			sReturn = GET_TDM_NAME(serverBDPassed, GET_RIVAL_TEAM(thisPlayer, serverBDpassed), bIsEnemy)
		ELSE
			sReturn = GET_TDM_NAME(serverBDPassed, GET_PLAYER_TEAM(thisPlayer), bIsEnemy)
		ENDIF
	ELSE
		sReturn = GET_PLAYER_NAME_FOR_HUD(thisPlayer, serverBDpassed, bIsEnemy, serverBDpassedLDB)
	ENDIF
	
	RETURN sReturn
ENDFUNC

FUNC BOOL IS_PLAYER(ServerBroadcastData &serverBDpassed)

	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL DRAW_AMMO(ServerBroadcastData &serverBDpassed)
	
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		
//			PRINTLN("DRAW_AMMO RETURN FALSE")
		
			RETURN FALSE
		ENDIF
	ENDIF
	
//	PRINTLN("DRAW_AMMO RETURN TRUE")

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_BVB_HELP(SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData &serverBDpassed)
	IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	TEXT_LABEL_15 tl15Help
	//-- Intro help
	IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_INTRO_HELP)
		IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
			IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)	
				INT iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
				INT iRivalTeam = (1-iMyTeam)
				PLAYER_INDEX playerRival
				
				IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
					playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
					IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
						IF IS_NET_PLAYER_OK(playerRival)
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
							OR IS_PLAYER_ON_BIKER_DM()
							
								tl15Help = "BVB_DESCH"
								
								IF IS_PLAYER_ON_BIKER_DM()

									IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)
										tl15Help = "BKVB_DESCH" // You have started a Deathmatch. Kill the rival Motorcyle Club to earn cash and RP.
									ELIF GB_IS_PLAYER_BOSS_OF_A_GANG(playerRival)
										tl15Help = "BKVB_DESCHV" // You have started a Deathmatch. Kill the rival Organization to earn cash and RP.
									ENDIF

								ENDIF
								
								PRINT_HELP_NO_SOUND(tl15Help) // Kill the rival Gang Boss to win.
							ELSE
								tl15Help = "BVB_DESCHG"
								
								IF IS_PLAYER_ON_BIKER_DM()
									IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(LocalPlayer)
										IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)
											tl15Help = "BKVBK_DESCHG" // Your MC President has started a Deathmatch. Kill the rival Motorcyle Club to earn cash and RP.
										ELSE
											tl15Help = "BKVBK_DESCV" // Your MC President has started a Deathmatch. Kill the rival Oraganization to earn cash and RP.
										ENDIF
									ELSE
										//-- Not in a biker gang
										IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)
											tl15Help = "BKVBK_MVPBK" // Your VIP has started a Deathmatch. Kill the rival Motorcyle Club to earn cash and RP.
										ELSE
											tl15Help = "BKVBK_MVPORG" // Your VIP has started a Deathmatch. Kill the rival Oraganization to earn cash and RP.
										ENDIF
									ENDIF
								ENDIF
								
								PRINT_HELP_NO_SOUND(tl15Help)
							ENDIF
							GB_SET_GANG_BOSS_HELP_BACKGROUND()
							SET_BIT(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_INTRO_HELP)
							CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] DONE INTRO HELP")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] NOT OK TO PRINT HELP - INTRO HELP")
			ENDIF
		ENDIF
	ENDIF
	
	INT iMyTeam
	INT iRivalTeam
	INT iD
	
	HUD_COLOURS hcGang
	
	PLAYER_INDEX playerRival
	
	//-- Help text for rival boss blip
	IF IS_BIT_SET(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_INTRO_HELP)
		IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_INTRO_HELP2)
			IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
				IF IS_OK_TO_PRINT_FREEMODE_HELP(FALSE, FALSE)	
					iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
					iRivalTeam = (1-iMyTeam)
					IF iRivalTeam >= 0 AND iRivalTeam < 2
						IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
							playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
							IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
								IF IS_NET_PLAYER_OK(playerRival)
									iD = GET_GANG_ID_FOR_PLAYER(playerRival)
									IF iD > -1
										hcGang = GET_HUD_COLOUR_FOR_GANG_ID(iD)	
										tl15Help = "BVB_HLP1"
										
										IF IS_PLAYER_ON_BIKER_DM()
											IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)
												tl15Help = "BKVBK_HLP1"
											ELIF GB_IS_PLAYER_BOSS_OF_A_GANG(playerRival)
												tl15Help = "BKVBK_HLP2"
											ENDIF
										ENDIF
										PRINT_HELP_WITH_COLOURED_STRING_NO_SOUND(tl15Help, "BVB_BLP", hcGang)
										GB_SET_GANG_BOSS_HELP_BACKGROUND()
										SET_BIT(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_INTRO_HELP2)
										CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] DONE BVB_BS1_DONE_INTRO_HELP2 HELP")
									#IF IS_DEBUG_BUILD
									ELSE
										CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_HELP] iD <= -1")
										#ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] NOT OK TO PRINT HELP - INTRO BVB_BS1_DONE_INTRO_HELP2")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BVB_OBJ(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	INT iMyTeam 
	INT iRivalTeam
	PLAYER_INDEX playerRival
	STRING sOrganization
	TEXT_LABEL_15 tl15Sublabel
	INT iD
	HUD_COLOURS hcGang
	IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_GB_START_OF_JOB)
		IF NOT IS_PLAYER_ON_BIKER_DM()
			IF NOT Is_This_The_Current_Objective_Text("GB_BVB_KILL")
			
			
				iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
				iRivalTeam = (1-iMyTeam)
				
				IF iRivalTeam >= 0 AND iRivalTeam < 2
					IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
						playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
						IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
							IF IS_NET_PLAYER_OK(playerRival)
								sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerRival)
								iD = GET_GANG_ID_FOR_PLAYER(playerRival)
								IF iD != -1
									hcGang = GET_HUD_COLOUR_FOR_GANG_ID(iD)
									tl15Sublabel = "GB_SGHT_BOSS"
									
	//								#IF FEATURE_BIKER
	//								IF IS_PLAYER_ON_BIKER_DM()
	//									IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)
	//										tl15Sublabel = "GB_BK_BOSS"
	//									ELIF GB_IS_PLAYER_BOSS_OF_A_GANG(playerRival)
	//										tl15Sublabel = "GB_BK_DMVIP"
	//									ENDIF
	//									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_OBJ] Using Biker label")
	//								ELSE
	//									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_OBJ] Not a biker!")	
	//								ENDIF
	//								#ENDIF
									Print_Objective_Text_With_Two_User_Created_Strings_And_String_Coloured("GB_BVB_KILL", GET_PLAYER_NAME(playerRival), tl15Sublabel, sOrganization, hcGang)	
									
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_OBJ] FORCE BLIP THIS PLAYER ", GET_PLAYER_NAME(playerRival))
								#IF IS_DEBUG_BUILD
								ELSE
									CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_OBJ] iD = -1")
									#ENDIF
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_OBJ] NOT FORCE BLIPPING PLAYER AS NOT OK!")
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//-- Biker DM
			IF NOT Is_This_The_Current_Objective_Text("GB_BK_DMOBJ")
				iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
				iRivalTeam = (1-iMyTeam)
				
				IF iRivalTeam >= 0 AND iRivalTeam < 2
					IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
						playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
						IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
							IF IS_NET_PLAYER_OK(playerRival)
								sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerRival)
								iD = GET_GANG_ID_FOR_PLAYER(playerRival)
								IF iD != -1
									hcGang = GET_HUD_COLOUR_FOR_GANG_ID(iD)
									Print_Objective_Text_With_Coloured_String("GB_BK_DMOBJ", sOrganization, hcGang)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//-- 2606380
	IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_RESET_NO_SHUFFLE)
		SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_RESET_NO_SHUFFLE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed,  PCF_PreventAutoShuffleToDriversSeat, FALSE)
		CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] MAINTAIN_BVB_OBJ PCF_PreventAutoShuffleToDriversSeat, FALSE ")
	ENDIF		
ENDPROC

PROC RESET_ALL_BVB_PLAYER_BLIPS(SHARED_DM_VARIABLES &dmVarsPassed)
	INT iPart
	PARTICIPANT_INDEX part
	PLAYER_INDEX PlayerId
	
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			Part = INT_TO_PARTICIPANTINDEX(iPart)
			PlayerId = NETWORK_GET_PLAYER_INDEX(Part)
			IF NOT IS_PLAYER_SCTV(PlayerId)

				IF IS_BIT_SET(dmVarsPassed.iBlipRivalGangBitset, iPart)
					SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TEMP_4, FALSE)
	
					FORCE_BLIP_PLAYER(PlayerId, FALSE, FALSE)
					SET_PLAYER_BLIP_AS_LONG_RANGE(PlayerId, FALSE)
					SET_FIXED_BLIP_SCALE_FOR_PLAYER(PlayerId, g_sMPTunables.fgangboss_Job_blip_scale, FALSE)
					CLEAR_BIT(dmVarsPassed.iBlipRivalGangBitset, iPart)
					CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [RESET_ALL_BVB_PLAYER_BLIPS] CLEAR RIVAL BLIP FOR PLAYER ", GET_PLAYER_NAME(PlayerId), " In same gang as ", GET_PLAYER_NAME(PlayerId))
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_BVB_BOSS_BLIP(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	
	INT iMyTeam
	INT iRivalTeam

	
	PLAYER_INDEX playerRival
	
		INT iPart
		PARTICIPANT_INDEX part
		PLAYER_INDEX PlayerId
	IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset ,BVB_BS1_RIVAL_BOSS_BLIP)
		iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
		iRivalTeam = (1-iMyTeam)
		IF iRivalTeam >= 0 AND iRivalTeam < 2
			IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
				playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
				IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
					IF IS_NET_PLAYER_OK(playerRival)
					
						IF NOT IS_PLAYER_ON_BIKER_DM()
							SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(playerRival, RADAR_TRACE_TEMP_4, TRUE)
							
							FORCE_BLIP_PLAYER(playerRival, TRUE, TRUE)
							SET_PLAYER_BLIP_AS_LONG_RANGE(playerRival, TRUE)
							SET_FIXED_BLIP_SCALE_FOR_PLAYER(playerRival, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_BOSS_BLIP] SET RIVAL BOSS BLIP FOR PLAYER ", GET_PLAYER_NAME(playerRival))
							
							SET_BIT(dmVarsPassed.iBVBbitset ,BVB_BS1_RIVAL_BOSS_BLIP)
							
							//-- Blip everyone on the rival team
						ELSE
							REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPart
								IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
									Part = INT_TO_PARTICIPANTINDEX(iPart)
									PlayerId = NETWORK_GET_PLAYER_INDEX(Part)
									IF NOT IS_PLAYER_SCTV(PlayerId)
										IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PlayerId, playerRival)
											IF NOT IS_BIT_SET(dmVarsPassed.iBlipRivalGangBitset, iPart)
												SET_CUSTOM_SPRITE_BLIP_FOR_PLAYER(PlayerId, RADAR_TRACE_TEMP_4, TRUE)
								
												FORCE_BLIP_PLAYER(PlayerId, TRUE, TRUE)
												SET_PLAYER_BLIP_AS_LONG_RANGE(PlayerId, TRUE)
												IF NOT IS_PLAYER_ON_BIKER_DM()
													SET_FIXED_BLIP_SCALE_FOR_PLAYER(PlayerId, g_sMPTunables.fgangboss_Job_blip_scale, TRUE)
												ENDIF
												SET_BIT(dmVarsPassed.iBlipRivalGangBitset, iPart)
												CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - [MAINTAIN_BVB_BOSS_BLIP] SET RIVAL BLIP FOR PLAYER ", GET_PLAYER_NAME(PlayerId), " In same gang as ", GET_PLAYER_NAME(PlayerId))
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset ,BVB_BS1_CHECK_RIVAL_IS_BIKER)

		iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
		iRivalTeam = (1-iMyTeam)
		IF iRivalTeam >= 0 AND iRivalTeam < 2
			IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
				playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
				IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
					IF GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(playerRival)
						dmVarsPassed.bRivalBossIsBiker = TRUE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - Set bRivalBossIsBiker as rival boss ", GET_PLAYER_NAME(playerRival), " is a biker")
					ELSE
						dmVarsPassed.bRivalBossIsBiker = FALSE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - Not settt bRivalBossIsBiker as rival boss ", GET_PLAYER_NAME(playerRival), " is not a biker")
					ENDIF
					SET_BIT(dmVarsPassed.iBVBbitset ,BVB_BS1_CHECK_RIVAL_IS_BIKER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF					
ENDPROC

PROC MAINTAIN_BVB_BOSS_MARKER(ServerBroadcastData &serverBDpassed)
	IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	
	INT iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
	INT iRivalTeam = (1-iMyTeam)
	INT iR, iG, iB, iA
	
	PLAYER_INDEX playerRival
	
	HUD_COLOURS hclPlayer
	
	BOOL bDoMarker 
	IF iRivalTeam >= 0 AND iRivalTeam < 2
		IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
			playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
			IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
				IF IS_NET_PLAYER_OK(playerRival)
					hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerRival)
					GET_HUD_COLOUR(hclPlayer, iR, iG, iB, iA)
					bDoMarker = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDoMarker
		IF NOT IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(playerRival))
			DRAW_MARKER(MARKER_ARROW, GET_ENTITY_COORDS(GET_PLAYER_PED(playerRival))+<<0,0,2.0>>, <<0,0,0>>, <<180,0,0>>, <<0.5,0.5,0.5>>, iR, iG, iB, 100, TRUE, TRUE)
		ELSE
			FM_EVENTDRAW_MARKER_ABOVE_VEHICLE(GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(playerRival)), iR, iG, iB)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Don't want participants to be driven by non-participants, or there's issues to do with not being able to destroy the vehicle. 
PROC MAINTAIN_PLAYER_DRIVEN_BY_NON_PARTICIPANT()
	VEHICLE_INDEX vehPlayer
	PED_INDEX pedDriver
	PLAYER_INDEX playerDriver
	
	IF bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			pedDriver = GET_PED_IN_VEHICLE_SEAT(vehPlayer)
			IF NOT IS_PED_INJURED(pedDriver)
				IF pedDriver != LocalPlayerPed
					IF IS_PED_A_PLAYER(pedDriver)
						playerDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
						IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerDriver)
							IF (GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(LocalPlayerPed, SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK)
								CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS - GIVING TASK_LEAVE_ANY_VEHICLE AS I'M IN A VEHICLE WITH NON-PARTICIPANT ", GET_PLAYER_NAME(playerDriver))
								TASK_LEAVE_ANY_VEHICLE(LocalPlayerPed)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF						
ENDPROC
 // FEATURE_GANG_BOSS

PROC SET_EARLY_END_DEATCHMATCH_SPECTATE(INT iNewState, PlayerBroadcastData &playerBDPassed[])	
	PRINTLN("[LM][DM_LIVES][SET_EARLY_END_DEATCHMATCH_SPECTATE] - OldState: ", playerBDPassed[iLocalPart].iSpectatorState, " NewState: ", iNewState)	
	playerBDPassed[iLocalPart].iSpectatorState = iNewState
ENDPROC

PROC REQUEST_SPECTATOR_CELEB_SCREEN(SHARED_DM_VARIABLES &dmVarsPassed)
	INITIALISE_CELEBRATION_SCREEN_DATA(dmVarsPassed.sCelebrationData)
	CELEBRATION_SCREEN_TYPE eCelebType = CELEBRATION_STANDARD
	REQUEST_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, eCelebType)
	IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
		SET_MANUAL_RESPAWN_STATE(MRS_EXIT_SPECTATOR_AFTERLIFE)
		DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
		RESET_GLOBAL_SPECTATOR_STRUCT()
	ENDIF
ENDPROC

FUNC BOOL FIND_PREFERRED_SPECTATOR_TARGET(INT &iTarget)
	INT iPart			
	FOR iPart = 0 TO (MAX_NUM_MC_PLAYERS - 1)
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPart))
			PLAYER_INDEX tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPart))
			IF IS_NET_PLAYER_OK(tempPlayer)
				SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
				iTarget = NATIVE_TO_INT(tempPlayer)
				PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - FIND_PREFERRED_SPECTATOR_TARGET - SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS.")				
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC CLEANUP_EARLY_END_DEATHMATCH_SPECTATORS(PlayerBroadcastData &playerBDPassed[])
	IF playerBDPassed[iLocalPart].iSpectatorState = ci_SpectatorState_WAIT
	OR playerBDPassed[iLocalPart].iSpectatorState = ci_SpectatorState_END
		EXIT
	ENDIF
	PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - CLEANUP_EARLY_END_DEATHMATCH_SPECTATORS - Cleaning up the early end spectator state!")
	DO_SCREEN_FADE_IN(250)			
	DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
	NETWORK_SET_IN_SPECTATOR_MODE(FALSE, LocalPlayerPed)	
	SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_END, playerBDPassed)
ENDPROC
			
PROC PROCESS_EARLY_END_DEATHMATCH_SPECTATORS(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData &serverBDpassed)
	
	IF (g_bMissionEnding
	OR g_bDeathmatchFinished
	OR GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
	OR HAS_DEATHMATCH_FINISHED(serverBDpassed))
	AND playerBDPassed[iLocalPart].iSpectatorState < ci_SpectatorState_CLEANUP
		PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - HAS_DEATHMATCH_FINISHED sending to cleanup...")
		SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_CLEANUP, playerBDPassed)
	ENDIF 
	
	SWITCH playerBDPassed[iLocalPart].iSpectatorState
		CASE ci_SpectatorState_WAIT
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Spectator_Arena_Roaming)
				IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.tdSpectatorStartTimer)
					PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Starting Timer.")
					START_NET_TIMER(dmVarsPassed.tdSpectatorStartTimer)
				ENDIF			
				IF HAS_NET_TIMER_EXPIRED_READ_ONLY(dmVarsPassed.tdSpectatorStartTimer, 3000)
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Fading out.")
						DO_SCREEN_FADE_OUT(500)
					ELSE
						IF IS_SCREEN_FADED_OUT()
							NETWORK_RESURRECT_LOCAL_PLAYER(g_FMMC_STRUCT.vStartPos, 0.0, 0, FALSE, FALSE)
							PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Fade complete.")
							CLEANUP_KILL_STRIP()
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
							CLEAR_ALL_BIG_MESSAGES()						
							SET_BIT(MPSpecGlobals.SCTVData.iBitSet, SCTV_BIT_ALLOW_SPECTATING_RIVAL_TEAMS)
							MPSpecGlobals.iPreferredSpectatorPlayerID = -1
							SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_INIT, playerBDPassed)
						ELSE
							PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Waiting for fade out.")
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Waiting for timer to leave.")
				ENDIF
			ELSE
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SPECTATOR)
				SET_GO_TO_SPECIAL_SPECTATOR()
				CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_Roaming_Spectator_Free_Spin_On_Death)
					SET_SPECIAL_SPECTATOR_INITIAL_WHEEL_SPIN()
				ENDIF
				SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_END, playerBDPassed)
			ENDIF			
		BREAK
		
		CASE ci_SpectatorState_INIT
			IF NOT GET_ENTITY_COLLISION_DISABLED(LocalPlayerPed)
				SET_ENTITY_COLLISION(LocalPlayerPed, FALSE)
				PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Disabled collision for local player as spectator")
			ENDIF
			IF FIND_PREFERRED_SPECTATOR_TARGET(MPSpecGlobals.iPreferredSpectatorPlayerID)
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SPECTATOR)
				SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_SET_TO_SPECTATOR, playerBDPassed)
			ELSE
				PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Cannot find a valid spectator target yet.")
			ENDIF
		BREAK
		
		CASE ci_SpectatorState_SET_TO_SPECTATOR
			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			IF GET_PED_PARACHUTE_STATE(GET_PLAYER_PED(LocalPlayer)) <> PPS_INVALID
				CLEAR_PED_TASKS(GET_PLAYER_PED(LocalPlayer))
			ENDIF	
			INT i	
			FOR i = 0 TO (MAX_NUM_MC_PLAYERS - 1)
				PARTICIPANT_INDEX tempPart 
				tempPart = INT_TO_PARTICIPANTINDEX(i)
				IF NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
					PLAYER_INDEX playerIndex
					playerIndex = NETWORK_GET_PLAYER_INDEX(tempPart)
					
					NETWORK_OVERRIDE_SEND_RESTRICTIONS(playerIndex, FALSE)
					PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Setting All Players NETWORK_OVERRIDE_SEND_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
					NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS(playerIndex, FALSE)
					PRINTLN("[LM][DM_LIVES][PROCESS_EARLY_END_DEATHMATCH_SPECTATORS] - Setting All Players NETWORK_OVERRIDE_RECEIVE_RESTRICTIONS to FALSE... Name = ", GET_PLAYER_NAME(playerIndex))
				ENDIF
			ENDFOR
			g_bEndOfMissionCleanUpHUD = FALSE
			HIDE_SPECTATOR_BUTTONS(FALSE)
			HIDE_SPECTATOR_HUD(FALSE)
			REPLAY_PREVENT_RECORDING_THIS_FRAME()
			SET_GAME_STATE_ON_DEATH(AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
			SET_BIG_MESSAGE_ENABLED_WHEN_SPECTATING(TRUE)
			ACTIVATE_SPECTATOR_CAM(g_BossSpecData)
			SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_MAINTAIN, playerBDPassed)			
		BREAK
		
		CASE ci_SpectatorState_MAINTAIN
			MAINTAIN_SPECTATOR(g_BossSpecData)
		BREAK
		
		CASE ci_SpectatorState_CLEANUP
			CLEANUP_EARLY_END_DEATHMATCH_SPECTATORS(playerBDPassed)
		BREAK
		
		CASE ci_SpectatorState_END
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

FUNC BOOL OK_TO_SHOW_SCORES_BOTTOM_RIGHT(ServerBroadcastData &serverBDpassed#IF IS_DEBUG_BUILD, SHARED_DM_VARIABLES &dmVarsPassed#ENDIF)

	#IF IS_DEBUG_BUILD
		IF dmVarsPassed.debugDmVars.bHideDMHud = TRUE
			PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, bHideDMHud")
		
			RETURN FALSE
		ENDIF
		
		IF dmVarsPassed.debugDmVars.bDrawBIGLeaderboard = TRUE
			PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, bDrawBIGLeaderboard")
			RETURN FALSE
		ENDIF
		
		IF dmVarsPassed.debugDmVars.bDrawDpadLeaderboard = TRUE
			PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, bDrawDpadLeaderboard")
			RETURN FALSE
		ENDIF
	#ENDIF

	IF NOT HAS_DM_STARTED(serverBDpassed)
		PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, HAS_DM_STARTED")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_OK_TO_DRAW_BOTTOM_RIGHT_HUD(TRUE)
		PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, IS_OK_TO_DRAW_BOTTOM_RIGHT_HUD")
		RETURN FALSE
	ENDIF
	
	IF g_bMissionEnding
		PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, g_bMissionEnding")
		RETURN FALSE
	ENDIF
	
	IF g_bInAtm
		PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, g_bInAtm")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	AND g_sImpromptuVars.playerImpromptuRival = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF bIsSCTV
		IF g_BossSpecData.specHUDData.specHUDMode = SPEC_HUD_MODE_HIDDEN
		
			PRINTLN("OK_TO_SHOW_SCORES_BOTTOM_RIGHT, SPEC_HUD_MODE_HIDDEN")
		
			RETURN FALSE
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC DRAW_TIMER_WHEN_DEAD(ServerBroadcastData &serverBDpassed)
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_RemoveTimeLimitForDM)
	AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE_PRIORITY_HIGH)
		INT iTime = GET_HUD_TIME(serverBDpassed) 
		DRAW_GENERIC_TIMER(iTime, "TIMER_TIME", 0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	ENDIF
ENDPROC

PROC HIDE_AREA_NAMES_BOTTOM_RIGHT(ServerBroadcastData &serverBDpassed)
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PERK_ACTIVE()

	IF g_e_Chosen_Perk = KS_PP_SQUARE_ALL_BLIP
	ELIF g_e_Chosen_Perk = KS_PP_CIRCLE_HIDE_BLIP
	ELIF g_e_Chosen_Perk = KS_PP_TRIANGLE_QUAD
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_PERK_TIMER(SHARED_DM_VARIABLES &dmVarsPassed)
	INT iTimeLeftPerk
	
	SWITCH dmVarsPassed.iPerkBarProgress 
	
		CASE 0
			IF PERK_ACTIVE()
				
				dmVarsPassed.iPerkBarProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_STARTED(dmVarsPassed.timePerk)
				iTimeLeftPerk = (PERK_TIMEOUT - (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timePerk.Timer)))
				IF NOT HAS_NET_TIMER_EXPIRED(dmVarsPassed.timePerk, PERK_TIMEOUT)
				AND (iTimeLeftPerk >= 0)
					DRAW_GENERIC_TIMER(iTimeLeftPerk, "HUD_PERK", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
				ELSE
					DRAW_GENERIC_TIMER(0, "HUD_PERK", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_ARENA_INTERIOR_MAP(SHARED_DM_VARIABLES &dmVarsPassed)
	BOOL bShowInterior = TRUE
	VECTOR vInteriorPos = <<2800.0, -3800.0, 100.0>>
	INT i
	
	IF IS_PAUSE_MENU_ACTIVE()
		IF NOT IS_PAUSEMAP_IN_INTERIOR_MODE()
			bShowInterior = FALSE
		ENDIF
	ELIF g_PlayerBlipsData.bBigMapIsActive
		bShowInterior = FALSE
	ENDIF
	
	IF bShowInterior
		SET_RADAR_AS_INTERIOR_THIS_FRAME(HASH("xs_x18_int_01"),vInteriorPos.x, vInteriorPos.y, 0, GET_ARENA_ENTITY_SET_LEVEL_NUMBER(g_FMMC_STRUCT.sArenaInfo.iArena_Theme, g_FMMC_STRUCT.sArenaInfo.iArena_Variation))
		HIDE_MINIMAP_EXTERIOR_MAP_THIS_FRAME()
		
		REPEAT FMMC_MAX_NUM_DYNOPROPS i
			IF NOT DOES_BLIP_EXIST(dmVarsPassed.biTrapCamBlip[i])
				RELOOP
			ENDIF
			SET_BLIP_DISPLAY(dmVarsPassed.biTrapCamBlip[i], DISPLAY_BLIP)
		ENDREPEAT
		
		IF DOES_BLIP_EXIST(biBlipTrapCam)
			SET_BLIP_DISPLAY(biBlipTrapCam, DISPLAY_BLIP)
		ENDIF
			
	ELSE
		SET_FAKE_PAUSEMAP_PLAYER_POSITION_THIS_FRAME(ARENA_X, ARENA_Y)
		HIDE_MINIMAP_INTERIOR_MAP_THIS_FRAME()
		
		REPEAT FMMC_MAX_NUM_DYNOPROPS i
			IF NOT DOES_BLIP_EXIST(dmVarsPassed.biTrapCamBlip[i])
				RELOOP
			ENDIF
			SET_BLIP_DISPLAY(dmVarsPassed.biTrapCamBlip[i], DISPLAY_NOTHING)
		ENDREPEAT
		
		IF DOES_BLIP_EXIST(biBlipTrapCam)
			SET_BLIP_DISPLAY(biBlipTrapCam, DISPLAY_NOTHING)
		ENDIF
			
	ENDIF
ENDPROC

PROC PROCESS_INTERIOR_BLIP_DISPLACEMENT(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, INTERIOR2_ARENA)
		IF GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = g_ArenaInterior
		OR GET_INTERIOR_FROM_ENTITY(LocalPlayerPed) = dmVarsPassed.iArenaInterior_VIPLoungeIndex
			IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLACEMENT_BLIP_SET)
				SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLACEMENT_BLIP_SET)
				SET_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA(NATIVE_TO_INT(g_ArenaInterior),<<ARENA_X, ARENA_Y, ARENA_Z>>)
			ENDIF
			PROCESS_ARENA_INTERIOR_MAP(dmVarsPassed)
		ELSE
			IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLACEMENT_BLIP_SET)
				CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLACEMENT_BLIP_SET)
				CLEAR_LOCAL_PLAYER_CUSTOM_DISPLACED_INTERIOR_DATA()
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DRAW_PLAYERS_REMAINING_FOR_DM(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ArenaPlayersRemaining)
		EXIT
	ENDIF
	
	INT i
	INT iPlayersRemaining[FMMC_MAX_TEAMS]
	INT iPlayersMax[FMMC_MAX_TEAMS]
	FOR i = 0 TO NUM_NETWORK_PLAYERS-1
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			
			IF NOT IS_ARENA_WARS_JOB()
			AND piPlayer = thisPlayer
			AND NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
				RELOOP
			ENDIF
			
			INT iTeam
			IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
				iTeam = GET_PLAYER_TEAM(piPlayer)
			ENDIF
			IF iTeam > -1 AND iTeam < FMMC_MAX_TEAMS				
				IF NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
				AND NOT IS_PLAYER_SCTV(piPlayer)
					iPlayersMax[iTeam]++
					
					IF NOT IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
						IF IS_ENTITY_ALIVE(GET_PLAYER_PED(piPlayer))
						AND NOT IS_PLAYER_USING_ACTIVE_ROAMING_SPECTATOR_MODE(piPlayer)
							iPlayersRemaining[iTeam]++
						ENDIF
					ELSE
						IF NOT IS_BIT_SET(playerBDPassed[i].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
							iPlayersRemaining[iTeam]++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		FOR i = 0 TO (FMMC_MAX_TEAMS - 1)
			IF iPlayersMax[i] > 0
				TEXT_LABEL_15 tl15 = "ARNA_RMNI_"
				HUD_COLOURS hcTeamColour
				IF IS_ARENA_WARS_JOB()
					IF i = GET_PLAYER_TEAM(thisPlayer)
						tl15 += "Y"
						hcTeamColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
					ELSE
						tl15 += "C"
						hcTeamColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
					ENDIF				
					tl15 += i
				ELSE
					IF i = GET_PLAYER_TEAM(thisPlayer)
						tl15 = "DM_MODE_TDM1"
						hcTeamColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
					ELSE
						tl15 = "DZ_E_TEAM_U"
						hcTeamColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_SharedTeamLives)
					
					INT iSharedTeamLivesRemaining = (GET_MAX_NUMBER_OF_SHARED_LIVES_FOR_TEAM(serverBDpassed, i) - serverBDpassed.iTeamDeaths[i])
					IF iSharedTeamLivesRemaining < 0
						iSharedTeamLivesRemaining = 0
					ENDIF
					TEXT_LABEL_15 tlLives
					IF i = GET_PLAYER_TEAM(thisPlayer)
						tlLives = "FMMC_AL_L"
					ELSE
						tlLives = "MC_OTLIVES"
					ENDIF
					
					IF DOES_TEAM_HAVE_UNLIMITED_LIVES(serverBDpassed, i)
						DRAW_GENERIC_DOUBLE_TEXT(tlLives, "DM_TLT_UNL", FALSE, FALSE, DEFAULT, hcTeamColour, IS_LANGUAGE_NON_ROMANIC())
					ELSE
						IF iSharedTeamLivesRemaining <= 1
							DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlayersRemaining[i], iPlayersMax[i], tl15, DEFAULT, hcTeamColour,  DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcTeamColour)
						ELSE
							DRAW_GENERIC_BIG_NUMBER(iSharedTeamLivesRemaining, tlLives, DEFAULT, hcTeamColour, DEFAULT, DEFAULT, DEFAULT, hcTeamColour)
						ENDIF
					ENDIF
				ELSE
					DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlayersRemaining[i], iPlayersMax[i], tl15, DEFAULT, hcTeamColour,  DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hcTeamColour)
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		TEXT_LABEL_15 tl15 = "ARNA_RMNI_CR"
		IF NOT IS_ARENA_WARS_JOB()
			tl15 = "ENMY_RMN"
		ENDIF
		DRAW_GENERIC_BIG_DOUBLE_NUMBER(iPlayersRemaining[0], iPlayersMax[0], tl15)
	ENDIF
	
ENDPROC

PROC PROCESS_LAST_LIFE_SHARD(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData &serverBDpassed, PLAYER_INDEX thisPlayer)
	IF NOT IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DMLastLifeShard)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_LAST_LIFE_SHARD_DONE)
		EXIT
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(thisPlayer)
		EXIT
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		EXIT
	ENDIF
	
	INT iMaxLives = GET_MAX_NUMBER_OF_LIVES_FOR_PLAYER(playerBDPassed, serverBDpassed, thisPlayer)
	INT iCurrentLives = iMaxLives - GET_NUMBER_OF_DEATHS_FOR_PLAYER(playerBDPassed, serverBDpassed, thisPlayer)
	
	IF iCurrentLives <= 1
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "DM_LST_LF", "", DEFAULT, 7500)
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_LAST_LIFE_SHARD_DONE)
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_SHOW_FFA_HUD_AND_TDM_HUD()
	RETURN NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType) 
			AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_FFAScoringInTDM)
			AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_FFAHUDAndTDM)
ENDFUNC

FUNC BOOL SHOULD_SHOW_FFA_HUD_INSTEAD_OF_TDM_HUD()
	RETURN NOT IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_LivesEndType)
			AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_FFAScoringInTDM)
			AND IS_BIT_SET(g_FMMC_STRUCT.sDMCustomSettings.iDMOptionsMenuBitset, ciDM_OptionsBS_FFAHUDInsteadOfTDM)
ENDFUNC

// Player scores and time left
PROC DRAW_DM_HUD(PLAYER_INDEX thisPlayer, ServerBroadcastData &serverBDpassed, BOOL bDrawAmmo, SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB, PlayerBroadcastData &playerBDPassed[]) 
//	PRINTLN("DRAW_DM_HUD CAlled...")
	INT iMyTeam	
	INT iRivalTeam
	INT iTimeLeft
	iTimeLeft = GET_HUD_TIME(serverBDpassed)

	IF (NOT OK_TO_SHOW_SCORES_BOTTOM_RIGHT(serverBDpassed#IF IS_DEBUG_BUILD, dmVarsPassed #ENDIF)
	OR iTimeLeft <= 0)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_RemoveTimeLimitForDM)
	OR IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		IF iTimeLeft <= 0
//			PRINTLN("DRAW_DM_HUD EXIT 1 - iTimeLeft <= 0")
		ENDIF
		
		IF NOT OK_TO_SHOW_SCORES_BOTTOM_RIGHT(serverBDpassed#IF IS_DEBUG_BUILD, dmVarsPassed #ENDIF)
//			PRINTLN("DRAW_DM_HUD EXIT 1 - OK_TO_SHOW_SCORES_BOTTOM_RIGHT")
		ENDIF
		EXIT
	ENDIF
	
	IF bIsAnySpectator
	AND IS_SPECTATOR_HUD_HIDDEN()
		EXIT
	ENDIF
	
	IF IS_NET_PLAYER_OK(thisPlayer, FALSE)
//		PRINTLN("DRAW_DM_HUD 22.")
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(thisPlayer)
		OR IS_PLAYER_ON_IMPROMPTU_DM()
		OR IS_PLAYER_ON_BOSSVBOSS_DM()
//			PRINTLN("DRAW_DM_HUD 23")
			#IF IS_DEBUG_BUILD
				DEBUG_HIDE_HUD(dmVarsPassed)
			#ENDIF
			
			IF bDrawAmmo
				DRAW_KILLSTREAK_DISPLAY(GlobalplayerBD_FM[NATIVE_TO_INT(thisPlayer)].iKillStreak) 
				SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			ENDIF
			
			// 1603129
			DRAW_PERK_TIMER(dmVarsPassed)
			
			DRAW_VEH_HEALTHBAR_OVERHEADS()
			
			IF IS_PLAYER_ON_BOSSVBOSS_DM()
				iMyTeam 			= GET_PLAYER_TEAM(thisPlayer)
				iRivalTeam 			= (1 - iMyTeam)
				HUD_COLOURS hclPlayer 	= GB_GET_PLAYER_GANG_HUD_COLOUR(LocalPlayer)
				PLAYER_INDEX playerRival
				TEXT_LABEL_15 tl15Score = "BVB_MYLV"
				IF iRivalTeam >= 0 AND iRivalTeam < 2
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG() 
					OR IS_PLAYER_ON_BIKER_DM()
					
						IF IS_PLAYER_ON_BIKER_DM()
							tl15Score = "BKVBK_MYLV"
						ENDIF
						
						IF serverBDpassed.iLivesRemaining[iMyTeam] > 0 
							DRAW_GENERIC_SCORE(serverBDpassed.iLivesRemaining[iMyTeam], tl15Score, DEFAULT, hclPlayer, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hclPlayer) // Your lives
						ELSE
							DRAW_GENERIC_SCORE(serverBDpassed.iLivesRemaining[iMyTeam], tl15Score, 999999999, HUD_COLOUR_RED, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDFLASHING_FLASHRED, DEFAULT, HUD_COLOUR_RED)
						ENDIF
					ELSE
						tl15Score = "BVB_MYTLV"
						
						IF IS_PLAYER_ON_BIKER_DM()
							tl15Score = "BKVBK_MYTLV"
						ENDIF
						
						IF serverBDpassed.iLivesRemaining[iMyTeam] > 0 
							DRAW_GENERIC_SCORE(serverBDpassed.iLivesRemaining[iMyTeam], tl15Score, DEFAULT, hclPlayer, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hclPlayer) // Your boss lives
						ELSE
							DRAW_GENERIC_SCORE(serverBDpassed.iLivesRemaining[iMyTeam], tl15Score, 999999999, HUD_COLOUR_RED, HUDORDER_THIRDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, HUDFLASHING_FLASHRED,DEFAULT, HUD_COLOUR_RED)
						ENDIF
					ENDIF
					
					playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
					IF playerRival != INVALID_PLAYER_INDEX()
						IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
							tl15Score = "BVB_RTLV"
							
							IF IS_PLAYER_ON_BIKER_DM()
								tl15Score = "BKVBK_RTLV"
							ENDIF
						
							hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerRival)
							DRAW_GENERIC_SCORE(serverBDpassed.iLivesRemaining[iRivalTeam], tl15Score, DEFAULT, hclPlayer, HUDORDER_SECONDBOTTOM, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, hclPlayer) // Enemy boss lives
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_RemoveTimeLimitForDM)
						IF iTimeLeft > 31000
							tl15Score = "GB_WORK_END"

							IF IS_PLAYER_ON_BIKER_DM()
								tl15Score = "CLUB_WORK_END"
							ENDIF
								
							DRAW_GENERIC_TIMER(iTimeLeft, tl15Score,0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM)
						ELSE
							tl15Score = "GB_WORK_END"

							IF IS_PLAYER_ON_BIKER_DM()
								tl15Score = "CLUB_WORK_END"
							ENDIF
							DRAW_GENERIC_TIMER(iTimeLeft, tl15Score,0, TIMER_STYLE_DONTUSEMILLISECONDS, 0, PODIUMPOS_NONE, HUDORDER_BOTTOM, DEFAULT, HUD_COLOUR_RED, DEFAULT, DEFAULT, DEFAULT, HUD_COLOUR_RED)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				TEXT_LABEL tlLocal
				TEXT_LABEL tlEnemy
				
				IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
				AND NOT SHOULD_SHOW_FFA_HUD_INSTEAD_OF_TDM_HUD()
					tlLocal = GET_NAME_FOR_HUD(thisPlayer, serverBDpassed, FALSE, serverBDpassedLDB)
					tlEnemy = GET_NAME_FOR_HUD(thisPlayer, serverBDpassed, TRUE, serverBDpassedLDB)
					iMyTeam = GET_PLAYER_TEAM(thisPlayer)
					iRivalTeam = GET_RIVAL_TEAM(thisPlayer, serverBDpassed)
//					PRINTLN("DRAW_DM_HUD 1 iMyTeam = ",iMyTeam, " iRivalTeam = ", iRivalTeam)
					
					HUD_COLOURS hcLocalColour, hcEnemyColour
					IF NOT bIsAnySpectator
						hcLocalColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE, DEFAULT)
						hcEnemyColour = GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE, iRivalTeam)
					ELSE
						hcLocalColour = GET_HUD_COLOUR_FOR_TARGET_RELATION(thisPlayer, FALSE)
						hcEnemyColour = GET_HUD_COLOUR_FOR_TARGET_RELATION(thisPlayer, TRUE, iRivalTeam)
					ENDIF
					
					DRAW_PLAYER_DEATHMATCH_HUD(	GET_TEAM_DM_SCORE(serverBDpassed, iMyTeam),
												GET_TARGET_SCORE(serverBDpassed, iMyTeam),
												tlLocal, 
												GET_TEAM_DM_SCORE(serverBDpassed, iRivalTeam),
												tlEnemy,
												hcLocalColour,
												hcEnemyColour,
												HUDORDER_SIXTHBOTTOM, 
												HUDORDER_FIFTHBOTTOM,
												iTimeLeft,
												"TIMER_TIME",
												0, 
												IS_PLAYER(serverBDpassed),
												IS_TARGET_MATCH(serverBDpassed),
												!DO_ALL_DM_TEAMS_HAVE_SAME_TARGET_SCORE(),
												GET_TARGET_SCORE(serverBDpassed, iRivalTeam))
												
					IF SHOULD_SHOW_FFA_HUD_AND_TDM_HUD()
						TEXT_LABEL tlLocalPlayerName = GET_PLAYER_NAME_FOR_HUD(thisPlayer, serverBDpassed, FALSE, serverBDpassedLDB)
						TEXT_LABEL tlEnemyPlayerName = GET_PLAYER_NAME_FOR_HUD(thisPlayer, serverBDpassed, TRUE, serverBDpassedLDB)
						IF IS_PLAYER_WINNING(thisPlayer, serverBDpassed, serverBDpassedLDB)
							DRAW_GENERIC_SCORE(GET_SCORE_FOR_POS(serverBDpassedLDB, 1), tlEnemyPlayerName, 0, GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE), HUDORDER_SECONDBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE))
							DRAW_GENERIC_SCORE(GET_PLAYER_SCORE(serverBDpassed, thisPlayer, serverBDpassedLDB), tlLocalPlayerName, 0, GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE), HUDORDER_THIRDBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE))
						ELSE
							DRAW_GENERIC_SCORE(GET_SCORE_FOR_POS(serverBDpassedLDB, 0), tlEnemyPlayerName, 0, GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE), HUDORDER_SECONDBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE))
							DRAW_GENERIC_SCORE(GET_PLAYER_SCORE(serverBDpassed, thisPlayer, serverBDpassedLDB), tlLocalPlayerName, 0, GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE), HUDORDER_THIRDBOTTOM, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE))
						ENDIF
					ENDIF
					
				ELSE
//					PRINTLN("DRAW_DM_HUD 2") 
					tlLocal = GET_NAME_FOR_HUD(thisPlayer, serverBDpassed, FALSE, serverBDpassedLDB, SHOULD_SHOW_FFA_HUD_INSTEAD_OF_TDM_HUD())
					tlEnemy = GET_NAME_FOR_HUD(thisPlayer, serverBDpassed, TRUE, serverBDpassedLDB, SHOULD_SHOW_FFA_HUD_INSTEAD_OF_TDM_HUD())
					IF IS_PLAYER_WINNING(thisPlayer, serverBDpassed, serverBDpassedLDB)
//						PRINTLN("DRAW_DM_HUD 3")
						DRAW_PLAYER_DEATHMATCH_HUD(	GET_PLAYER_SCORE(serverBDpassed, thisPlayer, serverBDpassedLDB),
													GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam),
													tlLocal, 
													GET_SCORE_FOR_POS(serverBDpassedLDB, 1),
													tlEnemy,
													GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE),
													GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE), 
													HUDORDER_SIXTHBOTTOM, 
													HUDORDER_FIFTHBOTTOM,
													iTimeLeft,
													"TIMER_TIME",
													0, 
													TRUE,
													IS_TARGET_MATCH(serverBDpassed))
					ELSE
//						PRINTLN("DRAW_DM_HUD 4")
						DRAW_PLAYER_DEATHMATCH_HUD(	GET_PLAYER_SCORE(serverBDpassed, thisPlayer, serverBDpassedLDB),
													GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam),
													tlLocal, 
													GET_SCORE_FOR_POS(serverBDpassedLDB, 0),
													tlEnemy,
													GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE),
													GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE), 
													HUDORDER_SIXTHBOTTOM, 
													HUDORDER_FIFTHBOTTOM,
													iTimeLeft,
													"TIMER_TIME",
													0, 
													TRUE,
													IS_TARGET_MATCH(serverBDpassed))
					ENDIF
				ENDIF
			ENDIF
			 // #IF FEATURE_GANG_BOSS
			
			DRAW_PLAYERS_REMAINING_FOR_DM(thisPlayer, serverBDpassed, playerBDPassed)
			
			PROCESS_FMMC_VEHICLE_MACHINE_GUN_HUD(dmVarsPassed.sVehMG)
			
			PROCESS_POWERUP_HUD(dmVarsPassed.sPowerUps)
			
			PROCESS_DEATHMATCH_LIVES_REMAINING(GET_NUMBER_OF_DEATHS_FOR_PLAYER(playerBDPassed, serverBDpassed, thisPlayer), GET_MAX_NUMBER_OF_LIVES_FOR_PLAYER(playerBDPassed, serverBDpassed, thisPlayer))
			
			PROCESS_LAST_LIFE_SHARD(dmVarsPassed, playerBDPassed, serverBDpassed, thisPlayer)
			
			#IF IS_DEBUG_BUILD
			IF IS_DEBUG_KEY_PRESSED(KEY_NUMPAD4, KEYBOARD_MODIFIER_NONE, "")
				IF IS_PLAYER_WINNING(thisPlayer, serverBDpassed, serverBDpassedLDB)
					PRINTLN("PLAYER_WINNING ")
				ELSE
					PRINTLN("PLAYER_LOSING ")
				ENDIF
				PRINTLN("GET_SCORE_FOR_POS(serverBDpassedLDB, 0) ", GET_SCORE_FOR_POS(serverBDpassedLDB, 0))
				PRINTLN("GET_SCORE_FOR_POS(serverBDpassedLDB, 1) ", GET_SCORE_FOR_POS(serverBDpassedLDB, 1))
			ENDIF
			#ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC DRAW_SPECTATOR_HUD(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB, PlayerBroadcastData &playerBDPassed[])
	
	IF DOES_ENTITY_EXIST(PlayerPedToUse)
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerToUse)
			DRAW_DM_HUD(PlayerToUse, serverBDpassed, FALSE, dmVarsPassed, serverBDpassedLDB, playerBDPassed)
		ENDIF
	ENDIF	
ENDPROC

// -----------------------------------		DISPLAY LEADERBOARD

PROC CLEAR_DM_DPAD_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_IHELP_OTP")
		CLEAR_HELP(TRUE)
	ENDIF
//	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("BASEJ_HELP3")
//		CLEAR_HELP(TRUE)
//	ENDIF
ENDPROC

FUNC BOOL SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU(INT leaderboardIndex, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	UNUSED_PARAMETER(serverBDpassed)
	PLAYER_INDEX PlayerId = serverBDpassedLDB.leaderBoard[leaderboardIndex].playerID
	INT participantID = serverBDpassedLDB.leaderBoard[leaderboardIndex].iParticipant

#IF IS_DEBUG_BUILD
	INT iPlayer = NATIVE_TO_INT(PlayerId)
	
	IF participantID <> -1
		PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU leaderboard index = ", leaderboardIndex, " participantID = ", participantID, "PlayerId = ", iPlayer, " PlayerName = ", serverBDpassed.tl63_ParticipantNames[participantID])
	ELSE
		PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU leaderboard index = ", leaderboardIndex, " participantID = ", participantID, "PlayerId = ", iPlayer)
	ENDIF
#ENDIF	//	IS_DEBUG_BUILD

	IF participantID <> -1 // 1348994
		IF NOT IS_BIT_SET(serverBDpassedLDB.leaderBoard[leaderboardIndex].iLbdBitSet, ciLBD_BIT_QUIT) //	Chris removed this check in CL 4794226. I'm not sure if it should be here or not
			IF (PlayerId <> INVALID_PLAYER_INDEX()) 
				IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, playerId, participantID)
					IF IS_NET_PLAYER_OK(playerId, FALSE)
					
						PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU returning TRUE for leaderboard index = ", leaderboardIndex)
						
						RETURN TRUE
					ELSE
						PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " IS_NET_PLAYER_OK returned FALSE")
					ENDIF
				ELSE
					PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " IS_PLAYER_SPECTATOR returned TRUE")
				ENDIF
			ELSE
				PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " playerID = INVALID_PLAYER_INDEX")
			ENDIF
		ELSE
			PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " bQuit is TRUE")
		ENDIF
	ELSE
		PRINTLN("[CS_DPAD] SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU returning FALSE for leaderboard index = ", leaderboardIndex, " participantID = -1")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_DEATHMATCH_TEAM_COUNTS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT &iTeamWinCount, INT &iTeam2Count, INT &iTeam3Count, INT &iTeam4Count, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	INT unsortedTeamCounts[MAX_NUM_DM_TEAMS]
	
	INT loop = 0
	WHILE loop < MAX_NUM_DM_TEAMS
		unsortedTeamCounts[loop] = 0
		loop++
	ENDWHILE
	
	iTeamWinCount = 0
	iTeam2Count = 0
	iTeam3Count = 0
	iTeam4Count = 0
	
	INT i, iPlayerTeam
	REPEAT NUM_NETWORK_PLAYERS i
		IF SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU(i, serverBDpassed, playerBDPassed, serverBDpassedLDB)
			iPlayerTeam = serverBDpassedLDB.leaderBoard[i].iTeam

			IF iPlayerTeam <> -1
				IF (iPlayerTeam >= 0) AND (iPlayerTeam < MAX_NUM_DM_TEAMS)
					unsortedTeamCounts[iPlayerTeam]++
				ELSE
					SCRIPT_ASSERT("GET_DEATHMATCH_TEAM_COUNTS - player team index is out of the range 0 to 3")
				ENDIF					
			ENDIF
		ENDIF
	ENDREPEAT

	IF serverBDpassed.iWinningTeam <> -1
		iTeamWinCount = unsortedTeamCounts[serverBDpassed.iWinningTeam]
	ENDIF
	IF serverBDpassed.iSecondTeam <> -1
		iTeam2Count = unsortedTeamCounts[serverBDpassed.iSecondTeam]
	ENDIF
	IF serverBDpassed.iThirdTeam <> -1
		iTeam3Count = unsortedTeamCounts[serverBDpassed.iThirdTeam]
	ENDIF
	IF serverBDpassed.iFourthTeam <> -1
		iTeam4Count = unsortedTeamCounts[serverBDpassed.iFourthTeam]
	ENDIF
	
	PRINTLN("[CS_DPAD] GET_DEATHMATCH_TEAM_COUNTS: Ordered Team Counts ", serverBDpassed.iWinningTeam, " has ", iTeamWinCount, ", ", serverBDpassed.iSecondTeam, " has ", iTeam2Count)
	PRINTLN("[CS_DPAD] ", serverBDpassed.iThirdTeam, " has ", iTeam3Count, ", ", serverBDpassed.iFourthTeam, " has ", iTeam4Count)
ENDPROC

FUNC BOOL SHOULD_DRAW_SPECTATOR_EYE(PlayerBroadcastData &playerBDPassed[], INT iParticipant)

	IF iParticipant < 0
	OR iParticipant >= NUM_NETWORK_PLAYERS
		RETURN FALSE
	ENDIF

	IF CONTENT_IS_USING_ARENA()
	
		IF playerBDPassed[iParticipant].eClientLogicStage = CLIENT_MISSION_STAGE_SCTV
		OR playerBDPassed[iParticipant].eClientLogicStage = CLIENT_MISSION_STAGE_SPECTATOR
			RETURN TRUE
		ENDIF

	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_APPEAR_CROSSED_OUT(PlayerBroadcastData &playerBDPassed[], PLAYER_INDEX playerId, INT iParticipant)

	IF NOT IS_NET_PLAYER_OK(playerId)
		RETURN TRUE
	ENDIF
	
	IF iParticipant < 0
	OR iParticipant >= NUM_NETWORK_PLAYERS
		RETURN FALSE
	ENDIF
	
	IF playerBDPassed[iParticipant].eClientLogicStage > CLIENT_MISSION_STAGE_FIGHTING
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(playerBDPassed[iParticipant].iClientBitset, CLIENT_BITSET_EXPLODED_FROM_BEING_TAGGED)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC POPULATE_DPAD(LBD_SUB_MODE eSubMode, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	INT i, iPlayerTeam, iRank, iPlayer, iParticipantId//, iIcon
	INT iTeamWinCount, iTeam2Count, iTeam3Count, iTeam4Count
	INT iTeamSlotCounter[MAX_NUM_DM_TEAMS]
	PLAYER_INDEX PlayerId
	STRING strHeadshotTxd
	TEXT_LABEL_15 tlCrewTag
	INT iRow = 0
	INT iTeamRank = -1
	
	g_i_NumDpadLbdPlayers = 0
	
	BOOL bPassBomb
	BOOL bArena
	IF CONTENT_IS_USING_ARENA()
		bArena = TRUE
		IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
			bPassBomb = TRUE
		ENDIF
	ENDIF
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	
		REPEAT MAX_NUM_DM_TEAMS i
			iTeamSlotCounter[i] = 0
		ENDREPEAT
	
		GET_DEATHMATCH_TEAM_COUNTS(serverBDpassed, playerBDPassed, iTeamWinCount, iTeam2Count, iTeam3Count, iTeam4Count, serverBDpassedLDB)
	ENDIF
	
	EMPTY_DPAD_MENU_IF_REQUIRED(dmVarsPassed.scaleformDpadMovie)
	
	REPEAT NUM_NETWORK_PLAYERS i

		IF SHOULD_PLAYER_BE_DISPLAYED_ON_DEATHMATCH_DPAD_DOWN_MENU(i, serverBDpassed, playerBDPassed, serverBDpassedLDB)

			PlayerId = serverBDpassedLDB.leaderBoard[i].playerID
			iPlayer = NATIVE_TO_INT(PlayerId)
			
			iParticipantId = serverBDpassedLDB.leaderBoard[i].iParticipant
				
			// Grab names
			dmVarsPassed.dpadVars.tl63LeaderboardPlayerNames = serverBDpassed.tl63_ParticipantNames[iParticipantId]	//	GET_PLAYER_NAME(PlayerId)	
			
			dmVarsPassed.dpadVars.mpIconActive = ICON_EMPTY
			IF SHOULD_DRAW_SPECTATOR_EYE(playerBDPassed, iParticipantId)
				dmVarsPassed.dpadVars.mpIconActive = SPECTATOR
			ELIF bPassBomb AND serverBDPassed.iPartTagged = iParticipantId  
				dmVarsPassed.dpadVars.mpIconActive = DPAD_BOMB
			ELSE
				dmVarsPassed.dpadVars.mpIconActive = ICON_EMPTY
			ENDIF
			
			IF SHOULD_APPEAR_CROSSED_OUT(playerBDPassed, PlayerId, iParticipantId)
				SET_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
			ELSE
				CLEAR_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_PLAYER_DEAD)
			ENDIF
		
			// Grab crew tags
			tlCrewTag = GET_CREW_TAG_DPAD_LBD(PlayerId)
			
			// Rank
			iRank = GlobalplayerBD_FM[iPlayer].scoreData.iRank
				
			IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
			
				SET_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_IS_TEAM)

				// Grab player team
				iPlayerTeam = serverBDpassedLDB.leaderBoard[i].iTeam

				IF iPlayerTeam <> -1	
				
					iRow = GET_ROW_TO_DRAW_PLAYERS(iPlayerTeam, 
												serverBDpassed.iNumberOfTeams, 
												iTeamSlotCounter[iPlayerTeam], 
												iTeamWinCount, 
												iTeam2Count, 
												iTeam3Count, 0, 0, 0, 0,
												serverBDpassed.iWinningTeam,
												serverBDpassed.iSecondTeam, 
												serverBDpassed.iThirdTeam,
												-1,	-1, -1, -1, 
												serverBDpassed.iLosingTeam, TRUE, FALSE)	
												
					dmVarsPassed.dpadVars.iTeamRow = GET_TEAM_BARS_ROW(	iPlayerTeam, serverBDpassed.iNumberOfTeams,
																		iTeamWinCount, iTeam2Count, iTeam3Count, iTeam4Count, 0,0,0,0,
																		serverBDpassed.iWinningTeam, serverBDpassed.iSecondTeam, serverBDpassed.iThirdTeam, -1, -1, -1, -1, 
																		serverBDpassed.iLosingTeam, TRUE)		

					IF playerId = LocalPlayer
						dmVarsPassed.dpadVars.iPlayersRow = (iTeamSlotCounter[iPlayerTeam] + dmVarsPassed.dpadVars.iTeamRow)
					ENDIF

					IF bArena
					OR (IS_KING_OF_THE_HILL() AND USE_KOTH_TEAM_COLOURS())
						dmVarsPassed.dpadVars.hudColour = GET_HUD_COLOUR_FOR_FMMC_TEAM(iPlayerTeam, playerId, TRUE)
					ELSE
						dmVarsPassed.dpadVars.hudColour = GET_HUD_COLOUR_FOR_OPPONENT_CHOSEN_TEAM(playerBDPassed[iLocalPart].iTeam, iPlayerTeam, TRUE, (NUM_NETWORK_PLAYERS/2))
					ENDIF				
					
					IF iRow = dmVarsPassed.dpadVars.iTeamRow
						iTeamRank = GET_TEAM_RANK(serverBDpassed, iPlayerTeam, serverBDpassed.iNumberOfTeams)
					ELSE
						iTeamRank = -2	//	Don't display the team rank for any rows except the top row for that team
					ENDIF
					
					PRINTLN("[CS_DPAD] Team Deathmatch about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iRow, " player name = ", dmVarsPassed.dpadVars.tl63LeaderboardPlayerNames, " iParticipant = ", iParticipantId, " iPlayer = ", iPlayer, " iTeamRank = ", iTeamRank)
					
					POPULATE_NEW_FREEMODE_DPAD_ROWS(eSubMode, dmVarsPassed.scaleformDpadMovie, dmVarsPassed.dpadVars, iRow, tlCrewTag, strHeadshotTxd, iRank, serverBDpassedLDB.leaderBoard[i].iScore, dmVarsPassed.dpadVars.hudColour, DISPLAY_POSITION_NUMBER, iTeamRank)
					g_i_NumDpadLbdPlayers ++
														
					PRINTLN("[CS_DPAD] TEAM iRow = ", iTeamSlotCounter[iPlayerTeam], " iRank = ", iRank)
					
					dmVarsPassed.dpadVars.sDpadMics[iPlayer].iRow = iRow 
														
					iTeamSlotCounter[iPlayerTeam]++
				ENDIF
			ELSE
				IF playerId = LocalPlayer
					dmVarsPassed.dpadVars.iPlayersRow = iRow
					dmVarsPassed.dpadVars.hudColour = FRIENDLY_DM_COLOUR()
				ELSE
					dmVarsPassed.dpadVars.hudColour = HUD_COLOUR_NET_PLAYER1
				ENDIF

				PRINTLN("[CS_DPAD] Normal Deathmatch about to call POPULATE_NEW_FREEMODE_DPAD_ROWS with row = ", iRow, " player name = ", dmVarsPassed.dpadVars.tl63LeaderboardPlayerNames, " iParticipant = ", iParticipantId, " iPlayer = ", iPlayer)

				POPULATE_NEW_FREEMODE_DPAD_ROWS(eSubMode, dmVarsPassed.scaleformDpadMovie, dmVarsPassed.dpadVars, iRow, tlCrewTag, strHeadshotTxd, iRank, serverBDpassedLDB.leaderBoard[i].iScore, dmVarsPassed.dpadVars.hudColour, DISPLAY_POSITION_NUMBER, iRow+1)
				g_i_NumDpadLbdPlayers ++
				
				PRINTLN("[CS_DPAD] iRow = ", iRow, " iRank = ", iRank)
				
				// Store out mics row
				dmVarsPassed.dpadVars.sDpadMics[iPlayer].iRow = iRow
				
				iRow++
			ENDIF
		ENDIF
	ENDREPEAT
	
//	// IMPORTANT!
//	RESET_DPAD_REFRESH()
	CLEAR_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
ENDPROC

PROC DO_THE_MICS_DM(SHARED_DM_VARIABLES &dmVarsPassed)
	INT iIcon, i
	PLAYER_INDEX PlayerId
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerId = INT_TO_PLAYERINDEX(i)
		IF PlayerId <> INVALID_PLAYER_INDEX()
			IF IS_NET_PLAYER_OK(playerId, FALSE)
				IF dmVarsPassed.dpadVars.sDpadMics[i].iRow <> -1
					iIcon = GET_DPAD_PLAYER_VOICE_ICON(dmVarsPassed.dpadVars.sDpadMics[i].iVoiceChatState)
					SET_PLAYER_MIC(dmVarsPassed.scaleformDpadMovie, dmVarsPassed.dpadVars.sDpadMics[i].iRow, iIcon, GlobalplayerBD_FM[i].scoreData.iRank)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DPAD_LEADERBOARD(LBD_SUB_MODE eSubMode, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	
	IF HAS_SERVER_DONE_AT_LEAST_ONE_LBD_UPDATE(serverBDpassed)
		IF GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_RESULTS
		OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
			IF SHOULD_DPAD_LBD_DISPLAY(dmVarsPassed.scaleformDpadMovie, eSubMode, dmVarsPassed.dpadVars, serverBDpassed.iNumActivePlayers)
		
				IF dmVarsPassed.dpadVars.bShowJp
					dmVarsPassed.dpadVars.bShowJp = FALSE
				ENDIF
			
				SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_SEEN_DPAD_LEADERBOARD)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
		
				SWITCH dmVarsPassed.dpadVars.iDrawProgress
				
					CASE DPAD_INIT
						POPULATE_DPAD(eSubMode, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)				// SET_DATA_SLOT
						
						IF NOT IS_BIT_SET(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
							HIGHLIGHT_LOCAL_PLAYER(dmVarsPassed.dpadVars, dmVarsPassed.scaleformDpadMovie)
							PRINTLN("DPAD_LEADERBOARD, DPAD_INIT, DISPLAY_VIEW ")
							DISPLAY_VIEW(dmVarsPassed.scaleformDpadMovie)								// DISPLAY_VIEW
							SET_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
						ENDIF
						
						// Draw the mics
						DO_THE_MICS_DM(dmVarsPassed)															// SET_ICON
						
						IF IS_BIT_SET(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
							CLEAR_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
						ENDIF
			
						PRINTLN("DPAD_LEADERBOARD, dmVarsPassed.dpadVars.iDrawProgress = DPAD_POPULATE ")
						dmVarsPassed.dpadVars.iDrawProgress = DPAD_POPULATE
					BREAK
				
					CASE DPAD_POPULATE
						POPULATE_DPAD(eSubMode, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)				// SET_DATA_SLOT
			
						dmVarsPassed.dpadVars.iDrawProgress = DPAD_DRAW
					BREAK
					
					CASE DPAD_DRAW
					   	IF SHOULD_DPAD_REFRESH()
							POPULATE_DPAD(eSubMode, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)			// SET_DATA_SLOT
						ENDIF

						// Commented out to fix memory leak 2557358
//						IF NOT IS_BIT_SET(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED) 
//							PRINTLN("DPAD_LEADERBOARD, DPAD_DRAW, DISPLAY_VIEW ")
//							DISPLAY_VIEW(dmVarsPassed.scaleformDpadMovie)									// DISPLAY_VIEW
//							SET_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_IS_UPDATED)
//						ENDIF
						
						HIGHLIGHT_LOCAL_PLAYER(dmVarsPassed.dpadVars, dmVarsPassed.scaleformDpadMovie)
						
						IF MAINTAIN_DPAD_VOICE_CHAT(dmVarsPassed.dpadVars)
						
							PRINTLN("[CS_DPAD]  MAINTAIN_DPAD_VOICE_CHAT, dpadVars.iDrawProgress = DPAD_INIT")
							
							DO_THE_MICS_DM(dmVarsPassed)	
						ENDIF
					BREAK
				ENDSWITCH
					
				IF HAS_SCALEFORM_MOVIE_LOADED(dmVarsPassed.scaleformDpadMovie)
					DRAW_SCALEFORM_MOVIE(dmVarsPassed.scaleformDpadMovie, SDPAD_X, SDPAD_Y, SDPAD_W, SDPAD_H, 255, 255, 255, 255)
				ENDIF
			ELSE
				RESET_DPAD_REFRESH()
				
				IF IS_BIT_SET(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
					CLEAR_BIT(dmVarsPassed.dpadVars.iDpadBitSet, ciDPAD_ROW_HIGHLIGHT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RESET_SCRIPT_GFX_ALIGN()
ENDPROC

// -----------------------------------	TEXT

FUNC BOOL DO_BOSSVBOSS_OPENING_SPLASH(ServerBroadcastData &serverBDpassed)
	STRING sOrganization
	HUD_COLOURS hclPlayer
	
	TEXT_LABEL_15 tl15ShardTitle = "BIGM_BOSSVBOSS"
	TEXT_LABEL_15 tl15ShardDesc = "BIGM_BOSSVBOSSD"
	
	
	
	INT iMyTeam = GET_PLAYER_TEAM(LocalPlayer)
	INT iRivalTeam = (1-iMyTeam)
	PLAYER_INDEX playerRival
	
	
	
	IF iRivalTeam >= 0 AND iRivalTeam < 2
		IF serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1
			playerRival = INT_TO_PLAYERINDEX(serverBDpassed.iTeamBossPlayer[iRivalTeam])
			IF NETWORK_IS_PLAYER_ACTIVE(playerRival)
				sOrganization = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(playerRival)
				hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(playerRival)
				
				IF IS_PLAYER_ON_BIKER_DM()
					tl15ShardTitle = "BIGM_BKVBK"
					tl15ShardDesc = "BIGM_BKVBKD"
				ENDIF
				
				CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [DO_BOSSVBOSS_OPENING_SPLASH] Done tl15ShardTitle = ", tl15ShardTitle, " tl15ShardDesc = ", tl15ShardDesc)
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_START_OF_JOB, tl15ShardTitle, tl15ShardDesc, sOrganization, hclPlayer) 
				RETURN TRUE
			ELSE
				CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [DO_BOSSVBOSS_OPENING_SPLASH] Failing - NETWORK_IS_PLAYER_ACTIVE")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [DO_BOSSVBOSS_OPENING_SPLASH] Failing - serverBDpassed.iTeamBossPlayer[iRivalTeam] > -1")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [DO_BOSSVBOSS_OPENING_SPLASH] Failing - IF iRivalTeam >= 0 AND iRivalTeam < 2")
	ENDIF			
	
	RETURN FALSE
	//SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_START_OF_JOB, "BIGM_BOSSVBOSS", "BIGM_BOSSVBOSSD")
ENDFUNC

PROC PRINT_WINNING_KILL_TICKER(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_WINNING_KILL_TICKER)
			IF serverBDpassed.playerMatchWinner <> INVALID_PLAYER_INDEX()
				IF IS_NET_PLAYER_OK(serverBDpassed.playerMatchWinner)
					PRINTLN("PRINT_WINNING_KILL_TICKER")
					// "~a~~HUD_COLOUR_WHITE~ got the winning kill."
					PRINT_TICKER_WITH_PLAYER_NAME( "DM_TICK11", serverBDpassed.playerMatchWinner)
					SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_WINNING_KILL_TICKER)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRINT_MVP_TICKER(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed) 
		IF NOT DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDPassed)
			IF NOT DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
				IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_MVP_TICKER)
					IF serverBDpassed.iMVP <> -1
						PLAYER_INDEX mvPlayer
						mvPlayer = INT_TO_PLAYERINDEX(serverBDpassed.iMVP)
						IF IS_NET_PLAYER_OK(mvPlayer, FALSE)
							PRINTLN(" [MVP] PRINT_MVP_TICKER")
							// "Most Valuable Player ~a~. "
							PRINT_TICKER_WITH_PLAYER_NAME("DM_TICK13", INT_TO_PLAYERINDEX(serverBDpassed.iMVP)) 
							SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_MVP_TICKER)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL WAS_PED_EXECUTED(PED_INDEX pedDead)
	IF WAS_PED_KILLED_BY_STEALTH(pedDead)
	
		RETURN TRUE
	ENDIF
	IF WAS_PED_KILLED_BY_TAKEDOWN(pedDead)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC DM_PROGRESS GET_DM_WIN_STATE_FOR_TICKERS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], INT iLocalTeam, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	DM_PROGRESS eReturn = DM_DRAWING
	INT iLocalPlayerScore	
	INT iLeadersScore		
	INT iRunnerUpScore	
	PLAYER_INDEX playerWinner 			
	INT iWinner
	PLAYER_INDEX player2nd 				
	INT i2nd 	

	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		IF iLocalTeam > -1
			IF serverBDpassed.iTeamWithHighestScore = iLocalTeam
			
				eReturn = DM_WINNING
			ENDIF
		ENDIF
	ELSE
		// Player in 1st
		playerWinner 			= GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0)
		iWinner 				= NATIVE_TO_INT(playerWinner)

		// Player in 2nd
		player2nd 				= GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 1)
		i2nd 					= NATIVE_TO_INT(player2nd)

		// Scores
		IF iLocalPart <> -1
			iLocalPlayerScore	= playerBDPassed[iLocalPart].iScore
		ENDIF
		IF iWinner <> -1
			iLeadersScore		= playerBDPassed[iWinner].iScore
		ENDIF
		IF i2nd <> -1
			iRunnerUpScore		= playerBDPassed[i2nd].iScore
		ENDIF

		IF playerWinner = LocalPlayer
		AND (iLocalPlayerScore > iRunnerUpScore)
		
			eReturn = DM_WINNING
		ELIF iLocalPlayerScore = iLeadersScore
		
			eReturn = DM_DRAWING
		ELIF (iLocalPlayerScore < iLeadersScore)
		
//			PRINTLN("[WTF] iScore = ", iLocalPlayerScore, " iLeadersScore = ", iLeadersScore)
		
			eReturn = DM_LOSING
		ENDIF
	ENDIF
	
	RETURN eReturn
ENDFUNC

FUNC STRING GET_LEAD_TICKER(ServerBroadcastData &serverBDpassed, BOOL bWinning)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		IF bWinning
			// "Your team gained the lead."	
			RETURN "DM_TICK9"
		ELSE
			// "Your team lost the lead."
			RETURN "DM_TICK10"
		ENDIF
	ELSE
		IF bWinning
			// "You gained the lead"	F
			RETURN "DM_TICK4"
		ELSE
			// "You lost the lead"
			RETURN "DM_TICK5"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC

// Gained and lost lead tickers
PROC PROCESS_LEADER_TICKERS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)

	PLAYER_INDEX playerId = LocalPlayer
	
	IF CONTENT_IS_USING_ARENA()
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		EXIT
	ENDIF

	IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_FINISHING_UP)
	OR IS_PLAYER_SPECTATOR(playerBDPassed, playerId, iLocalPart)
	OR GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_FIGHTING
		
		EXIT
	ENDIF
	
	INT iTeam = GET_PLAYER_TEAM(playerId)
	DM_PROGRESS eDMProgress = GET_DM_WIN_STATE_FOR_TICKERS(serverBDpassed, playerBDPassed, iTeam, serverBDpassedLDB)
	STRING sTicker = GET_LEAD_TICKER(serverBDpassed, (eDMProgress = DM_WINNING))
	
	IF eDMProgress = DM_WINNING
		IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_TOOK_LEAD_TICKER)
			PRINT_TICKER(sTicker)
			SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_TOOK_LEAD_TICKER)
			CLEAR_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_LOST_LEAD_TICKER)
		ENDIF	
//		PRINTLN("eDMProgress = DM_F)
//	ELIF eDMProgress = DM_DRAWING
//
//		PRINTLN("eDMProgress = DM_DRAWING")
	ELSE
//		PRINTLN("eDMProgress = DM_LOSING")
		IF NOT IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_LOST_LEAD_TICKER)
		AND IS_BIT_SET(dmVarsPassed.iTxtBitset, TXT_BITSET_TOOK_LEAD_TICKER)
			PRINT_TICKER(sTicker)
			SET_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_LOST_LEAD_TICKER)
			CLEAR_BIT(dmVarsPassed.iTxtBitset, TXT_BITSET_TOOK_LEAD_TICKER)
		ENDIF	
	ENDIF
ENDPROC

PROC SAVE_THUMB_VOTE(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)	
		IF IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMBS_DONE)
			IF IS_BIT_SET(dmVarsPassed.sSaveOutVars.iBitSet, ciRATINGS_RUN_UP_LOAD)
				IF SAVE_OUT_UGC_PLAYER_DATA(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl31CurrentMissionFileName, dmVarsPassed.sSaveOutVars)
					CLEAR_BIT(dmVarsPassed.sSaveOutVars.iBitSet, ciRATINGS_RUN_UP_LOAD)
					SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMB_VOTE_SAVED)
					PRINTLN("[UGC] HAS_DM_VOTE_BEEN_SAVED, SAVED vote")
				ELSE
					PRINTLN("HAS_DM_VOTE_BEEN_SAVED....saving.....vote")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC RESET_DM_GLOBALS()
	PRINTLN("RESET_DM_GLOBALS ")
	g_bShouldBlockPauseJobs = FALSE
	g_b_IsDMSpectator = FALSE
	g_b_PopulateReady = FALSE
	g_SwapBettingColumnForVotes = FALSE
	g_i_PowerPlayer = -1
	g_i_BrucieInstanceDm = -1
	g_i_IdlePlayerBit = 0
	g_b_OnResults = FALSE
	g_b_SkipKillStrip = FALSE

	SET_DM_FINISHED_GLOBAL(FALSE)
	SET_ON_DEATHMATCH_GLOBAL(FALSE)	
//	SET_RESPAWN_GLOBAL(FALSE)
	SET_DEAD_IN_DM_GLOBAL(FALSE)
	g_bFM_ON_TEAM_DEATHMATCH = FALSE
	SET_ON_VEH_DEATHMATCH_GLOBAL(FALSE)
	SET_ON_IMPROMPTU_DEATHMATCH_GLOBAL(FALSE)
	CLEAR_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_IMPROMPTU_DM)
	CLEAR_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)
	CLEAR_BIT(g_sDmGlobalVars.iBitSet, ciDM_GLOBAL_BITSET_FIRST_STRIKE)
	SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)

//		g_bFM_TAGS_ON = FALSE
	PRINTLN("[overhead] g_iOverheadNamesState = -1 - RESET ")
	g_iOverheadNamesState = -1
		
	//Set Kill / Death globals
	g_iMyAmountOfKills = 0
	g_iMyAmountOfDeaths = 0
	g_iMyAmountOfSuicides = 0
	g_iMyDMScore = 0
	g_f_MyDamageTaken = 0
	g_f_MyDamageDealt = 0
	RESET_JOB_CASH()
	//gdisablerankupmessage = FALSE
	SET_DISABLE_RANK_UP_MESSAGE(FALSE)
	g_bMissionEnding = FALSE
	PRINTLN("g_bMissionEnding = FALSE Deathmatch")
ENDPROC

PROC SET_DM_GLOBALS(ServerBroadcastData &serverBDpassed)
	SET_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_JOB)
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_ANY_SPAWN_VEHICLE_MODIFIER_SET_FOR_DM()
		PRINTLN("SET_DM_GLOBALS, SET_ON_VEH_DEATHMATCH_GLOBAL ")
		SET_ON_VEH_DEATHMATCH_GLOBAL(TRUE)
	ENDIF
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		PRINTLN("SET_DM_GLOBALS, g_bFM_ON_TEAM_DEATHMATCH ")
		g_bFM_ON_TEAM_DEATHMATCH = TRUE
	ELSE
		g_bFM_ON_TEAM_DEATHMATCH = FALSE
	ENDIF
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("SET_DM_GLOBALS, SET_ON_IMPROMPTU_DEATHMATCH_GLOBAL ")
		SET_ON_IMPROMPTU_DEATHMATCH_GLOBAL(TRUE)
		SET_BIT(GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iXPBitset, ciGLOBAL_XP_BIT_ON_IMPROMPTU_DM)
	ENDIF
	
	SET_DM_FINISHED_GLOBAL(FALSE)
	SET_ON_DEATHMATCH_GLOBAL(TRUE)	
	//Set Kill / Death globals
	g_iMyAmountOfKills = 0
	g_iMyAmountOfDeaths = 0
	g_iMyAmountOfSuicides = 0
	g_iMyDMScore = 0
	RESET_JOB_CASH()
	g_b_PopulateReady = FALSE
	
	g_f_MyDamageTaken = 0
	g_f_MyDamageDealt = 0
	//gdisablerankupmessage = TRUE
	SET_DISABLE_RANK_UP_MESSAGE(TRUE)
	
	IF NATIVE_TO_INT(LocalPlayer) > -1
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Init DM CLEAR_BIT(GlobalplayerBD[", NATIVE_TO_INT(LocalPlayer), "].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)")
			#ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("SET_DM_GLOBALS ")
ENDPROC

PROC DO_STARTING_STATS(ServerBroadcastData &serverBDpassed)
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	AND SHOULD_PROGRESS_DM_AWARDS()
		PRINTLN("DO_STARTING_STATS,  MP_STAT_CL_PLAY_TEAM_DM ")
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_PLAY_TEAM_DM, TRUE)
	ENDIF
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		PRINTLN("DO_STARTING_STATS, MP_STAT_CL_PLAY_VEHICLE_DM ")
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_PLAY_VEHICLE_DM, TRUE)
	ENDIF
ENDPROC

PROC HANDLE_END_OF_DM_JOB_POINTS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	INT iFinishPos
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		iFinishPos = GET_TEAM_RANK(serverBDpassed, playerBDPassed[iLocalPart].iTeam, serverBDpassed.iNumberOfTeams)
		PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_JOB_POINTS, GET_TEAM_RANK, iFinishPos = ", iFinishPos)
	ELSE
		IF CONTENT_IS_USING_ARENA()
			iFinishPos = GET_LOCAL_PLAYER_LEADERBOARD_POSITION(serverBDpassedLDB)
			PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_JOB_POINTS, GET_LOCAL_PLAYER_LEADERBOARD_POSITION, iFinishPos = ", iFinishPos)
		ELSE
			iFinishPos = GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT())
			PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_JOB_POINTS, GET_PLAYER_FINISH_POS, iFinishPos = ", iFinishPos)
		ENDIF
	ENDIF
	
	INT iJobPoints = GET_POINTS_FOR_POSITION(iFinishPos, NETWORK_PLAYER_ID_TO_INT())
		
	IF IS_THIS_A_ROUNDS_MISSION()
		playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerJobPoints = iJobPoints
		g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints += playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerJobPoints		
		PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_JOB_POINTS = ", g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints)
	ELSE
		playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerJobPoints = iJobPoints
		PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_JOB_POINTS = ", playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerJobPoints)
	ENDIF
ENDPROC

PROC HANDLE_END_OF_DM_XP(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, BOOL bShouldQuarter, ServerBroadcastData_Leaderboard &serverBDpassedLDB)//, SHARED_DM_VARIABLES &dmVarsPassed)
	IF dmVarsPassed.DID_PLAYER_CREATE_ACTIVITY = TRUE
		CHECK_AND_REWARD_USER_RACES_AND_DEATHMATCHES(dmVarsPassed.DID_PLAYER_CREATE_ACTIVITY)
	ENDIF
	
	XP_TYPE eXpType 
	FLOAT fXPAward
	FLOAT fXPAdditionalXP // for celebration screen
	INT iAdditionalXP, iFinishPos, iWinnerBonus
	
	BOOL bTDM = IS_THIS_TEAM_DEATHMATCH(serverBDpassed) AND NOT IS_TDM_USING_FFA_SCORING()
	
	IF bTDM
		eXpType = eXPTYPE_CO_OP_JOBFM 
		IF DID_I_WIN(serverBDpassed, serverBDpassedLDB)
			iFinishPos = TEAM_WON_REWARD_FINISH_POS
			iWinnerBonus = WINNER_BONUS
			PRINTLN("[GET_DM_END_XP_AWARD] iWinnerBonus = ", WINNER_BONUS)
		ELSE
			iFinishPos = TEAM_LOST_REWARD_FINISH_POS
		ENDIF
	ELSE
		eXpType = eXPTYPE_STANDARD
		iFinishPos = GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT())
	ENDIF
	
	INT iXpAward = GET_DM_END_XP_AWARD(serverBDpassed.iNumDMPlayers, GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam), serverBDpassed.iAverageRank, iFinishPos, dmVarsPassed.stRewardPercentageTimer, bTDM)
	PRINTLN("[GET_DM_END_XP_AWARD] iXpAward = ", iXpAward)
	// Give winners etc. as failsafe
	iXpAward = (iXpAward + iWinnerBonus)
	
	PRINTLN("[GET_DM_END_XP_AWARD] iXpAward + iWinnerBonus (",iWinnerBonus,") iXpAward = ", iXpAward)
	
	IF bShouldQuarter
		FLOAT fXp = TO_FLOAT(iXpAward)
		fXp = (fXp/4)
		iXpAward = ROUND(fXp)
		PRINTLN("HANDLE_END_OF_DM_XP, bShouldQuarter = ", iXpAward)
		PRINTLN("[GET_DM_END_XP_AWARD] QUARTERED iXpAward = ", iXpAward)
	ENDIF
	
	INT iNumPlayers
	IF bTDM
		iNumPlayers = serverBDpassed.iNumberOfTeams
	ELSE
		iNumPlayers = serverBDpassed.iNumDMPlayers
	ENDIF
	IF REWARD_XP_FOR_PLAYING_OWN_CONTENT_AGAINST_OTHERS(iNumPlayers)
		fXPAdditionalXP = (fXPAdditionalXP + (500 * ROUND(g_sMPTunables.fxp_tunable_Play_your_creations_against_other_players)))
		PRINTLN("[CS_XP] HANDLE_END_OF_DM_XP, REWARD_XP_FOR_PLAYING_OWN_CONTENT_AGAINST_OTHERS, fXPAdditionalXP = ", fXPAdditionalXP)
	ENDIF
	
	// Apply multiplier
	fXPAward = TO_FLOAT(iXpAward)
	PRINTLN("[GET_DM_END_XP_AWARD] TO_FLOAT fXPAward = ", fXPAward)
//	fXPAward = (fXPAward * g_sMPTunables.xpMultiplier)
	IF bTDM
		IF NOT DID_I_WIN(serverBDpassed, serverBDpassedLDB)
//			fXPAward = (fXPAward/2)
			IF fXPAward < 100
				fXPAward = 100
				PRINTLN("[CS_XP] HANDLE_END_OF_DM_XP, fXPAward = ", fXPAward, " less than 100, setting to 100.")
			ENDIF
//			PRINTLN("[CS_XP] HANDLE_END_OF_DM_XP, LOSING_TEAM_HALF_RP, fXPAward = ", fXPAward)
		ENDIF
	ENDIF
	iXpAward = ROUND(fXPAward)
	PRINTLN("[GET_DM_END_XP_AWARD] ROUNDING iXpAward = ", iXpAward)
	ROUND_MAX_XP_AMOUNT_PER_JOB(iXpAward)
	
	PRINTLN("[GET_DM_END_XP_AWARD] Rounded to max xp iXpAward = ", iXpAward)
	
	// Additional XP
	iAdditionalXP = ROUND(fXPAdditionalXP)
	dmVarsPassed.iXPAdditional = iAdditionalXP
	PRINTLN(" [CS_XP] [CS_MOD] HANDLE_END_OF_DM_XP iXPAdditional = ", dmVarsPassed.iXPAdditional)
		
	// Client bd lbd XP
	IF IS_THIS_A_ROUNDS_MISSION()
		dmVarsPassed.iXPToShowOnLeaderboard = iXpAward
		playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerXP = (dmVarsPassed.iXPToShowOnLeaderboard+dmVarsPassed.iXPAdditional)
		g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained += playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerXP
		playerBDPassed[iLocalPart].iJobXP = g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRpGained				
		PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_XP = ", playerBDPassed[iLocalPart].iJobXP)
	ELSE
		dmVarsPassed.iXPToShowOnLeaderboard = GIVE_LOCAL_PLAYER_XP(eXpType, "XPT_TOP_THREE", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DM, iXpAward, 1, -1, TRUE) 
		playerBDPassed[iLocalPart].iJobXP = dmVarsPassed.iXPToShowOnLeaderboard
		PRINTLN(" [CS_MOD] HANDLE_END_OF_DM_XP = ", playerBDPassed[iLocalPart].iJobXP)
	ENDIF
	
ENDPROC

PROC DEAL_WITH_PLAYLIST_LEADERBOARD(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	INT i, iRank
	IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, serverBDpassedLDB.leaderBoard[i].playerID, serverBDpassedLDB.leaderBoard[i].iParticipant)
			SET_PLAYLIST_LEADERBOARD_ACTIVE(TRUE)
			FOR i = 0 TO (NUM_NETWORK_PLAYERS-1)
				IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
				AND NOT IS_TDM_USING_FFA_SCORING()
					iRank = serverBDpassed.iTeamPosition[serverBDpassedLDB.leaderBoard[i].iTeam]
				ELSE
					iRank = serverBDpassedLDB.leaderBoard[i].iRank
				ENDIF
				WRITE_MISSION_LB_DATA_TO_GLOBALS(	serverBDpassedLDB.leaderBoard[i].playerID,
													serverBDpassedLDB.leaderBoard[i].iTeam,
													iRank,
													-1,
													-1,
													serverBDpassedLDB.leaderBoard[i].iKills,
													serverBDpassedLDB.leaderBoard[i].iDeaths,
													serverBDpassedLDB.leaderBoard[i].iHeadshots)
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_ANY_DM_BIG_MESSAGES_FOR_LBD(PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
			CLEAR_ALL_BIG_MESSAGES()
		ELSE
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_CUSTOM)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WASTED)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_POWER_PLAY)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_DOUBLE_DAMAGE)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_ONE_ON_ONE_DM)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED)
			CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_END_OF_JOB)
		ENDIF
		PRINTLN("CLEAR_ANY_DM_BIG_MESSAGES_FOR_LBD")
	ENDIF
ENDPROC

PROC CLEANUP_DYNAMIC_PROPS(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed)
	INT i
	FOR i = 0 TO (FMMC_MAX_NUM_DYNOPROPS -1)
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niDynoProps[i])
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverFMMCPassed.niDynoProps[i])
				DELETE_NET_ID(serverFMMCPassed.niDynoProps[i])
				PRINTLN(" [CS_DYNO] CLEANUP_DYNAMIC_PROPS i = ", i)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

PROC CLEAR_ALL_DAMAGE_EVENT_BITS(ServerBroadcastData &serverBDpassed)
	IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		EXIT
	ENDIF

	INT iParticipant, iPlayer
	PLAYER_INDEX PlayerId
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant		

		PlayerID = INVALID_PLAYER_INDEX()
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		
			PlayerId 	= NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			iPlayer		= NATIVE_TO_INT(PlayerID)
			
			// (1638856) Once the player is alive clear the bit that blocks dupe events
			IF iPlayer <> -1
				PRINTLN(" [CS_VEH_DM] INITIAL CLEARING_BIT FOR iPlayer = ", iPlayer)
				CLEAR_BIT(GlobalplayerBD_FM[iPlayer].iVehDeathmatchKillBitSet, iPlayer)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC HANDLE_VEH_JACKING(ServerBroadcastData &serverBDpassed, BOOL bBlock)
	VEHICLE_INDEX veh
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			veh = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			BLOCK_VEHICLE_JACKING(veh, bBlock)
		ENDIF
	ENDIF
ENDPROC

PROC DO_BOSS_V_BOSS_CLEANUP(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_BIT_SET(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_CLEANUP)
		EXIT
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM)
		GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_ACCEPTED_BOSSVBOSS_DM)
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM)
		GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_INTITIATED_BOSSVBOSS_DM)
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
		GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
	ENDIF
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_BOSS_QUIT_BOSSVBOSS_DM)
		GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_BOSS_QUIT_BOSSVBOSS_DM)
	ENDIF 
	
	IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_SUICIDE_BOSS_BOSSVBOSS_DM)
		GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_SUICIDE_BOSS_BOSSVBOSS_DM)
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
		CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] DO_BOSS_V_BOSS_CLEANUP BVB_BS1_DONE_TELEMETRY NOT SET - ASSUME LEFT")
		GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
		SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
	ENDIF
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed,  PCF_PreventAutoShuffleToDriversSeat, FALSE)
	CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] DO_BOSS_V_BOSS_CLEANUP PCF_PreventAutoShuffleToDriversSeat, FALSE ")
	
	SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()
	
	RESET_CHAT_PROXIMITY()
	
	GB_COMMON_BOSS_MISSION_CLEANUP()
	
	IF NOT IS_PLAYER_ON_BIKER_DM()
		PROCESS_CURRENT_BOSS_WORK_PLAYSTATS()
	ENDIF
	IF IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PPAF_GB_BOSS_V_BOSS_DM)
		CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PPAF_GB_BOSS_V_BOSS_DM)
	ENDIF
	
	IF NOT IS_BIT_SET(g_FM_EVENT_VARS.iEventBitSet, CIFM_EVENT_EVENT_UNHIDE_ALL_MISSIONSATCOORDS_BLIPS)
	AND NOT FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(LocalPlayer)
		CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] DO_BOSS_V_BOSS_CLEANUP - Block_All_MissionsAtCoords_Missions(FALSE)")
		Block_All_MissionsAtCoords_Missions(FALSE)
	ELSE	
		CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] DO_BOSS_V_BOSS_CLEANUP - Not unblocking")
	ENDIF
	
	Clear_Any_Objective_Text_From_This_Script()
	
	SET_BIT(dmVarsPassed.iBVBbitset ,BVB_BS1_DONE_CLEANUP)
	CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] DO_BOSS_V_BOSS_CLEANUP - BVB_BS1_DONE_CLEANUP")
ENDPROC

PROC STORE_GARAGE_SLOT(PlayerBroadcastData &playerBDPassed[])
	IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(LocalPlayer)
		MPSV_GET_DISPLAY_SLOT_FROM_SAVE_SLOT(g_iMyRaceModelChoice, playerBDPassed[iLocalPart].iDisplaySlot) //g_iMyRaceModelChoice
		PRINTLN("[STORE_GARAGE_SLOT] 5456221 iDisplaySlot: ", playerBDPassed[iLocalPart].iDisplaySlot)
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("[STORE_GARAGE_SLOT] 5456221 Custom vehicle not being used, not setting iDisplaySlot.")
	#ENDIF
	ENDIF
ENDPROC

FUNC BOOL GRAB_ARENA_WINNER_VEHICLE_NAME(ServerBroadcastData &serverBDPassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF IS_STRING_NULL_OR_EMPTY(dmVarsPassed.tl23WinnerVehicle)
		IF CONTENT_IS_USING_ARENA() 
			IF IS_PLAYER_SELECTING_CUSTOM_VEHICLE(serverBDpassedLDB.leaderBoard[0].playerID)
				IF HAS_DEATHMATCH_FINISHED(serverBDPassed)
				
					INT iPartWinner = serverBDpassedLDB.leaderBoard[0].iParticipant
					PRINTLN("5456221 GRAB_ARENA_WINNER_VEHICLE_NAME, iPartWinner = ", iPartWinner)
					
					INT iSlot 
					IF iPartWinner != -1
						iSlot = playerBDPassed[iPartWinner].iDisplaySlot
						PRINTLN("5456221 GRAB_ARENA_WINNER_VEHICLE_NAME, iSlot = ", iSlot)
					ENDIF
					
					IF iPartWinner = iLocalPart
					
						dmVarsPassed.tl23WinnerVehicle = GET_ARENA_VEHICLE_NAME(DEFAULT, playerBDPassed[iLocalPart].iDisplaySlot)
						PRINTLN("5456221 GRAB_ARENA_WINNER_VEHICLE_NAME, tl23WinnerVehicle = ", dmVarsPassed.tl23WinnerVehicle)
					ELSE
						INT iPlayer = NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[0].playerID)
						IF iPlayer != -1
						AND GlobalplayerBD_FM_3[iPlayer].sMagnateGangBossData.eLanguage = GlobalplayerBD_FM_3[NATIVE_TO_INT(LocalPlayer)].sMagnateGangBossData.eLanguage
							BOOL bRestricted
							IF NOT REQUEST_REMOTE_PLAYER_ARENA_VEHICLE_NAME(serverBDpassedLDB.leaderBoard[0].playerID, iSlot, dmVarsPassed.tl23WinnerVehicle,bRestricted)
								RETURN FALSE
							ELSE
								IF bRestricted
									dmVarsPassed.tl23WinnerVehicle = ""
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_APPLICABILITY_TRIGGERS(ServerBroadcastData &serverBDPassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF IS_THIS_LEGACY_DM_CONTENT()
		EXIT
	ENDIF
	
	IF IS_THIS_FFA_DEATHMATCH(serverBDpassed)
	
		IF dmVarsPassed.sPlayerModifierData.iIndividualScore != dmVarsPassed.iMyPreviousScore
			PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_TRIGGERS - FFA score updated, requested refresh.")
			REQUEST_APPLICABILITY_UPDATE(dmVarsPassed.sPlayerModifierData)
			dmVarsPassed.iMyPreviousScore = dmVarsPassed.sPlayerModifierData.iIndividualScore
		ENDIF
	ELSE
		IF dmVarsPassed.sPlayerModifierData.iTeamScore != dmVarsPassed.iMyPreviousScore
			PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_TRIGGERS - Team score updated, requested refresh.")
			REQUEST_APPLICABILITY_UPDATE(dmVarsPassed.sPlayerModifierData)
			dmVarsPassed.iMyPreviousScore = dmVarsPassed.sPlayerModifierData.iTeamScore
		ENDIF
	ENDIF
	
	IF dmVarsPassed.sPlayerModifierData.eMyPosition != dmVarsPassed.eMyPreviousPosition
		PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_TRIGGERS - Position updated, requested refresh.")
		REQUEST_APPLICABILITY_UPDATE(dmVarsPassed.sPlayerModifierData)
		dmVarsPassed.eMyPreviousPosition = dmVarsPassed.sPlayerModifierData.eMyPosition
	ENDIF
	
	IF dmVarsPassed.sPlayerModifierData.iNextTimeToRefresh != -1
	AND HAS_MODIFIER_SET_TIMER_REQUIREMENT_BEEN_MET(dmVarsPassed.sPlayerModifierData.iTimerConditionTimeStamp, dmVarsPassed.sPlayerModifierData.iNextTimeToRefresh)
		PRINTLN("[Player Modifiers] - PROCESS_APPLICABILITY_TRIGGERS - Timer updated, requested refresh.")
		REQUEST_APPLICABILITY_UPDATE(dmVarsPassed.sPlayerModifierData)
		dmVarsPassed.sPlayerModifierData.iNextTimeToRefresh = -1		
	ENDIF
	
	IF dmVarsPassed.iCachedModset != playerBDPassed[iLocalPart].iCurrentModset
		SET_UP_CUSTOM_SPAWN_POINTS(serverBDpassed, playerBDPassed, dmVarsPassed, TRUE)
		CACHE_DM_PLAYER_CURRENT_MODSET(dmVarsPassed, playerBDPassed[iLocalPart].iCurrentModset)
	ENDIF
	
ENDPROC

PROC CLEANUP_IPLS_AND_INTERIORS()

	ARENA_CLEANUP(DEFAULT, DEFAULT, TRUE)
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iIPLOptions, IPL_ISLAND)
		HEIST_ISLAND_LOADING__UNLOAD_ISLAND_IPLS()
		UNLOAD_MISSION_ISLAND_CUSTOMISATION()
	ENDIF

ENDPROC

FUNC BOOL SHOULD_RETURN_WEAPON_THROUGH_MP_REWARD()
	IF NOT (SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD() OR ROUNDS_MISSION_OVER_FOR_JIP_LOCAL_PLAYER(LocalPlayer))
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS, GlobalPlayerBroadcastDataFM_BS_bQuitJob)
		IF IS_THIS_A_ROUNDS_MISSION()
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SCRIPT_CLEANUP(ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, LEADERBOARD_PLACEMENT_TOOLS& Placement, INT iPassType, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	RELEASE_REMOTE_PLAYER_ARENA_VEHICLE_NAME_DATA()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND iLocalPart > -1
		END_PLAYSTATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_END_STATS)
		PRINTLN("[NET_DEATHMATCH] DM_BOOLS2_END_STATS isn't set and we're clearing up!")
	ENDIF
	
	IF NETWORK_IS_SIGNED_ONLINE() AND NETWORK_IS_GAME_IN_PROGRESS()
	AND HAS_DEATHMATCH_FINISHED(serverBDpassed)
		IF bIsLocalPlayerHost
			SERVER_CLEAR_PED_AND_TRAFFIC_DENSITIES(serverBDpassed.iPopulationHandle)
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF CONTENT_IS_USING_ARENA()
		END_SERIES_UDS_ACTIVITY(g_FMMC_STRUCT.iRootContentIDHash)
	ENDIF
	#ENDIF
	
	IF NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
	AND NETWORK_IS_GAME_IN_PROGRESS()
		IF iLocalPart > -1
			
			IF IS_KING_OF_THE_HILL()
				playerBDPassed[iLocalPart].iScore = 0
				g_iMyDMScore = 0
			ENDIF
		
			playerBDPassed[iLocalPart].iJobCash = 0
			g_iMyJobCash = 0
			
			dmVarsPassed.iXPToShowOnLeaderboard = 0
			playerBDPassed[iLocalPart].iJobXP = 0
			
			PRINTLN("SCRIPT_CLEANUP - Clearing score, cash & rp for me, participant ", iLocalPart)
		ELSE
			PRINTLN("SCRIPT_CLEANUP - My participant id is ", iLocalPart, " so I'm not clearing my playerBD as I leave")
		ENDIF
	ENDIF
	
	g_bArenaBigScreenBinkPlaying = FALSE
	g_iArenaBigScreenBinkControllerBS = 0
	
	g_iLastDamagerPlayer = -1
	g_iLastDamagerTimeStamp = -1
	g_iLastPlayerIDamaged = -1
	g_iLastPlayerIDamagedTimeStamp = -1
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		GlobalPlayerBD[NATIVE_TO_INT(LocalPlayer)].iLastDamagerPlayer = -1
	ENDIF
	
	g_vehStandbyVehicle = NULL
	g_iCurrentSpectatorTrapSpawned = 0
	g_iFailedTrapCamSwitchBlackList = 0
	SET_ABILITY_BAR_VISIBILITY(FALSE)
	g_bMissionClientGameStateRunning = FALSE
	g_bMission321Done = FALSE
	g_bIntroJobShardPlayed = FALSE
	g_fTimeSpentInSpectatorMode = 0
	g_fTimeSpentInSpectatorPowerup = 0
	g_iKillsInSpectatorMode = 0
	g_iKillsInSpectatorPowerup = 0
	RESET_NET_TIMER(g_tdTelemetryDelay)
	RESET_NET_TIMER(g_tdTelemetryDelay2)
	g_iSpecialSpectatorTelemetryBS = 0
	g_iBSPlayerLeftDoSomething = 0
	CLEANUP_ARENA_ANNOUNCER_VARIABLES()	
	SET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS(-1)
	IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Veh_Select_To_VIP_Transition_Scene")
		STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Veh_Select_To_VIP_Transition_Scene")
	ENDIF	
	IF IS_AUDIO_SCENE_ACTIVE("DLC_AW_Arena_Lobby_Veh_Select_To_Arena_Transition_Scene")
		STOP_AUDIO_SCENE("DLC_AW_Arena_Lobby_Veh_Select_To_Arena_Transition_Scene")
	ENDIF	
	
	SET_AMBIENT_ZONE_STATE("AZ_COUNTRY_MAJESTIC_QUARRY_PIT", TRUE, TRUE)
	
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER() //In case the script terminated before the winner screen cleaned up.
		CLEANUP_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData)
		SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
		SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(FALSE) //Cleans up celebration chat settings.
		RESTORE_PLAYER_HEADWEAR()
		IF NOT IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
			DEAL_WITH_DLC_WEAPONS(dmVarsPassed.iDlcWeaponBitSet, FALSE)
		ENDIF
	ELSE
		HANDLE_VEH_JACKING(serverBDpassed, FALSE)
		SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
	ENDIF
	IF dmVarsPassed.iFakeAmmoRechargeSoundID > -1
		STOP_SOUND(dmVarsPassed.iFakeAmmoRechargeSoundID)
		RELEASE_SOUND_ID(dmVarsPassed.iFakeAmmoRechargeSoundID)
		dmVarsPassed.iFakeAmmoRechargeSoundID = -1
	ENDIF
	INT iQuad
	PRINTLN("[DM] REMOVE_EXTRA_CALMING_QUAD - ....")
	FOR iQuad = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		IF dmVarsPassed.iWaterCalmingQuad[iQuad] != -1
			PRINTLN("[DM] REMOVE_EXTRA_CALMING_QUAD - Removing water calming quad as iQuad ",iQuad)
			REMOVE_EXTRA_CALMING_QUAD(dmVarsPassed.iWaterCalmingQuad[iQuad])
			dmVarsPassed.iWaterCalmingQuad[iQuad] = -1
		ENDIF
	ENDFOR
	DISABLE_IN_WATER_PTFX(FALSE)
		
	IF CONTENT_IS_USING_ARENA()
		IF NETWORK_IS_GAME_IN_PROGRESS()
			NETWORK_CLEAR_CLOCK_TIME_OVERRIDE()
		ENDIF		
		CLEAR_OVERRIDE_WEATHER()
	ENDIF		
	
		IF IS_PLAYER_ON_BOSSVBOSS_DM()
			CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] CLEANUP I'M ON BOSS V BOSS")
			DO_BOSS_V_BOSS_CLEANUP(dmVarsPassed)
		ELSE	
			CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] CLEANUP I'M NOT ON BOSS V BOSS")
		ENDIF
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_PARTICIPANT_ACTIVE(PARTICIPANT_ID())
		APPLY_DM_END_STATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
	ENDIF
	RESET_IMPROMPTU_GLOBALS()
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		CLEANUP_DYNAMIC_PROPS(serverFMMCPassed)
		PRINTLN("[DM] SCRIPT_CLEANUP - Cleaning up dynoprops because I didn't leave early")
	ELSE
		PRINTLN("[DM] SCRIPT_CLEANUP - Not cleaning up dynoprops because I have quit early")
	ENDIF
	
	CLEANUP_FMMC_PROPS(dmVarsPassed.oiProps, dmVarsPassed.oiPropsChildren)
	SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
	PRINTLN("SCRIPT_CLEANUP_START_dm ")
	CLEANUP_END_JOB_VOTING(dmVarsPassed.nextJobStruct)
	UNLOAD_ALL_COMMON_LEADERBOARD_ELEMENTS(Placement)
	CLEANUP_ALL_FX()
	SET_IMPROMPTU_TARGET_PRIORITY_OFF(dmVarsPassed)
	RESET_PERK(dmVarsPassed)
	SET_ON_JOB_INTRO(FALSE)
	COLOUR_PC_TEXT_CHAT(FALSE)
	g_bMissionHideRespawnBar = FALSE
	
	CLEANUP_ALL_TRAP_PTFX(serverBDpassed.sTrapInfo_Host)
	
	DISABLE_SPECTATOR_FILTER_OPTION(FALSE)
	
	// Return weapons at the end
	IF IS_JOB_FORCED_WEAPON_ONLY()
	OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
	OR CONTENT_IS_USING_ARENA()
	OR IS_KING_OF_THE_HILL()
	OR IS_BIT_SET(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INVENTORY_GIVEN_NO_RESET)
		
		REMOVE_ALL_PLAYERS_WEAPONS()
		
		IF dmVarsPassed.wtWeaponToRemove != WEAPONTYPE_INVALID
			IF HAS_PED_GOT_WEAPON(LocalPlayerPed, dmVarsPassed.wtWeaponToRemove)
				REMOVE_WEAPON_FROM_PED(LocalPlayerPed, dmVarsPassed.wtWeaponToRemove)
				PRINTLN("[CS_WEAPON] - REMOVING WEAPON LOANED AT START ")
			ENDIF
		ENDIF
	
		GIVE_MP_REWARD_WEAPON_IN_FREEMODE(TRUE, FALSE, WEAPONINHAND_FIRSTSPAWN_HOLSTERED, FALSE, FALSE, FALSE, TRUE)
		
		IF SHOULD_RETURN_WEAPON_THROUGH_MP_REWARD()
			g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = TRUE
			PRINTLN("[DM] SCRIPT_CLEANUP - Setting SHOULD_RETURN_WEAPON_THROUGH_MP_REWARD to TRUE")
		ELSE
			g_GIVE_PLAYER_ALL_UNLOCKED_AND_EQUIPED_WEAPONS_FM_CHECK = FALSE
			PRINTLN("[DM] SCRIPT_CLEANUP - SHOULD_RETURN_WEAPON_THROUGH_MP_REWARD = FALSE")
		ENDIF
		
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Local_Player_Arrow_Team_Colour)
		SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(FALSE)
	ENDIF
	
	SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(FALSE)
	
	// Impromptu reset
	g_sImpromptuVars.playerThatSentImpromptuInvite = INVALID_PLAYER_INDEX()
	g_sImpromptuVars.playerImpromptuRival = INVALID_PLAYER_INDEX()
	
	//FLASH_THIS_PLAYER_BLIP(dmVarsPassed, serverBDpassed.playerPowerStoredLast, FALSE)
	IF IS_PLAYER_FORCE_BLIPPED(LocalPlayer)
		FORCE_BLIP_PLAYER(LocalPlayer, FALSE)
	ENDIF
	IF HAS_THIS_ADDITIONAL_TEXT_LOADED("DMATCH", MISSION_TEXT_SLOT)
		CLEAR_HELP_IF_THIS_PRINT_DISPLAYED("DM_ONE_FAR")
	ENDIF
	STOP_DM_ARENA_SCORE(dmVarsPassed)
	// Cleanup power plays/bounties
	PU_FORCE_END_RAGE()
	CLEANUP_RECEIVING_DOUBLE_DAMAGE(dmVarsPassed, LocalPlayer)
	CLEANUP_GIVING_DAMAGE(dmVarsPassed, LocalPlayer)
	DEACTIVATE_ALL_BLIPS(dmVarsPassed)
	CLEANUP_THE_VEHICLE(dmVarsPassed)
	ACTIVATE_HIDE_BLIP_PERK(dmVarsPassed, FALSE)
	
	TRIGGER_MUSIC_EVENT("MP_DM_COUNTDOWN_60_SEC_FIRA")
	//STOP_DM_SCORE(dmVarsPassed)
	KILL_DM_NEARLY_FINISHED_AUDIO(serverBDpassed, dmVarsPassed)
	SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
	SET_AUDIO_FLAG("DisableFlightMusic", FALSE)
	TOGGLE_STRIPPER_AUDIO(TRUE)
	STOP_AUDIO_SCENE("MP_DM_GENERAL_SCENE")
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(LocalPlayerPed, 0.0)
	UN_MUTE_RADIOS(dmVarsPassed)
		
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_AWXM2018/Arena_Traps")
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("dlc_awxm2018/aw_ptb_01")
	
	ANIMPOSTFX_STOP("MP_Celeb_Win")
	ANIMPOSTFX_STOP("MP_Celeb_Lose")
	STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
	STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
	IF CONTENT_IS_USING_ARENA()
		IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
			STOP_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
		ENDIF
	ENDIF
	TRIGGER_MUSIC_EVENT("GTA_ONLINE_STOP_SCORE")
	
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(dmVarsPassed.scLB_control)
	IF HAS_SCALEFORM_MOVIE_LOADED(dmVarsPassed.scLB_scaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(dmVarsPassed.scLB_scaleformID)
		PRINTLN("CLEANUP_SOCIAL_CLUB_LEADERBOARD marking SCLB scaleform movie as no longer needed") 
	ENDIF
	
	IF HAS_SCALEFORM_MOVIE_LOADED(dmVarsPassed.scLB_scaleformID)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(dmVarsPassed.scaleformDpadMovie)
	ENDIF
	
	SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
	RESET_THE_STATS_THAT_NEED_RESETTING()
	CLEANUP_LEADERBOARD_PREPARATION()
	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		SET_ENTITY_PROOFS(LocalPlayerPed,FALSE,FALSE,FALSE,FALSE,FALSE)
	ENDIF
	
	STOP_STREAM()
	NETWORK_CLEAR_VOICE_CHANNEL()
	
	TURN_SPINNER_OFF()
	
	// B*1466917.
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		IF IS_MP_GAMER_TAG_ACTIVE(iPlayer)
			SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(iPlayer, FALSE)
			PRINTLN("[WJK] - OVH - called SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(FALSE) on player: ", iPlayer)
		ENDIF
	ENDREPEAT
	dmVarsPassed.bSetupVehDmHealthBars = FALSE
	
	CLEANUP_JOB_INTRO_CUTSCENE(dmVarsPassed.DMIntroData, FALSE, FALSE) //May need to remove this
	CLEANUP_NEW_ANIMS(dmVarsPassed.DMIntroData)
	
	IF NATIVE_TO_INT(LocalPlayer) > -1
		CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(LocalPlayer)].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== HUD === Cleanup DM CLEAR_BIT(GlobalplayerBD[", NATIVE_TO_INT(LocalPlayer), "].iSpecInfoBitset, SPEC_INFO_BS_RESPAWNING)")
		#ENDIF
	ENDIF
	
	REFRESH_DM_SCALEFORM_BUTTON_SLOTS(Placement)
	
	REMOVE_PICKUPS_AND_THEIR_BLIPS(dmVarsPassed)
	
	INT iBargeProp = 0
	REPEAT 2 iBargeProp
		IF DOES_ENTITY_EXIST(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[iBargeProp])
			DELETE_OBJECT(g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[iBargeProp])
			PRINTLN("[TMS] Clearing up g_FMMC_STRUCT_ENTITIES.oiBargeCollisionProps[", iBargeProp, "]")
		ENDIF
	ENDREPEAT
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		PRINTLN("[DM] SCRIPT_CLEANUP - Doing CLEANUP_ALL_FMMC_ENTITIES because I didn't leave early")
		CLEANUP_ALL_FMMC_ENTITIES(serverFMMCPassed)
	ELSE
		PRINTLN("[DM] SCRIPT_CLEANUP - I left early, so I won't be calling CLEANUP_ALL_FMMC_ENTITIES")
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		CLEAR_MISSION_HELP()
		CLEAR_ALL_DAMAGE_EVENT_BITS(serverBDpassed)
	ENDIF
	
//	CLEANUP_NETWORK_CHAT()
	ALLOW_SPECTATORS_TO_CHAT(FALSE)
	
	// Spectator cam
    IF NOT IS_THIS_SPECTATOR_CAM_OFF(g_BossSpecData.specCamData)
		FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData, TRUE)
    ENDIF
	// Early End Spectator.
	IF playerBDPassed[iLocalPart].iSpectatorState != ci_SpectatorState_WAIT
	AND playerBDPassed[iLocalPart].iSpectatorState != ci_SpectatorState_END
		PRINTLN("[DM] SCRIPT_CLEANUP - Forcing some spectator cam cleanup procedures.")
		FORCE_CLEANUP_SPECTATOR_CAM(g_BossSpecData)
		CLEANUP_EARLY_END_DEATHMATCH_SPECTATORS(playerBDPassed)
	ENDIF	
	CLEAR_DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData)
	
	SET_TIME_OF_DAY(TIME_OFF, TRUE)
	
	// Spawn
	CLEAR_CUSTOM_SPAWN_POINTS()
	CLEAR_SPAWN_AREA()
	USE_CUSTOM_SPAWN_POINTS(FALSE)
	
	RESET_WARP_TO_SPAWN_LOCATION_GLOBALS() 
	RESET_NET_WARP_TO_COORD_GLOBALS()
	
	SET_KILLSTRIP_AUTO_RESPAWN(FALSE)
	
	// Cleanup health
	IF bLocalPlayerOK
	AND NETWORK_IS_GAME_IN_PROGRESS()
	AND iLocalPart <> -1 // overrun
	AND NOT IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
		CLEAR_ANY_DM_BIG_MESSAGES_FOR_LBD(playerBDPassed)
		SET_DM_FINISHED_BOOL(playerBDPassed)
		SET_PLAYER_CAN_BE_KNOCKED_OFF_VEHICLE(KNOCKOFFVEHICLE_DEFAULT)
		//RESTORE_PED_STARTING_ARMOUR(dmVarsPassed.ARMOUR_VALUE)
		SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_WillFlyThroughWindscreen, TRUE)
		SET_ENTITY_INVINCIBLE(LocalPlayerPed, FALSE)
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_OnlyExitVehicleOnButtonRelease, FALSE)
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			PRINTLN("IS_PED_IN_ANY_VEHICLE = TRUE")
			VEHICLE_INDEX viTemp = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
			IF IS_VEHICLE_DRIVEABLE(viTemp)
				SET_VEHICLE_DOORS_LOCKED(viTemp, VEHICLELOCK_UNLOCKED)
				PRINTLN("SET_VEHICLE_DOORS_LOCKED(viTemp, VEHICLELOCK_UNLOCKED)")
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("SET_VEHICLE_DOORS_LOCKED - vehicle dead and can't unlock")
			#ENDIF
			ENDIF
		ENDIF

		// Cleanup team targetting
		SET_PLAYER_DEFAULT_FRIENDLY_FIRE_OPTION()	
		
		INT iTeam
		REPEAT MAX_NUM_DM_TEAMS iTeam
			IF playerBDPassed[iLocalPart].iTeam = iTeam
				PRINTLN("SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeam, TRUE)")
				SET_PED_CAN_BE_TARGETTED_BY_TEAM(LocalPlayerPed, iTeam, TRUE)
			ENDIF	
		ENDREPEAT

		STORE_LOCAL_PLAYERS_PERMANENT_DM_STATS(playerBDPassed, dmVarsPassed)
	ENDIF
	
	PRINTLN(" REMOVE_SCENARIO_BLOCKING_AREAS, dm ")
	REMOVE_SCENARIO_BLOCKING_AREAS()

	SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_FOR_DEATHMATCH(FALSE)

	// Reset afterlife state (411389)
	RESET_GAME_STATE_ON_DEATH() 

	// Emergency services
	CLEANUP_POLICE()
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)			
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)	
	
	// Cleanup traffic
	PRINTLN(" SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA, TRUE")
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-9999.9, -9999.9, -9999.9>>, <<9999.9, 9999.9, 9999.9>>, TRUE, FALSE)

	// Stop the players being invisible
	IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
		AND NOT NETWORK_IS_TUTORIAL_SESSION_CHANGE_PENDING()
			NETWORK_END_TUTORIAL_SESSION()
			#IF IS_DEBUG_BUILD
				PRINTSTRING("--------------- BWW - NETWORK_END_TUTORIAL_SESSION")PRINTNL()	
			#ENDIF
		ENDIF	
	ENDIF
	
	SET_AMBIENT_PEDS_DROP_MONEY(TRUE)
	RESET_FMMC_MISSION_VARIABLES(TRUE, FALSE, iPassType)
	RESET_DM_GLOBALS()
	//If the player is dead and on a playlist then force them to respawn
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT bIsSCTV
			SET_LOCAL_PLAYER_TEAM(-1)
		ENDIF
		IF IS_PLAYER_ON_A_PLAYLIST_INT(NATIVE_TO_INT(LocalPlayer))
			IF NOT bLocalPlayerOK			
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF	
		//Force the player to respawn
		IF g_sFMMCEOM.iVoteStatus = ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART
		OR g_sFMMCEOM.iVoteStatus =  ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP
			IF NOT bLocalPlayerOK			
		  		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_AT_CURRENT_POSITION)
				PRINTLN("[TS] RESET_FMMC_MISSION_VARIABLES - SPAWN_LOCATION_AT_CURRENT_POSITION - SET_PLAYER_NEXT_RESPAWN_LOCATION - FORCE_RESPAWN(TRUE)")
				FORCE_RESPAWN(TRUE)
			ENDIF
		ENDIF
	ENDIF	
	
	IF bIsSCTV
		ACTIVATE_KILL_TICKERS(FALSE)
		PRINTLN("DM - PRE_GAME - SCTV - ACTIVATE_KILL_TICKERS - SET TO FALSE")
	ENDIF
	
	IF g_iSpecialSpectatorBS != 0
		PRINTLN("[JT][SPEC_SPEC][DM] SCRIPT_CLEANUP - setting g_ciSpecial_Spectator_BS_Request_Cleanup")
		SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
	ENDIF
	
	LAUNCH_ARENA_SPECTATOR_ACTIVITIES(FALSE)
	
	CLEAR_CACHED_AVAILABLE_VEHICLE_COUNTS()
	
	CLEANUP_IPLS_AND_INTERIORS()
	
	CLEANUP_ARENA_TRAPS(serverBDPassed.sTrapInfo_Host, dmVarsPassed.sTrapInfo_Local)
	SET_TEAM_COLOUR_OVERRIDE_FLAG(FALSE)
	IF CONTENT_IS_USING_ARENA()
		PRINTLN("Calling CLEANUP_MODE_LEAVERS_ON_OPTION for Deathmmatch")
		CLEANUP_MODE_LEAVERS_ON_OPTION()
	ENDIF
	
	LBD_SCENE_CLEANUP()
	CLEAR_RANK_REDICTION_DETAILS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_VFX_CROSS_THE_LINE)
		IF ANIMPOSTFX_IS_RUNNING("CrossLine")
			PRINTLN("[ML] Stopping Walk The Line VFX")
			ANIMPOSTFX_STOP("CrossLine")
			STOP_SOUND(-1)	// Is this needed?
		ENDIF
	ENDIF
	
	SET_IN_ARENA_MODE(FALSE)
	SET_VEHICLE_COMBAT_MODE(FALSE)
	SET_IN_STUNT_MODE(FALSE)
	ALLOW_PLAYER_HEIGHTS(FALSE)
	IF CONTENT_IS_USING_ARENA()
		CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_HAS_AIRHORN_PLAYED)
		PRINTLN("[MJL][NET_DEATHMATCH] Clearing DM_BOOL2_HAS_AIRHORN_PLAYED FALSE as we're in SCRIPT_CLEANUP")
		CLEAR_AREA_OF_VEHICLES(<<2800.0, -3800.5, 150.0>>, 200.0)
		PRINTLN("[DM][ARENA] SCRIPT_CLEANUP - Doing huge clear area with radius of 200")
	ENDIF
	g_bArenaWarsUseForcedStockImperator = FALSE
	g_bArenaWarsUseForcedRaceOutfit = FALSE
	PRINTLN("[DM] SCRIPT_CLEANUP - Cleaning up g_bArenaWarsUseForcedStockImperator & g_bArenaWarsUseForcedRaceOutfit")
	IF IS_ARENA_WARS_JOB()
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn = TRUE
		PRINTLN("[ARENA][DM] g_TransitionSessionNonResetVars.sPostMissionCleanupData.bArenaPostMissionSpawn - TRUE")
	ENDIF
	
	UNLOAD_DM_SOUNDS(dmVarsPassed)
	
	g_b_WastedShard_Eliminated_Not_Suicide = FALSE
	g_b_PTB_Timer_Up = FALSE
	g_iMyRaceModelChoice = -1
	
	ARENA_CONTESTANT_TURRET_CLEANUP(dmVarsPassed.arenaTurretContext)

	ARENA_CONTESTANT_TURRET_STACK_CLEAR()
	
	ENABLE_COUNTERMEASURE_FOR_MISSION_VEHICLES(FALSE)
	
	IF IS_KING_OF_THE_HILL()
		CLEANUP_KING_OF_THE_HILL(dmVarsPassed.sKOTH_LocalInfo)
	ENDIF
	
	LEGACY_CLEANUP_WORLD_PROPS(dmVarsPassed.sRuntimeWorldPropData)
	
	RESET_NET_TIMER(g_stPTBCooldownTimer)
	
	POWERUP_SYSTEM_CLEANUP(dmVarsPassed.sPowerUps)
	
	PROCESS_FMMC_VEHICLE_MACHINE_GUN_CLEANUP(dmVarsPassed.sVehMG)
	
	ENABLE_KILL_YOURSELF_OPTION()
	
	RESET_WEAPON_DAMAGE_MODIFIERS()
	PROCESS_DM_WEAPON_DAMAGE_MODIFIERS_CLEANUP()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND iLocalPart > -1
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			PROCESS_PLAYER_MODIFIER_CLEANUP(dmVarsPassed.sPlayerModifierData)
		ENDIF
	ENDIF
	
	BLOCK_SNACKS(FALSE)
	
	CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	PRINTLN("FINISH_FULL_DM_CLEANUP")

	TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()
ENDPROC

FUNC INT GET_WINNING_TEAM_SCORE(ServerBroadcastData &serverBDpassed) 

	INT iMyTeam 		= GET_PLAYER_TEAM(LocalPlayer)
	INT iRivalTeam 		= GET_RIVAL_TEAM(LocalPlayer, serverBDpassed)
	
	INT iMyTeamScore 	= GET_TEAM_DM_SCORE(serverBDpassed, iMyTeam)
	INT iEnemyTeamScore = GET_TEAM_DM_SCORE(serverBDpassed, iRivalTeam)

	IF iMyTeamScore >= iEnemyTeamScore
		RETURN iMyTeamScore
	ELSE
		RETURN iEnemyTeamScore
	ENDIF
	
	RETURN -1                
ENDFUNC

FUNC INT GET_LOSING_TEAM_SCORE(ServerBroadcastData &serverBDpassed) 

	INT iMyTeam 		= GET_PLAYER_TEAM(LocalPlayer)
	INT iRivalTeam 		= GET_RIVAL_TEAM(LocalPlayer, serverBDpassed)
	
	INT iMyTeamScore 	= GET_TEAM_DM_SCORE(serverBDpassed, iMyTeam)
	INT iEnemyTeamScore = GET_TEAM_DM_SCORE(serverBDpassed, iRivalTeam)

	IF iMyTeamScore < iEnemyTeamScore
		RETURN iMyTeamScore
	ELSE
		RETURN iEnemyTeamScore
	ENDIF
	
	RETURN -1                 
ENDFUNC

FUNC HUD_COLOURS GET_RECT_COLOUR_TDM_WIN(ServerBroadcastData &serverBDpassed)

	INT iMyTeam 		= GET_PLAYER_TEAM(LocalPlayer)
	INT iRivalTeam 		= GET_RIVAL_TEAM(LocalPlayer, serverBDpassed)
	
	INT iMyTeamScore 	= GET_TEAM_DM_SCORE(serverBDpassed, iMyTeam)
	INT iEnemyTeamScore = GET_TEAM_DM_SCORE(serverBDpassed, iRivalTeam)
	
	IF iMyTeamScore = iEnemyTeamScore
	
		RETURN GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
	ELSE
		IF iMyTeamScore = GET_WINNING_TEAM_SCORE(serverBDpassed)
		
			RETURN GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
		ELSE
			RETURN GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
		ENDIF
	ENDIF
	
	RETURN HUD_COLOUR_GREEN
ENDFUNC

FUNC HUD_COLOURS GET_RECT_COLOUR_TDM_LOS(ServerBroadcastData &serverBDpassed)

	INT iMyTeam 		= GET_PLAYER_TEAM(LocalPlayer)
	INT iRivalTeam 		= GET_RIVAL_TEAM(LocalPlayer, serverBDpassed)
	
	INT iMyTeamScore 	= GET_TEAM_DM_SCORE(serverBDpassed, iMyTeam)
	INT iEnemyTeamScore = GET_TEAM_DM_SCORE(serverBDpassed, iRivalTeam)
	
	IF iMyTeamScore = iEnemyTeamScore
	
		RETURN GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
	ELSE
		IF iMyTeamScore = GET_WINNING_TEAM_SCORE(serverBDpassed)
		
			RETURN GET_TEAM_DEATHMATCH_HUD_COLOUR(TRUE)
		ELSE
			RETURN GET_TEAM_DEATHMATCH_HUD_COLOUR(FALSE)
		ENDIF
	ENDIF
	
	RETURN HUD_COLOUR_GREEN
ENDFUNC

PROC START_SCOREBOARD_RUNNING_TIMER(SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_START_SCORE_TIMER)
		START_NET_TIMER(dmVarsPassed.timeScoreboardRunning)
		PRINTLN("START_SCOREBOARD_RUNNING_TIMER ")
		SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_START_SCORE_TIMER)
	ENDIF
ENDPROC

FUNC INT ARENA_WARS_SC_LDB_GET_DM_MODE_STAT_1(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN playerBDPassed[iLocalPart].iKills
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN serverBDpassed.iPTB_Passes[iLocalPart]
	ENDIF
	RETURN 0
ENDFUNC
FUNC INT ARENA_WARS_SC_LDB_GET_DM_MODE_STAT_2(PlayerBroadcastData &playerBDPassed[])
	IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN ROUND(playerBDpassed[iLocalPart].fDamageDealt)
	ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN playerBDPassed[iLocalPart].iFinishedAtTime/1000
	ENDIF
	RETURN 0
ENDFUNC
PROC ARENA_WARS_SC_LDB_DM_WRITE(PlayerBroadcastData &playerBDPassed[], ServerBroadcastData &serverBDpassed)
	ARENA_WARS_SC_LDB_SET_STAT_1(playerBDPassed[iLocalPart].sArenaSCLBStats, ARENA_WARS_SC_LDB_GET_DM_MODE_STAT_1(serverBDpassed, playerBDpassed))
	ARENA_WARS_SC_LDB_SET_STAT_2(playerBDPassed[iLocalPart].sArenaSCLBStats, ARENA_WARS_SC_LDB_GET_DM_MODE_STAT_2(playerBDpassed))
	BOOL bWinner
	IF IS_THIS_FFA_DEATHMATCH(serverBDpassed)
		bWinner = LocalPlayer = serverBDpassed.playerDMWinner
		PRINTLN("[ARENA][SC LDB] - WRITE_TO_SC_LEADERBOARD - Did I win: ", bWinner, " Winning player: ", NATIVE_TO_INT(serverBDpassed.playerDMWinner))
	ELSE
		bWinner = (GET_PLAYER_TEAM(LocalPlayer) = serverBDpassed.iWinningTeam)
		PRINTLN("[ARENA][SC LDB] - WRITE_TO_SC_LEADERBOARD - Did my team win: ", bWinner, " Winning team: ", serverBDpassed.iWinningTeam)
	ENDIF
	playerBDPassed[iLocalPart].sArenaSCLBStats.iFavVehicle = GET_ARENA_FAV_VEH(LocalPlayer)
	playerBDPassed[iLocalPart].sArenaSCLBStats.iSkillLevel = GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(LocalPlayer)
	PRINTLN("[ARENA][SC LDB] - WRITE_TO_MISSION_SC_LB - iFavVeh: ", playerBDPassed[iLocalPart].sArenaSCLBStats.iFavVehicle, " iSkillLevel: ", playerBDPassed[iLocalPart].sArenaSCLBStats.iSkillLevel)
	
	ARENA_WARS_SC_LDB_WRTE_TO_LEADERBOARD_DATA(bWinner, playerBDPassed[iLocalPart].sArenaPoints, playerBDPassed[iLocalPart].sArenaSCLBStats)
ENDPROC

PROC WRITE_LEADERBOARD_DATA(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	IF DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed)
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR NOT IS_THIS_A_RSTAR_ACTIVITY()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_WRITE_LBD_STATS)
		//IF serverBDpassed.iMissionId <> -1
		IF IS_ARENA_WARS_JOB(TRUE)
			ARENA_WARS_SC_LDB_DM_WRITE(playerBDPassed, serverBDpassed)
		ELSE
			WRITE_TO_DEATHMATCH_LEADERBOARD(GET_SUB_MODE_FOR_LBD_READ_WRITE(serverBDpassed), serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassed.iMissionId, serverBDpassedLDB)
		ENDIF
		//ELSE
		//	NET_PRINT_STRING_INT("WRITE_LEADERBOARD_DATA: serverBDpassed.iMissionId =  ", serverBDpassed.iMissionId) NET_NL()
		//ENDIF
		SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_WRITE_LBD_STATS)
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_CLIENTS_ON_LBD(ServerBroadcastData &serverBDpassed)
	
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_EVERYONE_ON_LBD)
ENDFUNC

FUNC BOOL HAS_LEADERBOARD_TIMEOUT_EXPIRED(SHARED_DM_VARIABLES &dmVarsPassed)
	IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeLeaderboardTimeOut)
	
		// Debug never time out
		#IF IS_DEBUG_BUILD
		IF dmVarsPassed.debugDmVars.bNeverTimeOut = TRUE
		
			RETURN FALSE
		ENDIF
		#ENDIF
		
		IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeLeaderboardTimeOut.timer) >= VIEW_LEADERBOARD_TIME
		
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF NOT FM_PLAY_LIST_HAVE_I_VOTED()
					FM_PLAY_LIST_I_WANT_TO_PLAY_NEXT()
				ENDIF
			ENDIF
			PRINTLN("HAS_LEADERBOARD_TIMEOUT_EXPIRED , RETURN TRUE")
			SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
		
			RETURN TRUE
		ENDIF
	ELSE
//		IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
//			IF ARE_ALL_CLIENTS_ON_LBD(serverBDpassed)
				PRINTLN("HAS_LEADERBOARD_TIMEOUT_EXPIRED , START_NET_TIMER")
				START_NET_TIMER(dmVarsPassed.timeLeaderboardTimeOut)
//			ELSE
//				PRINTLN("ARE_ALL_CLIENTS_ON_LBD")
//			ENDIF
//		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SOCIAL_CLUB_LBD_ACTIVE(SHARED_DM_VARIABLES &dmVarsPassed)
	RETURN dmVarsPassed.sEndOfMission.bActiveSCLeaderboard
ENDFUNC

PROC POPULATE_VEHICLE_FOR_LBD(ServerBroadcastData_Leaderboard &serverBDpassedLDB, PlayerBroadcastData &playerBDPassed[])

	IF CONTENT_IS_USING_ARENA()
		
		INIT_ARENA_CUSTOM_VEHICLE_NAMES()
		
		INT i
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i			
			IF serverBDpassedLDB.leaderBoard[i].playerID <> INVALID_PLAYER_INDEX()
				INT iParticipant = serverBDpassedLDB.leaderBoard[i].iParticipant
				IF iParticipant != -1
				
					MODEL_NAMES model = serverBDpassedLDB.leaderBoard[i].mnVehModel

					IF IS_MODEL_VALID(model)
						g_tlRaceVeh[iParticipant]	= GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(model)
						PRINTLN("[POPULATE_VEHICLE_FOR_LBD] tlRaceVeh = ", g_tlRaceVeh[iParticipant])
					ELSE
						PRINTLN("[POPULATE_VEHICLE_FOR_LBD] IS_MODEL_VALID FALSE ")
					ENDIF
				
					TEXT_LABEL_31 tlCustomName = PROCESS_ARENA_CUSTOM_VEHICLE_NAMES(serverBDpassedLDB.leaderBoard[i].playerID, iParticipant, playerBDPassed[iParticipant].iDisplaySlot)
					IF NOT IS_STRING_NULL_OR_EMPTY(tlCustomName)
						g_tlRaceVeh[iParticipant] = tlCustomName
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC DEATHMATCH_POPULATE_LEADERBOARD_COLUMNS(ServerBroadcastData_Leaderboard &serverBDpassedLDB, LBD_SUB_MODE submode, INT &iColumn1, INT &iColumn2, INT &iColumn3, INT &iColumn4, INT &iColumn5, INT &iColumn6, FLOAT &fFloatValue, INT i)

	FLOAT fKills, fDeaths
	
	IF submode = SUB_DM_UPDATE
		iColumn1 = serverBDpassedLDB.leaderBoard[i].iScore
		iColumn2 = serverBDpassedLDB.leaderBoard[i].iKills
		iColumn3 = serverBDpassedLDB.leaderBoard[i].iDeaths
		iColumn4 = serverBDpassedLDB.leaderBoard[i].iJobXP
		iColumn5 = serverBDpassedLDB.leaderBoard[i].iJobCash
		
		UNUSED_PARAMETER(iColumn6)
		
		fKills = TO_FLOAT(serverBDpassedLDB.leaderBoard[i].iKills)
		fDeaths = TO_FLOAT(serverBDpassedLDB.leaderBoard[i].iDeaths)
		fFloatValue = GET_KD_RATIO(fDeaths, fKills)
	ENDIF

ENDPROC

FUNC BOOL HAS_LEADERBOARD_FINISHED(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, LEADERBOARD_PLACEMENT_TOOLS& Placement, ServerBroadcastData &serverBDpassed, 
											PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB, BOOL bIsSpectatorLeaderboard = FALSE)
												
	INT i, iSlotCounter, iPlayerTeam, iRow
	INT iTeamRow = -1			
	INT iTeamNamesRow = -1
	INT iTeamSlotCounter[MAX_NUM_DM_TEAMS]
	INT iLocalTeam, iEnemyTeam
	INT iLbdPlayer
	LBD_SUB_MODE submode
	submode = GET_SUB_MODE(serverBDpassed)
	INT iNumColumns
	iNumColumns = GET_NUM_LBD_COLUMNS(submode)
	BOOL bIsTeamMode
	bIsTeamMode = IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
	IF g_FinalRoundsLbd
	OR IS_TDM_USING_FFA_SCORING()
		bIsTeamMode = FALSE
	ENDIF
	BOOL bDrawTeamRow[MAX_NUM_DM_TEAMS]
	INT iSpectatorNumber
	STRING sSpectatorName
	BOOL bDrawPlayersRow[MAX_NUM_DM_TEAMS]
	BOOL bDrawPlayersRowUsed[MAX_NUM_DM_TEAMS]
	INT iPlayerCount = serverBDpassed.iNumDMPlayers
	DISABLE_ALL_MP_HUD_THIS_FRAME()
	
	INT iColumn1, iColumn2, iColumn3, iColumn4, iColumn5, iColumn6

	INT iRowForLbd
	FLOAT fFloatValue
	
	#IF IS_DEBUG_BUILD
	IF g_SkipCelebAndLbd
	
		RETURN TRUE
	ENDIF	
	#ENDIF
	
	BOOL bArena
	IF CONTENT_IS_USING_ARENA()
		bArena = TRUE
	ENDIF
	
	IF NOT HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED(Placement, TRUE)
		PRINTLN("HAS_LEADERBOARD_FINISHED, HAVE_ALL_COMMON_LEADERBOARD_ELEMENTS_LOADED EXIT ")
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeScoreboardRunning)
	AND NOT bIsSpectatorLeaderboard
		START_SCOREBOARD_RUNNING_TIMER(dmVarsPassed)
	ELSE
		// Team scrolling
		//IF NOT bIsSpectatorLeaderboard
			TRACK_LBD_ROW_FOR_SCROLL(dmVarsPassed.lbdVars, serverBDpassed.iNumberOfTeams, serverBDpassed.iNumDMPlayers + serverBDpassed.iNumSpectators, bIsTeamMode AND serverBDpassed.iNumSpectators = 0)
		//ENDIF
		INT iConorsSubMode = GET_SUBTYPE_FOR_SC(serverBDpassed)
		IF IS_THIS_A_RSTAR_ACTIVITY()
			//CDM 13/2/13
			//For the moment at least dont pass in laps.. it works its just whether we want to display it or not.
			TEXT_LABEL_23 modeName
			IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
				modeName = 	GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_CARNAGE)
				SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(dmVarsPassed.scLB_control,
													SCLB_TYPE_ARENA_WARS_MODES,
													"ArenaMode",
													modeName,
													SCLB_TYPE_ARENA_MODE_CARNAGE)
			ELIF IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
				modeName = 	GET_ARENA_WARS_MODE_NAME(SCLB_TYPE_ARENA_MODE_HOT_BOMB)
				SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(dmVarsPassed.scLB_control,
													SCLB_TYPE_ARENA_WARS_MODES,
													"ArenaMode",
													modeName,
													SCLB_TYPE_ARENA_MODE_HOT_BOMB)
			ELSE
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(dmVarsPassed.scLB_control,
										GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iCurrentMissionType,
										g_FMMC_STRUCT.tl31LoadedContentID,
										g_FMMC_STRUCT.tl63MissionName,//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl31MissionName,
										iConorsSubMode, 
										0)
			//LOAD_SOCIAL_CLUB_LEADERBOARD_DATA(dmVarsPassed.scLB_control)
			ENDIF
		ENDIF
		
		IF IS_SOCIAL_CLUB_LBD_ACTIVE(dmVarsPassed)
			DRAW_MAIN_HUD_BACKGROUND()
			DRAW_SC_SCALEFORM_LEADERBOARD(dmVarsPassed.scLB_scaleformID,dmVarsPassed.scLB_control)	
		ELSE
			// Check for player card press
			//IF NOT bIsSpectatorLeaderboard
				IF g_i_ActualLeaderboardPlayerSelected <> -1
				AND IS_BIT_SET(dmVarsPassed.iSpectatorBit, g_i_ActualLeaderboardPlayerSelected)
					HANDLE_SPECTATOR_PLAYER_CARDS(dmVarsPassed.lbdVars, dmVarsPassed.iSavedSpectatorRow)
				ELSE
					BOOL bJobSpectating = (GET_SPECTATOR_HUD_STAGE(g_BossSpecData.specHUDData) = SPEC_HUD_STAGE_LEADERBOARD)
					HANDLE_THE_PLAYER_CARDS(serverBDpassedLDB.leaderBoard, dmVarsPassed.lbdVars, dmVarsPassed.iSavedRow, bJobSpectating)
				ENDIF
			//ENDIF
			PRINTLN("HAS_LEADERBOARD_FINISHED - iLocalPart: ", iLocalPart)
			iLocalTeam = playerBDPassed[iLocalPart].iTeam				
		
			// Scroll players
			//IF NOT bIsSpectatorLeaderboard
				PROCESS_LBD_SCROLL_LOGIC(dmVarsPassed.lbdVars, dmVarsPassed.timeScrollDelay, dmVarsPassed.iPlayerSelection, dmVarsPassed.iTeamRowBit, dmVarsPassed.iTeamNameRowBit,(serverBDpassed.iNumDMPlayers + serverBDpassed.iNumSpectators), serverBDpassed.iNumberOfTeams, bIsTeamMode)
				GRAB_SPECTATOR_PLAYER_SELECTED(serverBDpassedLDB.leaderBoard, dmVarsPassed.iSavedRow)
			//ENDIF
			// Draw the leaderboard
			RENDER_LEADERBOARD(	dmVarsPassed.lbdVars, Placement, submode, TRUE, GET_LEADERBOARD_TITLE(submode, g_FMMC_STRUCT.tl63MissionName), iNumColumns, -1)		
			
			IF bIsTeamMode
				iPlayerCount = (serverBDpassed.iNumWinners + serverBDpassed.iTeam2Count + serverBDpassed.iTeam3Count + serverBDpassed.iTeam4Count)
			ENDIF
			
			INT iNumLbdTeams = serverBDpassed.iNumberOfTeams
			IF iNumLbdTeams > iPlayerCount
				iNumLbdTeams = iPlayerCount
			ENDIF
			
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
			
				// Draw spectators
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
					INT iPlayer = NATIVE_TO_INT(playerId)
					INT iSpectatorRow = GET_SPECTATOR_ROW(bIsTeamMode, iPlayerCount, iNumLbdTeams, iSpectatorNumber)
					IF iPlayer <> -1
						IF IS_PLAYER_SPECTATOR(playerBDPassed, playerId, i)
							sSpectatorName = GET_PLAYER_NAME(playerId)
							DRAW_SPECTATOR_ROW(dmVarsPassed.lbdVars, Placement, submode, playerId, sSpectatorName, GlobalplayerBD_FM[iPlayer].scoreData.iRank, iSpectatorRow, i)
							dmVarsPassed.iSavedSpectatorRow[iPlayer] = iSpectatorRow	
							SET_BIT(dmVarsPassed.iSpectatorBit, iSpectatorRow)
							
							iSpectatorNumber ++
						ENDIF
					ENDIF
				ENDIF
				
				IF (serverBDpassedLDB.leaderBoard[i].playerID <> INVALID_PLAYER_INDEX())
				
					iLbdPlayer = NATIVE_TO_INT(serverBDpassedLDB.leaderBoard[i].playerID)
					
					IF iLbdPlayer = -1
						PRINTLN("HAS_LEADERBOARD_FINISHED - Skipping player ", i, " ", GET_PLAYER_NAME(serverBDpassedLDB.leaderBoard[i].playerID), " iLbdPlayer is -1")
						RELOOP
					ENDIF
					
					// Swap betting column for votes
					IF g_SwapBettingColumnForVotes = FALSE
						IF HAS_PLAYER_VOTED_AT_END_OF_MISSION(serverBDpassedLDB.leaderBoard[i].playerID)
							IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeScoreboardRunning, BET_SWAP_DELAY)
								g_SwapBettingColumnForVotes = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_MODEL_VALID(serverBDpassedLDB.leaderBoard[i].mnVehModel)
						dmVarsPassed.lbdVars.tlRaceVeh	= GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(serverBDpassedLDB.leaderBoard[i].mnVehModel)
						PRINTLN("[RACE_VEH_NAME] tlRaceVeh = ", dmVarsPassed.lbdVars.tlRaceVeh, " i = ", i)
					ELSE
						PRINTLN("[RACE_VEH_NAME] IS_MODEL_VALID FALSE for i = ", i)
					ENDIF
					
					IF bIsTeamMode	
						// Grab player team
						iPlayerTeam = serverBDpassedLDB.leaderBoard[i].iTeam
					ELSE
					
						IF serverBDpassedLDB.leaderBoard[i].playerID = LocalPlayer
							iLocalTeam = iPlayerTeam
							iEnemyTeam = iPlayerTeam
						ELSE
							iLocalTeam = iPlayerTeam
							iEnemyTeam = -2
						ENDIF
					
					ENDIF
					
					// Check whether to draw thumbs on lbd/ up/ down
					CHECK_LBD_THUMBS(dmVarsPassed.lbdVars, iLbdPlayer)

					IF iPlayerTeam <> -1
						// Hide passengers from the leaderboard
						IF NOT bDrawPlayersRow[iPlayerTeam]
							IF bDrawPlayersRowUsed[iPlayerTeam] = FALSE
								bDrawPlayersRow[iPlayerTeam] = TRUE
								bDrawPlayersRowUsed[iPlayerTeam] = TRUE
							ENDIF
						ELSE
							bDrawPlayersRow[iPlayerTeam] = FALSE
						ENDIF
						
						// Hiding position numbers on end lbd
						IF serverBDpassed.iNumWinners <= 1
						AND serverBDpassed.iTeam2Count <= 1
						AND serverBDpassed.iTeam3Count <= 1
						AND serverBDpassed.iTeam4Count <= 1
						
							dmVarsPassed.lbdVars.bHidePos = TRUE
						ELSE
							IF bDrawPlayersRow[iPlayerTeam]
								dmVarsPassed.lbdVars.bHidePos = FALSE
							ELSE
								dmVarsPassed.lbdVars.bHidePos = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					iEnemyTeam = iPlayerTeam
					
					PRINTLN("iPlayerTeam: ", iPlayerTeam)
					PRINTLN("iLbdPlayer: ", iLbdPlayer)
					PRINTLN("i: ", i)
					
					IF bIsTeamMode		
					
						iRow = GET_ROW_TO_DRAW_PLAYERS(iPlayerTeam, iNumLbdTeams, iTeamSlotCounter[iPlayerTeam],
														serverBDpassed.iNumWinners, serverBDpassed.iTeam2Count, serverBDpassed.iTeam3Count, serverBDpassed.iTeam4Count,0,0,0,
														serverBDpassed.iWinningTeam, serverBDpassed.iSecondTeam, serverBDpassed.iThirdTeam, -1, -1, -1, -1,
														serverBDpassed.iLosingTeam, FALSE, TRUE)
														
						dmVarsPassed.iSavedRow[iLbdPlayer] = iRow
					
					
						iTeamRow = GET_TEAM_BARS_ROW(iPlayerTeam, iNumLbdTeams,
													serverBDpassed.iNumWinners, serverBDpassed.iTeam2Count, serverBDpassed.iTeam3Count, serverBDpassed.iTeam4Count,0,0,0,0,
													serverBDpassed.iWinningTeam, serverBDpassed.iSecondTeam, serverBDpassed.iThirdTeam, -1, -1, -1, -1, 
													serverBDpassed.iLosingTeam, FALSE)
								
						iTeamNamesRow = GET_TEAM_NAME_ROWS(iPlayerTeam, iNumLbdTeams, iTeamSlotCounter[iPlayerTeam],
													serverBDpassed.iNumWinners, serverBDpassed.iTeam2Count, serverBDpassed.iTeam3Count, serverBDpassed.iTeam4Count,0,0,0,
													serverBDpassed.iWinningTeam, serverBDpassed.iSecondTeam, serverBDpassed.iThirdTeam, -1, -1, -1, -1)
					ELSE
						dmVarsPassed.iSavedRow[iLbdPlayer] = iSlotCounter
					ENDIF
							
					IF iTeamRow <> -1							
						SET_BIT(dmVarsPassed.iTeamRowBit, iTeamRow)		
					ENDIF
					
					IF iTeamNamesRow <> -1							
						SET_BIT(dmVarsPassed.iTeamNameRowBit, iTeamNamesRow)		
					ENDIF
					
					iRowForLbd = iSlotCounter
					IF bIsTeamMode
						iRowForLbd = iRow
					ENDIF
					
					SET_LBD_SELECTION_TO_PLAYER_POS(dmVarsPassed.lbdVars, 
													serverBDpassedLDB.leaderBoard[i].playerID, 
													dmVarsPassed.iPlayerSelection, 
													iRowForLbd, 
													serverBDpassed.iNumDMPlayers)
					FLOAT fKills, fDeaths
					IF IS_THIS_A_ROUNDS_MISSION()
						IF NOT g_FinalRoundsLbd
							iColumn1 = serverBDpassedLDB.leaderBoard[i].iScore
							iColumn2 = serverBDpassedLDB.leaderBoard[i].iDeaths
							iColumn3 = serverBDpassedLDB.leaderBoard[i].iJobXP
							iColumn4 = serverBDpassedLDB.leaderBoard[i].iJobCash
							iColumn5 = serverBDpassedLDB.leaderBoard[i].iBets
							
							fKills = TO_FLOAT(serverBDpassedLDB.leaderBoard[i].iKills)
							fDeaths = TO_FLOAT(serverBDpassedLDB.leaderBoard[i].iDeaths)
							fFloatValue = GET_KD_RATIO(fDeaths, fKills)
							
							DEATHMATCH_POPULATE_LEADERBOARD_COLUMNS(serverBDpassedLDB, submode, iColumn1, iColumn2, iColumn3, iColumn4, iColumn5, iColumn6, fFloatValue, i)
						ELSE
							iColumn1 = GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iScore
							iColumn2 = GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iKills
							iColumn3 = GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iDeaths
							iColumn4 = GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iRP
							iColumn5 = GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iCash
							
							fKills = TO_FLOAT(GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iKills)
							fDeaths = TO_FLOAT(GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iDeaths)
							fFloatValue = GET_KD_RATIO(fDeaths, fKills)
						ENDIF						
					ELSE
						iColumn1 = serverBDpassedLDB.leaderBoard[i].iScore
						iColumn2 = serverBDpassedLDB.leaderBoard[i].iDeaths
						iColumn3 = serverBDpassedLDB.leaderBoard[i].iJobXP
						iColumn4 = serverBDpassedLDB.leaderBoard[i].iJobCash
						iColumn5 = serverBDpassedLDB.leaderBoard[i].iBets
						
						fFloatValue = serverBDpassedLDB.leaderBoard[i].fKDRatio							
						
						IF bArena
							iColumn3 = serverBDpassedLDB.leaderBoard[i].iArenaPoints			
							fFloatValue = serverBDpassedLDB.leaderBoard[i].fDamageDealt
						ENDIF
						
						IF submode = SUB_KOTH
							iColumn2 = serverBDpassedLDB.leaderBoard[i].iKills
							iColumn3 = serverBDpassedLDB.leaderBoard[i].iDeaths
							iColumn4 = serverBDpassedLDB.leaderBoard[i].iJobXP 
							iColumn5 = serverBDpassedLDB.leaderBoard[i].iJobCash 
							iColumn6 = serverBDpassedLDB.leaderBoard[i].iBets
						ENDIF
						
						DEATHMATCH_POPULATE_LEADERBOARD_COLUMNS(serverBDpassedLDB, submode, iColumn1, iColumn2, iColumn3, iColumn4, iColumn5, iColumn6, fFloatValue, i)
					ENDIF
					
					PRINTLN("HAS_LEADERBOARD_FINISHED - Player: ", serverBDpassed.tl63_ParticipantNames[serverBDpassedLDB.leaderBoard[i].iParticipant])
					PRINTLN("HAS_LEADERBOARD_FINISHED - iColumn1: ", iColumn1)
					PRINTLN("HAS_LEADERBOARD_FINISHED - iColumn2: ", iColumn2)
					PRINTLN("HAS_LEADERBOARD_FINISHED - iColumn3: ", iColumn3)
					PRINTLN("HAS_LEADERBOARD_FINISHED - iColumn4: ", iColumn4)
					PRINTLN("HAS_LEADERBOARD_FINISHED - iColumn5: ", iColumn5)
					PRINTLN("HAS_LEADERBOARD_FINISHED - iColumn6: ", iColumn6)
					PRINTLN("HAS_LEADERBOARD_FINISHED - fFloatValue: ", fFloatValue)
										
					POPULATE_COLUMN_LBD(dmVarsPassed.lbdVars,
										Placement,
										submode, 
										GET_VOTE_FOR_PLAYER(serverBDpassedLDB.leaderBoard[i].playerID, serverBDpassedLDB.leaderBoard[i].iParticipant),
										serverBDpassed.tl63_ParticipantNames,
										iNumColumns,
										fFloatValue,
										iColumn1,
										iColumn2,
										iColumn3,		
										iColumn4,		
										iColumn5,	
										iColumn6,
										iLocalTeam,
										iEnemyTeam,
										iRowForLbd, 
										serverBDpassedLDB.leaderBoard[i].iRank,
										serverBDpassedLDB.leaderBoard[i].iParticipant,
										serverBDpassedLDB.leaderBoard[i].iBadgeRank, 
										serverBDpassedLDB.leaderBoard[i].playerID, 
										bIsTeamMode,
										bDrawTeamRow[iPlayerTeam],
										GET_TEAM_DM_SCORE(serverBDpassed, iPlayerTeam, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)),  // Get a rounds version of this.
										iTeamRow,
										serverBDpassed.iTeamPosition[iPlayerTeam],
										GlobalplayerBD_FM_2[serverBDpassedLDB.leaderBoard[i].iParticipant].sJobRoundData.iRoundScore,
										iTeamNamesRow,
										DEFAULT,
										serverBDpassedLDB.leaderBoard[i].iFinishTime)
					
					IF bIsTeamMode
						iTeamSlotCounter[iPlayerTeam]++		
					ELSE
						iSlotCounter++	
					ENDIF

				ENDIF
			ENDREPEAT
		ENDIF
		
		IF NOT bIsSpectatorLeaderboard
			IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
					IF SHOULD_ALLOW_THUMB_VOTE()
					AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_THUMB_VOTING_ALLOWED)
					ENDIF
				ELSE
					BOOL bDoneSaveAlready = IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMB_VOTE_SAVED)
					DO_THUMB_VOTING(dmVarsPassed.sSaveOutVars, dmVarsPassed.sEndOfMission, bDoneSaveAlready, HAS_LEADERBOARD_TIMEOUT_EXPIRED(dmVarsPassed), TRUE)
				ENDIF
				IF CONTROL_PLAYLIST_ON_MISSION_LEADER_BOARD_BUTTONS(dmVarsPassed.iRestartProgress, 
																	dmVarsPassed.sEndOfMission, 
																	dmVarsPassed.timeLeaderboardTimeOut.timer, 
																	VIEW_LEADERBOARD_TIME, 
																	"END_LBD_CONTN", 
																	FALSE, 
																	FALSE, 
																	DEFAULT,
																	TRUE)
																	
																	
																	
					SCRIPT_CLEANUP(serverBDpassed, serverFMMCPassed, playerBDPassed, dmVarsPassed, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED, serverBDpassedLDB)
				ENDIF
			ELSE
				BOOL bSCLeaderboardAvailable
				IF IS_THIS_A_RSTAR_ACTIVITY()
					bSCLeaderboardAvailable = TRUE
				ELSE
					bSCLeaderboardAvailable = FALSE
				ENDIF
				IF MAINTAIN_END_OF_MISSION_SCREEN(	dmVarsPassed.lbdVars,
													dmVarsPassed.sEndOfMission, 
													serverBDpassed.sServerFMMC_EOM,
													HAS_LEADERBOARD_TIMEOUT_EXPIRED(dmVarsPassed),
													TRUE,
													FALSE,
													TRUE,
													TRUE,
													bSCLeaderboardAvailable,
													dmVarsPassed.timeLeaderboardTimeOut.timer,
													VIEW_LEADERBOARD_TIME,
													"END_LBD_WAITPN",
													FALSE,
													TRUE,
													SHOULD_RUN_NEW_VOTING_SYSTEM(dmVarsPassed.sEndOfMission.iQuitProgress, serverBDpassed.sServerFMMC_EOM.iVoteStatus))
													
					PRINTLN("MAINTAIN_END_OF_MISSION_SCREEN, RETURN TRUE ")
					
					THEFEED_FORCE_RENDER_OFF()
					
					RETURN TRUE
				ELSE
					BOOL bDoneSaveAlready = IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMB_VOTE_SAVED)
					DO_THUMB_VOTING(dmVarsPassed.sSaveOutVars, dmVarsPassed.sEndOfMission, bDoneSaveAlready, HAS_LEADERBOARD_TIMEOUT_EXPIRED(dmVarsPassed))
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_ROUNDS_LEADERBOARD_FINISHED(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, LEADERBOARD_PLACEMENT_TOOLS& Placement, ServerBroadcastData &serverBDpassed, 
											PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB, BOOL bIsSpectatorLeaderboard = FALSE)
		
	START_NET_TIMER(dmVarsPassed.tdLeaderboardRoundTimer)
		
	IF IS_ROUNDS_MISSION_OVER_FOR_JIP_PLAYER(LocalPlayer)
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(dmVarsPassed.tdLeaderboardRoundTimer, 7500)
			IF NOT IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)
				g_FinalRoundsLbd = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("HAS_ROUNDS_LEADERBOARD_FINISHED - tdLeaderboardRoundTimer: ", GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(dmVarsPassed.tdLeaderboardRoundTimer))
	
	PRINTLN("AM_I_AT_END_OF_ROUNDS_MISSION - ", AM_I_AT_END_OF_ROUNDS_MISSION())
	PRINTLN("SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD - ", SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD())
	
	IF NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
	OR (SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD() AND NOT IS_PLAYER_ON_A_PLAYLIST(LocalPlayer))
		IF HAS_NET_TIMER_EXPIRED_READ_ONLY(dmVarsPassed.tdLeaderboardRoundTimer, 15000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF HAS_LEADERBOARD_FINISHED(serverFMMCPassed, Placement, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB, bIsSpectatorLeaderboard)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

// Repeat through all clients
PROC MAINTAIN_DEATHMATCH_PLAYER_LOOP(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_DEATHMATCH_PLAYER_LOOP") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF	
	
	INT iParticipant, iPlayer
	PLAYER_INDEX PlayerId
	PED_INDEX playerPed
	BOOL bVehicleDeathmatch =  IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	
	IF IS_KING_OF_THE_HILL()
		CLEAR_KING_OF_THE_HILL_PLAYERS_IN_ALL_HILLS(serverBDpassed.sKOTH_HostInfo)
	ENDIF
	
//	IF HAS_DM_STARTED(serverBDpassed)
	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_PRE_LOOP_MARKER("loop")
		#ENDIF
		#ENDIF
		
		dmVarsPassed.fFurthestTargetDist = 0.0
		FLOAT fDistance
		
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant		
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			
			#ENDIF
			#ENDIF	
		
			PlayerID = INVALID_PLAYER_INDEX()
			BOOL bParticipantActive = FALSE
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			
				PlayerId 	= NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				playerPed 	= GET_PLAYER_PED(PlayerID)
				iPlayer		= NATIVE_TO_INT(PlayerID)
				bParticipantActive = TRUE
			
				IF IS_NET_PLAYER_OK(PlayerId, FALSE)	
				
					// 1638856
					// Once the player is alive clear the bit that blocks dupe events
					IF IS_NET_PLAYER_OK(PlayerId, TRUE)	
						IF IS_BIT_SET(GlobalplayerBD_FM[iPlayer].iVehDeathmatchKillBitSet, iPlayer)
							PRINTLN(" [CS_VEH_DM] CLEARING_BIT FOR iPlayer = ", iPlayer)
							CLEAR_BIT(GlobalplayerBD_FM[iPlayer].iVehDeathmatchKillBitSet, iPlayer)
						ENDIF
					ENDIF
			
					IF NOT bVehicleDeathmatch
						IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, PlayerId, iParticipant)
							IF playerPed <> LocalPlayerPed
								// Grab distance between players for radar zoom
								fDistance = GET_DISTANCE_BETWEEN_ENTITIES(LocalPlayerPed, playerPed) 
								//PRINTLN("[ZOOM] fDistance = ", dmVarsPassed.fFurthestTargetDist)
								IF fDistance > dmVarsPassed.fFurthestTargetDist
									dmVarsPassed.fFurthestTargetDist = fDistance 
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// DEFINE_NAMES_ARRAY, grab names for lbd and gamer handles
					IF IS_STRING_NULL_OR_EMPTY(serverBDpassed.tl63_ParticipantNames[iparticipant])	
						IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, PlayerId, iParticipant)
							STORE_GAMER_HANDLES(dmVarsPassed.lbdVars, PlayerId, iparticipant)
							PRINTLN(" [CS_DPAD] DEFINE_NAMES_ARRAY,  iparticipant = ", iparticipant, " tl63_ParticipantNames = ", serverBDpassed.tl63_ParticipantNames[iparticipant])
						ENDIF
					ENDIF
					
					IF HAS_DM_STARTED(serverBDpassed)
						
						POPULATE_HEADSHOTS_LBD(PlayerId)
						
						// Only do this stuff when the deathmatch is running
						IF playerBDPassed[iParticipant].eClientLogicStage >= CLIENT_MISSION_STAGE_FIGHTING
						AND playerBDPassed[iParticipant].eClientLogicStage < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
							// -----------------------------------------------------------------------------------------------//					
							IF IS_PLAYER_ON_IMPROMPTU_DM()
							OR IS_PLAYER_ON_BOSSVBOSS_DM()
//								serverBDpassed.tl63_ParticipantNames[iParticipant] = GET_PLAYER_NAME(PlayerId)
							ELSE
								// 	Check for idle players					
								// -----------------------------------------------------------------------------------------------//
									CLIENT_SET_IDLE_GLOBALS(serverBDpassed, playerBDPassed, dmVarsPassed, PlayerId, iParticipant)
									#IF IS_DEBUG_BUILD
									#IF SCRIPT_PROFILER_ACTIVE
									ADD_SCRIPT_PROFILE_MARKER("CLIENT_SET_IDLE_GLOBALS")
									#ENDIF
									#ENDIF
									
								// 	Show big kill streak bounty messagex
								// -----------------------------------------------------------------------------------------------//
									HANDLE_KILL_STREAK_BOUNTY_MESSAGES(serverBDpassed, playerBDPassed, PlayerId, dmVarsPassed)
									#IF IS_DEBUG_BUILD
									#IF SCRIPT_PROFILER_ACTIVE
									ADD_SCRIPT_PROFILE_MARKER("HANDLE_KILL_STREAK_BOUNTY_MESSAGES")
									#ENDIF
									#ENDIF
								// -----------------------------------------------------------------------------------------------//
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			IF IS_KING_OF_THE_HILL()
				PROCESS_KOTH_PARTICIPANT_EVERY_FRAME_CHECKS(iParticipant, bParticipantActive, serverBDpassed, playerBDPassed)
			ENDIF
		ENDREPEAT
//	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	ADD_SCRIPT_PROFILE_POST_LOOP_MARKER()
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF		
ENDPROC


FUNC BOOL IS_PED_IN_SAME_VEHICLE_AS_ANY_PARTICIPANT(PED_INDEX ped)
	IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(ped)
		RETURN FALSE
	ENDIF
	
	INT i
	PLAYER_INDEX playerGhost
	PED_INDEX pedGhost
	REPEAT NUM_NETWORK_PLAYERS i
		playerGhost = INT_TO_PLAYERINDEX(i)
		IF NETWORK_IS_PLAYER_ACTIVE(playerGhost)
			IF NETWORK_IS_PLAYER_A_PARTICIPANT(playerGhost)
				pedGhost = GET_PLAYER_PED(playerGhost)
				IF pedGhost != ped
					IF ARE_PEDS_IN_SAME_VEHICLE(ped, pedGhost)
						CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [IS_PED_IN_SAME_VEHICLE_AS_ANY_PARTICIPANT] IN SAME VEHICLE AS PARTICIPANT ", GET_PLAYER_NAME(playerGhost))
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Want to set players not involved in the Boss Vs Boss DM as ghosted to those taking part
/// PARAMS:
///    dmVarsPassed - 
PROC MAINTAIN_GHOST_NON_BOSS_V_BOSS_PARTICIPANTS(SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	
	INT i
	PLAYER_INDEX playerGhost
	REPEAT NUM_NETWORK_PLAYERS i
		playerGhost = INT_TO_PLAYERINDEX(i)
		IF playerGhost != LocalPlayer
			IF NETWORK_IS_PLAYER_ACTIVE(playerGhost)
				IF NOT IS_BIT_SET(dmVarsPassed.iInitRemoteGhostBitset, i)
					SET_REMOTE_PLAYER_AS_GHOST(playerGhost, FALSE)
					SET_BIT(dmVarsPassed.iInitRemoteGhostBitset, i)
					CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [MAINTAIN_GHOST_NON_BOSS_V_BOSS_PARTICIPANTS] I HAVE SET PLAYER ", GET_PLAYER_NAME(playerGhost), " AS NOT REMOTELY GHOSTED AS THE SCRIPT HAS JUST LAUNCHED")
				ELSE	
					IF NOT NETWORK_IS_PLAYER_A_PARTICIPANT(playerGhost)
						IF NOT IS_PED_INJURED(GET_PLAYER_PED(playerGhost))
							IF bLocalPlayerPedOK
								IF NOT IS_BIT_SET(dmVarsPassed.iSetRemotelyGhostedBitset, i)
									IF NOT ARE_PEDS_IN_SAME_VEHICLE(LocalPlayerPed, GET_PLAYER_PED(playerGhost))
									AND NOT IS_PED_IN_SAME_VEHICLE_AS_ANY_PARTICIPANT(GET_PLAYER_PED(playerGhost))
										SET_REMOTE_PLAYER_AS_GHOST(playerGhost, TRUE)
										SET_BIT(dmVarsPassed.iSetRemotelyGhostedBitset, i)
										CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [MAINTAIN_GHOST_NON_BOSS_V_BOSS_PARTICIPANTS] I HAVE SET PLAYER ", GET_PLAYER_NAME(playerGhost), " AS REMOTELY GHOSTED AS THEY ARE NOT A PARTICIPANT")
									ENDIF
								ELSE
									IF ARE_PEDS_IN_SAME_VEHICLE(LocalPlayerPed, GET_PLAYER_PED(playerGhost))
										SET_REMOTE_PLAYER_AS_GHOST(playerGhost, FALSE)
										CLEAR_BIT(dmVarsPassed.iSetRemotelyGhostedBitset, i)
										CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [MAINTAIN_GHOST_NON_BOSS_V_BOSS_PARTICIPANTS] I HAVE SET PLAYER ", GET_PLAYER_NAME(playerGhost), " AS NOT REMOTELY GHOSTED AS WE ARE IN THE SAME VEHICLE")
									ELIF IS_PED_IN_SAME_VEHICLE_AS_ANY_PARTICIPANT(GET_PLAYER_PED(playerGhost))
										SET_REMOTE_PLAYER_AS_GHOST(playerGhost, FALSE)
										CLEAR_BIT(dmVarsPassed.iSetRemotelyGhostedBitset, i)
										CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [MAINTAIN_GHOST_NON_BOSS_V_BOSS_PARTICIPANTS] I HAVE SET PLAYER ", GET_PLAYER_NAME(playerGhost), " AS NOT REMOTELY GHOSTED AS IS_PED_IN_SAME_VEHICLE_AS_ANY_PARTICIPANT")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF IS_BIT_SET(dmVarsPassed.iSetRemotelyGhostedBitset, i)
							SET_REMOTE_PLAYER_AS_GHOST(playerGhost, FALSE)
							CLEAR_BIT(dmVarsPassed.iSetRemotelyGhostedBitset, i)
							CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [MAINTAIN_GHOST_NON_BOSS_V_BOSS_PARTICIPANTS] I HAVE SET PLAYER ", GET_PLAYER_NAME(playerGhost), " AS NOT REMOTELY GHOSTED AS THEY ARE NOW A PARTICIPANT")
									
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
					
	ENDREPEAT
ENDPROC

//PROC STORE_CLIENT_FINISHING_POS(ServerBroadcastData &serverBDPassed, PlayerBroadcastData &playerBDPassed[])
//
//	GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()) = GET_PLAYER_POSITION(serverBDPassed, LocalPlayer)
//	
//	PRINTLN("STORE_CLIENT_FINISHING_POS = ", GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()))
//ENDPROC

FUNC BOOL IS_PLAYER_READY_TO_RESPAWN_FROM_DEATH(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	// If one player has finished the deathmatch, move on, we want to be alive for results
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
		#IF IS_DEBUG_BUILD NET_PRINT_TIME() NET_PRINT_STRINGS("----------> IS_PLAYER_READY_TO_RESPAWN_FROM_DEATH, HAS_DEATHMATCH_FINISHED()", tl31ScriptName) NET_NL() #ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Respawn timer expired
//	IF (dmVarsPassed.iSpectatorTimeCurrent - dmVarsPassed.iSpectatorTimeStart)  >= GET_DM_RESPAWN_TIME()
	IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeRespawning, GET_DM_RESPAWN_TIME())
		#IF IS_DEBUG_BUILD NET_PRINT_TIME() NET_PRINT_STRINGS("----------> IS_PLAYER_READY_TO_RESPAWN_FROM_DEATH, RESPAWN_TIME_EXPIRED", tl31ScriptName) NET_NL() #ENDIF
		
		RETURN TRUE
	ENDIF

//	// Debug skip
//	#IF IS_DEBUG_BUILD
//	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip")
//		NET_PRINT_TIME() NET_PRINT_STRINGS("----------> IS_PLAYER_READY_TO_RESPAWN_FROM_DEATH, PLAYER_J_SKIPPED", tl31ScriptName) NET_NL()
//
//	
//		RETURN TRUE
//	ENDIF
//	#ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_HEALTHBARS_ON(ServerBroadcastData &serverBDpassed)
	
	IF serverBDPassed.iHealthBar = DM_HEALTH_BARS_ON
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DM_PED_DENSITY()
//	SWITCH GlobalServerBD_DM.iPeds
//		CASE PEDS_OFF 	RETURN 0.1 	BREAK
//		CASE PEDS_LOW 	RETURN 0.2 	BREAK
//		CASE PEDS_MED 	RETURN 0.5 	BREAK
//		CASE PEDS_HIGH	RETURN NETWORK_PED_DENSITY	BREAK
//	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_TRAFFIC_DENSITY_FOR_DM(ServerBroadcastData &serverBDpassed, BOOL bDmStarted = TRUE)

	IF bDmStarted = FALSE
	
		RETURN 0.0
	ENDIF

	SWITCH serverBDPassed.iTraffic
		CASE TRAFFIC_OFF
			RETURN 0.0
		CASE TRAFFIC_ON	
			RETURN NETWORK_VEHICLE_DENSITY
	ENDSWITCH
	
	RETURN 0.0
ENDFUNC

FUNC BOOL IS_TRAFFIC_DISABLED_FOR_DM(ServerBroadcastData &serverBDpassed)
	RETURN (serverBDPassed.iTraffic = TRAFFIC_OFF)
ENDFUNC

FUNC FLOAT GET_PARKED_TRAFFIC_DENSITY_FOR_DM(ServerBroadcastData &serverBDpassed, BOOL bDmStarted = TRUE)

	IF bDmStarted = FALSE
	
		RETURN 0.0
	ENDIF

	IF serverBDPassed.iTraffic = TRAFFIC_ON	
		RETURN NETWORK_VEHICLE_DENSITY
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC CLEAR_RESPAWN_HELP()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DM_VEH_RSP")
		CLEAR_HELP(TRUE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_FORCE_A_RESPAWN_WARP()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Force_Manual_Respawn_For_Undriveable_Veh_DM)
	AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
	AND bLocalPlayerPedOK
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
			IF NOT IS_VEHICLE_DRIVEABLE(vehPlayer)
				PRINTLN("[LM][SHOULD_FORCE_A_RESPAWN_WARP] - Returning TRUE - Veh not driveable.")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_FORCED_WARP(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed )
	BOOL bStartWarp
	
	// Don't allow respawn until race started
	IF NOT HAS_DM_STARTED(serverBDpassed)
	OR (NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed) AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Manual_Respawn_For_Any_DM))
	OR IS_PHONE_ONSCREEN()
	OR HAS_DEATHMATCH_FINISHED(serverBDpassed)
	OR serverBDpassed.iPartTagged = iLocalPart
	OR (CONTENT_IS_USING_ARENA() AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Manual_Respawn_For_Any_DM))
	OR IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
		EXIT
	ENDIF
	
	IF IS_NON_ARENA_KING_OF_THE_HILL()
		IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
			VEHICLE_INDEX viCurrentVeh = GET_VEHICLE_PED_IS_USING(LocalPlayerPed)
			
			IF viCurrentVeh = dmVarsPassed.dmVehIndex
				// All good to manually respawn
			ELSE
				PRINTLN("[VEH] MAINTAIN_FORCED_WARP - Exiting because we're in the wrong veh")
				EXIT
			ENDIF
		ELSE
			PRINTLN("[VEH] MAINTAIN_FORCED_WARP - Exiting because we're not in a vehicle")
			EXIT
		ENDIF
	ENDIF
	
	IF bLocalPlayerOK
		SET_PED_RESET_FLAG(LocalPlayerPed, PRF_OnlyExitVehicleOnButtonRelease, TRUE)
	ENDIF
	
	bStartWarp = SHOULD_FORCE_A_RESPAWN_WARP()
	HUDORDER eHUDOrder = HUDORDER_EIGHTHBOTTOM
	
	IF IS_KING_OF_THE_HILL()
		eHUDOrder = HUDORDER_SECONDBOTTOM
	ENDIF

	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_TRIANGLE_WARP) 
	AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		OR IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_EXIT)			
			IF DOES_ENTITY_EXIST(LocalPlayerPed)
				IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
					IF NOT IS_PED_JACKING(LocalPlayerPed)
					AND NOT IS_PED_BEING_JACKED(LocalPlayerPed)
						
						IF HAS_NET_TIMER_STARTED(dmVarsPassed.timeForceWarp)
						AND NOT IS_NON_ARENA_KING_OF_THE_HILL()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						ENDIF
					
						START_NET_TIMER(dmVarsPassed.timeForceWarp)
						
						IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeForceWarp, FORCE_WARP_DELAY)
									
							INT ms = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeForceWarp.Timer)
							IF NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
								DRAW_GENERIC_METER(ms - FORCE_WARP_DELAY, TRIANGLE_RESPAWN_TIME, "TRI_WARP", HUD_COLOUR_RED, 0, eHUDOrder, -1, -1, FALSE, TRUE)
								IF playerBDPassed[iLocalPart].iScore > 0
								AND NOT IS_KING_OF_THE_HILL()
									PRINT_HELP_FOREVER("DM_VEH_RSP")
								ENDIF
							ENDIF
							IF (ms - FORCE_WARP_DELAY) >= TRIANGLE_RESPAWN_TIME
								bStartWarp = TRUE								
							ENDIF
						ENDIF
					ELSE
						CLEAR_RESPAWN_HELP()
						RESET_NET_TIMER(dmVarsPassed.timeForceWarp)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			CLEAR_RESPAWN_HELP()
			RESET_NET_TIMER(dmVarsPassed.timeForceWarp)
		ENDIF
	ENDIF
	
	IF bStartWarp
		SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_TRIANGLE_WARP)		
	ENDIF
	
	STOP_JACKING_IF_OTHER_PLAYER_IS_WARPING()	
ENDPROC

PROC PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END(PlayerBroadcastData &playerBDPassed[])
	
	IF NOT IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
	OR g_bMissionEnding
	OR g_bCelebrationScreenIsActive
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
	OR NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS2, 12)
		EXIT
	ENDIF
	
	INT iTotalTeamKills[FMMC_MAX_TEAMS]
	INT iTotalTeamDamageDealt[FMMC_MAX_TEAMS]
	
	INT iWinningTeam = 0
	INT iWinningPlayer = 0
	
	INT iPlayer = 0
	FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1
				
		PLAYER_INDEX piPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		
		IF NETWORK_IS_PLAYER_ACTIVE(piPlayer)
			PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
			
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(piPlayer)].iMissionDataBitSetTwo, ciMISSION_DATA_TWO_SPECIAL_SPECTATOR_ENABLED)
			AND NOT IS_PED_INJURED(pedPlayer)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Player Part: ", iPlayer)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Player Kills: ", playerBDPassed[iPlayer].iKills)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Player Damage: ", playerBDPassed[iPlayer].fDamageDealt)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] ---------------------------------------------")
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Winner Player: ", iWinningPlayer)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Winner Kills: ", playerBDPassed[iWinningPlayer].iKills)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Winner Damage: ", playerBDPassed[iWinningPlayer].fDamageDealt)
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] ---------------------------------------------")
				
				IF playerBDPassed[iPlayer].iKills > playerBDPassed[iWinningPlayer].iKills			// Kills
					PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] (Kills) New iWinningPlayer: ", iWinningPlayer)
					iWinningPlayer = iPlayer
				ELIF playerBDPassed[iPlayer].fDamageDealt > playerBDPassed[iWinningPlayer].fDamageDealt		// Damage Dealt
					PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] (Damage) New iWinningPlayer: ", iWinningPlayer)
					iWinningPlayer = iPlayer
				ENDIF
				
				INT iTeam = GET_PLAYER_TEAM(piPlayer)
				IF iTeam > -1
				AND iTeam < FMMC_MAX_TEAMS
					iTotalTeamDamageDealt[iTeam] += ROUND(playerBDPassed[iPlayer].fDamageDealt)
					iTotalTeamKills[iTeam] += playerBDPassed[iPlayer].iKills
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_TEAM_DEATHMATCH()
		INT iTeam = 0
		FOR iTeam = 0 TO 1
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Team: ", iTeam)
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Team Kills: ", iTotalTeamKills[iTeam])
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Team Damage: ", iTotalTeamDamageDealt[iTeam])
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] ---------------------------------------------")
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Team Winner: ", iWinningTeam)
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Team Winner Kills: ", iTotalTeamKills[iWinningTeam])
			PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] Team Winner Damage: ", iTotalTeamDamageDealt[iWinningTeam])
			
			IF iTotalTeamKills[iTeam] > iTotalTeamKills[iWinningTeam]
				iWinningTeam = iTeam
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] (Kills) New iWinningTeam: ", iWinningTeam)
			ELIF iTotalTeamDamageDealt[iTeam] = iTotalTeamDamageDealt[iWinningTeam]
				iWinningTeam = iTeam
				PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] (Damage) New iWinningTeam: ", iWinningTeam)
			ENDIF
		ENDFOR	
	ENDIF
	
	IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	AND NOT IS_LOCAL_PLAYER_IN_ARENA_SPECTATOR_BOX()
		
		BOOL bShouldExplode		
		IF IS_TEAM_DEATHMATCH()
			IF iWinningTeam > -1
				IF iWinningTeam != GET_PLAYER_TEAM(LocalPlayer)
					bShouldExplode = TRUE
					PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] bShouldExplode = TRUE | Team: ", GET_PLAYER_TEAM(LocalPlayer), " != iWinningTeam: ", iWinningTeam)
				ENDIF
			ENDIF
		ELSE
			IF iWinningPlayer > -1
				IF iWinningPlayer != NATIVE_TO_INT(LocalPlayer)
					bShouldExplode = TRUE
					PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] bShouldExplode = TRUE | Player: ", NATIVE_TO_INT(LocalPlayer), " != iWinningPlayer: ", iWinningPlayer)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("[LM][PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END] GET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS: ", GET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS(), " bShouldExplode: ", bShouldExplode, " IS_TEAM_DEATHMATCH: ", IS_TEAM_DEATHMATCH())
		
		IF g_iInstancedContentTimeLeft <= 3000
		AND g_iInstancedContentTimeLeft > 0
			IF bShouldExplode
			
				PED_INDEX pedPlayer = LocalPlayerPed
				VEHICLE_INDEX vehPlayerIsIn
				
				IF bLocalPlayerPedOK
					IF IS_PED_IN_ANY_VEHICLE(pedPlayer)
						vehPlayerIsIn = GET_VEHICLE_PED_IS_IN(pedPlayer)
						
						IF NOT IS_ENTITY_DEAD(vehPlayerIsIn)
							SET_ENTITY_INVINCIBLE(PedPlayer, FALSE)
							SET_ENTITY_PROOFS(PedPlayer, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
							NETWORK_EXPLODE_VEHICLE(VehPlayerIsIN, TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC TRACK_KILLS_DEATHS_DAMAGE(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[]#IF IS_DEBUG_BUILD,SHARED_DM_VARIABLES &dmVarsPassed#ENDIF)

	INT iScoreCap = GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam)
	BOOL bDMEnded = SERVER_CHECKS_HAS_DM_ENDED(serverBDpassed, TRUE #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF)

	IF NOT bDMEnded
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		IF playerBDPassed[iLocalPart].iScore != g_iMyDMScore
		
			IF g_iMyDMScore >= 0
			OR NOT IS_THIS_LEGACY_DM_CONTENT()
				playerBDPassed[iLocalPart].iScore = g_iMyDMScore
				IF playerBDPassed[iLocalPart].iScore > iScoreCap
					playerBDPassed[iLocalPart].iScore = iScoreCap
				ENDIF
				PRINTLN("TRACK_KILLS_DEATHS_DAMAGE iScore = ", playerBDPassed[iLocalPart].iScore)
			ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT bDMEnded
		IF playerBDPassed[iLocalPart].iKills != g_iMyAmountOfKills
			playerBDPassed[iLocalPart].iKills = g_iMyAmountOfKills
			PRINTLN("TRACK_KILLS_DEATHS_DAMAGE iKills = ", playerBDPassed[iLocalPart].iKills)
						
			IF SHOULD_PROGRESS_DM_AWARDS()
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_TOTALKILLS, 1)
			ENDIF
		ENDIF	
		IF playerBDPassed[iLocalPart].iDeaths != g_iMyAmountOfDeaths
			playerBDPassed[iLocalPart].iDeaths = g_iMyAmountOfDeaths
			PRINTLN("TRACK_KILLS_DEATHS_DAMAGE iDeaths = ", playerBDPassed[iLocalPart].iDeaths)
			
			IF SHOULD_PROGRESS_DM_AWARDS()
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_DM_CURRENT_DEATHS, DEATHMATCH_ONE_POINT)
			ENDIF
			
			IF IS_ARENA_WARS_JOB_ELIMINATION_BASED()
				g_sArena_Telemetry_data.m_timeEliminated = GET_CLOUD_TIME_AS_INT()
				PRINTLN("[ARENA][TEL] - DM - Time eliminated - ", g_sArena_Telemetry_data.m_timeEliminated)
			ENDIF
		ENDIF
		IF playerBDPassed[iLocalPart].iSuicides != g_iMyAmountOfSuicides
			playerBDPassed[iLocalPart].iSuicides = g_iMyAmountOfSuicides
			PRINTLN("TRACK_KILLS_DEATHS_DAMAGE iSuicides = ", playerBDPassed[iLocalPart].iSuicides)
		ENDIF
		IF playerBDPassed[iLocalPart].fDamageTaken != g_f_MyDamageTaken
			IF g_f_MyDamageTaken >= 0
				playerBDPassed[iLocalPart].fDamageTaken = g_f_MyDamageTaken
				IF IS_ARENA_WARS_JOB(TRUE)
					g_sArena_Telemetry_data.m_damageReceived = ROUND(g_f_MyDamageTaken)
					PRINTLN("[ARENA][TEL] - DM Damage taken: ", g_sArena_Telemetry_data.m_damageReceived)
				ENDIF
				PRINTLN("TRACK_KILLS_DEATHS_DAMAGE fDamageTaken = ", playerBDPassed[iLocalPart].fDamageTaken)
			ENDIF
		ENDIF
		IF playerBDPassed[iLocalPart].fDamageDealt != g_f_MyDamageDealt
			IF g_f_MyDamageDealt >= 0
				playerBDPassed[iLocalPart].fDamageDealt = g_f_MyDamageDealt
				IF IS_ARENA_WARS_JOB(TRUE)
					g_sArena_Telemetry_data.m_damageDealt = ROUND(g_f_MyDamageDealt)
					PRINTLN("[ARENA][TEL] - DM Damage dealt: ", g_sArena_Telemetry_data.m_damageDealt)
				ENDIF
				PRINTLN("TRACK_KILLS_DEATHS_DAMAGE fDamageDealt = ", playerBDPassed[iLocalPart].fDamageDealt)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_LBD_CAM(SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_LEADERBOARD_CAM_READY()
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			IF IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMBS_DONE) //This is set after the celebration screens are complete, we don't want to freeze the game until after then.
				//REQUEST_LEADERBOARD_CAM()
				PRINTLN("DO_LBD_CAM() ")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CAP_DISTANCE(SHARED_DM_VARIABLES &dmVarsPassed)
	IF dmVarsPassed.fOldFurthestTargetDist < 50
		dmVarsPassed.fOldFurthestTargetDist = 50
	ENDIF
	IF dmVarsPassed.fOldFurthestTargetDist > 400
		dmVarsPassed.fOldFurthestTargetDist = 400
	ENDIF
ENDPROC

PROC CONTROL_RADAR_ZOOM(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

	FLOAT fZoomLevel
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	#IF IS_DEBUG_BUILD
	OR dmVarsPassed.debugDmVars.bDisableZoom
	#ENDIF
		
		EXIT
	ENDIF	
	
	IF dmVarsPassed.fOldFurthestTargetDist > dmVarsPassed.fFurthestTargetDist
		IF (dmVarsPassed.fOldFurthestTargetDist - dmVarsPassed.fFurthestTargetDist) > 5
			dmVarsPassed.fOldFurthestTargetDist = (dmVarsPassed.fOldFurthestTargetDist -cfRADAR_ZOOM_SPEED)
		ENDIF
	ELSE
		IF (dmVarsPassed.fFurthestTargetDist - dmVarsPassed.fOldFurthestTargetDist) > 5
			dmVarsPassed.fOldFurthestTargetDist = (dmVarsPassed.fOldFurthestTargetDist + cfRADAR_ZOOM_SPEED)
		ENDIF
	ENDIF
	CAP_DISTANCE(dmVarsPassed)
	fZoomLevel = (dmVarsPassed.fOldFurthestTargetDist/cfRADAR_ZOOM_ADJUST)	
	
	// Cap the zoom level
	IF (fZoomLevel >= cfRADAR_ZOOM_CAP)
		fZoomLevel = cfRADAR_ZOOM_CAP
	ELIF (fZoomLevel < cfRADAR_ZOOM_CAP_MIN)
		fZoomLevel = cfRADAR_ZOOM_CAP_MIN
	ENDIF
	
	// Allow debug tweak of radar zoom 
	// Script/ FreeMode/Les Widgets/Radar Zoom /Radar Zoom 
	#IF IS_DEBUG_BUILD
	IF (f_i_RadarZoom > -1.0)
		SET_RADAR_ZOOM_TO_DISTANCE(f_i_RadarZoom)
	ELSE
	#ENDIF
	
	IF DO_RADAR_ZOOM()
		SET_RADAR_ZOOM_TO_DISTANCE(fZoomLevel)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

PROC HANDLE_INTERACTION_MENU()
	IF WAS_KILL_YOURSELF_USED_IN_DM()
		PRINTLN("HANDLE_INTERACTION_MENU")
		DEDUCT_DEATHMATCH_SCORE()
		CLEAR_KILL_YOURSELF_USED_IN_DM()
	ENDIF
ENDPROC

PROC BLOCK_REWARD_WEAPONS_DURING_LOCKED_DMS()
	IF IS_JOB_FORCED_WEAPON_ONLY()
	OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
		DISABLE_PLAYER_VEHICLE_REWARDS(LocalPlayer)
	ENDIF	
ENDPROC

PROC DO_OUTROS(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_OUTRO_DONE)
			IF GET_CLIENT_MISSION_STAGE(playerBDPassed) > CLIENT_MISSION_STAGE_RESPAWN
				ANIM_OUTRO_PREP(dmVarsPassed.DMIntroData)
				REQUEST_ANIMATIONS_FOR_INTRO(dmVarsPassed.DMIntroData)
			ENDIF
		ENDIF
		IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOLS2_OUTRO_OK_TO_DO)
			IF HAS_STARTED_NEW_OUTRO(dmVarsPassed.DMIntroData)
				PRINTLN("DO_OUTROS, YES")
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_OUTRO_OK_TO_DO)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOSS_V_BOSS_DM_CLIENT()
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		IF NOT GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_BOSS_QUIT_BOSSVBOSS_DM)
			IF GB_IS_PLAYER_MEMBER_OF_A_GANG(LocalPlayer)
				IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(LocalPlayer)
					IF GB_GET_LOCAL_PLAYER_GANG_BOSS() = INVALID_PLAYER_INDEX()
						CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] I NO LONGER HAVE A BOSS! ")
						GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_BOSS_QUIT_BOSSVBOSS_DM)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC SERVER_STORE_CELEB_WINNER_VEHICLE(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	UNUSED_PARAMETER(playerBDPassed)

	VEHICLE_INDEX vehId
		
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CELEB_VEH) 
	AND serverBDpassed.playerDMWinner <> INVALID_PLAYER_INDEX()
		
		// url:bugstar:5497575 | Freemode seemed to be sending the player into spectator on the same frame we start coming into here.
		IF NETWORK_IS_IN_SPECTATOR_MODE()
			NETWORK_SET_IN_SPECTATOR_MODE(FALSE, LocalPlayerPed)
			FORCE_PlAYER_BACK_INTO_PLAYING_STATE(FALSE, TRUE)
			PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 0 - Host was sent to spectator. Forcing out!")
		ENDIF
		
		PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 1 - playerMatchWinner = ", GET_PLAYER_NAME(serverBDpassed.playerDMWinner))
	
		PED_INDEX playerPed
		playerPed = GET_PLAYER_PED(serverBDpassed.playerDMWinner)
		NETWORK_INDEX netVehId
	
		IF DOES_ENTITY_EXIST(playerPed)
		AND NOT IS_ENTITY_DEAD(playerPed)
		
			PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 2 -  " )
			
			IF IS_PED_IN_ANY_VEHICLE(playerPed, TRUE)
			
				PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 3 -  " )
			
				IF DOES_ENTITY_EXIST(playerPed)
				AND NOT IS_ENTITY_DEAD(playerPed)
				
					PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 4 -  ")
				
					vehId = GET_VEHICLE_PED_IS_IN(playerPed, TRUE)
					
					IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehId)
					
						PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 5 -  ")
					
//						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
						IF TAKE_CONTROL_OF_ENTITY(vehId)
							SET_ENTITY_AS_MISSION_ENTITY(vehId,FALSE,TRUE)
							PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN, SET_ENTITY_AS_MISSION_ENTITY 1 ")
						ELSE
							IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehId)
								netVehId = VEH_TO_NET(vehId)
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehId)
								PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN, NETWORK_REQUEST_CONTROL_OF_NETWORK_ID 1 ")
							ENDIF	
						ENDIF
					ENDIF	
					
					IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehId)
					
						PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 6 -  ")
						
						netVehId = VEH_TO_NET(vehId)
					
//						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
						IF TAKE_CONTROL_OF_ENTITY(vehId)
						
							SET_ENTITY_AS_MISSION_ENTITY(vehId,FALSE,TRUE)
//							NETWORK_REGISTER_ENTITY_AS_NETWORKED(vehId)
							
							serverBDPassed.vehWinner = netVehId
							PRINTLN("[3399057][SERVER_STORE_CELEB_WINNER_VEHICLE] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN, = ", NATIVE_TO_INT(serverBDPassed.vehWinner), " name = ", GET_PLAYER_NAME(serverBDpassed.playerDMWinner))
							SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBDPassed.vehWinner, TRUE)
							
							SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_CELEB_VEH)
	
						ELSE
							IF NOT bIsAnySpectator
								NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehId)
								PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN, NETWORK_REQUEST_CONTROL_OF_NETWORK_ID 2 ")
							ENDIF
						ENDIF
					ELSE
						PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 7 -  ")
						IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehId)
							PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 7a -  ")
//							IF NETWORK_HAS_CONTROL_OF_ENTITY(vehId)
							IF TAKE_CONTROL_OF_ENTITY(vehId)
								PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 7b -  ")
								SET_ENTITY_AS_MISSION_ENTITY(vehId,FALSE,TRUE)
								NETWORK_REGISTER_ENTITY_AS_NETWORKED(vehId)
								PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN, NETWORK_REGISTER_ENTITY_AS_NETWORKED ")
							ELSE
								IF NOT bIsAnySpectator
									PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 7c -  ")
									
									IF NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(vehId)
										PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] 7d -  ")
										netVehId = VEH_TO_NET(vehId)
										NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netVehId)
										PRINTLN("[SERVER_STORE_CELEB_WINNER_VEHICLE] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN, NETWORK_REQUEST_CONTROL_OF_NETWORK_ID 3 ")
									ENDIF	
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)	
	VEHICLE_INDEX vehId
	IF serverBDpassed.playerDMWinner <> INVALID_PLAYER_INDEX()
		IF dmVarsPassed.iWinningParticipantCopyForCelebrationScreen != NATIVE_TO_INT(serverBDpassed.playerDMWinner)
			PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - new winning participant. serverBDpassed.playerDMWinner = ", GET_PLAYER_NAME(serverBDpassed.playerDMWinner))
			IF IS_NET_PLAYER_OK(serverBDpassed.playerDMWinner, FALSE)
				PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - new winning participant is active.")
				PED_INDEX saveVehPed = GET_PLAYER_PED(serverBDpassed.playerDMWinner)
				IF DOES_ENTITY_EXIST(saveVehPed)
					PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - new winning participant ped exists.")
					IF NOT IS_ENTITY_DEAD(saveVehPed)
						PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - new winning participant ped is alive.", NATIVE_TO_INT(GET_NETWORK_TIME()))
						IF NETWORK_DOES_NETWORK_ID_EXIST(serverBDPassed.vehWinner)
							vehId = NET_TO_VEH(serverBDPassed.vehWinner)
							PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID YES ")
							IF NOT IS_ENTITY_DEAD(vehId)
								PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - NOT IS_ENTITY_DEAD(vehId) ")
								dmVarsPassed.winnerVehicle = vehId
								PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - dmVarsPassed.winnerVehicle = ", NATIVE_TO_INT(dmVarsPassed.winnerVehicle))
								VEHICLE_SETUP_STRUCT_MP tempVehicleSetupMP
								dmVarsPassed.sCelebrationData.vehicleSetupMp = tempVehicleSetupMP
								PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - dmVarsPassed.sCelebrationData.vehicleSetupMp = tempVehicleSetupMP")
								GET_VEHICLE_SETUP_MP(dmVarsPassed.winnerVehicle, dmVarsPassed.sCelebrationData.vehicleSetupMp)
								PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - GET_VEHICLE_SETUP_MP")
								dmVarsPassed.iWinningParticipantCopyForCelebrationScreen = NATIVE_TO_INT(serverBDpassed.playerDMWinner)
								PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - save race winner car - dmVarsPassed.iWinningParticipantCopyForCelebrationScreen = ", dmVarsPassed.iWinningParticipantCopyForCelebrationScreen)
								PRINTLN("[NETCELEBRATIONS] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - GET_PLAYER_NAME = ", GET_PLAYER_NAME(serverBDpassed.playerDMWinner))
								
								PRINTLN("[3399057] - MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN - CLIENT_STORED_VEH_SUCCESS = ", NATIVE_TO_INT(GET_NETWORK_TIME())) 
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAVE_CELEB_VEHICLE(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_CELEB_GRABBED_VEH)
		IF serverBDpassed.playerDMWinner <> INVALID_PLAYER_INDEX()
			IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.stCelebVehicle)
				START_NET_TIMER(dmVarsPassed.stCelebVehicle)
			ELSE
				IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.stCelebVehicle, 500)
					IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_CELEB_GRABBED_VEH)
						PRINTLN("[NETCELEBRATIONS] SAVE_CELEB_VEHICLE- save race winner car - we have a race winner, doing save.")
						IF DOES_ENTITY_EXIST(dmVarsPassed.winnerVehicle)
							PRINTLN("[NETCELEBRATIONS] SAVE_CELEB_VEHICLE- save race winner car - dmVarsPassed.winnerVehicle exists.")
							GET_VEHICLE_SETUP_MP(dmVarsPassed.winnerVehicle, dmVarsPassed.sCelebrationData.vehicleSetupMp)
							SET_BIT(dmVarsPassed.iDmBools2, DM_CELEB_GRABBED_VEH)
							PRINTLN("[NETCELEBRATIONS] SAVE_CELEB_VEHICLE- save race winner car - completed save, setting bit BS3_SAVED_CELEB_VEHICLE.")
						ELSE
							PRINTLN("[NETCELEBRATIONS] SAVE_CELEB_VEHICLE- save race winner car - dmVarsPassed.winnerVehicle does not exist.")
							SET_BIT(dmVarsPassed.iDmBools2, DM_CELEB_GRABBED_VEH)
							PRINTLN("[NETCELEBRATIONS] SAVE_CELEB_VEHICLE- save race winner car - cannot complete save, setting bit BS3_SAVED_CELEB_VEHICLE so we can continue.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PED_IN_ZONE(PED_INDEX tempPed, INT iZone, BOOL b3DCheck = TRUE, BOOL bCheckZ = FALSE)
	
	IF NOT IS_PED_INJURED(tempPed)
		IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iZoneBS2, ciFMMC_ZONEBS2_TRIGGER_ZONE_WHEN_OUTSIDE_OF_IT)
			RETURN NOT IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(tempPed), iZone, b3DCheck,FALSE, bCheckZ)
		ELSE
			RETURN IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(tempPed), iZone, b3DCheck,FALSE, bCheckZ)
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SELECT_NEXT_WAVE_DAMPING_SCALER(SHARED_DM_VARIABLES &dmVarsPassed, PED_INDEX pedTemp)
	
	INT iZone = -1
	INT iZonesToConsiderBS
	
	INT i
	FLOAT fSmallestLength = 99999
	VECTOR vLengthTemp
	FLOAT fTempLength
	INT iTempDampingZoneBitset = dmVarsPassed.iWaveDampingZoneBitset
	
	FOR i = 0 TO (FMMC_MAX_NUM_ZONES - 1)
		IF IS_BIT_SET(iTempDampingZoneBitset, i)
			//pick ones of smallest length
			//from those, pick one w closest centrepoint
			
			vLengthTemp = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1] - g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
			vLengthTemp.z = 0
			
			fTempLength = VMAG(vLengthTemp)
			
			IF fTempLength < (fSmallestLength * 0.9)
				fSmallestLength = fTempLength
				iZonesToConsiderBS = 0
				SET_BIT(iZonesToConsiderBS, i)
				iZone = i
			ELIF (fTempLength >= (fSmallestLength * 0.9))
			AND (fTempLength <= (fSmallestLength * 1.1))
				SET_BIT(iZonesToConsiderBS, i)
				iZone = -1
			ENDIF
			
			CLEAR_BIT(iTempDampingZoneBitset, i)
		ENDIF
		
		IF iTempDampingZoneBitset = 0
			i = FMMC_MAX_NUM_ZONES //Break out, we've checked all the zones we need to!
		ENDIF
	ENDFOR
	
	IF iZone = -1
	AND iZonesToConsiderBS > 0
		//Have more than one zone to consider, find the one with the closest centrepoint:
		
		VECTOR vPlayer = GET_ENTITY_COORDS(pedTemp)
		
		fSmallestLength = 999999999
		
		FOR i = 0 TO (FMMC_MAX_NUM_ZONES - 1)
			IF IS_BIT_SET(iZonesToConsiderBS, i)
				
				fTempLength = VDIST2(vPlayer, ((g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]) * 0.5) )
				
				IF fTempLength < fSmallestLength
					fSmallestLength = fTempLength
					iZone = i
				ENDIF
				
				CLEAR_BIT(iZonesToConsiderBS, i)
			ENDIF
			
			IF iZonesToConsiderBS = 0
				i = FMMC_MAX_NUM_ZONES //Break out, we've checked all the zones we need to!
			ENDIF
		ENDFOR
		
	ENDIF
	
	PRINTLN("[RC] SELECT_NEXT_WAVE_DAMPING_SCALER - next zone found is ",iZone)
	
	IF iZone != -1
		IF GET_DEEP_OCEAN_SCALER() != g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2
			PRINTLN("[RC] SELECT_NEXT_WAVE_DAMPING_SCALER - calling SET_DEEP_OCEAN_SCALER for ",iZone," with value ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
			SET_DEEP_OCEAN_SCALER(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].fZoneValue2)
		ENDIF
	ELSE
		IF GET_DEEP_OCEAN_SCALER() != 1.0
			PRINTLN("[RC] SELECT_NEXT_WAVE_DAMPING_SCALER - Calling RESET_DEEP_OCEAN_SCALER")
			RESET_DEEP_OCEAN_SCALER()
		ENDIF
	ENDIF
	
ENDPROC

PROC EVERY_FRAME_ZONE_PROCESSING(SHARED_DM_VARIABLES &dmVarsPassed)
	INT iZone
	FOR iZone = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].iType
			CASE ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
				PROCESS_PLAYER_EXPLODE_ZONE_EVERY_FRAME_HUD(dmVarsPassed.stExplosionZoneTimer[iZone], iZone)
			BREAK
		ENDSWITCH
	ENDFOR
ENDPROC

FUNC BOOL SHOULD_USE_FULL_NUMBER_OF_ZONE_EXPLOSIONS()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_ZONES(SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, PlayerBroadcastData &playerBDPassed[])
	
	IF HAS_DEATHMATCH_FINISHED(serverBDPassed)
		EXIT
	ENDIF
	
	IF (dmVarsPassed.iLocalZoneCheckBitset > 0)		
		
		IF dmVarsPassed.iZoneStaggeredLoop >= FMMC_MAX_NUM_ZONES
			dmVarsPassed.iZoneStaggeredLoop = 0
		ENDIF
		
		PED_INDEX pedTemp
		
		INT iObj
		OBJECT_INDEX tempObj
		
		SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[dmVarsPassed.iZoneStaggeredLoop].iType	
			CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING // We'll only get here if fZoneValue2 != 1.0
			
				pedTemp = GET_SPECTATOR_CURRENT_FOCUS_PED()
				IF pedTemp = NULL
					pedTemp = LocalPlayerPed
				ENDIF
									
				IF IS_PED_IN_ZONE(pedTemp, dmVarsPassed.iZoneStaggeredLoop)
					
					//don't want to keep switching between this and other zones we're in! use a bitset!!
					IF NOT IS_BIT_SET(dmVarsPassed.iWaveDampingZoneBitset, dmVarsPassed.iZoneStaggeredLoop)
						IF GET_DEEP_OCEAN_SCALER() != g_FMMC_STRUCT_ENTITIES.sPlacedZones[dmVarsPassed.iZoneStaggeredLoop].fZoneValue2
							PRINTLN("[DM] PROCESS_ZONES, Zone ", dmVarsPassed.iZoneStaggeredLoop, ", Wave Amounts - Calling SET_DEEP_OCEAN_SCALER w value ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[dmVarsPassed.iZoneStaggeredLoop].fZoneValue2)
							SET_DEEP_OCEAN_SCALER(g_FMMC_STRUCT_ENTITIES.sPlacedZones[dmVarsPassed.iZoneStaggeredLoop].fZoneValue2)
						ENDIF
						DISABLE_IN_WATER_PTFX(TRUE)
						SET_BIT(dmVarsPassed.iWaveDampingZoneBitset, dmVarsPassed.iZoneStaggeredLoop)
					ENDIF
				ELSE
					IF IS_BIT_SET(dmVarsPassed.iWaveDampingZoneBitset, dmVarsPassed.iZoneStaggeredLoop)
						PRINTLN("[DM] PROCESS_ZONES, Zone ", dmVarsPassed.iZoneStaggeredLoop, ", Wave Amounts - No longer in this zone")
						
						CLEAR_BIT(dmVarsPassed.iWaveDampingZoneBitset, dmVarsPassed.iZoneStaggeredLoop)
						
						IF dmVarsPassed.iWaveDampingZoneBitset = 0
							DISABLE_IN_WATER_PTFX(FALSE)
						ENDIF
						
						IF dmVarsPassed.iWaveDampingZoneBitset > 0
							IF GET_DEEP_OCEAN_SCALER() = g_FMMC_STRUCT_ENTITIES.sPlacedZones[dmVarsPassed.iZoneStaggeredLoop].fZoneValue2 //If this one is the zone running right now
								SELECT_NEXT_WAVE_DAMPING_SCALER(dmVarsPassed, pedTemp)
							ENDIF
						ELSE
							IF GET_DEEP_OCEAN_SCALER() != 1.0
								PRINTLN("[DM] PROCESS_ZONES, Zone ", dmVarsPassed.iZoneStaggeredLoop, ", Wave Amounts - Calling RESET_DEEP_OCEAN_SCALER")
								RESET_DEEP_OCEAN_SCALER()
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
				IF bLocalPlayerPedOK
				AND IS_PED_IN_ZONE(LocalPlayerPed, dmVarsPassed.iZoneStaggeredLoop, TRUE, TRUE)
				AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				AND NOT bIsSCTV
				AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
				AND GET_CLIENT_MISSION_STAGE(playerBDPassed) != CLIENT_MISSION_STAGE_SPECTATOR
					PROCESS_PLAYER_EXPLODE_ZONE(dmVarsPassed.stExplosionZoneTimer[dmVarsPassed.iZoneStaggeredLoop], g_FMMC_STRUCT_ENTITIES.sPlacedZones[dmVarsPassed.iZoneStaggeredLoop].fZoneValue, SHOULD_USE_FULL_NUMBER_OF_ZONE_EXPLOSIONS())
				ELSE
					IF HAS_NET_TIMER_STARTED(dmVarsPassed.stExplosionZoneTimer[dmVarsPassed.iZoneStaggeredLoop])
						CLEAR_ALL_BIG_MESSAGES()
						RESET_NET_TIMER(dmVarsPassed.stExplosionZoneTimer[dmVarsPassed.iZoneStaggeredLoop])
					ENDIF
				ENDIF
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN
				FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niObject[iObj])
						tempObj = NET_TO_OBJ(serverFMMCPassed.niObject[iObj])
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
								PRINTLN("PROCESS_ZONES - ciFMMC_ZONE_TYPE__OBJECT_RESPAWN || Obj - ", iObj, " Zone - ", dmVarsPassed.iZoneStaggeredLoop)
								
								IF NOT IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(tempObj), dmVarsPassed.iZoneStaggeredLoop, TRUE, FALSE, TRUE)
									SET_ENTITY_COORDS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos)
									PRINTLN("PROCESS_ZONES - ciFMMC_ZONE_TYPE__OBJECT_RESPAWN || Moving object ", iObj, " back to its starting position!")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			BREAK
			
			CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN
				FOR iObj = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfObjects - 1
					IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niObject[iObj])
						tempObj = NET_TO_OBJ(serverFMMCPassed.niObject[iObj])
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(tempObj)
							IF NOT IS_ENTITY_ATTACHED_TO_ANY_PED(tempObj)
								PRINTLN("PROCESS_ZONES - ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN || Obj - ", iObj, " Zone - ", dmVarsPassed.iZoneStaggeredLoop)
								
								IF IS_LOCATION_IN_FMMC_ZONE(GET_ENTITY_COORDS(tempObj), dmVarsPassed.iZoneStaggeredLoop, TRUE, FALSE, TRUE)
									SET_ENTITY_COORDS(tempObj, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].vPos)
									PRINTLN("PROCESS_ZONES - ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN || Moving object ", iObj, " back to its starting position!")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			BREAK
		ENDSWITCH
	ENDIF
	
	dmVarsPassed.iZoneStaggeredLoop++
	
	EVERY_FRAME_ZONE_PROCESSING(dmVarsPassed)
	
ENDPROC

/// PURPOSE: This function will find and return the first unused array position in the iWaterCalmingQuad array of water calming quads.
///    If a value of -1 is returned, there are no free entries in the array and we have reached the maximum number of water calming quads.
///    This value is currently set at 8 which is a script limit, but the code limit on the number of quads is also 8.
FUNC INT GET_FREE_WATER_CALMING_QUAD(SHARED_DM_VARIABLES &dmVarsPassed)
	
	INT iQuadNum = -1
	
	INT iQuad
	
	FOR iQuad = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
		IF dmVarsPassed.iWaterCalmingQuad[iQuad] = -1
			iQuadNum = iQuad
			iQuad = FMMC_MAX_WATER_CALMING_QUADS
		ENDIF
	ENDFOR
	
	RETURN iQuadNum
	
ENDFUNC

FUNC BOOL SHOULD_CREATE_ZONE(INT iZone)
	IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[iZone].vPos[0])
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CREATE_ZONES(SHARED_DM_VARIABLES &dmVarsPassed)
	DEBUG_PRINTCALLSTACK()
	INT i
	
	VECTOR vZonePos0 , vZonePos1
	
	FOR i = 0 TO (g_FMMC_STRUCT_ENTITIES.iNumberOfZones-1)
		
		IF NOT IS_BIT_SET(dmVarsPassed.iLocalZoneBitset, i)
		
			BOOL bShouldCreateZone = FALSE // B* 2233648
			bShouldCreateZone = SHOULD_CREATE_ZONE( i )
		
			IF( bShouldCreateZone )
				CPRINTLN( DEBUG_SIMON, "CREATE_ZONES - Creating zone this frame iZone: ", i, " vPos: ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0])
				IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0])
				
					VECTOR vcenter = (g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0] + g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
					vcenter.x = vcenter.x/2
					vcenter.y = vcenter.y/2
					vcenter.z = vcenter.z/2
									
					vZonePos0 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]
					vZonePos1 = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1]
					
					IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iAreaType != ciFMMC_ZONE_SHAPE__SPHERE
						vZonePos0.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0].z - 100.0
						vZonePos1.z = g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1].z + 500.0
					ENDIF
					
					SWITCH g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType
						CASE ciFMMC_ZONE_TYPE__WATER_DAMPENING
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue != 1.0
								IF dmVarsPassed.iZoneWaterCalmingQuadID[i] = -1
									INT iQuad
									iQuad = GET_FREE_WATER_CALMING_QUAD(dmVarsPassed)
									
									IF iQuad > -1
									AND iQuad < FMMC_MAX_WATER_CALMING_QUADS
									AND dmVarsPassed.iWaterCalmingQuad[iQuad] = -1
										dmVarsPassed.iWaterCalmingQuad[iQuad] = ADD_EXTRA_CALMING_QUAD(vZonePos0.x,vZonePos0.y,vZonePos1.x,vZonePos1.y,g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue)
										dmVarsPassed.iZoneWaterCalmingQuadID[i] = iQuad
										PRINTLN("[DM] CREATE_ZONES - Added water calming quad ",i," as iQuad ",iQuad)
										SET_BIT(dmVarsPassed.iLocalZoneBitset, i)
									ELSE
										CASSERTLN(DEBUG_CONTROLLER,"[RC] CREATE_ZONES - Trying to create too many water dampening zones/quads! There is a maximum of 8.")
										PRINTLN("[DM] CREATE_ZONES - Trying to create too many water dampening zones/quads! There is a maximum of 8.")
									ENDIF
								ELSE
									PRINTLN("[DM] CREATE_ZONES - Not making a calming quad because dmVarsPassed.iZoneWaterCalmingQuadID[", i, "] is ", dmVarsPassed.iZoneWaterCalmingQuadID[i])
								ENDIF
							ELSE
								PRINTLN("[DM] CREATE_ZONES - Not making a calming quad because g_FMMC_STRUCT_ENTITIES.sPlacedZones[", i, "].fZoneValue is ", g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue)
							ENDIF
							
							IF g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].fZoneValue2 != 1.0
								SET_BIT(dmVarsPassed.iLocalZoneCheckBitset, i)
								PRINTLN("[DM] CREATE_ZONES - Every frame check zone w iType ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].iType," in area: ",i," pos 0: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[0]," pos 1: ",g_FMMC_STRUCT_ENTITIES.sPlacedZones[i].vPos[1])
							ENDIF
							
						BREAK
						CASE ciFMMC_ZONE_TYPE__EXPLODE_PLAYER
							IF NOT IS_BIT_SET(dmVarsPassed.iLocalZoneCheckBitset, i)
								SET_BIT(dmVarsPassed.iLocalZoneCheckBitset, i)
								PRINTLN("[DM] CREATE_ZONES - Creating - ciFMMC_ZONE_TYPE__EXPLODE_PLAYER")
							ENDIF
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN
							IF NOT IS_BIT_SET(dmVarsPassed.iLocalZoneCheckBitset, i)
								SET_BIT(dmVarsPassed.iLocalZoneCheckBitset, i)
								PRINTLN("[DM] CREATE_ZONES - Creating - ciFMMC_ZONE_TYPE__OBJECT_RESPAWN")
							ENDIF
						BREAK
						
						CASE ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN
							IF NOT IS_BIT_SET(dmVarsPassed.iLocalZoneCheckBitset, i)
								SET_BIT(dmVarsPassed.iLocalZoneCheckBitset, i)
								PRINTLN("[DM] CREATE_ZONES - Creating - ciFMMC_ZONE_TYPE__OBJECT_RESPAWN_WITHIN")
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF	
		ENDIF
	ENDFOR
			
ENDPROC

FUNC ARENA_ANIM_SET GET_CELEB_TYPE(ServerBroadcastData &serverBDpassed)

	IF CONTENT_IS_USING_ARENA()
		
		IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		
			RETURN CELEBRATION_ARENA_ANIM_FLAT
		ENDIF
		
		RETURN CELEBRATION_ARENA_ANIM_PODIUM
	ENDIF	

	RETURN CELEBRATION_ARENA_ANIM_INVALID
ENDFUNC

PROC PROCESS_TIME_REMAINING_CACHE(ServerBroadcastData &serverBDpassed)
	INT iTimeMs = -1
	
	IF NOT g_bMissionClientGameStateRunning
		IF (GET_FRAME_COUNT() % 20) = 0
			PRINTLN("[LM][PROCESS_TIME_REMAINING_CACHE] g_bMissionClientGameStateRunning: ", g_bMissionClientGameStateRunning)
		ENDIF
		iTimeMs = -1
	ENDIF
	
	IF g_bCelebrationScreenIsActive
	OR g_bMissionEnding
		IF (GET_FRAME_COUNT() % 20) = 0
			PRINTLN("[LM][PROCESS_TIME_REMAINING_CACHE] g_bMissionEnding: ", g_bMissionEnding, " g_bCelebrationScreenIsActive: ", g_bCelebrationScreenIsActive)
		ENDIF
		iTimeMs = 0
	ENDIF
	
	IF HAS_DM_STARTED(serverBDpassed)
		IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)
			iTimeMs = GET_HUD_TIME(serverBDpassed)
			IF HAS_DM_TIME_EXPIRED(serverBDpassed)
				iTimeMs = 0
			ENDIF
		ENDIF
	ENDIF
	
	SET_TIME_LEFT_FOR_INSTANCED_CONTENT_IN_MILISECONDS(iTimeMs)
ENDPROC

// ################ IF YOU'RE UPDATING THIS, UPDATE THE CREATOR VERSION OF THESE FUNCTION TOO ############### ---------------------------------
PROC PROCESS_DM_RESPAWN_ENTITITES(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, ServerBroadcastData& serverBDPassed, KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo, SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF NOT HAS_DM_STARTED(serverBDPassed)
	OR g_bCelebrationScreenIsActive
	OR NOT IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
	OR NOT g_bMissionClientGameStateRunning
	OR NOT bIsLocalPlayerHost
	OR HAS_DEATHMATCH_FINISHED(serverBDPassed)
		EXIT
	ENDIF
	
	KOTH_HILL_RUNTIME_HOST_STRUCT sTemp
	
	// Vehicles
	INT iVeh
	FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1
		IF SHOULD_DM_VEH_RESPAWN(serverFMMCPassed, serverBDPassed, iVeh)
			IF CLEANED_UP_DM_VEH(serverFMMCPassed,  serverBDPassed, iVeh)
				IF RESPAWN_DM_VEH(serverFMMCPassed, iVeh, serverBDPassed)
				
					RESET_NET_TIMER(serverBDPassed.td_VehCleanupTimer[iVeh])
					CLEAR_BIT(serverBDPassed.iVehicleRespawnBitset, iVeh)
					
					INT iArea
					FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
						IF g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillEntity = iVeh
							IF g_FMMC_STRUCT.sKotHData.sHillData[iArea].iHillType = ciKOTH_HILL_TYPE__VEHICLE
								sKOTH_HostInfo.sHills[iArea] = sTemp
							ENDIF
						ENDIF
					ENDFOR		
				ENDIF
			ENDIF
		ENDIF
				
		IF IS_BIT_SET(serverBDPassed.iVehicleRespawnGhostedBitset, iVeh)		
			IF IS_RESPAWN_DM_VEH_NOW_SAFE(serverFMMCPassed, iVeh, dmVarsPassed)
				NETWORK_INDEX niVeh = serverFMMCPassed.niVehicle[iVeh]
				IF NETWORK_DOES_NETWORK_ID_EXIST(niVeh)
					VEHICLE_INDEX viVeh = NET_TO_VEH(niVeh)
					
					IF NETWORK_HAS_CONTROL_OF_ENTITY(viVeh)
						IF IS_VEHICLE_DRIVEABLE(viVeh)
							SET_ENTITY_COLLISION(viVeh, TRUE)
							SET_ENTITY_ALPHA(viVeh, 255, FALSE)
							FREEZE_ENTITY_POSITION(viVeh, FALSE)	
							SET_NETWORK_ID_CAN_MIGRATE(niVeh, TRUE)
							CLEAR_BIT(serverBDPassed.iVehicleRespawnGhostedBitset, iVeh)
						ENDIF
					ELSE
						NETWORK_REQUEST_CONTROL_OF_ENTITY(viVeh)
						PRINTLN("[LM][DM_ENTITY_RESPAWN][PROCESS_DM_RESPAWN_ENTITITES] Requesting control of vehicle ", iVeh)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
				
		IF iVeh = dmVarsPassed.iVehToShapeTest
		AND (NOT IS_BIT_SET(serverBDPassed.iVehicleRespawnGhostedBitset, iVeh))
			dmVarsPassed.iVehToShapeTest++
			PRINTLN("[LM][DM_ENTITY_RESPAWN][PROCESS_DM_RESPAWN_ENTITITES] Shape Test Veh was dirty. Moved onto iVeh: ", dmVarsPassed.iVehToShapeTest)
		ENDIF
		IF dmVarsPassed.iVehToShapeTest >= 32
			dmVarsPassed.iVehToShapeTest = 0
		ENDIF
	ENDFOR
	
	// Other Entities?
ENDPROC
// ################ IF YOU'RE UPDATING THIS, UPDATE THE CREATOR VERSION OF THESE FUNCTION TOO ############### ---------------------------------

PROC PROCESS_RESPAWN_VEHICLES_LOCAL(ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, SHARED_DM_VARIABLES &dmVarsPassed)
	VEHICLE_INDEX viVeh	
	INT iVeh
	FOR iVeh = 0 TO FMMC_MAX_VEHICLES-1	
		IF NETWORK_DOES_NETWORK_ID_EXIST(serverFMMCPassed.niVehicle[iVeh])
			viVeh = NET_TO_VEH(serverFMMCPassed.niVehicle[iVeh])
		ELSE
			viVeh = NULL
			RELOOP
		ENDIF
		
		IF IS_BIT_SET(serverBDPassed.iVehicleRespawnGhostedBitset, iVeh)
			DRAW_LOCAL_RESPAWN_VEHICLE_BOUNDS(iVeh)
			IF IS_VEHICLE_DRIVEABLE(viVeh)
				PRINTLN("PROCESS_RESPAWN_VEHICLES_LOCAL - Fading out veh ", iVeh, " / NetID: ", NETWORK_ENTITY_GET_OBJECT_ID(viVeh))
				SET_ENTITY_ALPHA(viVeh, 125, FALSE)
				SET_BIT(dmVarsPassed.iVehRespawnAlphaBS, iVeh)
			ENDIF
		ELIF IS_BIT_SET(dmVarsPassed.iVehRespawnAlphaBS, iVeh)
			IF IS_VEHICLE_DRIVEABLE(viVeh)
				SET_ENTITY_ALPHA(viVeh, 255, FALSE)
				CLEAR_BIT(dmVarsPassed.iVehRespawnAlphaBS, iVeh)
			ENDIF			
		ENDIF
	ENDFOR
ENDPROC

PROC PROCESS_FACIAL_IDLES(INT iFacialProgress, ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF NOT HAS_DM_STARTED(serverBDpassed)
	AND IS_ANIM_A_NEW_DM_INTRO(dmVarsPassed.DMIntroData)
		EXIT
	ENDIF
	
	MAINTAIN_FACIALS_IDLES(iFacialProgress)
	
ENDPROC

PROC DEAL_WITH_DEATHMATCH(ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB #IF IS_DEBUG_BUILD , LEADERBOARD_PLACEMENT_TOOLS& Placement #ENDIF)			

	// MUST BE AT START OF FUNCTION
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("DEAL_WITH_DEATHMATCH") 
	#ENDIF
	#ENDIF	
	
	IF CONTENT_IS_USING_ARENA() // done further down
		// MOVE TO RESULTS WHEN RACE ENDS
		//////////////////////////////////////////////////////////////////////////////////////////
		CLIENT_MOVES_TO_FIX_DEAD_WHEN_DM_OVER(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLIENT_MOVES_TO_FIX_DEAD_WHEN_DM_OVER") 
		#ENDIF
		#ENDIF
	ENDIF
		
	IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, LocalPlayer, iLocalPart)
	
		DO_OUTROS(playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DO_OUTROS") 
		#ENDIF
		#ENDIF
	
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			IF GET_CLIENT_MISSION_STAGE(playerBDPassed) != CLIENT_MISSION_STAGE_DEAD
				MAINTAIN_FORCED_CAMERA(serverBDPassed.iFixedCamera)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FORCED_CAMERA") 
		#ENDIF
		#ENDIF
	
		BLOCK_REWARD_WEAPONS_DURING_LOCKED_DMS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("BLOCK_REWARD_WEAPONS_DURING_LOCKED_DMS") 
		#ENDIF
		#ENDIF
	
		MANAGE_AUDIO_MIX_GROUPS(dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MANAGE_AUDIO_MIX_GROUPS") 
		#ENDIF
		#ENDIF
	
		PROCESS_PICKUPS_STAGGERED_CLIENT(serverBDpassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PICKUPS_STAGGERED_CLIENT") 
		#ENDIF
		#ENDIF
		
		HANDLE_INTERACTION_MENU()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("HANDLE_INTERACTION_MENU()")
		#ENDIF
		#ENDIF
	
		IF NOT CONTENT_IS_USING_ARENA() // done further up
			// MOVE TO RESULTS WHEN RACE ENDS
			//////////////////////////////////////////////////////////////////////////////////////////
			CLIENT_MOVES_TO_FIX_DEAD_WHEN_DM_OVER(serverBDpassed, playerBDPassed, dmVarsPassed)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("CLIENT_MOVES_TO_FIX_DEAD_WHEN_DM_OVER") 
			#ENDIF
			#ENDIF
		ENDIF
		
		MANAGE_FX(serverBDpassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MANAGE_FX")
		#ENDIF
		#ENDIF
		
		INCREMENT_POWER_PLAYER_STAT(serverBDpassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("INCREMENT_POWER_PLAYER_STAT")
		#ENDIF
		#ENDIF
		
		DOUBLE_XP_FINAL_KILL(serverBDpassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DOUBLE_XP_FINAL_KILL")
		#ENDIF
		#ENDIF
	
		TRACK_KILLS_DEATHS_DAMAGE(serverBDpassed, playerBDPassed#IF IS_DEBUG_BUILD, dmVarsPassed#ENDIF)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("TRACK_KILLS_DEATHS_DAMAGE")
		#ENDIF
		#ENDIF
		
		IF NOT IS_NON_ARENA_KING_OF_THE_HILL()
			MAINTAIN_VEHICLE_LOCK_STATE(serverBDpassed, dmVarsPassed, playerBDPassed)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_LOCK_STATE") 
			#ENDIF
			#ENDIF	
		ENDIF
		
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(LocalPlayer) //*1213240
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME")
		#ENDIF
		#ENDIF
		
		MAINTAIN_VEHICLE_SPECIAL_WEAPONS(serverBDpassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_VEHICLE_SPECIAL_WEAPONS") 
		#ENDIF
		#ENDIF
		
		CLIENT_HANDLE_DM_PERKS(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLIENT_HANDLE_DM_PERKS") 
		#ENDIF
		#ENDIF
		
		CLIENT_SETS_IDLE_BIT(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLIENT_SETS_IDLE_BIT") 
		#ENDIF
		#ENDIF
		
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(GET_DM_PED_DENSITY(), 0.0, 0.0, GET_TRAFFIC_DENSITY_FOR_DM(serverBDpassed), GET_TRAFFIC_DENSITY_FOR_DM(serverBDpassed), GET_PARKED_TRAFFIC_DENSITY_FOR_DM(serverBDpassed), GET_TRAFFIC_DENSITY_FOR_DM(serverBDpassed), IS_TRAFFIC_DISABLED_FOR_DM(serverBDpassed))
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME") 
		#ENDIF
		#ENDIF	
		
		CLIENT_POWER_PLAYS(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLIENT_POWER_PLAYS") 
		#ENDIF
		#ENDIF
		
		CLIENT_MANAGES_DOUBLE_DAMAGE_RECEIVED(serverBDpassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLIENT_MANAGES_DOUBLE_DAMAGE_RECEIVED") 
		#ENDIF
		#ENDIF
		
		// VEH DM START_____________________________________________________
		MAINTAIN_FORCED_WARP(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_FORCED_WARP") 
		#ENDIF
		#ENDIF
		
		BLOCK_VEHICLE_WEAPONS_WHILE_INVINCIBLE()
		
		MOVE_TO_RESPAWN_STAGE(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MOVE_TO_RESPAWN_STAGE") 
		#ENDIF
		#ENDIF
		
		CACHE_PLAYER_LAST_VEHICLE(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CACHE_PLAYER_LAST_VEHICLE") 
		#ENDIF
		#ENDIF	
		
		PROCESS_ZONES(dmVarsPassed, serverBDPassed, serverFMMCPassed, playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_ZONES") 
		#ENDIF
		#ENDIF	
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_ShowVehicleHealthinFP)
		AND NOT IS_ON_SCREEN_WHEEL_SPIN_MINIGAME_ACTIVE()
		AND IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
		AND NOT g_bMissionEnding
		AND NOT g_bCelebrationScreenIsActive
		AND g_bMissionClientGameStateRunning
			PROCESS_FP_VEHICLE_HEALTHBAR(1000)
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_FP_VEHICLE_HEALTHBAR") 
			#ENDIF
			#ENDIF	
		ENDIF
		
		PROCESS_GENERAL_VDM()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_GENERAL_VDM") 
		#ENDIF
		#ENDIF
		// __________________________________________________________________
		
		STORE_CLIENT_KILL_DEATH_RATIO(playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("STORE_CLIENT_KILL_DEATH_RATIO ")
		#ENDIF
		#ENDIF
		
		PROCESS_MISSION_TEXT(serverBDpassed, playerBDPassed[iLocalPart], dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_MISSION_TEXT")
		#ENDIF
		#ENDIF
		
		STORE_LOCAL_HIGHEST_STREAK(dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("STORE_LOCAL_HIGHEST_STREAK")
		#ENDIF
		#ENDIF
		
		PROCESS_LEADER_TICKERS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_LEADER_TICKERS")
		#ENDIF
		#ENDIF
		
		MAINTAIN_BRU_BOX_DEATHMATCH(g_i_BrucieInstanceDm)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BRU_BOX_DEATHMATCH")
		#ENDIF
		#ENDIF
		
		PROCESS_DISABLED_CONTROL_ACTIONS(dmVarsPassed.sPlayerModifierData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DISABLED_CONTROL_ACTIONS") 
		#ENDIF
		#ENDIF
		
		HANDLE_DEATHMATCH_WANTED_RATINGS(serverBDpassed)
		
		// 1643191
		IF HAS_DM_STARTED(serverBDpassed)
			IF ( IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed) AND GET_DM_VEH_MODEL() = RHINO )
			OR CONTENT_IS_USING_ARENA() 
			
//				IF IS_NON_ARENA_KING_OF_THE_HILL()
//				AND bIsLocalPlayerHost
//					serverBDpassed.iVehicleRemovalCap = NETWORK_GET_NUM_PARTICIPANTS()
//				ENDIF
			
				NETWORK_ENABLE_EMPTY_CROWDING_VEHICLES_REMOVAL(TRUE)
				NETWORK_CAP_EMPTY_CROWDING_VEHICLES_REMOVAL(serverBDpassed.iVehicleRemovalCap)
			ENDIF
			
			IF CONTENT_IS_USING_ARENA() 
				MAINTAIN_UPDATE_WINNER_VEHICLE_INDEX_FOR_CELEBRATION_SCREEN(serverBDpassed, dmVarsPassed)
				SAVE_CELEB_VEHICLE(serverBDpassed, dmVarsPassed)
			ENDIF
			
			IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
			AND NETWORK_HAS_CONTROL_OF_ENTITY(dmVarsPassed.dmVehIndex)
				IF NOT IS_ENTITY_ALIVE(dmVarsPassed.dmVehIndex)
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(dmVarsPassed.dmVehIndex, FALSE)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(dmVarsPassed.dmVehIndex)
						PRINTLN("[DM][KOTH] Setting dmVarsPassed.dmVehIndex as no longer needed!")
					ELSE
						SET_ENTITY_AS_MISSION_ENTITY(dmVarsPassed.dmVehIndex, FALSE, TRUE)
						PRINTLN("[DM][KOTH] Grabbing dmVarsPassed.dmVehIndex from other scripts!")
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_NON_ARENA_KING_OF_THE_HILL()
				IF NOT DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
				AND DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
					dmVarsPassed.dmVehIndex = g_SpawnData.MissionSpawnDetails.SpawnedVehicle
					//SET_ENTITY_AS_MISSION_ENTITY(dmVarsPassed.dmVehIndex, FALSE, TRUE)
					PRINTLN("[DM][KOTH] Setting dmVarsPassed.dmVehIndex to g_SpawnData.MissionSpawnDetails.SpawnedVehicle")
				ENDIF
			ENDIF
			
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("NETWORK_ENABLE_EMPTY_CROWDING_VEHICLES_REMOVAL")
		#ENDIF
		#ENDIF
		
		MAINTAIN_BOSS_V_BOSS_DM_CLIENT()
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("NETWORK_ENABLE_EMPTY_CROWDING_VEHICLES_REMOVAL")
		#ENDIF
		#ENDIF
		
		PROCESS_DM_HELPTEXT(dmVarsPassed.iHelptextBitset, playerBDPassed[iLocalPart].iTeam, IS_TEAM_DEATHMATCH(), dmVarsPassed.stHelptextTimer)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DM_HELPTEXT")
		#ENDIF
		#ENDIF
		
		 // FEATURE_GANG_BOSS
		
	ENDIF // This closes the spectator check
	
	MAINTAIN_DM_SCORE(serverBDpassed, dmVarsPassed)
		
	PROCESS_MUSIC(dmVarsPassed, serverBDpassed, playerBDPassed)
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DM_SCORE")
	#ENDIF
	#ENDIF
	
	PLAY_DEATHMATCH_NEARLY_FINISHED_AUDIO(serverBDpassed, playerBDPassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PLAY_DEATHMATCH_NEARLY_FINISHED_AUDIO")
	#ENDIF
	#ENDIF
	
	PROCESS_KART_BATTLE_ROUND_COUNTDOWN_AUDIO(serverBDpassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_KART_BATTLE_ROUND_COUNTDOWN_AUDIO")
	#ENDIF
	#ENDIF
	
	PROCESS_TIME_REMAINING_CACHE(serverBDpassed)
	
	PROCESS_FACIAL_IDLES(dmVarsPassed.iFacialProgress, serverBDpassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_FACIAL_IDLES")
	#ENDIF
	#ENDIF
	
	REFRESH_DPAD_WHEN_DATA_CHANGES(serverBDpassed.iHostRefreshDpadValue, dmVarsPassed.iClientRefreshDpadValue)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("REFRESH_DPAD_WHEN_DATA_CHANGES")
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
				
	//	Deal with debug passing
	// -----------------------------------------------------------------------------------------------//
		PROCESS_DEATHMATCH_DEBUG_KEY_PRESSES(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_DEATHMATCH_DEBUG_KEY_PRESSES")
		#ENDIF
		#ENDIF
	// -----------------------------------------------------------------------------------------------//
	
//		DEBUG_DISPLAY_DPAD_LEADERBOARD(Placement, serverBDpassed, dmVarsPassed)
		
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DEBUG_DISPLAY_DPAD_LEADERBOARD")
		#ENDIF
		
		DEBUG_DISPLAY_BIG_LEADERBOARD(Placement, serverBDpassed, dmVarsPassed)
		
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DEBUG_DISPLAY_BIG_LEADERBOARD")
		#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF	
	
	IF dmVarsPassed.DID_PLAYER_CREATE_ACTIVITY =FALSE
		DID_I_CREATE_THIS_MISSION(dmVarsPassed.DID_PLAYER_CREATE_ACTIVITY) // checks if player is playing a  race created 
	ENDIF
ENDPROC

PROC SETUP_FAKE_CAMERA_PAUSE(SHARED_DM_VARIABLES &dmVarsPassed)
	
	//RESET_PAUSED_RENDERPHASES()
	
	IF NOT dmVarsPassed.bFakeCameraSetup
	
		SET_HIDOF_OVERRIDE(TRUE,FALSE,0,0,0,0)
	
		VECTOR cameraStartPos
		VECTOR cameraStartRot
		FLOAT fcamefov
		
		cameraStartPos = g_CoronaJobLaunchData.vFakeCamCoords
		cameraStartRot = g_CoronaJobLaunchData.vFakeCamRotation
		fcamefov = g_CoronaJobLaunchData.fFakeCameraFov
		
		// fix bug 1510934 & 1632828
		IF fcamefov <= 0.000000
			fcamefov = 40.0
		ENDIF
		
		IF NOT DOES_CAM_EXIST(dmVarsPassed.DMIntroData.jobIntroCam)
			dmVarsPassed.DMIntroData.jobIntroCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
		ENDIF

		SET_CAM_COORD(dmVarsPassed.DMIntroData.jobIntroCam,cameraStartPos)
		SET_CAM_ROT(dmVarsPassed.DMIntroData.jobIntroCam,cameraStartRot)
		SET_CAM_FOV(dmVarsPassed.DMIntroData.jobIntroCam,fcamefov)

		SET_CAM_ACTIVE(dmVarsPassed.DMIntroData.jobIntroCam, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
		
		//SET_SKYBLUR_BLURRY()
		SET_SKYFREEZE_FROZEN()
		
		ANIMPOSTFX_PLAY("MP_job_load", 0, TRUE)
		
		dmVarsPassed.bFakeCameraSetup = TRUE
	
	ENDIF
	//TOGGLE_RENDERPHASES(FALSE)
	//TOGGLE_RENDERPHASES(FALSE)
	
ENDPROC

PROC SET_UP_OVERHEAD_NAMES_DM(ServerBroadcastData &serverBDpassed)	
	
	IF serverBDPassed.iTagSetting = TAGS_OFF
		g_iOverheadNamesState = TAGS_OFF
	ELSE
		IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
			g_iOverheadNamesState = TAGS_WHEN_INTEAM
		ELSE
			g_iOverheadNamesState = serverBDPassed.iTagSetting
		ENDIF
	ENDIF
	PRINTLN("SET_UP_OVERHEAD_NAMES_DM, g_iOverheadNamesState = ", g_iOverheadNamesState)
ENDPROC

PROC TURN_SPECTATOR_CHAT_ON_WHEN_DM_ENDS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_SPECTATOR_CHAT)
		IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
			ALLOW_SPECTATORS_TO_CHAT(TRUE)
			PRINTLN("[DM_CHAT] TURN_SPECTATOR_CHAT_ON_WHEN_DM_ENDS ")
			SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_SPECTATOR_CHAT)
		ENDIF
	ENDIF
ENDPROC

PROC CLIENT_SET_VOICE_CHAT(ServerBroadcastData &serverBDpassed)
	PRINTLN("[DM_CHAT] CLIENT_SET_VOICE_CHAT")
	
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
	AND NOT IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		CLIENT_SET_UP_EVERYONE_CHAT()
	ELIF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
		CLIENT_SET_UP_TEAM_ONLY_CHAT()//(FFA_DM_TEAM_PROXIMITY)
	ELSE
		IF IS_KING_OF_THE_HILL()
			CLIENT_SET_UP_EVERYONE_CHAT()
		ELSE
			CLIENT_SET_UP_PROXIMITY_CHAT(FFA_DM_PROXIMITY)
		ENDIF
	ENDIF
ENDPROC

PROC SET_SPECTATOR_CHAT(ServerBroadcastData &serverBDpassed)
	IF IS_THIS_FFA_DEATHMATCH(serverBDpassed)
		CLIENT_SET_UP_EVERYONE_CHAT()
		PRINTLN("[DM_CHAT] SET_SPECTATOR_CHAT")
	ENDIF
ENDPROC

FUNC BOOL DID_MY_GANG_WIN_BOSS_VS_BOSS_DM(ServerBroadcastData &serverBDpassed)
	INT iMyTeam 			= GET_PLAYER_TEAM(LocalPlayer)
//	INT iMyTeamScore		= 0
//	INT iRivalTeam 			= GET_RIVAL_TEAM(LocalPlayer, serverBDpassed)
//	INT iRivalTeamScore		= 0
//	
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DID_MY_GANG_WIN_BOSS_VS_BOSS_DM] iMyTeam = ", iMyTeam, " iRivalTeam = ", iRivalTeam) 
//	IF iMyTeam > -1
//		IF iRivalTeam > -1
//			iMyTeamScore 	= GET_TEAM_DM_SCORE(serverBDpassed, iMyTeam)
//			iRivalTeamScore = GET_TEAM_DM_SCORE(serverBDpassed, iRivalTeam)
//			
//			CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DID_MY_GANG_WIN_BOSS_VS_BOSS_DM] iMyTeam = ", iMyTeam, " iMyTeamScore = ", iMyTeamScore, " iRivalTeam = ", iRivalTeam, " iRivalTeamScore = ", iRivalTeamScore)
//			IF iMyTeamScore > iRivalTeamScore
//				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DID_MY_GANG_WIN_BOSS_VS_BOSS_DM] TRUE")
//				RETURN TRUE
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DID_MY_GANG_WIN_BOSS_VS_BOSS_DM] FALSE")
	
//	RETURN FALSE
	
	CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DID_MY_GANG_WIN_BOSS_VS_BOSS_DM] iMyTeam = ", iMyTeam, " serverBDpassed.iWinningTeam = ", serverBDpassed.iWinningTeam) 
	
	RETURN (iMyTeam = serverBDpassed.iWinningTeam )
	
ENDFUNC




PROC DO_END_BOSSVBOSS_SPLASH(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	BOOL bAbandoned
	GANG_BOSS_MANAGE_REWARDS_DATA gbRewards
	STRING sOrganization
	HUD_COLOURS hclPlayer
	BOOL bPutAwayPhone
	TEXT_LABEL_23 tl23Desc
	TEXT_LABEL_23 tl23Title
	
	BOOL bResetBlips
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		Clear_Any_Objective_Text_From_This_Script()
		INT iSlot = NATIVE_TO_INT(LocalPlayer)
		IF iSlot <> -1
			// "One on One Deathmatch abandoned"
			IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob) 
				bAbandoned = TRUE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] bAbandoned = TRUE")
				
				tl23Desc = "BIGM_BVB_ABN"
				tl23Title = "GB_WORK_OVER"
				IF IS_PLAYER_ON_BIKER_DM()
					tl23Desc = "BIGM_BK_ABN"
					tl23Title = "BK_RUN_OVER"
				ENDIF
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, tl23Title, tl23Desc)
				IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LEFT)
					SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
				ENDIF
				bPutAwayPhone = TRUE
				IF NOT IS_PLAYER_ON_BIKER_DM()
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards)
				ELSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards, TRUE)
				
				ENDIF
					IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
						GB_SET_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_BOSS_QUIT_BOSSVBOSS_DM)
					ENDIF
					bResetBlips = TRUE	
			ENDIF
		ENDIF
		
		IF NOT bAbandoned
			CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] MY TEAM = ", GET_PLAYER_TEAM(LocalPlayer), " serverBDpassed.iWinningTeam = ", serverBDpassed.iWinningTeam)
			IF HAS_GANG_BOSS_QUIT_BOSSVBOSS_DM(serverBDpassed)
				//-- One of the bosses quit
				IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(LocalPlayer)
				AND NOT IS_PLAYER_ON_BIKER_DM()
					IF GET_PLAYER_TEAM(LocalPlayer) = serverBDpassed.iWinningTeam
						//-- My Boss quit
						tl23Title = "GB_WORK_OVER"
						IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG() // Added back in for 2597013
						OR IS_PLAYER_ON_BIKER_DM()
							tl23Desc = "BIGM_BOSSVBOSSOMBQ"
							
							IF IS_PLAYER_ON_BIKER_DM()
								tl23Desc = "BIGM_BKVBKOMBQ"
								tl23Title = "BK_RUN_OVER"
							ENDIF
							SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, tl23Title, tl23Desc)
						ENDIF
						IF NOT IS_PLAYER_ON_BIKER_DM()
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards)
						ELSE
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards, TRUE)
						ENDIF
						IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
							SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						ENDIF
						bPutAwayPhone = TRUE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] MY BOSS QUIT")
					ELSE
						tl23Desc = "BIGM_BOSSVBOSSORBQ"
						tl23Title = "GB_WORK_OVER"
						IF IS_PLAYER_ON_BIKER_DM()
							IF dmVarsPassed.bRivalBossIsBiker
								tl23Desc = "BIGM_BKVBKORBQ"
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - think rival boss was a biker")
							ELSE
								tl23Desc = "BIGM_BOSSVBOSSORBQ"
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - think rival boss was a VIP")
							ENDIF
							tl23Title = "BK_RUN_OVER"
						ENDIF
							
						//-- Rival boss quit
						SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, tl23Title, tl23Desc)
						
						IF NOT IS_PLAYER_ON_BIKER_DM()
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards)
						ELSE
							GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards, TRUE)
						
						ENDIF
						IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
							GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
							SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						ENDIF
						bPutAwayPhone = TRUE
						CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - 1")
					ENDIF
				ELSE
					tl23Desc = "BIGM_BOSSVBOSSORBQ"
					tl23Title = "GB_WORK_OVER"
					IF IS_PLAYER_ON_BIKER_DM()
						IF dmVarsPassed.bRivalBossIsBiker
							IF GET_PLAYER_TEAM(LocalPlayer) = serverBDpassed.iWinningTeam
								tl23Desc = "BIGM_BKVBKOMBQ"
								tl23Title = "BK_RUN_OVER"
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - think Your MC President quit")
							ELSE
								tl23Desc = "BIGM_BKVBKORBQ"
								tl23Title = "BK_RUN_OVER"
								CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - think rival boss was a biker 2")
							
							ENDIF
						ELSE
							tl23Desc = "BIGM_BOSSVBOSSORBQ"
							CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - think rival boss was a VIP 2")
						ENDIF
						tl23Title = "BK_RUN_OVER"
					ENDIF
					
					//-- Rival boss quit
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_GENERIC, tl23Title, tl23Desc)
					IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_BOSS_LEFT)
						SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					ENDIF
					IF NOT IS_PLAYER_ON_BIKER_DM()
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards)
					ELSE
						GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards, TRUE)
					
					ENDIF
					
					SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					bPutAwayPhone = TRUE
					CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] RIVAL BOSS QUIT - 2")
				ENDIF
					bResetBlips = TRUE	
			ELIF CHECK_FOR_NO_BOSS_VS_BOSS_DM_WINNER(serverBDpassed)
				tl23Desc = "BIGM_BVBDRD"
				tl23Title = "GB_WORK_OVER"	
				IF IS_PLAYER_ON_BIKER_DM()
					tl23Desc = "BIGM_CLBWRK_OV"
					tl23Title = "BK_RUN_OVER"
				ENDIF
					
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, tl23Title, tl23Desc)
				IF NOT IS_PLAYER_ON_BIKER_DM()
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards)
				ELSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards, TRUE)
				
				ENDIF
				IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_TIME_OUT)
					SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
				ENDIF
				bPutAwayPhone = TRUE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] NO WINNER")
					bResetBlips = TRUE	
			ELIF DID_MY_GANG_WIN_BOSS_VS_BOSS_DM(serverBDpassed)
				tl23Desc = "BIGM_BOSSVBOSSWD"
					
				IF IS_PLAYER_ON_BIKER_DM()
					tl23Desc = "BIGM_BKVBKWD"
				ENDIF
			
				sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
				hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(LocalPlayer)
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_SUCCESS, "GB_WINNER", tl23Desc, sOrganization, hclPlayer) 
				
				
				IF NOT IS_PLAYER_ON_BIKER_DM()
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, TRUE, gbRewards)
				ELSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, TRUE, gbRewards, TRUE)
				
				ENDIF
				IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
					SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
				ENDIF
				bPutAwayPhone = TRUE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] WE WON")
					bResetBlips = TRUE	
			ELSE
				tl23Desc = "BIGM_BOSSVBOSSLD"
				tl23Title = "GB_WORK_OVER"		
				IF IS_PLAYER_ON_BIKER_DM()
					tl23Desc = "BIGM_BKVBKLD"
					tl23Title = "BK_RUN_OVER"
				ENDIF
				
				sOrganization = GB_GET_ORGANIZATION_NAME_AS_A_STRING()
				hclPlayer = GB_GET_PLAYER_GANG_HUD_COLOUR(LocalPlayer)
				SETUP_NEW_BIG_MESSAGE_FOR_ORGANIZATION(BIG_MESSAGE_GB_END_OF_JOB_OVER, tl23Title, tl23Desc, sOrganization, hclPlayer) 
				
				IF NOT IS_PLAYER_ON_BIKER_DM()
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards)
				ELSE
					GANG_BOSS_MANAGE_REWARDS(FMMC_TYPE_GB_BOSS_DEATHMATCH, FALSE, gbRewards, TRUE)
				
				ENDIF
				IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
					SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
				ENDIF
				bPutAwayPhone = TRUE
				CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] WE LOST")
					bResetBlips = TRUE	
			ENDIF
		ENDIF
	ENDIF 
	
	IF bPutAwayPhone
		IF IS_PHONE_ONSCREEN()
			CPRINTLN(DEBUG_NET_MAGNATE, "     ----->    [dsw] [BOSSVBOSS] [DO_END_BOSSVBOSS_SPLASH] HANG_UP_AND_PUT_AWAY_PHONE")
			HANG_UP_AND_PUT_AWAY_PHONE()
		ENDIF
	ENDIF
	
	IF bResetBlips
		RESET_ALL_BVB_PLAYER_BLIPS(dmVarsPassed)
	ENDIF
ENDPROC
 // FEATURE_GANG_BOSS

PROC DEAL_WITH_PLAYER_ANIMATIONS()

	INT i
	
	REPEAT NUM_NETWORK_PLAYERS i
	
		// call this regardless of whether they are playing an anim or not
		IF DOES_ENTITY_EXIST(g_CoronaClonePeds.piPlayersPed[i].pedID)
			PRINTLN("DOES_ENTITY_EXIST, g_CoronaClonePeds.piPlayersPed[i]", i)
			UPDATE_INTERACTION_ANIM_FOR_PED(g_CoronaClonePeds.piPlayersPed[i].pedID, i, gInteractionsPedsData[i])
		ENDIF
		
	ENDREPEAT
	
	INT iPlayerID
	BOOL bQuickplayPressed = FALSE

	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
			bQuickplayPressed = TRUE
		ENDIF
	ELSE
		
	IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
	AND IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
			bQuickplayPressed = TRUE
		ENDIF
	
	ENDIF
	

	IF bQuickplayPressed
		PRINTLN("IS_QUICKPLAY_ANIM_BUTTON_PRESSED, TRUE")
		iPlayerID = NATIVE_TO_INT(LocalPlayer)
		IF iPlayerID <> -1
			PRINTLN("IS_QUICKPLAY_ANIM_BUTTON_PRESSED, iPlayerID = ", iPlayerID)
			MPGlobalsInteractions.PedInteraction[iPlayerID].iInteractionType = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM_TY)
			MPGlobalsInteractions.PedInteraction[iPlayerID].iInteractionAnim = GET_MP_INT_CHARACTER_STAT(MP_STAT_LAST_PLAYED_CORONA_ANIM)
			MPGlobalsInteractions.PedInteraction[iPlayerID].bPlayInteractionAnim = TRUE // starts playing the anim
			MPGlobalsInteractions.PedInteraction[iPlayerID].bHoldLoop = FALSE  // set this if you want to hold the anim
		ENDIF
	ENDIF


ENDPROC

PROC SET_BIT_WHEN_PLAYER_QUITS(PlayerBroadcastData &playerBDPassed[])
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iSlot = NATIVE_TO_INT(LocalPlayer)
		IF NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)
			IF iSlot <> -1
				IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob) = TRUE
					PRINTLN("SET_BIT_WHEN_PLAYER_QUITS, CLIENT_BITSET_PLAYER_QUIT_JOB")
					SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_PLAYER_QUIT_JOB)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a list of players that were on the winning team, in order of their position (if it's not a team event then just one player is stored).
FUNC BOOL GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], PLAYER_INDEX &winningPlayers[MAX_NUM_CELEBRATION_PEDS], INt &iNumberPlayersFound, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	BOOL bArenaAnims, bPodium

	IF CONTENT_IS_USING_ARENA()
		bArenaAnims = TRUE
		IF ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_PODIUM )
//		OR ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_FLAT )
			bPodium = TRUE
		ENDIF
	ENDIF
	
	INT iNumPlayersFound = 0
	INT i = 0

	//First wipe the list (default value is zero which is a valid player index).
	REPEAT COUNT_OF(winningPlayers) i
		winningPlayers[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	IF bArenaAnims
	
		IF bPodium
		
			REPEAT NUM_NETWORK_PLAYERS i
				IF iNumPlayersFound < MAX_PODIUM
					IF serverBDpassedLDB.leaderBoard[i].iParticipant > -1
					AND NOT IS_BIT_SET(playerBDPassed[serverBDpassedLDB.leaderBoard[i].iParticipant].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)

						PLAYER_INDEX currentPlayer = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, i)	
					
						IF IS_NET_PLAYER_OK(currentPlayer, FALSE)
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player being added to list of winners:")
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iNumPlayersFound: ", iNumPlayersFound)
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] playerBDPassed[i]: ", i)
						
							winningPlayers[iNumPlayersFound] = currentPlayer
							iNumPlayersFound++
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, bPodium ")
		
		ELSE
	
			REPEAT NUM_NETWORK_PLAYERS i
				IF iNumPlayersFound < COUNT_OF(winningPlayers)
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, A: ", i)
					IF serverBDpassedLDB.leaderBoard[i].iParticipant > -1
						CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, B: ", i)
						IF serverBDpassedLDB.leaderBoard[i].iTeam > -1 
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, C: ", i)
							IF NOT IS_BIT_SET(playerBDPassed[serverBDpassedLDB.leaderBoard[i].iParticipant].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)
								CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, D: ", i)
								IF serverBDpassedLDB.leaderBoard[i].iTeam = serverBDpassed.iWinningTeam
									CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, E: ", i)
									PLAYER_INDEX currentPlayer = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, i)	
								
									IF IS_NET_PLAYER_OK(currentPlayer, FALSE)
										CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player being added to list of winners:")
										CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iNumPlayersFound: ", iNumPlayersFound)
										CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] playerBDPassed[i]: ", i)
									
										winningPlayers[iNumPlayersFound] = currentPlayer
										iNumPlayersFound++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, FLAT ")
		ENDIF
		
		// Failsafe, Only 1 player remains so set them as winner
		IF iNumPlayersFound = 0
		AND NETWORK_GET_NUM_PARTICIPANTS() = 1
			winningPlayers[0] = LocalPlayer
			iNumPlayersFound++
			CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, Only 1 player remains so set them as winner, iNumPlayersFound = ", iNumPlayersFound)
		ENDIF
		

	ELIF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
	AND NOT IS_TDM_USING_FFA_SCORING()
		REPEAT NUM_NETWORK_PLAYERS i
			IF iNumPlayersFound < COUNT_OF(winningPlayers)
				IF serverBDpassedLDB.leaderBoard[i].iParticipant > -1
				AND serverBDpassedLDB.leaderBoard[i].iTeam > -1 
				AND NOT IS_BIT_SET(playerBDPassed[serverBDpassedLDB.leaderBoard[i].iParticipant].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)
					IF serverBDpassedLDB.leaderBoard[i].iTeam = serverBDpassed.iWinningTeam

						PLAYER_INDEX currentPlayer = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, i)	
					
						IF IS_NET_PLAYER_OK(currentPlayer, FALSE)
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Player being added to list of winners:")
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] iNumPlayersFound: ", iNumPlayersFound)
							CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] playerBDPassed[i]: ", i)
						
							winningPlayers[iNumPlayersFound] = currentPlayer
							iNumPlayersFound++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] bArenaAnims, FALSE TDM ")
	ELSE
		CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - Not a team event, just grabbing the top player.")
	
		IF serverBDpassedLDB.leaderBoard[0].iParticipant > -1
			winningPlayers[0] = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0)
			
			IF IS_NET_PLAYER_OK(winningPlayers[0], FALSE)
				iNumPlayersFound++
			ENDIF
		ENDIF
	ENDIF
	
	iNumberPlayersFound = iNumPlayersFound
	
	IF iNumPlayersFound > 0
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN - No valid players found.")
	
	RETURN FALSE
ENDFUNC

PROC ADD_ARENA_CELEBRATION_SHARDS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, STRING strScreenName, INT iFinishPos)

	IF dmVarsPassed.sSplitSession.bSplitSession 
	OR bSplitSessionDuringJob
		
		PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, EXIT as bSplitSession ")
		
		EXIT
	ENDIF
	
	BOOL bPlayedThisMatchWithFriend 

	// Populate Arena points struct
	FILL_ARENA_POINTS_STRUCT(playerBDPassed[iLocalPart].sArenaPoints, IS_THIS_TEAM_DEATHMATCH(serverBDpassed), iFinishPos, serverBDpassed.iNumDMPlayers)
	
	bPlayedThisMatchWithFriend = (playerBDPassed[iLocalPart].sArenaPoints.iNumFriends > 0)
	
	// Technical calculate Arena points
	IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(iFinishPos, IS_THIS_TEAM_DEATHMATCH(serverBDpassed), DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed), IS_PLAYER_SELECTING_CUSTOM_VEHICLE(GET_PLAYER_INDEX(), TRUE), serverBDpassed.iNumDMPlayers, bPlayedThisMatchWithFriend, serverBDpassed.iNumberOfTeams)
	ENDIF
	
	// Populate Arena points
	FEED_ARENA_POINTS_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, playerBDPassed[iLocalPart].sArenaPoints)
	
	// Display Arena points bar
	ADD_ARENA_POINTS_BAR_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName)
	
	// Display Reward unlocks if appropriate
	playerBDPassed[iLocalPart].sArenaPoints.iNumRewardsUnlocked = GET_NUM_ITEMS_UNLOCKED_AFTER_LAST_ARENA_EVENT()   
	
	ARENA_CAREER_UNLOCK_ITEMS eUnlock
	TEXT_LABEL_15 tl15Reward
	TEXT_LABEL_15 tl15RewardSubString
	INT iValuePassed 

	IF WERE_ANY_ITEMS_UNLOCKED_AFTER_LAST_ARENA_EVENT()
	AND playerBDPassed[iLocalPart].sArenaPoints.iNumRewardsUnlocked > 0
	
		INT i
		REPEAT playerBDPassed[iLocalPart].sArenaPoints.iNumRewardsUnlocked i
		
			eUnlock = GET_ARENA_CARRER_ITEM_UNLOCKED(i)
			
			tl15Reward = GET_ARENA_CAREER_UNLOCK_TEXT_LABEL(eUnlock, tl15RewardSubString) 
			
			iValuePassed = GET_ARENA_CARRER_ITEM_UNLOCKED_AMMOUNT(i) 
			
			#IF IS_DEBUG_BUILD
				IF IS_STRING_NULL_OR_EMPTY(tl15RewardSubString)
					PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, sRewardName = ", tl15Reward, " i = ", i, " iNumRewardsUnlocked = ", playerBDPassed[iLocalPart].sArenaPoints.iNumRewardsUnlocked, " iValuePassed = ", iValuePassed)
				ELSE	
					PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, sRewardName = ", tl15RewardSubString, " i = ", i, " iNumRewardsUnlocked = ", playerBDPassed[iLocalPart].sArenaPoints.iNumRewardsUnlocked, " iValuePassed = ", iValuePassed)
				ENDIF
			#ENDIF
			
			ADD_ARENA_REWARDS_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, tl15Reward, tl15RewardSubString, iValuePassed)
	
		ENDREPEAT
		
		// Increase display time to allow these to show
		dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += ( CELEBRATION_SCREEN_STAT_DISPLAY_TIME * playerBDPassed[iLocalPart].sArenaPoints.iNumRewardsUnlocked )
	ELSE
		dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += 1000
	ENDIF
	
	// To show on the leaderboard
	INT iApEarned = GET_ARENA_EVENT_ARENA_CAREER_POINTS_REWARD(IS_THIS_TEAM_DEATHMATCH(serverBDpassed), iFinishPos, FALSE, serverBDpassed.iNumDMPlayers, FALSE, FALSE)
	playerBDPassed[iLocalPart].iApEarned = iApEarned
	PRINTLN("[NETCELEBRATION] ADD_ARENA_CELEBRATION_SHARDS, playerBDPassed[iLocalPart].iApEarned = ", playerBDPassed[iLocalPart].iApEarned)

ENDPROC

/// PURPOSE:
///    Runs the race celebration screen showing the race end stats.
/// RETURNS:
///    TRUE once the sequence has finished.
FUNC BOOL HAS_CELEBRATION_SUMMARY_FINISHED(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB)
			
	#IF IS_DEBUG_BUILD
	IF g_SkipCelebAndLbd
		RETURN TRUE
	ENDIF	
	#ENDIF
	
	IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
		PRINTLN("[NETDEATHMATCH] HAS_CELEBRATION_SUMMARY_FINISHED - DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE is TRUE")
		
		dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
	
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		PRINTLN("[NETDEATHMATCH] HAS_CELEBRATION_SUMMARY_FINISHED - IS_LOCAL_PLAYER_SPECTATOR_ONLY is TRUE")
		dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
		
		SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
	
		RETURN TRUE
	ENDIF
	
//	IF DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed) // 907242
//		PRINTLN("[NETDEATHMATCH] HAS_CELEBRATION_SUMMARY_FINISHED, DID_DM_END_WITHOUT_ENOUGH_PLAYERS, RETURN TRUE")
//	
//		RETURN TRUE
//	ENDIF
	
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	IF IS_PLAYER_CONTROL_ON(LocalPlayer)
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
	ENDIF
	
	//If the screen was frozen then make sure pad shake is disabled too.
	IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
		STOP_CONTROL_SHAKE(PLAYER_CONTROL)
	ENDIF
	
	// 1632360 (Speirs)
	INT iDeathmatchCelebrationXP 
	IF IS_THIS_A_ROUNDS_MISSION()
		iDeathmatchCelebrationXP = playerBDPassed[iLocalPart].iJobXP
	ELSE
		iDeathmatchCelebrationXP = (dmVarsPassed.iXPToShowOnLeaderboard + dmVarsPassed.iXPAdditional)
	ENDIF
	
	INT iJobPoints
	
	BOOL bArena
	IF IS_ARENA_WARS_JOB(TRUE)
		bArena = TRUE
	ENDIF
			
	SWITCH dmVarsPassed.sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			
			REQUEST_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData)
			SET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE(TRUE)
			//PLAYER_INDEX winningPlayer
			BOOL bFetchedStatsFromServer, bPlayerInSameCrewAsWinner
			GAMER_HANDLE sWinnerHandle
			//winningPlayer = GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0)
			//sWinnerHandle = GET_GAMER_HANDLE_PLAYER(winningPlayer)
			IF serverBDpassedLDB.leaderBoard[0].iParticipant > -1
				sWinnerHandle = dmVarsPassed.lbdVars.aGamer[serverBDpassedLDB.leaderBoard[0].iParticipant] //This should be able to grab handles for players that have left already left the game.
			ENDIF
			bPlayerInSameCrewAsWinner = IS_PLAYER_IN_SAME_CREW_AS_ME(sWinnerHandle, bFetchedStatsFromServer)
			
			IF HAS_CELEBRATION_SCREEN_LOADED(dmVarsPassed.sCelebrationData)
			AND bFetchedStatsFromServer
			AND IS_SCREEN_FADED_IN()
			AND NOT ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
				
				STRING strScreenName, strBackgroundColour
				INT iFinishPos, iCash, iScore, iCurrentRP, iCurrentLvl, iNextLvl, iRPToReachCurrentLvl, iRPToReachNextLvl, iChallengePart, iTotalChallengeParts
				BOOL bWonChallengePart
				
				//Retrieve all the required stats for the race end screen.
				strScreenName = "SUMMARY"
				strBackgroundColour = "HUD_COLOUR_BLACK"
				IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
				AND NOT IS_TDM_USING_FFA_SCORING()
					iFinishPos = GET_TEAM_RANK(serverBDpassed, playerBDPassed[iLocalPart].iTeam, serverBDpassed.iNumberOfTeams)
					PRINTLN("HAS_CELEBRATION_SUMMARY_FINISHED, GET_TEAM_RANK, iFinishPos = ", iFinishPos)
				ELSE
					IF CONTENT_IS_USING_ARENA()
						iFinishPos = GET_LOCAL_PLAYER_LEADERBOARD_POSITION(serverBDpassedLDB)
						PRINTLN("HAS_CELEBRATION_SUMMARY_FINISHED, GET_LOCAL_PLAYER_LEADERBOARD_POSITION, iFinishPos = ", iFinishPos)
					ELSE
						iFinishPos = GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT())
						PRINTLN("HAS_CELEBRATION_SUMMARY_FINISHED, GET_PLAYER_FINISH_POS, iFinishPos = ", iFinishPos)
					ENDIF
				ENDIF
				iCash = playerBDPassed[iLocalPart].iJobCash
				iCurrentRP = GET_PLAYER_FM_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) - iDeathmatchCelebrationXP
				iCurrentLvl = GET_FM_RANK_FROM_XP_VALUE(iCurrentRP) //GET_PLAYER_GLOBAL_RANK(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())) //We don't want the current level, but their level before the XP was given.
				iNextLvl = iCurrentLvl + 1
				iRPToReachCurrentLvl = GET_XP_NEEDED_FOR_FM_RANK(iCurrentLvl)
				iRPToReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(iNextLvl)
				iChallengePart = g_sCurrentPlayListDetails.iPlaylistProgress
				iTotalChallengeParts = g_sCurrentPlayListDetails.iLength
				dmVarsPassed.sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
				AND NOT IS_TDM_USING_FFA_SCORING()
					iScore = GET_TEAM_DM_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam)
				ELSE
					iScore = GET_PLAYER_SCORE(serverBDpassed, LocalPlayer, serverBDpassedLDB)
				ENDIF
				
				//Build the screen
				CREATE_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, strBackgroundColour)
				
				IF IS_TEAM_DEATHMATCH()
				AND NOT IS_TDM_USING_FFA_SCORING()
					IF iFinishPos = 1
						ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_VS_MISSION, TRUE, "", "", "")
					ELSE
						ADD_MISSION_RESULT_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, CELEBRATION_JOB_TYPE_VS_MISSION, FALSE, "", "", "")
					ENDIF
				ELSE
					ADD_FINISH_POSITION_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, iFinishPos)
				ENDIF
				dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				
				IF IS_PLAYLIST_DOING_HEAD_TO_HEAD()
					ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, CHALLENGE_PART_HEAD_TO_HEAD, iChallengePart, iTotalChallengeParts, bPlayerInSameCrewAsWinner)
					dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ELIF IS_PLAYLIST_DOING_CHALLENGE()
					IF iScore >= g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].iBestScore
					AND iFinishPos = 1
						bWonChallengePart = TRUE
					ENDIF
				
					ADD_CHALLENGE_PART_RESULT_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, CHALLENGE_PART_CREW, iChallengePart, iTotalChallengeParts, bWonChallengePart)
					dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME
				ENDIF
				
				IF bArena
					
					ADD_ARENA_CELEBRATION_SHARDS(serverBDpassed, playerBDPassed, dmVarsPassed, strScreenName, iFinishPos)
					
				ELSE
					IF NOT IS_THIS_A_ROUNDS_MISSION()
					OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
						IF IS_THIS_A_ROUNDS_MISSION()
							iJobPoints = g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iJobPoints							
						ELSE
							iJobPoints = playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerJobPoints							
						ENDIF
						
						PRINTLN("[JOB POINT], iJobPoints on Celebration screen = ", iJobPoints)
						
						IF iCash != 0
							ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, iJobPoints)
						ELSE
							ADD_JOB_POINTS_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, iJobPoints, TRUE, FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_THIS_A_ROUNDS_MISSION()
				OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					IF iCash != 0
						ADD_CASH_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, iCash)
						dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME //If displaying cash do an extra screen.
						IF bArena
							INCREMENT_ARENA_CASH_EARNED_FOR_AWARD(iCash)
							PRINTLN("[MJL] MP_AWARD_ARENA_WAGEWORKER = ", GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ARENA_WAGEWORKER))
						ENDIF
					ENDIF
					
					IF NOT bArena					
						ADD_REP_POINTS_AND_RANK_BAR_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, iDeathmatchCelebrationXP, iCurrentRP, 
												   	   iRPToReachCurrentLvl, iRPToReachNextLvl, iCurrentLvl, iNextLvl)					
					ENDIF
				ENDIF
				
				dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_DISPLAY_TIME * 2 //2 screens are guaranteed.					
				
				IF iCurrentRP + iDeathmatchCelebrationXP > iRPToReachNextLvl
					dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += ((CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 2) + (CELEBRATION_SCREEN_STAT_DISPLAY_TIME / 4))
				ENDIF
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName)
				SHOW_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName)
				
				//If the cam failed to create earlier then the effects were delayed, do them here instead.
				IF NOT DOES_CAM_EXIST(dmVarsPassed.camEndScreen)
					IF bLocalPlayerOK
						ANIMPOSTFX_STOP_ALL()
					ENDIF
					
					IF SHOULD_POSTFX_BE_WINNER_VERSION(playerBDPassed, serverBDpassed)
						PLAY_CELEB_WIN_POST_FX()
					ELSE
						PLAY_CELEB_LOSE_POST_FX()
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					
					//1626331 - If the celebration cam didn't successfully create then turn the ped to face the game cam.
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						IF bLocalPlayerOK
						AND NOT IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							TASK_TURN_PED_TO_FACE_COORD(LocalPlayerPed, GET_FINAL_RENDERED_CAM_COORD())
						ELSE
							SET_SKYFREEZE_FROZEN()
							
							IF bLocalPlayerOK
								SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Audio scenes: see B*1642903 for implementation notes.
				IF CONTENT_IS_USING_ARENA()
					IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
						START_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
					ENDIF
				ELIF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
					STOP_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
				ENDIF
				
				START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
				PRINTLN("[WJK] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
				
				RESET_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTimer)
				
				CLEAR_HELP()
				
				SET_CHAT_ENABLED_FOR_CELEBRATION_SCREEN(TRUE)
				RESET_CELEBRATION_PRE_LOAD(dmVarsPassed.sCelebrationData)
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - setting dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING")
				
				dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING")
			
			ELSE
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.sCelebrationData.timerDeathPostFxDelay)
						START_NET_TIMER(dmVarsPassed.sCelebrationData.timerDeathPostFxDelay)
						PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, started timer timerDeathPostFxDelay.")
					ELSE
						IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.timerDeathPostFxDelay, 1000)
							PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - DeathFailMPIn is running, timer timerDeathPostFxDelay >= 1000. Calling CLEAR_KILL_STRIP_DEATH_EFFECTS.")
							CLEAR_KILL_STRIP_DEATH_EFFECTS()
						ENDIF
					ENDIF	
				ENDIF
				IF NOT HAS_CELEBRATION_SCREEN_LOADED(dmVarsPassed.sCelebrationData)
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - NOT HAS_CELEBRATION_SCREEN_LOADED(dmVarsPassed.sCelebrationData)")
				ENDIF
				IF NOT bFetchedStatsFromServer
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - NOT bFetchedStatsFromServer")
				ENDIF
				IF NOT IS_SCREEN_FADED_IN()
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - NOT IS_SCREEN_FADED_IN()")
				ENDIF
				IF ANIMPOSTFX_IS_RUNNING("DeathFailMPIn")
					PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - ANIMPOSTFX_IS_RUNNING('DeathFailMPIn')")
				ENDIF
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
		
			PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING")

			DRAW_CELEBRATION_SCREEN_THIS_FRAME(dmVarsPassed.sCelebrationData, FALSE)
			PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - calling DRAW_CELEBRATION_SCREEN_THIS_FRAME.")
			
			//If we're going straight from the summary to a winner screen then do an early flash to help the transition.
			IF NOT dmVarsPassed.sCelebrationData.bTriggeredEarlyFlash
				//Detach the cam on the player if they're stationary to stop the weird swings if the player turns slightly.
				IF bLocalPlayerOK
				AND DOES_CAM_EXIST(dmVarsPassed.camEndScreen)				
					IF GET_ENTITY_SPEED(LocalPlayerPed) < 0.1
						DETACH_CAM(dmVarsPassed.camEndScreen)
						STOP_CAM_POINTING(dmVarsPassed.camEndScreen)
					ENDIF
				ENDIF
			
//				IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.sCelebrationTimer, dmVarsPassed.sCelebrationData.iEstimatedScreenDuration - (CELEBRATION_SCREEN_STAT_WIPE_TIME / 2))
				IF HAS_CELEBRATION_SHARD_FINISHED_FALL_AWAY()
					IF SHOULD_POSTFX_BE_WINNER_VERSION(playerBDPassed, serverBDpassed)
						ANIMPOSTFX_PLAY("MP_Celeb_Win_Out", 0, FALSE)
					ELSE
						ANIMPOSTFX_PLAY("MP_Celeb_Lose_Out", 0, FALSE)
					ENDIF
					
					START_CELEBRATION_CAMERA_ZOOM_TRANSITION(dmVarsPassed.camEndScreen)
					dmVarsPassed.sCelebrationData.bTriggeredEarlyFlash = TRUE
				ENDIF
			ENDIF
			
			//Once the timer finishes then progress: this depends on how many elements were added to the screen.
			IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.sCelebrationTimer, dmVarsPassed.sCelebrationData.iEstimatedScreenDuration)
				dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED")
				
				PRINTLN("[NETCELEBRATION] HAS_CELEBRATION_SUMMARY_FINISHED - dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED")
				
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CELEBRATION_ANIMS_LOADED(TEXT_LABEL_63 tl63_AnimDict1, TEXT_LABEL_63 tl63_AnimDict2, BOOL bFailsafe)

	IF bFailsafe
	
		PRINTLN("[NET_DEATHMATCH] CELEBRATION_ANIMS_LOADED, bFailsafe hit ")
	
		RETURN TRUE
	ENDIF

	IF IS_STRING_NULL_OR_EMPTY(tl63_AnimDict1)
	
		RETURN FALSE
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(tl63_AnimDict2)
	
		RETURN FALSE
	ENDIF
	
	REQUEST_ANIM_DICT(tl63_AnimDict1)
	REQUEST_ANIM_DICT(tl63_AnimDict2)

	IF HAS_ANIM_DICT_LOADED(tl63_AnimDict1) 
	AND HAS_ANIM_DICT_LOADED(tl63_AnimDict2) 
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Runs the race celebration screen showing who won and any bets won/lost.
/// RETURNS:
///    TRUE once the sequence has finished.
FUNC BOOL HAS_CELEBRATION_WINNER_FINISHED(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[], ServerBroadcastData_Leaderboard &serverBDpassedLDB, BOOL bKeepDrawingAfterFinished = FALSE)
	
	BOOL bArenaAnims

	IF CONTENT_IS_USING_ARENA()
		IF ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_PODIUM )
		OR ( GET_CELEB_TYPE(serverBDpassed) = CELEBRATION_ARENA_ANIM_FLAT )
			bArenaAnims = TRUE
		ENDIF
	ENDIF
	
	IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
		PRINTLN("[NETDEATHMATCH] HAS_CELEBRATION_WINNER_FINISHED - DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE is TRUE")
		dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
	
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	AND NOT bIsSCTV // So we can see the winner scene on the live feed.
	AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	AND NOT bArenaAnims
		PRINTLN("[NETDEATHMATCH] HAS_CELEBRATION_WINNER_FINISHED - IS_LOCAL_PLAYER_SPECTATOR_ONLY is TRUE")
		dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
	
		RETURN TRUE
	ENDIF
	
	HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
	
	PLAYER_INDEX winningPlayers[MAX_NUM_CELEBRATION_PEDS]
	INT iWinningPlayer
	BOOL bSuccessfullyGrabbedWinners = GET_LIST_OF_WINNERS_FOR_CELEBRATION_SCREEN(serverBDpassed, playerBDPassed, winningPlayers, dmVarsPassed.sCelebrationData.iNumPlayerFoundForWinnerScene, serverBDpassedLDB)
	TEXT_LABEL_63 tl63_AnimDict, tl63_AnimName
	TEXT_LABEL_63 tl63_IdleAnimDict, tl63_IdleAnimName
	
	//If the winning player/s left the game, then create the player as a fallback and display a "LOSER" screen instead.
	IF NOT bSuccessfullyGrabbedWinners
		winningPlayers[0] = LocalPlayer
		IF NOT dmVarsPassed.sCelebrationData.bCreateWinnerSceneEntities
			dmVarsPassed.sCelebrationData.iDisplayingLocalPlayerAsLoser = 1
			PRINTLN("[NETCELEBRATION] - could not grab list of winners, setting flag to indicate we are displaying fallback loser screen.")
		ENDIF
	ELSE
		IF NOT dmVarsPassed.sCelebrationData.bCreateWinnerSceneEntities
			dmVarsPassed.sCelebrationData.iDisplayingLocalPlayerAsLoser = 0
			PRINTLN("[NETCELEBRATION] - grabbed list of winners, setting flag to indicate we are not displaying fallback loser screen.")
		ENDIF
	ENDIF
	
	iWinningPlayer = NATIVE_TO_INT(winningPlayers[0])
	
	IF dmVarsPassed.sCelebrationData.bAllowPlayerNameToggles
		
		IF IS_XBOX360_VERSION()
		OR IS_PS3_VERSION()
			DISPLAY_CELEBRATION_PLAYER_NAMES(dmVarsPassed.sCelebrationData.pedWinnerClones, dmVarsPassed.sCelebrationData.tl31_pedWinnerClonesNames, dmVarsPassed.sCelebrationData.eCurrentStage, dmVarsPassed.sCelebrationData.sfCelebration)
		ELSE
			DRAW_NG_CELEBRATION_PLAYER_NAMES(	dmVarsPassed.sCelebrationData, dmVarsPassed.sCelebrationData.iDrawNamesStage, dmVarsPassed.sCelebrationData.iNumNamesToDisplay, 
												dmVarsPassed.sCelebrationData.pedWinnerClones, dmVarsPassed.sCelebrationData.tl31_pedWinnerClonesNames, 
												dmVarsPassed.sCelebrationData.eCurrentStage, dmVarsPassed.sCelebrationData.sfCelebration,
												dmVarsPassed.sCelebrationData.playerNameMovies, dmVarsPassed.sCelebrationData.bToggleNames)
		ENDIF
	
		DRAW_THE_PLAYER_LIST(dmVarsPassed.dpadVars)
		MAINTAIN_CELEBRATION_INSTRUCTIONAL_BUTTONS(dmVarsPassed.sCelebrationData)
		CELEB_SET_PLAYERS_INVISIBLE_THIS_FRAME()
		
	ENDIF
	
	HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(dmVarsPassed.sCelebrationData) // Fix for B* 2278693 - stop player models being seen during winner scene, only want ot see ped clones.
	
	#IF IS_DEBUG_BUILD
	STRING sStage = DEBUG_CELEB_STAGE(dmVarsPassed.sCelebrationData.eCurrentStage)
	PRINTLN("sStage = ", sStage)
	#ENDIF
	
	SWITCH dmVarsPassed.sCelebrationData.eCurrentStage
		CASE CELEBRATION_STAGE_SETUP
			
			REQUEST_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData)
	
			IF LOAD_WINNER_SCENE_INTERIOR(dmVarsPassed.sCelebrationData)
			
				IF bArenaAnims
				AND NOT HAS_NG_FAILSAFE_TIMER_EXPIRED(dmVarsPassed.sCelebrationData)
					
					IF NOT ARENA_ANIMS_READY(serverBDpassed.sCelebServer)
						PRINTLN("[NETCELEBRATION] [ARENA] HAS_RACE_CELEBRATION_WINNER_FINISHED  - ARENA_ANIMS_READY = FALSE ")
						RETURN FALSE
					ENDIF
				ENDIF
				
				IF HAS_CELEBRATION_SCREEN_LOADED(dmVarsPassed.sCelebrationData)
				AND CREATE_WINNER_SCENE_ENTITIES(serverBDpassed.sCelebServer, dmVarsPassed.sCelebrationData, winningPlayers, DEFAULT, NOT bSuccessfullyGrabbedWinners, NULL, CONTENT_IS_USING_ARENA(), FALSE, IS_BIT_SET(dmVarsPassed.iDmBools2, DM_CELEB_GRABBED_VEH), FALSE, DUMMY_MODEL_FOR_SCRIPT, GlobalplayerBD[iWinningPlayer].iInteractionAnim)
					
					IF bArenaAnims
			
						GET_ARENA_CELEB_ANIMS(serverBDpassed.sCelebServer, tl63_AnimDict, tl63_AnimName, 0)
						
						PRINTLN("[NET_DEATHMATCH] - [NET_CELEBRATION] - initial anim loading - GET_ARENA_CELEB_ANIMS, tl63_AnimDict = ", tl63_AnimDict, " tl63_AnimName = ", tl63_AnimName, " at ", NATIVE_TO_INT(GET_NETWORK_TIME()))
					ELSE
						GET_CELEBRATION_ANIM_TO_PLAY(GlobalplayerBD[iWinningPlayer].iInteractionAnim, IS_PED_MALE(dmVarsPassed.sCelebrationData.pedWinnerClones[0]), FALSE, tl63_AnimDict, tl63_AnimName, INT_TO_ENUM(INTERACTION_ANIM_TYPES, GlobalplayerBD[iWinningPlayer].iInteractionType), INT_TO_ENUM(CREW_INTERACTIONS, GlobalplayerBD[iWinningPlayer].iCrewAnim))
					ENDIF
					GET_CELEBRATION_IDLE_ANIM_TO_USE(dmVarsPassed.sCelebrationData, tl63_IdleAnimDict, tl63_IdleAnimName, IS_PED_MALE(dmVarsPassed.sCelebrationData.pedWinnerClones[0]))
					
					IF CELEBRATION_ANIMS_LOADED(tl63_AnimDict, tl63_IdleAnimDict, HAS_NG_FAILSAFE_TIMER_EXPIRED(dmVarsPassed.sCelebrationData))
						
						IF NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimDict)
						AND NOT IS_STRING_NULL_OR_EMPTY(tl63_AnimName)
							dmVarsPassed.sCelebrationData.fAnimLength = GET_ANIM_DURATION(tl63_AnimDict, tl63_AnimName)
							PRINTLN("[NETCELEBRATION] - HAS_CELEBRATION_WINNER_FINISHED, CELEBRATION_ANIMS_LOADED = TRUE - fAnimLength = ", dmVarsPassed.sCelebrationData.fAnimLength)
						ENDIF
						
						RESET_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTransitionTimer)
						START_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTransitionTimer)
						
						STOP_CELEBRATION_SCREEN_ANIM(dmVarsPassed.sCelebrationData, "SUMMARY")
						FLASH_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, 0.333, 0.15, 0.333)

						//Cache the winner ID for later use in the anims, so that if the player leaves we still have the clone and anim array index.
						IF bSuccessfullyGrabbedWinners
						AND iWinningPlayer > -1
							dmVarsPassed.sCelebrationData.iWinnerPlayerID = iWinningPlayer
						ENDIF
						
						IF IS_THIS_A_MISSION()
						AND CONTENT_IS_USING_ARENA()
						
							PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - sCelebrationData.iWinnerPlayerID: ", dmVarsPassed.sCelebrationData.iWinnerPlayerID)
							
							INT iWinnerTeam
							PLAYER_INDEX piPlayerWinner
							piPlayerWinner = INT_TO_NATIVE(PLAYER_INDEX, dmVarsPassed.sCelebrationData.iWinnerPlayerID)
							
							IF piPlayerWinner != INVALID_PLAYER_INDEX()
							AND IS_NET_PLAYER_OK(piPlayerWinner, FALSE, FALSE)
								iWinnerTeam = GET_PLAYER_TEAM(piPlayerWinner)
								PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - piPlayerWinner: ", GET_PLAYER_NAME(piPlayerWinner))
								PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - sCelebrationData.iWinnerTeam: ", iWinnerTeam)
							ELSE
								PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - INVALID PLAYER INDEX...")
							ENDIF	
							
							IF iWinnerTeam > -1
							AND iWinnerTeam < FMMC_MAX_TEAMS
							AND IS_VALID_INTERIOR(dmVarsPassed.sCelebrationData.winnerSceneInterior)
								INT iColour
								STRING sEntitySetName
								INT i						
								FOR i = 0 TO 3
									iColour = g_FMMC_STRUCT.sArenaInfo.iArena_CrowdColour[iWinnerTeam]
									sEntitySetName = ""
									SWITCH i
										CASE 0
											sEntitySetName = "Set_Crowd_A"
										BREAK
										CASE 1
											sEntitySetName = "Set_Crowd_B"
										BREAK
										CASE 2
											sEntitySetName = "Set_Crowd_C"
										BREAK
										CASE 3
											sEntitySetName = "Set_Crowd_D"
										BREAK
									ENDSWITCH
									SET_INTERIOR_ENTITY_SET_TINT_INDEX(dmVarsPassed.sCelebrationData.winnerSceneInterior, sEntitySetName, iColour)
									
									iColour = g_FMMC_STRUCT.sArenaInfo.iArena_BandColour[iWinnerTeam]
									sEntitySetName = ""
									SWITCH i
										CASE 0
											sEntitySetName = "Set_Team_Band_A"
										BREAK
										CASE 1
											sEntitySetName = "Set_Team_Band_B"
										BREAK
										CASE 2
											sEntitySetName = "Set_Team_Band_C"
										BREAK
										CASE 3
											sEntitySetName = "Set_Team_Band_D"
										BREAK
									ENDSWITCH							
									SET_INTERIOR_ENTITY_SET_TINT_INDEX(dmVarsPassed.sCelebrationData.winnerSceneInterior, sEntitySetName, iColour)
									
									PRINTLN("[NETCELEBRATION] - LOAD_WINNER_SCENE_INTERIOR - Setting colour of ", sEntitySetName, " to ", iColour)
								ENDFOR
								REFRESH_INTERIOR(dmVarsPassed.sCelebrationData.winnerSceneInterior)
							ENDIF
						ENDIF

						CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Started flash.")

						dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_DOING_FLASH
					
					ENDIF
					
				ENDIF
			
				IF HAS_CELEBRATION_SCREEN_LOADED(dmVarsPassed.sCelebrationData)
					DRAW_CELEBRATION_SCREEN_THIS_FRAME(dmVarsPassed.sCelebrationData)
				ENDIF
			
			ENDIF
			
		BREAK
		
		CASE CELEBRATION_STAGE_DOING_FLASH
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(dmVarsPassed.sCelebrationData)
			
			IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.sCelebrationTransitionTimer, 375) OR IS_SCREEN_FADED_OUT()
				RESET_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTimer)

				IF bLocalPlayerOK
					SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					REMOVE_PED_HELMET(LocalPlayerPed, TRUE) // url:bugstar:2244903 - remove helmet mid-flash.
				ENDIF

				ANIMPOSTFX_STOP_ALL()
				SET_SKYFREEZE_CLEAR(TRUE)
				SET_SKYBLUR_CLEAR()
				IF CONTENT_IS_USING_ARENA()
					IF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
						START_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
					ENDIF
				ELIF NOT IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
					START_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
				ENDIF
				
				dmVarsPassed.sCelebrationData.bUseFullBodyWinnerAnim = SHOULD_WINNER_SCREEN_USE_FULL_BODY_ANIM(dmVarsPassed.sCelebrationData, iWinningPlayer)
				PLACE_WINNER_SCENE_CAMERA(serverBDpassed.sCelebServer, dmVarsPassed.sCelebrationData, dmVarsPassed.camEndScreen, DEFAULT, iWinningPlayer, dmVarsPassed.sCelebrationData.bUseFullBodyWinnerAnim)
				FORCE_WEATHER_AND_TIME_FOR_CELEBRATION_WINNER()
				serverBDPassed.iTimeOfDay = TIME_OFF

				dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_TRANSITIONING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_TRANSITIONING
			BOOL bUseTextLabel
			DRAW_CELEBRATION_SCREEN_THIS_FRAME(dmVarsPassed.sCelebrationData)
			
			IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.sCelebrationTimer, 375)
				SET_HIDE_PLAYERS_FOR_CELEBRATION_WINNER_SCENE(dmVarsPassed.sCelebrationData)
			ENDIF
			
			IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.sCelebrationTimer, 750)
				STRING strScreenName, strBackgroundColour
				TEXT_LABEL_23 strWinnerName
				TEXT_LABEL_63 strCrewName
				JOB_WIN_STATUS eJobWinStatus
				INT iBetWinnings, iFinishPos
			
				//Retrieve all the required stats for the race end screen.
				strScreenName = "WINNER"
				iBetWinnings = playerBDPassed[iLocalPart].iBets
				dmVarsPassed.sCelebrationData.iEstimatedScreenDuration = CELEBRATION_SCREEN_STAT_WIPE_TIME * 2 
				
				strWinnerName = ""
				strCrewName = ""
				
				IF winningPlayers[0] != INVALID_PLAYER_INDEX()
				AND serverBDpassedLDB.leaderBoard[0].iParticipant > -1
				AND bSuccessfullyGrabbedWinners
					//strWinnerName = GET_PLAYER_NAME(winningPlayers[0])
					strWinnerName = serverBDpassed.tl63_ParticipantNames[serverBDpassedLDB.leaderBoard[0].iParticipant]
					strCrewName = GET_CREW_NAME_FOR_CELEBRATION_SCREEN(winningPlayers[0])
				ENDIF
				
				IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
				AND NOT IS_TDM_USING_FFA_SCORING()
					iFinishPos = GET_TEAM_RANK(serverBDpassed, playerBDPassed[iLocalPart].iTeam, serverBDpassed.iNumberOfTeams)
					strWinnerName = GET_TDM_NAME(serverBDpassed, playerBDPassed[iLocalPart].iTeam, FALSE, FALSE)
					bUseTextLabel = TRUE
					
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Setting bUseTextLabel to true.")
				ELSE
					iFinishPos = GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT())
					
					CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Not TDM")
				ENDIF
				
				IF iFinishPos = 1
					eJobWinStatus = JOB_STATUS_WIN
					strBackgroundColour = "HUD_COLOUR_FRIENDLY"
					PLAY_MISSION_COMPLETE_AUDIO("FRANKLIN_BIG_01")
				ELSE
					//NOTE: use the winner status in all cases except if the winner ped couldn't be retrieved.
					IF bSuccessfullyGrabbedWinners
						eJobWinStatus = JOB_STATUS_WIN
					ELSE
						eJobWinStatus = JOB_STATUS_LOSE
					ENDIF
					strBackgroundColour = "HUD_COLOUR_NET_PLAYER1"
					PLAY_MISSION_COMPLETE_AUDIO("GENERIC_FAILED")
				ENDIF	
			
			
				//Build the screen (1641310 - Winner screens last a little longer.
				CREATE_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, strBackgroundColour)
				SET_CELEBRATION_SCREEN_STAT_DISPLAY_TIME(dmVarsPassed.sCelebrationData, CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME - CELEBRATION_SCREEN_STAT_WIPE_TIME)
				
				IF CONTENT_IS_USING_ARENA() 
				
					TEXT_LABEL_15 tl15Skill 
					STRING sSkillLevelTitle
					
					tl15Skill = GET_ARENA_SKILL_LEVEL_TITLE(GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(winningPlayers[0]))	
					sSkillLevelTitle = TEXT_LABEL_TO_STRING(tl15Skill)				
					
					IF IS_STRING_NULL_OR_EMPTY(dmVarsPassed.tl23WinnerVehicle)
						IF IS_MODEL_VALID(dmVarsPassed.sCelebrationData.vehicleSetupMp.VehicleSetup.eModel)
							dmVarsPassed.tl23WinnerVehicle = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(dmVarsPassed.sCelebrationData.vehicleSetupMp.VehicleSetup.eModel)
							dmVarsPassed.tl23WinnerVehicle = GET_FILENAME_FOR_AUDIO_CONVERSATION(dmVarsPassed.tl23WinnerVehicle)
						ENDIF
					ENDIF
					
					PRINTLN("[NETCELEBRATION] ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN, 5456221 tl15Skill = ", tl15Skill, " sSkillLevelTitle = ", sSkillLevelTitle, " dmVarsPassed.tl23WinnerVehicle = ", dmVarsPassed.tl23WinnerVehicle, " strWinnerName = ", strWinnerName, " bUseTextLabel: ", bUseTextLabel)
				
					ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, strWinnerName, (eJobWinStatus = JOB_STATUS_WIN), dmVarsPassed.tl23WinnerVehicle, sSkillLevelTitle, bUseTextLabel, PICK_INT(IS_ARENA_WARS_JOB(TRUE), GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(winningPlayers[0]), -1))
				ELSE
					
					PRINTLN("[NETCELEBRATION] ADD_ARENA_WINNER_TO_CELEBRATION_SCREEN, strScreenName = ", strScreenName, " strWinnerName = ", strWinnerName, " strCrewName = ", strCrewName, " iBetWinnings = ", iBetWinnings, " bUseTextLabel: ", bUseTextLabel)
					
					IF winningPlayers[1] != INVALID_PLAYER_INDEX() //1637254 - If multiple players won then don't show specifics on the winner display.
						strWinnerName = ""
						
						//Deathmatches currently don't have team names, so we don't need to check this.
						//IF serverBDpassed.iWinningTeam > -1
						//AND serverBDpassed.iWinningTeam < FMMC_MAX_TEAMS
						//	strWinnerName = g_sMission_TeamName[serverBDpassed.iWinningTeam] 
						//ENDIF
					
						ADD_WINNER_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, eJobWinStatus, strWinnerName, "", "", iBetWinnings, DEFAULT, TRUE, DEFAULT, bUseTextLabel)
					ELSE
						ADD_WINNER_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, eJobWinStatus, strWinnerName, strCrewName,  "", iBetWinnings, FALSE, DEFAULT, DEFAULT, bUseTextLabel)
					ENDIF 
				
				ENDIF
				
				dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME //Winner screen is guaranteed.
				
				//Only add second screen timings if the betting results are going to be displayed.
				IF iBetWinnings != 0
					dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_WINNER_STAT_DISPLAY_TIME
				ENDIF
				
				dmVarsPassed.sCelebrationData.iEstimatedScreenDuration += CELEBRATION_SCREEN_STAT_WIPE_TIME * 2
				
				ADJUST_ESTIMATED_TIME_BASED_ON_ANIM_LENGTH(dmVarsPassed.sCelebrationData)
				
				START_AUDIO_SCENE("MP_JOB_CHANGE_RADIO_MUTE")
				PRINTLN("[WJK] - called START_AUDIO_SCENE(MP_JOB_CHANGE_RADIO_MUTE).")
				
				ADD_BACKGROUND_TO_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName, 75)
				SHOW_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, strScreenName)
				
				RESET_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTimer)
				START_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTimer)
				
				SET_SKYFREEZE_CLEAR(TRUE)
				
				CDEBUG1LN(DEBUG_MISSION, "[NETCELEBRATION] HAS_CELEBRATION_WINNER_FINISHED - Starting playback of winner screen.")
				RESET_CELEBRATION_PRE_LOAD(dmVarsPassed.sCelebrationData)
				dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_PLAYING
			ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_PLAYING
			VECTOR vCamRot
			vCamRot = GET_FINAL_RENDERED_CAM_ROT()
		
			//Don't draw the screen if we're in skycam.
			IF ABSF(vCamRot.x) < 45.0
			#IF IS_DEBUG_BUILD
			AND NOT bFreezeCelebration
			#ENDIF
			
				DRAW_CELEBRATION_SCREEN_USING_3D_BACKGROUND(dmVarsPassed.sCelebrationData, <<0.0, -0.5, 0.0>>, <<0.0, 0.0, 0.0>>, <<10.0, 5.0, 5.0>>)
				
				DRAW_BLACK_RECT_FOR_WINNER_SCREEN_END_TRANSITION(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.sCelebrationData.sCelebrationTimer.Timer), 
																 dmVarsPassed.sCelebrationData.iEstimatedScreenDuration)
																 
				//Block the switch PostFX as it comes on during the winner screen.
				ANIMPOSTFX_STOP_ALL()
				RESET_ADAPTATION(1)
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF NOT bFreezeCelebration
			#ENDIF
			
				//Once the timer finishes then progress: this depends on how many elements were added to the screen.
				IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.sCelebrationData.sCelebrationTimer, dmVarsPassed.sCelebrationData.iEstimatedScreenDuration)
					IF NOT bKeepDrawingAfterFinished
						dmVarsPassed.sCelebrationData.eCurrentStage = CELEBRATION_STAGE_FINISHED
					ENDIF
					
					RESET_WEATHER_AND_TIME_AFTER_CELEBRATION_WINNER()
					
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND NOT bArenaAnims
						VECTOR vCamCoords
						vCamCoords = GET_CAM_COORD(dmVarsPassed.camEndScreen)
						vCamCoords.z = 1000.0
						SET_CAM_COORD(dmVarsPassed.camEndScreen, vCamCoords)
						SET_ENTITY_COORDS(LocalPlayerPed, vCamCoords, FALSE)
						FREEZE_ENTITY_POSITION(LocalPlayerPed, TRUE)
						PRINTLN("[NETCELEBRATION] - put winner cam and player ped at ", vCamCoords)
					ENDIF
					
					dmVarsPassed.sCelebrationData.bAllowPlayerNameToggles = FALSE
					
					IF bIsSCTV
						CLEAR_SPECTATOR_OVERRIDE_COORDS()
					ENDIF
					
					PRINTLN("[NETCELEBRATION] - RETURN TRUE 1 ")
					
					RETURN TRUE
				
				ELSE
					
					dmVarsPassed.sCelebrationData.bAllowPlayerNameToggles = TRUE
					
				ENDIF
			
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		BREAK
		
		CASE CELEBRATION_STAGE_FINISHED
			IF bIsSCTV
				CLEAR_SPECTATOR_OVERRIDE_COORDS()
			ENDIF
			PRINTLN("[NETCELEBRATION] - RETURN TRUE 2 ")
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	//1645969 - The anim needs to work if the winning player leaves half way through, their ID will be cached so we only need to check the following:
	// - The clone was created successfully.
	// - The clone is not the player.
	// - The clone is the player and the player won the race (i.e. we're not doing the fallback "Loser" screen).
	IF dmVarsPassed.sCelebrationData.eCurrentStage > CELEBRATION_STAGE_SETUP
	AND dmVarsPassed.sCelebrationData.eCurrentStage < CELEBRATION_STAGE_FINISHED
		IF dmVarsPassed.sCelebrationData.iDisplayingLocalPlayerAsLoser = 0	
		
			IF bArenaAnims
				// Repeat through players the podium to play their anims
				INT iLoop
				REPEAT MAX_CELEB_PLAYERS_TO_ANIMATE(serverBDpassed.sCelebServer) iLoop
					IF NOT IS_PED_INJURED(dmVarsPassed.sCelebrationData.pedWinnerClones[iLoop])

						IF NATIVE_TO_INT(winningPlayers[iLoop]) > -1
						AND NATIVE_TO_INT(winningPlayers[iLoop]) < NUM_NETWORK_PLAYERS
							PRINTLN("[Paired_Anims] - bArenaAnims, calling UPDATE_CELEBRATION_WINNER_ANIMS for winner ", iLoop)
							UPDATE_CELEBRATION_WINNER_ANIMS_ARENA(serverBDpassed.sCelebServer, 
																	dmVarsPassed.sCelebrationData, 
																	dmVarsPassed.sCelebrationData.pedWinnerClones[iLoop], 
																	NATIVE_TO_INT(winningPlayers[iLoop]), iLoop)

						ELSE
							#IF IS_DEBUG_BUILD
							INT iTemp = NATIVE_TO_INT(winningPlayers[iLoop])
							PRINTLN("[Paired_Anims] - not calling UPDATE_CELEBRATION_WINNER_ANIMS for winner, iLoop = ", iLoop, ". winningPlayers[iLoop] = ", iTemp)
							#ENDIF
						ENDIF

					ENDIF
				ENDREPEAT
		
			ELIF DOES_ENTITY_EXIST(dmVarsPassed.sCelebrationData.pedWinnerClones[0])
				IF NOT IS_PED_INJURED(dmVarsPassed.sCelebrationData.pedWinnerClones[0])
					IF dmVarsPassed.sCelebrationData.pedWinnerClones[0] != LocalPlayerPed
					OR (dmVarsPassed.sCelebrationData.pedWinnerClones[0] = LocalPlayerPed AND bSuccessfullyGrabbedWinners)
						IF DOES_CAM_EXIST(dmVarsPassed.camEndScreen)
							IF IS_CAM_RENDERING(dmVarsPassed.camEndScreen)
								UPDATE_CELEBRATION_WINNER_ANIMS(dmVarsPassed.sCelebrationData, dmVarsPassed.sCelebrationData.pedWinnerClones[0], dmVarsPassed.sCelebrationData.iWinnerPlayerID, 0, FALSE, FALSE, FALSE, PAIRED_CELEBRATION_GENDER_COMBO_NOT_SET)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
				
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LINE_OF_SIGHT_CLEAR_TO_CENTRE_POINT(JOB_INTRO_CUT_DATA &jobIntroData, VECTOR vTeamCentre)

	INT iHitCount
	VECTOR vHitPoint
	VECTOR vHitNormal
	ENTITY_INDEX eiHitEntity
	
	IF jobIntroData.stiPlayerSightTest = NULL
		
		INT         iFlags      = SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_FOLIAGE
		
		INT iMaxCamType
		
		VECTOR vVec1, vVec2
		
		iMaxCamType = 6
		
		jobIntroData.iCameraType = GET_RANDOM_INT_IN_RANGE(0, iMaxCamType) //Camera area we are going to check
		
		vVec1 = 	vTeamCentre+<<0,0,0.5>>
		vVec2 = 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTeamCentre,GET_ENTITY_HEADING(LocalPlayerPed),GET_TEAM_CAM_OFFSET(1,jobIntroData.iCameraType, TRUE)) //GET_TEAM_CAM_OFFSET(1,jobIntroData.iCameraType)

		jobIntroData.stiPlayerSightTest = START_SHAPE_TEST_CAPSULE(vVec1,vVec2,0.1,iFlags,LocalPlayerPed,SCRIPT_SHAPETEST_OPTION_DEFAULT)
		
	ELSE
	
		SHAPETEST_STATUS eStatus = GET_SHAPE_TEST_RESULT(jobIntroData.stiPlayerSightTest, iHitCount, vHitPoint, vHitNormal, eiHitEntity)
		
		SWITCH (eStatus)
		
			CASE SHAPETEST_STATUS_RESULTS_READY
			
				IF iHitCount = 0
					NET_PRINT("IS_LINE_OF_SIGHT_CLEAR_TO_CENTRE_POINT: SHAPETEST_STATUS_RESULTS_READY - jobIntroData.bCamAreaClear = TRUE ") NET_NL()
					jobIntroData.iCameraAttempts ++
					jobIntroData.bCamSeePoint = TRUE
				ELSE
					jobIntroData.iCameraAttempts ++
					PRINTLN("IS_LINE_OF_SIGHT_CLEAR_TO_CENTRE_POINT jobIntroData.iCameraAttempts = ", jobIntroData.iCameraAttempts)
					jobIntroData.stiPlayerSightTest = NULL
					jobIntroData.bCamSeePoint = FALSE
				ENDIF
				
				IF jobIntroData.iCameraAttempts > 5
				OR jobIntroData.bCamSeePoint = TRUE
					jobIntroData.stiPlayerSightTest = NULL
					RETURN TRUE
				ENDIF
				
			BREAK
			
			CASE SHAPETEST_STATUS_RESULTS_NOTREADY
			BREAK
			
			CASE SHAPETEST_STATUS_NONEXISTENT
				jobIntroData.bCamSeePoint = FALSE
				jobIntroData.stiPlayerSightTest = NULL
				RETURN TRUE
				
            BREAK

		
		ENDSWITCH

	
	ENDIF
	
	RETURN FALSE
	
	
ENDFUNC

FUNC BOOL IS_AREA_FOR_TEAM_CUT_CLEAR(JOB_INTRO_CUT_DATA &jobIntroData, VECTOR vTeamCentre)

	INT iHitCount
	VECTOR vHitPoint
	VECTOR vHitNormal
	ENTITY_INDEX eiHitEntity
	
	PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR - jobIntroData.iCameraAttempts = ", jobIntroData.iCameraAttempts)

	IF jobIntroData.stiShapeTest = NULL
		
		INT         iFlags      = SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_FOLIAGE
		
		VECTOR vCameraVec1, vCameraVec2
				
		vCameraVec1 = 	GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTeamCentre,GET_ENTITY_HEADING(LocalPlayerPed),GET_TEAM_CAM_OFFSET(1,jobIntroData.iCameraType, TRUE)) //GET_TEAM_CAM_OFFSET(1,jobIntroData.iCameraType)
		vCameraVec2 = 	 GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vTeamCentre,GET_ENTITY_HEADING(LocalPlayerPed),GET_TEAM_CAM_OFFSET(2,jobIntroData.iCameraType, TRUE)) //GET_TEAM_CAM_OFFSET(2,jobIntroData.iCameraType)
		PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR vCameraVec1 = ", vCameraVec1)
		PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR vCameraVec2 = ", vCameraVec2)
		
		IF jobIntroData.iCameraAttempts % 2 = 0
			VECTOR vTemp = vCameraVec1
			vCameraVec1 = vCameraVec2
			vCameraVec2 = vTemp
			PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR - Doing reverse shapetest this time!")
		ENDIF
		
		jobIntroData.stiShapeTest = START_SHAPE_TEST_CAPSULE(vCameraVec1,vCameraVec2,0.5,iFlags,LocalPlayerPed,SCRIPT_SHAPETEST_OPTION_DEFAULT)
	ELSE
	
		SHAPETEST_STATUS eStatus = GET_SHAPE_TEST_RESULT(jobIntroData.stiShapeTest, iHitCount, vHitPoint, vHitNormal, eiHitEntity)
		
		SWITCH (eStatus)
		
			CASE SHAPETEST_STATUS_RESULTS_READY
			
				IF iHitCount = 0
					NET_PRINT("IS_AREA_FOR_TEAM_CUT_CLEAR: SHAPETEST_STATUS_RESULTS_READY - jobIntroData.bCamAreaClear = TRUE ") NET_NL()
					jobIntroData.bCamAreaClear = TRUE
					jobIntroData.iCameraAttempts = 0
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR RETURN TRUE : jobIntroData.iCameraType = ", jobIntroData.iCameraType)
					RETURN TRUE
				ELSE
					NET_PRINT("IS_AREA_FOR_TEAM_CUT_CLEAR: SHAPETEST_STATUS_RESULTS_READY - jobIntroData.bCamAreaClear = FALSE ") NET_NL()
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR iHitCount = ", iHitCount)
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR vHitPoint.x = ", vHitPoint.x)
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR vHitPoint.y = ", vHitPoint.y)
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR vHitPoint.z = ", vHitPoint.z)
					//jobIntroData.iCameraAttempts ++
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR jobIntroData.iCameraAttempts = ", jobIntroData.iCameraAttempts)
					jobIntroData.stiShapeTest = NULL
					jobIntroData.bCamAreaClear = FALSE
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR RETURN TRUE : jobIntroData.bCamAreaClear = FALSE ")
					RETURN TRUE
				ENDIF
				
				/*
				//IF jobIntroData.iCameraAttempts > 6
				IF jobIntroData.bCamAreaClear = TRUE
					PRINTLN("IS_AREA_FOR_TEAM_CUT_CLEAR RETURN TRUE : jobIntroData.iCameraType = ", jobIntroData.iCameraType)
					jobIntroData.stiShapeTest = NULL
					jobIntroData.iCameraAttempts = 0
					RETURN TRUE
				ELSE
					RETURN TRUE
				ENDIF
				*/
				
			BREAK
			
			CASE SHAPETEST_STATUS_RESULTS_NOTREADY
			BREAK
			
			CASE SHAPETEST_STATUS_NONEXISTENT
				NET_PRINT("IS_AREA_FOR_TEAM_CUT_CLEAR: SHAPETEST_STATUS_NONEXISTENT") NET_NL()
				jobIntroData.bCamAreaClear = FALSE
				jobIntroData.stiShapeTest = NULL
				RETURN TRUE
				
            BREAK

		
		ENDSWITCH

	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL TEAM_CAMERA_CHECKS(JOB_INTRO_CUT_DATA &jobIntroData, VECTOR vTeamCentre)

	IF jobIntroData.bCamSeePoint = TRUE
	OR IS_LINE_OF_SIGHT_CLEAR_TO_CENTRE_POINT(jobIntroData,vTeamCentre)
		IF jobIntroData.bCamSeePoint = FALSE
			PRINTLN("TEAM_CAMERA_CHECKS RETURN FALSE - jobIntroData.bCamSeePoint = FALSE ")
			RETURN FALSE
		ELSE
			IF IS_AREA_FOR_TEAM_CUT_CLEAR(jobIntroData,vTeamCentre)
				IF jobIntroData.bCamAreaClear = FALSE
					IF jobIntroData.iCameraAttempts > 5
						PRINTLN("TEAM_CAMERA_CHECKS RETURN FALSE - jobIntroData.iCameraAttempts > 5 ")
						RETURN FALSE
					ELSE
						PRINTLN("TEAM_CAMERA_CHECKS - jobIntroData.iCameraAttempts < 5 - TRY AGAIN - jobIntroData.bCamSeePoint = FALSE")
						jobIntroData.bCamSeePoint = FALSE
					ENDIF
				ELSE
					PRINTLN("TEAM_CAMERA_CHECKS RETURN FALSE - IS_AREA_FOR_TEAM_CUT_CLEAR - jobIntroData.bCamAreaClear = TRUE ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF jobIntroData.bCamSeePoint = TRUE
	AND jobIntroData.bCamAreaClear = TRUE
		PRINTLN("TEAM_CAMERA_CHECKS RETURN TRUE ")
		RETURN TRUE
	ENDIF
	
	PRINTLN("TEAM_CAMERA_CHECKS RETURN FALSE - GENERAL ")
	RETURN FALSE
	
ENDFUNC 

FUNC BOOL WORK_OUT_CENTRE_POINT(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	
	VECTOR vPlayerVec, vTeamVecCent
	
	vPlayerVec = vLocalPlayerPosition
	vTeamVecCent = serverBDpassed.vTeamCentreStartPoint[playerBDPassed[iLocalPart].iTeam]
	
	PRINTLN("vPlayerVec = ", vPlayerVec)
	PRINTLN("serverBDpassed.vTeamCentreStartPoint[playerBDPassed[iLocalPart].iTeam] =  ", vTeamVecCent)
	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("WORK_OUT_CENTRE_POINT RETURN FALSE - IS_PLAYER_ON_IMPROMPTU_DM() ")
		RETURN FALSE
	ENDIF
	
	IF serverBDPassed.iDeathmatchType = DEATHMATCH_TYPE_FFA
		PRINTLN("WORK_OUT_CENTRE_POINT RETURN FALSE - DEATHMATCH_TYPE_FFA ")
		RETURN FALSE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(vPlayerVec,vTeamVecCent) < 5.0
		PRINTLN("WORK_OUT_CENTRE_POINT RETURN TRUE ")
		RETURN TRUE
	ELSE
		FLOAT fFailDis
		fFailDis = GET_DISTANCE_BETWEEN_COORDS(vPlayerVec,vTeamVecCent)
		PRINTLN("GET_DISTANCE_BETWEEN_COORDS > 5.0 =  ", fFailDis)
	ENDIF
	
	PRINTLN("WORK_OUT_CENTRE_POINT RETURN FALSE - GENERAL ")
	RETURN FALSE

ENDFUNC

FUNC TEXT_LABEL_23 GET_MATCH_ID()

	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		RETURN "BM_ONE_DM"
	ENDIF

	RETURN g_FMMC_STRUCT.tl31LoadedContentID
ENDFUNC

PROC STORE_CASH_EARNED_DURING_DM(PlayerBroadcastData &playerBDPassed[])
	INT iScriptTransactionIndex
	INT iCurrentCash
	// Give bounty cash back
	INT	iBountyCash = GlobalplayerBD_FM[NETWORK_PLAYER_ID_TO_INT()].iPlayerBounty 
	iBountyCash = MULTIPLY_CASH_BY_TUNABLE(iBountyCash)
	RESET_BOUNTY()
	// Total up
	iCurrentCash 	= (playerBDPassed[iLocalPart].iJobCash + iBountyCash)
	PRINTLN(" DO_PLAYER_FINISHING_TOTALS, STORE_CASH_EARNED_DURING_DM, iCurrentCash = ", iCurrentCash, " iBountyCash = ", iBountyCash)
	IF iCurrentCash > 0
		IF CONTENT_IS_USING_ARENA()
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ARENA_WAR, iCurrentCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_ARENA_WAR(iCurrentCash, 0, 0, 0)
			ENDIF
		ELSE
			IF USE_SERVER_TRANSACTIONS()
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iCurrentCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
				g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
			ELSE
				NETWORK_EARN_FROM_JOB(iCurrentCash, g_FMMC_STRUCT.tl31LoadedContentID)
			ENDIF
		ENDIF
		ADD_TO_JOB_CASH_EARNINGS(iCurrentCash)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_QUARTER_REWARDS(ServerBroadcastData &serverBDpassed)
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed)
	AND GET_DM_TOTAL_TIME(serverBDpassed) < QUARTER_TIME
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
	
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_AWARDS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		IF INSUFFICIENT_PLAYERS(serverBDpassed)
			IF HAS_NET_TIMER_STARTED(serverBDpassed.timeServerMainDeathmatchTimer)		
				IF NOT HAS_NET_TIMER_EXPIRED(serverBDpassed.timeServerMainDeathmatchTimer, 30000)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_KING_OF_THE_HILL()
		AND g_fKOTHIdleCounter > 0.0
		
			PRINTLN("[KOTH] SHOULD_BLOCK_AWARDS - fTimeSpentCheckingIdle = ", dmVarsPassed.fTimeSpentCheckingIdle, " || g_fKOTHIdleCounter = ", g_fKOTHIdleCounter)
			
			//IF g_fKOTHIdleCounter > g_sMPTunables.fKOTH_IDLE_TIME_PERCENTAGE_FOR_LOSING_AWARDS
			IF KOTH_IDLE_FOR_TOO_LONG(dmVarsPassed)
				PRINTLN("[KOTH] SHOULD_BLOCK_AWARDS - Returning true because g_fKOTHIdleCounter is ", g_fKOTHIdleCounter)
				RETURN TRUE
			ELSE
				PRINTLN("[KOTH] SHOULD_BLOCK_AWARDS - Fine for now || g_fKOTHIdleCounter is ", g_fKOTHIdleCounter)
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC ASSIGN_LEADERBOARD_PLAYER_FINISHING_STATS(PlayerBroadcastData &playerBDPassed[])
	
	INT iPart = iLocalPart
	
	playerBDPassed[iPart].sCelebrationStats.iLocalPlayerKills = playerBDPassed[iPart].iKills
	g_MissionControllerserverBD_LB.sleaderboard[iPart].iKills += playerBDPassed[iPart].iKills	
	
	playerBDPassed[iPart].sCelebrationStats.iLocalPlayerDeaths = playerBDPassed[iPart].iDeaths
	g_MissionControllerserverBD_LB.sleaderboard[iPart].iDeaths += playerBDPassed[iPart].iDeaths
	
	playerBDPassed[iPart].sCelebrationStats.iLocalPlayerHeadshots	= playerBDPassed[iPart].iHeadshots
	g_MissionControllerserverBD_LB.sleaderboard[iPart].iHeadshots += playerBDPassed[iPart].iHeadshots
	
	playerBDPassed[iPart].sCelebrationStats.iLocalPlayerScore = playerBDPassed[iPart].iScore
	g_MissionControllerserverBD_LB.sleaderboard[iPart].iScore += playerBDPassed[iPart].iScore
	
ENDPROC

PROC DO_PLAYER_FINISHING_TOTALS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
		IF CONTENT_IS_USING_ARENA()
		AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
			HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(0, IS_THIS_TEAM_DEATHMATCH(serverBDpassed), TRUE, DEFAULT, DEFAULT, DEFAULT, serverBDpassed.iNumberOfTeams)
		ENDIF
		PRINTLN("DO_PLAYER_FINISHING_TOTALS, EXIT, DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE ")
		EXIT
	ENDIF
	
	ASSIGN_LEADERBOARD_PLAYER_FINISHING_STATS(playerBDPassed)
	
	IF SHOULD_BLOCK_AWARDS(serverBDpassed, dmVarsPassed)
		PRINTLN("DO_PLAYER_FINISHING_TOTALS, EXIT, SHOULD_BLOCK_AWARDS ")
		
		EXIT
	ENDIF
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	
	STORE_BETTING_WINNINGS(serverBDpassed, playerBDPassed, serverBDpassedLDB)
	STORE_CASH_EARNED_DURING_DM(playerBDPassed)
	INT iTotalCash, iWinnerBonus
	FLOAT fTunable, fTotal, fImpromptuFee
	BOOL bShouldQuarterRewards = SHOULD_QUARTER_REWARDS(serverBDpassed)
	BOOL bTimedOut
	INT iRewardCash
	
	HANDLE_END_OF_DM_JOB_POINTS(serverBDpassed, playerBDPassed, serverBDpassedLDB)

	IF IS_PLAYER_ON_IMPROMPTU_DM() // 1380283
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		IF DID_I_WIN_IMPROMPTU(serverBDpassed, playerBDPassed)
			fImpromptuFee = (TO_FLOAT(ONE_ON_ONE_ENTRY_FEE) * g_sMPTunables.fImpromptuDmEntryMultiplier)
			PRINTLN("[CS_IMPROMPTU] DO_PLAYER_FINISHING_TOTALS, WINNER ")
			iTotalCash = (ROUND(fImpromptuFee) *2) // Both player's cash in the pot
		ENDIF		
	ELSE
		HANDLE_END_OF_DM_XP(serverBDpassed, playerBDPassed, dmVarsPassed, bShouldQuarterRewards, serverBDpassedLDB)

		SCRIPT_TIMER stTimer
		START_NET_TIMER(stTimer)
		stTimer.Timer = GET_NETWORK_TIME()
		
		INT iFinishPos = GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT())
		INT iTeam = playerBDPassed[iLocalPart].iTeam
		// 1685938
		IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		AND NOT IS_TDM_USING_FFA_SCORING()
			INT iTeamPos = GET_TEAM_POSITION(serverBDpassed, iTeam)
			IF DID_I_WIN(serverBDpassed, serverBDpassedLDB)
				iFinishPos = TEAM_WON_REWARD_FINISH_POS
				iWinnerBonus = WINNER_BONUS
				PRINTLN("[GET_END_OF_JOB_CASH] iWinnerBonus = ", WINNER_BONUS)
			ELIF iTeamPos = 2
				iFinishPos = TEAM_2ND_PLACE_FINISH_POS
				PRINTLN("[GET_END_OF_JOB_CASH] 2ND = ", TEAM_2ND_PLACE_FINISH_POS)
			ELIF iTeamPos = 3
				iFinishPos = TEAM_3RD_PLACE_FINISH_POS
				PRINTLN("[GET_END_OF_JOB_CASH] 3RD = ", TEAM_3RD_PLACE_FINISH_POS)
			ELSE
				iFinishPos = TEAM_4TH_PLACE_FINISH_POS
				PRINTLN("[GET_END_OF_JOB_CASH] 4TH = ", TEAM_4TH_PLACE_FINISH_POS)
			ENDIF
		ENDIF
		
		// 2490789
		IF serverBDPassed.iDuration > DM_DURATION_5
			IF HAS_DM_TIME_EXPIRED(serverBDpassed)
				bTimedOut = TRUE
			ENDIF
		ENDIF
		
//		IF CONTENT_IS_USING_ARENA()
//			IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
//				INT iTeamPos = GET_TEAM_POSITION(serverBDpassed, playerBDPassed[iLocalPart].iTeam)
//				HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(iTeamPos, TRUE, DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed))
//			ELSE
//				HANDLE_END_OF_ARENA_EVENT_PLAYER_CAREER_REWARDS(iFinishPos, FALSE, DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed))
//			ENDIF
//		ENDIF
		
		iTotalCash = GET_END_OF_JOB_CASH(serverBDpassed.iNumDMPlayers, GET_JOB_DECIDER(serverBDpassed, iTeam), serverBDpassed.iAverageRank, iFinishPos, dmVarsPassed.stRewardPercentageTimer, 
										stTimer, FALSE, REWARD_JOB_DM, DEFAULT, bTimedOut)
		// Give winners etc. as failsafe
		iTotalCash = (iTotalCash + iWinnerBonus)
	ENDIF
	
	IF IS_ARENA_WARS_JOB(TRUE)
		g_sArena_Telemetry_data.m_cashearned = iTotalCash
		PRINTLN("[ARENA][TEL] - DM Cash ", g_sArena_Telemetry_data.m_cashearned)
	ENDIF
	
	IF IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
		IF NOT DID_I_WIN(serverBDpassed, serverBDpassedLDB)
//			iTotalCash = (iTotalCash/2)
			
			// Cap
			IF iTotalCash < 100
				iTotalCash = 100
			ENDIF
			
			PRINTLN(" DO_PLAYER_FINISHING_TOTALS, HALVED_LOSING_TEAM  = ", iTotalCash)
		ENDIF
	ENDIF
	
	PRINTLN(" DO_PLAYER_FINISHING_TOTALS, iTotalCash = ", iTotalCash)
	IF iTotalCash > 0
		PRINTLN(" DO_PLAYER_FINISHING_TOTALS, iTotalCash = ", iTotalCash)
		
		fTunable = TO_FLOAT(iTotalCash)
		PRINTLN(" DO_PLAYER_FINISHING_TOTALS, fTunable = ", fTunable)
		
		IF bShouldQuarterRewards
			fTunable = (fTunable/4)
			PRINTLN(" DO_PLAYER_FINISHING_TOTALS, SHOULD_QUARTER_REWARDS, fTunable = ", fTunable)
		ENDIF
		
		fTotal = (fTunable * g_sMPTunables.fearnings_Win_Deathmatch_modifier) // 1616495
		PRINTLN(" DO_PLAYER_FINISHING_TOTALS, fTotal = ", fTotal)
		iTotalCash = ROUND(fTotal)
		PRINTLN(" DO_PLAYER_FINISHING_TOTALS, iTotalCash = ", iTotalCash)
		
		iTotalCash = MULTIPLY_CASH_BY_TUNABLE(iTotalCash)
		INT iScriptTransactionIndex
		IF IS_PLAYER_ON_IMPROMPTU_DM()
		OR IS_PLAYER_ON_BOSSVBOSS_DM()
			iRewardCash = iTotalCash
			
			IF IS_PLAYER_ON_IMPROMPTU_DM()
				PRINTLN("[AM MOVING TARGET][MAGNATE_GANG_BOSS] - Cash before boss cut: $", iTotalCash)
				GB_HANDLE_GANG_BOSS_CUT(iTotalCash)
				PRINTLN("[AM MOVING TARGET][MAGNATE_GANG_BOSS] - Cash after boss cut: $", iTotalCash)
			ENDIF
		
			IF iTotalCash > 0
				IF USE_SERVER_TRANSACTIONS()
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iTotalCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
					g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = "BM_ONE_DM"
				ELSE
					NETWORK_EARN_FROM_JOB(iTotalCash, "BM_ONE_DM")
				ENDIF
			ENDIF
		ELSE
			iRewardCash = iTotalCash
			IF IS_THIS_A_ROUNDS_MISSION()
				g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash += iTotalCash
				playerBDPassed[iLocalPart].sCelebrationStats.iLocalPlayerCash = iTotalCash
				
				iRewardCash = g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iCash
			ENDIF
			
			IF NOT IS_THIS_A_ROUNDS_MISSION()
			OR IS_ROUNDS_MISSION_OVER_FOR_JIP_PLAYER(LocalPlayer)
				IF CONTENT_IS_USING_ARENA()
					IF USE_SERVER_TRANSACTIONS()
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ARENA_WAR, iRewardCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
						g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
					ELSE
						NETWORK_EARN_ARENA_WAR(iRewardCash, 0, 0, 0)
					ENDIF
				ELSE
					IF USE_SERVER_TRANSACTIONS()
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_JOBS, iRewardCash, iScriptTransactionIndex, DEFAULT, DEFAULT)
						g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = g_FMMC_STRUCT.tl31LoadedContentID
					ELSE
						NETWORK_EARN_FROM_JOB(iRewardCash, g_FMMC_STRUCT.tl31LoadedContentID)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		playerBDPassed[iLocalPart].iJobCash = iRewardCash
		ADD_TO_JOB_CASH_EARNINGS(iTotalCash) // deliberately totalcash.
	ENDIF

	PRINTLN(" DO_PLAYER_FINISHING_TOTALS, playerBDPassed[iLocalPart].iJobCash = ", playerBDPassed[iLocalPart].iJobCash)
ENDPROC

PROC DO_SPECTATOR_PREP(PlayerBroadcastData &playerBDPassed[])
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
	OR bIsSCTV
		/*IF IS_SCREEN_FADED_OUT()		//removal for fade bug, was fading in too early
			DO_SCREEN_FADE_IN(2000)
		ENDIF*/
//		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
//			//NET_SET_PLAYER_CONTROL(LocalPlayer,FALSE,FALSE,TRUE,FALSE,FALSE,TRUE,FALSE,TRUE,TRUE)	
//			NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_CLEAR_TASKS | NSPC_LEAVE_CAMERA_CONTROL_ON)
//		ELSE
//			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
//		ENDIF
		PRINTLN("DO_SPECTATOR_PREP")
		SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_JOINED_AS_SPECTATOR)
		IF NOT bIsSCTV
			CLEAR_IDLE_KICK_TIME_OVERRIDDEN()
			SET_GO_TO_SPECIAL_SPECTATOR()
		ENDIF
		g_b_IsDMSpectator = TRUE		
	ENDIF
ENDPROC

PROC SET_PLAYERS_INVISIBLE(PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("SET_PLAYERS_INVISIBLE ")
		// Invisible
		NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS  | NSPC_FREEZE_POSITION)
	ENDIF
ENDPROC

PROC HIDE_RADAR_DURING_INITIAL_STAGES(PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		IF GET_INITIAL_STAGE(playerBDPassed) <> INITIAL_CAMERA_BLEND_OUT
		AND GET_INITIAL_STAGE(playerBDPassed) <> INITIAL_FADE_STAGE_FADED_IN
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		ENDIF
		DISABLE_FIRST_PERSON_CAMS()
	ENDIF
ENDPROC

FUNC BOOL DO_RANK_PREDICTION_FOR_DM(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	INT iSubMode = GET_SUBTYPE_FOR_SC(serverBDpassed)
	
	// Don't write if spectating
	IF IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR NOT IS_THIS_A_RSTAR_ACTIVITY()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("DO_RANK_PREDICTION_FOR_DM bypassed because IS_LOCAL_PLAYER_SPECTATOR_ONLY OR IS_PLAYER_ON_IMPROMPTU_DM() OR NOT IS_THIS_A_RSTAR_ACTIVITY()")
		PRINTLN("DO_RANK_PREDICTION_FOR_DM bypassed because IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE = ",IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE(g_FMMC_STRUCT.iAdversaryModeType))
		PRINTLN("DO_RANK_PREDICTION_FOR_DM bypassed because IS_THIS_ROCKSTAR_MISSION_NEW_DM_CARNAGE = ",IS_THIS_ROCKSTAR_MISSION_NEW_DM_PASS_THE_BOMB(g_FMMC_STRUCT.iAdversaryModeType))
		RETURN TRUE
	ENDIF
	IF ARE_STRINGS_EQUAL(g_FMMC_STRUCT.tl31LoadedContentID,"")
		PRINTLN("DO_RANK_PREDICTION_FOR_DM bypassed because empty file name")
		RETURN TRUE
	ENDIF
	
	// STEP 1 Setup the read data
	SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(dmVarsPassed.scLB_control,
										GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].iCurrentMissionType,
										g_FMMC_STRUCT.tl31LoadedContentID,
										g_FMMC_STRUCT.tl63MissionName,//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].tl31MissionName,
										iSubMode, 
										0)
										
										
	// 1887218 lbd timeout
	IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.lbdRankPredictionTimeout)
		START_NET_TIMER(dmVarsPassed.lbdRankPredictionTimeout)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.lbdRankPredictionTimeout, RANK_PREDICTION_TIMEOUT)
			
			IF NOT IS_THIS_A_ROUNDS_MISSION()
				WRITE_LEADERBOARD_DATA(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("DO_RANK_PREDICTION_FOR_DM, WRITE_LEADERBOARD_DATA (RANK_PREDICTION_TIMEOUT) " ) 
				SCRIPT_ASSERT("DO_RANK_PREDICTION_FOR_DM hit timeout, bug Chris Speirs")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	// STEP 2 wait for the leaderboard read to finish before doing your write 										
	IF scLB_rank_predict.bFinishedRead
	AND NOT scLB_rank_predict.bFinishedWrite
	
		IF NOT IS_THIS_A_ROUNDS_MISSION()
			WRITE_LEADERBOARD_DATA(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINTLN("GET_RANK_PREDICTION_DETAILS CURRENT RUN values (JUST AFTER WRITE) " ) 
			PRINT_RANK_PREDICTION_STRUCT(scLB_rank_predict.currentResult)
		#ENDIF
	
		scLB_rank_predict.bFinishedWrite = TRUE
	ENDIF
	
	IF GET_RANK_PREDICTION_DETAILS(dmVarsPassed.scLB_control)
		sclb_useRankPrediction = TRUE
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CLIENT_CREATED_PICKUPS_AND_CREATE_FMMC_PROPS_MP(SHARED_DM_VARIABLES &dmVarsPassed)
		
	IF NOT CREATE_FMMC_PROPS_MP(dmVarsPassed.oiProps, dmVarsPassed.oiPropsChildren)
		PRINTLN("HAS_CLIENT_CREATED_PICKUPS_AND_CREATE_FMMC_PROPS_MP - Not made all the props yet")
		RETURN FALSE
	ENDIF
	IF NOT HAS_CLIENT_CREATED_PICKUPS(dmVarsPassed)
		PRINTLN("HAS_CLIENT_CREATED_PICKUPS_AND_CREATE_FMMC_PROPS_MP - Not made all the pickups yet")
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL HAVE_DM_ANIMS_LOADED()

	REQUEST_DM_ANIMS()
	
	IF HAS_ACTION_MODE_ASSET_LOADED("TREVOR_ACTION")
	AND HAS_ACTION_MODE_ASSET_LOADED("MICHAEL_ACTION")
	AND HAS_ACTION_MODE_ASSET_LOADED("FRANKLIN_ACTION")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SETUP_SCRIPTED_COVER_POINTS()
	INT i
	FOR i = 0 TO (FMMC_MAX_NUM_COVER-1)
		IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos)
			ADD_COVER_POINT((g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos),g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection,INT_TO_ENUM(COVERPOINT_USAGE, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage),INT_TO_ENUM(COVERPOINT_HEIGHT, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight),INT_TO_ENUM(COVERPOINT_ARC, g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverArc))
			PRINTLN("ADDING COVER POINT AT: ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].vPos," direction: ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].fDirection," cover type (left/right) ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverUsage," cover height ",g_FMMC_STRUCT_ENTITIES.sPlacedCoverPoints[i].iCoverHeight)
		ENDIF
	ENDFOR
ENDPROC

FUNC INT GET_NUMBER_OF_CLOSEBY_PLAYERS(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	INT i
	INT iNumberOfClosebyPeds
	
	PLAYER_INDEX thePlayerID
	PED_INDEX pedLocal

	FOR i = 0 TO serverBDpassed.iNumDmStarters
	
		thePlayerID = INT_TO_PLAYERINDEX(i)
		pedLocal = GET_PLAYER_PED(thePlayerID)
		
		IF DOES_ENTITY_EXIST(pedLocal)
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedLocal, serverBDpassed.vTeamCentreStartPoint[playerBDPassed[i].iTeam], TRUE) < 4.0
				iNumberOfClosebyPeds++
			ENDIF
		ENDIF
	
	ENDFOR
	
	PRINTLN("GET_NUMBER_OF_CLOSEBY_PLAYERS", iNumberOfClosebyPeds )
	
	RETURN iNumberOfClosebyPeds	
	
ENDFUNC

FUNC BOOL HAS_INITIAL_LOADSCENE_COMPLETED(PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	IF NOT IS_PLAYER_SPECTATOR(playerBDPassed, LocalPlayer, iLocalPart)		
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			IF NOT IS_NEW_LOAD_SCENE_LOADED()
				IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeInitialLoadScene) < 10000
					#IF IS_DEBUG_BUILD
						NET_PRINT("HAS_INITIAL_LOADSCENE_COMPLETED - loading... time = ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), dmVarsPassed.timeInitialLoadScene)) NET_NL()	
					#ENDIF
					RETURN(FALSE)
				ELSE
					NET_PRINT("HAS_INITIAL_LOADSCENE_COMPLETED - hit timeout!") NET_NL()
				ENDIF
			ELSE
				NET_PRINT("HAS_INITIAL_LOADSCENE_COMPLETED - IS_NEW_LOAD_SCENE_LOADED = TRUE") NET_NL()
			ENDIF
		ELSE
			NET_PRINT("HAS_INITIAL_LOADSCENE_COMPLETED - IS_NEW_LOAD_SCENE_ACTIVE = FALSE") NET_NL()
		ENDIF
	ELSE
		NET_PRINT("HAS_INITIAL_LOADSCENE_COMPLETED - player is spectator") NET_NL()
	ENDIF
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	RETURN(TRUE)
ENDFUNC

PROC SET_TEAM_RELATIONSHIPS(SHARED_DM_VARIABLES &dmVarsPassed, RELATIONSHIP_TYPE relType, REL_GROUP_HASH relGroup)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF relGroup <> dmVarsPassed.rgFM_DEATHMATCH[i]
			SET_RELATIONSHIP_BETWEEN_GROUPS(relType, relGroup, dmVarsPassed.rgFM_DEATHMATCH[i]) 
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAKE_DM_TEAMS_LIKE_FM_PLAYERS(SHARED_DM_VARIABLES &dmVarsPassed, PlayerBroadcastData &playerBDPassed[])
	CPRINTLN(DEBUG_NET_MAGNATE, "[dsw] [BOSSVBOSS] [MAKE_DM_TEAMS_LIKE_FM_PLAYERS] CALLED")
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgFM_Team[i],   dmVarsPassed.rgFM_DEATHMATCH[playerBDPassed[iLocalPart].iTeam])	
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,   dmVarsPassed.rgFM_DEATHMATCH[playerBDPassed[iLocalPart].iTeam], rgFM_Team[i])
	ENDREPEAT
ENDPROC
PROC MAKE_RIVAL_TEAMS_HATE_EACH_OTHER(SHARED_DM_VARIABLES &dmVarsPassed)
	INT i
	
	// Setup rel groups
	REPEAT NUM_NETWORK_PLAYERS i
		TEXT_LABEL_63 tlRelGroup
		tlRelGroup = "dmVars.rgFM_DEATHMATCH"
		tlRelGroup += i
		ADD_RELATIONSHIP_GROUP(tlRelGroup, dmVarsPassed.rgFM_DEATHMATCH[i])
	ENDREPEAT
	REPEAT NUM_NETWORK_PLAYERS i
		SET_TEAM_RELATIONSHIPS(dmVarsPassed, ACQUAINTANCE_TYPE_PED_HATE, dmVarsPassed.rgFM_DEATHMATCH[i])
	ENDREPEAT
ENDPROC

//PURPOSE: For dynoprop processing that all players need to do not just the host
PROC PROCESS_DYNOPROPS_CLIENT_DM(PlayerBroadcastData& playerBDPassed[], ServerBroadcastData &serverBDpassed, FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, SHARED_DM_VARIABLES &dmVarsPassed)
	
	INT iDynoProp = 0
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps = 0
		EXIT
	ENDIF
	
	BOOL bSeaMines = TRUE
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
	OR IS_PLAYER_SPECTATOR(playerBDPassed, LocalPlayer, iLocalPart)
	OR IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
	OR CONTENT_IS_USING_ARENA()
		bSeaMines = FALSE
	ENDIF
	
	PROCESS_PUTTING_OUT_FIREPIT_FIRES(dmVarsPassed.sTrapInfo_Local)
	PROCESS_PRELOOP_ARENA_TRAPS(dmVarsPassed.sTrapInfo_Local)
	
	BOOL bProcessTraps = CAN_PROCESS_TRAPS()
	
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfDynoProps iDynoprop
		NETWORK_INDEX niDynoProp = serverFMMCPassed.niDynoProps[iDynoprop]	
		IF NETWORK_DOES_NETWORK_ID_EXIST(niDynoProp)
			OBJECT_INDEX objDynoProp = NET_TO_OBJ(niDynoProp)
			
			SET_BIT(g_iCurrentSpectatorTrapSpawned, iDynoprop)
			
			IF bSeaMines
				PROCESS_UGC_SEA_MINES(objDynoProp, iDynoProp, TRUE, dmVarsPassed.iSeaMine_HasExplodedBS)
			ENDIF
			
			IF bProcessTraps
				PROCESS_ARENA_TRAPS(objDynoProp, iDynoprop, TRUE, serverBDpassed.sTrapInfo_Host, dmVarsPassed.sTrapInfo_Local)
			ENDIF
			
			PROCESS_REMOVING_TARGETTABLE_FROM_DYNO_PROP(objDynoProp)
			
		ELSE
			IF IS_BIT_SET(g_iCurrentSpectatorTrapSpawned, iDynoprop)
				CLEANUP_TRAP(iDynoprop, serverBDpassed.sTrapInfo_Host, dmVarsPassed.sTrapInfo_Local)
				CLEAR_BIT(g_iCurrentSpectatorTrapSpawned, iDynoprop)
			ENDIF
		ENDIF
		
		PROCESS_TRAP_RESPAWNING(iDynoProp, serverFMMCPassed.niDynoProps, dmVarsPassed.sTrapInfo_Local, serverBDpassed.sTrapInfo_Host)
	ENDREPEAT
ENDPROC

PROC PROCESS_PROOFS()
	IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
		IF NOT g_bProofsSet
			SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
			g_bProofsSet = TRUE
			PRINTLN("[MJL][PROCESS_PROOFS] - Deathmatch - Proofs are SET")
		ENDIF
	ELSE	
		IF g_bProofsSet
			SET_ENTITY_PROOFS(LocalPlayerPed,FALSE,FALSE,FALSE,FALSE,FALSE,FALSE,DEFAULT,FALSE)
			g_bProofsSet = FALSE
			PRINTLN("[MJL][PROCESS_PROOFS] - Deathmatch - Proofs are all set to FALSE")	
		ENDIF
	ENDIF
ENDPROC

PROC REQUEST_COUNTDOWN_UI(DM_INTRO_COUNTDOWN_UI &uiToRequest)
	uiToRequest.uiCountdown = REQUEST_SCALEFORM_MOVIE("COUNTDOWN")
ENDPROC

PROC RELEASE_COUNTDOWN_UI(DM_INTRO_COUNTDOWN_UI &uiToRelease)
	IF HAS_SCALEFORM_MOVIE_LOADED(uiToRelease.uiCountdown)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(uiToRelease.uiCountdown)
		PRINTLN("RELEASE_COUNTDOWN_UI")
	ENDIF
	PRINTLN("RELEASE_NAMED_SCRIPT_AUDIO_BANK - HUD_321_GO Called from fm_mission_controller_hud.sch 1")
ENDPROC

PROC CLEAN_COUNTDOWN(DM_INTRO_COUNTDOWN_UI &uiToUpdate)
	uiToUpdate.iBitFlags = 0
	CANCEL_TIMER(uiToUpdate.CountdownTimer)
ENDPROC

FUNC BOOL IS_COUNTDOWN_OK_TO_PROCEED(ServerBroadcastData &serverBDpassed, DM_INTRO_COUNTDOWN_UI &cduiIntro) 
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
		PRINTLN("[MMacK][321] ciRACE_STYLE_INTRO SET")
		
		IF bIsLocalPlayerHost
		AND NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_STARTED_COUNTDOWN)
			SCRIPT_TIMER sTimerTemp
		
			IF NOT HAS_NET_TIMER_STARTED(sTimerTemp)
				START_NET_TIMER(sTimerTemp) 
			ENDIF
			
			REINIT_NET_TIMER(serverBDpassed.timeServerCountdown)
				
			serverBDpassed.timeServerCountdown = GET_NET_TIMER_OFFSET(sTimerTemp, ciSYNC_RACE_COUNTDOWN_TIMER)
			
			SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_STARTED_COUNTDOWN)
		ENDIF
		
		IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_STARTED_COUNTDOWN)
			SCRIPT_TIMER sTimerTemp2
			
			IF NOT HAS_NET_TIMER_STARTED(sTimerTemp2)
				START_NET_TIMER(sTimerTemp2) 
				PRINTLN("[MMacK][321] [IS_COUNTDOWN_OK_TO_PROCEED], START_NET_TIMER")
			ENDIF
							
			IF IS_NET_TIMER_MORE_THAN(sTimerTemp2, serverBDpassed.timeServerCountdown)
				PRINTLN("[MMacK][321] [IS_COUNTDOWN_OK_TO_PROCEED], COUNT_DOWN_HAS_EXPIRED")
				
				RELEASE_COUNTDOWN_UI(cduiIntro)
				CLEAN_COUNTDOWN(cduiIntro)
				
				IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
				AND NOT bIsAnySpectator
				AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
				ENDIF
				
				SET_DISABLE_RANK_UP_MESSAGE(FALSE)
				
				IF bIsLocalPlayerHost
					RESET_NET_TIMER(serverBDpassed.timeServerCountdown)
					SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
				ENDIF
				PRINTLN("[MMacK][321] [IS_COUNTDOWN_OK_TO_PROCEED], RETURNING TRUE AS TIMER EXPIRED ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
				RETURN TRUE
			ENDIF	
		ENDIF
		
		RETURN FALSE
	ELSE
		PRINTLN("[MMacK][321] [IS_COUNTDOWN_OK_TO_PROCEED], RETURNING TRUE AS SBBOOL2_FINISHED_COUNTDOWN IS SET ", NATIVE_TO_INT(GET_NETWORK_TIME_ACCURATE()))
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SET_COUNTDOWN_NUMBER(DM_INTRO_COUNTDOWN_UI &uiToSet, INT iSecond)
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(uiToSet.uiCountdown, "SET_MESSAGE")
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
			ADD_TEXT_COMPONENT_INTEGER(ABSI(iSecond))
		END_TEXT_COMMAND_SCALEFORM_STRING()
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC SHOW_GO_SPLASH(DM_INTRO_COUNTDOWN_UI &uiToUpdate)
	IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
	AND HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
		PRINTLN("[MMacK][321GO] Played Go UNNATURAL")
		SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)

		INT iR, iG, iB, iA
		GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
		
		BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
		END_SCALEFORM_MOVIE_METHOD()
				
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
			IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
				PRINTLN("[JS] SHOW_GO_SPLASH - Boost timer")
				START_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
			ENDIF
		ENDIF
			
		IF bLocalPlayerPedOK	
			NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_INTRO_COUNTDOWN(DM_INTRO_COUNTDOWN_UI &uiToUpdate, SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData &serverBDpassed)
	UNUSED_PARAMETER(serverBDpassed)
	IF HAS_SCALEFORM_MOVIE_LOADED(uiToUpdate.uiCountdown)
		IF NOT IS_TIMER_STARTED(uiToUpdate.CountdownTimer)
			RESTART_TIMER_NOW(uiToUpdate.CountdownTimer)
		ENDIF
		CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLAYED_INTRO_SHARD)
		
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		DRAW_SCALEFORM_MOVIE_FULLSCREEN(uiToUpdate.uiCountdown, 255, 255, 255, 100)
		
		INT iTimerVal = FLOOR(GET_TIMER_IN_SECONDS(uiToUpdate.CountdownTimer))
		INT iSeconds = ABSI(iTimerVal - 3)
		
		IF IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
			IF uiToUpdate.iFrameCount >= 5
				IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
					IF CONTENT_IS_USING_ARENA()
						PLAY_SOUND_FRONTEND(-1, "Countdown_GO", "DLC_AW_Frontend_Sounds")
						PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_GO ")
						SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
						STOP_STREAM()
					ELIF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
						PLAY_SOUND_FRONTEND(-1, "GO", "HUD_MINI_GAME_SOUNDSET", FALSE) 
						SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI_SoundGo)
						STOP_STREAM()
					ENDIF
				ENDIF
			ELSE
				uiToUpdate.iFrameCount++
			ENDIF
		ENDIF
		
		IF NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
			IF (iSeconds = 3) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_3)
								
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Countdown_3", "DLC_AW_Frontend_Sounds")
					PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_3 ")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_3", "DLC_SR_TR_General_Sounds")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
					PLAY_POWERUPS_SOUND_FRONTEND(POWERUP_NONE, "FE_Countdown_3", FALSE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				ENDIF
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
			ELIF (iSeconds = 2) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_2)
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Countdown_2", "DLC_AW_Frontend_Sounds")
					PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_2 ")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_2", "DLC_SR_TR_General_Sounds")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
					PLAY_POWERUPS_SOUND_FRONTEND(POWERUP_NONE, "FE_Countdown_2", FALSE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				ENDIF				
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
			ELIF (iSeconds = 1) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_1)
				IF CONTENT_IS_USING_ARENA()
					PLAY_SOUND_FRONTEND(-1, "Countdown_1", "DLC_AW_Frontend_Sounds")
					PRINTLN("[MJL] [DISPLAY_INTRO_COUNTDOWN][FM_MISSION_CONTROLLER_HUD] PLAY_SOUND_FRONTEND, Countdown_1 ")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciENABLE_TINY_RACERS_SOUNDS)
					PLAY_SOUND_FRONTEND(-1,"Countdown_1", "DLC_SR_TR_General_Sounds")
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
					PLAY_POWERUPS_SOUND_FRONTEND(POWERUP_NONE, "FE_Countdown_1", FALSE)
				ELSE
					PLAY_SOUND_FRONTEND(-1, "3_2_1", "HUD_MINI_GAME_SOUNDSET", FALSE)
				ENDIF
				SET_COUNTDOWN_NUMBER(uiToUpdate, iSeconds)
				NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
			ELIF (iSeconds = 0) AND NOT IS_BITMASK_AS_ENUM_SET(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				SET_BITMASK_AS_ENUM(uiToUpdate.iBitFlags, CNTDWN_UI2_Played_Go)
				PRINTLN("[MMacK][321GO] Played Go NATURAL")
				INT iR, iG, iB, iA
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(uiToUpdate.uiCountdown, "SET_MESSAGE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("CNTDWN_GO")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)
				END_SCALEFORM_MOVIE_METHOD()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
					IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
						PRINTLN("[JS] DISPLAY_INTRO_COUNTDOWN - Boost timer")
						START_NET_TIMER(g_FMMC_STRUCT.stBoostTimer)
					ENDIF
				ENDIF
				
				PLAY_ARENA_ANNOUNCER_GO_LINE()
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciENABLE_GO_AIRHORN)
					IF CONTENT_IS_USING_ARENA()
						PLAY_SOUND_FRONTEND(-1, "Start", "DLC_AW_Frontend_Sounds" , FALSE)
					ELSE
						PLAY_SOUND_FRONTEND(-1, "Airhorn", "DLC_TG_Running_Back_Sounds" , FALSE)
					ENDIF
				ELIF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
					PLAY_POWERUPS_SOUND_FRONTEND(POWERUP_NONE, "FE_Countdown_Go", FALSE)
				ENDIF
				
				NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_ALL_PLAYERS_READY_FOR_COUNTDOWN(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
		RETURN TRUE
	ENDIF
	
	IF NOT HAS_SERVER_CHECKED_EVERYONE_READY(serverBDpassed)
		PRINTLN("[321] ARE_ALL_PLAYERS_READY_FOR_COUNTDOWN - Server is waiting for HAS_SERVER_CHECKED_EVERYONE_READY")
		RETURN FALSE
	ENDIF
	
	INT iPlayer = 0
	FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1
	
		PLAYER_INDEX piPlayer = INT_TO_PLAYERINDEX(iPlayer)
		
		IF piPlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_ACTIVE(piPlayer)
		AND NOT IS_PLAYER_USING_ACTIVE_ROAMING_SPECTATOR_MODE(piPlayer)
		AND NOT DID_PLAYER_JOIN_MISSION_AS_SPECTATOR(piPlayer)
		AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(piPlayer)
		AND NOT IS_THIS_PLAYER_ON_JIP_WARNING(piPlayer)
		AND NOT IS_THIS_PLAYER_NOT_GOING_TO_TAKE_PART_IN_THIS_CORONA(piPlayer)
			IF playerBDPassed[iPlayer].eClientLogicStage != CLIENT_MISSION_STAGE_FIGHTING
			AND playerBDPassed[iPlayer].eClientLogicStage != CLIENT_MISSION_STAGE_SCTV
			AND playerBDPassed[iPlayer].eClientLogicStage != CLIENT_MISSION_STAGE_SPECTATOR
				PRINTLN("[321] ARE_ALL_PLAYERS_READY_FOR_COUNTDOWN - Waiting for iPlayer: ", iPlayer, " | ", GET_PLAYER_NAME(piPlayer))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
		
ENDFUNC

FUNC BOOL HAS_3_2_1_GO_FINISHED(SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF NOT (bIsSCTV OR DID_I_JOIN_MISSION_AS_SPECTATOR())
		
		IF NOT ARE_ALL_PLAYERS_READY_FOR_COUNTDOWN(serverBDpassed, playerBDPassed)
			PRINTLN("HAS_3_2_1_GO_FINISHED - Player(s) aren't ready yet")
			
			PRINTLN("HAS_3_2_1_GO_FINISHED - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD - player(s) not ready yet")
			DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(FALSE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_ALLOW_REV_ON_COUNTDOWN) ,FALSE, TRUE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciLBD_ALLOW_HYDRO_FOR_COUNTDOWN))
			dmVarsPassed.bLocalHandBrakeOnFlag = TRUE
			
			RETURN FALSE
		ENDIF
		
		IF NOT IS_COUNTDOWN_OK_TO_PROCEED(serverBDpassed, dmVarsPassed.cduiIntro)	
					
			DISPLAY_INTRO_COUNTDOWN(dmVarsPassed.cduiIntro, dmVarsPassed, serverBDpassed)
			
			//Allow for some player control during the race countdown
			IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_STARTED_COUNTDOWN)
				IF NOT IS_BITMASK_AS_ENUM_SET(dmVarsPassed.cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go)
					// Due to Hover Mode issues.
					IF bLocalPlayerPedOK
					AND IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
					AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(LocalPlayerPed)) = DELUXO
						NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
					ENDIF
					IF NOT g_bDisableVehicleMines
					AND NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
						DISABLE_VEHICLE_MINES(TRUE)
						PRINTLN("[LM] - LIMIT RACE CONROLS - Disabling Vehicle Mines 1.")						
					ENDIF
					SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_CLEAR_VEHICLE_MINES_DIABLE)
					
					PRINTLN("LIMIT RACE CONROLS - DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD")
					DISABLE_CONTROL_THIS_FRAME_ENABLE_HORN_AND_HOOD(FALSE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_ALLOW_REV_ON_COUNTDOWN) ,FALSE, TRUE, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciLBD_ALLOW_HYDRO_FOR_COUNTDOWN))
					dmVarsPassed.bLocalHandBrakeOnFlag = TRUE
				ELSE										
					PRINTLN("LIMIT RACE CONROLS - SBBOOL2_FINISHED_COUNTDOWN")
					SET_VEHICLE_HANDBRAKE_STATE(FALSE)
					dmVarsPassed.bLocalHandBrakeOnFlag = FALSE
				ENDIF
			ELSE
				PRINTLN("LIMIT RACE CONROLS - SBBOOL2_STARTED_COUNTDOWN not set")
				IF NOT g_bDisableVehicleMines
				AND NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
					DISABLE_VEHICLE_MINES(TRUE)
					PRINTLN("[LM] - LIMIT RACE CONROLS - Disabling Vehicle Mines 2.")		
				ENDIF
				SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_CLEAR_VEHICLE_MINES_DIABLE)
			ENDIF			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciRACE_BOOST_START) 
				IF HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stBoostTimer)
					DO_VEH_BOOST()
				ENDIF
				UPDATE_VEHICLE_BOOSTING()
			ENDIF
		
			RETURN FALSE
		ELSE
			IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLAYED_INTRO_SHARD)
				IF IS_BITMASK_AS_ENUM_SET(dmVarsPassed.cduiIntro.iBitFlags, CNTDWN_UI2_Played_Go) 
					RELEASE_COUNTDOWN_UI(dmVarsPassed.cduiIntro)
					CLEAN_COUNTDOWN(dmVarsPassed.cduiIntro)
					SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLAYED_INTRO_SHARD)
				ENDIF
				
				IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_STARTED_COUNTDOWN)
				AND dmVarsPassed.cduiIntro.iBitFlags != 0
					//if we have a lag issue on countdown, this might not cleanup properly
					IF dmVarsPassed.bLocalHandBrakeOnFlag = TRUE
						SET_VEHICLE_HANDBRAKE_STATE(FALSE)
						PRINTLN("LIMIT RACE CONROLS - SET_VEHICLE_HANDBRAKE_STATE - set to false")
						dmVarsPassed.bLocalHandBrakeOnFlag = FALSE
					ENDIF
					
					IF bIsLocalPlayerHost
						RESET_NET_TIMER(serverBDpassed.timeServerCountdown)
						SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_DISPLAYED_INTRO_SHARD)
					SHOW_GO_SPLASH(dmVarsPassed.cduiIntro)					
				ENDIF
				
				IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_CLEAR_VEHICLE_MINES_DIABLE)
				AND IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
					CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_CLEAR_VEHICLE_MINES_DIABLE)
					DISABLE_VEHICLE_MINES(FALSE)
					PRINTLN("[LM] - LIMIT RACE CONROLS - Enabling Vehicle Mines")		
				ENDIF
				
				//IF NOT IS_BIT_SET(iLocalBoolCheck13, LBOOL13_SHOWN_TEAM_SHARD)
					//SHOW_TEAM_SHARD_AT_START_OF_VS_MISSION()					
				//	SET_BIT(iLocalBoolCheck13, LBOOL13_SHOWN_TEAM_SHARD) //if there's a massive lag on GO, this can happen twice
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF dmVarsPassed.bLocalHandBrakeOnFlag = TRUE
		IF SET_VEHICLE_HANDBRAKE_STATE(FALSE)
			PRINTLN("LIMIT RACE CONROLS - SET_VEHICLE_HANDBRAKE_STATE - set to false - bLocalHandBrakeOnFlag = FALSE")
			dmVarsPassed.bLocalHandBrakeOnFlag = FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PROCESS_DM_SPECIFIC_POWERUPS_LOGIC(FMMC_POWERUP_STRUCT &sPowerUp, SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF HAS_DEATHMATCH_FINISHED_LOCAL(dmVarsPassed)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(sPowerUp.sVehicleSwap.ptfxID)
			STOP_PARTICLE_FX_LOOPED(sPowerUp.sVehicleSwap.ptfxID, FALSE)
		ENDIF
	ENDIF
	
	IF GET_CURRENT_POWERUP(sPowerUp) != POWERUP_TANK
	AND GET_CURRENT_POWERUP(sPowerUp) != POWERUP_MONSTER_TRUCK
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_FRESH_POWERUP_TRANSFORM)
	AND sPowerUp.ePowerUpState = POWERUP_STATE_RUNNING
		SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_FRESH_POWERUP_TRANSFORM)
		PRINTLN("PROCESS_DM_SPECIFIC_POWERUPS_LOGIC - Setting DM_BOOL2_FRESH_POWERUP_TRANSFORM")
	ENDIF
	
	IF sPowerUp.ePowerUpState = POWERUP_STATE_CLEANUP_EFFECT
	AND IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_FRESH_POWERUP_TRANSFORM)
		CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_FRESH_POWERUP_TRANSFORM)
		CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_VDM_SET_UP_VEH)
		PRINTLN("PROCESS_DM_SPECIFIC_POWERUPS_LOGIC - Clearing - DM_BOOL2_FRESH_POWERUP_TRANSFORM & DM_BOOL2_VDM_SET_UP_VEH")
	ENDIF
ENDPROC

FUNC BOOL IS_DM_READY_TO_SHOW_CELEBRATION(SHARED_DM_VARIABLES &dmVarsPassed)
	
	IF NOT CONTENT_IS_USING_ARENA()
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
		PRINTLN("IS_DM_READY_TO_SHOW_CELEBRATION - No need to check timer")
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.stCelebrationDelay, 1500)
		PRINTLN("IS_DM_READY_TO_SHOW_CELEBRATION - Timer has passed")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#IF FEATURE_GEN9_EXCLUSIVE
PROC PROCESS_DISABLE_REALTIME_MULTIPLAYER(PlayerBroadcastData &playerBDPassed[])
	
	IF g_bBlock_PROCESS_DISABLE_REALTIME_MULTIPLAYER
		PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - GLOBAL BLOCK SET - EXITING")
		EXIT
	ENDIF
	
	IF g_sMPTunables.bDisableRealtimeMPCheck_JIP_DEATHMATCH // If set use the functionality tested before url:bugstar:7534395
		IF GET_CLIENT_MISSION_STAGE(playerBDPassed) >= CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
		AND GET_CLIENT_MISSION_STAGE(playerBDPassed) != CLIENT_MISSION_STAGE_SCTV
			NETWORK_DISABLE_REALTIME_MULTIPLAYER()
			PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Deathmatch - bDisableRealtimeMPCheck_JIP_DEATHMATCH")
		ENDIF
	ELSE
		IF GET_CLIENT_MISSION_STAGE(playerBDPassed) >= CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
		AND GET_CLIENT_MISSION_STAGE(playerBDPassed) != CLIENT_MISSION_STAGE_SCTV
		AND GET_CLIENT_MISSION_STAGE(playerBDPassed) != CLIENT_MISSION_STAGE_SPECTATOR
			NETWORK_DISABLE_REALTIME_MULTIPLAYER()
			PRINTLN("PROCESS_DISABLE_REALTIME_MULTIPLAYER - Calling NETWORK_DISABLE_REALTIME_MULTIPLAYER for Deathmatch")
		ENDIF
	ENDIF

ENDPROC
#ENDIF

PROC PROCESS_PLAYER_MODIFIER_SPAWN_VEHICLE_UPDATE(PLAYER_MODIFIER_DATA &sPlayerModifierData)
	IF sPlayerModifierData.mnRespawnVehicle != mnPrevPlayerModifierRespawnVeh
		BOOL bSpawnInVeh = sPlayerModifierData.mnRespawnVehicle != DUMMY_MODEL_FOR_SCRIPT
		mnPrevPlayerModifierRespawnVeh = sPlayerModifierData.mnRespawnVehicle
		SET_PLAYER_RESPAWN_IN_VEHICLE(bSpawnInVeh, mnPrevPlayerModifierRespawnVeh, TRUE, FALSE)
		PRINTLN("PROCESS_PLAYER_MODIFIER_SPAWN_VEHICLE_UPDATE - bSpawnInVeh: ", PICK_STRING(bSpawnInVeh, "TRUE", "FALSE"), " and mnPrevRespawnVeh cached as ", GET_MODEL_NAME_FOR_DEBUG(mnPrevPlayerModifierRespawnVeh))
	ENDIF
ENDPROC

PROC PROCESS_PLAYERS_READY_TO_START_SHRINKING_BOUNDS(DM_SHRINKING_BOUNDS_SERVER_DATA &sBoundsServerData[], PlayerBroadcastData &playerBDPassed[])
	
	IF NOT bIsLocalPlayerHost
		EXIT
	ENDIF
	
	INT i, j
	FOR i = 0 TO FMMC_MAX_NUM_DM_BOUNDS - 1
	
		//Just bail if this bounds isn't set to shrink or already has
		IF g_FMMC_STRUCT.sDeathmatchBoundsInfo[i].iShrinkToPercent >= 100
		OR IS_BIT_SET(sBoundsServerData[i].iBitset, ciSHRINKING_BOUNDS_ALL_PLAYERS_READY)
			RELOOP
		ENDIF
		
		BOOL bAllReady = TRUE
		FOR j = 0 TO GET_NUM_PLAYERS_IN_SESSION() - 1
			PLAYER_INDEX piTemp = INT_TO_PLAYERINDEX(i)
			
			//if they are active
			IF NOT NETWORK_IS_PLAYER_ACTIVE(piTemp)
			OR IS_PLAYER_SCTV(piTemp)
				RELOOP
			ENDIF
		
			IF NOT IS_BIT_SET(playerBDPassed[j].sShrinkingBoundsData[i].iBitset, ciSHRINKING_BOUNDS_BS_READY_TO_SHRINK)
				PRINTLN("[ShrinkingBounds][Bounds ", i,"] PROCESS_PLAYERS_READY_TO_START_SHRINKING_BOUNDS - Player ", j, " isn't ready")
				bAllReady = FALSE
			ENDIF
		ENDFOR
		
		IF bAllReady
			PRINTLN("[ShrinkingBounds][Bounds ", i,"] PROCESS_PLAYERS_READY_TO_START_SHRINKING_BOUNDS - All players ready to start shrinking bounds")
			SET_BIT(sBoundsServerData[i].iBitset, ciSHRINKING_BOUNDS_ALL_PLAYERS_READY)
		ENDIF
		
	ENDFOR
	
ENDPROC

PROC PROCESS_TDM_GUARANTEE()
	IF NOT IS_THIS_A_ROUNDS_MISSION()
		EXIT
	ENDIF
	
	IF GlobalServerBD_DM.bIsTeamDM
	AND g_sDM_SB_CoronaOptions.bIsTeamDM = FALSE
		g_sDM_SB_CoronaOptions.bIsTeamDM = TRUE
		PRINTLN("PROCESS_TDM_GUARANTEE - Setting g_sDM_SB_CoronaOptions.bIsTeamDM to be TRUE because the server says it should be!!!")
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC MANAGE_HOST_MIGRATION(SHARED_DM_VARIABLES &dmVarsPassed)

	IF bIsLocalPlayerHost
		IF IS_BIT_SET(dmVarsPassed.iDmBools3, WAS_NOT_THE_HOST)
			
			PRINTLN("[7719312] - MANAGE_HOST_MIGRATION - HOST MIGRATED TO ME")
			CLEAR_BIT(dmVarsPassed.iDmBools3, WAS_NOT_THE_HOST)
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(dmVarsPassed.iDmBools3, WAS_NOT_THE_HOST)
			PRINTLN("[7719312] - MANAGE_HOST_MIGRATION - NO LONGER THE HOST - new host = ", NATIVE_TO_INT(NETWORK_GET_HOST_OF_THIS_SCRIPT()))
			SET_BIT(dmVarsPassed.iDmBools3, WAS_NOT_THE_HOST)
		ENDIF
	ENDIF

ENDPROC
#ENDIF

PROC PROCESS_CLIENT_DEATHMATCH(FMMC_SERVER_DATA_STRUCT &serverFMMCPassed, LEADERBOARD_PLACEMENT_TOOLS& Placement, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	BOOL bHideHud
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("PROCESS_CLIENT_DEATHMATCH") // MUST BE AT START OF FUNCTION
	#ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	MANAGE_HOST_MIGRATION(dmVarsPassed)
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("MANAGE_HOST_MIGRATION") 
	#ENDIF
	#ENDIF
	
	PROCESS_TDM_GUARANTEE()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_TDM_GUARANTEE") 
	#ENDIF
	#ENDIF
		
	PROCESS_APPLICABILITY_TRIGGERS(serverBDpassed, playerBDPassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_APPLICABILITY_TRIGGERS") 
	#ENDIF
	#ENDIF
	
	PROCESS_APPLICABILITY_EVERY_FRAME(dmVarsPassed.sPlayerModifierData, playerBDPassed[iLocalPart].iCurrentModSet, playerBDPassed[iLocalPart].iPendingModSet)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_APPLICABILITY_EVERY_FRAME") 
	#ENDIF
	#ENDIF
	
	GRAB_ARENA_WINNER_VEHICLE_NAME(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("CLIENT_MANAGES_BLIPS") 
	#ENDIF
	#ENDIF
	
	CLIENT_MANAGES_BLIPS(serverBDpassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("CLIENT_MANAGES_BLIPS") 
	#ENDIF
	#ENDIF
	
	CLIENT_CUSTOM_TDM_BLIP_COLOURS(playerBDPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("CLIENT_CUSTOM_TDM_BLIP_COLOURS") 
	#ENDIF
	#ENDIF
	
	NUM_DPAD_LBD_PLAYERS_IS(serverBDpassed.iNumActivePlayers)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("NUM_DPAD_LBD_PLAYERS_IS") 
	#ENDIF
	#ENDIF
	
	IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
		GRAB_ELO_DATA(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("GRAB_ELO_DATA")
		#ENDIF
		#ENDIF
	ENDIF

	PROCESS_ARENA_INTERIOR()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("NUM_DPAD_LBD_PLAYERS_IS") 
	#ENDIF
	#ENDIF
	
//	IF CONTENT_IS_USING_ARENA() 
//		CLEAR_DESTROYED_ARENA_VEHICLES(dmVarsPassed)
//	ENDIF
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE 
//		ADD_SCRIPT_PROFILE_MARKER("CLEAR_DESTROYED_ARENA_VEHICLES") 
//	#ENDIF
//	#ENDIF
	
	PROCESS_EXTRA_VEHICLE_COLLISION_DAMAGE(timerExtraVehDamageForceHitting, ciTimeExtraVehDamageToBeHittingAgain)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_EXTRA_VEHICLE_COLLISION_DAMAGE") 
	#ENDIF
	#ENDIF
	
	PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_CLIENT_EXPLODE_AT_ZERO_HEALTH") 
	#ENDIF
	#ENDIF
	
	PROCESS_PROPS_CLIENT(dmVarsPassed.oiProps, serverBDPassed.sTrapInfo_Host, dmVarsPassed.sTrapInfo_Local)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_PROPS_CLIENT") 
	#ENDIF
	#ENDIF
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps > 0
	AND g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps <= GET_FMMC_MAX_WORLD_PROPS()
		LEGACY_PROCESS_WORLD_PROPS(dmVarsPassed.iWorldPropIterator, dmVarsPassed.sRuntimeWorldPropData)
		dmVarsPassed.iWorldPropIterator++
		IF dmVarsPassed.iWorldPropIterator >= g_FMMC_STRUCT_ENTITIES.iNumberOfWorldProps
			dmVarsPassed.iWorldPropIterator = 0
		ENDIF
	ENDIF
		
	// Because the script launches early via corona
	IF HAS_DM_STARTED(serverBDpassed)
	
		PROCESS_CUSTOM_DM_BLIPS(playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_CUSTOM_DM_BLIPS")
		#ENDIF
		#ENDIF
		
		POPULATE_VEHICLE_FOR_LBD(serverBDpassedLDB, playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("POPULATE_VEHICLE_FOR_LBD") 
		#ENDIF
		#ENDIF
	
		#IF IS_DEBUG_BUILD
		MAINTAIN_DM_DEBUG_DISPLAY(dmVarsPassed)
		IF dmVarsPassed.debugDmVars.bRenderServerLeaderBoard
			DEBUG_RENDER_SERVER_LEADERBOARD(serverBDpassed, serverBDpassedLDB)
		ENDIF
		#ENDIF
		
		TARGET_DRAW_FLOATING_SCORES(dmVarsPassed.sTargetFloatingScores)
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DEBUG_RENDER_SERVER_LEADERBOARD")
		#ENDIF
		#ENDIF 
	
		SET_JOB_ENDED_GLOBAL(HAS_DEATHMATCH_FINISHED(serverBDPassed))
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_JOB_ENDED_GLOBAL")
		#ENDIF
		#ENDIF 
	
		MAINTAIN_CELEBRATION_PRE_LOAD(dmVarsPassed.sCelebrationData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_CELEBRATION_PRE_LOAD") 
		#ENDIF
		#ENDIF
	
		// 1802067
		IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
			SET_MISSION_OVER_AND_ON_LB_FOR_SCTV()
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_MISSION_OVER_AND_ON_LB_FOR_SCTV") 
		#ENDIF
		#ENDIF
	
		GET_OPPONENTS_AVERAGE_ELO(serverBDpassed, playerBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("GET_OPPONENTS_AVERAGE_ELO")
		#ENDIF
		#ENDIF
		
		SET_IMPROMPTU_TARGET_PRIORITY_ON(dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_IMPROMPTU_TARGET_PRIORITY_ON")
		#ENDIF
		#ENDIF
		
		IF SHOULD_RUN_NEW_VOTING_SYSTEM(dmVarsPassed.sEndOfMission.iQuitProgress, serverBDpassed.sServerFMMC_EOM.iVoteStatus)
			IF NOT READY_TO_MOVE_TO_NEXT_JOBS(dmVarsPassed.nextJobStruct)
				DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, dmVarsPassed.nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("CLIENT_JOB_VOTE_GRAB_IMAGES")
				#ENDIF
				#ENDIF
			ENDIF
		ENDIF
		
		PROCESS_PARACHUTES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PARACHUTES")
		#ENDIF
		#ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_DM_TAGGED_EXPLOSION_TIMER)
		AND NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
		AND IS_SKYSWOOP_AT_GROUND()
			PROCESS_PASSTHEBOMB_CLIENT(serverBDpassed, playerBDPassed, dmVarsPassed)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_PASSTHEBOMB_CLIENT")
			#ENDIF
			#ENDIF
		ELSE
			END_DM_SOUND(dmVarsPassed.iTagBombCountdownSndID)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("END_DM_SOUND")
			#ENDIF
			#ENDIF
		ENDIF
		
		IF NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
		AND IS_SKYSWOOP_AT_GROUND()
		
			IF IS_KING_OF_THE_HILL()
				
				IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT)
				AND GET_HUD_TIME(serverBDpassed) > -1
				AND GET_HUD_TIME(serverBDpassed) <= 5000
					PLAY_SOUND_FRONTEND(-1, "5S", "MP_MISSION_COUNTDOWN_SOUNDSET", FALSE)
					PRINTLN("[KOTH_SOUND] Playing 5s countdown sound!")
					SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_PLAYED_COUNTDOWN_SOUND_EFFECT)
				ENDIF
				
				BOOL bIgnoreStick = FALSE
				BOOL bIgnoreButtons = bIgnoreStick
				
				g_fKOTHIdleCounter = dmVarsPassed.sInactiveDetectionStruct.fFrameTimeSpentInactive
				dmVarsPassed.fTimeSpentCheckingIdle += GET_FRAME_TIME()
				
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_RacesInactiveRewardBanDebug")
					TEXT_LABEL_63 tlDebugText
					tlDebugText = "Percentage of Match Spent Idle: "
					tlDebugText += FLOAT_TO_STRING((g_fKOTHIdleCounter / dmVarsPassed.fTimeSpentCheckingIdle) * 100.0)
					tlDebugText += "%"
					DRAW_DEBUG_TEXT_2D(tlDebugText, <<0.25, 0.7, 0.5>>)
				ENDIF
				#ENDIF
				
				IF DEAL_WITH_INACTIVE_PLAYERS_REWARDS(dmVarsPassed.sInactiveDetectionStruct, KOTH_IDLE_FOR_TOO_LONG(dmVarsPassed), bIgnoreStick, bIgnoreButtons)
					//g_fKOTHIdleCounter += GET_FRAME_TIME()
					PRINTLN("DEAL_WITH_INACTIVE_PLAYERS_REWARDS returned true || Incrementing g_fKOTHIdleCounter up to ", g_fKOTHIdleCounter, " / fTimeSpentCheckingIdle is ", dmVarsPassed.fTimeSpentCheckingIdle)
				ELSE
					//g_fKOTHIdleCounter = 0.0
					PRINTLN("DEAL_WITH_INACTIVE_PLAYERS_REWARDS returned false || g_fKOTHIdleCounter is ", g_fKOTHIdleCounter, " / fTimeSpentCheckingIdle is ", dmVarsPassed.fTimeSpentCheckingIdle)
				ENDIF
				
				PLAYER_INDEX piPlayerToUse = LocalPlayer
				INT iPart = iLocalPart
				
				ASSIGN_ALL_KING_OF_THE_HILL_ENTITIES(dmVarsPassed.sKOTH_LocalInfo, serverFMMCPassed.niVehicle, serverFMMCPassed.niObject)
				
				IF playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam = -1
					IF IS_TEAM_DEATHMATCH()
						playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam = playerBDPassed[iPart].iTeam
					ELSE
						playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam = iPart
					ENDIF
					
					PRINTLN("[KOTH_CRITICAL] Setting sKOTH_PlayerInfo.iCachedKOTHTeam to ", playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam)
				ENDIF
				
				IF bIsAnySpectator
					
					IF DOES_ENTITY_EXIST(PlayerPedToUse)
					
						piPlayerToUse = PlayerToUse
						dmVarsPassed.sKOTH_LocalInfo.piPedToUse = PlayerPedToUse
						
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerToUse)
							INT iSpecTargetPart = iPartToUse
							INT iTempCachedTeam = playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam
							
							IF playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam != playerBDPassed[iSpecTargetPart].sKOTH_PlayerInfo.iCachedKOTHTeam
								playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam = playerBDPassed[iSpecTargetPart].sKOTH_PlayerInfo.iCachedKOTHTeam
							ENDIF
							
							IF iTempCachedTeam != playerBDPassed[iPart].sKOTH_PlayerInfo.iCachedKOTHTeam
								//Cached Team has changed
								CLEAR_BIT(dmVarsPassed.sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_AssignedTeamNames)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					g_iMyDMScore = FLOOR(playerBDPassed[iPart].sKOTH_PlayerInfo.fKOTHScore)
					playerBDPassed[iPart].iScore = g_iMyDMScore
					
					dmVarsPassed.sKOTH_LocalInfo.piPedToUse = dmVarsPassed.sKOTH_LocalInfo.piLocalPlayerPed
				ENDIF
				
				IF dmVarsPassed.sKOTH_LocalInfo.iNumberOfKotHPlayers != serverBDpassed.iNumActivePlayers
					dmVarsPassed.sKOTH_LocalInfo.iNumberOfKotHPlayers = serverBDpassed.iNumActivePlayers
					PRINTLN("[KOTH] Setting dmVarsPassed.sKOTH_LocalInfo.iNumberOfKotHPlayers to ", dmVarsPassed.sKOTH_LocalInfo.iNumberOfKotHPlayers)
				ENDIF
				
				ASSIGN_KOTH_TEAM_NAMES(serverBDpassed, serverBDpassed.sKOTH_HostInfo, playerBDPassed[iPart].sKOTH_PlayerInfo, dmVarsPassed.sKOTH_LocalInfo)
				
				IF GET_CLIENT_MISSION_STAGE(playerBDPassed) >= CLIENT_MISSION_STAGE_FIGHTING
				AND (GET_CLIENT_MISSION_STAGE(playerBDPassed) < CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
				OR GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_SPECTATOR
				OR GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_SCTV)
					
					IF OK_TO_SHOW_SCORES_BOTTOM_RIGHT(serverBDpassed#IF IS_DEBUG_BUILD, dmVarsPassed #ENDIF)
						IF GET_HUD_TIME(serverBDpassed) > -1
							DRAW_KOTH_HUD(serverBDpassed, serverBDpassed.sKOTH_HostInfo, playerBDPassed[iPart].sKOTH_PlayerInfo, dmVarsPassed.sKOTH_LocalInfo, piPlayerToUse, serverBDpassedLDB)
						ELSE
							PRINTLN("[KOTH] Hiding KOTH HUD due to GET_HUD_TIME being -1")
						ENDIF
					ELSE
						PRINTLN("[KOTH] Hiding KOTH HUD due to OK_TO_SHOW_SCORES_BOTTOM_RIGHT")
					ENDIF
					
					PROCESS_KING_OF_THE_HILL_PLAYER(serverBDpassed.sKOTH_HostInfo, playerBDPassed[iPart].sKOTH_PlayerInfo, dmVarsPassed.sKOTH_LocalInfo, DEFAULT, GET_CLIENT_MISSION_STAGE(playerBDPassed) = CLIENT_MISSION_STAGE_RESPAWN)
				ENDIF
			ENDIF
		ENDIF
		
		PROCESS_ARENA_OPTIONS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_ARENA_OPTIONS")
		#ENDIF
		#ENDIF
		
		SET_BIT_WHEN_PLAYER_QUITS(playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("SET_BIT_WHEN_PLAYER_QUITS")
		#ENDIF
		#ENDIF
	
		CLIENT_MANAGE_BULLSHARK_PICKUP(serverBDpassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("CLIENT_MANAGE_BULLSHARK_PICKUP")
		#ENDIF
		#ENDIF
		
		// Common client procs running always
		DEAL_WITH_DEATHMATCH(serverBDpassed, serverFMMCPassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB #IF IS_DEBUG_BUILD , Placement #ENDIF )	
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("DEAL_WITH_DEATHMATCH")
		#ENDIF
		#ENDIF
		
		PROCESS_RESPAWN_VEHICLES_LOCAL(serverBDpassed, serverFMMCPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_RESPAWN_VEHICLES_LOCAL")
		#ENDIF
		#ENDIF
		
		STORE_JOB_CASH(serverBDPassed, playerBDPassed #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF )
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("STORE_JOB_CASH")
		#ENDIF
		#ENDIF
		
		PROCESS_DYNOPROPS_CLIENT_DM(playerBDPassed, serverBDPassed, serverFMMCPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DYNOPROPS_CLIENT_DM")
		#ENDIF
		#ENDIF
		
		IF IS_ARENA_WARS_JOB()
			ARENA_CONTESTANT_TURRET_UPDATE(dmVarsPassed.arenaTurretContext, serverBDPassed.arenaContestantTurretServer, bIsLocalPlayerHost)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("ARENA_CONTESTANT_TURRET_UPDATE")
			#ENDIF
			#ENDIF
		ENDIF
			
		IF NOT bIsSCTV
		AND NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
		AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		AND NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
		AND NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_SENT_TO_SPECTATOR)
		
			MAINTAIN_SPECTATOR(g_BossSpecData)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR")
			#ENDIF
			#ENDIF
		ENDIF
		
		IF bIsSCTV
		OR DID_I_JOIN_MISSION_AS_SPECTATOR()
			IF NOT IS_SKYSWOOP_AT_GROUND()
			AND IS_PLAYER_CONTROL_ON(LocalPlayer)
				SET_PLAYER_CONTROL(LocalPlayer, FALSE)
				PRINTLN("[SPECTATOR] Turning off player control because we're in the sky")
			ENDIF
		ENDIF
		
		IF IS_PLAYER_ON_BOSSVBOSS_DM()
			IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
				GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_KILLED_BOSS_BOSSVBOSS_DM)
				playerBDPassed[iLocalPart].iMyRivalBossKills++
				CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] MY RIVAL BOSS KILLS NOW ", playerBDPassed[iLocalPart].iMyRivalBossKills)
			ENDIF
			
			IF GB_IS_GLOBAL_CLIENT_BIT0_SET(LocalPlayer, eGB_GLOBAL_CLIENT_BITSET_0_SUICIDE_BOSS_BOSSVBOSS_DM)
				GB_CLEAR_GLOBAL_CLIENT_BIT0(eGB_GLOBAL_CLIENT_BITSET_0_SUICIDE_BOSS_BOSSVBOSS_DM)
				playerBDPassed[iLocalPart].iMyBossKillSelf++
				CPRINTLN(DEBUG_NET_MAGNATE, " ----->     [dsw] [BOSSVBOSS] MY KILL SELF NOW ", playerBDPassed[iLocalPart].iMyBossKillSelf)
			ENDIF
		ENDIF
		 //FEATURE_GANG_BOSS
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("IS_PLAYER_ON_BOSSVBOSS_DM")
		#ENDIF
		#ENDIF
		
		PLAYER_INDEX playerPosition = INT_TO_PLAYERINDEX(GET_SPECTATOR_LIST_PLAYER_ID_FOR_THIS_FRAME(g_BossSpecData))
		IF IS_NET_PLAYER_OK(playerPosition, FALSE)
			MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION(g_BossSpecData, GET_SPECIFIED_LBD_PLAYER(serverBDpassedLDB, playerPosition))
		ELSE
			MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION(g_BossSpecData, -1)
		ENDIF
		
		IF IS_TEAM_DEATHMATCH()
		AND GET_SPECTATOR_LIST_TEAM_FOR_THIS_FRAME(g_BossSpecData) < MAX_NUM_DM_TEAMS
			MAINTAIN_SPECTATOR_TEAM_LIST_POSITION(g_BossSpecData, GET_TEAM_POSITION(serverBDpassed, GET_SPECTATOR_LIST_TEAM_FOR_THIS_FRAME(g_BossSpecData)))
		ELSE
			MAINTAIN_SPECTATOR_TEAM_LIST_POSITION(g_BossSpecData, -1)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR_PLAYER_LIST_POSITION")
		#ENDIF
		#ENDIF

		TURN_SPECTATOR_CHAT_ON_WHEN_DM_ENDS(serverBDPassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("TURN_SPECTATOR_CHAT_ON_WHEN_DM_ENDS")
		#ENDIF
		#ENDIF
		
		PROCESS_BULLETPROOF_TYRES()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_BULLETPROOF_TYRES")
		#ENDIF
		#ENDIF
		
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_ProofsInvalidatedOnFoot)
			PROCESS_PROOFS()
		ENDIF
		
		PROCESS_VEHICLE_WEAPON_FAKE_AMMO(dmVarsPassed.fVehicleWeaponShootingTime, dmVarsPassed.fVehicleWeaponTimeSinceLastfired, dmVarsPassed.bRegenVehicleWeapons, dmVarsPassed.iFakeAmmoRechargeSoundID)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_VEHICLE_WEAPON_FAKE_AMMO")
		#ENDIF
		#ENDIF
		
		IF NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
		AND NOT HAS_DEATHMATCH_FINISHED_LOCAL(dmVarsPassed)
			PROCESS_FMMC_POWERUPS(dmVarsPassed.sPowerUps)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_FMMC_POWERUPS")
			#ENDIF
			#ENDIF
		ENDIF
		
		PROCESS_DM_SPECIFIC_POWERUPS_LOGIC(dmVarsPassed.sPowerUps, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DM_SPECIFIC_POWERUPS_LOGIC")
		#ENDIF
		#ENDIF
		
		PROCESS_FMMC_VEHICLE_MACHINE_GUN_FIRING(dmVarsPassed.sVehMG, playerBDPassed[iLocalPart].sScriptedMachineGun)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_FMMC_VEHICLE_MACHINE_GUN_FIRING")
		#ENDIF
		#ENDIF
		
		PROCESS_REMOTE_SCRIPTED_MACHINE_GUNS(dmVarsPassed.sVehMG, playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_REMOTE_SCRIPTED_MACHINE_GUNS")
		#ENDIF
		#ENDIF
		
		PROCESS_DELAYED_PICKUP_SPAWNING(serverBDpassed, dmVarsPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DELAYED_PICKUP_SPAWNING")
		#ENDIF
		#ENDIF
		
		//Bounds
		PROCESS_PLAYERS_READY_TO_START_SHRINKING_BOUNDS(serverBDpassed.sBoundsServerData, playerBDPassed)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_PLAYERS_READY_TO_START_SHRINKING_BOUNDS")
		#ENDIF
		#ENDIF
		
		dmVarsPassed.sShrinkingBoundsConditionData.iDeathmatchDuration = GET_ROUND_DURATION(serverBDpassed)
		PROCESS_DEATHMATCH_SHRINKING_BOUNDS_CLIENT(serverBDpassed.sBoundsServerData, playerBDPassed[iLocalPart].sShrinkingBoundsData, dmVarsPassed.sShrinkingBoundsLocalRuntimeData, 
													dmVarsPassed.sDeathmatchBoundsData, dmVarsPassed.sShrinkingBoundsConditionData)
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DEATHMATCH_SHRINKING_BOUNDS_CLIENT")
		#ENDIF
		#ENDIF
		
		IF NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
		AND NOT HAS_DEATHMATCH_FINISHED_LOCAL(dmVarsPassed)
			INT iBoundsIndex = GET_CURRENT_DM_BOUNDS_INDEX(dmVarsPassed.sPlayerModifierData)
			BOOL bDrawMarker = GET_CLIENT_DM_SHRINKING_BOUNDS_STATE(dmVarsPassed.sShrinkingBoundsLocalRuntimeData, iBoundsIndex) >= DM_SHRINKING_BOUNDS_STATE_SHRINK
			PROCESS_DEATHMATCH_BOUNDS(dmVarsPassed.sDeathmatchBoundsData, iBoundsIndex, dmVarsPassed.iPreviousBoundsIndex, PlayerPedToUse, bDrawMarker)
		ENDIF
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_DEATHMATCH_BOUNDS")
		#ENDIF
		#ENDIF
		
		IF NOT IS_THIS_LEGACY_DM_CONTENT()
			BOOL bTDM = IS_TEAM_DEATHMATCH()
			IF IS_TDM_USING_FFA_SCORING()
				bTDM = FALSE
			ENDIF
			PROCESS_KEEP_PLAYER_MODIFIER_SCORES_TO_CHECK_UP_TO_DATE(dmVarsPassed.sPlayerModifierData, GET_PLAYER_SCORE(serverBDpassed, LocalPlayer, serverBDpassedLDB), PICK_INT(bTDM, GET_TEAM_DM_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam), GET_PLAYER_SCORE(serverBDpassed, LocalPlayer, serverBDpassedLDB)))
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_KEEP_PLAYER_MODIFIER_SCORES_TO_CHECK_UP_TO_DATE")
			#ENDIF
			#ENDIF
			
			PROCESS_KEEP_PLAYER_MODIFIER_POSITION_TO_CHECK_UP_TO_DATE(dmVarsPassed.sPlayerModifierData, IS_PLAYER_WINNING_OUTRIGHT(LocalPlayer, serverBDPassed, serverBDpassedLDB, playerBDPassed), IS_PLAYER_LOSING_OUTRIGHT(LocalPlayer, serverBDPassed, serverBDpassedLDB, playerBDPassed))
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_KEEP_PLAYER_MODIFIER_POSITION_TO_CHECK_UP_TO_DATE")
			#ENDIF
			#ENDIF
			
			SET_MODIFIER_SET_TIMER_REQUIREMENT_TIME_STAMP(dmVarsPassed.sPlayerModifierData)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("SET_MODIFIER_SET_TIMER_REQUIREMENT_TIME_STAMP")
			#ENDIF
			#ENDIF
			
			PROCESS_PLAYER_MODIFIER_CONTEXT_HELP_TEXT()
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_PLAYER_MODIFIER_CONTEXT_HELP_TEXT")
			#ENDIF
			#ENDIF
			
			PROCESS_PLAYER_MODIFIER_SPAWN_VEHICLE_UPDATE(dmVarsPassed.sPlayerModifierData)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
				ADD_SCRIPT_PROFILE_MARKER("PROCESS_PLAYER_MODIFIER_SPAWN_VEHICLE_UPDATE")
			#ENDIF
			#ENDIF
		ENDIF
	ELSE
		IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
		AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
			IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				FM_MAINTAIN_MISSION_DENSITYS_THIS_FRAME(GET_DM_PED_DENSITY(), 0.0, 0.0, GET_TRAFFIC_DENSITY_FOR_DM(serverBDpassed), GET_TRAFFIC_DENSITY_FOR_DM(serverBDpassed, FALSE), GET_PARKED_TRAFFIC_DENSITY_FOR_DM(serverBDpassed, FALSE), GET_TRAFFIC_DENSITY_FOR_DM(serverBDpassed, FALSE), TRUE)
			ENDIF
		ENDIF
		
		PROCESS_FMMC_VEHICLE_MACHINE_GUN_BLOCKED_INPUTS()
		#IF IS_DEBUG_BUILD
		#IF SCRIPT_PROFILER_ACTIVE 
			ADD_SCRIPT_PROFILE_MARKER("PROCESS_FMMC_VEHICLE_MACHINE_GUN_BLOCKED_INPUTS")
		#ENDIF
		#ENDIF
		
	ENDIF
	
	SET_LBD_BOOL(HAS_SERVER_DONE_AT_LEAST_ONE_LBD_UPDATE(serverBDpassed))
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("SET_LBD_BOOL") 
	#ENDIF
	#ENDIF
	
	KILL_DM_NEARLY_FINISHED_AUDIO(serverBDpassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("KILL_DM_NEARLY_FINISHED_AUDIO")
	#ENDIF
	#ENDIF
	
	//Make the weapons
	IF NOT dmVarsPassed.bWeaponsAndPropsCreated
		IF IS_PLAYER_ON_IMPROMPTU_DM()
		OR IS_PLAYER_ON_BOSSVBOSS_DM()
			dmVarsPassed.bWeaponsAndPropsCreated = TRUE
		ELSE
			IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
			OR (HAS_DM_STARTED(serverBDPassed) AND IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed))
				IF HAS_CLIENT_CREATED_PICKUPS_AND_CREATE_FMMC_PROPS_MP(dmVarsPassed)
					IF HAVE_DM_ANIMS_LOADED()
						PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") HAS_CLIENT_CREATED_PICKUPS_AND_CREATE_FMMC_PROPS_MP = TRUE")				
						PRINTLN("INITIAL_FADE_STAGE_OBJ - waiting for HAS_CLIENT_CREATED_PICKUPS_AND_CREATE_FMMC_PROPS_MP")
						dmVarsPassed.bWeaponsAndPropsCreated = TRUE
					ELSE
						PRINTLN("STUCK  - HAVE_DM_ANIMS_LOADED = FALSE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("bWeaponsAndPropsCreated")
	#ENDIF
	#ENDIF
	
	IF DOES_ENTITY_EXIST(LocalPlayerPed)
	AND NOT IS_ENTITY_DEAD(LocalPlayerPed)
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciREMOVE_PLAYER_HELMETS)
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontTakeOffHelmet, FALSE)
			REMOVE_PLAYER_HELMET(GET_PLAYER_INDEX(), TRUE)
			PRINTLN("Calling REMOVE_PLAYER_HELMET due to ciREMOVE_PLAYER_HELMETS")
		ELSE
			PRINTLN("Calling PCF_DontTakeOffHelmet due to not ciREMOVE_PLAYER_HELMETS")
			SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_DontTakeOffHelmet, TRUE)
		ENDIF
		
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("ciREMOVE_PLAYER_HELMETS")
	#ENDIF
	#ENDIF
	
	// url:bugstar:2064834
	IF NOT CONTENT_IS_USING_ARENA()
	 // #IF FEATURE_ARENA_WARS
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
		IF DOES_ENTITY_EXIST(LocalPlayerPed)
		AND NOT IS_ENTITY_DEAD(LocalPlayerPed)
			PRINTLN("DM - Setting PRF_ForceAutoEquipHelmetsInAicraft TRUE - not checking vehicle model")
			SET_PED_RESET_FLAG(LocalPlayerPed, PRF_ForceAutoEquipHelmetsInAicraft, TRUE)
		ENDIF
	ENDIF
	ENDIF
	 // #IF FEATURE_ARENA_WARS
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PRF_ForceAutoEquipHelmetsInAicraft")
	#ENDIF
	#ENDIF
	
	// Special Spectator.
	IF IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_SENT_TO_SPECTATOR)
		PROCESS_EARLY_END_DEATHMATCH_SPECTATORS(dmVarsPassed, playerBDPassed, serverBDpassed)
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_EARLY_END_DEATHMATCH_SPECTATORS")
	#ENDIF
	#ENDIF
	
	PROCESS_PLAY_ARENA_AUDIO_SCORE(serverBDpassed, dmVarsPassed)
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("PROCESS_PLAY_ARENA_AUDIO_SCORE")
	#ENDIF
	#ENDIF
	
	IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
	AND IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	AND g_eSpecialSpectatorState != SSS_CLEANUP
		IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
			PRINTLN("[LM][SPEC_SPEC][CLIENT_MISSION_STAGE_CELEBRATION] - We are in Roaming Spectator Mode. and HAS_DEATHMATCH_FINISHED = TRUE Requesting Cleanup!")
			SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
			SET_BIT(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Dont_Cleanup_Arena_Activities)
		ENDIF
	ENDIF
	
	PROC_DEATHMATCH_ARENA_ANNOUNCER_TRACKING(serverBDpassed, playerBDPassed, dmVarsPassed)
		
	// For last player alive.
	IF SERVER_CHECKS_HAS_DM_ENDED(serverBDpassed, TRUE #IF IS_DEBUG_BUILD , dmVarsPassed #ENDIF)
	OR IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_RAN_OUT_OF_LIVES)
	OR IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF playerBDPassed[iLocalPart].iFinishedAtTime = 0
			PRINTLN("[LM][FINISH_TIME] - (5) Assigning Finished at time: ", (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000)
			playerBDPassed[iLocalPart].iFinishedAtTime = (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
		ADD_SCRIPT_PROFILE_MARKER("SERVER_CHECKS_HAS_DM_ENDED")
	#ENDIF
	#ENDIF
	
	PROCESS_ARENA_CROWD_CHEERING()
	
	#IF FEATURE_GEN9_EXCLUSIVE
		PROCESS_DISABLE_REALTIME_MULTIPLAYER(playerBDPassed)
	#ENDIF
	
	SWITCH GET_CLIENT_MISSION_STAGE(playerBDPassed)
	
		CASE CLIENT_MISSION_STAGE_INIT
		
			SET_DM_GLOBALS(serverBDPassed)
			
			PROCESS_DEATHMATCH_INIT_SCORE_AND_TIME(dmVarsPassed.sPlayerModifierData, GET_TARGET_SCORE(serverBDpassed, playerBDPassed[iLocalPart].iTeam), GET_DM_DURATION_IN_MS(serverBDpassed.iDuration))
			
			INITIALIZE_PLAYSTATS_FOR_ROUNDS()
			
			HANDLE_TEAM_START_WEAPONS(playerBDPassed[iLocalPart].iTeam)
			
			g_b_HoldupSkycamBetweenRounds = FALSE
			g_b_HoldupCutsceneBetweenRounds = FALSE
			PRINTLN("DM - INIT Setting g_b_HoldupSkycamBetweenRounds = FALSE & g_b_HoldupCutsceneBetweenRounds = FALSE ")
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Local_Player_Arrow_Team_Colour)
				SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(TRUE)
			ENDIF
			
			DISABLE_VEHICLE_MINES(TRUE)
			PRINTLN("[DM] DISABLE_VEHICLE_MINES(TRUE)")
			
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
				REQUEST_COUNTDOWN_UI(dmVarsPassed.cduiIntro)
			ENDIF
			
			HOST_SET_ARENA_AUDIO_SCORE(serverBDpassed)
					
			IF IS_PLAYER_ON_IMPROMPTU_DM()
			
				IF HAS_INITIAL_SPAWN_FINISHED(serverBDpassed, dmVarsPassed, playerBDPassed)
					CHARGE_IMPROMPTU_ENTRANCE_WAGER()
					SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
					SEND_IMPROMPTU_STARTED_TICKER()
					HANDLE_DEATHMATCH_WANTED_RATINGS(serverBDpassed)
					
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SETUP)
				ELSE
					PRINTLN("CLIENT_MISSION_STAGE_INIT, HAS_INITIAL_SPAWN_FINISHED, IS_PLAYER_ON_IMPROMPTU_DM)")
				ENDIF
			ELIF IS_PLAYER_ON_BOSSVBOSS_DM()
				IF HAS_INITIAL_SPAWN_FINISHED(serverBDpassed, dmVarsPassed, playerBDPassed)	
					IF HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM(serverBDpassed, playerBDPassed) 
					//	CHARGE_IMPROMPTU_ENTRANCE_WAGER()
						SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
						
					//	MAKE_DM_TEAMS_LIKE_FM_PLAYERS(dmVarsPassed)
					//	SEND_IMPROMPTU_STARTED_TICKER()
						HANDLE_DEATHMATCH_WANTED_RATINGS(serverBDpassed)
						
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SETUP)
					ELSE
						PRINTLN("Stuck IS_PLAYER_ON_BOSSVBOSS_DM ASSIGN_LOCAL_PLAYER_TEAM, DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING")				
				
						ASSIGN_LOCAL_PLAYER_TEAM(serverBDpassed, playerBDPassed)
					ENDIF
				ELSE
					PRINTLN("CLIENT_MISSION_STAGE_INIT, HAS_INITIAL_SPAWN_FINISHED, IS_PLAYER_ON_BOSSVBOSS_DM)")
				ENDIF
			ELSE
				
				DO_SPECTATOR_PREP(playerBDPassed)
				SET_PLAYERS_INVISIBLE(playerBDPassed)			
				INT i
				FOR i = 0 TO (FMMC_MAX_NUM_ZONES-1)
					dmVarsPassed.iZoneWaterCalmingQuadID[i] = -1
				ENDFOR
				FOR i = 0 TO (FMMC_MAX_WATER_CALMING_QUADS - 1)
					dmVarsPassed.iWaterCalmingQuad[i] = -1
				ENDFOR
				CREATE_ZONES(dmVarsPassed)
			
				IF IS_PLAYER_SPECTATOR(playerBDPassed, LocalPlayer, iLocalPart)				
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_HOLDING)	
				ELSE
					IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
						CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,-1,TRUE)
						IF NOT IS_CORONA_FREEZE_AND_START_FX_SET()
							SETUP_FAKE_CAMERA_PAUSE(dmVarsPassed)
						ENDIF
					ENDIF
					IF HAS_CORONA_CLEANED_BET_INTRO_CAM()
					OR NOT IS_PLAYER_IN_CORONA()
					#IF IS_DEBUG_BUILD
					OR GlobalServerBD_DM.bOnePlayerDeathmatch
					#ENDIF
						// Rel groups see 1672230
						MAKE_RIVAL_TEAMS_HATE_EACH_OTHER(dmVarsPassed)
						
						IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
							SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, dmVarsPassed.rgFM_DEATHMATCH[iLocalPart])	//Put all players on default team to start with
						ENDIF
						PRINTLN("DM g_FMMC_STRUCT.vStartPos", g_FMMC_STRUCT.vStartPos)
						IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
							SET_FOCUS_POS_AND_VEL(g_FMMC_STRUCT.vStartPos, <<0,0,0>>)
						ENDIF
						dmVarsPassed.timeInitialSpawn = GET_NETWORK_TIME()
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING)	
					ELSE
						PRINTLN("CLIENT_MISSION_STAGE_INIT, HAS_CORONA_CLEANED_BET_INTRO_CAM()")
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING
			IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,-1,TRUE)
				IF NOT IS_CORONA_FREEZE_AND_START_FX_SET()
					SETUP_FAKE_CAMERA_PAUSE(dmVarsPassed)
				ENDIF
			ENDIF
			IF HAVE_ALL_PLAYERS_BEEN_ASSIGNED_TEAM(serverBDpassed, playerBDPassed)
				
				IF NOT IS_BIT_SET(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INITIAL_MOD_SET_APPLICATION_DONE)
					//Do initial modifier pass here
					PROCESS_APPLICABILITY_INIT(dmVarsPassed.sPlayerModifierData, playerBDPassed[iLocalPart].iCurrentModSet, playerBDPassed[iLocalPart].iPendingModSet)
					CACHE_DM_PLAYER_CURRENT_MODSET(dmVarsPassed, playerBDPassed[iLocalPart].iCurrentModSet)
				ENDIF
				
				IF HAS_INITIAL_SPAWN_FINISHED(serverBDpassed, dmVarsPassed, playerBDPassed)	
					SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
					//HANDLE_TANK_TURRETS(serverBDpassed, dmVarsPassed)
					
					//START_VEHICLE_RADIO(dmVarsPassed)		
					
					REQUEST_APPLICABILITY_UPDATE(dmVarsPassed.sPlayerModifierData)
					
					VECTOR vPlayerPos
					vPlayerPos = GET_PLAYER_COORDS(LocalPlayer)
					PRINTLN(" CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - PLAYER COORDS = ", vPlayerPos)
					PRINTLN(" CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - CORONA COORDS = ", g_FMMC_STRUCT.vStartPos)
					PRINTLN(" CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - DISTANCE BETWEEN PLAYER POS AND CORONA POS = ", GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, g_FMMC_STRUCT.vStartPos))
					
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, g_FMMC_STRUCT.vStartPos) < 200
					AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
						NEW_LOAD_SCENE_START_SPHERE(vPlayerPos, 20.0)
						dmVarsPassed.timeInitialLoadScene = GET_NETWORK_TIME()
						PRINTLN(" CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - DO LOAD SCENE")
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN(" CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING - SKIP LOAD SCENE")
					#ENDIF
					ENDIF
					
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_HOLDING)	
				ELSE
					PRINTLN("Stuck HAS_INITIAL_SPAWN_FINISHED, DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING")				
				ENDIF
			ELSE						
				PRINTLN("Stuck ASSIGN_LOCAL_PLAYER_TEAM, DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") CLIENT_MISSION_STAGE_TEAMS_AND_SPAWNING")				
				
				ASSIGN_LOCAL_PLAYER_TEAM(serverBDpassed, playerBDPassed)
				
			ENDIF
		BREAK
		
		// Waits in here at corona stage
		CASE CLIENT_MISSION_STAGE_HOLDING
		
			BOOL bCanProceed
			bCanProceed = TRUE
			
			IF IS_SKYSWOOP_IN_SKY()
				IF bIsSCTV
				OR DID_I_JOIN_MISSION_AS_SPECTATOR()
					IF IS_SCREEN_FADED_OUT()
						IF g_MissionControllerserverBD_LB.iHashMAC = 0 
							IF NOT HAS_NET_TIMER_STARTED(g_FMMC_STRUCT.stWaitForHashMACTimer)
								START_NET_TIMER(g_FMMC_STRUCT.stWaitForHashMACTimer)
								bCanProceed = FALSE
								PRINTLN("[SPECTRANS] Screen faded out - waiting for g_MissionControllerserverBD_LB.iHashMAC to be set - starting g_FMMC_STRUCT.stWaitForHashMACTimer and setting bCanProceed to FALSE")
							ELIF NOT HAS_NET_TIMER_EXPIRED(g_FMMC_STRUCT.stWaitForHashMACTimer, 10000)
								bCanProceed = FALSE
								PRINTLN("[SPECTRANS] Screen faded out - waiting for g_MissionControllerserverBD_LB.iHashMAC to be set - setting bCanProceed to FALSE")
							ENDIF
							
							PRINTLN("[SPECTRANS] Screen faded out - waiting for g_MissionControllerserverBD_LB.iHashMAC to be set - Leaving bCanProceed as TRUE because g_FMMC_STRUCT.stWaitForHashMACTimer has expired")
						ELSE			
							PRINTLN("[SPECTRANS] Leaving bCanProceed as TRUE because the screen is already faded")
						ENDIF
					ELSE
						bCanProceed = FALSE
						DO_SCREEN_FADE_OUT(1000)
						PRINTLN("[SPECTRANS] Need to fade out - setting bCanProceed to FALSE")
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_INITIAL_LOADSCENE_COMPLETED(playerBDPassed, dmVarsPassed)
			AND bCanProceed
				IF IS_CORONA_READY_TO_START_WITH_JOB()
				OR IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_CORONA_READY)
				OR (HAS_DM_STARTED(serverBDPassed) AND IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed))
					PRINTLN("CLIENT_MISSION_STAGE_HOLDING, IS_CORONA_READY_TO_START_WITH_JOB")
					IF bIsSCTV
						PRINTLN("------------------------------------------------------------------------------")
						PRINTLN("--                   JOINING_DM_SCTV					 		 			 --")
						PRINTLN("------------------------------------------------------------------------------")
						IF NOT g_bArenaBigScreenBinkPlaying
							IF NOT CONTENT_IS_USING_ARENA()
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
									SET_TIME_OF_DAY(serverBDPassed.iTimeOfDay) //*1594515
								ELSE
									NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
								ENDIF
								IF CONTENT_IS_USING_ARENA()
									SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
									CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[TMS][ARENA][1] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
						ENDIF
						
						IF CONTENT_IS_USING_ARENA()
							DISABLE_SPECTATOR_FILTER_OPTION(TRUE)
						ENDIF
						
						GRAB_GAMER_HANDLES(dmVarsPassed.lbdVars)
						START_DM_SCORE(dmVarsPassed)
						SET_SPECTATOR_CHAT(serverBDPassed)
						Clear_All_Generated_MP_Headshots(FALSE) // Stop players having wrong headshot in deathmatch leaderboard.
						
						// Make sure we clean up all corona effects when entering as SCTV
						CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,-1,TRUE)
						CLEANUP_ALL_CORONA_FX(FALSE)
						SET_SKYFREEZE_CLEAR()
						
						IF ANIMPOSTFX_IS_RUNNING("MP_job_load")
							ANIMPOSTFX_STOP("MP_job_load")
						ENDIF
						SET_SCTV_TICKER_SCORE_ON()
						DEAL_WITH_FM_MATCH_START(g_FMMC_STRUCT.iMissionType, DEATHMATCH_GET_POSIX_FOR_PLAYSTATS(serverBDPassed), DEATHMATCH_GET_HASHMAC_FOR_PLAYSTATS(serverBDPassed))		
						RESET_CAM_VIEW_FOR_ROAMING_SPECTATOR()
						
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SCTV)
					ELIF DID_I_JOIN_MISSION_AS_SPECTATOR()
						IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAITING_FOR_EXTERNAL_TERMINATION_CALL
						OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_MAINTRANSITION
						OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_SP_SWOOP_DOWN
							
							PRINTLN("------------------------------------------------------------------------------")
							PRINTLN("--                   JOINING_DM_AS_SPECTATOR						 		 --")
							PRINTLN("------------------------------------------------------------------------------")
							IF NOT g_bArenaBigScreenBinkPlaying
								IF NOT CONTENT_IS_USING_ARENA()
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
										SET_TIME_OF_DAY(serverBDPassed.iTimeOfDay) //*1594515
									ELSE
										NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
									ENDIF
									IF CONTENT_IS_USING_ARENA()
										SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
										CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[TMS][ARENA][2] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
							ENDIF
							
							GRAB_GAMER_HANDLES(dmVarsPassed.lbdVars)
							START_DM_SCORE(dmVarsPassed)
							SHUTDOWN_LOADING_SCREEN()
							TAKE_CONTROL_OF_TRANSITION()
							SET_SPECTATOR_CHAT(serverBDPassed)
							DEAL_WITH_FM_MATCH_START(g_FMMC_STRUCT.iMissionType, DEATHMATCH_GET_POSIX_FOR_PLAYSTATS(serverBDPassed), DEATHMATCH_GET_HASHMAC_FOR_PLAYSTATS(serverBDPassed))
							Clear_All_Generated_MP_Headshots(FALSE) // Stop players having wrong headshot in deathmatch leaderboard.
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_CLEAR_TASKS | NSPC_LEAVE_CAMERA_CONTROL_ON | NSPC_FREEZE_POSITION)
							RESET_CAM_VIEW_FOR_ROAMING_SPECTATOR()
														
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SPECTATOR)
						ELSE
							#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_SPECTATOR, 	"=== DM === Not moving to spectate as GET_CURRENT_TRANSITION_STATE = ", 
														GET_TRANSITION_STATE_STRING(GET_CURRENT_TRANSITION_STATE()))
							#ENDIF
						ENDIF
					ELSE
						// THIS IS THE BIT WHERE WE CAN DO STUFF EARLY IF SUITABLE
						#IF IS_DEBUG_BUILD
						PRINTLN("------------------------------------------------------------------------------")
						PRINTLN("--                   JOINING_DM_AS_FIGHTER							 		 --")
						PRINTLN("------------------------------------------------------------------------------")
						INITIALISE_DEBUG_LBD_TEAMS(dmVarsPassed)
						#ENDIF
						IF NOT IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
							CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
							SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE)
							PRINTLN("=== HUD === [2339732] SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RESET_STATE) ")
						ENDIF
						CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,-1,TRUE)
						IF NOT IS_CORONA_FREEZE_AND_START_FX_SET()
							SETUP_FAKE_CAMERA_PAUSE(dmVarsPassed)
						ENDIF
						HANDLE_DEATHMATCH_WANTED_RATINGS(serverBDpassed)
						MUTE_RADIOS(dmVarsPassed)
						IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
						OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, dmVarsPassed.sPlayerModifierData)
							IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
								PRINTLN("CLIENT_MISSION_STAGE_HOLDING, SET_ENTITY_VISIBLE")
								SET_ENTITY_VISIBLE(dmVarsPassed.dmVehIndex, TRUE)
							ENDIF
						ENDIF
						IF NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
							STORE_PLAYER_HEADWEAR(IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciREMOVE_PLAYER_HELMETS))
						ENDIF
						Clear_All_Generated_MP_Headshots(FALSE) // Stop players having wrong headshot in deathmatch leaderboard.

						SET_ON_JOB_INTRO(TRUE)
						
						IF NOT IS_KING_OF_THE_HILL()
							IF IS_ARENA_WARS_JOB(TRUE)
								SET_LOCAL_PLAYER_STARTED_ARENA_EVENT()
								PRINTLN("[ARENA] Calling SET_LOCAL_PLAYER_STARTED_ARENA_EVENT")
							ENDIF
						ENDIF
						
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SETUP)	
					ENDIF		
				ENDIF
			ENDIF
		BREAK
	
		CASE CLIENT_MISSION_STAGE_SETUP
			HIDE_RADAR_DURING_INITIAL_STAGES(playerBDPassed)
			SWITCH GET_INITIAL_STAGE(playerBDPassed)
			
				CASE INITIAL_FADE_STAGE_START
					
					IF CONTENT_IS_USING_ARENA()
						CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
						IF IS_VALID_INTERIOR(g_ArenaInterior)
						AND IS_INTERIOR_READY(g_ArenaInterior)
							IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.tdArenaSpawnTimer)
								REINIT_NET_TIMER(dmVarsPassed.tdArenaSpawnTimer)
								PRINTLN("DM - CLIENT_MISSION_STAGE_INIT - Holding up as arena starting arena timer")
								EXIT
							ELSE
								IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.tdArenaSpawnTimer, 1000)
									IF NOT IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
										RETAIN_ENTITY_IN_INTERIOR(LocalPlayerPed, g_ArenaInterior)
										FORCE_ROOM_FOR_ENTITY(LocalPlayerPed, g_ArenaInterior, 289098304)
										IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
											FORCE_ROOM_FOR_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), g_ArenaInterior, 289098304)
											PRINTLN("DM - CLIENT_MISSION_STAGE_INIT, moving vehicle")
										ENDIF
										PRINTLN("DM - CLIENT_MISSION_STAGE_INIT, HAS_INITIAL_SPAWN_FINISHED - Arena Wars Job - Forcing player into the arena. Room Key: 289098304 (arena)")
									ENDIF
								ELSE
									PRINTLN("DM - CLIENT_MISSION_STAGE_INIT - Holding up as arena arena timer not expired")
									EXIT
								ENDIF
							ENDIF
						ELSE
							PRINTLN("DM - CLIENT_MISSION_STAGE_INIT - Holding up as arena interior not ready yet")
							EXIT
						ENDIF
					ENDIF
					
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					#IF IS_DEBUG_BUILD	START_PRINTING_USEFUL_DM_STUFF(serverBDPassed) #ENDIF
					SETUP_SCRIPTED_COVER_POINTS()
					IF IS_PLAYER_ON_IMPROMPTU_DM()
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
						SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE)
						REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_ONE_ON_ONE_DM()
						PRINTLN("CLIENT_MISSION_STAGE_SETUP, INITIAL_FADE_STAGE_START, CLIENT_BITSET_ENTERED_IMPROMPTU")
						SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_ENTERED_IMPROMPTU)
						CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_ONE_ON_ONE_DM)
						IF IS_PLAYER_ON_IMPROMPTU_DM()
							DO_IMPROMPTU_OPENING_SPLASH()
						ENDIF
						
						IF IS_PLAYER_ON_BOSSVBOSS_DM()
							IF NOT DO_BOSSVBOSS_OPENING_SPLASH(serverBDpassed)
								EXIT
							ENDIF
						ENDIF
					ELSE
						//SETUP_FAKE_CAMERA_PAUSE(dmVarsPassed)
					ENDIF
					
					SETUP_VEH_DM(serverBDpassed, playerBDPassed, dmVarsPassed.sPlayerModifierData)
					SET_KILLSTRIP_AUTO_RESPAWN(TRUE)
					SET_KILLSTRIP_AUTO_RESPAWN_TIME(GET_DM_RESPAWN_TIME())
					DO_STARTING_STATS(serverBDPassed)
					CLIENT_HANDLE_AMBIENT_TRAFFIC() 			// 885412   
					//GET_PED_STARTING_ARMOUR(dmVarsPassed.ARMOUR_VALUE)
					IF NOT IS_BIT_SET(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_MAX_HEALTH_CHANGED)
						DEFAULT_PLAYER_HEALTH_AND_REMOVE_ARMOUR(FALSE) 	// 925893
						GIVE_PLAYER_HEALTH()
					ENDIF
					APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS(FALSE, TRUE, FALSE)
					STORE_STARTING_XP(dmVarsPassed)
					CLIENT_STORES_LAST_NUM_TEAMS_FOR_RESTART(serverBDpassed.iNumberOfTeams)
					playerBDPassed[iLocalPart].bEnteredDM = TRUE // Lbd
					SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
					SET_AUDIO_FLAG("DisableFlightMusic", TRUE)
					TOGGLE_STRIPPER_AUDIO(FALSE)
					APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS()
					SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
					
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						CLIENT_SET_POLICE(serverBDpassed.iPolice)
					ENDIF
					PROCESS_INTERIOR_BLIP_DISPLACEMENT(dmVarsPassed)
					PROCESS_DM_WEAPON_DAMAGE_MODIFIERS()
					GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_FADE_STAGE_FADE_OUT)
				BREAK
				
				CASE INITIAL_FADE_STAGE_FADE_OUT
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF IS_PLAYER_ON_IMPROMPTU_DM()	
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
						IF (IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed) AND NOT IS_PLAYER_IN_DM_VEH_MODEL())
							DO_SCREEN_FADE_OUT(2000)
						ENDIF
					ELSE
						// (639025)
						REFRESH_ALL_OVERHEAD_DISPLAY()
						SET_WEATHER_FOR_FMMC_MISSION(serverBDpassed.iWeather)
						
						CLIENT_CLEAR_WHOLE_MAP_AREA()
						
						IF bLocalPlayerOK
							CLEAR_AREA_OF_VEHICLES(vLocalPlayerPosition, 500, FALSE, FALSE, TRUE, TRUE, TRUE)	//This will only clear the Wrecked Vehicles due to that flag being set.
							CLEAR_AREA_OF_COPS(vLocalPlayerPosition, 500, TRUE)
							CLEAR_AREA_OF_PEDS(vLocalPlayerPosition, 500, TRUE)
						ENDIF
						TAKE_CONTROL_OF_TRANSITION() 
					ENDIF
					SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_FOR_DEATHMATCH(TRUE)		
					SET_UP_OVERHEAD_NAMES_DM(serverBDpassed)												
					SET_FM_MISSION_AS_JOINABLE() 
					// Store team for leaderboards
					playerBDPassed[iLocalPart].iTeam = GET_PLAYER_TEAM(LocalPlayer)	
					PRINTLN("playerBDPassed[iLocalPart].iTeam ", playerBDPassed[iLocalPart].iTeam)
					PRINTLN("MY_TEAM_IS --->  ", GET_PLAYER_TEAM(LocalPlayer) )
					DISABLE_MP_PASSIVE_MODE(PMER_START_OF_A_MATCH)
					// Stop teammates shooting each other
					IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
						PRINTLN("---------->     NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)")
						NETWORK_SET_FRIENDLY_FIRE_OPTION(FALSE)
						PRINTLN("SET_PED_RELATIONSHIP_GROUP_HASH, ", playerBDPassed[iLocalPart].iTeam)
						SET_PED_RELATIONSHIP_GROUP_HASH(LocalPlayerPed, dmVarsPassed.rgFM_DEATHMATCH[playerBDPassed[iLocalPart].iTeam])
						COLOUR_PC_TEXT_CHAT(TRUE)
						
						IF IS_PLAYER_ON_BOSSVBOSS_DM()
					//		MAKE_DM_TEAMS_LIKE_FM_PLAYERS(dmVarsPassed, playerBDPassed)
						ENDIF
					ENDIF	
					
					GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_FADE_STAGE_FADED_OUT)
				BREAK
				
				CASE INITIAL_FADE_STAGE_FADED_OUT	
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					ASSIGN_CLIENT_DM_OBJECTIVE(serverBDpassed.iNumDmStarters, IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed))
					PREPARE_PLAYER_AFTER_INITIAL_SPAWN(serverBDpassed, playerBDPassed, dmVarsPassed)
					INITIALISE_CLIENT_BROADCAST_DATA(playerBDPassed)
					dmVarsPassed.timeWarp = GET_NETWORK_TIME()
						IF IS_PLAYER_ON_BIKER_DM()
							GB_SET_COMMON_TELEMETRY_DATA_ON_START(DEFAULT, DEFAULT, TRUE, FMMC_TYPE_GB_BOSS_DEATHMATCH)
							GB_SET_PLAYER_AS_QUALIFYING_PARTICIPANT()
							GB_SET_PLAYER_AS_PERMANENT_ON_BOSS_MISSION() 
						ENDIF
					
					IF CONTENT_IS_USING_ARENA()
						IF IS_ENTITY_ALIVE(dmVarsPassed.dmVehIndex)
							MODEL_NAMES mn 
							mn = GET_ENTITY_MODEL(dmVarsPassed.dmVehIndex)
							IF GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(mn) != WEAPONTYPE_UNARMED
								SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(mn))
								PRINTLN("[ARENA] SET_CURRENT_PED_VEHICLE_WEAPON - Setting weapon to ", ENUM_TO_INT(GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(mn)))
							ENDIF
						ENDIF
					ENDIF
					
					GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_FADE_STAGE_OBJ)
				BREAK	
				
				CASE INITIAL_FADE_STAGE_OBJ
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF NOT BUSYSPINNER_IS_ON()
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
						END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
					ENDIF
					IF dmVarsPassed.bWeaponsAndPropsCreated = TRUE
						//HANDLE_TANK_TURRETS(serverBDpassed, dmVarsPassed)					
					
						GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_FADE_STAGE_FADE_IN)
					ELSE
						PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") HAS_CLIENT_CREATED_PICKUPS")				
						PRINTLN("INITIAL_FADE_STAGE_OBJ - waiting for HAS_CLIENT_CREATED_PICKUPS") 
					ENDIF
				BREAK

				CASE INITIAL_FADE_STAGE_FADE_IN
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF NOT BUSYSPINNER_IS_ON()
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
						END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
					ENDIF
					IF HAS_SERVER_CHECKED_EVERYONE_READY(serverBDpassed)
					OR IS_PLAYER_ON_IMPROMPTU_DM()
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
						CHECK_AND_REWARD_ROCKSTAR_RACES_AND_DEATHMATCHES()
						START_DM_SCORE(dmVarsPassed)
						IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
						AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
							DO_SCREEN_FADE_IN(500)
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_CLEAR_TASKS ) //*1424566
						ELSE
							IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
							AND NOT IS_PLAYER_RESPAWNING(LocalPlayer)
								DO_SCREEN_FADE_IN(250)
							ENDIF
						ENDIF
						g_b_TransitionActive = FALSE
						
						IF NOT g_bArenaBigScreenBinkPlaying
							IF NOT CONTENT_IS_USING_ARENA()
								IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
									SET_TIME_OF_DAY(serverBDPassed.iTimeOfDay) //*1594515
								ELSE
									NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
								ENDIF
								IF CONTENT_IS_USING_ARENA()
									SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
									CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
								ENDIF
							ENDIF
						ELSE
							PRINTLN("[TMS][ARENA][3] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
						ENDIF						
						
						IF IS_ARENA_WARS_JOB()
							SET_RADIO_TO_STATION_NAME("OFF") 
						ENDIF
						
						IF IS_THIS_TEAM_DEATHMATCH(serverBDPassed)
							IF IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_TEAM_CPS_DONE)
							AND HAS_COLLISION_LOADED_AROUND_ENTITY(LocalPlayerPed)
								IF WORK_OUT_CENTRE_POINT(serverBDpassed,playerBDPassed)
									IF TEAM_CAMERA_CHECKS(dmVarsPassed.DMIntroData, serverBDpassed.vTeamCentreStartPoint[playerBDPassed[iLocalPart].iTeam])
									AND GET_NUMBER_OF_CLOSEBY_PLAYERS(serverBDpassed,playerBDPassed) > 6
									//IF IS_AREA_FOR_TEAM_CUT_CLEAR(dmVarsPassed.DMIntroData, serverBDpassed.vTeamCentreStartPoint[playerBDPassed[iLocalPart].iTeam])
										GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_TEAM_OVERVIEW) //INITIAL_CAMERA_TEAM_OVERVIEW
									ELSE
										IF dmVarsPassed.DMIntroData.iCameraAttempts > 5//6 //We have used all camera attemps
										OR GET_NUMBER_OF_CLOSEBY_PLAYERS(serverBDpassed,playerBDPassed) <= 6
											//CLEANUP_ALL_CORONA_FX(FALSE)
											//SET_SKYFREEZE_CLEAR()
											dmVarsPassed.DMIntroData.iCameraAttempts = 0 //Reset as they are used again below
											GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_PLAYER_CUT) //INITIAL_CAMERA_OVERVIEW
										ENDIF
									ENDIF
								ELSE
									//CLEANUP_ALL_CORONA_FX(FALSE)
									//SET_SKYFREEZE_CLEAR()
									GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_PLAYER_CUT) //INITIAL_CAMERA_OVERVIEW
								ENDIF
							ELSE
								IF HAS_COLLISION_LOADED_AROUND_ENTITY(LocalPlayerPed)
									PRINTLN("INITIAL_CAMERA_TEAM_OVERVIEW : STUCK - WAITING FOR SERVER - SERV_BITSET_TEAM_CPS_DONE")
								ELSE
									PRINTLN("INITIAL_CAMERA_TEAM_OVERVIEW : STUCK - WAITING FOR SERVER - HAS_COLLISION_LOADED_AROUND_ENTITY is not true")
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
							AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
							//AND NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed) // See B*1635679 - vehicle deathmatches now have intro cameras
								GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_PLAYER_CUT)//INITIAL_CAMERA_OVERVIEW
							ELSE
								GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_BLEND_OUT)
							ENDIF
						ENDIF
					ELSE
						PRINTLN("INITIAL_FADE_STAGE_FADE_IN : STUCK - WAITING FOR SERVER - HAS_SERVER_CHECKED_EVERYONE_READY")
					ENDIF
					PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") INITIAL_FADE_STAGE_FADE_IN")				
				BREAK
				
				CASE INITIAL_CAMERA_TEAM_OVERVIEW
					PRINTLN("DM - TRANSITION TIME - F-(", GET_FRAME_COUNT(), ") S(", GET_CLOUD_TIME_AS_INT(), ") INITIAL_CAMERA_TEAM_OVERVIEW")				
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						DEAL_WITH_PLAYER_ANIMATIONS()
					ENDIF
					IF PROCESS_NEW_TEAM_OVERVIEW_CUT(dmVarsPassed.DMIntroData,serverBDpassed.vTeamCentreStartPoint[playerBDPassed[iLocalPart].iTeam],serverBDPassed.iDeathmatchType)
						CLEANUP_ALL_CORONA_FX(FALSE)
						SET_SKYFREEZE_CLEAR()
						GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_BLEND_OUT) //INITIAL_CAMERA_PLAYER_CUT
					ENDIF
				BREAK
				
				CASE INITIAL_CAMERA_OVERVIEW
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF IS_SCREEN_FADING_IN()
					OR IS_SCREEN_FADED_IN()
						IF IS_PLAYER_ON_IMPROMPTU_DM()
						OR IS_PLAYER_ON_BOSSVBOSS_DM()
						//OR IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed) // See B*1635679 - vehicle deathmatches now have intro cameras so will use the following PROCESS_JOB_OVERVIEW_CUT
						OR PROCESS_JOB_OVERVIEW_CUT(dmVarsPassed.DMIntroData, TRUE)
							CLEANUP_ALL_CORONA_FX(FALSE)
							SET_SKYFREEZE_CLEAR()
							
							GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_PLAYER_CUT)
						ENDIF
					ENDIF
				BREAK
				
				CASE INITIAL_CAMERA_PLAYER_CUT
					bHideHud = TRUE
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF IS_PLAYER_ON_IMPROMPTU_DM() //*1415642
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
					OR PROCESS_DEATHMATCH_STARTING_CUT(dmVarsPassed.DMIntroData,serverBDpassed.iNumDMPlayers,serverBDPassed.iDeathmatchType)
					
						ANIMPOSTFX_STOP("MP_job_load")
						CLEANUP_ALL_CORONA_FX(FALSE)
						SET_SKYFREEZE_CLEAR()
						CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
						//SET_INVINCIBLE_START_OF_JOB(FALSE)
						DISABLE_SCRIPT_HUD(HUDPART_MAP, FALSE)
						DISPLAY_RADAR(TRUE)
						GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_CAMERA_BLEND_OUT) //INITIAL_FADE_STAGE_FADED_IN
					ENDIF
				BREAK
				
				CASE INITIAL_CAMERA_BLEND_OUT
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					IF IS_PLAYER_ON_IMPROMPTU_DM()
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
					//OR IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed) // See B*1635679 - vehicle deathmatches now have intro cameras so will use the following PROCESS_DEATHMATCH_BLEND_OUT
					OR PROCESS_DEATHMATCH_BLEND_OUT(dmVarsPassed.DMIntroData,serverBDPassed.iDeathmatchType)
						ANIMPOSTFX_STOP("MP_job_load")
						CLEANUP_ALL_CORONA_FX(FALSE)
						SET_SKYFREEZE_CLEAR()
						ENABLE_ALL_MP_HUD()
						SET_INVINCIBLE_START_OF_JOB(FALSE)
						CLEANUP_CELEBRATION_SCALEFORMS(dmVarsPassed.DMIntroData)
						GOTO_INITIAL_STAGE(playerBDPassed, INITIAL_FADE_STAGE_FADED_IN)
					ENDIF
				BREAK
				
				CASE INITIAL_FADE_STAGE_FADED_IN
					
					bHideHud = TRUE
					
					IF IS_PLAYER_ON_BOSSVBOSS_DM()
						bHideHud = FALSE
					ENDIF
					
					IF bHideHud
						HIDE_HUD_AND_RADAR_THIS_FRAME()
					ENDIF
					
					BOOL bReady
					bReady = TRUE
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
						INT iPlayer
						FOR iPlayer = 0 TO NUM_NETWORK_PLAYERS-1	
							PLAYER_INDEX tempPlayer
							tempPlayer = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
							IF tempPlayer != INVALID_PLAYER_INDEX()
							AND NETWORK_IS_PLAYER_ACTIVE(tempPlayer)
							AND NOT IS_PLAYER_USING_ACTIVE_ROAMING_SPECTATOR_MODE(tempPlayer)
								IF playerBDPassed[iPlayer].iInitialFadeProgress < INITIAL_FADE_STAGE_FADED_IN
								AND playerBDPassed[iPlayer].eClientLogicStage < CLIENT_MISSION_STAGE_FIGHTING
								AND NOT IS_PLAYER_SPECTATOR(playerBDPassed, tempPlayer, iPlayer)
								AND NOT IS_PLAYER_IN_PROCESS_OF_JOINING_MISSION_AS_SPECTATOR(tempPlayer)
								AND NOT IS_THIS_PLAYER_ON_JIP_WARNING(tempPlayer)
									PRINTLN("INITIAL_FADE_STAGE_FADED_IN - Waiting for iPlayer: ", iPlayer, " | ", GET_PLAYER_NAME(tempPlayer))
									bReady = FALSE
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					
					IF bReady
						IF IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							IF IS_PLAYER_ON_IMPROMPTU_DM()
							OR IS_PLAYER_ON_BOSSVBOSS_DM()
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
								HANDLE_VEH_JACKING(serverBDpassed, TRUE)
							ELSE
								NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
							ENDIF					
							START_NET_TIMER(dmVarsPassed.stRewardPercentageTimer)
							SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
							STARTED_DM_STAT()
							GRAB_NUM_TIMES_DM_STARTED_STAT(dmVarsPassed) // Must be after incrementing
							CLIENT_SET_VOICE_CHAT(serverBDpassed) //976900
							DEAL_WITH_FM_MATCH_START(g_FMMC_STRUCT.iMissionType, DEATHMATCH_GET_POSIX_FOR_PLAYSTATS(serverBDPassed), DEATHMATCH_GET_HASHMAC_FOR_PLAYSTATS(serverBDPassed))
							ACTIVATE_KILL_TICKERS(TRUE) 
							FORCE_TIMER_ORDER_REFRESH()
							STORE_PLAYER_INTERACTION_ANIM_VALUES_FOR_CELEBRATION_SCREEN()
							CLEANUP_JOB_INTRO_CUTSCENE(dmVarsPassed.DMIntroData, FALSE, FALSE) //Do not to interp *1415642
							CLEANUP_NEW_ANIMS(dmVarsPassed.DMIntroData)
							CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam,FMMC_TYPE_DEATHMATCH,TRUE)
							CLEANUP_ALL_CORONA_FX(FALSE)
							SET_SKYFREEZE_CLEAR()
							APPLY_CORONA_ALCOHOL_AND_SMOKE_EFFECTS(FALSE, FALSE, TRUE)
							IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
								CLEAR_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
							ENDIF
							PROCESS_INTERIOR_BLIP_DISPLACEMENT(dmVarsPassed)
							ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
							
							IF NOT CONTENT_IS_USING_ARENA()
								PLAY_SOUND_FRONTEND(-1, "Screen_Flash_Start", "Deathmatch_Sounds")
							ENDIF
							
							IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
							OR IS_ANY_SPAWN_VEHICLE_MODIFIER_SET_FOR_DM()
							OR CONTENT_IS_USING_ARENA()
							
								IF CONTENT_IS_USING_ARENA()
									IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
										PLAY_SOUND_FRONTEND(-1, "Start", "DLC_AW_Frontend_Sounds")
										PRINTLN("[MJL] [NET_DEATHMATCH] [PROCESS_CLIENT_DEATHMATCH] PLAY_SOUND_FRONTEND, Start ")
									ENDIF
								ENDIF
								
							 // #IF FEATURE_ARENA_WARS
							
								IF DOES_ENTITY_EXIST(dmVarsPassed.dmVehIndex)
									IF IS_VEHICLE_DRIVEABLE(dmVarsPassed.dmVehIndex)
										IF NETWORK_HAS_CONTROL_OF_ENTITY(dmVarsPassed.dmVehIndex)
											SET_ENTITY_INVINCIBLE(dmVarsPassed.dmVehIndex, FALSE)
											PRINTLN("INITIAL_FADE_STAGE_FADED_IN, SET_ENTITY_INVINCIBLE, dmVehIndex, FALSE 1 ")
										ENDIF
	//									FREEZE_ENTITY_POSITION(dmVarsPassed.dmVehIndex, FALSE)
										IF NOT CONTENT_IS_USING_ARENA()
										AND NOT IS_NON_ARENA_KING_OF_THE_HILL()
											SET_VEHICLE_AS_NO_LONGER_NEEDED(dmVarsPassed.dmVehIndex)
										ENDIF
										PRINTLN("INITIAL_FADE_STAGE_FADED_IN, SET_VEHICLE_AS_NO_LONGER_NEEDED ")
										PRINTLN("5342883, INITIAL_FADE_STAGE_FADED_IN, SET_VEHICLE_AS_NO_LONGER_NEEDED")
									ENDIF
								ENDIF
							ENDIF
							
							// 2363579 failsafe
							IF IS_PLAYER_ON_IMPROMPTU_DM()
							OR IS_PLAYER_ON_BOSSVBOSS_DM()
							OR IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
								IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed) 
									IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
										IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
											IF IS_PLAYER_ON_IMPROMPTU_DM()
											OR IS_PLAYER_ON_BOSSVBOSS_DM()
												IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
													FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
													PRINTLN("INITIAL_FADE_STAGE_FADED_IN, FREEZE_ENTITY_POSITION, FALSE 2 ")
												ENDIF
											ENDIF
											IF NETWORK_HAS_CONTROL_OF_ENTITY(GET_VEHICLE_PED_IS_IN(LocalPlayerPed))
												SET_ENTITY_INVINCIBLE(GET_VEHICLE_PED_IS_IN(LocalPlayerPed), FALSE)
												PRINTLN("INITIAL_FADE_STAGE_FADED_IN, SET_ENTITY_INVINCIBLE, FALSE 2 ")
											ENDIF
										ENDIF
									ENDIF
								ENDIF						
							ENDIF
							dmVarsPassed.eFreeze = eFREEZE_STATE_NO
							SET_HIDOF_OVERRIDE(FALSE,FALSE,0,0,0,0)
	//						SETUP_RADAR(serverBDpassed, playerBDPassed, dmVarsPassed)
							START_AUDIO_SCENE("MP_DM_GENERAL_SCENE") 
							SETUP_DM_AUDIO_GROUPS()
							//gdisablerankupmessage = FALSE
							IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
								SET_DISABLE_RANK_UP_MESSAGE(FALSE)
							ENDIF
							CLEAR_ALL_DAMAGE_EVENT_BITS(serverBDpassed)
							//Set their team decarator
							SET_LOCAL_PLAYERS_FM_TEAM_DECORATOR(playerBDPassed[iLocalPart].iTeam)
							GRAB_GAMER_HANDLES(dmVarsPassed.lbdVars)
							SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(TRUE)
							SET_ON_JOB_INTRO(FALSE)
							
							ALLOW_PLAYER_HEIGHTS(TRUE)

							STORE_GARAGE_SLOT(playerBDPassed)
							
							DISABLE_VEHICLE_MINES(TRUE)
							PRINTLN("[DM] DISABLE_VEHICLE_MINES(TRUE)")
							
							SET_IDLE_KICK_TIME_OVERRIDDEN(180000)
													
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_FIGHTING)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CLIENT_MISSION_STAGE_FIGHTING
			
			PROCESS_INTERIOR_BLIP_DISPLACEMENT(dmVarsPassed)						
			g_bMission321Done = FALSE
			IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
			AND bLocalPlayerPedOK
				IF NOT HAS_3_2_1_GO_FINISHED(dmVarsPassed, serverBDpassed, playerBDPassed)					
					EXIT
				ELSE
					IF bIsLocalPlayerHost
						START_DM_TIMER(serverBDpassed)
					ENDIF
				ENDIF
			ENDIF						
			g_bMission321Done = TRUE
			IF NOT g_bArenaBigScreenBinkPlaying
				IF NOT CONTENT_IS_USING_ARENA()
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
						IF CONTENT_IS_USING_ARENA()
							SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
							CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
						ENDIF
					ENDIF
				ENDIF		
			ELSE
				PRINTLN("[TMS][ARENA][4] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
			ENDIF
			
			MAINTAIN_DM_VEHICLE_INITIALIZATIONS(playerBDPassed, dmVarsPassed)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_DM_VEHICLE_INITIALIZATIONS")
			#ENDIF
			#ENDIF
			
			//-- Help text for Boss Vs Boss deathmatch
			MAINTAIN_BVB_HELP(dmVarsPassed, serverBDpassed)
			
			//-- Objective text for Boss Vs Boss deathmatch
			MAINTAIN_BVB_OBJ(serverBDpassed, dmVarsPassed)
			
			//-- Draw a marker above the opposing boss's head
			MAINTAIN_BVB_BOSS_MARKER(serverBDpassed)
			
			//-- Blip rival boss with custom blip
			MAINTAIN_BVB_BOSS_BLIP(serverBDpassed, dmVarsPassed)
			
			//-- Don't allow particpants to be driven around by non-participants
			MAINTAIN_PLAYER_DRIVEN_BY_NON_PARTICIPANT()
			
			//-- Deal with players hiding in map holes
			//HANDLE_MAP_EXITING_PLAYERS()
			
			 //FEATURE_GANG_BOSS
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_BVB_STUFF")
			#ENDIF
			#ENDIF
			
			DRAW_DM_HUD(LocalPlayer, serverBDpassed, DRAW_AMMO(serverBDpassed), dmVarsPassed, serverBDpassedLDB, playerBDPassed)
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE 
				ADD_SCRIPT_PROFILE_MARKER("DRAW_DM_HUD")
			#ENDIF
			#ENDIF
						
			PROCESS_EXPLODE_REMAINING_PLAYERS_AT_END(playerBDPassed)
			
			IF NOT HAS_PLAYER_DIED(LocalPlayer, dmVarsPassed.iDied)
//				DONT_ZOOM_MINIMAP_WHEN_RUNNING_THIS_FRAME()
				TURN_SPINNER_OFF()	
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("TURN_SPINNER_OFF")
				#ENDIF
				#ENDIF
				
				DPAD_LEADERBOARD(GET_SUB_MODE(serverBDpassed), serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("DPAD_LEADERBOARD")
				#ENDIF
				#ENDIF
						
				IF KILL_PLAYER_WHEN_OUT_OF_VEHICLE()
					IF IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
					AND GET_NUMBER_OF_DEATHS_FOR_PLAYER(playerBDPassed, serverBDpassed, LocalPlayer) + 1 >= GET_MAX_NUMBER_OF_LIVES_FOR_PLAYER(playerBDPassed, serverBDpassed)
						SET_BIT(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_ELIMINATED_FROM_FALLING_OUT_OF_VEH)
						PRINTLN("KILL_PLAYER_WHEN_OUT_OF_VEHICLE - Setting CLIENT_BITSET_ELIMINATED_FROM_FALLING_OUT_OF_VEH")
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				#IF SCRIPT_PROFILER_ACTIVE 
					ADD_SCRIPT_PROFILE_MARKER("KILL_PLAYER_WHEN_OUT_OF_VEHICLE")
				#ENDIF
				#ENDIF
				
				// B*1466917.
				INT iPlayer
				IF NOT dmVarsPassed.bSetupVehDmHealthBars
					IF IS_THIS_VEHICLE_DEATHMATCH(serverBDPassed)
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							IF IS_MP_GAMER_TAG_ACTIVE(iPlayer)
								SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(iPlayer, TRUE)
								PRINTLN("[WJK] - OVH - called SET_MP_GAMER_TAGS_SHOULD_USE_VEHICLE_HEALTH(TRUE) on player: ", iPlayer)
							ENDIF
						ENDREPEAT
					ENDIF
					dmVarsPassed.bSetupVehDmHealthBars = TRUE
				ENDIF
				
			ELSE
				DISABLE_SELECTOR()
				REFRESH_DM_SCALEFORM_BUTTON_SLOTS(Placement)
				REINIT_NET_TIMER(dmVarsPassed.timeRespawning)
				SET_DEAD_IN_DM_GLOBAL(TRUE)
				FORCE_TIMER_ORDER_REFRESH()
				CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_TEXT)
				SET_APPLICABILITY_APPLY_CONDITION_RESPAWNING(dmVarsPassed.sPlayerModifierData)
//				RESET_DPAD_LBD(dmVarsPassed)
			
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_DEAD)
			ENDIF	
		BREAK
		
		CASE CLIENT_MISSION_STAGE_DEAD
			
			IF NOT g_bArenaBigScreenBinkPlaying
				IF NOT CONTENT_IS_USING_ARENA()
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
						IF CONTENT_IS_USING_ARENA()
							SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
							CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[TMS][ARENA][5] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
			ENDIF
			
			TURN_SPINNER_OFF()
			DRAW_DM_HUD(LocalPlayer, serverBDpassed, DRAW_AMMO(serverBDpassed), dmVarsPassed, serverBDpassedLDB, playerBDPassed)
			
			// Draw hud during Killstrip
			IF IS_SCREEN_FADED_IN()
//				PRINTLN("DPAD_LEADERBOARD, YES ")
				DPAD_LEADERBOARD(GET_SUB_MODE(serverBDpassed), serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
				DISPLAY_RADAR(TRUE)
			ENDIF
			
			PRINTLN("CLIENT_MISSION_STAGE_DEAD - serverBDPassed.iPlayerLives: ", serverBDPassed.iPlayerLives, " || iDeaths: ", playerBDPassed[iLocalPart].iDeaths)
			
			BOOL bForceSpectator
			IF GET_MAX_NUMBER_OF_LIVES_FOR_PLAYER(playerBDPassed, serverBDpassed) = 1
			AND NOT bLocalPlayerPedOK
				PRINTLN("CLIENT_MISSION_STAGE_DEAD - Forcing into Spectator. We had 1 life and we are dead.")
				bForceSpectator = TRUE
			ENDIF
			
			// [DM_LIVES] Send to Spectator if deaths > lives
			IF IS_LIVES_BASED_DEATHMATCH(serverBDpassed)
			OR IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_EXPLODED_FROM_BEING_TAGGED)
			AND NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
				IF HAVE_LIVES_RUN_OUT_FOR_PLAYER(playerBDPassed, serverBDpassed, bForceSpectator, LocalPlayer)
				OR IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_EXPLODED_FROM_BEING_TAGGED)
				OR IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_ELIMINATED_FROM_FALLING_OUT_OF_VEH)
					IF NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_SENT_TO_SPECTATOR)
						PRINTLN("[LM][DM_LIVES] - Setting CLIENT_BITSET_SENT_TO_SPECTATOR - iPlayerLives: ", serverBDPassed.iPlayerLives, " iDeaths: ", playerBDPassed[iLocalPart].iDeaths, " Game Time: ", GET_GAME_TIMER())
						SET_BIT(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_SENT_TO_SPECTATOR)
						SET_BIT(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
						g_bMissionHideRespawnBar = TRUE
						SET_EARLY_END_DEATCHMATCH_SPECTATE(ci_SpectatorState_WAIT, playerBDPassed)
						SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
						PRINTLN("[LM][FINISH_TIME] - (1) Assigning Finished at time: ", (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000)
						playerBDPassed[iLocalPart].iFinishedAtTime = (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitset, CLIENT_BITSET_SENT_TO_SPECTATOR)
			OR HAS_DEATHMATCH_FINISHED(serverBDpassed)
				
				IF playerBDPassed[iLocalPart].iFinishedAtTime = 0
				AND HAS_DEATHMATCH_FINISHED(serverBDpassed)
					PRINTLN("[LM][FINISH_TIME] - (2) Assigning Finished at time: ", (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000)
					playerBDPassed[iLocalPart].iFinishedAtTime = (ROUND((TO_FLOAT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBDpassed.timeServerForFinishedLast))/1000)))*1000
				ENDIF
				
				// Move on when player revived
				IF bLocalPlayerOK
				OR HAS_DEATHMATCH_FINISHED(serverBDpassed)
					ENABLE_SELECTOR()
					//SET_DEAD_IN_DM_GLOBAL(FALSE)
					FORCE_TIMER_ORDER_REFRESH()
					THEFEED_FLUSH_QUEUE() // 1421107 
					IF THEFEED_IS_PAUSED()
						PRINTLN("THEFEED_IS_PAUSED, THEFEED_RESUME")
						THEFEED_RESUME()
					ENDIF
					g_b_ReapplyStickySaveFailedFeed = FALSE
					IF IS_PLAYER_ON_DEATH_STREAK()
					AND NOT IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
						DO_DEATH_STREAK_BIG_MESSAGE()
					ENDIF
					
					PROCESS_PLAYER_SETUP_AFTER_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData)
					
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
					AND NOT IS_BIT_SET(dmVarsPassed.sPlayerModifierData.iPlayerModifierBS, ciPLAYERMODIFIER_INVENTORY_GIVEN)
						DEAL_WITH_FORCED_WEAPON_AND_AMMO(FALSE, dmVarsPassed.iDlcWeaponBitSet, dmVarsPassed.wtWeaponToRemove)
					ENDIF
					RESET_NET_TIMER(dmVarsPassed.timeRespawn)
					// 1682132 Clear Bullshark message
					CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_GENERIC_MIDSIZED)
					
					SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
					
					IF IS_KING_OF_THE_HILL()
						SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
						PRINTLN("[KOTH] CLIENT_MISSION_STAGE_DEAD - Turning PCF_BlockAutoSwapOnWeaponPickups on")
					ENDIF
					
					BOOL bWait
					IF NOT CONTENT_IS_USING_ARENA()
					OR IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
						
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed)
							VEHICLE_INDEX vehNew
							vehNew = GET_VEHICLE_PED_IS_IN(LocalPlayerPed)
							
							IF NOT IS_ENTITY_ALIVE(vehNew)
								PRINTLN("[ARENA] CLIENT_MISSION_STAGE_DEAD\IS_NET_PLAYER_OK - Vehicle is returning Null or Not Alive...")
								bWait = TRUE
							ELSE
								APPLY_ARENA_SETTINGS_TO_PLAYER_VEH(vehNew, IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_USING_PERSONAL_VEH))
									
								IF IS_ENTITY_ALIVE(vehNew)
									MODEL_NAMES mn
									mn = GET_ENTITY_MODEL(vehNew)
									IF GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(mn) != WEAPONTYPE_UNARMED
										SET_CURRENT_PED_VEHICLE_WEAPON(LocalPlayerPed, GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(mn))
										PRINTLN("[ARENA] SET_CURRENT_PED_VEHICLE_WEAPON - Setting weapon to ", ENUM_TO_INT(GET_ARENA_WARS_WEAPON_TYPE_FROM_VEHICLE(mn)))
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT bWait
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_FIGHTING)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_RESPAWN
			
			IF NOT g_bArenaBigScreenBinkPlaying
				IF NOT CONTENT_IS_USING_ARENA()
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
						IF CONTENT_IS_USING_ARENA()
							SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
							CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[TMS][ARENA][6] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
			ENDIF
			
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			IF PROCESS_PLAYER_RESPAWN(dmVarsPassed, playerBDPassed)
				CLEAR_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_TRIANGLE_WARP)
				RESET_NET_TIMER(dmVarsPassed.timeForceWarp)
				RESET_NET_TIMER(dmVarsPassed.timeRespawn)
				RESET_DM_RESPAWN_STATE(dmVarsPassed)
				SET_VEH_NEEDS_LOCKED(dmVarsPassed)
				SET_UP_LOCAL_PLAYER_PROOF_SETTINGS()
				PROCESS_PLAYER_SETUP_AFTER_SPAWN(playerBDPassed, dmVarsPassed.sPlayerModifierData)
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Local_Player_Arrow_Team_Colour)
					SET_LOCAL_PLAYER_ARROW_TO_CURRENT_HUD_COLOUR(TRUE)
				ENDIF
				
				IF CONTENT_IS_USING_ARENA()
					CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
				ENDIF
				IF IS_SPECIAL_CONTROLLER_RESPAWN_REQUESTED()
					CPRINTLN( DEBUG_CONTROLLER, "[spawning][RSPWN][SPEC_SPEC] Clearing Spectator bits from Roaming Spectator Respawn.")
					SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
					NET_SET_PLAYER_CONTROL(LocalPlayer, TRUE)
					REAPPLY_SETTINGS_FROM_SPECTATOR()	
				ENDIF
				
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_FIGHTING)
			ENDIF
		BREAK
	
		CASE CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE
			BOOL bContinue
			BOOL bPlacedEndCameraSuccessfully
			SET_DAILY_OBJECTIVE_TICKERS_ENABLED_ON_MISSION(FALSE)
					
			IF IS_PLAYER_ON_IMPROMPTU_DM()
			OR DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)			
			OR ( IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed) AND NOT bIsSCTV) // So we can see the winner scene on the live feed.
			OR DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed) // 907242
			OR IS_PLAYER_ON_BOSSVBOSS_DM()
				IF HAS_SERVER_STORED_FINISH_POSITIONS(serverBDpassed)
				OR NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
				OR DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					bContinue = TRUE
				ENDIF
			ELSE
//				SET_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_OUTRO_OK_TO_DO)

				IF CONTENT_IS_USING_ARENA()
				AND NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_HAS_AIRHORN_PLAYED)
					IF serverBDpassed.playerMatchWinner != INVALID_PLAYER_INDEX()
						IF serverBDpassed.playerMatchWinner = GET_PLAYER_INDEX()
							PRINTLN("[MJL] This player is the winner with PlayerIndex : ", NATIVE_TO_INT(GET_PLAYER_INDEX()))
							PLAY_SOUND_FRONTEND(-1, "Finish_Win", "DLC_AW_Frontend_Sounds")
							SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_HAS_AIRHORN_PLAYED)
						ELSE 
							PRINTLN("[MJL] This player is NOT the winner with PlayerIndex : ", NATIVE_TO_INT(GET_PLAYER_INDEX()))
							SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_HAS_AIRHORN_PLAYED)
							PLAY_SOUND_FRONTEND(-1, "Finish_Default", "DLC_AW_Frontend_Sounds")
						ENDIF
					ELSE	
						PRINTLN("[MJL] Invalid player index for serverBDpassed.playerMatchWinner")
					ENDIF
				ENDIF
				
				IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.stCelebrationDelay)
					START_NET_TIMER(dmVarsPassed.stCelebrationDelay)
				ENDIF
				
				IF IS_BIT_SET(playerBDPassed[iLocalPart].iClientBitSet, CLIENT_BITSET_RAN_OUT_OF_LIVES)
				AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS, FALSE, FALSE, FALSE, TRUE, TRUE, TRUE)
							bContinue = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF PLACE_CAMERA_FOR_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData, dmVarsPassed.camEndScreen, bPlacedEndCameraSuccessfully)
					IF HAS_SERVER_STORED_FINISH_POSITIONS(serverBDpassed)
						IF IS_DM_READY_TO_SHOW_CELEBRATION(dmVarsPassed)
							bContinue = TRUE		
						ENDIF
					ELSE
						PRINTLN("[CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE], HAS_SERVER_STORED_FINISH_POSITIONS(serverBDpassed), FALSE")	
					ENDIF
				ELSE
					PRINTLN("[CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE], PLACE_CAMERA_FOR_CELEBRATION_SCREEN ")
				ENDIF
			ENDIF
			
			PRINTLN("[CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE] - HAS_SERVER_STORED_FINISH_POSITIONS(serverBDpassed): ", HAS_SERVER_STORED_FINISH_POSITIONS(serverBDpassed))
			
			IF IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType)
			OR NOT IS_THIS_LEGACY_DM_CONTENT()
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyOne, ciOptionsBS21_Last_Man_Standing_Style_Leaderboard_Order)
				AND NOT HAS_SERVER_STORED_FINISH_POSITIONS(serverBDpassed)
				AND NOT DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					bContinue = FALSE
				ENDIF
			ENDIF
			
			IF bContinue 
				
				PRINTLN("[CLIENT_MISSION_STAGE_FIX_DEAD_PEOPLE] - bContinue")
				SEND_EVENT_IF_PLAYER_COMPLETES_JOB_IN_UNDER_TEN_SECONDS(dmVarsPassed.stRewardPercentageTimer, IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed), DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed))
				IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
				AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
					g_bMissionEnding = TRUE
					PRINTLN("g_bMissionEnding = TRUE Deathmatch")
				ENDIF
				IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					PRINTLN("[LM][DM][SPEC_SPEC] - Setting g_ciSpecial_Spectator_BS_Request_Spectator_Wait_For_Cleanup")
					SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Spectator_Wait_For_Cleanup)
				ENDIF
				STOP_DM_ARENA_SCORE(dmVarsPassed)
				STOP_DM_SCORE(dmVarsPassed)
				STOP_AUDIO_SCENE("MP_DM_GENERAL_SCENE")
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(LocalPlayerPed, 0.0)
				CHANGE_MY_RESPAWN_STATE()
//				STORE_CLIENT_FINISHING_POS(serverBDPassed, playerBDPassed)
				SET_DM_FINISHED_BOOL(playerBDPassed)
//				CLEANUP_ALL_FX()
				IF NOT IS_PLAYER_ON_IMPROMPTU_DM() 
				AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				ENDIF
				THEFEED_FLUSH_QUEUE()
				SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_MOVED_TO_FIX_DEAD_PEOPLE)
				HIDE_HUD_FOR_LBD()
				ACTIVATE_KILL_TICKERS(FALSE) // Stop kill tickers
				CLEANUP_DPAD_LBD(dmVarsPassed.scaleformDpadMovie)
				//STOP_DM_SCORE(dmVarsPassed)
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
					PRINTLN("HAS_SPECTATOR_CAM_BEEN_RESET_AND_KILLED, RETURN TRUE, RESET_CAM_FINISH")		
					//DEACTIVATE_SPECTATOR_CAM(g_BossSpecData)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
				ENDIF
				DO_LBD_CAM(dmVarsPassed)
				CLIENT_SET_UP_EVERYONE_CHAT() // Allow chat on scoreboard
				START_AUDIO_FADE(dmVarsPassed.timeAudioFade)
				STOP_STREAM() // 983980
				SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_FINISHING_UP)
				SET_BIT(dmVarsPassed.sSaveOutVars.iBitSet, ciRATINGS_PLAYED)
				SET_DM_FINISHED_GLOBAL(TRUE) // Used for Neil to block player control
				
				IF NOT IS_THIS_A_ROUNDS_MISSION()
					APPLY_DM_END_STATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)				
					DO_PLAYER_FINISHING_TOTALS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
				ENDIF
				
				IF NOT DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					DO_CREW_CUT_ACHIEVEMENT()
				ENDIF
				IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
				AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
					PREPARE_PLAYER_FOR_LEADERBOARD()
				ENDIF	
				CLEAR_ANY_DM_BIG_MESSAGES_FOR_LBD(playerBDPassed)	
				
				IF NOT IS_KING_OF_THE_HILL()
					PRINT_WINNING_KILL_TICKER(serverBDpassed, dmVarsPassed)
				ENDIF
				
				IF NOT CONTENT_IS_USING_ARENA() // DM logic unrelated to Arena
					PRINT_MVP_TICKER(serverBDpassed, playerBDPassed, dmVarsPassed)
				ENDIF
				DO_END_IMPROMPTU_SPLASH(serverBDpassed, playerBDPassed, dmVarsPassed)
				
				DO_END_BOSSVBOSS_SPLASH(serverBDpassed, dmVarsPassed)
				
				IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
				AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
					INITIALISE_CELEBRATION_SCREEN_DATA(dmVarsPassed.sCelebrationData)
					REQUEST_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData)
					SET_CELEBRATION_SCREEN_AS_ACTIVE(TRUE)
					KILL_UI_FOR_CELEBRATION_SCREEN()
					HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
					IF DOES_ENTITY_EXIST(LocalPlayerPed)
						IF NOT IS_ENTITY_DEAD(LocalPlayerPed)
							g_bPlayEndOfJobRadioFrontEnd = TRUE
							PRINTLN("Player in a vehicle - called SET_AUDIO_FLAG(TRUE) and SET_MOBILE_PHONE_RADIO_STATE(TRUE).")
						ENDIF
					ENDIF
					IF IS_NET_PLAYER_OK(LocalPlayer, FALSE)
					AND IS_SKYSWOOP_AT_GROUND()
						IF IS_PED_IN_ANY_VEHICLE(LocalPlayerPed, TRUE)
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE, NSPC_FREEZE_POSITION | NSPC_NO_COLLISION)
						ELSE
							NET_SET_PLAYER_CONTROL(LocalPlayer, FALSE)
						ENDIF
					ENDIF
					IF bPlacedEndCameraSuccessfully
						IF bLocalPlayerOK
							ANIMPOSTFX_STOP_ALL()
							
//								//Reset the action mode to the default one.
//								SET_PED_USING_ACTION_MODE(LocalPlayerPed, FALSE)
//								SET_PED_USING_ACTION_MODE(LocalPlayerPed, TRUE, -1, "DEFAULT_ACTION")
						ENDIF
						
						IF SHOULD_POSTFX_BE_WINNER_VERSION(playerBDPassed, serverBDpassed)
							PLAY_CELEB_WIN_POST_FX()
						ELSE
							PLAY_CELEB_LOSE_POST_FX()
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					ENDIF
				ELSE
					ANIMPOSTFX_PLAY("MinigameEndNeutral", 0, FALSE)
				ENDIF
				FINISH_JOB_ELO(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)

				SAVE_PLAYER_END_JOB_POSITION_FOR_POST_MISSION_CLEANUP()
				IF DOES_ENTITY_EXIST(LocalPlayerPed)
					SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(GET_ENTITY_COORDS(LocalPlayerPed, FALSE))
				ENDIF
				
				// PHONE QUITTERS BAILING HERE >>>>>>>>>>>>>>>>>>>>>
				IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						SET_SKYFREEZE_FROZEN() //*1618098
						ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE) 
					ENDIF
				
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)					
				ELSE
					IF bPlacedEndCameraSuccessfully
						SET_BIT(dmVarsPassed.iDmBools2, DM_BOOLS2_OUTRO_OK_TO_DO)
					ENDIF
					IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
					AND NOT IS_PLAYER_ON_BOSSVBOSS_DM()
						IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
							DO_RENDER_PHASE_PAUSE_FOR_CELEBRATION_SCREEN()
						ENDIF
					ENDIF
					
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_CONOR_LBD)	
				ENDIF
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_CONOR_LBD
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
		
			IF IS_PLAYER_ON_IMPROMPTU_DM()
			OR DO_RANK_PREDICTION_FOR_DM(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
			OR IS_PLAYER_ON_BOSSVBOSS_DM()
//				IF IS_PLAYER_ON_IMPROMPTU_DM()
//				OR HAS_STARTED_NEW_OUTRO(dmVarsPassed.DMIntroData)
					
					IF NOT IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)
						MVP_BONUS_JOB_POINT(serverBDpassed, playerBDPassed, serverBDpassedLDB)
						ADD_TO_JOB_POINTS_TOTAL(GET_POINTS_FOR_POSITION(GET_PLAYER_FINISH_POS(serverBDpassed, NETWORK_PLAYER_ID_TO_INT()), NETWORK_PLAYER_ID_TO_INT()))
					ENDIF
					
					IF IS_PAUSE_MENU_ACTIVE()
						SET_FRONTEND_ACTIVE(FALSE)
					ENDIF
					
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_CELEBRATION)	
//				ELSE
//					PRINTLN("CLIENT_MISSION_STAGE_CONOR_LBD, HAS_STARTED_NEW_OUTRO")
//				ENDIF
			ELSE
				PRINTLN("CLIENT_MISSION_STAGE_CONOR_LBD, DO_RANK_PREDICTION_FOR_DM")
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_CELEBRATION
			DISABLE_DPADDOWN_THIS_FRAME()
			HIDE_PROBLEM_MODELS_FOR_CELEBRATION_SCREEN()
			
			#IF IS_DEBUG_BUILD
				IF g_SkipCelebAndLbd				
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_LBD_CAM)	
				ELSE
			#ENDIF
					
			IF IS_THIS_A_ROUNDS_MISSION()
				// need to call before values are modified.
				IF IS_THIS_A_RSTAR_ACTIVITY()
					WRITE_LEADERBOARD_DATA(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
				ENDIF
				
				SERVER_SET_WINING_TEAM_BROAD_CAST_DATA()
				
				INT iFFAWinnerPlayer
				IF NOT IS_THIS_TEAM_DEATHMATCH(serverBDpassed)
				OR (IS_THIS_TEAM_DEATHMATCH(serverBDpassed) AND IS_TDM_USING_FFA_SCORING())
					iFFAWinnerPlayer = NATIVE_TO_INT(GET_PLAYER_IN_LEADERBOARD_POS(serverBDpassedLDB, 0))
				ELSE
					iFFAWinnerPlayer = -1
				ENDIF
				
				SET_UP_ROUNDS_DETAILS_AT_END_OF_MISSION(serverBDpassed.iWinningTeam, playerBDPassed[iLocalPart].iTeam, iFFAWinnerPlayer)
				
				IF NOT AM_I_AT_END_OF_ROUNDS_MISSION() 
				AND NOT SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					EXIT
				ENDIF
									
				IF IS_ROUNDS_MISSION_OVER_FOR_JIP_PLAYER(LocalPlayer)
					g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl = GET_FM_RANK_FROM_XP_VALUE(GET_PLAYER_XP(NETWORK_GET_PLAYER_INDEX(PARTICIPANT_ID())))
					g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachStartLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel)
					g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRPToReachEndLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iEndLvl)
					
					g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl = (g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iStartLevel + 1)
					g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iRptoReachNextLvl = GET_XP_NEEDED_FOR_FM_RANK(g_TransitionSessionNonResetVars.sGlobalCelebrationRoundsData.iNextLvl)										
				ENDIF
				
				IF NOT IS_BIT_SET(dmVarsPassed.iDmBools3, DM_BOOL3_DO_PLAYER_TOTALS_FOR_ROUNDS)
					APPLY_DM_END_STATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)					
					DO_PLAYER_FINISHING_TOTALS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
					
					IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_SAVED_ROUNDS_LBD_DATA)
						PRINTLN("[DM-Rounds] - [TS] [MSROUND] - WRITE_TO_ROUNDS_MISSION_END_LB_DATA")
						INT iScore, iKills, iRoundDeaths, iTime, iCash, iRP
						iKills = playerBDPassed[iLocalPart].iKills
						iRoundDeaths = playerBDPassed[iLocalPart].iDeaths
						iScore = playerBDPassed[iLocalPart].iScore
						iCash = playerBDPassed[iLocalPart].iJobCash
						iRP = playerBDPassed[iLocalPart].iJobXP
						
						WRITE_TO_ROUNDS_MISSION_END_LB_DATA(iScore,
												iKills,
												iRoundDeaths,	
												iRP,
												iCash,
												0, // assists don't exist
												iTime,
												0)
												
						SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_SAVED_ROUNDS_LBD_DATA)										
					ENDIF
					
					SET_BIT(dmVarsPassed.iDmBools3, DM_BOOL3_DO_PLAYER_TOTALS_FOR_ROUNDS)
				ENDIF
				
				SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_ROUNDS_DM_CALL_SWAP_TEAM_LOGIC)
				
			ENDIF
					
			IF IS_PLAYER_ON_IMPROMPTU_DM()
			OR IS_PLAYER_ON_BOSSVBOSS_DM()
			OR IS_PLAYER_ON_BIKER_DM()
				CLEANUP_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData)
				SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
				END_PLAYSTATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
			
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)	
			ELSE
				IF NOT dmVarsPassed.bCelebrationSummaryComplete
					IF HAS_CELEBRATION_SUMMARY_FINISHED(serverBDpassed, dmVarsPassed, playerBDPassed, serverBDpassedLDB)
						RESET_CELEBRATION_SCREEN_STATE(dmVarsPassed.sCelebrationData)
						
						RESET_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTransitionTimer)
						START_NET_TIMER(dmVarsPassed.sCelebrationData.sCelebrationTransitionTimer)
						
						dmVarsPassed.bCelebrationSummaryComplete = TRUE
						
						PRINTLN("CLIENT_MISSION_STAGE_CELEBRATION, HAS_CELEBRATION_SUMMARY_FINISHED YES ")
					ENDIF
				ELSE
					BOOL bSafeToContinue
					
					IF NOT IS_THIS_A_ROUNDS_MISSION()
					OR SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
						IF HAS_CELEBRATION_WINNER_FINISHED(serverBDpassed, dmVarsPassed, playerBDPassed, serverBDpassedLDB, TRUE)
							bSafeToContinue = TRUE
							PRINTLN("CLIENT_MISSION_STAGE_CELEBRATION, HAS_CELEBRATION_WINNER_FINISHED YES ")
						ENDIF
					ELSE
						PRINTLN("CLIENT_MISSION_STAGE_CELEBRATION, SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD true so bSafeToContinue = TRUE")
						bSafeToContinue = TRUE
					ENDIF
					
					IF bSafeToContinue
						IF CONTENT_IS_USING_ARENA()
							SET_ARENA_LOBBY_BACKGROUND_TO_DRAW_AFTER_MISSION()
						ENDIF
						
						//Cleanup audio scenes: see B*1642903 for implementation notes
						IF CONTENT_IS_USING_ARENA()
							IF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_ARENA_SCENE")
								STOP_AUDIO_SCENE("MP_CELEB_SCREEN_ARENA_SCENE")
							ENDIF
						ELIF IS_AUDIO_SCENE_ACTIVE("MP_CELEB_SCREEN_SCENE")
							STOP_AUDIO_SCENE("MP_CELEB_SCREEN_SCENE")
						ENDIF
						
						IF SET_SKYSWOOP_UP(FALSE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, IS_THIS_A_ROUNDS_MISSION(), FALSE, FALSE, IS_THIS_A_ROUNDS_MISSION())
							IF NOT IS_AUDIO_SCENE_ACTIVE("MP_LEADERBOARD_SCENE")
								START_AUDIO_SCENE("MP_LEADERBOARD_SCENE")
							ENDIF
		
							CLEAR_ALL_BIG_MESSAGES()
							STOP_CELEBRATION_SCREEN_ANIM(dmVarsPassed.sCelebrationData, "WINNER")
							STOP_CELEBRATION_SCREEN_ANIM(dmVarsPassed.sCelebrationData, "SUMMARY")
							CLEANUP_CELEBRATION_SCREEN(dmVarsPassed.sCelebrationData)
							SET_CELEBRATION_SCREEN_AS_ACTIVE(FALSE)
							SET_BIT(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMBS_DONE)
							
							IF SHOULD_DO_TRANSITION_MENU_AUDIO()
								//1665147 - Have the pause menu music running over the leaderboard.
								RUN_TRANSITION_MENU_AUDIO(TRUE)
							ENDIF
							
							//1632888 - Keep the screen blurry and frozen at the top of the skyswoop (this should be cleaned up when the skyswoop starts descending).
							//Currently commented out as part of 1638857.
							//SET_SKYBLUR_BLURRY() 
							//SET_SKYFREEZE_FROZEN()
			
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_LBD_CAM)	
						ENDIF
					ENDIF
					
					// Stop post fx on first switch cut.
					IF NOT IS_BIT_SET(dmVarsPassed.iDmBools3, StoppedAllPostFxForSwitch)
						IF IS_PLAYER_SWITCH_IN_PROGRESS()
							IF (GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT)
								CLEAR_ALL_CELEBRATION_POST_FX()
								PRINTLN("CLIENT_MISSION_STAGE_CELEBRATION - called ANIMPOSTFX_STOP_ALL, switch state is in SWITCH_STATE_JUMPCUT_ASCENT.")
								SET_BIT(dmVarsPassed.iDmBools3, StoppedAllPostFxForSwitch)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			
				ENDIF
			#ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_LBD_CAM
			DISABLE_SELECTOR_THIS_FRAME()
			START_LBD_AUDIO_SCENE()
			HIDE_HUD_FOR_LBD()
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			DISABLE_DPADDOWN_THIS_FRAME()
			
			IF IS_LEADERBOARD_CAM_READY(TRUE)
				IF HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeAudioFade, RADIO_FADE_TIME)

					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_SOCIAL_LBD)	
				ELSE
					PRINTLN("CLIENT_MISSION_STAGE_LBD_CAM, RADIO_FADE_TIME")
				ENDIF
			ELSE
				PRINTLN("CLIENT_MISSION_STAGE_LBD_CAM, IS_LEADERBOARD_CAM_READY")
			ENDIF	
		BREAK
		
		CASE CLIENT_MISSION_STAGE_SOCIAL_LBD
			HIDE_HUD_FOR_LBD()
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			IF IS_THIS_A_RSTAR_ACTIVITY()
				IF NOT IS_THIS_A_ROUNDS_MISSION()
					WRITE_LEADERBOARD_DATA(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
				ENDIF
			ENDIF

			GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_RESULTS)	
		BREAK
		
		CASE CLIENT_MISSION_STAGE_RESULTS
			DISABLE_FRONTEND_THIS_FRAME()
			HIDE_HUD_FOR_LBD()
			HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
			g_b_OnResults = TRUE
			IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
			OR DOES_PLAYER_HAVE_ACTIVE_SPAWN_VEHICLE_MODIFIER(playerBDPassed[iLocalPart].iCurrentModSet, dmVarsPassed.sPlayerModifierData)
				TIDY_PLAYER_AND_VEHICLE(dmVarsPassed.dmVehIndex)
			ENDIF
			SWITCH GET_LEADERBOARD_STAGE(playerBDPassed)				
				CASE LEADERBOARD_STAGE_SCALEFORM
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					SET_FM_MISSION_AS_NOT_JOINABLE()
					//SET_CLIENT_FINISHED_BIT(playerBDPassed)
					IF bLocalPlayerOK
						CLEAR_PED_TASKS_IMMEDIATELY(LocalPlayerPed) // 1392333
					ENDIF
					IF HAS_SERVER_COUNTED_FINISHING_TEAMS(serverBDpassed)	
					OR DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
						REFRESH_DM_SCALEFORM_BUTTON_SLOTS(Placement)
						g_b_RestartButtons = FALSE
						g_b_PlayAgain = FALSE
					
						GOTO_LEADERBOARD_STAGE(playerBDPassed, LEADERBOARD_STAGE_MAKE_SAFE)
					ELSE
						PRINTLN("GET_LEADERBOARD_STAGE, HAS_SERVER_COUNTED_FINISHING_TEAMS = FALSE")
					ENDIF
				BREAK
				CASE LEADERBOARD_STAGE_MAKE_SAFE
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					IF NOT HAS_NET_TIMER_STARTED(dmVarsPassed.timeBigMessageFailSafe)
						START_NET_TIMER(dmVarsPassed.timeBigMessageFailSafe)
					ELSE
						IF NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
						OR HAS_NET_TIMER_EXPIRED(dmVarsPassed.timeBigMessageFailSafe, BIG_MESSAGE_TIMEOUT)
							CLEAR_MISSION_HELP()
							CLEAR_HELP(TRUE)
							#IF IS_DEBUG_BUILD
							PRINT_DEATHMATCH_DETAILS_BEFORE_LEADERBOARD_STAGE(serverBDpassed, serverBDpassedLDB)
							#ENDIF
							REFRESH_DM_SCALEFORM_BUTTON_SLOTS(Placement)
								
							GOTO_LEADERBOARD_STAGE(playerBDPassed, LEADERBOARD_STAGE_CHALLENGES)
						ELSE
							PRINTLN("GET_LEADERBOARD_STAGE, BIG_MESSAGE_TIMEOUT ")
						ENDIF
					ENDIF
				BREAK
				
				CASE LEADERBOARD_STAGE_CHALLENGES
				
					IF IS_THIS_A_ROUNDS_MISSION()
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_LEADERBOARD)
						
					ELIF IS_PLAYER_ON_IMPROMPTU_DM()
					OR IS_PLAYER_ON_BOSSVBOSS_DM()
					
						GOTO_LEADERBOARD_STAGE(playerBDPassed, LEADERBOARD_STAGE_AWARD)						
					ELIF CONTROL_UPDATING_CHALLENGES()
					
						GOTO_LEADERBOARD_STAGE(playerBDPassed, LEADERBOARD_STAGE_AWARD)
												
					#IF IS_DEBUG_BUILD	
					ELSE
						PRINTLN("GET_LEADERBOARD_STAGE, CONTROL_UPDATING_CHALLENGES ")
					#ENDIF
					ENDIF
				BREAK
				
				CASE LEADERBOARD_STAGE_AWARD 
					DISABLE_SELECTOR_THIS_FRAME()
					DISABLE_DPADDOWN_THIS_FRAME()
					IF NOT DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed)
						CLEANUP_THUMBS()
						DISABLE_ALL_MP_HUD()
						
						THEFEED_FLUSH_QUEUE()
						 g_b_ReapplyStickySaveFailedFeed = FALSE
					ENDIF
					
					DEAL_WITH_PLAYLIST_LEADERBOARD(serverBDpassed, playerBDPassed, serverBDpassedLDB)
					END_PLAYSTATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
					INVISIBLE_FOR_LBD()
					
					PRINTLN("[LM][SPEC_SPEC][JIP] - BoxCheck: ", SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS(), " ArenaCheck: ", CONTENT_IS_USING_ARENA(),
									" SpecCheck: ", DID_I_JOIN_MISSION_AS_SPECTATOR())				
					IF (SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS())
					AND CONTENT_IS_USING_ARENA()
					AND DID_I_JOIN_MISSION_AS_SPECTATOR()
						PRINTLN("[LM][SPEC_SPEC][JIP] - Setting that we want to stay as a special spectator so we can double check.")
						SET_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS(TRUE)
					ENDIF
					
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_LEADERBOARD)
				BREAK
			
				DEFAULT 
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("DM: Problem in dmVarsPassed.iLeaderboardProgress") 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE CLIENT_MISSION_STAGE_LEADERBOARD
			
			IF IS_THIS_A_ROUNDS_MISSION()
				
				// EarlyEnd, might not be necessary.
				IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_ROUNDS_DM_END_SET_UP)
					
					IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					OR IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
					
					#IF IS_DEBUG_BUILD
					OR IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_F_FAIL)
					OR IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_S_PASS)
					#ENDIF	
								
						PRINTLN("[DM-Rounds] - Clearing rounds data - ")
						
						#IF IS_DEBUG_BUILD
						IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)							
							PRINTLN("[DM-Rounds] - Clearing rounds data - DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE")
						ENDIF
						
						IF IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND)
							PRINTLN("[DM-Rounds] - Clearing rounds data - SERV_BITSET2_TOO_FEW_PLAYERS_FOR_NEXT_ROUND")
						ENDIF
						
						IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_F_FAIL)
							PRINTLN("[DM-Rounds] - Clearing rounds data - DEBUG_BITSET_F_FAIL")
						ENDIF 
						
						IF IS_BIT_SET(serverBDpassed.iServerDebugBitSet, DEBUG_BITSET_S_PASS)
							PRINTLN("[DM-Rounds] - Clearing rounds data - DEBUG_BITSET_S_PASS")
						ENDIF
						#ENDIF	
						
						IF NOT IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
							SET_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
							IF IS_PLAYER_ON_A_PLAYLIST(LocalPlayer)								
								DEAL_WITH_PLAYLIST_LEADERBOARD(serverBDpassed, playerBDPassed, serverBDpassedLDB)
							ENDIF
							SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_HANDLE_PLAYLIST_POSITION_DATA_WRITE_CALLED)
						ENDIF
						
						RESET_ROUND_MISSION_DATA(DEFAULT, DEFAULT, FALSE)
						
						CLEAR_USE_BACKED_UP_MENU_SELECTION_ON_RESTART()
						
						PRINTLN("[DM-Rounds] - SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_ROUNDS_DM_END_SET_UP)")
						
						SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_ROUNDS_DM_END_SET_UP)
						
					ENDIF
					
				ENDIF

				IF IS_ROUNDS_MISSION_OVER_FOR_JIP_PLAYER(LocalPlayer)
					DEAL_WITH_PLAYLIST_LEADERBOARD(serverBDpassed, playerBDPassed, serverBDpassedLDB)
					END_PLAYSTATS(serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
					INVISIBLE_FOR_LBD()
				ELSE					
					IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_ROUNDS_DM_CALL_SWAP_TEAM_LOGIC)
						CLEAR_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_ROUNDS_DM_CALL_SWAP_TEAM_LOGIC)
						
						PRINTLN("[DM-Rounds] - CLEAR_BIT(iLocalBoolCheck15, DM_BOOL2_ROUNDS_DM_CALL_SWAP_TEAM_LOGIC)")
						
						IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)
							PRINTLN("[DM-Rounds] - [SST] * - IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciALWAYS_SWAP_SMALL_TEAM)")
							IF SHOULD_I_JOIN_SMALLEST_TEAM(serverBDPassed, GET_LOCAL_PLAYER_LEADERBOARD_POSITION(serverBDpassedLDB), TRUE)
								PRINTLN("[DM-Rounds] - [SST] * - SHOULD_I_JOIN_SMALLEST_TEAM")
								SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(TRUE)
							ELSE
								SET_VARS_AND_END_OF_ALWAYS_SWAP_SMALLEST_TEAM_MISSION(FALSE)
							ENDIF
						ELSE
							IF SHOULD_I_JOIN_SMALLEST_TEAM(serverBDPassed, GET_LOCAL_PLAYER_LEADERBOARD_POSITION(serverBDpassedLDB))
								PRINTLN("[DM-Rounds] - [SST] * - SHOULD_I_JOIN_SMALLEST_TEAM")
								SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(TRUE)
							ELSE
								SET_VARS_AND_END_OF_SWAP_TO_SMALLEST_TEAM_MISSION(FALSE)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
				
				IF HAS_ROUNDS_LEADERBOARD_FINISHED(serverFMMCPassed, Placement, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)				
												
					SET_ON_LBD_GLOBAL(FALSE)				
					
					START_NET_TIMER(dmVarsPassed.tdresultstimer)
				
					#IF IS_DEBUG_BUILD
					IF g_SkipCelebAndLbd
						SET_MY_EMO_VOTE_STATUS(ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART) 
						FM_PLAY_LIST_SET_ME_AS_VOTED()
					ENDIF
					#ENDIF
					
					// Show a final accumulative Rounds leaderboard (see DISPLAY_ROUNDS_LBD)
					IF (SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD()
					OR ROUNDS_MISSION_OVER_FOR_JIP_LOCAL_PLAYER(LocalPlayer))
					AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS, GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
					
					#IF IS_DEBUG_BUILD
					AND NOT g_SkipCelebAndLbd
					#ENDIF
							
						PRINTLN("[DM-Rounds] - SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD / ROUNDS_MISSION_OVER_FOR_JIP_LOCAL_PLAYER")	
					
						IF SHOULD_RUN_NEW_VOTING_SYSTEM(dmVarsPassed.sEndOfMission.iQuitProgress, serverBDpassed.sServerFMMC_EOM.iVoteStatus)
							PRINTLN("[DM-Rounds] - Setting CLIENT_MISSION_STAGE_PICK")
							SET_READY_FOR_NEXT_JOBS(dmVarsPassed.nextJobStruct)
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_PICK)
						ELSE
							PRINTLN("[DM-Rounds] - Setting CLIENT_MISSION_STAGE_END")
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)
						ENDIF
						
						IF bIsLocalPlayerHost
							IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_ROUNDS_LBD_DO)
								SET_BIT(serverBDpassed.iServerBitSet2, SERV_BITSET2_ROUNDS_LBD_DO)
								PRINTLN("[DM-Rounds] - [CS_RNDS] [HOST] SHOULD_DISPLAY_ROUNDS_MISSION_LEADER_BOARD, LBOOL6_SAVED_ROUNDS_DO_ROUNDS_LBD ")
							ENDIF
						ENDIF
					ELSE
						IF SHOULD_RUN_NEW_VOTING_SYSTEM(dmVarsPassed.sEndOfMission.iQuitProgress, serverBDpassed.sServerFMMC_EOM.iVoteStatus)
						AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)//GlobalplayerBD_FM[NATIVE_TO_INT(LocalPlayer)].bQuitJob
							SET_READY_FOR_NEXT_JOBS(dmVarsPassed.nextJobStruct)
							PRINTLN("[DM-Rounds] - Setting CLIENT_MISSION_STAGE_PICK")
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_PICK)
						ELSE
							PRINTLN("[DM-Rounds] - Setting CLIENT_MISSION_STAGE_END")
							GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)
						ENDIF
					ENDIF			
				ELSE
					// Block pause menu
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)					
				ENDIF
			ELSE
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				HIDE_HUD_FOR_LBD()
				HIDE_UI_FOR_CELEBRATION_SCREEN_THIS_FRAME()
				DISABLE_DPADDOWN_THIS_FRAME()
				DISABLE_SELECTOR_THIS_FRAME()
				DO_LBD_CAM(dmVarsPassed)
				
				IF HAS_LEADERBOARD_FINISHED(serverFMMCPassed, Placement, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
					SET_ON_LBD_GLOBAL(FALSE)
					IF SHOULD_RUN_NEW_VOTING_SYSTEM(dmVarsPassed.sEndOfMission.iQuitProgress, serverBDpassed.sServerFMMC_EOM.iVoteStatus)
					AND NOT DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
						SET_READY_FOR_NEXT_JOBS(dmVarsPassed.nextJobStruct)
				
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_PICK)
					ELSE
					
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)
					ENDIF
				ELSE
					// Block pause menu
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_PICK
			IF DO_END_JOB_VOTING_SCREEN(g_sMC_serverBDEndJob, dmVarsPassed.nextJobStruct, g_sMC_serverBDEndJob.timerVotingEnds.Timer)
			
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_SCTV
			IF NOT g_bArenaBigScreenBinkPlaying
				IF NOT CONTENT_IS_USING_ARENA()
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
						IF CONTENT_IS_USING_ARENA()
							SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
							CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
						ENDIF
					ENDIF					
				ENDIF
			ELSE
				PRINTLN("[TMS][ARENA][7] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
			OR IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
				g_bMission321Done = TRUE
			ENDIF
			
			IF HAS_DEATHMATCH_FINISHED(serverBDpassed)
					
				IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				AND g_eSpecialSpectatorState != SSS_CLEANUP
					IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
						PRINTLN("[LM][SPEC_SPEC][CLIENT_MISSION_STAGE_CELEBRATION] - We are in Roaming Spectator Mode. and HAS_DEATHMATCH_FINISHED = TRUE Requesting Cleanup!")
						SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Cleanup)
						SET_BIT(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Dont_Cleanup_Arena_Activities)
					ENDIF
				ENDIF
				
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
					PRINTLN("CLIENT_MISSION_STAGE_SCTV - DEACTIVATE_SPECTATOR_CAM ")	
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
				ENDIF
				SET_SCTV_TICKER_SCORE_OFF()
				
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_CELEBRATION)
			ELSE
				IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					DPAD_LEADERBOARD(GET_SUB_MODE(serverBDpassed), serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
				ENDIF
				
				IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
				AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					HAS_LEADERBOARD_FINISHED(serverFMMCPassed, Placement, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
					//HAS_LEADERBOARD_TIMEOUT_EXPIRED(serverBDpassed, dmVarsPassed)
				ELSE
					SET_ON_LBD_GLOBAL(FALSE)
					IF IS_A_SPECTATOR_CAM_RUNNING()
					OR IS_ROCKSTAR_DEV()
						DRAW_SPECTATOR_HUD(serverBDpassed, dmVarsPassed, serverBDpassedLDB, playerBDPassed)
					ENDIF
				ENDIF
				IF (IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_QUIT_REQUESTED) AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE())
				OR DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					SET_SKYFREEZE_FROZEN() //*1618098
					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE) 			
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)	
				ENDIF
				UPDATE_SCTV_TICKER_SCORE_BASED_ON_WHO_IM_SPECTATING_DM_LEADERBOARD_STRUCT(serverBDpassedLDB.leaderBoard, serverBDpassed.tl63_ParticipantNames)
			ENDIF
		BREAK
		
		//This MAINTAIN_SPECTATOR is just for player specating, not SCTV
		CASE CLIENT_MISSION_STAGE_SPECTATOR
		
			IF NOT g_bArenaBigScreenBinkPlaying
				IF NOT CONTENT_IS_USING_ARENA()
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours, g_FMMC_STRUCT.iTODOverrideMinutes, 0)
						IF CONTENT_IS_USING_ARENA()
							SET_BIT(dmVarsPassed.iDmBools2 , DM_BOOL2_SET_TIME_FOR_ARENA)
							CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[TMS][ARENA][8] Not setting time of day due to g_bArenaBigScreenBinkPlaying")
			ENDIF
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DM_321GO_Countdown)
			OR IS_BIT_SET(serverBDpassed.iServerBitSet2, SERV_BITSET2_FINISHED_COUNTDOWN)
				g_bMission321Done = TRUE
			ENDIF
				
			IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				DISABLE_DPADDOWN_THIS_FRAME()
			ELSE
				DRAW_DM_HUD(LocalPlayer, serverBDPassed, FALSE, dmVarsPassed, serverBDpassedLDB, playerBDPassed)
				DPAD_LEADERBOARD(GET_SUB_MODE(serverBDpassed), serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB)
			ENDIF
			
			IF NOT HAS_DEATHMATCH_FINISHED(serverBDpassed)
				IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
					SET_SKYFREEZE_FROZEN() //*1618098
					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE) 			
					GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)	
				ENDIF
			ENDIF
			
			// Cleanup spectator cam			
			IF DOES_THIS_SPECTATOR_CAM_WANT_TO_LEAVE(g_BossSpecData.specCamData) // confirm quit						
			
				IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
					PRINTLN("CLIENT_MISSION_STAGE_SPECTATOR - DEACTIVATE_SPECTATOR_CAM ")	
					//DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, /*DSCF_SWOOP_UP|*/DSCF_REMAIN_HIDDEN)
					DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
				ENDIF
				STOP_DM_SCORE(dmVarsPassed)
				STOP_DM_ARENA_SCORE(dmVarsPassed)
				GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_END)
			ELIF HAS_DEATHMATCH_FINISHED(serverBDpassed)
				IF NOT CONTENT_IS_USING_ARENA() // done in CLIENT_MOVES_TO_FIX_DEAD_WHEN_DM_OVER
					IF IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
						PRINTLN("CLIENT_MISSION_STAGE_SPECTATOR - DEACTIVATE_SPECTATOR_CAM ")	
						//DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, /*DSCF_SWOOP_UP|*/DSCF_REMAIN_HIDDEN)
						DEACTIVATE_SPECTATOR_CAM(g_BossSpecData, DSCF_BAIL_FOR_TRANSITION)
					ENDIF
					PRINTLN("CLIENT_MISSION_STAGE_SPECTATOR - CLIENT_MISSION_STAGE_LEADERBOARD ")
					
					IF IS_PLAYER_USING_ANY_TURRET(LocalPlayer)
						TURRET_MANAGER_UNLOCK()
					ENDIF
					
					IF IS_THIS_A_ROUNDS_MISSION()
						SET_UP_ROUNDS_DETAILS_AT_END_OF_MISSION(serverBDpassed.iWinningTeam, playerBDPassed[iLocalPart].iTeam)	 // Spectators don't need any of the params here to be populated as they're ignored in the functions called, it's just important we call SET_THAT_IM_READY_FOR_END_OF_ROUND.
					ENDIF
					
					IF SET_SKYSWOOP_UP(TRUE, TRUE, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT)
						STOP_DM_SCORE(dmVarsPassed)
						STOP_DM_ARENA_SCORE(dmVarsPassed)
						PRINTLN("[LM][SPEC_SPEC][JIP] - BoxCheck: ", SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS(), " ArenaCheck: ", CONTENT_IS_USING_ARENA(),
											" SpecCheck: ", DID_I_JOIN_MISSION_AS_SPECTATOR())				
						IF SHOULD_LOCAL_PLAYER_STAY_IN_ARENA_BOX_BETWEEN_ROUNDS()
						AND CONTENT_IS_USING_ARENA()
						AND DID_I_JOIN_MISSION_AS_SPECTATOR()
							PRINTLN("[LM][SPEC_SPEC][JIP] - Setting that we want to stay as a special spectator so we can double check.")
							SET_JIP_SHOW_STAY_AS_SPECTATOR_SCREEN_BETWEEN_ROUNDS(TRUE)
						ENDIF
						
						GOTO_CLIENT_MISSION_STAGE(playerBDPassed, CLIENT_MISSION_STAGE_LEADERBOARD)
					ENDIF
				ENDIF
			ELSE
				// Launch spectator cam
				IF NOT bIsSCTV
				AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
					IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(g_BossSpecData.specCamData)
						PRINTLN(" ACTIVATE_SPECTATOR_CAM() ")
						ACTIVATE_SPECTATOR_CAM(g_BossSpecData, SPEC_MODE_PLAYERS_RESPAWN_EVENT, NULL, ASCF_HIDE_LOCAL_PLAYER|ASCF_QUIT_SCREEN)						
					ENDIF
				ENDIF
				
				IF NOT g_bCelebrationScreenIsActive 
					IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_READY_FOR_LEADERBOARD)
					AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
						HAS_LEADERBOARD_FINISHED(serverFMMCPassed, Placement, serverBDpassed, playerBDPassed, dmVarsPassed, serverBDpassedLDB, TRUE)
					ELSE
						SET_ON_LBD_GLOBAL(FALSE)
						IF IS_A_SPECTATOR_CAM_RUNNING()
							IF NOT IS_SPECTATOR_SHOWING_NEWS_HUD()
								DRAW_SPECTATOR_HUD(serverBDpassed, dmVarsPassed, serverBDpassedLDB, playerBDPassed)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_QUIT_REQUESTED)
					DO_SCREEN_FADE_OUT(50)
					SCRIPT_CLEANUP(serverBDpassed, serverFMMCPassed, playerBDPassed, dmVarsPassed, Placement, ciFMMC_END_OF_MISSION_STATUS_SCTV_LEAVING, serverBDpassedLDB)
				ENDIF
			ENDIF
		BREAK
		
		CASE CLIENT_MISSION_STAGE_END
			IF IS_BIT_SET(dmVarsPassed.iNonBDBitSet, NONBD_BITSET_THUMB_VOTE_SAVED)	
			OR IS_PLAYER_ON_IMPROMPTU_DM()
			OR NOT IS_BIT_SET(dmVarsPassed.sSaveOutVars.iBitSet, ciRATINGS_VOTED) // didn't vote
			OR IS_LOCAL_PLAYER_SPECTATOR_ONLY(playerBDPassed)	
			OR IS_PLAYER_ON_BOSSVBOSS_DM()
				
				IF NOT IS_PLAYER_ON_BOSSVBOSS_DM()
				
					SCRIPT_CLEANUP(serverBDpassed, serverFMMCPassed, playerBDPassed, dmVarsPassed, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED, serverBDpassedLDB)
					
				ELSE
					IF GB_MAINTAIN_BOSS_END_UI(dmVarsPassed.gbBossEndUi) 
						SCRIPT_CLEANUP(serverBDpassed, serverFMMCPassed, playerBDPassed, dmVarsPassed, Placement, ciFMMC_END_OF_MISSION_STATUS_PASSED, serverBDpassedLDB)
					ELSE
						IF GET_FRAME_COUNT() % 100 = 0
							CPRINTLN(DEBUG_NET_MAGNATE, "     ---------->     BOSSVBOSS -  WAITING FOR GB_MAINTAIN_BOSS_END_UI ")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("Waiting_on_Thumb_Vote_Saving...")
			ENDIF
		BREAK
		
		DEFAULT 
			#IF IS_DEBUG_BUILD
				SCRIPT_ASSERT("DM: Problem in PROCESS_CLIENT_LOBBY_DEATHMATCH GET_CLIENT_MISSION_STAGE") 
			#ENDIF
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP() 
	#ENDIF
	#ENDIF		
	
ENDPROC
