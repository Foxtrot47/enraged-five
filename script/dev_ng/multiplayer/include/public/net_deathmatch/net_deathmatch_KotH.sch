USING "net_deathmatch_include.sch"

PROC ASSIGN_KOTH_TEAM_NAMES(ServerBroadcastData &serverBDpassed, KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo, KING_OF_THE_HILL_RUNTIME_PLAYER_STRUCT& sKOTH_PlayerInfo, KING_OF_THE_HILL_RUNTIME_LOCAL_STRUCT& sKOTH_LocalInfo)

	UNUSED_PARAMETER(sKOTH_PlayerInfo)
	
	IF IS_BIT_SET(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_AssignedTeamNames)
		EXIT
	ENDIF
	
	INT iTeam
	FOR iTeam = 0 TO GET_KOTH_TEAM_COUNT(NOT IS_TEAM_DEATHMATCH()) - 1
		IF IS_TEAM_KOTH(sKOTH_HostInfo)
			sKOTH_LocalInfo.tlCachedTeamNames[iTeam] = GET_TDM_NAME(serverBDPassed, iTeam, sKOTH_PlayerInfo.iCachedKOTHTeam != iTeam)
		ELSE
			PARTICIPANT_INDEX piPart = INT_TO_PARTICIPANTINDEX(iTeam)
			
			IF NETWORK_IS_PARTICIPANT_ACTIVE(piPart)
				PLAYER_INDEX thisPlayer = NETWORK_GET_PLAYER_INDEX(piPart)
				sKOTH_LocalInfo.tlCachedTeamNames[iTeam] = GET_PLAYER_NAME(thisPlayer)
			ENDIF
		ENDIF
		
		PRINTLN("[KOTH_INIT] tlCachedTeamNames[", iTeam, "] = ", sKOTH_LocalInfo.tlCachedTeamNames[iTeam])
	ENDFOR
	
	SET_BIT(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_AssignedTeamNames)
ENDPROC

FUNC BOOL NEED_TO_SKIP_PART_FOR_KOTH_HUD(INT& iRivalPlayer, INT& iLeaderboardPos)
	
	IF iLeaderboardPos >= NUM_NETWORK_PLAYERS - 1
		RETURN FALSE
	ENDIF
	
	IF iRivalPlayer = -1
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iRivalPlayer))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DRAW_KOTH_HUD(ServerBroadcastData &serverBDpassed, KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo, KING_OF_THE_HILL_RUNTIME_PLAYER_STRUCT& sKOTH_PlayerInfo, KING_OF_THE_HILL_RUNTIME_LOCAL_STRUCT& sKOTH_LocalInfo, PLAYER_INDEX piPlayerToUse, ServerBroadcastData_Leaderboard &serverBDpassedLDB)
	
	INT iTargetScore = GET_TARGET_SCORE(serverBDpassed, -1)
	BOOL bLiteralString = NOT IS_TEAM_KOTH(sKOTH_HostInfo)
	BOOL bIsArenaSpectator = IS_BIT_SET(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_Spectating) AND CONTENT_IS_USING_ARENA()
	
	IF sKOTH_PlayerInfo.iCachedKOTHTeam = -1
		//Not ready yet!
		PRINTLN("[KOTH] DRAW_KOTH_HUD - Not drawing HUD due to sKOTH_PlayerInfo.iCachedKOTHTeam being -1")
		EXIT
	ENDIF
	
	IF IS_TEAM_KOTH(sKOTH_HostInfo)
		INT iKOTH_LeaderboardPosToDraw = 0
		
		FOR iKOTH_LeaderboardPosToDraw = 0 TO serverBDpassed.iNumberOfTeams - 1
			INT iKOTHTeamToDraw
			INT iOrder = (ENUM_TO_INT(HUDORDER_TENTHBOTTOM) - ENUM_TO_INT(ciKotH_MAX_HILLS)) - iKOTH_LeaderboardPosToDraw
			HUDORDER eOrder = INT_TO_ENUM(HUDORDER, iOrder)
			
			SWITCH iKOTH_LeaderboardPosToDraw
				CASE 0		iKOTHTeamToDraw = serverBDpassed.iWinningTeam BREAK
				CASE 1		iKOTHTeamToDraw = serverBDpassed.iSecondTeam BREAK
				CASE 2		iKOTHTeamToDraw = serverBDpassed.iThirdTeam BREAK
				CASE 3		iKOTHTeamToDraw = serverBDpassed.iFourthTeam BREAK
				CASE 4		iKOTHTeamToDraw = serverBDpassed.iLosingTeam BREAK
			ENDSWITCH
			
			IF iKOTHTeamToDraw > -1
				TEXT_LABEL_63 tlName = GET_TDM_NAME(serverBDpassed, iKOTHTeamToDraw, iKOTHTeamToDraw != sKOTH_PlayerInfo.iCachedKOTHTeam, FALSE)
				
				IF iKOTHTeamToDraw = sKOTH_PlayerInfo.iCachedKOTHTeam
				AND NOT bIsArenaSpectator
					tlName = "MC_TRH_TM0"
				ENDIF
				
				DRAW_KOTH_TEAM_BAR(sKOTH_HostInfo, sKOTH_PlayerInfo, iKOTHTeamToDraw, tlName, serverBDpassed.iTeamScore[iKOTHTeamToDraw], iTargetScore, eOrder, FALSE)
			ENDIF
		ENDFOR
	ELSE
		INT iRivalPlayer = serverBDpassedLDB.leaderBoard[0].iParticipant
		INT iRivalScore = GET_SCORE_FOR_POS(serverBDpassedLDB, 0)
		INT iLeaderboardPos = 0
		
		INT iMyScore = g_iMyDMScore
		
		IF IS_BIT_SET(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_Spectating)
			iMyScore = GET_PLAYER_SCORE(serverBDpassed, piPlayerToUse, serverBDpassedLDB)
		ENDIF
		
		// If we're in position 0, show info from position 1 as our rival instead
		IF IS_PLAYER_WINNING(piPlayerToUse, serverBDpassed, serverBDpassedLDB)
			iRivalPlayer = serverBDpassedLDB.leaderBoard[1].iParticipant
			iRivalScore = GET_SCORE_FOR_POS(serverBDpassedLDB, 1)
			iLeaderboardPos = 1
		ENDIF
		
		WHILE NEED_TO_SKIP_PART_FOR_KOTH_HUD(iRivalPlayer, iLeaderboardPos)
			iLeaderboardPos++
			PRINTLN("[KOTH][HUD] Moving onto ", iLeaderboardPos)
			
			IF serverBDpassedLDB.leaderBoard[iLeaderboardPos].iParticipant != iLocalPart
				iRivalPlayer = serverBDpassedLDB.leaderBoard[iLeaderboardPos].iParticipant
				iRivalScore = GET_SCORE_FOR_POS(serverBDpassedLDB, iLeaderboardPos)
			ELSE
				PRINTLN("[KOTH][HUD] This is me! Not using this one")
			ENDIF
		ENDWHILE
		
		DRAW_KOTH_TEAM_BAR(sKOTH_HostInfo, sKOTH_PlayerInfo, sKOTH_PlayerInfo.iCachedKOTHTeam, sKOTH_LocalInfo.tlCachedTeamNames[sKOTH_PlayerInfo.iCachedKOTHTeam], iMyScore, iTargetScore, HUDORDER_FIFTHBOTTOM, bLiteralString)
		
		IF iRivalPlayer > -1
			DRAW_KOTH_TEAM_BAR(sKOTH_HostInfo, sKOTH_PlayerInfo, iRivalPlayer, sKOTH_LocalInfo.tlCachedTeamNames[iRivalPlayer], iRivalScore, iTargetScore, HUDORDER_FOURTHBOTTOM, bLiteralString)
		ENDIF
	ENDIF	
	
	//Intro Shard
	IF NOT IS_BIT_SET(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_DisplayedShard)
		STRING sShardText
		IF GET_KOTH_ACTIVE_HILL_COUNT(sKOTH_HostInfo, sKOTH_LocalInfo) > 1
			sShardText = "KOTH_C_OUT_M"
		ELSE
			sShardText = "KOTH_C_OUT_S"
		ENDIF
		
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_MIDSIZED, "KOTH_SHRD", sShardText)
		SET_BIT(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_DisplayedShard)
	ENDIF
ENDPROC

PROC PROCESS_KING_OF_THE_HILL_HOST(FMMC_SERVER_DATA_STRUCT& serverFMMCPassed, ServerBroadcastData& serverBDPassed, KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo, KING_OF_THE_HILL_RUNTIME_LOCAL_STRUCT& sKOTH_LocalInfo, PlayerBroadcastData& playerBDPassed[], BOOL bFFA)
	
	UNUSED_PARAMETER(serverBDPassed)
	
	INT iArea
	INT iTeamLoop = FMMC_MAX_TEAMS
			
	IF NOT IS_BIT_SET(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_AssignedEntities)
		PRINTLN("[KOTH_HOST] PROCESS_KING_OF_THE_HILL_HOST - Exiting early because we're waiting for ciKOTH_LocalBS_AssignedEntities")
		EXIT
	ENDIF
	
	IF bFFA
		iTeamLoop = NUM_NETWORK_PLAYERS
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF NOT IS_BIT_SET(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_IsHost)
			PROCESS_BECOMING_HOST_OF_KING_OF_THE_HILL(sKOTH_HostInfo, sKOTH_LocalInfo)
			SET_BIT(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_IsHost)
		ENDIF
	ENDIF
	
	// Object Respawning
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfObjects > 0
		IF NOT CREATE_FMMC_OBJECTS_MP(serverFMMCPassed.niObject, TRUE)
			PRINTLN("[KOTH_HOST] CREATE_FMMC_OBJECTS_MP returned FALSE!")
		ENDIF
	ENDIF
	
	FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
	
		PRINTLN("[KOTH_HOST] Processing Hill ", iArea, " ==============================================================")
		
		IF NOT IS_KOTH_HILL_ACTIVE(iArea, serverBDPassed.iNumActivePlayers)
			RELOOP
		ENDIF
		
		INT iTeam = 0
		INT iDifferentTeamsInArea = 0
		
		FOR iTeam = 0 TO iTeamLoop - 1
			
			IF sKOTH_HostInfo.sHills[iArea].iPlayersInHillPerTeam[iTeam] > 0
			OR sKOTH_HostInfo.sHills[iArea].iTeamHolding = iTeam
			OR sKOTH_HostInfo.sHills[iArea].iTeamCapturing = iTeam
				PRINTLN("[KOTH_HOST] Team ", iTeam, " --- ", sKOTH_HostInfo.sHills[iArea].iPlayersInHillPerTeam[iTeam], " players in area")
				PRINTLN("[KOTH_HOST][KOTH_POINTS] iPlayersToGainPointsBS: ", sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS)
				
			ENDIF
		
			HOST_PROCESS_KOTH_HILL_FOR_TEAM(sKOTH_HostInfo, sKOTH_LocalInfo, iArea, iTeam, iDifferentTeamsInArea)
		ENDFOR
		
		HOST_PROCESS_KOTH_AREA(sKOTH_HostInfo, playerBDPassed[iLocalPart].sKOTH_PlayerInfo, sKOTH_LocalInfo, iArea, iDifferentTeamsInArea)
	ENDFOR
ENDPROC

PROC INIT_KING_OF_THE_HILL(PlayerBroadcastData& playerBDPassed[], ServerBroadcastData &serverBDPassed, KING_OF_THE_HILL_RUNTIME_HOST_STRUCT& sKOTH_HostInfo, KING_OF_THE_HILL_RUNTIME_PLAYER_STRUCT& sKOTH_PlayerInfo, KING_OF_THE_HILL_RUNTIME_LOCAL_STRUCT& sKOTH_LocalInfo)
	
	UNUSED_PARAMETER(sKOTH_PlayerInfo)
	UNUSED_PARAMETER(playerBDPassed)
	UNUSED_PARAMETER(serverBDPassed)
	
	IF bIsLocalPlayerHost
		IF IS_TEAM_DEATHMATCH()
			SET_BIT(sKOTH_HostInfo.iKingOfTheHillBS, ciKotH_Runtime__TeamMode)
			PRINTLN("[KOTH_INIT] Caching as Team KOTH")
		ENDIF
	ENDIF
	
	IF DID_I_JOIN_MISSION_AS_SPECTATOR()
		SET_BIT(sKOTH_LocalInfo.iKOTH_LocalBS, ciKOTH_LocalBS_Spectating)
		PRINTLN("[KOTH_INIT] Setting ciKOTH_LocalBS_Spectating")
	ELSE
		PRINTLN("[KOTH_INIT] NOT setting ciKOTH_LocalBS_Spectating")
	ENDIF
	
	SET_PED_CONFIG_FLAG(LocalPlayerPed, PCF_BlockAutoSwapOnWeaponPickups, TRUE)
	PRINTLN("[KOTH_INIT][FORCE_WEAPON] Setting PCF_BlockAutoSwapOnWeaponPickups")
	
	gweapon_type_CurrentlyHeldWeapon = WEAPONTYPE_INVALID
	PRINTLN("[KOTH_INIT][FORCE_WEAPON] Setting gweapon_type_CurrentlyHeldWeapon to WEAPONTYPE_INVALID")
	
	FORCE_RENDER_IN_GAME_UI(TRUE)
	
	SET_FORCED_KOTH_SETTINGS(IS_TEAM_DEATHMATCH())
	
ENDPROC

FUNC BOOL IS_PLAYER_VALID_FOR_KING_OF_THE_HILL(INT iParticipant, PlayerBroadcastData& playerBDPassed[])
	
	BOOL bPrintDebug = FALSE
	
	#IF IS_DEBUG_BUILD
	bPrintDebug = GET_COMMANDLINE_PARAM_EXISTS("KotH_ValidPlayerDebug")
	#ENDIF
	
	IF iParticipant = -1
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
		IF bPrintDebug
			PRINTLN("[KOTH_SPAM] IS_PLAYER_VALID_FOR_KING_OF_THE_HILL - Returning FALSE || Participant ", iParticipant, " is not active")
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF playerBDPassed[iParticipant].eClientLogicStage != CLIENT_MISSION_STAGE_FIGHTING
	AND playerBDPassed[iParticipant].eClientLogicStage != CLIENT_MISSION_STAGE_DEAD
	AND playerBDPassed[iParticipant].eClientLogicStage != CLIENT_MISSION_STAGE_RESPAWN
		IF bPrintDebug
			PRINTLN("[KOTH_SPAM] IS_PLAYER_VALID_FOR_KING_OF_THE_HILL - Returning FALSE || Participant ", iParticipant, " is not at CLIENT_MISSION_STAGE_FIGHTING or DEAD or RESPAWN")
		ENDIF
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
	
	IF IS_PARTICIPANT_A_SPECTATOR(playerBDPassed, iParticipant)
	OR IS_PLAYER_SPECTATOR(playerBDPassed, piPlayer, iParticipant)
		IF bPrintDebug
			PRINTLN("[KOTH_SPAM] IS_PLAYER_VALID_FOR_KING_OF_THE_HILL - Returning FALSE || Participant ", iParticipant, " is some form of spectator || ", GET_PLAYER_NAME(piPlayer))
		ENDIF
		RETURN FALSE
	ENDIF
	
//	IF IS_PLAYER_DEAD(piPlayer)
//		IF bPrintDebug
//			PRINTLN("[KOTH_SPAM] IS_PLAYER_VALID_FOR_KING_OF_THE_HILL - Returning FALSE || Participant ", iParticipant, " is dead || ", GET_PLAYER_NAME(piPlayer))
//		ENDIF
//		RETURN FALSE
//	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(piPlayer, FALSE)
		IF bPrintDebug
			PRINTLN("[KOTH_SPAM] IS_PLAYER_VALID_FOR_KING_OF_THE_HILL - Returning FALSE || Participant ", iParticipant, " isn't okay || ", GET_PLAYER_NAME(piPlayer))
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF bPrintDebug
		PRINTLN("[KOTH_SPAM] IS_PLAYER_VALID_FOR_KING_OF_THE_HILL - Returning TRUE!! || Participant ", iParticipant, " is valid || ", GET_PLAYER_NAME(piPlayer))
	ENDIF
	RETURN TRUE
ENDFUNC

PROC PROCESS_KOTH_PARTICIPANT_EVERY_FRAME_CHECKS(INT iParticipant, BOOL bParticipantActive, ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	IF bIsLocalPlayerHost
		IF playerBDPassed[iParticipant].sKOTH_PlayerInfo.iCurrentHillBS > -1
			INT iPlayerTeam = playerBDPassed[iParticipant].sKOTH_PlayerInfo.iCachedKOTHTeam
			BOOL bValid = IS_PLAYER_VALID_FOR_KING_OF_THE_HILL(iParticipant, playerBDPassed)
			
			IF iPlayerTeam = -1
				PRINTLN("[KOTH] Exiting out of PROCESS_KOTH_PARTICIPANT_EVERY_FRAME_CHECKS because iPlayerTeam = -1")
				EXIT
			ENDIF
			
			PLAYER_INDEX piPlayer
			BOOL bValidToCapture = FALSE
			
			IF bParticipantActive
				piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				bValidToCapture = IS_NET_PLAYER_OK(piPlayer, TRUE)
			ENDIF
			
			INT iArea
			FOR iArea = 0 TO g_FMMC_STRUCT.sKotHData.iNumberOfHills - 1
				
				IF bValid
				AND bValidToCapture
					IF IS_CAPTURING_KOTH_HILL(playerBDPassed[iParticipant].sKOTH_PlayerInfo, iArea)
						serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersInHillPerTeam[iPlayerTeam]++
						serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersInHill++
						
						SET_BIT(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersInHillBS, iPlayerTeam)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, iParticipant)
					IF serverBDpassed.sKOTH_HostInfo.sHills[iArea].iTeamHolding != iPlayerTeam
						CLEAR_BIT(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, iParticipant)
						serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPoints--
						PRINTLN("[KOTH_HOST][KOTH_POINTS] Removing participant ", iParticipant, " from iPlayersInHillBS because they're not in charge of it")
					ENDIF
					
					IF NOT bParticipantActive
					OR NOT bValid
						CLEAR_BIT(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, iParticipant)
						serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPoints--
						PRINTLN("[KOTH_HOST][KOTH_POINTS] Removing participant ", iParticipant, " from iPlayersInHillBS because they're not active/valid")
					ENDIF
				ELSE
					IF bValid
						IF IS_CAPTURING_KOTH_HILL(playerBDPassed[iParticipant].sKOTH_PlayerInfo, iArea)
						AND serverBDpassed.sKOTH_HostInfo.sHills[iArea].iTeamHolding = iPlayerTeam
						AND bParticipantActive
							
							SET_BIT(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, iParticipant)
							serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPoints++
							PRINTLN("[KOTH_HOST] Adding participant ", iParticipant, " to iPlayersInHillBS")
							
							//Remove someone who's not in the Hill any more
							INT iRemovePart = 0
							REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iRemovePart
								IF iRemovePart = iParticipant
									RELOOP
								ENDIF
								IF IS_BIT_SET(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, iRemovePart)
									IF IS_CAPTURING_KOTH_HILL(playerBDPassed[iRemovePart].sKOTH_PlayerInfo, iArea)
										PRINTLN("[KOTH_HOST][KOTH_POINTS] Letting participant ", iRemovePart, " stay in iPlayersInHillBS")
									ELSE
										CLEAR_BIT(serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPointsBS, iRemovePart)
										serverBDpassed.sKOTH_HostInfo.sHills[iArea].iPlayersToGainPoints--
										PRINTLN("[KOTH_HOST][KOTH_POINTS] Removing participant ", iRemovePart, " from iPlayersInHillBS")
										BREAKLOOP
									ENDIF
								ENDIF
							ENDREPEAT
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL KOTH_IDLE_FOR_TOO_LONG(SHARED_DM_VARIABLES &dmVarsPassed)

	IF PLAYED_LONG_ENOUGH(dmVarsPassed.sInactiveDetectionStruct)
		#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 60 = 0
			PRINTLN("[KOTH] KOTH_IDLE_FOR_TOO_LONG || Returning FALSE due to PLAYED_LONG_ENOUGH")
		ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN (g_fKOTHIdleCounter / dmVarsPassed.fTimeSpentCheckingIdle) >= g_sMPTunables.fKOTH_IDLE_TIME_PERCENTAGE_FOR_LOSING_AWARDS
ENDFUNC
