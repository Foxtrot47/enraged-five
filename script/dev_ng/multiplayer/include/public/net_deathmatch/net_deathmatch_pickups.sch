USING "net_deathmatch_include.sch"

FUNC BOOL SHOULD_ALLOW_HEIGHT_ON_PICKUP_BLIP(PICKUP_TYPE ptType)
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_DMShowBlipHeightOnVehiclePickups)
		IF ptType = PICKUP_VEHICLE_CUSTOM_SCRIPT 
		OR ptType = PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC PROCESS_PICKUP_BLIPS(SHARED_DM_VARIABLES &dmVarsPassed, INT iwpn)
	IF DOES_PICKUP_EXIST(dmVarsPassed.Pickup[iwpn])
		IF NOT SHOULD_PICKUP_HAVE_BLIP(dmVarsPassed.Pickup[iwpn], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].sWeaponBlipStruct, iwpn)
			IF DOES_BLIP_EXIST(dmVarsPassed.blipPickups[iwpn])
				REMOVE_BLIP(dmVarsPassed.blipPickups[iwpn])
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(dmVarsPassed.blipPickups[iwpn])
				dmVarsPassed.blipPickups[iwpn] = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].vPos)
				SET_BLIP_SPRITE(dmVarsPassed.blipPickups[iwpn], GET_WEAPON_PICKUP_SPRITE_BLIP(iwpn, DEFAULT, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].iVehicleWeaponPickupType))
				
				IF IS_KING_OF_THE_HILL()
					SET_BLIP_SCALE(dmVarsPassed.blipPickups[iwpn], BLIP_SIZE_NETWORK_PICKUP)
				ELSE
					SET_BLIP_SCALE(dmVarsPassed.blipPickups[iwpn], BLIP_SIZE_NETWORK_PICKUP_LARGE)
				ENDIF
				
				SET_BLIP_AS_SHORT_RANGE(dmVarsPassed.blipPickups[iwpn],TRUE)
				SET_BLIP_PRIORITY(dmVarsPassed.blipPickups[iwpn], GET_CORRECT_BLIP_PRIORITY(BP_WEAPON))
				
				IF SHOULD_ALLOW_HEIGHT_ON_PICKUP_BLIP(GET_WEAPON_PICKUP_TYPE(iwpn))
					SHOW_HEIGHT_ON_BLIP(dmVarsPassed.blipPickups[iwpn], TRUE)
				ENDIF
				
				IF IS_PICKUP_HEALTH(GET_WEAPON_PICKUP_TYPE(iwpn))
					SET_BLIP_COLOUR(dmVarsPassed.blipPickups[iwpn], BLIP_COLOUR_GREEN)
				ELIF IS_PICKUP_ARMOUR(GET_WEAPON_PICKUP_TYPE(iwpn))
					SET_BLIP_COLOUR(dmVarsPassed.blipPickups[iwpn], BLIP_COLOUR_BLUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	FLOAT fPickupDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LocalPlayerPed,g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iwpn].vPos)
//	IF fPickupDist > dmVarsPassed.fFurthestWeaponDistTemp
//		//PRINTLN("PROCESS_PICKUP_BLIPS, fPickupDist = ", fPickupDist)
//		dmVarsPassed.fFurthestWeaponDistTemp = fPickupDist
//	ENDIF
ENDPROC

PROC REMOVE_PICKUPS_AND_THEIR_BLIPS(SHARED_DM_VARIABLES &dmVarsPassed)

	PRINTLN("REMOVE_PICKUPS_AND_THEIR_BLIPS ")
	INT i
	REPEAT g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons i
		IF i < (FMMC_MAX_WEAPONS - 1)
			IF DOES_PICKUP_EXIST(dmVarsPassed.Pickup[i])
				IF NETWORK_HAS_CONTROL_OF_PICKUP(dmVarsPassed.Pickup[i])
					REMOVE_PICKUP(dmVarsPassed.Pickup[i])
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(dmVarsPassed.blipPickups[i])
				REMOVE_BLIP(dmVarsPassed.blipPickups[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC SERVER_SETS_BRUCIE_BOX_COORDS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
		
	IF NOT HAS_DM_STARTED(serverBDpassed)
	OR g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
	OR IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	OR IS_KING_OF_THE_HILL()
	OR NOT SHOULD_EXTRA_GAMEPLAY_FEATURES_RUN_IN_THIS_DM()
		EXIT
	ENDIF
	
	// Debug relaunch
	#IF IS_DEBUG_BUILD
	IF dmVarsPassed.debugDmVars.bDebugBrucieBox = TRUE
		CLEAR_DM_BRU_BOX_DATA()
		serverBDpassed.iBrucieProgress = BRUCIE_STAGE_RESET_TIMER
	ENDIF
	#ENDIF
	
	IF dmVarsPassed.iBrucieInstance = -1	
		IF g_i_BrucieInstanceDm > -1
			dmVarsPassed.iBrucieInstance = (g_i_BrucieInstanceDm)
			PRINTLN("SERVER_SETS_BRUCIE_BOX_COORDS, iInstance = ", dmVarsPassed.iBrucieInstance, " g_i_BrucieInstanceDm = ", g_i_BrucieInstanceDm)
		ENDIF
	ELSE
		IF g_i_BrucieInstanceDm = -1
			dmVarsPassed.iBrucieInstance = -1
			PRINTLN("SERVER_SETS_BRUCIE_BOX_COORDS, RESET iBrucieInstance AS GLOBAL SAYS SO")
		ENDIF
	ENDIF

	// Check whether Brucie box has been activated
	IF NETWORK_IS_SCRIPT_ACTIVE("AM_BRU_BOX", dmVarsPassed.iBrucieInstance)	
		PRINTLN("SERVER_SETS_BRUCIE_BOX_COORDS, NETWORK_IS_SCRIPT_ACTIVE ")
		IF serverBDpassed.bScriptLaunched = FALSE
			PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS, NETWORK_IS_SCRIPT_ACTIVE")
			// If it has then reset
			serverBDpassed.iBrucieProgress = BRUCIE_STAGE_RESET_TIMER
			serverBDpassed.bScriptLaunched = TRUE
		ENDIF
	ELSE
		PRINTLN("SERVER_SETS_BRUCIE_BOX_COORDS, NOT NETWORK_IS_SCRIPT_ACTIVE dmVarsPassed.iBrucieInstance = ", dmVarsPassed.iBrucieInstance)
		IF serverBDpassed.bScriptLaunched = TRUE
			PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS, serverBDpassed.bScriptLaunched = FALSE")
			serverBDpassed.bScriptLaunched = FALSE
		ENDIF
	ENDIF

	INT iRand, iRandPickup
	
	IF serverBDpassed.bScriptLaunched = FALSE
	
		SWITCH serverBDpassed.iBrucieProgress
			
			// Start the timer
			CASE BRUCIE_STAGE_RESET_TIMER
				REINIT_NET_TIMER(serverBDpassed.timeBrucieBox)
				serverBDpassed.vPosBrucie = <<0,0,0>>
				serverBDpassed.fBrucieHeading = 0.0
				dmVarsPassed.vBruciePos = <<0,0,0>>
				
				serverBDpassed.iBrucieProgress = BRUCIE_STAGE_GET_BOX_COORDS
			BREAK
			
			// Grab the coords
			CASE BRUCIE_STAGE_GET_BOX_COORDS
				IF HAS_NET_TIMER_EXPIRED(serverBDpassed.timeBrucieBox, BRUCIE_BOX_TIMER)
				#IF IS_DEBUG_BUILD
				OR dmVarsPassed.debugDmVars.bDebugBrucieBox = TRUE // debug
				#ENDIF
					IF IS_VECTOR_ZERO(serverBDpassed.vPosBrucie)
						iRand = GET_RANDOM_DM_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
						PRINTNL() PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS iRand = ", iRand)
						PRINTLN(" [CS_BRU] g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = ", g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
						dmVarsPassed.vBruciePos = GET_PICKUP_VECTOR(iRand)
						
						serverBDpassed.iBrucieProgress = BRUCIE_STAGE_GET_SAFE_SPOT
					ELSE
						#IF IS_DEBUG_BUILD
						dmVarsPassed.debugDmVars.bDebugBrucieBox = FALSE
						#ENDIF
						
						serverBDpassed.iBrucieProgress = BRUCIE_STAGE_BOX_WAIT_COLLECTION
					ENDIF
				ENDIF
			BREAK
			
			CASE BRUCIE_STAGE_GET_SAFE_SPOT
				iRandPickup = GET_RANDOM_DM_INT(g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons)
				SPAWN_SEARCH_PARAMS SpawnSearchParams
				IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons > 0
				AND iRandPickup >= 0 
				AND iRandPickup <= FMMC_MAX_WEAPONS
				
					PRINTLN("[CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS || About to get coords for Bullshark using GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY near pickup ", iRandPickup)
					
					IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iRandPickup].vPos, 20, serverBDpassed.vPosBrucie, serverBDpassed.fBrucieHeading, SpawnSearchParams)
						serverBDpassed.fBrucieHeading 	= g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iRandPickup].vRot.z
						
						IF IS_VECTOR_ZERO(serverBDpassed.vPosBrucie)
							PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS IS_VECTOR_ZERO, BRUCIE_STAGE_GET_SAFE_SPOT [FAILSAFE] ", iRandPickup)
							serverBDpassed.iBrucieProgress = BRUCIE_STAGE_RESET_TIMER
						ELSE
							PRINTNL() PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS vPosBrucie = ", serverBDpassed.vPosBrucie, " fBrucieHeading = ", serverBDpassed.fBrucieHeading, " iRandPickup = ", iRandPickup)PRINTNL()
							serverBDpassed.iBrucieProgress = BRUCIE_STAGE_BOX_WAIT_COLLECTION
						ENDIF
					ELSE
						PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS || GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY returning FALSE looking for safe area near pickup ", iRandPickup)
					ENDIF
				ELIF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(dmVarsPassed.vBruciePos, 30, serverBDpassed.vPosBrucie, serverBDpassed.fBrucieHeading, SpawnSearchParams)
					IF IS_VECTOR_ZERO(serverBDpassed.vPosBrucie)
						PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS IS_VECTOR_ZERO, BRUCIE_STAGE_GET_SAFE_SPOT [FAILSAFE] ")
						serverBDpassed.iBrucieProgress = BRUCIE_STAGE_RESET_TIMER
					ELSE
						PRINTNL() PRINTLN(" [CS_BRU] SERVER_SETS_BRUCIE_BOX_COORDS vPosBrucie = ", serverBDpassed.vPosBrucie, " fBrucieHeading = ", serverBDpassed.fBrucieHeading)PRINTNL()
						serverBDpassed.iBrucieProgress = BRUCIE_STAGE_BOX_WAIT_COLLECTION
					ENDIF
				ENDIF
			BREAK
			
			CASE BRUCIE_STAGE_BOX_WAIT_COLLECTION		
	
				// Brucie box has spawned and awaits collection
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLIENT_MANAGE_BULLSHARK_PICKUP(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)

//	IF NOT HAS_DM_STARTED(serverBDpassed)
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
	
		EXIT
	ENDIF
	
	IF IS_KING_OF_THE_HILL()
		
		EXIT
	ENDIF

	SWITCH serverBDpassed.iBrucieProgress

		CASE BRUCIE_STAGE_BOX_WAIT_COLLECTION
			IF dmVarsPassed.bMakeBrucieBox = FALSE
				IF NOT IS_VECTOR_ZERO(serverBDpassed.vPosBrucie)
					IF NOT serverBDpassed.bScriptLaunched
						SET_DEATHMATCH_BRU_BOX(serverBDpassed.vPosBrucie, serverBDpassed.fBrucieHeading)
						PRINTLN("CLIENT_MANAGE_BULLSHARK_PICKUP, vPosBrucie = ", serverBDpassed.vPosBrucie, " fBrucieHeading = ", serverBDpassed.fBrucieHeading)
						dmVarsPassed.bMakeBrucieBox = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Reset Brucie box so it can be made again
		DEFAULT
			dmVarsPassed.bMakeBrucieBox = FALSE
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_DM_PICKUP_SPAWN_AT_START(INT iPickup)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_DM_SpawnOnTimer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_DM_PICKUP_SPAWN_NOW(INT iPickup, BOOL bInitialSpawn)
	
	IF IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iPlacedBitset, ciFMMC_WEP_DM_SpawnOnTimer)
		RETURN !bInitialSpawn
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL HAS_MODEL_LOADED_FOR_DM_PICKUP(MODEL_NAMES mnPickupModel)
	REQUEST_MODEL(mnPickupModel)
	
	IF HAS_MODEL_LOADED(mnPickupModel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_DM_PICKUP(SHARED_DM_VARIABLES &dmVarsPassed, INT i)
	
	VECTOR vPos = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos
	
	IF IS_VECTOR_ZERO(vPos)
	OR g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt = NUM_PICKUPS
		RETURN FALSE
	ENDIF
	
	INT iPlacementFlags = 0

	SET_PLACEMENT_FLAGS_FOR_PICKUP(iPlacementFlags, i)
	
	IF GET_WEAPON_PICKUP_TYPE(i) = PICKUP_VEHICLE_CUSTOM_SCRIPT 
	OR GET_WEAPON_PICKUP_TYPE(i) = PICKUP_VEHICLE_CUSTOM_SCRIPT_LOW_GLOW
		PRINTLN("Weapon subtype = ", g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType)
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType != -1
			MODEL_NAMES modelName = GET_PICKUP_MODEL_FROM_WEAPON_TYPE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt, TRUE, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iVehicleWeaponPickupType, i, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupModel)
			IF HAS_MODEL_LOADED_FOR_DM_PICKUP(modelName)
				vPos.z += 0.8
				dmVarsPassed.Pickup[i] = CREATE_PICKUP_ROTATE(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt , vPos, <<0.0, 0.0, 0.0>>, iPlacementFlags, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips,EULER_YXZ,DEFAULT,modelName)
				PRINTLN("CREATE_DM_PICKUP - Vehicle weapon pickup")
			ELSE
				PRINTLN("CREATE_DM_PICKUP - Model not loaded for vehicle weapon pickup")
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType = ciSCRIPT_PICKUP_SPEED_BOOST // Vehicle boost
			
			IF HAS_MODEL_LOADED_FOR_DM_PICKUP(VEHICLE_BOOST_PICKUP_MODEL())
				dmVarsPassed.Pickup[i] = CREATE_VEHICLE_BOOST_PICKUP(vPos, iPlacementFlags)
				PRINTLN("CREATE_DM_PICKUP - Boost pickup")
			ENDIF
		ELIF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iSubType = ciSCRIPT_PICKUP_SPIKE // Spikes	
			IF HAS_MODEL_LOADED_FOR_DM_PICKUP(VEHICLE_SPIKE_PICKUP_MODEL()) // Spike
				dmVarsPassed.Pickup[i] = CREATE_VEHICLE_SPIKE_PICKUP(vPos, iPlacementFlags)
				PRINTLN("CREATE_DM_PICKUP - Spike pickup")
			ENDIF	
		ELSE
			IF HAS_MODEL_LOADED_FOR_DM_PICKUP(VEHICLE_ROCKET_PICKUP_MODEL()) // Rockets
				dmVarsPassed.Pickup[i] = CREATE_VEHICLE_ROCKET_PICKUP(vPos, iPlacementFlags)
				PRINTLN("CREATE_DM_PICKUP - Rocket pickup")
			ENDIF
		ENDIF
	ELSE
		WEAPON_TYPE wtPickupWeaponType = GET_WEAPON_TYPE_FROM_PICKUP_TYPE(GET_WEAPON_PICKUP_TYPE(i))
		INT iAmmoToGive = FMMC_GET_AMMO_FOR_PICKUP_WEAPON(wtPickupWeaponType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iClips, IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DM_UseBulletsForAmmo))
		MODEL_NAMES mnPickupModel = DUMMY_MODEL_FOR_SCRIPT
		IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
			mnPickupModel = GET_MODEL_NAME_FOR_CUSTOM_SCRIPT_PICKUP(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iCustomPickupType, -1)
			REQUEST_MODEL(mnPickupModel)
		ENDIF
		
		IF mnPickupModel = DUMMY_MODEL_FOR_SCRIPT
		OR (mnPickupModel != DUMMY_MODEL_FOR_SCRIPT AND HAS_MODEL_LOADED(mnPickupModel))
			dmVarsPassed.Pickup[i] = CREATE_PICKUP_ROTATE(GET_WEAPON_PICKUP_TYPE(i), vPos + <<0,0,0.10>>, <<0,360,0>>, iPlacementFlags, iAmmoToGive, DEFAULT, DEFAULT, mnPickupModel)
			PRINTLN("CREATE_DM_PICKUP - Weapon pickup - iAmmoToGive: ", iAmmoToGive, " Bullets? ", PICK_STRING(IS_BIT_SET(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].iPlacedBitset, ciFMMC_WEP_DM_UseBulletsForAmmo), "YES", "NO"))
		ENDIF
	ENDIF
	
	IF DOES_PICKUP_EXIST(dmVarsPassed.Pickup[i])
		PRINTLN("CREATE_DM_PICKUP - Created pickup at ",i," and the coordinates ",vPos)
		RETURN TRUE
	ENDIF
	
	PRINTLN("CREATE_DM_PICKUP - Failed to create pickup ",i)
	RETURN FALSE
ENDFUNC

PROC SET_UP_DM_PICKUP(SHARED_DM_VARIABLES &dmVarsPassed, INT i)
	
	IF NOT DOES_PICKUP_EXIST(dmVarsPassed.Pickup[i])
		EXIT
	ENDIF
	
	SET_PICKUP_REGENERATION_TIME(dmVarsPassed.Pickup[i], GET_RESPAWN_TIME_FOR_PICKUP(GET_WEAPON_PICKUP_TYPE(i)))
ENDPROC

FUNC BLIP_SPRITE GET_BLIP_SPRITE_FOR_DM_PICKUP(INT iPickup)
	INT iSubType = -1
	IF g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType != ciCUSTOM_PICKUP_TYPE__NONE
		iSubType = g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iCustomPickupType
	ENDIF
	RETURN GET_WEAPON_PICKUP_SPRITE_BLIP(iPickup, iSubType, g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[iPickup].iVehicleWeaponPickupType)
ENDFUNC

PROC ADD_BLIP_FOR_DM_PICKUP(SHARED_DM_VARIABLES &dmVarsPassed, INT i)
	IF NOT DOES_BLIP_EXIST(dmVarsPassed.blipPickups[i])
		dmVarsPassed.blipPickups[i] = ADD_BLIP_FOR_COORD(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].vPos)
		
		SET_BLIP_PRIORITY(dmVarsPassed.blipPickups[i], GET_CORRECT_BLIP_PRIORITY(BP_WEAPON))
		BLIP_SPRITE bsSprite = GET_BLIP_SPRITE_FOR_DM_PICKUP(i)
		SET_BLIP_SPRITE(dmVarsPassed.blipPickups[i], bsSprite)
		PRINTLN("ADD_BLIP_FOR_DM_PICKUP - Creating blip ",i,"with sprite ", bsSprite)
		
		IF IS_KING_OF_THE_HILL()
			SET_BLIP_SCALE(dmVarsPassed.blipPickups[i], BLIP_SIZE_NETWORK_PICKUP)
		ELSE
			SET_BLIP_SCALE(dmVarsPassed.blipPickups[i], BLIP_SIZE_NETWORK_PICKUP_LARGE)
		ENDIF
		
		SET_BLIP_AS_SHORT_RANGE(dmVarsPassed.blipPickups[i], TRUE)
		
		IF SHOULD_ALLOW_HEIGHT_ON_PICKUP_BLIP(GET_WEAPON_PICKUP_TYPE(i))
			SHOW_HEIGHT_ON_BLIP(dmVarsPassed.blipPickups[i], TRUE)
		ENDIF
		
		IF NOT IS_PICKUP_TYPE_INVALID_NAME(g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
			SET_WEAPON_BLIP_NAME_FROM_PICKUP_TYPE(dmVarsPassed.blipPickups[i], g_FMMC_STRUCT_ENTITIES.sPlacedWeapon[i].pt)
		ENDIF
		
		IF bsSprite = RADAR_TRACE_PICKUP_RANDOM
			SET_BLIP_NAME_FROM_TEXT_FILE(dmVarsPassed.blipPickups[i], "PU_RAND")
		ENDIF
		
		IF IS_PICKUP_HEALTH(GET_WEAPON_PICKUP_TYPE(i))
			SET_BLIP_COLOUR(dmVarsPassed.blipPickups[i], BLIP_COLOUR_GREEN)
		ELIF IS_PICKUP_ARMOUR(GET_WEAPON_PICKUP_TYPE(i))
			SET_BLIP_COLOUR(dmVarsPassed.blipPickups[i], BLIP_COLOUR_BLUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_FORCED_WEAPONS_ONLY_DM_PICKUP(INT i)
	IF IS_JOB_FORCED_WEAPON_ONLY()
	AND GET_DEATHMATCH_CREATOR_RESPAWN_WEAPON(GlobalServerBD_DM.iStartWeapon) = WEAPONTYPE_UNARMED
	AND NOT IS_PICKUP_HEALTH(GET_WEAPON_PICKUP_TYPE(i))
	AND NOT IS_PICKUP_ARMOUR(GET_WEAPON_PICKUP_TYPE(i))
	AND NOT IS_PICKUP_PARACHUTE(GET_WEAPON_PICKUP_TYPE(i))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_CLIENT_CREATED_PICKUPS(SHARED_DM_VARIABLES &dmVarsPassed, BOOL bInitialSpawn = TRUE)
	
	IF g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons = 0
	OR IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		PRINTLN("HAS_CLIENT_CREATED_PICKUPS, WEAPONS_OFF " ) 
		
		RETURN TRUE
	ENDIF
	
	INT i
	REQUEST_MODEL(VEHICLE_SPIKE_DROP_MODEL())
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		
		IF SHOULD_DM_PICKUP_SPAWN_NOW(i, bInitialSpawn)
		AND NOT IS_FORCED_WEAPONS_ONLY_DM_PICKUP(i)
			IF NOT DOES_PICKUP_EXIST(dmVarsPassed.Pickup[i])
				IF CREATE_DM_PICKUP(dmVarsPassed, i)
					SET_UP_DM_PICKUP(dmVarsPassed, i)
					ADD_BLIP_FOR_DM_PICKUP(dmVarsPassed, i) 
					PRINTLN("HAS_CLIENT_CREATED_PICKUPS - Successfully created pickup ", i, " bInitialSpawn: ", PICK_STRING(bInitialSpawn, "TRUE", "FALSE"))
				ELSE
					PRINTLN("HAS_CLIENT_CREATED_PICKUPS - Failed to create pickup ", i, " bInitialSpawn: ", PICK_STRING(bInitialSpawn, "TRUE", "FALSE"))
				ENDIF
			ENDIF
		ENDIF

	ENDFOR
	
	FOR i = 0 TO g_FMMC_STRUCT_ENTITIES.iNumberOfWeapons-1
		IF NOT DOES_PICKUP_EXIST(dmVarsPassed.Pickup[i])
		AND SHOULD_DM_PICKUP_SPAWN_NOW(i, bInitialSpawn)
		AND NOT IS_FORCED_WEAPONS_ONLY_DM_PICKUP(i)
			PRINTLN("HAS_CLIENT_CREATED_PICKUPS - RETURN FALSE, Pickup ", i, " is required at the start but not created yet!")
			RETURN FALSE
		ENDIF
	ENDFOR

	
	PRINTLN("HAS_CLIENT_CREATED_PICKUPS - All pickups required at the start have been created!")
	
	RETURN TRUE
ENDFUNC

PROC ADD_BLIP_FOR_RESPAWNED_PICKUPS(PICKUP_INDEX& dmPickupIndex[], BLIP_INDEX& blipPickups[], INT iNumPickups)
	INT i
	REPEAT iNumPickups i
		IF DOES_PICKUP_EXIST(dmPickupIndex[i])
			IF DOES_PICKUP_OBJECT_EXIST(dmPickupIndex[i])
				IF NOT DOES_BLIP_EXIST(blipPickups[i])
					VECTOR vPos = GET_PICKUP_VECTOR(i)
					IF NOT IS_VECTOR_ZERO(vPos)
						blipPickups[i] = ADD_BLIP_FOR_COORD(vPos)
						SET_BLIP_PRIORITY(blipPickups[i], GET_CORRECT_BLIP_PRIORITY(BP_WEAPON))
						SET_BLIP_SPRITE(blipPickups[i], GET_BLIP_SPRITE_FOR_DM_PICKUP(i))
						
						IF IS_KING_OF_THE_HILL()
							SET_BLIP_SCALE(blipPickups[i], BLIP_SIZE_NETWORK_PICKUP)
						ELSE
							SET_BLIP_SCALE(blipPickups[i], BLIP_SIZE_NETWORK_PICKUP_LARGE)
						ENDIF
						
						SET_BLIP_AS_SHORT_RANGE(blipPickups[i], TRUE)
						
						IF SHOULD_ALLOW_HEIGHT_ON_PICKUP_BLIP(GET_WEAPON_PICKUP_TYPE(i))
							SHOW_HEIGHT_ON_BLIP(blipPickups[i], TRUE)
						ENDIF
						
						IF IS_PICKUP_HEALTH(GET_WEAPON_PICKUP_TYPE(i))
							SET_BLIP_COLOUR(blipPickups[i], BLIP_COLOUR_GREEN)
						ELIF IS_PICKUP_ARMOUR(GET_WEAPON_PICKUP_TYPE(i))
							SET_BLIP_COLOUR(blipPickups[i], BLIP_COLOUR_BLUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC REMOVE_BLIP_FOR_COLLECTED_PICKUP(PICKUP_INDEX& dmPickupIndex[], BLIP_INDEX& blipPickups[], INT iNumPickups)
	INT i
	REPEAT iNumPickups i	
		IF DOES_PICKUP_EXIST(dmPickupIndex[i])
			IF NOT DOES_PICKUP_OBJECT_EXIST(dmPickupIndex[i])
				IF DOES_BLIP_EXIST(blipPickups[i])
					REMOVE_BLIP(blipPickups[i])
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_DELAYED_PICKUP_SPAWNING(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	
	INT iShardTimer = 5000
	
	IF IS_BIT_SET(dmVarsPassed.iDmBools2, DM_BOOL2_DELAYED_PICKUPS_SPAWNED)
		
		IF HAS_NET_TIMER_STARTED_AND_EXPIRED(dmVarsPassed.tdPickupSpawnShardTimer, iShardTimer)
			RESET_NET_TIMER(dmVarsPassed.tdPickupSpawnShardTimer)
			STOP_SOUND(dmVarsPassed.iPickupSpawnShardSoundID)
			RELEASE_SOUND_ID(dmVarsPassed.iPickupSpawnShardSoundID)
			dmVarsPassed.iPickupSpawnShardSoundID = -1
		ENDIF
		
		EXIT
	ENDIF
	
	IF g_FMMC_STRUCT.iSpawnPickupsAtStartTimer = 0
		EXIT
	ENDIF
	
	IF !dmVarsPassed.bWeaponsAndPropsCreated
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(serverBDpassed.timeServerForFinishedLast)
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(serverBDpassed.tdPickupSpawnTimer)
		IF bIsLocalPlayerHost
			REINIT_NET_TIMER(serverBDpassed.tdPickupSpawnTimer)
		ENDIF
	ELSE
		IF HAS_NET_TIMER_EXPIRED(serverBDpassed.tdPickupSpawnTimer, g_FMMC_STRUCT.iSpawnPickupsAtStartTimer*1000)
			IF HAS_CLIENT_CREATED_PICKUPS(dmVarsPassed, FALSE)
				
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "DM_PWR_SPWN", "", DEFAULT, iShardTimer)
				
				IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnablePowerUps)
					REINIT_NET_TIMER(dmVarsPassed.tdPickupSpawnShardTimer)
					dmVarsPassed.iPickupSpawnShardSoundID = GET_SOUND_ID()
					PLAY_SOUND_FRONTEND(dmVarsPassed.iPickupSpawnShardSoundID, "PowerupShard_Attract_Loop", "Go_Kart_Death_Match_Soundset")
				ENDIF
				
				SET_BIT(dmVarsPassed.iDmBools2, DM_BOOL2_DELAYED_PICKUPS_SPAWNED)
				
				IF bIsLocalPlayerHost
					CLEAR_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_COUNTED_PICKUPS)
				ENDIF
				
				PRINTLN("PROCESS_DELAYED_PICKUP_SPAWNING - Delayed pickups have been spawned!!")
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC PROCESS_REMOTE_SCRIPTED_MACHINE_GUNS(FMMC_VEHICLE_MACHINE_GUN &sVehMG, PlayerBroadcastData &playerBDPassed[])
	
	IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_EnableFMMCVehicleMachineGuns)
		EXIT
	ENDIF
	
	REQUEST_NAMED_PTFX_ASSET("scr_ie_vv")
	IF NOT HAS_NAMED_PTFX_ASSET_LOADED("scr_ie_vv")
		EXIT
	ENDIF
	
	INT iPart
	FOR iPart = 0 TO MAX_NUM_MC_PLAYERS - 1
	
		IF iPart = iLocalPart
			RELOOP
		ENDIF
		PARTICIPANT_INDEX piPartToUse = INT_TO_PARTICIPANTINDEX(iPart)
		
		IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(piPartToUse)
			RELOOP
		ENDIF
		
		PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(piPartToUse)
		PED_INDEX piPlayerPed = GET_PLAYER_PED(piPlayer)
		
		IF NOT IS_NET_PLAYER_OK(piPlayer)
			RELOOP
		ENDIF
		
		PROCESS_REMOTE_FMMC_VEHICLE_MACHINE_GUN_FIRING(sVehMG, playerBDPassed[iPart].sScriptedMachineGun, iPart, piPlayerPed)
		
	ENDFOR
ENDPROC

PROC MAINTAIN_VEHICLE_SPECIAL_WEAPONS(ServerBroadcastData &serverBDpassed)
	IF IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)
		UPDATE_VEHICLE_SPIKES()
		UPDATE_VEHICLE_ROCKETS()
		
		IF g_VehicleSpikeInfo.bIsCollected = TRUE
		OR g_VehicleRocketInfo.bIsCollected = TRUE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
			SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(PROP_TYRE_SPIKE_01,FALSE)
			SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(PROP_LD_TOILET_01,FALSE) //This will need changing for rocket model name
		ELSE
			SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(PROP_TYRE_SPIKE_01,TRUE)
			SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(PROP_LD_TOILET_01,TRUE) //This will need changing for rocket model name
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
		ENDIF
	ENDIF
ENDPROC

// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC SERVER_COUNTS_PICKUPS(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	INT i
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_COUNTED_PICKUPS)
		IF HAS_DM_STARTED(serverBDpassed)
			FOR i = 0 TO (FMMC_MAX_WEAPONS -1)
				IF DOES_PICKUP_EXIST(dmVarsPassed.Pickup[i])
					serverBDPassed.iNumPickCreated ++
				ENDIF
			ENDFOR
		
			PRINTLN("SERVER_COUNTS_PICKUPS, serverBDPassed.iNumPickCreated = ", serverBDPassed.iNumPickCreated)
			SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_COUNTED_PICKUPS)
		ENDIF
	ENDIF
ENDPROC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Client
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROC PROCESS_PICKUPS_STAGGERED_CLIENT(ServerBroadcastData &serverBDpassed, SHARED_DM_VARIABLES &dmVarsPassed)
	IF serverBDpassed.iNumPickCreated > 0	
	AND serverBDpassed.iNumPickCreated <= FMMC_MAX_WEAPONS
		IF dmVarsPassed.iWeaponIterator >= serverBDpassed.iNumPickCreated
			dmVarsPassed.iWeaponIterator = 0
		ENDIF
		IF dmVarsPassed.iWeaponIterator < serverBDpassed.iNumPickCreated
			PROCESS_PICKUP_BLIPS(dmVarsPassed, dmVarsPassed.iWeaponiterator)
			PROCESS_POWERUP_PICKUP_SOUND_LOOP(dmVarsPassed.sPowerUps, dmVarsPassed.Pickup[dmVarsPassed.iWeaponiterator], dmVarsPassed.iWeaponiterator)
			PROCESS_PICKUP_WEAPON_ATTACHMENTS(GET_PICKUP_OBJECT(dmVarsPassed.Pickup[dmVarsPassed.iWeaponiterator]), dmVarsPassed.iWeaponIterator, DOES_PICKUP_EXIST(dmVarsPassed.Pickup[dmVarsPassed.iWeaponiterator]))
			dmVarsPassed.iWeaponIterator++
		ENDIF
	ENDIF
ENDPROC
