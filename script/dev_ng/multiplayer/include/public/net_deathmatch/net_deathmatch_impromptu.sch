USING "net_deathmatch_include.sch"

PROC SET_IMPROMPTU_TARGET_PRIORITY_ON(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_PLAYER_ON_IMPROMPTU_DM()
//	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		IF NOT IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_TARGET_PRIORITY)
			PED_INDEX pedTarget = GET_PLAYER_PED(g_sImpromptuVars.playerImpromptuRival)
			IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerImpromptuRival)
				SET_ENTITY_IS_TARGET_PRIORITY(pedTarget, TRUE)
				PRINTLN(" [CS_IMPROMPTU] SET_IMPROMPTU_TARGET_PRIORITY_ON ", NATIVE_TO_INT(g_sImpromptuVars.playerImpromptuRival))
				SET_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_TARGET_PRIORITY)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_IMPROMPTU_TARGET_PRIORITY_OFF(SHARED_DM_VARIABLES &dmVarsPassed)
	IF IS_PLAYER_ON_IMPROMPTU_DM()
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		IF IS_BIT_SET(dmVarsPassed.iNonBDBitset, NONBD_BITSET_TARGET_PRIORITY)
			PED_INDEX pedTarget = GET_PLAYER_PED(g_sImpromptuVars.playerImpromptuRival)
			IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerImpromptuRival)
				SET_ENTITY_IS_TARGET_PRIORITY(pedTarget, FALSE)

				PRINTLN(" [CS_IMPROMPTU] SET_IMPROMPTU_TARGET_PRIORITY_OFF  ", NATIVE_TO_INT(g_sImpromptuVars.playerImpromptuRival))

				CLEAR_BIT(dmVarsPassed.iNonBDBitset, NONBD_BITSET_TARGET_PRIORITY)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_IMPROMPTU_MIDDPOINT_COORD_BEEN_SET(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_GRABBED_IMP_COORD)
ENDFUNC

FUNC BOOL HAVE_IMPROMPTU_PLAYERS_BEEN_CLOSE(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_CLOSE)
ENDFUNC

PROC CHARGE_IMPROMPTU_ENTRANCE_WAGER()
	FLOAT fTuneable 
	INT iFee = ONE_ON_ONE_ENTRY_FEE
	INT iScriptTransactionIndex
	fTuneable = TO_FLOAT(iFee)
	fTuneable = (fTuneable * g_sMPTunables.fImpromptuDmEntryMultiplier)
	iFee = ROUND(fTuneable)
	iFee = MULTIPLY_CASH_BY_TUNABLE(iFee)
	IF NETWORK_CAN_SPEND_MONEY(iFee, FALSE, TRUE, FALSE)
		IF USE_SERVER_TRANSACTIONS()
			TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_MATCH_ENTRY_FEE, iFee, iScriptTransactionIndex, FALSE, TRUE)
			g_cashTransactionData[iScriptTransactionIndex].eExtraData.tl31ContenID = "BM_ONE_DM"
		ELSE
		NETWORK_PAY_MATCH_ENTRY_FEE(iFee, "BM_ONE_DM", FALSE, TRUE)
	ENDIF
	ENDIF
ENDPROC

// 1570539
PROC SET_BIT_WHEN_PLAYERS_CLOSE_ENOUGH(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])
	IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
	OR NOT HAS_IMPROMPTU_MIDDPOINT_COORD_BEEN_SET(serverBDpassed)
	OR IS_PLAYER_ON_BOSSVBOSS_DM()
		EXIT
	ENDIF
	VECTOR vPosPlayer, VPosRival
	PED_INDEX pedRival
	INT ipartLocal
	INT ipartRival
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_CLOSE)
		IF bLocalPlayerOK
			IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerImpromptuRival)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(LocalPlayer)
				AND NETWORK_IS_PLAYER_A_PARTICIPANT(g_sImpromptuVars.playerImpromptuRival)
					ipartLocal = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(LocalPlayer))
					ipartRival = NATIVE_TO_INT(NETWORK_GET_PARTICIPANT_INDEX(g_sImpromptuVars.playerImpromptuRival))
					IF ipartLocal <> -1
					AND ipartRival <> -1
						IF IS_BIT_SET(playerBDPassed[ipartLocal].iClientBitSet, CLIENT_BITSET_ENTERED_IMPROMPTU)
						AND IS_BIT_SET(playerBDPassed[ipartRival].iClientBitSet, CLIENT_BITSET_ENTERED_IMPROMPTU)
							IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_OK_DISTANCE_CHECK)
								PRINTLN("[CS_IMPROMPTU] SET_BIT_WHEN_PLAYERS_CLOSE_ENOUGH, SERV_BITSET_IMPROMPTU_OK_DISTANCE_CHECK ")			
								SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_OK_DISTANCE_CHECK)
							ENDIF
						ENDIF
						IF playerBDPassed[ipartLocal].eClientLogicStage = CLIENT_MISSION_STAGE_FIGHTING
						AND playerBDPassed[ipartRival].eClientLogicStage = CLIENT_MISSION_STAGE_FIGHTING				
							vPosPlayer 	= vLocalPlayerPosition
							pedRival	= GET_PLAYER_PED(g_sImpromptuVars.playerImpromptuRival)
							VPosRival 	= GET_ENTITY_COORDS(pedRival)
							IF GET_DISTANCE_BETWEEN_COORDS(vPosPlayer, VPosRival) <= IMPROMPTU_DISTANCE_CLOSE
								PRINTLN("[CS_IMPROMPTU] SET_BIT_WHEN_PLAYERS_CLOSE_ENOUGH, SERV_BITSET_IMPROMPTU_CLOSE ")
								SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_CLOSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL OK_TO_CHECK_DISTANCE(ServerBroadcastData &serverBDpassed)
	RETURN IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_IMPROMPTU_CLOSE)
ENDFUNC

FUNC BOOL IS_PLAYER_ABANDONING_IMPROMPTU(ServerBroadcastData &serverBDpassed)

	VECTOR vZero = <<0.0,0.0,0.0>>
	VECTOR vPosPlayer
	
	IF ARE_VECTORS_EQUAL(serverBDPassed.vImpromptuCoord, vZero)
	OR IS_THIS_VEHICLE_DEATHMATCH(serverBDpassed)	
	
		RETURN FALSE
	ENDIF
	
	IF bLocalPlayerOK
	
		vPosPlayer = vLocalPlayerPosition
		
		IF GET_DISTANCE_BETWEEN_COORDS(vPosPlayer, serverBDPassed.vImpromptuCoord) >= IMPROMPTU_DISTANCE_WARNING
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_ABANDONED_IMPROMPTU(ServerBroadcastData &serverBDpassed, PLAYER_INDEX PlayerId)

	VECTOR vPosPlayer
	VECTOR vZero = <<0.0,0.0,0.0>>
	
	IF ARE_VECTORS_EQUAL(serverBDPassed.vImpromptuCoord, vZero)
	
		RETURN FALSE
	ENDIF
	
	IF IS_NET_PLAYER_OK(PlayerId)
		PED_INDEX PlayerPedId = GET_PLAYER_PED(PlayerId)
		
		vPosPlayer = GET_ENTITY_COORDS(PlayerPedId) 
		
		IF GET_DISTANCE_BETWEEN_COORDS(vPosPlayer, serverBDPassed.vImpromptuCoord, FALSE) >= IMPROMPTU_DISTANCE_LIMIT
		
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### Section Name: Server
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
// ##### -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
PROC SERVER_GRABS_IMPROMPTU_VECTOR(ServerBroadcastData &serverBDpassed)

	VECTOR vLocalPos
	VECTOR vRivalPos
	VECTOR vZero
	VECTOR vSum
	
	IF NOT IS_BIT_SET(serverBDpassed.iServerBitSet, SERV_BITSET_GRABBED_IMP_COORD)
		IF HAS_DM_STARTED(serverBDpassed)
			IF bLocalPlayerOK
				vLocalPos = vLocalPlayerPosition
				PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, vLocalPos = ", vLocalPos)
			ELSE
				PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, IS_NET_PLAYER_OK  PLAYER_ID  ")
			ENDIF
			
			IF IS_NET_PLAYER_OK(g_sImpromptuVars.playerImpromptuRival)
				IF g_sImpromptuVars.playerImpromptuRival <> LocalPlayer
					PED_INDEX ped = GET_PLAYER_PED(g_sImpromptuVars.playerImpromptuRival)
					vRivalPos = GET_ENTITY_COORDS(ped)
					PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, vRivalPos = ", vRivalPos)
					
				ELSE
					PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, g_sImpromptuVars.playerImpromptuRival =   ")
				ENDIF
			ELSE
				PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, IS_NET_PLAYER_OK  playerImpromptuRival  ")
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(vLocalPos, vZero)
			AND NOT ARE_VECTORS_EQUAL(vRivalPos, vZero)
						
				vSum.x = ((vLocalPos.x + vRivalPos.x)/2)
				vSum.y = ((vLocalPos.y + vRivalPos.y)/2)
				vSum.z = ((vLocalPos.z + vRivalPos.z)/2)
			
				serverBDPassed.vImpromptuCoord = vSum
				PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, vImpromptuCoord = ", serverBDPassed.vImpromptuCoord)
				SET_BIT(serverBDpassed.iServerBitSet, SERV_BITSET_GRABBED_IMP_COORD)
			ELSE
				PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, vLocalPos =   ",vLocalPos)
				PRINTLN("SERVER_GRABS_IMPROMPTU_VECTOR, vRivalPos =   ",vRivalPos)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DO_IMPROMPTU_OPENING_SPLASH()
	SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_START_OF_JOB, "STRAP_ONE", "", HUD_COLOUR_WHITE)
ENDPROC

PROC SEND_IMPROMPTU_WIN_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_WON_IMPROMPTU_DM
	TickerEventData.playerID	= LocalPlayer
	TickerEventData.playerID2	= g_sImpromptuVars.playerImpromptuRival
	PRINTLN(" [CS_IMPROMPTU] BROADCAST_TICKER_EVENT TickerEventData.playerID ", NATIVE_TO_INT(TickerEventData.playerID))
	PRINTLN(" [CS_IMPROMPTU] BROADCAST_TICKER_EVENT TickerEventData.playerID2 ", NATIVE_TO_INT(TickerEventData.playerID2))
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(TRUE))
ENDPROC

PROC SEND_IMPROMPTU_STARTED_TICKER()
	SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
	TickerEventData.TickerEvent = TICKER_EVENT_START_IMPROMPTU_DM
	TickerEventData.playerID	= LocalPlayer
	TickerEventData.playerID2	= g_sImpromptuVars.playerImpromptuRival
	PRINTLN(" [CS_IMPROMPTU] BROADCAST_TICKER_EVENT TickerEventData.playerID ", NATIVE_TO_INT(TickerEventData.playerID))
	PRINTLN(" [CS_IMPROMPTU] BROADCAST_TICKER_EVENT TickerEventData.playerID2 ", NATIVE_TO_INT(TickerEventData.playerID2))
	BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(TRUE))
ENDPROC

FUNC BOOL WAS_IMPROMPTU_DRAW(ServerBroadcastData &serverBDpassed)
	IF serverBDpassed.iImpromptuWinner = -1
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DID_I_WIN_IMPROMPTU(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[])

	IF DID_LOCAL_PLAYER_QUIT_DM_VIA_PHONE(playerBDPassed)
	
		RETURN FALSE
	ENDIF
	
	IF serverBDpassed.playerAbandonedImpromptu <> INVALID_PLAYER_INDEX()
		IF LocalPlayer <> serverBDpassed.playerAbandonedImpromptu
		
			RETURN TRUE
		ENDIF
	ENDIF

	IF serverBDpassed.iImpromptuWinner = iLocalPart
	
		RETURN TRUE
	ENDIF
	
	IF DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed) 
	
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC DO_END_IMPROMPTU_SPLASH(ServerBroadcastData &serverBDpassed, PlayerBroadcastData &playerBDPassed[], SHARED_DM_VARIABLES &dmVarsPassed)
	BOOL bAbandoned
	

	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
		INT iSlot = NATIVE_TO_INT(LocalPlayer)
		IF iSlot <> -1
			// "One on One Deathmatch abandoned"
			IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
				bAbandoned = TRUE
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "BM_IMP_ABN", "", HUD_COLOUR_RED)
			ELIF DID_DM_END_WITHOUT_ENOUGH_PLAYERS(serverBDpassed) // 907242
				bAbandoned = TRUE
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "BM_IMP_ABN", "", HUD_COLOUR_RED)
			ENDIF
				IF IS_PLAYER_ON_BIKER_DM()
					IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_FORCED)
						SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					ENDIF
				ENDIF
		ENDIF
		IF WAS_IMPROMPTU_DRAW(serverBDpassed)
		AND bAbandoned = FALSE
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_TEXT, "IMP_DM_DRAW", "", HUD_COLOUR_WHITE)
				IF IS_PLAYER_ON_BIKER_DM()
					IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
						SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					ENDIF	
				ENDIF
		ELIF DID_I_WIN_IMPROMPTU(serverBDpassed, playerBDPassed)
				IF IS_PLAYER_ON_BIKER_DM()
					
					IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(TRUE, GB_TELEMETRY_END_WON)
						SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					ENDIF	
				ENDIF
			//If I won tell everybody! 
			SEND_IMPROMPTU_WIN_TICKER()
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_WINNER, "BM_OOO_WON", "", HUD_COLOUR_WHITE)
		ELSE
				IF IS_PLAYER_ON_BIKER_DM()
					
					IF NOT IS_BIT_SET(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
						GB_SET_COMMON_TELEMETRY_DATA_ON_END(FALSE, GB_TELEMETRY_END_LOST)
						SET_BIT(dmVarsPassed.iBVBbitset, BVB_BS1_DONE_TELEMETRY)
					ENDIF	
				ENDIF
			SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_LOSER, "BM_OOO_LOS", "", HUD_COLOUR_RED)
		ENDIF
	ENDIF
ENDPROC
