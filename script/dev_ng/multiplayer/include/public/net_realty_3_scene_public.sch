USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD
USING "net_realty_scene_debug.sch"
#ENDIF

ENUM enumnet_realty_3_scenePans
	net_realty_3_SCENE_PAN_descent,
	net_realty_3_SCENE_PAN_MAX
ENDENUM

ENUM enumnet_realty_3_sceneCuts
	net_realty_3_SCENE_CUT_null = 0,
	net_realty_3_SCENE_CUT_MAX
ENDENUM

ENUM enumnet_realty_3_sceneMarkers
	net_realty_3_SCENE_MARKER_gotoA = 0,
	net_realty_3_SCENE_MARKER_gotoREVERSE,
	net_realty_3_SCENE_MARKER_gotoB,
	net_realty_3_SCENE_MARKER_MAX
ENDENUM

ENUM enumnet_realty_3_scenePlacers
	net_realty_3_SCENE_PLACER_resetCoords = 0,
	net_realty_3_SCENE_PLACER_MAX
ENDENUM

ENUM enumnet_realty_3_sceneAngArea
	NET_REALTY_3_SCENE_ANGAREA_area0 = 0,
	NET_REALTY_3_SCENE_ANGAREA_MAX
ENDENUM

STRUCT STRUCT_net_realty_3_SCENE
	structSceneTool_Pan		mPans[net_realty_3_SCENE_PAN_MAX]
	structSceneTool_Cut		mCuts[net_realty_3_SCENE_CUT_MAX]
	
	structSceneTool_Marker	mMarkers[net_realty_3_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[net_realty_3_SCENE_PLACER_MAX]
	
	structSceneTool_AngArea	mAngAreas[NET_REALTY_3_SCENE_ANGAREA_MAX]
	
	BOOL					bEnablePans[net_realty_3_SCENE_PAN_MAX]
	FLOAT					fExitDelay
ENDSTRUCT

FUNC VECTOR GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(INT iBuilding, INT i, BOOL bRotation)
	MP_PROP_OFFSET_STRUCT tempStruct[3]
	
	// Base for offset - OFFICE 2
	tempStruct[0].vLoc = <<-1358.03, -471.41, 30.61>>
	tempStruct[0].vRot = <<0.0, 0.0, -81.19>>
	
	SWITCH iBuilding
		CASE MP_PROPERTY_OFFICE_BUILDING_1
			tempStruct[1].vLoc = <<-1535.10, -581.22, 24.71>>
			tempStruct[1].vRot = <<0.0, 0.0, -146.31>>
		BREAK
		
		CASE MP_PROPERTY_OFFICE_BUILDING_2
			tempStruct[1].vLoc = <<-1358.03, -471.41, 30.61>>
			tempStruct[1].vRot = <<0.0, 0.0, -81.19>>
		BREAK
		
		CASE MP_PROPERTY_OFFICE_BUILDING_3
			tempStruct[1].vLoc = <<-142.62, -572.42, 31.42>>
			tempStruct[1].vRot = <<0.0, 0.0, -19.8>>
		BREAK
		
		CASE MP_PROPERTY_OFFICE_BUILDING_4
			tempStruct[1].vLoc = <<-84.87, -825.55, 35.05>>
			tempStruct[1].vRot = <<0.0, 0.0, 170.0>>
		BREAK
	ENDSWITCH
	
	VEHICLE_INDEX VehicleID
	BOOL bIsBike
	BOOL bIsBig
	VECTOR vMin, vMax
	FLOAT fHeight
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
			
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(VehicleID))
			OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(VehicleID))
				bIsBike = TRUE
			ELSE
				GET_MODEL_DIMENSIONS( GET_ENTITY_MODEL(VehicleID), vMin, vMax )
				fHeight = vMax.z - vMin.z
				PRINTLN("GET_OFFICE_GARAGE_VECTOR_AS_OFFSET - height = ", fHeight)
				IF (fHeight > 2.0)
					bIsBig = TRUE	
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	SWITCH i
		CASE 0 // Camera start position for OFFICE 2
			
			IF (bIsBike)
				tempStruct[2].vLoc = <<-1358.6030, -473.2463, 32.1693>>
				tempStruct[2].vRot = <<-3.3136, -0.0000, 82.6857>>
			ELIF (bIsBig)
				tempStruct[2].vLoc = <<-1358.1569, -473.4932, 33.3569>>
				tempStruct[2].vRot = <<-15.8994, -0.0000, 69.3715>>
			ELSE
				tempStruct[2].vLoc = <<-1358.2449, -473.1768, 32.2119>>
				tempStruct[2].vRot = <<-5.5905, 0.0000, 74.1722>>
			ENDIF
			
		BREAK
		
		CASE 1 // Camera end position for OFFICE 2
			IF (bIsBike)
				tempStruct[2].vLoc = <<-1358.6030, -473.2463, 32.1693>>
				tempStruct[2].vRot = <<-3.3136, -0.0000, 82.6857>>
			ELIF (bIsBig)
				tempStruct[2].vLoc = <<-1358.1569, -473.4932, 33.3569>>
				tempStruct[2].vRot = <<-15.8994, -0.0000, 69.3715>>
			ELSE
				tempStruct[2].vLoc = <<-1358.2449, -473.1768, 32.2119>>
				tempStruct[2].vRot = <<-5.5905, 0.0000, 74.1722>>
			ENDIF
		BREAK
		
		CASE 2 // Camera null position for OFFICE 2 (Possibly not used)
			tempStruct[2].vLoc = <<-1358.2461, -473.1762, 32.0766>>
			tempStruct[2].vRot = <<-16.2814, 0.0000, 64.1577>>
		BREAK
		
		CASE 3 // Go To A position for OFFICE 2
			//tempStruct[2].vLoc = <<-1362.241, -472.036, 30.5957>>
			tempStruct[2].vLoc = <<-1359.0, -471.6, 30.5957>>
		BREAK
		
		CASE 4 // Go To Reverse position for OFFICE 2
			tempStruct[2].vLoc = <<-1362.241, -472.036, 30.5957>>
		BREAK
		
		CASE 5 // Go To B position for OFFICE 2
			//tempStruct[2].vLoc = <<-1357.485, -471.295, 30.5957>>
			tempStruct[2].vLoc = <<-1359.0, -471.6, 30.5957>>
		BREAK
		
		CASE 6 // Vehicle starting position for OFFICE 2
			tempStruct[2].vLoc = <<-1371.0309, -473.4054, 30.5939>>
			tempStruct[2].vRot = <<0.0, 0.0, 278.8534>>
		BREAK
		
		CASE 7 // Angled Area in elevator for OFFICE 2
			tempStruct[2].vLoc = <<-1358.031860,-471.419067,30.607216>>
			tempStruct[2].vRot = <<-1366.101685,-472.662262,35.016788>>
		BREAK
		
		CASE 8 // Left elevator door
			tempStruct[2].vLoc = <<-1365.925, -474.760, 30.600>>
			tempStruct[2].vRot = <<0.0, 0.0, 99.250>>
		BREAK
		
		CASE 9 // Right elevator door
			tempStruct[2].vLoc = <<-1366.572, -470.598, 30.600>>
			tempStruct[2].vRot = <<0.0, 0.0, 99.250>>
		BREAK
		
		CASE 10 // Left elevator door - open
			tempStruct[2].vLoc = <<-1365.592, -476.897, 30.600>>
			tempStruct[2].vRot = <<0.0, 0.0, 99.250>>
		BREAK
		
		CASE 11 // Right elevator door - open 
			tempStruct[2].vLoc = <<-1366.929, -468.485, 30.600>>
			tempStruct[2].vRot = <<0.0, 0.0, 99.250>>
		BREAK
	ENDSWITCH
	
	VECTOR vOffset = (tempStruct[2].vLoc - tempStruct[0].vLoc)
	vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, -tempStruct[0].vRot.z)
	vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, tempStruct[1].vRot.z)
	
	tempStruct[2].vLoc = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(tempStruct[1].vLoc,  0.0, vOffset)
	
	WHILE tempStruct[0].vRot.z > 180.0
		tempStruct[0].vRot.z -= 360.0
	ENDWHILE
	
	WHILE tempStruct[0].vRot.z < -180.0
		tempStruct[0].vRot.z += 360.0
	ENDWHILE
	
	WHILE tempStruct[1].vRot.z > 180.0
		tempStruct[1].vRot.z -= 360.0
	ENDWHILE
	
	WHILE tempStruct[1].vRot.z < -180.0
		tempStruct[1].vRot.z += 360.0
	ENDWHILE
	
	tempStruct[2].vRot.z = tempStruct[2].vRot.z + (tempStruct[1].vRot.z - tempStruct[0].vRot.z)
	
	WHILE tempStruct[2].vRot.z > 180.0
		tempStruct[2].vRot.z -= 360.0
	ENDWHILE
	
	WHILE tempStruct[2].vRot.z < -180.0
		tempStruct[2].vRot.z += 360.0
	ENDWHILE
	
	IF bRotation
		RETURN tempStruct[2].vRot
	ELSE
		RETURN tempStruct[2].vLoc
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC BOOL IsPlayerOnBikeOrQuad()
	VEHICLE_INDEX VehicleID
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)
			IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(VehicleID))
			OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(VehicleID))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL Private_Get_net_realty_3_scene(INT iBuildingID, STRUCT_net_realty_3_SCENE& scene	#IF ACTIVATE_PROPERTY_CS_DEBUG,	BOOL bIgnoreAssert = FALSE	#ENDIF	)
	SWITCH iBuildingID
		CASE 0	RETURN FALSE	BREAK
		
		//Luke Howard, 07-09-2013 (#1627154)
		CASE MP_PROPERTY_BUILDING_1	//High apt 1,2,3,4
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-801.0135, 332.7260, 85.4419>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<4.8393, -0.0098, -162.9744>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-798.7095, 333.4185, 85.4418>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<4.8393, -0.0098, -162.1292>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 30.0228
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-801.0135, 332.7260, 85.4419>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<4.8393, -0.0098, -162.9744>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 30.0228
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-796.1608, 323.2557, 84.7015>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-796.0895, 306.9656, 84.7015>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-787.4305, 336.9863, 84.7015>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-796.1099, 307.6792, 84.7022>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 0.0000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-800.4028, 313.7549, 84.7211>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-791.6549, 313.3977, 84.7060>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.6250
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK

		CASE MP_PROPERTY_BUILDING_51	//garage new 18
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<873.3696, -2235.5352, 32.9233>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<-5.3132, -0.0000, 26.6492>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<873.3696, -2235.5352, 32.9233>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<-16.2593, 0.0000, 26.6492>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 50.0000
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 6.5000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<864.1264, -2235.6619, 32.3054>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-16.9066, -0.0000, -56.8647>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<870.9843, -2229.7058, 29.5195>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<870.5140, -2235.2815, 29.6163>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<871.2320, -2227.6548, 29.5195>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<870.5704, -2232.8079, 29.5401>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 0.0000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<868.8420, -2231.7966, 30.0477>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<872.7557, -2232.1396, 29.8316>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 1.1250
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 1.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke H
		CASE MP_PROPERTY_BUILDING_6	//High apt 14,15
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-883.0992, -345.9912, 34.6601>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<6.3632, 0.0000, -160.3029>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-883.4265, -345.3335, 34.5787>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<6.3632, 0.0000, -161.1172>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 36.2740
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-935.5062, -393.9272, 40.0824>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-8.3063, 0.0000, -116.5835>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-884.9076, -346.0437, 33.5340>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-875.0308, -365.1419, 35.9402>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-899.6184, -340.5005, 33.5340>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-875.3135, -364.5881, 36.6168>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 27.3600
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-880.0560, -360.5660, 34.8627>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-875.9731, -358.4306, 34.8581>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 1.5000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 3.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke H
		CASE MP_PROPERTY_BUILDING_7	//High apt 16,17
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-618.2849, 59.2275, 44.2642>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<1.6448, 0.0000, 104.6055>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-617.4031, 59.1322, 44.2404>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<1.6448, -0.0000, 108.1057>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 37.2008
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-634.5964, 55.6069, 44.4266>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-5.5060, 0.0000, 87.8762>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-622.2662, 56.7073, 42.7323>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-638.7336, 56.6194, 42.9082>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-616.5451, 48.1717, 42.7443>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-637.3478, 56.6297, 42.8559>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 271.8300
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-630.1014, 60.9404, 42.7312>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-630.0441, 52.2495, 42.7250>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 4.6250
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 07-09-2013 (#1627154)
		CASE MP_PROPERTY_BUILDING_5	//High apt 12,13
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-38.8625, -621.3514, 35.3381>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<0.1933, 0.0000, -95.2019>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-39.0986, -621.3379, 35.3421>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<-0.9550, 0.0000, -93.2745>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 39.6494
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 6.5000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-58.6693, -582.4872, 37.4534>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-0.6088, 0.0000, -104.2720>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-37.2861, -620.4693, 34.0684>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-27.8079, -623.9610, 34.4169>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-36.3041, -612.9705, 34.0841>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-28.1169, -623.8500, 34.3976>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 70.0000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-34.9953, -625.4520, 34.1735>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-32.2612, -617.8603, 34.2148>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.1250
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 07-09-2013 (#1627154)
		CASE MP_PROPERTY_BUILDING_2	//High apt 5,6
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-292.2810, -993.2393, 24.2923>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<0.2725, 0.0000, -97.9788>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-294.4084, -992.5660, 24.2819>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<0.2725, 0.0000, -95.7095>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 37.9699
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 6.5000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-267.4284, -956.8759, 31.9557>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-8.2883, 0.0000, 30.0385>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-290.0483, -992.5299, 23.1368>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-276.0764, -997.3793, 24.2061>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-293.3160, -982.4184, 24.1999>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-276.4688, -997.2326, 24.1341>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 69.1800
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-283.6024, -998.5330, 23.1368>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-281.3513, -991.9869, 23.1368>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.3000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 6.9000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 07-09-2013 (#1627154)
		CASE MP_PROPERTY_BUILDING_4	//High apt 9,10,11
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-810.3807, -425.9025, 35.8310>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<4.4490, 0.0000, 139.9636>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-810.2816, -425.4070, 35.7966>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<3.2217, 0.0000, 139.1450>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 36.5979
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 7.0300
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-831.3160, -448.3651, 37.5418>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-6.2719, 0.0000, -44.8760>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-812.2617, -429.5818, 34.3929>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-825.9319, -440.7628, 35.6399>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-814.4553, -418.6729, 32.7103>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-825.6384, -440.5773, 35.6399>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 299.0000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-822.4520, -433.3539, 35.6609>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-818.7817, -440.3885, 35.6609>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 4.6000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 07-09-2013 (#1627154)
		CASE MP_PROPERTY_BUILDING_23	//Low apt 7
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-1602.8921, -440.3293, 38.6895>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<-1.0184, -0.0019, 157.3578>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-1602.2571, -439.3541, 38.7099>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<-1.0184, -0.0019, 163.0943>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 40.9081
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 6.5000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-1594.6119, -443.0864, 38.5771>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-12.3558, 0.0000, -89.3312>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 50.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.2500
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-1600.2324, -438.9460, 37.2166>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-1609.2898, -451.8584, 37.1881>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-1586.0852, -447.5367, 36.2120>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-1609.1001, -451.6195, 37.1823>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 325.3000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-1607.5859, -445.3638, 37.2258>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-1603.1171, -449.1536, 37.2260>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 4.5000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.1250
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		//Luke Howard, 07-09-2013 (#1627154)
		CASE MP_PROPERTY_BUILDING_3	//High apt 7,8
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-1450.2659, -509.8492, 32.0504>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<-0.4615, 0.0000, 45.9398>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-1449.8395, -510.3843, 32.0558>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<-0.4615, 0.0000, 47.4915>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 39.0991
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.3000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = <<-1447.6608, -513.3250, 31.9311>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = <<-0.6590, 0.0000, 49.2517>>
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 41.4552
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.0000
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-1449.3281, -514.3160, 30.8522>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-1459.0117, -498.6699, 31.7324>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-1444.2711, -525.8814, 30.8522>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-1458.6981, -499.1344, 31.6752>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 212.0000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-1453.2833, -502.1619, 31.1400>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-1458.3475, -505.6930, 31.1630>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.0000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 3.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_OFFICE_BUILDING_3
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 0, FALSE)
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 0, TRUE)
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 1, FALSE)
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 1, TRUE)
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 37.0000
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 2, FALSE)
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 2, TRUE)
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 37.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.0000
			

			IF IsPlayerOnBikeOrQuad()
				scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-143.4, -575.0, 31.9>>
			ELSE
				scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 3, FALSE)
			ENDIF
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 4, FALSE)
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 6, FALSE)
			VECTOR vTemp
			vTemp = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 6, TRUE)
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = vTemp.z
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 7, FALSE)
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = GET_OFFICE_GARAGE_VECTOR_AS_OFFSET(iBuildingID, 7, TRUE)
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 4.687500
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.409572
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 1.0000
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_OFFICE_BUILDING_1
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-1541.9135, -571.6335, 25.6177>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<2.9268, -0.8030, 39.3203>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-1541.1565, -571.0165, 25.6313>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<2.9268, -0.8030, 39.3203>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 23.6123
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = scene.mPans[net_realty_3_SCENE_PAN_descent].fFov
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = scene.mPans[net_realty_3_SCENE_PAN_descent].fShake
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-1547.0789, -566.9713, 24.7078>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-1556.4376, -554.6019, 27.2087>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-1541.8, -574.4, 24.7079>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-1554.2053, -557.1197, 26.0365>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 217.4663
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-1536.000, -564.000, 24.475>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-1548.700, -573.300, 27.575>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 5.0
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_OFFICE_BUILDING_2 
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-1398.7780, -476.1264, 32.2348>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<7.7275, -0.0000, 96.9099>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-1396.5002, -474.7093, 31.9051>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<7.7275, -0.0000, 96.9099>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 22.05
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = scene.mPans[net_realty_3_SCENE_PAN_descent].fFov
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = scene.mPans[net_realty_3_SCENE_PAN_descent].fShake
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-1410.2128, -476.8650, 32.2377>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-1419.4785, -481.8223, 32.7237>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-1385.7, -479.0, 30.7364>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-1416.8383, -480.1383, 32.7208>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 303.7230
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-1394.800, -472.400, 31.000>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-1393.800, -480.900, 34.000>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 5.0
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_OFFICE_BUILDING_4
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = <<-82.8416, -810.1617, 36.0567>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = <<4.9621, 0.0174, -7.4456>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = <<-81.7104, -810.3091, 36.0567>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = <<4.9621, 0.0174, -7.4456>>
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = 20.0
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.2500
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 10.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos 
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = 37.0000
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = 0.0000
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = <<-80.5756, -793.8742, 36.4201>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = <<-84.7520, -773.4540, 38.7651>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = <<-84.7, -815.5, 35.4947>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = <<-81.6358, -782.3251, 37.4860>>
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 195.0365
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = <<-73.3, -809.3, 39.6>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = <<-89.4, -808.5, 34.3>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 5.3
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 5.0
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.0000
			RETURN TRUE
		BREAK
		
		DEFAULT
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos = GET_GAMEPLAY_CAM_COORD()
			scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot = GET_GAMEPLAY_CAM_ROT()
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vPos = GET_GAMEPLAY_CAM_COORD()
			scene.mPans[net_realty_3_SCENE_PAN_descent].mEnd.vRot = GET_GAMEPLAY_CAM_ROT()
			scene.mPans[net_realty_3_SCENE_PAN_descent].fFov = GET_GAMEPLAY_CAM_FOV()
			scene.mPans[net_realty_3_SCENE_PAN_descent].fShake = 0.25
			scene.mPans[net_realty_3_SCENE_PAN_descent].fDuration = 6.500
			scene.bEnablePans[net_realty_3_SCENE_PAN_descent] = FALSE
			
			scene.fExitDelay = 0.0000
			
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].fRot = 0
			
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vPos = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos
			scene.mCuts[net_realty_3_SCENE_CUT_null].mCam.vRot = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vRot
			scene.mCuts[net_realty_3_SCENE_CUT_null].fFov = scene.mPans[net_realty_3_SCENE_PAN_descent].fFov
			scene.mCuts[net_realty_3_SCENE_CUT_null].fShake = scene.mPans[net_realty_3_SCENE_PAN_descent].fShake
			
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos = scene.mPlacers[net_realty_3_SCENE_PLACER_resetCoords].vPos + <<1.0, 0.0, 0.0>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos = scene.mMarkers[net_realty_3_SCENE_MARKER_gotoA].vPos + <<1.0, 0.0, 0.0>>
			scene.mMarkers[net_realty_3_SCENE_MARKER_gotoB].vPos = scene.mMarkers[net_realty_3_SCENE_MARKER_gotoREVERSE].vPos + <<1.0, 0.0, 0.0>>
			
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos0 = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos - <<5,5,0>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].vPos1 = scene.mPans[net_realty_3_SCENE_PAN_descent].mStart.vPos + <<5,5,0>>
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fWidth = 4.125000
			scene.mAngAreas[NET_REALTY_3_SCENE_ANGAREA_area0].fHeight = 4.125000
			
			#IF ACTIVATE_PROPERTY_CS_DEBUG
			IF NOT bIgnoreAssert
				TEXT_LABEL_63 str
				str = "Private_Get_NET_REALTY_3_SCENE() invalid ID "
				str += GET_STRING_FROM_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID), GET_LENGTH_OF_LITERAL_STRING("MP_PROPERTY_"), GET_LENGTH_OF_LITERAL_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID)))
				SCRIPT_ASSERT(str)
			ENDIF
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#IF ACTIVATE_PROPERTY_CS_DEBUG
USING "net_realty_3_scene_debug.sch"
#ENDIF

