
// ====================================================================================
// ====================================================================================
//
// Name:        net_MP_Radio.sch
// Description: Header for multiplayer Radio, found in the garages
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================

//USING "net_hud_activating.sch"
USING "globals.sch"  
// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"

USING "freemode_header.sch"

USING "rage_builtins.sch"
USING "net_spectator_cam.sch"
USING "net_realty_details.sch"
USING "context_control_public.sch"
USING "net_realty_new.sch"
USING "NET_PROP_ACT_COMMON.SCH"
USING "net_simple_interior.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
USING "profiler.sch"
#ENDIF

/// PURPOSE: List of MP radio client stage IDs
ENUM MP_RADIO_CLIENT_STAGE
	MP_RADIO_CLIENT_STAGE_INIT=0,
	MP_RADIO_CLIENT_STAGE_WALKING,
	MP_RADIO_CLIENT_STAGE_ACTIVATED,
	MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
ENDENUM

/// PURPOSE: List of MP radio server stage IDs
ENUM MP_RADIO_SERVER_STAGE
	MP_RADIO_SERVER_STAGE_INIT=0,
	MP_RADIO_SERVER_STAGE_ON,
	MP_RADIO_SERVER_STAGE_OFF,
	MP_RADIO_SERVER_HEIST_STAGE_ON,
	MP_RADIO_SERVER_HEIST_STAGE_OFF
ENDENUM

CONST_FLOAT MP_RADIO_MAX_VOLUME					0.0
CONST_FLOAT MP_RADIO_MIN_VOLUME					-36.0
CONST_FLOAT MP_RADIO_VOLUME_INCREMENT			0.5
CONST_INT MP_RADIO_STATION_CHANGE_WAIT_TIME		1000
CONST_INT MP_RADIO_INCREMENT_WAIT_TIME			200


CONST_INT MP_RADIO_MAX_NUM_APT_RADIO_UNITS		2

CONST_INT MP_RADIO_MAX_NUM_APT_RADIO_EMITTERS	3

//save bitset
CONST_INT MP_RADIO_SAVE_BS_RADIO_ON0									30
CONST_INT MP_RADIO_SAVE_BS_RADIO_ON1									31

//local iBitset
CONST_INT MP_RADIO_LOCAL_BS_UPDATE_BUTTONS								0
CONST_INT MP_RADIO_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET0			1
CONST_INT MP_RADIO_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET1			2
CONST_INT MP_RADIO_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET		3
CONST_INT MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0							4
CONST_INT MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON1							5
CONST_INT MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0							6
CONST_INT MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF1							7
CONST_INT MP_RADIO_LOCAL_BS_FORCE_STRIPPER_MUSIC						8
CONST_INT MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC						9
CONST_INT MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE							10
CONST_INT MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_ON0					11
CONST_INT MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0					12
CONST_INT MP_RADIO_LOCAL_BS_IN_RADIO_ANGLED_AREA						13 // Don't use this unless you know exactly what this is for.

CONST_INT RADIO_IDX_0 0
CONST_INT RADIO_IDX_1 1
CONST_INT RADIO_IDX_2 2
CONST_INT RADIO_IDX_3 3
CONST_INT RADIO_IDX_4 4
CONST_INT RADIO_IDX_COUNT 5

STRUCT LOCATE
	VECTOR v0
	VECTOR v1
ENDSTRUCT

/// PURPOSE: Holds all the local information for the apartment radios
STRUCT MP_RADIO_LOCAL_DATA_STRUCT
	INT iBitset
	
	INT iContextButtonIntention = NEW_CONTEXT_INTENTION
	INT iHeistContextButtonIntention = NEW_CONTEXT_INTENTION
	
	INT iCurrentStationID
	
	FLOAT fVolume = 0.0
	INT iVolumeTimer = 0
	
	CAMERA_INDEX cam
	
	//local server
	TIME_DATATYPE timeServerStationChange
	TIME_DATATYPE timeServerGarageStationChange
	INT iServerPlayerStagger = 0
	
	SCALEFORM_INSTRUCTIONAL_BUTTONS scaleformInstructionalButtons
	SCALEFORM_INDEX sfButton
	PLAYER_INDEX piApartmentOwner
	INT iWarehouseSize , iFactoryType
	OBJECT_INDEX RadioPropHangarPersonalQuarters, RadioPropHangarOffice
	
	INT iRadioStaggerCount = 0
	INT iRadioStaggerCountPreviousValue = 0

	INT iMaintainAmbientRadioStagger = 0
	INT iAlternateHeistRadioStagger = 0
	
	LOCATE radioLocates[RADIO_IDX_COUNT]
	MP_PROP_OFFSET_STRUCT radioAnimations[RADIO_IDX_COUNT]
	
	//#IF IS_DEBUG_BUILD
	// Added by Philip O'D on 29/05/2014
	// PURPOSE: Animation variables
	STRING sRadioAnimDictName 
	INT    iRadioAnimHeading 
	VECTOR vRadioAnimPosition 
	STRING sRadioAnimClip 
	STRING sRadioAnimClipEnter
	STRING sRadioAnimClipIdle
	STRING sRadioAnimClipButton
	STRING sRadioAnimClipExit
	INT    iRadioNetSceneID
	
	CONTROL_ACTION caRadioOnOffInput
	BOOL bOnYachtDeck
	INT iCurrentApartmentVariation = -1
	INT iYachtID
	INT iRadioActivatingSwitch
	INT iHeistRadioActivatingSwitch
	MP_RADIO_CLIENT_STAGE eStage = MP_RADIO_CLIENT_STAGE_INIT
	MP_RADIO_CLIENT_STAGE eHeistStage = MP_RADIO_CLIENT_STAGE_INIT
	INT iPreviousSavedStation
	INT iPreviousSavedGarageStation = -1
	
	BOOL bInCasinoApartment
	BOOL bInCasinoApartment2
ENDSTRUCT


//client iBitset
CONST_INT MP_RADIO_CLIENT_BS_IN_GARAGE						0
CONST_INT MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET				1
CONST_INT MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0				2
CONST_INT MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON1				3
CONST_INT MP_RADIO_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON		4
CONST_INT MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION 				5
CONST_INT MP_RADIO_CLIENT_BS_TRIGGER_HEIST_RADIO			6
CONST_INT MP_RADIO_CLIENT_BS_HEIST_RADIO_OFF				7
CONST_INT MP_RADIO_CLIENT_BS_PLAYING_ANIM  	 				8
CONST_INT MP_RADIO_CLIENT_BS_MOVING_TO_RADIO 				9
CONST_INT MP_RADIO_CLIENT_BS_ENTER_ANIM						10
/* BS 11 REMOVED */
CONST_INT MP_RADIO_CLIENT_BS_EXIT_ANIM 						12
CONST_INT MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO 		13
/* BS 14 REMOVED */
CONST_INT MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE			15
CONST_INT MP_RADIO_CLIENT_BS_APARTMENT_VARIATION_CHANGE		16
// Packed int input bits
CONST_INT MP_RADIO_CLIENT_BS_INPUT_START					17
CONST_INT MP_RADIO_CLIENT_BS_INPUT_B1						18
CONST_INT MP_RADIO_CLIENT_BS_INPUT_B2						19
CONST_INT MP_RADIO_CLIENT_BS_INPUT_B3						20
CONST_INT MP_RADIO_CLIENT_BS_INPUT_B4						21
CONST_INT MP_RADIO_CLIENT_BS_INPUT_B5						22
CONST_INT MP_RADIO_CLIENT_BS_INPUT_END						23
// Packed int heist input bits
CONST_INT MP_RADIO_CLIENT_BS_HEIST_INPUT_START				24
CONST_INT MP_RADIO_CLIENT_BS_HEIST_INPUT_B1					25
CONST_INT MP_RADIO_CLIENT_BS_HEIST_INPUT_B2					26
CONST_INT MP_RADIO_CLIENT_BS_HEIST_INPUT_B3					27
CONST_INT MP_RADIO_CLIENT_BS_HEIST_INPUT_B4					28
CONST_INT MP_RADIO_CLIENT_BS_HEIST_INPUT_B5					29
CONST_INT MP_RADIO_CLIENT_BS_HESIT_INPUT_END				30

//Input bits (index into packed input bits starting at: MP_RADIO_CLIENT_BS_INPUT_START and MP_RADIO_CLIENT_BS_HEIST_INPUT_START in client bitset
CONST_INT MP_RADIO_INPUT_POWER_ON						0
CONST_INT MP_RADIO_INPUT_POWER_OFF						1
CONST_INT MP_RADIO_INPUT_NEXT_CHANNEL					2
CONST_INT MP_RADIO_INPUT_PREVIOUS_CHANNEL				3
CONST_INT MP_RADIO_INPUT_HEIST_POWER_ON					4
CONST_INT MP_RADIO_INPUT_HEIST_POWER_OFF				5
#IF FEATURE_TUNER
CONST_INT MP_RADIO_INPUT_AUDIO_PLAYER					6
#ENDIF

PROC SET_INPUT_BITS(INT &iBs, INT iBits)
	SET_BITS_IN_RANGE(iBs, MP_RADIO_CLIENT_BS_INPUT_START, MP_RADIO_CLIENT_BS_INPUT_END, iBits)
ENDPROC
PROC SET_HEIST_INPUT_BITS(INT& iBs, INT iBits)
	SET_BITS_IN_RANGE(iBs, MP_RADIO_CLIENT_BS_HEIST_INPUT_START, MP_RADIO_CLIENT_BS_HESIT_INPUT_END, iBits)
ENDPROC

FUNC INT GET_INPUT_BITS(INT iBs)
	RETURN GET_BITS_IN_RANGE(iBs, MP_RADIO_CLIENT_BS_INPUT_START, MP_RADIO_CLIENT_BS_INPUT_END)
ENDFUNC

FUNC INT GET_HEIST_INPUT_BITS(INT iBs)
	RETURN GET_BITS_IN_RANGE(iBs, MP_RADIO_CLIENT_BS_HEIST_INPUT_START, MP_RADIO_CLIENT_BS_HESIT_INPUT_END)
ENDFUNC

/// PURPOSE: Holds all the client broadcast information for the apartment radios
STRUCT MP_RADIO_CLIENT_DATA_STRUCT
	// Verified
	INT iBitset
	// Verified
	INT iTimeOfInputStationID = 0
	// Unsure
	INT iActivatedRadio = -1
ENDSTRUCT

/// PURPOSE: Holds all the server broadcast information for the apartment radios
STRUCT MP_RADIO_SERVER_DATA_STRUCT
	INT iBitset
	
	INT iCurrentStationID
	INT iCurrentGarageStationID = 0
	
	INT iYachtID
	BOOL bStaticEmitterReserved = FALSE
	
	NETWORK_INDEX niYachtBarBB
	NETWORK_INDEX niYachtBedRoomBB
	NETWORK_INDEX niYachtBedRoom2BB
	NETWORK_INDEX niYachtBedRoom3BB
	
	VECTOR vPropYachtBarBBPosition 
	VECTOR vPropYachtBedRoomBBPosition
	VECTOR vPropYachtBedRoom2BBPosition
	VECTOR vPropYachtBedRoom3BBPosition
	
	NETWORK_INDEX niBedRoomBB
	NETWORK_INDEX niLivingRoomBB
	NETWORK_INDEX niHeistRoomBB
	
	NETWORK_INDEX niYachtExtJacuzziBB
	NETWORK_INDEX niYachtExtEntranceBB
	NETWORK_INDEX niYachtExtTopDeckBB
	NETWORK_INDEX niYachtExtFrontBB
				
	
	VECTOR vPropHeistRoomBBPosition
	VECTOR vPropBedRoomBBPosition
	VECTOR vPropLivingRoomBBPosition
	
	VECTOR vPropYachtExtJacuzziBBPosition
	VECTOR vPropYachtExtEntranceBBPosition
	VECTOR vPropYachtExtTopDeckBBPosition
	VECTOR vPropYachtExtFrontBBPosition
	
	BOOL bOnYachtDeck
	
	MP_RADIO_SERVER_STAGE eStage
	MP_RADIO_SERVER_STAGE eGarageStage = MP_RADIO_SERVER_STAGE_INIT

	MP_RADIO_SERVER_STAGE eHeistRadioStage = MP_RADIO_SERVER_STAGE_INIT
	INT iWarehouseSize ,iFactoryType
ENDSTRUCT

#IF IS_DEBUG_BUILD
	
	/// PURPOSE:
	///    Gets the supplied radio client stage debug name
	/// PARAMS:
	///    eStage - client stage to get the name of
	/// RETURNS:
	///    radio client stage name as a STRING
	FUNC STRING GET_MP_RADIO_CLIENT_STAGE_NAME(MP_RADIO_CLIENT_STAGE eStage)
		SWITCH eStage
			CASE MP_RADIO_CLIENT_STAGE_INIT
				RETURN "MP_RADIO_CLIENT_STAGE_INIT"
			BREAK
			CASE MP_RADIO_CLIENT_STAGE_WALKING
				RETURN "MP_RADIO_CLIENT_STAGE_WALKING"
			BREAK
			CASE MP_RADIO_CLIENT_STAGE_ACTIVATED
				RETURN "MP_RADIO_CLIENT_STAGE_ACTIVATED"
			BREAK
			CASE MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
				RETURN "MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED"
			BREAK
			
		ENDSWITCH
		RETURN "INVALID_CLIENT_STAGE"
	ENDFUNC
	
	/// PURPOSE:
	///    Gets the supplied radio server stage debug name
	/// PARAMS:
	///    eStage - server stage to get the name of
	/// RETURNS:
	///    radio server stage name as a STRING
	FUNC STRING GET_MP_RADIO_SERVER_STAGE_NAME(MP_RADIO_SERVER_STAGE eStage)
		SWITCH eStage
			CASE MP_RADIO_SERVER_STAGE_INIT
				RETURN "MP_RADIO_CLIENT_STAGE_INIT"
			BREAK
			CASE MP_RADIO_SERVER_STAGE_ON
				RETURN "MP_RADIO_SERVER_STAGE_ON"
			BREAK
			CASE MP_RADIO_SERVER_STAGE_OFF
				RETURN "MP_RADIO_SERVER_STAGE_OFF"
			BREAK
			CASE MP_RADIO_SERVER_HEIST_STAGE_ON
				RETURN "MP_RADIO_SERVER_HEIST_STAGE_ON"
			BREAK
			CASE MP_RADIO_SERVER_HEIST_STAGE_OFF
				RETURN "MP_RADIO_SERVER_HEIST_STAGE_OFF"
			BREAK
		ENDSWITCH
		RETURN "INVALID_SERVER_STAGE"
	ENDFUNC
	
#ENDIF

/// PURPOSE:
///    Returns the number of radios in the supplied property
/// PARAMS:
///    MPRadioClient - used to check if the player is in a garage
///    property - property ID to get the number of radios of
/// RETURNS:
///    number of radios as an INT
FUNC INT GET_MP_RADIO_NUMBER_OF_UNITS(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_PROPERTY_STRUCT &property)
	IF IS_PROPERTY_STILT_APARTMENT(PROPERTY.iIndex)
		RETURN 3
	ENDIF
	IF IS_PROPERTY_CUSTOM_APARTMENT(PROPERTY.iIndex)
		RETURN 2
	ENDIF
	IF IS_PROPERTY_YACHT_APARTMENT(PROPERTY.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
		RETURN 5
	ENDIF
	IF MPRadioLocal.bOnYachtDeck
		RETURN 3
	ENDIF
	IF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
		RETURN 2
	ENDIF
	IF IS_PROPERTY_OFFICE(PROPERTY.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR MPRadioLocal.bInCasinoApartment
	OR MPRadioLocal.bInCasinoApartment2
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	OR IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	OR IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
	#ENDIF
		RETURN 1
	ENDIF
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN 2
	ENDIF
	#IF FEATURE_FIXER
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		RETURN 3
	ENDIF
	#ENDIF
	
	IF property.iGarageSize = PROP_GARAGE_SIZE_10
		IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
			RETURN (MP_RADIO_MAX_NUM_APT_RADIO_UNITS-1)
		ENDIF
	ENDIF
	
	RETURN 2
ENDFUNC

/// PURPOSE:
///    Returns the number of audio emitters in the supplied property
/// PARAMS:
///    property - property ID to get the number of emitters of
/// RETURNS:
///    number of emitters as an INT
FUNC INT GET_MP_RADIO_NUMBER_OF_EMITTERS(MP_PROPERTY_STRUCT &property)
	
	IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
//		#IF FEATURE_RADIO_ANIMS
//			RETURN 3
//		#ENDIF
//		
//		#IF NOT FEATURE_RADIO_ANIMS
//			RETURN 2
//		#ENDIF
		RETURN 3
	ELSE
		IF property.iGarageSize = PROP_GARAGE_SIZE_10
			RETURN MP_RADIO_MAX_NUM_APT_RADIO_EMITTERS
		ENDIF
	ENDIF
	
	RETURN 1
ENDFUNC

/// PURPOSE:
///    Gets the server stage the client requires based on if they are in the garage
/// PARAMS:
///    MPRadioServer - server data struct to get stage from
///    MPRadioClient - client data struct to query if is in a garage
/// RETURNS:
///    server stage as a MP_RADIO_SERVER_STAGE
FUNC MP_RADIO_SERVER_STAGE GET_MP_RADIO_SERVER_STAGE(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient)
	
	IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		RETURN MPRadioServer.eGarageStage
	ELSE
		RETURN MPRadioServer.eStage
	ENDIF
	
ENDFUNC

//#IF FEATURE_HEIST_PLANNING
/// PURPOSE:
///    Gets the Heist Radio server stage the client requires based on if they are in the garage
/// PARAMS:
///    MPRadioServer - server data struct to get stage from
///    MPRadioClient - client data struct to query if is in a garage
/// RETURNS:
///    server stage as a MP_RADIO_SERVER_STAGE
FUNC MP_RADIO_SERVER_STAGE GET_MP_HEIST_RADIO_SERVER_STAGE(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer)
//	println("POD: RADIO: GET_MP_HEIST_RADIO_SERVER_STAGE: eHeistRadioStage: ", MPRadioServer.eHeistRadioStage)
	RETURN MPRadioServer.eHeistRadioStage
	
ENDFUNC
//#ENDIF

/// PURPOSE:
///    Gets the server current station the client requires based on if they are in the garage
/// PARAMS:
///    MPRadioServer - server data struct to get station from
///    MPRadioClient - client data struct to query if is in a garage
/// RETURNS:
///    server station as an INT
FUNC INT GET_MP_RADIO_SERVER_CURRENT_STATION(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient)
	
	IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		RETURN MPRadioServer.iCurrentGarageStationID
	ELSE
		RETURN MPRadioServer.iCurrentStationID
	ENDIF
	
ENDFUNC

//#IF FEATURE_HEIST_PLANNING
/// PURPOSE:
///    Server sets the radio server stage
/// PARAMS:
///    MPRadioServer - server data struct to set stage of
///    eStage - desired stage
///    bGarage - whether to set the stage for the garage or not
PROC SET_MP_HEIST_RADIO_SERVER_STAGE(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_SERVER_STAGE eStage, BOOL bGarage)
	IF bGarage
		#IF IS_DEBUG_BUILD
			IF MPRadioServer.eGarageStage <> eStage
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === setting heist garage stage to ", GET_MP_RADIO_SERVER_STAGE_NAME(eStage))
			ENDIF
		#ENDIF
		MPRadioServer.eGarageStage = eStage
	ELSE
		#IF IS_DEBUG_BUILD
			IF MPRadioServer.eHeistRadioStage <> eStage
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === setting heist stage to ", GET_MP_RADIO_SERVER_STAGE_NAME(eStage))
			ENDIF
		#ENDIF
		MPRadioServer.eHeistRadioStage = eStage
	ENDIF
ENDPROC

/// PURPOSE:
///    Server sets the radio server stage
/// PARAMS:
///    MPRadioServer - server data struct to set stage of
///    eStage - desired stage
///    bGarage - whether to set the stage for the garage or not
PROC SET_MP_RADIO_SERVER_STAGE(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_SERVER_STAGE eStage, BOOL bGarage)
	IF bGarage
		#IF IS_DEBUG_BUILD
			IF MPRadioServer.eGarageStage <> eStage
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === setting garage stage to ", GET_MP_RADIO_SERVER_STAGE_NAME(eStage))
			ENDIF
		#ENDIF
		MPRadioServer.eGarageStage = eStage
	ELSE
		#IF IS_DEBUG_BUILD
			IF MPRadioServer.eStage <> eStage
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === setting stage to ", GET_MP_RADIO_SERVER_STAGE_NAME(eStage))
			ENDIF
		#ENDIF
		MPRadioServer.eStage = eStage
	ENDIF
ENDPROC

/// PURPOSE:
///    Clients sets the radio client stage
/// PARAMS:
///    MPRadioClient - client data struct to set stage of
///    eStage - desired stage
PROC SET_MP_RADIO_CLIENT_STAGE(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_CLIENT_STAGE eStage)
	#IF IS_DEBUG_BUILD
		IF MPRadioLocal.eStage <> eStage
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === setting stage to ", GET_MP_RADIO_CLIENT_STAGE_NAME(eStage))
		ENDIF
	#ENDIF
	MPRadioLocal.eStage = eStage
ENDPROC

//#IF FEATURE_HEIST_PLANNING
/// PURPOSE:
///    Clients sets the radio client stage
/// PARAMS:
///    MPRadioClient - client data struct to set stage of
///    eStage - desired stage
PROC SET_MP_HEIST_RADIO_CLIENT_STAGE(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_CLIENT_STAGE eStage)
	#IF IS_DEBUG_BUILD
		IF MPRadioLocal.eHeistStage <> eStage
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === setting heist stage to ", GET_MP_RADIO_CLIENT_STAGE_NAME(eStage))
		ENDIF
	#ENDIF
	MPRadioLocal.eHeistStage = eStage
ENDPROC
//#ENDIF
/// PURPOSE:
///    Returns the position of the activated radio in the supplied property
/// PARAMS:
///    MPRadioClient - client data struct to get the current activated radio from
///    property - property to get coordinates in
/// RETURNS:
///    radio position as a VECTOR
FUNC VECTOR GET_MP_RADIO_VECTOR(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_PROPERTY_STRUCT &property)	
	MP_PROP_OFFSET_STRUCT tempProp
	IF NOT MPRadioLocal.bOnYachtDeck
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					RETURN <<176.2662, -999.4290, -98.8624>>
				ELSE
					RETURN <<263.3552, -994.6862, -98.9227>>
				ENDIF
			BREAK
			CASE PROP_GARAGE_SIZE_6
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					RETURN <<201.9062, -993.8659, -98.9293>>
				ELSE
					RETURN <<341.5449, -1001.0634, -99.0576>>
				ENDIF
			BREAK
			CASE PROP_GARAGE_SIZE_10
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					RETURN <<230.6372, -974.8629, -98.8201>>
				ELSE
					SWITCH MPRadioClient.iActivatedRadio
						CASE 0
							IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
								GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_RADIO_PROP0, tempProp, PROPERTY_BUS_HIGH_APT_1)
							ELSE
								GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_RADIO_PROP0, tempProp)
							ENDIF
							RETURN tempProp.vLoc
						BREAK
						CASE 1
							IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
								GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_RADIO_PROP1, tempProp, PROPERTY_BUS_HIGH_APT_1)
							ELSE
								GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_RADIO_PROP1, tempProp)
							ENDIF
							RETURN tempProp.vLoc
						BREAK
					ENDSWITCH
				ENDIF
			BREAK
		ENDSWITCH
		
	ELSE
		SWITCH MPRadioClient.iActivatedRadio
			CASE 0
				GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO_PROP0, tempProp)
				RETURN tempProp.vLoc
			BREAK
			CASE 1
				GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO_PROP1, tempProp)
				RETURN tempProp.vLoc
			BREAK
			CASE 2
				GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO_PROP2, tempProp)
				RETURN tempProp.vLoc
			BREAK
		ENDSWITCH
	ENDIF
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///     Cleans up radio activation
/// PARAMS:
///    MPRadioLocal - local radio data struct
PROC TIDYUP_MP_RADIO(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO LOCAL === Tidyup")
	#ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	
	IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE)
		STOP_AUDIO_SCENE("MP_APT_STRIPPER_SCENE")
		CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE)
	ENDIF
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPRadioLocal.sfButton)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
	
	ENABLE_INTERACTION_MENU()
	ENABLE_WEAPON_SWAP()
	Enable_MP_Comms()
	
ENDPROC

/// PURPOSE:
///    Cleans up the entire radio header
/// PARAMS:
///    MPRadioLocal - local radio data struct
PROC CLEANUP_MP_RADIO(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)
	DEBUG_PRINTCALLSTACK()
	CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO === Cleanup")
	g_bInRadioLocate = FALSE
	g_bRadioActTriggered = FALSE
	TIDYUP_MP_RADIO(MPRadioLocal)
	CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
	//LOCAL
	RELEASE_CONTEXT_INTENTION(MPRadioLocal.iContextButtonIntention)
	RELEASE_CONTEXT_INTENTION(MPRadioLocal.iHeistContextButtonIntention)
	MPRadioLocal.iBitset = 0
	MPRadioLocal.iCurrentStationID = 0
	
	//CLIENT
	MPRadioClient.iBitSet = 0
	
	MPRadioClient.iActivatedRadio = -1
	MPRadioLocal.eStage = MP_RADIO_CLIENT_STAGE_INIT
	MPRadioLocal.eHeistStage = MP_RADIO_CLIENT_STAGE_INIT
	
	MPRadioLocal.iRadioActivatingSwitch = 0
	MPRadioLocal.iHeistRadioActivatingSwitch = 0
	
	//Other prop is grabbed from the world...
	IF DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarPersonalQuarters)
		SET_STATIC_EMITTER_ENABLED("se_xm_int_02_bedroom_radio", FALSE)
		DELETE_OBJECT(MPRadioLocal.RadioPropHangarPersonalQuarters)
		SET_OBJECT_AS_NO_LONGER_NEEDED(MPRadioLocal.RadioPropHangarPersonalQuarters)
		MPRadioLocal.RadioPropHangarPersonalQuarters = null
	ENDIF
ENDPROC

/// PURPOSE:
///    Private- gets a garage radio audio emitter ID
/// PARAMS:
///    iGarageSize - garage type to get the emitter for
/// RETURNS:
///    radio audio emitter ID as a STRING
FUNC STRING PRV_MP_RADIO_GET_GARAGE_EMITTER_NAME(INT iGarageSize)
	SWITCH iGarageSize
		CASE PROP_GARAGE_SIZE_2
			RETURN "SE_MP_GARAGE_S_RADIO"
		BREAK
		CASE PROP_GARAGE_SIZE_6
			RETURN "SE_MP_GARAGE_M_RADIO"
		BREAK
		CASE PROP_GARAGE_SIZE_10
			RETURN "SE_MP_GARAGE_L_RADIO"
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Private - gets an apartment radio audio emitter ID
/// PARAMS:
///    iGarageSize - garage type to get the emitter for
///    iPropertyIndex - property to get the emitter for
///    iEmitterID - ID of which emitter in the property to get
/// RETURNS:
///    radio audio emitter ID as a STRING
FUNC STRING PRV_MP_RADIO_GET_APARTMENT_EMITTER_NAME(INT iGarageSize, INT iPropertyIndex, INT iEmitterID)
	SWITCH iGarageSize
		CASE PROP_GARAGE_SIZE_2
			RETURN "SE_MP_AP_RAD_v_studio_lo_living"
		BREAK
		CASE PROP_GARAGE_SIZE_6
			RETURN "SE_MP_AP_RAD_v_apart_midspaz_lounge"
		BREAK
		CASE PROP_GARAGE_SIZE_10
			SWITCH iPropertyIndex
			
				//building #1
				CASE PROPERTY_HIGH_APT_1
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_1_1" BREAK
						CASE 1 RETURN "SE_MP_APT_1_2" BREAK
						CASE 2 RETURN "SE_MP_APT_1_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_2
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_2_1" BREAK
						CASE 1 RETURN "SE_MP_APT_2_2" BREAK
						CASE 2 RETURN "SE_MP_APT_2_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_3
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_3_1" BREAK
						CASE 1 RETURN "SE_MP_APT_3_2" BREAK
						CASE 2 RETURN "SE_MP_APT_3_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_4
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_4_1" BREAK
						CASE 1 RETURN "SE_MP_APT_4_2" BREAK
						CASE 2 RETURN "SE_MP_APT_4_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_BUS_HIGH_APT_1
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_NEW_4_1" BREAK
						CASE 1 RETURN "SE_MP_APT_NEW_4_2" BREAK
						CASE 2 RETURN "SE_MP_APT_NEW_4_3" BREAK
					ENDSWITCH
				BREAK
				
				
				//building #2
				CASE PROPERTY_HIGH_APT_5
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_5_1" BREAK
						CASE 1 RETURN "SE_MP_APT_5_2" BREAK
						CASE 2 RETURN "SE_MP_APT_5_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_6
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_6_1" BREAK
						CASE 1 RETURN "SE_MP_APT_6_2" BREAK
						CASE 2 RETURN "SE_MP_APT_6_3" BREAK
					ENDSWITCH
				BREAK
				
				//building #3
				CASE PROPERTY_HIGH_APT_7
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_7_1" BREAK
						CASE 1 RETURN "SE_MP_APT_7_2" BREAK
						CASE 2 RETURN "SE_MP_APT_7_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_8
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_8_1" BREAK
						CASE 1 RETURN "SE_MP_APT_8_2" BREAK
						CASE 2 RETURN "SE_MP_APT_8_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_BUS_HIGH_APT_2
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_NEW_1_1" BREAK
						CASE 1 RETURN "SE_MP_APT_NEW_1_2" BREAK
						CASE 2 RETURN "SE_MP_APT_NEW_1_3" BREAK
					ENDSWITCH
				BREAK
				
				//building #4
				CASE PROPERTY_HIGH_APT_9
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_9_1" BREAK
						CASE 1 RETURN "SE_MP_APT_9_2" BREAK
						CASE 2 RETURN "SE_MP_APT_9_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_10
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_10_1" BREAK
						CASE 1 RETURN "SE_MP_APT_10_2" BREAK
						CASE 2 RETURN "SE_MP_APT_10_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_11
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_11_1" BREAK
						CASE 1 RETURN "SE_MP_APT_11_2" BREAK
						CASE 2 RETURN "SE_MP_APT_11_3" BREAK
					ENDSWITCH
				BREAK
				
				//Building #5
				CASE PROPERTY_HIGH_APT_12
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_12_1" BREAK
						CASE 1 RETURN "SE_MP_APT_12_2" BREAK
						CASE 2 RETURN "SE_MP_APT_12_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_13
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_13_1" BREAK
						CASE 1 RETURN "SE_MP_APT_13_2" BREAK
						CASE 2 RETURN "SE_MP_APT_13_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_BUS_HIGH_APT_5
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_NEW_5_1" BREAK
						CASE 1 RETURN "SE_MP_APT_NEW_5_2" BREAK
						CASE 2 RETURN "SE_MP_APT_NEW_5_3" BREAK
					ENDSWITCH
				BREAK
				
				//Building #6
				CASE PROPERTY_HIGH_APT_14
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_14_1" BREAK
						CASE 1 RETURN "SE_MP_APT_14_2" BREAK
						CASE 2 RETURN "SE_MP_APT_14_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_15
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_15_1" BREAK
						CASE 1 RETURN "SE_MP_APT_15_2" BREAK
						CASE 2 RETURN "SE_MP_APT_15_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_BUS_HIGH_APT_3
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_NEW_2_1" BREAK
						CASE 1 RETURN "SE_MP_APT_NEW_2_2" BREAK
						CASE 2 RETURN "SE_MP_APT_NEW_2_3" BREAK
					ENDSWITCH
				BREAK
				
				//Building #7
				CASE PROPERTY_HIGH_APT_16
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_16_1" BREAK
						CASE 1 RETURN "SE_MP_APT_16_2" BREAK
						CASE 2 RETURN "SE_MP_APT_16_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_HIGH_APT_17
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_17_1" BREAK
						CASE 1 RETURN "SE_MP_APT_17_2" BREAK
						CASE 2 RETURN "SE_MP_APT_17_3" BREAK
					ENDSWITCH
				BREAK
				CASE PROPERTY_BUS_HIGH_APT_4
					SWITCH iEmitterID
						CASE 0 RETURN "SE_MP_APT_NEW_3_1" BREAK
						CASE 1 RETURN "SE_MP_APT_NEW_3_2" BREAK
						CASE 2 RETURN "SE_MP_APT_NEW_3_3" BREAK
					ENDSWITCH
				BREAK
				
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns the radio audio emitter name for the property and emitter ID supplied
/// PARAMS:
///    MPRadioClient - client data struct to query if in garage
///    property - property to get the emitter name of
///    iEmitterID - emitter ID of which radio to get
/// RETURNS:
///    emitter name as a STRING
FUNC STRING GET_RADIO_EMITTER_NAME(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_PROPERTY_STRUCT &property, INT iEmitterID)
	IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		RETURN PRV_MP_RADIO_GET_GARAGE_EMITTER_NAME(property.iGarageSize)
	ELSE
		RETURN PRV_MP_RADIO_GET_APARTMENT_EMITTER_NAME(property.iGarageSize, property.iIndex, iEmitterID)
	ENDIF
	
	RETURN ""
ENDFUNC

FUNC STRING GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, INT iProperty, INT iRoomID = 1, BOOL bWarehouse = FALSE, INT iWarehouseSize = 0 , BOOL bFactory = FALSE, INT iFactoryGoodsCate = 0, BOOL bIEGarage = FALSE, INT iEmitterId = 0)
	STRING returnString = ""
	
	IF bIEGarage
		returnString = "DLC_IE_Warehouse_Radio_01"
	ENDIF
	
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		SWITCH iEmitterId
			CASE 0	RETURN "se_hei4_int_sub_mess_radio"
		ENDSWITCH
	ENDIF
	
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		SWITCH iEmitterId
			CASE 0	RETURN "SE_tr_tuner_mod_garage_Radio_01"
		ENDSWITCH
	ENDIF
	
	IF IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		SWITCH iEmitterId
			CASE 0	RETURN "SE_tr_tuner_car_meet_Meet_rm_Music_Takeover"
		ENDSWITCH
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		SWITCH iEmitterId
			CASE 0	RETURN "LOS_SANTOS_ARMENIAN_CAR_DEALERSHIP"
		ENDSWITCH
	ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		SWITCH iEmitterId
			CASE 0	RETURN "SE_sf_dlc_office_sec_Player_Office"
			CASE 1	RETURN "SE_sf_dlc_office_sec_hacker_01"
			CASE 2	RETURN "SE_sf_dlc_office_sec_apartment_01"
		ENDSWITCH
	ENDIF
	#ENDIF
			
	IF MPRadioLocal.bInCasinoApartment
		SWITCH iEmitterId
			CASE 0	RETURN "se_vw_dlc_casino_apart_Apart_Default_Room_radio"
			CASE 1	RETURN "se_vw_dlc_casino_apart_Apart_Lounge_Room_radio"
		ENDSWITCH
	ENDIF
	
	IF MPRadioLocal.bInCasinoApartment2
		SWITCH iEmitterId
			CASE 0	RETURN "se_vw_dlc_casino_apart_Apart_Arcade_Room_radio"
		ENDSWITCH
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	SWITCH iEmitterID
		CASE 0 RETURN "se_xm_int_02_bedroom_radio"
		CASE 1 RETURN "se_xm_int_02_lounge_radio"
	ENDSWITCH
	ENDIF
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		SWITCH iRoomID
			CASE 1 // Living Quarters
				SWITCH iEmitterId
					CASE 0
						returnString = "SE_DLC_SM_Hangar_Radio_Living_Quarters_01"
					BREAK
					CASE 1
						returnString = "SE_DLC_SM_Hangar_Radio_Living_Quarters_02"
					BREAK
				ENDSWITCH
			BREAK
			CASE 2 // Office 
				SWITCH iEmitterId
					CASE 0
						returnString = "SE_DLC_SM_Hangar_Radio_Office_01"
					BREAK
					CASE 1
						returnString = "SE_DLC_SM_Hangar_Radio_Office_02"
					BREAK
					CASE 2
						returnString = "SE_DLC_SM_Hangar_Radio_Office_03"
					BREAK
					CASE 3
						returnString = "SE_DLC_SM_Hangar_Radio_Office_04"
					BREAK
				ENDSWITCH
			BREAK
			CASE 3 // Mod shop	
				SWITCH iEmitterId
					CASE 0
						returnString = "SE_DLC_SM_Hangar_Radio_Mechanic"
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	IF bFactory
		IF iFactoryGoodsCate = 		ENUM_TO_INT(FACTORY_TYPE_METH)
			returnString = "SE_DLC_Biker_Meth_Warehouse_Radio"
		ELIF iFactoryGoodsCate = 	ENUM_TO_INT(FACTORY_TYPE_WEED)
			returnString = "SE_DLC_Biker_Weed_Warehouse_Radio"
		ELIF iFactoryGoodsCate =	ENUM_TO_INT(FACTORY_TYPE_CRACK)
			returnString = "SE_DLC_Biker_Crack_Warehouse_Radio"
		ELIF iFactoryGoodsCate =	ENUM_TO_INT(FACTORY_TYPE_FAKE_MONEY)
			returnString = "SE_DLC_Biker_Cash_Warehouse_Radio"
		ELIF iFactoryGoodsCate =	ENUM_TO_INT(FACTORY_TYPE_FAKE_IDS)
			returnString = "SE_DLC_Biker_FakeID_Warehouse_Radio"
		ENDIF
	ENDIF
	
	IF bWarehouse
		IF iWarehouseSize = ciSMALL_WAREHOUSE_CRATE_CAPACITY
			returnString = "SE_EXEC_WH_S_RADIO"
		ELIF iWarehouseSize = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY 
			returnString = "SE_EXEC_WH_M_RADIO"
		ELIF iWarehouseSize = ciLARGE_WAREHOUSE_CRATE_CAPACITY
			returnString = "SE_EXEC_WH_L_RADIO"
		ENDIF
	ENDIF
	SWITCH iRoomID
		CASE 1 // Living Room
			SWITCH GET_BASE_PROPERTY_FROM_PROPERTY(iProperty)
				CASE PROPERTY_STILT_APT_5_BASE_A
					returnString = "SE_DLC_APT_Stilts_A_Living_Room"
				BREAK
				CASE PROPERTY_STILT_APT_1_BASE_B
					returnString = "SE_DLC_APT_Stilts_B_Living_Room"
				BREAK
				CASE PROPERTY_CUSTOM_APT_1_BASE
					returnString = "SE_DLC_APT_Custom_Living_Room"
				BREAK
				CASE PROPERTY_OFFICE_2_BASE
					SWITCH g_iCurrentPropertyVariation 
						CASE PROPERTY_VARIATION_1
							returnString = "SE_ex_int_office_01a_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_2
							returnString = "SE_ex_int_office_01b_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_3
							returnString = "SE_ex_int_office_01c_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_4
							returnString = "SE_ex_int_office_02a_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_5
							returnString = "SE_ex_int_office_02b_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_6
							returnString = "SE_ex_int_office_02c_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_7
							returnString = "SE_ex_int_office_03a_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_8
							returnString = "SE_ex_int_office_03b_Radio_01"
						BREAK
						CASE PROPERTY_VARIATION_9
							returnString = "SE_ex_int_office_03c_Radio_01"
						BREAK
					ENDSWITCH
					
				BREAK
				
			ENDSWITCH
		BREAK
		CASE 2 // Bedroom
			SWITCH GET_BASE_PROPERTY_FROM_PROPERTY(iProperty)
				CASE PROPERTY_STILT_APT_5_BASE_A
					returnString = "SE_DLC_APT_Stilts_A_Bedroom"
				BREAK
				CASE PROPERTY_STILT_APT_1_BASE_B
					returnString = "SE_DLC_APT_Stilts_B_Bedroom"
				BREAK
				CASE PROPERTY_CUSTOM_APT_1_BASE
					returnString = "SE_DLC_APT_Custom_Bedroom"
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			SWITCH GET_BASE_PROPERTY_FROM_PROPERTY(iProperty)
				CASE PROPERTY_STILT_APT_5_BASE_A
					returnString = "SE_DLC_APT_Stilts_A_Heist_Room"
				BREAK
				CASE PROPERTY_STILT_APT_1_BASE_B
					returnString = "SE_DLC_APT_Stilts_B_Heist_Room"
				BREAK
				CASE PROPERTY_CUSTOM_APT_1_BASE
					returnString = "SE_DLC_APT_Custom_Heist_Room"
				BREAK
			ENDSWITCH
		BREAK
		CASE 4 //garage
			SWITCH GET_BASE_PROPERTY_FROM_PROPERTY(iProperty)
				CASE PROPERTY_OFFICE_3_GARAGE_LVL1
					returnString = "DLC_IE_Office_Garage_Radio_01"
				BREAK
			ENDSWITCH
		BREAK
		CASE 5 //car mod shop
			SWITCH GET_BASE_PROPERTY_FROM_PROPERTY(iProperty)
				CASE PROPERTY_OFFICE_3_GARAGE_LVL1
					returnString = "DLC_IE_Office_Garage_Mod_Shop_Radio_01"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN returnString 
ENDFUNC

/// PURPOSE:
///    Repeats through all the radio audio emitters in all properties and turns them off
PROC MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_PROPERTY_STRUCT &property)
	//CDEBUG2LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS")
	
	SET_STATIC_EMITTER_ENABLED(PRV_MP_RADIO_GET_GARAGE_EMITTER_NAME(PROP_GARAGE_SIZE_2), FALSE)
	SET_STATIC_EMITTER_ENABLED(PRV_MP_RADIO_GET_GARAGE_EMITTER_NAME(PROP_GARAGE_SIZE_6), FALSE)
	SET_STATIC_EMITTER_ENABLED(PRV_MP_RADIO_GET_GARAGE_EMITTER_NAME(PROP_GARAGE_SIZE_10), FALSE)
	
	SET_STATIC_EMITTER_ENABLED(PRV_MP_RADIO_GET_APARTMENT_EMITTER_NAME(PROP_GARAGE_SIZE_2, 0, 0), FALSE)
	SET_STATIC_EMITTER_ENABLED(PRV_MP_RADIO_GET_APARTMENT_EMITTER_NAME(PROP_GARAGE_SIZE_6, 0, 0), FALSE)
	
	IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bar", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_02", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_03", FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Yacht interior emitters have been disabled")
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND MPRadioLocal.bOnYachtDeck
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_02", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_03", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_04", FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Yacht exterior emitters have been disabled")
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_5_BASE_A)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_A_Bedroom", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_A_Heist_Room", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_A_Living_Room", FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: SE_DLC_APT_Stilts_A_Bedroom, SE_DLC_APT_Stilts_A_Heist_Room, SE_DLC_APT_Stilts_A_Living_Room have been disabled")
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_1_BASE_B)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_B_Bedroom", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_B_Heist_Room", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_B_Living_Room", FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: SE_DLC_APT_Stilts_B_Bedroom, SE_DLC_APT_Stilts_B_Heist_Room, SE_DLC_APT_Stilts_B_Living_Room have been disabled")
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Custom_Bedroom", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Custom_Heist_Room", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Custom_Living_Room", FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: SE_DLC_APT_Custom_Living_Room, SE_DLC_APT_Custom_Heist_Room, SE_DLC_APT_Custom_Bedroom have been disabled")
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex), FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS:  has been disabled emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
	ELIF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioLocal.iWarehouseSize), FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS:  has been disabled emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioLocal.iWarehouseSize))		
	ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4), FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Office Garage - has been disabled emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, PICK_INT(IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID()), 5, 4)))
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5), FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Office GarageModShop - has been disabled emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, PICK_INT(IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID()), 5, 4)))
	ELIF IS_PLAYER_IN_FACTORY(PLAYER_ID())	
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, TRUE, MPRadioLocal.iFactoryType), FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS:  has been disabled emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, TRUE, MPRadioLocal.iFactoryType))
	ELIF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, FALSE, MPRadioLocal.iFactoryType, TRUE), FALSE)
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_IE_GARAGE_EMITTERS:  has been disabled emitter = ",GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, FALSE, MPRadioLocal.iFactoryType, TRUE))
	ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
//		IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(MPRadioLocal.piApartmentOwner)
			CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Hangar Personal Quarters static emitters")
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), FALSE)
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), FALSE)
//		#IF IS_DEBUG_BUILD
//		ELSE
//			CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: NOT Turning off Hangar Personal Quarters static emitters")
//		#ENDIF
//		ENDIF
		
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Hangar Office static emitters")
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), FALSE)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), FALSE)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 2), FALSE)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3), FALSE)
		
		IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(MPRadioLocal.piApartmentOwner)
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), TRUE)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), "radio_01_class_rock")
			CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning on Hangar ModShop static emitters")
		ELSE
			CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Hangar ModShop static emitters")
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), FALSE)
		
		ENDIF
	ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Facility radio static emitters")
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), FALSE)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), FALSE)
	ELIF MPRadioLocal.bInCasinoApartment
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Casino Apartment radio static emitters")
		
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), FALSE)
		IF NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE()
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), FALSE)
		ENDIF
	ELIF MPRadioLocal.bInCasinoApartment2
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Casino Apartment radio static emitters")
		
		IF NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE()
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), FALSE)
		ENDIF
	ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Submarine radio static emitter")
		
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1), FALSE)
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Auto Shop radio static emitter")
		
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1), FALSE)
	ELIF IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Car Meet Private Takeover radio static emitter")
		
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1), FALSE)	
	#IF FEATURE_DLC_1_2022
	ELIF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off SIMEON SHOWROOM radio static emitter")
		
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1), FALSE)	
	#ENDIF
	#IF FEATURE_TUNER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS: Turning off Fixer HQ radio static emitter")
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_office_sec_Player_Office", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_office_sec_hacker_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_sf_dlc_office_sec_apartment_01", FALSE)
	ENDIF
	#ENDIF
	
	
	INT iPropertyLoop
	INT iEmitterLoop
	STRING sEmitter
	
	REPEAT MAX_MP_PROPERTIES+1 iPropertyLoop
		REPEAT MP_RADIO_MAX_NUM_APT_RADIO_EMITTERS iEmitterLoop
			sEmitter = PRV_MP_RADIO_GET_APARTMENT_EMITTER_NAME(PROP_GARAGE_SIZE_10, iPropertyLoop, iEmitterLoop)
			IF NOT IS_STRING_NULL_OR_EMPTY(sEmitter)
				SET_STATIC_EMITTER_ENABLED(sEmitter, FALSE)
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Alters the radio volume
/// PARAMS:
///    MPRadioLocal - local data struct to hold the volume level
///    bIncrease - increase volume or decrease volume
PROC INCREMENT_MP_RADIO_VOLUME(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, BOOL bIncrease)
	FLOAT fCheckVolume = MPRadioLocal.fVolume
	
	IF MPRadioLocal.iVolumeTimer = 0
		IF bIncrease
			MPRadioLocal.fVolume += MP_RADIO_VOLUME_INCREMENT
		ELSE
			MPRadioLocal.fVolume -=  MP_RADIO_VOLUME_INCREMENT
		ENDIF
		MPRadioLocal.iVolumeTimer = GET_GAME_TIMER() +  MP_RADIO_INCREMENT_WAIT_TIME
	ELSE
		IF MPRadioLocal.iVolumeTimer < GET_GAME_TIMER()
			IF bIncrease
				MPRadioLocal.fVolume += MP_RADIO_VOLUME_INCREMENT
			ELSE
				MPRadioLocal.fVolume -= MP_RADIO_VOLUME_INCREMENT
			ENDIF
		ENDIF
	ENDIF
	
	IF MPRadioLocal.fVolume >= MP_RADIO_MAX_VOLUME
		MPRadioLocal.fVolume = MP_RADIO_MAX_VOLUME
	ENDIF
	IF MPRadioLocal.fVolume <= MP_RADIO_MIN_VOLUME
		MPRadioLocal.fVolume = MP_RADIO_MIN_VOLUME
	ENDIF
	
	IF fCheckVolume <> MPRadioLocal.fVolume
		//Change here
	ENDIF
ENDPROC

/// PURPOSE:
///    Resets the radio volume to the default value
/// PARAMS:
///    MPRadioLocal - local data struct to hold the volume level
PROC RESET_INCREMENT_MP_RADIO_VOLUME(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)
	MPRadioLocal.iVolumeTimer = 0
ENDPROC

/// PURPOSE:
///    Disables player control apart from the specific actions required to control the radio
PROC DISABLE_MP_RADIO_CONTROL_ACTIONS_GENERAL(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, CONTROL_ACTION caRadioOnOffInput)
	UNUSED_PARAMETER(MPRadioLocal)
	
	IF NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT IS_PAUSE_MENU_ACTIVE()
		DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
		DISABLE_ALL_CONTROL_ACTIONS(FRONTEND_CONTROL)
		
		#IF FEATURE_TUNER
		IF MPRadioLocal.piApartmentOwner = PLAYER_ID()
		AND HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES()
			ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
		ENDIF
		#ENDIF
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, caRadioOnOffInput)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
		ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
	ENDIF
ENDPROC

PROC LOAD_LOCATE_FROM_PROPERTY_DATA(INT iPropertyIndex, INT iLocateV0Index, INT iLocateV1Index, INT iAnimIndex,  MP_PROP_OFFSET_STRUCT& out_animData, LOCATE& out_angledAreaLocate, INT iBasePropertyIndex = -1, BOOL bIsOfficeModGarage = FALSE)
	MP_PROP_OFFSET_STRUCT offset
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iPropertyIndex, iLocateV0Index, offset, iBasePropertyIndex, bIsOfficeModGarage)
	out_angledAreaLocate.v0 = offset.vLoc
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iPropertyIndex, iLocateV1Index, offset, iBasePropertyIndex, bIsOfficeModGarage)
	out_angledAreaLocate.v1 = offset.vLoc
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iPropertyIndex, iAnimIndex, out_animData, iBasePropertyIndex, bIsOfficeModGarage)						
ENDPROC

PROC LOAD_LOCATE_FROM_YACHT_DATA(INT iYachtIndex, INT iLocateV0Index, INT iLocateV1Index, INT iAnimIndex,  MP_PROP_OFFSET_STRUCT& out_animData, LOCATE& out_angledAreaLocate, BOOL bExteriorOffset=FALSE)
	MP_PROP_OFFSET_STRUCT offset
	GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtIndex, iLocateV0Index, offset, bExteriorOffset)
	out_angledAreaLocate.v0 = offset.vLoc
	GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtIndex, iLocateV1Index, offset, bExteriorOffset)
	out_angledAreaLocate.v1 = offset.vLoc
	GET_POSITION_AS_OFFSET_FOR_YACHT(iYachtIndex, iAnimIndex, out_animData, bExteriorOffset)
ENDPROC

/// PURPOSE:
///    Calculates the coords of the angledAreaLocates of all the radios
/// PARAMS:
///    MPRadioClient - client data struct to query if the player is in a garage
///    property - property to check if activated in
///    iRadioID - ID of radio within the property to check
/// RETURNS:
///    true/false
FUNC BOOL CALCULATE_LOCATE_COORDS(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal,  MP_PROPERTY_STRUCT &property)
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		MPRadioLocal.radioLocates[0].v0 = <<1564.025024,385.898224,-54.286575>>
		MPRadioLocal.radioLocates[0].v1 = <<1562.704224,385.920563,-52.289154>>
		MPRadioLocal.radioAnimations[0].vLoc = <<1563.636, 385.864, -53.2888>>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, 180.0>>
		
		RETURN TRUE
	ENDIF	
	
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		MPRadioLocal.radioLocates[0].v0 = <<-1355.740601,150.008286,-100.194366>>
		MPRadioLocal.radioLocates[0].v1 = <<-1355.729858,151.194839,-98.194366>>
		MPRadioLocal.radioAnimations[0].vLoc = << -1355.925, 150.166, -99.157 >>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, 90.0>>
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		MPRadioLocal.radioLocates[0].v0 = <<-2168.369873,1110.768555,-25.370466>>
		MPRadioLocal.radioLocates[0].v1 = <<-2168.368164,1111.326538,-23.370487>>
		MPRadioLocal.radioAnimations[0].vLoc = <<-2168.397, 1111.525, -24.350>>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, -90.0>>
		
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		MPRadioLocal.radioLocates[0].v0 = <<-32.763088,-1100.454712,25.540852>>
		MPRadioLocal.radioLocates[0].v1 = <<-33.936031,-1100.039795,27.672344>>
		MPRadioLocal.radioAnimations[0].vLoc = <<-33.137+0.1695, -1100.455+0.4929, 26.459>>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, -110.000>>	
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
		
		// Player's office
		MPRadioLocal.radioLocates[0].v0 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 6.91971, -9.33882, 3.8003 >>, eSimpleInterior)		// Original value at Hawick <<373.671692,-64.035339,106.163300>>
		MPRadioLocal.radioLocates[0].v1 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 6.92308, -10.4297, 5.8003 >>, eSimpleInterior)		// Original value at Hawick <<372.645477,-63.665413,108.163300>>
		MPRadioLocal.radioAnimations[0].vLoc = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 7.15299, -9.45153, 4.825 >>, eSimpleInterior)	// Original value at Hawick << 373.486, -64.216, 107.188 >>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(265.401978, eSimpleInterior)>>		// Original value at Hawick 155.402
								
		// Operations floor back room
		MPRadioLocal.radioLocates[1].v0 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< -11.4769, 11.5415, 8.67532 >>, eSimpleInterior)		// Original value at Hawick <<399.584778,-53.889618,111.038322>>
		MPRadioLocal.radioLocates[1].v1 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< -10.3667, 11.5237, 10.6 >>, eSimpleInterior)			// Original value at Hawick <<399.188354,-54.926804,112.963020>>
		MPRadioLocal.radioAnimations[1].vLoc = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< -11.5705, 12.0834, 9.637 >>, eSimpleInterior)	// Original value at Hawick << 400.126, -53.987, 112.000 >>
		MPRadioLocal.radioAnimations[1].vRot = <<0.0, 0.0, TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(358.399994, eSimpleInterior)>>		// Original value at Hawick -111.600 >>
											
		// Personal quarters		
		MPRadioLocal.radioLocates[2].v0 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 7.80937, -0.0539093, 8.84151 >>, eSimpleInterior)	// Original value at Hawick <<382.092377,-68.046967,111.204506>>
		MPRadioLocal.radioLocates[2].v1 = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 8.7156, -0.0429809, 10.8415 >>, eSimpleInterior)		// Original value at Hawick <<381.792694,-68.902290,113.204506>>
		MPRadioLocal.radioAnimations[2].vLoc = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 7.81505, 0.635223, 9.637 >>, eSimpleInterior)	// Original value at Hawick << 382.738, -68.288, 112.000 >>
		MPRadioLocal.radioAnimations[2].vRot = <<0.0, 0.0, TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(353.359985, eSimpleInterior)>>		// Original value at Hawick -116.640
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF MPRadioLocal.bInCasinoApartment
		MPRadioLocal.radioLocates[0].v0 = <<975.517517,68.740929,115.164154>>
		MPRadioLocal.radioLocates[0].v1 = <<974.676758,69.272110,117.164154>>
		MPRadioLocal.radioAnimations[0].vLoc = <<975.500, 68.720, 116.1848>>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, 148.5>>
		
		RETURN TRUE
	ENDIF
	
	IF MPRadioLocal.bInCasinoApartment2
		MPRadioLocal.radioLocates[0].v0 = <<947.814819,5.245894,115.164215>>
		MPRadioLocal.radioLocates[0].v1 = <<948.919617,4.532114,117.164215>>
		MPRadioLocal.radioAnimations[0].vLoc = <<948.303, 5.233, 116.1848>>
		MPRadioLocal.radioAnimations[0].vRot = <<0.0, 0.0, -32.0>>
		
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: angledAreaLocate coords assigned")
		MPRadioLocal.radioLocates[0].v0 	 = <<-1237.810669,-3008.222168,-43.887272>> 
		MPRadioLocal.radioLocates[0].v1		 = <<-1237.797485,-3009.318115,-42.137272>> 
		MPRadioLocal.radioAnimations[0].vLoc = << -1237.647, -3008.303, -42.875 >>
		MPRadioLocal.radioAnimations[0].vRot = << 0.000, 0.000, -88.920 >>
		
		IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(MPRadioLocal.piApartmentOwner)
			MPRadioLocal.radioLocates[1].v0 	 = <<-1236.293091,-2982.683838,-42.263611>> 
			MPRadioLocal.radioLocates[1].v1 	 = <<-1236.315063,-2981.646240,-39.763611>> 
			MPRadioLocal.radioAnimations[1].vLoc = << -1236.553, -2982.447, -41.250 >>
			MPRadioLocal.radioAnimations[1].vRot = << 0.000, 0.000, 83.160 >>
			CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED = TRUE")
		ELSE
			CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED = FALSE")
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_STILT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_10
				IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
						LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS, MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0], PROPERTY_BUS_HIGH_APT_1)
						LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO1_0, MP_PROP_ELEMENT_RADIO1_1, MP_PROP_ELEMENT_RADIO1_ANIMS, MPRadioLocal.radioAnimations[1], MPRadioLocal.radioLocates[1], PROPERTY_BUS_HIGH_APT_1)				
						// bedroom
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_life_apment"
					ELSE
						LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS, MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0])	
						LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO1_0, MP_PROP_ELEMENT_RADIO1_1, MP_PROP_ELEMENT_RADIO1_ANIMS, MPRadioLocal.radioAnimations[1], MPRadioLocal.radioLocates[1])
					ENDIF
					
					IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
						LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO2_0, MP_PROP_ELEMENT_RADIO2_1, MP_PROP_ELEMENT_RADIO2_ANIMS, MPRadioLocal.radioAnimations[2], MPRadioLocal.radioLocates[2], PROPERTY_BUS_HIGH_APT_1)	
					ELSE
						LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO2_0, MP_PROP_ELEMENT_RADIO2_1, MP_PROP_ELEMENT_RADIO2_ANIMS, MPRadioLocal.radioAnimations[2], MPRadioLocal.radioLocates[2])	
					ENDIF
				ENDIF
			BREAK
			CASE PROP_GARAGE_SIZE_20
				LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS, MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
				CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[0].v0: ", MPRadioLocal.radioLocates[0].v0, ", MPRadioLocal.radioLocates[0].v1: ", MPRadioLocal.radioLocates[0].v1, ", MPRadioLocal.radioAnimations[0].vLoc: ", MPRadioLocal.radioAnimations[0].vLoc)
			BREAK
		ENDSWITCH
	
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)	
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS,  MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[0].v0: ", MPRadioLocal.radioLocates[0].v0, ", MPRadioLocal.radioLocates[0].v1: ", MPRadioLocal.radioLocates[0].v1, ", MPRadioLocal.radioAnimations[0].vLoc: ", MPRadioLocal.radioAnimations[0].vLoc)
	
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO1_0, MP_PROP_ELEMENT_RADIO1_1, MP_PROP_ELEMENT_RADIO1_ANIMS,  MPRadioLocal.radioAnimations[1], MPRadioLocal.radioLocates[1], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[1].v0: ", MPRadioLocal.radioLocates[1].v0, ", MPRadioLocal.radioLocates[1].v1: ", MPRadioLocal.radioLocates[1].v1, ", MPRadioLocal.radioAnimations[1].vLoc: ", MPRadioLocal.radioAnimations[1].vLoc)
	
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO2_0, MP_PROP_ELEMENT_RADIO2_1, MP_PROP_ELEMENT_RADIO2_ANIMS,  MPRadioLocal.radioAnimations[2], MPRadioLocal.radioLocates[2], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[2].v0: ", MPRadioLocal.radioLocates[2].v0, ", MPRadioLocal.radioLocates[2].v1: ", MPRadioLocal.radioLocates[2].v1, ", MPRadioLocal.radioAnimations[2].vLoc: ", MPRadioLocal.radioAnimations[2].vLoc)
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0_VAR2, MP_PROP_ELEMENT_RADIO0_1_VAR2, MP_PROP_ELEMENT_RADIO0_ANIMS_VAR2,  MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[0].v0: ", MPRadioLocal.radioLocates[0].v0, ", MPRadioLocal.radioAnimations[0].v1: ", MPRadioLocal.radioLocates[0].v1, ", MPRadioLocal.radioAnimations[0].vLoc: ", MPRadioLocal.radioAnimations[0].vLoc)
		
		// Stairs radio stored in 3
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS,  MPRadioLocal.radioAnimations[3], MPRadioLocal.radioLocates[3], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[3].v0: ", MPRadioLocal.radioLocates[3].v0, ", MPRadioLocal.radioLocates[3].v1: ", MPRadioLocal.radioLocates[3].v1, ", MPRadioLocal.radioAnimations[3].vLoc: ", MPRadioLocal.radioAnimations[3].vLoc)
		
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO1_0, MP_PROP_ELEMENT_RADIO1_1, MP_PROP_ELEMENT_RADIO1_ANIMS,  MPRadioLocal.radioAnimations[1], MPRadioLocal.radioLocates[1], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[1].v0: ", MPRadioLocal.radioLocates[1].v0, ", MPRadioLocal.radioLocates[1].v1: ", MPRadioLocal.radioLocates[1].v1, ", MPRadioLocal.radioAnimations[1].vLoc: ", MPRadioLocal.radioAnimations[1].vLoc)
	
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO2_0, MP_PROP_ELEMENT_RADIO2_1, MP_PROP_ELEMENT_RADIO2_ANIMS,  MPRadioLocal.radioAnimations[2], MPRadioLocal.radioLocates[2], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[2].v0: ", MPRadioLocal.radioLocates[2].v0, ", MPRadioLocal.radioLocates[2].v1: ", MPRadioLocal.radioLocates[2].v1, ", MPRadioLocal.radioAnimations[2].vLoc: ", MPRadioLocal.radioAnimations[2].vLoc)
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
		LOAD_LOCATE_FROM_PROPERTY_DATA(property.iIndex, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS,  MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: MPRadioLocal.radioLocates[0].v0: ", MPRadioLocal.radioLocates[0].v0, ", MPRadioLocal.radioLocates[0].v1: ", MPRadioLocal.radioLocates[0].v1, ", MPRadioLocal.radioAnimations[0].vLoc: ", MPRadioLocal.radioAnimations[0].vLoc)
	ELIF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		IF MPRadioLocal.iWarehouseSize = ciLARGE_WAREHOUSE_CRATE_CAPACITY
			MPRadioLocal.radioLocates[0].v0 		= <<994.937866,-3097.307373,-39.990833>> 
			MPRadioLocal.radioLocates[0].v0 		= <<994.934631,-3098.541504,-38.240833>> 
			MPRadioLocal.radioAnimations[0].vLoc 	= << 995.367, -3097.480, -39.115 >>
			MPRadioLocal.radioAnimations[0].vRot 	= << -0.000, 0.000, -92.843 >>
		ELIF  MPRadioLocal.iWarehouseSize = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY 
			MPRadioLocal.radioLocates[0].v0 		= <<1049.197998,-3100.076660,-39.999931>>
			MPRadioLocal.radioLocates[0].v0 		= <<1049.168457,-3099.129883,-38.249931>> 
			MPRadioLocal.radioAnimations[0].vLoc 	= << 1048.582, -3100.444, -39.115 >>
			MPRadioLocal.radioAnimations[0].vRot  	= << 0.000, 0.000, 84.600 >>
		ELIF  MPRadioLocal.iWarehouseSize = ciSMALL_WAREHOUSE_CRATE_CAPACITY 
			MPRadioLocal.radioLocates[0].v0  		= <<1088.615112,-3100.931641,-39.999931>>
			MPRadioLocal.radioLocates[0].v0 		= <<1088.619263,-3099.956787,-38.249931>> 
			MPRadioLocal.radioAnimations[0].vLoc	= << 1088.011, -3100.987, -39.125 >>
			MPRadioLocal.radioAnimations[0].vRot	= << 0.000, 0.000, 88.200 >>
		ENDIF
	ELIF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		MPRadioLocal.radioLocates[0].v0 		= <<965.73, -2991.764, -40.59>>
		MPRadioLocal.radioLocates[0].v0 		= <<965.73, -2990.864, -38.790>>
		MPRadioLocal.radioAnimations[0].vLoc	= <<965.890, -2991.614, -39.670>>
		MPRadioLocal.radioAnimations[0].vRot	= << 0.000, 0.000, 86.760 >>
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
		INT iYachtID = MPRadioLocal.iYachtID
		IF iYachtID = -1
			CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: iYachtID = -1")
			RETURN FALSE			
		ENDIF
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: in Yacht apartment, iYachtID  = ", MPRadioLocal.iYachtID)
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO0_0, MP_PROP_ELEMENT_RADIO0_1, MP_PROP_ELEMENT_RADIO0_ANIMS,  MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0])
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO1_0, MP_PROP_ELEMENT_RADIO1_1, MP_PROP_ELEMENT_RADIO1_ANIMS,  MPRadioLocal.radioAnimations[1], MPRadioLocal.radioLocates[1])
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO2_0, MP_PROP_ELEMENT_RADIO2_1, MP_PROP_ELEMENT_RADIO2_ANIMS,  MPRadioLocal.radioAnimations[2], MPRadioLocal.radioLocates[2])
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO3_0, MP_PROP_ELEMENT_RADIO3_1, MP_PROP_ELEMENT_RADIO3_ANIMS,  MPRadioLocal.radioAnimations[3], MPRadioLocal.radioLocates[3])
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO4_0, MP_PROP_ELEMENT_RADIO4_1, MP_PROP_ELEMENT_RADIO4_ANIMS,  MPRadioLocal.radioAnimations[4], MPRadioLocal.radioLocates[4])
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT 5 i
		CDEBUG1LN(DEBUG_RADIO, "Yacht radio [",i,"] angledAreaLocate loaded : ", MPRadioLocal.radioLocates[i].v0, ", ", MPRadioLocal.radioLocates[i].v1) 
		ENDREPEAT
		#ENDIF
	ELIF MPRadioLocal.bOnYachtDeck
		
		IF MPRadioLocal.iYachtID = -1
			CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: iYachtID = -1")
			RETURN FALSE			
		ENDIF
		CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: in Yacht apartment")
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO0_0_EXT, MP_PROP_ELEMENT_RADIO0_1_EXT, MP_PROP_ELEMENT_RADIO0_ANIMS_EXT,  MPRadioLocal.radioAnimations[0], MPRadioLocal.radioLocates[0])
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO1_0_EXT, MP_PROP_ELEMENT_RADIO1_1_EXT, MP_PROP_ELEMENT_RADIO0_ANIMS_EXT,  MPRadioLocal.radioAnimations[1], MPRadioLocal.radioLocates[1]) //@@anim was commented out before and now using dummy index here (0)
		LOAD_LOCATE_FROM_YACHT_DATA(MPRadioLocal.iYachtID, MP_PROP_ELEMENT_RADIO2_0_EXT, MP_PROP_ELEMENT_RADIO2_1_EXT, MP_PROP_ELEMENT_RADIO0_ANIMS_EXT,  MPRadioLocal.radioAnimations[2], MPRadioLocal.radioLocates[2]) //@@anim was commented out before and now using dummy index here (0)
		CDEBUG1LN(DEBUG_RADIO, "Yacht ext radio [0] angledAreaLocate loaded : ", MPRadioLocal.radioLocates[0].v0, ", ", MPRadioLocal.radioLocates[0].v1) 
		CDEBUG1LN(DEBUG_RADIO, "Yacht ext radio [1] angledAreaLocate loaded : ", MPRadioLocal.radioLocates[1].v0, ", ", MPRadioLocal.radioLocates[1].v1) 
		CDEBUG1LN(DEBUG_RADIO, "Yacht ext radio [2] angledAreaLocate loaded : ", MPRadioLocal.radioLocates[2].v0, ", ", MPRadioLocal.radioLocates[2].v1) 
	ENDIF
	CDEBUG1LN(DEBUG_RADIO, "CALCULATE_LOCATE_COORDS: return true")
	RETURN TRUE
ENDFUNC

FUNC BOOL USE_DEFUNCT_BASE_QUARTERS_RADIO()
	RETURN IS_PLAYER_DEFUNCT_BASE_PERSONAL_QUARTERS_2_PURCHASED(g_ownerOfBasePropertyIAmIn)
	OR IS_PLAYER_DEFUNCT_BASE_PERSONAL_QUARTERS_3_PURCHASED(g_ownerOfBasePropertyIAmIn)
ENDFUNC

FUNC BOOL IS_PLAYER_FACING_RADIO(INT iRadioID)
	BOOL bCheckHeading = FALSE
	FLOAT fHeading = 0.0
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		SWITCH GET_SIMPLE_INTERIOR_TYPE_LOCAL_PLAYER_IS_IN()
			CASE SIMPLE_INTERIOR_TYPE_AUTO_SHOP
				bCheckHeading = TRUE
				fHeading = 90.0
			BREAK
			CASE SIMPLE_INTERIOR_TYPE_FIXER_HQ
				bCheckHeading = TRUE
				SWITCH iRadioID
					CASE 0 fHeading = 268.713501 BREAK 	// Original world value at Hawick 158.7135 - Player's office
					CASE 1 fHeading = 359.211700 BREAK	// Original world value at Hawick 249.2117 - Op floor
					CASE 2 fHeading = 359.018005 BREAK	// Original world value at Hawick 249.0180 - Personal quarters
				ENDSWITCH
				
				fHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(fHeading, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))						
			BREAK
		ENDSWITCH
	ENDIF
	IF bCheckHeading = TRUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
			RETURN IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), fHeading, 45.0)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the local player is in the angled area required to activate the supplied radio ID
/// PARAMS:
///    MPRadioClient - client data struct to query if the player is in a garage
///    property - property to check if activated in
///    iRadioID - ID of radio within the property to check
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_PROPERTY_STRUCT &property, INT iRadioID, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)

	IF g_bWarehouseLaptopContextTriggered
	AND IS_LOCAL_PLAYER_IN_WAREHOUSE_THEY_OWN()
		CDEBUG1LN(DEBUG_RADIO, "[NET_RADIO] IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE Returning false because g_bWarehouseLaptopContextTriggered is TRUE")
		RETURN FALSE
	ENDIF	
	
	
	unused_parameter(property)
	unused_parameter(iRadioID)
	LOCATE angledAreaLocate
	FLOAT fWidth = 0.0
	
	BOOL bIndependantRadio = FALSE

	
	
	
	
	SWITCH property.iGarageSize
		CASE PROP_GARAGE_SIZE_2
		CASE PROP_GARAGE_SIZE_6
//			CWARNINGLN(DEBUG_RADIO, "CLEAR_BIT - MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION")
			CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
		BREAK
	ENDSWITCH
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		IF NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn) //and not cheap lounge
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<360.891724,4841.154297,-59.999626>>, <<360.487366,4839.927246,-57.691368>>, 1.5)
			MPRadioLocal.sRadioAnimDictName 		= "anim@mp_radio@garage@low"
			MPRadioLocal.iRadioAnimHeading 			= 155
			MPRadioLocal.vRadioAnimPosition 		= <<360.460, 4840.405, -58.960>>
			MPRadioLocal.sRadioAnimClip 			= "action_a"
			MPRadioLocal.sRadioAnimClipEnter 		= "enter"
			MPRadioLocal.sRadioAnimClipIdle 		= "idle_a"
			MPRadioLocal.sRadioAnimClipButton 		= "button_press"
			MPRadioLocal.sRadioAnimClipExit 		= "exit"
			g_bInRadioLocate 		= TRUE
			CDEBUG1LN(DEBUG_RADIO, "In Facility lounge angledAreaLocate")
			RETURN TRUE
		ELIF USE_DEFUNCT_BASE_QUARTERS_RADIO() //and not cheap quarters
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<363.889557,4826.533691,-59.991413>>, <<363.904327,4827.508301,-58.010437>>, 1.500000)
			MPRadioLocal.sRadioAnimDictName 		= "anim@mp_radio@garage@low"
			MPRadioLocal.iRadioAnimHeading 			= 0			
			MPRadioLocal.vRadioAnimPosition 		= <<364.14, 4827.09, -58.958>>
			MPRadioLocal.sRadioAnimClip 			= "action_a"	
			MPRadioLocal.sRadioAnimClipEnter	 	= "enter"
			MPRadioLocal.sRadioAnimClipIdle 		= "idle_a"
			MPRadioLocal.sRadioAnimClipButton 		= "button_press"
			MPRadioLocal.sRadioAnimClipExit 		= "exit"
			g_bInRadioLocate 		= TRUE
			CDEBUG1LN(DEBUG_RADIO, "In Facility quarters angledAreaLocate")
			RETURN TRUE
		ENDIF
		g_bInRadioLocate = FALSE
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_STILT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex, TRUE)
	AND NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPRadioLocal.bInCasinoApartment
	AND NOT MPRadioLocal.bInCasinoApartment2
	AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<176.273575,-999.476624,-97.587227>>, <<176.267380,-1000.868042,-100.062447>>, 1.687500)
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@low"
						MPRadioLocal.iRadioAnimHeading = 0
						MPRadioLocal.vRadioAnimPosition = <<176.601, -999.790, -98.957>>
						MPRadioLocal.sRadioAnimClip = "action_a"	
						
						MPRadioLocal.sRadioAnimClipEnter = "enter"
						MPRadioLocal.sRadioAnimClipIdle = "idle_a"
						MPRadioLocal.sRadioAnimClipButton = "button_press"
						MPRadioLocal.sRadioAnimClipExit = "exit"
						RETURN TRUE
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<263.614990,-994.406799,-97.697914>>, <<263.606750,-996.170898,-100.071136>>, 1.5)
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@low_apment"
						MPRadioLocal.iRadioAnimHeading = -1
						MPRadioLocal.vRadioAnimPosition = <<263.035, -995.132, -99.040>>
						MPRadioLocal.sRadioAnimClip = "action_a_kitchen"	
						
						MPRadioLocal.sRadioAnimClipEnter = "enter_kitchen"
						MPRadioLocal.sRadioAnimClipIdle = "idle_a_kitchen"
						MPRadioLocal.sRadioAnimClipButton = "button_press_kitchen"
						MPRadioLocal.sRadioAnimClipExit = "exit_kitchen"
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE PROP_GARAGE_SIZE_6
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<201.882751,-993.942688,-97.581116>>, <<202.002441,-995.127747,-100.062462>>, 1.687500)
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@medium"
						MPRadioLocal.iRadioAnimHeading = 0
						MPRadioLocal.vRadioAnimPosition = <<201.455, -994.257, -98.979>>
						MPRadioLocal.sRadioAnimClip = "action_a"
						
						MPRadioLocal.sRadioAnimClipEnter = "enter"
						MPRadioLocal.sRadioAnimClipIdle = "idle_a"
						MPRadioLocal.sRadioAnimClipButton = "button_press"
						MPRadioLocal.sRadioAnimClipExit = "exit"
						RETURN TRUE
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<340.929352,-1002.604614,-100.196220>>, <<340.988678,-1000.321594,-98.196220>>, 1.000000)
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@medium_apment"
						MPRadioLocal.iRadioAnimHeading = -90
						MPRadioLocal.vRadioAnimPosition = <<341.355, -1000.652, -99.140>>
						MPRadioLocal.sRadioAnimClip = "action_a_kitchen"	
						
						MPRadioLocal.sRadioAnimClipEnter = "enter_kitchen"
						MPRadioLocal.sRadioAnimClipIdle = "idle_a_kitchen"
						MPRadioLocal.sRadioAnimClipButton = "button_press_kitchen"
						MPRadioLocal.sRadioAnimClipExit = "exit_kitchen"
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
			CASE PROP_GARAGE_SIZE_10
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<231.841080,-975.634949,-100.089783>>, <<230.351517,-975.611084,-96.041504>>, 2.500000)
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@high"
						MPRadioLocal.iRadioAnimHeading = 0
						MPRadioLocal.vRadioAnimPosition = <<230.302 , -975.685, -98.979>>
						MPRadioLocal.sRadioAnimClip = "action_a"	
						
						MPRadioLocal.sRadioAnimClipEnter = "enter"
						MPRadioLocal.sRadioAnimClipIdle = "idle_a"
						MPRadioLocal.sRadioAnimClipButton = "button_press"
						MPRadioLocal.sRadioAnimClipExit = "exit"
						RETURN TRUE
					ENDIF
				ELSE
	//				PRINTLN("POD: RADIO: ANIMATION: RADIO ID: ", iRadioID)
					SWITCH iRadioID
						CASE 0
							angledAreaLocate = MPRadioLocal.radioLocates[0]
							fWidth = 1.812500
							
							IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
							
								MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[0].vLoc
								MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[0].vRot.Z)
								
								MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_life_apment"
	//							MPRadioLocal.iRadioAnimHeading = 90
	//							MPRadioLocal.vRadioAnimPosition = <<-786.995, 337.645, 211.150>>
								MPRadioLocal.sRadioAnimClip = "action_a_living_room"
								
								MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
								MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
								MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
								MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
								
							ELSE
								MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[0].vLoc
								MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[0].vRot.Z)
								
								MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
	//							MPRadioLocal.iRadioAnimHeading = 180
	//							MPRadioLocal.vRadioAnimPosition = <<-794.419, 335.328, 201.441>>
								MPRadioLocal.sRadioAnimClip = "action_a_bedroom"	
								
								MPRadioLocal.sRadioAnimClipEnter = "enter_bedroom"
								MPRadioLocal.sRadioAnimClipIdle = "idle_a_bedroom"
								MPRadioLocal.sRadioAnimClipButton = "button_press_bedroom"
								MPRadioLocal.sRadioAnimClipExit = "exit_bedroom"
							ENDIF
							
	//						#IF IS_DEBUG_BUILD
	//						#IF SCRIPT_PROFILER_ACTIVE
	//						ADD_SCRIPT_PROFILE_MARKER("RADIO CALCULATE LOCATE COORDS 0")
	//						#ENDIF
	//						#ENDIF
				
						BREAK
						CASE 1
							angledAreaLocate = MPRadioLocal.radioLocates[1]
							fWidth = 1.812500
							
							IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
								// bed room
								MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_life_apment"
								MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[1].vLoc
								MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[1].vRot.Z)
	//							MPRadioLocal.iRadioAnimHeading = -90
	//							MPRadioLocal.vRadioAnimPosition = <<-789.262, 333.715, 210.790>>
								MPRadioLocal.sRadioAnimClip = "action_a_bedroom"	
								
								MPRadioLocal.sRadioAnimClipEnter = "enter_bedroom"
								MPRadioLocal.sRadioAnimClipIdle = "idle_a_bedroom"
								MPRadioLocal.sRadioAnimClipButton = "button_press_bedroom"
								MPRadioLocal.sRadioAnimClipExit = "exit_bedroom"
								
							ELSE
								MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[1].vLoc
								MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[1].vRot.Z)
								
								MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
	//							MPRadioLocal.iRadioAnimHeading = 180
	//							MPRadioLocal.vRadioAnimPosition = <<-800.012, 325.807, 206.097>>
								MPRadioLocal.sRadioAnimClip = "action_a_living_room"
								
								MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
								MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
								MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
								MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
							ENDIF
							
	//						#IF IS_DEBUG_BUILD
	//						#IF SCRIPT_PROFILER_ACTIVE
	//						ADD_SCRIPT_PROFILE_MARKER("RADIO CALCULATE LOCATE COORDS 1")
	//						#ENDIF
	//						#ENDIF
						BREAK
						CASE 2
							angledAreaLocate = MPRadioLocal.radioLocates[2]
							fWidth = 1.0

							IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
								MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[2].vLoc
								MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[2].vRot.Z)
	//							CDEBUG1LN(DEBUG_RADIO, "IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE: Independant radio MPRadioLocal.vRadioAnimPosition: ", MPRadioLocal.vRadioAnimPosition )
								// Heist room
								MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_life_apment"
	//							MPRadioLocal.iRadioAnimHeading = 180
	//							MPRadioLocal.vRadioAnimPosition = <<-764.350, 326.850, 211.117>>
								MPRadioLocal.sRadioAnimClip = "action_a_study"	
								
								MPRadioLocal.sRadioAnimClipEnter = "enter_study"
								MPRadioLocal.sRadioAnimClipIdle = "idle_a_study"
								MPRadioLocal.sRadioAnimClipButton = "button_press_study"
								MPRadioLocal.sRadioAnimClipExit = "exit_study"
							ELSE
								MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[2].vLoc
	//							//CDEBUG2LN(DEBUG_RADIO, "Heist Camera pos: ", MPRadioLocal.radioAnimations[2].vLoc)
								MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[2].vRot.Z)
	//							CDEBUG1LN(DEBUG_RADIO, "IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE: Independant radio MPRadioLocal.vRadioAnimPosition: ", MPRadioLocal.vRadioAnimPosition )
								
								MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
	//							MPRadioLocal.iRadioAnimHeading = 90
	//							MPRadioLocal.vRadioAnimPosition = <<-784.067, 329.130, 207.344>>
								MPRadioLocal.sRadioAnimClip = "action_a_study"
								
								MPRadioLocal.sRadioAnimClipEnter = "enter_study"
								MPRadioLocal.sRadioAnimClipIdle = "idle_a_study"
								MPRadioLocal.sRadioAnimClipButton = "button_press_study"
								MPRadioLocal.sRadioAnimClipExit = "exit_study"
							ENDIF
							
							bIndependantRadio = TRUE

							//						#IF IS_DEBUG_BUILD
	//						#IF SCRIPT_PROFILER_ACTIVE
	//						ADD_SCRIPT_PROFILE_MARKER("RADIO CALCULATE LOCATE COORDS 2")
	//						#ENDIF
	//						#ENDIF

						BREAK
						
					ENDSWITCH
	//				PRINTLN("POD: radio: end of switch")
					
					IF angledAreaLocate.v0.x <> 0
					AND angledAreaLocate.v0.y <> 0
					AND angledAreaLocate.v0.z <> 0
	//					PRINTLN("POD: radio: angledAreaLocate.v0: ", angledAreaLocate.v0, " angledAreaLocate.v1: ", angledAreaLocate.v1)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), angledAreaLocate.v0, angledAreaLocate.v1, fWidth)
							#IF IS_DEBUG_BUILD
//								PRINTLN("POD in angled area ")
//								PRINTLN("POD RADIO: ANIMATION: MPRadioLocal.sRadioAnimDictName: ", MPRadioLocal.sRadioAnimDictName)
//								PRINTLN("POD RADIO: ANIMATION: MPRadioLocal.iRadioAnimHeading: ", MPRadioLocal.iRadioAnimHeading)
//								PRINTLN("POD RADIO: ANIMATION: MPRadioLocal.vRadioAnimPosition: ", MPRadioLocal.vRadioAnimPosition)
//								PRINTLN("POD RADIO: ANIMATION: MPRadioLocal.sRadioAnimClip: ", MPRadioLocal.sRadioAnimClip)
							#ENDIF
							IF bIndependantRadio = TRUE
								IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
									CWARNINGLN(DEBUG_RADIO, "SET_BIT - MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION")
									SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
								ENDIF
								bIndependantRadio = FALSE
							ENDIF
							RETURN TRUE
						ELSE	
							bIndependantRadio = FALSE
	//						CDEBUG1LN(DEBUG_RADIO, "CLEAR_BIT - MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION")
							CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
	//						PRINTLN("POD: RADIO: player isn't in any radio angled area ")
							MPRadioLocal.vRadioAnimPosition = <<0,0,0>>
							MPRadioLocal.iRadioAnimHeading = 0
							MPRadioLocal.sRadioAnimDictName = ""

							MPRadioLocal.sRadioAnimClip = ""
							MPRadioLocal.sRadioAnimClipEnter = ""
							MPRadioLocal.sRadioAnimClipIdle = ""
							MPRadioLocal.sRadioAnimClipButton = ""
							MPRadioLocal.sRadioAnimClipExit = ""
						ENDIF
					
					ENDIF
					
				ENDIF
			BREAK
			CASE PROP_GARAGE_SIZE_20	
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), MPRadioLocal.radioLocates[0].v0, MPRadioLocal.radioLocates[0].v1, 1.4)
					MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[0].vRot.Z)
					MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[0].vLoc
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@medium"
					MPRadioLocal.sRadioAnimClip = "action_a"	
					MPRadioLocal.sRadioAnimClipEnter = "enter"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a"
					MPRadioLocal.sRadioAnimClipButton = "button_press"
					MPRadioLocal.sRadioAnimClipExit = "exit"
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	OR IS_PROPERTY_STILT_APARTMENT(property.iIndex)
	OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
	OR MPRadioLocal.bOnYachtDeck
	OR IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR MPRadioLocal.bInCasinoApartment
	OR MPRadioLocal.bInCasinoApartment2
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	OR IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	OR IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
	#ENDIF
	#IF FEATURE_FIXER
	OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
	//AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
//		CDEBUG1LN(DEBUG_RADIO, "Checking for stilt or yacht angled areas")
		SWITCH iRadioID
			CASE 0
				MP_PROP_OFFSET_STRUCT tempAnimPosition
				angledAreaLocate = MPRadioLocal.radioLocates[0]
				fWidth = 1.812500

				tempAnimPosition = MPRadioLocal.radioAnimations[0]
				 
				IF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
					IF g_iCurrentPropertyVariation = PROPERTY_VARIATION_1
					OR g_iCurrentPropertyVariation = PROPERTY_VARIATION_4
					OR g_iCurrentPropertyVariation = PROPERTY_VARIATION_5
					OR g_iCurrentPropertyVariation = PROPERTY_VARIATION_8
						 angledAreaLocate = MPRadioLocal.radioLocates[3]
						 tempAnimPosition = MPRadioLocal.radioAnimations[3]
					ELIF g_iCurrentPropertyVariation = PROPERTY_VARIATION_2
					OR g_iCurrentPropertyVariation = PROPERTY_VARIATION_3
					OR g_iCurrentPropertyVariation = PROPERTY_VARIATION_6
					OR g_iCurrentPropertyVariation = PROPERTY_VARIATION_7
						 angledAreaLocate = MPRadioLocal.radioLocates[0]
						 tempAnimPosition = MPRadioLocal.radioAnimations[0]
					ENDIF
				ENDIF
				
				MPRadioLocal.vRadioAnimPosition = tempAnimPosition.vLoc
				MPRadioLocal.iRadioAnimHeading = ROUND(tempAnimPosition.vRot.Z)
	
				MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"

				MPRadioLocal.sRadioAnimClip = "action_a_living_room"	
				
				MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
				MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
				MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
				MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
				
				 
				IF MPRadioLocal.bOnYachtDeck
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@low"
					MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[0].vLoc
					MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[0].vRot.Z)
					MPRadioLocal.sRadioAnimClip = "action_a"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a"
					MPRadioLocal.sRadioAnimClipButton = "button_press"
					MPRadioLocal.sRadioAnimClipExit = "exit"
				ENDIF
				
				IF IS_PROPERTY_OFFICE(property.iIndex)
					
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
					MPRadioLocal.sRadioAnimClip = "action_a_bedroom"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter_bedroom"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_bedroom"
					MPRadioLocal.sRadioAnimClipButton = "button_press_bedroom"
					MPRadioLocal.sRadioAnimClipExit = "exit_bedroom"
				ENDIF
				
				IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
				OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"

					MPRadioLocal.sRadioAnimClip = "action_a_living_room"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
					MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
					MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
					
				ENDIF
				
				IF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@low_apment"

					MPRadioLocal.sRadioAnimClip = "action_a_kitchen"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter_kitchen"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_kitchen"
					MPRadioLocal.sRadioAnimClipButton = "button_press_kitchen"
					MPRadioLocal.sRadioAnimClipExit = "exit_kitchen"
				ENDIF
				
				IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				OR MPRadioLocal.bInCasinoApartment
				OR MPRadioLocal.bInCasinoApartment2
				OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
				OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
				OR IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
				#IF FEATURE_DLC_1_2022
				OR IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
				#ENDIF
				#IF FEATURE_FIXER
				OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
					IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						fWidth = 1.0
					ENDIF				
				#ENDIF
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@medium"

					MPRadioLocal.sRadioAnimClip = "action_a"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a"
					MPRadioLocal.sRadioAnimClipButton = "button_press"
					MPRadioLocal.sRadioAnimClipExit = "exit"	
				ENDIF
	
		
			BREAK
			CASE 1
				angledAreaLocate = MPRadioLocal.radioLocates[1]
				fWidth = 1.812500

				MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[1].vLoc
				MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[1].vRot.Z)
				
				MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
				MPRadioLocal.sRadioAnimClip = "action_a_living_room"
				
				MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
				MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
				MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
				MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
				IF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
					MPRadioLocal.sRadioAnimClip = "action_a_study"
					
					MPRadioLocal.sRadioAnimClipEnter = "enter_study"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_study"
					MPRadioLocal.sRadioAnimClipButton = "button_press_study"
					MPRadioLocal.sRadioAnimClipExit = "exit_study"
				ENDIF
				
				IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				OR MPRadioLocal.bInCasinoApartment
				OR MPRadioLocal.bInCasinoApartment2
				OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
				OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())	
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@medium"

					MPRadioLocal.sRadioAnimClip = "action_a"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a"
					MPRadioLocal.sRadioAnimClipButton = "button_press"
					MPRadioLocal.sRadioAnimClipExit = "exit"	
				ENDIF
				
				#IF FEATURE_FIXER
				IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())				
					fWidth = 1.0
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"

					MPRadioLocal.sRadioAnimClip = "action_a_bedroom"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter_bedroom"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_bedroom"
					MPRadioLocal.sRadioAnimClipButton = "button_press_bedroom"
					MPRadioLocal.sRadioAnimClipExit = "exit_bedroom"	
				ENDIF
				#ENDIF
								
			BREAK
			CASE 2
				angledAreaLocate = MPRadioLocal.radioLocates[2]
				fWidth = 1.812500

				MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[2].vLoc
				MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[2].vRot.Z)
				
				MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
				MPRadioLocal.sRadioAnimClip = "action_a_living_room"
				
				MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
				MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
				MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
				MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
				
				IF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
				OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
					
					MPRadioLocal.sRadioAnimClip = "action_a_study"	
								
					MPRadioLocal.sRadioAnimClipEnter = "enter_study"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_study"
					MPRadioLocal.sRadioAnimClipButton = "button_press_study"
					MPRadioLocal.sRadioAnimClipExit = "exit_study"
				ENDIF
				
				#IF FEATURE_FIXER
				IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())				
					fWidth = 0.75
					MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"

					MPRadioLocal.sRadioAnimClip = "action_a_bedroom"	
					
					MPRadioLocal.sRadioAnimClipEnter = "enter_bedroom"
					MPRadioLocal.sRadioAnimClipIdle = "idle_a_bedroom"
					MPRadioLocal.sRadioAnimClipButton = "button_press_bedroom"
					MPRadioLocal.sRadioAnimClipExit = "exit_bedroom"	
				ENDIF
				#ENDIF

			BREAK
			CASE 3
				angledAreaLocate = MPRadioLocal.radioLocates[3]
				fWidth = 1.812500

				MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[3].vLoc
				MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[3].vRot.Z)
				
				MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
				MPRadioLocal.sRadioAnimClip = "action_a_living_room"
				
				MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
				MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
				MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
				MPRadioLocal.sRadioAnimClipExit = "exit_living_room"				
			BREAK
			
			CASE 4
				angledAreaLocate = MPRadioLocal.radioLocates[4]
				fWidth = 1.812500

				MPRadioLocal.vRadioAnimPosition = MPRadioLocal.radioAnimations[4].vLoc
				MPRadioLocal.iRadioAnimHeading = ROUND(MPRadioLocal.radioAnimations[4].vRot.Z)
				
				MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@high_apment"
				MPRadioLocal.sRadioAnimClip = "action_a_living_room"
				
				MPRadioLocal.sRadioAnimClipEnter = "enter_living_room"
				MPRadioLocal.sRadioAnimClipIdle = "idle_a_living_room"
				MPRadioLocal.sRadioAnimClipButton = "button_press_living_room"
				MPRadioLocal.sRadioAnimClipExit = "exit_living_room"
				
			BREAK
		ENDSWITCH
//				PRINTLN("POD: radio: end of switch")
		IF angledAreaLocate.v0.x <> 0
		AND angledAreaLocate.v0.y <> 0
		AND angledAreaLocate.v0.z <> 0

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), angledAreaLocate.v0, angledAreaLocate.v1, fWidth)
			AND IS_PLAYER_FACING_RADIO(iRadioID)												
				g_bInRadioLocate = TRUE																
				RETURN TRUE
			ELSE	
				bIndependantRadio = FALSE
				g_bInRadioLocate = FALSE
				CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
				MPRadioLocal.vRadioAnimPosition = <<0,0,0>>
				MPRadioLocal.iRadioAnimHeading = 0
				MPRadioLocal.sRadioAnimDictName = ""

				MPRadioLocal.sRadioAnimClip = ""
				MPRadioLocal.sRadioAnimClipEnter = ""
				MPRadioLocal.sRadioAnimClipIdle = ""
				MPRadioLocal.sRadioAnimClipButton = ""
				MPRadioLocal.sRadioAnimClipExit = ""
			ENDIF
		ENDIF
		// Custom / stilt apartment garage radio set up 
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_10
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<231.841080,-975.634949,-100.089783>>, <<230.351517,-975.611084,-96.041504>>, 2.500000)
						MPRadioLocal.sRadioAnimDictName = "anim@mp_radio@garage@high"
						MPRadioLocal.iRadioAnimHeading = 0
						MPRadioLocal.vRadioAnimPosition = <<230.302 , -975.685, -98.979>>
						MPRadioLocal.sRadioAnimClip = "action_a"	
						
						MPRadioLocal.sRadioAnimClipEnter = "enter"
						MPRadioLocal.sRadioAnimClipIdle = "idle_a"
						MPRadioLocal.sRadioAnimClipButton = "button_press"
						MPRadioLocal.sRadioAnimClipExit = "exit"
						RETURN TRUE
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	

	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("RADIO IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE")
//	#ENDIF
//	#ENDIF
			
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the apartment radio should be preparing to play stripper music
/// PARAMS:
///    MPRadioClient - client data to query if in garage
///    MPRadioLocal - local data to check if stripper flag set
/// RETURNS:
///    true/false
FUNC BOOL MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)
	RETURN NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
			AND IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
ENDFUNC

FUNC BOOL USE_RADIO_ANIMATION(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)	
	IF MPRadioLocal.bOnYachtDeck = TRUE
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Client processes the local player approaching the radio unit and selecting to access radio control
/// PARAMS:
///    MPRadioClient - client data to query for client broadcast flags
///    MPRadioLocal - local data struct
///    property - property ID to check for radio activation
/// RETURNS:
///    true when radio activated
FUNC BOOL CLIENT_MAINTAIN_ACTIVATING_RADIO(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_PROPERTY_STRUCT &property, INT &iTempContextIntention, INT &iRadioSwitch, INT &iActivityRequested, MP_PROP_ACT_SERVER_CONTROL_STRUCT &control, BOOL bHeistRadioCheck = FALSE)
	CDEBUG2LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO ")
	BOOL bReadyForContext = FALSE
	PROPERTY_ACTIVITY_REQUEST_RESULT requestResult = MP_PROP_ACT_RESULT_IN_PROGRESS
//	INT iRadioCount
	
	INT localScene = 0
	
	IF bHeistRadioCheck
//		PRINTLN("POD: RADIO: CLIENT_MAINTAIN_ACTIVATING_RADIO: Heist iRadioSwitch: ", iRadioSwitch)
	ELSE
//		PRINTLN("POD: RADIO: CLIENT_MAINTAIN_ACTIVATING_RADIO: iRadioSwitch: ", iRadioSwitch)
	ENDIF
	
	IF iRadioSwitch > 0
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	ENDIF
	SWITCH iRadioSwitch
		CASE 0
			IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_MOVING_TO_RADIO)
			AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO)
			AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ENTER_ANIM)
			AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
			AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
//			#IF IS_DEBUG_BUILD
//			#IF SCRIPT_PROFILER_ACTIVE
//			ADD_SCRIPT_PROFILE_MARKER("RADIO ANIM BIT CHECKS")
//			#ENDIF
//			#ENDIF

			INT iNumberOfRadioUnits
			IF bHeistRadioCheck = FALSE
				iNumberOfRadioUnits = GET_MP_RADIO_NUMBER_OF_UNITS(MPRadioLocal, MPRadioClient, property)
				//CDEBUG3LN(DEBUG_RADIO, "bHeistRadioCheck = FALSE GET_MP_RADIO_NUMBER_OF_UNITS: ", iNumberOfRadioUnits)
			ELSE
				iNumberOfRadioUnits  = 2
			ENDIF
			
			//PRINTLN("POD: RADIO: CLIENT_MAINTAIN_ACTIVATING_RADIO: radio loop: ", iRadioCount, " iNumberOfRadioUnits: ", iNumberOfRadioUnits)
			IF bHeistRadioCheck = FALSE
				IF MPRadioLocal.iRadioStaggerCount > iNumberOfRadioUnits
					//CDEBUG3LN(DEBUG_RADIO, "MPRadioLocal.iRadioStaggerCount: ", MPRadioLocal.iRadioStaggerCount, " > iNumberOfRadioUnits: ", iNumberOfRadioUnits, ", Heist: " , bHeistRadioCheck)
					MPRadioLocal.iRadioStaggerCount = 0
				ENDIF
			ENDIF


			IF bHeistRadioCheck = TRUE
				MPRadioLocal.iRadioStaggerCountPreviousValue = MPRadioLocal.iRadioStaggerCount
				MPRadioLocal.iRadioStaggerCount = 2
			ENDIF
			
			//CDEBUG3LN(DEBUG_RADIO, "MPRadioLocal.iRadioStaggerCount = ", MPRadioLocal.iRadioStaggerCount, ", Heist: " , bHeistRadioCheck)
			#IF IS_DEBUG_BUILD
//			CDEBUG1LN(DEBUG_RADIO, "MPRadioLocal.iRadioStaggerCount = ", MPRadioLocal.iRadioStaggerCount)
			IF IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE(MPRadioClient, property, MPRadioLocal.iRadioStaggerCount, MPRadioLocal)
			AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
				CDEBUG1LN(DEBUG_RADIO, "in area MPRadioLocal.iRadioStaggerCount = ", MPRadioLocal.iRadioStaggerCount)
			ELSE
				//CDEBUG2LN(DEBUG_RADIO, "not in area MPRadioLocal.iRadioStaggerCount = ", MPRadioLocal.iRadioStaggerCount)
			ENDIF
			#ENDIF
		
			IF IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE(MPRadioClient, property, MPRadioLocal.iRadioStaggerCount, MPRadioLocal)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			AND Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) <= 9
			AND NOT IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_INSTANT_PASS_OUT)
			AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
			AND NOT g_SpawnData.bPassedOutDrunk
				IF IS_PROPERTY_ACTIVITY_FREE_AND_CAN_PLAYER_USE_IT(INT_TO_ENUM(ACTIVITY_ENUMS, (ENUM_TO_INT(RADIO_ENUM_1) + MPRadioLocal.iRadioStaggerCount)), MPRadioLocal.vRadioAnimPosition, control)
				AND NOT (MPRadioLocal.bInCasinoApartment2 AND IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE())
					//CDEBUG2LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_PROPERTY_ACTIVITY_FREE_AND_CAN_PLAYER_USE_IT: TRUE, radioEnumToInt:", (ENUM_TO_INT(RADIO_ENUM_1) + MPRadioLocal.iRadioStaggerCount), " radioLocation: ", MPRadioLocal.vRadioAnimPosition)
//					//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE: TRUE")
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("RADIO_NA_PP")
						CLEAR_HELP()
					ENDIF
					
					SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_IN_RADIO_ANGLED_AREA)
					IF NOT bReadyForContext
						//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT bReadyForContext: TRUE")
						IF NOT g_bCelebrationScreenIsActive 
						AND NOT IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
							//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT g_bCelebrationScreenIsActive : TRUE")
							//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT IS_AUDIO_SCENE_ACTIVE : TRUE")

							IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
							AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
							AND NOT IS_INTERACTION_MENU_OPEN()
							AND NOT MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MPRadioClient, MPRadioLocal)
							AND NOT IS_PAUSE_MENU_ACTIVE_EX()
							AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(PLAYER_ID())
								//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_NET_PLAYER_OK : TRUE etc")
							
								IF bHeistRadioCheck = TRUE
									PRINTLN("POD: RADIO: CLIENT_MAINTAIN_ACTIVATING_RADIO: IN HEIST ANGLED AREA radio loop: ", MPRadioLocal.iRadioStaggerCount, " iNumberOfRadioUnits: ", iNumberOfRadioUnits)
								ENDIF
								
								IF USE_RADIO_ANIMATION(MPRadioLocal)
								
//										PRINTLN("POD: RADIO: CLIENT_MAINTAIN_ACTIVATING_RADIO: IN HEIST ANGLED AREA radio loop: ", iRadioCount, " iNumberOfRadioUnits: ", iNumberOfRadioUnits)
									
									
									//IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
		//								PRINTLN("POD: RADIO: USING RADIO: YES ")
										REQUEST_ANIM_DICT(MPRadioLocal.sRadioAnimDictName)
			//							IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
										IF HAS_ANIM_DICT_LOADED(MPRadioLocal.sRadioAnimDictName)
											IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
												//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT GLOBAL_SPEC_BS_WATCHING_MP_TV: TRUE")
												#IF IS_DEBUG_BUILD
													PRINTLN("POD: RADIO: bReady for context")
												#ENDIF
												bReadyForContext = TRUE
											ELSE	
												//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT GLOBAL_SPEC_BS_WATCHING_MP_TV: FALSE")
											ENDIF
										ELSE
											REQUEST_ANIM_DICT(MPRadioLocal.sRadioAnimDictName)
										ENDIF
		//							ELSE
		//								PRINTLN("POD: RADIO: USING RADIO: NO")
		//							ENDIF

								ELSE

//									#IF NOT FEATURE_RADIO_ANIMS
										#IF IS_DEBUG_BUILD
											PRINTLN("POD: RADIO: bReady for context")
										#ENDIF
										bReadyForContext = TRUE
//									#ENDIF					
								ENDIF
							ELSE	
								IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_NET_PLAYER_OK : TRUE")
								ELSE
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_NET_PLAYER_OK : FALSE")
								ENDIF
								
								IF NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT biKY_AnimStarted: TRUE")
								ELSE
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT biKY_AnimStarted: FALSE")
								ENDIF
								
								IF NOT IS_INTERACTION_MENU_OPEN()
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT IS_INTERACTION_MENU_OPEN: TRUE")
								ELSE
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT IS_INTERACTION_MENU_OPEN: FALSE")
								ENDIF
								
								IF NOT MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MPRadioClient, MPRadioLocal)
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER: TRUE")
								ELSE
									//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER: FALSE")
								ENDIF
								
								//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_NET_PLAYER_OK : TRUE etc")
								
							ENDIF
						ELSE
							IF NOT g_bCelebrationScreenIsActive 
								//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT g_bCelebrationScreenIsActive : TRUE")
							ELSE
								//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT g_bCelebrationScreenIsActive : FALSE")
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("MP_POSITIONED_RADIO_MUTE_SCENE")
								//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT IS_AUDIO_SCENE_ACTIVE : TRUE")	
							ELSE
								//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT IS_AUDIO_SCENE_ACTIVE : FALSE")	
							ENDIF
							
						ENDIF
					ELSE	
						//CDEBUG3LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: NOT bReadyForContext: FALSE")
					ENDIF
				ELSE
					//CDEBUG2LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATING_RADIO: IS_PROPERTY_ACTIVITY_FREE_AND_CAN_PLAYER_USE_IT: FALSE")
					IF (MPRadioLocal.bInCasinoApartment2 AND IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE())
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("RADIO_NA_PP")
							PRINT_HELP("RADIO_NA_PP")
						ENDIF
					ENDIF
					
					INT iParticipant
						
					REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
						IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))

							PLAYER_INDEX PlayerId 
							PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
							IF PlayerID <> PLAYER_ID()
								IF IS_NET_PLAYER_OK(PlayerId)
									FLOAT tempDistance
									tempDistance = GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PlayerId), GET_PLAYER_COORDS(PLAYER_ID()) )
									#IF IS_DEBUG_BUILD
										PRINTLN("POD: GET_DISTANCE_BETWEEN_COORDS: ", tempDistance)
									#ENDIF
									
									IF tempDistance < 2
									AND tempDistance != 0
//										PRINT_HELP_NO_SOUND("POD_TOO_MANY")
									ENDIF
								ENDIF
							ENDIF

						ENDIF
					ENDREPEAT
				ENDIF
				
				IF bReadyForContext
					IF bInPropertyOwnerNotPaidLastUtilityBill
						PRINTLN("POD: RADIO: bInPropertyOwnerNotPaidLastUtilityBill TRUE")
						IF iTempContextIntention = NEW_CONTEXT_INTENTION
							REGISTER_CONTEXT_INTENTION(iTempContextIntention, CP_MEDIUM_PRIORITY, "MPRD_BILL")
						ENDIF
					ELSE
						PRINTLN("POD: RADIO: bInPropertyOwnerNotPaidLastUtilityBill FALSE")
						#IF IS_DEBUG_BUILD
	//						PRINTLN("POD: RADIO: paid last utility bill")
						#ENDIF
						
						IF iTempContextIntention = NEW_CONTEXT_INTENTION
							PRINTLN("POD: RADIO: iTempContextIntention = NEW_CONTEXT_INTENTION")
							REGISTER_CONTEXT_INTENTION(iTempContextIntention, CP_MEDIUM_PRIORITY, "MPRD_CTXT")
							#IF IS_DEBUG_BUILD
								PRINTLN("POD: RADIO: REGISTER NEW CONTEXT INTENTION: ", iTempContextIntention)
							#ENDIF
						ELSE
							PRINTLN("POD: RADIO: iTempContextIntention != NEW_CONTEXT_INTENTION")
							IF HAS_CONTEXT_BUTTON_TRIGGERED(iTempContextIntention)
								RELEASE_CONTEXT_INTENTION(iTempContextIntention)
								g_bRadioActTriggered = TRUE
								IF USE_RADIO_ANIMATION(MPRadioLocal)
		//								VECTOR rotTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClip, MPRadioLocal.vRadioAnimPosition, <<0, 0, MPRadioLocal.iRadioAnimHeading>>)
		//								VECTOR posTemp = GET_ANIM_INITIAL_OFFSET_POSITION(MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClip, MPRadioLocal.vRadioAnimPosition, <<0, 0, MPRadioLocal.iRadioAnimHeading>>)
		//								PRINTLN("POD: RADIO: anim coord destination: ", posTemp )
		//								VECTOR playerPosTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
		//								PRINTLN("POD: RADIO: player position: ", playerPosTemp )
		//								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClip, MPRadioLocal.vRadioAnimPosition, <<0, 0, MPRadioLocal.iRadioAnimHeading>>), 
		//															PEDMOVEBLENDRATIO_WALK, 5000, rotTemp.Z, 
		//															0.05)
										
										SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO)
										
										MPRadioClient.iActivatedRadio = MPRadioLocal.iRadioStaggerCount
										iRadioSwitch++
										PRINTLN("POD: Radio: player going to coords")
								ELSE
								
//									#IF NOT FEATURE_RADIO_ANIMS
										MPRadioClient.iActivatedRadio = MPRadioLocal.iRadioStaggerCount
										#IF IS_DEBUG_BUILD
										CPRINTLN(DEBUG_RADIO, "=== MP_RADIO CLIENT === MPRadioClient.iActivatedRadio = ", MPRadioLocal.iRadioStaggerCount)
										#ENDIF
										
										TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), GET_MP_RADIO_VECTOR(MPRadioLocal, MPRadioClient, property))
										TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_MP_RADIO_VECTOR(MPRadioLocal, MPRadioClient, property), -1)
										
										//CREATE_MP_RADIO_CAMERA(MPRadioClient, MPRadioLocal, property)
										SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
										SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
										RETURN TRUE
//									#ENDIF
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_IN_RADIO_ANGLED_AREA)
				IF bHeistRadioCheck = TRUE
					MPRadioLocal.iRadioStaggerCount = MPRadioLocal.iRadioStaggerCountPreviousValue 
//					//CDEBUG3LN(DEBUG_RADIO, "MPRadioLocal.iRadioStaggerCount: ", MPRadioLocal.iRadioStaggerCount, " = MPRadioLocal.iRadioStaggerCountPreviousValue: ", MPRadioLocal.iRadioStaggerCountPreviousValue, ", Heist: " , bHeistRadioCheck)
				ELSE
					MPRadioLocal.iRadioStaggerCount++	
				ENDIF
			ENDIF
			
			
			
			
			//ENDFOR
			
			ENDIF
//			
//			#IF IS_DEBUG_BUILD
//			#IF SCRIPT_PROFILER_ACTIVE
//			ADD_SCRIPT_PROFILE_MARKER("RADIO CONTEXT CHECKS")
//			#ENDIF
//			#ENDIF
		BREAK
	
		CASE 1
//			INT iTempDebugout 
//			iTempDebugout = ENUM_TO_INT(RADIO_ENUM_1) + MPRadioLocal.iRadioStaggerCount)
//			CDEBUG1LN(DEBUG_RADIO, "Activity request Id = ", iTempDebugout)
			IF REQUEST_PROPERTY_ACTIVITY(INT_TO_ENUM(ACTIVITY_ENUMS, (ENUM_TO_INT(RADIO_ENUM_1) + MPRadioLocal.iRadioStaggerCount)), requestResult, iActivityRequested, control)
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO)
				AND requestResult = MP_PROP_ACT_RESULT_SUCCESS
				OR MPRadioLocal.bOnYachtDeck
				AND requestResult = MP_PROP_ACT_RESULT_SUCCESS
					
					//IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE(MPRadioClient, property, MPRadioClient.iActivatedRadio)
					VECTOR rotTemp 
					rotTemp = GET_ANIM_INITIAL_OFFSET_ROTATION(MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClip, MPRadioLocal.vRadioAnimPosition, <<0, 0, MPRadioLocal.iRadioAnimHeading>>)
					VECTOR posTemp 
					posTemp = GET_ANIM_INITIAL_OFFSET_POSITION(MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClip, MPRadioLocal.vRadioAnimPosition, <<0, 0, MPRadioLocal.iRadioAnimHeading>>)
					PRINTLN("POD: RADIO: anim coord destination: ", posTemp, " iActivatedRadio: ", MPRadioClient.iActivatedRadio)
					VECTOR playerPosTemp 
					playerPosTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
					PRINTLN("POD: RADIO: player position: ", playerPosTemp )
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClip, MPRadioLocal.vRadioAnimPosition, <<0, 0, MPRadioLocal.iRadioAnimHeading>>), 
												PEDMOVEBLENDRATIO_WALK, 2500, rotTemp.Z, 
												0.10)

					CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO)
					SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_MOVING_TO_RADIO)
					iRadioSwitch++
				ELSE
					IF requestResult = MP_PROP_ACT_RESULT_IN_USE
						CDEBUG1LN(DEBUG_RADIO, "requestResult = MP_PROP_ACT_RESULT_IN_USE")
						CLEAR_REQUEST_PROPERTY_ACTIVITY(iActivityRequested)
						CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO)
						CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
						CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
						iRadioSwitch = 0
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_RADIO, "REQUEST_PROPERTY_ACTIVITY = FALSE")
			ENDIF
			
		BREAK

		CASE 2
		//	#IF IS_DEBUG_BUILD
			
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_MOVING_TO_RADIO)
				
				// Prevent player running around and starting other activites while in this task
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CONTEXT)
				
				//IS_PLAYER_IN_ANGLED_AREA_TO_ACTIVATE(MPRadioClient, property, MPRadioClient.iActivatedRadio)
				PRINTLN("POD: Radio: player not at coords yet")
		//		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					PRINTLN("POD: Radio: player at coords")
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "POD === MP_RADIO CLIENT === MPRadioClient.iActivatedRadio = ", MPRadioClient.iActivatedRadio)
					#ENDIF
					
					SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ENTER_ANIM)
					
					//--
					MPRadioLocal.iRadioNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(MPRadioLocal.vRadioAnimPosition, <<0,0,MPRadioLocal.iRadioAnimHeading>>, DEFAULT, TRUE)
					#IF IS_DEBUG_BUILD	
					println("POD: RADIO: ANIM ENTER: PLAYER added to Net Sync Scene, ANIM_DICT: ", MPRadioLocal.sRadioAnimDictName, ", ANIM_NAME: ", MPRadioLocal.sRadioAnimClipEnter)
					#ENDIF
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), MPRadioLocal.iRadioNetSceneID, MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClipEnter, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
					#IF IS_DEBUG_BUILD
					println("POD: RADIO: START net sync scene, SCENE_ID: " , MPRadioLocal.iRadioNetSceneID)
					#ENDIF
					NETWORK_START_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
					//
					
					CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_MOVING_TO_RADIO)
					iRadioSwitch++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ENTER_ANIM)
				localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MPRadioLocal.iRadioNetSceneID)
				IF localScene != -1
				AND IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
					println("POD: RADIO: ANIM ENTER: waiting for enter anim to finish: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))
					IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.9
						println("POD: RADIO: ANIM ENTER: NET SCENE FINISHED - phase greater than 0.990")

						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === MPRadioClient.iActivatedRadio = ", MPRadioClient.iActivatedRadio)
						#ENDIF
						
						IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
							SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_TRIGGER_HEIST_RADIO)
						ENDIF
						
						SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
						SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
						CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ENTER_ANIM)
						
						//
						MPRadioLocal.iRadioNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(MPRadioLocal.vRadioAnimPosition, <<0,0,MPRadioLocal.iRadioAnimHeading>>, DEFAULT, FALSE, TRUE)
						NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), MPRadioLocal.iRadioNetSceneID, MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClipIdle, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
						NETWORK_START_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
						iRadioSwitch = 0
						//
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	IF NOT bReadyForContext
		RELEASE_CONTEXT_INTENTION(iTempContextIntention)
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Controls the local client requesting to change the radio station and volume on an activated radio
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    MPRadioClient - client broadcast data struct
///    MPRadioLocal - local data struct
/// RETURNS:
///    true when deactivating the radio
FUNC BOOL CLIENT_MAINTAIN_MP_RADIO_INPUT(	MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, 
											MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, 
											MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, 
											INT &iInputBit, 
											BOOL bHeistRadioCheck = FALSE)

	IF USE_RADIO_ANIMATION(MPRadioLocal)
		INT localScene = 0
		
		IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
			localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MPRadioLocal.iRadioNetSceneID)
			PRINTLN("POD: RADIO: PLAY: END ANIM: localScene: ", MPRadioLocal.iRadioNetSceneID)
			
			IF localScene != -1
			AND IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
				PRINTLN("POD: RADIO: PLAY: END ANIM: GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.9
					PRINTLN("POD: RADIO: PLAY: TURNING OFF")
					CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
					// Clearing the idle bit causes the idel animation to start playing again
					MPRadioLocal.iRadioNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(MPRadioLocal.vRadioAnimPosition, <<0,0,MPRadioLocal.iRadioAnimHeading>>, DEFAULT, FALSE, TRUE)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), MPRadioLocal.iRadioNetSceneID, MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClipIdle, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
					NETWORK_START_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	
		// PC uses different input for turning on/off the radio.
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			MPRadioLocal.caRadioOnOffInput = INPUT_CONTEXT
		ELSE
			MPRadioLocal.caRadioOnOffInput = INPUT_SCRIPT_RUP
		ENDIF
	
		IF NOT bHeistRadioCheck
			IF IS_BIT_SET(iInputBit, MP_RADIO_INPUT_POWER_ON)
				IF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_ON
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iInputBit, MP_RADIO_INPUT_POWER_OFF)
				IF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_OFF//get heist server stage
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
				IF GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_OFF
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
				IF GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_ON
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
				ENDIF
			ENDIF
		
		ENDIF
		
		
		IF MPRadioClient.iTimeOfInputStationID <> GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
			CLEAR_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)
			CLEAR_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)
			#IF FEATURE_TUNER
			CLEAR_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)
			#ENDIF
			MPRadioClient.iTimeOfInputStationID = GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, 	"=== MP_RADIO CLIENT === New station ", GET_RADIO_STATION_NAME(GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)), 
										" seen, resetting station change requests.")
			#ENDIF
		ENDIF
		
		
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === CLIENT_MAINTAIN_MP_TV_INPUT = TRUE")
			#ENDIF
			SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
			
			CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_TRIGGER_HEIST_RADIO)
			
			RETURN TRUE
		ELSE
	
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, MPRadioLocal.caRadioOnOffInput)
			#IF FEATURE_TUNER
			OR (MPRadioLocal.piApartmentOwner = PLAYER_ID() AND HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT))
			#ENDIF
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)
				#IF FEATURE_TUNER
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)
				#ENDIF
				IF bHeistRadioCheck = FALSE
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)
				ELSE
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
					CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
				ENDIF
//				#IF IS_DEBUG_BUILD
				IF USE_RADIO_ANIMATION(MPRadioLocal)
					
					#IF IS_DEBUG_BUILD	
					println("POD: RADIO: ANIM is anim playing bit: " , IS_BIT_SET(iInputBit, MP_RADIO_CLIENT_BS_PLAYING_ANIM))
					#ENDIF
					
					IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
						PRINTLN("POD: RADIO: ANIM: MPRadioLocal.iRadioNetSceneID: ", MPRadioLocal.iRadioNetSceneID,", MPRadioLocal.sRadioAnimDictName: ", MPRadioLocal.sRadioAnimDictName,", MPRadioLocal.sRadioAnimClip: ", MPRadioLocal.sRadioAnimClipButton, ", MPRadioLocal.vRadioAnimPosition: ", MPRadioLocal.vRadioAnimPosition, ", MPRadioLocal.iRadioAnimHeading: ", MPRadioLocal.iRadioAnimHeading)
						// only play the button press animation for the station change if the radio is on
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
						#IF FEATURE_TUNER
						OR (MPRadioLocal.piApartmentOwner = PLAYER_ID() AND HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT))
						#ENDIF
							IF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_ON
							OR GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_ON
								#IF IS_DEBUG_BUILD
								println("POD: RADIO: ANIM: MPRadioLocal.vRadioAnimPosition: " , MPRadioLocal.vRadioAnimPosition)
								println("POD: RADIO: ANIM: CREATE net sync scene, SCENE_ID: " , MPRadioLocal.iRadioNetSceneID)
								println("POD: RADIO: ANIM: CREATE net sync scene, MPRadioLocal.iRadioAnimHeading: " , MPRadioLocal.iRadioAnimHeading)
								println("POD: RADIO: ANIM: PLAY:4 MP_RADIO_CLIENT_BS_PLAYING_ANIM = FALSE")
								#ENDIF
								
								MPRadioLocal.iRadioNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(MPRadioLocal.vRadioAnimPosition, <<0,0,MPRadioLocal.iRadioAnimHeading>>, DEFAULT, TRUE)
								NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), MPRadioLocal.iRadioNetSceneID, MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClipButton, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
								NETWORK_START_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
								//CLEAR_BIT(iInputBit, MP_RADIO_CLIENT_BS_IDLE_ANIM_PLAYING)
								SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							println("POD: RADIO: ANIM: MPRadioLocal.vRadioAnimPosition: " , MPRadioLocal.vRadioAnimPosition)
							println("POD: RADIO: ANIM: CREATE net sync scene, SCENE_ID: " , MPRadioLocal.iRadioNetSceneID)
							println("POD: RADIO: ANIM: CREATE net sync scene, MPRadioLocal.iRadioAnimHeading: " , MPRadioLocal.iRadioAnimHeading)
							println("POD: RADIO: ANIM: PLAY:4 MP_RADIO_CLIENT_BS_PLAYING_ANIM = FALSE")
							#ENDIF
							
							MPRadioLocal.iRadioNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(MPRadioLocal.vRadioAnimPosition, <<0,0,MPRadioLocal.iRadioAnimHeading>>, DEFAULT, TRUE)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), MPRadioLocal.iRadioNetSceneID, MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClipButton, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)
							NETWORK_START_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
							//CLEAR_BIT(iInputBit, MP_RADIO_CLIENT_BS_IDLE_ANIM_PLAYING)
							SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
						ENDIF
					ENDIF
					

				ENDIF
//				#ENDIF
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					SET_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)
					#IF IS_DEBUG_BUILD
					println("POD: RADIO: ANIM: INPUT_FRONTEND_RIGHT")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === MPRadioClient.iTimeOfInputStationID = ", GET_RADIO_STATION_NAME(MPRadioClient.iTimeOfInputStationID))
					#ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_LEFT)
					SET_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)
					#IF IS_DEBUG_BUILD
					println("POD: RADIO: ANIM: INPUT_FRONTEND_LEFT")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === MPRadioClient.iTimeOfInputStationID = ", GET_RADIO_STATION_NAME(MPRadioClient.iTimeOfInputStationID))
					#ENDIF
				#IF FEATURE_TUNER
				ELIF (MPRadioLocal.piApartmentOwner = PLAYER_ID() AND HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT))
					SET_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)
					#IF IS_DEBUG_BUILD
					PRINTLN("POD: RADIO: ANIM: INPUT_SCRIPT_RLEFT")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === MPRadioClient.iTimeOfInputStationID = ", GET_RADIO_STATION_NAME(MPRadioClient.iTimeOfInputStationID))
					#ENDIF
				#ENDIF
				ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, MPRadioLocal.caRadioOnOffInput)
					println("POD: RADIO: ANIM: INPUT_FRONTEND_Y")
					//CDEBUG2LN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === Y pressed: bHeistRadioCheck: ", bHeistRadioCheck)
					IF bHeistRadioCheck = FALSE
						IF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_OFF
												
								SET_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)
								#IF IS_DEBUG_BUILD
								//CDEBUG2LN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)")
								CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)")
								#ENDIF

						ELIF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_ON
							
	//							#IF FEATURE_RADIO_ANIMS
	//								IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = 157260863
	//								OR GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = 1232071087
	//									SET_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
	//									#IF IS_DEBUG_BUILD
	//									PRINTLN("POD: RADIO: MP_RADIO_INPUT_HEIST_POWER_OFF")
	//									#ENDIF
	//								ENDIF
	//							#ENDIF
								
								SET_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)")
								#ENDIF
						ENDIF	
					ELSE
//						#IF IS_DEBUG_BUILD
//						CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === bHeistRadioCheck = TRUE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!)")
//						SCRIPT_ASSERT("POD: bHeistRadioCheck = TRUE")
//						#ENDIF
						IF GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_OFF
							//IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
								SET_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)")
								#ENDIF
							//ENDIF
							
						ELIF GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_ON
							//IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
								SET_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
								#IF IS_DEBUG_BUILD
								CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === SET_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)")
								#ENDIF
							//ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			/*IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
				INCREMENT_MP_RADIO_VOLUME(MPRadioLocal, TRUE)
			ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
				INCREMENT_MP_RADIO_VOLUME(MPRadioLocal, FALSE)
			ELSE
				RESET_INCREMENT_MP_RADIO_VOLUME(MPRadioLocal)
			ENDIF*/
			IF bHeistRadioCheck = FALSE
				IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
				OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPRadioLocal.scaleformInstructionalButtons)
					
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT), 
														"HUD_INPUT3", MPRadioLocal.scaleformInstructionalButtons)//BACK
				
					IF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_ON
					
						#IF FEATURE_TUNER
						IF MPRadioLocal.piApartmentOwner = PLAYER_ID()
						AND HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT), 
															"JBOX_PLIST_9", MPRadioLocal.scaleformInstructionalButtons)//Audio Player
						ENDIF
						#ENDIF
						
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_SCRIPT_DPAD_LR), 
															"HUD_INPUT80", MPRadioLocal.scaleformInstructionalButtons)//SELECT STATION
					
						//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), 
						//										"HUD_INPUT77", MPRadioLocal.scaleformInstructionalButtons)//CHANGE VOLUME
						
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, MPRadioLocal.caRadioOnOffInput), 
															"HUD_INPUT82", MPRadioLocal.scaleformInstructionalButtons)//TURN OFF
						
					ELIF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_OFF
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, MPRadioLocal.caRadioOnOffInput), 
															"HUD_INPUT81", MPRadioLocal.scaleformInstructionalButtons)//TURN ON
					ENDIF
					
					CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
				ENDIF	
			ELSE
				IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPRadioLocal.scaleformInstructionalButtons)
					
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT), 
														"HUD_INPUT3", MPRadioLocal.scaleformInstructionalButtons)//BACK
				
					IF GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_ON
					
						#IF FEATURE_TUNER
						IF MPRadioLocal.piApartmentOwner = PLAYER_ID()
						AND HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES()
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT), 
															"JBOX_PLIST_9", MPRadioLocal.scaleformInstructionalButtons)//Audio Player
						ENDIF
						#ENDIF
						
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_SCRIPT_DPAD_LR), 
															"HUD_INPUT80", MPRadioLocal.scaleformInstructionalButtons)//SELECT STATION
					
						//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), 
						//										"HUD_INPUT77", MPRadioLocal.scaleformInstructionalButtons)//CHANGE VOLUME
						
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, MPRadioLocal.caRadioOnOffInput), 
															"HUD_INPUT82", MPRadioLocal.scaleformInstructionalButtons)//TURN OFF
						
					ELIF GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_OFF
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, MPRadioLocal.caRadioOnOffInput), 
															"HUD_INPUT81", MPRadioLocal.scaleformInstructionalButtons)//TURN ON
					ENDIF
					
					CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
				ENDIF	
			ENDIF
			
			SPRITE_PLACEMENT thisSpritePlacement = 	GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
			
			SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(MPRadioLocal.scaleformInstructionalButtons, 1.0)
			
			RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPRadioLocal.sfButton, 
												thisSpritePlacement, 
												MPRadioLocal.scaleformInstructionalButtons, 
												SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPRadioLocal.scaleformInstructionalButtons))
			
			SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Performs every-frame processing for local client for when the radio has been activated
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    MPRadioClient - client broadcast data struct
///    MPRadioLocal - local data struct
/// RETURNS:
///    true when player no longer activated radio
FUNC BOOL CLIENT_MAINTAIN_ACTIVATED_RADIO(	MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, 
											MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient,
											MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, 
											INT &iInputBit, 
											INT &iActivityRequested, 
											BOOL bRadioHeistCheck = FALSE )
												
	INT localScene = 0
//	INT iInputBit
//	IF bHeistRadio = TRUE
//		iInputBit = MPRadioClient.iHeistInputBitset
//	ELSE
//		iInputBit = MPRadioClient.iInputBitset
//	ENDIF
	
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()

	DISPLAY_AMMO_THIS_FRAME(FALSE)			
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)


	HIDE_HUD_AND_RADAR_THIS_FRAME()
	/*HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	HIDE_HELP_TEXT_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()*/

	DISABLE_MP_RADIO_CONTROL_ACTIONS_GENERAL(MPRadioLocal, MPRadioLocal.caRadioOnOffInput)
	
	IF USE_RADIO_ANIMATION(MPRadioLocal)
			IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_EXIT_ANIM)
				IF CLIENT_MAINTAIN_MP_RADIO_INPUT(MPRadioServer, MPRadioClient, MPRadioLocal, iInputBit, bRadioHeistCheck)
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === CLIENT_MAINTAIN_ACTIVATED_RADIO returning true")
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === MPRadioClient.iActivatedRadio = -1")
					

					MPRadioLocal.iRadioNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(MPRadioLocal.vRadioAnimPosition, <<0,0,MPRadioLocal.iRadioAnimHeading>>, DEFAULT, FALSE)
					PRINTLN("POD: RADIO: ANIM IDLE: MPRadioLocal.iRadioNetSceneID: ", MPRadioLocal.iRadioNetSceneID,", MPRadioLocal.sRadioAnimDictName: ", MPRadioLocal.sRadioAnimDictName,", MPRadioLocal.sRadioAnimClip: ", MPRadioLocal.sRadioAnimClipExit, ", MPRadioLocal.vRadioAnimPosition: ", MPRadioLocal.vRadioAnimPosition, ", MPRadioLocal.iRadioAnimHeading: ", MPRadioLocal.iRadioAnimHeading)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), MPRadioLocal.iRadioNetSceneID, MPRadioLocal.sRadioAnimDictName, MPRadioLocal.sRadioAnimClipExit, REALLY_SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT  | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT, REALLY_SLOW_BLEND_IN)

					NETWORK_START_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)

					SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_EXIT_ANIM)

				ENDIF
			ENDIF
			
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_EXIT_ANIM)
				CDEBUG1LN(DEBUG_RADIO, "CLIENT_MAINTAIN_ACTIVATED_RADIO: CLEAR_REQUEST_PROPERTY_ACTIVITY(iActivityRequested)")
				CLEAR_REQUEST_PROPERTY_ACTIVITY(iActivityRequested)
				localScene  = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(MPRadioLocal.iRadioNetSceneID)
				IF localScene != -1
				AND IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
					PRINTLN("POD: RADIO: ANIM: EXIT ANIM: localScene: ", MPRadioLocal.iRadioNetSceneID)
					PRINTLN("POD: RADIO: ANIM: EXIT ANIM: GET_SYNCHRONIZED_SCENE_PHASE: ", GET_SYNCHRONIZED_SCENE_PHASE(localScene))
					IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.35
							#IF IS_DEBUG_BUILD
								PRINTLN("POD: RADIO: ANIM: EXIT ANIM: >= 0.35 ")
							#ENDIF
							
							PRINTLN("RADIO: ANIMATION: PLAY: TURNING OFF")
							NETWORK_STOP_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
							CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
							CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_EXIT_ANIM)
							
							MPRadioClient.iActivatedRadio = -1
							CLEAR_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)
							CLEAR_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)
							#IF FEATURE_TUNER
							CLEAR_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)
							#ENDIF
							
							IF bRadioHeistCheck = TRUE
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
							ELSE
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)
							ENDIF
							
							RETURN TRUE
						ENDIF
					ELSE
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.90
							#IF IS_DEBUG_BUILD
								PRINTLN("POD: RADIO: ANIM: EXIT ANIM: >= 0.90 ")
							#ENDIF
							PRINTLN("RADIO: ANIMATION: PLAY: TURNING OFF")
							NETWORK_STOP_SYNCHRONISED_SCENE(MPRadioLocal.iRadioNetSceneID)
							CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PLAYING_ANIM)
							CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_EXIT_ANIM)
							
							MPRadioClient.iActivatedRadio = -1
							CLEAR_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)
							CLEAR_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)
							#IF FEATURE_TUNER
							CLEAR_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)
							#ENDIF
							
							IF bRadioHeistCheck = TRUE
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
							ELSE
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)
								CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)
							ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	ELSE
	
//		#IF NOT FEATURE_RADIO_ANIMS
		IF CLIENT_MAINTAIN_MP_RADIO_INPUT(MPRadioServer, MPRadioClient, MPRadioLocal, iInputBit, bRadioHeistCheck)
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === CLIENT_MAINTAIN_ACTIVATED_RADIO returning true")
			#ENDIF
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === MPRadioClient.iActivatedRadio = -1")
			#ENDIF
			MPRadioClient.iActivatedRadio = -1
			CLEAR_BIT(iInputBit, MP_RADIO_INPUT_NEXT_CHANNEL)
			CLEAR_BIT(iInputBit, MP_RADIO_INPUT_PREVIOUS_CHANNEL)
			#IF FEATURE_TUNER
			CLEAR_BIT(iInputBit, MP_RADIO_INPUT_AUDIO_PLAYER)
			#ENDIF
				
			CLEAR_REQUEST_PROPERTY_ACTIVITY(iActivityRequested)
			IF bRadioHeistCheck = TRUE
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_ON)
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_HEIST_POWER_OFF)
			ELSE
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_ON)
				CLEAR_BIT(iInputBit, MP_RADIO_INPUT_POWER_OFF)
			ENDIF
			
			RETURN TRUE
		ENDIF
		
//		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Ability to disable specific property emitters
///    *when moving into garage from a property the property emitters do not get disabled.
 PROC DISABLE_PROPERTY_EMITTERS(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, INT iProperty, INT iRoomID = 1, BOOL bWarehouse = FALSE, INT iWarehouseSize = 0 , BOOL bFactory = FALSE, INT iFactoryGoodsCate = 0)
 	STRING sEmitterName = ""
	
	IF GET_BASE_PROPERTY_FROM_PROPERTY(iProperty) = PROPERTY_OFFICE_2_BASE
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_01a_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_01b_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_01c_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_02a_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_02b_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_02c_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_03a_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_03b_Radio_01", FALSE)
		SET_STATIC_EMITTER_ENABLED("SE_ex_int_office_03c_Radio_01", FALSE)
		EXIT
 	ENDIF
 
 	sEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, iProperty, iRoomID, bWarehouse, iWarehouseSize, bFactory, iFactoryGoodsCate)
	IF NOT IS_STRING_NULL_OR_EMPTY(sEmitterName)	
		SET_STATIC_EMITTER_ENABLED(sEmitterName, FALSE)
	ENDIF
	
 ENDPROC

///// PURPOSE:
/////    Sets the audio emitters for a radio to a specified radio station
///// PARAMS:
/////    MPRadioClient - client broadcast data struct
/////    MPRadioLocal - local data struct
/////    property - property ID of current property
/////    bOnOff - to turn the radio on or off (true on)
/////    iStation - ID of the station to set the radio to
//PROC SET_MP_RADIO_LOCAL_STATION(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_PROPERTY_STRUCT &property, BOOL bOnOff, INT iStation = -1)
//	INT iEmitterCount
//	
//	STRING sStationName
//	BOOL bPower
//	
//	SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
//	
//	
//	
//	REPEAT GET_MP_RADIO_NUMBER_OF_EMITTERS(property) iEmitterCount
//		SET_STATIC_EMITTER_ENABLED(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), bPower)
//		IF bOnOff
//			SET_EMITTER_RADIO_STATION(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), sStationName)
//		ENDIF
//	ENDREPEAT
//	
//	CASE PROPERTY_BUS_HIGH_APT_1
//					SWITCH iEmitterID
//						CASE 0 RETURN "SE_MP_APT_NEW_4_2" BREAK
//						CASE 1 RETURN "SE_MP_APT_NEW_4_1" BREAK
//					ENDSWITCH
//				BREAK
//	
//	SET_STATIC_EMITTER_ENABLED(SE_MP_APT_NEW_4_2, bPower)
//	
//		SET_EMITTER_RADIO_STATION(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), sStationName)
//	
//	ENDREPEAT
//	
//ENDPROC

/// PURPOSE:
///    Sets the audio emitters for a radio to a specified radio station
/// PARAMS:
///    MPRadioClient - client broadcast data struct
///    MPRadioLocal - local data struct
///    property - property ID of current property
///    bOnOff - to turn the radio on or off (true on)
///    iStation - ID of the station to set the radio to
PROC SET_MP_RADIO_LOCAL_STATION(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_PROPERTY_STRUCT &property, BOOL bOnOff, INT iStation = -1, BOOL bHeistRadio = FALSE)
	INT iEmitterCount
	
	STRING sStationName
	BOOL bPower
	
	SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
	
	IF MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MPRadioClient, MPRadioLocal)
		sStationName = "HIDDEN_RADIO_STRIP_CLUB"
		bPower = TRUE
	ELSE
		sStationName = GET_RADIO_STATION_NAME(iStation)
		bPower = bOnOff
	ENDIF
	
	CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: bPower set to ", bPower)
	
	IF bHeistRadio
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: changing heist radio station to: ", sStationName, ", ID: ", iStation)
	ELSE
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: changing radio station to: ", sStationName, ", ID: ", iStation)
	ENDIF
	
	GET_MP_RADIO_NUMBER_OF_EMITTERS(property)
	
	#IF IS_DEBUG_BUILD
		//PRINTLN("POD: RADIO: GET_MP_RADIO_NUMBER_OF_EMITTERS: ", GET_MP_RADIO_NUMBER_OF_EMITTERS(property))
	#ENDIF
	
	IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bar", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Yacht_Bar turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Bar, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Bar", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Bar TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Yacht_Bar") 
		
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom", bPower)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_02", bPower)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_03", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Yacht_Bedroom turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Bedroom, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Bedroom", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Bedroom_02", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Bedroom_03", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Bedroom TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Yacht_Bedroom") 
	ELIF MPRadioLocal.bOnYachtDeck
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_01", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Yacht_Exterior_01 turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_01, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Exterior_01", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_01 TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Yacht_Exterior_01") 
		
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_02", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Yacht_Exterior_02 turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_02, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Exterior_02", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_02 TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Yacht_Exterior_02") 
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_03", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Yacht_Exterior_03 turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_03, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Exterior_03", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_03 TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Yacht_Exterior_03")
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_04", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Yacht_Exterior_04 turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_04, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Yacht_Exterior_04", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Yacht_Exterior_04 TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Yacht_Exterior_04")
		
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
		IF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_5_BASE_A)	
			
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_A_Bedroom", bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Stilts_A_Bedroom turned: ", bPower)
			IF bOnOff
				//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_A_Bedroom, station: ", sStationName)
				SET_EMITTER_RADIO_STATION("SE_DLC_APT_Stilts_A_Bedroom", sStationName)
			ELSE	
				//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_A_Bedroom TURNING: ", bPower)
			ENDIF
			//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Stilts_A_Bedroom") 
			
			
			
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_A_Heist_Room", bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Stilts_A_Heist_Room turned: ", bPower)
			IF bOnOff
				//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_A_Heist_Room, station: ", sStationName)
				SET_EMITTER_RADIO_STATION("SE_DLC_APT_Stilts_A_Heist_Room", sStationName)
			ELSE	
				//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_A_Heist_Room TURNING: ", bPower)
			ENDIF
			//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Stilts_A_Heist_Room") 
			
			
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_A_Living_Room", bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Stilts_A_Living_Room turned: ", bPower)
			IF bOnOff
				//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_A_Living_Room, station: ", sStationName)
				SET_EMITTER_RADIO_STATION("SE_DLC_APT_Stilts_A_Living_Room", sStationName)
			ELSE	
				//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_A_Living_Room TURNING: ", bPower)
			ENDIF
			//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Stilts_A_Living_Room") 
		
		ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_1_BASE_B)	
		
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_B_Bedroom", bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Stilts_B_Bedroom turned: ", bPower)
			IF bOnOff
				//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_B_Bedroom, station: ", sStationName)
				SET_EMITTER_RADIO_STATION("SE_DLC_APT_Stilts_B_Bedroom", sStationName)
			ELSE	
				//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_B_Bedroom TURNING: ", bPower)
			ENDIF
			//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Stilts_B_Bedroom") 
			
			
//			IF bHeistRadio = TRUE
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_B_Heist_Room", bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Stilts_B_Heist_Room turned: ", bPower)
			IF bOnOff
				//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_B_Heist_Room, station: ", sStationName)
				SET_EMITTER_RADIO_STATION("SE_DLC_APT_Stilts_B_Heist_Room", sStationName)
			ELSE	
				//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_B_Heist_Room TURNING: ", bPower)
			ENDIF
			//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Stilts_B_Heist_Room") 
//			ENDIF
			
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Stilts_B_Living_Room", bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Stilts_B_Living_Room turned: ", bPower)
			IF bOnOff
				//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_B_Living_Room, station: ", sStationName)
				SET_EMITTER_RADIO_STATION("SE_DLC_APT_Stilts_B_Living_Room", sStationName)
			ELSE	
				//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Stilts_B_Living_Room TURNING: ", bPower)
			ENDIF
			//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Stilts_B_Living_Room") 
		ENDIF
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Custom_Bedroom", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Custom_Bedroom turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Custom_Bedroom", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Custom_Bedroom") 
		
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Custom_Heist_Room", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Custom_Heist_Room turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Heist_Room, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Custom_Heist_Room", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Heist_Room TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Custom_Heist_Room") 
		
		SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Custom_Living_Room", bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter SE_DLC_APT_Custom_Living_Room turned: ", bPower)
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Living_Room, station: ", sStationName)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Custom_Living_Room", sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Living_Room TURNING: ", bPower)
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: SE_DLC_APT_Custom_Living_Room") 
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom, station: ", sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex), sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED:  TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex)) 
	ELIF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioLocal.iWarehouseSize), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioLocal.iWarehouseSize))
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom, station: ", sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioLocal.iWarehouseSize), sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED:  TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex)) 
	ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex) AND NOT IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4), bPower)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5), FALSE)
		DISABLE_PROPERTY_EMITTERS(MPRadioLocal, PROPERTY_OFFICE_2_BASE, 1) //disable office emitters
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: OfficeGarage - Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4))
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4), sStationName)
		ENDIF
	ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex) AND IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5), bPower)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4), FALSE)
		DISABLE_PROPERTY_EMITTERS(MPRadioLocal, PROPERTY_OFFICE_2_BASE, 1) //disable office emitters
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: OfficeModShop - Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5))
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5), sStationName)
		ENDIF
	ELIF IS_PLAYER_IN_FACTORY(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, TRUE, MPRadioLocal.iFactoryType), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, TRUE, MPRadioLocal.iFactoryType))
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom, station: ", sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, TRUE, MPRadioLocal.iFactoryType), sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED:  TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
		ENDIF
		//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex)) 
	ELIF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, FALSE, 0, TRUE), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, FALSE, 0, TRUE))
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom, station: ", sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, FALSE, 0, TRUE), sStationName)
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED:  TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
		ENDIF
	
	ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		

		
		IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(MPRadioLocal.piApartmentOwner)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0))
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1))
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), bPower)
		ENDIF
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0))
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1))
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 2))
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 2), bPower)
		CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3))
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3), bPower)
		
//		IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(MPRadioLocal.piApartmentOwner)
//			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0))
//			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bOnOff)
//		ENDIF
		IF bOnOff
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: SE_DLC_APT_Custom_Bedroom, station: ", sStationName)
			IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(MPRadioLocal.piApartmentOwner)
				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), sStationName)
			ENDIF
			
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 2), sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 3), sStationName)
			
			// Modshop radio should always play LS Rock Radio
//			IF IS_PLAYER_HANGAR_MODSHOP_PURCHASED(MPRadioLocal.piApartmentOwner)
//				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
//			ENDIF
		ELSE	
			//CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED:  TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex))
		ENDIF
	ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		IF USE_DEFUNCT_BASE_QUARTERS_RADIO()
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Quarters? Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0))
		ENDIF
		IF NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn)
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), bPower)
			CDEBUG1LN(DEBUG_RADIO, "SET_MP_RADIO_LOCAL_STATION: Lounge? Static emitter turned: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1))
		ENDIF
		
		IF bOnOff
			IF USE_DEFUNCT_BASE_QUARTERS_RADIO()
				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
				CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: Quarters? TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0))
			ENDIF
			IF NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn)
				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), sStationName)
				CDEBUG2LN(DEBUG_RADIO, "SET_STATIC_EMITTER_ENABLED: Lounge? TURNING: ", bPower, " emitter = ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1))
			ENDIF
		ENDIF
	ELIF MPRadioLocal.bInCasinoApartment
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bPower)
		IF NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE()
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), bPower)
		ENDIF
		
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
			IF NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE()
				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), sStationName)
			ENDIF
		ENDIF
	ELIF MPRadioLocal.bInCasinoApartment2
		IF NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE()
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bPower)
		ENDIF
		
		IF bOnOff
			IF NOT IS_PLAYER_IN_SIMPLE_INTERIOR_WITH_PARTY_MODE_ACTIVE()
				SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
			ENDIF
		ENDIF
	ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), bPower)
		
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), sStationName)
		ENDIF
	ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), bPower)
		
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), sStationName)
		ENDIF
	ELIF IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), bPower)
		
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), sStationName)
		ENDIF
	#IF FEATURE_DLC_1_2022
	ELIF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), bPower)
		
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1), sStationName)
		ENDIF
	#ENDIF
	#IF FEATURE_FIXER
	ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), bPower)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), bPower)
		SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 2), bPower)
		
		IF bOnOff
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 0), sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 1), sStationName)
			SET_EMITTER_RADIO_STATION(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, -1, -1, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, 2), sStationName)
		ENDIF
	#ENDIF
	ENDIF
	
	
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_STILT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
	AND NOT MPRadioLocal.bOnYachtDeck
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPRadioLocal.bInCasinoApartment
	AND NOT MPRadioLocal.bInCasinoApartment2
	AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
	OR IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		REPEAT GET_MP_RADIO_NUMBER_OF_EMITTERS(property) iEmitterCount
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount))
					SET_STATIC_EMITTER_ENABLED(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), bPower)
					//CDEBUG2LN(DEBUG_RADIO, "Garage SET_STATIC_EMITTER_ENABLED: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), " TURNING: ", bPower)
					IF bOnOff
						SET_EMITTER_RADIO_STATION(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), sStationName)
					ELSE
		//					SCRIPT_ASSERT("TURNING OF GARAGE STATIC EMITTER")
					ENDIF
				ENDIF
				
			ELIF bHeistRadio = TRUE
			AND iEmitterCount = 2
			
				#IF IS_DEBUG_BUILD
					//PRINTLN("POD: RADIO: HEIST EMITTER: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), " TURNING: ", bPower)
				#ENDIF
				
		//		IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_TRIGGER_HEIST_RADIO)
		//			SET_STATIC_EMITTER_ENABLED(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), bPower)
		//			
		//		ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount))
					SET_STATIC_EMITTER_ENABLED(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), bPower)
					IF bOnOff
						//CDEBUG2LN(DEBUG_RADIO, "Heist SET_STATIC_EMITTER_ENABLED: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), " TURNING: ", bPower, ", station: ", sStationName)
						SET_EMITTER_RADIO_STATION(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), sStationName)
					ELSE
						//CDEBUG2LN(DEBUG_RADIO, "Heist SET_STATIC_EMITTER_ENABLED: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), " TURNING: ", bPower)
					ENDIF
					//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount)) 
				ENDIF
			ELSE
				IF bHeistRadio = FALSE
				AND iEmitterCount != 2 
					IF NOT IS_STRING_NULL_OR_EMPTY(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount))
						SET_STATIC_EMITTER_ENABLED(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), bPower)
						IF bOnOff
							//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), " TURNING: ", bPower, ", station: ", sStationName)
							SET_EMITTER_RADIO_STATION(GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), sStationName)
						ELSE	
							//CDEBUG2LN(DEBUG_RADIO, "Non Heist SET_STATIC_EMITTER_ENABLED: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount), " TURNING: ", bPower)
						ENDIF
						//CDEBUG3LN(DEBUG_RADIO, "Setting for emitter: ", GET_RADIO_EMITTER_NAME(MPRadioClient, property, iEmitterCount)) 
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns an ID for a sound effect for the radio turning on, based on the current apartment
/// PARAMS:
///    MPRadioClient - client broadcast data struct
///    iGarageSize - type of property
/// RETURNS:
///    sound effect name as STRING
FUNC STRING MP_RADIO_GET_TURN_ON_SOUND(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, INT iGarageSize)
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN "On_High"
	ENDIF
	
	SWITCH iGarageSize
		CASE PROP_GARAGE_SIZE_2
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "On_Low"
			ELSE
				RETURN "On_Low"
			ENDIF
		BREAK
		CASE PROP_GARAGE_SIZE_6
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "On_High"
			ELSE
				RETURN "On_Low"
			ENDIF
		BREAK
		CASE PROP_GARAGE_SIZE_10
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "On_Low"
			ELSE
				RETURN "On_High"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns an ID for a sound effect for the radio turning off, based on the current apartment
/// PARAMS:
///    MPRadioClient - client broadcast data struct
///    iGarageSize - type of property
/// RETURNS:
///    sound effect name as STRING
FUNC STRING MP_RADIO_GET_TURN_OFF_SOUND(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, INT iGarageSize)
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN "Off_High"
	ENDIF

	SWITCH iGarageSize
		CASE PROP_GARAGE_SIZE_2
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "Off_Low"
			ELSE
				RETURN "Off_Low"
			ENDIF
		BREAK
		CASE PROP_GARAGE_SIZE_6
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "Off_High"
			ELSE
				RETURN "Off_Low"
			ENDIF
		BREAK
		CASE PROP_GARAGE_SIZE_10
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "Off_Low"
			ELSE
				RETURN "Off_High"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Returns an ID for a sound effect for retuning the radio, based on the current apartment
/// PARAMS:
///    MPRadioClient - client broadcast data struct
///    iGarageSize - type of property
/// RETURNS:
///    sound effect name as STRING
FUNC STRING MP_RADIO_GET_RETUNE_SOUND(MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, INT iGarageSize)
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN "Retune_High"
	ENDIF
	
	SWITCH iGarageSize
		CASE PROP_GARAGE_SIZE_2
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "Retune_Low"
			ELSE
				RETURN "Retune_Low"
			ENDIF
		BREAK
		CASE PROP_GARAGE_SIZE_6
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "Retune_High"
			ELSE
				RETURN "Retune_Low"
			ENDIF
		BREAK
		CASE PROP_GARAGE_SIZE_10
			IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				RETURN "Retune_Low"
			ELSE
				RETURN "Retune_High"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Maintains local player's ambient radio functionality (namely changing the station when other players change station or turn on/off the radio)
/// PARAMS:
///    MPRadioServer - server broadcast data
///    MPRadioClient - client broadcast data
///    MPRadioLocal - local broadcast data
///    property - current property ID
///    bForceUpdate - forces the radio to set the emitters to the correct station this frame
PROC CLIENT_MAINTAIN_AMBIENT_RADIO(	MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, 
									MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_PROPERTY_STRUCT &property, BOOL bForceUpdate = FALSE)
	
	BOOL bUpdateNow = bForceUpdate
	
	
//	IF bUpdateNow = TRUE
//	OR MPRadioLocal.iMaintainAmbientRadioStagger%5 = 0
	
//	INT tempIntStage = ENUM_TO_INT(GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient))
//	PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: STAGE: ", tempIntStage)
//	IF NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		SWITCH GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient)
			CASE MP_RADIO_SERVER_STAGE_ON
				IF MPRadioLocal.iCurrentStationID <> GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
					MPRadioLocal.iCurrentStationID = GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
					
					IF MPRadioLocal.eStage = MP_RADIO_CLIENT_STAGE_ACTIVATED
					AND NOT MPRadioLocal.bInCasinoApartment
					AND NOT MPRadioLocal.bInCasinoApartment2
					AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
					AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
					AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
					#IF FEATURE_DLC_1_2022
					AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
					#ENDIF
					#IF FEATURE_FIXER
					OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
					#ENDIF
						IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
						OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
						OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
						OR IS_PLAYER_IN_OFFICE_MOD_INTERIOR(PLAYER_ID())
						OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
							PLAY_SOUND_FRONTEND(-1, "Retune_Low", "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend retune sound Retune_Low")
						ELIF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
						AND NOT IS_PROPERTY_OFFICE(property.iIndex)
							PLAY_SOUND_FRONTEND(-1, MP_RADIO_GET_RETUNE_SOUND(MPRadioClient, property.iGarageSize), "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend retune sound")
						ELSE
							PLAY_SOUND_FRONTEND(-1, "Retune_High", "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend retune sound Retune_High")
						ENDIF
					ENDIF
					bUpdateNow = TRUE
				ENDIF
				IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_RADIO, "=== MP_RADIO LOCAL === CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)")
					#ENDIF
					CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)
				ENDIF
				IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)
					IF MPRadioLocal.eStage = MP_RADIO_CLIENT_STAGE_ACTIVATED
					AND NOT MPRadioLocal.bInCasinoApartment
					AND NOT MPRadioLocal.bInCasinoApartment2
					AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
					AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
					AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
					#IF FEATURE_DLC_1_2022
					AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
					#ENDIF
					#IF FEATURE_FIXER
					AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
					#ENDIF
						IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
						OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
						OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
						OR IS_PLAYER_IN_OFFICE_MOD_INTERIOR(PLAYER_ID())
						OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
							PLAY_SOUND_FRONTEND(-1, "Retune_Low", "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend retune sound Retune_Low")
						ELIF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
						AND NOT IS_PROPERTY_OFFICE(property.iIndex)
							PLAY_SOUND_FRONTEND(-1, MP_RADIO_GET_RETUNE_SOUND(MPRadioClient, property.iGarageSize), "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend retune sound ", MP_RADIO_GET_RETUNE_SOUND(MPRadioClient, property.iGarageSize))
						ELSE
							PLAY_SOUND_FRONTEND(-1, "Retune_High", "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend retune sound Retune_High")
						ENDIF
					ENDIF
				
					bUpdateNow = TRUE
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_RADIO, "=== MP_RADIO LOCAL === SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)")
					#ENDIF
					
					SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)
				ENDIF
				
				IF bUpdateNow
					SET_MP_RADIO_LOCAL_STATION(MPRadioClient, MPRadioLocal, property, TRUE, MPRadioLocal.iCurrentStationID)
				ENDIF
			BREAK
			CASE MP_RADIO_SERVER_STAGE_OFF
				IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_RADIO, "=== MP_RADIO LOCAL === CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)")
					#ENDIF
					CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)
				ENDIF
				IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)
					IF MPRadioLocal.eStage = MP_RADIO_CLIENT_STAGE_ACTIVATED
						IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
						OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
						OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
						OR IS_PLAYER_IN_OFFICE_MOD_INTERIOR(PLAYER_ID())
							PLAY_SOUND_FRONTEND(-1, "Off_Low", "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend Off_Low")
						ELIF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
						AND NOT IS_PROPERTY_OFFICE(property.iIndex)
							PLAY_SOUND_FRONTEND(-1, MP_RADIO_GET_TURN_OFF_SOUND(MPRadioClient, property.iGarageSize), "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend ", MP_RADIO_GET_TURN_OFF_SOUND(MPRadioClient, property.iGarageSize))
						ELSE
							PLAY_SOUND_FRONTEND(-1, "Off_High", "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_OFF: Play sound Off_High")
						ENDIF
					ENDIF
					bUpdateNow = TRUE
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_RADIO, "=== MP_RADIO LOCAL === SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)")
					#ENDIF
					SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)
				ENDIF
				
				IF bUpdateNow
					SET_MP_RADIO_LOCAL_STATION(MPRadioClient, MPRadioLocal, property, FALSE, MPRadioLocal.iCurrentStationID)
				ENDIF
			BREAK
		ENDSWITCH
//	ENDIF
	
	//	INT tempHeistIntStage = ENUM_TO_INT(GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer))
	//	PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: HEIST STAGE: ", tempHeistIntStage)
		IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
//		AND NOT IS_PROPERTY_STILT_APARTMENT(property.iIndex)
			SWITCH GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer)
				CASE MP_RADIO_SERVER_HEIST_STAGE_ON
	//				PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: MP_RADIO_SERVER_HEIST_STAGE_ON")
					
					IF MPRadioLocal.iCurrentStationID <> GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
						MPRadioLocal.iCurrentStationID = GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
						
						IF MPRadioLocal.eHeistStage = MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
							PLAY_SOUND_FRONTEND(-1, MP_RADIO_GET_RETUNE_SOUND(MPRadioClient, property.iGarageSize), "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend ", MP_RADIO_GET_RETUNE_SOUND(MPRadioClient, property.iGarageSize))
						ENDIF
						
						bUpdateNow = TRUE
					ENDIF
					IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0)
	//					PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: STAGE ON: MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0")
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_heist_requested_turn_OFF0)")
						#ENDIF
						CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0)
					ELSE
	//					PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: STAGE ON: MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0: NOT SET")
					ENDIF
					IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_ON0)
						
						IF MPRadioLocal.eHeistStage = MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
							PLAY_SOUND_FRONTEND(-1, MP_RADIO_GET_TURN_ON_SOUND(MPRadioClient, property.iGarageSize), "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend ", MP_RADIO_GET_TURN_ON_SOUND(MPRadioClient, property.iGarageSize))
						ENDIF
					
						bUpdateNow = TRUE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_heist_requested_turn_ON0)")
						#ENDIF
						SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_ON0)
						
						
						IF IS_PLAYER_IN_CORONA()
						AND NOT IS_TRANSITION_SESSION_RESTARTING()
						AND NOT NETWORK_IS_ACTIVITY_SESSION()
							//CDEBUG2LN(DEBUG_RADIO, "CLIENT_MAINTAIN_AMBIENT_RADIO: TURNING ON HEIST RADIO: START_AUDIO_SCENE: DLC_MPHEIST_LOBBY_FADE_IN_RADIO_SCENE")
							START_AUDIO_SCENE("DLC_MPHEIST_LOBBY_FADE_IN_RADIO_SCENE")
						ELSE
							//CDEBUG2LN(DEBUG_RADIO, "CLIENT_MAINTAIN_AMBIENT_RADIO: TURNING ON HEIST RADIO: START_AUDIO_SCENE: NOT PLAYING, Player isn't in corona intro, in: ", GET_CORONA_STATUS_DEBUG_NAME(GET_PLAYER_CORONA_STATUS(PLAYER_ID())))
							//CDEBUG2LN(DEBUG_RADIO, "CLIENT_MAINTAIN_AMBIENT_RADIO: TURNING ON HEIST RADIO: START_AUDIO_SCENE: NOT PLAYING, IS_THIS_PLAYER_IN_ACTIVE_CORONA_SCREEN_POST_TRANSITION: ", IS_THIS_PLAYER_IN_ACTIVE_CORONA_SCREEN_POST_TRANSITION(PLAYER_ID()))
							
						ENDIF
					ENDIF
					
					IF bUpdateNow
						SET_MP_RADIO_LOCAL_STATION(MPRadioClient, MPRadioLocal, property, TRUE, MPRadioLocal.iCurrentStationID, TRUE)
					ENDIF
					
	//				PRINTLN("POD: HEIST RADIO: TURNED ON")
				BREAK
				
				CASE MP_RADIO_SERVER_HEIST_STAGE_OFF
					PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: MP_RADIO_SERVER_HEIST_STAGE_OFF")
					IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_ON0)
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_heist_requested_turn_ON0)")
						#ENDIF
						CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_ON0)
					ENDIF
					IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0)
						PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0")
						PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: eHeistStage: ", MPRadioLocal.eHeistStage)
						IF MPRadioLocal.eHeistStage = MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
							PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED")
							PLAY_SOUND_FRONTEND(-1, MP_RADIO_GET_TURN_OFF_SOUND(MPRadioClient, property.iGarageSize), "MP_RADIO_SFX")
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_ON: Play sound frontend ", MP_RADIO_GET_TURN_OFF_SOUND(MPRadioClient, property.iGarageSize))
						ENDIF
						bUpdateNow = TRUE
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_heist_requested_turn_OFF0)")
						#ENDIF
						SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0)
					ELSE
						PRINTLN("POD: CLIENT_MAINTAIN_AMBIENT_RADIO: MP_RADIO_LOCAL_BS_HEIST_REQUESTED_TURN_OFF0: NOT SET")
					ENDIF
					
					IF bUpdateNow
						SET_MP_RADIO_LOCAL_STATION(MPRadioClient, MPRadioLocal, property, FALSE, MPRadioLocal.iCurrentStationID, TRUE)
					ENDIF
					//SET_MP_RADIO_LOCAL_STATION(MPRadioClient, MPRadioLocal, property, FALSE, MPRadioLocal.iCurrentStationID)
					PRINTLN("POD: HEIST RADIO: TURNED OFF")
				BREAK
			ENDSWITCH
		ENDIF
		IF GET_MP_RADIO_SERVER_STAGE(MPRadioServer, MPRadioClient) = MP_RADIO_SERVER_STAGE_ON
		OR GET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer) = MP_RADIO_SERVER_HEIST_STAGE_ON
			IF MPRadioLocal.iCurrentStationID <> GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
				MPRadioLocal.iCurrentStationID = GET_MP_RADIO_SERVER_CURRENT_STATION(MPRadioServer, MPRadioClient)
			ENDIF
		ENDIF
		
		
		IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_FORCE_STRIPPER_MUSIC)
			IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === SET_BIT(MPTVLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)")
				#ENDIF
				SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
				
				SET_MP_RADIO_LOCAL_STATION(MPRadioClient, MPRadioLocal, property, TRUE, MPRadioLocal.iCurrentStationID)
				
			ENDIF
		ELSE
			IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO LOCAL === CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)")
				#ENDIF
				CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
				
				//CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_ON0)	//force radio to set itself to desired state
				//CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_REQUESTED_TURN_OFF0)
			ENDIF
		ENDIF
		
		IF MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MPRadioClient, MPRadioLocal)
			IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE)
				START_AUDIO_SCENE("MP_APT_STRIPPER_SCENE")
				SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE)
			ENDIF
		ELSE
			IF IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE)
				STOP_AUDIO_SCENE("MP_APT_STRIPPER_SCENE")
				CLEAR_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SET_STRIPPER_SCENE)
			ENDIF
		ENDIF
//	ENDIF
	
ENDPROC

/// PURPOSE:
///    Server sets the radio station to be broadcast out to all other players
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    bGarage - true if in garage
///    iThisStationID - ID of the desired station
PROC SET_SERVER_MP_RADIO_STATION(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, BOOL bGarage, INT iThisStationID)
	IF bGarage
		MPRadioServer.iCurrentGarageStationID = iThisStationID
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === MPRadioServer.iCurrentGarageStationID = ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentGarageStationID), ", ID = ", MPRadioServer.iCurrentGarageStationID)
		#ENDIF
	ELSE
		MPRadioServer.iCurrentStationID = iThisStationID
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === MPRadioServer.iCurrentStationID is now = ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID), ", ID = ", MPRadioServer.iCurrentStationID)
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Server changes the station to the next suitable station
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    bGarage - true if in garage
PROC SWITCH_TO_NEXT_MP_RADIO_STATION(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, BOOL bGarage)
//	#IF IS_DEBUG_BUILD
//		IF bGarage
//			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_NEXT_MP_RADIO_STATION garage")
//		ELSE
//			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_NEXT_MP_RADIO_STATION")
//		ENDIF
//	#ENDIF
	
	
	INT iThisCurrentStationID 
	IF bGarage
		iThisCurrentStationID = MPRadioServer.iCurrentGarageStationID
	ELSE
		iThisCurrentStationID = MPRadioServer.iCurrentStationID
//		CDEBUG1LN(DEBUG_RADIO, "SWITCH_TO_NEXT_MP_RADIO_STATION: changing station from: ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID), ", ID: ", MPRadioServer.iCurrentStationID)
	ENDIF
	
	INT iProcess = iThisCurrentStationID
	
	INT iSelection = -1
	BOOL bFinished = FALSE
	
	WHILE bFinished = FALSE
		iProcess += 1
		IF iProcess > GET_NUM_UNLOCKED_RADIO_STATIONS()-1
			CDEBUG1LN(DEBUG_RADIO, "SWITCH_TO_NEXT_MP_RADIO_STATION: resetting ")
			iProcess = 0
		ENDIF
		IF iProcess = iThisCurrentStationID	//If we've looped all the way round
			bFinished = TRUE
		ELSE
			iSelection = iProcess
			bFinished = TRUE
		ENDIF
		
	ENDWHILE
	
//	CDEBUG1LN(DEBUG_RADIO, "SWITCH_TO_NEXT_MP_RADIO_STATION: going from ID: ", MPRadioServer.iCurrentStationID, ", to: ", iSelection)
	
	IF iSelection <> -1
		SET_SERVER_MP_RADIO_STATION(MPRadioServer, bGarage, iSelection)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Server changes the station to the previous suitable station
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    bGarage - true if in garage
PROC SWITCH_TO_PREVIOUS_MP_RADIO_STATION(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, BOOL bGarage)
	#IF IS_DEBUG_BUILD
//		IF bGarage
//			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_PREVIOUS_MP_RADIO_STATION garage")
//		ELSE
//			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_PREVIOUS_MP_RADIO_STATION")
//		ENDIF
	#ENDIF
	
	INT iThisCurrentStationID 
	IF bGarage
		iThisCurrentStationID = MPRadioServer.iCurrentGarageStationID
	ELSE
		iThisCurrentStationID = MPRadioServer.iCurrentStationID
//		CDEBUG1LN(DEBUG_RADIO, "SWITCH_TO_PREVIOUS_MP_RADIO_STATION: changing station from: ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID), ", ID: ", MPRadioServer.iCurrentStationID)
	ENDIF
	
	INT iProcess = iThisCurrentStationID
	
	INT iSelection = -1
	BOOL bFinished = FALSE
	
	WHILE bFinished = FALSE
		iProcess -= 1
		IF iProcess < 0
			iProcess = GET_NUM_UNLOCKED_RADIO_STATIONS()-1
		ENDIF
	
		IF iProcess = iThisCurrentStationID	//If we've looped all the way round
			bFinished = TRUE
		ELSE
			iSelection = iProcess
			bFinished = TRUE
		ENDIF
		
	ENDWHILE
	
//	CDEBUG1LN(DEBUG_RADIO, "SWITCH_TO_PREVIOUS_MP_RADIO_STATION: going from ID: ", MPRadioServer.iCurrentStationID, ", to: ", iSelection)
	
	IF iSelection <> -1
		SET_SERVER_MP_RADIO_STATION(MPRadioServer, bGarage, iSelection)
	ENDIF
	
ENDPROC

#IF FEATURE_TUNER
/// PURPOSE:
///    Retrieve the radio index for the Audio Player station
FUNC INT GET_AUDIO_PLAYER_STATION_INDEX()
	RETURN FIND_RADIO_STATION_INDEX(HASH("RADIO_36_AUDIOPLAYER"))
ENDFUNC

/// PURPOSE:
///    Server changes the station to the audio player
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    bGarage - true if in garage
PROC SWITCH_TO_AUDIO_PLAYER(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, BOOL bGarage)
	SET_SERVER_MP_RADIO_STATION(MPRadioServer, bGarage, GET_AUDIO_PLAYER_STATION_INDEX())	
ENDPROC
#ENDIF

/// PURPOSE:
///    Server processes what station should be the current station based on the requests of the client
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    MPRadioStaggerClient - client broadcast data struct to check this frame
///    MPRadioLocal - local data struct
///    bGarage - true if for garage
PROC SERVER_MAINTAIN_CURRENT_MP_RADIO_STATION(	MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioStaggerClient, 
												MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, BOOL bGarage)
	BOOL bReadyForUpdate = FALSE
	BOOL bChangedStationThisFrame = FALSE
	INT iCurrentServerStationID
	BOOL bIsClientSuitableForChange = FALSE
	
	IF bGarage
		iCurrentServerStationID = MPRadioServer.iCurrentGarageStationID
		IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET)
		OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPRadioLocal.timeServerGarageStationChange) > MP_RADIO_STATION_CHANGE_WAIT_TIME
			bReadyForUpdate = TRUE
			IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				IF MPRadioStaggerClient.iTimeOfInputStationID = iCurrentServerStationID
					bIsClientSuitableForChange = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		iCurrentServerStationID = MPRadioServer.iCurrentStationID
		IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET0)
		OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPRadioLocal.timeServerStationChange) > MP_RADIO_STATION_CHANGE_WAIT_TIME
			bReadyForUpdate = TRUE
			IF NOT IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
				IF MPRadioStaggerClient.iTimeOfInputStationID = iCurrentServerStationID
					bIsClientSuitableForChange = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bReadyForUpdate
		IF bIsClientSuitableForChange
			IF IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_NEXT_CHANNEL)
			OR IS_BIT_SET(GET_HEIST_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_NEXT_CHANNEL)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_NEXT_MP_RADIO_CHANNEL for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
				#ENDIF
				SWITCH_TO_NEXT_MP_RADIO_STATION(MPRadioServer, bGarage)
				bChangedStationThisFrame = TRUE
			ELIF IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_PREVIOUS_CHANNEL)
			OR IS_BIT_SET(GET_HEIST_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_PREVIOUS_CHANNEL)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_PREVIOUS_MP_RADIO_CHANNEL for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
				#ENDIF
				SWITCH_TO_PREVIOUS_MP_RADIO_STATION(MPRadioServer, bGarage)
				bChangedStationThisFrame = TRUE
			#IF FEATURE_TUNER
			ELIF IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_AUDIO_PLAYER)
			OR IS_BIT_SET(GET_HEIST_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_AUDIO_PLAYER)
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === SWITCH_TO_AUDIO_PLAYER for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
				#ENDIF
				SWITCH_TO_AUDIO_PLAYER(MPRadioServer, bGarage)
				bChangedStationThisFrame = TRUE
			#ENDIF
			ENDIF
		ENDIf
	ENDIF
	
	IF bChangedStationThisFrame
		IF bGarage
			iCurrentServerStationID = MPRadioServer.iCurrentGarageStationID
		ELSE
			iCurrentServerStationID = MPRadioServer.iCurrentStationID
		ENDIF
		
		IF bGarage
			MPRadioLocal.timeServerGarageStationChange = GET_NETWORK_TIME()
			SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET)
		ELSE
			MPRadioLocal.timeServerStationChange = GET_NETWORK_TIME()
			SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET0)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Maintains storing what channel should be used upon initialising the property on next entry
/// PARAMS:
///    MPRadioServer - server broadcast data struct
PROC MAINTAIN_RADIO_SAVE_GAME(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer)
	
	IF MPRadioServer.eStage > MP_RADIO_SERVER_STAGE_INIT
		IF MPRadioServer.eStage = MP_RADIO_SERVER_STAGE_ON
			IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0]
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0] = TRUE")
				#ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0] = TRUE
				
				GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iApartmentRadioStationOn = -1 // Reset global broadcast data for radio on/off option, this will cause freemode logic to regrab data stored in g_savedMPGlobalsNew
			ENDIF
		ENDIF
		IF MPRadioServer.eStage = MP_RADIO_SERVER_STAGE_OFF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0]
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0] = FALSE")
				#ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0] = FALSE
				
				GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iApartmentRadioStationOn = -1 // Reset global broadcast data for radio on/off option, this will cause freemode logic to regrab data stored in g_savedMPGlobalsNew
			ENDIF
		ENDIF
		
		IF MPRadioServer.iCurrentStationID <> g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iRadioStationID[0]
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iRadioStationID[0] = ", MPRadioServer.iCurrentStationID)
			#ENDIF
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iRadioStationID[0] = MPRadioServer.iCurrentStationID
			
			GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iPreviousApartmentRadioStation = -1 // Reset global broadcast data for radio station, this will cause freemode logic to regrab data stored in g_savedMPGlobalsNew
		ENDIF
	ENDIF
	
	
	IF MPRadioServer.eGarageStage > MP_RADIO_SERVER_STAGE_INIT
		IF MPRadioServer.eGarageStage = MP_RADIO_SERVER_STAGE_ON
			IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn = TRUE")
				#ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn = TRUE
			ENDIF
		ENDIF
		IF MPRadioServer.eGarageStage = MP_RADIO_SERVER_STAGE_OFF
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn = FALSE")
				#ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn = FALSE
			ENDIF
		ENDIF
		IF MPRadioServer.iCurrentGarageStationID <> g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageRadioStationID
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_SPECTATOR, "=== MP_RADIO CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageRadioStationID = ", MPRadioServer.iCurrentGarageStationID)
			#ENDIF
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageRadioStationID = MPRadioServer.iCurrentGarageStationID
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_STARTING_RADIO_STATION()
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEFUNCT_BASE_HAVE_FORCED_NEW_RADIO_STATION) 
		RETURN 14 //RADIO_21_DLC_XM17
		
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		RETURN 17 //RADIO_02_POP
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SUBMARINE_NEW_RADIO_STATION)
		RETURN FIND_RADIO_STATION_INDEX(HASH("RADIO_35_DLC_HEI4_MLR"))
	ENDIF
	#ENDIF
	
	#IF FEATURE_TUNER
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_USB)
		AND HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_4_MOODY_MAN)						
			STRING trackList = GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_4_MOODY_MAN)	
			PRINTLN("POD: RADIO: Entered Auto Shop and collected USB_MUSIC_MIX_4_MOODY_MAN and haven't changed radio station yet, play: USB_MUSIC_MIX_4_MOODY_MAN on RADIO_36_AUDIOPLAYER")
			STRING sAudioPlayerStationName = GET_RADIO_STATION_NAME(GET_AUDIO_PLAYER_STATION_INDEX())
			UNLOCK_RADIO_STATION_TRACK_LIST(sAudioPlayerStationName, trackList)
			FORCE_MUSIC_TRACK_LIST(sAudioPlayerStationName, trackList, GET_RANDOM_INT_IN_RANGE(0, 13) * 60000) // Make sure the Mix is playing on the Audio Player
									
			RETURN GET_AUDIO_PLAYER_STATION_INDEX()	// Play Audio Player with Moodymann USB mix on first visit after collecting the USB until player changes station	
									
		ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_ML)
			PRINTLN("POD: RADIO: Entered Auto Shop and haven't changed radio station yet, play: RADIO_32_DLC_HEI4_MM_CLUB")
			RETURN FIND_RADIO_STATION_INDEX(HASH("RADIO_35_DLC_HEI4_MLR")) // Play Music Locker set on first visit until player changes station	
		
		ENDIF
		
	ENDIF		
	#ENDIF
	
	#IF FEATURE_FIXER
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		RETURN FIND_RADIO_STATION_INDEX(HASH("RADIO_09_HIPHOP_OLD"))
	ENDIF
	#ENDIF
	
	RETURN g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iRadioStationID[0]
ENDFUNC

FUNC BOOL SHOULD_RADIO_START_ON()
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEFUNCT_BASE_HAVE_FORCED_NEW_RADIO_STATION)
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SUBMARINE_NEW_RADIO_STATION)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN TRUE
	ENDIF	
	
	IF IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bRadioOn[0]
ENDFUNC

PROC MAINTAIN_RADIO_STATS(MP_RADIO_SERVER_DATA_STRUCT& data, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)
	
	IF g_OwnerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX()
	AND MPRadioLocal.piApartmentOwner = INVALID_PLAYER_INDEX()
		EXIT
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND PLAYER_ID() = g_OwnerOfBasePropertyIAmIn
	AND data.eStage = MP_RADIO_SERVER_STAGE_ON
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEFUNCT_BASE_HAVE_FORCED_NEW_RADIO_STATION, TRUE)
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND PLAYER_ID() = MPRadioLocal.piApartmentOwner
	AND data.eStage = MP_RADIO_SERVER_STAGE_ON
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SUBMARINE_NEW_RADIO_STATION)
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SUBMARINE_NEW_RADIO_STATION, TRUE)
		ENDIF
	ENDIF
	#ENDIF
	
	#IF FEATURE_TUNER
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	AND PLAYER_ID() = MPRadioLocal.piApartmentOwner
	AND data.eStage = MP_RADIO_SERVER_STAGE_ON
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_ML) // Record when Auto Shop station is changed when Moodymann Music Locker mix was playing
		AND data.iCurrentStationID != FIND_RADIO_STATION_INDEX(HASH("RADIO_35_DLC_HEI4_MLR"))
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_ML, TRUE)
			PRINTLN("POD: RADIO: SET PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_ML = TRUE")
		
		ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_USB)	// Record when Auto Shop station is changed when Moodymann USB mix was playing
		AND HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_4_MOODY_MAN)	
		AND data.iCurrentStationID != GET_AUDIO_PLAYER_STATION_INDEX()
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_USB, TRUE)
			PRINTLN("POD: RADIO: SET PACKED_MP_BOOL_AUTO_SHOP_RADIO_MM_USB = TRUE")
		
		ENDIF		
	ENDIF
	#ENDIF		
ENDPROC


/// PURPOSE:
///    Loads the previously used radio station for use in re-creating the same station setup on initialising the property
/// PARAMS:
///    MPRadioClient - client broadcast data struct
PROC LOAD_PREVIOUS_RADIO_SAVE_GAME(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient)
	
	IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
		IF SHOULD_RADIO_START_ON()
			SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)")
			#ENDIF
		ELSE
			CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)")
			#ENDIF
		ENDIF
		
		MPRadioLocal.iPreviousSavedStation = GET_STARTING_RADIO_STATION()
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === MPRadioLocal.iPreviousSavedStation = ", MPRadioLocal.iPreviousSavedStation, ", ", GET_RADIO_STATION_NAME(MPRadioLocal.iPreviousSavedStation))
		#ENDIF
		
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageRadioOn
			SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)")
			#ENDIF
		ELSE
			CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)")
			#ENDIF
		ENDIF
		MPRadioLocal.iPreviousSavedGarageStation = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageRadioStationID
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === MPRadioLocal.iPreviousSavedGarageStation = ", MPRadioLocal.iPreviousSavedGarageStation)
		#ENDIF
		
		SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
	ENDIF
ENDPROC


PROC RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_PROPERTY_STRUCT &property)
	IF MPRadioServer.bStaticEmitterReserved = FALSE
		IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
		AND NOT MPRadioServer.bOnYachtDeck
			
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 4,false,true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 4)
			
				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 4 Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))	
				MPRadioServer.bStaticEmitterReserved = TRUE
			ENDIF
		ELIF MPRadioServer.bOnYachtDeck
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 4,false,true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 4)
			

				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 4 Props registered , GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))	
				MPRadioServer.bStaticEmitterReserved = TRUE
			ENDIF
		ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
			
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 6,false,true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 6)

				MPRadioServer.bStaticEmitterReserved = TRUE
				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 6 Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))	
			ENDIF
					
		ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 3,false,true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 3)

				MPRadioServer.bStaticEmitterReserved = TRUE
				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 3 Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))	
			ENDIF
		ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 2,false,true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 2)

				MPRadioServer.bStaticEmitterReserved = TRUE
				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 2 Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))	
			ENDIF
		
		ELIF IS_PROPERTY_OFFICE(property.iIndex)
		OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
		OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
		OR MPRadioLocal.bInCasinoApartment
		OR MPRadioLocal.bInCasinoApartment2
		OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		OR IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		#IF FEATURE_DLC_1_2022
		OR IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		#ENDIF
		#IF FEATURE_FIXER
		OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#ENDIF
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 1,false,true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 1)

				MPRadioServer.bStaticEmitterReserved = TRUE
				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 1 Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))	
			ENDIF
		ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY) + 2, false, true)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)+ 2)
				MPRadioServer.bStaticEmitterReserved = TRUE
				CDEBUG1LN(DEBUG_RADIO, "RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION: 2 Props registered, GET_NUM_RESERVED_MISSION_OBJECTS: ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL CREATE_RADIO_PROPS(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal)
	CDEBUG1LN(DEBUG_RADIO, "CREATE_RADIO_PROPS")
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING propYachtBarBB")
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))		
			IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), << -1237.322, -3008.762, -42.743 >>, FALSE)
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, << 0.000, -0.000, -90.000 >>)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(MPRadioLocal.piApartmentOwner)
		IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarPersonalQuarters)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING propYachtBarBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_RADIO_PROPS: IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED = TRUE")
					MPRadioLocal.RadioPropHangarPersonalQuarters = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), << -1236.810, -2981.990, -41.170 >>, FALSE)
					SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarPersonalQuarters, << 0.000, -0.000, 90.000 >>)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_RADIO, "CREATE_RADIO_PROPS: IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED = FALSE")
	ENDIF
	
	IF IS_PLAYER_HANGAR_PERSONAL_QUARTERS_PURCHASED(MPRadioLocal.piApartmentOwner)
		IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarPersonalQuarters)
		OR NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
			RETURN FALSE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
ENDFUNC

FUNC BOOL CREATE_PROPS_FOR_DEFUNCT_BASE(MP_RADIO_LOCAL_DATA_STRUCT& data)
	IF g_ownerOfBasePropertyIAmIn = INVALID_PLAYER_INDEX() 
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn)
		IF NOT DOES_ENTITY_EXIST(data.RadioPropHangarOffice)
			data.RadioPropHangarOffice = GET_CLOSEST_OBJECT_OF_TYPE(<<360.5515, 4840.0977, -58.911>> , 0.1, INT_TO_ENUM(MODEL_NAMES, HASH("v_res_fh_speakerdock")), FALSE, FALSE, FALSE)
			IF DOES_ENTITY_EXIST(data.RadioPropHangarOffice)
				LINK_STATIC_EMITTER_TO_ENTITY("se_xm_int_02_lounge_radio", data.RadioPropHangarOffice)
				CDEBUG1LN(DEBUG_RADIO, "Found lounge radio prop and linked se_xm_int_02_lounge_radio.")
			ENDIF
		ENDIF
	ENDIF
	
	//If we dont have the cheap lounge, then we have traditional or modern
	IF USE_DEFUNCT_BASE_QUARTERS_RADIO()
		IF NOT DOES_ENTITY_EXIST(data.RadioPropHangarPersonalQuarters)
			MODEL_NAMES eRadioModel = INT_TO_ENUM(MODEL_NAMES, HASH("as_prop_as_SpeakerDock"))
			IF REQUEST_LOAD_MODEL(eRadioModel)
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					data.RadioPropHangarPersonalQuarters = CREATE_OBJECT_NO_OFFSET(eRadioModel, <<363.900, 4827.320, -58.912>>, FALSE, FALSE, FALSE)
					SET_MODEL_AS_NO_LONGER_NEEDED(eRadioModel)
					SET_ENTITY_ROTATION(data.RadioPropHangarPersonalQuarters, <<0.0, 0.0, 0.0>>)
					LINK_STATIC_EMITTER_TO_ENTITY("se_xm_int_02_bedroom_radio", data.RadioPropHangarPersonalQuarters)
					CDEBUG1LN(DEBUG_RADIO, "Created quarters radio prop and linked se_xm_int_02_bedroom_radio.")
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_PROPS_FOR_DEFUNCT_BASE() = CAN_REGISTER_MISSION_ENTITIES() = FALSE")
				ENDIF	
			ENDIF
		ENDIF	
	ENDIF
 
 	IF USE_DEFUNCT_BASE_QUARTERS_RADIO()
	AND NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn)
		RETURN DOES_ENTITY_EXIST(data.RadioPropHangarOffice)
		AND DOES_ENTITY_EXIST(data.RadioPropHangarPersonalQuarters)
	ENDIF	
	
	IF NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn)
		RETURN DOES_ENTITY_EXIST(data.RadioPropHangarOffice)
	ENDIF
	
	IF USE_DEFUNCT_BASE_QUARTERS_RADIO()
		RETURN DOES_ENTITY_EXIST(data.RadioPropHangarPersonalQuarters)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_PROPS_FOR_CASINO_APARTMENT(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))		
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), <<948.837, 5.295, 116.276>>, FALSE)
				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, -32.0>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC

FUNC BOOL CREATE_PROPS_FOR_CASINO_APARTMENT_2(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))		
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), <<974.967, 68.650, 116.276>>, FALSE)
				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, 148.5>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC

FUNC BOOL CREATE_RADIO_PROPS_FOR_SUBMARINE(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), <<1563.203, 385.525, -53.211>>, FALSE)
				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, 180.0>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC

FUNC BOOL CREATE_RADIO_PROPS_FOR_AUTO_SHOP(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), << -1356.264, 150.584, -99.05 >>, FALSE)
				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, 90.0>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC

FUNC BOOL CREATE_RADIO_PROPS_FOR_CAR_MEET(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), <<-2168.058, 1111.075, -24.214>>, FALSE)
				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, -90.0>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC

#IF FEATURE_DLC_1_2022
FUNC BOOL CREATE_RADIO_PROPS_FOR_SIMEON_SHOWROOM(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
//	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
//		RETURN FALSE
//	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), <<-32.772, -1100.451, 26.541>>, FALSE)
				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, -110.000>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC
#ENDIF

#IF FEATURE_FIXER
FUNC BOOL CREATE_RADIO_PROPS_FOR_FIXER_HQ(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal)
	IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = INVALID_PLAYER_INDEX() 
	OR NOT HAS_PLAYER_VIEWED_FIXER_HQ_INTRO_MOCAP(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
	OR IS_PLAYER_WATCHING_FIXER_HQ_INTRO_CUTSCENE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")))
			IF CAN_REGISTER_MISSION_ENTITIES(0, 0, 1, 0)
				SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
				
				VECTOR vPosition = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 7.44543, -9.89069, 4.918 >>, eSimpleInterior)	// Original value at Hawick <<372.9733, -64.3406, 107.2810>>
				FLOAT vHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(270.000000, eSimpleInterior)	// Original value at Hawick 160.0
						
				MPRadioLocal.RadioPropHangarOffice = CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("sm_Prop_Smug_Wall_Radio_01")), vPosition, FALSE)				
				SET_ENTITY_ROTATION(MPRadioLocal.RadioPropHangarOffice, <<0.0, 0.0, vHeading>>)
			ENDIF
		ENDIF
	ENDIF
	
 	RETURN DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
ENDFUNC
#ENDIF

FUNC BOOL CREATE_STATIC_EMITTER_PROPS(MP_RADIO_LOCAL_DATA_STRUCT& MPRadioLocal, MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_PROPERTY_STRUCT &property, BOOL bEnableEmittersByDefault = TRUE)
	
	CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Enable Created Emitters by default? ", bEnableEmittersByDefault)
	IF NETWORK_IS_IN_MP_CUTSCENE()
		CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Player is in a mp cutscene: SET_NETWORK_CUTSCENE_ENTITIES = TRUE")
		SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
	ENDIF	
	
//	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
//		CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS - SIMEON SHOWROOM... Not spawning any static emitter props. ")
//		RETURN TRUE
//	ENDIF
//	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		IF NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(g_ownerOfBasePropertyIAmIn)
			SET_STATIC_EMITTER_ENABLED("se_xm_int_02_lounge_radio", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS - Facility lounge... bEnableEmittersByDefault ", bEnableEmittersByDefault)
		ENDIF
		IF USE_DEFUNCT_BASE_QUARTERS_RADIO()
			SET_STATIC_EMITTER_ENABLED("se_xm_int_02_bedroom_radio", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS - Facility quarters... bEnableEmittersByDefault ", bEnableEmittersByDefault)
		ENDIF
	ENDIF
	
	IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioServer.bOnYachtDeck
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBarBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING propYachtBarBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) >= 0
						GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_PLAYER_IS_ON(PLAYER_ID()), MP_PROP_ELEMENT_YACHT_BAR_STATIC_EMITTER, offset)
						MPRadioServer.vPropYachtBarBBPosition = offset.vLoc
						MPRadioServer.niYachtBarBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtBarBBPosition))
						
						SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bar", bEnableEmittersByDefault)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bar")
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bar to niYachtBarBB")
						LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bar", NET_TO_ENT(MPRadioServer.niYachtBarBB))
						
						SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtBarBB), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtBarBB), TRUE)
						SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtBarBB), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtBarBB), FALSE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtBarBB),TRUE)
						
						//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING propYachtBarBB, position: ", MPRadioServer.vPropYachtBarBBPosition)
					ELSE
						CWARNINGLN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: yacht id less than or equal to 0")
					ENDIF
					
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: propYachtBarBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING propYachtBedRoomBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) >= 0
						GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_PLAYER_IS_ON(PLAYER_ID()), MP_PROP_ELEMENT_YACHT_BEDROOM_STATIC_EMITTER, offset)
						MPRadioServer.vPropYachtBedroomBBPosition = offset.vLoc
						MPRadioServer.niYachtBedRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtBedRoomBBPosition))
						
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bedroom to niYachtBedRoomBB")
						SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom", bEnableEmittersByDefault)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bedroom")
						LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bedroom", NET_TO_ENT(MPRadioServer.niYachtBedRoomBB))
						
						SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtBedRoomBB), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtBedRoomBB), TRUE)
						
						SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtBedRoomBB), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtBedRoomBB), FALSE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtBedRoomBB),TRUE)
						
						//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING propYachtBedRoomBB, position: ", MPRadioServer.vPropYachtBedRoomBBPosition)
					ELSE
						CWARNINGLN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: yacht id less than or equal to 0")
					ENDIF
					
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: propYachtBedRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoom2BB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING propYachtBedRoom2BB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) >= 0
						GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_PLAYER_IS_ON(PLAYER_ID()), MP_PROP_ELEMENT_YACHT_BEDROOM2_STATIC_EMITTER, offset)
						MPRadioServer.vPropYachtBedRoom2BBPosition = offset.vLoc
						MPRadioServer.niYachtBedRoom2BB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtBedRoom2BBPosition))
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bedroom_02 to niYachtBedRoom2BB")
						
						SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_02", bEnableEmittersByDefault)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bedroom_02")
						LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bedroom_02", NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB))
						
						SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB), TRUE)
						
						SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB), FALSE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB),TRUE)
						
						
						
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING propYachtBedRoom2BB, position: ", MPRadioServer.vPropYachtBedRoom2BBPosition)
					
					ELSE
						CWARNINGLN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: yacht id less than or equal to 0")
					ENDIF
					
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: propYachtBedRoom2BB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoom3BB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING propYachtBedRoom3BB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					IF GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) >= 0
						GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_PLAYER_IS_ON(PLAYER_ID()), MP_PROP_ELEMENT_YACHT_BEDROOM3_STATIC_EMITTER, offset)
						MPRadioServer.vPropYachtBedRoom3BBPosition = offset.vLoc
						MPRadioServer.niYachtBedRoom3BB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtBedRoom3BBPosition))
						
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bedroom_03 to niYachtBedRoom3BB")
						SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_03", bEnableEmittersByDefault)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bedroom_03")
						LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bedroom_03", NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB))
						
						SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB), TRUE)
						
						SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB), FALSE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB),TRUE)
						ELSE
						CWARNINGLN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: yacht id less than or equal to 0")
					ENDIF
										
					//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING propYachtBedRoom3BB, position: ", MPRadioServer.vPropYachtBedRoom3BBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: propYachtBedRoom3BB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ELIF MPRadioServer.bOnYachtDeck
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtJacuzziBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING niYachtExtJacuzziBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioServer.iYachtID, MP_PROP_ELEMENT_YACHT_EXT_JACUZZI_STATIC_EMITTER, offset)
					MPRadioServer.vPropYachtExtJacuzziBBPosition = offset.vLoc
					MPRadioServer.niYachtExtJacuzziBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtExtJacuzziBBPosition ))
					SET_ENTITY_ROTATION(NET_TO_OBJ(MPRadioServer.niYachtExtJacuzziBB), offset.vRot)	

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_01 to niYachtExtJacuzziBB")
					SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_01", bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_01")
					LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_01", NET_TO_ENT(MPRadioServer.niYachtExtJacuzziBB))
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtExtJacuzziBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtExtJacuzziBB), TRUE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtExtJacuzziBB),TRUE)				
					
					
					
					//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niYachtExtJacuzziBB, position: ", MPRadioServer.vPropYachtExtJacuzziBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niYachtExtJacuzziBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtEntranceBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING niYachtExtEntranceBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioServer.iYachtID, MP_PROP_ELEMENT_YACHT_EXT_ENTRANCE_STATIC_EMITTER, offset)
					MPRadioServer.vPropYachtExtEntranceBBPosition = offset.vLoc
					MPRadioServer.niYachtExtEntranceBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtExtEntranceBBPosition ))
						

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_02 to niYachtExtEntranceBB")
					SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_02", bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_02")
					LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_02", NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB))
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB), TRUE)
					
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB),TRUE)
					
					
					
					//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niYachtExtEntranceBB, position: ", MPRadioServer.vPropYachtExtEntranceBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niYachtExtEntranceBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtTopDeckBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING niYachtExtTopDeckBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioServer.iYachtID, MP_PROP_ELEMENT_YACHT_EXT_TOP_DECK_STATIC_EMITTER, offset)
					MPRadioServer.vPropYachtExtTopDeckBBPosition = offset.vLoc
					MPRadioServer.niYachtExtTopDeckBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtExtTopDeckBBPosition ))
					SET_ENTITY_ROTATION(NET_TO_OBJ(MPRadioServer.niYachtExtTopDeckBB), offset.vRot)	

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_03 to niYachtExtTopDeckBB")
					SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_03", bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_03")
					LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_03", NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB))
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB), TRUE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB),TRUE)
//					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB), FALSE)
//					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB), FALSE)
					
					
					
					
					//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niYachtExtTopDeckBB, position: ", MPRadioServer.vPropYachtExtTopDeckBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niYachtExtTopDeckBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtFrontBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING niYachtExtFrontBB")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
				
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					GET_POSITION_AS_OFFSET_FOR_YACHT(MPRadioServer.iYachtID, MP_PROP_ELEMENT_YACHT_EXT_FRONT_STATIC_EMITTER, offset)
					MPRadioServer.vPropYachtExtFrontBBPosition = offset.vLoc
					MPRadioServer.niYachtExtFrontBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropYachtExtFrontBBPosition ))
					SET_ENTITY_ROTATION(NET_TO_OBJ(MPRadioServer.niYachtExtFrontBB), offset.vRot)	

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_04 to niYachtExtFrontBB")
					SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_04", bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_04")
					LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_04", NET_TO_ENT(MPRadioServer.niYachtExtFrontBB))
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niYachtExtFrontBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niYachtExtFrontBB), TRUE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niYachtExtFrontBB),TRUE)
//					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niYachtExtFrontBB), FALSE)
//					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niYachtExtFrontBB), FALSE)
					
					
					
					
					//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niYachtExtFrontBB, position: ", MPRadioServer.vPropYachtExtFrontBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niYachtExtFrontBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
	OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING PROP_BOOMBOX_01")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
						
						
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_LIVINGROOM_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					MPRadioServer.vPropLivingRoomBBPosition = offset.vLoc
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropLivingRoomBBPosition = ",  MPRadioServer.vPropLivingRoomBBPosition)
					MPRadioServer.niLivingRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropLivingRoomBBPosition))
				
				
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1)," to niLivingRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1), NET_TO_ENT(MPRadioServer.niLivingRoomBB))
					
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niLivingRoomBB),TRUE)
					

					
					
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niLivingRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niLivingRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING PROP_BOOMBOX_01")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
							
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_BEDROOM_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					MPRadioServer.vPropBedRoomBBPosition = offset.vLoc
					MPRadioServer.niBedRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropBedRoomBBPosition ))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropBedRoomBBPosition = ",  MPRadioServer.vPropBedRoomBBPosition)
					
//					IF NOT IS_STRING_EMPTY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2))
						SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2), bEnableEmittersByDefault)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2))
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2)," to niBedRoomBB")
						LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2), NET_TO_ENT(MPRadioServer.niBedRoomBB))
						
						SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niBedRoomBB), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niBedRoomBB), TRUE)
						SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niBedRoomBB), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niBedRoomBB), FALSE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niBedRoomBB),TRUE)
//					ELSE
//						CWARNINGLN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Static emitter string for bed room is empty")
//					ENDIF

					
					
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niBedRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niBedRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING PROP_BOOMBOX_01")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_HEIST_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					MPRadioServer.vPropHeistRoomBBPosition = offset.vLoc
					MPRadioServer.niHeistRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropHeistRoomBBPosition))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropHeistRoomBBPosition = ",  MPRadioServer.vPropHeistRoomBBPosition)
//					IF NOT IS_STRING_EMPTY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3))
						SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3), bEnableEmittersByDefault)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3))
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3)," to niHeistRoomBB")
						LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3), NET_TO_ENT(MPRadioServer.niHeistRoomBB))
						
						SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niHeistRoomBB), TRUE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niHeistRoomBB), TRUE)
						SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niHeistRoomBB), FALSE)
						SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niHeistRoomBB), FALSE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niHeistRoomBB),TRUE)
//					ELSE
//						CWARNINGLN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Static emitter string for heist room is empty")
//					ENDIF
					

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niHeistRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niHeistRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING PROP_BOOMBOX_01")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
						
						
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_LIVINGROOM_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					MPRadioServer.vPropLivingRoomBBPosition = offset.vLoc
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropLivingRoomBBPosition = ",  MPRadioServer.vPropLivingRoomBBPosition)
					MPRadioServer.niLivingRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropLivingRoomBBPosition))				
					
					
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1)," to niLivingRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1), NET_TO_ENT(MPRadioServer.niLivingRoomBB))
					
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niLivingRoomBB),TRUE)
					

					
					
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niLivingRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niLivingRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
		//Office Garage
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset		
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_LIVINGROOM_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					MPRadioServer.vPropLivingRoomBBPosition = offset.vLoc
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropLivingRoomBBPosition = ",  MPRadioServer.vPropLivingRoomBBPosition)
					MPRadioServer.niLivingRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropLivingRoomBBPosition))
				
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4)," to niLivingRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4), NET_TO_ENT(MPRadioServer.niLivingRoomBB))
					
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niLivingRoomBB),TRUE)

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niLivingRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niLivingRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
		
		//Office Garage Modshop
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					MP_PROP_OFFSET_STRUCT offset
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_BEDROOM_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
					MPRadioServer.vPropBedRoomBBPosition = offset.vLoc
					MPRadioServer.niBedRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropBedRoomBBPosition ))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropBedRoomBBPosition = ",  MPRadioServer.vPropBedRoomBBPosition)
					
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5)," to niBedRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5), NET_TO_ENT(MPRadioServer.niBedRoomBB))
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niBedRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niBedRoomBB), TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niBedRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niBedRoomBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niBedRoomBB),TRUE)
						
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niBedRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niBedRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF

	ELIF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING PROP_BOOMBOX_01")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
//					MP_PROP_OFFSET_STRUCT offset
//					MPRadioServer.vPropYachtBarBBPosition = <<-1574.0, -4085.6, 9.0>>
					
						
						
//					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_LIVINGROOM_STATIC_EMITTER, offset, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					IF MPRadioServer.iWarehouseSize = ciLARGE_WAREHOUSE_CRATE_CAPACITY
						MPRadioServer.vPropLivingRoomBBPosition = <<995.6363, -3098.0159, -38.3565>>
					ELIF MPRadioServer.iWarehouseSize = ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
						MPRadioServer.vPropLivingRoomBBPosition = <<1048.3282, -3100.1477, -38.3521>>
					ELIF MPRadioServer.iWarehouseSize = ciSMALL_WAREHOUSE_CRATE_CAPACITY
						MPRadioServer.vPropLivingRoomBBPosition = <<1087.5259, -3100.6770, -38.4417>>
					ENDIF
					
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropLivingRoomBBPosition = ",  MPRadioServer.vPropLivingRoomBBPosition)
					MPRadioServer.niLivingRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropLivingRoomBBPosition))
				
				
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioServer.iWarehouseSize), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioServer.iWarehouseSize))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioServer.iWarehouseSize)," to niLivingRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioServer.iWarehouseSize), NET_TO_ENT(MPRadioServer.niLivingRoomBB))
					
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niLivingRoomBB),TRUE)
					

					
					
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niLivingRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niLivingRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ELIF IS_PLAYER_IN_FACTORY(PLAYER_ID())
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			//CDEBUG2LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: REQUESTING PROP_BOOMBOX_01")
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("prop_radio_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)
					FLOAT fHeading
					IF MPRadioServer.iFactoryType = ENUM_TO_INT(FACTORY_TYPE_METH)
						MPRadioServer.vPropLivingRoomBBPosition = <<1001.9400, -3193.7649, -39.1060>>
						fHeading = 0.00
					ELIF MPRadioServer.iFactoryType = ENUM_TO_INT(FACTORY_TYPE_WEED )
						MPRadioServer.vPropLivingRoomBBPosition = <<1030.6600, -3204.5200, -38.2192>>
						fHeading = 75.5200
					ELIF MPRadioServer.iFactoryType = ENUM_TO_INT(FACTORY_TYPE_CRACK )
						MPRadioServer.vPropLivingRoomBBPosition = <<1086.0601, -3195.5601, -39.1100>>
						fHeading =  90.0000
					ELIF MPRadioServer.iFactoryType = ENUM_TO_INT(FACTORY_TYPE_FAKE_MONEY )
						MPRadioServer.vPropLivingRoomBBPosition = <<1130.0930, -3193.2620, -40.4976>>
						fHeading = 0.0
					ELIF MPRadioServer.iFactoryType = ENUM_TO_INT(FACTORY_TYPE_FAKE_IDS)
						MPRadioServer.vPropLivingRoomBBPosition = <<1156.1801, -3196.3330, -38.0976>>
						fHeading = 90.000 
					ENDIF

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropLivingRoomBBPosition = ",  MPRadioServer.vPropLivingRoomBBPosition)
					MPRadioServer.niLivingRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("prop_radio_01")), MPRadioServer.vPropLivingRoomBBPosition))
				
				
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , TRUE, MPRadioServer.iFactoryType), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , TRUE, MPRadioServer.iFactoryType))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , TRUE, MPRadioServer.iFactoryType)," to niLivingRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , TRUE, MPRadioServer.iFactoryType), NET_TO_ENT(MPRadioServer.niLivingRoomBB))
					
					SET_ENTITY_HEADING(NET_TO_ENT(MPRadioServer.niLivingRoomBB),fHeading)
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					//SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niLivingRoomBB),TRUE)
					

					
					
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niLivingRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niLivingRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ELIF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		//IE GARAGE
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			IF REQUEST_LOAD_MODEL(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")))					
				IF CAN_REGISTER_MISSION_ENTITIES(0,0,1,0)

					MPRadioServer.vPropLivingRoomBBPosition = <<965.5109, -2991.2646, -39.7606>>
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: MPRadioServer.vPropLivingRoomBBPosition = ",  MPRadioServer.vPropLivingRoomBBPosition)
					MPRadioServer.niLivingRoomBB = OBJ_TO_NET(CREATE_OBJECT_NO_OFFSET(INT_TO_ENUM(MODEL_NAMES, HASH("PROP_BOOMBOX_01")), MPRadioServer.vPropLivingRoomBBPosition))
				
					SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , FALSE, MPRadioServer.iFactoryType, TRUE), bEnableEmittersByDefault)
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , FALSE, MPRadioServer.iFactoryType, TRUE))
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , FALSE, MPRadioServer.iFactoryType, TRUE), " to niLivingRoomBB")
					LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0 , FALSE, MPRadioServer.iFactoryType, TRUE), NET_TO_ENT(MPRadioServer.niLivingRoomBB))
					
					
					SET_ENTITY_INVINCIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), TRUE)
					SET_ENTITY_COLLISION(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_ENT(MPRadioServer.niLivingRoomBB), FALSE)
					NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(MPRadioServer.niLivingRoomBB),TRUE)

					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATING niLivingRoomBB, position: ", MPRadioServer.vPropLivingRoomBBPosition)
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: niLivingRoomBB - CAN_REGISTER_MISSION_ENTITIES = FALSE")
	    		ENDIF

			ELSE
				CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: PROP_BOOMBOX_01 not Loaded yet")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioServer.bOnYachtDeck
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBarBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
		
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS CREATED for YACHT_APARTMENT")
			
			RETURN TRUE
		ENDIF
	ELIF MPRadioServer.bOnYachtDeck
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtEntranceBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtTopDeckBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtFrontBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtJacuzziBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS CREATED for Yacht Exterior")
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: NOT ALL PROPS CREATED for Yacht Exterior")
			RETURN FALSE
		ENDIF
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS CREATED for STILT_APARTMENT")
			
			RETURN TRUE
		ENDIF
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS CREATED for CUSTOM_APARTMENT")
			RETURN TRUE
		ENDIF
	
		ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS CREATED for PROPERTY_OFFICE GARAGE")
			RETURN TRUE
		ENDIF
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			RETURN TRUE
		ENDIF
	ENDIF
	
	
	CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: NOT ALL PROPS CREATED")
	RETURN FALSE
ENDFUNC

FUNC BOOL DESTROY_STATIC_EMITTER_PROPS(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_PROPERTY_STRUCT &property)
	
	IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioServer.bOnYachtDeck
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBarBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niYachtBarBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niYachtBarBB")
				DELETE_NET_ID(MPRadioServer.niYachtBarBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niYachtBarBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtBarBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niYachtBedRoomBB")
					
					DELETE_NET_ID(MPRadioServer.niYachtBedRoomBB)
					RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(MPRadioServer.niYachtBedRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtBedRoomBB")
				ENDIF
			ENDIF
		ENDIF
	ELIF MPRadioServer.bOnYachtDeck
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtJacuzziBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niYachtExtJacuzziBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niYachtExtJacuzziBB")
				
				DELETE_NET_ID(MPRadioServer.niYachtExtJacuzziBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niYachtExtJacuzziBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtExtJacuzziBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtEntranceBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niYachtExtEntranceBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niYachtExtEntranceBB")
				
				DELETE_NET_ID(MPRadioServer.niYachtExtEntranceBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niYachtExtEntranceBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtExtEntranceBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtTopDeckBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niYachtExtTopDeckBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niYachtExtTopDeckBB")
				
				DELETE_NET_ID(MPRadioServer.niYachtExtTopDeckBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niYachtExtTopDeckBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtExtTopDeckBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtFrontBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niYachtExtFrontBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niYachtExtFrontBB")
				
				DELETE_NET_ID(MPRadioServer.niYachtExtFrontBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niYachtExtFrontBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niYachtExtFrontBB")
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niLivingRoomBB")
				
				DELETE_NET_ID(MPRadioServer.niLivingRoomBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niLivingRoomBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niBedRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niBedRoomBB")
					
					DELETE_NET_ID(MPRadioServer.niBedRoomBB)
					RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(MPRadioServer.niBedRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niBedRoomBB")
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niHeistRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niHeistRoomBB")
					
					DELETE_NET_ID(MPRadioServer.niHeistRoomBB)
					RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(MPRadioServer.niHeistRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niHeistRoomBB")
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niLivingRoomBB")
				
				DELETE_NET_ID(MPRadioServer.niLivingRoomBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niLivingRoomBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niBedRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niBedRoomBB")
					
					DELETE_NET_ID(MPRadioServer.niBedRoomBB)
					RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(MPRadioServer.niBedRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niBedRoomBB")
				ENDIF
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niHeistRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niHeistRoomBB")
					
					DELETE_NET_ID(MPRadioServer.niHeistRoomBB)
					RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
				ELSE
					TAKE_CONTROL_OF_NET_ID(MPRadioServer.niHeistRoomBB)
					CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niHeistRoomBB")
				ENDIF
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niLivingRoomBB")
				
				DELETE_NET_ID(MPRadioServer.niLivingRoomBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niLivingRoomBB")
			ENDIF
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niBedRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niBedRoomBB")
				
				DELETE_NET_ID(MPRadioServer.niBedRoomBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niBedRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niBedRoomBB")
			ENDIF
		ENDIF
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: niLivingRoomBB")
				
				DELETE_NET_ID(MPRadioServer.niLivingRoomBB)
				RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY)- 1)
			ELSE
				TAKE_CONTROL_OF_NET_ID(MPRadioServer.niLivingRoomBB)
				CDEBUG1LN(DEBUG_RADIO, "DESTROY_STATIC_EMITTER_PROPS: PLAYER: ", GET_PLAYER_NAME(PLAYER_ID()), " doesn't have control of niLivingRoomBB")
			ENDIF
		ENDIF
	ENDIF
	
	
	
	IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioServer.bOnYachtDeck
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBarBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
		
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS DESTROYED for YACHT_APARTMENT")
			MPRadioServer.bStaticEmitterReserved = FALSE
			RETURN TRUE
		ENDIF
	ELIF MPRadioServer.bOnYachtDeck
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtFrontBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtTopDeckBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtEntranceBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtJacuzziBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS DESTROYED for YACHT Exterior_APARTMENT")
			MPRadioServer.bStaticEmitterReserved = FALSE
			RETURN TRUE
		ENDIF
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS DESTROYED for Stilt_APARTMENT")
			MPRadioServer.bStaticEmitterReserved = FALSE
			RETURN TRUE
		ENDIF
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
		AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS DESTROYED for Stilt_APARTMENT")
			MPRadioServer.bStaticEmitterReserved = FALSE
			RETURN TRUE
		ENDIF
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: ALL PROPS DESTROYED for Stilt_APARTMENT")
			MPRadioServer.bStaticEmitterReserved = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RELINK_STATIC_EMITTER_PROPS(MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_PROPERTY_STRUCT &property, BOOL bEnableEmittersByDefault = TRUE)
	CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Enable Relinked Emitters by default? ", bEnableEmittersByDefault)
	
	#IF FEATURE_DLC_1_2022
	IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Not linking any emitters to props in SIMEON SHOWROOM.")
		EXIT
	ENDIF
	#ENDIF
	
	IF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
	OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
	OR IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR MPRadioLocal.bInCasinoApartment
	OR MPRadioLocal.bInCasinoApartment2
	OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	OR IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
	#IF FEATURE_FIXER
	OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
		STRING sStationName = GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID)
		CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: setting radio station to: ", sStationName)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
		
			STRING strEmitterName
			IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
				CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: we are in a warehouse")
				strEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, TRUE, MPRadioServer.iWarehouseSize)
			ELIF IS_PLAYER_IN_FACTORY(PLAYER_ID())
				CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: we are in a factory")
				strEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, TRUE,MPRadioServer.iFactoryType)
			ELIF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
				CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: we are in a IR GARAGE")
				strEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1, FALSE, 0, FALSE, MPRadioServer.iFactoryType, TRUE)
			ELSE
				strEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 1)
			ENDIF
			
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter", strEmitterName)
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: ", strEmitterName, " to niLivingRoomBB")
			LINK_STATIC_EMITTER_TO_ENTITY(strEmitterName, NET_TO_ENT(MPRadioServer.niLivingRoomBB))				
			SET_STATIC_EMITTER_ENABLED(strEmitterName, bEnableEmittersByDefault)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Custom_Bedroom", sStationName)
		ELSE
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB) = FALSE")
		ENDIF

		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2))
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2)," to niBedRoomBB")
			LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2), NET_TO_ENT(MPRadioServer.niBedRoomBB))			
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 2), bEnableEmittersByDefault)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Custom_Bedroom", sStationName)
		ELSE
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB) = FALSE")
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB)
			
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter",  GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3))
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: ", GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3)," to niHeistRoomBB")
			LINK_STATIC_EMITTER_TO_ENTITY(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3), NET_TO_ENT(MPRadioServer.niHeistRoomBB))
			SET_STATIC_EMITTER_ENABLED(GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 3), bEnableEmittersByDefault)
			SET_EMITTER_RADIO_STATION("SE_DLC_APT_Custom_Bedroom", sStationName)
		ELSE
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niHeistRoomBB) = FALSE")
		ENDIF
		
	ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex) AND IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: we are in a office garage carmodshop")
		CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: setting radio station to: ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID))
		STRING strEmitterName
		strEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 5)
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niBedRoomBB)
			LINK_STATIC_EMITTER_TO_ENTITY(strEmitterName, NET_TO_ENT(MPRadioServer.niBedRoomBB))	
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter: ", strEmitterName)			
			SET_STATIC_EMITTER_ENABLED(strEmitterName, bEnableEmittersByDefault)
			SET_EMITTER_RADIO_STATION(strEmitterName, GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID))
		ENDIF
	ELIF IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex) AND NOT IS_PROPERTY_OFFICE_MOD_GARAGE(PLAYER_ID())
		CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: we are in a office garage")
		CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: setting radio station to: ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID))
		STRING strEmitterName
		strEmitterName = GET_STATIC_EMITTER_NAME_FROM_PROPERTY(MPRadioLocal, property.iIndex, 4)
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niLivingRoomBB)
			LINK_STATIC_EMITTER_TO_ENTITY(strEmitterName, NET_TO_ENT(MPRadioServer.niLivingRoomBB))	
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter: ", strEmitterName)			
			SET_STATIC_EMITTER_ENABLED(strEmitterName, bEnableEmittersByDefault)
			SET_EMITTER_RADIO_STATION(strEmitterName, GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID))
		ENDIF
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT MPRadioServer.bOnYachtDeck
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBarBB)
	
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bar", NET_TO_ENT(MPRadioServer.niYachtBarBB))
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bar", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bar")
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bar to niYachtBarBB")
	
		ENDIF
		
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoomBB)
			
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bedroom", NET_TO_ENT(MPRadioServer.niYachtBedRoomBB))
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bedroom to niYachtBedRoomBB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bedroom")
					
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoom2BB)
			
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bedroom_02", NET_TO_ENT(MPRadioServer.niYachtBedRoom2BB))
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bedroom_02 to niYachtBedRoom2BB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_02", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bedroom_02")
					
		ENDIF
		
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtBedRoom3BB)
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Bedroom_03", NET_TO_ENT(MPRadioServer.niYachtBedRoom3BB))
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Bedroom_03 to niYachtBedRoom3BB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Bedroom_03", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "RELINK_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Bedroom_03")
		ENDIF
					
	ELIF MPRadioServer.bOnYachtDeck
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtJacuzziBB)
				
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_01", NET_TO_ENT(MPRadioServer.niYachtExtJacuzziBB))
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_01 to niYachtExtJacuzziBB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_01", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_01")
			
														
		ENDIF
		
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtEntranceBB)
			
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_02", NET_TO_ENT(MPRadioServer.niYachtExtEntranceBB))
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_02 to niYachtExtEntranceBB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_02", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_02")
			
			
		ENDIF
		
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtTopDeckBB)
			
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_03", NET_TO_ENT(MPRadioServer.niYachtExtTopDeckBB))
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_03 to niYachtExtTopDeckBB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_03", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_03")
			
		ENDIF
		
		IF  NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(MPRadioServer.niYachtExtFrontBB)
			
			LINK_STATIC_EMITTER_TO_ENTITY("SE_DLC_APT_Yacht_Exterior_04", NET_TO_ENT(MPRadioServer.niYachtExtFrontBB))
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: Linking static emitter: SE_DLC_APT_Yacht_Exterior_04 to niYachtExtFrontBB")
			SET_STATIC_EMITTER_ENABLED("SE_DLC_APT_Yacht_Exterior_04", bEnableEmittersByDefault)
			CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: enabling emitter SE_DLC_APT_Yacht_Exterior_04")
			
		ENDIF
	ENDIF
ENDPROC
PROC CLEANUP_SERVER_RADIO(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_PROPERTY_STRUCT &property)
	CDEBUG1LN(DEBUG_RADIO, "CLEANUP_SERVER_RADIO")
	MAINTAIN_RADIO_SAVE_GAME(MPRadioServer)
	DESTROY_STATIC_EMITTER_PROPS(MPRadioServer, property)
	CDEBUG1LN(DEBUG_RADIO, "CLEANUP_SERVER_RADIO: GET_NUM_RESERVED_MISSION_OBJECTS = ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))
	SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_INIT, FALSE)
ENDPROC 
// ====================================================================================
// ====================================================================================
//
// MAINTAIN PROCS
//
// ====================================================================================
// ====================================================================================

/// PURPOSE:
///    Server every frame processing, to be run by the host of the property script
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    MPRadioStaggerClient - client broadcast data struct to be checked this frame
///    MPRadioLocal - local data struct
PROC SERVER_MAINTAIN_MP_RADIO(	MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioStaggerClient, 
								MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal , MP_PROPERTY_STRUCT &property)
	


//	IF IS_SKYSWOOP_MOVING()
//		#IF IS_DEBUG_BUILD
//			IF IS_SKYSWOOP_MOVING()
//				CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO SERVER === IS_SKYSWOOP_MOVING: true, therefore Cleaningup Radio")
//			ENDIF
//		#ENDIF
//		SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_INIT, FALSE)
//		CLEANUP_MP_RADIO(MPRadioStaggerClient, MPRadioLocal)
//		DESTROY_STATIC_EMITTER_PROPS(MPRadioServer, property)
//		EXIT
//	ENDIF

	IF g_bChampaignCelebrationCutsceneIsActive = TRUE
		IF MPRadioServer.eStage != MP_RADIO_SERVER_STAGE_ON
			CDEBUG1LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: g_bChampaignCelebrationCutsceneIsActive: Turning Radio station on")
			SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, FALSE)
		ENDIF
		
		#IF IS_NEXTGEN_BUILD 

		IF MPRadioServer.iCurrentStationID != 27
			CDEBUG1LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: g_bChampaignCelebrationCutsceneIsActive: setting radio station from ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID) ," to ", GET_RADIO_STATION_NAME(27))
			MPRadioServer.iCurrentStationID = 27
			CDEBUG1LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: g_bChampaignCelebrationCutsceneIsActive: Radio station on: ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID))
		ENDIF
		#ENDIF
		
		#IF NOT IS_NEXTGEN_BUILD

		IF MPRadioServer.iCurrentStationID != 14
			CDEBUG1LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: g_bChampaignCelebrationCutsceneIsActive: setting radio station from ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID) ," to ", GET_RADIO_STATION_NAME(14))
			MPRadioServer.iCurrentStationID = 14
			CDEBUG1LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: g_bChampaignCelebrationCutsceneIsActive: Radio station on: ", GET_RADIO_STATION_NAME(MPRadioServer.iCurrentStationID))
		ENDIF
		#ENDIF
		
		g_bChampaignCelebrationCutsceneIsActive = FALSE
	ENDIF
	
//	CDEBUG1LN(DEBUG_RADIO, "RADIO ID FOR HIDDEN MOTOWN: ", FIND_RADIO_STATION_INDEX(GET_HASH_KEY("HIDDEN_RADIO_15_MOTOWN")))
	//CDEBUG3LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: MPRadioServer.eStage = ", MPRadioServer.eStage)
	SWITCH MPRadioServer.eStage
		
		CASE MP_RADIO_SERVER_STAGE_INIT
			MPRadioServer.iWarehouseSize = MPRadioLocal.iWarehouseSize
			MPRadioServer.iFactoryType = MPRadioLocal.iFactoryType
			BOOL bWaitForStaticEmitters
			bWaitForStaticEmitters = FALSE
			
			IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
			OR IS_PROPERTY_STILT_APARTMENT(property.iIndex)
			OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
			OR IS_PROPERTY_OFFICE(property.iIndex)
			OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			OR IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
			OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
			OR MPRadioServer.bOnYachtDeck
			OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())

				bWaitForStaticEmitters = TRUE
				IF MPRadioServer.bStaticEmitterReserved = TRUE
					IF CREATE_STATIC_EMITTER_PROPS(MPRadioLocal, MPRadioServer, property, FALSE)
						CDEBUG1LN(DEBUG_RADIO, "CREATE_STATIC_EMITTER_PROPS: CREATED GET_NUM_RESERVED_MISSION_OBJECTS = ", GET_NUM_RESERVED_MISSION_OBJECTS(FALSE,RESERVATION_LOCAL_ONLY))
						bWaitForStaticEmitters = FALSE
					ENDIF
				ELSE
					RESERVE_STATIC_EMITTER_PROPS_FOR_CREATION(MPRadioLocal, MPRadioServer, property)
				ENDIF
			ENDIF
			
			
			
			IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
				CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: IS_PROPERTY_YACHT_APARTMENT = TRUE")
			ELSE
				CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: IS_PROPERTY_YACHT_APARTMENT = FALSE")
			ENDIF
			CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: MPRadioServer.bOnYachtDeck = ", MPRadioServer.bOnYachtDeck)
			IF NOT bWaitForStaticEmitters
			// if there is a saved radio station, go to turn on radio stage
				IF NOT bInPropertyOwnerNotPaidLastUtilityBill
					IF MPRadioServer.bOnYachtDeck = FALSE
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))
							IF MPRadioLocal.piApartmentOwner = INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)	//We're checking the owner!	
								IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
									
									CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: #0 Setting iCurrentStationID to iPreviousSavedStation: ", GET_RADIO_STATION_NAME(MPRadioLocal.iPreviousSavedStation), " (",MPRadioLocal.iPreviousSavedStation,") for apartment owner: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
									MPRadioServer.iCurrentStationID = MPRadioLocal.iPreviousSavedStation
									
									
									IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
									AND NOT bInPropertyOwnerNotPaidLastUtilityBill
										CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: SERVER_MAINTAIN_MP_RADIO: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0 = TRUE, therefore turning on ")
										SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, FALSE)

										
									ELSE
	//									PRINTLN("POD: turning off3")
										CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: SERVER_MAINTAIN_MP_RADIO: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0 = FALSE, therefore turning off ")
										SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, FALSE)

									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET bit not set")
								ENDIF
							ELSE
								IF NATIVE_TO_INT(MPRadioLocal.piApartmentOwner) > -1
									CDEBUG1LN(DEBUG_RADIO, "Owner of apartment:", GET_PLAYER_NAME(MPRadioLocal.piApartmentOwner), ", not owner of apartment: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
								ELSE
									CDEBUG1LN(DEBUG_RADIO, "Player owner = -1")
								ENDIF
							ENDIF	
						ELSE
							IF MPRadioLocal.iServerPlayerStagger > -1
							AND NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))
								IF MPRadioLocal.piApartmentOwner = INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)	//We're checking the owner!	
									CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Setting Station from player outside hangar: Setting Station from player outside hangar: Player stagger is not a participant: ", MPRadioLocal.iServerPlayerStagger, " = ",  GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
									IF NOT IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
										SET_BIT(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
										CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Setting Station from player outside hangar: Setting iCurrentStationID to iPreviousSavedStation: ", GET_RADIO_STATION_NAME(GlobalplayerBD_FM_3[MPRadioLocal.iServerPlayerStagger].iPreviousApartmentRadioStation), " for apartment owner: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
										MPRadioServer.iCurrentStationID = GlobalplayerBD_FM_3[MPRadioLocal.iServerPlayerStagger].iPreviousApartmentRadioStation
										
										IF GlobalplayerBD_FM_3[MPRadioLocal.iServerPlayerStagger].iApartmentRadioStationOn = 1
											SET_BIT(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
										ELIF GlobalplayerBD_FM_3[MPRadioLocal.iServerPlayerStagger].iApartmentRadioStationOn = 0
											CLEAR_BIT(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
										ELSE
											CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Setting Station from player outside hangar: SERVER_MAINTAIN_MP_RADIO: Invalid radio power setting for hangar owner, iApartmentRadioStationOn = ", GlobalplayerBD_FM_3[MPRadioLocal.iServerPlayerStagger].iApartmentRadioStationOn)
										ENDIF
										
										IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
										AND NOT bInPropertyOwnerNotPaidLastUtilityBill
											CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Setting Station from player outside hangar: SERVER_MAINTAIN_MP_RADIO: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0 = TRUE, therefore turning on ")
											SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, FALSE)

											
										ELSE
		//									PRINTLN("POD: turning off3")
											CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Setting Station from player outside hangar: SERVER_MAINTAIN_MP_RADIO: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0 = FALSE, therefore turning off ")
											SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, FALSE)

										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Setting Station from player outside hangar: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET bit not set")
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						IF MPRadioLocal.piApartmentOwner = INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)	//We're checking the owner!	
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: Found the owner of the Yacht : ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
							IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))
								CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: And Yacht owner is a participant of the script")
								IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
									
									CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: #1 Setting iCurrentStationID to iPreviousSavedStation: ", GET_RADIO_STATION_NAME(MPRadioLocal.iPreviousSavedStation), " for apartment owner: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
									MPRadioServer.iCurrentStationID = MPRadioLocal.iPreviousSavedStation
									
									
									IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
									AND NOT bInPropertyOwnerNotPaidLastUtilityBill
										CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: SERVER_MAINTAIN_MP_RADIO: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0 = TRUE, therefore turning on ")
										SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, FALSE)

										
									ELSE
	//									PRINTLN("POD: turning off3")
										CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: SERVER_MAINTAIN_MP_RADIO: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0 = FALSE, therefore turning off ")
										SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, FALSE)

									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET bit not set")
								ENDIF
							ELSE
								CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: And Yacht owner is not a participant of the script")
								MPRadioServer.iCurrentStationID = 11
								SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, FALSE)
							ENDIF
						ELSE
							IF NATIVE_TO_INT(MPRadioLocal.piApartmentOwner) > -1
								CDEBUG1LN(DEBUG_RADIO, "Player:", GET_PLAYER_NAME(MPRadioLocal.piApartmentOwner), " not owner of apartment: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger)))
							ELSE
								CDEBUG1LN(DEBUG_RADIO, "Player owner = -1")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MPRadioServer.iCurrentStationID = 0
					CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: turning radio off, client hasn't paid their utility bill")
					SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, FALSE)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: waiting for static emitters")
			ENDIF
		BREAK
		
		CASE MP_RADIO_SERVER_STAGE_ON
			// if the radio's power is off, turn off
			// else run the radio
			IF (IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_POWER_OFF)
				AND NOT IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE))
			OR bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, FALSE)
			ELSE
				SERVER_MAINTAIN_CURRENT_MP_RADIO_STATION(MPRadioServer, MPRadioStaggerClient, MPRadioLocal, FALSE)
			ENDIF
		BREAK
		
		CASE MP_RADIO_SERVER_STAGE_OFF
			IF ((IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_POWER_ON) 
				AND NOT IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE))	//if user not in garage is turning on the radio
			OR	IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_FORCE_STRIPPER_MUSIC))		//or stripper time!!
			AND NOT bInPropertyOwnerNotPaidLastUtilityBill
			OR IS_PLAYER_IN_CORONA()
				//CDEBUG2LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: TURNING ON RADIO: IS_PLAYER_IN_CORONA: ", IS_PLAYER_IN_CORONA())
				SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, FALSE)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT IS_PROPERTY_STILT_APARTMENT(PROPERTY.iIndex)
	AND NOT IS_PROPERTY_CUSTOM_APARTMENT(PROPERTY.iIndex)
	AND NOT IS_PROPERTY_YACHT_APARTMENT(PROPERTY.iIndex)
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPRadioLocal.bInCasinoApartment
	AND NOT MPRadioLocal.bInCasinoApartment2
	AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
	AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
	AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
	#IF FEATURE_DLC_1_2022
	AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
	#ENDIF
		SWITCH MPRadioServer.eHeistRadioStage
		
			CASE MP_RADIO_SERVER_STAGE_INIT
				// if there is a saved radio station, go to turn on radio stage
				IF NOT bInPropertyOwnerNotPaidLastUtilityBill
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger), FALSE, FALSE)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))
							IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = NETWORK_GET_PARTICIPANT_INDEX(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))	//We're checking the owner!	
								IF property.iGarageSize = PROP_GARAGE_SIZE_10
	//								IF MPRadioLocal.iPreviousSavedStation > 0
	//									PRINTLN("POD: Radio: heist: Radio init: saved radio station: ", MPRadioLocal.iPreviousSavedStation)
	//									MPRadioServer.iCurrentStationID = MPRadioLocal.iPreviousSavedStation
	//								ELSE
	//									PRINTLN("POD: Radio: heist: Radio init: no saved radio station")
	//									MPRadioServer.iCurrentStationID = 1
	//								ENDIF
									
									IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_ON0)
									AND NOT bInPropertyOwnerNotPaidLastUtilityBill
										PRINTLN("POD: Radio: heist: turning on")
										SET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_HEIST_STAGE_ON, FALSE)	
									ELSE
										PRINTLN("POD: Radio: heist: turning on")
										SET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_HEIST_STAGE_OFF, FALSE)	
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MPRadioServer.iCurrentStationID = 0
					CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT: turning radio off, client hasn't paid their utility bill")
					SET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_HEIST_STAGE_OFF, FALSE)
				ENDIF

			BREAK
			CASE MP_RADIO_SERVER_HEIST_STAGE_ON
				// if the radio's power is off, turn off
				// else run the radio
				IF (IS_BIT_SET(GET_HEIST_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_HEIST_POWER_OFF))
				OR bInPropertyOwnerNotPaidLastUtilityBill
					SET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_HEIST_STAGE_OFF, FALSE)
				ELSE
					SERVER_MAINTAIN_CURRENT_MP_RADIO_STATION(MPRadioServer, MPRadioStaggerClient, MPRadioLocal, FALSE)
				ENDIF
				IF IS_SKYSWOOP_MOVING()

					#IF IS_DEBUG_BUILD
						IF IS_SKYSWOOP_MOVING()
							CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === IS_SKYSWOOP_MOVING: true, therefore Cleaningup Radio")
						ENDIF
					#ENDIF
					
					CLEANUP_MP_RADIO(MPRadioStaggerClient, MPRadioLocal)
				ENDIF
			BREAK
			
			CASE MP_RADIO_SERVER_HEIST_STAGE_OFF
				IF IS_BIT_SET(GET_HEIST_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_HEIST_POWER_ON)
				AND NOT bInPropertyOwnerNotPaidLastUtilityBill
				OR IS_PLAYER_IN_CORONA()
	//				//CDEBUG2LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: TURNING ON HEIST RADIO: START_AUDIO_SCENE: DLC_MPHEIST_LOBBY_FADE_IN_RADIO_SCENE")
	//				IF IS_PLAYER_IN_CORONA()
	//					START_AUDIO_SCENE("DLC_MPHEIST_LOBBY_FADE_IN_RADIO_SCENE")
	//				ENDIF 
					//CDEBUG2LN(DEBUG_RADIO, "SERVER_MAINTAIN_MP_RADIO: TURNING ON HEIST RADIO: IS_PLAYER_IN_CORONA: ", IS_PLAYER_IN_CORONA())
					SET_MP_HEIST_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_HEIST_STAGE_ON, FALSE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH MPRadioServer.eGarageStage
	
		CASE MP_RADIO_SERVER_STAGE_INIT
			IF NOT bInPropertyOwnerNotPaidLastUtilityBill
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger), FALSE, FALSE)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))
						IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = NETWORK_GET_PARTICIPANT_INDEX(INT_TO_PLAYERINDEX(MPRadioLocal.iServerPlayerStagger))	//We're checking the owner!	
							IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_PREVIOUS_SAVE_SET)
								
								MPRadioServer.iCurrentGarageStationID = MPRadioLocal.iPreviousSavedGarageStation
								
								IF IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)
								AND NOT bInPropertyOwnerNotPaidLastUtilityBill
									SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, TRUE)
								ELSE
									SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				MPRadioServer.iCurrentGarageStationID = 0
				SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, TRUE)
			ENDIF
		BREAK
		
		CASE MP_RADIO_SERVER_STAGE_ON
			IF (IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_POWER_OFF)
				AND IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE))
			OR bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_OFF, TRUE)
			ELSE
				SERVER_MAINTAIN_CURRENT_MP_RADIO_STATION(MPRadioServer, MPRadioStaggerClient, MPRadioLocal, TRUE)
			ENDIF
		BREAK
		
		CASE MP_RADIO_SERVER_STAGE_OFF
			IF IS_BIT_SET(GET_INPUT_BITS(MPRadioStaggerClient.iBitset), MP_RADIO_INPUT_POWER_ON)
			AND IS_BIT_SET(MPRadioStaggerClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
			AND NOT bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_RADIO_SERVER_STAGE(MPRadioServer, MP_RADIO_SERVER_STAGE_ON, TRUE)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	MPRadioLocal.iServerPlayerStagger++
	IF MPRadioLocal.iServerPlayerStagger >= NUM_NETWORK_PLAYERS
		
		MPRadioLocal.iServerPlayerStagger = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Local client processing to be run by every player in the apartment. Keeps the radios playing the correct station ambiently, and also manging input for changing radio stations
/// PARAMS:
///    MPRadioServer - server broadcast data struct
///    MPRadioClient - client broadcast data struct
///    MPRadioLocal - local data struct
///    property - property ID of current property
PROC CLIENT_MAINTAIN_MP_RADIO(MP_RADIO_SERVER_DATA_STRUCT &MPRadioServer, MP_RADIO_CLIENT_DATA_STRUCT &MPRadioClient, MP_RADIO_LOCAL_DATA_STRUCT &MPRadioLocal, MP_PROPERTY_STRUCT &property, INT &iActivityRequested, MP_PROP_ACT_SERVER_CONTROL_STRUCT &control)
	//Facility starts with radio off and channel set to 14 (new channel)
	MAINTAIN_RADIO_STATS(MPRadioServer, MPRadioLocal)

	IF MPRadioLocal.eStage >= MP_RADIO_CLIENT_STAGE_WALKING
	AND IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT IS_PLAYER_DEFUNCT_BASE_LOUNGE_1_PURCHASED(PLAYER_ID())
	AND NOT DOES_ENTITY_EXIST(MPRadioLocal.RadioPropHangarOffice)
		#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			CDEBUG1LN(DEBUG_RADIO, "Facility lounge radio prop no longer exists... re-grabbing... Player distance from prop: ", GET_DISTANCE_BETWEEN_COORDS(<<360.5515, 4840.0977, -58.911>>, GET_ENTITY_COORDS(PLAYER_PED_ID())))
		ENDIF
		#ENDIF 
		IF NOT CREATE_PROPS_FOR_DEFUNCT_BASE(MPRadioLocal)
			CDEBUG1LN(DEBUG_RADIO, "Facility lounge radio prop not found")
		ENDIF
	ENDIF

	IF g_iCurrentPropertyVariation != MPRadioLocal.iCurrentApartmentVariation
		CDEBUG1LN(DEBUG_RADIO, "CLIENT_MAINTAIN_MP_RADIO: apartment variation changed from: ", MPRadioLocal.iCurrentApartmentVariation, ", to g_iCurrentPropertyVariation = ", g_iCurrentPropertyVariation)
		MPRadioLocal.iCurrentApartmentVariation = g_iCurrentPropertyVariation
		SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_APARTMENT_VARIATION_CHANGE)
	ENDIF
	
	IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_APARTMENT_VARIATION_CHANGE)
		MPRadioLocal.eStage = MP_RADIO_CLIENT_STAGE_INIT
		CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_CLIENT_BS_APARTMENT_VARIATION_CHANGE = TRUE, MPRadioLocal.eStage = MP_RADIO_CLIENT_STAGE_INIT")
		CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_APARTMENT_VARIATION_CHANGE)
	ENDIF
	
	IF IS_SKYSWOOP_MOVING()
	OR IS_PLAYER_DEAD(PLAYER_ID())

		#IF IS_DEBUG_BUILD
		IF IS_SKYSWOOP_MOVING()
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === IS_SKYSWOOP_MOVING: true, therefore Cleaningup Radio")
		ENDIF
		
		IF IS_PLAYER_DEAD(PLAYER_ID())
			CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === IS_PLAYER_DEAD: true, therefore Cleaningup Radio")
		ENDIF
		
		#ENDIF
		
		CLEANUP_MP_RADIO(MPRadioClient, MPRadioLocal)
		EXIT 
	ENDIF
	
	#IF IS_DEBUG_BUILD
//	PRINTLN("POD: RADIO: CLIENT MAINTAIN: Stage: ", MPRadioLocal.eStage)
	#ENDIF
	IF MPRadioLocal.iAlternateHeistRadioStagger > 5
		MPRadioLocal.iAlternateHeistRadioStagger = 0
	ENDIF
	
	IF MPRadioLocal.iMaintainAmbientRadioStagger > 30
		MPRadioLocal.iMaintainAmbientRadioStagger = 0
	ENDIF
	
	IF MPRadioLocal.eStage > MP_RADIO_CLIENT_STAGE_INIT
	OR MPRadioLocal.eHeistStage > MP_RADIO_CLIENT_STAGE_INIT
		
		CLIENT_MAINTAIN_AMBIENT_RADIO(MPRadioServer, MPRadioClient, MPRadioLocal, property)
	ENDIF
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("CLIENT_MAINTAIN_AMBIENT_RADIO")
//	#ENDIF
//	#ENDIF
	
	//IF MPRadioLocal.iMaintainAmbientRadioStagger = 30
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_IS_PLAYER_A_PARTICIPANT(PLAYER_ID())
			IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = NETWORK_GET_PARTICIPANT_INDEX(PLAYER_ID())		//Player is the owner
				MAINTAIN_RADIO_SAVE_GAME(MPRadioServer)
			ENDIF
		ENDIF
	ENDIF
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("Radio Save")
//	#ENDIF
//	#ENDIF
	
	IF MPRadioLocal.eStage <> MP_RADIO_CLIENT_STAGE_ACTIVATED
		IF GET_INPUT_BITS(MPRadioClient.iBitset) <> 0
			SET_INPUT_BITS(MPRadioClient.iBitset, 0)
		ENDIF
	ENDIF
	
	IF MPRadioLocal.eHeistStage <> MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
		IF GET_HEIST_INPUT_BITS(MPRadioClient.iBitset) <> 0
			SET_HEIST_INPUT_BITS(MPRadioClient.iBitset, 0)
		ENDIF
	ENDIF

	IF (property.iGarageSize = PROP_GARAGE_SIZE_10
	OR IS_PLAYER_IN_OFFICE_MOD_INTERIOR(PLAYER_ID())
	OR IS_PROPERTY_CUSTOM_APARTMENT(PROPERTY.iIndex)
	OR IS_PROPERTY_STILT_APARTMENT(PROPERTY.iIndex)
	OR IS_PROPERTY_YACHT_APARTMENT(PROPERTY.iIndex))
	OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
	OR IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
	OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
	OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
	AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		IF MPRadioServer.eStage > MP_RADIO_SERVER_STAGE_INIT
		AND MPRadioServer.eHeistRadioStage > MP_RADIO_SERVER_STAGE_INIT
		AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
		OR MPRadioServer.eStage > MP_RADIO_SERVER_STAGE_INIT
		AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
		AND (IS_PROPERTY_CUSTOM_APARTMENT(PROPERTY.iIndex)
			OR IS_PROPERTY_STILT_APARTMENT(PROPERTY.iIndex)
			OR IS_PROPERTY_YACHT_APARTMENT(PROPERTY.iIndex)
			OR IS_PROPERTY_OFFICE(property.iIndex)
			OR IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
			OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
			OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
			OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
			OR IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
			OR IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
			OR IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
			#IF FEATURE_DLC_1_2022
			OR IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
			#ENDIF
			#IF FEATURE_FIXER
			OR IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
			#ENDIF
			)
			MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS(MPRadioLocal, property)
			CDEBUG1LN(DEBUG_RADIO, "CLIENT_MAINTAIN_MP_RADIO: new apartment: MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS")
			SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
			
			IF IS_PROPERTY_CUSTOM_APARTMENT(PROPERTY.iIndex)
			OR IS_PROPERTY_STILT_APARTMENT(PROPERTY.iIndex)
			OR IS_PROPERTY_YACHT_APARTMENT(PROPERTY.iIndex)
			OR IS_PROPERTY_OFFICE(property.iIndex)
			OR IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
			OR IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
			OR IS_PLAYER_IN_FACTORY(PLAYER_ID())
			OR IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
				CDEBUG1LN(DEBUG_RADIO, "CLIENT_MAINTAIN_MP_RADIO: new apartment: RELINK_STATIC_EMITTER_PROPS")
				RELINK_STATIC_EMITTER_PROPS(MPRadioLocal, MPRadioServer, PROPERTY, FALSE)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF MPRadioServer.eStage <= MP_RADIO_SERVER_STAGE_INIT
					CDEBUG1LN(DEBUG_RADIO, "MPRadioServer.eStage <= MP_RADIO_SERVER_STAGE_INIT")
				ENDIF
				IF IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
//					CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE is set")
				ENDIF
			#ENDIF
		ENDIF
		
	ELSE
		IF MPRadioServer.eStage > MP_RADIO_SERVER_STAGE_INIT
		AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
			CDEBUG1LN(DEBUG_RADIO, "CLIENT_MAINTAIN_MP_RADIO: not in new apartment, not relinking static emitters, disabling, property.iIndex = ", property.iIndex)
			MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS(MPRadioLocal, property)
			SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
		ENDIF
	ENDIF
//	PRINTLN("POD: CLIENT_MAINTAIN_MP_RADIO: stage: ", ENUM_TO_INT(MPRadioLocal.eStage))
	BOOL bClientActivatedRadio
	INT iInputBs	
	
	IF MPRadioLocal.iAlternateHeistRadioStagger%2 = 0
		SWITCH MPRadioLocal.eStage
		
			CASE MP_RADIO_CLIENT_STAGE_INIT
				IF NOT bInPropertyOwnerNotPaidLastUtilityBill
					MPRadioLocal.iCurrentApartmentVariation =  g_iCurrentPropertyVariation
					LOAD_PREVIOUS_RADIO_SAVE_GAME(MPRadioLocal, MPRadioClient)
					
					IF MPRadioServer.eStage > MP_RADIO_SERVER_STAGE_INIT
					AND IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
					AND CALCULATE_LOCATE_COORDS(MPRadioClient, MPRadioLocal, property)
				
						CLIENT_MAINTAIN_AMBIENT_RADIO(MPRadioServer, MPRadioClient, MPRadioLocal, property, FALSE)
				
						SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
					
						MPRadioLocal.fVolume = (MP_RADIO_MAX_VOLUME + MP_RADIO_MIN_VOLUME)/2
						
						IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
							IF NOT CREATE_RADIO_PROPS(MPRadioLocal)
								CDEBUG1LN(DEBUG_RADIO, "Radio props not created for hangar")
								EXIT
							ENDIF
						ENDIF
						
						IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
						AND NOT CREATE_PROPS_FOR_DEFUNCT_BASE(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Facility")
							EXIT
						ENDIF
						
						IF MPRadioLocal.bInCasinoApartment
						AND NOT CREATE_PROPS_FOR_CASINO_APARTMENT(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Casino Apartment")
							EXIT
						ENDIF
						
						IF MPRadioLocal.bInCasinoApartment2
						AND NOT CREATE_PROPS_FOR_CASINO_APARTMENT_2(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Casino Apartment 2")
							EXIT
						ENDIF
						
						IF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
						AND NOT CREATE_RADIO_PROPS_FOR_SUBMARINE(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for submarine")
							EXIT
						ENDIF
						
						IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
						AND NOT CREATE_RADIO_PROPS_FOR_AUTO_SHOP(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Auto Shop")
							EXIT
						ENDIF
						
						IF IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
						AND NOT CREATE_RADIO_PROPS_FOR_CAR_MEET(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Car Meet Private Takeover")
							EXIT
						ENDIF
						
						#IF FEATURE_DLC_1_2022
						IF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
						AND NOT CREATE_RADIO_PROPS_FOR_SIMEON_SHOWROOM(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Simeon Showroom")
							EXIT
						ENDIF
						#ENDIF
						
						#IF FEATURE_FIXER
						IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						AND NOT CREATE_RADIO_PROPS_FOR_FIXER_HQ(MPRadioLocal)
							CDEBUG1LN(DEBUG_RADIO, "Radio props not created for Fixer HQ")
							EXIT
						ENDIF
						#ENDIF
						
						SET_MP_RADIO_CLIENT_STAGE(MPRadioLocal, MP_RADIO_CLIENT_STAGE_WALKING)
					ELSE
						IF MPRadioServer.eStage <> MP_RADIO_SERVER_STAGE_INIT
							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_SERVER_STAGE_INIT no inited yet")
						ENDIF
						IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
//							CDEBUG1LN(DEBUG_RADIO, "MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE not set yet")
						ENDIF
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_RADIO, "player hasn't paid electricty so, no progressing past init.")
				ENDIF
			BREAK
			
			CASE MP_RADIO_CLIENT_STAGE_WALKING
				IF CLIENT_MAINTAIN_ACTIVATING_RADIO(MPRadioClient, MPRadioLocal, property, MPRadioLocal.iContextButtonIntention, MPRadioLocal.iRadioActivatingSwitch, iActivityRequested, control, FALSE)
					DISABLE_INTERACTION_MENU()
					SET_MP_RADIO_CLIENT_STAGE(MPRadioLocal, MP_RADIO_CLIENT_STAGE_ACTIVATED)
				ENDIF
	//			#IF IS_DEBUG_BUILD
	//			#IF SCRIPT_PROFILER_ACTIVE
	//			ADD_SCRIPT_PROFILE_MARKER("MP_RADIO_CLIENT_STAGE_WALKING")
	//			#ENDIF
	//			#ENDIF
			BREAK
					
			CASE MP_RADIO_CLIENT_STAGE_ACTIVATED
				SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
				SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS) 
	//			IF IS_HUD_COMPONENT_HIDDEN_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
	//				PRINTLN("POD: RADIO WHEEL HIDDEN!!!" )
	//			ELSE
	////				PRINTLN("POD: RADIO WHEEL NOT HIDDEN" )
	//			ENDIF
	
				iInputBs = GET_INPUT_BITS(MPRadioClient.iBitset)
				bClientActivatedRadio = CLIENT_MAINTAIN_ACTIVATED_RADIO(MPRadioServer, MPRadioClient, MPRadioLocal, iInputBs, iActivityRequested)
				SET_INPUT_BITS(MPRadioClient.iBitset, iInputBs)
				
				IF bClientActivatedRadio
				OR bInPropertyOwnerNotPaidLastUtilityBill
				OR g_bCelebrationScreenIsActive
				OR MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MPRadioClient, MPRadioLocal)
				OR GET_PLAYER_CORONA_STATUS(PLAYER_ID()) = CORONA_STATUS_INIT_CORONA
				OR MPGlobals.bStartedMPCutscene = TRUE
					IF MPGlobals.bStartedMPCutscene = TRUE
						CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === MPGlobals.bStartedMPCutscene: true, therefore resetting stage")
					ENDIF
					IF GET_PLAYER_CORONA_STATUS(PLAYER_ID()) = CORONA_STATUS_INIT_CORONA
						CDEBUG1LN(DEBUG_RADIO, "=== MP_RADIO CLIENT === CORONA_STATUS_INIT_CORONA: true, therefore resetting stage")
					ENDIF
							
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
						g_bRadioActTriggered = FALSE
						TIDYUP_MP_RADIO(MPRadioLocal)

	//					IF SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
							//CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
	//					ENDIF
					
					SET_MP_RADIO_CLIENT_STAGE(MPRadioLocal, MP_RADIO_CLIENT_STAGE_WALKING)
				ENDIF
				
	//			#IF IS_DEBUG_BUILD
	//			#IF SCRIPT_PROFILER_ACTIVE
	//			ADD_SCRIPT_PROFILE_MARKER("MP_RADIO_CLIENT_STAGE_ACTIVATED")
	//			#ENDIF
	//			#ENDIF
			BREAK
			
		ENDSWITCH
	ELSE
	//	PRINTLN("POD: CLIENT_MAINTAIN_MP_RADIO: Heist stage: ", ENUM_TO_INT(MPRadioLocal.eHeistStage), " 1 = walking")
	//	#IF FEATURE_INDEPENDANT_RADIO
		IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_IN_GARAGE)
		AND NOT IS_PROPERTY_STILT_APARTMENT(PROPERTY.iIndex)
		AND NOT IS_PROPERTY_CUSTOM_APARTMENT(PROPERTY.iIndex)
		AND NOT IS_PROPERTY_YACHT_APARTMENT(PROPERTY.iIndex)
		AND NOT IS_PROPERTY_OFFICE(property.iIndex)
		AND NOT IS_PROPERTY_OFFICE_GARAGE(PROPERTY.iIndex)
		AND NOT IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		AND NOT IS_PLAYER_IN_FACTORY(PLAYER_ID())
		AND NOT IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
		AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
		AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		AND NOT MPRadioLocal.bInCasinoApartment
		AND NOT MPRadioLocal.bInCasinoApartment2
		AND NOT IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
		#IF FEATURE_DLC_1_2022
		AND NOT IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		#ENDIF
		AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
		AND NOT IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		#IF FEATURE_FIXER
		AND NOT IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		#ENDIF
			BOOL bClientActivatedHeistRadio
			INT iHeistInputBs 
						
			SWITCH MPRadioLocal.eHeistStage
			
				CASE MP_RADIO_CLIENT_STAGE_INIT
				
	//				LOAD_PREVIOUS_RADIO_SAVE_GAME(MPRadioClient)
				
					IF MPRadioServer.eStage > MP_RADIO_SERVER_STAGE_INIT
					AND IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_INIT_EMITTER_DISABLE)
	//					MP_RADIO_TURN_OFF_ALL_APARTMENT_EMITTERS(property)
						CLIENT_MAINTAIN_AMBIENT_RADIO(MPRadioServer, MPRadioClient, MPRadioLocal, property, TRUE)
				
						SET_BIT(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_UPDATE_BUTTONS)
					
						MPRadioLocal.fVolume = (MP_RADIO_MAX_VOLUME + MP_RADIO_MIN_VOLUME)/2
						
						SET_MP_HEIST_RADIO_CLIENT_STAGE(MPRadioLocal, MP_RADIO_CLIENT_STAGE_WALKING)
						
					ENDIF
					
					
				BREAK
				
				CASE MP_RADIO_CLIENT_STAGE_WALKING
				
		//			IF CLIENT_MAINTAIN_ACTIVATING_RADIO(MPRadioClient, MPRadioLocal, property, MPRadioClient.iHeistInputBitset, MPRadioLocal.iHeistContextButtonIntention, MPRadioClient.iBitset, TRUE)
					IF property.iGarageSize = PROP_GARAGE_SIZE_10
						IF CLIENT_MAINTAIN_ACTIVATING_RADIO(MPRadioClient, MPRadioLocal, property, MPRadioLocal.iHeistContextButtonIntention, MPRadioLocal.iHeistRadioActivatingSwitch, iActivityRequested, control, TRUE)
							DISABLE_INTERACTION_MENU()
							SET_MP_HEIST_RADIO_CLIENT_STAGE(MPRadioLocal, MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED)
						ENDIF
					ENDIF
					
		//			#IF IS_DEBUG_BUILD
		//			#IF SCRIPT_PROFILER_ACTIVE
		//			ADD_SCRIPT_PROFILE_MARKER("HEIST MP_RADIO_CLIENT_STAGE_WALKING")
		//			#ENDIF
		//			#ENDIF
				BREAK
						
				CASE MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED
					SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS) 
		//			IF IS_HUD_COMPONENT_HIDDEN_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
		//				PRINTLN("POD: RADIO WHEEL HIDDEN!!!" )
		//			ELSE
		////				PRINTLN("POD: RADIO WHEEL NOT HIDDEN" )
		//			ENDIF
					
					iHeistInputBs = GET_HEIST_INPUT_BITS(MPRadioClient.iBitset)
					bClientActivatedHeistRadio = CLIENT_MAINTAIN_ACTIVATED_RADIO(MPRadioServer, MPRadioClient, MPRadioLocal, iHeistInputBs, iActivityRequested, TRUE)
					SET_HEIST_INPUT_BITS(MPRadioClient.iBitset, iHeistInputBs)
					
					IF bClientActivatedHeistRadio
					OR bInPropertyOwnerNotPaidLastUtilityBill
					OR g_bCelebrationScreenIsActive
					OR MP_RADIO_SHOULD_OVERRIDE_WITH_STRIPPER(MPRadioClient, MPRadioLocal)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
						TIDYUP_MP_RADIO(MPRadioLocal)
						
		//					IF SET_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
								//CLEAR_BIT(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_AT_HEIST_LOCATION)
		//					ENDIF
						
						SET_MP_HEIST_RADIO_CLIENT_STAGE(MPRadioLocal, MP_RADIO_CLIENT_STAGE_WALKING)
					ENDIF
		//			#IF IS_DEBUG_BUILD
		//			#IF SCRIPT_PROFILER_ACTIVE
		//			ADD_SCRIPT_PROFILE_MARKER("MP_HEIST_RADIO_CLIENT_STAGE_ACTIVATED")
		//			#ENDIF
		//			#ENDIF
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_MOVING_TO_RADIO)
	AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ABOUT_TO_MOVE_TO_RADIO)
	AND NOT IS_BIT_SET(MPRadioClient.iBitset, MP_RADIO_CLIENT_BS_ENTER_ANIM)
	AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
		IF NOT IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_IN_RADIO_ANGLED_AREA)
			MPRadioLocal.iAlternateHeistRadioStagger++
//			//CDEBUG3LN(DEBUG_RADIO, "MPRadioLocal.iAlternateHeistRadioStagger++: ", MPRadioLocal.iAlternateHeistRadioStagger)
		ELSE
			//CDEBUG3LN(DEBUG_RADIO, "IS_BIT_SET(MPRadioLocal.iBitset, MP_RADIO_LOCAL_BS_IN_RADIO_ANGLED_AREA) = TRUE  therefore no incrememnt: MPRadioLocal.iAlternateHeistRadioStagger", MPRadioLocal.iAlternateHeistRadioStagger)	
		ENDIF
	ENDIF
	
	MPRadioLocal.iMaintainAmbientRadioStagger++
ENDPROC
