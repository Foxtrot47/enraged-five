///// NET_realty_Warehouse.sch
///// Mark Richardson - 02/01/16
///// MP warehouse functionality
USING "net_include.sch"
USING "net_common_functions.sch"

/////////////////////////////////////////
///    
///    Variables for widgets
///    
///    

CONST_INT SHOP_BASKET_STAGE_ADD			0
CONST_INT SHOP_BASKET_STAGE_PENDING		1
CONST_INT SHOP_BASKET_STAGE_SUCCESS		2
CONST_INT SHOP_BASKET_STAGE_FAILED		3

#IF IS_DEBUG_BUILD
STRUCT WAREHOUSE_DEBUG_WIDGET_VARS
	BOOL bPurchaseWarehouse						= FALSE
	BOOL bSellWarehouse							= FALSE
	BOOL bAddContraband							= FALSE
	BOOL bRemoveContraband						= FALSE
	BOOL bDisplayWarehouseInfo					= FALSE
	BOOL bWarpToCoords							= FALSE
	BOOL bClearAllSlots							= FALSE
	BOOL bTradeIn								= FALSE
	BOOL bTradeInSlot1							= FALSE
	BOOL bTradeInSlot2							= FALSE
	BOOL bTradeInSlot3							= FALSE
	BOOL bTradeInSlot4							= FALSE
	BOOL bRandomizeContraTypes					= FALSE
	BOOL bTestRandomTypes						= FALSE
	BOOL bSetSpecialItem						= FALSE
	BOOL bSetDebugBuyMissionCount				= FALSE
	
	INT iSpecialItemType						= 1
	INT iWarehouseToPurchaseOrSell				= 1
	INT iWarehouseToAddOrRemoveContrabandFrom 	= 1
	INT iContrabandToAddOrRemove 				= 1
	INT iBuyMissionCompleteCount				= 1
	INT iContrabandType							= 0
	
	BOOL bContraTypeMeds						= TRUE
	BOOL bContraTypeTobacco						= FALSE
	BOOL bContraTypeArt							= FALSE
	BOOL bContraTypeElectronics					= FALSE
	BOOL bContraTypeWeapons						= FALSE
	BOOL bContraTypeNarcotics					= FALSE
	BOOL bContraTypeGems						= FALSE
	BOOL bContraTypeWildlife					= FALSE
	BOOL bContraTypeEntertainment				= FALSE
	BOOL bContraTypeJewelry						= FALSE
	BOOL bContraTypeBullion						= FALSE
	BOOL bContraTypeGoldenEgg					= FALSE
	BOOL bContraTypeGoldenMiniG					= FALSE
	BOOL bContraTypeJewel						= FALSE
	BOOL bContraTypeSasquatch					= FALSE
	BOOL bContraTypeFilmReel					= FALSE
	BOOL bContraTypeTigerHide					= FALSE
	BOOL bSpecialItem							= FALSE
	BOOL bDisableSpecialItem					= TRUE
	
	BOOL bdrawDebugForBGScript					= FALSE
	BOOL bSetCDTime								= FALSE
	INT iTypeRefreshCD							= 2880	
	SCRIPT_TIMER stTimerDisplay
ENDSTRUCT
#ENDIF

/////////////////////////////////////////
///    
///    Functions
///    

/// PURPOSE:
///    Checks that the passed property ID is that of any storage warehouse
FUNC BOOL IS_PROPERTY_A_WAREHOUSE(INT iPropertyID)
	
	IF (iPropertyID < 1)
	OR (iPropertyID > ciMaxWarehouses)		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Checks if the LOCAL player owns the specified warehouse
///    Validates the warehouse ID before checking if the player owns it
FUNC BOOL DOES_LOCAL_PLAYER_OWN_WAREHOUSE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse = iWarehouse
				CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] Local player owns warehouse ", iWarehouse, " in slot ", iIter)
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_LOCAL_PLAYER_OWN_WAREHOUSE. player does not own warehouse: ", iWarehouse)
	RETURN FALSE
ENDFUNC

/// PURPOSE: Gets a count of the number of warehouses owned by the local player
FUNC INT GET_COUNT_OF_WAREHOUSES_OWNED()
	INT iIter
	INT iWarehouseTotal = 0
	
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			iWarehouseTotal ++
		ELSE
			//Return here as we wont have any warehouses in slots past this slot
			//RETURN iWarehouseTotal
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_COUNT_OF_WAREHOUSES_OWNED: Returning: ", iWarehouseTotal)
	RETURN iWarehouseTotal
ENDFUNC

/// PURPOSE: Checks if the specified player owns the specified warehouse by checking their broadcast data
///   Validates the warehouse ID before checking if the player owns it
FUNC BOOL DOES_PLAYER_OWN_WAREHOUSE(PLAYER_INDEX playerID, INT iWarehouse)

	IF playerID = INVALID_PLAYER_INDEX()
		//CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_PLAYER_OWN_WAREHOUSE passed invalid player index!")
		RETURN FALSE
	ENDIF
	
	IF CHECK_WAREHOUSE_ID(iWarehouse)	
		INT iIter	
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse = iWarehouse
				CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] Player ", NATIVE_TO_INT(playerID), " owns warehouse ", iWarehouse, " in slot ", iIter)
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	
//	CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_PLAYER_OWN_WAREHOUSE. player does not own a warehouse")
	RETURN FALSE	
ENDFUNC

/// PURPOSE: Get's the players owned warehouse in the given slot
///   Validates the warehouse ID before checking if the player owns it
FUNC INT GET_PLAYER_OWNED_WAREHOUSE_IN_SLOT(PLAYER_INDEX playerID, INT iSlot)

	IF playerID = INVALID_PLAYER_INDEX()
		RETURN ciW_Invalid
	ENDIF
	
	IF iSlot > -1
	AND iSlot < ciMaxOwnedWarehouses
		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iSlot].iWarehouse
	ENDIF
	
	RETURN ciW_Invalid	
ENDFUNC

/// PURPOSE: Checks if the specified player owns a warehouse in the given slot
///   Validates the warehouse ID before checking if the player owns it
FUNC BOOL DOES_PLAYER_OWN_WAREHOUSE_IN_SLOT(PLAYER_INDEX playerID, INT iSlot)
	RETURN GET_PLAYER_OWNED_WAREHOUSE_IN_SLOT(playerID, iSlot) != ciW_Invalid
ENDFUNC

/// PURPOSE: Checks if the specified player owns any warehouses
FUNC BOOL DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE()
	INT iIter
	
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] Local player owns warehouse ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse, " in slot ", iIter, " [any]")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE. player does not own a warehouse")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if any friend in session owns the specified warehouse
/// PARAMS:
///    iWarehouseID - 
/// RETURNS:
///    
FUNC BOOL DOES_ANY_FRIEND_IN_SESSION_OWN_THIS_WAREHOUSE(INT iWarehouseID)
	IF CHECK_WAREHOUSE_ID(iWarehouseID)
		// This is set in a staggered loop that runs in freemode
		RETURN IS_BIT_SET(g_SimpleInteriorData.iOwnedByFriendBitSet, iWarehouseID - 1)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the total number of contraband units for the passed warehouse owned by the local player
///    Validates the warehouse ID before checking its contraband stock level
FUNC INT GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE: Player does not own this warehouse!")
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the total number of contraband units for the passed warehouse owned by the specified player
///    Validates the warehouse ID before checking its contraband stock level
FUNC INT GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE_OF_REMOTE_PLAYER(INT iWarehouse, PLAYER_INDEX piPlayerID)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(piPlayerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPlayerID)].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE: Player does not own this warehouse!")
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the total number of contraband units for the passed warehouse owned by the passed in player
///    Validates the warehouse ID before checking its contraband stock level
FUNC INT GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(PLAYER_INDEX playerID, INT iWarehouse) 
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE: Player does not own this warehouse!")
	RETURN 0
ENDFUNC

FUNC INT GET_EXPORT_ENTITY_UNITS_TOTAL_FOR_WAREHOUSE_OF_REMOTE_PLAYER(INT iWarehouse, PLAYER_INDEX piPlayerID)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(piPlayerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPlayerID)].propertyDetails.bdWarehouseData[iIter].iVehicleExportEntitesTotal
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_EXPORT_ENTITY_UNITS_TOTAL_FOR_WAREHOUSE_OF_REMOTE_PLAYER: Player does not own this warehouse!")
	RETURN 0
ENDFUNC
FUNC INT GET_PLAYER_EXPORT_ENTITY_UNITS_TOTAL_FOR_WAREHOUSE(PLAYER_INDEX playerID, INT iWarehouse) 
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iVehicleExportEntitesTotal
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_PLAYER_EXPORT_ENTITY_UNITS_TOTAL_FOR_WAREHOUSE: Player does not own this warehouse!")
	RETURN 0
ENDFUNC

FUNC INT GET_PLAYER_ILLICIT_GOOD_UNITS_TOTAL_FOR_WAREHOUSE(PLAYER_INDEX playerID, INT iWarehouse) 
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE: Player does not own this warehouse!")
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the total number of contraband units the player has in all warehouses
FUNC INT GET_PLAYER_CONTRABAND_TOTAL(PLAYER_INDEX playerID)
	INT iIter
	INT iContrabandTotal = 0
	
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			iContrabandTotal += GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal
		ELSE
			//Return here as we wont have any warehouses in slots past this slot 
			//(In debug we can sell warehouses so further slots may not be empty)
			//RETURN iContrabandTotal
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_PLAYER_CONTRABAND_TOTAL: Returning: ", iContrabandTotal)
	RETURN iContrabandTotal
ENDFUNC

FUNC VECTOR GET_WAREHOUSE_INTERIOR_CRATE_COORDS_OFFSET(INT iWarehouse, EWarehouseSize eWHouseSize, INT iSlot)

	VECTOR vOffset = <<0.0, 0.0, 0.0>>
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		SWITCH eWHouseSize
			CASE eWarehouseSmall
				SWITCH iSlot
					CASE 0    vOffset = <<-7.575, -0.150, -1.560>>  BREAK  // Lower Left
					CASE 1    vOffset = <<-5.050, -0.150, -1.560>>  BREAK  // Lower Right
					CASE 2    vOffset = <<-7.575, -0.150,  0.647>>  BREAK  // Upper Left
					CASE 3    vOffset = <<-5.050, -0.150,  0.647>>  BREAK  // Upper Right
					CASE 4    vOffset = <<-1.275, -0.150, -1.560>>  BREAK
					CASE 5    vOffset = << 1.250, -0.150, -1.560>>  BREAK
					CASE 6    vOffset = <<-1.275, -0.150,  0.647>>  BREAK
					CASE 7    vOffset = << 1.250, -0.150,  0.647>>  BREAK
					CASE 8    vOffset = << 4.925, -0.150, -1.560>>  BREAK
					CASE 9    vOffset = << 7.450, -0.150, -1.560>>  BREAK
					CASE 10   vOffset = << 4.925, -0.150,  0.647>>  BREAK
					CASE 11   vOffset = << 7.450, -0.150,  0.647>>  BREAK
					CASE 12   vOffset = <<-6.250, -5.875, -1.560>>  BREAK
					CASE 13   vOffset = <<-4.200, -5.875, -1.560>>  BREAK
					CASE 14   vOffset = <<-1.920, -5.875, -1.560>>  BREAK
					CASE 15   vOffset = << 0.100, -5.875, -1.560>>  BREAK
				ENDSWITCH
			BREAK
			
			CASE eWarehouseMedium
				SWITCH iSlot
					// +Left Rack
					CASE 0    vOffset = <<-5.375, 13.250, -3.185>>  BREAK
					CASE 1    vOffset = <<-2.950, 13.250, -3.185>>  BREAK
					CASE 2    vOffset = <<-0.525, 13.250, -3.185>>  BREAK
					CASE 3    vOffset = << 1.900, 13.250, -3.185>>  BREAK
					CASE 4    vOffset = << 4.325, 13.250, -3.185>>  BREAK
					CASE 5    vOffset = << 6.750, 13.250, -3.185>>  BREAK
					CASE 6    vOffset = << 9.175, 13.250, -3.185>>  BREAK
					CASE 7    vOffset = <<-5.375, 13.250, -0.974>>  BREAK
					CASE 8    vOffset = <<-2.950, 13.250, -0.974>>  BREAK
					CASE 9    vOffset = <<-0.525, 13.250, -0.974>>  BREAK
					CASE 10   vOffset = << 1.900, 13.250, -0.974>>  BREAK
					CASE 11   vOffset = << 4.325, 13.250, -0.974>>  BREAK
					CASE 12   vOffset = << 6.750, 13.250, -0.974>>  BREAK
					CASE 13   vOffset = << 9.175, 13.250, -0.974>>  BREAK
					// +Mid Rack
					CASE 14   vOffset = <<-5.375,  6.050, -3.185>>  BREAK
					CASE 15   vOffset = <<-2.950,  6.050, -3.185>>  BREAK
					CASE 16   vOffset = <<-0.525,  6.050, -3.185>>  BREAK
					CASE 17   vOffset = << 1.900,  6.050, -3.185>>  BREAK
					CASE 18   vOffset = << 4.325,  6.050, -3.185>>  BREAK
					CASE 19   vOffset = << 6.750,  6.050, -3.185>>  BREAK
					CASE 20   vOffset = << 9.175,  6.050, -3.185>>  BREAK
					CASE 21   vOffset = <<-5.375,  6.050, -0.974>>  BREAK
					CASE 22   vOffset = <<-2.950,  6.050, -0.974>>  BREAK
					CASE 23   vOffset = <<-0.525,  6.050, -0.974>>  BREAK
					CASE 24   vOffset = << 1.900,  6.050, -0.974>>  BREAK
					CASE 25   vOffset = << 4.325,  6.050, -0.974>>  BREAK
					CASE 26   vOffset = << 6.750,  6.050, -0.974>>  BREAK
					CASE 27   vOffset = << 9.175,  6.050, -0.974>>  BREAK
					// +Right Rack
					CASE 28   vOffset = <<-5.375, -1.150, -3.185>>  BREAK
					CASE 29   vOffset = <<-2.950, -1.150, -3.185>>  BREAK
					CASE 30   vOffset = <<-0.525, -1.150, -3.185>>  BREAK
					CASE 31   vOffset = << 1.900, -1.150, -3.185>>  BREAK
					CASE 32   vOffset = << 4.325, -1.150, -3.185>>  BREAK
					CASE 33   vOffset = << 6.750, -1.150, -3.185>>  BREAK
					CASE 34   vOffset = << 9.175, -1.150, -3.185>>  BREAK
					CASE 35   vOffset = <<-5.375, -1.150, -0.974>>  BREAK
					CASE 36   vOffset = <<-2.950, -1.150, -0.974>>  BREAK
					CASE 37   vOffset = <<-0.525, -1.150, -0.974>>  BREAK
					CASE 38   vOffset = << 1.900, -1.150, -0.974>>  BREAK
					CASE 39   vOffset = << 4.325, -1.150, -0.974>>  BREAK
					CASE 40   vOffset = << 6.750, -1.150, -0.974>>  BREAK
					CASE 41   vOffset = << 9.175, -1.150, -0.974>>  BREAK
				ENDSWITCH
			BREAK
			
			CASE eWarehouseLarge
				SWITCH iSlot
					// +Far Left Rack
					// Lower
					CASE 0    vOffset = <<-7.250, 16.850, -3.180>>  BREAK
					CASE 1    vOffset = <<-4.825, 16.850, -3.180>>  BREAK
					CASE 2    vOffset = <<-2.400, 16.850, -3.180>>  BREAK
					CASE 3    vOffset = << 0.025, 16.850, -3.180>>  BREAK
					CASE 4    vOffset = << 2.450, 16.850, -3.180>>  BREAK
					CASE 5    vOffset = << 4.875, 16.850, -3.180>>  BREAK
					CASE 6    vOffset = << 7.300, 16.850, -3.180>>  BREAK
					// Mid
					CASE 7    vOffset = <<-7.250, 16.850, -0.971>>  BREAK
					CASE 8    vOffset = <<-4.825, 16.850, -0.971>>  BREAK
					CASE 9    vOffset = <<-2.400, 16.850, -0.971>>  BREAK
					CASE 10   vOffset = << 0.025, 16.850, -0.971>>  BREAK
					CASE 11   vOffset = << 2.450, 16.850, -0.971>>  BREAK
					CASE 12   vOffset = << 4.875, 16.850, -0.971>>  BREAK
					CASE 13   vOffset = << 7.300, 16.850, -0.971>>  BREAK
					// Upper
					CASE 14   vOffset = <<-7.250, 16.850,  1.059>>  BREAK
					CASE 15   vOffset = <<-4.825, 16.850,  1.059>>  BREAK
					CASE 16   vOffset = <<-2.400, 16.850,  1.059>>  BREAK
					CASE 17   vOffset = << 0.025, 16.850,  1.059>>  BREAK
					CASE 18   vOffset = << 2.450, 16.850,  1.059>>  BREAK
					CASE 19   vOffset = << 4.875, 16.850,  1.059>>  BREAK
					CASE 20   vOffset = << 7.300, 16.850,  1.059>>  BREAK
					// +Middle Left Rack
					// Lower
					CASE 21   vOffset = <<-7.250, 11.500, -3.180>>  BREAK
					CASE 22   vOffset = <<-4.825, 11.500, -3.180>>  BREAK
					CASE 23   vOffset = <<-2.400, 11.500, -3.180>>  BREAK
					CASE 24   vOffset = << 0.025, 11.500, -3.180>>  BREAK
					CASE 25   vOffset = << 2.450, 11.500, -3.180>>  BREAK
					CASE 26   vOffset = << 4.875, 11.500, -3.180>>  BREAK
					CASE 27   vOffset = << 7.300, 11.500, -3.180>>  BREAK
					// Mid
					CASE 28   vOffset = <<-7.250, 11.500, -0.971>>  BREAK
					CASE 29   vOffset = <<-4.825, 11.500, -0.971>>  BREAK
					CASE 30   vOffset = <<-2.400, 11.500, -0.971>>  BREAK
					CASE 31   vOffset = << 0.025, 11.500, -0.971>>  BREAK
					CASE 32   vOffset = << 2.450, 11.500, -0.971>>  BREAK
					CASE 33   vOffset = << 4.875, 11.500, -0.971>>  BREAK
					CASE 34   vOffset = << 7.300, 11.500, -0.971>>  BREAK
					// Upper
					CASE 35   vOffset = <<-7.250, 11.500,  1.059>>  BREAK
					CASE 36   vOffset = <<-4.825, 11.500,  1.059>>  BREAK
					CASE 37   vOffset = <<-2.400, 11.500,  1.059>>  BREAK
					CASE 38   vOffset = << 0.025, 11.500,  1.059>>  BREAK
					CASE 39   vOffset = << 2.450, 11.500,  1.059>>  BREAK
					CASE 40   vOffset = << 4.875, 11.500,  1.059>>  BREAK
					CASE 41   vOffset = << 7.300, 11.500,  1.059>>  BREAK
					// +Middle Right Rack
					// Lower
					CASE 42   vOffset = <<-7.250,  5.650, -3.180>>  BREAK
					CASE 43   vOffset = <<-4.825,  5.650, -3.180>>  BREAK
					CASE 44   vOffset = <<-2.400,  5.650, -3.180>>  BREAK
					CASE 45   vOffset = << 0.025,  5.650, -3.180>>  BREAK
					CASE 46   vOffset = << 2.450,  5.650, -3.180>>  BREAK
					CASE 47   vOffset = << 4.875,  5.650, -3.180>>  BREAK
					CASE 48   vOffset = << 7.300,  5.650, -3.180>>  BREAK
					// Mid
					CASE 49   vOffset = <<-7.250,  5.650, -0.971>>  BREAK
					CASE 50   vOffset = <<-4.825,  5.650, -0.971>>  BREAK
					CASE 51   vOffset = <<-2.400,  5.650, -0.971>>  BREAK
					CASE 52   vOffset = << 0.025,  5.650, -0.971>>  BREAK
					CASE 53   vOffset = << 2.450,  5.650, -0.971>>  BREAK
					CASE 54   vOffset = << 4.875,  5.650, -0.971>>  BREAK
					CASE 55   vOffset = << 7.300,  5.650, -0.971>>  BREAK
					// Upper
					CASE 56   vOffset = <<-7.250,  5.650,  1.059>>  BREAK
					CASE 57   vOffset = <<-4.825,  5.650,  1.059>>  BREAK
					CASE 58   vOffset = <<-2.400,  5.650,  1.059>>  BREAK
					CASE 59   vOffset = << 0.025,  5.650,  1.059>>  BREAK
					CASE 60   vOffset = << 2.450,  5.650,  1.059>>  BREAK
					CASE 61   vOffset = << 4.875,  5.650,  1.059>>  BREAK
					CASE 62   vOffset = << 7.300,  5.650,  1.059>>  BREAK
					// +Far Right Rack
					// Upper
					CASE 63   vOffset = <<-7.250, -0.050, -3.180>>  BREAK
					CASE 64   vOffset = <<-4.825, -0.050, -3.180>>  BREAK
					CASE 65   vOffset = <<-2.400, -0.050, -3.180>>  BREAK
					CASE 66   vOffset = << 0.025, -0.050, -3.180>>  BREAK
					CASE 67   vOffset = << 2.450, -0.050, -3.180>>  BREAK
					CASE 68   vOffset = << 4.875, -0.050, -3.180>>  BREAK
					CASE 69   vOffset = << 7.300, -0.050, -3.180>>  BREAK
					// Mid
					CASE 70   vOffset = <<-7.250, -0.050, -0.971>>  BREAK
					CASE 71   vOffset = <<-4.825, -0.050, -0.971>>  BREAK
					CASE 72   vOffset = <<-2.400, -0.050, -0.971>>  BREAK
					CASE 73   vOffset = << 0.025, -0.050, -0.971>>  BREAK
					CASE 74   vOffset = << 2.450, -0.050, -0.971>>  BREAK
					CASE 75   vOffset = << 4.875, -0.050, -0.971>>  BREAK
					CASE 76   vOffset = << 7.300, -0.050, -0.971>>  BREAK
					// Upper
					CASE 77   vOffset = <<-7.250, -0.050,  1.059>>  BREAK
					CASE 78   vOffset = <<-4.825, -0.050,  1.059>>  BREAK
					CASE 79   vOffset = <<-2.400, -0.050,  1.059>>  BREAK
					CASE 80   vOffset = << 0.025, -0.050,  1.059>>  BREAK
					CASE 81   vOffset = << 2.450, -0.050,  1.059>>  BREAK
					CASE 82   vOffset = << 4.875, -0.050,  1.059>>  BREAK
					CASE 83   vOffset = << 7.300, -0.050,  1.059>>  BREAK
					// +Front Left Rack
					CASE 84   vOffset = <<15.875, 16.860, -3.180>>  BREAK
					CASE 85   vOffset = <<15.875, 14.440, -3.180>>  BREAK
					CASE 86   vOffset = <<15.875, 12.020, -3.180>>  BREAK
					CASE 87   vOffset = <<15.875, 16.860, -0.971>>  BREAK
					CASE 88   vOffset = <<15.875, 14.440, -0.971>>  BREAK
					CASE 89   vOffset = <<15.875, 12.020, -0.971>>  BREAK
					CASE 90   vOffset = <<15.875, 16.860,  1.059>>  BREAK
					CASE 91   vOffset = <<15.875, 14.440,  1.059>>  BREAK
					CASE 92   vOffset = <<15.875, 12.020,  1.059>>  BREAK
					// +Front Right Rack
					CASE 93   vOffset = <<15.875,  1.925, -3.180>>  BREAK
					CASE 94   vOffset = <<15.875, -0.435, -3.180>>  BREAK
					CASE 95   vOffset = <<15.875, -2.930, -3.180>>  BREAK
					CASE 96   vOffset = <<15.875,  1.925, -0.971>>  BREAK
					CASE 97   vOffset = <<15.875, -0.435, -0.971>>  BREAK
					CASE 98   vOffset = <<15.875, -2.930, -0.971>>  BREAK
					CASE 99   vOffset = <<15.875,  1.925,  1.059>>  BREAK
					CASE 100  vOffset = <<15.875, -0.435,  1.059>>  BREAK
					CASE 101  vOffset = <<15.875, -2.930,  1.059>>  BREAK
					// +Back Right Rack
					CASE 102  vOffset = <<-17.525, -2.850, -3.180>>  BREAK
					CASE 103  vOffset = <<-17.525, -0.500, -3.180>>  BREAK
					CASE 104  vOffset = <<-17.525,  1.850, -3.180>>  BREAK
					CASE 105  vOffset = <<-17.525, -2.850, -0.971>>  BREAK
					CASE 106  vOffset = <<-17.525, -0.500, -0.971>>  BREAK
					CASE 107  vOffset = <<-17.525,  1.850, -0.971>>  BREAK
					CASE 108  vOffset = <<-17.525, -2.850,  1.059>>  BREAK
					CASE 109  vOffset = <<-17.525, -0.500,  1.059>>  BREAK
					CASE 110  vOffset = <<-17.525,  1.850,  1.059>>  BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	CDEBUG1LN(DEBUG_PROPERTY, "NET_REALTY_WAREHOUSE - GET_WAREHOUSE_INTERIOR_CRATE_COORDS_OFFSET - Crate Offset: ", vOffset)
	RETURN vOffset
ENDFUNC

FUNC VECTOR GET_WAREHOUSE_INTERIOR_CRATE_ROTATION(INT iWarehouse, EWarehouseSize eWHouseSize, INT iCrateSlot)
	
	VECTOR vRotation = <<0,0,0>>
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		SWITCH eWHouseSize
			CASE eWarehouseSmall
				// Floor crates face inwards
				IF iCrateSlot >= 12
					vRotation = <<0,0,180>>
				ENDIF
			BREAK
			CASE eWarehouseMedium
				// Mid shelf facing both ways
				IF iCrateSlot = 14
				OR iCrateSlot = 16
				OR iCrateSlot = 17
				OR iCrateSlot = 22
				OR iCrateSlot = 23
				OR iCrateSlot = 25
				OR iCrateSlot = 27
					vRotation = <<0,0,180>>
				ENDIF
				//Right shelf face inwards
				IF iCrateSlot > 27
					vRotation = <<0,0,180>>
				ENDIF
			BREAK
			CASE eWarehouseLarge
				// Mid left shelf facing both ways
				IF iCrateSlot = 21
				OR iCrateSlot = 22
				OR iCrateSlot = 24
				OR iCrateSlot = 27
				OR iCrateSlot = 28
				OR iCrateSlot = 30
				OR iCrateSlot = 31
				OR iCrateSlot = 32
				OR iCrateSlot = 36
				OR iCrateSlot = 37
				OR iCrateSlot = 40
				OR iCrateSlot = 41
					vRotation = <<0,0,180>>
				ENDIF
				// Mid right shelf facing both ways
				IF iCrateSlot = 21
				OR iCrateSlot = 42
				OR iCrateSlot = 45
				OR iCrateSlot = 46
				OR iCrateSlot = 49
				OR iCrateSlot = 53
				OR iCrateSlot = 54
				OR iCrateSlot = 56
				OR iCrateSlot = 59
				OR iCrateSlot = 60
				OR iCrateSlot = 62
					vRotation = <<0,0,180>>
				ENDIF
				// Far right shelf facing both ways
				IF iCrateSlot = 63
				OR iCrateSlot = 64
				OR iCrateSlot = 66
				OR iCrateSlot = 69
				OR iCrateSlot = 70
				OR iCrateSlot = 74
				OR iCrateSlot = 76
				OR iCrateSlot = 77
				OR iCrateSlot = 80
				OR iCrateSlot = 81
				OR iCrateSlot = 83
					vRotation = <<0,0,180>>
				ENDIF
				// Front left & right shelf facing both ways
				IF iCrateSlot > 83 AND iCrateSlot < 102
					vRotation = <<0,0,270>>
				ENDIF
				// Back right shelf facing both ways
				IF iCrateSlot >= 102 AND iCrateSlot < 111
					vRotation = <<0,0,90>>
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	RETURN vRotation
ENDFUNC

FUNC WAREHOUSE_CRATE_SIZE GET_WAREHOUSE_CREATE_SIZE_FROM_CRATE_SLOT(EWarehouseSize eWHouseSize, INT iCrateSlot)
	SWITCH eWHouseSize
		CASE eWarehouseSmall
		CASE eWarehouseMedium
			RETURN INT_TO_ENUM(WAREHOUSE_CRATE_SIZE, GET_RANDOM_INT_IN_RANGE(0,2))
			
		CASE eWarehouseLarge
			// Top shelves to be large only
			IF iCrateSlot > 13 AND iCrateSlot < 21
			OR iCrateSlot > 34 AND iCrateSlot < 42
			OR iCrateSlot > 55 AND iCrateSlot < 63
			OR iCrateSlot > 76 AND iCrateSlot < 84
			OR iCrateSlot > 89 AND iCrateSlot < 93
			OR iCrateSlot > 98 AND iCrateSlot < 102
			OR iCrateSlot > 107 AND iCrateSlot < 109
				RETURN WAREHOUSE_CRATE_SIZE_LARGE
			ELSE
				RETURN INT_TO_ENUM(WAREHOUSE_CRATE_SIZE, GET_RANDOM_INT_IN_RANGE(0,2))
			ENDIF
		BREAK
	ENDSWITCH
	RETURN WAREHOUSE_CRATE_SIZE_INVALID
ENDFUNC

FUNC BOOL IS_WAREHOUSE_CRATE_ON_BOTTOM_SHELF(EWarehouseSize eWHouseSize, INT iSlot)
	SWITCH eWHouseSize
		// Skip Small and Medium warehouses as players can see all their slots
		// Large warehouse has high shelves that players can't see so well
		CASE eWarehouseSmall
		CASE eWarehouseMedium
			RETURN TRUE
		CASE eWarehouseLarge
			IF iSlot < 7
			OR iSlot > 20 AND iSlot < 28
			OR iSlot > 41 AND iSlot < 49
			OR iSlot > 62 AND iSlot < 70
			OR iSlot > 83 AND iSlot < 87
			OR iSlot > 92 AND iSlot < 96
			OR iSlot > 101 AND iSlot < 105
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WAREHOUSE_CRATE_ON_TOP_SHELF(EWarehouseSize eWHouseSize, INT iSlot)
	SWITCH eWHouseSize
		// Skip Small and Medium warehouses as players can see all their slots
		// Large warehouse has high shelves that players can't see so well
		CASE eWarehouseSmall
		CASE eWarehouseMedium
			RETURN FALSE
		CASE eWarehouseLarge
			IF iSlot > 13 AND iSlot < 21
			OR iSlot > 34 AND iSlot < 42
			OR iSlot > 55 AND iSlot < 63
			OR iSlot > 76 AND iSlot < 84
			OR iSlot > 89 AND iSlot < 93
			OR iSlot > 98 AND iSlot < 102
			OR iSlot > 107 AND iSlot < 109
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_WAREHOUSE_SHELF_COORDS(EWarehouseSize eWHouseSize)
	SWITCH eWHouseSize
		CASE eWarehouseSmall	RETURN <<1096.3359, -3096.5315, -38.4359>>
		CASE eWarehouseMedium	RETURN <<1058.4756, -3108.6985, -36.8147>>
		CASE eWarehouseLarge	RETURN <<1010.8801, -3108.4541, -36.8147>>
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC BOOL DOES_CONTRABAND_TYPE_HAVE_MODEL_VARIATION(INT iContrabandType)
	CONTRABAND_TYPE eContrabandType = INT_TO_ENUM(CONTRABAND_TYPE, iContrabandType)
	SWITCH eContrabandType
		CASE CONTRABAND_TYPE_BULLION 
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_WAREHOUSE_SPECIAL_CRATE_MODEL(INT iContrabandType)
	CONTRABAND_TYPE eContrabandType = INT_TO_ENUM(CONTRABAND_TYPE, iContrabandType)
	
	SWITCH eContrabandType
		CASE CONTRABAND_TYPE_ART_AND_ANTIQUES RETURN EX_PROP_CRATE_OEGG
		CASE CONTRABAND_TYPE_WEAPONS_AND_AMMO RETURN EX_PROP_CRATE_MINIG
		CASE CONTRABAND_TYPE_GEMS 			  RETURN EX_PROP_CRATE_XLDIAM
		CASE CONTRABAND_TYPE_WILDLIFE 		  RETURN EX_PROP_CRATE_SHIDE
		CASE CONTRABAND_TYPE_ENTERTAINMENT    RETURN EX_PROP_CRATE_FREEL
		CASE CONTRABAND_TYPE_JEWELRY 		  RETURN EX_PROP_CRATE_WATCH
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC MODEL_NAMES GET_WAREHOUSE_CRATE_MODEL(INT iContrabandType, INT iCrateSize, BOOL bClosedCrate = FALSE)
	
	INT iVariationChance = GET_RANDOM_INT_IN_RANGE(0, 2)
	CONTRABAND_TYPE eContrabandType = INT_TO_ENUM(CONTRABAND_TYPE, iContrabandType)
	WAREHOUSE_CRATE_SIZE eCrateSize = INT_TO_ENUM(WAREHOUSE_CRATE_SIZE, iCrateSize)
	
	IF bClosedCrate
		SWITCH eContrabandType
			CASE CONTRABAND_TYPE_MEDICAL_SUPPLIES
				SWITCH eCrateSize
					CASE WAREHOUSE_CRATE_SIZE_SMALL
						SWITCH iVariationChance
							CASE 0 RETURN EX_PROP_CRATE_CLOSED_SC
							CASE 1 RETURN EX_PROP_CRATE_CLOSED_MS
						ENDSWITCH
					BREAK
					CASE WAREHOUSE_CRATE_SIZE_LARGE RETURN EX_PROP_CRATE_CLOSED_BC
				ENDSWITCH
			BREAK
			CASE CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL
				SWITCH eCrateSize
					CASE WAREHOUSE_CRATE_SIZE_SMALL
						SWITCH iVariationChance
							CASE 0 RETURN EX_PROP_CRATE_CLOSED_SC
							CASE 1 RETURN EX_PROP_CRATE_CLOSED_RW
						ENDSWITCH
					BREAK
					CASE WAREHOUSE_CRATE_SIZE_LARGE RETURN EX_PROP_CRATE_CLOSED_BC
				ENDSWITCH
			BREAK
			CASE CONTRABAND_TYPE_WEAPONS_AND_AMMO
				SWITCH eCrateSize
					CASE WAREHOUSE_CRATE_SIZE_SMALL
						SWITCH iVariationChance
							CASE 0 RETURN EX_PROP_CRATE_CLOSED_SC
							CASE 1 RETURN EX_PROP_CRATE_CLOSED_MW
						ENDSWITCH
					BREAK
					CASE WAREHOUSE_CRATE_SIZE_LARGE RETURN EX_PROP_CRATE_CLOSED_BC
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
	SWITCH eContrabandType
		CASE CONTRABAND_TYPE_MEDICAL_SUPPLIES
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_MED_SC
						CASE 1 RETURN EX_PROP_CRATE_BIOHAZARD_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_MED_BC
						CASE 1 RETURN EX_PROP_CRATE_BIOHAZARD_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL	RETURN EX_PROP_CRATE_TOB_SC
				CASE WAREHOUSE_CRATE_SIZE_LARGE	RETURN EX_PROP_CRATE_TOB_BC
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_ART_AND_ANTIQUES
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL	
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_ART_SC
						CASE 1 RETURN EX_PROP_CRATE_ART_02_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_ART_BC
						CASE 1 RETURN EX_PROP_CRATE_ART_02_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_ELECTRONICS
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL	RETURN EX_PROP_CRATE_ELEC_SC
				CASE WAREHOUSE_CRATE_SIZE_LARGE	RETURN EX_PROP_CRATE_ELEC_BC
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_WEAPONS_AND_AMMO
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_AMMO_SC
						CASE 1 RETURN EX_PROP_CRATE_EXPL_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE	
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_AMMO_BC
						CASE 1 RETURN EX_PROP_CRATE_EXPL_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_NARCOTICS
			iVariationChance = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_NARC_SC
						CASE 1 RETURN EX_PROP_CRATE_PHARMA_SC
						CASE 2 RETURN EX_PROP_CRATE_HIGHEND_PHARMA_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_NARC_BC
						CASE 1 RETURN EX_PROP_CRATE_PHARMA_BC
						CASE 2 RETURN EX_PROP_CRATE_HIGHEND_PHARMA_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_GEMS
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL	RETURN EX_PROP_CRATE_GEMS_SC
				CASE WAREHOUSE_CRATE_SIZE_LARGE	RETURN EX_PROP_CRATE_GEMS_BC
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_WILDLIFE
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_WLIFE_SC
						CASE 1 RETURN EX_PROP_CRATE_FURJACKET_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE	
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_WLIFE_BC
						CASE 1 RETURN EX_PROP_CRATE_FURJACKET_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_ENTERTAINMENT
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL	
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_MONEY_SC
						CASE 1 RETURN EX_PROP_CRATE_CLOTHING_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE	
				SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_MONEY_BC
						CASE 1 RETURN EX_PROP_CRATE_CLOTHING_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_JEWELRY
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_JEWELS_SC
						CASE 1 RETURN EX_PROP_CRATE_JEWELS_RACKS_SC
					ENDSWITCH
				BREAK
				CASE WAREHOUSE_CRATE_SIZE_LARGE
					SWITCH iVariationChance
						CASE 0 RETURN EX_PROP_CRATE_JEWELS_BC
						CASE 1 RETURN EX_PROP_CRATE_JEWELS_RACKS_BC
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CONTRABAND_TYPE_BULLION
			SWITCH eCrateSize
				CASE WAREHOUSE_CRATE_SIZE_SMALL RETURN EX_PROP_CRATE_BULL_SC_02
				CASE WAREHOUSE_CRATE_SIZE_LARGE RETURN EX_PROP_CRATE_BULL_BC_02
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC INT GET_WAREHOUSE_SIZE_INT(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		SWITCH iWarehouse
			CASE ciW_Elysian_Island_2  
			CASE ciW_La_Puerta_1 
			CASE ciW_La_Mesa_1 
			CASE ciW_Rancho_1	 
			CASE ciW_West_Vinewood_1	
			CASE ciW_Elysian_Island_1 
				RETURN ciSMALL_WAREHOUSE_CRATE_CAPACITY
				
			CASE ciW_El_Burro_Heights_1 
			CASE ciW_Elysian_Island_3 	
			CASE ciW_La_Mesa_2 			
			CASE ciW_Maze_Bank_Arena_1 	
			CASE ciW_Strawberry_1		
			CASE ciW_Downtown_Vinewood_1
			CASE ciW_Rancho_2
			CASE ciW_Del_Perro_1
				RETURN ciMEDIUM_WAREHOUSE_CRATE_CAPACITY
				
			CASE ciW_La_Mesa_3 			
			CASE ciW_La_Mesa_4 			
			CASE ciW_Cypress_Flats_2 	
			CASE ciW_Cypress_Flats_3 	
			CASE ciW_Vinewood_1 				
			CASE ciW_Banning_1
			CASE ciW_LSIA_1	
			CASE ciW_LSIA_2
				RETURN ciLARGE_WAREHOUSE_CRATE_CAPACITY
		ENDSWITCH
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE: 
///    Gets the owned warehouses of a specidied player
FUNC INT GET_NUMBER_OF_WAREHOUSES_OWNED_BY_PLAYER(PLAYER_INDEX playerID)
	INT iIter, iNumber
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse > ciW_Invalid
		AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse <= ciMaxWarehouses
			iNumber++
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_NUMBER_OF_WAREHOUSES_OWNED_BY_PLAYER. player owns: ", iNumber)
	RETURN iNumber
ENDFUNC

/// PURPOSE: 
///    Gets a count of hom many warehouses of a particulat size are owned by the specidied player
FUNC INT GET_COUNT_OF_WAREHOUSES_OWNED_BY_SIZE(EWarehouseSize size, PLAYER_INDEX playerID)
	INT iIter, iCount
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse > ciW_Invalid
		AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse <= ciMaxWarehouses
		AND GET_WAREHOUSE_SIZE(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse) = size
			iCount ++
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_COUNT_OF_WAREHOUSES_OWNED_BY_SIZE. player owns: ", iCount, " Warehouses of size: ", ENUM_TO_INT(size))
	RETURN iCount
ENDFUNC

/// PURPOSE: Checks if the LOCAL player owns a warehouse of the specified size
FUNC BOOL DOES_LOCAL_PLAYER_OWN_WAREHOUSE_OF_SIZE(EWarehouseSize size, INT iExcludeWarehouseID = -1)	
	INT iIter
	
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != iExcludeWarehouseID
		AND GET_WAREHOUSE_SIZE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse) = size
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_LOCAL_PLAYER_OWN_WAREHOUSE_OF_SIZE. player does own warehouse of size: ", size)
			RETURN TRUE
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_LOCAL_PLAYER_OWN_WAREHOUSE_OF_SIZE. player does not own warehouse of size: ", size)
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the coordinates of the specified warehouse
///    Validates the warehouse ID before returning its coords
FUNC VECTOR GET_WAREHOUSE_COORDS(INT iWarehouse)
	
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	
	IF NOT CHECK_WAREHOUSE_ID(iWarehouse)
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_COORDS: ", iWarehouse, " Invalid warehouse returning: ", vCoords)
		RETURN vCoords
	ENDIF
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2  		vCoords = <<50.9205, -2560.0098, 6.0>> 			BREAK
		CASE ciW_La_Puerta_1 			vCoords = <<-1074.8517, -1257.4519, 5.6938>> 	BREAK
		CASE ciW_La_Mesa_1 				vCoords = <<901.9360, -1019.4255, 34.9666>> 	BREAK
		CASE ciW_Rancho_1	 			vCoords = <<253.9006, -1955.9329, 22.9477>> 	BREAK
		CASE ciW_West_Vinewood_1		vCoords = <<-426.2790, 180.9043, 80.6855>> 		BREAK
		CASE ciW_LSIA_1					vCoords = <<-1050.3604, -2023.9388, 12.1616>> 	BREAK
		CASE ciW_Del_Perro_1 			vCoords = <<-1275.3776, -815.4295, 17.1148>> 	BREAK
		CASE ciW_LSIA_2 				vCoords = <<-883.2906, -2730.5417, 12.8285>> 	BREAK
		CASE ciW_Elysian_Island_1 		vCoords = <<269.6765, -3016.2786, 5.7176>> 		BREAK
		CASE ciW_El_Burro_Heights_1 	vCoords = <<1561.4023, -2138.6965, 77.5275>>	BREAK
		CASE ciW_Elysian_Island_3 		vCoords = <<-303.7892, -2698.3901, 6.0003>> 	BREAK
		CASE ciW_La_Mesa_2 				vCoords = <<507.2061, -649.5859, 24.7512>> 		BREAK
		CASE ciW_Maze_Bank_Arena_1 		vCoords = <<-525.8206, -1775.8699, 21.3501>> 	BREAK
		CASE ciW_Strawberry_1			vCoords = <<-291.6372, -1352.5558, 31.3146>> 	BREAK
		CASE ciW_Downtown_Vinewood_1	vCoords = <<348.6436, 324.4280, 104.3185>> 		BREAK
		CASE ciW_La_Mesa_3 				vCoords = <<922.0838, -1561.2987, 29.7475>> 	BREAK
		CASE ciW_La_Mesa_4 				vCoords = <<763.7448, -885.2086, 25.0869>> 		BREAK
		CASE ciW_Cypress_Flats_2 		vCoords = <<1043.8840, -2172.6033, 30.4710>> 	BREAK
		CASE ciW_Cypress_Flats_3 		vCoords = <<1001.6589, -2511.8804, 28.3020>> 	BREAK
		CASE ciW_Vinewood_1 			vCoords = <<-263.1380, 194.3424, 85.1840>> 		BREAK
		CASE ciW_Rancho_2 				vCoords = <<547.8112, -1942.5139, 23.9851>> 	BREAK
		CASE ciW_Banning_1 				vCoords = <<88.9678, -2216.8818, 6.0613>>		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_COORDS: ", iWarehouse, " valid warehouse found. Returning: ", vCoords)
	RETURN vCoords
ENDFUNC

FUNC VECTOR GET_WAREHOUSE_CRATE_COORDS(INT iWarehouse, INT iSlot)
	
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	
	IF NOT CHECK_WAREHOUSE_ID(iWarehouse)
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_CRATE_COORDS: ", iWarehouse, " Invalid warehouse returning: ", vCoords)
		RETURN vCoords
	ENDIF
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2 
			SWITCH iSlot				
				CASE 0 vCoords = <<58.1407, -2564.0688, 5.0046>> BREAK
				CASE 1 vCoords = <<48.8902, -2568.1853, 5.0046>> BREAK
				CASE 2 vCoords = <<52.7589, -2569.4202, 5.0046>> BREAK
				CASE 3 vCoords = <<65.5023, -2567.2563, 5.0046>> BREAK
				CASE 4 vCoords = <<70.4458, -2561.1528, 5.0000>> BREAK
				CASE 5 vCoords = <<73.8207, -2537.8933, 5.0000>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_La_Puerta_1 			
			SWITCH iSlot				
				CASE 0 vCoords = <<-1075.3143, -1267.2471, 4.9089>> BREAK
				CASE 1 vCoords = <<-1082.0457, -1254.0723, 4.4202>> BREAK
				CASE 2 vCoords = <<-1099.6638, -1255.2747, 4.2017>> BREAK
				CASE 3 vCoords = <<-1077.9191, -1238.6226, 4.1393>> BREAK
				CASE 4 vCoords = <<-1086.0402, -1239.4180, 4.0283>> BREAK
				CASE 5 vCoords = <<-1069.7028, -1251.1891, 4.7555>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_La_Mesa_1 			
			SWITCH iSlot
				CASE 0 vCoords = <<891.6446, -1015.9857, 33.9642>> BREAK
				CASE 1 vCoords = <<898.4188, -1031.3158, 33.9666>> BREAK
				CASE 2 vCoords = <<899.2636, -1016.2461, 33.9653>> BREAK
				CASE 3 vCoords = <<903.1688, -1049.4071, 31.8281>> BREAK
				CASE 4 vCoords = <<896.1467, -1063.0946, 31.8284>> BREAK
				CASE 5 vCoords = <<909.6455, -1054.0371, 31.8281>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Rancho_1	 		
			SWITCH iSlot
				CASE 0 vCoords = <<246.7747, -1954.1364, 22.2125>> BREAK
				CASE 1 vCoords = <<251.2670, -1949.6804, 22.6225>> BREAK
				CASE 2 vCoords = <<266.7080, -1961.3212, 22.1036>> BREAK
				CASE 3 vCoords = <<264.3738, -1970.8793, 21.2458>> BREAK
				CASE 4 vCoords = <<247.8190, -1969.2994, 20.9571>> BREAK
				CASE 5 vCoords = <<249.4811, -1958.0352, 22.1590>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_West_Vinewood_1		
			SWITCH iSlot				
				CASE 0 vCoords = <<-420.8889, 184.7032, 79.6799>> BREAK
				CASE 1 vCoords = <<-425.3852, 181.2360, 79.6334>> BREAK
				CASE 2 vCoords = <<-397.6064, 189.7199, 80.4766>> BREAK
				CASE 3 vCoords = <<-404.8292, 180.9603, 79.0463>> BREAK
				CASE 4 vCoords = <<-407.3180, 189.6532, 80.3635>> BREAK
				CASE 5 vCoords = <<-398.0254, 196.6035, 82.1793>> BREAK
			ENDSWITCH	
		BREAK
		CASE ciW_LSIA_1		
			SWITCH iSlot								
				CASE 0 vCoords = <<-1046.7917, -2023.5059, 12.1616>> BREAK
				CASE 1 vCoords = <<-1053.3463, -2022.7838, 12.1616>> BREAK
				CASE 2 vCoords = <<-1057.0607, -2020.4834, 12.1616>> BREAK
				CASE 3 vCoords = <<-1055.1471, -2015.9855, 12.1616>> BREAK
				CASE 4 vCoords = <<-1058.9725, -2012.9792, 12.1616>> BREAK
				CASE 5 vCoords = <<-1065.4832, -2012.6023, 12.1616>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Del_Perro_1 			
			SWITCH iSlot
				CASE 0 vCoords = <<-1282.1360, -804.4910, 16.5362>> BREAK
				CASE 1 vCoords = <<-1276.7335, -806.2763, 16.2988>> BREAK
				CASE 2 vCoords = <<-1270.4811, -814.0359, 16.1077>> BREAK
				CASE 3 vCoords = <<-1267.1460, -826.1518, 16.0992>> BREAK
				CASE 4 vCoords = <<-1280.8528, -829.8271, 16.0992>> BREAK
				CASE 5 vCoords = <<-1288.2783, -814.8292, 16.3336>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_LSIA_2 		
			SWITCH iSlot				
				CASE 0 vCoords = <<-875.6189, -2730.5769, 12.8285>> BREAK
				CASE 1 vCoords = <<-877.4825, -2737.7102, 12.8577>> BREAK
				CASE 2 vCoords = <<-884.0042, -2737.9275, 12.8285>> BREAK
				CASE 3 vCoords = <<-888.5006, -2744.6555, 12.8285>> BREAK
				CASE 4 vCoords = <<-876.5549, -2723.1450, 12.9446>> BREAK
				CASE 5 vCoords = <<-880.0393, -2725.3059, 12.8285>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Elysian_Island_1 
			SWITCH iSlot				
				CASE 0 vCoords = <<272.6804, -3015.4368, 4.7063>> BREAK
				CASE 1 vCoords = <<272.7151, -3031.1863, 4.7067>> BREAK
				CASE 2 vCoords = <<257.4472, -3026.8494, 4.7652>> BREAK
				CASE 3 vCoords = <<257.3760, -3011.4102, 4.7655>> BREAK
				CASE 4 vCoords = <<264.2401, -2981.8782, 3.9268>> BREAK
				CASE 5 vCoords = <<285.0505, -2982.3599, 4.5546>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_El_Burro_Heights_1 
			SWITCH iSlot				
				CASE 0 vCoords = <<1571.3472, -2143.4773, 76.6841>> BREAK
				CASE 1 vCoords = <<1548.2136, -2141.8391, 76.4531>> BREAK
				CASE 2 vCoords = <<1539.1769, -2144.4922, 76.4120>> BREAK
				CASE 3 vCoords = <<1555.5262, -2153.3149, 76.5056>> BREAK
				CASE 4 vCoords = <<1554.6790, -2102.8909, 76.4286>> BREAK
				CASE 5 vCoords = <<1523.4232, -2114.5339, 75.5872>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Elysian_Island_3 		
			SWITCH iSlot
				CASE 0 vCoords = <<-304.9910, -2702.4124, 5.0003>> BREAK
				CASE 1 vCoords = <<-273.4101, -2702.1838, 5.0003>> BREAK
				CASE 2 vCoords = <<-286.9616, -2698.9631, 5.0003>> BREAK
				CASE 3 vCoords = <<-286.9148, -2715.4265, 5.0003>> BREAK
				CASE 4 vCoords = <<-314.1167, -2721.5037, 5.0003>> BREAK
				CASE 5 vCoords = <<-304.0762, -2731.2512, 5.0003>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_La_Mesa_2 			
			SWITCH iSlot
				CASE 0 vCoords = <<519.8583, -652.6852, 23.7512>> BREAK
				CASE 1 vCoords = <<519.9045, -640.8054, 23.7512>> BREAK
				CASE 2 vCoords = <<520.7178, -631.8808, 23.7513>> BREAK
				CASE 3 vCoords = <<497.4826, -632.4976, 23.8400>> BREAK
				CASE 4 vCoords = <<503.9015, -624.7580, 23.8131>> BREAK
				CASE 5 vCoords = <<504.1672, -651.4606, 23.7512>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Maze_Bank_Arena_1 	
			SWITCH iSlot
				CASE 0 vCoords = <<-551.7751, -1768.5872, 20.7525>> BREAK
				CASE 1 vCoords = <<-556.3744, -1764.2369, 20.8385>> BREAK
				CASE 2 vCoords = <<-553.1461, -1790.1150, 21.3179>> BREAK
				CASE 3 vCoords = <<-561.3416, -1800.9445, 21.6363>> BREAK
				CASE 4 vCoords = <<-548.9277, -1801.1427, 21.1606>> BREAK
				CASE 5 vCoords = <<-524.0042, -1798.5270, 21.2237>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Strawberry_1	
			SWITCH iSlot
				CASE 0 vCoords = <<-301.4459, -1364.9283, 30.2957>> BREAK
				CASE 1 vCoords = <<-299.5699, -1323.7893, 30.3455>> BREAK
				CASE 2 vCoords = <<-326.0109, -1367.3632, 30.2935>> BREAK
				CASE 3 vCoords = <<-350.9810, -1389.5199, 30.3437>> BREAK
				CASE 4 vCoords = <<-332.9958, -1386.7516, 29.7473>> BREAK
				CASE 5 vCoords = <<-340.8613, -1356.5287, 30.3031>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Downtown_Vinewood_1	
			SWITCH iSlot				
				CASE 0 vCoords = <<367.1674, 330.0279, 102.5404>> BREAK
				CASE 1 vCoords = <<347.6043, 334.3985, 104.0242>> BREAK
				CASE 2 vCoords = <<343.6775, 348.4420, 104.2940>> BREAK
				CASE 3 vCoords = <<352.0158, 352.8416, 103.9726>> BREAK
				CASE 4 vCoords = <<335.1022, 342.0046, 104.2013>> BREAK
				CASE 5 vCoords = <<323.9394, 343.7375, 104.2013>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_La_Mesa_3 			
			SWITCH iSlot
				CASE 0 vCoords = <<986.0762, -1537.3174, 29.7428>> BREAK
				CASE 1 vCoords = <<977.4631, -1555.4877, 29.6023>> BREAK
				CASE 2 vCoords = <<959.0582, -1580.0042, 29.4350>> BREAK
				CASE 3 vCoords = <<926.3705, -1546.8308, 29.8298>> BREAK
				CASE 4 vCoords = <<925.9792, -1521.7189, 30.0568>> BREAK
				CASE 5 vCoords = <<981.4378, -1583.9445, 29.8153>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_La_Mesa_4 			
			SWITCH iSlot
				CASE 0 vCoords = <<752.1157, -884.9856, 24.1078>> BREAK
				CASE 1 vCoords = <<710.7881, -891.5815, 22.4802>> BREAK
				CASE 2 vCoords = <<713.0344, -921.5050, 22.8233>> BREAK
				CASE 3 vCoords = <<743.4722, -938.1176, 23.8859>> BREAK
				CASE 4 vCoords = <<760.3058, -931.9525, 24.2151>> BREAK
				CASE 5 vCoords = <<713.9677, -874.1277, 23.1139>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Cypress_Flats_2 
			SWITCH iSlot
				CASE 0 vCoords = <<714.8823, -2192.4543, 27.4354>> BREAK
				CASE 1 vCoords = <<704.0934, -2196.2310, 28.1781>> BREAK
				CASE 2 vCoords = <<708.7194, -2190.1860, 28.1955>> BREAK
				CASE 3 vCoords = <<716.2403, -2168.0110, 27.4354>> BREAK
				CASE 4 vCoords = <<712.1250, -2216.0210, 27.4354>> BREAK
				CASE 5 vCoords = <<705.5598, -2221.6353, 28.2330>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Cypress_Flats_3 	
			SWITCH iSlot
				CASE 0 vCoords = <<1004.0646, -2143.9756, 29.5516>> BREAK
				CASE 1 vCoords = <<1023.1718, -2146.0481, 29.6969>> BREAK
				CASE 2 vCoords = <<1006.7032, -2116.0886, 29.5516>> BREAK
				CASE 3 vCoords = <<1003.8282, -2162.4111, 29.5516>> BREAK
				CASE 4 vCoords = <<1040.3502, -2162.4585, 30.5318>> BREAK
				CASE 5 vCoords = <<1038.2123, -2177.6494, 30.5197>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Vinewood_1 	
			SWITCH iSlot
				CASE 0 vCoords = <<-277.4082, 199.9639, 84.6610>> BREAK
				CASE 1 vCoords = <<-251.3251, 202.4830, 83.3155>> BREAK
				CASE 2 vCoords = <<-258.7682, 184.1945, 78.4664>> BREAK
				CASE 3 vCoords = <<-279.3508, 182.8354, 78.7767>> BREAK
				CASE 4 vCoords = <<-276.5423, 162.0866, 77.7559>> BREAK
				CASE 5 vCoords = <<-268.0508, 186.8966, 84.4147>> BREAK
			ENDSWITCH	
		BREAK
		CASE ciW_Rancho_2 		
			SWITCH iSlot		
				CASE 0 vCoords = <<547.3005, -1948.0724, 23.9851>> BREAK
				CASE 1 vCoords = <<541.2623, -1943.5452, 23.9851>> BREAK
				CASE 2 vCoords = <<537.2211, -1935.7177, 23.9851>> BREAK
				CASE 3 vCoords = <<543.6740, -1934.4031, 23.9851>> BREAK
				CASE 4 vCoords = <<553.5723, -1949.7388, 23.9851>> BREAK
				CASE 5 vCoords = <<540.4761, -1963.6833, 23.9830>> BREAK
			ENDSWITCH
		BREAK
		CASE ciW_Banning_1 			
			SWITCH iSlot
				CASE 0 vCoords = <<126.5784, -2201.3335, 5.0333>> BREAK
				CASE 1 vCoords = <<93.6917, -2184.2961, 4.9530>>  BREAK
				CASE 2 vCoords = <<98.5062, -2225.9895, 5.1712>>  BREAK
				CASE 3 vCoords = <<111.6718, -2246.4082, 5.0866>> BREAK
				CASE 4 vCoords = <<147.0873, -2246.3245, 5.0856>> BREAK
				CASE 5 vCoords = <<105.0663, -2202.2441, 5.0384>> BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_CRATE_COORDS: ", iWarehouse, " valid warehouse found. Returning: ", vCoords)
	RETURN vCoords
ENDFUNC

/// PURPOSE: Returns the coordinates for drop offs at the specified warehouse
///    Validates the warehouse ID before returning its coords
FUNC VECTOR GET_WAREHOUSE_DROP_OFF_COORDS(INT iWarehouse)
	
	VECTOR vCoords = <<0.0, 0.0, 0.0>>
	
	IF NOT CHECK_WAREHOUSE_ID(iWarehouse)
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_DROP_OFF_COORDS: ", iWarehouse, " Invalid warehouse returning: ", vCoords)
		RETURN vCoords
	ENDIF
	
	SWITCH iWarehouse
		// CMcM 2741583
		CASE ciW_Elysian_Island_2			vCoords = <<58.3, -2560.8, 5.0046>>				BREAK // Done 5.0 - s1
		CASE ciW_La_Puerta_1				vCoords = <<-1082.9575, -1254.8550, 4.4214>>	BREAK // Done 5.0 - s2
		CASE ciW_La_Mesa_1					vCoords = <<900.0288, -1029.7622, 33.9714>>		BREAK // Done 5.0 - s3
		CASE ciW_Rancho_1					vCoords = <<247.7398, -1950.6573, 22.0817>>		BREAK // Done 5.0 - s4
		CASE ciW_West_Vinewood_1			vCoords = <<-404.5349, 189.8157, 80.5180>>		BREAK // Done 5.0 - s5
		CASE ciW_LSIA_1						vCoords = <<-1048.1227, -2017.1361, 12.1616>>	BREAK // Done 5.0 - l7
		CASE ciW_Del_Perro_1				vCoords = <<-1267.8877, -818.3130, 16.0992>>	BREAK // Done 5.0 - s6
		CASE ciW_LSIA_2						vCoords = <<-879.1935, -2731.8564, 12.8285>>	BREAK // Done 5.0 - l8
		CASE ciW_Elysian_Island_1			vCoords = <<274.4458, -3004.0000, 4.6994>>		BREAK // Done 5.0 - s7
		CASE ciW_El_Burro_Heights_1			vCoords = <<1566.5137, -2137.7759, 76.6229>>	BREAK // Done 5.0 - m1
		CASE ciW_Elysian_Island_3			vCoords = <<-314.3864, -2721.7917, 5.0003>>		BREAK // Done 5.0 - m2
		CASE ciW_La_Mesa_2					vCoords = <<497.1263, -635.2617, 23.8849>>		BREAK // Done 5.0 - m3
		CASE ciW_Maze_Bank_Arena_1			vCoords = <<-546.1532, -1775.2578, 20.6843>> 	BREAK // Done 5.0 - m4
		CASE ciW_Strawberry_1				vCoords = <<-307.6485, -1364.9143, 30.2957>>	BREAK // Done 5.0 - m5
		CASE ciW_Downtown_Vinewood_1		vCoords = <<366.4851, 333.3937, 102.4653>>		BREAK // Done 5.0 - m6
		CASE ciW_La_Mesa_3					vCoords = <<921.4070, -1556.4235, 29.7756>>		BREAK // Done 5.0 - l1
		CASE ciW_La_Mesa_4					vCoords = <<746.1452, -882.8884, 24.0639>>		BREAK // Done 5.0 - l2
		CASE ciW_Cypress_Flats_2			vCoords = <<1042.4622, -2177.7981, 30.4457>>	BREAK // Done 5.0 - l3
		CASE ciW_Cypress_Flats_3			vCoords = <<1013.8424, -2514.9395, 27.3049>>	BREAK // Done 5.0 - l4
		CASE ciW_Vinewood_1					vCoords = <<-272.0028, 193.3204, 84.5697>>		BREAK // Done 5.0 - l5
		CASE ciW_Rancho_2					vCoords = <<549.6728, -1929.7938, 23.8145>>		BREAK // Done 5.0 - m7
		CASE ciW_Banning_1					vCoords = <<126.5327, -2200.1716, 5.0333>>		BREAK // Done 5.0 - l6
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_DROP_OFF_COORDS: ", iWarehouse, " valid warehouse found. Returning: ", vCoords)
	RETURN vCoords
ENDFUNC

PROC GET_WAREHOUSE_ENTRY_LOCATE(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR& vLocate)
	SWITCH eSimpleInteriorID
	
		CASE SIMPLE_INTERIOR_WAREHOUSE_1 // ciW_Elysian_Island_2
			vLocate = <<54.191, -2569.248, 5.0046>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_2 // ciW_La_Puerta_1
			vLocate = <<-1083.054, -1261.893, 4.534>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_3 // ciW_La_Mesa_1
			vLocate = <<896.3665, -1035.7493, 34.1096>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_4 // ciW_Rancho_1
			vLocate = <<247.473, -1956.943, 22.1908>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_5 // ciW_West_Vinewood_1
			vLocate = <<-424.828, 185.825, 79.775>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_6 // ciW_LSIA_1
			vLocate = <<-1042.4821, -2023.5159, 12.1616>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_7 // ciW_Del_Perro_1
			vLocate = <<-1268.1187, -812.2741, 16.1075>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_8 // ciW_LSIA_2
			vLocate = <<-873.650, -2735.948, 12.9438>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_9 // ciW_Elysian_Island_1
			vLocate = <<274.5224, -3015.413, 4.6993>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_10 // ciW_El_Burro_Heights_1
			vLocate = <<1569.6896, -2129.7925, 77.3351>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_11 // ciW_Elysian_Island_3
			vLocate = <<-315.551, -2698.654, 6.5495>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_12 // ciW_La_Mesa_2
			vLocate = <<499.810, -651.982, 23.9090>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_13 // ciW_Maze_Bank_Arena_1
			vLocate = <<-528.5296, -1784.5729, 20.5853>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_14 // ciW_Strawberry_1
			vLocate = <<-295.8596, -1353.2384, 30.3138>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_15 // ciW_Downtown_Vinewood_1
			vLocate = <<349.839, 328.889, 103.272>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_16 // ciW_La_Mesa_3
			vLocate = <<926.2818, -1560.3114, 29.7404>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_17 // ciW_La_Mesa_4
			vLocate = <<759.566, -909.466, 24.244>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_18 // ciW_Cypress_Flats_2
			vLocate = <<1037.8134, -2173.0623, 30.5334>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_19 // ciW_Cypress_Flats_3
			vLocate = <<1019.116, -2511.690, 27.3020>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_20 // ciW_Vinewood_1
			vLocate = <<-245.3405, 203.3286, 82.818>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_21 // ciW_Rancho_2
			vLocate = <<539.346, -1945.682, 23.9840>>
		BREAK
		
		CASE SIMPLE_INTERIOR_WAREHOUSE_22 // ciW_Banning_1
			vLocate = <<96.1538, -2216.3997, 5.1712>>
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC VECTOR GET_WAREHOUSE_BLIP_COORDS(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)
	UNUSED_PARAMETER(iBlipIndex)
	
	VECTOR vBlipCoords
	GET_WAREHOUSE_ENTRY_LOCATE(eSimpleInteriorID, vBlipCoords)
	RETURN vBlipCoords
ENDFUNC

/// PURPOSE: Returns the coordinates for drop offs at the specified warehouse
///    Validates the warehouse ID before returning its coords
FUNC FLOAT GET_WAREHOUSE_DROP_OFF_RADIUS(INT iWarehouse)
	
	CONST_FLOAT fDefaultRadius 3.0 // CMcM 2741583
	FLOAT fRadius = 0.0
	
	IF NOT CHECK_WAREHOUSE_ID(iWarehouse)
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_DROP_OFF_RADIUS: ", iWarehouse, " Invalid warehouse returning: ", fRadius)
		RETURN fRadius
	ENDIF
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2  			fRadius = fDefaultRadius 		BREAK
		CASE ciW_La_Puerta_1 				fRadius = fDefaultRadius	 	BREAK
		CASE ciW_La_Mesa_1 					fRadius = fDefaultRadius	 	BREAK
		CASE ciW_Rancho_1	 				fRadius = fDefaultRadius		BREAK
		CASE ciW_West_Vinewood_1			fRadius = fDefaultRadius 		BREAK
		CASE ciW_LSIA_1						fRadius = fDefaultRadius	 	BREAK
		CASE ciW_Del_Perro_1 				fRadius = fDefaultRadius		BREAK
		CASE ciW_LSIA_2 					fRadius = fDefaultRadius	 	BREAK
		CASE ciW_Elysian_Island_1 			fRadius = fDefaultRadius 		BREAK
		CASE ciW_El_Burro_Heights_1 	 	fRadius = fDefaultRadius		BREAK
		CASE ciW_Elysian_Island_3 		 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_La_Mesa_2 				 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Maze_Bank_Arena_1 		 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Strawberry_1			 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Downtown_Vinewood_1	 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_La_Mesa_3 				 	fRadius = fDefaultRadius  		BREAK 
		CASE ciW_La_Mesa_4 				 	fRadius = fDefaultRadius 		BREAK 
		CASE ciW_Cypress_Flats_2 		 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Cypress_Flats_3 		 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Vinewood_1 			 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Rancho_2 				 	fRadius = fDefaultRadius 		BREAK
		CASE ciW_Banning_1 				 	fRadius = fDefaultRadius		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_DROP_OFF_RADIUS: ", iWarehouse, " valid warehouse found. Returning: ", fRadius)
	RETURN fRadius
ENDFUNC

/// PURPOSE: Returns the text label for the name of the specified warehouse
///    Validates the warehouse ID before returning its name
FUNC STRING GET_WAREHOUSE_NAME(INT iWarehouse)
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2		RETURN "MP_WHOUSE_0" BREAK
		CASE ciW_La_Puerta_1			RETURN "MP_WHOUSE_1" BREAK
		CASE ciW_La_Mesa_1				RETURN "MP_WHOUSE_2" BREAK
		CASE ciW_Rancho_1				RETURN "MP_WHOUSE_3" BREAK
		CASE ciW_West_Vinewood_1		RETURN "MP_WHOUSE_4" BREAK
		CASE ciW_LSIA_1					RETURN "MP_WHOUSE_5" BREAK
		CASE ciW_Del_Perro_1			RETURN "MP_WHOUSE_6" BREAK
		CASE ciW_LSIA_2					RETURN "MP_WHOUSE_7" BREAK
		CASE ciW_Elysian_Island_1		RETURN "MP_WHOUSE_8" BREAK
		CASE ciW_El_Burro_Heights_1		RETURN "MP_WHOUSE_9" BREAK
		CASE ciW_Elysian_Island_3		RETURN "MP_WHOUSE_10" BREAK
		CASE ciW_La_Mesa_2				RETURN "MP_WHOUSE_11" BREAK
		CASE ciW_Maze_Bank_Arena_1		RETURN "MP_WHOUSE_12" BREAK
		CASE ciW_Strawberry_1			RETURN "MP_WHOUSE_13" BREAK
		CASE ciW_Downtown_Vinewood_1	RETURN "MP_WHOUSE_14" BREAK
		CASE ciW_La_Mesa_3				RETURN "MP_WHOUSE_15" BREAK
		CASE ciW_La_Mesa_4				RETURN "MP_WHOUSE_16" BREAK
		CASE ciW_Cypress_Flats_2		RETURN "MP_WHOUSE_17" BREAK
		CASE ciW_Cypress_Flats_3		RETURN "MP_WHOUSE_18" BREAK
		CASE ciW_Vinewood_1				RETURN "MP_WHOUSE_19" BREAK
		CASE ciW_Rancho_2				RETURN "MP_WHOUSE_20" BREAK
		CASE ciW_Banning_1				RETURN "MP_WHOUSE_21" BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_NAME: ", iWarehouse, " valid warehouse found. Returning: empty name")
	RETURN ""
ENDFUNC

/// PURPOSE: Returns the text label for the description of the specified warehouse
///    Validates the warehouse ID before returning its description
FUNC STRING GET_WAREHOUSE_DESCRIPTION(INT iWarehouse)
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2		RETURN "MP_WHOUSE_0DES" BREAK
		CASE ciW_La_Puerta_1			RETURN "MP_WHOUSE_1DES" BREAK
		CASE ciW_La_Mesa_1				RETURN "MP_WHOUSE_2DES" BREAK
		CASE ciW_Rancho_1				RETURN "MP_WHOUSE_3DES" BREAK
		CASE ciW_West_Vinewood_1		RETURN "MP_WHOUSE_4DES" BREAK
		CASE ciW_LSIA_1					RETURN "MP_WHOUSE_5DES" BREAK
		CASE ciW_Del_Perro_1			RETURN "MP_WHOUSE_6DES" BREAK
		CASE ciW_LSIA_2					RETURN "MP_WHOUSE_7DES" BREAK
		CASE ciW_Elysian_Island_1		RETURN "MP_WHOUSE_8DES" BREAK
		CASE ciW_El_Burro_Heights_1		RETURN "MP_WHOUSE_9DES" BREAK
		CASE ciW_Elysian_Island_3		RETURN "MP_WHOUSE_10DES" BREAK
		CASE ciW_La_Mesa_2				RETURN "MP_WHOUSE_11DES" BREAK
		CASE ciW_Maze_Bank_Arena_1		RETURN "MP_WHOUSE_12DES" BREAK
		CASE ciW_Strawberry_1			RETURN "MP_WHOUSE_13DES" BREAK
		CASE ciW_Downtown_Vinewood_1	RETURN "MP_WHOUSE_14DES" BREAK
		CASE ciW_La_Mesa_3				RETURN "MP_WHOUSE_15DES" BREAK
		CASE ciW_La_Mesa_4				RETURN "MP_WHOUSE_16DES" BREAK
		CASE ciW_Cypress_Flats_2		RETURN "MP_WHOUSE_17DES" BREAK
		CASE ciW_Cypress_Flats_3		RETURN "MP_WHOUSE_18DES" BREAK
		CASE ciW_Vinewood_1				RETURN "MP_WHOUSE_19DES" BREAK
		CASE ciW_Rancho_2				RETURN "MP_WHOUSE_20DES" BREAK
		CASE ciW_Banning_1				RETURN "MP_WHOUSE_21DES" BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_DESCRIPTION: ", iWarehouse, " valid warehouse found. Returning: empty description")
	RETURN ""
ENDFUNC

/// PURPOSE: Returns the texture of the specified warehouse
///    Validates the warehouse ID before returning its TXD
FUNC STRING GET_WAREHOUSE_TXD(INT iWarehouse)
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2 		RETURN "DYN_MPWH_1" BREAK
		CASE ciW_La_Puerta_1 			RETURN "DYN_MPWH_2" BREAK
		CASE ciW_La_Mesa_1 				RETURN "DYN_MPWH_3" BREAK
		CASE ciW_Rancho_1 				RETURN "DYN_MPWH_4" BREAK
		CASE ciW_West_Vinewood_1 		RETURN "DYN_MPWH_5" BREAK
		CASE ciW_LSIA_1 				RETURN "DYN_MPWH_6" BREAK
		CASE ciW_Del_Perro_1 			RETURN "DYN_MPWH_7" BREAK
		CASE ciW_LSIA_2 				RETURN "DYN_MPWH_8" BREAK
		CASE ciW_Elysian_Island_1 		RETURN "DYN_MPWH_9" BREAK
		CASE ciW_El_Burro_Heights_1 	RETURN "DYN_MPWH_10" BREAK
		CASE ciW_Elysian_Island_3 		RETURN "DYN_MPWH_11" BREAK
		CASE ciW_La_Mesa_2 				RETURN "DYN_MPWH_12" BREAK
		CASE ciW_Maze_Bank_Arena_1 		RETURN "DYN_MPWH_13" BREAK
		CASE ciW_Strawberry_1 			RETURN "DYN_MPWH_14" BREAK
		CASE ciW_Downtown_Vinewood_1	RETURN "DYN_MPWH_15" BREAK
		CASE ciW_La_Mesa_3 				RETURN "DYN_MPWH_16" BREAK
		CASE ciW_La_Mesa_4 				RETURN "DYN_MPWH_17" BREAK
		CASE ciW_Cypress_Flats_2 		RETURN "DYN_MPWH_18" BREAK
		CASE ciW_Cypress_Flats_3 		RETURN "DYN_MPWH_19" BREAK
		CASE ciW_Vinewood_1 			RETURN "DYN_MPWH_20" BREAK
		CASE ciW_Rancho_2 				RETURN "DYN_MPWH_21" BREAK
		CASE ciW_Banning_1 				RETURN "DYN_MPWH_22" BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_TXD: ", iWarehouse, " valid warehouse found. Returning: empty texture")
	RETURN ""
ENDFUNC

/// PURPOSE: Returns the cost of the specified warehouse
///    Validates the warehouse ID before returning its cost
FUNC INT GET_WAREHOUSE_COST(INT iWarehouse)
	INT iCost = 0
	
	IF NOT CHECK_WAREHOUSE_ID(iWarehouse)
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_COST: ", iWarehouse, " Invalid warehouse returning: ", iCost)
		RETURN iCost
	ENDIF
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE0 BREAK
		CASE ciW_La_Puerta_1 			iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE1 BREAK
		CASE ciW_La_Mesa_1 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE2 BREAK
		CASE ciW_Rancho_1 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE3 BREAK
		CASE ciW_West_Vinewood_1 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE4 BREAK
		CASE ciW_LSIA_1 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE5 BREAK
		CASE ciW_Del_Perro_1 			iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE6 BREAK
		CASE ciW_LSIA_2 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE7 BREAK
		CASE ciW_Elysian_Island_1 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE8 BREAK
		CASE ciW_El_Burro_Heights_1 	iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE9 BREAK
		CASE ciW_Elysian_Island_3 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE10 BREAK
		CASE ciW_La_Mesa_2 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE11 BREAK
		CASE ciW_Maze_Bank_Arena_1 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE12 BREAK
		CASE ciW_Strawberry_1 			iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE13 BREAK
		CASE ciW_Downtown_Vinewood_1	iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE14 BREAK
		CASE ciW_La_Mesa_3 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE15 BREAK
		CASE ciW_La_Mesa_4 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE16 BREAK
		CASE ciW_Cypress_Flats_2 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE17 BREAK
		CASE ciW_Cypress_Flats_3 		iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE18 BREAK
		CASE ciW_Vinewood_1 			iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE19 BREAK
		CASE ciW_Rancho_2 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE20 BREAK
		CASE ciW_Banning_1 				iCost = g_sMPTunables.iSECUROSERV_WAREHOUSE_WAREHOUSE21 BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_COST: ", iWarehouse, " valid warehouse found. Returning: ", iCost)
	RETURN iCost
ENDFUNC

/// PURPOSE: Returns the initial cost of the specified warehouse
///    Used to check if the tuneable item has put the item on sale
FUNC INT GET_WAREHOUSE_BASE_COST(INT iWarehouse)
	INT iCost = 0
	
	IF NOT CHECK_WAREHOUSE_ID(iWarehouse)
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_BASE_COST: ", iWarehouse, " Invalid warehouse returning: ", iCost)
		RETURN iCost
	ENDIF
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE0 BREAK
		CASE ciW_La_Puerta_1 			iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE1 BREAK
		CASE ciW_La_Mesa_1 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE2 BREAK
		CASE ciW_Rancho_1 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE3 BREAK
		CASE ciW_West_Vinewood_1 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE4 BREAK
		CASE ciW_LSIA_1 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE5 BREAK
		CASE ciW_Del_Perro_1 			iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE6 BREAK
		CASE ciW_LSIA_2 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE7 BREAK
		CASE ciW_Elysian_Island_1 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE8 BREAK
		CASE ciW_El_Burro_Heights_1 	iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE9 BREAK
		CASE ciW_Elysian_Island_3 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE10 BREAK
		CASE ciW_La_Mesa_2 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE11 BREAK
		CASE ciW_Maze_Bank_Arena_1 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE12 BREAK
		CASE ciW_Strawberry_1 			iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE13 BREAK
		CASE ciW_Downtown_Vinewood_1	iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE14 BREAK
		CASE ciW_La_Mesa_3 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE15 BREAK
		CASE ciW_La_Mesa_4 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE16 BREAK
		CASE ciW_Cypress_Flats_2 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE17 BREAK
		CASE ciW_Cypress_Flats_3 		iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE18 BREAK
		CASE ciW_Vinewood_1 			iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE19 BREAK
		CASE ciW_Rancho_2 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE20 BREAK
		CASE ciW_Banning_1 				iCost = g_sMPTunables.iSECUROSERV_BASE_WAREHOUSE_WAREHOUSE21 BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_BASE_COST: ", iWarehouse, " valid warehouse found. Returning: ", iCost)
	RETURN iCost
ENDFUNC

//Returns true if the cost is less than the base cost
FUNC BOOL IS_WAREHOUSE_ON_SALE(INT iWarehouse)
	RETURN GET_WAREHOUSE_COST(iWarehouse) < GET_WAREHOUSE_BASE_COST(iWarehouse)
ENDFUNC

/// PURPOSE: Returns the slot in which the current warehouse is stored
///    Validates the warehouse ID before checking its save slot
FUNC INT GET_SAVE_SLOT_FOR_WAREHOUSE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse = iWarehouse
				RETURN iIter
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_SAVE_SLOT_FOR_WAREHOUSE: ", iWarehouse, " The player does not own this warehouse returning 0")
	RETURN -1
ENDFUNC

/// PURPOSE: Returns the slot in which the current warehouse is stored
///    Validates the warehouse ID before checking its save slot
FUNC INT GET_SAVE_SLOT_FOR_WAREHOUSE_FOR_PLAYER(INT iWarehouse, PLAYER_INDEX pOwner)
	IF CHECK_WAREHOUSE_ID(iWarehouse)	
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].propertyDetails.bdWarehouseData[iIter].iWarehouse = iWarehouse
				RETURN iIter
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_SAVE_SLOT_FOR_WAREHOUSE: ", iWarehouse, " The player does not own this warehouse returning 0")
	RETURN -1
ENDFUNC

FUNC MP_INT_STATS GET_STAT_ENUM_FOR_WAREHOUSE_PAID_PRICE(INT iSaveSlot)
	SWITCH iSaveSlot
		CASE 0	RETURN MP_STAT_PROP_WHOUSE_SL0_VAL
		CASE 1	RETURN MP_STAT_PROP_WHOUSE_SL1_VAL
		CASE 2	RETURN MP_STAT_PROP_WHOUSE_SL2_VAL
		CASE 3	RETURN MP_STAT_PROP_WHOUSE_SL3_VAL
		CASE 4	RETURN MP_STAT_PROP_WHOUSE_SL4_VAL
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_STAT_ENUM_FOR_WAREHOUSE_SLOT passed invalid slot: ", iSaveSlot)
	RETURN MP_STAT_PROP_WHOUSE_SL0_VAL
ENDFUNC

FUNC MP_INT_STATS GET_STAT_ENUM_FOR_WAREHOUSE_OWNERSHIP_VERIFICATION(INT iSaveSlot)
	SWITCH iSaveSlot
		CASE 0	RETURN MP_STAT_PROP_WHOUSE_SLOT0
		CASE 1	RETURN MP_STAT_PROP_WHOUSE_SLOT1
		CASE 2	RETURN MP_STAT_PROP_WHOUSE_SLOT2
		CASE 3	RETURN MP_STAT_PROP_WHOUSE_SLOT3
		CASE 4	RETURN MP_STAT_PROP_WHOUSE_SLOT4
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_STAT_ENUM_FOR_WAREHOUSE_SLOT passed invalid slot: ", iSaveSlot)
	RETURN MP_STAT_PROP_WHOUSE_SLOT0
ENDFUNC

FUNC BOOL IS_WAREHOUSE_PRICE_PAID_STAT_VALID(INT iSaveSlot, INT &iPriceReturnVal)
	IF GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_OWNERSHIP_VERIFICATION(iSaveSlot)) = g_MP_STAT_WAREHOUSE[iSaveSlot][GET_SLOT_NUMBER(-1)]
		iPriceReturnVal = GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_PAID_PRICE(iSaveSlot))
		RETURN (iPriceReturnVal >= 0)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE(INT iSaveSlot, INT iPricePaid)
	PRINTLN("[WHOUSE] SET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE - Called for new warehouse in slot ",  iSaveSlot, ". Paid $", iPricePaid)
	SET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_PAID_PRICE(iSaveSlot), iPricePaid)
ENDPROC

FUNC INT GET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE(INT IWarehouse)
	IF DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), IWarehouse)
		INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(IWarehouse)
		INT iPrice 
		
		IF IS_WAREHOUSE_PRICE_PAID_STAT_VALID(iSaveSlot, iPrice)
			RETURN iPrice
		ENDIF
	ENDIF
	
	RETURN -1
ENDFUNC

PROC INITALISE_WAREHOUSE_OWNERSHIP_AND_PRICE_PAID_STATS()
	INT i
	
	REPEAT MAX_NUMBER_OF_OWNED_WAREHOUSES i
		INT iPrimaryOwnershipStat	= GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[i].iWarehouse
		
		IF iPrimaryOwnershipStat > 0
			INT iValidationStat			= GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_OWNERSHIP_VERIFICATION(i))
			INT iPriceStat				= GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_PAID_PRICE(i))
			INT iRegularPrice			= GET_WAREHOUSE_COST(iPrimaryOwnershipStat)
			
			IF IS_WAREHOUSE_ON_SALE(iPrimaryOwnershipStat)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] INITALISE_WAREHOUSE_OWNERSHIP_AND_PRICE_PAID_STATS - Warehouse on sale: ", iPrimaryOwnershipStat, " using base price if needed")
				iRegularPrice = GET_WAREHOUSE_BASE_COST(iPrimaryOwnershipStat)
			ENDIF
			
			IF iPrimaryOwnershipStat != iValidationStat
				PRINTLN("[WHOUSE] INITALISE_WAREHOUSE_OWNERSHIP_AND_PRICE_PAID_STATS - Stats mismatch in slot ", i, "! iPrimaryOwnershipStat = ", iPrimaryOwnershipStat, " Validation Stat = ", iValidationStat, " price paid = $", iPriceStat)
				SET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_OWNERSHIP_VERIFICATION(i), iPrimaryOwnershipStat)
				
				IF iPriceStat != iRegularPrice
					PRINTLN("[WHOUSE][1] INITALISE_WAREHOUSE_OWNERSHIP_AND_PRICE_PAID_STATS - Price for this factory should have been $", iRegularPrice)
					SET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE(i, iRegularPrice)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT GET_CONTRABAND_SHIPMENT_COST(CONTRABAND_SIZE shipmentSize)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
	AND shipmentSize = CONTRABAND_LARGE
		RETURN 0
	ENDIF
	#ENDIF
	
	SWITCH shipmentSize
		CASE CONTRABAND_SMALL	RETURN	g_sMPTunables.iexec_contraband_small_cost
		CASE CONTRABAND_MEDIUM	RETURN	g_sMPTunables.iexec_contraband_medium_cost
		CASE CONTRABAND_LARGE	RETURN	g_sMPTunables.iexec_contraband_large_cost
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_CONTRABAND_BASE_SHIPMENT_COST(CONTRABAND_SIZE shipmentSize)
	SWITCH shipmentSize
		CASE CONTRABAND_SMALL	RETURN	g_sMPTunables.iexec_base_contraband_small_cost
		CASE CONTRABAND_MEDIUM	RETURN	g_sMPTunables.iexec_base_contraband_medium_cost
		CASE CONTRABAND_LARGE	RETURN	g_sMPTunables.iexec_base_contraband_large_cost
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//Returns true if the cost is less than the base cost
FUNC BOOL IS_CONTRABAND_SHIPMENT_ON_SALE(CONTRABAND_SIZE shipmentSize)
	RETURN GET_CONTRABAND_SHIPMENT_COST(shipmentSize) < GET_CONTRABAND_BASE_SHIPMENT_COST(shipmentSize)
ENDFUNC


/// PURPOSE:Returns 50% of the cost of the warehouse for use when trading in a warehouse
FUNC INT GET_WAREHOUSE_VALUE_FOR_SALE(INT iWarehouse)
	INT iPurchasePrice = GET_WAREHOUSE_PAID_FOR_PURCHASE_PRICE(iWarehouse)
	
	RETURN CEIL(iPurchasePrice * 0.5)
ENDFUNC

/// PURPOSE: Returns the ammount of space remaining in this warehouse for contraband
///    Validates the warehouse ID before checking its capacity
FUNC INT GET_WAREHOUSE_REMAINING_CAPACITY(INT iWarehouse)
	IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
		INT iContrabandTotal 			= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)
		EWarehouseSize WarehouseSize 	= GET_WAREHOUSE_SIZE(iWarehouse)
		
		IF WarehouseSize = eWarehouseLarge
			RETURN ( ciMaxWarehouseStorageLarge - iContrabandTotal)
		ELIF WarehouseSize = eWarehouseMedium
			RETURN ( ciMaxWarehouseStorageMedium - iContrabandTotal)
		ELSE
			RETURN ( ciMaxWarehousestorageSmall - iContrabandTotal)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_REMAINING_CAPACITY FAILED. Player does not own this warehouse: ", iWarehouse)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the ammount of space remaining in this warehouse for contraband
///    Validates the warehouse ID before checking its capacity
FUNC INT GET_WAREHOUSE_REMAINING_CAPACITY_FOR_REMOTE_PLAYER(INT iWarehouse, PLAYER_INDEX piPlayerID)
	IF DOES_PLAYER_OWN_WAREHOUSE(piPlayerID, iWarehouse)
		INT iContrabandTotal 			= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE_OF_REMOTE_PLAYER(iWarehouse, piPlayerID)
		EWarehouseSize WarehouseSize 	= GET_WAREHOUSE_SIZE(iWarehouse)
		
		IF WarehouseSize = eWarehouseLarge
			RETURN ( ciMaxWarehouseStorageLarge - iContrabandTotal)
		ELIF WarehouseSize = eWarehouseMedium
			RETURN ( ciMaxWarehouseStorageMedium - iContrabandTotal)
		ELSE
			RETURN ( ciMaxWarehousestorageSmall - iContrabandTotal)
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_REMAINING_CAPACITY_FOR_REMOTE_PLAYER FAILED. Player does not own this warehouse: ", iWarehouse)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE: Returns the slot in which the current warehouse is stored for specific player
///    Validates the warehouse ID before checking its save slot
FUNC INT GET_SAVE_SLOT_FOR_PLAYERS_WAREHOUSE(INT iWarehouse, PLAYER_INDEX pPlayer)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
	AND pPlayer != INVALID_PLAYER_INDEX()
		INT iIter
		
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(pPlayer)].propertyDetails.bdWarehouseData[iIter].iWarehouse = iWarehouse
				RETURN iIter
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_SAVE_SLOT_FOR_WAREHOUSE: ", iWarehouse, " The player does not own this warehouse returning 0")
	RETURN -1
ENDFUNC

/// PURPOSE: Returns the warehouse stored in the specified save slot, -1 if invalid
FUNC INT GET_WAREHOUSE_ID_FOR_SAVE_SLOT(INT iSaveSlot)
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iWarehouse != ciW_Invalid
		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iWarehouse
	ENDIF
	
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_ID_FOR_SAVE_SLOT: ", iSaveSlot, " no warehouses owned in this slot returning 0")
	RETURN ciW_Invalid
ENDFUNC

/// PURPOSE: Returns true if the warehouse has no more space for contraband
///    Validates the warehouse ID before checking its capacity
FUNC BOOL IS_WAREHOUSE_FULL(INT iWarehouse)
	//Returns true if full
	RETURN 0 = GET_WAREHOUSE_REMAINING_CAPACITY(iWarehouse)
ENDFUNC

/// PURPOSE: Returns true if the warehouse is empty
///    When using this function you must first check ownership of the specified warehouse
FUNC BOOL IS_WAREHOUSE_EMPTY(INT iWarehouse)
	RETURN GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) = 0		
ENDFUNC

#IF FEATURE_CASINO
FUNC BOOL DOES_PLAYER_OWN_A_WAREHOUSE_THAT_IS_NOT_FULL()
	INT iIter
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			IF NOT IS_WAREHOUSE_FULL(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the save slot of a warehouse that has capacity for more items
FUNC INT GET_WAREHOUSE_SAVE_SLOT_OF_FIRST_WH_WITH_SPACE()
	INT iIter
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			IF NOT IS_WAREHOUSE_FULL(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse)
				RETURN iIter
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC
#ENDIF

/// PURPOSE: Returns the lowest capacity warehouse that the player owns
FUNC EWarehouseSize GET_SMALLEST_OWNED_WAREHOUSE()
	EWarehouseSize Lowest_CapacityWhouse = eWarehouseLarge
	
	INT iIter
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			EWarehouseSize WhouseCapacity = GET_WAREHOUSE_SIZE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse)
			
			IF ENUM_TO_INT(WhouseCapacity) < ENUM_TO_INT(Lowest_CapacityWhouse)
				Lowest_CapacityWhouse = WhouseCapacity
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] NEW Lowest cpacity warehouse: ", Lowest_CapacityWhouse)
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_SMALLEST_OWNED_WAREHOUSE EARLY RETURN: ", Lowest_CapacityWhouse)
			RETURN Lowest_CapacityWhouse
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_SMALLEST_OWNED_WAREHOUSE RETURNING: ", Lowest_CapacityWhouse)
	RETURN Lowest_CapacityWhouse
ENDFUNC

/// PURPOSE:
///    Gets the stat enum for the specified warehouse slot
FUNC MP_INT_STATS GET_STAT_FOR_WAREHOUSE_PURCHASE(INT iSlot)
	SWITCH iSlot
		CASE 0	RETURN MP_STAT_WARHOUSESLOT0
		CASE 1	RETURN MP_STAT_WARHOUSESLOT1
		CASE 2	RETURN MP_STAT_WARHOUSESLOT2
		CASE 3	RETURN MP_STAT_WARHOUSESLOT3
		CASE 4	RETURN MP_STAT_WARHOUSESLOT4
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_STAT_FOR_WAREHOUSE_PURCHASE: RETURNING: MAX_NUM_MP_INT_STATS! INVALID SLOT PASSED: ", iSlot)
	RETURN MAX_NUM_MP_INT_STATS
ENDFUNC

/// PURPOSE:
///    Gets the stat enum for the specified warehouse slot for setting the contraband level
FUNC MP_INT_STATS GET_STAT_FOR_WAREHOUSE_CONTRABAND(INT iSlot)
	SWITCH iSlot
		CASE 0	RETURN MP_STAT_CONTOTALFORWHOUSE0
		CASE 1	RETURN MP_STAT_CONTOTALFORWHOUSE1
		CASE 2	RETURN MP_STAT_CONTOTALFORWHOUSE2
		CASE 3	RETURN MP_STAT_CONTOTALFORWHOUSE3
		CASE 4	RETURN MP_STAT_CONTOTALFORWHOUSE4
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_STAT_FOR_WAREHOUSE_CONTRABAND: RETURNING: MAX_NUM_MP_INT_STATS! INVALID SLOT PASSED: ", iSlot)
	RETURN MAX_NUM_MP_INT_STATS
ENDFUNC

FUNC TEXT_LABEL_15 GET_CONTRABAND_BUYER_COMPANY(INT iBuyerID)

	TEXT_LABEL_15 tlBuyerName = "CONTRA_BUYER"
	tlBuyerName += iBuyerID
	
	RETURN tlBuyerName
ENDFUNC

/// PURPOSE:
///    Resets all warehouse slots to empty
/// PARAMS:
///    bDebugWidget - Asserts if FALSE
PROC CLEAR_ALL_WAREHOUSE_SLOTS(BOOL bDebugWidget)
	INT iIter = 0
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF bDebugWidget
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal = 0
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_PURCHASE(iIter), ciW_Invalid)
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iIter), 0)
			
			//Set player broadcast data
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse 			= ciW_Invalid
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal 	= 0
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iVehicleExportEntitesTotal = 0
		ENDIF
	ENDFOR
	
	IF NOT bDebugWidget
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] CLEAR_ALL_WAREHOUSE_INFO")
	ENDIF
ENDPROC

FUNC BOOL IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(MODEL_NAMES iCrateModel)
	SWITCH iCrateModel
		CASE EX_PROP_CRATE_OEGG
		CASE EX_PROP_CRATE_MINIG
		CASE EX_PROP_CRATE_XLDIAM
		CASE EX_PROP_CRATE_SHIDE
		CASE EX_PROP_CRATE_FREEL
		CASE EX_PROP_CRATE_WATCH
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns true if contraband type has an associated special item
FUNC BOOL DOES_CONTRABAND_TYPE_HAVE_ASSOCIATED_SPECIAL_ITEM(CONTRABAND_TYPE contraType)
	IF contraType = CONTRABAND_TYPE_ART_AND_ANTIQUES
	OR contraType = CONTRABAND_TYPE_WEAPONS_AND_AMMO
	OR contraType = CONTRABAND_TYPE_GEMS
	OR contraType = CONTRABAND_TYPE_WILDLIFE
	OR contraType = CONTRABAND_TYPE_ENTERTAINMENT
	OR contraType = CONTRABAND_TYPE_JEWELRY
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_SELECTED_CONTRA_TYPE_IN_HISTORY(CONTRABAND_TYPE eContraType)
	INT iLoop
	
	REPEAT ciContrabandTypeHistoryListSize iLoop
		IF iContrabandTypesHistoryList[iLoop] = ENUM_TO_INT(eContraType)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC INIT_CONTRA_TYPE_HISTORY_LIST()
	INT iLoop
	
	REPEAT ciContrabandTypeHistoryListSize iLoop
		iContrabandTypesHistoryList[iLoop] = -1
	ENDREPEAT
ENDPROC

FUNC INT GET_CONTRABAND_TYPE_WEIGHTING(CONTRABAND_TYPE eContraType)
	
	IF IS_SELECTED_CONTRA_TYPE_IN_HISTORY(eContraType)
		RETURN 0
	ENDIF
	
	IF eContraType 		= CONTRABAND_TYPE_MEDICAL_SUPPLIES		RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL	RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_ART_AND_ANTIQUES		RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_ELECTRONICS			RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_WEAPONS_AND_AMMO		RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_NARCOTICS				RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_GEMS					RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_WILDLIFE				RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_ENTERTAINMENT			RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_JEWELRY				RETURN 11
	ELIF eContraType 	= CONTRABAND_TYPE_BULLION				RETURN 11
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_CONTRABAND_TYPE_WEIGHTING: FALIED, INVALID TYPE: ", eContraType)
	RETURN 0
ENDFUNC

/// PURPOSE: Returns true if the player has completed 10 or more buy misssions
///    THIS DOES NOT MEAN THE ITEM IS ACTIVE! ONLY THAT IT CAN BE RANDOMLY CHOSEN
FUNC BOOL IS_SPECIAL_ITEM_AVAILABLE()
	RETURN g_MP_STAT_NUM_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()] >= g_sMPTunables.iexec_contraband_special_item_threshold
ENDFUNC

/// PURPOSE: Grabs the buy mission started count
FUNC INT GET_BUY_MISSION_UNDERTAKEN_COUNT()
	RETURN g_MP_STAT_LIFETIME_BUY_MISSIONS_STARTED[GET_ACTIVE_CHARACTER_SLOT()]
ENDFUNC

/// PURPOSE: Grabs the buy mission completed count
FUNC INT GET_BUY_MISSION_COMPLETED_COUNT()
	RETURN g_MP_STAT_LIFETIME_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()]
ENDFUNC

/// PURPOSE: Grabs the buy mission completed count of a remote player
FUNC INT GET_REMOTE_PLAYER_LIFETIME_BUY_MISSIONS_COMPLETED(PLAYER_INDEX piPlayer)
	
	IF piPlayer != INVALID_PLAYER_INDEX()
		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].propertyDetails.iLifetimeBuyComplete
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE: Grabs the sell mission started count
FUNC INT GET_SELL_MISSION_UNDERTAKEN_COUNT()
	RETURN g_MP_STAT_LIFETIME_SELL_MISSIONS_STARTED[GET_ACTIVE_CHARACTER_SLOT()]
ENDFUNC

/// PURPOSE: Grabs the sell mission completed count
FUNC INT GET_SELL_MISSION_COMPLETED_COUNT()
	RETURN g_MP_STAT_LIFETIME_SELL_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()]
ENDFUNC

/// PURPOSE: Grabs the local players lifetime earnings from contraband missions
FUNC INT GET_LIFETIME_CONTRABAND_EARNINGS()
	RETURN g_MP_STAT_LIFETIME_CONTRABAND_EARNINGS[GET_ACTIVE_CHARACTER_SLOT()]
ENDFUNC

/// PURPOSE:
///    Gets int for delivery veh upgrade   
FUNC INT GET_DELIVERY_VEH_UPGRADE()
	RETURN g_MP_STAT_DELIVERY_VEH_UPGRADES[GET_ACTIVE_CHARACTER_SLOT()]
ENDFUNC

/// PURPOSE: Gets the total number of steal missions started. 
FUNC INT GET_TOTAL_NUM_STEAL_MISSIONS_STARTED()
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_STEAL_STARTED)
ENDFUNC

/// PURPOSE: Increments the total number of steal missions started.
PROC INCREMENT_TOTAL_NUM_STEAL_MISSIONS_STARTED()
	INT iValue = GET_TOTAL_NUM_STEAL_MISSIONS_STARTED()
	iValue++
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_STEAL_STARTED, iValue)
ENDPROC

/// PURPOSE: Gets the total number of export missions started.
FUNC INT GET_TOTAL_NUM_EXPORT_MISSIONS_STARTED()
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_EXPORT_STARTED)
ENDFUNC

/// PURPOSE: Increments the total number of export missions started.
PROC INCREMENT_TOTAL_NUM_EXPORT_MISSIONS_STARTED()
	INT iValue = GET_TOTAL_NUM_EXPORT_MISSIONS_STARTED()
	iValue++
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_EXPORT_STARTED, iValue)
ENDPROC

/// PURPOSE: Gets the total number of export missions completed.
FUNC INT GET_TOTAL_NUM_EXPORT_MISSIONS_COMPLETED()
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_EXPORT_COMPLETED)
ENDFUNC

/// PURPOSE: Increments the total number of export missions completed.
PROC INCREMENT_TOTAL_NUM_EXPORT_MISSIONS_COMPLETED()
	INT iValue = GET_TOTAL_NUM_EXPORT_MISSIONS_COMPLETED()
	iValue++

	SET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_EXPORT_COMPLETED, iValue)
ENDPROC

/// PURPOSE: Gets the total vehicle export mission earnings.
FUNC INT GET_TOTAL_EXPORT_EARNINGS()
	RETURN GET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_MISSION_EARNINGS)
ENDFUNC

/// PURPOSE: Incremented the total lifetime export mission earnings.
PROC INCREMENT_TOTAL_EXPORT_EARNINGS(INT iEarnings)
	INT iValue = GET_TOTAL_EXPORT_EARNINGS()
	iValue += iEarnings

	SET_MP_INT_CHARACTER_STAT(MP_STAT_LFETIME_IE_MISSION_EARNINGS, iValue)
ENDPROC

CONST_INT MAX_OFFICE_CASH_PILE_UNLOCKS 24

FUNC INT GET_CASH_PILE_UNLOCK_LEVEL(INT iLevel)
	SWITCH iLevel
		CASE 1 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD1 BREAK
		CASE 2  RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD2 BREAK
		CASE 3 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD3 BREAK
		CASE 4 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD4 BREAK
		CASE 5 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD5 BREAK
		CASE 6 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD6 BREAK
		CASE 7 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD7 BREAK
		CASE 8 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD8 BREAK
		CASE 9 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD9 BREAK
		CASE 10 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD10 BREAK
		CASE 11 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD11 BREAK
		CASE 12 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD12 BREAK
		CASE 13 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD13 BREAK
		CASE 14 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD14 BREAK
		CASE 15 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD15 BREAK
		CASE 16 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD16 BREAK
		CASE 17 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD17 BREAK
		CASE 18 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD18 BREAK
		CASE 19 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD19 BREAK
		CASE 20 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD20 BREAK
		CASE 21 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD21 BREAK
		CASE 22 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD22 BREAK
		CASE 23 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD23 BREAK
		CASE 24 RETURN g_sMPTunables.iEXEC_OFFICE_MONEY_PILES_THRESHOLD24 BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

///Purpose: Adds to the local players lifetime earnings from contraband activities total 
PROC ADD_TO_LIFETIME_CONTRABAND_EARNINGS(INT iEarningsToAdd)
	INT iTotal
	iTotal = g_MP_STAT_LIFETIME_CONTRABAND_EARNINGS[GET_ACTIVE_CHARACTER_SLOT()]
	iTotal += iEarningsToAdd
	
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_LIFETIME_CONTRA_EARNINGS, iTotal)
	
	// We also need to update the number of cash piles unlocked for total contraband earnings.
	IF GET_PACKED_STAT_INT(PACKED_MP_INT_OFFICE_CASH_PILES_UNLOCKED) < MAX_OFFICE_CASH_PILE_UNLOCKS
		INT iLevel, iLevelToSet
		FOR iLevel = 1 TO MAX_OFFICE_CASH_PILE_UNLOCKS
			IF iTotal >= GET_CASH_PILE_UNLOCK_LEVEL(iLevel)
				iLevelToSet = iLevel
			ENDIF
		ENDFOR
		PRINTLN("ADD_TO_LIFETIME_CONTRABAND_EARNINGS - Setting cash pile unlock level to ", iLevelToSet)
		SET_PACKED_STAT_INT(PACKED_MP_INT_OFFICE_CASH_PILES_UNLOCKED, iLevelToSet)
	ENDIF
ENDPROC

/// PURPOSE: Increments the buy mission started count
PROC INCREMENT_BUY_MISSION_UNDERTAKEN_COUNT()
	INT iCount
	iCount = g_MP_STAT_LIFETIME_BUY_MISSIONS_STARTED[GET_ACTIVE_CHARACTER_SLOT()]
	iCount ++
	
	g_MP_STAT_LIFETIME_BUY_MISSIONS_STARTED[GET_ACTIVE_CHARACTER_SLOT()] = iCount
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_LIFETIME_BUY_UNDERTAKEN, iCount)
ENDPROC

/// PURPOSE:
///    Increments the buy mission success counter and lifetime buy mission success count
PROC INCREMENT_BUY_MISSION_COMPLETE_COUNT()
	INT iCount, iLifetimeCount
	iCount 			= g_MP_STAT_NUM_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()]
	iLifetimeCount 	= g_MP_STAT_LIFETIME_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()]
	iCount ++
	iLifetimeCount ++
	
	g_MP_STAT_NUM_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()] 					= iCount
	g_MP_STAT_LIFETIME_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()] 				= iLifetimeCount
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iLifetimeBuyComplete	= iLifetimeCount
	
	
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_NUMBUYMISSIONSCOMPLETED, iCount)
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_LIFETIME_BUY_COMPLETE, iLifetimeCount)
ENDPROC

/// PURPOSE: Increments the lifetime sell mission complete count
PROC INCREMENT_SELL_MISSION_COMPLETE_COUNT()
	INT iCount
	iCount = g_MP_STAT_LIFETIME_SELL_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()]
	iCount ++
	
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_LIFETIME_SELL_COMPLETE, iCount)
ENDPROC

/// PURPOSE: Increments the lifetime sell mission started count
PROC INCREMENT_SELL_MISSION_UNDERTAKEN_COUNT()
	INT iCount
	iCount = g_MP_STAT_LIFETIME_SELL_MISSIONS_STARTED[GET_ACTIVE_CHARACTER_SLOT()]
	iCount ++
	
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_LIFETIME_SELL_UNDERTAKEN, iCount)
ENDPROC

/// PURPOSE:
///    Resets the buy mission success counter
PROC RESET_SPECIAL_ITEM_COUNT()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "RESET_SPECIAL_ITEM_COUNT called")
	SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_NUMBUYMISSIONSCOMPLETED, 0)
ENDPROC

/// PURPOSE:
///    Returns the relevant SPECIALITEMSCOLLECTEDWAREHOUSE stat to get/set
FUNC MP_INT_STATS GET_SPCOUNT_WH_STAT_FROM_INDEX(INT iIndex)	
	IF iIndex 	= 0 RETURN MP_STAT_SPCONTOTALFORWHOUSE0
	ELIF iIndex = 1 RETURN MP_STAT_SPCONTOTALFORWHOUSE1
	ELIF iIndex = 2 RETURN MP_STAT_SPCONTOTALFORWHOUSE2
	ELIF iIndex = 3 RETURN MP_STAT_SPCONTOTALFORWHOUSE3
	ELIF iIndex = 4 RETURN MP_STAT_SPCONTOTALFORWHOUSE4
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX Invalid index: ", iIndex)
	RETURN MP_STAT_SPCONTOTALFORWHOUSE0
ENDFUNC

/// PURPOSE:
///    Returns the current special item count for a specified warehouse
FUNC INT GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		MP_INT_STATS eWHStat = GET_SPCOUNT_WH_STAT_FROM_INDEX(GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse))
		RETURN GET_MP_INT_CHARACTER_STAT(eWHStat)
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the current special item count for the local player
FUNC INT GET_TOTAL_SPECIAL_ITEM_COUNT()
	INT iCount, iIter
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		INT iWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iIter)
		IF iWarehouse != ciW_Invalid
			iCount += GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iWarehouse)
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_TOTAL_SPECIAL_ITEM_COUNT: ", iCount)
	RETURN iCount
ENDFUNC

/// PURPOSE:
///    Returns the relevant SPECIALITEMSCOLLECTED stat to get/set
FUNC STATS_PACKED GET_SPECIAL_ITEM_STAT_FROM_INDEX(INT iIndex)
	IF iIndex = 0 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED0
	ELIF iIndex = 1 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED1
	ELIF iIndex = 2 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED2
	ELIF iIndex = 3 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED3
	ELIF iIndex = 4 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED4
	ELIF iIndex = 5 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED5
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_SPECIAL_ITEM_STAT_FROM_INDEX Invalid index: ", iIndex)
	RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTED0
ENDFUNC

/// PURPOSE:
///    Returns the relevant SPECIALITEMSCOLLECTEDWAREHOUSE stat to get/set
FUNC STATS_PACKED GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX(INT iIndex)	
	IF iIndex 	= 0 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE0
	ELIF iIndex = 1 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE1
	ELIF iIndex = 2 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE2
	ELIF iIndex = 3 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE3
	ELIF iIndex = 4 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE4
	ELIF iIndex = 5 RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE5
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX Invalid index: ", iIndex)
	RETURN PACKED_MP_INT_SPECIALITEMSCOLLECTEDWAREHOUSE0
ENDFUNC

/// PURPOSE:
///    Resets the stats controlling all special items in the warehouse
PROC REMOVE_ALL_SPECIAL_ITEMS_FROM_WAREHOUSE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		INT i
		REPEAT g_ciMaxSpecialItems i
			IF iSpecialItemWarehouse[i] = iWarehouse
				SET_PACKED_STAT_INT(GET_SPECIAL_ITEM_STAT_FROM_INDEX(i), ENUM_TO_INT(CONTRABAND_ITEM_INVALID))
				SET_PACKED_STAT_INT(GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX(i), ciW_Invalid)
				
				iContrabandSpecialItems[i] 	= ENUM_TO_INT(CONTRABAND_ITEM_INVALID)
				iSpecialItemWarehouse[i] 	= ciW_Invalid
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_OWNED_SPECIAL_ITEMS()
	INT iLoop
	
	REPEAT g_ciMaxSpecialItems iLoop
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DEBUG_PRINT_OWNED_SPECIAL_ITEMS: slot: ", iLoop," type in slot: ", iContrabandSpecialItems[iLoop], " Warehouse: ", iSpecialItemWarehouse[iLoop])
	ENDREPEAT
ENDPROC

FUNC STRING DEBUG_GET_STRING_FOR_WAREHOUSE(INT iWarehouse)
	STRING sWarehouse = "Empty"
	
	SWITCH iWarehouse
		CASE ciW_Elysian_Island_2  			sWarehouse = "Elysian_Island_2 "	BREAK
		CASE ciW_La_Puerta_1 				sWarehouse = "La_Puerta_1"	 		BREAK
		CASE ciW_La_Mesa_1 					sWarehouse = "La_Mesa_1"	 		BREAK
		CASE ciW_Rancho_1	 				sWarehouse = "Rancho_1"				BREAK
		CASE ciW_West_Vinewood_1			sWarehouse = "West_Vinewood_1"	 	BREAK
		CASE ciW_LSIA_1						sWarehouse = "LSIA_1"	 			BREAK
		CASE ciW_Del_Perro_1 				sWarehouse = "Del_Perro_1"	 		BREAK
		CASE ciW_LSIA_2 					sWarehouse = "LSIA_2"	 			BREAK
		CASE ciW_Elysian_Island_1 			sWarehouse = "Elysian_Island_1"	 	BREAK
		CASE ciW_El_Burro_Heights_1 	 	sWarehouse = "El_Burro_Heights_1"	BREAK
		CASE ciW_Elysian_Island_3 		 	sWarehouse = "Elysian_Island_3 "	BREAK
		CASE ciW_La_Mesa_2 				 	sWarehouse = "La_Mesa_2"			BREAK
		CASE ciW_Maze_Bank_Arena_1 		 	sWarehouse = "Maze_Bank_Arena_1"	BREAK
		CASE ciW_Strawberry_1			 	sWarehouse = "Strawberry_1"			BREAK
		CASE ciW_Downtown_Vinewood_1	 	sWarehouse = "Downtown_Vinewood_1"	BREAK
		CASE ciW_La_Mesa_3 				 	sWarehouse = "La_Mesa_3"			BREAK
		CASE ciW_La_Mesa_4 				 	sWarehouse = "La_Mesa_4"			BREAK
		CASE ciW_Cypress_Flats_2 		 	sWarehouse = "Cypress_Flats_2"		BREAK
		CASE ciW_Cypress_Flats_3 		 	sWarehouse = "Cypress_Flats_3"		BREAK
		CASE ciW_Vinewood_1 			 	sWarehouse = "Vinewood_1"			BREAK
		CASE ciW_Rancho_2 				 	sWarehouse = "Rancho_2"				BREAK
		CASE ciW_Banning_1 				 	sWarehouse = "Banning_1"			BREAK
	ENDSWITCH
	
	RETURN sWarehouse
ENDFUNC
#ENDIF

/// PURPOSE:
///    Adds the specfied item to a warehouse
PROC ADD_SPECIAL_ITEMS_TO_WAREHOUSE(INT iWarehouse, CONTRABAND_SPECIAL_ITEM eitem, INT iCountToAdd, INT iIndex)
	
	IF iIndex >= 0
	AND iIndex < g_ciMaxSpecialItems
	
		INT iCount
		REPEAT iCountToAdd iCount
			iContrabandSpecialItems[iIndex+iCount] 	= ENUM_TO_INT(eitem)
			iSpecialItemWarehouse[iIndex+iCount] 	= iWarehouse
			
			SET_PACKED_STAT_INT(GET_SPECIAL_ITEM_STAT_FROM_INDEX(iIndex+iCount), iContrabandSpecialItems[iIndex+iCount])
			SET_PACKED_STAT_INT(GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX(iIndex+iCount), iSpecialItemWarehouse[iIndex+iCount])
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_OVER] ADD_SPECIAL_ITEMS_TO_WAREHOUSE Added: ", iContrabandSpecialItems[iIndex], " To Warehouse: ", iSpecialItemWarehouse[iIndex], " in array slot: ", iIndex)
			
			#IF IS_DEBUG_BUILD
			DEBUG_PRINT_OWNED_SPECIAL_ITEMS()
			#ENDIF
		ENDREPEAT
		
		RESET_SPECIAL_ITEM_COUNT()
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE_OVER] ADD_SPECIAL_ITEMS_TO_WAREHOUSE: iIndex: ", iIndex, " out of range")
	ENDIF
ENDPROC

/// PURPOSE:
///    Removes the last special item in the warehouse
PROC REMOVE_SPECIAL_ITEMS_FROM_WAREHOUSE(INT iWarehouse, INT iCountToRemove)

	// Remove all the items starting from the end.
	INT iIndex
	INT iSpecialItemsRemoved = 0
	
	#IF IS_DEBUG_BUILD
		REPEAT g_ciMaxSpecialItems iIndex
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] Special item in warehouse ", iSpecialItemWarehouse[iIndex], " slot ", iIndex, ": ", iContrabandSpecialItems[iIndex])
		ENDREPEAT
	#ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_SPECIAL_ITEMS_FROM_WAREHOUSE WH: ", iWarehouse, " iCountToRemove ", iCountToRemove)
	
	FOR iIndex = g_ciMaxSpecialItems-1 TO 0 STEP -1
		IF iSpecialItemsRemoved < iCountToRemove
			IF iSpecialItemWarehouse[iIndex] = iWarehouse
				// Clear the item
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_OVER] REMOVE_SPECIAL_ITEMS_FROM_WAREHOUSE Removing: ", iContrabandSpecialItems[iIndex], " From Warehouse: ", iSpecialItemWarehouse[iIndex])
				
				iContrabandSpecialItems[iIndex]	= ENUM_TO_INT(CONTRABAND_ITEM_INVALID)
				iSpecialItemWarehouse[iIndex] 	= ciW_Invalid
				
				iSpecialItemsRemoved++
			ENDIF
		ENDIF
	ENDFOR
	
	// Shuffle the rest down - we have to do this since ADD_SPECIAL_ITEMS_TO_WAREHOUSE adds to the end of the list and doesn't care for gaps.
	INT iFreeSlot = -1
	REPEAT g_ciMaxSpecialItems iIndex
		IF iSpecialItemWarehouse[iIndex] = ciW_Invalid
			IF iFreeSlot = -1
				iFreeSlot = ciW_Invalid
			ENDIF
		ELSE
			IF iFreeSlot != -1
			
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_OVER] REMOVE_SPECIAL_ITEMS_FROM_WAREHOUSE Moving contents of slot ", iIndex, " to slot ", iFreeSlot)
			
				iContrabandSpecialItems[iFreeSlot] = iContrabandSpecialItems[iIndex]
				iSpecialItemWarehouse[iFreeSlot] = iSpecialItemWarehouse[iIndex]
				
				iContrabandSpecialItems[iIndex] = ENUM_TO_INT(CONTRABAND_ITEM_INVALID)
				iSpecialItemWarehouse[iIndex] = ciW_Invalid
				
				// We always move the next available item into the free slot we can guarantee the next item slot is free.
				iFreeSlot++
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Update stats
	REPEAT g_ciMaxSpecialItems iIndex
		SET_PACKED_STAT_INT(GET_SPECIAL_ITEM_STAT_FROM_INDEX(iIndex), iContrabandSpecialItems[iIndex])
		SET_PACKED_STAT_INT(GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX(iIndex), iSpecialItemWarehouse[iIndex])
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		REPEAT g_ciMaxSpecialItems iIndex
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] Special item in warehouse ", iSpecialItemWarehouse[iIndex], " slot ", iIndex, ": ", iContrabandSpecialItems[iIndex])
		ENDREPEAT
	#ENDIF
ENDPROC

PROC ADD_SPECIAL_ITEMS_TO_INVENTORY_STAT(INT iWarehouse, INT iCountToAdd)
	MP_INT_STATS eWHStat 	= GET_SPCOUNT_WH_STAT_FROM_INDEX(GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse))
	INT iStatCount 			= GET_MP_INT_CHARACTER_STAT(eWHStat)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_SPECIAL_ITEMS_TO_INVENTORY_STAT stat value before: ",iStatCount, " Adding: ", iCountToAdd)
	
	iStatCount += iCountToAdd
	SET_MP_INT_CHARACTER_STAT(eWHStat, iStatCount)
ENDPROC

PROC RESET_SPECIAL_ITEM_INVENTORY_STAT(INT iWarehouse)
	MP_INT_STATS eWHStat = GET_SPCOUNT_WH_STAT_FROM_INDEX(GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse))
	SET_MP_INT_CHARACTER_STAT(eWHStat, 0)
ENDPROC

PROC REMOVE_SPECIAL_ITEMS_FROM_INVENTORY_STAT(INT iWarehouse, INT iCountToRemove)
	MP_INT_STATS eWHStat = GET_SPCOUNT_WH_STAT_FROM_INDEX(GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse))
	INT iCurrentCount = GET_MP_INT_CHARACTER_STAT(eWHStat)
	IF iCurrentCount >= iCountToRemove
		SET_MP_INT_CHARACTER_STAT(eWHStat, iCurrentCount-iCountToRemove)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "REMOVE_SPECIAL_ITEMS_FROM_INVENTORY_STAT: Trying to remove more than what player owns: iCurrentCount=", iCurrentCount, ", iCountToRemove=", iCountToRemove)
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the local player owns ANY special items in the specified warehouse
FUNC BOOL DOES_WAREHOUSE_CONTAIN_A_SPECIAL_ITEM(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		RETURN GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iWarehouse) > 0
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the loacl player has the specified item
FUNC BOOL DOES_PLAYER_HAVE_SPECIAL_ITEM(CONTRABAND_SPECIAL_ITEM eitem)
	INT i
	REPEAT g_ciMaxSpecialItems i
		IF iContrabandSpecialItems[i] = ENUM_TO_INT(eitem)
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the loacl player has the specified item in the specified warehouse
FUNC BOOL DOES_PLAYER_HAVE_SPECIAL_ITEM_IN_WAREHOUSE(INT iWarehouse, CONTRABAND_SPECIAL_ITEM eitem)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		INT i
		REPEAT g_ciMaxSpecialItems i
			IF iContrabandSpecialItems[i] 	= ENUM_TO_INT(eitem)
			AND iSpecialItemWarehouse[i] 	= iWarehouse
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DOES_PLAYER_HAVE_SPECIAL_ITEM_IN_WAREHOUSE returning TRUE for item: ", eitem, " in WH: ", iWarehouse)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "DOES_PLAYER_HAVE_SPECIAL_ITEM_IN_WAREHOUSE returning FALSE for item: ", eitem, " in WH: ", iWarehouse)
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns the first special item stored in the selected warehouse
FUNC CONTRABAND_SPECIAL_ITEM GET_FIRST_SPECIAL_ITEM_IN_WAREHOUSE(INT iWarehouse)
	INT i
	
	FOR i = 1 TO g_ciMaxSpecialItems
		IF DOES_PLAYER_HAVE_SPECIAL_ITEM_IN_WAREHOUSE(iWarehouse, INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, i))
			RETURN INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, i)
		ENDIF
	ENDFOR
	
	RETURN CONTRABAND_ITEM_INVALID
ENDFUNC

FUNC BOOL IS_CONTRABAND_BUYER_IN_LIST(INT &iList[], INT iValue, INT iIndex)
	INT j
	
	REPEAT 3 j
		IF j != iIndex
		AND iList[j] = iValue
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

PROC SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE(INT iWarehouse)
	IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
		INT iSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
		INT i
		INT iNewBuyer[4]
		
		REPEAT 4 i
			IF i < 3
				iNewBuyer[i] = GET_RANDOM_INT_IN_RANGE(1, 21)
				
				//Horrible bad bit of script to ensure the same buyer is not selected more than once
				IF IS_CONTRABAND_BUYER_IN_LIST(iNewBuyer, iNewBuyer[i], i)
					IF iNewBuyer[i] = 20
						iNewBuyer[i] = 1
					ELSE
						iNewBuyer[i] ++
					ENDIF
					
					IF IS_CONTRABAND_BUYER_IN_LIST(iNewBuyer, iNewBuyer[i], i)
						IF iNewBuyer[i] = 20
							iNewBuyer[i] = 1
						ELSE
							iNewBuyer[i] ++
						ENDIF
						
						IF IS_CONTRABAND_BUYER_IN_LIST(iNewBuyer, iNewBuyer[i], i)
							IF iNewBuyer[i] = 20
								iNewBuyer[i] = 1
							ELSE
								iNewBuyer[i] ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
			ELSE
				INT iBuyer = ENUM_TO_INT(GET_FIRST_SPECIAL_ITEM_IN_WAREHOUSE(iWarehouse))
				iNewBuyer[i] = (20 + iBuyer)
			ENDIF
		
		ENDREPEAT
		
		g_iContrabandBuyerOneID[iSlot] 		= iNewBuyer[0]
		g_iContrabandBuyerTwoID[iSlot]		= iNewBuyer[1]
		g_iContrabandBuyerThreeID[iSlot]	= iNewBuyer[2]
		g_iContrabandBuyerFourID[iSlot]		= iNewBuyer[3]
		
		#IF IS_DEBUG_BUILD
		REPEAT 4 i
			CDEBUG1LN(DEBUG_INTERNET, "[WHOUSE] SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE buyer: ", i, " IS: ", iNewBuyer[i], " For WH: ", iWarehouse, " IN SLOT: ", iSlot)
		ENDREPEAT
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks that the capacity for the warehouse in the pased save slot is valid
PROC VALIDATE_WAREHOUSE_CONTRABAND_TOTALS(INT iWarehouseSaveSlot)
	INT iWarehouse = g_MP_STAT_WAREHOUSE[iWarehouseSaveSlot][GET_SLOT_NUMBER(-1)]
	
	IF iWarehouse != ciW_Invalid
		INT iMaxCapacity = GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)

		IF g_MP_STAT_CONTRABAND_TOT_FOR_WAREHOUSE[iWarehouseSaveSlot][GET_SLOT_NUMBER(-1)] > iMaxCapacity
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iWarehouseSaveSlot), iMaxCapacity)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] VALIDATE_WAREHOUSE_CONTRABAND_TOTALS FAILED FOR SLOT: ", iWarehouseSaveSlot, " Setting max capacity for warehouse: ", iWarehouse, "to: ", iMaxCapacity)
		ENDIF
		
		SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE(iWarehouse)
	ENDIF
ENDPROC

/// PURPOSE: Returns true if a special item is active
FUNC BOOL IS_SPECIAL_CONTRABAND_ITEM_ACTIVE()
	RETURN g_bSpecialItemActive
ENDFUNC

/// PURPOSE:
///    Returns the special item associated with a contraband type
FUNC CONTRABAND_SPECIAL_ITEM GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(CONTRABAND_TYPE eContraType)
	IF eContraType 		= CONTRABAND_TYPE_ART_AND_ANTIQUES		RETURN CONTRABAND_ITEM_ORNAMENTAL_EGG
	ELIF eContraType 	= CONTRABAND_TYPE_WEAPONS_AND_AMMO		RETURN CONTRABAND_ITEM_GOLDEN_MINIGUN
	ELIF eContraType 	= CONTRABAND_TYPE_GEMS					RETURN CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND
	ELIF eContraType 	= CONTRABAND_TYPE_WILDLIFE				RETURN CONTRABAND_ITEM_SASQUATCH_HIDE
	ELIF eContraType 	= CONTRABAND_TYPE_ENTERTAINMENT			RETURN CONTRABAND_ITEM_FILM_REEL
	ELIF eContraType 	= CONTRABAND_TYPE_JEWELRY				RETURN CONTRABAND_ITEM_RARE_POCKET_WATCH
	ENDIF
	
	RETURN CONTRABAND_ITEM_INVALID
ENDFUNC

/// PURPOSE:
///    Returns the special item associated with a contraband type
FUNC CONTRABAND_TYPE GET_CONTRABAND_TYPE_FROM_SPECIAL_ITEM(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	IF eSpecialItem 	= CONTRABAND_ITEM_ORNAMENTAL_EGG		RETURN CONTRABAND_TYPE_ART_AND_ANTIQUES
	ELIF eSpecialItem 	= CONTRABAND_ITEM_GOLDEN_MINIGUN		RETURN CONTRABAND_TYPE_WEAPONS_AND_AMMO
	ELIF eSpecialItem 	= CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	RETURN CONTRABAND_TYPE_GEMS
	ELIF eSpecialItem 	= CONTRABAND_ITEM_SASQUATCH_HIDE		RETURN CONTRABAND_TYPE_WILDLIFE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_FILM_REEL				RETURN CONTRABAND_TYPE_ENTERTAINMENT
	ELIF eSpecialItem 	= CONTRABAND_ITEM_RARE_POCKET_WATCH		RETURN CONTRABAND_TYPE_JEWELRY
	ENDIF
	
	RETURN CONTRABAND_TYPE_INVALID
ENDFUNC

/// PURPOSE:
///    Returns the special item model associated with a contraband type
FUNC MODEL_NAMES GET_SPECIAL_ITEM_MODEL_FROM_CONTRABAND_TYPE(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	
	IF eSpecialItem 	= CONTRABAND_ITEM_ORNAMENTAL_EGG		RETURN EX_PROP_CRATE_OEGG
	ELIF eSpecialItem 	= CONTRABAND_ITEM_GOLDEN_MINIGUN		RETURN EX_PROP_CRATE_MINIG
	ELIF eSpecialItem 	= CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	RETURN EX_PROP_CRATE_XLDIAM
	ELIF eSpecialItem 	= CONTRABAND_ITEM_SASQUATCH_HIDE		RETURN EX_PROP_CRATE_SHIDE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_FILM_REEL				RETURN EX_PROP_CRATE_FREEL
	ELIF eSpecialItem 	= CONTRABAND_ITEM_RARE_POCKET_WATCH		RETURN EX_PROP_CRATE_WATCH
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Returns the active special item
FUNC CONTRABAND_SPECIAL_ITEM GET_ACTIVE_SPECIAL_ITEM()
	RETURN GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(g_eContrabandType)
ENDFUNC

FUNC BOOL IS_SPECIAL_ITEM_BLOCKED()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FlowPlayBuyMissionPlaylist") 
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_SPECIAL_CONTRABAND_ITEM_COST(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	IF eSpecialItem 	= CONTRABAND_ITEM_ORNAMENTAL_EGG		RETURN g_sMPTunables.iEXEC_CONTRABAND_ORNAMENTAL_EGG_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_GOLDEN_MINIGUN		RETURN g_sMPTunables.iEXEC_CONTRABAND_GOLDEN_MINIGUN_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	RETURN g_sMPTunables.iEXEC_CONTRABAND_XL_DIAMOND_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_SASQUATCH_HIDE		RETURN g_sMPTunables.iEXEC_CONTRABAND_SASQUATCH_HIDE_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_FILM_REEL				RETURN g_sMPTunables.iEXEC_CONTRABAND_FILM_REEL_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_RARE_POCKET_WATCH		RETURN g_sMPTunables.iEXEC_CONTRABAND_POCKET_WATCH_COST
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_SPECIAL_CONTRABAND_ITEM_COST: Invalid special item: ", eSpecialItem)
	RETURN 0
ENDFUNC

FUNC INT GET_SPECIAL_CONTRABAND_ITEM_BASE_COST(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	IF eSpecialItem 	= CONTRABAND_ITEM_ORNAMENTAL_EGG		RETURN g_sMPTunables.iEXEC_BASE_CONTRABAND_ORNAMENTAL_EGG_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_GOLDEN_MINIGUN		RETURN g_sMPTunables.iEXEC_BASE_CONTRABAND_GOLDEN_MINIGUN_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	RETURN g_sMPTunables.iEXEC_BASE_CONTRABAND_XL_DIAMOND_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_SASQUATCH_HIDE		RETURN g_sMPTunables.iEXEC_BASE_CONTRABAND_SASQUATCH_HIDE_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_FILM_REEL				RETURN g_sMPTunables.iEXEC_BASE_CONTRABAND_FILM_REEL_COST
	ELIF eSpecialItem 	= CONTRABAND_ITEM_RARE_POCKET_WATCH		RETURN g_sMPTunables.iEXEC_BASE_CONTRABAND_POCKET_WATCH_COST
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_SPECIAL_CONTRABAND_ITEM_BASE_COST: Invalid special item: ", eSpecialItem)
	RETURN 0
ENDFUNC

//Returns true if the cost is less than the base cost
FUNC BOOL IS_SPECIAL_CONTRABAND_ITEM_ON_SALE(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	RETURN GET_SPECIAL_CONTRABAND_ITEM_COST(eSpecialItem) < GET_SPECIAL_CONTRABAND_ITEM_BASE_COST(eSpecialItem)
ENDFUNC

FUNC INT GET_SPECIAL_CONTRABAND_ITEM_VALUE(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	IF eSpecialItem 	= CONTRABAND_ITEM_ORNAMENTAL_EGG		RETURN g_sMPTunables.iEXEC_CONTRABAND_ORNAMENTAL_EGG_VALUE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_GOLDEN_MINIGUN		RETURN g_sMPTunables.iEXEC_CONTRABAND_GOLDEN_MINIGUN_VALUE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	RETURN g_sMPTunables.iEXEC_CONTRABAND_XL_DIAMOND_VALUE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_SASQUATCH_HIDE		RETURN g_sMPTunables.iEXEC_CONTRABAND_SASQUATCH_HIDE_VALUE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_FILM_REEL				RETURN g_sMPTunables.iEXEC_CONTRABAND_FILM_REEL_VALUE
	ELIF eSpecialItem 	= CONTRABAND_ITEM_RARE_POCKET_WATCH		RETURN g_sMPTunables.iEXEC_CONTRABAND_POCKET_WATCH_VALUE
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_SPECIAL_CONTRABAND_ITEM_VALUE: Invalid special item: ", eSpecialItem)
	RETURN 0
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DEBUG_PRINT_CONTRA_TYPE_HISTORY()
	INT iLoop
	
	REPEAT ciContrabandTypeHistoryListSize iLoop
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DEBUG_PRINT_CONTRA_TYPE_HISTORY: item: ", iLoop," type: ", iContrabandTypesHistoryList[iLoop])
	ENDREPEAT
ENDPROC
#ENDIF

PROC ADD_CONTRA_ITEM_TO_HISTORY(CONTRABAND_TYPE eContraType)
	
	iContrabandTypesHistoryList[iContrabanTypRandomPos] = ENUM_TO_INT(eContraType)
	
	iContrabanTypRandomPos ++ 
	
	IF iContrabanTypRandomPos >= ciContrabandTypeHistoryListSize
		iContrabanTypRandomPos = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_CONTRA_TYPE_HISTORY()
	#ENDIF
ENDPROC

/// PURPOSE: TO BE CALLED BY FREEMODE ONLY
///    Sets the gloabls to determine what contraband type 
///    will be used for buy missions at each warehouse
///    params:
///    	- bBuyMissionRefresh: Signifies that this is a refresh at the end of a buy mission
PROC RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES(BOOL bBuyMissionRefresh = FALSE)
	CONTRABAND_TYPE contraType
	INT IMaxContabandTypes 	= ENUM_TO_INT(CONTRABAND_TYPE_MAX)
	INT iSumOfWeighting 	= 0
	INT iIter				= 0
	INT iRandomVal			= 0
	
	//Check the history list is initalised
	IF iContrabanTypRandomPos = -1
		INIT_CONTRA_TYPE_HISTORY_LIST()
		iContrabanTypRandomPos = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINT_OWNED_SPECIAL_ITEMS()
	#ENDIF
	
	//Sum all item type weightings
	WHILE iIter < IMaxContabandTypes
		iSumOfWeighting += GET_CONTRABAND_TYPE_WEIGHTING(INT_TO_ENUM(CONTRABAND_TYPE, iIter))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_OVER] iIter: ", iIter, " IMaxContabandTypes: ", IMaxContabandTypes)
		iIter ++
	ENDWHILE
	
	iRandomVal 	= GET_RANDOM_INT_IN_RANGE(0, (iSumOfWeighting + 1))
	iIter		= 0
	
	//Find a type based on the random number using the weighting as an index
	WHILE iIter < IMaxContabandTypes
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_2_OVER] iIter: ", iIter, " IMaxContabandTypes: ", IMaxContabandTypes)
	  	IF iRandomVal < GET_CONTRABAND_TYPE_WEIGHTING(INT_TO_ENUM(CONTRABAND_TYPE, iIter))
			contraType 	= INT_TO_ENUM(CONTRABAND_TYPE, iIter)
			iIter 		= (IMaxContabandTypes - 1)
		ENDIF
	  	iRandomVal -= GET_CONTRABAND_TYPE_WEIGHTING(INT_TO_ENUM(CONTRABAND_TYPE, iIter))
		
		iIter ++
	ENDWHILE
	
	ADD_CONTRA_ITEM_TO_HISTORY(contraType)
	
	//Determine if this is a special item or not
	INT iSpecial 			= GET_RANDOM_INT_IN_RANGE(1, 101)
	INT iSpecialItemChance 	= ROUND(g_sMPTunables.fexec_contraband_special_item_chance * 100)
	
	IF iSpecial <= iSpecialItemChance
	AND NOT IS_SPECIAL_ITEM_BLOCKED()
	AND IS_SPECIAL_ITEM_AVAILABLE()
	AND DOES_CONTRABAND_TYPE_HAVE_ASSOCIATED_SPECIAL_ITEM(contraType)
	AND NOT DOES_PLAYER_HAVE_SPECIAL_ITEM(GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(contraType))
	AND GET_TOTAL_SPECIAL_ITEM_COUNT() < g_ciMaxSpecialItems
		g_bSpecialItemActive = TRUE
		
		IF GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
		AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_BUY
		AND NOT bBuyMissionRefresh
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] special chosen. Not calling the player as they're on a buy mission")
		ELSE
			GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_SPECIAL_AVALIABLE)
		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] special item chosen")
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF iSpecial > iSpecialItemChance
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] Not choosing special item by chance Random val: ", iSpecial, " Special chance: ", iSpecialItemChance)
		ENDIF
		IF IS_SPECIAL_ITEM_BLOCKED()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] Not choosing special item because IS_SPECIAL_ITEM_BLOCKED() is TRUE")
		ENDIF
		IF NOT IS_SPECIAL_ITEM_AVAILABLE()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] Not choosing special item because player has note done enough buy missions: ", g_MP_STAT_NUM_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()], "/", g_sMPTunables.iexec_contraband_special_item_threshold)
		ENDIF
		IF NOT DOES_CONTRABAND_TYPE_HAVE_ASSOCIATED_SPECIAL_ITEM(contraType)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] Not choosing special item because selected item has no associated special. Type: ", contraType)
		ENDIF	
		IF DOES_PLAYER_HAVE_SPECIAL_ITEM(GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(contraType))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] Not choosing special item because player already has special item: ", GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(contraType))
		ENDIF
		IF GET_TOTAL_SPECIAL_ITEM_COUNT() > g_ciMaxSpecialItems
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WH_SPEC] Not choosing special item because player already has the max count of special items")
		ENDIF
		#ENDIF
		
		g_bSpecialItemActive = FALSE
	ENDIF
	
	//Reset the timer
	g_iContrabandRefreshTime = GET_CLOUD_TIME_AS_INT()
	
	g_eContrabandType = contraType
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_ITEM: ", g_eContrabandType)
ENDPROC

/// PURPOSE:Returns the contraband item assigned to the local player
FUNC CONTRABAND_TYPE GET_WAREHOUSE_CONTRABAND_ITEM()
	RETURN g_eContrabandType
ENDFUNC

/// PURPOSE:Returns the contraband type assigned to the local playere as a text label
///    defined here: X:\gta5\assets_ng\GameText\dev_ng\American\americanNetExecutive.txt
FUNC TEXT_LABEL_23 GET_WAREHOUSE_CONTRABAND_TYPE_LABEL(CONTRABAND_TYPE contaType)
	TEXT_LABEL_23 tlContraType = "WH_CONTRATP_"	
	tlContraType += ENUM_TO_INT(contaType)
	
	CDEBUG3LN(DEBUG_INTERNET, "GET_WAREHOUSE_CONTRABAND_TYPE_STRING: ", tlContraType)
	RETURN tlContraType
ENDFUNC

/// PURPOSE:Returns the contraband type assigned to the local playere as a text label
///    defined here: X:\gta5\assets_ng\GameText\dev_ng\American\americanNetExecutive.txt
FUNC TEXT_LABEL_23 GET_WAREHOUSE_CONTRABAND_TYPE_LABEL_FOR_SECURO_APP(CONTRABAND_TYPE contaType)
	TEXT_LABEL_23 tlContraType = "CONTRATP_"	
	tlContraType += ENUM_TO_INT(contaType)
	tlContraType += "W"
	
	CDEBUG3LN(DEBUG_INTERNET, "GET_WAREHOUSE_CONTRABAND_TYPE_STRING: ", tlContraType)
	RETURN tlContraType
ENDFUNC

FUNC TEXT_LABEL_23 GET_WAREHOUSE_CONTRABAND_SPECIAL_ITEM_LABEL(CONTRABAND_SPECIAL_ITEM eSpecialItem)
	TEXT_LABEL_23 tlContraType
	SWITCH eSpecialItem
		CASE CONTRABAND_ITEM_ORNAMENTAL_EGG 		tlContraType = "BYCB_EGG"		BREAK
		CASE CONTRABAND_ITEM_GOLDEN_MINIGUN			tlContraType = "BYCB_MGUN"		BREAK
		CASE CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND	tlContraType = "BYCB_DMND"		BREAK
		CASE CONTRABAND_ITEM_SASQUATCH_HIDE			tlContraType = "BYCB_SASHDE"	BREAK
		CASE CONTRABAND_ITEM_FILM_REEL				tlContraType = "BYCB_FREEL"		BREAK
		CASE CONTRABAND_ITEM_RARE_POCKET_WATCH		tlContraType = "BYCB_WATCH"		BREAK
		DEFAULT 									tlContraType = "CONTRASPECIAL"	BREAK
	ENDSWITCH
	
	CDEBUG3LN(DEBUG_INTERNET, "GET_WAREHOUSE_CONTRABAND_SPECIAL_ITEM_LABEL: ", tlContraType)
	RETURN tlContraType
ENDFUNC

PROC INIT_SPECIAL_ITEM_GLOBAL_DATA()
	INT iIndex = 0
	REPEAT g_ciMaxSpecialItems iIndex
		iContrabandSpecialItems[iIndex] = GET_PACKED_STAT_INT(GET_SPECIAL_ITEM_STAT_FROM_INDEX(iIndex))
		iSpecialItemWarehouse[iIndex] 	= GET_PACKED_STAT_INT(GET_SPECIAL_ITEM_WH_STAT_FROM_INDEX(iIndex))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "INIT_SPECIAL_ITEM_GLOBAL_DATA Slot: ", iIndex, " Contains item: ", iContrabandSpecialItems[iIndex], " in WH: ", iSpecialItemWarehouse[iIndex])
	ENDREPEAT	
ENDPROC

PROC SET_PLAYER_BD_HAS_VIEWED_WAREHOUSE_INTRO_SCENE(BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_VIEWED_WAREHOUSE_INTRO)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_VIEWED_WAREHOUSE_INTRO)
			PRINTLN("SET_PLAYER_BD_HAS_VIEWED_WAREHOUSE_INTRO_SCENE - SETTING player BD mocap scene as viewed.")
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_VIEWED_WAREHOUSE_INTRO)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSFour, PROPERTY_BROADCAST_BS4_VIEWED_WAREHOUSE_INTRO)
			PRINTLN("SET_PLAYER_BD_HAS_VIEWED_WAREHOUSE_INTRO_SCENE - CLEARING player BD mocap scene as viewed.")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///   Sets broadcast data equal to save slot data
///    Called after the heavily accessed stats have been initialized
PROC ALIGN_WAREHOUSE_SLOT_AND_BROADCAST_DATA()	
	INT iIter = 0
	
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)		
		//Set player broadcast data
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse 			= g_MP_STAT_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)]
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal 	= g_MP_STAT_CONTRABAND_TOT_FOR_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)]
		
		//Run contraband validation
		VALIDATE_WAREHOUSE_CONTRABAND_TOTALS(iIter)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ALIGN_WAREHOUSE_SLOT_AND_BROADCAST_DATA SLOT: ", iIter, " WAREHOUSE: ", g_MP_STAT_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)], " CONTRANBAND TOTAL: ", g_MP_STAT_CONTRABAND_TOT_FOR_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)])
		
		#IF IS_DEBUG_BUILD
		IF g_MP_STAT_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)] != ciW_Invalid
			PRINTLN("[PROPERTY_OWNERSHIP] Player owns Warehouse: ", DEBUG_GET_STRING_FOR_WAREHOUSE(g_MP_STAT_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)]))
		ENDIF
		#ENDIF
	ENDFOR
	
	//Grab the special item stats
	INIT_SPECIAL_ITEM_GLOBAL_DATA()
	
	//Setup the contraband types
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES called for initial sesion start")
	RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES()
	
	//Set the securo tutorial to be able to display again
	g_iWebsiteTutorialDisplayedBS 	= 0
	g_iWebsiteTutorialDisplayedBS2	= 0
	
	//Set the lifetime buy mission complete BD equal to the stat
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iLifetimeBuyComplete = GET_BUY_MISSION_COMPLETED_COUNT()
	
	//Setup the price paid data
	INITALISE_WAREHOUSE_OWNERSHIP_AND_PRICE_PAID_STATS()
	
	//Set the intro scene BD
	SET_PLAYER_BD_HAS_VIEWED_WAREHOUSE_INTRO_SCENE(HAS_LOCAL_PLAYER_VIEWED_INTERIOR_CUTSCENE(biFmCut_WarehouseCutscene))	
ENDPROC

/// PURPOSE:
///    Returns TRUE if the specified warehouse is disabled by a tuneable
///    Validates the warehouse ID before returning the tuneable value
FUNC BOOL IS_WAREHOUSE_DISABLED_BY_TUNEABLE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		SWITCH iWarehouse
			CASE ciW_Elysian_Island_2  		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_0) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_0
			CASE ciW_La_Puerta_1 			CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_1) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_1
			CASE ciW_La_Mesa_1 				CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_2) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_2
			CASE ciW_Rancho_1	 			CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_3) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_3
			CASE ciW_West_Vinewood_1		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_4) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_4
			CASE ciW_LSIA_1					CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_5) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_5
			CASE ciW_Del_Perro_1 			CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_6) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_6
			CASE ciW_LSIA_2					CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_7) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_7
			CASE ciW_Elysian_Island_1 		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_8) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_8		
			CASE ciW_El_Burro_Heights_1 	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_9) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_9
			CASE ciW_Elysian_Island_3 		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_10) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_10
			CASE ciW_La_Mesa_2 				CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_11) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_11
			CASE ciW_Maze_Bank_Arena_1 		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_12) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_12
			CASE ciW_Strawberry_1			CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_13) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_13
			CASE ciW_Downtown_Vinewood_1	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_14) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_14		
			CASE ciW_La_Mesa_3 				CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_15) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_15
			CASE ciW_La_Mesa_4 				CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_16) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_16
			CASE ciW_Cypress_Flats_2 		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_17) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_17
			CASE ciW_Cypress_Flats_3 		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_18) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_18
			CASE ciW_Vinewood_1 			CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_19) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_19
			CASE ciW_Rancho_2				CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_20) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_20
			CASE ciW_Banning_1				CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: ", iWarehouse, " RETURNING: ", g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_21) RETURN g_sMPTunables.bEXEC_DISABLE_PURCHASE_WAREHOUSE_21
		ENDSWITCH
	ENDIF	
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_DISABLED_BY_TUNEABLE: RETURNING: TRUE! INVALID WAREHOUSE PASSED: ", iWarehouse)
	RETURN TRUE
ENDFUNC

PROC GENERATE_WAREHOUSE_KEY_FOR_CATALOGUE(TEXT_LABEL_63 &tlGeneratedKey, INT iWarehouseID)
	tlGeneratedKey = "WH_INDEX_"
	tlGeneratedKey += iWarehouseID
	tlGeneratedKey += "_t0_v0"
ENDPROC

PROC GENERATE_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(TEXT_LABEL_63 &tlGeneratedKey, BOOL bBuying, INT iContrbandCount, BOOL bSpecialItem = FALSE, INT iSpecialItemID = -1)
	IF bBuying
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF IS_PLAYER_ON_MP_INTRO()
		AND iContrbandCount = ENUM_TO_INT(CONTRABAND_LARGE)
			tlGeneratedKey = "CB_BUY_5_t0_v0"
			EXIT
		ENDIF
		#ENDIF
		
		IF iContrbandCount = -1
			tlGeneratedKey = "CB_NONE_0_t0_v0"
		ELSE
			IF bSpecialItem
				tlGeneratedKey = "CB_BUY_"
				tlGeneratedKey += iContrbandCount
				tlGeneratedKey += "_t1_v"
				tlGeneratedKey += iSpecialItemID
			ELSE
				tlGeneratedKey = "CB_BUY_"
				tlGeneratedKey += iContrbandCount
				tlGeneratedKey += "_t0_v0"
			ENDIF
		ENDIF
	ELSE
		// Player can only select to sell all just now.
		tlGeneratedKey = "CB_SELL_0_t0_v0"
	ENDIF
ENDPROC

FUNC INT GET_WAREHOUSE_KEY_FOR_CATALOGUE(INT iWarehouseID)
	TEXT_LABEL_63 tlCategoryKey
	GENERATE_WAREHOUSE_KEY_FOR_CATALOGUE(tlCategoryKey, iWarehouseID)
	PRINTLN("[CATALOGUE_KEY] key '", tlCategoryKey, "' = ", GET_HASH_KEY(tlCategoryKey))
	RETURN GET_HASH_KEY(tlCategoryKey)
ENDFUNC

FUNC INT GET_WAREHOUSE_INVENTORY_KEY_FOR_CATALOGUE(INT iSlot)
	TEXT_LABEL_63 tlGeneratedKey
	SWITCH iSlot
		CASE 0	tlGeneratedKey = "MP_STAT_WARHOUSESLOT0_v0" BREAK
		CASE 1	tlGeneratedKey = "MP_STAT_WARHOUSESLOT1_v0" BREAK
		CASE 2	tlGeneratedKey = "MP_STAT_WARHOUSESLOT2_v0" BREAK
		CASE 3	tlGeneratedKey = "MP_STAT_WARHOUSESLOT3_v0" BREAK
		CASE 4	tlGeneratedKey = "MP_STAT_WARHOUSESLOT4_v0" BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(tlGeneratedKey)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_WAREHOUSE_INVENTORY_KEY_FOR_CATALOGUE - Invalid warehouse slot specified - ", iSlot)
			CASSERTLN(DEBUG_SHOPS, "GET_WAREHOUSE_INVENTORY_KEY_FOR_CATALOGUE - Invalid warehouse slot specified")
		#ENDIF
		RETURN -1
	ENDIF
	RETURN GET_HASH_KEY(tlGeneratedKey)
ENDFUNC

FUNC INT GET_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(BOOL bBuying, INT iContrbandCount, BOOL bSpecialItem = FALSE, INT iSpecialItemID = -1)
	TEXT_LABEL_63 tlCategoryKey
	GENERATE_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(tlCategoryKey, bBuying, iContrbandCount, bSpecialItem, iSpecialItemID)
	PRINTLN("[CATALOGUE_KEY] key '", tlCategoryKey, "' = ", GET_HASH_KEY(tlCategoryKey))
	RETURN GET_HASH_KEY(tlCategoryKey)
ENDFUNC

FUNC INT GET_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(INT iSlot)
	TEXT_LABEL_63 tlGeneratedKey
	SWITCH iSlot
		CASE 0	tlGeneratedKey = "MP_STAT_CONTOTALFORWHOUSE0_v0" BREAK
		CASE 1	tlGeneratedKey = "MP_STAT_CONTOTALFORWHOUSE1_v0" BREAK
		CASE 2	tlGeneratedKey = "MP_STAT_CONTOTALFORWHOUSE2_v0" BREAK
		CASE 3	tlGeneratedKey = "MP_STAT_CONTOTALFORWHOUSE3_v0" BREAK
		CASE 4	tlGeneratedKey = "MP_STAT_CONTOTALFORWHOUSE4_v0" BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(tlGeneratedKey)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE - Invalid contraband slot specified - ", iSlot)
			CASSERTLN(DEBUG_SHOPS, "GET_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE - Invalid contraband slot specified")
		#ENDIF
		RETURN -1
	ENDIF
	RETURN GET_HASH_KEY(tlGeneratedKey)
ENDFUNC

FUNC INT GET_SPECIAL_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(INT iSlot)
	TEXT_LABEL_63 tlGeneratedKey
	SWITCH iSlot
		CASE 0	tlGeneratedKey = "MP_STAT_SPCONTOTALFORWHOUSE0_v0" BREAK
		CASE 1	tlGeneratedKey = "MP_STAT_SPCONTOTALFORWHOUSE1_v0" BREAK
		CASE 2	tlGeneratedKey = "MP_STAT_SPCONTOTALFORWHOUSE2_v0" BREAK
		CASE 3	tlGeneratedKey = "MP_STAT_SPCONTOTALFORWHOUSE3_v0" BREAK
		CASE 4	tlGeneratedKey = "MP_STAT_SPCONTOTALFORWHOUSE4_v0" BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(tlGeneratedKey)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_SPECIAL_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE - Invalid contraband slot specified - ", iSlot)
			CASSERTLN(DEBUG_SHOPS, "GET_SPECIAL_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE - Invalid contraband slot specified")
		#ENDIF
		RETURN -1
	ENDIF
	RETURN GET_HASH_KEY(tlGeneratedKey)
ENDFUNC

FUNC INT GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(INT iSlot)
	TEXT_LABEL_63 tlGeneratedKey
	SWITCH iSlot
		CASE 0	tlGeneratedKey = "MP_STAT_MISSIONFORWHOUSE0_v0" BREAK
		CASE 1	tlGeneratedKey = "MP_STAT_MISSIONFORWHOUSE1_v0" BREAK
		CASE 2	tlGeneratedKey = "MP_STAT_MISSIONFORWHOUSE2_v0" BREAK
		CASE 3	tlGeneratedKey = "MP_STAT_MISSIONFORWHOUSE3_v0" BREAK
		CASE 4	tlGeneratedKey = "MP_STAT_MISSIONFORWHOUSE4_v0" BREAK
	ENDSWITCH
	
	IF IS_STRING_NULL_OR_EMPTY(tlGeneratedKey)
		#IF IS_DEBUG_BUILD
			PRINTLN("GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE - Invalid contraband slot specified - ", iSlot)
			CASSERTLN(DEBUG_SHOPS, "GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE - Invalid contraband slot specified")
		#ENDIF
		RETURN -1
	ENDIF
	RETURN GET_HASH_KEY(tlGeneratedKey)
ENDFUNC

FUNC INT GET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY()
	RETURN g_LastActiveBuyContrabandMissionKey
ENDFUNC

PROC SET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY(INT iItemID)
	g_LastActiveBuyContrabandMissionKey = iItemID
	PRINTLN("SET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY - ", iItemID)
ENDPROC

FUNC BOOL PROCESS_TRANSACTION_FOR_ADD_CONTRABAND(INT iSlot, INT iQuantity, INT iSpecialQuantity, CONTRABAND_TRANSACTION_STATE &eResult)

	/*
		AddContraband: Buy Mission success (increase the CATEGORY_ CONTRABAND_QNTY and clears the CATEGORY_INVENTORY_CONTRABAND)
		0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
		1:  CATEGORY_CONTRABAND_QNTY, <quantity>
		2:  MISSION_PASSED, CATEGORY_ CONTRABAND_QNTY
	*/
	
	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			INT iInventoryKey, iMissionKey, iQuantityKey, iSPQuantityKey
			iInventoryKey = GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(iSlot)
			iMissionKey = GET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY()
			iQuantityKey = GET_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(iSlot)
			iSPQuantityKey = GET_SPECIAL_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(iSlot)
			
			//0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_CONTRABAND_MISSION, iMissionKey, NET_SHOP_ACTION_ADD_CONTRABAND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
				//1:  CATEGORY_CONTRABAND_QNTY, <quantity>
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iQuantityKey, NET_SHOP_ACTION_ADD_CONTRABAND, iQuantity, 0, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
				
					//2:  CATEGORY_CONTRABAND_QNTY, <quantity>
					IF iSpecialQuantity = 0
					OR NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iSPQuantityKey, NET_SHOP_ACTION_ADD_CONTRABAND, iSpecialQuantity, 0, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					
						//3:  MISSION_PASSED, CATEGORY_CONTRABAND_QNTY
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_FLAGS, HASH("CF_MISSION_PASSED_v1"), NET_SHOP_ACTION_ADD_CONTRABAND, 1, 0, iQuantity, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iQuantityKey)
							eResult = CONTRABAND_TRANSACTION_STATE_PENDING
						ELSE
							eResult = CONTRABAND_TRANSACTION_STATE_FAILED
							CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Failed to add flag item")
						ENDIF
					ELSE
						eResult = CONTRABAND_TRANSACTION_STATE_FAILED
						CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Failed to add special quantity item")
					ENDIF
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Failed to add quantity item")
				ENDIF
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Failed to add mission item")
			ENDIF
			
			IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Starting basket checkout")
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Failed to start checkout")
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Transaction finished - SUCCESS")
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING)
ENDFUNC

FUNC BOOL PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND(INT iSlot, INT iQuantity, INT iSpecialQuantity, INT iEarnValue, REMOVE_CONTRABAND_REASON_ENUM eReason, CONTRABAND_TRANSACTION_STATE &eResult, INT iDeliveredCount = 0)
	/*
		RemoveContraband (Sell Mission Success) – contraband removed an converted to some amount of cash <price>. CATEGORY_INVENTORY_CONTRABAND_MISSION is cleared.
		0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
		1:  CATEGORY_ CONTRABAND_QNTY, <quantity> ignored since all will be removed.
		2:  MISSION_PASSED, CATEGORY_ CONTRABAND_QNTY, <price>
		                
		RemoveContraband (Sell Mission Failed) – some percentage is removed for failing (50%?).  No cash awarded.  CATEGORY_INVENTORY_CONTRABAND_MISSION is cleared.
		0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
		1:  CATEGORY_ CONTRABAND_QNTY, <quantity> 
		2:  MISSION_FAILED, CATEGORY_ CONTRABAND_QNTY, <price = 0 >

		RemoveContraband (Sell Mission Started) - some percentage is removed for starting (5%).  No cash awarded.  CATEGORY_INVENTORY_CONTRABAND_MISSION is NOT touched
		0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
		1:  CATEGORY_ CONTRABAND_QNTY, <quantity> 
		2:  MISSION_STARTED, CATEGORY_ CONTRABAND_QNTY, <price = 0 >

		RemoveContraband (Warehouse attacked) - some percentage is removed for when warehouse attacked (5%).  No cash awarded.  CATEGORY_INVENTORY_CONTRABAND_MISSION is NOT touched
		0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
		1:  CATEGORY_ CONTRABAND_QNTY, <quantity> 
		2:  MISSION_STARTED, CATEGORY_ CONTRABAND_QNTY, <price = 0 >
	*/
	
	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			INT iInventoryKey, iMissionKey, iQuantityKey, iSPQuantityKey, iFlagKey
			iInventoryKey = GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(iSlot)
			iMissionKey = GET_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(FALSE, iQuantity)
			iQuantityKey = GET_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(iSlot)
			iSPQuantityKey = GET_SPECIAL_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(iSlot)
			
			SWITCH eReason
				CASE REMOVE_CONTRA_ATTACKED			iFlagKey = HASH("CF_ATTACKED") BREAK
				CASE REMOVE_CONTRA_MISSION_STARTED	iFlagKey = HASH("CF_MISSION_STARTED") BREAK
				CASE REMOVE_CONTRA_MISSION_FAILED	iFlagKey = HASH("CF_MISSION_FAILED") BREAK
				CASE REMOVE_CONTRA_MISSION_PASSED	iFlagKey = HASH("CF_MISSION_PASSED") BREAK
			ENDSWITCH
			
			//0: CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION
			IF eReason = REMOVE_CONTRA_ATTACKED // skip adding mission
			OR NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_CONTRABAND_MISSION, iMissionKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
				//1:  CATEGORY_CONTRABAND_QNTY, <quantity>
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iQuantityKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, iQuantity, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					//2:  CATEGORY_CONTRABAND_QNTY, <quantity> *special*
					IF iSpecialQuantity = 0
					OR NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iSPQuantityKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, iSpecialQuantity, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
						//3:  MISSION_PASSED, CATEGORY_CONTRABAND_QNTY
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_FLAGS, iFlagKey, NET_SHOP_ACTION_REMOVE_CONTRABAND, 1, iEarnValue, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iQuantityKey)
							eResult = CONTRABAND_TRANSACTION_STATE_PENDING
						ELSE
							eResult = CONTRABAND_TRANSACTION_STATE_FAILED
							CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Failed to add flag item")
						ENDIF
					ELSE
						eResult = CONTRABAND_TRANSACTION_STATE_FAILED
						CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Failed to add special quantity item")
					ENDIF	
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Failed to add quantity item")
				ENDIF
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Failed to add mission item")
			ENDIF
			
			IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
			AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Starting basket checkout")
			ELSE
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Failed to start checkout")
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Transaction finished - SUCCESS")
					
					IF iEarnValue > 0
						PRINTLN("PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Calling NETWORK_EARN_FROM_CONTRABAND: iEarnValue = ", iEarnValue, " iDeliveredCount = ", iDeliveredCount)
						NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
						NETWORK_EARN_FROM_CONTRABAND(iEarnValue, iDeliveredCount)	
					ENDIF
					
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING)
ENDFUNC

FUNC INT GET_WAREHOUSE_CRATE_MODEL_INDEX(MODEL_NAMES eCrateModel)
	SWITCH eCrateModel
		// Small
		CASE DUMMY_MODEL_FOR_SCRIPT		RETURN 0
		CASE EX_PROP_CRATE_CLOSED_SC  	RETURN 1
		CASE EX_PROP_CRATE_MED_SC  		RETURN 2
		CASE EX_PROP_CRATE_TOB_SC  		RETURN 3
		CASE EX_PROP_CRATE_ART_SC  		RETURN 4
		CASE EX_PROP_CRATE_ELEC_SC  	RETURN 5
		CASE EX_PROP_CRATE_AMMO_SC  	RETURN 6
		CASE EX_PROP_CRATE_NARC_SC  	RETURN 7
		CASE EX_PROP_CRATE_GEMS_SC  	RETURN 8
		CASE EX_PROP_CRATE_WLIFE_SC  	RETURN 9
		CASE EX_PROP_CRATE_JEWELS_SC 	RETURN 10
		CASE EX_PROP_CRATE_BULL_SC_02 	RETURN 11
		// Large
		CASE EX_PROP_CRATE_CLOSED_BC 	RETURN 12
		CASE EX_PROP_CRATE_MED_BC 		RETURN 13
		CASE EX_PROP_CRATE_TOB_BC 		RETURN 14
		CASE EX_PROP_CRATE_ART_BC		RETURN 15
		CASE EX_PROP_CRATE_ELEC_BC 		RETURN 16
		CASE EX_PROP_CRATE_AMMO_BC 		RETURN 17
		CASE EX_PROP_CRATE_NARC_BC 		RETURN 18
		CASE EX_PROP_CRATE_GEMS_BC 		RETURN 19
		CASE EX_PROP_CRATE_WLIFE_BC 	RETURN 20
		CASE EX_PROP_CRATE_JEWELS_BC 	RETURN 21
		CASE EX_PROP_CRATE_BULL_BC_02 	RETURN 22
		// Rare
		CASE EX_PROP_CRATE_OEGG			RETURN 23
		CASE EX_PROP_CRATE_MINIG 		RETURN 24
		CASE EX_PROP_CRATE_XLDIAM 		RETURN 25
		CASE EX_PROP_CRATE_SHIDE 		RETURN 26
		CASE EX_PROP_CRATE_FREEL 		RETURN 27
		CASE EX_PROP_CRATE_WATCH 		RETURN 28
		// Newly Added
		CASE EX_PROP_CRATE_EXPL_SC				RETURN 29
		CASE EX_PROP_CRATE_EXPL_BC				RETURN 30
		CASE EX_PROP_CRATE_JEWELS_RACKS_SC		RETURN 31
		CASE EX_PROP_CRATE_JEWELS_RACKS_BC		RETURN 32
		CASE EX_PROP_CRATE_FURJACKET_SC			RETURN 33
		CASE EX_PROP_CRATE_FURJACKET_BC			RETURN 34
		CASE EX_PROP_CRATE_PHARMA_SC			RETURN 35
		CASE EX_PROP_CRATE_PHARMA_BC			RETURN 36
		CASE EX_PROP_CRATE_HIGHEND_PHARMA_SC	RETURN 37
		CASE EX_PROP_CRATE_HIGHEND_PHARMA_BC	RETURN 38
		CASE EX_PROP_CRATE_BIOHAZARD_SC			RETURN 39
		CASE EX_PROP_CRATE_BIOHAZARD_BC			RETURN 40
		CASE EX_PROP_CRATE_CLOTHING_SC			RETURN 41
		CASE EX_PROP_CRATE_CLOTHING_BC			RETURN 42
		CASE EX_PROP_CRATE_MONEY_SC 			RETURN 43
		CASE EX_PROP_CRATE_MONEY_BC 			RETURN 44
		CASE EX_PROP_CRATE_CLOSED_MS 			RETURN 45
		CASE EX_PROP_CRATE_CLOSED_RW 			RETURN 46
		CASE EX_PROP_CRATE_CLOSED_MW 			RETURN 47
		CASE EX_PROP_CRATE_ART_02_BC 			RETURN 48
		CASE EX_PROP_CRATE_ART_02_SC 			RETURN 49
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC MODEL_NAMES GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(INT iModelIndex)
	SWITCH iModelIndex
		// Small
		CASE 0  RETURN DUMMY_MODEL_FOR_SCRIPT
		CASE 1  RETURN EX_PROP_CRATE_CLOSED_SC
		CASE 2  RETURN EX_PROP_CRATE_MED_SC
		CASE 3  RETURN EX_PROP_CRATE_TOB_SC
		CASE 4  RETURN EX_PROP_CRATE_ART_SC
		CASE 5  RETURN EX_PROP_CRATE_ELEC_SC
		CASE 6  RETURN EX_PROP_CRATE_AMMO_SC
		CASE 7  RETURN EX_PROP_CRATE_NARC_SC
		CASE 8  RETURN EX_PROP_CRATE_GEMS_SC
		CASE 9  RETURN EX_PROP_CRATE_WLIFE_SC
		CASE 10 RETURN EX_PROP_CRATE_JEWELS_SC
		CASE 11 RETURN EX_PROP_CRATE_BULL_SC_02
		// Large
		CASE 12 RETURN EX_PROP_CRATE_CLOSED_BC
		CASE 13 RETURN EX_PROP_CRATE_MED_BC
		CASE 14 RETURN EX_PROP_CRATE_TOB_BC
		CASE 15 RETURN EX_PROP_CRATE_ART_BC
		CASE 16 RETURN EX_PROP_CRATE_ELEC_BC
		CASE 17 RETURN EX_PROP_CRATE_AMMO_BC
		CASE 18 RETURN EX_PROP_CRATE_NARC_BC
		CASE 19 RETURN EX_PROP_CRATE_GEMS_BC
		CASE 20 RETURN EX_PROP_CRATE_WLIFE_BC
		CASE 21 RETURN EX_PROP_CRATE_JEWELS_BC
		CASE 22 RETURN EX_PROP_CRATE_BULL_BC_02
		// Rare
		CASE 23 RETURN EX_PROP_CRATE_OEGG
		CASE 24 RETURN EX_PROP_CRATE_MINIG
		CASE 25 RETURN EX_PROP_CRATE_XLDIAM
		CASE 26 RETURN EX_PROP_CRATE_SHIDE
		CASE 27 RETURN EX_PROP_CRATE_FREEL
		CASE 28 RETURN EX_PROP_CRATE_WATCH
		// Newly Added
		CASE 29	RETURN EX_PROP_CRATE_EXPL_SC
		CASE 30	RETURN EX_PROP_CRATE_EXPL_BC
		CASE 31	RETURN EX_PROP_CRATE_JEWELS_RACKS_SC
		CASE 32	RETURN EX_PROP_CRATE_JEWELS_RACKS_BC
		CASE 33	RETURN EX_PROP_CRATE_FURJACKET_SC
		CASE 34	RETURN EX_PROP_CRATE_FURJACKET_BC
		CASE 35	RETURN EX_PROP_CRATE_PHARMA_SC	
		CASE 36	RETURN EX_PROP_CRATE_PHARMA_BC	
		CASE 37	RETURN EX_PROP_CRATE_HIGHEND_PHARMA_SC
		CASE 38	RETURN EX_PROP_CRATE_HIGHEND_PHARMA_BC
		CASE 39	RETURN EX_PROP_CRATE_BIOHAZARD_SC
		CASE 40	RETURN EX_PROP_CRATE_BIOHAZARD_BC
		CASE 41	RETURN EX_PROP_CRATE_CLOTHING_SC
		CASE 42	RETURN EX_PROP_CRATE_CLOTHING_BC
		CASE 43	RETURN EX_PROP_CRATE_MONEY_SC
		CASE 44	RETURN EX_PROP_CRATE_MONEY_BC
		CASE 45	RETURN EX_PROP_CRATE_CLOSED_MS
		CASE 46	RETURN EX_PROP_CRATE_CLOSED_RW
		CASE 47	RETURN EX_PROP_CRATE_CLOSED_MW
		CASE 48 RETURN EX_PROP_CRATE_ART_02_BC
		CASE 49 RETURN EX_PROP_CRATE_ART_02_SC
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

FUNC INT GET_WAREHOUSE_CRATE_CAPACITY(EWarehouseSize eSize)
	SWITCH eSize
		CASE eWarehouseSmall	RETURN ciSMALL_WAREHOUSE_CRATE_CAPACITY		BREAK
		CASE eWarehouseMedium	RETURN ciMEDIUM_WAREHOUSE_CRATE_CAPACITY	BREAK
		CASE eWarehouseLarge	RETURN ciLARGE_WAREHOUSE_CRATE_CAPACITY		BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC MODEL_NAMES SET_WAREHOUSE_CRATE_MODEL(INT iWarehouse, INT iCrateIndex, CONTRABAND_TYPE eContrabandType)

	INT iClosedChance = GET_RANDOM_INT_IN_RANGE(0, 2)
	IF IS_WAREHOUSE_CRATE_ON_TOP_SHELF(GET_WAREHOUSE_SIZE(iWarehouse), iCrateIndex)
		iClosedChance = GET_RANDOM_INT_IN_RANGE(0, 4)
	ENDIF
	
	BOOL bClosedCrate = FALSE
	IF iClosedChance = 1
	OR iClosedChance = 2
	OR iClosedChance = 3
		bClosedCrate = TRUE
	ENDIF
	
	INT iCrateSize = ENUM_TO_INT(GET_WAREHOUSE_CREATE_SIZE_FROM_CRATE_SLOT(GET_WAREHOUSE_SIZE(iWarehouse), iCrateIndex))
	RETURN GET_WAREHOUSE_CRATE_MODEL(ENUM_TO_INT(eContrabandType), iCrateSize, bClosedCrate)
	
ENDFUNC

/// PURPOSE: Populates warehouse with newly added or removed contraband
PROC POPULATE_WAREHOUSE_CRATES(INT iWarehouse, INT iWHSaveSlot, INT iNewContrabandTotal, INT iSpecialContrabandCount, BOOL bAddingContraband, CONTRABAND_TYPE eContrabandType = CONTRABAND_TYPE_MAX)
	
	CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Displaying Info:")
	CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - iWarehouse: ", iWarehouse)
	CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - iSaveSlot: ", iWHSaveSlot)
	CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - iNewContrabandTotal: ", iNewContrabandTotal)
	CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - eContrabandType: ", ENUM_TO_INT(eContrabandType))
	CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - iSpecialContrabandCount: ",iSpecialContrabandCount)
	
	INT iCrateIndex
	INT iSpecialItemIndex
	INT iSpecialItemsAdded
	
	IF bAddingContraband
		CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Adding contraband")
		FOR iCrateIndex = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) TO (iNewContrabandTotal-1)
			IF iCrateIndex < GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
				IF iSpecialItemsAdded < iSpecialContrabandCount
					IF IS_WAREHOUSE_CRATE_ON_BOTTOM_SHELF(GET_WAREHOUSE_SIZE(iWarehouse), iCrateIndex)
						MODEL_NAMES eCrateModel = GET_WAREHOUSE_SPECIAL_CRATE_MODEL(ENUM_TO_INT(eContrabandType))
						SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex, GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
						iSpecialItemsAdded++
						
						CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Adding SPECIAL ITEM in slot: ", iCrateIndex, " Model Index: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
					ELSE
						// Swap special item with existing crate on bottom shelf if possible
						CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Special Item is not on bottom shelf, find crate to swap with.")
						INT iSpecialIndex
						FOR iSpecialIndex = 0 TO (iNewContrabandTotal-1)
							IF IS_WAREHOUSE_CRATE_ON_BOTTOM_SHELF(GET_WAREHOUSE_SIZE(iWarehouse), iSpecialIndex)
								IF NOT IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialIndex)))
									INT iReplacedCrate = GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialIndex)
									MODEL_NAMES eCrateModel = GET_WAREHOUSE_SPECIAL_CRATE_MODEL(ENUM_TO_INT(eContrabandType))
									SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialIndex, GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
									SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, (iNewContrabandTotal-1), iReplacedCrate)
									iSpecialItemsAdded++

									CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Replacing crate in slot: ", iSpecialIndex, " with SPECIAL ITEM Model Index: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
									CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Moving replaced crate to slot: ", (iNewContrabandTotal-1), " with Model Index: ", iReplacedCrate)
									EXIT
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ELSE
					INT iGenerateCounter = 0
					MODEL_NAMES eCrateModel = SET_WAREHOUSE_CRATE_MODEL(iWarehouse, iCrateIndex, eContrabandType)
					
					IF iCrateIndex > 0
					AND NOT IS_WAREHOUSE_CRATE_ON_TOP_SHELF(GET_WAREHOUSE_SIZE(iWarehouse), iCrateIndex)
						WHILE (eCrateModel = GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, (iCrateIndex-1))) AND iGenerateCounter < 10)
							eCrateModel = SET_WAREHOUSE_CRATE_MODEL(iWarehouse, iCrateIndex, eContrabandType)
							iGenerateCounter++
						ENDWHILE
					ENDIF
					
					IF eCrateModel != DUMMY_MODEL_FOR_SCRIPT
						IF GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex) = 0
							CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Stat for slot: ", iCrateIndex, " is equal to 0.  Continue to set stat")
							SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex, GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
						ENDIF
					ENDIF
					
					CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Adding Regular crate to slot: ", iCrateIndex, " with Model Index: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		BOOL bCrateReplaced
		CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Removing contraband")
		
		FOR iCrateIndex = iNewContrabandTotal TO (GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)-1)
			IF iCrateIndex < GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
				// Selling special items
				IF iSpecialContrabandCount > 0
					FOR iSpecialItemIndex = 0 TO (GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)-1)
						IF IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialItemIndex)))
							IF NOT IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex)))
								CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Removing a Special Item - Swapping its slot first - slot: ", iCrateIndex, " with existing crate slot: ", iSpecialItemIndex)
								INT iSpecialCrate = GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialItemIndex)
								SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialItemIndex, GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex))
								SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex, iSpecialCrate)
							ENDIF
						ENDIF
					ENDFOR
				ELSE
					IF IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex)))
						// We are not selling special item but one is about to be removed, swap it with regular crate if possible
						bCrateReplaced = FALSE
						FOR iSpecialItemIndex = 0 TO (g_ciMaxSpecialItems-1)
							IF NOT bCrateReplaced
								IF iNewContrabandTotal >= iSpecialItemIndex
									IF IS_WAREHOUSE_CRATE_ON_BOTTOM_SHELF(GET_WAREHOUSE_SIZE(iWarehouse), iSpecialItemIndex)
										IF NOT IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialItemIndex)))
											CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Saving a Special Item by swapping its slot: ", iCrateIndex, " with existing crate slot: ", iSpecialItemIndex)
											INT iReplacedCrate = GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialItemIndex)
											SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iSpecialItemIndex, GET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex))
											SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex, iReplacedCrate)
											bCrateReplaced = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		FOR iCrateIndex = iNewContrabandTotal TO (GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)-1)
			IF iCrateIndex < GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
				CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] POPULATE_WAREHOUSE_CRATES - Removing contraband in slot ", iCrateIndex)
				SET_WAREHOUSE_CRATES_STAT_INT(iWHSaveSlot, iCrateIndex, GET_WAREHOUSE_CRATE_MODEL_INDEX(DUMMY_MODEL_FOR_SCRIPT))
			ENDIF
		ENDFOR
	ENDIF
	
	g_bWHUpdateMissionCrates = FALSE
	FOR iCrateIndex = 0 TO (g_ciMaxContrabandDelivery-1)
		g_bWHCrateDelivered[iCrateIndex] = FALSE
	ENDFOR
ENDPROC

PROC VALIDATE_WAREHOUSE_CRATES()
	INT iWarehouses
	INT iCrateSlot
	INT iSpecialCrateSlot
	BOOL bSpecialItemPlaced
	FOR iWarehouses = 0 TO (ciMaxOwnedWarehouses-1)
		INT iWarehouseID = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iWarehouses)
		CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] VALIDATE_WAREHOUSE_CRATES - Warehouse Slot: ", iWarehouses, " iWarehouseID: ", iWarehouseID)
		IF iWarehouseID != ciW_Invalid
		AND CHECK_WAREHOUSE_ID(iWarehouseID)
			INT iWarehouseSavedSlot  = GET_SAVE_SLOT_FOR_PLAYERS_WAREHOUSE(iWarehouseID, PLAYER_ID())
			INT iWarehouseContraband = GET_PLAYER_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(PLAYER_ID(), iWarehouseID)
			INT iWarehouseCapacity 	 = GET_WAREHOUSE_CRATE_CAPACITY(GET_WAREHOUSE_SIZE(iWarehouseID))
			INT iRandContrabandType  = GET_RANDOM_INT_IN_RANGE(0, 11)
			INT iCurrContrabandType  = 0
			INT iContrabandTypeCount = 0
			FOR iCrateSlot = 0 TO (iWarehouseContraband-1)
				IF GET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, iCrateSlot) = 0
					INT iRandCount = 0
					IF iContrabandTypeCount  > 2
						iContrabandTypeCount = 0
						iRandContrabandType  = GET_RANDOM_INT_IN_RANGE(0, 11)
						WHILE (iCurrContrabandType = iRandContrabandType AND iRandCount < 10)
							iRandContrabandType    = GET_RANDOM_INT_IN_RANGE(0, 11)
							iRandCount++
						ENDWHILE
					ENDIF
					MODEL_NAMES eCrateModel = SET_WAREHOUSE_CRATE_MODEL(iWarehouseID, iCrateSlot, INT_TO_ENUM(CONTRABAND_TYPE, iRandContrabandType))
					iRandCount = 0
					IF iCrateSlot > 0
					AND NOT IS_WAREHOUSE_CRATE_ON_TOP_SHELF(GET_WAREHOUSE_SIZE(iWarehouseID), iCrateSlot)
						WHILE (eCrateModel = INT_TO_ENUM(MODEL_NAMES, GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, (iCrateSlot-1)))) AND iRandCount < 10)
							eCrateModel = SET_WAREHOUSE_CRATE_MODEL(iWarehouseID, iCrateSlot, INT_TO_ENUM(CONTRABAND_TYPE, iRandContrabandType))
							iRandCount++
						ENDWHILE
					ENDIF
					IF eCrateModel != DUMMY_MODEL_FOR_SCRIPT
						SET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, iCrateSlot, GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel))
						CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] VALIDATE_WAREHOUSE_CRATES - Setting Regular Crate Model: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(eCrateModel), " in Crate Slot: ", iCrateSlot)
						iContrabandTypeCount++
					ENDIF
					iCurrContrabandType = iRandContrabandType
				ENDIF
			ENDFOR
			
			IF GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iWarehouseID) > 0
				FOR iCrateSlot = 1 TO g_ciMaxSpecialItems
					IF DOES_PLAYER_HAVE_SPECIAL_ITEM_IN_WAREHOUSE(iWarehouseID, INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, iCrateSlot))
						bSpecialItemPlaced = FALSE
						INT iSpecialItemCount = 0
						FOR iSpecialCrateSlot = 0 TO (iWarehouseContraband-1)
							IF GET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, iSpecialCrateSlot) != GET_WAREHOUSE_CRATE_MODEL_INDEX(GET_SPECIAL_ITEM_MODEL_FROM_CONTRABAND_TYPE(INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, iCrateSlot)))
								iSpecialItemCount++
							ENDIF
						ENDFOR
						IF iSpecialItemCount = iWarehouseContraband
							FOR iSpecialCrateSlot = 1 TO g_ciMaxSpecialItems
								IF NOT bSpecialItemPlaced
									IF NOT IS_WAREHOUSE_CRATE_A_SPECIAL_ITEM(GET_WAREHOUSE_CRATE_MODEL_FROM_INDEX(GET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, iSpecialCrateSlot)))
										SET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, iSpecialCrateSlot, GET_WAREHOUSE_CRATE_MODEL_INDEX(GET_SPECIAL_ITEM_MODEL_FROM_CONTRABAND_TYPE(INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, iSpecialCrateSlot))))
										CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] VALIDATE_WAREHOUSE_CRATES - Setting Special Crate Model: ", GET_WAREHOUSE_CRATE_MODEL_INDEX(GET_SPECIAL_ITEM_MODEL_FROM_CONTRABAND_TYPE(INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, iSpecialCrateSlot))), " in Crate Slot: ", iSpecialCrateSlot)
										bSpecialItemPlaced = TRUE
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			
			FOR iCrateSlot = iWarehouseContraband TO (iWarehouseCapacity-1)
				SET_WAREHOUSE_CRATES_STAT_INT(iWarehouseSavedSlot, iCrateSlot, 0)
				CDEBUG1LN(DEBUG_PROPERTY, "[CONTRABAND] VALIDATE_WAREHOUSE_CRATES - REMAINING SLOTS - Clearing crate slot: ", iCrateSlot)
			ENDFOR
		ENDIF
	ENDFOR
ENDPROC

PROC UNLOCK_CONTRABAND_SWAG_IN_OFFICE(CONTRABAND_TYPE eContrabandType)
	SWITCH eContrabandType
		CASE CONTRABAND_TYPE_MEDICAL_SUPPLIES
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_PILLS, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_PILLS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_PILLS2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_MED3, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_PILLS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_PILLS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_PILLS3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_BOOZE_CIGS3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_ART_AND_ANTIQUES
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ART3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_ELECTRONICS
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_ELECTRONIC3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_WEAPONS_AND_AMMO
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GUNS3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_NARCOTICS
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGSTATUE, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGSTATUE, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGSTATUE2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGBAGS3, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGSTATUE, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGSTATUE2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_DRUGSTATUE3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_GEMS
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_GEMS3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_WILDLIFE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_IVORY, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_IVORY, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_IVORY2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_FURCOATS3, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_IVORY, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_IVORY2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_IVORY3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_ENTERTAINMENT
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_COUNTERFEIT3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_JEWELRY
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_JEWELWATCH3, TRUE)
			ENDIF
		BREAK
		CASE CONTRABAND_TYPE_BULLION
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER, TRUE)
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER2)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER2, TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER2, TRUE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER3, TRUE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC ADD_CONTRABAND_TO_WAREHOUSE(INT iWarehouse, INT iContrabandToAdd, INT iSpecialContrabandCount, CONTRABAND_TRANSACTION_STATE &eResult, CONTRABAND_TYPE eContrabandType)
	
	IF iContrabandToAdd <= 0
		IF iContrabandToAdd = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE Trying to add 0 contraband to warehouse: ", iWarehouse, " iContrabandToAdd:", iContrabandToAdd)
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE Trying to add a negative amount of contraband to warehouse: ", iWarehouse, " iContrabandToAdd:", iContrabandToAdd)
		ENDIF
		
		eResult = CONTRABAND_TRANSACTION_STATE_FAILED		
		EXIT
	ENDIF
	
	// Reset state if we need to.
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	IF eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
		IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE - Wait on already running transaction")
			EXIT
		ENDIF
	ENDIF

	//Check if we own the warehouse and then grab it's capacity
	INT iCapacityRemaining	= GET_WAREHOUSE_REMAINING_CAPACITY(iWarehouse)
	INT iSaveSlot			= GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	
	IF iCapacityRemaining = 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE - Capacity = 0. Not adding contraband")
		eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
		EXIT
	ELIF iContrabandToAdd > iCapacityRemaining
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE - Reducing contraband to add from ", iContrabandToAdd, " to ", iCapacityRemaining )
		iContrabandToAdd = iCapacityRemaining	
	ENDIF
	
	IF NOT USE_SERVER_TRANSACTIONS()
		IF iSaveSlot >= 0 
		AND iCapacityRemaining >= iContrabandToAdd
			//Calculate the new contraband total
			INT iNewContrabandTotal  = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) + iContrabandToAdd
			// Fill WH slots
			POPULATE_WAREHOUSE_CRATES(iWarehouse, iSaveSlot, iNewContrabandTotal, iSpecialContrabandCount, TRUE, eContrabandType)
			//Set the stat
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSaveSlot), iNewContrabandTotal)
			//Set the BD
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iContrabandUnitsTotal = iNewContrabandTotal
			
			IF (GET_TOTAL_SPECIAL_ITEM_COUNT() + iSpecialContrabandCount) <= g_ciMaxSpecialItems
				IF iSpecialContrabandCount != 0
					ADD_SPECIAL_ITEMS_TO_INVENTORY_STAT(iWarehouse, iSpecialContrabandCount)
					INT iIndex = (GET_TOTAL_SPECIAL_ITEM_COUNT() -1)
					ADD_SPECIAL_ITEMS_TO_WAREHOUSE(iWarehouse, GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(eContrabandType), iSpecialContrabandCount, iIndex)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE Tried to add more special items than we have space for to warehouse")
			ENDIF
			
			UNLOCK_CONTRABAND_SWAG_IN_OFFICE(eContrabandType)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES called after buy mission completed")
			SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE(iWarehouse)
			RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES(TRUE)
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
			ENDIF
			
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SOURCE_SPECIAL_CARGO)

			REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE: ", iWarehouse, " adding: ", iContrabandToAdd)
			eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS			
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE: Tried to add more contraband than capacity remaining to warehouse: ", iWarehouse, " iContrabandToAdd: ", iContrabandToAdd, " remaining capacity: ", iCapacityRemaining, " iSaveSlot: ", iSaveSlot)
			eResult = CONTRABAND_TRANSACTION_STATE_FAILED
			
		ENDIF
	ELSE
		IF iSaveSlot >= 0
		AND iCapacityRemaining >= iContrabandToAdd
			IF PROCESS_TRANSACTION_FOR_ADD_CONTRABAND(iSaveSlot, iContrabandToAdd, iSpecialContrabandCount, eResult)
				IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					//Calculate the new contraband total
					INT iNewContrabandTotal  = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) + iContrabandToAdd
					// Fill WH slots
					POPULATE_WAREHOUSE_CRATES(iWarehouse, iSaveSlot, iNewContrabandTotal, iSpecialContrabandCount, TRUE, eContrabandType)
					//Set the stat
					SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSaveSlot), iNewContrabandTotal)
					//Set the BD
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iContrabandUnitsTotal = iNewContrabandTotal
					
					IF (GET_TOTAL_SPECIAL_ITEM_COUNT() /*+ iSpecialContrabandCount !Not for PC! */) <= g_ciMaxSpecialItems
						IF iSpecialContrabandCount != 0
							ADD_SPECIAL_ITEMS_TO_INVENTORY_STAT(iWarehouse, iSpecialContrabandCount)
							INT iIndex = (GET_TOTAL_SPECIAL_ITEM_COUNT() -1)
							ADD_SPECIAL_ITEMS_TO_WAREHOUSE(iWarehouse, GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(eContrabandType), iSpecialContrabandCount, iIndex)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE Tried to add more special items than we have space for to warehouse")
					ENDIF
					
					UNLOCK_CONTRABAND_SWAG_IN_OFFICE(eContrabandType)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES called after buy mission completed")
					SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE(iWarehouse)
					RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES(TRUE)
					
					IF NETWORK_IS_ACTIVITY_SESSION()
						STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
					ENDIF

					REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE: ", iWarehouse, " adding: ", iContrabandToAdd)
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SOURCE_SPECIAL_CARGO)
				ELIF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE: TRANSACTION FAILED FOR ", iWarehouse, " trying to add: ", iContrabandToAdd)
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				ENDIF
			ENDIF
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE: PC transaction failed to add contraband to warehouse: ", iWarehouse, " iContrabandToAdd: ", iContrabandToAdd, " remaining capacity: ", iCapacityRemaining, " iSaveSlot: ", iSaveSlot, " Is warehouse disabled: ", IS_WAREHOUSE_DISABLED_BY_TUNEABLE(iWarehouse))
			eResult = CONTRABAND_TRANSACTION_STATE_FAILED
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_CONTRABAND_FROM_WAREHOUSE(INT iWarehouse, INT iContrabandToRemove, INT iEarnValue, REMOVE_CONTRABAND_REASON_ENUM eReason, CONTRABAND_TRANSACTION_STATE &eResult, BOOL bSpecialItemsOnly = FALSE)

	IF iContrabandToRemove <= 0
		IF iContrabandToRemove = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] REMOVE_CONTRABAND_FROM_WAREHOUSE Trying to remove 0 contraband from warehouse: ", iWarehouse, " iContrabandToRemove:", iContrabandToRemove)
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[CONTRABAND] REMOVE_CONTRABAND_FROM_WAREHOUSE Trying to remove a negative amount contraband from warehouse: ", iWarehouse, " iContrabandToRemove:", iContrabandToRemove)
		ENDIF
		
		eResult = CONTRABAND_TRANSACTION_STATE_FAILED		
		EXIT
	ENDIF

	// Reset state if we need to.
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF

	//Check if we own the warehouse and then grab the ammount of contraband stored
	INT iContraband	= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)
	INT iSaveSlot	= GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	
	IF NOT USE_SERVER_TRANSACTIONS()
		IF iSaveSlot >= 0 
		AND iContraband >= iContrabandToRemove
			//Calculate the new contraband total
			INT iNewContrabandTotal  = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) - iContrabandToRemove
			
			// We only want to remove the special items if there are no more standard crates to get rid of.
			INT iSpecialItemsToRemove = 0
			IF bSpecialItemsOnly
				iSpecialItemsToRemove = iContrabandToRemove
			ENDIF
			
			// Clear WH slots
			POPULATE_WAREHOUSE_CRATES(iWarehouse, iSaveSlot, iNewContrabandTotal, iSpecialItemsToRemove, FALSE)
			//Set the stat
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSaveSlot), iNewContrabandTotal)
			//Set the BD
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iContrabandUnitsTotal = iNewContrabandTotal
			
			IF iSpecialItemsToRemove != 0
				REMOVE_SPECIAL_ITEMS_FROM_INVENTORY_STAT(iWarehouse, iSpecialItemsToRemove)
				REMOVE_SPECIAL_ITEMS_FROM_WAREHOUSE(iWarehouse, iSpecialItemsToRemove)
			ENDIF
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
			ENDIF

			REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_CONTRABAND_FROM_WAREHOUSE: ", iWarehouse, " removing: ", iContrabandToRemove)
			eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_CONTRABAND_FROM_WAREHOUSE: Tried to REMOVE more contraband than remains from warehouse: ", iWarehouse, " iContrabandToRemove: ", iContrabandToRemove, " remaining capacity: ", iContraband)
			eResult = CONTRABAND_TRANSACTION_STATE_FAILED
		ENDIF
	ELSE
		IF iSaveSlot >= 0
		AND iContraband >= iContrabandToRemove
		
			//Calculate the new contraband total
			INT iNewContrabandTotal  = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) - iContrabandToRemove
			
			// We only want to remove the special items if there are no more standard crates to get rid of.
			INT iSpecialItemsToRemove = 0
			IF bSpecialItemsOnly
				iSpecialItemsToRemove = iContrabandToRemove
			ENDIF
		
			IF PROCESS_TRANSACTION_FOR_REMOVE_CONTRABAND(iSaveSlot, iContrabandToRemove, iSpecialItemsToRemove, iEarnValue, eReason, eResult,  MPGlobalsAmbience.sMagnateGangBossData.iContrabandUnitsDelivered)
				IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					// Clear WH slots
					POPULATE_WAREHOUSE_CRATES(iWarehouse, iSaveSlot, iNewContrabandTotal, iSpecialItemsToRemove, FALSE)
					//Set the stat
					SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSaveSlot), iNewContrabandTotal)
					//Set the BD
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iContrabandUnitsTotal = iNewContrabandTotal
					
					IF iSpecialItemsToRemove != 0
						//REMOVE_SPECIAL_ITEMS_FROM_INVENTORY_STAT(iWarehouse, iSpecialItemsToRemove)
						REMOVE_SPECIAL_ITEMS_FROM_WAREHOUSE(iWarehouse, iSpecialItemsToRemove)
					ENDIF
					
					IF NETWORK_IS_ACTIVITY_SESSION()
						STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
					ENDIF

					REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_CONTRABAND_FROM_WAREHOUSE: ", iWarehouse, " removing: ", iContrabandToRemove, " SpecialOnly? ", bSpecialItemsOnly, " iSpecialItemsToRemove: ", iSpecialItemsToRemove)
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
				ELIF eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_CONTRABAND_FROM_WAREHOUSE: TRANSACTION FAILED FOR ", iWarehouse, " trying to remove: ", iContrabandToRemove)
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				ENDIF
			ENDIF
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_CONTRABAND_FROM_WAREHOUSE: Tried to REMOVE more contraband than remains from warehouse: ", iWarehouse, " iContrabandToRemove: ", iContrabandToRemove, " remaining capacity: ", iContraband)
			eResult = CONTRABAND_TRANSACTION_STATE_FAILED
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns the total utility bill for all player owned warehouses
FUNC INT GET_TOTAL_UTILITY_CHARGE_FOR_WAREHOUSES()
	INT iIter
	INT iTotalUtilityCharge
	
	//Not calling does player own any warehouses as this loops through the 5 slots 
	//which we don't need to do if we're going to loop through them below anyway
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			EWarehouseSize WarehouseSize = GET_WAREHOUSE_SIZE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse)
				
			//Adds to the total based on the size of the warehouse
			IF WarehouseSize = eWarehouseLarge
				g_sBusinessCostBreakdown.iLargeWhouseFees += g_sMPTunables.iEXEC1_LARGE_WAREHOUSE_FEES
				iTotalUtilityCharge += g_sMPTunables.iEXEC1_LARGE_WAREHOUSE_FEES
			ELIF WarehouseSize = eWarehouseMedium
				g_sBusinessCostBreakdown.iMediumWhouseFees += g_sMPTunables.iEXEC1_MEDIUM_WAREHOUSE_FEES
				iTotalUtilityCharge += g_sMPTunables.iEXEC1_MEDIUM_WAREHOUSE_FEES
			ELSE
				g_sBusinessCostBreakdown.iSmallWhouseFees += g_sMPTunables.iEXEC1_SMALL_WAREHOUSE_FEES
				iTotalUtilityCharge += g_sMPTunables.iEXEC1_SMALL_WAREHOUSE_FEES
			ENDIF
			
			#IF IS_DEBUG_BUILD
			INT iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse	
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_TOTAL_UTILITY_CHARGE_FOR_WAREHOUSES: Warehouse:", iWarehouse, " Size: ", WarehouseSize, " iTotalUtilityCharge: ", iTotalUtilityCharge)
			#ENDIF
		ENDIF
	ENDFOR
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_TOTAL_UTILITY_CHARGE_FOR_WAREHOUSES: RETURNING: ", iTotalUtilityCharge)
	RETURN iTotalUtilityCharge
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_DEBUG_WAREHOUSE_TESTS(INT iWarehouse, BOOL bSelling = FALSE)
	//Debug prints for testing
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE: ", 				DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE())
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_LOCAL_PLAYER_OWN_WAREHOUSE: ", 				DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse))
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_COUNT_OF_WAREHOUSES_OWNED: ", 					GET_COUNT_OF_WAREHOUSES_OWNED())
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_PLAYER_OWN_WAREHOUSE: ", 						DOES_PLAYER_OWN_WAREHOUSE(PLAYER_ID(), iWarehouse))
	IF NOT bSelling
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE: ", 	GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse))
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_REMAINING_CAPACITY: ", 			GET_WAREHOUSE_REMAINING_CAPACITY(iWarehouse))
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_FULL: ", 							IS_WAREHOUSE_FULL(iWarehouse))
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] IS_WAREHOUSE_EMPTY: ", 						IS_WAREHOUSE_EMPTY(iWarehouse))
		
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE_STAT] CHECKING WHOUSE OWNERSHIP STAT: ", 		g_MP_STAT_WAREHOUSE[GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)][GET_SLOT_NUMBER(-1)])
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE_STAT] CHECKING WHOUSE CONTRABAND STAT: ", 		g_MP_STAT_CONTRABAND_TOT_FOR_WAREHOUSE[GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)][GET_SLOT_NUMBER(-1)])
		CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE_STAT] CHECKING WHOUSE SPECIAL CONTRABAND STAT: ", GET_MP_INT_CHARACTER_STAT(GET_SPCOUNT_WH_STAT_FROM_INDEX(GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)), GET_SLOT_NUMBER(-1)))
	ENDIF
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_PLAYER_CONTRABAND_TOTAL: ", 					GET_PLAYER_CONTRABAND_TOTAL(PLAYER_ID()))
	CDEBUG2LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_SIZE: ", 							GET_WAREHOUSE_SIZE(iWarehouse))
	GET_TOTAL_UTILITY_CHARGE_FOR_WAREHOUSES()
ENDPROC
#ENDIF //IF IS_DEBUG_BUILD

/// PURPOSE: Saves the stat and updates the broadcast data for the warehouse being purchased
PROC SET_WAREHOUSE_AS_PURCHASED(INT iWarehouseToPurchase)
	INT iIter
	
	IF NOT IS_WAREHOUSE_DISABLED_BY_TUNEABLE(iWarehouseToPurchase)
	AND NOT DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouseToPurchase)
	AND GET_COUNT_OF_WAREHOUSES_OWNED() < ciMaxOwnedWarehouses
		FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
			IF g_MP_STAT_WAREHOUSE[iIter][GET_SLOT_NUMBER(-1)]  = ciW_Invalid
				//Set the relevant stat
				SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_PURCHASE(iIter), iWarehouseToPurchase)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] SET_WAREHOUSE_AS_PURCHASED: Setting player stats, warehouse: ", iWarehouseToPurchase, " purchased in slot: ", iIter)
				
				//Set player broadcast data
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iWarehouse 			= iWarehouseToPurchase
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iIter].iContrabandUnitsTotal 	= 0
				#IF IS_DEBUG_BUILD
					PRINT_DEBUG_WAREHOUSE_TESTS(iWarehouseToPurchase)
				#ENDIF				
				
				SET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_OWNERSHIP_VERIFICATION(iIter), iWarehouseToPurchase)
				
				EXIT
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

/// PURPOSE: Sells the warehouse if it is currently owned by the local player 
///    (test function - in practice we won't be able to sell warehouses, only replace them)
PROC REMOVE_WAREHOUSE(INT iWarehouse)
	IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)	
		INT iSlotToRemove = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
		
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_PURCHASE(iSlotToRemove), ciW_Invalid)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSlotToRemove), 0)
		
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSlotToRemove].iWarehouse 			= ciW_Invalid
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSlotToRemove].iContrabandUnitsTotal 	= 0
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] REMOVE_WAREHOUSE: Setting player stats, warehouse: ", iWarehouse, " removed in slot: ", iSlotToRemove)
		
		#IF IS_DEBUG_BUILD
			PRINT_DEBUG_WAREHOUSE_TESTS(iWarehouse, TRUE)
		#ENDIF
	ENDIF
ENDPROC

FUNC PLAYER_INDEX GET_OWNER_OF_WAREHOUSE(INT iWarehouse)
	// VIPs warehouse
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		PLAYER_INDEX playerBoss = GB_GET_THIS_PLAYER_GANG_BOSS(PLAYER_ID())
        IF IS_NET_PLAYER_OK(playerBoss, FALSE)
	    	IF DOES_PLAYER_OWN_WAREHOUSE(playerBoss, iWarehouse)
	        	RETURN playerBoss
	    	ENDIF
     	ENDIF
	ELSE
		// Owned warehouse
		IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
			RETURN PLAYER_ID()
		ENDIF
    ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

#IF FEATURE_DLC_1_2022
FUNC MP_INT_STATS GET_CARGO_SOURCING_POSIX_STAT_FOR_SAVE_SLOT(INT iSaveSlot)
	SWITCH iSaveSlot
		CASE 0
			RETURN MP_STAT_WAREHOUSE_CARGO_SRC_POSIX_0
		BREAK
		CASE 1
			RETURN MP_STAT_WAREHOUSE_CARGO_SRC_POSIX_1
		BREAK
		CASE 2
			RETURN MP_STAT_WAREHOUSE_CARGO_SRC_POSIX_2
		BREAK
		CASE 3
			RETURN MP_STAT_WAREHOUSE_CARGO_SRC_POSIX_3
		BREAK
		CASE 4
			RETURN MP_STAT_WAREHOUSE_CARGO_SRC_POSIX_4
		BREAK
	ENDSWITCH
	
	RETURN MP_STAT_WAREHOUSE_CARGO_SRC_POSIX_0
ENDFUNC

PROC SET_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS(BOOL bInProgress, INT iSaveSlot)
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(iSaveSlot)
	#ENDIF
	IF g_sWarehouseCargoSourcingData.bIsCargoDeliveryTransactionInProgress != bInProgress
		IF NOT g_sWarehouseCargoSourcingData.bIsCargoDeliveryTransactionInProgress	
			PRINTLN("[WAREHOUSE][CARGO SOURCING][WAREHOUSE SLOT ",iSaveSlot,"] -------------------------- SETTING TRANSACTION IN PROGRESS = TRUE  -------------------------- ")	
		ELSE
			PRINTLN("[WAREHOUSE][CARGO SOURCING][WAREHOUSE SLOT ",iSaveSlot,"] -------------------------- SETTING TRANSACTION IN PROGRESS = FALSE  -------------------------- ")
		ENDIF
		
		g_sWarehouseCargoSourcingData.bIsCargoDeliveryTransactionInProgress = bInProgress
	ENDIF
ENDPROC

FUNC BOOL IS_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS()
	RETURN g_sWarehouseCargoSourcingData.bIsCargoDeliveryTransactionInProgress
ENDFUNC

PROC SET_SOURCING_CARGO_PACKED_STAT(INT iSaveSlot, BOOL bSourcing)
	SWITCH iSaveSlot
		CASE 0
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_0, bSourcing)
		BREAK
		CASE 1
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_1, bSourcing)
		BREAK
		CASE 2
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_2, bSourcing)
		BREAK
		CASE 3
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_3, bSourcing)
		BREAK
		CASE 4
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_4, bSourcing)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL GET_SOURCING_CARGO_PACKED_STAT(INT iSaveSlot)
	SWITCH iSaveSlot
		CASE 0
			RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_0)
		BREAK
		CASE 1
			RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_1)
		BREAK
		CASE 2
			RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_2)
		BREAK
		CASE 3
			RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_3)
		BREAK
		CASE 4
			RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SOURCING_CARGO_WAREHOUSE_4)
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC REQUEST_DELIVER_CARGO_TO_WAREHOUSE(INT iWarehouse)
	INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	IF iSaveSlot != -1
		g_sWarehouseCargoSourcingData.bRequestDeliverCargo[iSaveSlot] = TRUE
		SET_SOURCING_CARGO_PACKED_STAT(iSaveSlot, FALSE)
		PRINTLN("[WAREHOUSE][CARGO SOURCING] REQUEST_DELIVER_CARGO_TO_WAREHOUSE REQUESTING DELIVER TO WAREHOUSE  WAREHOUSE:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ELSE
		PRINTLN("[WAREHOUSE][CARGO SOURCING] REQUEST_DELIVER_CARGO_TO_WAREHOUSE FAILED  WAREHOUSE:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ENDIF
ENDPROC

FUNC BOOL IS_WAREHOUSE_PED_SOURCING_CARGO(INT iWarehouse)	
	
	IF iWarehouse != ciW_Invalid
	AND GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse) != -1		
		IF GET_OWNER_OF_WAREHOUSE(iWarehouse) = PLAYER_ID()	
			INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
						
			IF (GET_MP_INT_CHARACTER_STAT(GET_CARGO_SOURCING_POSIX_STAT_FOR_SAVE_SLOT(iSaveSlot)) > GET_CLOUD_TIME_AS_INT())
				#IF IS_DEBUG_BUILD
				IF g_sDebugWarehouseCargoSourcingData.bUseWidgetValues
				AND g_sDebugWarehouseCargoSourcingData.bForceReturnNow
					g_sDebugWarehouseCargoSourcingData.bForceReturnNow = FALSE
					SET_MP_INT_CHARACTER_STAT(GET_CARGO_SOURCING_POSIX_STAT_FOR_SAVE_SLOT(iSaveSlot), GET_CLOUD_TIME_AS_INT())
				ENDIF
				#ENDIF
				
				IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, iSaveSlot)
					PRINTLN("[WAREHOUSE][CARGO SOURCING] SETTING BIT FOR PED WAREHOUSE: ", iWarehouse, " SAVE SLOT: ", iSaveSlot)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, iSaveSlot)
				ENDIF
				RETURN TRUE		
			ELSE
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, iSaveSlot)
				OR GET_SOURCING_CARGO_PACKED_STAT(iSaveSlot) // IF JOINED SESSION, CARGO WAS SOURCED WHEN PREVIOUS SESSION ENDED BUT SHOULD BE DELIVERED BY NOW
					REQUEST_DELIVER_CARGO_TO_WAREHOUSE(iWarehouse)
					PRINTLN("[WAREHOUSE][CARGO SOURCING] RESETTING BIT FOR PED WAREHOUSE: ", iWarehouse, " SAVE SLOT: ", iSaveSlot)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, iSaveSlot)
				ENDIF
			ENDIF
		ELSE
			PLAYER_INDEX pOwner = GET_OWNER_OF_WAREHOUSE(iWarehouse)
			IF pOwner!=INVALID_PLAYER_INDEX()
				INT iSaveSlot = GET_SAVE_SLOT_FOR_PLAYERS_WAREHOUSE(iWarehouse, pOwner)	
				IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(GET_OWNER_OF_WAREHOUSE(iWarehouse))].iBSThree, iSaveSlot)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Returns true if the warehouse trade between the two passed warehouses is possible
FUNC BOOL ARE_WAREHOUSES_SUITABLE_FOR_TRADE(INT iOldWarehouse, INT iNewWarehouse)
	IF GB_IS_PLAYER_ON_CONTRABAND_MISSION(PLAYER_ID())
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ARE_WAREHOUSES_SUITABLE_FOR_TRADE FALSE GB_IS_PLAYER_ON_CONTRABAND_MISSION is TRUE")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF IS_WAREHOUSE_PED_SOURCING_CARGO(iOldWarehouse)	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ARE_WAREHOUSES_SUITABLE_FOR_TRADE FALSE IS_WAREHOUSE_PED_SOURCING_CARGO is TRUE")
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF iOldWarehouse = iNewWarehouse
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ARE_WAREHOUSES_SUITABLE_FOR_TRADE FALSE old warehouse: ", iOldWarehouse, " is the same as new: ", iNewWarehouse)
		RETURN FALSE
	ELIF GET_WAREHOUSE_SIZE(iOldWarehouse) <= GET_WAREHOUSE_SIZE(iNewWarehouse)
		IF IS_WAREHOUSE_EMPTY(iOldWarehouse)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ARE_WAREHOUSES_SUITABLE_FOR_TRADE TRUE old warehouse: ", iOldWarehouse, " is greater than or equal to the new warehouse: ", iNewWarehouse, ", and also empty")
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ARE_WAREHOUSES_SUITABLE_FOR_TRADE FALSE old warehouse: ", iOldWarehouse, " is not empty")
			RETURN FALSE
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ARE_WAREHOUSES_SUITABLE_FOR_TRADE FALSE old warehouse: ", iOldWarehouse, " is smaler than the new warehouse: ", iNewWarehouse)
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE: swaps an owned warehouse in a slot for a bigger warehouse
///    Keeps the contraband total for this warehouse 
PROC TRADE_IN_WAREHOUSE(INT iOldWarehouse, INT iNewWarehouse)
	IF NOT IS_WAREHOUSE_DISABLED_BY_TUNEABLE(iNewWarehouse)
		IF DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iOldWarehouse)
		AND ARE_WAREHOUSES_SUITABLE_FOR_TRADE(iOldWarehouse, iNewWarehouse)
			INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iOldWarehouse)
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_PURCHASE(iSaveSlot), iNewWarehouse)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iWarehouse = iNewWarehouse
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] TRADE_IN_WAREHOUSE: replacing old warehouse: ", iOldWarehouse, " with new warehouse: ", iNewWarehouse, " in save slot: ", iSaveSlot)
			
			SET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_WAREHOUSE_OWNERSHIP_VERIFICATION(iSaveSlot), iNewWarehouse)
		ENDIF
	ENDIF
ENDPROC

/////////////////////////////////////////
///    
///   DEBUG Functions
///   

#IF IS_DEBUG_BUILD
PROC DEBUG_ADD_REMOVE_WAREHOUSE(INT iWarehouse)
	IF NOT DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouse)
		SET_WAREHOUSE_AS_PURCHASED(iWarehouse)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] DEBUG_ADD_REMOVE_WAREHOUSE: DEBUG purchase warehouse: ", iWarehouse)
	ELSE
		REMOVE_WAREHOUSE(iWarehouse)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] DEBUG_ADD_REMOVE_WAREHOUSE: DEBUG remove warehouse: ", iWarehouse)
	ENDIF
ENDPROC

FUNC STRING DEBUG_GET_CONTRABAND_TYPE_LITERAL_STRING(CONTRABAND_TYPE eContraType)
	IF eContraType 		= CONTRABAND_TYPE_MEDICAL_SUPPLIES		RETURN "Medical Supplies"
	ELIF eContraType 	= CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL	RETURN "Tobacco & Alcahol"
	ELIF eContraType 	= CONTRABAND_TYPE_ART_AND_ANTIQUES		RETURN "Art"
	ELIF eContraType 	= CONTRABAND_TYPE_ELECTRONICS			RETURN "Electronics"
	ELIF eContraType 	= CONTRABAND_TYPE_WEAPONS_AND_AMMO		RETURN "Weapons & Ammo"
	ELIF eContraType 	= CONTRABAND_TYPE_NARCOTICS				RETURN "Narcotics"
	ELIF eContraType 	= CONTRABAND_TYPE_GEMS					RETURN "Gems"
	ELIF eContraType 	= CONTRABAND_TYPE_WILDLIFE				RETURN "Animal Materials"
	ELIF eContraType 	= CONTRABAND_TYPE_ENTERTAINMENT			RETURN "Counterfeit"
	ELIF eContraType 	= CONTRABAND_TYPE_JEWELRY				RETURN "Jewles"
	ELIF eContraType 	= CONTRABAND_TYPE_BULLION				RETURN "Bullion"
	ENDIF
	
	RETURN ""
ENDFUNC

PROC SETUP_PRIVATE_WAREHOUSE_WIDGETS(WAREHOUSE_DEBUG_WIDGET_VARS &debug_vars)	
	START_WIDGET_GROUP("CARGO SOURCING")
		ADD_WIDGET_BOOL("------Use widget values------", g_sDebugWarehouseCargoSourcingData.bUseWidgetValues)
		ADD_WIDGET_INT_SLIDER("Number of crates:", g_sDebugWarehouseCargoSourcingData.iNumCargo, 1, 3, 1)
		
		START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("RANDOM")
		INT i
		REPEAT ENUM_TO_INT(CONTRABAND_TYPE_MAX) i
			ADD_TO_WIDGET_COMBO(DEBUG_GET_CONTRABAND_TYPE_LITERAL_STRING(INT_TO_ENUM(CONTRABAND_TYPE, i)))
		ENDREPEAT
		STOP_WIDGET_COMBO("Contraband type:", g_sDebugWarehouseCargoSourcingData.iContrabandType )
		ADD_WIDGET_BOOL("Force special item:", g_sDebugWarehouseCargoSourcingData.bForceSpecialItem )
		ADD_WIDGET_INT_SLIDER("Cargo sourcing time (min):", g_sDebugWarehouseCargoSourcingData.iCargoSourcingTime, 1, 48, 1)	
		ADD_WIDGET_BOOL("Force 1 ped to return:", g_sDebugWarehouseCargoSourcingData.bForceReturnNow )	
	STOP_WIDGET_GROUP()
	
	
	START_WIDGET_GROUP("Warehouses")	
		START_WIDGET_GROUP("Warehouse Management")
			ADD_WIDGET_INT_SLIDER("Warehouse to Purchase or sell", debug_vars.iWarehouseToPurchaseOrSell, 1, 22, 1)
			ADD_WIDGET_BOOL("Buy warehouse", debug_vars.bPurchaseWarehouse)
			ADD_WIDGET_BOOL("Sell warehouse", debug_vars.bSellWarehouse)
			ADD_WIDGET_BOOL("Warp to warehouse", debug_vars.bWarpToCoords)
			ADD_WIDGET_BOOL("Display warehouse info", debug_vars.bDisplayWarehouseInfo)
			ADD_WIDGET_BOOL("Debug remove all warehouses", debug_vars.bClearAllSlots)
			ADD_WIDGET_BOOL("Trade in wharehouse from slot 0 (Fails if new warehouse is smaller)", debug_vars.bTradeIn)
			ADD_WIDGET_BOOL("Trade in wharehouse from slot 1", debug_vars.bTradeInSlot1)
			ADD_WIDGET_BOOL("Trade in wharehouse from slot 2", debug_vars.bTradeInSlot2)
			ADD_WIDGET_BOOL("Trade in wharehouse from slot 3", debug_vars.bTradeInSlot3)
			ADD_WIDGET_BOOL("Trade in wharehouse from slot 4", debug_vars.bTradeInSlot4)
		STOP_WIDGET_GROUP()
			
		START_WIDGET_GROUP("Contraband Management")
			ADD_WIDGET_INT_SLIDER("1. Warehouse to alter contraband", debug_vars.iWarehouseToAddOrRemoveContrabandFrom, 1, 22, 1)
			ADD_WIDGET_INT_SLIDER("2. Contraband", debug_vars.iContrabandToAddOrRemove, 1, 3, 1)
			ADD_WIDGET_BOOL("3. Buy contraband", debug_vars.bAddContraband)
			ADD_WIDGET_BOOL("4. Sell contraband", debug_vars.bRemoveContraband)
			ADD_WIDGET_BOOL("5. Activate special Item", debug_vars.bSetSpecialItem)
			ADD_WIDGET_INT_SLIDER("6. Special item type", debug_vars.iSpecialItemType, 1, 6, 1)
			
			START_WIDGET_GROUP("Setup Debug Office Trophy")
				ADD_WIDGET_BOOL("Set debug buy mission count for trophy", debug_vars.bSetDebugBuyMissionCount)
				ADD_WIDGET_INT_SLIDER("Debug buy mission trophy level", debug_vars.iBuyMissionCompleteCount, 1, 5, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Set Contraband Type")
				ADD_WIDGET_BOOL("Medical Supplies", debug_vars.bContraTypeMeds)
				ADD_WIDGET_BOOL("Tobacco & Alcahol", debug_vars.bContraTypeTobacco)
				ADD_WIDGET_BOOL("Art", debug_vars.bContraTypeArt)
				ADD_WIDGET_BOOL("Electronics", debug_vars.bContraTypeElectronics)
				ADD_WIDGET_BOOL("Weapons & Ammo", debug_vars.bContraTypeWeapons)
				ADD_WIDGET_BOOL("Narcotics", debug_vars.bContraTypeNarcotics)
				ADD_WIDGET_BOOL("Gems", debug_vars.bContraTypeGems)
				ADD_WIDGET_BOOL("Animal Materials", debug_vars.bContraTypeWildlife)
				ADD_WIDGET_BOOL("Entertainment", debug_vars.bContraTypeEntertainment)
				ADD_WIDGET_BOOL("Jewls", debug_vars.bContraTypeJewelry)
				ADD_WIDGET_BOOL("Bullion", debug_vars.bContraTypeBullion)
				ADD_WIDGET_STRING("")
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Set Special Item")
				ADD_WIDGET_BOOL("Golden Egg", debug_vars.bContraTypeGoldenEgg)
				ADD_WIDGET_BOOL("Golden Minigun", debug_vars.bContraTypeGoldenMiniG)
				ADD_WIDGET_BOOL("Jewel", debug_vars.bContraTypeJewel)
				ADD_WIDGET_BOOL("Sasquatch Hide", debug_vars.bContraTypeSasquatch)
				ADD_WIDGET_BOOL("Film Reel", debug_vars.bContraTypeFilmReel)
				ADD_WIDGET_BOOL("Watch", debug_vars.bContraTypeTigerHide)
				ADD_WIDGET_STRING("")
				ADD_WIDGET_BOOL("Disable Special Item", debug_vars.bDisableSpecialItem)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Contraband Type Refresh")
				ADD_WIDGET_BOOL("1. Display Timer info", debug_vars.bdrawDebugForBGScript)
				ADD_WIDGET_INT_SLIDER("2. Contraband refresh cooldown (Seconds)", debug_vars.iTypeRefreshCD, 0, 5000, 1)
				ADD_WIDGET_BOOL("3. Set cooldown time", debug_vars.bSetCDTime)
				ADD_WIDGET_BOOL("4. Randomize contraband types", debug_vars.bRandomizeContraTypes)
				ADD_WIDGET_BOOL("5. Test Random weighting", debug_vars.bTestRandomTypes)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
ENDPROC

FUNC STRING DEBUG_GET_ILLICIT_GOOD_TYPE_LITERAL_STRING(FACTORY_TYPE eIllicitGoodsType)
	SWITCH eIllicitGoodsType 	
		CASE FACTORY_TYPE_FAKE_IDS		RETURN "Fake IDs"
		CASE FACTORY_TYPE_WEED			RETURN "Weed"
		CASE FACTORY_TYPE_FAKE_MONEY 	RETURN "Fake Money"
		CASE FACTORY_TYPE_METH 			RETURN "Meth"
		CASE FACTORY_TYPE_CRACK 			RETURN "Crack"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC DEBUG_SET_CONTRABAND_SPECIAL_ITEM(WAREHOUSE_DEBUG_WIDGET_VARS &debug_vars)
	IF debug_vars.bContraTypeGoldenEgg
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_ART_AND_ANTIQUES)
		debug_vars.bContraTypeGoldenEgg = FALSE
		debug_vars.bSpecialItem = TRUE
		debug_vars.bDisableSpecialItem = FALSE
		
	ELIF debug_vars.bContraTypeGoldenMiniG
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_WEAPONS_AND_AMMO)
		debug_vars.bContraTypeGoldenMiniG = FALSE
		debug_vars.bSpecialItem = TRUE
		debug_vars.bDisableSpecialItem = FALSE
		
	ELIF debug_vars.bContraTypeJewel
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_GEMS)
		debug_vars.bContraTypeJewel = FALSE
		debug_vars.bSpecialItem = TRUE
		debug_vars.bDisableSpecialItem = FALSE
		
	ELIF debug_vars.bContraTypeSasquatch
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_WILDLIFE)
		debug_vars.bContraTypeSasquatch = FALSE
		debug_vars.bSpecialItem = TRUE
		debug_vars.bDisableSpecialItem = FALSE
		
	ELIF debug_vars.bContraTypeFilmReel
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_ENTERTAINMENT)
		debug_vars.bContraTypeFilmReel = FALSE
		debug_vars.bSpecialItem = TRUE
		debug_vars.bDisableSpecialItem = FALSE
		
	ELIF debug_vars.bContraTypeTigerHide
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_JEWELRY)
		debug_vars.bContraTypeTigerHide = FALSE
		debug_vars.bSpecialItem = TRUE
		debug_vars.bDisableSpecialItem = FALSE
	ENDIF
	
	IF debug_vars.bDisableSpecialItem
		debug_vars.bSpecialItem = FALSE
	ENDIF
ENDPROC

PROC DEBUG_SET_CONTRABAND_TYPE(WAREHOUSE_DEBUG_WIDGET_VARS &debug_vars)
	IF debug_vars.bContraTypeMeds
		debug_vars.bContraTypeMeds = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_MEDICAL_SUPPLIES)
		
	ELIF debug_vars.bContraTypeTobacco
		debug_vars.bContraTypeTobacco = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL)
		
	ELIF debug_vars.bContraTypeArt
		debug_vars.bContraTypeArt = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_ART_AND_ANTIQUES)
		
	ELIF debug_vars.bContraTypeElectronics
		debug_vars.bContraTypeElectronics = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_ELECTRONICS)
		
	ELIF debug_vars.bContraTypeWeapons
		debug_vars.bContraTypeWeapons = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_WEAPONS_AND_AMMO)
		
	ELIF debug_vars.bContraTypeNarcotics
		debug_vars.bContraTypeNarcotics = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_NARCOTICS)
		
	ELIF debug_vars.bContraTypeGems
		debug_vars.bContraTypeGems = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_GEMS)
		
	ELIF debug_vars.bContraTypeWildlife
		debug_vars.bContraTypeWildlife = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_WILDLIFE)
		
	ELIF debug_vars.bContraTypeEntertainment
		debug_vars.bContraTypeEntertainment = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_ENTERTAINMENT)
		
	ELIF debug_vars.bContraTypeJewelry
		debug_vars.bContraTypeJewelry = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_JEWELRY)
		
	ELIF debug_vars.bContraTypeBullion
		debug_vars.bContraTypeBullion = FALSE
		debug_vars.iContrabandType = ENUM_TO_INT(CONTRABAND_TYPE_BULLION)
	ENDIF
ENDPROC

PROC DRAW_DEBUG_INFO_FOR_CONTRABAND_REFRESH()
	FLOAT fPS4Spacing = 0.0
	
	IF NOT USE_SERVER_TRANSACTIONS()
		fPS4Spacing = 0.013
	ENDIF
	
	//Background rectangle
    DRAW_RECT((0.02 + fPS4Spacing), (0.355 + fPS4Spacing), 0.41, 0.29, 0, 0, 0, 200)
    SET_TEXT_SCALE((0.4 - fPS4Spacing), (0.4 - fPS4Spacing))
	SET_TEXT_COLOUR(0, 255, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.04 + fPS4Spacing), (0.21 + fPS4Spacing), "STRING", "Contraband Refresh Info")
    
	//Fremode Timer	
	INT iTimeRemaining = g_sMPTunables.iexec_contraband_type_refresh_time - (GET_CLOUD_TIME_AS_INT() - g_iContrabandRefreshTime)
	
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), 0.24, "STRING", "Freemode timer: ")
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_NUMBER((0.14 + fPS4Spacing), 0.24, "NUMBER", iTimeRemaining)
	
	//Freemode Contraband info
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), 0.26, "STRING", "Freemode contraband type:")
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.14 + fPS4Spacing), 0.26, "STRING", DEBUG_GET_CONTRABAND_TYPE_LITERAL_STRING(g_eContrabandType))
	
	//Cooldown Time
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), 0.28, "STRING", "Total cooldown time: ")
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_NUMBER((0.14 + fPS4Spacing), 0.28, "NUMBER", g_sMPTunables.iexec_contraband_type_refresh_time)
	
	//Contraband type history
	INT iLoop
	FLOAT fYSpacing = 0.02
	
	SET_TEXT_SCALE(0.4, 0.4)
	SET_TEXT_COLOUR(255, 0, 0, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.04 + fPS4Spacing), 0.305, "STRING", "Contraband Type History")
	
	REPEAT ciContrabandTypeHistoryListSize iLoop
		
		FLOAT fYPos = ((iLoop + 1) * fYSpacing) + 0.315
	
		SET_TEXT_SCALE(0.3, 0.3)
		DISPLAY_TEXT_WITH_NUMBER((0.01 + fPS4Spacing), fYPos, "NUMBER", iLoop)
		SET_TEXT_SCALE(0.3, 0.3)
		DISPLAY_TEXT_WITH_LITERAL_STRING((0.02 + fPS4Spacing), fYPos, "STRING", DEBUG_GET_CONTRABAND_TYPE_LITERAL_STRING(INT_TO_ENUM(CONTRABAND_TYPE, iContrabandTypesHistoryList[iLoop])))
	ENDREPEAT
	
	//Next item in the history list to be updated
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), 0.475, "STRING", "History list position: ")
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_NUMBER((0.1 + fPS4Spacing), 0.475, "NUMBER", iContrabanTypRandomPos)
ENDPROC

PROC DISPLAY_WAREHOUSE_INFO()
	FLOAT fSpacing 		= 0.02
	FLOAT fPS4Spacing 	= 0.0
	INT iIter
	
	IF NOT USE_SERVER_TRANSACTIONS()
		fPS4Spacing = 0.013
	ENDIF
	
	//Background rectangle    
    DRAW_RECT((0.02 + fPS4Spacing), (0.03 + fPS4Spacing), 0.53, 0.35, 0, 0, 0, 200)
	SET_TEXT_SCALE((0.4 - fPS4Spacing), (0.4 - fPS4Spacing))
	SET_TEXT_COLOUR(0, 191, 255, 255)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), (0.007 + fPS4Spacing), "STRING", "Warehouse Info")
    
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.16 + fPS4Spacing), 0.029, "STRING", "Contraband:    Special:")
		
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		INT iWarehouse = GET_WAREHOUSE_ID_FOR_SAVE_SLOT(iIter)
				
		//Print slot info
		TEXT_LABEL_31 sWarehouse
		TEXT_LABEL_63 tlCurrentContrabandTotal
		TEXT_LABEL_15 tlCurrentSlot = "Slot "
		tlCurrentSlot += iIter
		
		FLOAT yPos = 0.03 + (TO_FLOAT(iIter + 1) * fSpacing)
		
		IF iWarehouse > ciW_Invalid
			//Prevent asserts when we don't own a warehouse in this slot
			sWarehouse = DEBUG_GET_STRING_FOR_WAREHOUSE(iWarehouse)
			sWarehouse += "("
			sWarehouse += iWarehouse
			sWarehouse += ")"
			tlCurrentContrabandTotal 	= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)
			tlCurrentContrabandTotal	+= " / "
			tlCurrentContrabandTotal	+= GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
			tlCurrentContrabandTotal 	+= " Units"
			tlCurrentContrabandTotal 	+= "       "
			tlCurrentContrabandTotal	+= GET_COUNT_OF_SPECIAL_ITEMS_IN_WAREHOUSE(iWarehouse)
			tlCurrentContrabandTotal	+= " / "
			tlCurrentContrabandTotal	+= GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse)
		ELSE
			sWarehouse = DEBUG_GET_STRING_FOR_WAREHOUSE(iWarehouse)
			tlCurrentContrabandTotal = "0 Units"
		ENDIF
		
	    SET_TEXT_SCALE(0.3, 0.3)     
		DISPLAY_TEXT_WITH_LITERAL_STRING((0.007 + fPS4Spacing), yPos, "STRING", tlCurrentSlot)
		SET_TEXT_SCALE(0.3, 0.3)     
		DISPLAY_TEXT_WITH_LITERAL_STRING((0.04 + fPS4Spacing), yPos, "STRING", sWarehouse)
		
		SET_TEXT_SCALE(0.3, 0.3)
		IF iWarehouse > ciW_Invalid
		AND IS_WAREHOUSE_FULL(iWarehouse)
			SET_TEXT_COLOUR(255, 0, 0, 255)
		ENDIF
		DISPLAY_TEXT_WITH_LITERAL_STRING((0.16 + fPS4Spacing), yPos, "STRING", tlCurrentContrabandTotal)
	ENDFOR	
	TEXT_LABEL_31 tlCurrentContaItem = "Active item: "
	STRING tlI =  DEBUG_GET_CONTRABAND_TYPE_LITERAL_STRING(g_eContrabandType)
	tlCurrentContaItem += tlI
	SET_TEXT_SCALE(0.3, 0.3)
	DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), 0.16, "STRING", tlCurrentContaItem)
	
	IF IS_SPECIAL_CONTRABAND_ITEM_ACTIVE()
		TEXT_LABEL_31 tlCurrentSpecItem = "Special Item Active"
		SET_TEXT_SCALE(0.3, 0.3)
		DISPLAY_TEXT_WITH_LITERAL_STRING((0.01 + fPS4Spacing), 0.18, "STRING", tlCurrentSpecItem)
	ENDIF
ENDPROC

FUNC BOOL DEBUG_BUY_MP_CONTRABAND_MISSION(INT iWarehouseID, CONTRABAND_SIZE ShipmentSize, INT &iProcessingBasketStage, BOOL &bProcessingBasket, INT iShipmnetSize, CONTRABAND_TYPE securoContraType)
	
	TEXT_LABEL_15 tl_15ContrabandName 	= GET_WAREHOUSE_NAME(iWarehouseID)
	INT PropVal							= GET_CONTRABAND_SHIPMENT_COST(ShipmentSize)
	INT iResultSlot 					= GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouseID)
	
	IF USE_SERVER_TRANSACTIONS()
	
		/*
		BuyContrabandMission – 
                This is used to fill/set the CATEGORY_INVENTORY_CONTRABAND_MISSION
                It deducts money for the ‘purchasing’ of the mission
                Basket will be < CATEGORY_INVENTORY_CONTRABAND_MISSION, CATEGORY_CONTRABAND_MISSION, price>
                Validation must make sure the player owns the warehouse this CATEGORY_INVENTORY_CONTRABAND_MISSION is associated with

		*/
		IF NOT g_bBuyTransactionSetup
			bProcessingBasket 		= TRUE
			iProcessingBasketStage 	= SHOP_BASKET_STAGE_ADD
			g_bBuyTransactionSetup 	= TRUE
		ENDIF
		
		INT iItemId 			= GET_CONTRABAND_MISSION_KEY_FOR_CATALOGUE(TRUE, ENUM_TO_INT(iShipmnetSize), FALSE, ENUM_TO_INT(GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(securoContraType)))
		INT iInventoryKey 		= GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouseID))
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 tlLastClickedCONTRABANDIndex = tl_15ContrabandName
		IF IS_STRING_NULL_OR_EMPTY(tlLastClickedCONTRABANDIndex)
			CASSERTLN(DEBUG_INTERNET, "<SECURO> DEBUG_BUY_MP_CONTRABAND_MISSION: label for last clicked CONTRABAND index #", iWarehouseID, " is empty!")
		ENDIF
		#ENDIF
		
		INT iProcessSuccess = -1
		
		SWITCH iProcessingBasketStage
			// Add item to basket
			CASE SHOP_BASKET_STAGE_ADD
				
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_CONTRABAND_MISSION, iItemId, NET_SHOP_ACTION_BUY_CONTRABAND_MISSION, 1, propval, iResultSlot, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
					IF iProcessingBasketStage != SHOP_BASKET_STAGE_FAILED
						IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket checkout started")								
							iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to start basket checkout")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to add item to basket")
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
				ENDIF
				
				RETURN FALSE
			BREAK
			// Pending
			CASE SHOP_BASKET_STAGE_PENDING
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, success!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, failed!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					ENDIF
				ENDIF
				
				RETURN FALSE
			BREAK
			
			// Success
			CASE SHOP_BASKET_STAGE_SUCCESS
				bProcessingBasket = FALSE
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD				
				
				IF USE_SERVER_TRANSACTIONS()
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF

				iProcessSuccess = 2
			BREAK
			
			//Failed
			CASE SHOP_BASKET_STAGE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				bProcessingBasket = FALSE
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				IF USE_SERVER_TRANSACTIONS()
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
				
				iProcessSuccess = 0
			BREAK
		ENDSWITCH
		
		SWITCH iProcessSuccess
			CASE 2
				CPRINTLN(DEBUG_INTERNET, "<SECURO> DEBUG_BUY_MP_CONTRABAND_MISSION: success!!")
				
				// Track this so we can add contraband if the mision was successful.
				SET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY(iItemId)
			BREAK
			DEFAULT
				CDEBUG1LN(DEBUG_INTERNET, "<SECURO> DEBUG_BUY_MP_CONTRABAND_MISSION: unknown iProcessSuccess: \"", iProcessSuccess, "\"")
			BREAK
		ENDSWITCH
	ELSE
		CPRINTLN(DEBUG_INTERNET, "<SECURO> DEBUG_BUY_MP_CONTRABAND_MISSION: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
	ENDIF	
	
	RETURN TRUE
ENDFUNC

PROC UPDATE_PRIVATE_WAREHOUSE_WIDGETS(WAREHOUSE_DEBUG_WIDGET_VARS &debug_vars)

	BOOL bDebugWarehouseButtonPressed = IS_DEBUG_KEY_JUST_PRESSED(KEY_W, KEYBOARD_MODIFIER_SHIFT, "Warehouses")

	IF debug_vars.bPurchaseWarehouse
	OR bDebugWarehouseButtonPressed
		IF GET_COUNT_OF_WAREHOUSES_OWNED() < ciMaxOwnedWarehouses
			INT iWarehouseToBuy = (debug_vars.iWarehouseToPurchaseOrSell)
			
			//Find a random warehouse we don't already own
			IF debug_vars.iWarehouseToPurchaseOrSell = ciW_Invalid
			OR bDebugWarehouseButtonPressed
				BOOL bFoundWHouseNotOwned = FALSE
				WHILE bFoundWHouseNotOwned = FALSE
					iWarehouseToBuy = GET_RANDOM_INT_IN_RANGE(1, ciMaxWarehouses)
					IF NOT DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouseToBuy)
						bFoundWHouseNotOwned = TRUE
					ENDIF
				ENDWHILE
			ENDIF
			
			IF NOT DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouseToBuy)
				SET_WAREHOUSE_AS_PURCHASED(iWarehouseToBuy)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug buy called for warehouse ", debug_vars.iWarehouseToPurchaseOrSell)
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug buy called for warehouse FAILED. player already owns: ", debug_vars.iWarehouseToPurchaseOrSell)
			ENDIF
			
			IF bDebugWarehouseButtonPressed
				CONTRABAND_TRANSACTION_STATE eResult
				IF g_bSpecialItemActive
					ADD_CONTRABAND_TO_WAREHOUSE(iWarehouseToBuy, ENUM_TO_INT(CONTRABAND_SMALL), ENUM_TO_INT(CONTRABAND_SMALL), eResult, g_eContrabandType)
				ELSE
					ADD_CONTRABAND_TO_WAREHOUSE(iWarehouseToBuy, ENUM_TO_INT(CONTRABAND_SMALL), 0, eResult, g_eContrabandType)
				ENDIF
			ENDIF
		ENDIF
		
		debug_vars.bPurchaseWarehouse = FALSE
	ENDIF
	
	IF debug_vars.bSellWarehouse
		IF DOES_LOCAL_PLAYER_OWN_A_WAREHOUSE()
			INT iWarehouseToSell = (debug_vars.iWarehouseToPurchaseOrSell)
			IF iWarehouseToSell = ciW_Invalid
				iWarehouseToSell = GET_WAREHOUSE_ID_FOR_SAVE_SLOT((GET_COUNT_OF_WAREHOUSES_OWNED() - 1))
			ENDIF
			
			IF iWarehouseToSell != ciW_Invalid
			AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(iWarehouseToSell)
				REMOVE_WAREHOUSE(iWarehouseToSell)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug sell called for warehouse ", iWarehouseToSell)
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug sell called for warehouse FAILED. player does not own warehouse: ", iWarehouseToSell)
			ENDIF
		ENDIF
		debug_vars.bSellWarehouse = FALSE
	ENDIF
	
	IF debug_vars.bWarpToCoords
		IF debug_vars.iWarehouseToPurchaseOrSell != ciW_Invalid
			NET_WARP_TO_COORD(GET_WAREHOUSE_COORDS(debug_vars.iWarehouseToPurchaseOrSell), 100)
		ENDIF
		debug_vars.bWarpToCoords = FALSE
	ENDIF
	
	IF debug_vars.bAddContraband
		IF debug_vars.iContrabandToAddOrRemove > 0
		AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(debug_vars.iWarehouseToAddOrRemoveContrabandFrom)
		
			IF NOT g_bDebugMissionBuyComplete
				IF DEBUG_BUY_MP_CONTRABAND_MISSION(debug_vars.iWarehouseToAddOrRemoveContrabandFrom, INT_TO_ENUM(CONTRABAND_SIZE, debug_vars.iContrabandToAddOrRemove), g_iDebugProcessingBasketStage, g_bDebugProcessingTransaction, debug_vars.iContrabandToAddOrRemove, g_eContrabandType)
					g_bDebugMissionBuyComplete = TRUE
				ENDIF
			ELSE
				DEBUG_SET_CONTRABAND_TYPE(debug_vars)
				DEBUG_SET_CONTRABAND_SPECIAL_ITEM(debug_vars)
				IF debug_vars.bSpecialItem
					ADD_CONTRABAND_TO_WAREHOUSE(debug_vars.iWarehouseToAddOrRemoveContrabandFrom, debug_vars.iContrabandToAddOrRemove, debug_vars.iContrabandToAddOrRemove, g_ContaTransactionResult, INT_TO_ENUM(CONTRABAND_TYPE, debug_vars.iContrabandType))
				ELSE
					ADD_CONTRABAND_TO_WAREHOUSE(debug_vars.iWarehouseToAddOrRemoveContrabandFrom, debug_vars.iContrabandToAddOrRemove, 0, g_ContaTransactionResult, INT_TO_ENUM(CONTRABAND_TYPE, debug_vars.iContrabandType))
				ENDIF
				
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug add contraband to warehouse ", debug_vars.iWarehouseToAddOrRemoveContrabandFrom)
				IF g_ContaTransactionResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
				OR g_ContaTransactionResult = CONTRABAND_TRANSACTION_STATE_FAILED
					debug_vars.bAddContraband 		= FALSE
					g_bDebugMissionBuyComplete 		= FALSE
					g_bDebugProcessingTransaction 	= FALSE
					g_bBuyTransactionSetup			= FALSE
					g_iDebugProcessingBasketStage	= SHOP_BASKET_STAGE_ADD
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug add contraband to warehouse FAILED player does not own: ", debug_vars.iWarehouseToAddOrRemoveContrabandFrom)
			debug_vars.bAddContraband = FALSE
			debug_vars.bSpecialItem = FALSE
		ENDIF
		debug_vars.bDisableSpecialItem = TRUE
		//debug_vars.bSpecialItem = FALSE
	ENDIF
	
	IF debug_vars.bRemoveContraband
		IF debug_vars.iContrabandToAddOrRemove > 0
		AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(debug_vars.iWarehouseToAddOrRemoveContrabandFrom)
			REMOVE_CONTRABAND_FROM_WAREHOUSE(debug_vars.iWarehouseToAddOrRemoveContrabandFrom, debug_vars.iContrabandToAddOrRemove, 0, REMOVE_CONTRA_ATTACKED, g_ContaTransactionResult)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug remove contraband from warehouse ", debug_vars.iWarehouseToAddOrRemoveContrabandFrom)
			IF g_ContaTransactionResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
			OR g_ContaTransactionResult = CONTRABAND_TRANSACTION_STATE_FAILED
				debug_vars.bRemoveContraband = FALSE
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] UPDATE_PRIVATE_WAREHOUSE_WIDGETS: Debug remove contraband from warehouse FAILED player does not own: ", debug_vars.iWarehouseToAddOrRemoveContrabandFrom)
			debug_vars.bRemoveContraband = FALSE
		ENDIF
	ENDIF
	
	BOOL bDrawInfo
	IF bDebugWarehouseButtonPressed
		IF NOT HAS_NET_TIMER_STARTED(debug_vars.stTimerDisplay)
			START_NET_TIMER(debug_vars.stTimerDisplay)
		ELSE
			REINIT_NET_TIMER(debug_vars.stTimerDisplay)
		ENDIF
	ENDIF
	IF HAS_NET_TIMER_STARTED(debug_vars.stTimerDisplay)
		IF NOT HAS_NET_TIMER_EXPIRED(debug_vars.stTimerDisplay, 5000)
			bDrawInfo = TRUE
		ENDIF
	ENDIF
	
	IF debug_vars.bDisplayWarehouseInfo
	OR bDrawInfo
		DISPLAY_WAREHOUSE_INFO()
	ENDIF
	
	IF debug_vars.bClearAllSlots
		CLEAR_ALL_WAREHOUSE_SLOTS(TRUE)				
		debug_vars.bClearAllSlots = FALSE
	ENDIF
	
	IF debug_vars.bTradeIn
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(0))
		TRADE_IN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(0), (debug_vars.iWarehouseToPurchaseOrSell))
		debug_vars.bTradeIn = FALSE
	ENDIF
	
	IF debug_vars.bTradeInSlot1
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(1))
		TRADE_IN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(1), (debug_vars.iWarehouseToPurchaseOrSell))		
		debug_vars.bTradeInSlot1 = FALSE
	ENDIF
	
	IF debug_vars.bTradeInSlot2
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(2))
		TRADE_IN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(2), (debug_vars.iWarehouseToPurchaseOrSell))		
		debug_vars.bTradeInSlot2 = FALSE
	ENDIF
	
	IF debug_vars.bTradeInSlot3
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(3))
			TRADE_IN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(3), (debug_vars.iWarehouseToPurchaseOrSell))
		debug_vars.bTradeInSlot3 = FALSE
	ENDIF
	
	IF debug_vars.bTradeInSlot4
	AND DOES_LOCAL_PLAYER_OWN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(4))
		TRADE_IN_WAREHOUSE(GET_WAREHOUSE_ID_FOR_SAVE_SLOT(4), (debug_vars.iWarehouseToPurchaseOrSell))
		debug_vars.bTradeInSlot4 = FALSE
	ENDIF
	
	IF debug_vars.bRandomizeContraTypes
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES Called by debug")
		RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES()
		debug_vars.bRandomizeContraTypes = FALSE
	ENDIF
	
	IF debug_vars.bTestRandomTypes
		INT iTest 			= 0	
		INT i				= 0
		INT iSpecialCount 	= 0
		INT iRuns 			= 1000
		INT iTemp			= g_MP_STAT_NUM_BUY_MISSIONS_COMPLETED[GET_ACTIVE_CHARACTER_SLOT()]
		INT iTypePrint[11]
		
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_NUMBUYMISSIONSCOMPLETED, 11)
		
		FOR iTest = 0 TO iRuns
			RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES()
			iTypePrint[ENUM_TO_INT(g_eContrabandType)] ++
			IF IS_SPECIAL_CONTRABAND_ITEM_ACTIVE()
				iSpecialCount ++
				TEXT_LABEL_23 tlSpecItem = GET_WAREHOUSE_CONTRABAND_SPECIAL_ITEM_LABEL(GET_ACTIVE_SPECIAL_ITEM())
				TEXT_LABEL_23 tlSpecType = GET_WAREHOUSE_CONTRABAND_TYPE_LABEL(g_eContrabandType)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CTYPE_TEST] Special item label: ", tlSpecItem, " Type: ", tlSpecType)
			ENDIF
		ENDFOR
		
		REPEAT 11 i
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CTYPE_TEST] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPE: ", i, " = ", iTypePrint[i])
		ENDREPEAT
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CTYPE_TEST] AFTER:", iRuns, " runs. Special item count: ", iSpecialCount)
		SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_NUMBUYMISSIONSCOMPLETED, iTemp)
		debug_vars.bTestRandomTypes = FALSE
	ENDIF
	
	IF debug_vars.bSetSpecialItem
		IF debug_vars.iSpecialItemType = 1
			g_eContrabandType = CONTRABAND_TYPE_ART_AND_ANTIQUES
		ELIF debug_vars.iSpecialItemType = 2
			g_eContrabandType = CONTRABAND_TYPE_WEAPONS_AND_AMMO
		ELIF debug_vars.iSpecialItemType = 3
			g_eContrabandType = CONTRABAND_TYPE_GEMS
		ELIF debug_vars.iSpecialItemType = 4
			g_eContrabandType = CONTRABAND_TYPE_WILDLIFE
		ELIF debug_vars.iSpecialItemType = 5
			g_eContrabandType = CONTRABAND_TYPE_ENTERTAINMENT
		ELIF debug_vars.iSpecialItemType = 6
			g_eContrabandType = CONTRABAND_TYPE_JEWELRY
		ENDIF
		
		GB_SET_LOCAL_HELP_BIT(eGB_LOCAL_HELP_BITSET_NOTIFICATION_SPECIAL_AVALIABLE)
		g_bSpecialItemActive = TRUE
		debug_vars.bSetSpecialItem = FALSE
	ENDIF
	
	IF debug_vars.bSetDebugBuyMissionCount
		SWITCH debug_vars.iBuyMissionCompleteCount
			CASE 1	g_iDebugBuyMissionsComplete = 51	BREAK
			CASE 2	g_iDebugBuyMissionsComplete = 101	BREAK
			CASE 3	g_iDebugBuyMissionsComplete = 251	BREAK
			CASE 4	g_iDebugBuyMissionsComplete = 501	BREAK
			CASE 5	g_iDebugBuyMissionsComplete = 1001	BREAK
			//CASE 6	g_iDebugBuyMissionsComplete = 5001	BREAK
		ENDSWITCH
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "Setting debug buy mission count to ", g_iDebugBuyMissionsComplete)
		
		debug_vars.bSetDebugBuyMissionCount = FALSE
	ENDIF
	IF debug_vars.bSetCDTime
		g_sMPTunables.iexec_contraband_type_refresh_time = debug_vars.iTypeRefreshCD
		debug_vars.bSetCDTime = FALSE
	ENDIF
	
	IF debug_vars.bdrawDebugForBGScript
		DRAW_DEBUG_INFO_FOR_CONTRABAND_REFRESH()
	ENDIF
ENDPROC

#ENDIF //IF DEBUG

FUNC BOOL ARE_SECUROSERV_BUY_MISSIONS_DISABLED()
	RETURN g_sMPTunables.bexec_disable_buy_missions
ENDFUNC

FUNC BOOL PROCESSING_SECUROSERVE_CONTRABAND_BASKET(INT &iProcessSuccess, INT iPrice, INT iStatValue, SHOP_ITEM_CATEGORIES eCategory, ITEM_ACTION_TYPES eAction, INT iItemId, INT iInventoryKey, BOOL &bProcessingBasket, INT &iProcessingBasketStage)
	IF bProcessingBasket
		SWITCH iProcessingBasketStage
			// Add item to basket
			CASE SHOP_BASKET_STAGE_ADD
				IF NOT NETWORK_CAN_SPEND_MONEY(iPrice, FALSE, TRUE, FALSE)
					CASSERTLN(DEBUG_INTERNET, "[BASKET] PROCESSING_SECUROSERVE_CONTRABAND_BASKET - Player can't afford this item! iPrice= $", iPrice)
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					RETURN TRUE
				ENDIF
				
				CPRINTLN(DEBUG_INTERNET, "[BASKET] - Adding contraband mission to basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", iPrice:$", iPrice, ", iItemId:", iItemId, ", iInventoryKey:", iInventoryKey)
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, iItemId, eAction, 1, iPrice, iStatValue, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
					IF iProcessingBasketStage != SHOP_BASKET_STAGE_FAILED
						IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket checkout started")
							iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to start basket checkout")
							iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[BASKET] - failed to add item to basket")
					iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
				ENDIF
			BREAK
			// Pending
			CASE SHOP_BASKET_STAGE_PENDING
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, success!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[BASKET] - Basket transaction finished, failed!")
						iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					ENDIF
				ENDIF
			BREAK
			
			// Success
			CASE SHOP_BASKET_STAGE_SUCCESS
				bProcessingBasket = FALSE
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 2
				RETURN FALSE
			BREAK
			
			//Failed
			CASE SHOP_BASKET_STAGE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				bProcessingBasket = FALSE
				iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = 0
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF

	IF iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
		CPRINTLN(DEBUG_INTERNET, "[BASKET] - was FORCE_CLOSE_BROWSER called to termiate the transaction? Set success to 0")
		
		iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
		iProcessSuccess = 0
		RETURN FALSE
	ENDIF

	iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	iProcessSuccess = -1
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Special version of BUY_SPECIAL_CARGO_MISSION specifically for awarding items to the player via the casino lucky wheel
FUNC BOOL ADD_SPECIAL_CARGO_MISSION_TO_INVENTORY(INT &iTransactionState, INT iWarehouseSlot)
		
	IF USE_SERVER_TRANSACTIONS()
	
		INT iItemId 				= HASH("CB_BUY_4_t0_v0")
		INT iInventoryKey 			= GET_CONTRABAND_MISSION_INVENTORY_KEY_FOR_CATALOGUE(iWarehouseSlot)		
		INT iTransactionReturnValue = -1
		BOOL bProcessingBasket 		= TRUE
			
		///////////////////////////////////////////
		///   TRANSACTION FOR CONTRABAND
		IF PROCESSING_SECUROSERVE_CONTRABAND_BASKET(iTransactionReturnValue, 0, iWarehouseSlot, CATEGORY_INVENTORY_CONTRABAND_MISSION, NET_SHOP_ACTION_BUY_CONTRABAND_MISSION, iItemId, iInventoryKey, bProcessingBasket, iTransactionState)
		
			SWITCH iTransactionReturnValue
				CASE 0
					CWARNINGLN(DEBUG_INTERNET, "[LW_AWARD] ADD_SPECIAL_CARGO_MISSION_TO_INVENTORY: failed to process transaction")				
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				BREAK
				CASE 2
					CPRINTLN(DEBUG_INTERNET, "[LW_AWARD] ADD_SPECIAL_CARGO_MISSION_TO_INVENTORY: success!!")				
					// Track this so we can add contraband if the mision was successful.
					SET_LAST_PURCHASED_BUY_MISSION_CATALOGUE_KEY(iItemId)
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				BREAK
			ENDSWITCH
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	 
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE()
	IF g_iSpecialItemToAwardPlayer != -1
		SWITCH g_eContrabandAwardState
			CASE ACSIS_IDLE
				g_iSpecialItemToAwardToWHSlot = GET_WAREHOUSE_SAVE_SLOT_OF_FIRST_WH_WITH_SPACE()
				
				IF g_iSpecialItemToAwardToWHSlot = -1
					SCRIPT_ASSERT("MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Failed - No free warehouse space")
				ELIF DOES_PLAYER_HAVE_SPECIAL_ITEM(INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, g_iSpecialItemToAwardPlayer))
					SCRIPT_ASSERT("MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Failed - player already has this item")
				ELIF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[g_iSpecialItemToAwardToWHSlot].iWarehouse = ciW_Invalid
					SCRIPT_ASSERT("MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Failed - Invalid warehouse")
				ELSE
					PRINTLN("[LW_AWARD] MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Adding item ", g_iSpecialItemToAwardPlayer, " to slot ", g_iSpecialItemToAwardToWHSlot)
					IF USE_SERVER_TRANSACTIONS()
						g_eContrabandAwardState = ACSIS_ADD_MISSION_TO_INVENTRY
					ELSE
						g_eContrabandAwardState = ACSIS_ADD_CONTRABAND_ITEM
					ENDIF
				ENDIF
			BREAK
			CASE ACSIS_ADD_MISSION_TO_INVENTRY
				IF ADD_SPECIAL_CARGO_MISSION_TO_INVENTORY(g_iSpecialItemLWTransactionStage, g_iSpecialItemToAwardToWHSlot)
					IF g_iSpecialItemLWTransactionStage = SHOP_BASKET_STAGE_SUCCESS
						PRINTLN("[LW_AWARD] MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Mission transaction sucess - Moving on")
						g_eContrabandAwardState 			= ACSIS_ADD_CONTRABAND_ITEM
						g_iSpecialItemLWTransactionStage 	= ENUM_TO_INT(CONTRABAND_TRANSACTION_STATE_DEFAULT)
					ELIF g_iSpecialItemLWTransactionStage = SHOP_BASKET_STAGE_FAILED
						PRINTLN("[LW_AWARD] MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Mission transaction Failed - Cleanup")
						g_eContrabandAwardState = ACSIS_CLEANUP
					ENDIF
				ENDIF
			BREAK
			CASE ACSIS_ADD_CONTRABAND_ITEM
				INT iWarehouseID
				CONTRABAND_TRANSACTION_STATE eTransactionState
				CONTRABAND_SPECIAL_ITEM eSpecialItem
				
				iWarehouseID 		= GET_WAREHOUSE_ID_FOR_SAVE_SLOT(g_iSpecialItemToAwardToWHSlot)
				eSpecialItem		= INT_TO_ENUM(CONTRABAND_SPECIAL_ITEM, g_iSpecialItemToAwardPlayer)
				eTransactionState 	= INT_TO_ENUM(CONTRABAND_TRANSACTION_STATE, g_iSpecialItemLWTransactionStage)
				
				ADD_CONTRABAND_TO_WAREHOUSE(iWarehouseID, 1, 1, eTransactionState, GET_CONTRABAND_TYPE_FROM_SPECIAL_ITEM(eSpecialItem))
				
				IF eTransactionState = CONTRABAND_TRANSACTION_STATE_SUCCESS
					PRINTLN("[LW_AWARD] MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Contraband transaction sucess - Cleanup")
					g_eContrabandAwardState = ACSIS_CLEANUP
				ELIF eTransactionState = CONTRABAND_TRANSACTION_STATE_FAILED
					PRINTLN("[LW_AWARD] MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Contraband transaction failed - Cleanup")
					g_eContrabandAwardState = ACSIS_CLEANUP
				ELSE
					g_iSpecialItemLWTransactionStage = ENUM_TO_INT(g_iSpecialItemLWTransactionStage)
				ENDIF
			BREAK
			CASE ACSIS_CLEANUP
				g_eContrabandAwardState 			= ACSIS_IDLE
				g_iSpecialItemToAwardPlayer 		= -1
				g_iSpecialItemToAwardToWHSlot		= -1
				g_iSpecialItemLWTransactionStage 	= 0 
				PRINTLN("[LW_AWARD] MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Cleanup done")
			BREAK
		ENDSWITCH
	ELIF g_eContrabandAwardState != ACSIS_IDLE
		SCRIPT_ASSERT("MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE - Item var reset mid transaction!")
		g_eContrabandAwardState = ACSIS_IDLE
	ENDIF
ENDPROC

FUNC TEXT_LABEL_15 BUILD_BUY_MISSION_CONFIRM_STRING(CONTRABAND_TYPE contraType, CONTRABAND_SIZE eShipmentSize, BOOL bSpecialItemActive = FALSE)
	
	TEXT_LABEL_15 tlReturn = ""
	INT iContraType 	= ENUM_TO_INT(contraType)
	INT iSpecContraType = ENUM_TO_INT(GET_SPECIAL_ITEM_FROM_CONTRABAND_TYPE(contraType))
	INT iSize			= ENUM_TO_INT(eShipmentSize)

	IF bSpecialItemActive
	AND iSize = ENUM_TO_INT(CONTRABAND_SMALL)
		tlReturn = "WHOUSE_SHISI"
		tlReturn += iSpecContraType
	ELSE
		tlReturn = "WHOUSE_SHI"
		tlReturn += iSize
		tlReturn += "D"
		tlReturn += iContraType
	ENDIF
	
	RETURN tlReturn
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_VEH_UPGRADE_MENUS()
	
	IF IS_PHONE_ONSCREEN()
	OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
	OR MPGlobals.PlayerInteractionData.iTargetPlayerInt != -1 // interaction menu open
	OR g_bBrowserVisible = TRUE //browser open
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_CUTSCENE_ACTIVE()
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CONTRABAND_SELL
		RETURN FALSE
	ENDIF
	RETURN TRUE
	
ENDFUNC
#IF FEATURE_DLC_1_2022
FUNC BOOL SHOULD_CARGO_BE_DELIVERED_TO_WAREHOUSE(INT iWarehouse)
	INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	IF iSaveSlot != -1	
		RETURN g_sWarehouseCargoSourcingData.bRequestDeliverCargo[iSaveSlot]
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SHOULD_CARGO_BE_DELIVERED_TO_WAREHOUSE CARGO SHOULD BE DELIVERED TO:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ELSE
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SHOULD_CARGO_BE_DELIVERED_TO_WAREHOUSE FAILED  WAREHOUSE:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CARGO_BE_DELIVERED_TO_WAREHOUSE_AT_SAVESLOT(INT iSaveSlot)
	RETURN g_sWarehouseCargoSourcingData.bRequestDeliverCargo[iSaveSlot]	
ENDFUNC

PROC SET_BIT_FOR_CALL_FOR_WAREHOUSE(INT iSaveSlot, BOOL bFemale)
	SWITCH iSaveSlot
	CASE 0 //WAREHOUSE BOSS
		IF bFemale
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_BIT_FOR_CALL_FOR_WAREHOUSE  SLOT: ", iSaveSlot, " PLAYER FEMALE:", bFemale, " 	SETTING ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_2")
			SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_2) // (slot 0 and female player) 
		ELSE
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_BIT_FOR_CALL_FOR_WAREHOUSE  SLOT: ", iSaveSlot, " PLAYER FEMALE:", bFemale, " 	SETTING ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_1")
			SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_1) // (slot 0 and male player) 
		ENDIF
												
	BREAK
	
	CASE 1 //MALE WORKER
	CASE 3 //MALE WORKER
		IF bFemale
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_BIT_FOR_CALL_FOR_WAREHOUSE  SLOT: ", iSaveSlot, " PLAYER FEMALE:", bFemale, " 	SETTING ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_4")
			SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_4) // (slot 1 or 3 and female player)
		ELSE
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_BIT_FOR_CALL_FOR_WAREHOUSE  SLOT: ", iSaveSlot, " PLAYER FEMALE:", bFemale, " 	SETTING ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_3")
			SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_3) // (slot 1 or 3 and male player) 
		ENDIF
	BREAK
	
	CASE 2 //FEMALE WORKER
	CASE 4 //FEMALE WORKER
		IF bFemale
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_BIT_FOR_CALL_FOR_WAREHOUSE  SLOT: ", iSaveSlot, " PLAYER FEMALE:", bFemale, " 	SETTING ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_6")
			SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_6) // (slot 2 or 4 and female player)
		ELSE
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_BIT_FOR_CALL_FOR_WAREHOUSE  SLOT: ", iSaveSlot, " PLAYER FEMALE:", bFemale, " 	SETTING ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_5")
			SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_FIRST_ASSIGNMENT_COMPLETE_CALL_VAR_5) // (slot 2 or 4 and male player) 
		ENDIF
	BREAK
	ENDSWITCH
ENDPROC

FUNC STRING GET_CONTRABAND_NAME(CONTRABAND_TYPE eType)
	SWITCH eType
		CASE CONTRABAND_TYPE_MEDICAL_SUPPLIES 		RETURN "SRC_CRG_CT_MED"
		CASE CONTRABAND_TYPE_TOBACCO_AND_ALCAHOL 	RETURN "SRC_CRG_CT_TOB"
		CASE CONTRABAND_TYPE_ART_AND_ANTIQUES		RETURN "SRC_CRG_CT_ART"
		CASE CONTRABAND_TYPE_ELECTRONICS		 	RETURN "SRC_CRG_CT_ELE"
		CASE CONTRABAND_TYPE_WEAPONS_AND_AMMO 		RETURN "SRC_CRG_CT_WEA"
		CASE CONTRABAND_TYPE_NARCOTICS		 		RETURN "SRC_CRG_CT_NAR"
		CASE CONTRABAND_TYPE_GEMS					RETURN "SRC_CRG_CT_GEM"
		CASE CONTRABAND_TYPE_WILDLIFE		 		RETURN "SRC_CRG_CT_WIL"
		CASE CONTRABAND_TYPE_ENTERTAINMENT			RETURN "SRC_CRG_CT_ENT"
		CASE CONTRABAND_TYPE_JEWELRY			 	RETURN "SRC_CRG_CT_JEW"
		CASE CONTRABAND_TYPE_BULLION				RETURN "SRC_CRG_CT_BUL"
	ENDSWITCH
	
	RETURN "SRC_CRG_CT_INV"
ENDFUNC

FUNC STRING GET_SPECIAL_ITEM_NAME(INT iIndex)
	SWITCH iIndex
		CASE 0	RETURN "SRC_CRG_SP_0"
		CASE 1 	RETURN "SRC_CRG_SP_1"
		CASE 2	RETURN "SRC_CRG_SP_2"
		CASE 3	RETURN "SRC_CRG_SP_3"
		CASE 4	RETURN "SRC_CRG_SP_4"
		CASE 5	RETURN "SRC_CRG_SP_5"
	ENDSWITCH
	RETURN ""
ENDFUNC

PROC SET_SPECIAL_CARGO_WAREHOUSE_STAFF_HAS_BEEN_ASSIGNED(BOOL bSet)
	PRINTLN("SET_SPECIAL_CARGO_WAREHOUSE_STAFF_HAS_BEEN_ASSIGNED - setting to: ", bSet)
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ASSIGNED_WAREHOUSE_STAFF, bSet)
ENDPROC

PROC SET_CARGO_DELIVERED_TO_WAREHOUSE(INT iWarehouse)
	INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	IF iSaveSlot != -1
		
		IF  g_sWarehouseCargoSourcingData.iNumSpecialCargo = 0
			IF g_sWarehouseCargoSourcingData.iNumCargo = 1
				PRINT_TICKER_WITH_TWO_STRINGS("SRC_CRG_TICKER_1", GET_WAREHOUSE_NAME(iWarehouse), GET_CONTRABAND_NAME(g_sWarehouseCargoSourcingData.eType))
			ELSE
				PRINT_TICKER_WITH_TWO_STRINGS_AND_AN_INT("SRC_CRG_TICKER_2", GET_WAREHOUSE_NAME(iWarehouse), GET_CONTRABAND_NAME(g_sWarehouseCargoSourcingData.eType) , g_sWarehouseCargoSourcingData.iNumCargo)
			ENDIF	
		ELSE
			IF g_sWarehouseCargoSourcingData.iNumCargo = 1
				PRINT_TICKER_WITH_THREE_STRINGS("SRC_CRG_TICKER_11", GET_WAREHOUSE_NAME(iWarehouse), GET_CONTRABAND_NAME(g_sWarehouseCargoSourcingData.eType) ,GET_SPECIAL_ITEM_NAME(g_sWarehouseCargoSourcingData.iSpecialCargoType) )
			ELSE
				PRINT_TICKER_WITH_THREE_STRINGS_AND_AN_INT("SRC_CRG_TICKER_21", GET_WAREHOUSE_NAME(iWarehouse), GET_CONTRABAND_NAME(g_sWarehouseCargoSourcingData.eType), GET_SPECIAL_ITEM_NAME(g_sWarehouseCargoSourcingData.iSpecialCargoType) , g_sWarehouseCargoSourcingData.iNumCargo)
			ENDIF	
		ENDIF
		
		
	
		g_sWarehouseCargoSourcingData.bRequestDeliverCargo[iSaveSlot] = FALSE	
		g_sWarehouseCargoSourcingData.iNumSpecialCargo = -1
		g_sWarehouseCargoSourcingData.iSpecialCargoType = -1
		g_sWarehouseCargoSourcingData.iNumCargo = -1
		g_sWarehouseCargoSourcingData.eType = CONTRABAND_TYPE_INVALID
		
		//START A CALL FROM LUPE IF FIRST TIME 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_DONE_EXEC_FIRST_ASSIGNMENT_CALL)
			PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_CARGO_DELIVERED_TO_WAREHOUSE  CALLING SET_BIT_FOR_CALL_FOR_WAREHOUSE")
			SET_BIT_FOR_CALL_FOR_WAREHOUSE(iSaveSlot, IS_PLAYER_FEMALE())
		ENDIF
		
		SET_SPECIAL_CARGO_WAREHOUSE_STAFF_HAS_BEEN_ASSIGNED(TRUE)
		SET_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS(FALSE, iSaveSlot)
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_CARGO_DELIVERED_TO_WAREHOUSE CARGO SET AS DELIVERED TO:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ELSE
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_CARGO_DELIVERED_TO_WAREHOUSE FAILED  WAREHOUSE:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ENDIF
ENDPROC

PROC SET_CARGO_DELIVERED_TO_FULL_WAREHOUSE(INT iWarehouse)
	INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	IF iSaveSlot != -1	
		PRINT_TICKER_WITH_STRING("SRC_CRG_TICKER_F", GET_WAREHOUSE_NAME(iWarehouse))	
		
		g_sWarehouseCargoSourcingData.bRequestDeliverCargo[iSaveSlot] = FALSE	
		g_sWarehouseCargoSourcingData.iNumSpecialCargo = -1
		g_sWarehouseCargoSourcingData.iSpecialCargoType = -1
		g_sWarehouseCargoSourcingData.iNumCargo = -1
		g_sWarehouseCargoSourcingData.eType = CONTRABAND_TYPE_INVALID
				
		SET_SPECIAL_CARGO_WAREHOUSE_STAFF_HAS_BEEN_ASSIGNED(TRUE)
		SET_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS(FALSE, iSaveSlot)
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_CARGO_DELIVERED_TO_FULL_WAREHOUSE CARGO SET AS DELIVERED TO:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ELSE
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_CARGO_DELIVERED_TO_FULL_WAREHOUSE FAILED  WAREHOUSE:", iWarehouse, " SAVE SLOT:", iSaveSlot)
	ENDIF
ENDPROC

FUNC BOOL DID_PLAYER_REQUEST_CARGO_SOURCING()
	RETURN g_sWarehouseCargoSourcingData.bRequestCargoSourcing
ENDFUNC

PROC REQUEST_CARGO_SOURCING(BOOL bRequest)	
	PRINTLN("[WAREHOUSE][CARGO SOURCING] REQUEST_CARGO_SOURCING = ",bRequest)
	g_sWarehouseCargoSourcingData.bRequestCargoSourcing = bRequest
ENDPROC

PROC SEND_PED_TO_SOURCE_CARGO()
	REQUEST_CARGO_SOURCING(FALSE)
	g_sWarehouseCargoSourcingData.iBS = 0
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_CargoSourcingTime")
		g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_TIME = GET_COMMANDLINE_PARAM_INT("sc_CargoSourcingTime")
	ENDIF
	IF g_sDebugWarehouseCargoSourcingData.bUseWidgetValues
		g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_TIME = g_sDebugWarehouseCargoSourcingData.iCargoSourcingTime
	ENDIF
	#ENDIF
	INT iSaveSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
	SET_MP_INT_CHARACTER_STAT(GET_CARGO_SOURCING_POSIX_STAT_FOR_SAVE_SLOT(iSaveSlot), GET_CLOUD_TIME_AS_INT() + g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_TIME *60)
	SET_SOURCING_CARGO_PACKED_STAT(iSaveSlot, TRUE)
	IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, iSaveSlot)
		PRINTLN("[WAREHOUSE][CARGO SOURCING] SETTING BIT FOR PED WAREHOUSE save slot ", iSaveSlot)
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, iSaveSlot)
	ENDIF
	PRINT_TICKER("SRC_CRG_TICKER")
	
	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_DONE_EXEC_INTRO_CALL, TRUE)
	CLEAR_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_DO_EXEC_INTRO_CALL)
	
	//PRINT HELP TEXT ONLY THE FIRST TIME
	BOOL bDidDisplayHelpText = GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAYED_FIRST_CARGO_SOURCING_HELP)
	IF NOT bDidDisplayHelpText
		//DIFFERENT HELP TEXT IN SLOT 0 (WAREHOUSE BOSS)
		IF iSaveSlot = 0
			CLEAR_HELP()
			PRINT_HELP("SRC_CRG_HELP_1") //Lupe will contact you when they've sourced some Special Cargo and returned it to the Warehouse.
		ELSE
			CLEAR_HELP()
			PRINT_HELP("SRC_CRG_HELP_2") //Lupe will contact you when your staff member has sourced some Special Cargo and returned it to the Warehouse.
		ENDIF
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAYED_FIRST_CARGO_SOURCING_HELP, TRUE)
	ENDIF
	
	PRINTLN("[WAREHOUSE][CARGO SOURCING] SEND_PED_TO_SOURCE_CARGO, iSaveSlot = ", iSaveSlot, "  warehouse = ", GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID()))
	BROADCAST_LAUNCH_CUSTOM_PED_UPDATE_ACTION(0)
	REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
ENDPROC

FUNC BOOL RUN_CARGO_SOURCE_TRANSACTION()
	IF USE_SERVER_TRANSACTIONS()	
		IF NOT IS_BIT_SET(g_sWarehouseCargoSourcingData.iBS, WAREHOUSE_CARGO_SOURCING_STATUS_TRANSACTION_STARTED)
			IF NOT NETWORK_REQUEST_CASH_TRANSACTION(g_sWarehouseCargoSourcingData.iTransactionID,
						NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND,
						CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_SOURCE_CARGO,
						g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE , CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
						
				CASSERTLN(DEBUG_AMBIENT, "[RUN_CARGO_SOURCE_TRANSACTION][CASH] fail to cash consume transaction")
				DELETE_CASH_TRANSACTION(g_sWarehouseCargoSourcingData.iTransactionID)
				RETURN FALSE
			ENDIF
			CPRINTLN(DEBUG_AMBIENT, "[RUN_CARGO_SOURCE_TRANSACTION][CASH] request cash transaction")
			SET_BIT(g_sWarehouseCargoSourcingData.iBS, WAREHOUSE_CARGO_SOURCING_STATUS_TRANSACTION_STARTED)
		ENDIF
		
		IF NOT IS_BIT_SET(g_sWarehouseCargoSourcingData.iBS, WAREHOUSE_CARGO_SOURCING_STATUS_TRANSACTION_COMPLETED)
			IF IS_CASH_TRANSACTION_COMPLETE(g_sWarehouseCargoSourcingData.iTransactionID)
				IF GET_CASH_TRANSACTION_STATUS(g_sWarehouseCargoSourcingData.iTransactionID) = CASH_TRANSACTION_STATUS_SUCCESS
					PRINTLN("[RUN_CARGO_SOURCE_TRANSACTION][CASH] - Transaction ", g_sWarehouseCargoSourcingData.iTransactionID, " successful!")
					
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX( g_sWarehouseCargoSourcingData.iTransactionID))	
					
					INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
					INT iSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
					NETWORK_SPENT_CARGO_SOURCING(g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, FALSE, TRUE, g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, iWarehouse, iSlot)
					
					DELETE_CASH_TRANSACTION(g_sWarehouseCargoSourcingData.iTransactionID)
					SET_BIT(g_sWarehouseCargoSourcingData.iBS, WAREHOUSE_CARGO_SOURCING_STATUS_TRANSACTION_COMPLETED)		
				ELSE
					CASSERTLN(DEBUG_AMBIENT, "[RUN_CARGO_SOURCE_TRANSACTION][CASH] - Transaction ", g_sWarehouseCargoSourcingData.iTransactionID, " failed!")
					
					DELETE_CASH_TRANSACTION(g_sWarehouseCargoSourcingData.iTransactionID)	
					REQUEST_CARGO_SOURCING(FALSE)
					RETURN FALSE
				ENDIF
			ELSE
				PRINTLN("[RUN_CARGO_SOURCE_TRANSACTION][CASH] - Transaction ", g_sWarehouseCargoSourcingData.iTransactionID, " not complete!")
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_sWarehouseCargoSourcingData.iBS, WAREHOUSE_CARGO_SOURCING_STATUS_TRANSACTION_COMPLETED)
			IF (g_sWarehouseCargoSourcingData.iTransactionID < 0)
				PRINTLN("[RUN_CARGO_SOURCE_TRANSACTION][CASH] - Transaction ", g_sWarehouseCargoSourcingData.iTransactionID, " invalid!")
				DELETE_CASH_TRANSACTION(g_sWarehouseCargoSourcingData.iTransactionID)		// Delete the cash transaction
			ELSE
				PRINTLN("[RUN_CARGO_SOURCE_TRANSACTION][CASH] - Transaction ", g_sWarehouseCargoSourcingData.iTransactionID, " pending completed / save in progress...")
			ENDIF
			SEND_PED_TO_SOURCE_CARGO()
			RETURN TRUE		
		ENDIF
	ELSE
		PRINTLN("[RUN_CARGO_SOURCE_TRANSACTION][CASH] - Console Transaction completed ")
		INT iWarehouse = GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())
		INT iSlot = GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
		NETWORK_SPENT_CARGO_SOURCING(g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, FALSE, TRUE, g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_PRICE, iWarehouse, iSlot)
		
		SEND_PED_TO_SOURCE_CARGO()
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE eState)
	PRINTLN("[WAREHOUSE][CARGO SOURCING] SET_CARGO_SOURCING_TRANSACTION_STATE NEW STATE: ", eState )
	g_sWarehouseCargoSourcingData.eContrabandTransactionState = eState
ENDPROC

FUNC CONTRABAND_TRANSACTION_STATE GET_CARGO_SOURCING_TRANSACTION_STATE()	
	RETURN g_sWarehouseCargoSourcingData.eContrabandTransactionState
ENDFUNC

FUNC BOOL _PROCESS_TRANSACTION_ADD_GOODS_TO_WAREHOUSE(INT iSaveSlot, INT iContrabandToAdd, INT iSpecialContrabandCount, CONTRABAND_TRANSACTION_STATE &eCurrentResult)
	// Cleanup previous attempts
	IF eCurrentResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eCurrentResult != CONTRABAND_TRANSACTION_STATE_PENDING
		SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_DEFAULT)
	ENDIF
	
	
	INT iQuantityKey = GET_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(iSaveSlot)
	INT iSPQuantityKey = GET_SPECIAL_CONTRABAND_QUANTITY_INVENTORY_KEY_FOR_CATALOGUE(iSaveSlot)
	
	SWITCH eCurrentResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
				CPRINTLN(DEBUG_SHOPS, "_PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - Transaction waiting")
				RETURN FALSE
			ELSE				
				//CRATES
				//0:  CATEGORY_CONTRABAND_QNTY, <quantity>
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iQuantityKey, NET_SHOP_ACTION_UPDATE_BUSINESS_GOODS, iContrabandToAdd, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					//SPECIAL ITEMS
					//2:  CATEGORY_CONTRABAND_QNTY, <quantity>
					IF iSpecialContrabandCount = 0
					OR NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iSPQuantityKey, NET_SHOP_ACTION_ADD_CONTRABAND, iSpecialContrabandCount, 0, 0, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
						SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_PENDING)
					ELSE
						SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_FAILED)
						CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADD_CONTRABAND - Failed to add special quantity item")
					ENDIF
				ELSE
					SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_FAILED)
					CPRINTLN(DEBUG_SHOPS, "_PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - Failed to add product quantity item")
				ENDIF
				
				IF GET_CARGO_SOURCING_TRANSACTION_STATE() = CONTRABAND_TRANSACTION_STATE_PENDING 
				AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					CPRINTLN(DEBUG_SHOPS, "_PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - Starting basket checkout")
				ELSE
					SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_FAILED)
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CPRINTLN(DEBUG_SHOPS, "_PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - Failed to start checkout")
				ENDIF
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
				SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_FAILED)
				PRINTLN("[FIXER_AGENCY_PAYOUT] _PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - CONTRABAND_TRANSACTION_STATE_PENDING - Failed - Lost the reference to the basket")
			ELIF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_SUCCESS)
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					PRINTLN("[FIXER_AGENCY_PAYOUT] _PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - CONTRABAND_TRANSACTION_STATE_PENDING - Success")
				ELSE
					SET_CARGO_SOURCING_TRANSACTION_STATE(CONTRABAND_TRANSACTION_STATE_FAILED)
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					PRINTLN("[FIXER_AGENCY_PAYOUT] _PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - CONTRABAND_TRANSACTION_STATE_PENDING - Failed")
				ENDIF
			ENDIF
			
			PRINTLN("[FIXER_AGENCY_PAYOUT] _PROCESS_TRANSACTION_ADD_CASH_TO_FIXER_AGENCY_SAFE - CONTRABAND_TRANSACTION_STATE_PENDING")
		BREAK	
	ENDSWITCH
	
	RETURN GET_CARGO_SOURCING_TRANSACTION_STATE() != CONTRABAND_TRANSACTION_STATE_PENDING
ENDFUNC

FUNC CONTRABAND_SPECIAL_ITEM GET_SPECIAL_ITEM_FROM_INT(INT iIndex)
	SWITCH iIndex
		CASE 0	RETURN CONTRABAND_ITEM_ORNAMENTAL_EGG
		CASE 1 	RETURN CONTRABAND_ITEM_GOLDEN_MINIGUN
		CASE 2	RETURN CONTRABAND_ITEM_EXTRA_LARGE_DIAMOND
		CASE 3	RETURN CONTRABAND_ITEM_SASQUATCH_HIDE
		CASE 4	RETURN CONTRABAND_ITEM_FILM_REEL
		CASE 5	RETURN CONTRABAND_ITEM_RARE_POCKET_WATCH
	ENDSWITCH
	RETURN CONTRABAND_ITEM_INVALID
ENDFUNC

PROC ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING(INT iWarehouse, INT &iContrabandToAdd, INT iSpecialContrabandCount, CONTRABAND_TRANSACTION_STATE &eResult, CONTRABAND_TYPE eContrabandType, INT iSpecialItemType)
	
	IF iContrabandToAdd <= 0
		IF iContrabandToAdd = 0
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING Trying to add 0 contraband to warehouse: ", iWarehouse, " iContrabandToAdd:", iContrabandToAdd)
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING Trying to add a negative amount of contraband to warehouse: ", iWarehouse, " iContrabandToAdd:", iContrabandToAdd)
		ENDIF
		
		eResult = CONTRABAND_TRANSACTION_STATE_FAILED		
		EXIT
	ENDIF
	
	// Reset state if we need to.
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	IF eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
		IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING - Wait on already running transaction")
			EXIT
		ENDIF
	ENDIF

	//Check if we own the warehouse and then grab it's capacity
	INT iCapacityRemaining	= GET_WAREHOUSE_REMAINING_CAPACITY(iWarehouse)
	INT iSaveSlot			= GET_SAVE_SLOT_FOR_WAREHOUSE(iWarehouse)
	
	IF iContrabandToAdd > iCapacityRemaining
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING - Reducing contraband to add from ", iContrabandToAdd, "to ", iCapacityRemaining )
		iContrabandToAdd = iCapacityRemaining	
	ENDIF
	
	
	IF NOT USE_SERVER_TRANSACTIONS()
		IF iSaveSlot >= 0 
		AND iCapacityRemaining >= iContrabandToAdd
			//Calculate the new contraband total
			INT iNewContrabandTotal  = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) + iContrabandToAdd
			// Fill WH slots
			POPULATE_WAREHOUSE_CRATES(iWarehouse, iSaveSlot, iNewContrabandTotal, iSpecialContrabandCount, TRUE, eContrabandType)
			//Set the stat
			SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSaveSlot), iNewContrabandTotal)
			//Set the BD
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iContrabandUnitsTotal = iNewContrabandTotal
			
			IF (GET_TOTAL_SPECIAL_ITEM_COUNT() + iSpecialContrabandCount) <= g_ciMaxSpecialItems
				IF iSpecialContrabandCount != 0
					ADD_SPECIAL_ITEMS_TO_INVENTORY_STAT(iWarehouse, iSpecialContrabandCount)
					INT iIndex = (GET_TOTAL_SPECIAL_ITEM_COUNT() -1)
					ADD_SPECIAL_ITEMS_TO_WAREHOUSE(iWarehouse, GET_SPECIAL_ITEM_FROM_INT(iSpecialItemType), iSpecialContrabandCount, iIndex)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING Tried to add more special items than we have space for to warehouse")
			ENDIF
			
			UNLOCK_CONTRABAND_SWAG_IN_OFFICE(eContrabandType)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES called after buy mission completed")
			SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE(iWarehouse)
			RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES(TRUE)
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
			ENDIF
			
			SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SOURCE_SPECIAL_CARGO)

			REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING: ", iWarehouse, " adding: ", iContrabandToAdd)
			eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS			
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING: Tried to add more contraband than capacity remaining to warehouse: ", iWarehouse, " iContrabandToAdd: ", iContrabandToAdd, " remaining capacity: ", iCapacityRemaining, " iSaveSlot: ", iSaveSlot)
			eResult = CONTRABAND_TRANSACTION_STATE_FAILED
			
		ENDIF
	ELSE
		IF iSaveSlot >= 0
		AND iCapacityRemaining >= iContrabandToAdd
			IF _PROCESS_TRANSACTION_ADD_GOODS_TO_WAREHOUSE(iSaveSlot, iContrabandToAdd, iSpecialContrabandCount, eResult)//, iContrabandToAdd, iSpecialContrabandCount, eResult)
				IF GET_CARGO_SOURCING_TRANSACTION_STATE() = CONTRABAND_TRANSACTION_STATE_SUCCESS
					//Calculate the new contraband total
					INT iNewContrabandTotal  = GET_CONTRABAND_UNITS_TOTAL_FOR_WAREHOUSE(iWarehouse) + iContrabandToAdd
					// Fill WH slots
					POPULATE_WAREHOUSE_CRATES(iWarehouse, iSaveSlot, iNewContrabandTotal, iSpecialContrabandCount, TRUE, eContrabandType)
					//Set the stat
					SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(GET_STAT_FOR_WAREHOUSE_CONTRABAND(iSaveSlot), iNewContrabandTotal)
					//Set the BD
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[iSaveSlot].iContrabandUnitsTotal = iNewContrabandTotal
					
					IF (GET_TOTAL_SPECIAL_ITEM_COUNT() /*+ iSpecialContrabandCount !Not for PC! */) <= g_ciMaxSpecialItems
						IF iSpecialContrabandCount != 0
							ADD_SPECIAL_ITEMS_TO_INVENTORY_STAT(iWarehouse, iSpecialContrabandCount)
							INT iIndex = (GET_TOTAL_SPECIAL_ITEM_COUNT() -1)
							ADD_SPECIAL_ITEMS_TO_WAREHOUSE(iWarehouse, GET_SPECIAL_ITEM_FROM_INT(iSpecialItemType), iSpecialContrabandCount, iIndex)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[CONTRABAND] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING Tried to add more special items than we have space for to warehouse")
					ENDIF
					
					UNLOCK_CONTRABAND_SWAG_IN_OFFICE(eContrabandType)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE_CITEM] RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES called after buy mission completed")
					SELECT_RANDOM_BUYER_FOR_CONTRABAND_PURCHASE(iWarehouse)
					RANDOMIZE_WAREHOUSE_CONTRABAND_TYPES(TRUE)
					
					IF NETWORK_IS_ACTIVITY_SESSION()
						STAT_SET_OPEN_SAVETYPE_IN_JOB(STAT_SAVETYPE_END_SHOPPING)
					ENDIF

					REQUEST_SAVE(SSR_REASON_SPECIAL_CARGO_UPDATE, STAT_SAVETYPE_END_SHOPPING)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING: ", iWarehouse, " adding: ", iContrabandToAdd)
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					
					SET_MP_DAILY_OBJECTIVE_COMPLETE(MP_DAILY_AM_SOURCE_SPECIAL_CARGO)
				ELIF GET_CARGO_SOURCING_TRANSACTION_STATE() = CONTRABAND_TRANSACTION_STATE_FAILED
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING: TRANSACTION FAILED FOR ", iWarehouse, " trying to add: ", iContrabandToAdd)
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				ENDIF
			ENDIF
		ELSE
			CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING: PC transaction failed to add contraband to warehouse: ", iWarehouse, " iContrabandToAdd: ", iContrabandToAdd, " remaining capacity: ", iCapacityRemaining, " iSaveSlot: ", iSaveSlot, " Is warehouse disabled: ", IS_WAREHOUSE_DISABLED_BY_TUNEABLE(iWarehouse))
			eResult = CONTRABAND_TRANSACTION_STATE_FAILED
		ENDIF
	ENDIF
ENDPROC

PROC GENERATE_NUM_CARGO_TO_ADD()
	IF g_sWarehouseCargoSourcingData.iNumCargo != -1
		EXIT
	ENDIF
	
	FLOAT fRand = GET_RANDOM_FLOAT_IN_RANGE(0, g_sMPTunables.fWAREHOUSE_CARGO_SOURCING_WEIGHT_1 + g_sMPTunables.fWAREHOUSE_CARGO_SOURCING_WEIGHT_2 + g_sMPTunables.fWAREHOUSE_CARGO_SOURCING_WEIGHT_3)
	
	IF fRand <= g_sMPTunables.fWAREHOUSE_CARGO_SOURCING_WEIGHT_3
		g_sWarehouseCargoSourcingData.iNumCargo = 3
	ELIF fRand <= g_sMPTunables.fWAREHOUSE_CARGO_SOURCING_WEIGHT_2 + g_sMPTunables.fWAREHOUSE_CARGO_SOURCING_WEIGHT_3
		g_sWarehouseCargoSourcingData.iNumCargo = 2
	ELSE
		g_sWarehouseCargoSourcingData.iNumCargo = 1
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_sDebugWarehouseCargoSourcingData.bUseWidgetValues
		g_sWarehouseCargoSourcingData.iNumCargo = g_sDebugWarehouseCargoSourcingData.iNumCargo
	ENDIF	
	#ENDIF
	
ENDPROC

PROC GENERATE_NUM_SPECIAL_CARGO_TO_ADD()
	IF g_sWarehouseCargoSourcingData.iNumSpecialCargo != -1
		EXIT
	ENDIF
	
	INT iRand = GET_RANDOM_INT_IN_RANGE(1, 101)
	
	IF iRand <= g_sMPTunables.iWAREHOUSE_CARGO_SOURCING_CHANCE_SPECIAL
		g_sWarehouseCargoSourcingData.iNumSpecialCargo = 1
	ELSE
		g_sWarehouseCargoSourcingData.iNumSpecialCargo = 0
	ENDIF
		
	g_sWarehouseCargoSourcingData.iSpecialCargoType = GET_RANDOM_INT_IN_RANGE(0, 6)
	
	#IF IS_DEBUG_BUILD
	IF g_sDebugWarehouseCargoSourcingData.bUseWidgetValues
		IF g_sDebugWarehouseCargoSourcingData.bForceSpecialItem
			g_sWarehouseCargoSourcingData.iNumSpecialCargo = 1
		ENDIF
	ENDIF	
	#ENDIF
ENDPROC

PROC GENERATE_RANDOM_CONTRABAND_TYPE()
	IF g_sWarehouseCargoSourcingData.eType != CONTRABAND_TYPE_INVALID
		EXIT
	ENDIF	
	
	g_sWarehouseCargoSourcingData.eType = INT_TO_ENUM(CONTRABAND_TYPE ,GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CONTRABAND_TYPE_MAX)))
	
	#IF IS_DEBUG_BUILD
	IF g_sDebugWarehouseCargoSourcingData.bUseWidgetValues
		IF g_sDebugWarehouseCargoSourcingData.iContrabandType != 0
			g_sWarehouseCargoSourcingData.eType = INT_TO_ENUM(CONTRABAND_TYPE, g_sDebugWarehouseCargoSourcingData.iContrabandType-1)
		ENDIF
	ENDIF	
	#ENDIF
ENDPROC

PROC MAINTAIN_LAUNCHING_CARGO_SOURCING()
	IF NOT DOES_PLAYER_OWN_A_WAREHOUSE(PLAYER_ID())
		EXIT
	ENDIF	
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		EXIT
	ENDIF
	
	IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		IF GET_OWNER_OF_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())) = PLAYER_ID()
			IF DID_PLAYER_REQUEST_CARGO_SOURCING()
				RUN_CARGO_SOURCE_TRANSACTION()
			ENDIF 
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_WAREHOUSE_CARGO_SOURCING()
	//only concenrns players who own a warehouse
	IF NOT DOES_PLAYER_OWN_A_WAREHOUSE(PLAYER_ID())
		EXIT
	ENDIF	
	
	IF IS_PLAYER_IN_WAREHOUSE(PLAYER_ID())
		IF GET_OWNER_OF_WAREHOUSE(GET_WAREHOUSE_PLAYER_IS_IN(PLAYER_ID())) = PLAYER_ID()
			EXIT
		ENDIF
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION() //block updates in activity session unless transaction is already running
	AND NOT IS_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS()
		EXIT
	ENDIF
	
	////   UPDATE BUSINESS GOODS TRANSACTION
	///needs to be called every frame OUTSIDE OF THE OWNED WAREHOUSE, thus "EXIT" above - 	
	INT iWarehouse = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[g_sWarehouseCargoSourcingData.iWarehouseStagger].iWarehouse
	IF iWarehouse != ciW_Invalid
	AND	SHOULD_CARGO_BE_DELIVERED_TO_WAREHOUSE_AT_SAVESLOT(g_sWarehouseCargoSourcingData.iWarehouseStagger)
	AND NOT IS_WAREHOUSE_FULL(iWarehouse)
		SET_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS(TRUE, g_sWarehouseCargoSourcingData.iWarehouseStagger)
		GENERATE_NUM_CARGO_TO_ADD()
		GENERATE_NUM_SPECIAL_CARGO_TO_ADD()
		GENERATE_RANDOM_CONTRABAND_TYPE()
		
		ADD_CONTRABAND_TO_WAREHOUSE_AFTER_CARGO_SOURCING(iWarehouse, g_sWarehouseCargoSourcingData.iNumCargo ,g_sWarehouseCargoSourcingData.iNumSpecialCargo, g_sWarehouseCargoSourcingData.eContrabandTransactionState, g_sWarehouseCargoSourcingData.eType, g_sWarehouseCargoSourcingData.iSpecialCargoType)
				
		IF GET_CARGO_SOURCING_TRANSACTION_STATE() != CONTRABAND_TRANSACTION_STATE_PENDING
		AND GET_CARGO_SOURCING_TRANSACTION_STATE() != CONTRABAND_TRANSACTION_STATE_DEFAULT			
			SET_CARGO_DELIVERED_TO_WAREHOUSE(iWarehouse)	
		ENDIF	
	//IF THE WAREHOUSE IS FULL, SKIP THE TRANSACTION STUFF, JUST PRINT TICKER AND RESET ALL DATA
	ELIF iWarehouse != ciW_Invalid
	AND	SHOULD_CARGO_BE_DELIVERED_TO_WAREHOUSE_AT_SAVESLOT(g_sWarehouseCargoSourcingData.iWarehouseStagger)
	AND IS_WAREHOUSE_FULL(iWarehouse)
		SET_CARGO_DELIVERED_TO_FULL_WAREHOUSE(iWarehouse)	
	ELSE
		/// IF somehow PLAYER SELLS THE WAREHOUSE IN SLOT WE CANCEL CARGO SOURCING (trading in should be blocked if cargo sourcing is in progress)
		IF iWarehouse = ciW_Invalid
		AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, g_sWarehouseCargoSourcingData.iWarehouseStagger)
			PRINTLN("[WAREHOUSE][CARGO SOURCING] DATA SHOWS THAT PED IN WARHOUSE SLOT ", g_sWarehouseCargoSourcingData.iWarehouseStagger , " IS SOURCING CARGO BUT iWarehouse = ciW_Invalid. RESETTING DATA")
			SET_MP_INT_CHARACTER_STAT(GET_CARGO_SOURCING_POSIX_STAT_FOR_SAVE_SLOT(g_sWarehouseCargoSourcingData.iWarehouseStagger), GET_CLOUD_TIME_AS_INT())
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSThree, g_sWarehouseCargoSourcingData.iWarehouseStagger)
		ENDIF
	ENDIF
	
	//MAINTAINS THE BITS AND CHECKS STATS AS TO WHETHER PED IS AWAY AND/OR SHOULD BE BACK/IN	
	IF iWarehouse != ciW_Invalid
		//this maintains the bitsets as well as checks, no need to use it in an if here
		//this is the one that checks the stats, doesn't need to be called every frame
		IS_WAREHOUSE_PED_SOURCING_CARGO(iWarehouse)			
	ENDIF
	
	//IF WE'RE RUNNING TRANSACTION, ONLY PROCESS THIS ONE WAREHOUSE
	IF NOT IS_CARGO_DELIVERY_TRANSACTION_IN_PROGRESS()
		g_sWarehouseCargoSourcingData.iWarehouseStagger++
		IF g_sWarehouseCargoSourcingData.iWarehouseStagger >= ciMaxOwnedWarehouses
			g_sWarehouseCargoSourcingData.iWarehouseStagger = 0
		ENDIF
	ENDIF	
ENDPROC
#ENDIF
 //IF FEATURE_EXECUTIVE

//End of file
