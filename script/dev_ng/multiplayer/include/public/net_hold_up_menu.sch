USING "commands_misc.sch"
USING "commands_debug.sch"
USING "commands_script.sch"

USING "net_include.sch"
USING "context_control_public.sch"


CONST_INT TOTAL_CASH_SPENT_LIMIT		1000


CONST_INT biInitialSetup			0
CONST_INT biHelpDisplayed			1
CONST_INT biRegStaffReact			2
CONST_INT biExtraStaffReact0		3
CONST_INT biExtraStaffReact1		4
CONST_INT biRegisterEmptied			5
CONST_INT biCopsAlerted				6
CONST_INT biRegStaffAttack			7
CONST_INT biExtraStaffAttack		8
CONST_INT biEmptyRegisterStarted	9
CONST_INT biAimNoMicHelpTextDone	10
CONST_INT biRegStaffAttacking		11
CONST_INT biEmptyingDialogue		12
CONST_INT biGiveMeTheMoneyDialogue	13
CONST_INT biCashBagVisible			14
CONST_INT biCashBagDetached			15
CONST_INT biNearestStoreDataGrabbed	16
CONST_INT biCanIHelpDialogue		17
CONST_INT biAimWithMicHelpTextDone	18
CONST_INT biLeaveAreaTextDone		19
CONST_INT biXPandCashGiven			20
CONST_INT biGuardHelpDone			21
CONST_INT biInitialisationDone		22
CONST_INT biTutorialHelpDone		23
CONST_INT biCopsAlerted2			24
CONST_INT biAchieveHeading			25
CONST_INT biStoreMenuLoaded			26
CONST_INT biCopsAlertedToMelee		27
CONST_INT biDoingHeadAdditiveAnim	28
CONST_INT biHideTill				29
CONST_INT biHeadingBeforeSyncScene	30
CONST_INT biWeaponHolstered			31

CONST_INT bi2GotManualRobVector			0
CONST_INT bi2ManualRobHelpDone			1
CONST_INT bi2ExtraAnimsLoaded			2
CONST_INT bi2ClearFacialOverride		3
CONST_INT bi2FMEventCriticalHelp		4
CONST_INT bi2CashHasBeenDropped			5
CONST_INT bi2CashGrabbed				6
CONST_INT bi2TempPassiveModeEnabled		7
CONST_INT bi2OfficePASpeech_OFFER		8	// PA Menu - General offer of help
CONST_INT bi2OfficePASpeech_IDLE		9	// PA Menu - General Menu Idle
CONST_INT bi2OfficePASpeech_POSF		10	// PA Menu - General selection - Positive
CONST_INT bi2OfficePASpeech_NEGF		11	// PA Menu - General selection - Negative
CONST_INT bi2OfficePASpeech_SHELI		12	// PA Menu - Request Luxury Heli - Vehicle Selection 
CONST_INT bi2OfficePASpeech_PHELI		13	// PA Menu - Request Luxury Heli - Positive
CONST_INT bi2OfficePASpeech_NHELI		14	// PA Menu - Request Luxury Heli - Negative
CONST_INT bi2OfficePASpeech_SPEG		15	// PA Menu - Request Pegasus Vehicle - Vehicle Selection
CONST_INT bi2OfficePASpeech_PPEG		16	// PA Menu - Request Pegasus Vehicle - Positive
CONST_INT bi2OfficePASpeech_NPEG		17	// PA Menu - Request Pegasus Vehicle - Negative
CONST_INT bi2OfficePASpeech_SPERS		18	// PA Menu - Request Personal Vehicle - Selection
CONST_INT bi2OfficePASpeech_PPERS		19	// PA Menu - Request Personal Vehicle - Positive
CONST_INT bi2OfficePASpeech_NPERS		20	// PA Menu - Request Personal Vehicle - Negative
CONST_INT bi2OfficePASpeech_SLAP		21	// PA Menu - Lap Dancer Selection
CONST_INT bi2OfficePASpeech_PLAP		22	// PA Menu - Lap Dancer - Positive
CONST_INT bi2OfficePASpeech_PMLAP		23	// PA Menu - Lap Dancer (Multiple) - Positive
CONST_INT bi2OfficePASpeech_NLAP		24	// PA Menu - Lap Dancer - Negative
CONST_INT bi2OfficePASpeech_SNACK		25	// PA Menu - Snacks General
CONST_INT bi2OfficePASpeech_FULL		26	// PA Menu - Snacks Full


CONST_INT biSS_SnackBought						0
CONST_INT biSS_SnackRequestAnim					1
CONST_INT biSS_SnackShopLiftCheck				2
CONST_INT biSS_ReactToLiftLineDone				3
CONST_INT biSS_ShopLiftInterruptSnackBuy		4
CONST_INT biSS_SnacksCreated					5

CONST_INT biPS_SnackBought		0
CONST_INT biPS_SnackTaskDone	1
CONST_INT biPS_ReactToShopLift	2
CONST_INT biPS_ReactToLiftDone	3

CONST_INT biP_StaffReactToWeaponDone		0
CONST_INT biP_PlayerHasDemandedCash			1
//CONST_INT biP_CashPickupCreated				2
CONST_INT biP_GotWantedWithBlindEyePerk		2
CONST_INT biP_SyncSceneStarted				3
CONST_INT biP_CashBagDroppedEarly			4
CONST_INT biP_CashBagPickedUp				5
CONST_INT biP_Line4Done						6
CONST_INT biP_Line5Done						7
CONST_INT biP_PlayerHasAimedAtStaff			8
CONST_INT biP_StaffFleeToBackRoom			9
CONST_INT biP_StaffHandsUpLoop				10
CONST_INT biP_CashBagAvailbleForPickup		11
CONST_INT biP_RobbedRegisterManually		12
CONST_INT biP_CashBagPreventCollectionCleared	13
CONST_INT biP_FleeRouteOkayCheck			19
CONST_INT biP_RestrictedAreaLineDone		20
CONST_INT biP_RestrictedAreaEntered			21
CONST_INT biP_ReactToShoutLineDone			22
CONST_INT biP_ReactToGunShotLineDone		23
CONST_INT biP_ReactToGunLineDone			24
CONST_INT biP_ReactToWantedLevelLineDone	25
CONST_INT biP_ReactToUnarmedAimLineDone		26
CONST_INT biP_PlayerHasUnarmedAimedAtStaff	27
CONST_INT biP_PlayerIsAimingAtStaff			28
CONST_INT biP_ReactToCopArrival				29
CONST_INT biP_ReactToSnackBuy				30
CONST_INT biP_ReactToSnackLineDone			31

ENUM STAFF_STAGE_ENUM
	eSTAFF_CREATED,
	eSTAFF_WAIT,
	eSTAFF_REACT_TO_WEAPON,
	eSTAFF_GET_INTO_POSITION,
	eSTAFF_EMPTY_REGISTER,
	eSTAFF_HANDS_UP,
	eSTAFF_HANDS_UP_LOOP_ONLY,
	eSTAFF_ATTACK,
	eSTAFF_FLEE_TO_BACK_ROOM,
	eSTAFF_CLEANUP
ENDENUM

// SHOP SNACK VARS ------------------------------------------------------------------------------------------

ENUM SHOP_SNACKS_STATE
	SHOPSNACKS_INIT,
	SHOPSNACKS_WAIT_ACTIVATE,
	SHOPSNACKS_WAIT_ACTIVATE_MENU,
	SHOPSNACKS_SETUP_MENU,
	SHOPSNACKS_UPDATE_MENU,
	SHOPSNACKS_BUY_SERVER,
	SHOPSNACKS_BUY,
	SHOPSNACKS_SHOPLIFT,
	SHOPSNACKS_SHOPLIFT2,
	SHOPSNACKS_RESET,
	SHOPSNACKS_SOLD_OUT,
	SHOPSNACKS_STORE_ALERT,
	SHOPSNACKS_CLEANUP,
	SHOPSNACKS_DONE
ENDENUM

ENUM SNACK_BUY_ANIM_STATE
	SNACKBUY_INIT,
	SNACKBUY_PLAY_ANIM,
	SNACKBUY_CLEANUP_ANIM,
	SNACKBUY_DONE
ENDENUM

STRUCT MP_SHOP_SNACKS
	
	SHOP_ROBBERIES_SHOP_INDEX eCurrentShop
	
	CAMERA_INDEX ciSnacks
	
	VECTOR vCamPos
	VECTOR vCamRot
	
	VECTOR vPos
	FLOAT fRadius
	
	//menu info
	INT		iCurrentSelection
	INT		iMenuLength
	
	INT		iUseContext
	
	INT 	iHoldTime
	
	//buying snack locate info
	VECTOR 	vBuyEntryPos[2]		// The entry points for the locate
	VECTOR 	vBuyLocateCenter	// Coord used to check heading
	FLOAT	fBuyEntrySize		// The size used for line activation
	
	BOOL bSnackAttempted
	BOOL bSnackAfford
	BOOL bCheckMoney
	BOOL bCheckHealth
	BOOL bSnackNeeded
	BOOL bCheckInventory
	BOOL bSnackAvailable
	//BOOL bSnackStole
	//BOOL bSnackBuyDialogueSaid
	BOOL bSnacksSoldOut
	BOOL bSnackPurchased
	BOOL bSkipCashAlert
	
	STRING szHeaderTxd
	
	structTimer timerScrollUp
	structTimer timerScrollDown
	structTimer timerSnackReset
	
	BOOL bProcessingBasket = FALSE
	INT iProcessingBasketStage = 0
	INT iProcessSuccess = -1
	
	
	#IF IS_DEBUG_BUILD
	MP_MISSION thisHoldupMission = eAM_HOLD_UP
	#ENDIF
	Crim_HOLD_UP_POINT NearestHoldUpPoint
	INT iCashSpentThisTranssation
	INT iBoolsBitSet
	INT iBoolsBitSet2
	SHOP_SNACKS_STATE eShopSnacksState = SHOPSNACKS_INIT
	INT iIncrease
ENDSTRUCT

// to change the order of the menu, just change the order of the enums here
// currently the order is alphabetised
ENUM SNACK_CATEGORIES
	SNACK_SODA		= 0,	//eCola
	SNACK_MEDIUM	= 1,	//EgoChaser
	SNACK_LARGE		= 2,	//Meteorite
	SNACK_SMALL 	= 3, 	//P's & Q's
	SNACK_SMOKES	= 4,	//Pack of Redwood
	SNACK_BEER		= 5,	//Pisswasser
	#IF FEATURE_DLC_1_2022
	SNACK_SPRUNK	= 6,	//Sprunk soda
	#ENDIF
	/*#IF IS_DEBUG_BUILD
	#IF FEATURE_HEIST_PLANNING
	SNACK_LOTTERY	= 7,	//Lottery Ticket
	#ENDIF
	#ENDIF*/
	SNACK_TOTAL //currently 7
ENDENUM

//CONST_INT iSmallSnackPrice	1
//CONST_INT iMedSnackPrice	2
//CONST_INT iEpicSnackPrice	4
//CONST_INT iSmokePrice		7
//CONST_INT iSodaPrice		3
//CONST_INT iBoozePrice		4

CONST_INT iSnackStock 		10 		// store will have 10 of each item

CONST_INT iSnackTimeOut		300000 // 5 minutes
CONST_INT iSnackItemTimeout 150000


// END SHOP SNACK VARS --------------------------------------------------------------------------------------

// SHOP SNACK CONTENT ------------------------------------------------------------------------------------------

FUNC INT GET_NUMBER_OF_SNACKS()
	
	#IF FEATURE_DLC_1_2022
	IF NOT g_sMPTunables.bEnableSprunkPurchasing
		RETURN (ENUM_TO_INT(SNACK_TOTAL) - 1)
	ENDIF
	#ENDIF
	
	RETURN ENUM_TO_INT(SNACK_TOTAL)
ENDFUNC

FUNC STRING GET_SHOPLIFT_ANIM_DICT()
	IF IS_PLAYER_MALE(PLAYER_ID())
		RETURN "anim@am_hold_up@male"
	ENDIF
	
	RETURN "anim@am_hold_up@female"
ENDFUNC

FUNC STRING GET_SHOPLIFT_ANIM_HEIGHT()
	IF GET_RANDOM_BOOL()
		RETURN "Shoplift_Mid"
	ENDIF
	
	RETURN "Shoplift_Low"
ENDFUNC

FUNC STRING GET_SNACK_DESCRIPTION(INT iSnack)
	
	SNACK_CATEGORIES eWhichSnack
	
	IF iSnack != -1
		eWhichSnack = INT_TO_ENUM(SNACK_CATEGORIES, iSnack)
	ELSE
		eWhichSnack = SNACK_TOTAL
	ENDIF
	
	SWITCH eWhichSnack
		CASE SNACK_TOTAL		RETURN "SNK_AFFORD"
		CASE SNACK_SMALL		RETURN "SNK_ITEM1_D"
		CASE SNACK_MEDIUM		RETURN "SNK_ITEM2_D"
		CASE SNACK_LARGE		RETURN "SNK_ITEM3_D"
		CASE SNACK_SMOKES		RETURN "SNK_ITEM4_D"
		CASE SNACK_SODA			RETURN "SNK_ITEM5_D"
		CASE SNACK_BEER			RETURN "SNK_ITEM6_D"
		#IF FEATURE_DLC_1_2022
		CASE SNACK_SPRUNK		RETURN "SNK_ITEM7_D"
		#ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY		RETURN "SNK_ITEM8_D"
		#ENDIF
		#ENDIF*/
	ENDSWITCH

	RETURN ""
ENDFUNC

#IF NOT DEFINED(GET_SNACK_NAME)
FUNC STRING GET_SNACK_NAME(INT iSnack)
	
	SNACK_CATEGORIES eWhichSnack
	eWhichSnack = INT_TO_ENUM(SNACK_CATEGORIES, iSnack)
	
	SWITCH eWhichSnack
		CASE SNACK_SMALL		RETURN "SNK_ITEM1"
		CASE SNACK_MEDIUM		RETURN "SNK_ITEM2"
		CASE SNACK_LARGE		RETURN "SNK_ITEM3"
		CASE SNACK_SMOKES		RETURN "SNK_ITEM4"
		CASE SNACK_SODA			RETURN "SNK_ITEM5"
		CASE SNACK_BEER			RETURN "SNK_ITEM6"
		#IF FEATURE_DLC_1_2022
		CASE SNACK_SPRUNK		RETURN "SNK_ITEM7"
		#ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY		RETURN "SNK_ITEM8"
		#ENDIF
		#ENDIF*/
	ENDSWITCH

	RETURN ""
ENDFUNC
#ENDIF

FUNC INT GET_SNACK_VARIATION(MP_SHOP_SNACKS &shopSnacks)
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
	OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN 3
	ELIF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_LIQUOR_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_LIQUOR_5
		RETURN 0
	ELIF shopSnacks.NearestHoldUpPoint >= eCRIM_HUP_GAS_1
	AND shopSnacks.NearestHoldUpPoint <= eCRIM_HUP_GAS_5
		RETURN 1
	ELSE
		RETURN 2
	ENDIF
	
	CASSERTLN(DEBUG_SHOP_ROBBERIES, "GET_SNACK_VARIATION: invalid nearest holdup point \"", shopSnacks.NearestHoldUpPoint, "\"")
	RETURN -1
ENDFUNC

FUNC INT GET_SNACK_PRICE(MP_SHOP_SNACKS &shopSnacks, INT iSnack)
	
	SNACK_CATEGORIES eWhichSnack
	eWhichSnack = INT_TO_ENUM(SNACK_CATEGORIES, iSnack)
	
	/*#IF IS_DEBUG_BUILD
	#IF FEATURE_HEIST_PLANNING
	IF eWhichSnack = SNACK_LOTTERY
		RETURN g_sMPTunables.ilotteryticketcost	//2
	ENDIF
	#ENDIF
	#ENDIF*/
	
	SWITCH GET_SNACK_VARIATION(shopSnacks)
		CASE 0
			SWITCH eWhichSnack
				CASE SNACK_SMALL			RETURN 2
				CASE SNACK_MEDIUM			RETURN 3
				CASE SNACK_LARGE			RETURN 5
				CASE SNACK_SMOKES			RETURN 7
				CASE SNACK_SODA				RETURN g_sMPTunables.iShopEColaCanCost
				#IF FEATURE_DLC_1_2022
				CASE SNACK_SPRUNK			RETURN g_sMPTunables.iShopSprunkCanCost
				#ENDIF
				CASE SNACK_BEER				RETURN 2
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH eWhichSnack
				CASE SNACK_SMALL			RETURN 1
				CASE SNACK_MEDIUM			RETURN 2
				CASE SNACK_LARGE			RETURN 3
				CASE SNACK_SMOKES			RETURN 8
				CASE SNACK_SODA				RETURN g_sMPTunables.iShopEColaCanCost
				#IF FEATURE_DLC_1_2022
				CASE SNACK_SPRUNK			RETURN g_sMPTunables.iShopSprunkCanCost
				#ENDIF
				CASE SNACK_BEER				RETURN 5
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH eWhichSnack
				CASE SNACK_SMALL			RETURN 1
				CASE SNACK_MEDIUM			RETURN 2
				CASE SNACK_LARGE			RETURN 4
				CASE SNACK_SMOKES			RETURN 6
				CASE SNACK_SODA				RETURN g_sMPTunables.iPropertyEColaCanCost
				#IF FEATURE_DLC_1_2022
				CASE SNACK_SPRUNK			RETURN g_sMPTunables.iPropertySprunkCanCost
				#ENDIF
				CASE SNACK_BEER				RETURN 4
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC MP_INT_STATS GET_SNACK_MP_STAT(INT iSnack)
	
	SNACK_CATEGORIES eWhichSnack
	eWhichSnack = INT_TO_ENUM(SNACK_CATEGORIES, iSnack)
	
	SWITCH eWhichSnack
		CASE SNACK_SMALL			RETURN MP_STAT_NO_BOUGHT_YUM_SNACKS
		CASE SNACK_MEDIUM			RETURN MP_STAT_NO_BOUGHT_HEALTH_SNACKS
		CASE SNACK_LARGE			RETURN MP_STAT_NO_BOUGHT_EPIC_SNACKS
		CASE SNACK_SMOKES			RETURN MP_STAT_CIGARETTES_BOUGHT
		CASE SNACK_SODA				RETURN MP_STAT_NUMBER_OF_ORANGE_BOUGHT
		CASE SNACK_BEER				RETURN MP_STAT_NUMBER_OF_BOURGE_BOUGHT
		#IF FEATURE_DLC_1_2022
		CASE SNACK_SPRUNK			RETURN MP_STAT_NUMBER_OF_SPRUNK_BOUGHT
		#ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY			RETURN MP_STAT_LOTTERY_TICKETS_BOUGHT
		#ENDIF
		#ENDIF*/
	ENDSWITCH
	
	RETURN MAX_NUM_MP_INT_STATS
ENDFUNC

FUNC INT GET_SNACK_MP_STAT_HASH_FOR_TELEMETRY(INT iSnack)
	
	SNACK_CATEGORIES eWhichSnack
	eWhichSnack = INT_TO_ENUM(SNACK_CATEGORIES, iSnack)
	
	SWITCH eWhichSnack
		CASE SNACK_SMALL			RETURN HASH("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
		CASE SNACK_MEDIUM			RETURN HASH("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
		CASE SNACK_LARGE			RETURN HASH("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
		CASE SNACK_SMOKES			RETURN HASH("MP_STAT_CIGARETTES_BOUGHT_v0")
		CASE SNACK_SODA				RETURN HASH("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
		CASE SNACK_BEER				RETURN HASH("MP_STAT_NUMBER_OF_BOURGE_BOUGHT_v0")
		#IF FEATURE_DLC_1_2022
		CASE SNACK_SPRUNK			RETURN HASH("MP_STAT_NUMBER_OF_SPRUNK_BOUGHT_v0")
		#ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY			RETURN MP_STAT_LOTTERY_TICKETS_BOUGHT
		#ENDIF
		#ENDIF*/
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_SNACK_MAX(INT iSnack)
	
	SNACK_CATEGORIES eWhichSnack
	eWhichSnack = INT_TO_ENUM(SNACK_CATEGORIES, iSnack)
	
	SWITCH eWhichSnack
		CASE SNACK_SMALL			RETURN INV_SNACK_1_MAX
		CASE SNACK_MEDIUM			RETURN INV_SNACK_2_MAX
		CASE SNACK_LARGE			RETURN INV_SNACK_3_MAX
		CASE SNACK_SMOKES			RETURN INV_SMOKES_MAX
		CASE SNACK_SODA				RETURN INV_DRINK_1_MAX
		CASE SNACK_BEER				RETURN INV_DRINK_2_MAX
		#IF FEATURE_DLC_1_2022
		CASE SNACK_SPRUNK			RETURN INV_DRINK_4_MAX
		#ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		CASE SNACK_LOTTERY			RETURN g_sMPTunables.imax_number_lottery_tickets
		#ENDIF
		#ENDIF*/
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL IS_HELD_AMOUNT_ABOVE_MAX(INT iPassedIn)
	/*#IF IS_DEBUG_BUILD
	#IF FEATURE_HEIST_PLANNING
	IF iPassedIn = ENUM_TO_INT(SNACK_LOTTERY)
		IF (iLotteryTicketsAlreadyBought+iLotteryTicketsJustBought) >= GET_SNACK_MAX(iPassedIn)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	#ENDIF*/
	
	IF GET_MP_INT_CHARACTER_STAT(GET_SNACK_MP_STAT(iPassedIn)) >= GET_SNACK_MAX(iPassedIn)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SNACK_MENU_UP(MP_SHOP_SNACKS &shopSnacks)
	
	FLOAT fLeftY
	
	fLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	
	IF fLeftY < -0.8 OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		IF NOT IS_TIMER_STARTED(shopSnacks.timerScrollUp)
			START_TIMER_NOW(shopSnacks.timerScrollUp)
			RETURN TRUE
		ELIF GET_TIMER_IN_SECONDS(shopSnacks.timerScrollUp) > 0.25
			RESTART_TIMER_NOW(shopSnacks.timerScrollUp)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(shopSnacks.timerScrollUp)
			CANCEL_TIMER(shopSnacks.timerScrollUp)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SNACK_MENU_DOWN(MP_SHOP_SNACKS &shopSnacks)
	
	FLOAT fLeftY
	
	fLeftY = GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	
	IF fLeftY > 0.8 OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		IF NOT IS_TIMER_STARTED(shopSnacks.timerScrollDown)
			START_TIMER_NOW(shopSnacks.timerScrollDown)
			RETURN TRUE
		ELIF GET_TIMER_IN_SECONDS(shopSnacks.timerScrollDown) > 0.25
			RESTART_TIMER_NOW(shopSnacks.timerScrollDown)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_TIMER_STARTED(shopSnacks.timerScrollDown)
			CANCEL_TIMER(shopSnacks.timerScrollDown)
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_SNACK_MENU(MP_SHOP_SNACKS &shopSnacks, INT &serverBD_iSnackInventory[],BOOL bIsOfficeSnackMenu)
	STRING sTempLabel
	
	IF NOT shopSnacks.bCheckMoney
		
	ENDIF
	
	IF SNACK_MENU_UP(shopSnacks)
		shopSnacks.bSnackAttempted = FALSE
		CANCEL_TIMER(shopSnacks.timerSnackReset)
		
		shopSnacks.iCurrentSelection -= 1
		IF (shopSnacks.iCurrentSelection < 0)
			shopSnacks.iCurrentSelection = shopSnacks.iMenuLength - 1
		ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		IF g_sMPTunables.bdisable_lottery = TRUE
			IF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LOTTERY)
				PRINTLN("[AM HOLD UP] - MOVE UP - SKIP LOTTERY IT HAS BEEN BLOCKED BY A TUNABLE")
				shopSnacks.iCurrentSelection -= 1
				IF (shopSnacks.iCurrentSelection < 0)
					shopSnacks.iCurrentSelection = shopSnacks.iMenuLength - 1
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
		#ENDIF*/
		
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_LIQUOR_STORE_SOUNDSET")
		
		SET_CURRENT_MENU_ITEM(shopSnacks.iCurrentSelection)
		
		sTempLabel = GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection)
		shopSnacks.bCheckMoney = FALSE
		shopSnacks.bCheckHealth = FALSE
		shopSnacks.bCheckInventory = FALSE
		
		IF DOES_TEXT_LABEL_EXIST(sTempLabel)
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
		ENDIF
	ENDIF
	
	IF SNACK_MENU_DOWN(shopSnacks)
		shopSnacks.bSnackAttempted = FALSE
		CANCEL_TIMER(shopSnacks.timerSnackReset)
		
		shopSnacks.iCurrentSelection += 1
		IF (shopSnacks.iCurrentSelection > shopSnacks.iMenuLength - 1)
			shopSnacks.iCurrentSelection = 0
		ENDIF
		
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		IF g_sMPTunables.bdisable_lottery = TRUE
			IF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LOTTERY)
				PRINTLN("[AM HOLD UP] - MOVE DOWN - SKIP LOTTERY IT HAS BEEN BLOCKED BY A TUNABLE")
				shopSnacks.iCurrentSelection += 1
				IF (shopSnacks.iCurrentSelection > shopSnacks.iMenuLength - 1)
					shopSnacks.iCurrentSelection = 0
				ENDIF
			ENDIF
		ENDIF
		#ENDIF
		#ENDIF*/
		
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_LIQUOR_STORE_SOUNDSET")
		
		SET_CURRENT_MENU_ITEM(shopSnacks.iCurrentSelection)
		sTempLabel = GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection)
		shopSnacks.bCheckMoney = FALSE
		shopSnacks.bCheckHealth = FALSE
		shopSnacks.bCheckInventory = FALSE
		
		IF DOES_TEXT_LABEL_EXIST(sTempLabel)
			SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
		ENDIF
	ENDIF
	
	IF NOT shopSnacks.bCheckMoney
		
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		IF shopSnacks.iCurrentSelection != ENUM_TO_INT(SNACK_LOTTERY)
		#ENDIF
		#ENDIF*/
			IF NETWORK_CAN_SPEND_MONEY(GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection), FALSE, TRUE, FALSE)
				shopSnacks.bSnackAfford = TRUE
			ELSE
				shopSnacks.bSnackAfford = FALSE
				shopSnacks.bSkipCashAlert = TRUE
			ENDIF
			
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		ELSE
			IF NETWORK_GET_EVC_BALANCE() >= GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
				IF NETWORK_CAN_SPEND_MONEY(GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection), FALSE, TRUE, FALSE)
					shopSnacks.bSnackAfford = TRUE
				ELSE
					shopSnacks.bSnackAfford = FALSE
					shopSnacks.bSkipCashAlert = TRUE
				ENDIF
			ELSE
				shopSnacks.bSnackAfford = FALSE
				shopSnacks.bSkipCashAlert = FALSE
			ENDIF
		ENDIF
		#ENDIF
		#ENDIF*/
		
		shopSnacks.bCheckMoney = TRUE
		
//		IF (GET_PLAYER_CASH(PLAYER_ID())  >= GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection))
//			shopSnacks.bSnackAfford = TRUE
//		ELSE
//			shopSnacks.bSnackAfford = FALSE
//		ENDIF
	ENDIF
	
	IF NOT shopSnacks.bCheckHealth
		//IF shopSnacks.iCurrentSelection != 3
		//	shopSnacks.bSnackNeeded = (GET_ENTITY_HEALTH(PLAYER_PED_ID()) < GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
		//ELSE
			shopSnacks.bSnackNeeded = TRUE
		//ENDIF
		
		//Check how many more we can carry
		IF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SMALL)
			shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) < INV_SNACK_1_MAX)
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_MEDIUM)
			shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) < INV_SNACK_2_MAX)
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LARGE)
			shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) < INV_SNACK_3_MAX)
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SMOKES)
			shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT) < INV_SMOKES_MAX)
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SODA)
			shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT) < INV_DRINK_1_MAX)
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_BEER)
			shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT) < INV_DRINK_2_MAX)
		#IF FEATURE_DLC_1_2022
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SPRUNK)
			IF g_sMPTunables.bEnableSprunkPurchasing
				shopSnacks.bSnackNeeded = (GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT) < INV_DRINK_4_MAX)
			ELSE
				shopSnacks.bSnackNeeded = FALSE
			ENDIF
		#ENDIF
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LOTTERY)
			shopSnacks.bSnackNeeded = TRUE	//CHECK AGAINST MAX ENTRIES ON LEADERBOARD
		#ENDIF
		#ENDIF*/
		ENDIF
		shopSnacks.bCheckHealth = TRUE
	ENDIF
	
	IF NOT shopSnacks.bCheckInventory
		IF serverBD_iSnackInventory[shopSnacks.iCurrentSelection] < iSnackStock
			shopSnacks.bSnackAvailable = TRUE
		ELSE
			shopSnacks.bSnackAvailable = FALSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SOUT")
		ENDIF
		shopSnacks.bCheckInventory = TRUE
	ENDIF
	
	/*#IF IS_DEBUG_BUILD
	#IF FEATURE_HEIST_PLANNING
	IF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LOTTERY)
		IF IS_BIT_SET(iLotteryBitSet, biL_BuyingTicketsDisabled)
			shopSnacks.bSnackAvailable = FALSE
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_0)
				PRINTLN(" LOTTERY - biL_BuyingTicketsDisabled IS SET - MAKE bSnackAvailable = FALSE")
			ENDIF
			#ENDIF
		ELIF NOT NETWORK_CAN_BUY_LOTTERY_TICKET(GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection))
			shopSnacks.bSnackAvailable = FALSE
			#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_0)
				PRINTLN(" LOTTERY - NETWORK_CAN_BUY_LOTTERY_TICKET = FALSE - MAKE bSnackAvailable = FALSE")
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	#ENDIF
	#ENDIF*/
	
	IF shopSnacks.bSnackAttempted
		IF GET_TIMER_IN_SECONDS_SAFE(shopSnacks.timerSnackReset) < 3.0
			IF shopSnacks.bSnackPurchased
				IF !bIsOfficeSnackMenu
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_BOUGHT")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_STORED")
				ENDIF
			ELIF NOT shopSnacks.bSnackAfford
				SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_AFFORD")
			ELIF NOT shopSnacks.bSnackAvailable
				/*#IF IS_DEBUG_BUILD
				#IF FEATURE_HEIST_PLANNING
				IF shopSnacks.iCurrentSelection != ENUM_TO_INT(SNACK_LOTTERY)
				#ENDIF
				#ENDIF*/
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SOUT")
				/*#IF IS_DEBUG_BUILD
				#IF FEATURE_HEIST_PLANNING
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_LOTCLS")	//Lottery Closed
				ENDIF
				#ENDIF
				#ENDIF*/
			ELIF NOT shopSnacks.bSnackNeeded 				
				IF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SMALL)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SNK1FU")
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_MEDIUM)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SNK2FU")
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LARGE)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SNK3FU")
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SMOKES)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SMOKFU")
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SODA)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_DRK1FU")
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_BEER)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_DRK2FU")
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_SPRUNK)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_DRK7FU")
				/*#IF IS_DEBUG_BUILD
				#IF FEATURE_HEIST_PLANNING
				ELIF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LOTTERY)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_LOTTFU")	//DONE
				#ENDIF
				#ENDIF*/
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_NEEDED")
				ENDIF
				
				SET_BIT(shopSnacks.iBoolsBitSet2, bi2OfficePASpeech_FULL)
				
			ENDIF
		ELSE
			shopSnacks.bSnackAttempted = FALSE
			shopSnacks.bSnackPurchased = FALSE
			CANCEL_TIMER(shopSnacks.timerSnackReset)
			sTempLabel = GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection)
			IF DOES_TEXT_LABEL_EXIST(sTempLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC EXIT_SNACK_MENU(MP_SHOP_SNACKS &shopSnacks, INT &playerBD_iCashSpent)
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	
	CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****Exit snacks getting called ")
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	IF shopSnacks.iCashSpentThisTranssation > 0
		playerBD_iCashSpent = shopSnacks.iCashSpentThisTranssation
		shopSnacks.iCashSpentThisTranssation = 0
	ENDIF
	
	CLEANUP_TEMP_PASSIVE_MODE()
	CLEAR_BIT(shopSnacks.iBoolsBitSet2, bi2TempPassiveModeEnabled)
	
	/*#IF IS_DEBUG_BUILD
	#IF FEATURE_HEIST_PLANNING
	//Set flag to do lottery stuff
	IF g_sMPTunables.bdisable_lottery = FALSE
		iLotteryEndCheckStage = LECS_WRITE
		PRINTLN(" LOTTERY - EXIT_SNACK_MENU - iLotteryEndCheckStage = LECS_WRITE")
	ENDIF
	#ENDIF
	#ENDIF*/
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
		CLEAR_HELP()
		PRINTLN("*****[_mp_shop_snacks][AM HOLD UP] - CLEAR HELP 'SHR_MENU' - EXIT_SNACK_MENU")
	ENDIF
	
	IF shopSnacks.iUseContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
	ENDIF
	
	IF DOES_CAM_EXIST(shopSnacks.ciSnacks)
		DESTROY_CAM(shopSnacks.ciSnacks)
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE,FALSE)
ENDPROC

FUNC BOOL IS_SHOP_ALL_SOLD_OUT(INT &serverBD_iSnackInventory[])
	INT idx
	
	REPEAT ENUM_TO_INT(SNACK_TOTAL) idx
		IF serverBD_iSnackInventory[idx] < iSnackStock
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/*#IF IS_DEBUG_BUILD
#IF FEATURE_HEIST_PLANNING
////*** LOTTERY ***////
//Sets a flag if we have bought the max number of Lottery Tickets
/*PROC DO_MAX_LOTTERY_TICKETS_BOUGHT_CHECK()
	IF NOT IS_BIT_SET(iLotteryBitSet, biL_BuyingTicketsDisabled)
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_LOTTERY_TICKETS_BOUGHT) < g_sMPTunables.imax_number_lottery_tickets 
			SET_BIT(iLotteryBitSet, biL_BuyingTicketsDisabled)
			PRINTLN(" LOTTERY - biL_BuyingTicketsDisabled SET - MAX TICKETS BOUGHT")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Does a Leaderboard Read to check that Leaderboards are working
INT iTempLoadStage
BOOL bTempReadResult
PROC DO_LOTTERY_LEADERBOARD_TEST()
	IF g_sMPTunables.bdisable_lottery = FALSE
		IF NOT IS_BIT_SET(iLotteryBitSet, biL_LeaderboardTestPassed)
		AND NOT IS_BIT_SET(iLotteryBitSet, biL_LeaderboardTestFailed)
			INT iTestResult = HAS_LOTTERY_LEADERBOARD_TEST_PASSED(iTempLoadStage, bTempReadResult)
			IF iTestResult != 0
				IF iTestResult = 1
					
					//Do New Week Checks
					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_STORED_LOTTERY_WEEK) != g_sMPTunables.ilottery_week
						PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - NEW LOTTERY WEEK")
						SET_MP_INT_CHARACTER_STAT(MP_STAT_STORED_LOTTERY_WEEK, g_sMPTunables.ilottery_week)
						PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - MP_STAT_STORED_LOTTERY_WEEK - UPDATED TO ", g_sMPTunables.ilottery_week)
						SET_MP_INT_CHARACTER_STAT(MP_STAT_LOTTERY_TICKETS_BOUGHT, 0)
						PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - MP_STAT_LOTTERY_TICKETS_BOUGHT - RESET TO ", 0)
					ENDIF
					
					DO_MAX_LOTTERY_TICKETS_BOUGHT_CHECK()
					iLotteryTicketsAlreadyBought = GET_MP_INT_CHARACTER_STAT(MP_STAT_LOTTERY_TICKETS_BOUGHT)
					PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - iLotteryTicketsAlreadyBought = ", iLotteryTicketsAlreadyBought)
					
					CLEAR_BIT(iLotteryBitSet, biL_BuyingTicketsDisabled)
					PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - LEADERBOARD TEST PASSED - biL_BuyingTicketsDisabled CLEARED")
					
					SET_BIT(iLotteryBitSet, biL_LeaderboardTestPassed)
					PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - biL_LeaderboardTestPassed SET")
				ELSE
					SET_BIT(iLotteryBitSet, biL_LeaderboardTestFailed)
					PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - biL_LeaderboardTestFailed SET")
					SET_BIT(iLotteryBitSet, biL_BuyingTicketsDisabled)
					PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - LEADERBOARD TEST FAILED - biL_BuyingTicketsDisabled SET")
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(iLotteryBitSet, biL_BuyingTicketsDisabled)
				SET_BIT(iLotteryBitSet, biL_BuyingTicketsDisabled)
				PRINTLN(" LOTTERY - DO_LOTTERY_LEADERBOARD_TEST - LEADERBOARD TEST NOT DONE - biL_BuyingTicketsDisabled SET")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Checks the Save worked okay and then does the Leaderboard Write
PROC DO_LOTTERY_END_CHECKS()
	TEXT_LABEL_31 categoryNames[1]
	TEXT_LABEL_23 uniqueIdentifiers[1]
	
	SWITCH iLotteryEndCheckStage
		CASE LECS_WAIT
			//Do Nothing
		BREAK
		
		CASE LECS_SAVE
			IF NOT STAT_SAVE_PENDING_OR_REQUESTED()
				REQUEST_SAVE(STAT_SAVETYPE_END_SHOPPING)
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - REQUEST_SAVE(STAT_SAVETYPE_END_SHOPPING) called")
			ENDIF
			iLotteryEndCheckStage = LECS_CHECK_SAVE
			PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - iLotteryEndCheckStage = LECS_CHECK_SAVE")
		BREAK
		
		CASE LECS_CHECK_SAVE
			IF STAT_SAVE_PENDING_OR_REQUESTED()
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - STAT_SAVE_PENDING_OR_REQUESTED = TRUE")
			ELSE
				IF STAT_CLOUD_SLOT_SAVE_FAILED(-1)
					iLotteryEndCheckStage = LECS_REFUND
					PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - SAVE FAILED - iLotteryEndCheckStage = LECS_REFUND")
				ELSE
					iLotteryEndCheckStage = LECS_WRITE
					PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - SAVE SUCCESSFUL - iLotteryEndCheckStage = LECS_WRITE")
				ENDIF
			ENDIF
		BREAK
		
		CASE LECS_WRITE
			IF INIT_LEADERBOARD_WRITE(LEADERBOARD_FREEMODE_LOTTERY_TICKET, uniqueIdentifiers, categoryNames, 0)
				LEADERBOARDS_WRITE_ADD_COLUMN(LB_INPUT_COL_SCORE, iLotteryTicketsJustBought, 0)
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - WRITE TO LEADERBOARD DONE")
				
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_LOTTERY_TICKETS_BOUGHT, iLotteryTicketsJustBought)
				iLotteryTicketsJustBought = 0
				iLotteryTicketsAlreadyBought = GET_MP_INT_CHARACTER_STAT(MP_STAT_LOTTERY_TICKETS_BOUGHT)
				DO_MAX_LOTTERY_TICKETS_BOUGHT_CHECK()
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - MP_STAT_LOTTERY_TICKETS_BOUGHT INCREASED TO ", iLotteryTicketsAlreadyBought)
				
				iLotteryEndCheckStage = LECS_WAIT
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - iLotteryEndCheckStage = LECS_WAIT")
			ELSE
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - INIT_LEADERBOARD_WRITE - FAILED")
				iLotteryEndCheckStage = LECS_REFUND
				PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - iLotteryEndCheckStage = LECS_REFUND")
			ENDIF
		BREAK
		
		CASE LECS_REFUND
			NETWORK_REFUND_CASH_TYPE(iLotteryTicketsJustBought*GET_SNACK_PRICE(shopSnacks, ENUM_TO_INT(SNACK_LOTTERY)), MP_REFUND_TYPE_LOTTERY_TICKET, MP_REFUND_REASON_LOTTERY_TICKET_FAIL)
			iLotteryTicketsJustBought = 0
			DO_MAX_LOTTERY_TICKETS_BOUGHT_CHECK()
			PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - REFUND DONE")
			
			iLotteryEndCheckStage = LECS_WAIT
			PRINTLN(" LOTTERY - DO_LOTTERY_END_CHECKS - iLotteryEndCheckStage = LECS_WAIT")
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF
#ENDIF*/

CONST_INT SHOP_MENU_BASKET_STAGE_ADD 	0
CONST_INT SHOP_MENU_BASKET_STAGE_PENDING 1
CONST_INT SHOP_MENU_BASKET_STAGE_SUCCESS 2
CONST_INT SHOP_MENU_BASKET_STAGE_FAILED 	3

FUNC BOOL PROCESSING_STORE_SHOPPING_BASKET(MP_SHOP_SNACKS &shopSnacks, SHOP_ITEM_CATEGORIES eCategory, ITEM_ACTION_TYPES eAction, INT iPrice)
    IF shopSnacks.bProcessingBasket
        SWITCH shopSnacks.iProcessingBasketStage
            // Add item to basket
            CASE SHOP_MENU_BASKET_STAGE_ADD
				
				// Override the players cash.
				INT iWalletAmount, iBankAmount, iTempPrice
				iWalletAmount = 0
				iBankAmount = 0
				iTempPrice = iPrice
				// Bank first
				IF (NETWORK_GET_VC_BANK_BALANCE() > 0)
					IF NETWORK_GET_VC_BANK_BALANCE() >= iTempPrice
						iBankAmount = iTempPrice
					ELSE
						iBankAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_BANK_BALANCE())
					ENDIF
					iTempPrice -= iBankAmount
				ENDIF
				IF iTempPrice > 0
					IF (NETWORK_GET_VC_WALLET_BALANCE() > 0)
						IF NETWORK_GET_VC_WALLET_BALANCE() >= iTempPrice
							iWalletAmount = iTempPrice
						ELSE
							iWalletAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_WALLET_BALANCE())
						ENDIF
						iTempPrice -= iWalletAmount
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				IF iTempPrice > 0
					CASSERTLN(DEBUG_SHOP_ROBBERIES, "PROCESSING_STORE_SHOPPING_BASKET - Player can't afford this item! iTempPrice=", iTempPrice, ", iWalletAmount=", iWalletAmount, ", iBankAmount", iBankAmount)
				ENDIF
				#ENDIF
				
				STRING sLabel
				sLabel = GET_SNACK_NAME(shopSnacks.iCurrentSelection)
				
				CPRINTLN(DEBUG_SHOP_ROBBERIES, "[BASKET] - Adding basket eCategory:", GET_SHOP_ITEM_CATEGORIES_DEBUG_STRING(eCategory), ", eAction:", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(eAction), ", sLabel:\"", sLabel, "\", variation:", GET_SNACK_VARIATION(shopSnacks), "\", iPrice:$", iPrice)
				
				TEXT_LABEL_63 tlCategoryKey
				GENERATE_SNACK_KEY_FOR_CATALOGUE(tlCategoryKey, sLabel, GET_SNACK_VARIATION(shopSnacks))
				
				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, eCategory, GET_HASH_KEY(tlCategoryKey), eAction, 1, iPrice, 1, CATALOG_ITEM_FLAG_WALLET_THEN_BANK)//, iInventoryKey)
                    IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
                        CPRINTLN(DEBUG_SHOP_ROBBERIES, "[BASKET] - basket checkout started for \"", tlCategoryKey, "\": ", GET_HASH_KEY(tlCategoryKey))
						
						CPRINTLN(DEBUG_SHOP_ROBBERIES, "CHANGE_FAKE_MP_CASH(wallet=$-", iWalletAmount, ", bank=$-", iBankAmount, "): iPrice=$", iPrice, ", bankBalance=$", NETWORK_GET_VC_BANK_BALANCE(), ", walletBalance=$", NETWORK_GET_VC_WALLET_BALANCE())
						USE_FAKE_MP_CASH(TRUE)
						CHANGE_FAKE_MP_CASH(-iWalletAmount, -iBankAmount)
						
						shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_PENDING
                    ELSE
                        CPRINTLN(DEBUG_SHOP_ROBBERIES, "[BASKET] - failed to start basket checkout for \"", tlCategoryKey, "\": ", GET_HASH_KEY(tlCategoryKey))
                        shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_FAILED
                    ENDIF
                ELSE
                    CPRINTLN(DEBUG_SHOP_ROBBERIES, "[BASKET] - failed to add \"", tlCategoryKey, "\": ", GET_HASH_KEY(tlCategoryKey))
                    shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_FAILED
                ENDIF
            BREAK
            // Pending
            CASE SHOP_MENU_BASKET_STAGE_PENDING
                IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
                    IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
                        CPRINTLN(DEBUG_SHOP_ROBBERIES, "[BASKET] - Basket transaction finished, success!")
                        shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_SUCCESS
                    ELSE
                        CPRINTLN(DEBUG_SHOP_ROBBERIES, "[BASKET] - failed to process transaction!")
                        shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_FAILED
                    ENDIF
                ENDIF
            BREAK
            
            // Success
            CASE SHOP_MENU_BASKET_STAGE_SUCCESS
                //DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
                shopSnacks.bProcessingBasket = FALSE
                shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
                
                shopSnacks.iProcessSuccess = 2
                RETURN FALSE
            BREAK
            
            //Failed
            CASE SHOP_MENU_BASKET_STAGE_FAILED
                DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
                
                shopSnacks.bProcessingBasket = FALSE
                shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
                
                shopSnacks.iProcessSuccess = 0
                RETURN FALSE
            BREAK
        ENDSWITCH
		
        RETURN TRUE
    ENDIF
    
    shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_ADD
    shopSnacks.iProcessSuccess = -1
    RETURN FALSE
ENDFUNC


STRUCT ServerBroadcastDataMenu
	NETWORK_INDEX niRegStaff
	
	INT iServerSnackBitSet
	SNACK_BUY_ANIM_STATE eSnackBuyAnimState = SNACKBUY_DONE
	
	STAFF_STAGE_ENUM eStaffState
	
	INT iTotalCashSpent
	INT iSnackInventory[SNACK_TOTAL]
ENDSTRUCT

STRUCT PlayerBroadcastDataMenu
	INT iPlayerBitSet
	
	INT iPlayerSnackBitSet
	INT iPlayerWhichSnack
	
	INT iCashSpent		
ENDSTRUCT

PROC HANDLE_SNACK_BAR_TAKE_ALL_FACILITY(MP_SHOP_SNACKS &localData, BOOL bIsOfficeSnackMenu)
	
	BOOL bSnacksAdded = FALSE
	IF NOT IS_HELD_AMOUNT_ABOVE_MAX(localData.iCurrentSelection)
		INT iToAdd = GET_SNACK_MAX(localData.iCurrentSelection) - GET_MP_INT_CHARACTER_STAT(GET_SNACK_MP_STAT(localData.iCurrentSelection))
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, GET_SNACK_MP_STAT_HASH_FOR_TELEMETRY(localData.iCurrentSelection), hash("SNACK"), iToAdd, GET_LOCATION_HASH_FOR_TELEMETRY(), hash("PURCHASE"),  TRUE, PICK_INT(bIsOfficeSnackMenu, hash("PA"), hash("STORE")))						
		SET_MP_INT_CHARACTER_STAT(GET_SNACK_MP_STAT(localData.iCurrentSelection), GET_SNACK_MAX(localData.iCurrentSelection))
		
		bSnacksAdded = TRUE
	ENDIF	
	
	IF bSnacksAdded	
		NETWORK_BUY_ITEM(0, GET_HASH_KEY(GET_SNACK_NAME(localData.iCurrentSelection)), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)	
	ENDIF
			
	IF NOT bSnacksAdded
		PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ELSE
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    shopSnacks - 
///    scenePosition - 
///    sceneRotation - 
///    serverBD_menu - 
///    playerBD_menu - 
///    bInBackRoom - 
///    bIsOfficeSnackMenu - True if we want to use shop menu isnide office snacks menu
PROC UPDATE_MP_SHOP_SNACKS(MP_SHOP_SNACKS &shopSnacks, VECTOR scenePosition, VECTOR sceneRotation, ServerBroadcastDataMenu &serverBD_menu, PlayerBroadcastDataMenu &playerBD_menu, BOOL bInBackRoom,BOOL bIsOfficeSnackMenu = FALSE)
	
	INT iPrice
	INT iMoneyPaid
	VECTOR vPlayerCoords
	VECTOR vClerkCoords
	
	IF serverBD_menu.iTotalCashSpent >= TOTAL_CASH_SPENT_LIMIT
	OR (serverBD_menu.iTotalCashSpent + shopSnacks.iCashSpentThisTranssation) >= TOTAL_CASH_SPENT_LIMIT
	OR IS_SHOP_ALL_SOLD_OUT(serverBD_menu.iSnackInventory)
	OR (HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSoldOutTimer) AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsAmbience.iSoldOutTimer, iSnackTimeOut))
		
		IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSoldOutTimer)
			IF (GET_GAME_TIMER() % 2500) < 50
				NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] sold out timer is at ") 
				NET_PRINT_INT(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.iSoldOutTimer))	NET_NL()
			ENDIF
		ENDIF
		
		IF NOT shopSnacks.bSnacksSoldOut
			NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] setting sold out flag to true") NET_NL()
			
			shopSnacks.bSnacksSoldOut = TRUE
			
			IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSoldOutTimer)
				NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] starting global sold out timer") NET_NL()
				
				START_NET_TIMER(MPGlobalsAmbience.iSoldOutTimer)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iSoldOutTimer)
			NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] resetting globale sold out timer") NET_NL()
			RESET_NET_TIMER(MPGlobalsAmbience.iSoldOutTimer)
		ENDIF
		
		IF shopSnacks.bSnacksSoldOut
			NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] clearing sold out flag") NET_NL()
			shopSnacks.bSnacksSoldOut = FALSE
		ENDIF
	ENDIF
	
	IF (GET_GAME_TIMER() % 5000) < 50
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] shop snack proc hit ")
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] shopSnacks.eShopSnacksState =  ", shopSnacks.eShopSnacksState)
		CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
	ENDIF
	
	IF serverBD_menu.eStaffState < eSTAFF_REACT_TO_WEAPON
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_BIT_SET(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
	AND NOT IS_PLAYER_UNDER_ATTACK()
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
	AND NOT bInBackRoom
	AND NOT IS_NET_PED_INJURED(serverBD_menu.niRegStaff)
	
		//INT iCurrentHealth = GET_ENTITY_HEALTH(PLAYER_PED_ID())
		//INT iMaxHealth = GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID())
		/*#IF IS_DEBUG_BUILD
		#IF FEATURE_HEIST_PLANNING
		DO_LOTTERY_LEADERBOARD_TEST()
		#ENDIF
		#ENDIF*/
		BOOL bSnackFull = FALSE
		BOOL bCursorAccept = FALSE // for mouse cursor
		STRING sTempLabel // Because the mouse cursor can now change the item description too
		
		IF IS_PAUSE_MENU_ACTIVE()
		AND shopSnacks.eShopSnacksState!= SHOPSNACKS_INIT
		AND NOT IS_COMMERCE_STORE_OPEN()
			IF !bIsOfficeSnackMenu
				shopSnacks.eShopSnacksState = SHOPSNACKS_INIT
				EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
			ENDIF	
		ENDIF
		
		SWITCH shopSnacks.eShopSnacksState
			CASE SHOPSNACKS_INIT
				//CHECK LOCATES HERE
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
				AND (NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()) 
				OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CHAL_ROB_SHOP)
				AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
					IF GET_SHOP_BUY_LOCATE_DATA(shopSnacks.eCurrentShop, scenePosition, sceneRotation.z,
												shopSnacks.vBuyEntryPos[0], shopSnacks.vBuyEntryPos[1], shopSnacks.vBuyLocateCenter, shopSnacks.fBuyEntrySize)
						vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
						vClerkCoords = GET_ENTITY_COORDS(NET_TO_PED(serverBD_menu.niRegStaff))
						IF (GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, shopSnacks.vBuyLocateCenter) < 5.0
							AND ABSF(vPlayerCoords.z - vClerkCoords.z) < 0.25)
						OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), shopSnacks.vBuyEntryPos[0], shopSnacks.vBuyEntryPos[1], shopSnacks.fBuyEntrySize)
							OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
								IF IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), shopSnacks.vBuyLocateCenter, 135.0)
								OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
								
									IF shopSnacks.bSnacksSoldOut
										
										IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
											NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] coming back from marketplace store is sold out") NET_NL()
											CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
										ENDIF
										
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_HOLDUP_1")
										AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
										AND NOT IS_SELECTOR_CAM_ACTIVE()
										AND IS_FLOW_HELP_QUEUE_EMPTY()
											PRINT_HELP("SHR_SOLD_OUT")
											
											NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] SHOPSNACKS_INIT -> SHOPSNACKS_SOLD_OUT") NET_NL()
											
											shopSnacks.eShopSnacksState = SHOPSNACKS_SOLD_OUT
										ENDIF
									ELSE
										
										IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
											NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] starting shop menu as a result of marketplace flag") NET_NL()
										ENDIF
										
										shopSnacks.iUseContext = NEW_CONTEXT_INTENTION
										REGISTER_CONTEXT_INTENTION(shopSnacks.iUseContext, CP_HIGH_PRIORITY, "SHR_MENU")
										PRINTLN("[AM HOLD UP] - REGISTER SHOP CONTEXT - A")
										
										NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] SHOPSNACKS_INIT -> SHOPSNACKS_WAIT_ACTIVATE") NET_NL()
										
										shopSnacks.eShopSnacksState = SHOPSNACKS_WAIT_ACTIVATE_MENU
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() % 8000) < 50
										CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
										CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] player heading not facing counter ")
										CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
									ENDIF
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() % 8000) < 50
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] player not in angled area ")
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] entry pos 0: ", shopSnacks.vBuyEntryPos[0])
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] entry pos 1: ", shopSnacks.vBuyEntryPos[1])
									CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
								ENDIF
								
								DRAW_DEBUG_LINE(vPlayerCoords, shopSnacks.vBuyEntryPos[0])
								DRAW_DEBUG_LINE(vPlayerCoords, shopSnacks.vBuyEntryPos[1])
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() % 8000) < 50
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] player coords: ", vPlayerCoords)
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] locate center: ", shopSnacks.vBuyLocateCenter)
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] dist btw player and locate: ", GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, shopSnacks.vBuyLocateCenter))
								CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
							ENDIF
							
							DRAW_DEBUG_LINE(vPlayerCoords, shopSnacks.vBuyLocateCenter)
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] could not get locate data ")
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
					ENDIF
				ELSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
						CLEAR_HELP()
						PRINTLN("[AM HOLD UP] - CLEAR HELP 'SHR_MENU' - AA")
					ENDIF
					
					// Do help text for players on Hot Property and Dead Drop
					IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
						IF NOT IS_BIT_SET(shopSnacks.iBoolsBitSet2, bi2FMEventCriticalHelp)						
							PRINT_HELP("SHR_FM_CRIT")
							SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_MP_FREEMODE, HUD_COLOUR_PURPLE, 200)
							SET_BIT(shopSnacks.iBoolsBitSet2, bi2FMEventCriticalHelp)
						ENDIF						
					ENDIF
					
					IF (GET_GAME_TIMER() % 8000) < 50
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] IS_PAUSE_MENU_ACTIVE() = ", IS_PAUSE_MENU_ACTIVE())
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) = ", IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] IS_PED_RUNNING(PLAYER_PED_ID()) = ", IS_PED_RUNNING(PLAYER_PED_ID()))
						CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
					ENDIF
				ENDIF
			BREAK
			
			CASE SHOPSNACKS_WAIT_ACTIVATE_MENU
				vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
				vClerkCoords = GET_ENTITY_COORDS(NET_TO_PED(serverBD_menu.niRegStaff))
				IF (NOT IS_PED_HEADING_TOWARDS_POSITION(PLAYER_PED_ID(), shopSnacks.vBuyLocateCenter, 135.0)
					OR ABSF(vPlayerCoords.z - vClerkCoords.z) >= 0.25)
				AND NOT SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] Player left aisle, going back to SHOPSNACKS_INIT") NET_NL()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
						CLEAR_HELP()
						PRINTLN("[AM HOLD UP] - CLEAR HELP 'SHR_MENU' - A")
					ENDIF
					
					RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
					
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] jumping back to _init") NET_NL()
					shopSnacks.eShopSnacksState = SHOPSNACKS_INIT
				ENDIF
				
				IF IS_PAUSE_MENU_ACTIVE()
					PRINTLN("*****[_mp_shop_snacks] IS_PAUSE_MENU_ACTIVE = TRUE")
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] Pause Menu Active, going back to SHOPSNACKS_INIT") NET_NL()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
						CLEAR_HELP()
						PRINTLN("[AM HOLD UP] - CLEAR HELP 'SHR_MENU' - P")
					ENDIF
					
					RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
					
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] jumping back to _init - P") NET_NL()
					shopSnacks.eShopSnacksState = SHOPSNACKS_INIT
				ELSE
					PRINTLN("*****[_mp_shop_snacks] IS_PAUSE_MENU_ACTIVE = FALSE")
				ENDIF
				
				IF (IS_PHONE_ONSCREEN() OR IS_CELLPHONE_CAMERA_IN_USE())
					PRINTLN("*****[_mp_shop_snacks] IS_CELLPHONE_CAMERA_IN_USE = TRUE")
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] Cellphone Camera Active, going back to SHOPSNACKS_INIT") NET_NL()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
						CLEAR_HELP()
						PRINTLN("[AM HOLD UP] - CLEAR HELP 'SHR_MENU' - P")
					ENDIF
					
					RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
					
					CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(shopsnacks.eCurrentShop))
					CLEAR_BIT(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
					
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] jumping back to _init - P") NET_NL()
					shopSnacks.eShopSnacksState = SHOPSNACKS_INIT
				ELSE
					PRINTLN("*****[_mp_shop_snacks] IS_CELLPHONE_CAMERA_IN_USE = FALSE")
				ENDIF
								
				IF NOT IS_PED_RUNNING(PLAYER_PED_ID())
					IF HAS_CONTEXT_BUTTON_TRIGGERED(shopSnacks.iUseContext)
					OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] SHOPSNACKS_WAIT_ACTIVATE -> SHOPSNACKS_BUY") NET_NL()
						
						IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] clearing marketplace flag since player is back and menu is activated") NET_NL()
							CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						ENDIF
						
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")
							CLEAR_HELP()
							PRINTLN("[AM HOLD UP] - CLEAR HELP 'SHR_MENU' - B")
						ENDIF
						
						RESTART_TIMER_NOW(shopSnacks.timerSnackReset)
						
						RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
						
						SET_TEMP_PASSIVE_MODE()
						SET_BIT(shopSnacks.iBoolsBitSet2, bi2TempPassiveModeEnabled)
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
						
						THEFEED_FLUSH_QUEUE()
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						AND NOT IS_NET_PED_INJURED(serverBD_menu.niRegStaff)
							TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), NET_TO_PED(serverBD_menu.niRegStaff))
							CLEAR_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_SnackTaskDone)
						ENDIF
						
						SET_CURSOR_POSITION_FOR_MENU()	// B*2279463 - Moving this to here, as the menu gets rebuilt often, and cursor pos should not be reset every time this happens.
						shopSnacks.eShopSnacksState = SHOPSNACKS_SETUP_MENU
					ENDIF
				ENDIF
			BREAK
			CASE SHOPSNACKS_SETUP_MENU
				
				IF !bIsOfficeSnackMenu
				// let's do cams later
				IF NOT DOES_CAM_EXIST(shopSnacks.ciSnacks)
					shopSnacks.ciSnacks = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<0,0,0>>, <<0,0,0>>, 50, FALSE)
					
					FLOAT fov
					
					GET_SHOP_SNACK_CAM_DATA_NO_OFFSET(shopSnacks.eCurrentShop, shopSnacks.vCamPos, shopSnacks.vCamRot, fov)
					SET_CAM_COORD(shopSnacks.ciSnacks, shopSnacks.vCamPos)
					SET_CAM_ROT(shopSnacks.ciSnacks, shopSnacks.vCamRot)
					SET_CAM_FOV(shopSnacks.ciSnacks, fov)
					SHAKE_CAM(shopSnacks.ciSnacks, "HAND_SHAKE", 0.1)
					SET_CAM_ACTIVE(shopSnacks.ciSnacks, TRUE)
					
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					
				ENDIF
				shopSnacks.szHeaderTxd = GET_SHOP_HEADER_TEXTURE_STRING(shopSnacks.eCurrentShop)
				ENDIF
				
				// Paranoid check, sometimes this doesn't take the first go - B.S.
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				//shopSnacks.iCurrentSelection = 0
				shopSnacks.iMenuLength = GET_NUMBER_OF_SNACKS()	//6 //4
				
				CLEAR_MENU_DATA()
				
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				SET_MENU_TITLE("SNK_ITEM")
				
				IF !bIsOfficeSnackMenu
					SET_MENU_USES_HEADER_GRAPHIC(TRUE, shopSnacks.szHeaderTxd, shopSnacks.szHeaderTxd)
				ENDIF
				
				INT idx
				REPEAT shopSnacks.iMenuLength idx
					/*#IF IS_DEBUG_BUILD
					#IF FEATURE_HEIST_PLANNING
					IF idx != ENUM_TO_INT(SNACK_LOTTERY)
					OR (idx = ENUM_TO_INT(SNACK_LOTTERY) AND g_sMPTunables.bdisable_lottery = FALSE)
					#ENDIF
					#ENDIF*/
						bSnackFull = IS_HELD_AMOUNT_ABOVE_MAX(idx)
						ADD_MENU_ITEM_TEXT(idx, GET_SNACK_NAME(idx), 0, !bSnackFull)
						IF serverBD_menu.iSnackInventory[idx] >= iSnackStock
						OR ( HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iItemSoldOutTimer[idx]) 
							AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsAmbience.iItemSoldOutTimer[idx], iSnackItemTimeout)
							AND ENUM_TO_INT(shopSnacks.eCurrentShop) = MPGlobalsAmbience.iLastShop )
							ADD_MENU_ITEM_TEXT(idx, "", 1)
							ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
							
							IF ( HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iItemSoldOutTimer[idx]) 
								AND NOT HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsAmbience.iItemSoldOutTimer[idx], iSnackItemTimeout) )
								serverBD_menu.iSnackInventory[idx] = iSnackStock
							ENDIF
							
							IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iItemSoldOutTimer[idx])
								NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] starting global item sold out timer") NET_NL()
								MPGlobalsAmbience.iLastShop = ENUM_TO_INT(shopSnacks.eCurrentShop)
								START_NET_TIMER(MPGlobalsAmbience.iItemSoldOutTimer[idx])
							ENDIF
							
						ELIF bSnackFull
							ADD_MENU_ITEM_TEXT(idx, "SNK_FULL", 0, !bSnackFull)
						ELSE
							/*#IF IS_DEBUG_BUILD
							#IF FEATURE_HEIST_PLANNING
							IF (idx = ENUM_TO_INT(SNACK_LOTTERY) AND IS_BIT_SET(iLotteryBitSet, biL_BuyingTicketsDisabled))
								ADD_MENU_ITEM_TEXT(idx, "", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
							ELSE
							#ENDIF
							#ENDIF*/
								IF NOT bIsOfficeSnackMenu
									ADD_MENU_ITEM_TEXT(idx, "ITEM_COST", 1)
									ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_SNACK_PRICE(shopSnacks, idx))
								ELSE
									ADD_MENU_ITEM_TEXT(idx, "SNK_FREE")
								ENDIF
							/*#IF IS_DEBUG_BUILD
							#IF FEATURE_HEIST_PLANNING
							ENDIF
							#ENDIF
							#ENDIF*/
						ENDIF
					/*#IF IS_DEBUG_BUILD
					#IF FEATURE_HEIST_PLANNING
					ENDIF
					#ENDIF
					#ENDIF*/
				ENDREPEAT
				
				SET_TOP_MENU_ITEM(0)
				SET_CURRENT_MENU_ITEM(shopSnacks.iCurrentSelection)
				IF shopSnacks.bSnackPurchased
					IF !bIsOfficeSnackMenu
						SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_BOUGHT")
					ELSE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_STORED")
					ENDIF
				ELIF serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection] >= iSnackStock
					SET_CURRENT_MENU_ITEM_DESCRIPTION("SNK_SOUT")
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection))
				ENDIF
				
//				SET_CURSOR_POSITION_FOR_MENU()	// B*2279463 - Moving this out of setup state, as the menu gets rebuilt often, and cursor pos should not be reset every time this happens.
				
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")	// B*2132845 - Adding mouse menu support: Made these help keys clickable, note that if lottery tickets are re-enabled there'll need to be a different method for switching the help key on and off than re-adding them every frame (as this breaks the clickableness, see below)
				IF bIsOfficeSnackMenu
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "STORE_TAKE_ALL")
				ENDIF
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
				/*#IF IS_DEBUG_BUILD
				#IF FEATURE_HEIST_PLANNING
				IF shopSnacks.iCurrentSelection != ENUM_TO_INT(SNACK_LOTTERY)
				#ENDIF
				#ENDIF*/
				IF !bIsOfficeSnackMenu
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "SNK_LIFT")
					DRAW_MENU(TRUE, ENUM_TO_INT(shopSnacks.eCurrentShop))
				ENDIF
				/*#IF IS_DEBUG_BUILD
				#IF FEATURE_HEIST_PLANNING
				ENDIF
				#ENDIF
				#ENDIF*/
				
				//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
				
				
				
				shopSnacks.eShopSnacksState = SHOPSNACKS_UPDATE_MENU
			BREAK
			CASE SHOPSNACKS_UPDATE_MENU
				// Paranoid check. Sometimes this doesn't take on the first go - B.S.
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				ENDIF
				
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) = FINISHED_TASK
					IF NOT IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_SnackTaskDone)
						IF !bIsOfficeSnackMenu
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE )
						ELSE
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
						SET_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_SnackTaskDone)
					ENDIF
				ENDIF
				
//				REMOVE_MENU_HELP_KEYS()												// B*2132845 - Adding mouse menu support: Had to remove this, as clickable help buttons cannot work if re-added every frame
//				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
//				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
//				/*#IF IS_DEBUG_BUILD
//				#IF FEATURE_HEIST_PLANNING
//				IF shopSnacks.iCurrentSelection != ENUM_TO_INT(SNACK_LOTTERY)
//				#ENDIF
//				#ENDIF*/
//					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "SNK_LIFT")
//				/*#IF IS_DEBUG_BUILD
//				#IF FEATURE_HEIST_PLANNING
//				ENDIF
//				#ENDIF
//				#ENDIF*/
				
				///////////////////////////////////
				// Mouse menu support
				bCursorAccept = FALSE
				
				IF IS_PC_VERSION()	
					
					IF IS_USING_CURSOR(FRONTEND_CONTROL)
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
						
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
						ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
											
						HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
						HANDLE_MENU_CURSOR(FALSE)
						
						IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						
							IF g_iMenuCursorItem != shopSnacks.iCurrentSelection
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
								shopSnacks.iCurrentSelection = g_iMenuCursorItem
								
								/*#IF IS_DEBUG_BUILD		// B*2132845 - Adding mouse menu support: Note that this option would probably need to be disabled in the menu setup, rather than skipping it here after it's been selected
								#IF FEATURE_HEIST_PLANNING
								IF g_sMPTunables.bdisable_lottery = TRUE
									IF shopSnacks.iCurrentSelection = ENUM_TO_INT(SNACK_LOTTERY)
										PRINTLN("[AM HOLD UP] - MOVE UP - SKIP LOTTERY IT HAS BEEN BLOCKED BY A TUNABLE")
										shopSnacks.iCurrentSelection -= 1
										IF (shopSnacks.iCurrentSelection < 0)
											shopSnacks.iCurrentSelection = shopSnacks.iMenuLength - 1
										ENDIF
									ENDIF
								ENDIF
								#ENDIF
								#ENDIF*/
								
								SET_CURRENT_MENU_ITEM(shopSnacks.iCurrentSelection)
								
								shopSnacks.bSnackAttempted = FALSE
								shopSnacks.bCheckMoney = FALSE
								shopSnacks.bCheckHealth = FALSE
								shopSnacks.bCheckInventory = FALSE
								
								sTempLabel = GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection)
								IF DOES_TEXT_LABEL_EXIST(sTempLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION(sTempLabel)
								ENDIF
								
							ELSE
								bCursorAccept = TRUE
							ENDIF						
						
						ENDIF
					
					ENDIF
				
				ENDIF
				
				IF !bIsOfficeSnackMenu
					DRAW_MENU(TRUE, ENUM_TO_INT(shopSnacks.eCurrentShop))
				ENDIF
				UPDATE_SNACK_MENU(shopSnacks, serverBD_menu.iSnackInventory,bIsOfficeSnackMenu)
				
				IF serverBD_menu.eSnackBuyAnimState > SNACKBUY_CLEANUP_ANIM //SNACKBUY_PLAY_ANIM
				AND IS_BIT_SET(serverBD_menu.iServerSnackBitSet, biSS_SnackBought)
				AND IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_SnackBought)
					CLEAR_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_SnackBought)
					NET_PRINT_TIME() NET_PRINT("*****[snack_anims] cleared biPS_SnackBought flag") NET_NL()
				ENDIF
				
				IF bIsOfficeSnackMenu
				AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					HANDLE_SNACK_BAR_TAKE_ALL_FACILITY(shopSnacks, bIsOfficeSnackMenu)
					shopSnacks.bSnackPurchased = TRUE
					shopSnacks.eShopSnacksState = SHOPSNACKS_SETUP_MENU
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				OR bCursorAccept = TRUE
				OR shopSnacks.bProcessingBasket
					
					IF NOT shopSnacks.bProcessingBasket
						shopSnacks.bSnackAttempted = TRUE
						shopSnacks.bCheckMoney = FALSE
						shopSnacks.bCheckHealth = FALSE
						shopSnacks.bCheckInventory = FALSE
						
						RESTART_TIMER_NOW(shopSnacks.timerSnackReset)
					ENDIF
					
					IF shopSnacks.bSnackAfford
					AND shopSnacks.bSnackNeeded
					AND shopSnacks.bSnackAvailable
						
					    IF USE_SERVER_TRANSACTIONS()
							CPRINTLN(DEBUG_SHOP_ROBBERIES, "UPDATE_MP_SHOP_SNACKS:Purchase: shopSnacks.eShopSnacksState = SHOPSNACKS_BUY_SERVER")
							shopSnacks.bProcessingBasket = TRUE
							shopSnacks.iProcessingBasketStage = SHOP_MENU_BASKET_STAGE_ADD
							shopSnacks.iProcessSuccess = -1
							
							iPrice = 0
							IF NOT bIsOfficeSnackMenu
								iPrice = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							
							PROCESSING_STORE_SHOPPING_BASKET(shopSnacks, CATEGORY_MART, NET_SHOP_ACTION_SPEND, iPrice)
							shopSnacks.eShopSnacksState = SHOPSNACKS_BUY_SERVER
					    ELSE
					        CPRINTLN(DEBUG_SHOP_ROBBERIES, "UPDATE_MP_SHOP_SNACKS:Purchase: ignore NETWORK_REQUEST_BASKET_TRANSACTION")
								
							shopSnacks.bSnackPurchased = TRUE
							
							PLAY_SOUND_FRONTEND(-1, "PURCHASE", "HUD_LIQUOR_STORE_SOUNDSET")
							
							serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection]++
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] inventory for ") NET_PRINT_INT(shopSnacks.iCurrentSelection) NET_PRINT(" is now: ") 
							NET_PRINT_INT(serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection]) NET_NL()
							
							shopSnacks.eShopSnacksState = SHOPSNACKS_BUY
					    ENDIF	
						
					ELSE
						
						shopSnacks.bSnackPurchased = FALSE
						
						IF NOT shopSnacks.bSnackAfford
						AND NOT shopSnacks.bSkipCashAlert
							// go to warning state
							iPrice = 0
							IF NOT bIsOfficeSnackMenu
								iPrice = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							
							STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection)), iPrice, 0)
							LAUNCH_STORE_CASH_ALERT(DEFAULT, DEFAULT, SPL_STORE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] setting marketplace flag") NET_NL()
							shopSnacks.eShopSnacksState = SHOPSNACKS_STORE_ALERT
						ENDIF
						
						PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_LIQUOR_STORE_SOUNDSET")
					ENDIF
					
					IF NOT shopSnacks.bSnackAvailable
						shopSnacks.eShopSnacksState = SHOPSNACKS_SETUP_MENU
					ENDIF
					
				ENDIF
				
				IF !bIsOfficeSnackMenu
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					AND NOT shopSnacks.bProcessingBasket
						/*#IF IS_DEBUG_BUILD
						#IF FEATURE_HEIST_PLANNING
						IF shopSnacks.iCurrentSelection != ENUM_TO_INT(SNACK_LOTTERY)
						#ENDIF
						#ENDIF*/
							shopSnacks.bSnackAttempted = TRUE
							
							IF serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection] < iSnackStock
								shopSnacks.bSnackAvailable = TRUE
							ELSE
								shopSnacks.bSnackAvailable = FALSE
							ENDIF
							
							RESTART_TIMER_NOW(shopSnacks.timerSnackReset)
							
							PRINTLN("Current inventory count is: ", serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection])
							IF NOT shopSnacks.bSnackAvailable
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_LIQUOR_STORE_SOUNDSET")
								PRINTLN("Could not shoplift. The item is out of stock")
							ELIF NOT shopSnacks.bSnackNeeded
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_LIQUOR_STORE_SOUNDSET")
								PRINTLN("Could not shoplift. player has too much of this item")
							ELSE
								TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_SHOPLIFT_ANIM_DICT(), GET_SHOPLIFT_ANIM_HEIGHT(), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON)
								
								shopSnacks.eShopSnacksState = SHOPSNACKS_SHOPLIFT2
								shopSnacks.iHoldTime = GET_GAME_TIMER()
								PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_LIQUOR_STORE_SOUNDSET")
								
								IF !bIsOfficeSnackMenu
									EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
								ENDIF	
							ENDIF
						/*#IF IS_DEBUG_BUILD
						#IF FEATURE_HEIST_PLANNING
						ENDIF
						#ENDIF
						#ENDIF*/
					ENDIF
				ENDIF	
				
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
					IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] clearing marketplace flag since player exited menu") NET_NL()
						CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
					ENDIF
					
					shopSnacks.eShopSnacksState = SHOPSNACKS_RESET
					shopSnacks.iHoldTime = GET_GAME_TIMER()
					
					
					IF !bIsOfficeSnackMenu
						PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_LIQUOR_STORE_SOUNDSET")
						EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
					ELSE
						PLAY_SOUND_FRONTEND(-1, "BACK","HUD_FREEMODE_SOUNDSET")
						g_bCancelOfficeSnacksMenu = TRUE
					ENDIF	
				ENDIF
				
				IF shopSnacks.bSnacksSoldOut
					shopSnacks.eShopSnacksState = SHOPSNACKS_SOLD_OUT
					shopSnacks.iHoldTime = GET_GAME_TIMER()
					
					IF !bIsOfficeSnackMenu
						EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
					ENDIF
					
					PRINT_HELP("SHR_SOLD_OUT")
				ENDIF
				
				INT idy
				REPEAT shopSnacks.iMenuLength idy
					IF (HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iItemSoldOutTimer[idy]) AND HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsAmbience.iItemSoldOutTimer[idy], iSnackItemTimeout))
						NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] resetting globale sold out timer") NET_NL()
						RESET_NET_TIMER(MPGlobalsAmbience.iItemSoldOutTimer[idy])
						serverBD_menu.iSnackInventory[idy] = 0
						shopSnacks.eShopSnacksState = SHOPSNACKS_SETUP_MENU
					ENDIF
				ENDREPEAT
				
			BREAK
			CASE SHOPSNACKS_BUY_SERVER
				iPrice = 0
				IF !bIsOfficeSnackMenu
					DRAW_MENU(TRUE, ENUM_TO_INT(shopSnacks.eCurrentShop))
					iPrice = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
				ENDIF
				IF PROCESSING_STORE_SHOPPING_BASKET(shopSnacks, CATEGORY_MART, NET_SHOP_ACTION_SPEND, iPrice)
		            CDEBUG3LN(DEBUG_SHOP_ROBBERIES, "UPDATE_MP_SHOP_SNACKS: processing stage ", shopSnacks.iProcessingBasketStage, "...")
				ELSE
			        SWITCH shopSnacks.iProcessSuccess
			            CASE 0
			                CWARNINGLN(DEBUG_SHOP_ROBBERIES, "UPDATE_MP_SHOP_SNACKS:Purchase: failed to process transaction [bSnackAfford: ", GET_STRING_FROM_BOOL(shopSnacks.bSnackAfford), ", bSkipCashAlert: ", GET_STRING_FROM_BOOL(shopSnacks.bSkipCashAlert) , "]")
							shopSnacks.bSnackPurchased = FALSE
							
							IF NOT shopSnacks.bSnackAfford
							AND NOT shopSnacks.bSkipCashAlert
								IF NOT bIsOfficeSnackMenu
									iPrice = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
								ENDIF
				
								// go to warning state
								STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(GET_SNACK_DESCRIPTION(shopSnacks.iCurrentSelection)), iPrice, 0)
								LAUNCH_STORE_CASH_ALERT(DEFAULT, DEFAULT, SPL_STORE)
								SET_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
								SET_TRANSITION_SESSION_SHOP_STATE_INITIALISED() // dont wait for initialisation on return
								NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] setting marketplace flag") NET_NL()
								shopSnacks.eShopSnacksState = SHOPSNACKS_STORE_ALERT
							ELSE
								IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
									NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] - [STORETRANS] clearing marketplace flag since player exited menu") NET_NL()
									CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
								ENDIF
								
								shopSnacks.eShopSnacksState = SHOPSNACKS_RESET
								shopSnacks.iHoldTime = GET_GAME_TIMER()
								PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_LIQUOR_STORE_SOUNDSET")
								
								IF !bIsOfficeSnackMenu
									EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
								ENDIF	
							ENDIF
			            BREAK
			            CASE 2
			                CPRINTLN(DEBUG_SHOP_ROBBERIES, "UPDATE_MP_SHOP_SNACKS:Purchase: success!!")
								
							shopSnacks.bSnackPurchased = TRUE

							PLAY_SOUND_FRONTEND(-1, "PURCHASE", "HUD_LIQUOR_STORE_SOUNDSET")
							
							serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection]++
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] inventory for ") NET_PRINT_INT(shopSnacks.iCurrentSelection) NET_PRINT(" is now: ") 
							NET_PRINT_INT(serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection]) NET_NL()
							
							shopSnacks.eShopSnacksState = SHOPSNACKS_BUY
			            BREAK
			            DEFAULT
			                CASSERTLN(DEBUG_SHOP_ROBBERIES, "UPDATE_MP_SHOP_SNACKS:Purchase: unknown iProcessSuccess: \"", shopSnacks.iProcessSuccess, "\"")
			            BREAK
			        ENDSWITCH
		        ENDIF
			BREAK
			CASE SHOPSNACKS_BUY
				// anim?
				IF !bIsOfficeSnackMenu
					DRAW_MENU(TRUE, ENUM_TO_INT(shopSnacks.eCurrentShop))
				ENDIF
				IF USE_SERVER_TRANSACTIONS()
					PRINTLN("[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
					NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
				ENDIF
				
				IF bIsOfficeSnackMenu
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_ORDERED_SNACK)						
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_ORDERED_SNACK)
						CPRINTLN(DEBUG_SHOP_ROBBERIES, "NETWORK_SPENT_PA_SERVICE_SNACK success!!")
					ENDIF
					NETWORK_SPENT_PA_SERVICE_SNACK(0,shopSnacks.iCurrentSelection,FALSE,TRUE)
					iMoneyPaid = 0
					SWITCH INT_TO_ENUM(SNACK_CATEGORIES, shopSnacks.iCurrentSelection)
						CASE SNACK_SMALL
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, 1)
							SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("PA"))
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_YUM_SNACKS (P's & Q's) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_YUM_SNACKS (P's & Q's) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS)) NET_NL()
						BREAK
						CASE SNACK_MEDIUM
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, 1)
							SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("PA"))
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NO_BOUGHT_HEALTH_SNACKS (EGOCHASER) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NO_BOUGHT_HEALTH_SNACKS (EGOCHASER) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS)) NET_NL()
						BREAK
						CASE SNACK_LARGE
							SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE)
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("PA"))
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_EPIC_SNACKS (METEORITE) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_EPIC_SNACKS (METEORITE) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS)) NET_NL()
						BREAK
						CASE SNACK_SMOKES
							shopSnacks.iIncrease = INV_SMOKES_PER_PACK
							IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT) + INV_SMOKES_PER_PACK) > INV_SMOKES_MAX
								shopSnacks.iIncrease = (INV_SMOKES_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT))
							ENDIF
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_CIGARETTES_BOUGHT_v0"), hash("SNACK"), shopSnacks.iIncrease,  GET_LOCATION_HASH_FOR_TELEMETRY(),hash("PURCHASE"),  FALSE, hash("PA"))
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, shopSnacks.iIncrease)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_CIGARETTES_BOUGHT (SMOKES) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_CIGARETTES_BOUGHT (SMOKES) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT)) NET_NL()
						BREAK
						CASE SNACK_SODA
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("PA"))
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_ORANGE_BOUGHT (eCOLA) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_ORANGE_BOUGHT (eCOLA) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT)) NET_NL()
						BREAK
						CASE SNACK_BEER
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_BOURGE_BOUGHT_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("PA"))
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_BOURGE_BOUGHT (PISSWASSER) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_BOURGE_BOUGHT (PISSWASSER) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT)) NET_NL()
						BREAK
						#IF FEATURE_DLC_1_2022
						CASE SNACK_SPRUNK
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_SPRUNK_BOUGHT_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("PA"))
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_SPRUNK_BOUGHT (eCOLA) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_SPRUNK_BOUGHT (eCOLA) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT)) NET_NL()
						BREAK
						#ENDIF
					ENDSWITCH
				ELSE
							
					SWITCH INT_TO_ENUM(SNACK_CATEGORIES, shopSnacks.iCurrentSelection)
						CASE SNACK_SMALL
						
							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							/*IF (iCurrentHealth + INV_SNACK_1_HEALTH) > iMaxHealth
								SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
							ELSE
								SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + INV_SNACK_1_HEALTH))
							ENDIF*/
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY()  ,hash("PURCHASE"),  FALSE, hash("STORE"))
							SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE)
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM1"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_YUM_SNACKS (P's & Q's) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_YUM_SNACKS (P's & Q's) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS)) NET_NL()
						BREAK 
						CASE SNACK_MEDIUM
							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							/*IF (iCurrentHealth + INV_SNACK_2_HEALTH) > iMaxHealth
								SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
							ELSE
								SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + INV_SNACK_2_HEALTH))
							ENDIF*/
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0"), hash("SNACK"), 1,  GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("STORE"))
							
							SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE)
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM2"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NO_BOUGHT_HEALTH_SNACKS (EGOCHASER) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NO_BOUGHT_HEALTH_SNACKS (EGOCHASER) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS)) NET_NL()
						BREAK
						CASE SNACK_LARGE

							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE)
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, 1)
							/*IF (iCurrentHealth + 15) > iMaxHealth
								SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
							ELSE
								SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 15))
							ENDIF*/
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("STORE"))
							
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM3"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_EPIC_SNACKS (METEORITE) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - PACKED_MP_BOUGHT_EPIC_SNACKS (METEORITE) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS)) NET_NL()
						BREAK
						CASE SNACK_SMOKES
							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							shopSnacks.iIncrease = INV_SMOKES_PER_PACK
							IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT) + INV_SMOKES_PER_PACK) > INV_SMOKES_MAX
								shopSnacks.iIncrease = (INV_SMOKES_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT))
							ENDIF
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, shopSnacks.iIncrease)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_CIGARETTES_BOUGHT_v0"), hash("SNACK"), shopSnacks.iIncrease, GET_LOCATION_HASH_FOR_TELEMETRY()  ,hash("PURCHASE"),  FALSE, hash("STORE"))
							
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM4"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_CIGARETTES_BOUGHT (SMOKES) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_CIGARETTES_BOUGHT (SMOKES) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT)) NET_NL()
						BREAK
						CASE SNACK_SODA
							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("STORE"))
							
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM5"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_ORANGE_BOUGHT (eCOLA) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_ORANGE_BOUGHT (eCOLA) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT)) NET_NL()
						BREAK
						#IF FEATURE_DLC_1_2022
						CASE SNACK_SPRUNK
							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF					
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_SPRUNK_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("STORE"))
							
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM7"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_SPRUNK_BOUGHT - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_SPRUNK_BOUGHT - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT)) NET_NL()
						BREAK
						#ENDIF
						CASE SNACK_BEER
							IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
								iMoneyPaid = 0
							ELSE
								iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							ENDIF
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT, 1)
							SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_BOURGE_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("PURCHASE"),  FALSE, hash("STORE"))
							
							NETWORK_BUY_ITEM(iMoneyPaid, GET_HASH_KEY("SNK_ITEM6"), PURCHASE_FOOD, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_BOURGE_BOUGHT (PISSWASSER) - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - MP_STAT_NUMBER_OF_BOURGE_BOUGHT (PISSWASSER) - TOTAL = ") NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT)) NET_NL()
						BREAK
						/*#IF IS_DEBUG_BUILD
						#IF FEATURE_HEIST_PLANNING
						CASE SNACK_LOTTERY
							iMoneyPaid = GET_SNACK_PRICE(shopSnacks, shopSnacks.iCurrentSelection)
							iLotteryTicketsJustBought++
							NETWORK_BUY_LOTTERY_TICKET(iMoneyPaid, 1, FALSE, TRUE)
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_LOTTERY, iMoneyPaid)
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - LOTTERY TICKET - COST $") NET_PRINT_INT(iMoneyPaid) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - LOTTERY TICKET JUST BOUGHT = ") NET_PRINT_INT(iLotteryTicketsJustBought) NET_NL()
							NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] BOUGHT - LOTTERY TICKET TOTAL BOUGHT = ") NET_PRINT_INT(iLotteryTicketsJustBought+iLotteryTicketsAlreadyBought) NET_NL()
						BREAK
						#ENDIF
						#ENDIF*/
						
						
						
					ENDSWITCH
				ENDIF
				
				IF USE_SERVER_TRANSACTIONS()
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
					
				//DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_SNACKS, iMoneyPaid)
				
				GIVE_LOCAL_PLAYER_CASH(-iMoneyPaid)
				
				shopSnacks.iCashSpentThisTranssation += iMoneyPaid
				
				// dialog
				IF NOT IS_BIT_SET(playerBD_menu.iPlayerBitSet, biP_ReactToSnackBuy)
					SET_BIT(playerBD_menu.iPlayerBitSet, biP_ReactToSnackBuy)
				ENDIF
				
				IF NOT IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_SnackBought)
					SET_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_SnackBought)
					playerBD_menu.iPlayerWhichSnack = shopSnacks.iCurrentSelection
					NET_PRINT_TIME() NET_PRINT("*****[snack_anims] set biPS_SnackBought flag") NET_NL()
					NET_PRINT_TIME() NET_PRINT("*****[snack_anims] whichSnack set to ") NET_PRINT_INT(playerBD_menu.iPlayerWhichSnack) NET_NL()
				ENDIF
				
				IF shopSnacks.bSnacksSoldOut
				OR shopSnacks.iCashSpentThisTranssation >= TOTAL_CASH_SPENT_LIMIT
					shopSnacks.eShopSnacksState = SHOPSNACKS_SOLD_OUT
					IF !bIsOfficeSnackMenu
						EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
					ENDIF
					
					PRINT_HELP("SHR_SOLD_OUT")
				
				ELIF serverBD_menu.iSnackInventory[shopSnacks.iCurrentSelection] >= iSnackStock
				OR IS_HELD_AMOUNT_ABOVE_MAX(shopSnacks.iCurrentSelection)
				
					shopSnacks.eShopSnacksState = SHOPSNACKS_SETUP_MENU
				
				ELSE
					NET_PRINT_TIME() NET_PRINT("*****[_mp_shop_snacks] snack bought, going to _RESET") NET_NL()
					shopSnacks.eShopSnacksState = SHOPSNACKS_UPDATE_MENU
				ENDIF
			BREAK
			CASE SHOPSNACKS_SHOPLIFT2
				// No matter what, we want this shoplifting item to track the player.
				SET_XP_ANIM_TO_TRACK_PLAYER(TRUE)
				
				SWITCH INT_TO_ENUM(SNACK_CATEGORIES, shopSnacks.iCurrentSelection)
					CASE SNACK_SMALL
						//IF (iCurrentHealth + 10) > iMaxHealth
						//	SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						//ELSE
						//	SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 10))
						//ENDIF
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_SNACK_1, 1, HUD_ANIM_PICKUP_INT)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY() ,hash("STEAL"),  FALSE, hash("STORE"))//,  0)
							
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, 1)
						SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_SNACKS, TRUE)
					BREAK
					CASE SNACK_MEDIUM
						//IF (iCurrentHealth + 20) > iMaxHealth
						//	SET_ENTITY_HEALTH(PLAYER_PED_ID(), iMaxHealth)
						//ELSE
						//	SET_ENTITY_HEALTH(PLAYER_PED_ID(), (iCurrentHealth + 20))
						//ENDIF
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_SNACK_2, 1, HUD_ANIM_PICKUP_INT)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, 1)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY(),hash("STEAL"),  FALSE, hash("STORE"))//,  0)
						
						SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_SNACKS, TRUE)
					BREAK
					CASE SNACK_LARGE
						// this item is locked
						//iMoneyPaid = 10
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_SNACK_3, 1, HUD_ANIM_PICKUP_INT)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, 1)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY(),hash("STEAL"),  FALSE, hash("STORE"))//,  0)
						
						SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_SNACKS, TRUE)
					BREAK
					CASE SNACK_SMOKES
						shopSnacks.iIncrease = INV_SMOKES_PER_PACK
						IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT) + INV_SMOKES_PER_PACK) > INV_SMOKES_MAX
							shopSnacks.iIncrease = (INV_SMOKES_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT))
						ENDIF
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_SMOKES, TO_FLOAT(shopSnacks.iIncrease), HUD_ANIM_PICKUP_INT)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, shopSnacks.iIncrease)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_CIGARETTES_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY(),hash("STEAL"),  FALSE, hash("STORE"))//,  0)
						
					BREAK
					CASE SNACK_SODA
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_DRINK_1, 1, HUD_ANIM_PICKUP_INT)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, 1)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY(),hash("STEAL"),  FALSE, hash("STORE"))//,  0)
					BREAK
					#IF FEATURE_DLC_1_2022
					CASE SNACK_SPRUNK
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_DRINK_4, 1, HUD_ANIM_PICKUP_INT)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT, 1)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_SPRUNK_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY(),hash("STEAL"),  FALSE, hash("STORE"))//,  0)
					BREAK
					#ENDIF
					CASE SNACK_BEER
						SET_PICKUP_ANIM_FOR_ENTITY(MP_PICKUP_DRINK_2, 1, HUD_ANIM_PICKUP_INT)
						INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT, 1)
						SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_BOURGE_BOUGHT_v0"), hash("SNACK"), 1, GET_LOCATION_HASH_FOR_TELEMETRY(),hash("STEAL"),  FALSE, hash("STORE"))//,  0)
					BREAK
					/*#IF IS_DEBUG_BUILD
					#IF FEATURE_HEIST_PLANNING
					CASE SNACK_LOTTERY
						//CAN'T BE SHOPLIFTED
					BREAK
					#ENDIF
					#ENDIF*/
				ENDSWITCH
				
				IF !bIsOfficeSnackMenu
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				
				//go wanted
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
				AND NOT FM_EVENT_SHOULD_BLOCK_GIVING_WANTED_LEVEL(PLAYER_ID())
					REPORT_CRIME(PLAYER_ID(), CRIME_HASSLE, GET_WANTED_LEVEL_THRESHOLD(1))
					NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - REPORT_CRIME - SHOPLIFT   <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					SET_BIT(playerBD_menu.iPlayerBitSet, biP_ReactToWantedLevelLineDone)	//STOP WANTED LEVEL LINE BEING TRIGGERED
					IF NOT IS_BIT_SET(playerBD_menu.iPlayerBitSet, biP_PlayerHasDemandedCash)
						SET_BIT(playerBD_menu.iPlayerBitSet, biP_GotWantedWithBlindEyePerk)
						PRINTLN(" biP_GotWantedWithBlindEyePerk - SET - A")
					ENDIF
				ENDIF
				
				PRINTLN("*****[ShopRobberies.sc->UPDATE SNACKS] shoplift complete, going to _cleanup")
				
				//TASK_PLAY_ANIM(NET_TO_PED(serverBD_menu.niRegStaff), "misscommon@response", "give_me_a_break")
				
				// dialog
				IF NOT IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToShopLift)
					NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - setting biPS_ReactToShopLift TRUE <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
					SET_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToShopLift)
				ENDIF
				
				
				shopSnacks.eShopSnacksState = SHOPSNACKS_CLEANUP
				
			BREAK
			CASE SHOPSNACKS_STORE_ALERT
				IF NOT g_sShopSettings.bProcessStoreAlert
				AND NOT IS_COMMERCE_STORE_OPEN()
					shopSnacks.eShopSnacksState = SHOPSNACKS_UPDATE_MENU
				ENDIF
			BREAK
			CASE SHOPSNACKS_SOLD_OUT
				IF !bIsOfficeSnackMenu
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				IF (GET_GAME_TIMER() - shopSnacks.iHoldTime) > 1000
					shopSnacks.eShopSnacksState = SHOPSNACKS_CLEANUP
				ELSE
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				ENDIF
			BREAK
			CASE SHOPSNACKS_RESET
				IF (GET_GAME_TIMER() - shopSnacks.iHoldTime) > 1000
					shopSnacks.eShopSnacksState = SHOPSNACKS_INIT
				ELSE
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_X)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
					SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
				ENDIF
			BREAK
			CASE SHOPSNACKS_CLEANUP
				IF IS_BIT_SET(serverBD_menu.iServerSnackBitSet, biSS_ReactToLiftLineDone)
					IF IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToShopLift)
						CLEAR_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToShopLift)
					ENDIF
					IF IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToLiftDone)
						CLEAR_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToLiftDone)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
	ELSE
		IF (GET_GAME_TIMER() % 8000) < 50
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks]     not hitting snack switch state ")
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] serverBD_menu.eStaffState = ", serverBD_menu.eStaffState)
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] is player ok = ", GET_STRING_FROM_BOOL(IS_NET_PLAYER_OK(PLAYER_ID())))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] is menu loaded = ", GET_STRING_FROM_BOOL(IS_BIT_SET(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] is player under attack = ", GET_STRING_FROM_BOOL(IS_PLAYER_UNDER_ATTACK()))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] player wanted level = ", GET_PLAYER_WANTED_LEVEL(PLAYER_ID()))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] player in back room  = ", GET_STRING_FROM_BOOL(bInBackRoom))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] is staff injured = ", GET_STRING_FROM_BOOL(IS_NET_PED_INJURED(serverBD_menu.niRegStaff)))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "*****[_mp_shop_snacks] using online store = ", GET_STRING_FROM_BOOL(IS_COMMERCE_STORE_OPEN()))
			CDEBUG1LN(DEBUG_SHOP_ROBBERIES, "***************************************** ")
		ENDIF
		
		IF IS_BIT_SET(serverBD_menu.iServerSnackBitSet, biSS_ReactToLiftLineDone)
			IF IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToShopLift)
				CLEAR_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToShopLift)
				NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - setting biPS_ReactToShopLift FALSE <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			ENDIF
			IF IS_BIT_SET(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToLiftDone)
				CLEAR_BIT(playerBD_menu.iPlayerSnackBitSet, biPS_ReactToLiftDone)
				NET_PRINT_TIME() NET_PRINT_STRINGS("[AM HOLD UP] - setting biPS_ReactToLiftDone FALSE <----------     ", GET_MP_MISSION_NAME(shopSnacks.thisHoldupMission)) NET_NL()
			ENDIF
		ENDIF
		
		IF shopSnacks.eShopSnacksState != SHOPSNACKS_CLEANUP
		AND IS_BIT_SET(shopSnacks.iBoolsBitSet, biStoreMenuLoaded)
		AND NOT IS_COMMERCE_STORE_OPEN()
			IF shopSnacks.eShopSnacksState = SHOPSNACKS_UPDATE_MENU
			OR shopSnacks.eShopSnacksState = SHOPSNACKS_SETUP_MENU
			OR shopSnacks.eShopSnacksState = SHOPSNACKS_BUY
			OR shopSnacks.eShopSnacksState = SHOPSNACKS_STORE_ALERT
			OR shopSnacks.eShopSnacksState = SHOPSNACKS_BUY_SERVER
				EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
			ENDIF
			
			shopSnacks.eShopSnacksState = SHOPSNACKS_CLEANUP
		ENDIF
		
//		IF NOT g_sShopSettings.bProcessStoreAlert
//			IF DOES_CAM_EXIST(shopSnacks.ciSnacks)
//			AND IS_CAM_ACTIVE(shopSnacks.ciSnacks)
//				EXIT_SNACK_MENU(shopSnacks, playerBD_menu.iCashSpent)
//			ENDIF
//		ENDIF
		
		/*IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SHR_MENU")	//REMOVED FOR BUG 1644632
			CLEAR_HELP()
			PRINTLN("[AM HOLD UP] - CLEAR HELP 'SHR_MENU' - C")
		ENDIF*/
		
		IF shopSnacks.iUseContext != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(shopSnacks.iUseContext)
			PRINTLN("[AM HOLD UP] - RELEASE CONTEXT - C")
		ENDIF
		
	ENDIF
		
	/*#IF IS_DEBUG_BUILD
	#IF FEATURE_HEIST_PLANNING
	DO_LOTTERY_END_CHECKS()
	#ENDIF
	#ENDIF*/
ENDPROC

// END SHOP SNACK CONTENT --------------------------------------------------------------------------------------

