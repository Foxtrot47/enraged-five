
// ___________________________________________
//
//	Game: 
//	------
//	GTAV.
//
//
//  Script: 
//	-------
// 	Backup heli launching.
// 
//
//  Author: 
//	-------
//	William.Kennedy@RockstarNorth.com
//
//
// 	Description: 
//	------------
//	Player can call contact and
// 	pay for a helicopter to fly
// 	in and back them up.
//
// ___________________________________________ 

USING "globals.sch"
USING "script_ped.sch"
USING "commands_player.sch"
USING "script_network.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "net_script_timers.sch"
USING "net_mission.sch"

USING "net_include.sch"
USING "fm_unlocks_header.sch"
USING "net_freemode_cut.sch"

ENUM eBACKUP_HELI_LAUNCHING_STATE
	eBACKUPHELILAUNCHINGSTATE_INIT = 0,
	eBACKUPHELILAUNCHINGSTATE_WAIT,
	eBACKUPHELILAUNCHINGSTATE_LAUNCHING,
	eBACKUPHELILAUNCHINGSTATE_RUNNING
ENDENUM

STRUCT STRUCT_BACKUP_HELI_LAUNCHING_DATA
	eBACKUP_HELI_LAUNCHING_STATE eState
ENDSTRUCT

#IF IS_DEBUG_BUILD

FUNC STRING GET_BACKUP_HELI_LAUNCHING_STATE_NAME(eBACKUP_HELI_LAUNCHING_STATE eState)
	
	SWITCH eState
		
		CASE eBACKUPHELILAUNCHINGSTATE_INIT			RETURN "INIT"
		CASE eBACKUPHELILAUNCHINGSTATE_WAIT			RETURN "WAIT"
		CASE eBACKUPHELILAUNCHINGSTATE_LAUNCHING	RETURN "LAUNCHING"
		CASE eBACKUPHELILAUNCHINGSTATE_RUNNING		RETURN "RUNNING"
	ENDSWITCH
	
	RETURN "NOT_IN_SWITCH"
	
ENDFUNC

#ENDIF

PROC SET_BACKUP_HELI_LAUNCHING_STATE(STRUCT_BACKUP_HELI_LAUNCHING_DATA &sData, eBACKUP_HELI_LAUNCHING_STATE eNewState)
	
	#IF IS_DEBUG_BUILD
		IF sData.eState != eNewState
			NET_PRINT("[WJK] - Backup Heli Launching - changing state to ")NET_PRINT(GET_BACKUP_HELI_LAUNCHING_STATE_NAME(eNewState))NET_NL()
		ENDIF
	#ENDIF
	
	sData.eState = eNewState
	
ENDPROC

PROC MAINTAIN_BACKUP_HELI_LAUNCHING(STRUCT_BACKUP_HELI_LAUNCHING_DATA &sData)
	
	SWITCH sData.eState
		
		CASE eBACKUPHELILAUNCHINGSTATE_INIT
			SET_BACKUP_HELI_LAUNCHING_STATE(sData, eBACKUPHELILAUNCHINGSTATE_WAIT)
		BREAK
		
		CASE eBACKUPHELILAUNCHINGSTATE_WAIT
			IF MPGlobalsAmbience.bLaunchBackUpHeli
				NET_PRINT("[WJK] - Backup Heli Launching - MPGlobalsAmbience.bLaunchBackUpHeli = TRUE.")NET_NL()
				SET_BACKUP_HELI_LAUNCHING_STATE(sData, eBACKUPHELILAUNCHINGSTATE_LAUNCHING)
			ENDIF
		BREAK
		
		CASE eBACKUPHELILAUNCHINGSTATE_LAUNCHING
			
			// If the script is running on my machine, go to the running state.
			IF DOES_SCRIPT_EXIST("AM_backup_heli")
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_backup_heli")) <= 0
					
					MP_MISSION_DATA sLaunchdata
					sLaunchdata.mdID.idMission = eBACKUP_HELI
					sLaunchdata.iInstanceId = NATIVE_TO_INT(PLAYER_ID())
					
					IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(sLaunchdata)
						NET_PRINT("[WJK] - Backup Heli Launching - script AM_backup_heli now running.")NET_NL()
						SET_BACKUP_HELI_LAUNCHING_STATE(sData, eBACKUPHELILAUNCHINGSTATE_RUNNING)
					#IF IS_DEBUG_BUILD
					ELSE
						NET_PRINT("[WJK] - Backup Heli Launching - launching script AM_backup_heli.")NET_NL()
					#ENDIF
					ENDIF
					
				// If I am in free mode and I am not running the script, try to launch it.
				ELSE
					NET_PRINT("[WJK] - Backup Heli Launching - script AM_backup_heli now running.")NET_NL()
					SET_BACKUP_HELI_LAUNCHING_STATE(sData, eBACKUPHELILAUNCHINGSTATE_RUNNING)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE eBACKUPHELILAUNCHINGSTATE_RUNNING
			IF DOES_SCRIPT_EXIST("AM_backup_heli")
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_backup_heli")) <= 0
					NET_PRINT("[WJK] - Backup Heli Launching - script AM_backup_heli has terminated.")NET_NL()
					MPGlobalsAmbience.bLaunchBackUpHeli = FALSE
					SET_BACKUP_HELI_LAUNCHING_STATE(sData, eBACKUPHELILAUNCHINGSTATE_INIT)
				ENDIF
			ENDIF
		BREAK
			
	ENDSWITCH
	
ENDPROC



