///// NET_realty_new.sch
///// CONOR - 27/12/12
///// MP properties functionality
//// NOTE: think functionality will move to world point instead but setting up as header to get in functionality in before new year
//
USING "globals.sch"
// Game Headers
USING "commands_network.sch"
USING "net_include.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "net_spawn.sch"
USING "net_nodes.sch"
USING "fmmc_launcher_menu.sch"
USING "net_saved_vehicles.sch"
USING "net_realty_details.sch"
//USING "net_MP_TV.sch"
//USING "net_MP_Radio.sch"
USING "net_freemode_cut.sch"
USING "commands_money.sch"
USING "net_cash_transactions.sch"
USING "net_private_yacht.sch"
USING "net_realty_warehouse.sch"

USING "net_realty_biker_warehouse.sch"

USING "net_realty_vehicle_garage.sch"

USING "net_special_pv_properties.sch"

CONST_INT MP_PROP_MENU_DES_NO_MONEY				0
CONST_INT MP_PROP_MENU_DES_REJECTED_ENTRY		1
CONST_INT MP_PROP_MENU_DES_FAILED_DOOR_REQUEST	2
CONST_INT MP_PROP_MENU_DES_OWNS_PROPERTY		3
CONST_INT MP_PROP_MENU_DES_ON_HEIST				4
CONST_INT MP_PROP_MENU_DES_3RD_DISABLE			5
CONST_INT MP_PROP_MENU_DES_NOT_BOSS				6
CONST_INT MP_PROP_MENU_DES_NO_YACHT				7

CONST_INT MP_PROP_MENU_DES_TIME		2000

CONST_INT PROP_BUTTON_BS_PRESSED_ACCEPT			0
CONST_INT PROP_BUTTON_BS_PRESSED_CANCEL			1
CONST_INT PROP_BUTTON_BS_GIVEN_CONFIRMATION		2
CONST_INT PROP_BUTTON_BS_REPLACE_CAR_WARNING	3
CONST_INT PROP_BUTTON_BS_PRESSED_UP				4
CONST_INT PROP_BUTTON_BS_PRESSED_DOWN			5
CONST_INT PROP_BUTTON_BS_PRESSED_LEFT			6
CONST_INT PROP_BUTTON_BS_PRESSED_RIGHT			7
CONST_INT PROP_BUTTON_BS_PRESSED_X				8
CONST_INT PROP_BUTTON_BS_PRESSED_NEXT_CAM		9

CONST_INT PROP_ENTER_STAGE_INIT				0	
CONST_INT PROP_ENTER_STAGE_FULL				1
CONST_INT PROP_ENTER_STAGE_NO_VEHICLE		2
CONST_INT PROP_ENTER_STAGE_VEHICLE			3
CONST_INT PROP_ENTER_STAGE_FAKE_CUTSCENE_HEIST	4

CONST_INT MP_PROP_CREATE_CARS_BS_APPLIED_DETAILS	0
CONST_INT MP_PROP_CREATE_CARS_BS_MODS_LOADED		1

CONST_INT TIER_1_DRINKING						1
CONST_INT TIER_2_DRINKING						3
CONST_INT TIER_3_DRINKING						6

CONST_INT TIER_1_STRIPPERS						0
CONST_INT TIER_2_STRIPPERS						2
CONST_INT TIER_3_STRIPPERS						6

CONST_INT TIER_1_SMOKING						1
CONST_INT TIER_2_SMOKING						4
CONST_INT TIER_3_SMOKING						7

CONST_INT MAX_EXTRA_EXTERNAL_IPLS_FOR_PROPERTY  4

CONST_INT EXTERNAL_IPLS_FOR_PROPERTY_TYPE_DEFAULT   -1
CONST_INT EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP	1

//CONST_INT MENU_SLOT_TYPE_PLAYER		1
//CONST_INT MENU_SLOT_TYPE_NPC		2

//
//STRUCT MP_REALTY_CONTROL_STRUCT
//	INT iStage
//	INT iCurrentProperty = 1
//	INT iEntrance
//	
//	INT iSlowLoop = 1
//	INT iContextIntention
//
//	
//	
//	MP_REALITY_WARP_IN_STRUCT enteringStruct
//	INT iRejectedRequestsBS
//	INT iPriceOfPropertyJustBought
//	CAMERA_INDEX cam
//	
//	BLIP_INDEX CustomGarageBlips[MAX_MP_PROPERTIES][MAX_ENTRANCES]
//	VEHICLE_SETUP_STRUCT cloneVeh
//	VECTOR vSpawnLoc
//	FLOAT fSpawnHeading
//	SCRIPT_TIMER  mechanicChargeTimer
//	
//	INT iMenuDescriptionBS
//	SCRIPT_TIMER menuDescriptionTimer
//	
//	INT iCurrentVertSel
//	INT iMaxVertSel
//	SCRIPT_TIMER menuNavigationDelay
//
//	PLAYER_INDEX menuPlayerArray[NUM_NETWORK_PLAYERS]
//	INT menuPlayerRangBuzzerBS
//	SCRIPT_TIMER buzzerTimer
//	
//	SCRIPT_TIMER buzzerMenuAtDoorTimer
//	INT iKnowOwnerState[MAX_MP_PROPERTIES]
//
//	FLOAT maxDetails[ciVEHICLE_STAT_BOX_MAX]
//	
//	//INT iPropertyBlipLoop
//	
//	MP_TV_DATA_STRUCT MPTVData //if script is now a world point brain, let David G know he should move this
//	MP_RADIO_DATA_STRUCT MPRadioData //if script is now a world point brain, let David G know he should move this
//	
//	STRUCT_GARAGE_CUT  				garageCutscene
//	STRUCT_INTRO_TO_APARTMENTS_CUT	apartmentCutscene
//	
//	CORONA_VEHICLE_STATS_BOX_VARS		coronaVehBox
//	INT iCurrentVehicleClass
//	SCRIPT_TIMER statBoxDelayTimer
//ENDSTRUCT
//
CONST_INT REP_VEH_PROP_BS_SETUP 		0
CONST_INT REP_VEH_PROP_BS_USING			1
CONST_INT REP_VEH_PROP_BS_NO_MONEY		2
CONST_INT REP_VEH_PROP_BS_WARNING		3	
CONST_INT REP_VEH_PROP_BS_REQ_SF		4
CONST_INT REP_VEH_PROP_BS_HEIST_ACTIVE	5
CONST_INT REP_VEH_PROP_BS_3RD_DISABLE	6
CONST_INT REP_VEH_PROP_BS_EXIT_MENU		7

STRUCT REPLACE_MP_VEH_OR_PROP_MENU
	INT iStage
	INT iSlotIDS[MAX_MP_SAVED_VEHICLES]
	INT iEmptyPositionBS
	INT iLevel
	INT iCurrentVertSel
	INT iCurrentVertSelSecLevel //quicker than array
	INT iMaxVertSel
	INT iBS
	SCRIPT_TIMER menuNavigationDelay
	INT iButtonBS
	SCALEFORM_INDEX warningscreen
	INT iFirstLevelSelectedSlotID
ENDSTRUCT




STRUCT OWNED_PROPERTIES_IN_BUILDING
	INT iNumProperties
	INT iReturnedPropertyIDs[MAX_OWNED_PROPERTIES]
ENDSTRUCT


PROC UNPACK_INVITED_DATA_INT(INT iValue,INT &iPropertyNum, INT &iSlotID, INT &iEntrance ,INT &iVariation  )
	// 00|0|000|00
	// Variation|Entrance|PropertyID|iSlotID
	///#IF NOT FEATURE_HEIST_PLANNING iSlot is actually iInstance
	INT iTemp = ABSI(iValue) 
	PRINTLN("UNPACK_INVITED_DATA_INT: value = ",iValue )
	iVariation = FLOOR(TO_FLOAT(iTemp)/1000000)
	iTemp = iTemp - iVariation*1000000
	PRINTLN("UNPACK_INVITED_DATA_INT: iVariation = ",iVariation)
	iEntrance = FLOOR(TO_FLOAT(iTemp)/100000)
	iTemp = iTemp - iEntrance*100000
	PRINTLN("UNPACK_INVITED_DATA_INT: iEntrance = ",iEntrance )
	iPropertyNum = FLOOR(TO_FLOAT(iTemp)/100)
	iTemp = iTemp - iPropertyNum*100
	PRINTLN("UNPACK_INVITED_DATA_INT: iPropertyNum = ",iPropertyNum )
	iSlotID = iTemp
	PRINTLN("UNPACK_INVITED_DATA_INT: iSlotID = ",iSlotID )
ENDPROC

/// NOTE: This function also existing in net_transition_sessions.sch update both if changes are made
PROC PACK_INVITED_DATA_INT(INT &iValue,INT iPropertyNum, INT iSlotID, INT iEntrance ,INT iVariation )
	// 00|0|000|00
	// Variation|Entrance|PropertyID|iSlotID
	///#IF NOT FEATURE_HEIST_PLANNING iSlot is actually iInstance
	iValue = 0
	IF iVariation > 0
	 	iValue = iVariation*1000000
		PRINTLN("PACK_INVITED_DATA_INT: iVariation = ",iVariation )
	ENDIF
	iValue += iEntrance*100000
	PRINTLN("PACK_INVITED_DATA_INT: iEntrance = ",iEntrance )
	iValue += iPropertyNum*100
	PRINTLN("PACK_INVITED_DATA_INT: iPropertyNum = ",iPropertyNum )
	iValue += iSlotID
	PRINTLN("PACK_INVITED_DATA_INT: iSlotID = ",iSlotID )
	PRINTLN("PACK_INVITED_DATA_INT: final value = ",iValue )
ENDPROC

PROC SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA(INT iPropertyNum, INT iSlotID, INT iEntrance, GAMER_HANDLE ownerHandle ,INT iVariation )
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	PRINTLN("SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA: Called from ", GET_THIS_SCRIPT_NAME())
	PRINTLN("SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA: iPropertyNum = ",iPropertyNum)
	PRINTLN("SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA: iSlotID = ",iSlotID)
	PRINTLN("SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA: iEntrance = ",iEntrance)
	PRINTLN("SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA: iVariation = ",iVariation)
	PRINTLN("SET_TRANSITION_SESSION_PROPERTY_DETAILS_DATA: owner handle =")
	DEBUG_PRINT_GAMER_HANDLE(ownerHandle)
	#ENDIF
	///#IF NOT FEATURE_HEIST_PLANNING iSlot is actually iInstance
	g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.ownerHandle = ownerHandle
	PACK_INVITED_DATA_INT(g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt,iPropertyNum,iSlotID,iEntrance ,iVariation )
ENDPROC

PROC SET_PLAYER_IN_PROPERTY(BOOL inProperty,BOOL bInGarage, BOOL bCalledFromExt, BOOL bClearInviterFlag = TRUE ,INT iInstance = -1  , INT iVariation = -1 )
	INT i
	IF NETWORK_IS_GAME_IN_PROGRESS()			//fix for 1227129 DaveyG 25/03/2013
	
		IF inProperty
			IF bCalledFromExt
				SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_VIA_EXTERIOR)
			ENDIF
			
			
			//IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_SPECIAL_TRANSITION)
			//AND globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
				
//			ELSE
//				IF IS_GAMER_HANDLE_VALID(globalPropertyEntryData.ownerHandle)
//				IF globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
//					globalPropertyEntryData.ownerID = PLAYER_ID()
//				ENDIF
//				globalPropertyEntryData.ownerHandle = GET_GAMER_HANDLE_PLAYER(globalPropertyEntryData.ownerID)
//				//NETWORK_SET_PLAYER_GARAGE_INDEX(NATIVE_TO_INT(globalPropertyEntryData.ownerID)+1)
//				PRINTLN("SET_PLAYER_IN_PROPERTY: NETWORK_SET_PLAYER_GARAGE_INDEX #",NATIVE_TO_INT(globalPropertyEntryData.ownerID)) 
//				INT instance  
//			    instance = NATIVE_TO_INT(globalPropertyEntryData.ownerID)
//				//NETWORK_SET_VOICE_CHANNEL(FM_VOICE_CHANNEL_PROPERTY0+instance)
//				//PRINTLN("SET_PLAYER_IN_PROPERTY: set in voice session #",FM_VOICE_CHANNEL_PROPERTY0+instance )
//				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID =instance
//				PRINTLN("SET_PLAYER_IN_PROPERTY: set visibility ID #",instance) 
//			ENDIF
			//NETWORK_SET_TALKER_PROXIMITY(0.0)
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			//NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
			GAMER_HANDLE gamerHandle
			BOOL bSetInPropertyBroadcast
			IF IS_GAMER_HANDLE_VALID(globalPropertyEntryData.ownerHandle)
				gamerHandle = GET_LOCAL_GAMER_HANDLE()
				IF NETWORK_ARE_HANDLES_THE_SAME(globalPropertyEntryData.ownerHandle,gamerHandle)
					SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
					CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
					PRINTLN("SET_PLAYER_IN_PROPERTY: player now in property they own") 
					bSetInPropertyBroadcast = TRUE
				ENDIF
			ENDIF
			IF NOT bSetInPropertyBroadcast
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_ENTER_FRIENDS_HOUSE, TRUE)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				PRINTLN("SET_PLAYER_IN_PROPERTY: player now in property owned by someone else") 
				bSetInPropertyBroadcast = TRUE
			ENDIF
			IF bInGarage
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
				PRINTLN("SET_PLAYER_IN_PROPERTY: player in garage") 
			ELSE
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
				PRINTLN("SET_PLAYER_IN_PROPERTY: player not in garage")
			ENDIF
			//HIDE_MY_PLAYER_BLIP(TRUE)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPEC_WAS_JUST_IN_PROPERTY)
			
			//IF invitePlayerAptData.ipInviteApartment <=0
			INT iInvitedPropertyNum, iInvitedPropertyInstance, iInvitedPropertyEntrance
			INT iInvitedPropertyVariation
			
			INT iOwnedProperty
			iOwnedProperty = GET_LAST_USED_PROPERTY()
			IF g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt = 0
				IF globalPropertyEntryData.iPropertyEntered <= 0
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = iOwnedProperty
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation = GET_OWNED_PROPERTY_VARIATION(GET_OWNED_PROPERTY_SLOT_INDEX(iOwnedProperty))
				ELSE
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = globalPropertyEntryData.iPropertyEntered
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation = iVariation
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PLAYER_IN_PROPERTY:  #",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) 
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PLAYER_IN_PROPERTY: variation #",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation) 
			ELSE
				UNPACK_INVITED_DATA_INT(g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt,iInvitedPropertyNum, iInvitedPropertyInstance,iInvitedPropertyEntrance , iInvitedPropertyVariation )
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = iInvitedPropertyNum
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PLAYER_IN_PROPERTY: player was invited to property #",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) 
				IF IS_PROPERTY_STILT_APARTMENT(iInvitedPropertyNum)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation = iInvitedPropertyVariation
					CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PLAYER_IN_PROPERTY: player was invited to property variation #",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) 
				ENDIF
			ENDIF
			
			//INT instance
			IF IS_GAMER_HANDLE_VALID(globalPropertyEntryData.ownerHandle)
				IF NETWORK_IS_GAMER_IN_MY_SESSION(globalPropertyEntryData.ownerHandle)
					globalPropertyEntryData.ownerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
					PRINTLN("SET_PLAYER_IN_PROPERTY: setting property owner based on owner handle")
//					IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[0] > 0
//					AND GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[0] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
//						globalPropertyEntryData.iInstance = NATIVE_TO_INT(globalPropertyEntryData.ownerID)
//						PRINTLN("SET_PLAYER_IN_PROPERTY: entered owner's property #0")
//					ELIF GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[1] > 0
//					AND GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[1] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
//						globalPropertyEntryData.iInstance = NATIVE_TO_INT(globalPropertyEntryData.ownerID)+(NUM_NETWORK_PLAYERS+1)
//						PRINTLN("SET_PLAYER_IN_PROPERTY: entered owner's property #1")
//					ELSE
//						PRINTLN("SET_PLAYER_IN_PROPERTY: entered property does not match the owners properties????")
//						globalPropertyEntryData.iInstance = NUM_NETWORK_PLAYERS
//					ENDIF
				ELSE
					PRINTLN("SET_PLAYER_IN_PROPERTY: can't set property owner based on owner handle as they are NOT in the game so setting owner as invalid")
					globalPropertyEntryData.ownerID =INVALID_PLAYER_INDEX()
//					globalPropertyEntryData.iInstance = NUM_NETWORK_PLAYERS
				ENDIF
			ENDIF
			
			globalPropertyEntryData.iInstance = iInstance
			
//			#IF NOT FEATURE_HEIST_PLANNING
//			//INT instance  
//			IF IS_GAMER_HANDLE_VALID(globalPropertyEntryData.ownerHandle)
//				IF NETWORK_IS_GAMER_IN_MY_SESSION(globalPropertyEntryData.ownerHandle)
//					globalPropertyEntryData.ownerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(globalPropertyEntryData.ownerHandle)
//					PRINTLN("SET_PLAYER_IN_PROPERTY: setting property owner based on owner handle")
//					REPEAT MAX_OWNED_PROPERTIES i
//						IF GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[i] > 0
//						AND GlobalplayerBD_FM[NATIVE_TO_INT(globalPropertyEntryData.ownerID)].propertyDetails.iOwnedProperty[i] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
//							globalPropertyEntryData.iInstance = NATIVE_TO_INT(globalPropertyEntryData.ownerID)+(NUM_NETWORK_PLAYERS+1)*i
//							PRINTLN("SET_PLAYER_IN_PROPERTY: entered owner's property #",i)
//							bAssignedInstance = TRUE
//						ENDIF
//					ENDREPEAT
//					IF NOT bAssignedInstance 
//						PRINTLN("SET_PLAYER_IN_PROPERTY: entered property does not match the owners properties????")
//						globalPropertyEntryData.iInstance = NUM_NETWORK_PLAYERS
//					ENDIF
//				ELSE
//					PRINTLN("SET_PLAYER_IN_PROPERTY: can't set property owner based on owner handle as they are NOT in the game so setting owner as invalid")
//					globalPropertyEntryData.ownerID =INVALID_PLAYER_INDEX()
//					globalPropertyEntryData.iInstance = NUM_NETWORK_PLAYERS
//				ENDIF
//			ENDIF
//			
//			#ENDIF
			
            SET_BIT(mpPropMaintain.iVisibilityControlBS ,MP_PROP_VIS_BS_CHECK_ALL_PLAYERS)       
			//NETWORK_SET_VOICE_CHANNEL(FM_VOICE_CHANNEL_PROPERTY0+instance)
			//PRINTLN("SET_PLAYER_IN_PROPERTY: set in voice session #",FM_VOICE_CHANNEL_PROPERTY0+instance )
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID =globalPropertyEntryData.iInstance
			PRINTLN("SET_PLAYER_IN_PROPERTY: set visibility ID #",globalPropertyEntryData.iInstance) 
			
			invitePlayerAptData.ipInviteApartment = -1
			PRINTLN("SET_PLAYER_IN_PROPERTY: TRUE invitePlayerAptData.ipInviteApartment = -1")
			DEBUG_PRINTCALLSTACK()
			PRINTLN("SET_PLAYER_IN_PROPERTY: TRUE from ", GET_THIS_SCRIPT_NAME()) 
			
		ELSE
			//HIDE_MY_PLAYER_BLIP(FALSE)
			globalPropertyEntryData.ownerID= INVALID_PLAYER_INDEX()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "SET_PLAYER_IN_PROPERTY: setting globalPropertyEntryData.ownerID= INVALID_PLAYER_INDEX()") 
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = -1
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation = -1
			//IF bStartedCutscene
				//NETWORK_CLEAR_PLAYER_GARAGE_INDEX()
			//	PRINTLN("SET_PLAYER_IN_PROPERTY: NETWORK_CLEAR_PLAYER_GARAGE_INDEX()") 
			//ENDIF
//			NETWORK_SET_TALKER_PROXIMITY(CHAT_PROXIMITY_FM)
			//NETWORK_CLEAR_VOICE_CHANNEL()
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
			PRINTLN("SET_PLAYER_IN_PROPERTY: FALSE from ", GET_THIS_SCRIPT_NAME()) 
			PLAYER_INDEX playerToCheck
			REPEAT NUM_NETWORK_PLAYERS i
				playerToCheck = INT_TO_PLAYERINDEX(i)
				IF IS_NET_PLAYER_OK(playerToCheck,FALSE,TRUE)
					IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
						NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
					ENDIF
				ENDIF
			ENDREPEAT
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID = -1
			IF bClearInviterFlag
				invitePlayerAptData.ipInviteApartment = -1
				PRINTLN("SET_PLAYER_IN_PROPERTY: FALSE invitePlayerAptData.ipInviteApartment = -1")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_OWNED_PROPERTIES_IN_BUILDING_STRUCT(OWNED_PROPERTIES_IN_BUILDING &OwnedDetails)
	INT i
	OwnedDetails.iNumProperties = 0
	REPEAT MAX_OWNED_PROPERTIES i 
		OwnedDetails.iReturnedPropertyIDs[i] = 0
	ENDREPEAT
ENDPROC

FUNC BOOL DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(PLAYER_INDEX playerID, INT iProperty)
	IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL1 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_1
	OR iProperty = PROPERTY_OFFICE_2_GARAGE_LVL1 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_2_BASE
	OR iProperty = PROPERTY_OFFICE_3_GARAGE_LVL1 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_3
	OR iProperty = PROPERTY_OFFICE_4_GARAGE_LVL1 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_4
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1] = PROPERTY_OFFICE_1_GARAGE_LVL1
			RETURN TRUE
		ENDIF
	ENDIF
	IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL2 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_1
	OR iProperty = PROPERTY_OFFICE_2_GARAGE_LVL2 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_2_BASE
	OR iProperty = PROPERTY_OFFICE_3_GARAGE_LVL2 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_3
	OR iProperty = PROPERTY_OFFICE_4_GARAGE_LVL2 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_4
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2] = PROPERTY_OFFICE_1_GARAGE_LVL2
			RETURN TRUE
		ENDIF
	ENDIF
	IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL3 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_1
	OR iProperty = PROPERTY_OFFICE_2_GARAGE_LVL3 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_2_BASE
	OR iProperty = PROPERTY_OFFICE_3_GARAGE_LVL3 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_3
	OR iProperty = PROPERTY_OFFICE_4_GARAGE_LVL3 AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0] = PROPERTY_OFFICE_4
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3] = PROPERTY_OFFICE_1_GARAGE_LVL3
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_OFFICE_GARAGE_IN_BUILDING(PLAYER_INDEX thePlayer, INT iBuilding)
	IF iBuilding = MP_PROPERTY_OFFICE_BUILDING_1
		RETURN DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(thePlayer,PROPERTY_OFFICE_1_GARAGE_LVL1)
	ELIF iBuilding = MP_PROPERTY_OFFICE_BUILDING_2
		RETURN DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(thePlayer,PROPERTY_OFFICE_2_GARAGE_LVL1)

	ELIF iBuilding = MP_PROPERTY_OFFICE_BUILDING_3
		RETURN DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(thePlayer,PROPERTY_OFFICE_3_GARAGE_LVL1)
	ELIF iBuilding = MP_PROPERTY_OFFICE_BUILDING_4
		RETURN DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(thePlayer,PROPERTY_OFFICE_4_GARAGE_LVL1)
	ENDIF
	RETURN FALSE
ENDFUNC

CONST_INT BUILDING_OWNERSHIP_STATUS_NO_OWNED_PROPERTY						0
CONST_INT BUILDING_OWNERSHIP_STATUS_PLAYER_OWNS_PROPERTY					1
CONST_INT BUILDING_OWNERSHIP_STATUS_PLAYER_OWNS_PROPERTY_BUT_IN_GANG		2

FUNC INT GET_BUILDING_OWNERSHIP_STATUS(INT iBuilding)
	INT i,j 
	INT iResult = BUILDING_OWNERSHIP_STATUS_NO_OWNED_PROPERTY
	INT iOwnedProperty
	BOOL bBossPlayerOkay
	BUILDING_PROPERTIES building
	PLAYER_INDEX GangLeaderID = INVALID_PLAYER_INDEX()
	
	GET_BUILDING_PROPERTIES(building,iBuilding)

	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF GangLeaderID != INVALID_PLAYER_INDEX()
			bBossPlayerOkay = IS_NET_PLAYER_OK(GangLeaderID,FALSE)
		ENDIF
	ENDIF

	
	REPEAT building.iNumProperties i
		REPEAT MAX_OWNED_PROPERTIES j 
			iOwnedProperty = GET_OWNED_PROPERTY(j)
			
			
			IF iOwnedProperty > -1
				IF IS_PROPERTY_OFFICE_GARAGE(iOwnedProperty)
					IF GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[i]) = iOwnedProperty
					AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(PLAYER_ID(),building.iPropertiesInBuilding[i])
						iResult = BUILDING_OWNERSHIP_STATUS_PLAYER_OWNS_PROPERTY
					ENDIF
				ELSE
					IF building.iPropertiesInBuilding[i] = iOwnedProperty
						IF bBossPlayerOkay
						AND GangLeaderID != PLAYER_ID()
							IF IS_PROPERTY_ANY_CLUBHOUSE(building.iPropertiesInBuilding[i])
							OR IS_PROPERTY_OFFICE(building.iPropertiesInBuilding[i])
								RETURN BUILDING_OWNERSHIP_STATUS_PLAYER_OWNS_PROPERTY_BUT_IN_GANG
							ENDIF
						ENDIF
						iResult = BUILDING_OWNERSHIP_STATUS_PLAYER_OWNS_PROPERTY
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	RETURN iResult
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(INT iBuilding, OWNED_PROPERTIES_IN_BUILDING &OwnedDetails, BOOL bDoGangBossCheck  = TRUE, BOOL bIgnoreOfficeGarages = FALSE)
	INT i,j 
	BUILDING_PROPERTIES building
	GET_BUILDING_PROPERTIES(building,iBuilding)
	CLEAR_OWNED_PROPERTIES_IN_BUILDING_STRUCT(OwnedDetails)
	

	PLAYER_INDEX GangLeaderID = INVALID_PLAYER_INDEX()
	IF bDoGangBossCheck
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
			GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		ENDIF
	ENDIF
	BOOL bSkip
	REPEAT building.iNumProperties i
		REPEAT MAX_OWNED_PROPERTIES j 
			bSkip = FALSE
//			#IF FEATURE_BIKER
//			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)	
				IF IS_PROPERTY_CLUBHOUSE(building.iPropertiesInBuilding[i])
				AND GangLeaderID != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
					bSkip = TRUE
				ENDIF
				IF IS_PROPERTY_OFFICE(building.iPropertiesInBuilding[i])
				AND GangLeaderID != INVALID_PLAYER_INDEX()
				AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
				AND GangLeaderID != PLAYER_ID()
					bSkip = TRUE
				ENDIF
//			ELSE
//			#ENDIF
//				IF IS_PROPERTY_OFFICE(building.iPropertiesInBuilding[i])
//				AND GangLeaderID != INVALID_PLAYER_INDEX()
//				AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
//					bSkip = TRUE
//				ENDIF
//			#IF FEATURE_BIKER
//			ENDIF
//			#ENDIF

			IF NOT bSkip
				IF bIgnoreOfficeGarages
				AND IS_PROPERTY_OFFICE_GARAGE(GET_OWNED_PROPERTY(j))
				
				ELSE
					IF building.iPropertiesInBuilding[i] = GET_OWNED_PROPERTY(j)
					AND NOT IS_PROPERTY_OFFICE_GARAGE(GET_OWNED_PROPERTY(j))
						OwnedDetails.iReturnedPropertyIDs[OwnedDetails.iNumProperties] = building.iPropertiesInBuilding[i]
						OwnedDetails.iNumProperties++
					ELIF GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[i]) = GET_OWNED_PROPERTY(j)
					AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(PLAYER_ID(),building.iPropertiesInBuilding[i])
					AND GET_OWNED_PROPERTY(j) > -1
					//IS_PROPERTY_OFFICE_GARAGE(building.iPropertiesInBuilding[i])
						OwnedDetails.iReturnedPropertyIDs[OwnedDetails.iNumProperties] = building.iPropertiesInBuilding[i]
						OwnedDetails.iNumProperties++
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	IF OwnedDetails.iNumProperties > 0
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC BOOL DOES_LOCAL_PLAYER_OWN_YACHT(INT iYacht)
	IF iYacht = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_STAT_OWN_OFFICE()
	RETURN GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0) > 0
ENDFUNC

PROC SET_PLAYER_OWNS_OFFICE(BOOL bOwns)
	IF bOwns
		
		BOOL bDisplay
		INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8)
		IF NOT IS_BIT_SET(iStatInt, BI_FM_NMH11_UNLOCKED_PERSONAL_ASSISTANT)
			SET_BIT(iStatInt, BI_FM_NMH11_UNLOCKED_PERSONAL_ASSISTANT)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8, iStatInt)
			bDisplay = TRUE
		ENDIF
		
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OWNS_OFFICE)
			enumCharacterList CharacterToAdd = GET_OFFICE_PA_CHAR(FALSE)
			PRINTLN(GET_THIS_SCRIPT_NAME(), " SET_PLAYER_OWNS_OFFICE: Setting to: ", GET_STRING_FROM_BOOL(bOwns), " ", g_sCharacterSheetAll[CharacterToAdd].label)
			ADD_CONTACT_TO_PHONEBOOK(CharacterToAdd, MULTIPLAYER_BOOK, bDisplay)		
		ENDIF
		
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OWNS_OFFICE)
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OWNS_OFFICE)
			PRINTLN(GET_THIS_SCRIPT_NAME(), " SET_PLAYER_OWNS_OFFICE: Setting to: ", GET_STRING_FROM_BOOL(bOwns))
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OWNS_OFFICE)
	ENDIF
ENDPROC

PROC SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(BOOL bOwns)
	IF bOwns
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_OFFICE_MOD_GARAGE)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_OFFICE_MOD_GARAGE)
			PRINTLN("SET_PLAYER_OWNS_OFFICE_MOD_GARAGE: Setting to: ", bOwns)
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_PLAYER_OWNS_OFFICE_MOD_GARAGE: PROPERTY_BROADCAST_BS3_OWNS_OFFICE_MOD_GARAGE - TRUE")
			#ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_OFFICE_MOD_GARAGE)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_OFFICE_MOD_GARAGE)
			PRINTLN("SET_PLAYER_OWNS_OFFICE_MOD_GARAGE: Setting to: ", bOwns)
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("SET_PLAYER_OWNS_OFFICE_MOD_GARAGE: PROPERTY_BROADCAST_BS3_OWNS_OFFICE_MOD_GARAGE - FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DOES_LOCAL_PLAYER_STAT_OWN_CLUBHOUSE()
	RETURN GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE) > 0
ENDFUNC

PROC SET_PLAYER_OWNS_CLUBHOUSE(BOOL bOwns)
	IF bOwns
		
		BOOL bDisplay
//		INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8)
//		IF NOT IS_BIT_SET(iStatInt, BI_FM_NMH11_UNLOCKED_PERSONAL_ASSISTANT)
//			SET_BIT(iStatInt, BI_FM_NMH11_UNLOCKED_PERSONAL_ASSISTANT)
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8, iStatInt)
//			bDisplay = TRUE
//		ENDIF
		
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE)
			PRINTLN("SET_PLAYER_OWNS_CLUBHOUSE: Setting to: ", bOwns)
			ADD_CONTACT_TO_PHONEBOOK(GET_CLUBHOUSE_CHAR(FALSE), MULTIPLAYER_BOOK, bDisplay)		
		ENDIF
		
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE)
		
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE)
			PRINTLN("SET_PLAYER_OWNS_CLUBHOUSE: Setting to: ", bOwns)
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE)
	ENDIF
ENDPROC

PROC SET_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH(BOOL bOwns)
	IF bOwns
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MOD_BOOTH)
			PRINTLN("SET_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH: Setting to: ", bOwns)
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MOD_BOOTH)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MOD_BOOTH)
			PRINTLN("SET_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH: Setting to: ", bOwns)
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MOD_BOOTH)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH(PLAYER_INDEX clubHouseOwner)
	IF clubHouseOwner != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(clubHouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MOD_BOOTH)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "DOES_PLAYER_OWNS_CLUBHOUSE_MOD_BOOTH passed invalid player index")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_OWNS_CLUBHOUSE_GUN_LOCKER(BOOL bOwns)
	IF bOwns
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_GUN_LOCKER)
			PRINTLN("SET_PLAYER_OWNS_CLUBHOUSE_GUN_LOCKER: Setting to: ", bOwns)
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_GUN_LOCKER)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_GUN_LOCKER)
			PRINTLN("SET_PLAYER_OWNS_CLUBHOUSE_GUN_LOCKER: Setting to: ", bOwns)
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_GUN_LOCKER)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_OWNS_MOD_GUN_LOCKER(PLAYER_INDEX clubHouseOwner)
	IF clubHouseOwner != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(clubHouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_GUN_LOCKER)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "DOES_PLAYER_OWNS_MOD_GUN_LOCKER passed invalid player index")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_CLUBHOUSE_WALL_STYLE(BOOL bStyleB)
	IF bStyleB
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_STYLE_B)
			PRINTLN("SET_PLAYER_CLUBHOUSE_WALL_STYLE: Setting to: ", bStyleB)
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_STYLE_B)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_STYLE_B)
			PRINTLN("SET_PLAYER_CLUBHOUSE_WALL_STYLE: Setting to: ", bStyleB)
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_STYLE_B)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_OWNS_MOD_WALL_STYLE_B(PLAYER_INDEX clubHouseOwner)
	IF clubHouseOwner != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(clubHouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_STYLE_B)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "DOES_PLAYER_OWNS_MOD_WALL_STYLE_B passed invalid player index")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_CLUBHOUSE_FURNISHINGS_STYLE(BOOL bStyleB)
	IF bStyleB
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_FURNATURE_STYLE_B)
			PRINTLN("SET_PLAYER_CLUBHOUSE_FURNISHINGS_STYLE: Setting to: ", bStyleB)
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_FURNATURE_STYLE_B)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_FURNATURE_STYLE_B)
			PRINTLN("SET_PLAYER_CLUBHOUSE_FURNISHINGS_STYLE: Setting to: ", bStyleB)
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_FURNATURE_STYLE_B)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_OWNS_MOD_FURNISHINGS_STYLE_B(PLAYER_INDEX clubHouseOwner)
	IF clubHouseOwner != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(clubHouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_FURNATURE_STYLE_B)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "DOES_PLAYER_OWNS_MOD_FURNISHINGS_STYLE_B passed invalid player index")
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_PLAYER_CLUBHOUSE_WALL_HANGING_STYLE(BOOL bStyleB)
	IF bStyleB
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_HANGING_STYLE_B)
			PRINTLN("SET_PLAYER_CLUBHOUSE_WALL_HANGING_STYLE: Setting to: ", bStyleB)
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_HANGING_STYLE_B)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_HANGING_STYLE_B)
			PRINTLN("SET_PLAYER_CLUBHOUSE_WALL_HANGING_STYLE: Setting to: ", bStyleB)
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_HANGING_STYLE_B)
	ENDIF
ENDPROC

FUNC BOOL DOES_PLAYER_OWNS_MOD_WALL_HANGING_STYLE_B(PLAYER_INDEX clubHouseOwner)
	IF clubHouseOwner != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(clubHouseOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_WALL_HANGING_STYLE_B)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "DOES_PLAYER_OWNS_MOD_WALL_HANGING_STYLE_B passed invalid player index")
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CLUBHOUSE_MURAL_BS_FROM_ID(INT iMuralID)
	SWITCH iMuralID
		CASE 0 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_0
		CASE 1 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_1
		CASE 2 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_2
		CASE 3 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_3
		CASE 4 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_4
		CASE 5 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_5
		CASE 6 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_6
		CASE 7 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_7
		CASE 8 RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_8
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "GET_CLUBHOUSE_MURAL_BS_FROM_ID passed invalid mural ID: ", iMuralID, " returning MURAL_0")
	RETURN PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE_MURAL_0
ENDFUNC

PROC SET_PLAYER_CLUBHOUSE_MURAL_STYLE(INT iStyle)
	INT i
	
	REPEAT MAX_CLUBHOUSE_MURALS i
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, GET_CLUBHOUSE_MURAL_BS_FROM_ID(i))
	ENDREPEAT
	
	PRINTLN("SET_PLAYER_CLUBHOUSE_MURAL_STYLE: cleared all mural bitsets to make way for new mural: ", iStyle)
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo, GET_CLUBHOUSE_MURAL_BS_FROM_ID(iStyle))
ENDPROC

PROC SET_PLAYERS_FAVOURITE_PLAYLIST(INT iPlaylistID)
	PRINTLN("SET_PLAYERS_FAVOURITE_PLAYLIST: Setting playlist: ", iPlaylistID)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iClubhouseFavouritePlaylist = iPlaylistID
ENDPROC

FUNC INT GET_PLAYER_CLUBHOUSE_MURAL_STYLE(PLAYER_INDEX clubHouseOwner)
	IF clubHouseOwner != INVALID_PLAYER_INDEX()
		INT i
		
		REPEAT MAX_CLUBHOUSE_MURALS i
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(clubHouseOwner)].propertyDetails.iBSTwo, GET_CLUBHOUSE_MURAL_BS_FROM_ID(i))
				RETURN i
			ENDIF
		ENDREPEAT
		
		CASSERTLN(DEBUG_SAFEHOUSE, "GET_PLAYER_CLUBHOUSE_MURAL_STYLE passed unknown player index returning 0")
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "GET_PLAYER_CLUBHOUSE_MURAL_STYLE passed invalid player index returning 0")
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL DOES_ANYONE_I_KNOW_OWN_PROPERTY(INT& iKnowState, INT iProperty, BOOL bInside#IF IS_DEBUG_BUILD, BOOL bDebugOutput = FALSE #ENDIF)
	INT i, k//, iBuddyLoop
	//GAMER_HANDLE tempHandle
	//NETWORK_CLAN_DESC myClan
	//NETWORK_CLAN_DESC otherClan
	
	PLAYER_INDEX GangLeaderID = INVALID_PLAYER_INDEX()
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	ENDIF

	IF iKnowState = -1
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			#IF IS_DEBUG_BUILD
			IF bDebugOutput
				IF playerID = GangLeaderID
					IF IS_NET_PLAYER_OK(GangLeaderID,FALSE)
						PRINTLN("DOES_ANYONE_I_KNOW_OWN_PROPERTY; Checking gang leader: ", GET_PLAYER_NAME(playerID))
					ELSE
						PRINTLN("DOES_ANYONE_I_KNOW_OWN_PROPERTY; Checking gang leader")
					ENDIF
				ENDIF
			ENDIF
			#ENDIF
			IF playerID != PLAYER_ID()
				IF IS_NET_PLAYER_OK(playerID,FALSE)
					REPEAT MAX_OWNED_PROPERTIES k
						IF iKnowState = -1
							IF (((GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = iProperty
							AND NOT IS_PROPERTY_OFFICE_GARAGE(iProperty)
							)
							OR (GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(iProperty) = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k]
							AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(playerID,iProperty))
							 )
							AND (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = iProperty
							OR NOT bInside))
								//AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
								
								IF (IS_PROPERTY_OFFICE(iProperty) OR IS_PROPERTY_CLUBHOUSE(iProperty))
								AND GangLeaderID != INVALID_PLAYER_INDEX()
								AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
									IF playerID = GangLeaderID
										iKnowState = 1
										#IF IS_DEBUG_BUILD
											IF bDebugOutput
												PRINTLN("I know player they are my boss Property = ", iProperty)
											ENDIF
										#ENDIF
										RETURN TRUE
										#IF IS_DEBUG_BUILD
									ELSE
										IF bDebugOutput
											IF NOT  IS_PROPERTY_OFFICE(iProperty)
												PRINTLN("I DO NOT know player Property = ",iProperty)
											ENDIF
											IF NOT IS_NET_PLAYER_OK(GangLeaderID,FALSE)
												PRINTLN("I DO NOT know player gang leader not ok")
											ENDIF
											IF playerID != GangLeaderID
												PRINTLN("I DO NOT know player gang leader != player ")
											ENDIF
											
										ENDIF
									#ENDIF
									ENDIF
								ELSE
									g_ClanDescription = GET_PLAYER_CREW(PLAYER_ID())
									g_otherClanDescription = GET_PLAYER_CREW(playerID)
									IF g_ClanDescription.Id = g_otherClanDescription.Id
										iKnowState = 1
										#IF IS_DEBUG_BUILD
											IF bDebugOutput
												PRINTLN("I know player they are in my clan Property = ", iProperty)
											ENDIF
										#ENDIF
										RETURN TRUE
									ELSE
										g_GamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
										IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
											IF NETWORK_IS_FRIEND(g_GamerHandle)	
												iKnowState = 1
												#IF IS_DEBUG_BUILD
													IF bDebugOutput
														PRINTLN("I know player they are my friend Property = ", iProperty)
													ENDIF
												#ENDIF
												RETURN TRUE
											ENDIF
										ENDIF
										
										IF (IS_PROPERTY_OFFICE(iProperty) OR IS_PROPERTY_CLUBHOUSE(iProperty))
										AND GangLeaderID != INVALID_PLAYER_INDEX()
										AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
										AND playerID = GangLeaderID
											iKnowState = 1
											#IF IS_DEBUG_BUILD
												IF bDebugOutput
													PRINTLN("I know player they are my boss Property = ", iProperty)
												ENDIF
											#ENDIF
											RETURN TRUE
										#IF IS_DEBUG_BUILD
										ELSE
											IF bDebugOutput
												IF NOT  IS_PROPERTY_OFFICE(iProperty)
													PRINTLN("I DO NOT know player Property = ",iProperty)
												ENDIF
												IF NOT IS_NET_PLAYER_OK(GangLeaderID,FALSE)
													PRINTLN("I DO NOT know player gang leader not ok")
												ENDIF
												IF playerID != GangLeaderID
													PRINTLN("I DO NOT know player gang leader != player ")
												ENDIF
												
											ENDIF
										#ENDIF
										ENDIF
									ENDIF	
									/*REPEAT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.iNumBuddies iBuddyLoop
										IF IS_GAMER_HANDLE_VALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.BuddyGamerInfo[iBuddyLoop].Handle)
											IF NETWORK_ARE_HANDLES_THE_SAME(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.BuddyGamerInfo[iBuddyLoop].Handle,g_GamerHandle)
												iKnowState = 1
												#IF IS_DEBUG_BUILD
													IF bDebugOutput
														PRINTLN("I know player they are buddy Property = ", iProperty)
													ENDIF
												#ENDIF
												RETURN TRUE
											ENDIF
										ENDIF
									ENDREPEAT*/
								
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								IF bDebugOutput
									PRINTLN("This Player doesn't own iProperty = ", iProperty)
								ENDIF
							#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		IF iKnowState = 1
			RETURN TRUE
		ENDIF
	ENDIF
	iKnowState = 0
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_MY_BOSS_OWN_PROPERTY_IN_BUILDING(INT iBuilding, BOOL bInside #IF IS_DEBUG_BUILD, BOOL bDebugOutput = FALSE #ENDIF)
	INT iBuildingLoop, k
	BUILDING_PROPERTIES building
	GET_BUILDING_PROPERTIES(building,iBuilding)
	
	PLAYER_INDEX playerID = INVALID_PLAYER_INDEX()
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		playerID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
	ENDIF

	REPEAT building.iNumProperties iBuildingLoop
		IF playerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(playerID,FALSE)
			REPEAT MAX_OWNED_PROPERTIES k
				IF (((GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = building.iPropertiesInBuilding[iBuildingLoop]
				AND NOT IS_PROPERTY_OFFICE_GARAGE(building.iPropertiesInBuilding[iBuildingLoop])
				 )
				OR (GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[iBuildingLoop]) = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k]
				AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(playerID,building.iPropertiesInBuilding[iBuildingLoop]))
				 )
				AND (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = building.iPropertiesInBuilding[iBuildingLoop]
				OR NOT bInside))

					#IF IS_DEBUG_BUILD
						IF bDebugOutput
							PRINTLN("I know player they are my boss Property = ", building.iPropertiesInBuilding[iBuildingLoop])
						ENDIF
					#ENDIF
					RETURN TRUE
				#IF IS_DEBUG_BUILD
				ELSE
					IF bDebugOutput
						PRINTLN("my Boss doesn't own iProperty = ", building.iPropertiesInBuilding[iBuildingLoop])
					ENDIF
				#ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANYONE_I_KNOW_OWN_PROPERTY_IN_BUILDING(INT& iKnowState, INT iBuilding, BOOL bInside #IF IS_DEBUG_BUILD, BOOL bDebugOutput = FALSE #ENDIF)
	INT i, iBuildingLoop, k
	//GAMER_HANDLE tempHandle
	//NETWORK_CLAN_DESC myClan 
	//NETWORK_CLAN_DESC otherClan
	BUILDING_PROPERTIES building
	GET_BUILDING_PROPERTIES(building,iBuilding)
	
	
	PLAYER_INDEX GangLeaderID = INVALID_PLAYER_INDEX()
	BOOL bBikerGang = GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), FALSE)
	BOOL bOrganisation
	
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
		GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF NOT bBikerGang
			bOrganisation = TRUE
		ENDIF
	ENDIF
	
	
	
	IF iKnowState = -1
		REPEAT building.iNumProperties iBuildingLoop
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
				IF playerID != PLAYER_ID()
					IF IS_NET_PLAYER_OK(playerID,FALSE)
						REPEAT MAX_OWNED_PROPERTIES k
							IF iKnowState = -1
								IF (((GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = building.iPropertiesInBuilding[iBuildingLoop]
								AND NOT IS_PROPERTY_OFFICE_GARAGE(building.iPropertiesInBuilding[iBuildingLoop])
								 )
								OR (GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[iBuildingLoop]) = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k]
								AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(playerID,building.iPropertiesInBuilding[iBuildingLoop]))
								 )
								AND (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = building.iPropertiesInBuilding[iBuildingLoop]
								OR NOT bInside))
									IF ((IS_PROPERTY_OFFICE(building.iPropertiesInBuilding[iBuildingLoop]) AND bOrganisation)
									OR (IS_PROPERTY_CLUBHOUSE(building.iPropertiesInBuilding[iBuildingLoop]) AND bBikerGang))
									AND GangLeaderID != INVALID_PLAYER_INDEX()
									AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
										IF playerID = GangLeaderID
											iKnowState = 1
											#IF IS_DEBUG_BUILD
												IF bDebugOutput
													PRINTLN("I know player they are my boss Property = ", building.iPropertiesInBuilding[iBuildingLoop])
												ENDIF
											#ENDIF
											RETURN TRUE
											#IF IS_DEBUG_BUILD
										ELSE
											IF bDebugOutput
												IF NOT IS_NET_PLAYER_OK(GangLeaderID,FALSE)
													PRINTLN("I DO NOT know player gang leader not ok")
												ENDIF
												IF playerID != GangLeaderID
													PRINTLN("I DO NOT know player gang leader != player ")
												ENDIF
												
												IF NOT ((IS_PROPERTY_OFFICE(building.iPropertiesInBuilding[iBuildingLoop]) AND bOrganisation)
												OR (IS_PROPERTY_CLUBHOUSE(building.iPropertiesInBuilding[iBuildingLoop]) AND bBikerGang))
													PRINTLN("I DO NOT know player Property = ",building.iPropertiesInBuilding[iBuildingLoop])
												ENDIF
												
											ENDIF
										#ENDIF
										ENDIF
									ELSE
										//AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
										g_ClanDescription = GET_PLAYER_CREW(PLAYER_ID())
										g_otherClanDescription = GET_PLAYER_CREW(playerID)
										IF g_ClanDescription.Id = g_otherClanDescription.Id
											iKnowState = 1
											#IF IS_DEBUG_BUILD
												IF bDebugOutput
													PRINTLN("I know player they are in my clan Property = ", building.iPropertiesInBuilding[iBuildingLoop])
												ENDIF
											#ENDIF
											RETURN TRUE
										ELSE
											g_GamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
											IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
												IF NETWORK_IS_FRIEND(g_GamerHandle)	
													iKnowState = 1
													#IF IS_DEBUG_BUILD
														IF bDebugOutput
															PRINTLN("I know player they are my friend Property = ", building.iPropertiesInBuilding[iBuildingLoop])
														ENDIF
													#ENDIF
													RETURN TRUE
												ENDIF
											ENDIF
											
											IF IS_PROPERTY_OFFICE(building.iPropertiesInBuilding[iBuildingLoop])
											AND GangLeaderID != INVALID_PLAYER_INDEX()
											AND IS_NET_PLAYER_OK(GangLeaderID,FALSE)
											AND playerID = GangLeaderID
												iKnowState = 1
												#IF IS_DEBUG_BUILD
													IF bDebugOutput
														PRINTLN("I know player they are my boss Property = ", building.iPropertiesInBuilding[iBuildingLoop])
													ENDIF
												#ENDIF
												RETURN TRUE
											ENDIF
											
											/*REPEAT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.iNumBuddies iBuddyLoop
												IF IS_GAMER_HANDLE_VALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.BuddyGamerInfo[iBuddyLoop].Handle)
													IF NETWORK_ARE_HANDLES_THE_SAME(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBuddies.BuddyGamerInfo[iBuddyLoop].Handle,g_GamerHandle)
														iKnowState = 1
														#IF IS_DEBUG_BUILD
															IF bDebugOutput
																PRINTLN("I know player they are buddy Property = ", iProperty)
															ENDIF
														#ENDIF
														RETURN TRUE
													ENDIF
												ENDIF
											ENDREPEAT*/
										
										ENDIF
									ENDIF
								#IF IS_DEBUG_BUILD
								ELSE
									IF bDebugOutput
										PRINTLN("This Player doesn't own iProperty = ", building.iPropertiesInBuilding[iBuildingLoop])
									ENDIF
								#ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ELSE
		IF iKnowState = 1
			RETURN TRUE
		ENDIF
	ENDIF
	iKnowState = 0
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANYONE_IN_GAME_OWN_PROPERTY_IN_BUILDING(INT& iKnowState,INT iBuilding )//#IF IS_DEBUG_BUILD, BOOL bDebugOutput = FALSE #ENDIF)
	INT i, iBuildingLoop, k//, iBuddyLoop
	BUILDING_PROPERTIES building
	GET_BUILDING_PROPERTIES(building,iBuilding)
	IF iKnowState < 3
		REPEAT building.iNumProperties iBuildingLoop
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
				IF playerID != PLAYER_ID()
				//OR bIncludeLocalPlayer
					IF IS_NET_PLAYER_OK(playerID,FALSE)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = building.iPropertiesInBuilding[iBuildingLoop]
							REPEAT MAX_OWNED_PROPERTIES k
								IF iKnowState < 3
									IF (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = building.iPropertiesInBuilding[iBuildingLoop]
									AND NOT IS_PROPERTY_OFFICE_GARAGE(building.iPropertiesInBuilding[iBuildingLoop])
									 )
									OR (GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(building.iPropertiesInBuilding[iBuildingLoop]) = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k]
									AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(playerID,building.iPropertiesInBuilding[iBuildingLoop]))
										IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
											SET_BIT(iKnowState,2)
											//PRINTLN("Someone I know owns property # ",building.iPropertiesInBuilding[iBuildingLoop]," and they are in garage")
										ELSE
											SET_BIT(iKnowState,1)
											//PRINTLN("Someone I know owns property # ",building.iPropertiesInBuilding[iBuildingLoop]," and they are in house")
										ENDIF
									ENDIF
								ENDIF
							ENDREPEAT
						//#IF IS_DEBUG_BUILD
						///ELSE
							//IF bDebugOutput
								//PRINTLN("This Player is not inside or  doesn't own iProperty  = ",building.iPropertiesInBuilding[iBuildingLoop])
							//ENDIF
						//#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
	ELSE
		RETURN TRUE
	ENDIF
	IF iKnowState != 0
		RETURN TRUE
	ENDIF
	//iKnowState = 0
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANYONE_IN_GAME_OWN_YACHT(INT& iKnowState,INT iYacht)//#IF IS_DEBUG_BUILD, BOOL bDebugOutput = FALSE #ENDIF)
	INT i
	IF iKnowState < 3
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			IF playerID != PLAYER_ID()
				IF IS_NET_PLAYER_OK(playerID,FALSE)
					IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(playerID) = iYacht
					AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = PROPERTY_YACHT_APT_1_BASE
					AND IS_PLAYER_IN_MP_PROPERTY(playerID,TRUE)
						SET_BIT(iKnowState,1)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		RETURN TRUE
	ENDIF
	IF iKnowState != 0
		RETURN TRUE
	ENDIF
	//iKnowState = 0
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_ANYONE_IN_GAME_OWN_PROPERTY(INT& iKnowState,INT iProperty #IF IS_DEBUG_BUILD, BOOL bDebugOutput = FALSE #ENDIF)
	INT i,k//, iBuddyLoop

	IF iKnowState < 3
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			IF playerID != PLAYER_ID()
				IF IS_NET_PLAYER_OK(playerID,FALSE)
					REPEAT MAX_OWNED_PROPERTIES k
						IF iKnowState < 3
							IF (((GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = iProperty
							AND NOT IS_PROPERTY_OFFICE_GARAGE(iProperty)
							 )
							OR (GET_OFFICE_GARAGE_BASE_ID_FROM_PROPERTY(iProperty) = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k]
							AND DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(playerID,iProperty))
							 )
							AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = iProperty)
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
									SET_BIT(iKnowState,2)
									//PRINTLN("Someone I know owns property # ",iProperty," and they are in garage")
								ELSE
									SET_BIT(iKnowState,1)
									//PRINTLN("Someone I know owns property # ",iProperty," and they are in house")
								ENDIF
							#IF IS_DEBUG_BUILD
							ELSE
								IF bDebugOutput
									//PRINTLN("This Player is not inside or  doesn't own iProperty  = ", iProperty)
								ENDIF
							#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		RETURN TRUE
	ENDIF
	IF iKnowState != 0
		RETURN TRUE
	ENDIF
	//iKnowState = 0
	RETURN FALSE
ENDFUNC

PROC CHECK_OWNERSHIP_STATE_FOR_BUILDING(INT& iKnowState,INT iProperty)
	INT i, j, k
	BUILDING_PROPERTIES building
	IF iKnowState < 3
		REPEAT NUM_NETWORK_PLAYERS i
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
			IF playerID != PLAYER_ID()
				IF IS_NET_PLAYER_OK(playerID,FALSE)
					GET_BUILDING_PROPERTIES(building,GET_PROPERTY_BUILDING(iProperty))
					REPEAT building.iNumProperties j
						REPEAT MAX_OWNED_PROPERTIES k
							IF iKnowState < 3
								IF (GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[k] = building.iPropertiesInBuilding[j]
								AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty = building.iPropertiesInBuilding[j])
									IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
										SET_BIT(iKnowState,2)
										//PRINTLN("Someone I know owns property # ",iProperty," and they are in garage")
									ELSE
										SET_BIT(iKnowState,1)
										//PRINTLN("Someone I know owns property # ",iProperty," and they are in house")
									ENDIF
								ENDIF
							ENDIF
						ENDREPEAT
					ENDREPEAT
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		EXIT
	ENDIF
	IF iKnowState != 0
		EXIT
	ENDIF
	//iKnowState = 0
ENDPROC


FUNC INT GET_VALUE_OF_PROPERTY(INT iPropertyIndex)
	IF USE_SERVER_TRANSACTIONS()
		INT iPropertyVariation = 0
		BOOL bStarterPackItem = FALSE
		IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyIndex)
			IF NOT COULD_PROPERTY_BE_FREE_FOR_PLAYER_WITH_CRIMINAL_ENTERPRISE_STARTER_PACK(iPropertyIndex)
				iPropertyVariation = 1
			ELSE
				bStarterPackItem = TRUE
			ENDIF
		ENDIF
		
		INT iPropertyCatalogueKey = GET_PROPERTY_KEY_FOR_CATALOGUE(iPropertyIndex, iPropertyVariation, bStarterPackItem)
		IF NET_GAMESERVER_CATALOG_ITEM_KEY_IS_VALID(iPropertyCatalogueKey)
			RETURN NET_GAMESERVER_GET_PRICE(iPropertyCatalogueKey, CATEGORY_PROPERTIE, 1)
		ENDIF
	ENDIF
	
	RETURN mpProperties[iPropertyIndex].iValue
ENDFUNC

FUNC INT GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS(INT iOwnedSlot, BOOL bForTradePrice)
	INT iCost = 0
//	INT iOfficeStylePrice = 0
	INT iOfficeGunLockerPrice = 0, iOfficeVaultPrice = 0, iOfficeAccommodationPrice = 0
	
	UNUSED_PARAMETER(iOwnedSlot)
//	INT iOwned = GET_OWNED_PROPERTY(iOwnedSlot)
	
//	INT iOldStyle = GET_OWNED_PROPERTY_VARIATION(iOwnedSlot)
//	INT iOldStyleSF = -1
//	IF (iOldStyle != 0)
//		iOldStyleSF = GET_SF_FROM_OFFICE_INTERIOR(iOldStyle)
//	ENDIF
	
//	IF USE_SERVER_TRANSACTIONS()
//		iOfficeStylePrice = GET_MP_PROPERTY_INTERIOR_VALUE(iOldStyle, iOwned)
//	ELSE
//		iOfficeStylePrice = GET_MP_PROPERTY_INTERIOR_VALUE(iOldStyle-1, iOwned)
//	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	// use GET_PROPERTY_INTERIOR_NAME(INT iIndex, INT iCurrentProperty)  // //
//	TEXT_LABEL tlOldStyle = "PM_OFF_VAR_"									//
//	tlOldStyle += iOldStyleSF												//
//	// // // // // // // // // // // // // // // // // // // // // // // // //
//	STRING sOldStyleLabel = "null"
//	IF DOES_TEXT_LABEL_EXIST(tlOldStyle)
//		sOldStyleLabel = GET_STRING_FROM_TEXT_FILE(tlOldStyle)
//	ENDIF
//	#ENDIF

//	CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: interior currently ",
//			sOldStyleLabel, ":", iOldStyle,
//			" [sf:", iOldStyleSF,
//			"]")
//	iCost += iOfficeStylePrice
	
	
	IF IS_OFFICE_GUN_LOCKER_PURCHASED()
		iOfficeGunLockerPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_GUN)
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: Adding gunlocker: add $", iOfficeGunLockerPrice)
		iCost += iOfficeGunLockerPrice
	ENDIF
	IF IS_OFFICE_CASH_VAULT_PURCHASED()
		iOfficeVaultPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_LOCKER_CASH)
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: Adding vault: add $", iOfficeVaultPrice)
		iCost += iOfficeVaultPrice
	ENDIF
	IF IS_OFFICE_ACCOMMODATION_PURCHASED()
		iOfficeAccommodationPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_ACCOMMODATION)
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: Adding accommodation: add $", iOfficeAccommodationPrice)
		iCost += iOfficeAccommodationPrice
	ENDIF
	
//	INT iOfficeModshopPrice = 0
//	IF IS_OFFICE_MODSHOP_PURCHASED()
//		iOfficeModshopPrice = GET_OFFICE_PROPERTY_MOD_VALUE(OFFICE_MOD_MODSHOP, GET_MP_INT_CHARACTER_STAT(MP_STAT_PROP_OFFICE_MODSHOP))
//		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: Adding garage 4: add $", iOfficeModshopPrice)
//		iCost += iOfficeModshopPrice
//	ENDIF
	
	IF bForTradePrice
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: Trade value of changes: $", iCost, "*0.5 =  $", ROUND(TO_FLOAT(iCost)*0.5))
		iCost = ROUND(TO_FLOAT(iCost)*0.5)
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS: Total cost of changes: $", iCost)
	RETURN iCost
ENDFUNC

FUNC INT GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS(INT iOwnedSlot, BOOL bForTradePrice)
	INT iCost = 0
//	INT iClubhouseStylePrice = 0
	INT iClubhouseGunLockerPrice = 0, iClubhouseGaragePrice = 0
	
	UNUSED_PARAMETER(iOwnedSlot)
//	INT iOwned = GET_OWNED_PROPERTY(iOwnedSlot)
	
//	INT iOldStyle = GET_OWNED_PROPERTY_VARIATION(iOwnedSlot)
//	INT iOldStyleSF = -1
//	IF (iOldStyle != 0)
//		iOldStyleSF = GET_SF_FROM_CLUBHOUSE_INTERIOR(iOldStyle)
//	ENDIF
	
//	IF USE_SERVER_TRANSACTIONS()
//		iClubhouseStylePrice = GET_MP_PROPERTY_INTERIOR_VALUE(iOldStyle, iOwned)
//	ELSE
//		iClubhouseStylePrice = GET_MP_PROPERTY_INTERIOR_VALUE(iOldStyle-1, iOwned)
//	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	// use GET_PROPERTY_INTERIOR_NAME(INT iIndex, INT iCurrentProperty)  // //
//	TEXT_LABEL tlOldStyle = "PM_OFF_VAR_"									//
//	tlOldStyle += iOldStyleSF												//
//	// // // // // // // // // // // // // // // // // // // // // // // // //
//	STRING sOldStyleLabel = "null"
//	IF DOES_TEXT_LABEL_EXIST(tlOldStyle)
//		sOldStyleLabel = GET_STRING_FROM_TEXT_FILE(tlOldStyle)
//	ENDIF
//	#ENDIF

//	CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS: interior currently ",
//			sOldStyleLabel, ":", iOldStyle,
//			" [sf:", iOldStyleSF,
//			"]")
//	iCost += iClubhouseStylePrice
	
	
	IF IS_CLUBHOUSE_GUN_LOCKER_PURCHASED()
		iClubhouseGunLockerPrice = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_LOCKER_GUN, 1)
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS: Adding gunlocker: add $", iClubhouseGunLockerPrice)
		iCost += iClubhouseGunLockerPrice
	ENDIF
	IF IS_CLUBHOUSE_GARAGE_PURCHASED()
		iClubhouseGaragePrice = GET_CLUBHOUSE_PROPERTY_MOD_VALUE(CLUBHOUSE_MOD_GARAGE, 1)
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS: Adding garage: add $", iClubhouseGaragePrice)
		iCost += iClubhouseGaragePrice
	ENDIF
	
	IF bForTradePrice
		CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS: Trade value of changes: $", iCost, "*0.5 =  $", ROUND(TO_FLOAT(iCost)*0.5))
		iCost = ROUND(TO_FLOAT(iCost)*0.5)
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS: Total cost of changes: $", iCost)
	RETURN iCost
ENDFUNC

FUNC INT GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(INT iPropertySlot)
	INT iArrayIndex
	IF GET_OWNED_PROPERTY(iPropertySlot) > 0
		INT iCost = 0
		
		IF USE_SERVER_TRANSACTIONS()
			iCost = ROUND(GET_VALUE_OF_PROPERTY(GET_OWNED_PROPERTY(iPropertySlot))*0.5)
		ENDIF
		
		IF iPropertySlot = 0
			iCost = ROUND(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iCurrentPropertyValue*0.5)
		ELIF iPropertySlot = 1
			iCost = ROUND(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiProperty1Value*0.5)
		ELIF iPropertySlot < MAX_OWNED_PROPERTIES
			iArrayIndex = iPropertySlot-2
			iCost = ROUND(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iMultiPropertyValueArray[iArrayIndex]*0.5)
		ENDIF
		
		IF (iPropertySlot = PROPERTY_OWNED_SLOT_OFFICE_0)
			iCost += GET_VALUE_OF_CURRENTLY_OWNED_OFFICE_MODS(iPropertySlot, TRUE)
		ELIF (iPropertySlot = PROPERTY_OWNED_SLOT_CLUBHOUSE)
			iCost += GET_VALUE_OF_CURRENTLY_OWNED_CLUBHOUSE_MODS(iPropertySlot, TRUE)
		ENDIF
		
		RETURN iCost
	ENDIF
	RETURN 0
ENDFUNC

FUNC BOOL IS_OPTOUT_FOR_PROPERTY(TEXT_LABEL_15 tl_PropertyName, INT &iOptOutSlot)
	
	INT iPropertyNameHash = GET_HASH_KEY(tl_PropertyName)
	IF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE0)
		iOptOutSlot = 0		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE1)
		iOptOutSlot = 1		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE2)
		iOptOutSlot = 2		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE3)
		iOptOutSlot = 3		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE4)
		iOptOutSlot = 4		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE5)
		iOptOutSlot = 5		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE6)
		iOptOutSlot = 6		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE7)
		iOptOutSlot = 7		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE8)
		iOptOutSlot = 8		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE9)
		iOptOutSlot = 9		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE10)
		iOptOutSlot = 10	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE11)
		iOptOutSlot = 11	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE12)
		iOptOutSlot = 12	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE13)
		iOptOutSlot = 13	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE14)
		iOptOutSlot = 14	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE15)
		iOptOutSlot = 15	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE16)
		iOptOutSlot = 16	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE17)
		iOptOutSlot = 17	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE18)
		iOptOutSlot = 18	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.iOPTOUT_PROPERTYWEBSITE_SALE19)
		iOptOutSlot = 19	RETURN TRUE
	ENDIF
	
	iOptOutSlot = -1
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_REBATE_ACTIVE_FOR_PROPERTY(TEXT_LABEL_15 tl_PropertyName, INT &iRebateSlot)
	IF g_sMPTunables.bREBATE_PROPERTY_ALL
		iRebateSlot = HASH("REBATE_PROPERTY_ALL")
		RETURN TRUE
	ENDIF
	
	INT iPropertyNameHash = GET_HASH_KEY(tl_PropertyName)
	IF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_0)
		iRebateSlot = 0		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_1)
		iRebateSlot = 1		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_2)
		iRebateSlot = 2		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_3)
		iRebateSlot = 3		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_4)
		iRebateSlot = 4		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_5)
		iRebateSlot = 5		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_6)
		iRebateSlot = 6		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_7)
		iRebateSlot = 7		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_8)
		iRebateSlot = 8		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_9)
		iRebateSlot = 9		RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_10)
		iRebateSlot = 10	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_11)
		iRebateSlot = 11	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_12)
		iRebateSlot = 12	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_13)
		iRebateSlot = 13	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_14)
		iRebateSlot = 14	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_15)
		iRebateSlot = 15	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_16)
		iRebateSlot = 16	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_17)
		iRebateSlot = 17	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_18)
		iRebateSlot = 18	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_19)
		iRebateSlot = 19	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_20)
		iRebateSlot = 20	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_21)
		iRebateSlot = 21	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_22)
		iRebateSlot = 22	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_23)
		iRebateSlot = 23	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_24)
		iRebateSlot = 24	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_25)
		iRebateSlot = 25	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_26)
		iRebateSlot = 26	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_27)
		iRebateSlot = 27	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_28)
		iRebateSlot = 28	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_29)
		iRebateSlot = 29	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_30)
		iRebateSlot = 30	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_31)
		iRebateSlot = 31	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_32)
		iRebateSlot = 32	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_33)
		iRebateSlot = 33	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_34)
		iRebateSlot = 34	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_35)
		iRebateSlot = 35	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_36)
		iRebateSlot = 36	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_37)
		iRebateSlot = 37	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_38)
		iRebateSlot = 38	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_39)
		iRebateSlot = 39	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_40)
		iRebateSlot = 40	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_41)
		iRebateSlot = 41	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_42)
		iRebateSlot = 42	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_43)
		iRebateSlot = 43	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_44)
		iRebateSlot = 44	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_45)
		iRebateSlot = 45	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_46)
		iRebateSlot = 46	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_47)
		iRebateSlot = 47	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_48)
		iRebateSlot = 48	RETURN TRUE
	ELIF (iPropertyNameHash = g_sMPTunables.REBATE_PROPERTY_49)
		iRebateSlot = 49	RETURN TRUE
	ENDIF
	
	iRebateSlot = -1
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_SALE_ACTIVE_FOR_PROPERTY(INT iPropertyIndex)
	#IF IS_DEBUG_BUILD
	DEBUG_CHANNELS eChannel = DEBUG_INTERNET
	IF ARE_STRINGS_EQUAL(GET_THIS_SCRIPT_NAME(), "am_mp_property_ext")
		eChannel = DEBUG_SAFEHOUSE
	ENDIF
	#ENDIF
	
	INT iOptOutSlot = -1
	IF g_sMPTunables.bPROPERTYWEBSITE_SALE
	AND NOT IS_OPTOUT_FOR_PROPERTY(mpProperties[iPropertyIndex].tl_PropertyName, iOptOutSlot)
		IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyIndex)
			RETURN FALSE
		ENDIF
		
		INT iPrice = GET_MP_PROPERTY_VALUE(iPropertyIndex)
		INT iOriginalPrice = GET_MP_PROPERTY_ORIGINAL_VALUE(iPropertyIndex)
		
		#IF IS_DEBUG_BUILD
		TEXT_LABEL_15 sPropertyName = GET_PROPERTY_NAME(iPropertyIndex)
		#ENDIF
		
		IF iOriginalPrice != 0
		AND iOriginalPrice > iPrice
			CPRINTLN(eChannel, "IS_SALE_ACTIVE_FOR_PROPERTY: ", GET_THIS_SCRIPT_NAME(), " bPROPERTYWEBSITE_SALE is true, ", sPropertyName, " \"", GET_STRING_FROM_TEXT_FILE(sPropertyName), "\" is $", iOriginalPrice, " > $", iPrice, " - isReduced!!!")
			RETURN TRUE
		ELSE
			CDEBUG3LN(eChannel, "IS_SALE_ACTIVE_FOR_PROPERTY: ", GET_THIS_SCRIPT_NAME(), " bPROPERTYWEBSITE_SALE is true, ", sPropertyName, " \"", GET_STRING_FROM_TEXT_FILE(sPropertyName), "\" is $", iOriginalPrice, " <= $", iPrice, " - is not reduced...")
		ENDIF
	ELSE
		CDEBUG3LN(eChannel, "IS_SALE_ACTIVE_FOR_PROPERTY: ", GET_THIS_SCRIPT_NAME(), " bPROPERTYWEBSITE_SALE is false (iOptOutSlot:", iOptOutSlot, ")")
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL GET_SALE_INFORMATION_FOR_PROPERTY(INT iPropertyIndex, TEXT_LABEL_31 &tlSaleString)
	
	INT iRebateSlot = -1
	IF (iPropertyIndex = 999)
		tlSaleString = "WEB_VEHICLE_CASH_BACK"
		RETURN TRUE
	ELIF IS_REBATE_ACTIVE_FOR_PROPERTY(mpProperties[iPropertyIndex].tl_PropertyName, iRebateSlot)
		tlSaleString = "WEB_VEHICLE_REBATE"
		RETURN FALSE	//sale banner is FALSE when rebate is active
	ELIF IS_SALE_ACTIVE_FOR_PROPERTY(iPropertyIndex)
		tlSaleString = ""					//WEB_VEHICLE_SALE
		RETURN TRUE
	ENDIF
	
	tlSaleString = ""
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_UPDATE_CUSTOM_APT()
	RETURN g_bUpdateCustomApt
ENDFUNC
PROC SET_UPDATE_CUSTOM_APT(BOOL bSet)
	CDEBUG3LN(DEBUG_SAFEHOUSE, "SET_UPDATE_CUSTOM_APT: ", GET_THIS_SCRIPT_NAME(), " ", GET_STRING_FROM_BOOL(bSet))
	g_bUpdateCustomApt = bSet
ENDPROC

FUNC BOOL IS_PROPERTY_CUSTOMISABLE(INT iPropertyID)
	SWITCH iPropertyID	
		CASE PROPERTY_CUSTOM_APT_1_BASE		
		CASE PROPERTY_CUSTOM_APT_2			
		CASE PROPERTY_CUSTOM_APT_3		
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
	
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
	
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
		
		#IF FEATURE_DLC_2_2022
		CASE PROPERTY_MULTISTOREY_GARAGE
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PROPERTY_CUSTOM_APARTMENT_BITFLAG(INT iPropertyID, INT &iBitflag)
	IF IS_PROPERTY_CUSTOMISABLE(iPropertyID)
		iBitflag = 0
		
		INT iInterior
		REPEAT GET_MAX_INTERIOR_VARIATIONS_FOR_PROPERTY(iPropertyID) iInterior
			IF NOT DISABLE_INTERIOR_VARIATION(iInterior, iPropertyID)
				SET_BIT(iBitflag, iInterior)
			ELSE
				CLEAR_BIT(iBitflag, iInterior)
			ENDIF
		ENDREPEAT
		PRINTLN("GET_PROPERTY_CUSTOM_APARTMENT_BITFLAG iPropertyID:", iPropertyID, ",iBitflag: ", iBitflag)
		RETURN TRUE
	ENDIF
	
	iBitflag = 0
	RETURN FALSE
ENDFUNC

PROC DETERMINE_4_TOP_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(ENTITY_INDEX thisEntity,MODEL_NAMES theModel, VECTOR &FrontTopLeft, VECTOR &FrontTopRight, VECTOR &BackTopLeft, VECTOR &BackTopRight)
	VECTOR modelMin,modelMax
	GET_MODEL_DIMENSIONS(theModel,modelMin,modelMax)
	VECTOR tempFrontTopLeft, tempFrontTopRight, tempBackTopLeft, tempBackTopRight	
	//   *---*
	//  /|  /|
	// *-o-*-o
	// |/  |/
	// o---o
	
	//NET_PRINT_STRING_VECTOR("Model dimensions modelMin: ",modelMin) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions modelMax: ",modelMax) NET_NL()
	tempFrontTopLeft.x = modelMin.x
	tempFrontTopLeft.y = modelMax.y
	tempFrontTopLeft.z = modelMax.z
	
	tempFrontTopRight = modelMax

	tempBackTopLeft.x = modelMin.x
	tempBackTopLeft.y = modelMin.y
	tempBackTopLeft.z = modelMax.z
	
	tempBackTopRight.x = modelMax.x
	tempBackTopRight.y = modelMin.y
	tempBackTopRight.z = modelMax.z
	
	//NET_PRINT_STRING_VECTOR("Model dimensions FrontTopLeft: ",tempFrontTopLeft) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions FrontTopRight: ",tempFrontTopRight) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions BackTopLeft: ",tempBackTopLeft) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions BackTopRight: ",tempBackTopRight) NET_NL()
	FrontTopLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity,tempFrontTopLeft)
	FrontTopRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity,tempFrontTopRight) 
	BackTopLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity,tempBackTopLeft)
	BackTopRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity,tempBackTopRight)
	//NET_PRINT_STRING_VECTOR("Model dimensions in world coords FrontTopLeft: ",FrontTopLeft) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions in world coords  FrontTopRight: ",FrontTopRight) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions in world coords BackTopLeft: ",BackTopLeft) NET_NL()
	//NET_PRINT_STRING_VECTOR("Model dimensions in world coords BackTopRight: ",BackTopRight) NET_NL()
	//DRAW_DEBUG_SPHERE(FrontTopLeft,0.1,255,0,0,255)
	//DRAW_DEBUG_SPHERE(FrontTopRight,0.1,255,0,0,255)
	//DRAW_DEBUG_SPHERE(BackTopLeft,0.1,255,0,0,255)
	//DRAW_DEBUG_SPHERE(BackTopRight,0.1,255,0,0,255)
ENDPROC

PROC DETERMINE_4_BOTTOM_VECTORS_OF_MODEL_BOX_IN_WORLD_COORDS(ENTITY_INDEX thisEntity, MODEL_NAMES theModel, VECTOR &FrontBottomLeft, VECTOR &FrontBottomRight, VECTOR &BackBottomLeft, VECTOR &BackBottomRight)
	VECTOR modelMin, modelMax
	GET_MODEL_DIMENSIONS(theModel, modelMin, modelMax)
	VECTOR tempFrontBottomLeft, tempFrontBottomRight, tempBackBottomLeft, tempBackBottomRight	
	
	//   o---o
	//  /|  /|
	// o-*-o-*
	// |/  |/
	// *---*
	
	tempFrontBottomLeft.X = modelMin.X
	tempFrontBottomLeft.Y = modelMax.Y
	tempFrontBottomLeft.Z = modelMin.Z
	
	tempFrontBottomRight.X = modelMax.X
	tempFrontBottomRight.Y = modelMax.Y
	tempFrontBottomRight.Z = modelMin.Z
	
	tempBackBottomLeft.X = modelMin.X
	tempBackBottomLeft.Y = modelMin.Y
	tempBackBottomLeft.Z = modelMin.Z
	
	tempBackBottomRight.X = modelMax.X
	tempBackBottomRight.Y = modelMin.Y
	tempBackBottomRight.Z = modelMin.Z
	
	FrontBottomLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempFrontBottomLeft)
	FrontBottomRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempFrontBottomRight) 
	BackBottomLeft = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempBackBottomLeft)
	BackBottomRight = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisEntity, tempBackBottomRight)
ENDPROC

PROC CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iMenuType = -1, BOOL bSetExitFlag = FALSE)
	IF IS_BIT_SET(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
		bSetExitFlag = TRUE
	ENDIF	
	control.iStage = 0
	control.iCurrentVertSel = 0
	control.iMaxVertSel =  0
//	IF control.warningscreen != NULL
//		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(control.warningscreen)
//		PRINTLN("CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU: cleaning up scaleform movie.")
//	ENDIF
	control.iBS = 0
	IF bSetExitFlag
		SET_BIT(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
	ENDIF	
	control.iButtonBS = 0
	CLEANUP_MENU_ASSETS(DEFAULT, iMenuType)
ENDPROC

FUNC BOOL REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(REPLACE_MP_VEH_OR_PROP_MENU &control, CONTROL_ACTION theInput)
	IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,theInput))
	OR HAS_NET_TIMER_EXPIRED(control.menuNavigationDelay,250,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Retursn TRUE if the language needs the font to be condensed due to overlap issues.
FUNC BOOL DOES_LANGUAGE_NEED_CONDENSED_FONT_FOR_PROPERTY_NAME()
	
	SWITCH GET_CURRENT_LANGUAGE()
		//CASE LANGUAGE_CHINESE //Can't use condensed, instead we will use Shorter names
		CASE LANGUAGE_RUSSIAN
		CASE LANGUAGE_PORTUGUESE
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Retursn TRUE if the language needs to use the short version of the Property Name due to overlap issues.
FUNC BOOL DOES_LANGUAGE_NEED_TO_USE_SHORT_PROPERTY_NAME()
	
	SWITCH GET_CURRENT_LANGUAGE()
		CASE LANGUAGE_CHINESE
		CASE LANGUAGE_CHINESE_SIMPLIFIED
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PROPERTY_NEED_TO_USE_SHORT_NAME(INT iProperty)
	IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
	OR iProperty = PROPERTY_CUSTOM_APT_2
	OR iProperty = PROPERTY_CUSTOM_APT_3
//	OR iProperty = PROPERTY_OFFICE_2_BASE
//	OR iProperty = PROPERTY_OFFICE_4
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HANDLE_REPLACE_MP_SV_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control, BOOL bShopSetup, BOOL bSkipDescriptionUpdate, MODEL_NAMES vehModel, BOOL bShowNewVehicleNameInDescription = FALSE)
	//item description
	IF NOT bSkipDescriptionUpdate
		IF control.iLevel = 0
			IF control.iSlotIDS[control.iCurrentVertSel] = PROPERTY_OWNED_SLOT_HANGAR
				IF !bShowNewVehicleNameInDescription
					SET_CURRENT_MENU_ITEM_DESCRIPTION("DEL_VEH_SEL1ac",100)
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("CAS_VEH_SELLV")
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehModel))
				ENDIF
			ELSE
				IF !bShowNewVehicleNameInDescription
					SET_CURRENT_MENU_ITEM_DESCRIPTION("DEL_VEH_SEL1",100)
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("CAS_VEH_SELLV")
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(vehModel))
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
				IF DOES_VEHICLE_SLOT_BELONG_TO_HANGAR(control.iSlotIDS[control.iCurrentVertSel],DEFAULT,DEFAULT,TRUE)
					SET_WARNING_MESSAGE_WITH_HEADER("PLYVEH_CM_CONT", "PLYVEH_CM_1Bac",FE_WARNING_YES|FE_WARNING_NO)
					PRINTLN("HANDLE_REPLACE_MP_SV_MENU_DESCRIPTION: control.iSlotIDS[control.iCurrentVertSel] =", control.iSlotIDS[control.iCurrentVertSel])
				ELSE
					SET_WARNING_MESSAGE_WITH_HEADER("PLYVEH_CM_CONT", "PLYVEH_CM_1B",FE_WARNING_YES|FE_WARNING_NO)
				ENDIF

				//SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_SC_2",100)
			ELSE
				IF IS_BIT_SET(control.iEmptyPositionBS,control.iCurrentVertSelSecLevel)
					IF DOES_VEHICLE_SLOT_BELONG_TO_HANGAR(control.iSlotIDS[control.iCurrentVertSel],DEFAULT,DEFAULT,TRUE)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("DEL_VEH_SEL2ac",100)
					ELSE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("DEL_VEH_SEL2",100)
					ENDIF
				ELSE
					IF bShopSetup
						IF DOES_VEHICLE_SLOT_BELONG_TO_HANGAR(control.iSlotIDS[control.iCurrentVertSel])
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_SC_3ac",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_SC_3",100)
						ENDIF
					ELSE
						IF DOES_VEHICLE_SLOT_BELONG_TO_HANGAR(control.iSlotIDS[control.iCurrentVertSel],DEFAULT,DEFAULT,TRUE)
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_SC_1ac",100)
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_SC_1",100)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_REPLACE_MP_SV_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control,MODEL_NAMES vehModel,BOOL bShopSetup)
	INT iMenuCounter = 0
	INT i
	CLEAR_MENU_DATA()
	
	IF bShopSetup
		ADD_SHOP_MENU_GRAPHIC_TO_MENU(GET_SHOP_PLAYER_LAST_USED())
	ENDIF
	
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)	
	BOOL bSkip = FALSE
	INT iProperty
	INT iGarageStartIndex
	INT iBikeStartIndex
	TEXT_LABEL_15 tlShortPropertyName
	BOOL bCycles = IS_VEHICLE_A_CYCLE(vehModel)
	INT iDisplaySlot
	INT iSaveIndex
	SPECIAL_PV_PROPERTY SPVProperty
	
	INT iComponents = 1
	TEXT_LABEL_23 textLabel
	TEXT_LABEL_63 tl23VehName
							
	SWITCH control.iLevel
		CASE 0
			control.iFirstLevelSelectedSlotID = 0
			SET_MENU_TITLE("DEL_VEH_SEL0")
			REPEAT MAX_OWNED_PROPERTIES i
				iProperty = GET_OWNED_PROPERTY(i)
				PRINTLN("SETUP_REPLACE_PROPERTY_MENU: GET_OWNED_PROPERTY(",i,") = ",iProperty)
				IF iProperty > 0
				AND DOES_PROPERTY_HAVE_GARAGE(iProperty)
				#IF FEATURE_CASINO_HEIST
				AND CAN_GARAGE_BE_USED(i)
				#ENDIF
					IF NOT IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(vehModel,i)
					ELSE
						TEXT_LABEL_15 tl_PropertyName
						tl_PropertyName = mpProperties[iProperty].tl_PropertyName
						IF (iProperty = PROPERTY_HANGAR)
							tl_PropertyName = GET_HANGAR_NAME_FROM_ID(GET_PLAYERS_OWNED_HANGAR(PLAYER_ID()))
						ENDIF
						IF (iProperty = PROPERTY_DEFUNC_BASE)
							tl_PropertyName = GET_DEFUNCT_BASE_NAME_FROM_ID(GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID()))
						ENDIF
						IF (iProperty = PROPERTY_NIGHTCLUB)
							tl_PropertyName = "MP_BHUB_CLUBG"
						ENDIF
						IF (iProperty = PROPERTY_MEGAWARE_GARAGE_LVL1)
							tl_PropertyName = GET_BUSINESS_HUB_GARAGE_NAME_FROM_ID(1)
						ENDIF
						IF (iProperty = PROPERTY_MEGAWARE_GARAGE_LVL2)
							tl_PropertyName = GET_BUSINESS_HUB_GARAGE_NAME_FROM_ID(2)
						ENDIF
						IF (iProperty = PROPERTY_MEGAWARE_GARAGE_LVL3)
							tl_PropertyName = GET_BUSINESS_HUB_GARAGE_NAME_FROM_ID(3)
						ENDIF
						
						IF (iProperty = PROPERTY_ARENAWARS_GARAGE_LVL1)
							tl_PropertyName = GET_ARENA_GARAGE_FLOOR_NAME_FROM_ID(1)
						ENDIF
						IF (iProperty = PROPERTY_ARENAWARS_GARAGE_LVL2)
							tl_PropertyName = GET_ARENA_GARAGE_FLOOR_NAME_FROM_ID(2)
						ENDIF
						IF (iProperty = PROPERTY_ARENAWARS_GARAGE_LVL3)
							tl_PropertyName = GET_ARENA_GARAGE_FLOOR_NAME_FROM_ID(3)
						ENDIF
						IF (iProperty = PROPERTY_CASINO_GARAGE)
							tl_PropertyName = "CASINO_GARNAME"
						ENDIF
						IF (iProperty = PROPERTY_ARCADE_GARAGE)
							tl_PropertyName = "ARCADE_GARNAME"
						ENDIF
						#IF FEATURE_TUNER
						IF (iProperty = PROPERTY_AUTO_SHOP)
							tl_PropertyName = "AUT_SHP_GAR"
						ENDIF
						#ENDIF
						#IF FEATURE_FIXER
						IF (iProperty = PROPERTY_SECURITY_OFFICE_GARAGE)
							tl_PropertyName = "FIXER_GARNAME"
						ENDIF
						#ENDIF

						
						IF NOT DOES_LANGUAGE_NEED_CONDENSED_FONT_FOR_PROPERTY_NAME()
							IF NOT DOES_LANGUAGE_NEED_TO_USE_SHORT_PROPERTY_NAME()
							AND NOT DOES_PROPERTY_NEED_TO_USE_SHORT_NAME(iProperty)
								ADD_MENU_ITEM_TEXT(iMenuCounter,tl_PropertyName,0,TRUE)
							ELSE
								tlShortPropertyName = tl_PropertyName
								tlShortPropertyName += "S"
								ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,0,TRUE)
							ENDIF
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,TRUE)
							ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(tl_PropertyName), FALSE, TRUE)
						ENDIF
						control.iSlotIDS[iMenuCounter] = i
						iMenuCounter++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0) != 0
			AND GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_3_TYPE) = ENUM_TO_INT(AT_ST_VEHICLE_STORAGE)
			AND NOT g_sMPTunables.bDisable_store_PV
			AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(vehModel,DISPLAY_SLOT_START_ARMOURY_TRUCK)
			AND NOT IS_THIS_MODEL_A_HELI(vehModel)
			AND NOT IS_THIS_MODEL_A_PLANE(vehModel)
				ADD_MENU_ITEM_TEXT(iMenuCounter, GET_SPECIAL_PV_PROPERTY_TEXT_LABEL(SPVP_ARMORY_TRUCK), 0, TRUE)
				control.iSlotIDS[iMenuCounter] = GET_FAKE_PROPERTY_SLOT_FOR_SPECIAL_PV_PROPERTY(SPVP_ARMORY_TRUCK)
				iMenuCounter++
			ENDIF
			
			IF IS_HACKER_TRUCK_PURCHASED()
			AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(vehModel,DISPLAY_SLOT_TEROBYTE_OPP2)
				ADD_MENU_ITEM_TEXT(iMenuCounter, GET_SPECIAL_PV_PROPERTY_TEXT_LABEL(SPVP_CLUB_TRUCK), 0, TRUE)
				control.iSlotIDS[iMenuCounter] = GET_FAKE_PROPERTY_SLOT_FOR_SPECIAL_PV_PROPERTY(SPVP_CLUB_TRUCK)
				iMenuCounter++
			ENDIF
			#IF FEATURE_HEIST_ISLAND
			IF IS_SUBMARINE_OWNED_BY_CHARACTER()
			AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(vehModel,DISPLAY_SLOT_SUB_AQUA_CAR)
				ADD_MENU_ITEM_TEXT(iMenuCounter, GET_SPECIAL_PV_PROPERTY_TEXT_LABEL(SPVP_SUBMARINE), 0, TRUE)
				control.iSlotIDS[iMenuCounter] = GET_FAKE_PROPERTY_SLOT_FOR_SPECIAL_PV_PROPERTY(SPVP_SUBMARINE)
				iMenuCounter++
			ENDIF
			#ENDIF
			
//			#IF FEATURE_SMUGGLER
//			IF DOES_LOCAL_PLAYER_OWN_A_HANGER() 
//			AND (IS_THIS_MODEL_A_HELI(vehModel) OR IS_THIS_MODEL_A_PLANE(vehModel))
//				ADD_MENU_ITEM_TEXT(iMenuCounter, "PM_SPAWN_HNGR")
//				control.iSlotIDS[iMenuCounter] = GET_FAKE_PROPERTY_SLOT_FOR_SPECIAL_PV_PROPERTY(SPVP_ARMORY_TRUCK)
//				iMenuCounter++
//			ENDIF
//			#ENDIF
			
//			#IF FEATURE_GUNRUNNING
//			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_DBASE_OWNED) != 0
//			AND NOT g_sMPTunables.bDisable_store_PV
//			AND GET_FDS_VEHICLE_INDEX(vehModel) != -1
//				ADD_MENU_ITEM_TEXT(iMenuCounter, GET_SPECIAL_PV_PROPERTY_TEXT_LABEL(SPVP_DEFUNCT_BASE), 0, TRUE)
//				control.iSlotIDS[iMenuCounter] = GET_FAKE_PROPERTY_SLOT_FOR_SPECIAL_PV_PROPERTY(SPVP_DEFUNCT_BASE)
//				iMenuCounter++
//			ENDIF
//			#ENDIF
			
			control.iMaxVertSel	= iMenuCounter
			SET_CURRENT_MENU_ITEM(control.icurrentVertSel)
		BREAK
		
		CASE 1
			IF IS_THIS_MODEL_A_HELI(vehModel) OR IS_THIS_MODEL_A_PLANE(vehModel)
				SET_MENU_TITLE("DEL_VEH_SEL3ac")
			ELSE
				SET_MENU_TITLE("DEL_VEH_SEL3")
			ENDIF
			control.iEmptyPositionBS = 0
			iGarageStartIndex = GET_PROPERTY_SLOT_DISPLAY_START_INDEX(control.iFirstLevelSelectedSlotID)
			iBikeStartIndex = MP_PROP_BIKE_INDEX_1+(GET_PROPERTY_SLOT_DISPLAY_START_INDEX(control.iFirstLevelSelectedSlotID))
			IF control.iFirstLevelSelectedSlotID >= MAX_OWNED_PROPERTIES
				SPVProperty = GET_SPECIAL_PV_PROPERTY_FROM_FAKE_PROPERTY_SLOT(control.iFirstLevelSelectedSlotID)
				REPEAT GET_MAX_NUMBER_OF_VEHICLES_IN_SPECIAL_PV_PROPERTY(SPVProperty) i
					iDisplaySlot = i+iGarageStartIndex
					IF NOT bCycles
						IF MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
							bSkip = TRUE
						ENDIF
					ELSE
						IF NOT MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
							bSkip = TRUE
						ENDIF
					ENDIF
					IF NOT IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(vehModel,iDisplaySlot)
						bSkip = TRUE
					ENDIF
					PRINTLN("SETUP_REPLACE_MP_SV_MENU:- A bCycle: ",bCycles," Checking index: ",iDisplaySlot," bSkip = ",GET_STRING_FROM_BOOL(bSkip))
					IF NOT bSkip
						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSaveIndex)
						IF iSaveIndex >= 0
						AND g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
						AND IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel)

							iComponents = 1
							textLabel = ""
							IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
								iComponents++
							ENDIF
							IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
							AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
								iComponents++
							ENDIF
							tl23VehName = GET_ARENA_VEHICLE_NAME(DEFAULT, iDisplaySlot)
//							IF NOT IS_STRING_NULL_OR_EMPTY(tl23VehName)
//								iComponents++
//							ENDIF
							
							IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
								PRINTLN("destroyed")
								IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
								AND NOT bCycles
									textLabel = "MP_MAN_VEH_S0"
									textLabel += iComponents
									ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
									IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
										ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
									ELSE
										ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
									ENDIF
									IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
										ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
									ENDIF
									IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
									AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
										ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
									ENDIF
									control.iSlotIDS[iMenuCounter] = iDisplaySlot
									iMenuCounter++
									PRINTLN("insured")
								ELSE
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_MAN_VEH_S")
									control.iSlotIDS[iMenuCounter] = iDisplaySlot
									SET_BIT(control.iEmptyPositionBS,iMenuCounter)
									iMenuCounter++
									PRINTLN("empty-1")
								ENDIF
							ELSE
								PRINTLN("not destroyed")
								IF bCycles
									IF IS_VEHICLE_A_CYCLE(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel) 
										IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
											textLabel = "MP_MAN_VEH_S1"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ELSE
											textLabel = "PIL_MP_VEH_0"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ENDIF
										control.iSlotIDS[iMenuCounter] = iDisplaySlot
										iMenuCounter++
										PRINTLN("cycle")
									ENDIF
								ELSE	
									IF NOT IS_VEHICLE_A_CYCLE(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel) 
										//PRINTLN("not a cycle")
										IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
											textLabel = "MP_MAN_VEH_S1"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ELSE
											textLabel = "PIL_MP_VEH_0"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)	
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ENDIF
										control.iSlotIDS[iMenuCounter] = iDisplaySlot
										iMenuCounter++
										PRINTLN("not cycle")
									//ELSE
									//	PRINTLN("a cycle")
									ENDIF
								ENDIF
							ENDIF
						//ELSE
						//	PRINTLN("Model not in CD image")
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_MAN_VEH_S")
							control.iSlotIDS[iMenuCounter] = iDisplaySlot
							SET_BIT(control.iEmptyPositionBS,iMenuCounter)
							iMenuCounter++
							PRINTLN("empty-2")
						ENDIF
						//ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				iProperty = GET_OWNED_PROPERTY(control.iFirstLevelSelectedSlotID)
				REPEAT GET_MAX_NUMBER_OF_VEHICLES_IN_PROPERTY(control.iFirstLevelSelectedSlotID) i
					iDisplaySlot = i+iGarageStartIndex
					bSkip = FALSE
					IF iProperty <= 0
						bSkip= TRUE
					ELSE
						IF mpProperties[iProperty].iGarageSize = PROP_GARAGE_SIZE_2
							IF iDisplaySlot >= iGarageStartIndex + 2
							AND iDisplaySlot != iBikeStartIndex
								bSkip = TRUE
							ENDIF
						ELIF mpProperties[iProperty].iGarageSize = PROP_GARAGE_SIZE_6
							IF iDisplaySlot >= iGarageStartIndex + 6
							AND iDisplaySlot != iBikeStartIndex AND iDisplaySlot != iBikeStartIndex +1
								bSkip = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF NOT bCycles
						IF iDisplaySlot >= iBikeStartIndex
						AND MP_SAVE_VEHICLE_IS_DISPLAY_SLOT_A_CYCLE(iDisplaySlot)
							bSkip = TRUE
						ENDIF
					ELSE
						IF iDisplaySlot < iBikeStartIndex
							bSkip = TRUE
						ENDIF
					ENDIF
					IF NOT DOES_DISPLAY_SLOT_EXIST_IN_PROPERTY(iDisplaySlot,iProperty,control.iFirstLevelSelectedSlotID)
						bSkip = TRUE
					ENDIF
					IF NOT IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(vehModel,iDisplaySlot)
						bSkip = TRUE
					ENDIF
					
					PRINTLN("SETUP_REPLACE_MP_SV_MENU:- B bCycle: ",bCycles," Checking index: ",iDisplaySlot," bSkip = ",GET_STRING_FROM_BOOL(bSkip))
					IF NOT bSkip
						MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iSaveIndex)
						IF iSaveIndex >= 0
						AND g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
						AND IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel)
						//AND NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
						//AND NOT IS_BIT_SET(g_MpSavedVehicles[i].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
							PRINTLN("model= ", ENUM_TO_INT(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel), "save slot: ", iSaveIndex)
							iComponents = 1
							textLabel = ""
							IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
								iComponents++
							ENDIF
							IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
							AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
								iComponents++
							ENDIF
							tl23VehName = GET_ARENA_VEHICLE_NAME(DEFAULT, iDisplaySlot) 
//							IF NOT IS_STRING_NULL_OR_EMPTY(tl23VehName)
//								iComponents++
//							ENDIF
							
							IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
								PRINTLN("destroyed")
								IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
								AND NOT bCycles
									textLabel = "MP_MAN_VEH_S0"
									textLabel += iComponents
									ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
									IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
										ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
									ELSE
										ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
									ENDIF
									IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
										ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
									ENDIF
									IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
									AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
										ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
									ENDIF
									control.iSlotIDS[iMenuCounter] = iDisplaySlot
									iMenuCounter++
									PRINTLN("insured")
								ELSE
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_MAN_VEH_S")
									control.iSlotIDS[iMenuCounter] = iDisplaySlot
									SET_BIT(control.iEmptyPositionBS,iMenuCounter)
									iMenuCounter++
									PRINTLN("empty-1")
								ENDIF
							ELSE
								PRINTLN("not destroyed")
								IF bCycles
									IF IS_VEHICLE_A_CYCLE(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel) 
										IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
											textLabel = "MP_MAN_VEH_S1"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ELSE
											textLabel = "PIL_MP_VEH_0"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ENDIF
										control.iSlotIDS[iMenuCounter] = iDisplaySlot
										iMenuCounter++
										PRINTLN("cycle")
									ENDIF
								ELSE	
									IF NOT IS_VEHICLE_A_CYCLE(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel) 
										//PRINTLN("not a cycle")
										IF IS_BIT_SET(g_MpSavedVehicles[iSaveIndex].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
											textLabel = "MP_MAN_VEH_S1"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)		
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ELSE
											textLabel = "PIL_MP_VEH_0"
											textLabel += iComponents
											ADD_MENU_ITEM_TEXT(iMenuCounter,textLabel,iComponents)
											IF IS_STRING_NULL_OR_EMPTY(tl23VehName)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.eModel))
											ELSE
												ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tl23VehName,DEFAULT,DEFAULT,TRUE)
											ENDIF
											IF IS_VEHICLE_SETUP_WITH_LIVERY(g_MpSavedVehicles[iSaveIndex].VehicleSetupMP.VehicleSetup)
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_LIVERY")
											ENDIF
											IF IS_VEHICLE_TOPLESS(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iFlags, g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.vehicleSetup.iModIndex[MOD_ROOF])
											AND SHOULD_VEH_MODEL_DISPLAY_TOPLESS_LABEL(g_MpSavedVehicles[iSaveIndex].vehicleSetupMP.VehicleSetup.eModel)	
												ADD_MENU_ITEM_TEXT_COMPONENT_STRING("VEH_TOPL")
											ENDIF
										ENDIF
										control.iSlotIDS[iMenuCounter] = iDisplaySlot
										iMenuCounter++
										PRINTLN("not cycle")
									//ELSE
									//	PRINTLN("a cycle")
									ENDIF
								ENDIF
							ENDIF
						//ELSE
						//	PRINTLN("Model not in CD image")
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_MAN_VEH_S")
							control.iSlotIDS[iMenuCounter] = iDisplaySlot
							SET_BIT(control.iEmptyPositionBS,iMenuCounter)
							iMenuCounter++
							PRINTLN("empty-2")
						ENDIF
						//ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			control.iMaxVertSel	= iMenuCounter
			IF control.iCurrentVertSelSecLevel >= control.iMaxVertSel
				control.iCurrentVertSelSecLevel = 0
			ENDIF
			SET_CURRENT_MENU_ITEM(control.iCurrentVertSelSecLevel)
		BREAK
	ENDSWITCH

	//Once
	REMOVE_MENU_HELP_KEYS()
	ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_SELECT")
	ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_BACK")
	//ADD_MENU_HELP_KEY_CLICKABLE(INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
	
	PRINTLN("SETUP_SELL_CURRENT_CARS_MENU: setup menu: level: ",control.iLevel)
ENDPROC


FUNC BOOL HANDLE_REPLACE_MP_SV_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control,INT &iResultSlot, INT &iDisplaySlot, MODEL_NAMES vehModel, BOOL bShopSetup=FALSE, INT iMenuType = -1, BOOL bShowNewVehicleNameInDescription = FALSE)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PLAYER_IN_CORONA()
	OR g_bMissionEnding
	OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
		iResultSlot = -1
		NET_PRINT("HANDLE_REPLACE_MP_SV_MENU - NETWORK_IS_GAME_IN_PROGRESS = FALSE - returning slot -1") NET_NL()
		CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control, iMenuType)
		RETURN(TRUE)
	ENDIF

	BOOL bCursorSelect = FALSE
	//BOOL bCycles = IS_VEHICLE_A_CYCLE(vehModel)
	BOOL bSkipDescriptionUpdate
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_UP)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_DOWN)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_FORWARD)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_BACKWARD)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK2)
		
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		
		IF NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_WARNING_MESSAGE_ACTIVE()
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		ENDIF 

		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		SET_MOUSE_CURSOR_THIS_FRAME()		
	ENDIF
	
	SET_BIT(control.iBS,REP_VEH_PROP_BS_USING)
	IF LOAD_MENU_ASSETS(DEFAULT, iMenuType)
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_SETUP)
				SETUP_REPLACE_MP_SV_MENU(control,vehModel,bShopSetup)
				SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
				HANDLE_REPLACE_MP_SV_MENU_DESCRIPTION(control, bShopSetup, bSkipDescriptionUpdate, vehModel, bShowNewVehicleNameInDescription)
				DRAW_MENU(TRUE, iMenuType)
			ELSE
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				AND NOT g_sTransitionSessionData.sEndReserve.bTranCleanUpEndReservingSlot

					// Mouse selection
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						
						IF IS_MENU_CURSOR_ACCEPT_PRESSED()
							
							IF control.iLevel = 0
								
								IF g_iMenuCursorItem != control.iCurrentVertSel
									control.iCurrentVertSel = g_iMenuCursorItem
									PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
									CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								ELSE
									bCursorSelect = TRUE
								ENDIF
								
							ELSE
								
								IF g_iMenuCursorItem != control.iCurrentVertSelSecLevel
									control.iCurrentVertSelSecLevel = g_iMenuCursorItem
									PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
									CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								ELSE
									bCursorSelect = TRUE
								ENDIF

							ENDIF
						
						ENDIF

					ENDIF

					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
						OR IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE()
						OR bCursorSelect
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
							IF control.iLevel = 0
								control.iFirstLevelSelectedSlotID = 0
								IF control.iCurrentVertSel >= 0
								AND control.iSlotIDS[control.iCurrentVertSel] >= 0
									iDisplaySlot = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehModel,GET_OWNED_PROPERTY(control.iSlotIDS[control.iCurrentVertSel]))
									IF iDisplaySlot >= 0
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iResultSlot)
										IF iResultSlot < 0
											iResultSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehModel)
										ENDIF
									ENDIF
									IF iResultSlot >= 0
									AND iDisplaySlot >= 0
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
										CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control, iMenuType)
										#IF IS_DEBUG_BUILD
										IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iResultSlot].vehicleSetupMP.VehicleSetup.eModel)
											PRINTLN("HANDLE_REPLACE_MP_SV_MENU 1 player just replaced a stored vehicle: Slot #",iResultSlot, " Vehicle Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iResultSlot].vehicleSetupMP.VehicleSetup.eModel))
										ELSE
											PRINTLN("HANDLE_REPLACE_MP_SV_MENU 2 player just replaced a stored vehicle: Slot #",iResultSlot)
										ENDIF
										#ENDIF
										RETURN TRUE
									ENDIF
								ELSE
									IF control.iCurrentVertSel < 0
										PRINTLN("HANDLE_REPLACE_MP_SV_MENU: control.iCurrentVertSel <0 ??")
									ENDIF
								ENDIF
								IF control.iCurrentVertSel < 0
									control.iFirstLevelSelectedSlotID = 0
								ELSE
									control.iFirstLevelSelectedSlotID = control.iSlotIDS[control.iCurrentVertSel]
								ENDIF
								control.iLevel = 1
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								bSkipDescriptionUpdate =TRUE
							ELSE
								IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								AND NOT IS_BIT_SET(control.iEmptyPositionBS,control.iCurrentVertSelSecLevel)
									REMOVE_MENU_HELP_KEYS()
									ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_YES")
									ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_NO")
									SET_BIT(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								ELSE
									IF control.iCurrentVertSelSecLevel >= 0
										iDisplaySlot = control.iSlotIDS[control.iCurrentVertSelSecLevel]
										MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot,iResultSlot)
										PRINTLN("MPSV: MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT display slot #",iDisplaySlot," = saveSlot #",iResultSlot)
										IF iResultSlot < 0
											iResultSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehModel)
										ENDIF
										IF iResultSlot >= 0
										AND iDisplaySlot >= 0
											CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
											CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control, iMenuType)
											#IF IS_DEBUG_BUILD
											IF IS_MODEL_IN_CDIMAGE(g_MpSavedVehicles[iResultSlot].vehicleSetupMP.VehicleSetup.eModel)
												PRINTLN("HANDLE_REPLACE_MP_SV_MENU 3 player just replaced a stored vehicle: Slot #",iResultSlot, " Vehicle Model = ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(g_MpSavedVehicles[iResultSlot].vehicleSetupMP.VehicleSetup.eModel))
											ELSE
												PRINTLN("HANDLE_REPLACE_MP_SV_MENU 4 player just replaced a stored vehicle: Slot #",iResultSlot)
											ENDIF
											#ENDIF
										ELSE
											NET_PRINT("HANDLE_REPLACE_MP_SV_MENU: FAILED TO REPLACE VEHICLE!") NET_NL()
											SCRIPT_ASSERT("HANDLE_REPLACE_MP_SV_MENU: FAILED TO REPLACE VEHICLE! - see conor")
											CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control, iMenuType)
											iResultSlot = -1
											RETURN TRUE
										ENDIF
										RETURN TRUE
									ELSE
										PRINTLN("HANDLE_REPLACE_MP_SV_MENU: control.iCurrentVertSelSecLevel <0 ??")
									ENDIF
								ENDIF
							ENDIF
							SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
						AND NOT bCursorSelect
							CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						IF IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE()
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL)
						OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						OR (IS_MENU_CURSOR_CANCEL_PRESSED() AND (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM))
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
							PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
							IF control.iLevel = 0
							
								HANDLE_REPLACE_MP_SV_MENU_DESCRIPTION(control, bShopSetup, bSkipDescriptionUpdate, vehModel, bShowNewVehicleNameInDescription)
								DRAW_MENU(TRUE, iMenuType)
							
								NET_PRINT("HANDLE_REPLACE_MP_SV_MENU player exited menu") NET_NL()
								CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control, iMenuType, TRUE)
								iResultSlot = -1
								SET_BIT(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
								RETURN TRUE
							ELSE
								IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
									control.iLevel = 0
									bSkipDescriptionUpdate =TRUE
									CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								ELSE
									REMOVE_MENU_HELP_KEYS()
									ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_SELECT")
									ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_BACK")
									//ADD_MENU_HELP_KEY_CLICKABLE(INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
									CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								ENDIF
							ENDIF
							SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						AND NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_CANCEL)
						AND NOT IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE()
							CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ENDIF
					
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_UP)
								IF control.iLevel = 0
									control.iCurrentVertSel--
								ELSE
									control.iCurrentVertSelSecLevel--
								ENDIF
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								RESET_NET_TIMER(control.menuNavigationDelay)
							ENDIF
						ELSE
							IF REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(control,INPUT_CELLPHONE_UP)
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)	
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CURSOR_SCROLL_DOWN)
								IF control.iLevel = 0
									control.iCurrentVertSel++
								ELSE
									control.iCurrentVertSelSecLevel++
								ENDIF
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								RESET_NET_TIMER(control.menuNavigationDelay)
							ENDIF
						ELSE
							IF REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(control,INPUT_CELLPHONE_DOWN)
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)	
							ENDIF
						ENDIF
					ENDIF
					IF control.iLevel = 0
						IF control.iCurrentVertSel >= control.iMaxVertSel
							control.iCurrentVertSel = 0
						ENDIF
						IF Control.iCurrentVertSel < 0
							control.iCurrentVertSel = control.iMaxVertSel-1
						ENDIF
					ELSE
						IF control.iCurrentVertSelSecLevel >= control.iMaxVertSel
							control.iCurrentVertSelSecLevel = 0
						ENDIF
						IF Control.iCurrentVertSelSecLevel < 0
							control.iCurrentVertSelSecLevel = control.iMaxVertSel-1
						ENDIF
					ENDIF
					//PRINTLN("control.iCurrentVertSel = ",control.iCurrentVertSel)
					HANDLE_REPLACE_MP_SV_MENU_DESCRIPTION(control, bShopSetup, bSkipDescriptionUpdate, vehModel, bShowNewVehicleNameInDescription)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						DRAW_MENU(TRUE, iMenuType)
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CHECK_ONLY_ONE_SUITABLE_PROPERTY(MODEL_NAMES theVehModel, INT &iOnlyProperty)
	iOnlyProperty = -1
	//Check if you only own 1 normal property
	IF GET_OWNED_PROPERTY(0) >= 1
	AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_APT_1)
		IF GET_OWNED_PROPERTY(1) >= 1
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE) >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CLUBHOUSE))
		OR GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) >= 1
		OR IS_GUNRUNNING_TRUCK_PURCHASED()
		OR (DOES_LOCAL_PLAYER_OWN_A_HANGER() AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(theVehModel,DISPLAY_SLOT_START_HANGAR))
		OR (DOES_LOCAL_PLAYER_OWN_A_DEFUNCT_BASE() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_DEFUNCBASE))
		OR (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB))
		OR (IS_HACKER_TRUCK_PURCHASED() AND theVehModel = OPPRESSOR2)
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(0)
		EXIT
	//check you only own a clubhouse
	ELIF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE) >= 1
	AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CLUBHOUSE)
		IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) >= 1
		OR IS_GUNRUNNING_TRUCK_PURCHASED()
		OR (DOES_LOCAL_PLAYER_OWN_A_HANGER() AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(theVehModel,DISPLAY_SLOT_START_HANGAR))
		OR (DOES_LOCAL_PLAYER_OWN_A_DEFUNCT_BASE() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_DEFUNCBASE))
		OR (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB))
		OR (IS_HACKER_TRUCK_PURCHASED() AND theVehModel = OPPRESSOR2)
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE)
		EXIT
	//check if you only own an office garage
	ELIF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1) >= 1
	AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1)
		IF IS_GUNRUNNING_TRUCK_PURCHASED()
		OR (DOES_LOCAL_PLAYER_OWN_A_HANGER() AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(theVehModel,DISPLAY_SLOT_START_HANGAR))
		OR (DOES_LOCAL_PLAYER_OWN_A_DEFUNCT_BASE() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_DEFUNCBASE))
		OR (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB))
		OR (IS_HACKER_TRUCK_PURCHASED() AND theVehModel = OPPRESSOR2)
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1)
		EXIT
	//check if you only own an armory truck
	ELIF IS_GUNRUNNING_TRUCK_PURCHASED()
	AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(theVehModel,DISPLAY_SLOT_START_ARMOURY_TRUCK)
		IF (DOES_LOCAL_PLAYER_OWN_A_HANGER() AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(theVehModel,DISPLAY_SLOT_START_HANGAR))
		OR (DOES_LOCAL_PLAYER_OWN_A_DEFUNCT_BASE() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_DEFUNCBASE))
		OR (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB))
		OR (IS_HACKER_TRUCK_PURCHASED() AND theVehModel = OPPRESSOR2)
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = PROPERTY_TRUCK_REFERENCE_ONLY
		EXIT
	//check if you only own a hangar
	ELIF DOES_LOCAL_PLAYER_OWN_A_HANGER()
	AND IS_VEHICLE_SUITABLE_FOR_DISPLAY_SLOT(theVehModel,DISPLAY_SLOT_START_HANGAR)
		IF (DOES_LOCAL_PLAYER_OWN_A_DEFUNCT_BASE() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_DEFUNCBASE))
		OR (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB))
		OR (IS_HACKER_TRUCK_PURCHASED() AND theVehModel = OPPRESSOR2)
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_HANGAR)
		EXIT
	//check if you only own a defunc base
	ELIF DOES_LOCAL_PLAYER_OWN_A_DEFUNCT_BASE()
	AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_DEFUNCBASE)
		IF (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		IF IS_HACKER_TRUCK_PURCHASED()
		AND theVehModel = OPPRESSOR2
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_DEFUNCBASE)
		EXIT
	ELIF DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB()
	AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_NIGHTCLUB)
		IF (DOES_LOCAL_PLAYER_OWN_A_NIGHTCLUB() AND GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1) >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		IF IS_HACKER_TRUCK_PURCHASED()
		AND theVehModel = OPPRESSOR2
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_NIGHTCLUB)
		EXIT
	ELIF IS_HACKER_TRUCK_PURCHASED()
	AND theVehModel = OPPRESSOR2
		IF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = PROPERTY_CLUB_TRUCK_REFERENCE_ONLY
		EXIT
	ELIF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		IF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL2)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1)
		EXIT
	ELIF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel,PROPERTY_OWNED_SLOT_CASINO_GARAGE))
		IF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		OR (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		PRINTLN("CHECK_ONLY_ONE_SUITABLE_PROPERTY only casino property")
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CASINO_GARAGE)
		EXIT
	ELIF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_ARCADE_GARAGE) AND HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP())
		IF (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		PRINTLN("CHECK_ONLY_ONE_SUITABLE_PROPERTY only Arcade property")
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_ARCADE_GARAGE)
		EXIT
	ELIF (IS_SUBMARINE_OWNED_BY_CHARACTER() 
	AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		IF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		PRINTLN("CHECK_ONLY_ONE_SUITABLE_PROPERTY only sub property")
		iOnlyProperty = PROPERTY_SUBMARINE_REFERENCE_ONLY
		EXIT
	ELIF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY) AND HAS_LOCAL_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION())
		IF (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
		#IF FEATURE_FIXER
		OR (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		#ENDIF
			EXIT
		ENDIF
		PRINTLN("CHECK_ONLY_ONE_SUITABLE_PROPERTY only Auto Shop property")
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY)
		EXIT
	#IF FEATURE_FIXER
	ELIF (GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)  >= 1 AND IS_VEHICLE_SUITABLE_FOR_PROPERTY_SLOT(theVehModel, PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR))
		IF (IS_SUBMARINE_OWNED_BY_CHARACTER() 
		AND (theVehModel = TOREADOR OR theVehModel = STROMBERG))
			EXIT
		ENDIF
		PRINTLN("CHECK_ONLY_ONE_SUITABLE_PROPERTY only Fixer HQ property")
		iOnlyProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR)
		EXIT
	#ENDIF
	ENDIF
ENDPROC

FUNC BOOL RUN_WIN_VEHICLE(VEHICLE_INDEX theVeh, REPLACE_MP_VEH_OR_PROP_MENU &control, INT &iTransactionResult, INT &iResultSlot, INT &iDisplaySlot, INT &iControlState, BOOL bCarNowInGarage  = TRUE
						, BOOL bNowMostRecentCarUsed = FALSE, BOOL bDisplayExitWarning = TRUE, BOOL bPodiumVehicle = FALSE, SELL_PRIZE_VEHICLE_EARN_TYPE eSellPrizeVehEarnType = SPVET_CASINO_PODIUM)
	INT iOnlySuitableProperty = -1
	INT iSlotToUse
	BOOL bGiveImpoundWarning = FALSE
	FE_WARNING_FLAGS WarningBS
	MODEL_NAMES vehModel = GET_ENTITY_MODEL(theVeh)
	
	SWITCH iControlState
		CASE 0
			IF DOES_PLAYER_OWN_VEHICLE_STORAGE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(theVeh))
				CHECK_ONLY_ONE_SUITABLE_PROPERTY(vehModel,iOnlySuitableProperty)
				PRINTLN("RUN_WIN_VEHICLE: iOnlySuitableProperty: ", iOnlySuitableProperty)
				IF iOnlySuitableProperty != -1
					iResultSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehModel,iOnlySuitableProperty)
					iDisplaySlot = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,FALSE,vehModel,iOnlySuitableProperty)
					IF iResultSlot > -1
					AND iDisplaySlot > -1
						iControlState = 1
						PRINTLN("RUN_WIN_VEHICLE: iResultSlot: ", iResultSlot , " iDisplaySlot: ", iDisplaySlot)
					ENDIF	
				ELSE
					iSlotToUse = -1
				ENDIF
				
				IF iSlotToUse = -1
				OR iDisplaySlot = -1
					IF !IS_BIT_SET(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
						IF HANDLE_REPLACE_MP_SV_MENU(control, iResultSlot, iDisplaySlot, vehModel, DEFAULT, DEFAULT, TRUE)
							IF iResultSlot > -1
							AND iDisplaySlot > -1
								iControlState = 1
								PRINTLN("RUN_WIN_VEHICLE: iControlState: ", iControlState)
							ELSE
								PRINTLN("RUN_WIN_VEHICLE: HANDLE_REPLACE_MP_SV_MENU iResultSlot: ", iResultSlot , " iDisplaySlot: ", iDisplaySlot)
							ENDIF
						ENDIF
					ELSE
						IF bDisplayExitWarning
							WarningBS = FE_WARNING_YESNO
							SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "LW_LOSE_VEH", WarningBS)
							IF IS_WARNING_MESSAGE_ACTIVE()
							AND NOT IS_CUSTOM_MENU_ON_SCREEN()
								IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
									CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
									PRINTLN("RUN_WIN_VEHICLE player will lose the prize")
									iControlState = 3
									RETURN TRUE
								ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
								OR IS_MENU_CURSOR_CANCEL_PRESSED()
									CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
									PRINTLN("RUN_WIN_VEHICLE player would like to replace the vehicle")
								ENDIF
							ENDIF	
						ELSE
							CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_EXIT_MENU)
							PRINTLN("RUN_WIN_VEHICLE player wants to leave the vehicle")
							iControlState = 3
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF !IS_SVM_VEHICLE_MODEL(GET_ENTITY_MODEL(theVeh))
				AND !IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(theVeh))
					IF IS_PERSONAL_VEHICLE_DRIVEABLE_IN_FREEMODE()
					
						INT iVehSlot
						
						REPEAT MAX_MP_SAVED_VEHICLES iVehSlot
							IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(iVehSlot ) 	
								bGiveImpoundWarning = TRUE
								iVehSlot = MAX_MP_SAVED_VEHICLES + 1 // bail
							ENDIF
						ENDREPEAT

						WarningBS = FE_WARNING_OKCANCEL
						IF bGiveImpoundWarning
							SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "CASINO_PRIZE_V1", WarningBS)
						ELSE	
							SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "CASINO_PRIZE_V2", WarningBS)
						ENDIF
						
						IF IS_WARNING_MESSAGE_ACTIVE()
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
							OR IS_MENU_CURSOR_CANCEL_PRESSED()
								iControlState = 3
								PRINTLN("RUN_WIN_VEHICLE - player doesn't want to replace the impounded/active personal vehicle so they will lose the prize")
								RETURN TRUE
							ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
								iResultSlot   	= 0
								iDisplaySlot	= 0
								iControlState 	= 1
								PRINTLN("RUN_WIN_VEHICLE - player wants to replace the impounded/active personal vehicle with prize vehicle")
							ENDIF
						ENDIF	
						
					ELSE
						IF IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS, MP_SAVED_VEHICLE_INSURED)
							WarningBS = FE_WARNING_OKCANCEL
							SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "CASINO_PRIZE_V2", WarningBS)
							
							IF IS_WARNING_MESSAGE_ACTIVE()
								IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
								OR IS_MENU_CURSOR_CANCEL_PRESSED()
									iControlState = 3
									PRINTLN("RUN_WIN_VEHICLE - player doesn't want to replace the insured vehicle so they will lose the prize")
									RETURN TRUE
								ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
									iResultSlot   	= 0
									iDisplaySlot	= 0
									iControlState 	= 1
									PRINTLN("RUN_WIN_VEHICLE - player wants to replace the insured vehicle with prize vehicle")
								ENDIF
							ENDIF	
						ELSE
							iResultSlot   	= 0
							iDisplaySlot	= 0
							iControlState 	= 1
							PRINTLN("RUN_WIN_VEHICLE - player don't have garage and don't have any PV so use slot 0")
						ENDIF
					ENDIF	
				ELSE
					SCRIPT_ASSERT("RUN_WIN_VEHICLE you must have suitable property for this vehicle model")
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_ENTITY_ALIVE(theVeh)
				IF USE_SERVER_TRANSACTIONS()
					IF PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE(theVeh, iResultSlot, iTransactionResult, TRUE, TRUE)
						IF iTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS
							PRINTLN("RUN_WIN_VEHICLE: - SUCCESS!")
							PRINTLN("RUN_WIN_VEHICLE iResultSlot: ",iResultSlot)
							IF iResultSlot >= 0
								MPSV_SET_DISPLAY_SLOT(iDisplaySlot,iResultSlot)
							ENDIF
							
							IF !DOES_PLAYER_OWN_VEHICLE_STORAGE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(theVeh))
								CLEANUP_MP_SAVED_VEHICLE(TRUE)
								bCarNowInGarage = FALSE
								bNowMostRecentCarUsed = TRUE
								PRINTLN("RUN_WIN_VEHICLE: player don't own a garage set this as active personal vehicle")
							ENDIF
							IF bPodiumVehicle
								IF (eSellPrizeVehEarnType = SPVET_CASINO_PODIUM)
									PRINTLN("RUN_WIN_VEHICLE: vehicle reward MP_SAVED_VEHICLE_CASINO_PODIUM_VEH TRUE")
								ELIF (eSellPrizeVehEarnType = SPVET_CAR_MEET_PRIZE)
									PRINTLN("RUN_WIN_VEHICLE: vehicle reward MP_SAVED_VEHICLE_CAR_MEET_PRIZE_VEH TRUE")
								ENDIF
							ENDIF	
							MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(theVeh, iResultSlot, TRUE, bCarNowInGarage, bNowMostRecentCarUsed, TRUE,TRUE,bPodiumVehicle,TRUE, DEFAULT, eSellPrizeVehEarnType)
							INSURE_MP_SAVE_VEHICLE_AND_SET_FLAG(theVeh, iResultSlot)
							
							PRINTLN("RUN_WIN_VEHICLE: vehicle reward for pc done!")
							REQUEST_SAVE(SSR_REASON_CASINO_VEH_REWARD, STAT_SAVETYPE_IMMEDIATE_FLUSH)
							RETURN TRUE
						ELIF iTransactionResult = GARAGE_VEHICLE_TRANSACTION_STATE_FAILED
							PRINTLN("RUN_WIN_VEHICLE: - FAILED! Aborting")
						ENDIF
						
					ELSE
						PRINTLN("RUN_WIN_VEHICLE :waiting for PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE")
						RETURN FALSE
					ENDIF
				ELSE
					IF iResultSlot >= 0
						MPSV_SET_DISPLAY_SLOT(iDisplaySlot,iResultSlot)
					ENDIF
					
					IF !DOES_PLAYER_OWN_VEHICLE_STORAGE_FOR_VEHICLE_MODEL(GET_ENTITY_MODEL(theVeh))
						CLEANUP_MP_SAVED_VEHICLE(TRUE)
						bCarNowInGarage = FALSE
						bNowMostRecentCarUsed = TRUE
						PRINTLN("RUN_WIN_VEHICLE: player don't own a garage set this as active personal vehicle")
					ENDIF
					IF bPodiumVehicle
						IF (eSellPrizeVehEarnType = SPVET_CASINO_PODIUM)
							PRINTLN("RUN_WIN_VEHICLE: vehicle reward MP_SAVED_VEHICLE_CASINO_PODIUM_VEH TRUE")
						ELIF (eSellPrizeVehEarnType = SPVET_CAR_MEET_PRIZE)
							PRINTLN("RUN_WIN_VEHICLE: vehicle reward MP_SAVED_VEHICLE_CAR_MEET_PRIZE_VEH TRUE")
						ENDIF
					ENDIF
					MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(theVeh,iResultSlot, TRUE, bCarNowInGarage, bNowMostRecentCarUsed, TRUE,TRUE,bPodiumVehicle,TRUE, DEFAULT, eSellPrizeVehEarnType)
					INSURE_MP_SAVE_VEHICLE_AND_SET_FLAG(theVeh, iResultSlot)
					
					REQUEST_SAVE(SSR_REASON_CASINO_VEH_REWARD, STAT_SAVETYPE_IMMEDIATE_FLUSH)
					PRINTLN("RUN_WIN_VEHICLE: vehicle reward for console done!")
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[RUN_WIN_VEHICLE] - vehicle doesn't exist")
			ENDIF	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_APARTMENT_OR_GARAGE(INT iPropertyID)
	IF IS_PROPERTY_OFFICE(iPropertyID)
		RETURN FALSE
	ELIF IS_PROPERTY_CLUBHOUSE(iPropertyID)
		RETURN FALSE
	ELIF IS_PROPERTY_OFFICE_GARAGE(iPropertyID)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_CATALOG_ITEM_AN_APARTMENT_OR_GARAGE(INT iItemID)
	IF IS_CATALOG_ITEM_AN_OFFICE_PROPERTY_ITEM(iItemID)
		RETURN FALSE
	ELIF IS_CATALOG_ITEM_A_CLUBHOUSE_PROPERTY_ITEM(iItemID)
		RETURN FALSE
	ELIF IS_CATALOG_ITEM_AN_OFFICE_GARAGE_PROPERTY_ITEM(iItemID)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


PROC REPLACE_PROPERTY_WARNING_MESSAGE_WEB(INT iValue, INT iPropertyToBuy, BOOL bSmallerGarage)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF
	
	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
	IF bSmallerGarage
		IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyToBuy)
		OR IS_PROPERTY_CLUBHOUSE(iPropertyToBuy)
		OR IS_PROPERTY_APARTMENT_OR_GARAGE(iPropertyToBuy)
			pBodyTextLabel = ("BRDISPROP2B1")		//"You already own a property, purchasing this one will trade in your old one.~n~
													// You will also lose access to some of your stored vehicles as the Garage is smaller.  The trade in will ~a~ you $~1~.""
		ELSE
			IF iValue < 0  //3rd
				pBodyTextLabel = ("BRDISPROP2M2")	//"You already own a property, purchasing this one will trade in your old one.~n~
													// You will also lose access to some of your stored vehicles as the Garage is smaller.  The trade in could make you up to $~1~.
			ELSE
				pBodyTextLabel = ("BRDISPROP2S2")	//"You already own a property, purchasing this one will trade in your old one.~n~
													// You will also lose access to some of your stored vehicles as the Garage is smaller.  The trade in could save you up to $~1~.
			ENDIF
			pFirstSubStringTextLabel = NULL
		ENDIF
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ELSE
		IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyToBuy)
		OR IS_PROPERTY_CLUBHOUSE(iPropertyToBuy)
		OR IS_PROPERTY_APARTMENT_OR_GARAGE(iPropertyToBuy)
			pBodyTextLabel = ("BRDISPROPB1")		//"You already own a property, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
		ELSE
			IF iValue < 0  //3rd
				pBodyTextLabel = ("BRDISPROPM2")	//"You already own a property, purchasing this one will trade in your old one.  The trade in could make you up to $~1~.""
			ELSE
				pBodyTextLabel = ("BRDISPROPS2")	//"You already own a property, purchasing this one will trade in your old one.  The trade in could save you up to $~1~.""
			ENDIF
			pFirstSubStringTextLabel = NULL
		ENDIF
		pBodySubTextLabel = ("BRSHETPROSUB1")		//"Do you wish to continue with this purchase?
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

PROC REPLACE_FACTORY_WARNING_MESSAGE_WEB(INT iValue, FACTORY_ID eFactoryToBuy)
	UNUSED_PARAMETER(eFactoryToBuy)

	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF

	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
//	IF NOT SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryToBuy)
		pBodyTextLabel = ("BRDISBUNCB1")			//"You already own a bunker, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
//	ELSE
//		IF iValue < 0  //3rd
//			pBodyTextLabel = ("BRDISBUNCM2")		//"You already own a Bunker, purchasing this one will trade in your old one. The trade in could make you up to $~1~.""
//		ELSE
//			pBodyTextLabel = ("BRDISBUNCS2")		//"You already own a Bunker, purchasing this one will trade in your old one. The trade in could save you up to $~1~.""
//		ENDIF
//		pFirstSubStringTextLabel = NULL
//	ENDIF
	pBodySubTextLabel = ("BRSHETPROSUB1")			//"Do you wish to continue with this purchase?
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

PROC REPLACE_HANGAR_WARNING_MESSAGE_WEB(INT iValue)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF

	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
//	IF NOT SHOULD_HANGAR_BE_FREE_FOR_PLAYER(iPropertyToBuy)
		pBodyTextLabel = ("BRDISHANGB1")			//"You already own a hangar, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
//	ELSE
//		IF iValue < 0  //3rd
//			pBodyTextLabel = ("BRDISHANGM2")		//"You already own a hangar, purchasing this one will trade in your old one. The trade in could make you up to $~1~.""
//		ELSE
//			pBodyTextLabel = ("BRDISHANGS2")		//"You already own a hangar, purchasing this one will trade in your old one. The trade in could save you up to $~1~.""
//		ENDIF
//		pFirstSubStringTextLabel = NULL
//	ENDIF
	pBodySubTextLabel = ("BRSHETPROSUB1")			//"Do you wish to continue with this purchase?
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

PROC REPLACE_DEFUNCT_BASE_WARNING_MESSAGE_WEB(INT iValue)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF

	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
//	IF NOT SHOULD_DEFUNCT_BASE_BE_FREE_FOR_PLAYER(iPropertyToBuy)
		pBodyTextLabel = ("BRDISDBASB1")			//"You already own a defunct base, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
//	ELSE
//		IF iValue < 0  //3rd
//			pBodyTextLabel = ("BRDISDBASM2")		//"You already own a defunct base, purchasing this one will trade in your old one.  The trade in could make you up to $~1~.""
//		ELSE
//			pBodyTextLabel = ("BRDISDBASS2")		//"You already own a defunct base, purchasing this one will trade in your old one.  The trade in could save you up to $~1~.""
//		ENDIF
//		pFirstSubStringTextLabel = NULL
//	ENDIF
	pBodySubTextLabel = ("BRSHETPROSUB1")			//"Do you wish to continue with this purchase?
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

PROC REPLACE_NIGHTCLUB_WARNING_MESSAGE_WEB(INT iValue)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	INT NumberToInsert = -1
	STRING pFirstSubStringTextLabel = NULL
	
	NumberToInsert = iValue
	IF NumberToInsert < 0
		NumberToInsert *= -1			
	ENDIF
	IF iValue < 0  //3rd
		pFirstSubStringTextLabel = ("BRSHETMAK")	//make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	//save
	ENDIF

	pHeaderTextLabel = ("BRSCRWTEX")				//"Alert"
//	IF NOT SHOULD_NIGHTCLUB_BE_FREE_FOR_PLAYER(iPropertyToBuy)
		pBodyTextLabel = ("BRDISNCLB1")			//"You already own a nightclub, purchasing this one will trade in your old one.  The trade in will ~a~ you $~1~.""
//	ELSE
//		IF iValue < 0  //3rd
//			pBodyTextLabel = ("BRDISNCLSM2")		//"You already own a nightclub, purchasing this one will trade in your old one.  The trade in could make you up to $~1~.""
//		ELSE
//			pBodyTextLabel = ("BRDISNCLSS2")		//"You already own a nightclub, purchasing this one will trade in your old one.  The trade in could save you up to $~1~.""
//		ENDIF
//		pFirstSubStringTextLabel = NULL
//	ENDIF
	pBodySubTextLabel = ("BRSHETPROSUB1")			//"Do you wish to continue with this purchase?
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

PROC REPLACE_ARCADE_WARNING_MESSAGE_WEB(INT iValue)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	STRING pFirstSubStringTextLabel = NULL
	
	INT NumberToInsert = -1
	
	NumberToInsert = iValue
	
	IF NumberToInsert < 0
		NumberToInsert *= -1
	ENDIF
	
	IF iValue < 0
		pFirstSubStringTextLabel = ("BRSHETMAK")
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")
	ENDIF
	
	pHeaderTextLabel = ("BRSCRWTEX")
	pBodyTextLabel = ("BRDISARC1")
	pBodySubTextLabel = ("BRSHETPROSUB1")
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

PROC REPLACE_AUTO_SHOP_WARNING_MESSAGE_WEB(INT iValue)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	STRING pFirstSubStringTextLabel = NULL
	
	INT NumberToInsert = -1
	NumberToInsert = iValue
	
	IF NumberToInsert < 0
		NumberToInsert *= -1
	ENDIF
	
	IF iValue < 0
		pFirstSubStringTextLabel = ("BRSHETMAK")	// Make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	// Save
	ENDIF
	
	pHeaderTextLabel = ("BRSCRWTEX")		// "Alert"
	pBodyTextLabel = ("BRDISAUT1")			// "You already own an Auto Shop, purchasing this one will trade in your old one. The trade in will ~a~ you $~1~."
	pBodySubTextLabel = ("BRSHETPROSUB1")	// "Do you wish to continue with this purchase?"
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC

#IF FEATURE_FIXER
PROC REPLACE_FIXER_HQ_WARNING_MESSAGE_WEB(INT iValue)
	STRING pHeaderTextLabel
	STRING pBodyTextLabel
	STRING pBodySubTextLabel = NULL
	STRING pFirstSubStringTextLabel = NULL
	
	INT NumberToInsert = -1
	NumberToInsert = iValue
	
	IF NumberToInsert < 0
		NumberToInsert *= -1
	ENDIF
	
	IF iValue < 0
		pFirstSubStringTextLabel = ("BRSHETMAK")	// Make
	ELSE
		pFirstSubStringTextLabel = ("BRSHETPRSA")	// Save
	ENDIF
	
	pHeaderTextLabel = ("BRSCRWTEX")		// "Alert"
	pBodyTextLabel = ("BRDISFIX1")			// "You already own an Agency, purchasing this one will trade in your old one. The trade in will ~a~ you $~1~."
	pBodySubTextLabel = ("BRSHETPROSUB1")	// "Do you wish to continue with this purchase?"
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pFirstSubStringTextLabel)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT,
				pFirstSubStringTextLabel)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(pHeaderTextLabel,
				pBodyTextLabel,
				FE_WARNING_OKCANCEL,
				pBodySubTextLabel,
				TRUE,
				NumberToInsert,
				WARNING_MESSAGE_DEFAULT)
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(INT iPropertySlot)
	SWITCH iPropertySlot
		CASE PROPERTY_OWNED_SLOT_OFFICE_0
		CASE PROPERTY_OWNED_SLOT_CLUBHOUSE
		CASE PROPERTY_OWNED_SLOT_IE_WAREHOUSE
		CASE PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1
		CASE PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2
		CASE PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3
		CASE PROPERTY_OWNED_SLOT_HANGAR
		CASE PROPERTY_OWNED_SLOT_DEFUNCBASE
		CASE PROPERTY_OWNED_SLOT_NIGHTCLUB
		CASE PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1
		CASE PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2
		CASE PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3
		CASE PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1
		CASE PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL2
		CASE PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL3
		CASE PROPERTY_OWNED_SLOT_CASINO_GARAGE
		CASE PROPERTY_OWNED_SLOT_ARCADE_GARAGE
		#IF FEATURE_TUNER
		CASE PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY  //MAX_MP_PROPERTIES
		#ENDIF
		#IF FEATURE_FIXER
		CASE PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR
		#ENDIF
		#IF IS_DEBUG_BUILD
		#IF FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER
		CASE PROPERTY_OWNED_SLOT_AIR_CARRIER
		#ENDIF
		#ENDIF
			RETURN TRUE
		BREAK
		CASE -1
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_RESTRICTED_SLOT_ONLY(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
		CASE PROPERTY_NIGHTCLUB
		CASE PROPERTY_MEGAWARE_GARAGE_LVL1
		CASE PROPERTY_MEGAWARE_GARAGE_LVL2
		CASE PROPERTY_MEGAWARE_GARAGE_LVL3
		CASE PROPERTY_ARENAWARS_GARAGE_LVL1
		CASE PROPERTY_ARENAWARS_GARAGE_LVL2
		CASE PROPERTY_ARENAWARS_GARAGE_LVL3
	#IF IS_DEBUG_BUILD
	#IF FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER
		//CASE PROPERTY_OWNED_SLOT_AIR_CARRIER
	#ENDIF
	#ENDIF
		CASE PROPERTY_AUTO_SHOP
		#IF FEATURE_FIXER
		CASE PROPERTY_SECURITY_OFFICE_GARAGE  //MAX_MP_PROPERTIES
		#ENDIF
		CASE PROPERTY_HANGAR
			RETURN TRUE
		BREAK
		CASE -1
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC INT GET_PROPERTY_RESTRICTED_SLOT(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
			RETURN PROPERTY_OWNED_SLOT_OFFICE_0
		BREAK
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
			RETURN PROPERTY_OWNED_SLOT_CLUBHOUSE
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			RETURN PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			RETURN PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			RETURN PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3
		BREAK
		CASE PROPERTY_HANGAR
			RETURN PROPERTY_OWNED_SLOT_HANGAR
		BREAK
		CASE PROPERTY_NIGHTCLUB
			RETURN PROPERTY_OWNED_SLOT_NIGHTCLUB
		BREAK
		CASE PROPERTY_MEGAWARE_GARAGE_LVL1
			RETURN PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL1
		BREAK
		CASE PROPERTY_MEGAWARE_GARAGE_LVL2
			RETURN PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL2
		BREAK
		CASE PROPERTY_MEGAWARE_GARAGE_LVL3
			RETURN PROPERTY_OWNED_SLOT_MEGAWARE_GARAGE_LVL3
		BREAK
		CASE PROPERTY_ARENAWARS_GARAGE_LVL1
			RETURN PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL1
		BREAK
		CASE PROPERTY_ARENAWARS_GARAGE_LVL2
			RETURN PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL2
		BREAK
		CASE PROPERTY_ARENAWARS_GARAGE_LVL3
			RETURN PROPERTY_OWNED_SLOT_ARENAWARS_GARAGE_LVL3
		BREAK
		CASE PROPERTY_CASINO_GARAGE
			RETURN PROPERTY_OWNED_SLOT_CASINO_GARAGE
		BREAK
		CASE PROPERTY_ARCADE_GARAGE
			RETURN PROPERTY_OWNED_SLOT_ARCADE_GARAGE
		BREAK
		CASE PROPERTY_AUTO_SHOP
			RETURN PROPERTY_OWNED_SLOT_AUTOSHOP_PROPERTY
		BREAK
		#IF FEATURE_FIXER
		CASE PROPERTY_SECURITY_OFFICE_GARAGE  //MAX_MP_PROPERTIES
			RETURN PROPERTY_OWNED_SLOT_SECURITY_OFFICE_GAR
		BREAK
		#ENDIF
	#IF IS_DEBUG_BUILD
	#IF FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER
		//CASE PROPERTY_OWNED_SLOT_AIR_CARRIER
	#ENDIF
	#ENDIF
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC HANDLE_REPLACE_PROPERTY_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control,INT iPropertyToBuy)
	//item description
	BOOL bSmallerGarage
	INT iTradeIn
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		//SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_2",100)
		IF GET_PROPERTY_GARAGE_SIZE(iPropertyToBuy) < GET_PROPERTY_GARAGE_SIZE(GET_OWNED_PROPERTY(control.iSlotIDS[control.iCurrentVertSel]))
			bSmallerGarage = TRUE
		ENDIF
		
		iTradeIn = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(control.iSlotIDS[control.iCurrentVertSel])
		REPLACE_PROPERTY_WARNING_MESSAGE_WEB(iTradeIn, iPropertyToBuy, bSmallerGarage)
	ELSE
//		IF control.warningscreen != NULL
//			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(control.warningscreen)
//			PRINTLN("HANDLE_REPLACE_PROPERTY_MENU_DESCRIPTION: cleaning up scaleform movie.")
//		ENDIF
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_WARNING)
		IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",1000)
		ELSE
			IF NOT SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPropertyToBuy)
			OR IS_PROPERTY_CLUBHOUSE(iPropertyToBuy)
			OR IS_PROPERTY_APARTMENT_OR_GARAGE(iPropertyToBuy)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_3",100)
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_3a",100)
			ENDIF
		ENDIF	
	ENDIF
ENDPROC



PROC SETUP_REPLACE_PROPERTY_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control , INT iSpecialReplacementSlot = -1 )
	INT iMenuCounter
	TEXT_LABEL_15 tlShortPropertyName
	INT i
	CLEAR_MENU_DATA()
	
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		//REMOVE_MENU_HELP_KEYS()
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")

		//SET_CURRENT_MENU_ITEM(-1)
	ELSE
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		IF GET_OWNED_PROPERTY(1) > 0
			SET_MENU_TITLE("MP_REP_PROP_0")
		ELSE
			SET_MENU_TITLE("MP_REP_PROP_0b")
		ENDIF
		INT iProperty
		REPEAT MAX_OWNED_PROPERTIES i
			IF i = PROPERTY_OWNED_SLOT_APT_6  
			AND NOT g_sMPtunables.bENABLE_BIKER_PROPERTY
			
			ELSE
				iProperty = GET_OWNED_PROPERTY(i)
				PRINTLN("SETUP_REPLACE_PROPERTY_MENU: GET_OWNED_PROPERTY(",i,") = ",iProperty)
				IF (NOT IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i) AND iSpecialReplacementSlot = -1)
				OR (IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i) AND iSpecialReplacementSlot = i)
					IF iProperty > 0 
						IF NOT DOES_LANGUAGE_NEED_CONDENSED_FONT_FOR_PROPERTY_NAME()
							IF NOT DOES_LANGUAGE_NEED_TO_USE_SHORT_PROPERTY_NAME()
							AND NOT DOES_PROPERTY_NEED_TO_USE_SHORT_NAME(iProperty)
								ADD_MENU_ITEM_TEXT(iMenuCounter,mpProperties[iProperty].tl_PropertyName,0,TRUE)
							ELSE
								tlShortPropertyName = mpProperties[iProperty].tl_PropertyName
								tlShortPropertyName += "S"
								ADD_MENU_ITEM_TEXT(iMenuCounter,tlShortPropertyName,0,TRUE)
							ENDIF
						ELSE
							ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,TRUE)
							ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(mpProperties[iProperty].tl_PropertyName), FALSE, TRUE)
						ENDIF
						ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,TRUE)
						
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(i))
						control.iSlotIDS[iMenuCounter] = i
						iMenuCounter++
					ELSE
						IF i > 0
							IF i <= PROPERTY_OWNED_SLOT_OFFICE_0
								IF (GET_OWNED_PROPERTY(i-1) > 0 
								AND NOT IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i-1))
								OR (IS_PROPERTY_SLOT_RESTRICTED_SLOT_ONLY(i) AND iSpecialReplacementSlot = i)
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
									control.iSlotIDS[iMenuCounter] = i
									iMenuCounter++
								ENDIF
							ELIF i = PROPERTY_OWNED_SLOT_APT_6
								IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_5) > 0
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
									control.iSlotIDS[iMenuCounter] = i
									iMenuCounter++
								ENDIF
							ELIF i = PROPERTY_OWNED_SLOT_APT_7
								IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_6) > 0
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
									control.iSlotIDS[iMenuCounter] = i
									iMenuCounter++
								ENDIF
							ELIF i = PROPERTY_OWNED_SLOT_APT_8
								IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_7) > 0
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
									control.iSlotIDS[iMenuCounter] = i
									iMenuCounter++
								ENDIF
							ELIF i = PROPERTY_OWNED_SLOT_APT_9
								IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_8) > 0
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
									control.iSlotIDS[iMenuCounter] = i
									iMenuCounter++
								ENDIF
							ELIF i = PROPERTY_OWNED_SLOT_APT_10 //MAX_OWNED_PROPERTIES //CDM PROPERTY STAT UPDATE //OLD PROPERTIES
								IF GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_9) > 0
									ADD_MENU_ITEM_TEXT(iMenuCounter,"MP_REP_PROP_1",0)
									control.iSlotIDS[iMenuCounter] = i
									iMenuCounter++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.icurrentVertSel)
		
		//Once
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "BB_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
	ENDIF	
	NET_PRINT("SETUP_REPLACE_PROPERTY_MENU: setup menu") NET_NL()
ENDPROC

PROC HANDLE_REPLACE_FACTORY_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control,INT iFactoryToBuy)
	FACTORY_ID eFactoryToBuy = INT_TO_ENUM(FACTORY_ID, iFactoryToBuy)
	//item description
	
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	//	IF GET_factory_GARAGE_SIZE(iFactoryToBuy) < GET_factory_GARAGE_SIZE(GET_OWNED_factory(control.iSlotIDS[control.iCurrentVertSel]))
	//		bSmallerGarage = TRUE
	//	ENDIF
		
		FACTORY_ID eTradeInFactory = GET_OWNED_BUNKER(PLAYER_ID())
		INT iTradeIn = GET_FACTORY_SALE_VALUE(eTradeInFactory)
		REPLACE_FACTORY_WARNING_MESSAGE_WEB(iTradeIn, eFactoryToBuy)
	ELSE
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_WARNING)
		IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("REP_BUNKER_4",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
	//	ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
	//		SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",1000)
		ELSE
		//	IF NOT SHOULD_FACTORY_BE_FREE_FOR_PLAYER(eFactoryToBuy)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_BUNC_3",100)
		//	ELSE
		//		SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_BUNC_3a",100)
		//	ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_FACTORY_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingFactoryID)
	FACTORY_ID eNewFactory = INT_TO_ENUM(FACTORY_ID, iPurchasingFactoryID)
	FACTORY_TYPE eNewFactoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eNewFactory)
	
	INT iMenuCounter
	INT i
//	INT iOwnedCount = GET_COUNT_OF_WAREHOUSES_OWNED()
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	
//		REPEAT ciMAX_OWNED_FACTORIES i
//			iWHOrder[i] = iOwnedCount
//		ENDREPEAT
	
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_BUNKER_0")
		FACTORY_ID eFactoryID
		REPEAT ciMAX_OWNED_FACTORIES i
			eFactoryID = GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), i)
			IF eFactoryID > FACTORY_ID_INVALID
				IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID) = eNewFactoryType
					BOOL bIsSelectable = ARE_FACTORIES_SUITABLE_FOR_TRADE(eFactoryID, eNewFactory)
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,bIsSelectable)
					// B* 2750666
					STRING sName = GET_FACTORY_NAME_FROM_ID(eFactoryID)
					IF IS_LANGUAGE_NON_ROMANIC()
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
					ELSE
						ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
					ENDIF
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,bIsSelectable)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_FACTORY_SALE_VALUE(eFactoryID))
					
					control.iSlotIDS[i] 	= ENUM_TO_INT(eFactoryID)
					iMenuCounter++
//				ELSE
//					//-dont offer to trade different types of factories
				ENDIF
//			ELSE
//				// -dont include empty factory slot
//				IF INT_TO_ENUM(FACTORY_ID, i) > FACTORY_ID_INVALID
//					IF GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), i-1) > FACTORY_ID_INVALID
//						ADD_MENU_ITEM_TEXT(iMenuCounter,"REP_BUNKER_1",0)
//						control.iSlotIDS[i] 	= ENUM_TO_INT(eFactoryID)
//						iMenuCounter++
//					ENDIF
//				ENDIF
			ENDIF
		ENDREPEAT
		
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.iCurrentVertSel)
		
		//Once
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "BB_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
	ENDIF
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_FACTORY_MENU: setup menu")
ENDPROC

PROC HANDLE_REPLACE_HANGAR_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control)
	//item description
	
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	//	IF GET_HANGAR_GARAGE_SIZE(iHANGARToBuy) < GET_HANGAR_GARAGE_SIZE(GET_OWNED_HANGAR(control.iSlotIDS[control.iCurrentVertSel]))
	//		bSmallerGarage = TRUE
	//	ENDIF
		
		HANGAR_ID eTradeInHangar = GET_PLAYERS_OWNED_HANGAR(PLAYER_ID())
		INT iTradeIn = GET_HANGAR_SALE_VALUE(eTradeInHangar)
		REPLACE_HANGAR_WARNING_MESSAGE_WEB(iTradeIn)
	ELSE
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_WARNING)
		IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
	//	ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
	//		SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",1000)
		ELSE
	//		IF NOT SHOULD_HANGAR_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_HANG_3",100)
	//		ELSE
	//			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_HANG_3a",100)
	//		ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_HANGAR_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingHangarID)
	HANGAR_ID eNewHangar = INT_TO_ENUM(HANGAR_ID, iPurchasingHangarID)
	
	INT iMenuCounter
	INT i
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_HANGAR_0")
		HANGAR_ID eHangarID
//		REPEAT ciMAX_OWNED_FACTORIES i
			eHangarID = GET_PLAYERS_OWNED_HANGAR(PLAYER_ID())	//GET_HANGAR_ID_FROM_HANGAR_SLOT(PLAYER_ID(), i)
			IF eHangarID > HANGAR_ID_INVALID
//				IF GET_GOODS_CATEGORY_FROM_HANGAR_ID(eHangarID) = eNewHangarType
					BOOL bIsSelectable = ARE_HANGERS_SUITABLE_FOR_TRADE(eHangarID, eNewHangar)
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,bIsSelectable)
					// B* 2750666
					STRING sName = GET_HANGAR_NAME_FROM_ID(eHangarID)
					IF IS_LANGUAGE_NON_ROMANIC()
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
					ELSE
						ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
					ENDIF
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,bIsSelectable)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_HANGAR_SALE_VALUE(eHangarID))
					
					control.iSlotIDS[i] 	= ENUM_TO_INT(eHangarID)
					iMenuCounter++
//				ENDIF
			ENDIF
//		ENDREPEAT
		
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.iCurrentVertSel)
		
		//Once
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "BB_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
	ENDIF
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_HANGAR_MENU: setup menu")
ENDPROC

PROC HANDLE_REPLACE_DEFUNCT_BASE_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control)
	//item description
	
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	//	IF GET_DEFUNCT_BASE_GARAGE_SIZE(iDEFUNCT_BASEToBuy) < GET_DEFUNCT_BASE_GARAGE_SIZE(GET_OWNED_DEFUNCT_BASE(control.iSlotIDS[control.iCurrentVertSel]))
	//		bSmallerGarage = TRUE
	//	ENDIF
		
		DEFUNCT_BASE_ID eTradeInBase = GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID())
		INT iTradeIn = GET_DEFUNCT_BASE_SALE_VALUE(eTradeInBase)
		REPLACE_DEFUNCT_BASE_WARNING_MESSAGE_WEB(iTradeIn)
	ELSE
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_WARNING)
		IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
	//	ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
	//		SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",1000)
		ELSE
	//		IF NOT SHOULD_DEFUNCT_BASE_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_DBASE_3",100)
	//		ELSE
	//			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_DBASE_3a",100)
	//		ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_DEFUNCT_BASE_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingBaseID)
	DEFUNCT_BASE_ID eNewBaseID = INT_TO_ENUM(DEFUNCT_BASE_ID, iPurchasingBaseID)
	
	INT iMenuCounter
	INT i
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_DBASE_0")
		DEFUNCT_BASE_ID eBaseID
//		REPEAT ciMAX_OWNED_FACTORIES i
			eBaseID = GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID())	//GET_DEFUNCT_BASE_ID_FROM_DEFUNCT_BASE_SLOT(PLAYER_ID(), i)
			IF eBaseID > DEFUNCT_BASE_ID_INVALID
//				IF GET_GOODS_CATEGORY_FROM_DEFUNCT_BASE_ID(eBaseID) = eNewBaseIDType
					BOOL bIsSelectable = ARE_DEFUNCT_BASES_SUITABLE_FOR_TRADE(eBaseID, eNewBaseID)
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,bIsSelectable)
					// B* 2750666
					STRING sName = GET_DEFUNCT_BASE_NAME_FROM_ID(eBaseID)
					IF IS_LANGUAGE_NON_ROMANIC()
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
					ELSE
						ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
					ENDIF
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,bIsSelectable)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_DEFUNCT_BASE_SALE_VALUE(eBaseID))
					
					control.iSlotIDS[i] 	= ENUM_TO_INT(eBaseID)
					iMenuCounter++
//				ENDIF
			ENDIF
//		ENDREPEAT
		
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.iCurrentVertSel)
		
		//Once
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "BB_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
	ENDIF
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_DEFUNCT_BASE_MENU: setup menu")
ENDPROC

PROC HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control)
	//item description
	
	IF IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
	//	IF GET_NIGHTCLUB_GARAGE_SIZE(iNIGHTCLUBToBuy) < GET_NIGHTCLUB_GARAGE_SIZE(GET_OWNED_NIGHTCLUB(control.iSlotIDS[control.iCurrentVertSel]))
	//		bSmallerGarage = TRUE
	//	ENDIF
		
		NIGHTCLUB_ID eTradeInBase = GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID())
		INT iTradeIn = GET_NIGHTCLUB_SALE_VALUE(eTradeInBase)
		REPLACE_NIGHTCLUB_WARNING_MESSAGE_WEB(iTradeIn)
	ELSE
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_WARNING)
		IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4",100)
		ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0",100)
	//	ELIF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
	//		SET_CURRENT_MENU_ITEM_DESCRIPTION("PROP3RDDIS",1000)
		ELSE
	//		IF NOT SHOULD_NIGHTCLUB_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_NCLUB_3",100)
	//		ELSE
	//			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_NCLUB_3a",100)
	//		ENDIF
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_NIGHTCLUB_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingBaseID)
	NIGHTCLUB_ID eNewBaseID = INT_TO_ENUM(NIGHTCLUB_ID, iPurchasingBaseID)
	
	INT iMenuCounter
	INT i
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_NCLUB_0")
		NIGHTCLUB_ID eBaseID
//		REPEAT ciMAX_OWNED_FACTORIES i
			eBaseID = GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID())	//GET_NIGHTCLUB_ID_FROM_NIGHTCLUB_SLOT(PLAYER_ID(), i)
			IF eBaseID > NIGHTCLUB_ID_INVALID
//				IF GET_GOODS_CATEGORY_FROM_NIGHTCLUB_ID(eBaseID) = eNewBaseIDType
					BOOL bIsSelectable = ARE_NIGHTCLUBS_SUITABLE_FOR_TRADE(eBaseID, eNewBaseID)
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"PIM_DNAME",1,bIsSelectable)
					// B* 2750666
					STRING sName = GET_NIGHTCLUB_NAME_FROM_ID(eBaseID)
					IF IS_LANGUAGE_NON_ROMANIC()
						ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
					ELSE
						ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
					ENDIF
					
					ADD_MENU_ITEM_TEXT(iMenuCounter,"CUST_GAR_COST",1,bIsSelectable)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_NIGHTCLUB_SALE_VALUE(eBaseID))
					
					control.iSlotIDS[i] 	= ENUM_TO_INT(eBaseID)
					iMenuCounter++
//				ENDIF
			ENDIF
//		ENDREPEAT
		
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.iCurrentVertSel)
		
		//Once
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "BB_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
	ENDIF
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_NIGHTCLUB_MENU: setup menu")
ENDPROC

#IF FEATURE_CASINO_HEIST
PROC HANDLE_REPLACE_ARCADE_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control)
	IF IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		ARCADE_PROPERTY_ID eTradeInArcade = GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID())
		
		INT iTradeIn = GET_ARCADE_SALE_VALUE(eTradeInArcade)
		
		REPLACE_ARCADE_WARNING_MESSAGE_WEB(iTradeIn)
	ELSE
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_WARNING)
		
		IF IS_BIT_SET(control.iBS, REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4", 100)
		ELIF IS_BIT_SET(control.iBS, REP_VEH_PROP_BS_HEIST_ACTIVE)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_PROP_HEI_0", 100)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_ARC_3", 100)
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_ARCADE_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingArcadeID)
	ARCADE_PROPERTY_ID eNewArcadeID = INT_TO_ENUM(ARCADE_PROPERTY_ID, iPurchasingArcadeID)
	
	INT iMenuCounter
	
	INT i
	
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_ARC_0")
		
		ARCADE_PROPERTY_ID eArcadeID = GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID())
		
		IF eArcadeID > ARCADE_PROPERTY_ID_INVALID
			BOOL bIsSelectable = ARE_ARCADES_SUITABLE_FOR_TRADE(eArcadeID, eNewArcadeID)
			
			ADD_MENU_ITEM_TEXT(iMenuCounter, "PIM_DNAME", 1, bIsSelectable)
			
			STRING sName = GET_ARCADE_NAME_FROM_ID(eArcadeID)
			
			IF IS_LANGUAGE_NON_ROMANIC()
				ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
			ELSE
				ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
			ENDIF
			
			ADD_MENU_ITEM_TEXT(iMenuCounter, "CUST_GAR_COST", 1, bIsSelectable)
			ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_ARCADE_SALE_VALUE(eArcadeID))
			
			control.iSlotIDS[i] = ENUM_TO_INT(eArcadeID)
			
			iMenuCounter++
		ENDIF
		
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_NO_MONEY)
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_HEIST_ACTIVE)
		
		control.iMaxVertSel	= iMenuCounter
		
		SET_CURRENT_MENU_ITEM(control.iCurrentVertSel)
		
		REMOVE_MENU_HELP_KEYS()
		
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_SELECT")
		
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_BACK")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_ARCADE_MENU: setup menu")
ENDPROC
#ENDIF

PROC HANDLE_REPLACE_AUTO_SHOP_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &control)
	IF IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		AUTO_SHOP_PROPERTY_ID eTradeInAutoShop = GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID())
		INT iTradeIn = GET_AUTO_SHOP_SALE_VALUE(eTradeInAutoShop)
		REPLACE_AUTO_SHOP_WARNING_MESSAGE_WEB(iTradeIn)
	ELSE
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_WARNING)
		
		IF IS_BIT_SET(control.iBS, REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4", 100)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_AUT_3", 100)
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_AUTO_SHOP_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control, INT iPurchasingAutoShopID)
	AUTO_SHOP_PROPERTY_ID eNewAutoShopID = INT_TO_ENUM(AUTO_SHOP_PROPERTY_ID, iPurchasingAutoShopID)
	
	INT iMenuCounter
	INT i
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_AUT_0")
		
		AUTO_SHOP_PROPERTY_ID eAutoShopID = GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID())
		
		IF eAutoShopID > AUTO_SHOP_PROPERTY_ID_INVALID
			BOOL bIsSelectable = ARE_AUTO_SHOPS_SUITABLE_FOR_TRADE(eAutoShopID, eNewAutoShopID)
			
			ADD_MENU_ITEM_TEXT(iMenuCounter, "PIM_DNAME", 1, bIsSelectable)
			
			STRING sName = GET_AUTO_SHOP_NAME_FROM_ID(eAutoShopID)
			
			IF IS_LANGUAGE_NON_ROMANIC()
				ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
			ELSE
				ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
			ENDIF
			
			ADD_MENU_ITEM_TEXT(iMenuCounter, "CUST_GAR_COST", 1, bIsSelectable)
			ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_AUTO_SHOP_SALE_VALUE(eAutoShopID))
			
			control.iSlotIDS[i] = ENUM_TO_INT(eAutoShopID)
			iMenuCounter++
		ENDIF
		
		CLEAR_BIT(control.iBS, REP_VEH_PROP_BS_NO_MONEY)
		
		control.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(control.iCurrentVertSel)
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_BACK")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_AUTO_SHOP_MENU: setup menu")
ENDPROC

#IF FEATURE_FIXER
PROC HANDLE_REPLACE_FIXER_HQ_MENU_DESCRIPTION(REPLACE_MP_VEH_OR_PROP_MENU &ref_sControl)
	IF IS_BIT_SET(ref_sControl.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		FIXER_HQ_ID eTradeInFixerHQ = GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID())
		INT iTradeIn = GET_FIXER_HQ_SALE_VALUE(eTradeInFixerHQ)
		REPLACE_FIXER_HQ_WARNING_MESSAGE_WEB(iTradeIn)
	ELSE
		CLEAR_BIT(ref_sControl.iBS, REP_VEH_PROP_BS_REQ_SF)
		CLEAR_BIT(ref_sControl.iBS, REP_VEH_PROP_BS_WARNING)
		
		IF IS_BIT_SET(ref_sControl.iBS, REP_VEH_PROP_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_PROP_4", 100)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("MP_REP_FIX_3", 100)
		ENDIF	
	ENDIF
ENDPROC

PROC SETUP_REPLACE_FIXER_HQ_MENU(REPLACE_MP_VEH_OR_PROP_MENU &ref_sControl, INT iPurchasingFixerHQID)
	FIXER_HQ_ID eNewFixerHQID = INT_TO_ENUM(FIXER_HQ_ID, iPurchasingFixerHQID)
	
	INT iMenuCounter
	INT i
	CLEAR_MENU_DATA()
	
	IF NOT IS_BIT_SET(ref_sControl.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		SET_MENU_TITLE("REP_FIX_0")
		
		FIXER_HQ_ID eFixerHQID = GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID())
		
		IF eFixerHQID > FIXER_HQ_ID_INVALID
			BOOL bIsSelectable = ARE_FIXER_HQS_SUITABLE_FOR_TRADE(eFixerHQID, eNewFixerHQID)
			
			ADD_MENU_ITEM_TEXT(iMenuCounter, "PIM_DNAME", 1, bIsSelectable)
			
			STRING sName = GET_FIXER_HQ_NAME_FROM_ID(eFixerHQID)
			
			IF IS_LANGUAGE_NON_ROMANIC()
				ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName))
			ELSE
				ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(GET_FILENAME_FOR_AUDIO_CONVERSATION(sName), FALSE, TRUE)
			ENDIF
			
			ADD_MENU_ITEM_TEXT(iMenuCounter, "CUST_GAR_COST", 1, bIsSelectable)
			ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_FIXER_HQ_SALE_VALUE(eFixerHQID))
			
			ref_sControl.iSlotIDS[i] = ENUM_TO_INT(eFixerHQID)
			iMenuCounter++
		ENDIF
		
		CLEAR_BIT(ref_sControl.iBS, REP_VEH_PROP_BS_NO_MONEY)
		
		ref_sControl.iMaxVertSel	= iMenuCounter
		SET_CURRENT_MENU_ITEM(ref_sControl.iCurrentVertSel)
		REMOVE_MENU_HELP_KEYS()
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ADD_MENU_HELP_KEY_INPUT(INPUT_CELLPHONE_CANCEL, "BB_BACK")
	ENDIF
	
	CPRINTLN(DEBUG_INTERNET, "SETUP_REPLACE_FIXER_HQ_MENU: setup menu")
ENDPROC
#ENDIF

ENUM REPLACE_PROPERTY_MENU_TYPE_ENUM
	 rpmt_0 = 0
	,rpmt_1_FACTORY
	,rpmt_2_HANGAR
	,rpmt_3_DEFUNCT_BASE
	,rpmt_4_NIGHTCLUB
	,rpmt_5_ARENA_GARAGE
	,rpmt_6_CASINO_APARTMENT
	,rpmt_7_ARCADE
	,rpmt_8_AUTO_SHOP
	#IF FEATURE_FIXER
	,rpmt_9_FIXER_HQ
	#ENDIF
ENDENUM

FUNC BOOL RUN_REPLACE_PROPERTY_MENU(REPLACE_MP_VEH_OR_PROP_MENU &control,INT &iResultSlot,INT iPurchasingPropertyID
		, INT iSpecialReplacementSlot = -1 
		, REPLACE_PROPERTY_MENU_TYPE_ENUM eReplacePropertyMenuType = rpmt_0 
		)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_PLAYER_IN_CORONA()
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR g_bMissionEnding
	OR g_bAbortPropertyMenus
	OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
		IF g_bAbortPropertyMenus
			PRINTLN("RUN_REPLACE_PROPERTY_MENU: aborting menu. g_bAbortPropertyMenus = TRUE")
		ENDIF
		iResultSlot = -1
		NET_PRINT("RUN_REPLACE_PROPERTY_MENU - NETWORK_IS_GAME_IN_PROGRESS = FALSE - returning slot -1") NET_NL()
		CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
		RETURN(TRUE)
	ENDIF
	
//	INT iSelectedSlot
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()

	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_FORWARD)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_BACKWARD)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK2)
	
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		
		IF NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_WARNING_MESSAGE_ACTIVE()
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		ENDIF 

		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		SET_MOUSE_CURSOR_THIS_FRAME()		
	ENDIF
	
	SET_BIT(control.iBS,REP_VEH_PROP_BS_USING)
//	#IF FEATURE_EXECUTIVE
//	IF iSpecialReplacementSlot != -1
//		iSelectedSlot = iSpecialReplacementSlot
//	ELSE
//	#ENDIF
//		iSelectedSlot = control.iCurrentVertSel
//	#IF FEATURE_EXECUTIVE
//	ENDIF
//	#ENDIF
	IF LOAD_MENU_ASSETS()
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_SETUP)
				IF (eReplacePropertyMenuType = rpmt_1_FACTORY)
					SETUP_REPLACE_FACTORY_MENU(control,iPurchasingPropertyID)
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_FACTORY_MENU_DESCRIPTION(control,iPurchasingPropertyID)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_2_HANGAR)
					SETUP_REPLACE_HANGAR_MENU(control,iPurchasingPropertyID)
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_HANGAR_MENU_DESCRIPTION(control)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_3_DEFUNCT_BASE)
					SETUP_REPLACE_DEFUNCT_BASE_MENU(control,iPurchasingPropertyID)
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_DEFUNCT_BASE_MENU_DESCRIPTION(control)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_4_NIGHTCLUB)
					SETUP_REPLACE_NIGHTCLUB_MENU(control,iPurchasingPropertyID)
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(control)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_5_ARENA_GARAGE)
					SETUP_REPLACE_NIGHTCLUB_MENU(control,iPurchasingPropertyID)
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(control)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_6_CASINO_APARTMENT)
					SETUP_REPLACE_NIGHTCLUB_MENU(control,iPurchasingPropertyID)
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(control)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_7_ARCADE)
					SETUP_REPLACE_ARCADE_MENU(control, iPurchasingPropertyID)
					
					SET_BIT(control.iBS, REP_VEH_PROP_BS_SETUP)
					
					HANDLE_REPLACE_ARCADE_MENU_DESCRIPTION(control)
					
					IF NOT IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ELIF (eReplacePropertyMenuType = rpmt_8_AUTO_SHOP)
					SETUP_REPLACE_AUTO_SHOP_MENU(control, iPurchasingPropertyID)
					SET_BIT(control.iBS, REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_AUTO_SHOP_MENU_DESCRIPTION(control)
					
					IF NOT IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				#IF FEATURE_FIXER
				ELIF (eReplacePropertyMenuType = rpmt_9_FIXER_HQ)
					SETUP_REPLACE_FIXER_HQ_MENU(control, iPurchasingPropertyID)
					SET_BIT(control.iBS, REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_FIXER_HQ_MENU_DESCRIPTION(control)
					
					IF NOT IS_BIT_SET(control.iButtonBS, PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				#ENDIF
				ELSE
					SETUP_REPLACE_PROPERTY_MENU(control ,iSpecialReplacementSlot )
					SET_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
					HANDLE_REPLACE_PROPERTY_MENU_DESCRIPTION(control,iPurchasingPropertyID)
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						DRAW_MENU()
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				AND NOT g_sTransitionSessionData.sEndReserve.bTranCleanUpEndReservingSlot
				AND NOT (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_IN() OR IS_SCREEN_FADING_OUT())
					
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						IF IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE()
						OR (IS_MENU_CURSOR_ACCEPT_PRESSED() AND (g_iMenuCursorItem = control.iCurrentVertSel))
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
							INT propval, iOwnedProperty, iTradeInValue 
							IF (eReplacePropertyMenuType = rpmt_1_FACTORY)
								propval = CEIL(TO_FLOAT(GET_FACTORY_PRICE(INT_TO_ENUM(FACTORY_ID, iPurchasingPropertyID)))*g_sMPTunables.fPropertyMultiplier)
								IF SHOULD_FACTORY_BE_FREE_FOR_PLAYER(INT_TO_ENUM(FACTORY_ID, iPurchasingPropertyID))
									propval = 0
								ENDIF
								iOwnedProperty = ENUM_TO_INT(GET_OWNED_BUNKER(PLAYER_ID()))
								iTradeInValue = GET_FACTORY_SALE_VALUE(INT_TO_ENUM(FACTORY_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_2_HANGAR)
								propval = CEIL(TO_FLOAT(GET_HANGAR_PRICE(INT_TO_ENUM(HANGAR_ID, iPurchasingPropertyID)))*g_sMPTunables.fPropertyMultiplier)
							//	IF SHOULD_HANGAR_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
							//		propval = 0
							//	ENDIF
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_HANGAR(PLAYER_ID()))
								iTradeInValue = GET_HANGAR_SALE_VALUE(INT_TO_ENUM(HANGAR_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_3_DEFUNCT_BASE)
								propval = CEIL(TO_FLOAT(GET_DEFUNCT_BASE_PRICE(INT_TO_ENUM(DEFUNCT_BASE_ID, iPurchasingPropertyID)))*g_sMPTunables.fPropertyMultiplier)
							//	IF SHOULD_DEFUNCT_BASE_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
							//		propval = 0
							//	ENDIF
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_DEFUNCT_BASE(PLAYER_ID()))
								iTradeInValue = GET_DEFUNCT_BASE_SALE_VALUE(INT_TO_ENUM(DEFUNCT_BASE_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_4_NIGHTCLUB)
								propval = CEIL(TO_FLOAT(GET_NIGHTCLUB_PRICE(INT_TO_ENUM(NIGHTCLUB_ID, iPurchasingPropertyID)))*g_sMPTunables.fPropertyMultiplier)
							//	IF SHOULD_NIGHTCLUB_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
							//		propval = 0
							//	ENDIF
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_NIGHTCLUB(PLAYER_ID()))
								iTradeInValue = GET_NIGHTCLUB_SALE_VALUE(INT_TO_ENUM(NIGHTCLUB_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_5_ARENA_GARAGE)
								propval = CEIL(TO_FLOAT(GET_ARENA_GARAGE_PRICE(INT_TO_ENUM(ARENA_GARAGE_ID, iPurchasingPropertyID)))*g_sMPTunables.fPropertyMultiplier)
							//	IF SHOULD_ARENA_GARAGE_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
							//		propval = 0
							//	ENDIF
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_ARENA_GARAGE(PLAYER_ID()))
								iTradeInValue = GET_ARENA_GARAGE_SALE_VALUE(INT_TO_ENUM(ARENA_GARAGE_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_6_CASINO_APARTMENT)
								propval = CEIL(TO_FLOAT(GET_CASINO_APT_PRICE(INT_TO_ENUM(CASINO_APT_ID, iPurchasingPropertyID)))*g_sMPTunables.fPropertyMultiplier)
							//	IF SHOULD_CASINO_APT_BE_FREE_FOR_PLAYER(iBuyPropertyIndex)
							//		propval = 0
							//	ENDIF
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_CASINO_APARTMENT(PLAYER_ID()))
								iTradeInValue = GET_CASINO_APT_SALE_VALUE(INT_TO_ENUM(CASINO_APT_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_7_ARCADE)
								propval = CEIL(TO_FLOAT(GET_ARCADE_PRICE(INT_TO_ENUM(ARCADE_PROPERTY_ID, iPurchasingPropertyID))) * g_sMPTunables.fPropertyMultiplier)
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_ARCADE_PROPERTY(PLAYER_ID()))
								iTradeInValue = GET_ARCADE_SALE_VALUE(INT_TO_ENUM(ARCADE_PROPERTY_ID, iOwnedProperty))
							ELIF (eReplacePropertyMenuType = rpmt_8_AUTO_SHOP)
								propval = CEIL(TO_FLOAT(GET_AUTO_SHOP_PRICE(INT_TO_ENUM(AUTO_SHOP_PROPERTY_ID, iPurchasingPropertyID))) * g_sMPTunables.fPropertyMultiplier)
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_AUTO_SHOP_PROPERTY(PLAYER_ID()))
								iTradeInValue = GET_AUTO_SHOP_SALE_VALUE(INT_TO_ENUM(AUTO_SHOP_PROPERTY_ID, iOwnedProperty))
							#IF FEATURE_FIXER
							ELIF (eReplacePropertyMenuType = rpmt_9_FIXER_HQ)
								propval = CEIL(TO_FLOAT(GET_FIXER_HQ_COST(INT_TO_ENUM(FIXER_HQ_ID, iPurchasingPropertyID))) * g_sMPTunables.fPropertyMultiplier)
								iOwnedProperty = ENUM_TO_INT(GET_PLAYERS_OWNED_FIXER_HQ(PLAYER_ID()))
								iTradeInValue = GET_FIXER_HQ_SALE_VALUE(INT_TO_ENUM(FIXER_HQ_ID, iOwnedProperty))
							#ENDIF
							ELSE
								propval = CEIL(TO_FLOAT(mpProperties[iPurchasingPropertyID].iValue)*g_sMPTunables.fPropertyMultiplier)
								IF SHOULD_PROPERTY_BE_FREE_FOR_PLAYER(iPurchasingPropertyID)
									propval = 0
								ENDIF
								iOwnedProperty = GET_OWNED_PROPERTY(control.iSlotIDS[control.iCurrentVertSel])
								iTradeInValue = GET_VALUE_OF_CURRENTLY_OWNED_PROPERTY(control.iSlotIDS[control.iCurrentVertSel])
							ENDIF
							
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
							PRINTLN("CDM:control.iCurrentVertSel ", control.iCurrentVertSel, " ownedProperty: ", iOwnedProperty )
							PRINTLN("CDM: propVal = ", propval, " iTradeInValue = ", iTradeInValue, " iDiff = ", (propval - iTradeInValue))
							
						//	INT iBankBalance = NETWORK_GET_VC_BANK_BALANCE()//bank only 1589342 GET_LOCAL_PLAYER_VC_AMOUNT(TRUE)
						//	INT iWalletBalance = NETWORK_GET_VC_WALLET_BALANCE()
						//	
						//	PRINTLN("CDM: iBankBalance = ",iBankBalance," iWalletBalance = ",iWalletBalance)
						//	
						//	IF (iBankBalance+iWalletBalance) >= (propval - iTradeInValue)
							IF (propval - iTradeInValue) <= 0
							OR NETWORK_CAN_SPEND_MONEY(propval - iTradeInValue,FALSE,TRUE,FALSE)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
								IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								AND iOwnedProperty != 0
									IF g_sMPTUNABLES.bdisable_purchase_of_third_property
									AND control.iSlotIDS[control.iCurrentVertSel] >= 2
										SET_BIT(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
									ELIF (Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
									OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
										SET_BIT(control.iBS,REP_VEH_PROP_BS_HEIST_ACTIVE)
									ELSE	
										//REMOVE_MENU_HELP_KEYS()
										//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_YES")
										//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_NO")
										CLEANUP_MENU_ASSETS()
										SET_BIT(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
										RETURN FALSE
									ENDIF
								ELSE
									IF g_sMPTUNABLES.bdisable_purchase_of_third_property
									AND control.iSlotIDS[control.iCurrentVertSel] >= 2
										SET_BIT(control.iBS,REP_VEH_PROP_BS_3RD_DISABLE)
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
									ELIF control.iSlotIDS[control.iCurrentVertSel] = 0
										iResultSlot = control.iSlotIDS[control.iCurrentVertSel]
										CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
										CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
										RETURN TRUE
									ELSE
										//IF IS_BIT_SET(control.iBS,REP_VEH_PROP_BS_REQ_SF)
										//AND HAS_SCALEFORM_MOVIE_LOADED(control.warningscreen)
											iResultSlot = control.iSlotIDS[control.iCurrentVertSel]
											CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
											CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
											RETURN TRUE
										//ENDIF
									ENDIF
								ENDIF
							ELSE
								SET_BIT(control.iBS,REP_VEH_PROP_BS_NO_MONEY)
							ENDIF
							SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						ENDIF
					ELSE 
						IF NOT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT)
							CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_ACCEPT)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						IF IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE()
						OR (IS_MENU_CURSOR_CANCEL_PRESSED() AND (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM))
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
							PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
							IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								NET_PRINT("RUN_REPLACE_PROPERTY_MENU player exited menu") NET_NL()
								CLEANUP_REPLACE_MP_VEH_OR_PROP_MENU(control)
								iResultSlot = -1
								RETURN TRUE
							ELSE
								//REMOVE_MENU_HELP_KEYS()
								//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_ACCEPT, "BB_SELECT")
								//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_CANCEL, "BB_BACK")
								//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_DPAD_UD,"HUD_INPUT1C")
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
							ENDIF
							
							SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
							CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_CANCEL)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
						IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_MENU_CURSOR_SCROLL_UP_PRESSED()
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								control.iCurrentVertSel--
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								RESET_NET_TIMER(control.menuNavigationDelay)
							ENDIF
						ELSE
							IF REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(control,INPUT_CELLPHONE_UP)
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_UP)	
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								control.iCurrentVertSel++
								SET_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)
								CLEAR_BIT(control.iBS,REP_VEH_PROP_BS_SETUP)
								RESET_NET_TIMER(control.menuNavigationDelay)
							ENDIF
						ELSE
							IF REPLACE_MP_VEH_OR_PROP_CHECK_MENU_INPUT_DELAY(control,INPUT_CELLPHONE_DOWN)
								CLEAR_BIT(control.iButtonBS,PROP_BUTTON_BS_PRESSED_DOWN)	
							ENDIF
						ENDIF
					ENDIF
					
					// handle clicking on a new highlighted option
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						IF (g_iMenuCursorItem != control.iCurrentVertSel)
							control.iCurrentVertSel = g_iMenuCursorItem
							SET_CURRENT_MENU_ITEM(control.icurrentVertSel)
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
						ENDIF
					ENDIF
					
					IF control.iCurrentVertSel >= control.iMaxVertSel
						control.iCurrentVertSel = 0
					ENDIF
					IF Control.iCurrentVertSel < 0
						control.iCurrentVertSel = control.iMaxVertSel-1
					ENDIF
					//PRINTLN("control.iCurrentVertSel = ",control.iCurrentVertSel)
					IF (eReplacePropertyMenuType = rpmt_1_FACTORY)
						HANDLE_REPLACE_FACTORY_MENU_DESCRIPTION(control,iPurchasingPropertyID)
					ELIF (eReplacePropertyMenuType = rpmt_2_HANGAR)
						HANDLE_REPLACE_HANGAR_MENU_DESCRIPTION(control)
					ELIF (eReplacePropertyMenuType = rpmt_3_DEFUNCT_BASE)
						HANDLE_REPLACE_DEFUNCT_BASE_MENU_DESCRIPTION(control)
					ELIF (eReplacePropertyMenuType = rpmt_4_NIGHTCLUB)
						HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(control)
					ELIF (eReplacePropertyMenuType = rpmt_5_ARENA_GARAGE)
						HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(control)
					ELIF (eReplacePropertyMenuType = rpmt_6_CASINO_APARTMENT)
						HANDLE_REPLACE_NIGHTCLUB_MENU_DESCRIPTION(control)
					ELIF (eReplacePropertyMenuType = rpmt_7_ARCADE)
						HANDLE_REPLACE_ARCADE_MENU_DESCRIPTION(control)
					ELIF (eReplacePropertyMenuType = rpmt_8_AUTO_SHOP)
						HANDLE_REPLACE_AUTO_SHOP_MENU_DESCRIPTION(control)
					#IF FEATURE_FIXER
					ELIF (eReplacePropertyMenuType = rpmt_9_FIXER_HQ)
						HANDLE_REPLACE_FIXER_HQ_MENU_DESCRIPTION(control)
					#ENDIF
					ELSE
						HANDLE_REPLACE_PROPERTY_MENU_DESCRIPTION(control,iPurchasingPropertyID)
					ENDIF
					IF NOT IS_BIT_SET(control.iButtonBS,PROP_BUTTON_BS_GIVEN_CONFIRMATION)
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
						DRAW_MENU()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MP_PROP_FLAG_SET(INT& iBS[],INT iBit)
	RETURN IS_BIT_SET(iBS[FLOOR(TO_FLOAT(iBit/32))],iBit%32)
//	IF iBit <= 31
//	AND iBit >= 0
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[0] bit# ",iBit)
//		RETURN IS_BIT_SET(iBS[0],iBit)
//	ELIF iBit <= 63
//	AND iBit >= 32
//		iBit = iBit - 32
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[1] bit# ",iBit)
//		RETURN IS_BIT_SET(iBS[1],iBit)
//	ELSE
//		iBit = iBit - 64
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[2] bit# ",iBit)
//		RETURN IS_BIT_SET(iBS[2],iBit)
//	ENDIF
//	RETURN FALSE
ENDFUNC

PROC SET_MP_PROP_FLAG(INT& iBS[],INT iBit)
	SET_BIT(iBS[FLOOR(TO_FLOAT(iBit/32))],iBit%32)
//	IF iBit <= 31
//	AND iBit >= 0
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[0] bit# ",iBit)
//		SET_BIT(iBS[0],iBit)
//	ELIF iBit <= 63
//	AND iBit >= 32
//		iBit = iBit - 32
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[1] bit# ",iBit)
//		SET_BIT(iBS[1],iBit)
//	ELSE
//		iBit = iBit - 64
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[2] bit# ",iBit)
//		SET_BIT(iBS[2],iBit)
//	ENDIF
	
ENDPROC

PROC CLEAR_MP_PROP_FLAG(INT& iBS[],INT iBit)
	CLEAR_BIT(iBS[FLOOR(TO_FLOAT(iBit/32))],iBit%32)
//	IF iBit <= 31
//	AND iBit >= 0
//		CLEAR_BIT(iBS[0],iBit)
//
//	ELIF iBit <= 63
//	AND iBit >= 32
//		iBit = iBit - 32
//		CLEAR_BIT(iBS[1],iBit)
//	ELSE
//		iBit = iBit - 64
//		//PRINTLN("SET_MP_PROP_FLAG: checking iBS[1] bit# ",iBit)
//		SET_BIT(iBS[2],iBit)
//	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_USE_PROPERTY(BOOL bIgnoreUnlockCheck = FALSE #IF IS_DEBUG_BUILD, BOOL bPrintReason = FALSE#ENDIF)
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)
	AND NOT bIgnoreUnlockCheck
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE not unlocked garage")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	IF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE s_Player_Currently_On_MP_LTS_Mission")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE Is_Player_Currently_On_MP_CTF_Mission")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE Is_Player_Currently_On_MP_Versus_Mission")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	
	IF IS_PLAYER_ON_IMPROMPTU_DM()
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE IS_PLAYER_ON_IMPROMPTU_DM()")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_BOSSVBOSS_DM()
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE IS_PLAYER_ON_BOSSVBOSS_DM()")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	 // FEATURE_GANG_BOSS
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- false on DM or survival")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_RACE_TO_POINT
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- false on race to point")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION() 
		IF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FINALE()
		#IF FEATURE_CASINO_HEIST
		OR CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		#ENDIF
			#IF IS_DEBUG_BUILD
			IF bPrintReason
				PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE on Heist mission finale")
			ENDIF
			#ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyThree, ciOptionsBS23_DisablePlayerPropertiesInMission)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE ciOptionsBS23_DisablePlayerPropertiesInMission")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF IS_PLAYER_ON_MP_MISSION(PLAYER_ID(), eFM_MISSION_CLOUD)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- true on cloud mission")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID(),TRUE)
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE on FM mission")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	IF HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE starting race")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE mission launch in progress")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	IF g_bLauncherWillControlMiniMap
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE g_bLauncherWillControlMiniMap")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF	
	
	IF IS_FAKE_MULTIPLAYER_MODE_SET() 
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE IS_FAKE_MULTIPLAYER_MODE_SET")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SPECTATING(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE IS_PLAYER_SPECTATING")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
 	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE IS_PLAYER_AN_ANIMAL")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_USING_ANY_RC_VEHICLE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		IF bPrintReason
			PRINTLN("CAN_PLAYER_USE_PROPERTY- FALSE IS_PLAYER_USING_ANY_RC_VEHICLE")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC INITIALISE_MP_PROPERTY_BLIPS()
	INT i
	REPEAT MP_PROPERTY_BUILDING_MAX i
		CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertySetBlipBS,i)
		CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,i)
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_PROPERTY_BLIPS_BE_HIDDEN(BOOL bIsYacht = FALSE)
	IF SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
		RETURN TRUE
	ENDIF
	
	IF bIsYacht
		IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_SUPER_YACHTS)
			RETURN TRUE
		ENDIF
	ELSE
		IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_PROPERTIES_PROPERTY)
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CASHING_OUT
		RETURN TRUE
	ENDIF
	IF IS_LOCAL_PLAYER_USING_RCBANDITO()
		RETURN TRUE
	ENDIF
	#IF FEATURE_HEIST_ISLAND
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND() 
		RETURN TRUE
	ENDIF
	IF (GB_IS_PLAYER_ON_ISLAND_HEIST_PREP_MISSION(PLAYER_ID()) AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID()))
		RETURN TRUE
	ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLIP_APPEAR_FOR_ENTRANCE(INT iBuilding, INT iEntrance)
	IF ARE_VECTORS_EQUAL(tempPropertyStruct.vBlipLocation[iEntrance],<<0,0,0>>)
		RETURN FALSE
	ENDIF
	IF IS_BUILDING_OFFICE_PROPERTY(iBuilding)
	AND iEntrance = 1
		RETURN FALSE
	ENDIF
	IF IS_BUILDING_OFFICE_PROPERTY(iBuilding)
	AND iEntrance = 2
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


FUNC BOOL IS_BUILDING_A_GANG_RELATED_PROPERTY(INT iBuilding)
	IF IS_BUILDING_OFFICE_PROPERTY(iBuilding)
		RETURN TRUE
	ENDIF
	IF IS_BUILDING_CLUBHOUSE_PROPERTY(iBuilding)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_A_GANG_RELATED_PROPERTY(INT iPropertyID)
	IF IS_PROPERTY_OFFICE(iPropertyID)
		RETURN TRUE
	ENDIF
	IF IS_PROPERTY_CLUBHOUSE(iPropertyID)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_THIS_PROPERTY(PLAYER_INDEX playerID, INT iPropertyID)
	INT i
	IF playerID != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(playerID,FALSE)
		REPEAT MAX_OWNED_PROPERTIES i
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[i] = iPropertyID
				RETURN TRUE
			ENDIF	
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(INT iPropertyID)
	INT i

	REPEAT MAX_OWNED_PROPERTIES i
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[i] = iPropertyID
			RETURN TRUE
		ENDIF	
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_BUSINESS_APARTMENT(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_BUS_HIGH_APT_1
		CASE PROPERTY_BUS_HIGH_APT_2
		CASE PROPERTY_BUS_HIGH_APT_3
		CASE PROPERTY_BUS_HIGH_APT_4
		CASE PROPERTY_BUS_HIGH_APT_5
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE 
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_PROPERTY_OF_SIZE(PLAYER_INDEX playerID, INT iPropertySize, BOOL bExcludeBusHighEnd = FALSE)
	INT i
	
	IF iPropertySize < PROP_SIZE_TYPE_2_GAR
	OR iPropertySize > PROP_SIZE_TYPE_OFFICE_GARAGE
		SCRIPT_ASSERT("DOES_PLAYER_OWN_PROPERTY_OF_SIZE - Passed invalid size!")
		RETURN FALSE
	ENDIF
	
	REPEAT MAX_MP_PROPERTIES i
		IF GET_PROPERTY_SIZE_TYPE(i) = iPropertySize
		AND (NOT IS_PROPERTY_BUSINESS_APARTMENT(i) OR NOT bExcludeBusHighEnd)
			IF DOES_PLAYER_OWN_THIS_PROPERTY(playerID, i)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(INT iPropertySize, BOOL bExcludeBusHighEnd = FALSE)
	INT i
	
	IF iPropertySize < PROP_SIZE_TYPE_2_GAR
	OR iPropertySize > PROP_SIZE_TYPE_OFFICE_GARAGE
		SCRIPT_ASSERT("DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE - Passed invalid size!")
		RETURN FALSE
	ENDIF
	
	REPEAT MAX_MP_PROPERTIES i
		IF GET_PROPERTY_SIZE_TYPE(i) = iPropertySize
		AND (NOT IS_PROPERTY_BUSINESS_APARTMENT(i) OR NOT bExcludeBusHighEnd)
			IF DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(i)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(INT iPropertyID)
	PLAYER_INDEX theBoss
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(),TRUE)
		IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
			IF IS_PROPERTY_CLUBHOUSE(iPropertyID)
				theBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF DOES_PLAYER_OWN_THIS_PROPERTY(theBoss,iPropertyID)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			IF IS_PROPERTY_OFFICE(iPropertyID)
				theBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
				IF DOES_PLAYER_OWN_THIS_PROPERTY(theBoss,iPropertyID)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE()

	IF IS_PLAYER_IN_BUNKER(PLAYER_ID())
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
	OR IS_PLAYER_IN_BUSINESS_HUB(PLAYER_ID())
	OR IS_PLAYER_IN_CASINO(PLAYER_ID())
	OR IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
	OR IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
	OR (GB_IS_BUSINESS_BATTLES_MISSION(GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()))
	AND GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
	AND GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID()))
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND() 
		RETURN TRUE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		IF GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID())
		OR GB_IS_PLAYER_PERMANENT_PARTICIPANT(PLAYER_ID())
			RETURN TRUE
		ENDIF
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FIXER_PAYPHONE
			RETURN TRUE
		ENDIF
	ENDIF	
	
	IF GB_IS_PLAYER_ON_GANG_BOSS_MISSION_OF_CATEGORY(PLAYER_ID(), GB_BOSS_MISSION_CATEGORY_NON_AFFILIATED)
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

PROC SET_PROPERTY_BLIP_AS_SHORT_RANGE(BLIP_INDEX BlipID,BOOL bSet)
	IF bSet
		IF NOT IS_BLIP_SHORT_RANGE(BlipID)
			SET_BLIP_AS_SHORT_RANGE(BlipID,TRUE)
		ENDIF
	ELSE
		IF IS_BLIP_SHORT_RANGE(BlipID) AND NOT ARE_TEMP_SHORT_RANGE_BLIPS_ACTIVE()
			SET_BLIP_AS_SHORT_RANGE(BlipID,FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC HANDLE_OFFICE_GARAGE_BLIPS(INT iBuilding)
	BOOL bOwned, bOrg, bFriend
	//INT iOfficeID
	INT i
	PLAYER_INDEX GangLeaderID = INVALID_PLAYER_INDEX()
	IF IS_BUILDING_OFFICE_PROPERTY(iBuilding)

		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
		AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
			GangLeaderID = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		ENDIF
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1] = PROPERTY_OFFICE_1_GARAGE_LVL1
		AND GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0)) = iBuilding
			bOwned = TRUE
		ELSE
//			IF iBuilding = MP_PROPERTY_OFFICE_BUILDING_1
//				iOfficeID = PROPERTY_OFFICE_1
//			ELIF iBuilding = MP_PROPERTY_OFFICE_BUILDING_2
//				iOfficeID = PROPERTY_OFFICE_2_BASE
//			ELIF iBuilding = MP_PROPERTY_OFFICE_BUILDING_3
//				iOfficeID = PROPERTY_OFFICE_3
//			ELIF iBuilding = MP_PROPERTY_OFFICE_BUILDING_4
//				iOfficeID = PROPERTY_OFFICE_4
//			ENDIF
			REPEAT NUM_NETWORK_PLAYERS i
				PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
				IF playerID != PLAYER_ID()
					IF IS_NET_PLAYER_OK(playerID,FALSE)
						IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1] = PROPERTY_OFFICE_1_GARAGE_LVL1
						AND GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0]) = iBuilding
							IF IS_NET_PLAYER_OK(GangLeaderID,FALSE)
								IF playerID = GangLeaderID
									bOrg = TRUE
								ELSE
									g_ClanDescription = GET_PLAYER_CREW(PLAYER_ID())
									g_otherClanDescription = GET_PLAYER_CREW(playerID)
									IF g_ClanDescription.Id = g_otherClanDescription.Id
										bFriend = TRUE
									ELSE
										g_GamerHandle = GET_GAMER_HANDLE_PLAYER(playerID)
										IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
											IF NETWORK_IS_FRIEND(g_GamerHandle)	
												bFriend = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		IF bOwned OR bOrg OR bFriend
			IF NOT DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iBuilding][2])
				mpPropMaintain.PropertyBlips[iBuilding][2] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[2])
				SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iBuilding][2],RADAR_TRACE_GARAGE)
				//	IF tempPropertyStruct.iType != PROPERTY_TYPE_GARAGE
					SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iBuilding][2],BLIPPRIORITY_LOW_MED)
				//	ELSE
				//		SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
				//	ENDIF
				IF bOwned
					SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iBuilding][2],"MP_PROP_OWN0")
					IF GangLeaderID = PLAYER_ID()
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
					ENDIF
					SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
				ELIF bOrg
					BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_GANGGAR")
						TEXT_LABEL_63 sBlipName
						sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
						IF IS_STRING_NULL_OR_EMPTY(sBlipName)
							sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
						ENDIF
						#IF FEATURE_GEN9_EXCLUSIVE
						IF IS_PLAYER_ON_MP_INTRO_MISSION()
							sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
							PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
						ENDIF
						#ENDIF
						ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
					END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iBuilding][2])
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
					SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
					IF NOT bOwned
						SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],TRUE)
					ENDIF
				ELIF bFriend
					SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iBuilding][2],"MP_PROP_OWN3")
					SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],TRUE)
					SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2],BLIP_COLOUR_BLUE)
				ENDIF
				
				PRINTLN("HANDLE_OFFICE_GARAGE_BLIPS: adding garage blip for property garage building #",iBuilding," Entrance #",2)
				PRINTLN("HANDLE_OFFICE_GARAGE_BLIPS: blip added at: ", tempPropertyStruct.vBlipLocation[2])
			ELSE
				IF bOwned
					IF GangLeaderID = PLAYER_ID()
						//IF GET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2]) != GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID())
							SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
						//ENDIF
					ELSE
						IF GET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2]) != BLIP_COLOUR_WHITE
							SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2], BLIP_COLOUR_WHITE)
						ENDIF
					ENDIF
					SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
				ELIF bOrg
					//IF GET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2]) != GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS())
						SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
					//ENDIF
					SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
					IF NOT bOwned
						SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],TRUE)
					ENDIF
				ELIF bFriend
					IF GET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2]) != BLIP_COLOUR_BLUE
						SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iBuilding][2], BLIP_COLOUR_BLUE)
					ENDIF
					SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iBuilding][2],TRUE)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iBuilding][2])
				REMOVE_BLIP(mpPropMaintain.PropertyBlips[iBuilding][2])
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL SHOULD_PROPERTY_BLIP_BE_HIDDEN_FOR_MP_INTRO()
	IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_GEN9_WINDFALL_CHAR)
		RETURN FALSE
	ENDIF
	IF IS_PLAYER_ON_MP_INTRO()
	OR (NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DONE_GEN9_WINDFALL_TEXT)
	AND NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet13, BI_FM_NMH13_G9_WINDFALL_TEXT_SENT))
		OWNED_PROPERTIES_IN_BUILDING ownedDetails
	
		IF mpPropMaintain.iSlowPropIDLoop = MP_PROPERTY_BUILDING_3//PROPERTY_HIGH_APT_8)
			RETURN TRUE
		ENDIF
		IF NOT DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpPropMaintain.iSlowPropIDLoop,ownedDetails,FALSE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL HAVE_PROPERTY_BLIPS_BEEN_SET_TO_HIDDEN()
	RETURN IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_HIDE_PROPERTY_BLIP)
ENDFUNC

PROC SET_PROPERTY_BLIP_HIDDEN(BOOL bHidden)

	IF bHidden
		IF NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_HIDE_PROPERTY_BLIP)
			SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_HIDE_PROPERTY_BLIP)
			PRINTLN("SET_PROPERTY_BLIP_HIDDEN -",bHidden)
		ENDIF
	ELSE
		IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_HIDE_PROPERTY_BLIP)
			CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_HIDE_PROPERTY_BLIP)
			PRINTLN("SET_PROPERTY_BLIP_HIDDEN -",bHidden)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_MP_PROPERTY_BLIPS()
	INT iIndex, j
	iIndex = mpPropMaintain.iSlowPropIDLoop
	INT iKnowOwnerState = -1
	FLOAT fDistance
	OWNED_PROPERTIES_IN_BUILDING ownedDetails
	FLOAT fDistanceToApt = -1
	
	BOOL bHasHeist
	//PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: SHOULD_PROPERTY_BLIPS_BE_HIDDEN() = ",SHOULD_PROPERTY_BLIPS_BE_HIDDEN())
	
	IF NOT CAN_PLAYER_USE_PROPERTY()
	OR GET_PROPERTY_BUILDING(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty) = mpPropMaintain.iSlowPropIDLoop
	OR (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY) 
	AND mpPropMaintain.iSlowPropIDLoop > 0
	AND GET_PROPERTY_BUILDING(globalPropertyEntryData.iPropertyEntered)= mpPropMaintain.iSlowPropIDLoop)
	OR IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
	#IF FEATURE_GEN9_EXCLUSIVE
	OR SHOULD_PROPERTY_BLIP_BE_HIDDEN_FOR_MP_INTRO()
	#ENDIF
	OR HAVE_PROPERTY_BLIPS_BEEN_SET_TO_HIDDEN()
	
		IF IS_MP_PROP_FLAG_SET(mpPropMaintain.iPropertySetBlipBS,iIndex)
			GET_BUILDING_EXTERIOR_DETAILS(tempPropertyStruct,iIndex)
			j = 0
			INT iLoop = tempPropertyStruct.iNumEntrances
			IF iLoop < 2
				iLoop = 2
			ENDIF
			REPEAT iLoop j
				IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
					REMOVE_BLIP(mpPropMaintain.PropertyBlips[iIndex][j])
				ENDIF
			ENDREPEAT
			PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: removing blips on mission for property building #",iIndex)
			CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertySetBlipBS,iIndex)
			CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
		ENDIF
	ELIF SHOULD_PROPERTY_BLIPS_BE_HIDDEN()
	AND (NOT DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(iIndex,ownedDetails)
	OR (IS_BUILDING_A_GANG_RELATED_PROPERTY(tempPropertyStruct.iIndex)
	AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)))
		j = 0
		REPEAT MAX_ENTRANCES j
			IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
				REMOVE_BLIP(mpPropMaintain.PropertyBlips[iIndex][j])
				CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertySetBlipBS,iIndex)
				CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
				PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: hiding blips for property Building #",iIndex," should be hidden and not owned")
			ENDIF
		ENDREPEAT
	ELSE
		IF Is_There_A_Heist_Available_In_Apartment()
		OR g_hasReplayBoardBeenUnlocked
			bHasHeist = TRUE
		ENDIF

		IF NOT IS_MP_PROP_FLAG_SET(mpPropMaintain.iPropertySetBlipBS,iIndex)
		OR IS_MP_PROP_FLAG_SET(mpPropMaintain.iPropertyUpdateBlipBS,iIndex)
			
			PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS:  getting properties for building ID: ", iIndex)
			GET_BUILDING_EXTERIOR_DETAILS(tempPropertyStruct,iIndex)
			PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: got details for property ID: ", tempPropertyStruct.iIndex)
			
			
			IF IS_MP_PROP_FLAG_SET(mpPropMaintain.iPropertyUpdateBlipBS,iIndex)
				j = 0
				REPEAT MAX_ENTRANCES j
					IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
						REMOVE_BLIP(mpPropMaintain.PropertyBlips[iIndex][j])
						CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
					ENDIF
				ENDREPEAT
				PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: IS_MP_PROP_FLAG_SET(mpPropMaintain.iPropertyUpdateBlipBS, #",iIndex)
			ENDIF
			
			HANDLE_OFFICE_GARAGE_BLIPS(iIndex)
			
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(iIndex,ownedDetails,DEFAULT , TRUE )
//			AND NOT (IS_PROPERTY_A_GANG_RELATED_PROPERTY(tempPropertyStruct.iIndex)
//			AND GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE))
				j = 0
//				#IF FEATURE_IMPORT_EXPORT
//				bOfficeGarageBlipAppears = DOES_PLAYER_OWN_THIS_OFFICE_GARAGE(PLAYER_ID(),PROPERTY_OFFICE_1_GARAGE_LVL1))
//				#ENDIF
				//PRINTLN("DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(iIndex,ownedDetails) = TRUE")
				REPEAT tempPropertyStruct.iNumEntrances j
						// don't add your own office is you are in a gang and not the boss.
						//but do add if boss and you own same thing
						
					IF NOT DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
						//PRINTLN("blip doesnt exist #",j)
						IF NOT SHOULD_BLIP_APPEAR_FOR_ENTRANCE(iIndex,j)
							//PRINTLN("blip shouldnt appear",j)
						ELSE
							IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_GARAGE
								IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,TRUE)
									
									fDistance = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[j])
									fDistanceToApt = -1
									IF tempPropertyStruct.entrance[0].iType = ENTRANCE_TYPE_HOUSE
										fDistanceToApt = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[0])
									ENDIF
									IF fDistance <= 100
									OR tempPropertyStruct.iType = PROPERTY_TYPE_GARAGE
									OR (fDistanceToApt != -1 AND fDistanceToApt <= 100)
										mpPropMaintain.PropertyBlips[iIndex][j] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[j])
										//SET_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
										SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_GARAGE)
										SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWN0")
										IF tempPropertyStruct.iType != PROPERTY_TYPE_GARAGE
											SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_LOW_MED)
										ELSE
											SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
										ENDIF
										PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: adding garage blip for owned property garage building #",iIndex," Entrance #",j)
										PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: blip added at: ", tempPropertyStruct.vBlipLocation[j])
										IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
											// Your boss
											IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
											ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
											ENDIF
										ENDIF
										SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
									ENDIF
									SET_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
								ENDIF
								
							ELIF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_HOUSE
								PRINTLN("entrance is house",j)
								mpPropMaintain.PropertyBlips[iIndex][j] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[j])
								//SET_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
								
								IF IS_PROPERTY_OFFICE(tempPropertyStruct.iIndex)
									SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_OFFICE)
									BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNOFF0")
										TEXT_LABEL_63 sBlipName
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
										AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
											sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: 4 getting office blip name: \"", sBlipName, "\" - office name")
											IF IS_STRING_NULL_OR_EMPTY(sBlipName)
												sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
												PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: 4 getting office blip name: \"", sBlipName, "\" - org name")
											ENDIF
										ELSE
											sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: 4 getting office blip name: \"", sBlipName, "\" - not gang member")
										ENDIF
										#IF FEATURE_GEN9_EXCLUSIVE
										IF IS_PLAYER_ON_MP_INTRO_MISSION()
											sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
										ENDIF
										#ENDIF
										g_sOfficeBlipName = sBlipName
										ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
									END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: 4 setting office blip name: ",sBlipName )
									//SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWNOFF0")
								ELIF IS_PROPERTY_CLUBHOUSE(tempPropertyStruct.iIndex)
									SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_BIKER_CLUBHOUSE)
									SET_BLIP_SCALE(mpPropMaintain.PropertyBlips[iIndex][j],1.2)
									BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNCLUB0")
										TEXT_LABEL_63 sBlipName
										IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
											sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											IF IS_STRING_NULL_OR_EMPTY(sBlipName)
												sBlipName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME(PLAYER_ID())
											ENDIF
										ELSE
											sBlipName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME(PLAYER_ID())
										ENDIF
										#IF FEATURE_GEN9_EXCLUSIVE
										IF IS_PLAYER_ON_MP_INTRO_MISSION()
											sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
										ENDIF
										#ENDIF
										g_sClubhouseBlipName = sBlipName
										ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
									END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: 4 setting Clubhouse blip name: ",sBlipName )
								ELSE
									SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_SAFEHOUSE)
									SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWN1")
								ENDIF
								SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
								SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
								IF bHasHeist
								AND GET_PROPERTY_SIZE_TYPE(tempPropertyStruct.iIndex) = PROP_SIZE_TYPE_LARGE_APT
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],HUD_COLOUR_GREEN)
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: setting blip green for heist")
								ELSE
									// url:bugstar:2817398 - Exec1 - Colour offices and warehouse blips the Org colour when you are a Bodyguard and change the access to your own offices (warehouse access is already blocked)
									// A property you own, blip doesn't exist
									IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
										IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
											SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
										ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
											SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
										ELSE
											SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_WHITE)
										ENDIF
									ELSE
										SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_WHITE)
									ENDIF
								ENDIF
								//GET_CORRECT_BLIP_PRIORITY(BP_CUFFED_OR_KEYS)
								PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: adding safehouse blip for owned property building #",iIndex," Entrance #",j)
								PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS <house>: blip added at: ", tempPropertyStruct.vBlipLocation[j])
								SET_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
							ENDIF
						ENDIF
					ELSE
						SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
						//PRINTLN("blip existS #",j)
						IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_HOUSE
							IF bHasHeist
							AND GET_PROPERTY_SIZE_TYPE(tempPropertyStruct.iIndex) = PROP_SIZE_TYPE_LARGE_APT
								IF GET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN)
									SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],HUD_COLOUR_GREEN)
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: setting blip green for heist -2")
								ENDIF
							ELSE
								// url:bugstar:2817398 - Exec1 - Colour offices and warehouse blips the Org colour when you are a Bodyguard and change the access to your own offices (warehouse access is already blocked)
								// A property you own, blip exists
								IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
									IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
										SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
									ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
										SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
									ELSE
										SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_WHITE)
									ENDIF
								ELSE
									SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_WHITE)
								ENDIF
							ENDIF
						ENDIF
						
						IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_HOUSE
							IF IS_PROPERTY_OFFICE(tempPropertyStruct.iIndex)
								TEXT_LABEL_63 sBlipName
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
								AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
									sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
									IF IS_STRING_NULL_OR_EMPTY(sBlipName)
										sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
									ENDIF
								ELSE
									sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
								ENDIF
								#IF FEATURE_GEN9_EXCLUSIVE
								IF IS_PLAYER_ON_MP_INTRO_MISSION()
									sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
								ENDIF
								#ENDIF
								
								IF NOT ARE_STRINGS_EQUAL(sBlipName,g_sOfficeBlipName)
									BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNOFF0")
										ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
									END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
									g_sOfficeBlipName = sBlipName
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 3 setting office blip name: ",sBlipName )
								ENDIF
							ELIF IS_PROPERTY_CLUBHOUSE(tempPropertyStruct.iIndex)
								TEXT_LABEL_63 sBlipName
								IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(),TRUE)
									sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
									IF IS_STRING_NULL_OR_EMPTY(sBlipName)
										sBlipName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME(PLAYER_ID())
									ENDIF
								ELSE
									sBlipName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME(PLAYER_ID())
								ENDIF
								#IF FEATURE_GEN9_EXCLUSIVE
								IF IS_PLAYER_ON_MP_INTRO_MISSION()
									sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
								ENDIF
								#ENDIF
								IF NOT ARE_STRINGS_EQUAL(sBlipName,g_sClubhouseBlipName)
									BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNCLUB0")
										ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
									END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
									g_sClubhouseBlipName = sBlipName
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 3 setting clubhouse blip name: ",sBlipName )
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ELSE
				//PRINTLN("DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(iIndex,ownedDetails) = FALSE")
				IF DOES_ANYONE_I_KNOW_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,iIndex,FALSE)
					REPEAT tempPropertyStruct.iNumEntrances j
						IF NOT DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
							IF NOT SHOULD_BLIP_APPEAR_FOR_ENTRANCE(iIndex,j)
						
							ELSE
								IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_GARAGE
									IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,TRUE)
										fDistance = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[j])
										fDistanceToApt = -1
										IF tempPropertyStruct.entrance[0].iType = ENTRANCE_TYPE_HOUSE
											fDistanceToApt = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[0])
										ENDIF
										IF fDistance <= 100
										OR tempPropertyStruct.iType = PROPERTY_TYPE_GARAGE
										OR (fDistanceToApt != -1 AND fDistanceToApt <= 100)
											mpPropMaintain.PropertyBlips[iIndex][j] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[j])
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_GARAGE)
											IF tempPropertyStruct.iType = PROPERTY_TYPE_GARAGE
												SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_LOW_MED)
											ELSE
												SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
											ENDIF
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
											//SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
											SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWN3")
											//SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],GET_CORRECT_BLIP_PRIORITY(BP_DEFAULT))
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 1 adding friend blip for garage of property building #",iIndex)
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: blip added at: ", tempPropertyStruct.vBlipLocation[j])
											IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
												// Your boss
												IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
													SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
												// Your friend
												ELSE
													SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
												ENDIF
											ELSE
												SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
											ENDIF
										ENDIF
									ENDIF
									SET_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
									
								ELIF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_HOUSE
									mpPropMaintain.PropertyBlips[iIndex][j] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[j])
									

									IF IS_PROPERTY_OFFICE(tempPropertyStruct.iIndex)
										IF DOES_MY_BOSS_OWN_PROPERTY_IN_BUILDING(iIndex,FALSE)
											TEXT_LABEL_63 sBlipName
											IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
											AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
												sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											ENDIF
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_OFFICE)
											IF IS_STRING_NULL_OR_EMPTY(sBlipName)
												SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWNOFF1")
												PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: setting office as friends apartment no org name??")
											ELSE
												//IF NOT ARE_STRINGS_EQUAL(sBlipName,g_sOfficeBlipName)
													BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNOFF0")
														ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
													END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
													PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 1 setting office blip name: ",sBlipName )
													g_sOfficeBlipName = sBlipName
												//ENDIF
											ENDIF
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
										ELSE
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: property office but boss doesn't own it ")
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_OFFICE)
											SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWNOFF1")
										ENDIF
									ELIF IS_PROPERTY_CLUBHOUSE(tempPropertyStruct.iIndex)
										IF DOES_MY_BOSS_OWN_PROPERTY_IN_BUILDING(iIndex,FALSE)
											TEXT_LABEL_63 sBlipName
											IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
												sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											ENDIF
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_BIKER_CLUBHOUSE)
											SET_BLIP_SCALE(mpPropMaintain.PropertyBlips[iIndex][j],1.2)
											IF IS_STRING_NULL_OR_EMPTY(sBlipName)
												SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWNCLUB1")
												PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: setting clubhouse as friends apartment no org name??")
											ELSE
												//IF NOT ARE_STRINGS_EQUAL(sBlipName,g_sClubhouseBlipName)
													BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNCLUB0")
														ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
													END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
													PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 1 setting clubhouse blip name: ",sBlipName )
													g_sClubhouseBlipName = sBlipName
												//ENDIF
											ENDIF
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
										ELSE
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: property clubhouse but boss doesn't own it ")
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_BIKER_CLUBHOUSE)
											SET_BLIP_SCALE(mpPropMaintain.PropertyBlips[iIndex][j],1.2)
											SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWNCLUB1")
										ENDIF
									ELSE
										SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
										SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_SAFEHOUSE)
										SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWN2")
									ENDIF
									
									// url:bugstar:2817398 - Exec1 - Colour offices and warehouse blips the Org colour when you are a Bodyguard and change the access to your own offices (warehouse access is already blocked)
									// - Someone you know who owns the property
									IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
										// Your boss
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
											SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
										// Your friend
										ELSE
											SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
										ENDIF
									ELSE
										SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
									ENDIF
									
									SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
									
									SET_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 2 adding friend blip for house of property building #",iIndex)
									PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: blip added at: ", tempPropertyStruct.vBlipLocation[j])
								ENDIF
							ENDIF
						ELSE
							SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
							PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: someone I know owns property building but already blipped #",iIndex, " Entrance = ",j)
						ENDIF
					ENDREPEAT
				ELSE
					PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: no one I know owns property building #",iIndex)
				ENDIF
			ENDIF
			SET_MP_PROP_FLAG(mpPropMaintain.iPropertySetBlipBS,iIndex)
			CLEAR_MP_PROP_FLAG(mpPropMaintain.iPropertyUpdateBlipBS,iIndex)
		ELSE
			
			IF IS_MP_PROP_FLAG_SET(mpPropMaintain.iPropertyAddBlipBS,iIndex)
				GET_BUILDING_EXTERIOR_DETAILS(tempPropertyStruct,iIndex)
				HANDLE_OFFICE_GARAGE_BLIPS(iIndex)
				IF iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_1))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_2))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_3))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_4))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_5))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_6))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_7))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_8))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_9))
				OR iIndex = GET_PROPERTY_BUILDING(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_APT_10))  //CDM PROPERTY STAT UPDATE //OLD PROPERTIES
				OR IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
					j = 0
					REPEAT tempPropertyStruct.iNumEntrances j
						IF NOT SHOULD_BLIP_APPEAR_FOR_ENTRANCE(iIndex,j)
						
						ELSE
							IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_GARAGE
								fDistance = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[j])
								fDistanceToApt = -1
								IF tempPropertyStruct.entrance[0].iType = ENTRANCE_TYPE_HOUSE
									fDistanceToApt = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[0])
								ENDIF
								IF fDistance <= 100
								OR tempPropertyStruct.iType = PROPERTY_TYPE_GARAGE
								OR (fDistanceToApt != -1 AND fDistanceToApt <= 100)
									IF NOT DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
										IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_GARAGE
											mpPropMaintain.PropertyBlips[iIndex][j] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[j])
											//SET_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_GARAGE)
											SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWN0")
											IF tempPropertyStruct.iType != PROPERTY_TYPE_GARAGE
												SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_LOW_MED)
											ELSE
												SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
											ENDIF
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: adding garage blip for owned property building #",iIndex," Entrance #",j)
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: blip added at: ", tempPropertyStruct.vBlipLocation[j])
											IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
												IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
													SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
												ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
													SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
											IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
											ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
											ENDIF
										ENDIF
										SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
									ENDIF
								ELSE
									IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
										REMOVE_BLIP(mpPropMaintain.PropertyBlips[iIndex][j])
										PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: removing garage blip for owned property building #",iIndex," Entrance #",j)
									ENDIF
								ENDIF
							ELIF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_HOUSE
								IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
									SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],SHOULD_ALL_PROPERTY_BLIPS_BE_SHORT_RANGE())
									IF bHasHeist
									AND GET_PROPERTY_SIZE_TYPE(tempPropertyStruct.iIndex) = PROP_SIZE_TYPE_LARGE_APT
										IF GET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN)
											SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],HUD_COLOUR_GREEN)
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: setting blip green for heist-3")
										ENDIF
									ELSE
									
										// url:bugstar:2817398 - Exec1 - Colour offices and warehouse blips the Org colour when you are a Bodyguard and change the access to your own offices (warehouse access is already blocked)
										// Adding a blip for your own property
										IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
											IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
											ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
											ELSE
												SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_WHITE)
											ENDIF
										ELSE
											SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_WHITE)
										ENDIF
									ENDIF
									IF IS_PROPERTY_OFFICE(tempPropertyStruct.iIndex)
										TEXT_LABEL_63 sBlipName
										IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE)
										AND NOT GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
											sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											//PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 4 getting office blip name: \"", sBlipName, "\" - org name")
											IF IS_STRING_NULL_OR_EMPTY(sBlipName)
												sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
												//PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 4 getting office blip name: \"", sBlipName, "\" - office name")
											ENDIF
										ELSE
											sBlipName = GB_GET_PLAYER_OFFICE_NAME(PLAYER_ID())
											//PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 4 getting office blip name: \"", sBlipName, "\" - not gang member")
										ENDIF
										#IF FEATURE_GEN9_EXCLUSIVE
										IF IS_PLAYER_ON_MP_INTRO_MISSION()
											sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
										ENDIF
										#ENDIF
										IF NOT ARE_STRINGS_EQUAL(sBlipName,g_sOfficeBlipName)
											BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNOFF0")
												ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
											END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 4 setting office blip name: \"", sBlipName, "\" replacing \"", g_sOfficeBlipName, "\"")
											g_sOfficeBlipName = sBlipName
										ENDIF
									ELIF IS_PROPERTY_CLUBHOUSE(tempPropertyStruct.iIndex)
										TEXT_LABEL_63 sBlipName
										IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID(), TRUE)
											sBlipName = GB_GET_PLAYER_ORGANIZATION_NAME(PLAYER_ID() #IF IS_DEBUG_BUILD , TRUE #ENDIF)
											IF IS_STRING_NULL_OR_EMPTY(sBlipName)
												sBlipName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME(PLAYER_ID())
											ENDIF
										ELSE
											sBlipName = GB_GET_PLAYER_MC_CLUBHOUSE_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS player not a biker gang member using local")
										ENDIF
										#IF FEATURE_GEN9_EXCLUSIVE
										IF IS_PLAYER_ON_MP_INTRO_MISSION()
											sBlipName = GET_DEFAULT_GANG_NAME(PLAYER_ID())
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: player on intro mission using default")
										ENDIF
										#ENDIF
										IF NOT ARE_STRINGS_EQUAL(sBlipName,g_sClubhouseBlipName)
											BEGIN_TEXT_COMMAND_SET_BLIP_NAME("MP_PROP_OWNCLUB0")
												ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sBlipName)
											END_TEXT_COMMAND_SET_BLIP_NAME(mpPropMaintain.PropertyBlips[iIndex][j])
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 4 setting clubhouse blip name: ",sBlipName )
											g_sClubhouseBlipName = sBlipName
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELSE
					j = 0
					REPEAT tempPropertyStruct.iNumEntrances j
						IF SHOULD_BLIP_APPEAR_FOR_ENTRANCE(iIndex,j)
						
						ELSE
							IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_GARAGE
							AND NOT IS_BUILDING_OFFICE_PROPERTY(iIndex)
								fDistance = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[j])
								fDistanceToApt = -1
								IF tempPropertyStruct.entrance[0].iType = ENTRANCE_TYPE_HOUSE
									fDistanceToApt = VDIST(GET_PLAYER_COORDS(PLAYER_ID()),tempPropertyStruct.vBlipLocation[0])
								ENDIF
								IF (fDistance <= 100
								OR tempPropertyStruct.iType = PROPERTY_TYPE_GARAGE
								OR (fDistanceToApt != -1 AND fDistanceToApt <= 100))
								AND DOES_ANYONE_I_KNOW_OWN_PROPERTY_IN_BUILDING(iKnowOwnerState,iIndex,FALSE)
									IF NOT DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
										IF tempPropertyStruct.entrance[j].iType = ENTRANCE_TYPE_GARAGE
											mpPropMaintain.PropertyBlips[iIndex][j] = CREATE_BLIP_FOR_COORD(tempPropertyStruct.vBlipLocation[j])
											SET_BLIP_SPRITE(mpPropMaintain.PropertyBlips[iIndex][j],RADAR_TRACE_GARAGE)
											//SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
											IF tempPropertyStruct.iType != PROPERTY_TYPE_GARAGE
												SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_LOW_MED)
											ELSE
												SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],BLIPPRIORITY_MED)
											ENDIF
											SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
											SET_BLIP_NAME_FROM_TEXT_FILE(mpPropMaintain.PropertyBlips[iIndex][j],"MP_PROP_OWN3")
											//SET_BLIP_PRIORITY(mpPropMaintain.PropertyBlips[iIndex][j],GET_CORRECT_BLIP_PRIORITY(BP_DEFAULT))
											SET_MP_PROP_FLAG(mpPropMaintain.iPropertyAddBlipBS,iIndex)
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: 3 adding friend blip for garage of property building #",iIndex)
											PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: blip added at: ", tempPropertyStruct.vBlipLocation[j])
											IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
												IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
													SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
												ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
													SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
												ELSE
													SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
												ENDIF
											ELSE
												SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
											ENDIF
										ENDIF
									ELSE DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
										IF IS_PLAYER_IN_GANG_RELATED_TO_THIS_PROPERTY(tempPropertyStruct.iIndex)
											IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(PLAYER_ID()))
											ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
												SET_BLIP_COLOUR_FROM_HUD_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j], GB_GET_PLAYER_GANG_HUD_COLOUR(GB_GET_LOCAL_PLAYER_GANG_BOSS()))
											ELSE
												SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
											ENDIF
										ELSE
											SET_BLIP_COLOUR(mpPropMaintain.PropertyBlips[iIndex][j],BLIP_COLOUR_BLUE)
										ENDIF
										//SET_PROPERTY_BLIP_AS_SHORT_RANGE(mpPropMaintain.PropertyBlips[iIndex][j],TRUE)
									ENDIF
								ELSE
									IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iIndex][j])
										REMOVE_BLIP(mpPropMaintain.PropertyBlips[iIndex][j])
										PRINTLN("MAINTAIN_MP_PROPERTY_BLIPS: removing friend blip for garage of property building #",iIndex)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	mpPropMaintain.iSlowPropIDLoop++
	IF mpPropMaintain.iSlowPropIDLoop >= MP_PROPERTY_BUILDING_MAX
		mpPropMaintain.iSlowPropIDLoop = 1
	ENDIF
	
ENDPROC


FUNC BOOL SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
	IF IS_PLAYER_ON_MISSION(PLAYER_ID())
	OR IS_PLAYER_ON_ANY_RACE(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID()) 
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FM_GANGOPS
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_SURVIVAL
	OR IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())	
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_COST_OF_MECHANIC()
	
	INT iChargeTimer = MECHANIC_CHARGE_INTERVAL
	INT i
	
	#IF IS_DEBUG_BUILD
	IF g_iMechanicFeeTime > 0 
		iChargeTimer = g_iMechanicFeeTime
	ENDIF
	#ENDIF
	
	IF HAS_INTRO_TO_APARTMENTS_CUTSCENE_BEEN_DONE() // 1424616
	OR HAS_PURCHASE_FIRST_GARAGE_CUTSCENE_BEEN_DONE()
		IF NOT HAS_NET_TIMER_STARTED(mpPropMaintain.mechanicChargeTimer)
			START_NET_TIMER(mpPropMaintain.mechanicChargeTimer,TRUE)
			PRINTLN("MAINTAIN_COST_OF_MECHANIC(): Starting timer")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(mpPropMaintain.mechanicChargeTimer,iChargeTimer,TRUE)
			AND SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
			#IF IS_DEBUG_BUILD
			OR g_bChargeOwnershipFeesNow
			#ENDIF
				INT iCharge
				
				REPEAT MAX_OWNED_PROPERTIES i
					IF GET_OWNED_PROPERTY(i) > 0
					AND NOT IS_PROPERTY_SLOT_NO_PER_VEHS_PROPERTY(i)
						iCharge += g_sMPTunables.iMECHANIC_DAILY_FEE
					ENDIF
				ENDREPEAT
				PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC - Cost after checking owned garages: ", iCharge)
				
				//Add aircraft fees
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdHangarData.eHangar != HANGAR_ID_INVALID
					iCharge += GET_HANGAR_AIRCRAFT_CHARGES_FOR_MECHANIC_WAGE()
				ENDIF
				PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC - Cost after checking owned aircraft: ", iCharge)
				
				IF iCharge > 0
					IF NETWORK_CAN_SPEND_MONEY(iCharge, FALSE, TRUE, FALSE)
						
						IF SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
							PRINT_TICKER_WITH_INT("MP_PROP_MECH",iCharge)
						ELSE
							PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC: bypassing ticker for missions") //2194622
						ENDIF

						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_MECHANIC_WAGE, iCharge, iTransactionID, FALSE, TRUE)
						ELSE
							NETWORK_PAY_EMPLOYEE_WAGE(iCharge, FALSE, TRUE)
						ENDIF
							
						CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_MECHANIC)
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC: Player paid mechanic un-blocking calls. COST: ", iCharge)
					ELSE
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC: Player failed to pay mechanic blocking calls. COST: ", iCharge)
						SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_MECHANIC)
					ENDIF
				ENDIF
				RESET_NET_TIMER(mpPropMaintain.mechanicChargeTimer)
			#IF IS_DEBUG_BUILD
			ELSE
				IF g_bOutOwnershipChargeDetails
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC mechanic start time: ", GET_TIME_AS_STRING(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())))
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC current time: ", GET_TIME_AS_STRING(mpPropMaintain.mechanicChargeTimer.Timer))
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_COST_OF_MECHANIC current time difference: ",(ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), mpPropMaintain.mechanicChargeTimer.Timer))))
				ENDIF
			#ENDIF
			ENDIF
		
		ENDIF
	ENDIF
ENDPROC

ENUM OWNERSHIP_COST_PROPERTY
	//Apartments
	OC_PROP_SMALL_APT,
	OC_PROP_MED_APT,
	OC_PROP_LARGE_APT,
	
	OC_PROP_YACHT,
	OC_PROP_OFFICE,
	OC_PROP_CASINO_PENTHOUSE,
	
	OC_PROP_WAREHOUSE,
	OC_PROP_BIKER_FACTORY,
	OC_PROP_BUNKER,
	OC_PROP_HANGAR,
	
	OC_PROP_DEFUNCT_BASE,
	OC_PROP_NIGHTCLUB,
	OC_PROP_ARCADE,
	OC_PROP_SUBMARINE,
	OC_PROP_AUTO_SHOP,
	OC_PROP_FIXER_HQ
ENDENUM

FUNC INT GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OWNERSHIP_COST_PROPERTY eProperty)	
	SWITCH eProperty
		CASE OC_PROP_HANGAR				RETURN GET_TOTAL_UTILITY_CHARGE_FOR_HANGAR()			BREAK
		CASE OC_PROP_DEFUNCT_BASE		RETURN GET_TOTAL_UTILITY_CHARGE_FOR_DEFUNCT_BASE()		BREAK
		CASE OC_PROP_SUBMARINE			RETURN g_sMPTunables.iHS4_SUBMARINE_UTILITY_COST		BREAK
		CASE OC_PROP_CASINO_PENTHOUSE	RETURN GET_OWNED_CASINO_APT_UTILITY_COST(PLAYER_ID())	BREAK
		CASE OC_PROP_YACHT				RETURN g_sMPTunables.iYACHT_UTILITIES_COST				BREAK
		CASE OC_PROP_SMALL_APT 			RETURN g_sMPTunables.iLowApartmentUtilityCost			BREAK
		CASE OC_PROP_MED_APT  			RETURN g_sMPTunables.iMidApartmentUtilityCost			BREAK
		CASE OC_PROP_LARGE_APT  		RETURN g_sMPTunables.iHighApartmentUtilityCost			BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC INT GET_BUSINESS_CHARGES_FOR_PROPERTY(OWNERSHIP_COST_PROPERTY eProperty)
	
	SWITCH eProperty
		CASE OC_PROP_HANGAR			RETURN GET_TOTAL_UTILITY_CHARGE_FOR_HANGAR()		BREAK
		CASE OC_PROP_AUTO_SHOP		RETURN g_sMPTunables.iAUTO_SHOP_UTILITY_COST		BREAK
		CASE OC_PROP_ARCADE			RETURN g_sMPTunables.iARCADE_UTILITY_COST			BREAK
		CASE OC_PROP_OFFICE			RETURN g_sMPTunables.iEXEC1_OFFICE_FEES				BREAK
		CASE OC_PROP_FIXER_HQ		RETURN g_sMPTunables.iFIXER_HQ_UTILITY_COST			BREAK
		CASE OC_PROP_WAREHOUSE		RETURN GET_TOTAL_UTILITY_CHARGE_FOR_WAREHOUSES()	BREAK
		CASE OC_PROP_NIGHTCLUB
			RETURN (GET_OWNED_BUSINESS_HUB_UTILITY_COST(PLAYER_ID()) + GET_OWNED_NIGHTCLUB_STAFF_COST(PLAYER_ID()))
		BREAK
		CASE OC_PROP_BUNKER
			RETURN (GET_OWNED_FACTORY_UTILITY_COST(BUNKER_SAVE_SLOT) + GET_OWNED_FACTORY_STAFF_COST(BUNKER_SAVE_SLOT))
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

PROC CLEAR_OUT_PROPERTY_UTILITIES_STRUCT()
	APT_UTIL_BREAKDOWN sCleanStruct
	g_sPropertyCostBreakdown = sCleanStruct
ENDPROC

PROC CLEAR_OUT_BUSINESS_COSTS_STRUCT()
	BUSINESS_UTIL_BREAKDOWN sCleanStruct
	g_sBusinessCostBreakdown = sCleanStruct
ENDPROC

PROC CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES()
	INT i, iHouseStat, iMyPropertySize, iCharge, iValueToAdd
	
	CLEAR_OUT_PROPERTY_UTILITIES_STRUCT()
	
	//Standard properties
	REPEAT MAX_OWNED_PROPERTIES i
		iHouseStat 		= GET_OWNED_PROPERTY(i)
		iMyPropertySize = GET_PROPERTY_SIZE_TYPE(iHouseStat)
		
		IF iMyPropertySize = PROP_SIZE_TYPE_LARGE_APT
			iValueToAdd = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_LARGE_APT)
			g_sPropertyCostBreakdown.iHighAptFees += iValueToAdd
			iCharge += iValueToAdd
		ELIF iMyPropertySize = PROP_SIZE_TYPE_MED_APT
			iValueToAdd = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_MED_APT)
			g_sPropertyCostBreakdown.iMedAptFees += iValueToAdd
			iCharge += iValueToAdd
		ELIF iMyPropertySize = PROP_SIZE_TYPE_SMALL_APT
			iValueToAdd = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_SMALL_APT)
			g_sPropertyCostBreakdown.iLowAptFees += iValueToAdd
			iCharge += iValueToAdd
		ENDIF
	ENDREPEAT
	
	PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES - Total charge after adding apartments: ", iCharge)
	
	//Yacht
	IF GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(PLAYER_ID()) >= 0
	AND NOT g_sMPTunables.bYACHT_DISABLE_UTILITIES 
		g_sPropertyCostBreakdown.iYachtFees = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_YACHT)
		iCharge += g_sPropertyCostBreakdown.iYachtFees
		PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES - Total charge after adding Yacht charges: ", iCharge)
	ENDIF
	
	//Facility
	IF DOES_PLAYER_OWN_A_DEFUNCT_BASE(PLAYER_ID())
		g_sPropertyCostBreakdown.iFacilityFees = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_DEFUNCT_BASE)
		iCharge += g_sPropertyCostBreakdown.iFacilityFees
		PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES - Total charge after adding Defunct base charges: ", iCharge)
	ENDIF
	
	//Submarine
	IF DOES_PLAYER_OWN_A_SUBMARINE(PLAYER_ID())
		g_sPropertyCostBreakdown.iKosatkaFees = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_SUBMARINE)
		iCharge += g_sPropertyCostBreakdown.iKosatkaFees
		PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES - Total charge after adding Submarine charges: ", iCharge)
	ENDIF
	
	//Casino APT
	IF DOES_PLAYER_OWN_A_CASINO_APARTMENT(PLAYER_ID())
		g_sPropertyCostBreakdown.iPenthouseFees = GET_UTILITIES_CHARGE_FOR_RESIDENTIAL_PROPERTY(OC_PROP_CASINO_PENTHOUSE)
		icharge += g_sPropertyCostBreakdown.iPenthouseFees
		PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES - Total charge after adding casino penthouse charges: ", iCharge)
	ENDIF
	
	IF iCharge > 0
		IF NETWORK_CAN_SPEND_MONEY(iCharge,FALSE,TRUE,FALSE)
			IF SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
				PRINT_TICKER_WITH_INT("MP_PROP_UTIL", iCharge)
			ELSE
				PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES: bypassing ticker for missions") //2194622
			ENDIF

			IF USE_SERVER_TRANSACTIONS()									
				IF iCharge > 0
					INT iTransactionID
					TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_UTILITY_BILLS, iCharge, iTransactionID, FALSE,TRUE)
				ENDIF
			ELSE
				PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES: iCharge: ",iCharge)
				
				IF iCharge > 0
					NETWORK_SPEND_APARTMENT_UTILITIES(iCharge, FALSE, TRUE, g_sPropertyCostBreakdown)
				ENDIF
			ENDIF
			CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_UTILITIES)
			PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES: (INIT CHECK) player has paid for their utilities")
		ELSE
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_UTILITIES)
			SET_BIT(g_iFmNmhBitSet8, BI_FM_NMH8_NO_AFFORD_UTILITY)
			PRINTLN("[PROP_OWN_FEES] CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES: (INIT CHECK)  player has NOT paid for their utilities")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PROPERTY_UTILITY_CHARGES()
	//INT iBankAmount, iWalletAmount
	INT iChargeTimer = UTILITY_CHARGE_INTERVAL
	
	#IF IS_DEBUG_BUILD
	IF g_iApartmentFeeTime > 0 
		iChargeTimer = g_iApartmentFeeTime
	ENDIF
	#ENDIF
	
	IF HAS_INTRO_TO_APARTMENTS_CUTSCENE_BEEN_DONE() // 1424616
	OR HAS_PURCHASE_FIRST_GARAGE_CUTSCENE_BEEN_DONE()
	OR HAS_OFFICE_INTRO_CUTSCENE_BEEN_DONE()
		#IF IS_DEBUG_BUILD
		IF NOT g_bDemo
		#ENDIF
			IF NOT HAS_NET_TIMER_STARTED(mpPropMaintain.utilitiesChargeTimer)
				START_NET_TIMER(mpPropMaintain.utilitiesChargeTimer,TRUE)
				PRINTLN("[PROP_OWN_FEES] MAINTAIN_PROPERTY_UTILITY_CHARGES: Starting timer")
			ELSE
				IF NOT g_bCheckedUtilities
				AND SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
				
					g_bCheckedUtilities = TRUE
					
					IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_UTILITIES)
						CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES()
					ENDIF
					
					RESET_NET_TIMER(mpPropMaintain.utilitiesChargeTimer)
				ENDIF
				
				IF HAS_NET_TIMER_EXPIRED(mpPropMaintain.utilitiesChargeTimer,iChargeTimer,TRUE)
				AND SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
				#IF IS_DEBUG_BUILD
				OR g_bChargeOwnershipFeesNow
				#ENDIF
					
					g_bCheckedUtilities = TRUE
					
					CALCULATE_AND_CHARGE_PLAYER_FOR_PROPERTY_UTILITIES()
					
					RESET_NET_TIMER(mpPropMaintain.utilitiesChargeTimer)					
					
				#IF IS_DEBUG_BUILD
				ELSE
					IF g_bOutOwnershipChargeDetails
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_PROPERTY_UTILITY_CHARGES Utility start time: ", GET_TIME_AS_STRING(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())))
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_PROPERTY_UTILITY_CHARGES current time: ", GET_TIME_AS_STRING(mpPropMaintain.utilitiesChargeTimer.Timer))
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_PROPERTY_UTILITY_CHARGES current time difference: ",(ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), mpPropMaintain.utilitiesChargeTimer.Timer))))
					ENDIF
				#ENDIF
				ENDIF
			
			ENDIF
		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
ENDPROC

FUNC INT CALCULATE_BIKER_BUSINESS_CHARGES()
	FACTORY_ID eFactoryID	
	INT i, iSumUtilityCost, iSumStaffCost
				
	REPEAT MAX_OWNED_BIKER_FACTORIES i
		eFactoryID = GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), i)
		
		IF CHECK_FACTORY_ID(eFactoryID #IF IS_DEBUG_BUILD , TRUE #ENDIF)
			INT iUtilityCharge = GET_OWNED_FACTORY_UTILITY_COST(i)
			INT iStaffCharge = GET_OWNED_FACTORY_STAFF_COST(i)
			iSumUtilityCost = iSumUtilityCost + iUtilityCharge
			iSumStaffCost = iSumStaffCost + iStaffCharge
			
			SWITCH GET_FACTORY_TYPE_FROM_FACTORY_ID(eFactoryID)
				CASE FACTORY_TYPE_CRACK			g_sBusinessCostBreakdown.iCocaineFees 	+= (iUtilityCharge + iStaffCharge)	BREAK
				CASE FACTORY_TYPE_WEED			g_sBusinessCostBreakdown.iWeedFees 		+= (iUtilityCharge + iStaffCharge)	BREAK
				CASE FACTORY_TYPE_FAKE_IDS		g_sBusinessCostBreakdown.iDocForgeFees 	+= (iUtilityCharge + iStaffCharge)	BREAK
				CASE FACTORY_TYPE_METH			g_sBusinessCostBreakdown.iMethFees 		+= (iUtilityCharge + iStaffCharge)	BREAK
				CASE FACTORY_TYPE_FAKE_MONEY	g_sBusinessCostBreakdown.iFakeCashFees 	+= (iUtilityCharge + iStaffCharge)	BREAK
			ENDSWITCH
			
			PRINTLN("CALCULATE_BIKER_BUSINESS_CHARGES - Charges for factory ", eFactoryID, " - charging ", iUtilityCharge, " for utilities and ", iStaffCharge, " for staff.")
		ENDIF
	ENDREPEAT
	
	RETURN (iSumUtilityCost + iSumStaffCost)
ENDFUNC

PROC MAINTAIN_BUSINESS_CHARGES()
	
	INT iTotalCost
	INT iChargeTimer = BUSINESS_CHARGE_INTERVAL
	
	#IF IS_DEBUG_BUILD
	IF g_iBusinessFeeTime > 0 
		iChargeTimer = g_iBusinessFeeTime
	ENDIF
	#ENDIF
	
	IF HAS_INTRO_TO_APARTMENTS_CUTSCENE_BEEN_DONE() // 1424616
	OR HAS_PURCHASE_FIRST_GARAGE_CUTSCENE_BEEN_DONE()
		IF NOT HAS_NET_TIMER_STARTED(mpPropMaintain.businessChargeTimer)
			START_NET_TIMER(mpPropMaintain.businessChargeTimer, TRUE)
			PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES: Starting timer")
		ELSE
			IF HAS_NET_TIMER_EXPIRED(mpPropMaintain.businessChargeTimer, iChargeTimer, TRUE)
			AND SHOULD_PROPERTY_CHARGES_TICKER_DISPLAY()
			#IF IS_DEBUG_BUILD
			OR g_bChargeOwnershipFeesNow
			#ENDIF
				PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES: Timer expired, time to calculate the charges for all owned businesses.")
				
				CLEAR_OUT_BUSINESS_COSTS_STRUCT()
				
				//Biker factories
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_BIKER_GANG()
					// charges don't apply if you're not boss (url:bugstar:3015019)
					iTotalCost += CALCULATE_BIKER_BUSINESS_CHARGES()
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Biker business charges: ", iTotalCost)
				ENDIF
				
				//Hangar
				IF DOES_PLAYER_OWN_A_HANGER(PLAYER_ID())
					g_sBusinessCostBreakdown.iHangarFees = GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_HANGAR)
					iTotalCost += g_sBusinessCostBreakdown.iHangarFees
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Hangar charges: ", iTotalCost)
				ENDIF
				
				//Auto shop
				IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
					g_sBusinessCostBreakdown.iAutoShopFees = GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_AUTO_SHOP)
					iTotalCost += g_sBusinessCostBreakdown.iAutoShopFees
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Auto shop charges: ", iTotalCost)
				ENDIF
				
				//Arcade
				IF DOES_PLAYER_OWN_AN_ARCADE_PROPERTY(PLAYER_ID())
					IF HAS_LOCAL_PLAYER_COMPLETE_FULL_ARCADE_SETUP()
						g_sBusinessCostBreakdown.iArcadeFees = GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_ARCADE)
						iTotalCost += g_sBusinessCostBreakdown.iArcadeFees
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Arcade charges: ", iTotalCost)
					ELSE
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - No arcade charge for not setup property")
					ENDIF
				ENDIF
				
				//Nightclub
				IF DOES_PLAYER_OWN_A_NIGHTCLUB(PLAYER_ID())					
					g_sBusinessCostBreakdown.iNightclubFees 	= GET_OWNED_BUSINESS_HUB_UTILITY_COST(PLAYER_ID())
					g_sBusinessCostBreakdown.iNightclubStaff	= GET_OWNED_NIGHTCLUB_STAFF_COST(PLAYER_ID())
					iTotalCost += g_sBusinessCostBreakdown.iNightclubFees
					iTotalCost += g_sBusinessCostBreakdown.iNightclubStaff
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Nightclub charges: ", iTotalCost)
				ENDIF
				
				//Bunker
				IF GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
				AND DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
				AND IS_FACTORY_IN_AN_ACTIVE_PRODUCTION_STAGE(PLAYER_ID(), BUNKER_SAVE_SLOT)
					// charges dont' apply if you're not boss (url:bugstar:3015019)
					g_sBusinessCostBreakdown.iBunkerFees = GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_BUNKER)
					iTotalCost += g_sBusinessCostBreakdown.iBunkerFees
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Bunker charges: ", iTotalCost)
				ENDIF
				
				//Office
				IF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
					//Property fee
					g_sBusinessCostBreakdown.iExecOfficeFees = GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_OFFICE)
					iTotalCost += g_sBusinessCostBreakdown.iExecOfficeFees
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Office charges: ", iTotalCost)
					//Exec assistant fee
					g_sBusinessCostBreakdown.iExecAssistantFees = g_sMPTunables.iEXEC1_PA_FEES
					iTotalCost += g_sMPTunables.iEXEC1_PA_FEES
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding PA charges: ", iTotalCost)
				ENDIF
				
				//Warehouses
				iTotalCost += GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_WAREHOUSE)
				PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Warehouse charges: ", iTotalCost)
				
				//Fixer HQ
				IF DOES_PLAYER_OWN_A_FIXER_HQ(PLAYER_ID())
					g_sBusinessCostBreakdown.iFixerAgencyFees = GET_BUSINESS_CHARGES_FOR_PROPERTY(OC_PROP_FIXER_HQ)
					iTotalCost += g_sBusinessCostBreakdown.iFixerAgencyFees
					PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES - Total charge after adding Fixer HQ charges: ", iTotalCost)
				ENDIF
				
				IF iTotalCost > 0
					IF GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) > iTotalCost
					OR NETWORK_CAN_SPEND_MONEY(iTotalCost, FALSE, TRUE, FALSE) //Use OR to catch cash values over SCRIPT_MAX_INT32.
						PRINT_TICKER_WITH_INT("MP_BUS_CHARGE", iTotalCost)
								
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_BUSINESS_EXPENSES, iTotalCost, iTransactionID, FALSE, TRUE)
						ELSE
							NETWORK_SPEND_BUSINESS_PROPERTY_FEES(iTotalCost, FALSE, TRUE, g_sBusinessCostBreakdown)
						ENDIF
						
						CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS, MP_SAVE_GENERAL_BS_NOT_PAID_BUSINESS_PROPERTY_STAFF_AND_UTILITIES)
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES: Paying total business costs of ", iTotalCost)
						
					ELSE
						SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS, MP_SAVE_GENERAL_BS_NOT_PAID_BUSINESS_PROPERTY_STAFF_AND_UTILITIES)
						PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES: Not enough money to pay all the charges!")
					ENDIF
				ENDIF
				
				RESET_NET_TIMER(mpPropMaintain.businessChargeTimer)
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF g_bOutOwnershipChargeDetails
			PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES() Current time: ", GET_TIME_AS_STRING(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER())))
			PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES() Will charge at time: ", GET_TIME_AS_STRING(mpPropMaintain.businessChargeTimer.Timer))
			PRINTLN("[PROP_OWN_FEES] MAINTAIN_BUSINESS_CHARGES() Time difference: ",(ABSI(GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), mpPropMaintain.businessChargeTimer.Timer))))
		ENDIF
	#ENDIF
ENDPROC

//used to check if local player is physically inside the property
FUNC BOOL IS_PLAYER_WITHIN_PROPERTY_INTERIOR(MP_PROPERTY_STRUCT& currentPropertyDetails, INT iEntranceType , FLOAT extendRadius = 0.0 )
	//PRINTLN("IS_PLAYER_WITHIN_PROPERTY_INTERIOR: running next check with type: ",iEntranceType)
	IF iEntranceType = ENTRANCE_TYPE_GARAGE
	AND NOT IS_PROPERTY_CLUBHOUSE(currentPropertyDetails.iIndex)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),currentPropertyDetails.garage.vMidPoint) <= 30
	
			IF IS_POINT_IN_ANGLED_AREA( GET_PLAYER_COORDS(PLAYER_ID()), currentPropertyDetails.garage.Bounds.vPos1, 
														currentPropertyDetails.garage.Bounds.vPos2, currentPropertyDetails.garage.Bounds.fWidth)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD
		
		VECTOR vTempPlayerPos  = GET_PLAYER_COORDS(PLAYER_ID())
//		#ENDIF
		
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),currentPropertyDetails.house.vMidPoint) <= 40
			#IF IS_DEBUG_BUILD
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: player within 30 meters of the midpoint")
//			
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: GET_PLAYER_COORDS: ", vTempPlayerPos, " Bounds[0].vPos1: ", currentPropertyDetails.house.Bounds[0].vPos1, ", Bounds[0].vPos2: ", currentPropertyDetails.house.Bounds[0].vPos2, ", width: ", currentPropertyDetails.house.Bounds[0].fWidth)
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: GET_PLAYER_COORDS: ", vTempPlayerPos, " Bounds[1].vPos1: ", currentPropertyDetails.house.Bounds[1].vPos1, ", Bounds[1].vPos2: ", currentPropertyDetails.house.Bounds[1].vPos2, ", width: ", currentPropertyDetails.house.Bounds[1].fWidth)
//			CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: GET_PLAYER_COORDS: ", vTempPlayerPos
//								," Bounds[1].vPos1.Z: ", currentPropertyDetails.house.Bounds[1].vPos1.Z
//								," Bounds[0].vPos1: ", currentPropertyDetails.house.Bounds[0].vPos1.Z
//								," fCircleBoundPosition: ", currentPropertyDetails.house.vCircleBoundPosition
//								," distance = ", GET_DISTANCE_BETWEEN_COORDS(vTempPlayerPos, currentPropertyDetails.house.vCircleBoundPosition))
			#ENDIF
			IF IS_POINT_IN_ANGLED_AREA( GET_PLAYER_COORDS(PLAYER_ID()), currentPropertyDetails.house.Bounds[0].vPos1, 
															currentPropertyDetails.house.Bounds[0].vPos2, currentPropertyDetails.house.Bounds[0].fWidth)
			OR IS_POINT_IN_ANGLED_AREA( GET_PLAYER_COORDS(PLAYER_ID()), currentPropertyDetails.house.Bounds[1].vPos1, 
															currentPropertyDetails.house.Bounds[1].vPos2, currentPropertyDetails.house.Bounds[1].fWidth)
			OR IS_POINT_IN_ANGLED_AREA( GET_PLAYER_COORDS(PLAYER_ID()), currentPropertyDetails.house.Bounds[2].vPos1, 
															currentPropertyDetails.house.Bounds[2].vPos2, currentPropertyDetails.house.Bounds[2].fWidth)
															
				//CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: player withen angled area")
				RETURN TRUE
				
//			VECTOR vTempPlayerPos  = GET_PLAYER_COORDS(PLAYER_ID())
			ELIF GET_DISTANCE_BETWEEN_COORDS(vTempPlayerPos, currentPropertyDetails.house.vCircleBoundPosition) < currentPropertyDetails.house.fCircleBoundRadius + extendRadius
			AND (vTempPlayerPos.Z > currentPropertyDetails.house.Bounds[2].vPos1.Z AND vTempPlayerPos.Z < currentPropertyDetails.house.Bounds[2].vPos2.Z)
			//OR (vTempPlayerPos.Z > currentPropertyDetails.house.Bounds[1].vPos1.Z AND vTempPlayerPos.Z < currentPropertyDetails.house.Bounds[0].vPos1.Z))
				//CDEBUG2LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: player within upper Yacht area")
				RETURN TRUE
			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: player not within angled area")
			ENDIF
		ELSE
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: player not withen 30 meters of the midpoint, it's: ", GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),currentPropertyDetails.house.vMidPoint))
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_HEIST_POSSIBLY_QR_IN_APT() 
//	IF NOT g_bHeistQuickRestart
//		RETURN FALSE
//	ENDIF
//	IF FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
//	OR IS_BIT_SET(g_TransitionSessionNonResetVars.iRestartBitset, FMMC_RESTART_BITSET_RETRY_STRAND_PASS_START_POINT)
//	OR FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
//	OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED()
//	OR HAS_FIRST_STRAND_MISSION_BEEN_PASSED_NO_RESET()
//		RETURN FALSE
//	ENDIF
//	
//	RETURN TRUE
	IF g_TransitionSessionNonResetVars.bHeistQuickRestartInProg
	AND NOT g_b_HasCheckedApartmentInfo 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SKIP_CHECK_PLAYER_IS_INSIDE_YACHT()
	IF mpPropMaintain.iYachtToCheck = -1
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT()
	IF IS_PRIVATE_YACHT_ACTIVE(mpPropMaintain.iYachtToCheck)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpYachts[mpPropMaintain.iYachtToCheck].yachtPropertyDetails.house.vMidPoint)<= 30
			IF IS_PLAYER_RESPAWNING(PLAYER_ID())
				IF NOT g_SpawnData.bSpawningInProperty	
				AND NOT g_SpawnData.bSpawningInYacht
					PRINTLN("CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT: Player is repsawning bypassing")
					RETURN FALSE
				ENDIF
			ENDIF
			IF IS_PLAYER_WITHIN_PROPERTY_INTERIOR(mpYachts[mpPropMaintain.iYachtToCheck].yachtPropertyDetails,ENTRANCE_TYPE_HOUSE)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
					RETURN TRUE
				ELSE
					PRINTLN("CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT: Player is now inside property but not launching g_bInMissionCreatorApartment # ", mpPropMaintain.iYachtToCheck)
				ENDIF
			ELSE
				PRINTLN("CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT: PLAYER within 30 but not in Interior of yacht # ",mpPropMaintain.iYachtToCheck) 
			ENDIF
		ELSE
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT: player is further than 30 meters from property: ", mpPropMaintain.iYachtToCheck)
			//CDEBUG2LN(DEBUG_SAFEHOUSE, "CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT: player is : ",GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpYachts[mpPropMaintain.iYachtToCheck].yachtPropertyDetails.house.vMidPoint), " meters from midpoint, midpoint: ", mpYachts[mpPropMaintain.iYachtToCheck].yachtPropertyDetails.house.vMidPoint)
		ENDIF	
	ENDIF
	mpPropMaintain.iYachtToCheck++
	IF mpPropMaintain.iYachtToCheck >= NUMBER_OF_PRIVATE_YACHTS
		mpPropMaintain.iYachtToCheck = -1
	ENDIF
	RETURN FALSE
ENDFUNC

///
/// Purpose: 
///    Get the name of external IPL required by property, 
///    these are mainly balonies for stilt apartmens
FUNC STRING GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(INT iPropertyID, INT iExtraIplIndex = 0)
	

	SWITCH iPropertyID
		CASE PROPERTY_STILT_APT_1_BASE_B
			RETURN "apa_stilt_ch2_05e_ext1"
		BREAK
		CASE PROPERTY_STILT_APT_2_B
			RETURN "apa_stilt_ch2_04_ext1"
		BREAK
		CASE PROPERTY_STILT_APT_3_B
			RETURN "apa_stilt_ch2_09b_ext3"
		BREAK
		CASE PROPERTY_STILT_APT_4_B
			RETURN "apa_stilt_ch2_09c_ext2"
		BREAK
		CASE PROPERTY_STILT_APT_5_BASE_A
			RETURN "apa_stilt_ch2_05c_ext1"
		BREAK
		CASE PROPERTY_STILT_APT_7_A
			RETURN "apa_stilt_ch2_09c_ext3"
		BREAK
		CASE PROPERTY_STILT_APT_8_A
			RETURN "apa_stilt_ch2_09c_ext1"
		BREAK
		CASE PROPERTY_STILT_APT_10_A
			RETURN "apa_stilt_ch2_09b_ext2"
		BREAK
		CASE PROPERTY_STILT_APT_12_A
			RETURN "apa_stilt_ch2_12b_ext1"
		BREAK
		CASE PROPERTY_STILT_APT_13_A
			RETURN "apa_stilt_ch2_04_ext2"
		BREAK
		CASE PROPERTY_OFFICE_1
			RETURN "hei_sm_13_exshadowmesh"
		BREAK
		CASE PROPERTY_OFFICE_2_BASE
			RETURN "hei_sm_15_exshadowmesh"
		BREAK
		CASE PROPERTY_OFFICE_3
			RETURN "hei_dt1_02_exshadowmesh"
		BREAK
		CASE PROPERTY_OFFICE_4
			RETURN "hei_dt1_11_exshadowmesh"
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
			IF iExtraIplIndex = 0 
				RETURN "HEI_SM_13_ImpExpProxy_A"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_SM_13_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
			IF iExtraIplIndex = 0 
				RETURN "HEI_SM_13_ImpExpProxy_B"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_SM_13_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
			IF iExtraIplIndex = 0 
				RETURN "HEI_SM_13_ImpExpProxy_C"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_SM_13_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
			IF iExtraIplIndex = 0 
				RETURN "HEI_SM_15_ImpExpProxy_A"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_SM_15_ImpExpProxy_ModShops"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
			IF iExtraIplIndex = 0 
				RETURN "HEI_SM_15_ImpExpProxy_B"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_SM_15_ImpExpProxy_ModShops"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
			IF iExtraIplIndex = 0 
				RETURN "HEI_SM_15_ImpExpProxy_C"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_SM_15_ImpExpProxy_ModShops"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
			IF iExtraIplIndex = 0
				RETURN "HEI_DT1_02_ImpExpProxy_A"
			ELIF iExtraIplIndex = 1 
				RETURN "HEI_DT1_02_ImpExpEmProxy_A"
			ELIF iExtraIplIndex = 2
				RETURN "HEI_DT1_02_ImpExpProxy_ModShop"
			ELIF iExtraIplIndex = 3
				RETURN "HEI_DT1_02_ImpExpEmProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
			IF iExtraIplIndex = 0
				RETURN "HEI_DT1_02_ImpExpProxy_B"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_DT1_02_ImpExpEmProxy_B"
			ELIF iExtraIplIndex = 2
				RETURN "HEI_DT1_02_ImpExpProxy_ModShop"
			ELIF iExtraIplIndex = 3
				RETURN "HEI_DT1_02_ImpExpEmProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
			IF iExtraIplIndex = 0
				RETURN "HEI_DT1_02_ImpExpProxy_C"
			ELIF iExtraIplIndex = 1 
				RETURN "HEI_DT1_02_ImpExpEmProxy_C"	
			ELIF iExtraIplIndex = 2
				RETURN "HEI_DT1_02_ImpExpProxy_ModShop"
			ELIF iExtraIplIndex = 3
				RETURN "HEI_DT1_02_ImpExpEmProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF iExtraIplIndex = 0 
				RETURN "HEI_DT1_11_ImpExpProxy_A"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_DT1_11_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF iExtraIplIndex = 0 
				RETURN "HEI_DT1_11_ImpExpProxy_B"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_DT1_11_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF iExtraIplIndex = 0 
				RETURN "HEI_DT1_11_ImpExpProxy_C"
			ELIF iExtraIplIndex = 1
				RETURN "HEI_DT1_11_ImpExpProxy_ModShop"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

FUNC INT GET_EXTERNAL_IPL_TYPE_FOR_PROPERTY(INT iPropertyID, INT iExtraIplIndex = 0)
	SWITCH iPropertyID
		CASE 9999
			IF iExtraIplIndex != 0 //for compile without import export
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_SM_13_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_SM_13_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_SM_13_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_SM_15_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_SM_15_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_SM_15_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
			IF iExtraIplIndex = 2
			OR iExtraIplIndex = 3
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_DT1_02_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
			IF iExtraIplIndex = 2
			OR iExtraIplIndex = 3
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_DT1_02_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
			IF iExtraIplIndex = 2
			OR iExtraIplIndex = 3
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_DT1_02_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_DT1_11_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_DT1_11_ImpExpProxy_ModShop"
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF iExtraIplIndex = 1
				RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_MOD_SHOP //"HEI_DT1_11_ImpExpProxy_ModShop"
			ENDIF
		BREAK
	ENDSWITCH
	RETURN EXTERNAL_IPLS_FOR_PROPERTY_TYPE_DEFAULT
ENDFUNC

///
/// Purpose:
///    Requests external IPLs needed for some of the stilt houses and
///    returns TRUE if the request was successful. If there's nothing to
///    request returns TRUE as well
/// 	    
FUNC BOOL ARE_PROPERTY_EXTRA_IPLS_REQUESTED(INT iProperty, INT iType = EXTERNAL_IPLS_FOR_PROPERTY_TYPE_DEFAULT)
	INT i
	// can't request ipl's when warping or doing load scene. 2573013
	IF IS_NEW_LOAD_SCENE_ACTIVE()
	OR IS_PLAYER_TELEPORT_ACTIVE()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_PROPERTY_EXTRA_IPLS_REQUESTED - cant request, load scene active")
		RETURN FALSE
	ENDIF
	STRING strIPLName
	REPEAT MAX_EXTRA_EXTERNAL_IPLS_FOR_PROPERTY i
		IF iType = GET_EXTERNAL_IPL_TYPE_FOR_PROPERTY(iProperty,i)
			strIPLName = GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(iProperty,i)
			IF NOT IS_STRING_NULL_OR_EMPTY(strIPLName)
				IF NOT IS_IPL_ACTIVE(strIPLName)
					REQUEST_IPL(strIPLName)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_PROPERTY_EXTRA_IPLS_REQUESTED - requesting additional IPL for property ", strIPLName)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_PROPERTY_EXTRA_IPLS_REQUESTED - this property doesn't require any external IPLS")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_PROPERTY_EXTRA_IPLS_ACTIVE(INT iProperty, INT iType = EXTERNAL_IPLS_FOR_PROPERTY_TYPE_DEFAULT)
	INT i
	STRING strIPLName 
	REPEAT MAX_EXTRA_EXTERNAL_IPLS_FOR_PROPERTY i
		IF iType = GET_EXTERNAL_IPL_TYPE_FOR_PROPERTY(iProperty,i)
			strIPLName = GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(iProperty,i)
			IF NOT IS_STRING_NULL_OR_EMPTY(strIPLName)
				RETURN IS_IPL_ACTIVE(strIPLName)
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PROPERTY_EXTRA_IPL_ACTIVE - this property doesn't require any external IPLS")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_PROPERTY_EXTRA_IPLS_UNLOADED(INT iProperty, INT iType = EXTERNAL_IPLS_FOR_PROPERTY_TYPE_DEFAULT)
	INT i
	// can't request ipl's when warping or doing load scene. 2573013
	IF IS_NEW_LOAD_SCENE_ACTIVE()
	OR IS_PLAYER_TELEPORT_ACTIVE()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_PROPERTY_EXTRA_IPLS_UNLOADED - cant unload, load scene active")
		RETURN FALSE
	ENDIF
	STRING strIPLName 
	REPEAT MAX_EXTRA_EXTERNAL_IPLS_FOR_PROPERTY i
		IF iType = GET_EXTERNAL_IPL_TYPE_FOR_PROPERTY(iProperty,i)
			strIPLName = GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(iProperty,i)
			IF NOT IS_STRING_NULL_OR_EMPTY(strIPLName)
				IF IS_IPL_ACTIVE(strIPLName)
					REMOVE_IPL(strIPLName)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_PROPERTY_EXTRA_IPLS_UNLOADED - unloading IPL for property ", strIPLName)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_PROPERTY_EXTRA_IPLS_UNLOADED - this property doesn't require any external IPLS")
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED()
	// can't request ipl's when warping or doing load scene. 2573013
	IF IS_NEW_LOAD_SCENE_ACTIVE()
	OR IS_PLAYER_TELEPORT_ACTIVE()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED - cant unload, load scene active")
		RETURN FALSE
	ENDIF
	
	INT i,j 
	INT iPropertyIDs[12]
	iPropertyIDs[0] = PROPERTY_STILT_APT_1_BASE_B
	iPropertyIDs[1] = PROPERTY_STILT_APT_2_B
	iPropertyIDs[2] = PROPERTY_STILT_APT_3_B
	iPropertyIDs[3] = PROPERTY_STILT_APT_4_B
	iPropertyIDs[4] = PROPERTY_STILT_APT_5_BASE_A
	iPropertyIDs[5] = PROPERTY_STILT_APT_7_A
	iPropertyIDs[6] = PROPERTY_STILT_APT_8_A
	iPropertyIDs[7] = PROPERTY_STILT_APT_10_A
	iPropertyIDs[8] = PROPERTY_STILT_APT_12_A
	iPropertyIDs[9] = PROPERTY_STILT_APT_13_A
	
	STRING strIPL
	
	REPEAT 10 i
		strIPL = GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(iPropertyIDs[i])
		IF IS_IPL_ACTIVE(strIPL)
			REMOVE_IPL(strIPL)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED - removing IPL ", strIPL)
		ENDIF
	ENDREPEAT
	
	i = 0
	iPropertyIDs[0] = PROPERTY_OFFICE_1
	iPropertyIDs[1] = PROPERTY_OFFICE_2_BASE 
	iPropertyIDs[2] = PROPERTY_OFFICE_3
	iPropertyIDs[3] = PROPERTY_OFFICE_4
	REPEAT 4 i
		strIPL = GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(iPropertyIDs[i])
		IF IS_IPL_ACTIVE(strIPL)
			REMOVE_IPL(strIPL)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED - removing IPL ", strIPL)
		ENDIF
	ENDREPEAT
	
	i = 0
	iPropertyIDs[0] = PROPERTY_OFFICE_1_GARAGE_LVL1
	iPropertyIDs[1] = PROPERTY_OFFICE_1_GARAGE_LVL2
	iPropertyIDs[2] = PROPERTY_OFFICE_1_GARAGE_LVL3
	iPropertyIDs[3] = PROPERTY_OFFICE_2_GARAGE_LVL1
	iPropertyIDs[4] = PROPERTY_OFFICE_2_GARAGE_LVL2
	iPropertyIDs[5] = PROPERTY_OFFICE_2_GARAGE_LVL3
	iPropertyIDs[6] = PROPERTY_OFFICE_3_GARAGE_LVL1
	iPropertyIDs[7] = PROPERTY_OFFICE_3_GARAGE_LVL2
	iPropertyIDs[8] = PROPERTY_OFFICE_3_GARAGE_LVL3
	iPropertyIDs[9] = PROPERTY_OFFICE_4_GARAGE_LVL1
	iPropertyIDs[10] = PROPERTY_OFFICE_4_GARAGE_LVL2
	iPropertyIDs[11] = PROPERTY_OFFICE_4_GARAGE_LVL3
	REPEAT 12 i
		REPEAT MAX_EXTRA_EXTERNAL_IPLS_FOR_PROPERTY j
			strIPL = GET_EXTERNAL_IPL_REQUIRED_BY_PROPERTY(iPropertyIDs[i],j)
			IF NOT IS_STRING_NULL_OR_EMPTY(strIPL)
				IF IS_IPL_ACTIVE(strIPL)
					REMOVE_IPL(strIPL)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED - removing IPL ", strIPL)
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED - all unloaded")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL BLOCK_APARTMENT_AREA_CHECK_FOR_SVM_MISSION()
	IF NETWORK_IS_ACTIVITY_SESSION()
	AND (IS_TRANSITION_SESSION_LAUNCHING()
	OR IS_TRANSITION_SESSION_RESTARTING()
	OR (IS_PLAYER_IN_CORONA() AND GET_CORONA_STATUS() <= CORONA_STATUS_BALANCE_AND_LOAD))
	AND SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH()
	RETURN IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_BLOCK_PROPERTY_BOUNDS_CHECK)
ENDFUNC

PROC BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH(BOOL bBlock)
	IF bBlock
		IF NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_BLOCK_PROPERTY_BOUNDS_CHECK)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH: set to true")
			SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_BLOCK_PROPERTY_BOUNDS_CHECK)
		ENDIF
	ELSE
		IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_BLOCK_PROPERTY_BOUNDS_CHECK)
			DEBUG_PRINTCALLSTACK()
			PRINTLN("BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH: set to false")
			CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_BLOCK_PROPERTY_BOUNDS_CHECK)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT()
	//IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_DisableApartScript)
		INT propertyIntStacksize = PROPERTY_INT_STACK_SIZE
		MP_PROPERTY_NON_AXIS_DETAILS bounds
		IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_GARAGE)
		OR IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_HOUSE)
		OR IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
		OR IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
		OR IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_PLAYER_INTERACTION_INVITE)
		OR IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_FROM_HELICOPTER)
		OR IS_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()
			IF NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
				AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_PROPERTY_INT",mpPropMaintain.iMostRecentPropertyID,TRUE)
					REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
					IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
						START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
						//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_GARAGE)
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_GARAGE!")
						ELIF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_HOUSE)
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script GLOBAL_PROPERTY_ENTRY_BS_BUZZER_ENTER_HOUSE!")
						ELIF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE)
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script GLOBAL_PROPERTY_ENTRY_BS_ENTER_GARAGE!")
						ELIF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE!")
						ELIF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_PLAYER_INTERACTION_INVITE)
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script GLOBAL_PROPERTY_ENTRY_BS_PLAYER_INTERACTION_INVITE!")
						ELIF IS_BIT_SET(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_FROM_HELICOPTER)
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script GLOBAL_PROPERTY_ENTRY_BS_ENTER_FROM_HELICOPTER!")
						ELIF IS_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script IS_CONTACT_MISSION_LAUNCHING_IN_APARTMENT()!")
						ENDIF
						#ENDIF
						SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
					ENDIF
				ENDIF
			ENDIF
		ELIF g_SpawnData.bSpawningInProperty	
			IF NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
				AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_PROPERTY_INT",mpPropMaintain.iMostRecentPropertyID,TRUE)
					REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
					IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
						START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
						//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
						PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from spawn!")
						SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
					ENDIF
				ENDIF
			ENDIF
		ELIF globalPropertyEntryData.iPropertyEntered != 0 //warping into property
		AND IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
			IF NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
				AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_PROPERTY_INT",mpPropMaintain.iMostRecentPropertyID,TRUE)
					REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
					IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
						START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
						//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
						SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
						PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from entering a property!")
					ENDIF
				ENDIF
			ENDIF
		ELIF g_b_TriggerLaunchInteriorQuickRestart //quick restart in Apartment
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
			AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_PROPERTY_INT",mpPropMaintain.iMostRecentPropertyID,TRUE)
				REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
				IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
					START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
					//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
					SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
					PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from g_b_TriggerLaunchInteriorQuickRestart")
					g_b_TriggerLaunchInteriorQuickRestart = FALSE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
			AND NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_DisableApartScript)
			AND (NOT g_sPlayerAcceptedCrossSessionInviteToHeist 
			OR (g_sPlayerAcceptedCrossSessionInviteToHeist AND g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt > 0))
			AND NOT IS_HEIST_POSSIBLY_QR_IN_APT()
			AND NOT BLOCK_APARTMENT_AREA_CHECK_FOR_SVM_MISSION()
			AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()		
			AND NOT SHOULD_BLOCK_PROPERTY_BOUNDS_CHECK_LAUNCH()
			AND NOT g_bRunGroupYachtExit	
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) <= 0
				AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_PROPERTY_INT",mpPropMaintain.iMostRecentPropertyID,TRUE)
					IF NOT SKIP_CHECK_PLAYER_IS_INSIDE_YACHT()
						//CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: Checking player is in yacht ", mpPropMaintain.iYachtToCheck)
						IF CHECK_LAUNCH_INT_SCRIPT_FOR_YACHT()
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: Player is now inside yacht # ", mpPropMaintain.iYachtToCheck, " launching interior script")
							REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
							IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
								START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
								SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from already being inside property (inside yacht)")
								SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
								SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_LAUNCHED_ALREADY_INSIDE)
								EXIT
							ENDIF
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: waiting for script to load")
							EXIT
						ENDIF
						
	//					PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT = checking property # ",mpPropMaintain.iPropertyInsideCheck )
	//					VECTOR vTemp = GET_PLAYER_COORDS(PLAYER_ID())
	//					PRINTLN("Player coords = ",vTemp)
	//					PRINTLN("Garage coords = ",vTemp)
	//					IF NOT IS_PROPERTY_ONLY_A_GARAGE(mpPropMaintain.iPropertyInsideCheck)
	//						vTemp = GET_MP_HOUSE_INTERIOR_MIDPOINT(mpPropMaintain.iPropertyInsideCheck)
	//						PRINTLN("House coords = ",vTemp)
	//					ENDIF
	//					IF IS_PROPERTY_ONLY_A_GARAGE(mpPropMaintain.iPropertyInsideCheck)
					ELSE
						//CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: Checking player is in property ", mpPropMaintain.iPropertyInsideCheck)
						IF NOT IS_PROPERTY_ONLY_A_GARAGE(mpPropMaintain.iPropertyInsideCheck)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_MP_HOUSE_INTERIOR_MIDPOINT(mpPropMaintain.iPropertyInsideCheck))<= 30
							IF IS_PLAYER_WITHIN_PROPERTY_INTERIOR(mpProperties[mpPropMaintain.iPropertyInsideCheck], ENTRANCE_TYPE_HOUSE)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: Player is now inside property # ", mpPropMaintain.iPropertyInsideCheck, " launching interior script")
									CDEBUG2LN(DEBUG_SAFEHOUSE, "Player is now inside property # ", mpPropMaintain.iPropertyInsideCheck, " launching interior script")
									REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
									IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
				//						coords_struct tempStruct
				//						tempStruct.number_of_coords = 1
				//						tempStruct.vec_coord[0] = <<260.5406, -999.5367, -100.0087>>
											//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
										START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
										SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from already being inside property (inside apt)")
										SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_LAUNCHED_ALREADY_INSIDE)
										EXIT
									ENDIF
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: waiting for script to load")
									EXIT
								#IF IS_DEBUG_BUILD
								ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: not launching g_bInMissionCreatorApartment # ", mpPropMaintain.iPropertyInsideCheck)
								ELSE
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: property is a clubhouse and launching is blocked prop # ", mpPropMaintain.iPropertyInsideCheck)
								#ENDIF
								ENDIF
							ELSE
								bounds.vPos1 = GET_PLAYER_COORDS(PLAYER_ID())
								PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: PLAYER within 30 at ",bounds.vPos1  ,"but not in Interior #", mpPropMaintain.iPropertyInsideCheck) 
							ENDIF
						ELSE
//							IF mpPropMaintain.iPropertyInsideCheck = 73
//								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: player is further than 30 meters from stilt base house b, midpoint ", mpPropMaintain.iPropertyInsideCheck)
//							ENDIF
//							CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: player is further than 30 meters from property: ", mpPropMaintain.iPropertyInsideCheck)
//							VECTOR tempMidpoint = GET_MP_HOUSE_INTERIOR_MIDPOINT(mpPropMaintain.iPropertyInsideCheck)
//							CDEBUG2LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: player is : ",GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_MP_HOUSE_INTERIOR_MIDPOINT(mpPropMaintain.iPropertyInsideCheck)), " meters from midpoint, midpoint: ", tempMidpoint)
						ENDIF
						IF IS_PROPERTY_OFFICE_GARAGE(mpPropMaintain.iPropertyInsideCheck)
							IF IS_PLAYER_IN_OFFICE_GARAGE_MOD_INTERIOR(mpPropMaintain.iPropertyInsideCheck,PLAYER_ID(),DEFAULT,TRUE)
							OR IS_PLAYER_WITHIN_PROPERTY_INTERIOR(mpProperties[mpPropMaintain.iPropertyInsideCheck], ENTRANCE_TYPE_HOUSE)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: Player is now inside property # ", mpPropMaintain.iPropertyInsideCheck, " launching interior script")
									CDEBUG2LN(DEBUG_SAFEHOUSE, "Player is now inside property # ", mpPropMaintain.iPropertyInsideCheck, " launching interior script")
									REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
									IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
				//						coords_struct tempStruct
				//						tempStruct.number_of_coords = 1
				//						tempStruct.vec_coord[0] = <<260.5406, -999.5367, -100.0087>>
											//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
										START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
										SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from already being inside property (inside apt)")
										SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_LAUNCHED_ALREADY_INSIDE)
										EXIT
									ENDIF
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: waiting for script to load")
									EXIT
								ELSE
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: not launching g_bInMissionCreatorApartment # ", mpPropMaintain.iPropertyInsideCheck)
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_MP_GARAGE_INTERIOR_MIDPOINT(PROP_GARAGE_SIZE_2)) <= 30
							GET_MP_GARAGE_INTERIOR_BOUNDS(bounds, PROP_GARAGE_SIZE_2)
							PRINTLN("POD: PLAYER with 30 of 2 car garage") 
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), bounds.vPos1,bounds.vPos2, bounds.fWidth)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
									IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
										//coords_struct tempStruct
										//tempStruct.number_of_coords = 1
										//tempStruct.vec_coord[0] = <<173.1406, -1008.0995, -99.9999>>
										START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
										SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
										//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
										PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from already being inside property (small garage)")
										SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_LAUNCHED_ALREADY_INSIDE)
										EXIT
									ENDIF
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: waiting for script to load (small garage)")
									EXIT
								ELSE
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: not launching g_bInMissionCreatorApartment (small garage)")
								ENDIF
							ENDIF
						ENDIF
						IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_MP_GARAGE_INTERIOR_MIDPOINT(PROP_GARAGE_SIZE_6)) <= 30
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: PLAYER with 30 of 6 car garage") 
							GET_MP_GARAGE_INTERIOR_BOUNDS(bounds, PROP_GARAGE_SIZE_6)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), bounds.vPos1,bounds.vPos2, bounds.fWidth)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
									IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
										//coords_struct tempStruct
										//tempStruct.number_of_coords = 1
										//tempStruct.vec_coord[0] = <<205.2725, -997.4227, -99.9999>>
										START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
										SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
										//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
										PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from already being inside property (med garage)")
										SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_LAUNCHED_ALREADY_INSIDE)
										EXIT
									ENDIF
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: waiting for script to load (med garage)")
									EXIT
								ELSE
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: but not launching g_bInMissionCreatorApartment(med garage)")
								ENDIF
							ENDIF
						ENDIF
						IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),GET_MP_GARAGE_INTERIOR_MIDPOINT(PROP_GARAGE_SIZE_10)) <= 30
							PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: PLAYER with 30 of 10 car garage") 
							GET_MP_GARAGE_INTERIOR_BOUNDS(bounds, PROP_GARAGE_SIZE_10)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), bounds.vPos1,bounds.vPos2, bounds.fWidth)
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_g_bInMissionCreatorApartment)
									REQUEST_SCRIPT("AM_MP_PROPERTY_INT")
									IF HAS_SCRIPT_LOADED("AM_MP_PROPERTY_INT")
										//coords_struct tempStruct
										//tempStruct.number_of_coords = 1
										//tempStruct.vec_coord[0] = <<229.2159, -1005.1038, -99.9999>>
										START_NEW_SCRIPT("AM_MP_PROPERTY_INT", propertyIntStacksize)
										SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_PROPERTY_INT")
										PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: launched script from already being inside property (large garage)")
										//START_NEW_SCRIPT_WITH_ARGS("AM_MP_PROPERTY_INT", tempStruct, SIZE_OF(tempStruct), MULTIPLAYER_MISSION_STACK_SIZE)
										SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
										SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_LAUNCHED_ALREADY_INSIDE)
										EXIT
									ENDIF
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: waiting for script to load (large garage)")
									EXIT
								ELSE
									PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT: but not launching g_bInMissionCreatorApartment (large garage)")
								ENDIF
							ENDIF
						ENDIF
						mpPropMaintain.iPropertyInsideCheck++
						IF mpPropMaintain.iPropertyInsideCheck > MAX_MP_PROPERTIES
						OR IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_CHECK_TRANS_DETAILS_CLEANUP)
							mpPropMaintain.iPropertyInsideCheck = 0
							mpPropMaintain.iYachtToCheck = 0
							IF IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_CHECK_TRANS_DETAILS_CLEANUP)
								PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT:MP_PROP_MAINTAIN_BS_CHECK_TRANS_DETAILS_CLEANUP cleared")
								CLEAR_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_CHECK_TRANS_DETAILS_CLEANUP)
							ENDIF
							IF g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt != 0
								IF NOT IS_TRANSITION_SESSION_RESTARTING()
								AND NOT IS_TRANSITION_SESSION_LAUNCHING()
								AND NOT NETWORK_IS_IN_TRANSITION()
									IF NOT IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
									AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
									AND NOT IS_PLAYER_IN_CORONA()
										IF NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()
										AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
										AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
											IF NOT TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
												IF NOT IS_PLAYER_IN_LAUNCHED_TRANSITION_SESSION_CORONAS()
													IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment
														PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT(): reseting transitional session invite details as we are not trying to launch interior script by any means")
														CLEAR_TRANSITION_SESSION_PROPERTY_INVITE_DETAILS()
													ELSE
														PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT(): g_TransitionSessionNonResetVars.sPostMissionCleanupData.bPlaceInApartment is TRUE")
													ENDIF
												ELSE
													PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT(): IS_PLAYER_IN_LAUNCHED_TRANSITION_SESSION_CORONAS() ")
												ENDIF
											ELSE
												PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT(): TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY() ")
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				//ELSE
				//	PRINTLN("GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH(AM_MP_PROPERTY_INT)) > 0")
				ENDIF
				
			//ELSE
			//	PRINTLN("IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)")
			#IF IS_DEBUG_BUILD
			ELSE
				IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT)
					PRINTLN("IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT) = ", IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_INT_SCRIPT))
				ELSE
					IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_DisableApartScript)
						PRINTLN("    ->     MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT - FALSE - biITA_DisableApartScript SET")
					ENDIF
					IF g_sPlayerAcceptedCrossSessionInviteToHeist
						PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT - FALSE g_sPlayerAcceptedCrossSessionInviteToHeist. Transition data = ",g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt)
					ENDIF
					IF IS_HEIST_POSSIBLY_QR_IN_APT()
						PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT - FALSE IS_HEIST_POSSIBLY_QR_IN_APT )")
					ENDIF
					IF BLOCK_APARTMENT_AREA_CHECK_FOR_SVM_MISSION()
						PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT - FALSE BLOCK_APARTMENT_AREA_CHECK_FOR_SVM_MISSION() )")
					ENDIF
					IF g_bRunGroupYachtExit
						PRINTLN("MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT - FALSE g_bRunGroupYachtExit )")
					ENDIF
				ENDIF
			#ENDIF	
			//	ENDIF
			ENDIF
		ENDIF
//	#IF IS_DEBUG_BUILD
//	ELSE
//		PRINTLN("    ->     MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT - FALSE - biITA_DisableApartScript SET")
//	#ENDIF	
//	ENDIF
ENDPROC

PROC CHECK_PLAYER_CONCEAL_STATE(PLAYER_INDEX playerToCheck, BOOL bForceRemoveConceal = FALSE)
	#IF IS_DEBUG_BUILD
	IF g_db_bDisplayConcealedState
		IF IS_NET_PLAYER_OK(playerToCheck,FALSE,FALSE)
			PRINTLN("CHECK_PLAYER_CONCEAL_STATE: OUTPUT START")
			PRINTLN("LOCAL: visibility ID = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID)
			PRINTLN("LOCAL: PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY))
			PRINTLN("LOCAL: PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING))
			
			PRINTLN("REMOTE: player = ",GET_PLAYER_NAME(playerToCheck))
			PRINTLN("REMOTE:  running state = ",IS_NET_PLAYER_OK(playerToCheck,FALSE,TRUE))
			PRINTLN("REMOTE: PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY))
			PRINTLN("REMOTE: PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING))
			PRINTLN("REMOTE: visibility ID = ",GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iVisibilityID )
			PRINTLN("LOCAL: PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW))
			PRINTLN("REMOTE: PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW))
			PRINTLN("LOCAL: PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED))
			PRINTLN("REMOTE: PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED))
			PRINTLN("LOCAL: PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED))
			PRINTLN("REMOTE: PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED = ",IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED))
		ENDIF
	ENDIF
	#ENDIF
	IF g_sTransitionSessionData.bMissionCutsceneInProgress 
		PRINTLN("CHECK_PLAYER_CONCEAL_STATE: bypass for g_sTransitionSessionData.bMissionCutsceneInProgress ")
		EXIT
	ENDIF
	//VEHICLE_INDEX tempVeh
	//PED_INDEX tempPed
	IF playerToCheck != PLAYER_ID()
	

		IF bForceRemoveConceal
			IF IS_NET_PLAYER_OK(playerToCheck,FALSE,TRUE)
				IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
					NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
					#IF IS_DEBUG_BUILD
					IF IS_PLAYER_PLAYING(playerToCheck)
						PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as NOT concealed to local player (FORCED) ")
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF g_db_bDisplayConcealedState
				PRINTLN("My visibility ID = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID)
				PRINTLN("Player I am checking visibility ID = ",GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iVisibilityID )
			ENDIF
			#ENDIF
			IF NETWORK_IS_PLAYER_ACTIVE(playerToCheck)
				IF IS_NET_PLAYER_OK(playerToCheck,FALSE,TRUE)
//					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
//					AND (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
//					OR IS_PLAYER_IN_PROPERTY(playerToCheck,TRUE))
//						IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
//							NETWORK_CONCEAL_PLAYER(playerToCheck,TRUE)
//							#IF IS_DEBUG_BUILD
//							IF IS_PLAYER_PLAYING(playerToCheck)
//								PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- local player is entering a property")
//							ENDIF
//							#ENDIF
//						ENDIF
//						
//						#IF IS_DEBUG_BUILD
//						IF g_db_bDisplayConcealedState
//							IF IS_PLAYER_PLAYING(playerToCheck)
//								PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- local player is entering a property")
//							ENDIF
//						ENDIF
//						#ENDIF
//					IF GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iVisibilityID = -1
//						IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
//							NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
//							#IF IS_DEBUG_BUILD
//							IF IS_PLAYER_PLAYING(playerToCheck)
//								PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as NOT concealed to local player- their visibility id is -1 ")
//							ENDIF
//							#ENDIF
//						ENDIF
//					ELSE
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
						OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
							#IF IS_DEBUG_BUILD
							IF g_db_bDisplayConcealedState
								IF IS_PLAYER_PLAYING(playerToCheck)
									PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- local player or remote player spawning in property")
								ENDIF
							ENDIF
							#ENDIF
							IF NOT IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
							OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
							OR (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY) AND IS_PLAYER_IN_PROPERTY(playerToCheck,TRUE))
								IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
									NETWORK_CONCEAL_PLAYER(playerToCheck,TRUE)
									#IF IS_DEBUG_BUILD
									IF IS_PLAYER_PLAYING(playerToCheck)
										PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- local player or remote player spawning in property")
									ENDIF
									#ENDIF
								ENDIF
							ENDIF
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
								CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
							ENDIF
							//SET_BIT(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
						ELSE
						
							IF IS_PLAYER_IN_PROPERTY(playerToCheck,TRUE)
								IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID != GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iVisibilityID
									#IF IS_DEBUG_BUILD
									IF g_db_bDisplayConcealedState
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- They have different visibility IDs")
										ENDIF
									ENDIF
									#ENDIF
									IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
										NETWORK_CONCEAL_PLAYER(playerToCheck,TRUE)
										#IF IS_DEBUG_BUILD
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- They have different visibility IDs")
										ENDIF
										#ENDIF
									ENDIF
									//SET_BIT(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
								ELIF (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW)
								OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW))
									#IF IS_DEBUG_BUILD
									IF g_db_bDisplayConcealedState
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- local player is previewing new interior")
										ENDIF
									ENDIF
									#ENDIF
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
									ENDIF
									IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
									AND NOT IS_SCREEN_FADING_OUT()
									AND IS_SCREEN_FADED_OUT()
									AND NOT IS_SCREEN_FADING_IN()
										NETWORK_CONCEAL_PLAYER(playerToCheck,TRUE)
										#IF IS_DEBUG_BUILD
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player Previewing ")
										ENDIF
										#ENDIF
									ELSE
										PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck),"NOT FADEING as not concealed to local player previewing ")
									ENDIF		
								
								ELSE
									IF  (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED)
									OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED))
									AND (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
									OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED) )
											IF NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(playerToCheck)
												IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
													NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
													IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
														CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
													ENDIF	
													#IF IS_DEBUG_BUILD
													IF IS_PLAYER_PLAYING(playerToCheck)
														PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as not concealed to local player Purchased ")
													ENDIF
													#ENDIF
												ENDIF
											ENDIF	
										EXIT
									ELIF  (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CANCEL_PREVIEW)
									OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CANCEL_PREVIEW))
									AND (IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED) 
									OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED) )
											IF NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(playerToCheck)
												IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
													NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
													IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
														CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
													ENDIF	
													#IF IS_DEBUG_BUILD
													IF IS_PLAYER_PLAYING(playerToCheck)
														PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as not concealed to local player cancel Purchased ")
													ENDIF
													#ENDIF
												ENDIF
											ENDIF	
										EXIT
									ELSE	
										//IF IS_BIT_SET(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
										#IF IS_DEBUG_BUILD
											IF g_db_bDisplayConcealedState
												IF IS_PLAYER_PLAYING(playerToCheck)
													PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," as NOT concealed to local player- same visibility IDs")
												ENDIF
											ENDIF
										#ENDIF
										IF NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(playerToCheck)
											IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
												NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
												#IF IS_DEBUG_BUILD
												IF IS_PLAYER_PLAYING(playerToCheck)
													PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as NOT concealed to local player- same visibility IDs")
												ENDIF
												#ENDIF
											ENDIF
										ENDIF	
										//CLEAR_BIT(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
									ENDIF
								ENDIF
							ELSE
							//IF IS_BIT_SET(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
								IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING)
									#IF IS_DEBUG_BUILD
									IF g_db_bDisplayConcealedState
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING set")
										ENDIF
									ENDIF
									#ENDIF
									IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
										IF NOT ARE_PLAYER_IN_SAME_VEHICLE(PLAYER_ID(),playerToCheck)
											NETWORK_CONCEAL_PLAYER(playerToCheck,TRUE)
											#IF IS_DEBUG_BUILD
											IF IS_PLAYER_PLAYING(playerToCheck)
												PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- PROPERTY_BROADCAST_BS_PLAYER_SHOULD_BE_CONCEALED_WHEN_ENTERING set")
											ENDIF
											#ENDIF
										ENDIF
									ENDIF
//									IF NOT ARE_PLAYER_IN_SAME_VEHICLE(PLAYER_ID(),playerToCheck)
//										tempPed = GET_PLAYER_PED(playerToCheck)
//										IF NOT IS_PED_INJURED(tempPed)
//											IF IS_PED_IN_ANY_VEHICLE(tempPed)
//												SET_ENTITY_LOCALLY_INVISIBLE(GET_VEHICLE_PED_IS_IN(tempPed,TRUE))
//												PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," hiding their vehicle while they are concealed entering property")
//											ENDIF
//										ENDIF
//									ENDIF
								ELSE
									#IF IS_DEBUG_BUILD
									IF g_db_bDisplayConcealedState
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: precheck ",GET_PLAYER_NAME(playerToCheck)," as NOT concealed to local player- player checked not in property")
										ENDIF
									ENDIF
									#ENDIF
									IF NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
										NETWORK_CONCEAL_PLAYER(playerToCheck,FALSE)
										#IF IS_DEBUG_BUILD
										IF IS_PLAYER_PLAYING(playerToCheck)
											PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as NOT concealed to local player- player checked not in property")
										ENDIF
										#ENDIF
									ENDIF
								ENDIF
								
								//CLEAR_BIT(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
							//ENDIF
							ENDIF
						ENDIF
//					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					IF g_db_bDisplayConcealedState	
						IF IS_PLAYER_PLAYING(playerToCheck)
							PRINTLN("CHECK_PLAYER_CONCEAL_STATE: pre check ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- NOT in running state")
						ENDIF
					ENDIF
					#ENDIF
					IF NOT NETWORK_IS_PLAYER_CONCEALED(playerToCheck)
						NETWORK_CONCEAL_PLAYER(playerToCheck,TRUE)
						#IF IS_DEBUG_BUILD
						IF IS_PLAYER_PLAYING(playerToCheck)
							PRINTLN("CHECK_PLAYER_CONCEAL_STATE: setting player ",GET_PLAYER_NAME(playerToCheck)," as concealed to local player- NOT in running state")
						ENDIF
						#ENDIF
					ENDIF
				
					//SET_BIT(mpPropMaintain.iVisibilityPlayerBS, NATIVE_TO_INT(playerToCheck))
						
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MP_PROPERTY_VISIBILITY()
	PLAYER_INDEX playerToCheck
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(),TRUE)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_ENTERING_A_PROPERTY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PREVIEW)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_PURCHASED)
		INT i
		//IF IS_BIT_SET(mpPropMaintain.iVisibilityControlBS ,MP_PROP_VIS_BS_CHECK_ALL_PLAYERS)
			REPEAT NUM_NETWORK_PLAYERS i
				playerToCheck = INT_TO_PLAYERINDEX(i)
				
				CHECK_PLAYER_CONCEAL_STATE(playerToCheck)
			ENDREPEAT
			CLEAR_BIT(mpPropMaintain.iVisibilityControlBS ,MP_PROP_VIS_BS_CHECK_ALL_PLAYERS)
		//ELSE
		//	playerToCheck = INT_TO_PLAYERINDEX(mpPropMaintain.iVisibilitySlowLoop)
		//	CHECK_PLAYER_CONCEAL_STATE(playerToCheck)
		//	
		//ENDIF
	ELSE
		IF GlobalPlayerBD[mpPropMaintain.iVisibilitySlowLoop].SimpleInteriorBD.eCurrentSimpleInterior = SIMPLE_INTERIOR_INVALID // block unconcealing if player is in simple interior (so it doesnt clash with simple interior conceal stuff)
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
			playerToCheck = INT_TO_PLAYERINDEX(mpPropMaintain.iVisibilitySlowLoop)
			CHECK_PLAYER_CONCEAL_STATE(playerToCheck,TRUE)
		ENDIF
	ENDIF
	
	mpPropMaintain.iVisibilitySlowLoop++
	IF mpPropMaintain.iVisibilitySlowLoop >= NUM_NETWORK_PLAYERS
		mpPropMaintain.iVisibilitySlowLoop = 0
	ENDIF
ENDPROC

//PROC DO_FAKE_PROPERTY_WARPS()
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1626.382080,-4068.388184,4.382338>>,
//							<<-1627.532959,-4067.995117,7.632306>> ,
//							1.750000,
//							FALSE,TRUE,TM_ON_FOOT)
//		SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1603.4801, -4076.4915, 4.8851>>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(),183.7798)
//		//SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_ENTER_HOUSE)
//	ENDIF
//	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-1605.285522,-4075.294189,4.634495>>,
//							<<-1604.318237,-4075.597412,7.635061>>,
//							2.500000,
//							FALSE,TRUE,TM_ON_FOOT)
//		SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1630.0397, -4067.5461, 4.8821>>)	
//		SET_ENTITY_HEADING(PLAYER_PED_ID(),64.7917)
//	ENDIF
//ENDPROC

FUNC BOOL IS_SPECTATED_PLAYER_NEAR_YACHT(INT iYacht, INT iDistance)
	IF IS_PLAYER_SPECTATING(PLAYER_ID()) 
		IF DOES_ENTITY_EXIST(GET_SPECTATOR_SELECTED_PED())
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_SPECTATOR_SELECTED_PED(),FALSE),mpYachts[iYacht].vBaseLocation,TRUE) <= iDistance
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT()
	IF (NOT IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_YACHT_SCRIPT)
	AND NOT IS_PLAYER_IN_MP_PROPERTY(PLAYER_ID(),FALSE))
	OR IS_PLAYER_SPECTATING(PLAYER_ID())
		IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
			mpPropMaintain.iYachtNearbyCheck = mpPropMaintain.iMostRecentYachtID
			PRINTLN("MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT: MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT forcing check on last yacht #",mpPropMaintain.iMostRecentYachtID)
		ENDIF
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_YACHT")) <= 0
		AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_YACHT",mpPropMaintain.iMostRecentYachtID,TRUE))
		OR (IS_PLAYER_SPECTATING(PLAYER_ID()) AND NOT NETWORK_IS_SCRIPT_ACTIVE("AM_MP_YACHT",mpPropMaintain.iYachtNearbyCheck,TRUE))
			IF IS_PRIVATE_YACHT_ACTIVE(mpPropMaintain.iYachtNearbyCheck)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(PLAYER_ID()),mpYachts[mpPropMaintain.iYachtNearbyCheck].vBaseLocation,TRUE) <= 300
				OR IS_SPECTATED_PLAYER_NEAR_YACHT(mpPropMaintain.iYachtNearbyCheck,300)
					PRINTLN("MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT: Player is now in range of yacht # ", mpPropMaintain.iYachtNearbyCheck, " launching yacht ext script")
					REQUEST_SCRIPT("AM_MP_YACHT")
					IF HAS_SCRIPT_LOADED("AM_MP_YACHT")
						START_NEW_SCRIPT("AM_MP_YACHT", AM_MP_YACHT_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("AM_MP_YACHT")
						mpPropMaintain.iMostRecentYachtID = mpPropMaintain.iYachtNearbyCheck
						IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
							CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
							PRINTLN("MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT: MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT cleared restarted script")
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT: launched script for player being nearby #",mpPropMaintain.iMostRecentYachtID)
						SET_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_LAUNCHED_YACHT_SCRIPT)
						EXIT
					ENDIF
				ELSE
					IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
						CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
						PRINTLN("MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT: MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT cleared not nearby")
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
					CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT)
					PRINTLN("MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT: MP_PROP_MAINTAIN_BS_RELAUNCH_CURRENT_YACHT cleared not assigned")
				ENDIF
			ENDIF
		ENDIF
		mpPropMaintain.iYachtNearbyCheck++
		IF mpPropMaintain.iYachtNearbyCheck >= NUMBER_OF_PRIVATE_YACHTS
			mpPropMaintain.iYachtNearbyCheck = 0
		ENDIF
	ENDIF
ENDPROC

PROC SET_YACHT_OWNERSHIP_DETAILS_BASED_ON_STATS()
	INT iOption = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_MOD)
	INT iTint = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_COLOR)
	INT iLighting = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_LIGHTING)
	INT iRailing = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FIXTURE)
	INT iFlag = GET_PACKED_STAT_INT(PACKED_MP_CHAR_YACHT_FLAG)
	TEXT_LABEL_63 sName = GET_MP_LONG_STRING_CHARACTER_STAT(MP_STAT_YACHT_NAME, MP_STAT_YACHT_NAME2)
				
	SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(TRUE, iOption, iTint, iLighting, iRailing, iFlag)
		
	SET_LOCAL_PLAYER_BD_YACHT_NAME(sName) 
ENDPROC

PROC ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY(INT iProperty, INT iVariation)
	PRINTLN("ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: called with iProperty: ",iProperty," iVariation: ",iVariation)
	IF IS_PROPERTY_CUSTOM_APARTMENT(iProperty)
	OR iProperty = -1
		//property 1
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_01_A")
				REQUEST_IPL("Apa_V_mp_h_01_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_01_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_01_A")
				REMOVE_IPL("Apa_V_mp_h_01_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_01_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_02_A")
				REQUEST_IPL("Apa_V_mp_h_02_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_02_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_02_A")
				REMOVE_IPL("Apa_V_mp_h_02_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_02_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_03_A")
				REQUEST_IPL("Apa_V_mp_h_03_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_03_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_03_A")
				REMOVE_IPL("Apa_V_mp_h_03_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_03_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_04_A")
				REQUEST_IPL("Apa_V_mp_h_04_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_04_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_04_A")
				REMOVE_IPL("Apa_V_mp_h_04_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_04_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_05_A")
				REQUEST_IPL("Apa_V_mp_h_05_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_05_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_05_A")
				REMOVE_IPL("Apa_V_mp_h_05_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_05_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_06_A")
				REQUEST_IPL("Apa_V_mp_h_06_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_06_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_06_A")
				REMOVE_IPL("Apa_V_mp_h_06_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_06_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_07_A")
				REQUEST_IPL("Apa_V_mp_h_07_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_07_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_07_A")
				REMOVE_IPL("Apa_V_mp_h_07_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_07_A")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_2
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_08_A")
				REQUEST_IPL("Apa_V_mp_h_08_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_08_A")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_08_A")
				REMOVE_IPL("Apa_V_mp_h_08_A")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_08_A")
			ENDIF
		ENDIF
		
		
		//property
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_01_B")
				REQUEST_IPL("Apa_V_mp_h_01_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_01_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_01_B")
				REMOVE_IPL("Apa_V_mp_h_01_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_01_B")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_02_B")
				REQUEST_IPL("Apa_V_mp_h_02_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_02_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_02_B")
				REMOVE_IPL("Apa_V_mp_h_02_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_02_B")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_03_B")
				REQUEST_IPL("Apa_V_mp_h_03_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_03_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_03_B")
				REMOVE_IPL("Apa_V_mp_h_03_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_03_B")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_04_B")
				REQUEST_IPL("Apa_V_mp_h_04_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_04_B")
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iInteriorBS3, 24) //CONST_INT INTERIOR3_CUSTOM_HIGH_END_APARTMENT		24
				IF IS_IPL_ACTIVE("Apa_V_mp_h_04_B")
					REMOVE_IPL("Apa_V_mp_h_04_B")
					CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_04_B")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: leaving Apa_V_mp_h_04_B as it is used on mission")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_05_B")
				REQUEST_IPL("Apa_V_mp_h_05_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_05_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_05_B")
				REMOVE_IPL("Apa_V_mp_h_05_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_05_B")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_06_B")
				REQUEST_IPL("Apa_V_mp_h_06_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_06_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_06_B")
				REMOVE_IPL("Apa_V_mp_h_06_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_06_B")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_07_B")
				REQUEST_IPL("Apa_V_mp_h_07_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_07_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_07_B")
				REMOVE_IPL("Apa_V_mp_h_07_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_07_B")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_3
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_08_B")
				REQUEST_IPL("Apa_V_mp_h_08_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_08_B")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_08_B")
				REMOVE_IPL("Apa_V_mp_h_08_B")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_08_B")
			ENDIF
		ENDIF
		//property = PROPERTY_CUSTOM_APT_1_BASE
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_01_C")
				REQUEST_IPL("Apa_V_mp_h_01_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_01_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_01_C")
				REMOVE_IPL("Apa_V_mp_h_01_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_01_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_02_C")
				REQUEST_IPL("Apa_V_mp_h_02_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_02_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_02_C")
				REMOVE_IPL("Apa_V_mp_h_02_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_02_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_03_C")
				REQUEST_IPL("Apa_V_mp_h_03_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_03_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_03_C")
				REMOVE_IPL("Apa_V_mp_h_03_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_03_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_04_C")
				REQUEST_IPL("Apa_V_mp_h_04_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_04_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_04_C")
				REMOVE_IPL("Apa_V_mp_h_04_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_04_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_05_C")
				REQUEST_IPL("Apa_V_mp_h_05_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_05_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_05_C")
				REMOVE_IPL("Apa_V_mp_h_05_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_05_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_06_C")
				REQUEST_IPL("Apa_V_mp_h_06_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_06_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_06_C")
				REMOVE_IPL("Apa_V_mp_h_06_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_06_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_07_C")
				REQUEST_IPL("Apa_V_mp_h_07_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_07_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_07_C")
				REMOVE_IPL("Apa_V_mp_h_07_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_07_C")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_CUSTOM_APT_1_BASE
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("Apa_V_mp_h_08_C")
				REQUEST_IPL("Apa_V_mp_h_08_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating Apa_V_mp_h_08_C")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("Apa_V_mp_h_08_C")
				REMOVE_IPL("Apa_V_mp_h_08_C")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing Apa_V_mp_h_08_C")
			ENDIF
		ENDIF
	ENDIF
	IF IS_PROPERTY_OFFICE(iProperty)
	OR IS_PROPERTY_OFFICE_GARAGE(iProperty) //turn off office ipls when in garage
	OR iProperty = -1
		//PROPERTY_OFFICE_1
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_01a")
				REQUEST_IPL("ex_sm_13_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_01a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_01a")
				REMOVE_IPL("ex_sm_13_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_01a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_01b")
				REQUEST_IPL("ex_sm_13_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_01b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_01b")
				REMOVE_IPL("ex_sm_13_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_01b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_01c")
				REQUEST_IPL("ex_sm_13_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_01c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_01c")
				REMOVE_IPL("ex_sm_13_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_01c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_02a")
				REQUEST_IPL("ex_sm_13_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_02a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_02a")
				REMOVE_IPL("ex_sm_13_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_02a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_02b")
				REQUEST_IPL("ex_sm_13_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_02b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_02b")
				REMOVE_IPL("ex_sm_13_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_02b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_02c")
				REQUEST_IPL("ex_sm_13_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_02c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_02c")
				REMOVE_IPL("ex_sm_13_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_02c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_03a")
				REQUEST_IPL("ex_sm_13_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_03a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_03a")
				REMOVE_IPL("ex_sm_13_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_03a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_03b")
				REQUEST_IPL("ex_sm_13_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_03b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_03b")
				REMOVE_IPL("ex_sm_13_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_03b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1
		AND iVariation = 9
			IF NOT IS_IPL_ACTIVE("ex_sm_13_office_03c")
				REQUEST_IPL("ex_sm_13_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_13_office_03c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_13_office_03c")
				REMOVE_IPL("ex_sm_13_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_13_office_03c")
			ENDIF
		ENDIF
		//PROPERTY_OFFICE_2_BASE
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_01a")
				REQUEST_IPL("ex_sm_15_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_01a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_01a")
				REMOVE_IPL("ex_sm_15_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_01a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_01b")
				REQUEST_IPL("ex_sm_15_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_01b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_01b")
				REMOVE_IPL("ex_sm_15_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_01b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_01c")
				REQUEST_IPL("ex_sm_15_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_01c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_01c")
				REMOVE_IPL("ex_sm_15_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_01c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_02a")
				REQUEST_IPL("ex_sm_15_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_02a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_02a")
				REMOVE_IPL("ex_sm_15_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_02a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_02b")
				REQUEST_IPL("ex_sm_15_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_02b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_02b")
				REMOVE_IPL("ex_sm_15_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_02b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_02c")
				REQUEST_IPL("ex_sm_15_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_02c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_02c")
				REMOVE_IPL("ex_sm_15_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_02c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_03a")
				REQUEST_IPL("ex_sm_15_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_03a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_03a")
				REMOVE_IPL("ex_sm_15_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_03a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_03b")
				REQUEST_IPL("ex_sm_15_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_03b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_03b")
				REMOVE_IPL("ex_sm_15_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_03b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_BASE
		AND iVariation = 9
			IF NOT IS_IPL_ACTIVE("ex_sm_15_office_03c")
				REQUEST_IPL("ex_sm_15_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_sm_15_office_03c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_sm_15_office_03c")
				REMOVE_IPL("ex_sm_15_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_sm_15_office_03c")
			ENDIF
		ENDIF
		//PROPERTY_OFFICE_3
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_01a")
				REQUEST_IPL("ex_dt1_02_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_01a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_01a")
				REMOVE_IPL("ex_dt1_02_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_01a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_01b")
				REQUEST_IPL("ex_dt1_02_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_01b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_01b")
				REMOVE_IPL("ex_dt1_02_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_01b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_01c")
				REQUEST_IPL("ex_dt1_02_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_01c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_01c")
				REMOVE_IPL("ex_dt1_02_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_01c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_02a")
				REQUEST_IPL("ex_dt1_02_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_02a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_02a")
				REMOVE_IPL("ex_dt1_02_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_02a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_02b")
				REQUEST_IPL("ex_dt1_02_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_02b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_02b")
				REMOVE_IPL("ex_dt1_02_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_02b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_02c")
				REQUEST_IPL("ex_dt1_02_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_02c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_02c")
				REMOVE_IPL("ex_dt1_02_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_02c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_03a")
				REQUEST_IPL("ex_dt1_02_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_03a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_03a")
				REMOVE_IPL("ex_dt1_02_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_03a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_03b")
				REQUEST_IPL("ex_dt1_02_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_03b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_03b")
				REMOVE_IPL("ex_dt1_02_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_03b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3
		AND iVariation = 9
			IF NOT IS_IPL_ACTIVE("ex_dt1_02_office_03c")
				REQUEST_IPL("ex_dt1_02_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_02_office_03c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_02_office_03c")
				REMOVE_IPL("ex_dt1_02_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_02_office_03c")
			ENDIF
		ENDIF
		//PROPERTY_OFFICE_4
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 1
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_01a")
				REQUEST_IPL("ex_dt1_11_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_01a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_01a")
				REMOVE_IPL("ex_dt1_11_office_01a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_01a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 2
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_01b")
				REQUEST_IPL("ex_dt1_11_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_01b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_01b")
				REMOVE_IPL("ex_dt1_11_office_01b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_01b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 3
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_01c")
				REQUEST_IPL("ex_dt1_11_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_01c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_01c")
				REMOVE_IPL("ex_dt1_11_office_01c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_01c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 4
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_02a")
				REQUEST_IPL("ex_dt1_11_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_02a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_02a")
				REMOVE_IPL("ex_dt1_11_office_02a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_02a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 5
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_02b")
				REQUEST_IPL("ex_dt1_11_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_02b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_02b")
				REMOVE_IPL("ex_dt1_11_office_02b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_02b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 6
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_02c")
				REQUEST_IPL("ex_dt1_11_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_02c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_02c")
				REMOVE_IPL("ex_dt1_11_office_02c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_02c")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 7
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_03a")
				REQUEST_IPL("ex_dt1_11_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_03a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_03a")
				REMOVE_IPL("ex_dt1_11_office_03a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_03a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 8
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_03b")
				REQUEST_IPL("ex_dt1_11_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_03b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_03b")
				REMOVE_IPL("ex_dt1_11_office_03b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_03b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4
		AND iVariation = 9
			IF NOT IS_IPL_ACTIVE("ex_dt1_11_office_03c")
				REQUEST_IPL("ex_dt1_11_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating ex_dt1_11_office_03c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("ex_dt1_11_office_03c")
				REMOVE_IPL("ex_dt1_11_office_03c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing ex_dt1_11_office_03c")
			ENDIF
		ENDIF
//		IF IS_IPL_ACTIVE("imp_sm_13_modgarage")
//            REMOVE_IPL("imp_sm_13_modgarage")
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_13_modgarage")
//        ENDIF
//		IF IS_IPL_ACTIVE("imp_sm_15_modgarage")
//            REMOVE_IPL("imp_sm_15_modgarage")
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_1_modgarage")
//        ENDIF
//		IF IS_IPL_ACTIVE("imp_dt1_02_modgarage")
//            REMOVE_IPL("imp_dt1_02_modgarage")
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_02_modgarage")
//        ENDIF
//		IF IS_IPL_ACTIVE("imp_dt1_11_modgarage")
//            REMOVE_IPL("imp_dt1_11_modgarage")
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_11_modgarage")
//        ENDIF
	ENDIF
	IF IS_PROPERTY_OFFICE_GARAGE(iProperty)
	OR iS_PROPERTY_OFFICE(iProperty) //to turn off these ipls when in office.
	OR iProperty = -1
		//PROPERTY_OFFICE_1
		IF (iProperty = PROPERTY_OFFICE_1_GARAGE_LVL1
		OR iProperty = PROPERTY_OFFICE_1_GARAGE_LVL2
		OR iProperty = PROPERTY_OFFICE_1_GARAGE_LVL3)
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_13_modgarage")
				REQUEST_IPL("imp_sm_13_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_13_modgarage")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_13_modgarage")
                REMOVE_IPL("imp_sm_13_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_13_modgarage")
            ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL1
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_13_cargarage_a")
				REQUEST_IPL("imp_sm_13_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_13_cargarage_a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_13_cargarage_a")
				REMOVE_IPL("imp_sm_13_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_13_cargarage_a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL2
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_13_cargarage_b")
				REQUEST_IPL("imp_sm_13_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_13_cargarage_b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_13_cargarage_b")
				REMOVE_IPL("imp_sm_13_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_13_cargarage_b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL3
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_13_cargarage_c")
				REQUEST_IPL("imp_sm_13_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_13_cargarage_c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_13_cargarage_c")
				REMOVE_IPL("imp_sm_13_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_13_cargarage_c")
			ENDIF
		ENDIF
		
		IF (iProperty = PROPERTY_OFFICE_2_GARAGE_LVL1
		OR iProperty = PROPERTY_OFFICE_2_GARAGE_LVL2
		OR iProperty = PROPERTY_OFFICE_2_GARAGE_LVL3)
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_15_modgarage")
				REQUEST_IPL("imp_sm_15_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_15_modgarage")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_15_modgarage")
                REMOVE_IPL("imp_sm_15_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_1_modgarage")
            ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_GARAGE_LVL1
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_15_cargarage_a")
				REQUEST_IPL("imp_sm_15_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_15_cargarage_a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_15_cargarage_a")
				REMOVE_IPL("imp_sm_15_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_15_cargarage_a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_GARAGE_LVL2
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_15_cargarage_b")
				REQUEST_IPL("imp_sm_15_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_15_cargarage_b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_15_cargarage_b")
				REMOVE_IPL("imp_sm_15_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_15_cargarage_b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_2_GARAGE_LVL3
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_sm_15_cargarage_c")
				REQUEST_IPL("imp_sm_15_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_sm_15_cargarage_c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_sm_15_cargarage_c")
				REMOVE_IPL("imp_sm_15_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_sm_15_cargarage_c")
			ENDIF
		ENDIF
		
		IF (iProperty = PROPERTY_OFFICE_3_GARAGE_LVL1
		OR iProperty = PROPERTY_OFFICE_3_GARAGE_LVL2
		OR iProperty = PROPERTY_OFFICE_3_GARAGE_LVL3)
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_02_modgarage")
				REQUEST_IPL("imp_dt1_02_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_02_modgarage")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_02_modgarage")
                REMOVE_IPL("imp_dt1_02_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_02_modgarage")
            ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3_GARAGE_LVL1
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_02_cargarage_a")
				REQUEST_IPL("imp_dt1_02_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_02_cargarage_a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_02_cargarage_a")
				REMOVE_IPL("imp_dt1_02_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_02_cargarage_a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3_GARAGE_LVL2
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_02_cargarage_b")
				REQUEST_IPL("imp_dt1_02_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_02_cargarage_b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_02_cargarage_b")
				REMOVE_IPL("imp_dt1_02_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_02_cargarage_b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_3_GARAGE_LVL3
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_02_cargarage_c")
				REQUEST_IPL("imp_dt1_02_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_02_cargarage_c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_02_cargarage_c")
				REMOVE_IPL("imp_dt1_02_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_02_cargarage_c")
			ENDIF
		ENDIF
		
		IF (iProperty = PROPERTY_OFFICE_4_GARAGE_LVL1
		OR iProperty = PROPERTY_OFFICE_4_GARAGE_LVL2
		OR iProperty = PROPERTY_OFFICE_4_GARAGE_LVL3)
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_11_modgarage")
				REQUEST_IPL("imp_dt1_11_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_11_modgarage")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_11_modgarage")
                REMOVE_IPL("imp_dt1_11_modgarage")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_11_modgarage")
            ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4_GARAGE_LVL1
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_11_cargarage_a")
				REQUEST_IPL("imp_dt1_11_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_11_cargarage_a")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_11_cargarage_a")
				REMOVE_IPL("imp_dt1_11_cargarage_a")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_11_cargarage_a")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4_GARAGE_LVL2
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_11_cargarage_b")
				REQUEST_IPL("imp_dt1_11_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_11_cargarage_b")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_11_cargarage_b")
				REMOVE_IPL("imp_dt1_11_cargarage_b")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_11_cargarage_b")
			ENDIF
		ENDIF
		IF iProperty = PROPERTY_OFFICE_4_GARAGE_LVL3
		AND iVariation != -1
			IF NOT IS_IPL_ACTIVE("imp_dt1_11_cargarage_c")
				REQUEST_IPL("imp_dt1_11_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: activating imp_dt1_11_cargarage_c")
			ENDIF
		ELSE
			IF IS_IPL_ACTIVE("imp_dt1_11_cargarage_c")
				REMOVE_IPL("imp_dt1_11_cargarage_c")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY: removing imp_dt1_11_cargarage_c")
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAINTAIN_PROPERTY_IPLS_DEACTIVATION()
	// Clean external IPLs if needed
	IF g_bPropertyIPLSNeedUnloaded
		CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_MP_PROPERTIES - external ipls havent been cleaned up yet")
		IF ARE_ALL_PROPERTY_EXTRA_IPLS_UNLOADED()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_MP_PROPERTIES - external ipls cleaned up")
			g_bPropertyIPLSNeedUnloaded = FALSE
		ENDIF
	ENDIF
	IF IS_BIT_SET(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_CUSTOM_INT_NEED_DISABLED)
		IF g_bPropInteriorScriptRunning
			CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_CUSTOM_INT_NEED_DISABLED)
			PRINTLN("MP_PROP_MAINTAIN_BS_CUSTOM_INT_NEED_DISABLED - CLEARED - interior script running")
		ELSE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
			OR IS_PLAYER_TELEPORT_ACTIVE()
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY- cant unload, load scene active")
				EXIT
			ENDIF
			ACTIVATE_IPLS_FOR_CUSTOM_PROPERTY(-1, -1)
			CLEAR_BIT(mpPropMaintain.iBS, MP_PROP_MAINTAIN_BS_CUSTOM_INT_NEED_DISABLED)
			PRINTLN("MP_PROP_MAINTAIN_BS_CUSTOM_INT_NEED_DISABLED - CLEARED")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_MP_PROPERTY_PROP_FLAGS()

	INT iBankBalance 
	INT iWalletBalance
	
//	PRINTLN("CDM TEMP: START g_iOfficePropsStage = ",g_iOfficePropsStage)
//	PRINTLN("CDM TEMP: START g_iOfficePropsStageGenLoop = ",g_iOfficePropsStageGenLoop)
//	PRINTLN("CDM TEMP: START g_iOfficePropsStageLevelToSet = ",g_iOfficePropsStageLevelToSet)
	// Owned warehouses
	IF g_iOfficePropsStage = 0
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[0] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[0].iWarehouse
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[1] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[1].iWarehouse
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[2] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[2].iWarehouse
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[3] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[3].iWarehouse
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[4] = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdWarehouseData[4].iWarehouse
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[5] = ENUM_TO_INT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdIEgarageData.eGarage)
		
		// Debug override
		#IF IS_DEBUG_BUILD
			IF g_bDebug_OfficeProp_Override
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[0] = g_iDebug_Warehouse_Override[0]
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[1] = g_iDebug_Warehouse_Override[1]
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[2] = g_iDebug_Warehouse_Override[2]
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[3] = g_iDebug_Warehouse_Override[3]
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[4] = g_iDebug_Warehouse_Override[4]
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeProp_Warehouses[5] = g_iDebug_Warehouse_Override[5]
			ENDIF
		#ENDIF
		g_iOfficePropsStage++
		g_iOfficePropsStageGenLoop = 1
		g_iOfficePropsStageLevelToSet = 0
	// Cash unlocks
	ELIF g_iOfficePropsStage = 1
		
		INT iUnlockLevel = GET_PACKED_STAT_INT(PACKED_MP_INT_OFFICE_CASH_PILES_UNLOCKED)
		
		IF iUnlockLevel > 0
			iBankBalance = NETWORK_GET_VC_BANK_BALANCE()
			iWalletBalance = NETWORK_GET_VC_WALLET_BALANCE()
			IF iBankBalance+iWalletBalance >= GET_CASH_PILE_UNLOCK_LEVEL(g_iOfficePropsStageGenLoop)
				g_iOfficePropsStageLevelToSet = g_iOfficePropsStageGenLoop
			ELIF NETWORK_CAN_SPEND_MONEY(GET_CASH_PILE_UNLOCK_LEVEL(g_iOfficePropsStageGenLoop), FALSE, TRUE, FALSE)
				g_iOfficePropsStageLevelToSet = g_iOfficePropsStageGenLoop
				SCRIPT_ASSERT("MAINTAIN_MP_PROPERTY_PROP_FLAGS - Use native cash check. Tell Kenneth R.")
			ENDIF
		ENDIF

		// Debug override
		#IF IS_DEBUG_BUILD
			IF g_bDebug_OfficeProp_Override
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Cash != g_iDebug_Cash_Override
					PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - Changing cash level to ", g_iOfficePropsStageGenLoop, ", *DEBUG*")
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Cash = g_iDebug_Cash_Override
				ENDIF
			ENDIF
		#ENDIF
		
		g_iOfficePropsStageGenLoop++
		IF g_iOfficePropsStageGenLoop >= iUnlockLevel
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Cash != g_iOfficePropsStageLevelToSet
				PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - Changing cash level to ", g_iOfficePropsStageLevelToSet, ", iUnlockLevel=", iUnlockLevel, ", iBankBalance+iWalletBalance=", iBankBalance+iWalletBalance)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Cash = g_iOfficePropsStageLevelToSet
			ENDIF
			g_iOfficePropsStage++
			g_iOfficePropsStageGenLoop = 0
			g_iOfficePropsStageLevelToSet = 0
		ENDIF
	// Contraband unlocks
	ELIF g_iOfficePropsStage = 2

		IF GET_PACKED_STAT_BOOL(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_BOOL_CONTRABAND_SWAG_SILVER)+g_iOfficePropsStageGenLoop))
			IF g_iOfficePropsStageGenLoop != 19 // Block Swag_Gems 2 for url:bugstar:2882310 - [PUBLIC] [REPORTED] Players are often stuck when exiting office chair in office.
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Contraband[g_iOfficePropsStageGenLoop/32], g_iOfficePropsStageGenLoop%32)
			ENDIF
		ELSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Contraband[g_iOfficePropsStageGenLoop/32], g_iOfficePropsStageGenLoop%32)
		ENDIF

		
		// Debug override
		#IF IS_DEBUG_BUILD
			IF g_bDebug_OfficeProp_Override
				INT i, j
				FOR i = 0 TO 13
					FOR j = 0 TO 2
						IF g_iDebug_Contraband_Override[i] >= (j+1)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Contraband[(j+(i*3))/32], (j+(i*3))%32)
						ELSE
							CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iOfficeES_Contraband[(j+(i*3))/32], (j+(i*3))%32)
						ENDIF
					ENDFOR
				ENDFOR
			ENDIF
		#ENDIF
		
		g_iOfficePropsStageGenLoop++
		IF g_iOfficePropsStageGenLoop >= 42
			g_iOfficePropsStage++
			g_iOfficePropsStageGenLoop = 1
			g_iOfficePropsStageLevelToSet = 0
		ENDIF
	// Clubhouse cash unlocks
	ELIF g_iOfficePropsStage = 3
		INT iUnlockLevel = GET_PACKED_STAT_INT(PACKED_MP_INT_CH_CASH_PILES_UNLOCKED)
		IF iUnlockLevel > 0
			iBankBalance = NETWORK_GET_VC_BANK_BALANCE()
			iWalletBalance = NETWORK_GET_VC_WALLET_BALANCE()
			IF iBankBalance+iWalletBalance >= GET_CASH_PILE_UNLOCK_LEVEL_FOR_CLUBHOUSE(g_iOfficePropsStageGenLoop)
				g_iOfficePropsStageLevelToSet = g_iOfficePropsStageGenLoop
			ELIF NETWORK_CAN_SPEND_MONEY(GET_CASH_PILE_UNLOCK_LEVEL_FOR_CLUBHOUSE(g_iOfficePropsStageGenLoop), FALSE, TRUE, FALSE)
				g_iOfficePropsStageLevelToSet = g_iOfficePropsStageGenLoop
				SCRIPT_ASSERT("MAINTAIN_MP_PROPERTY_PROP_FLAGS [2] - Use native cash check. Tell Kenneth R.")
			ENDIF
		ENDIF
		
		
		// Debug override
		#IF IS_DEBUG_BUILD
			IF g_bDebug_OfficeProp_Override
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Cash != g_iDebug_Cash_Override
					PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - Changing clubhouse cash level to ", g_iDebug_Cash_Override, ", *DEBUG*")
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Cash = g_iDebug_Cash_Override
				ENDIF
			ENDIF
		#ENDIF
		g_iOfficePropsStageGenLoop++
		IF g_iOfficePropsStageGenLoop >= iUnlockLevel
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Cash != g_iOfficePropsStageLevelToSet
				PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - Changing clubhouse cash level to ", g_iOfficePropsStageLevelToSet, ", iUnlockLevel=", iUnlockLevel, ", iBankBalance+iWalletBalance=", iBankBalance+iWalletBalance)
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Cash = g_iOfficePropsStageLevelToSet
			ENDIF
			g_iOfficePropsStage++
			g_iOfficePropsStageGenLoop = 0
			g_iOfficePropsStageLevelToSet = 0
		ENDIF
	// Biker unlocks
	ELIF g_iOfficePropsStage = 4

		//FOR iFactorySlot = 0 TO ciMAX_OWNED_FACTORIES-1
		IF IS_FACTORY_PRODUCTION_ACTIVE(PLAYER_ID(), g_iOfficePropsStageGenLoop)
			SWITCH GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), g_iOfficePropsStageGenLoop))
				CASE FACTORY_TYPE_METH
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_METH_1)
						PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - METH production started!")
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_METH_1, TRUE)
					ENDIF
				BREAK
				CASE FACTORY_TYPE_WEED
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_WEED_1)
						PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - WEED production started!")
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_WEED_1, TRUE)
					ENDIF
				BREAK
				CASE FACTORY_TYPE_CRACK
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_CRACK_1)
						PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - CRACK production started!")
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_CRACK_1, TRUE)
					ENDIF
				BREAK
				CASE FACTORY_TYPE_FAKE_MONEY
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_CASH_1)
						PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - CASH production started!")
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_CASH_1, TRUE)
					ENDIF
				BREAK
				CASE FACTORY_TYPE_FAKE_IDS
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_IDS_1)
						PRINTLN("MAINTAIN_MP_PROPERTY_PROP_FLAGS - IDS production started!")
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MC_SWAG_IDS_1, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF

		g_iOfficePropsStageGenLoop++
		IF g_iOfficePropsStageGenLoop >= ciMAX_OWNED_FACTORIES
			g_iOfficePropsStage++
			g_iOfficePropsStageGenLoop = 0
		ENDIF
	// Clubhouse contraband unlocks
	ELIF g_iOfficePropsStage = 5
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Contraband[0] = 0
		
		IF GET_PACKED_STAT_BOOL(INT_TO_ENUM(STATS_PACKED, ENUM_TO_INT(PACKED_MP_BOOL_MC_SWAG_WEED_1)+g_iOfficePropsStageGenLoop))
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Contraband[g_iOfficePropsStageGenLoop/32], g_iOfficePropsStageGenLoop%32)
		ELSE
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Contraband[g_iOfficePropsStageGenLoop/32], g_iOfficePropsStageGenLoop%32)
		ENDIF

		
		// Debug override
		#IF IS_DEBUG_BUILD
			IF g_bDebug_ClubhouseProp_Override
				INT i, j
				FOR i = 0 TO 4
					FOR j = 0 TO 2
						IF g_iDebug_CHContraband_Override[i] >= (j+1)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Contraband[(j+(i*3))/32], (j+(i*3))%32)
						ELSE
							CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iClubhouseES_Contraband[(j+(i*3))/32], (j+(i*3))%32)
						ENDIF
					ENDFOR
				ENDFOR
			ENDIF
		#ENDIF
		g_iOfficePropsStageGenLoop++
		IF g_iOfficePropsStageGenLoop >= 15
			g_iOfficePropsStage++
			g_iOfficePropsStageGenLoop = 0
		ENDIF
	ENDIF

//	PRINTLN("CDM TEMP: g_iOfficePropsStage = ",g_iOfficePropsStage)
//	PRINTLN("CDM TEMP: g_iOfficePropsStageGenLoop = ",g_iOfficePropsStageGenLoop)
//	PRINTLN("CDM TEMP: g_iOfficePropsStageLevelToSet = ",g_iOfficePropsStageLevelToSet)
	IF g_iOfficePropsStage > 5
		g_iOfficePropsStage = 0
		g_iOfficePropsStageGenLoop = 0
		//tdOfficePropsTimer = GET_NETWORK_TIME()
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_CLUBHOUSE(PLAYER_INDEX playerID)

	IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_INSIDE_CLUBHOUSE_THEY_OWN(PLAYER_INDEX playerID)

	IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iCurrentlyInsideProperty)
	AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DISABLE_CUSTOM_WEAPON_LOADOUT_FOR_MISSIONS()
	IF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
	AND IS_OFFICE_GUN_LOCKER_PURCHASED()
	OR
	(DOES_PLAYER_OWN_CLUBHOUSE(PLAYER_ID())
	AND IS_CLUBHOUSE_GUN_LOCKER_PURCHASED())
	OR (DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
	AND IS_BUNKER_GUNLOCKER_PURCHASED())
	OR (IS_GUNRUNNING_TRUCK_PURCHASED()
	AND IS_PLAYER_PURCHASED_GUN_LOCKER_FOR_TRUCK(PLAYER_ID()))
	OR (DOES_PLAYER_OWN_A_DEFUNCT_BASE(PLAYER_ID())
	AND IS_DEFUNCT_BASE_SECURITY_ROOM_PURCHASED())
	OR (DOES_PLAYER_OWN_A_NIGHTCLUB(PLAYER_ID())
	AND IS_NIGHTCLUB_SECURITY_PURCHASED())
	OR DOES_PLAYER_OWN_AN_ARENA_GARAGE(PLAYER_ID())
		IF g_bEnableCustomWeaponLoadout
			INT iWeapon, iTotal
			iTotal = COUNT_OF(WEAPON_BITSET)
			FOR iWeapon = 0 TO iTotal
				WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, iWeapon))
				IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
					IF NOT IS_WEAPON_AN_MK2_WEAPON(eWeaponType)
						IF IS_WEAPONTYPE_MK2_UPGRADABLE(eWeaponType)
						AND IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, eWeaponType)
							eWeaponType = GET_MK2_WEAPON_TYPE_FROM_MK1_WEAPON(eWeaponType)
						ENDIF
						IF (IS_MP_WEAPON_PURCHASED(eWeaponType))
						OR (eWeaponType = WEAPONTYPE_MOLOTOV
						AND GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), eWeaponType) > 0)
							// Show all purchased weapons
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),eWeaponType,0,FALSE,FALSE)
								GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(eWeaponType)
								GIVE_WEAPON_EQUIPPED_TINT(eWeaponType)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			g_bEnableCustomWeaponLoadout = FALSE
			PRINTLN("DISABLE_CUSTOM_WEAPON_LOADOUT_FOR_MISSIONS - set g_bEnableCustomWeaponLoadout = FALSE")
		ENDIF
	ENDIF	
ENDPROC					

PROC CONCEAL_MY_ACTIVE_PERSONAL_VEHICLE()
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
ENDPROC


/// PURPOSE:
///    Requests parking for personal vehicle from a particular player
/// PARAMS:
///    ownerID - owner of property you are requesting
///    iPropertyRequestedFor - Property ID
///    iReturnSlot - slot player can park
FUNC BOOL REQUEST_GUEST_PARKING_FROM_PLAYER(PLAYER_INDEX ownerID,INT iPropertyRequestedFor, INT& iReturnSlot, INT iTypeID = PV_CONCEAL_TYPE_CLUBHOUSE)
	INT i
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = NATIVE_TO_INT(ownerID)
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = iPropertyRequestedFor
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = iTypeID
	PRINTLN("REQUEST_GUEST_PARKING_FROM_PLAYER: requesting group parking access from ",GET_PLAYER_NAME(ownerID), " property #",iPropertyRequestedFor, " iType = ", iTypeID)
	REPEAT MAX_GUEST_PARKING i
		IF GlobalplayerBD_FM[NATIVE_TO_INT(ownerID)].propertyDetails.iGuestParking[i] = NATIVE_TO_INT(PLAYER_ID())
			SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
			iReturnSlot = i
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC SET_CONCEAL_CURRENT_PERSONAL_VEHICLE(PLAYER_INDEX ownerID,INT iConcealID, INT iTypeID)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInPlayerOwnedLocation = NATIVE_TO_INT(ownerID)
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CONCEAL_MY_PV)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInID = iConcealID
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPVConcealInType = iTypeID
	SET_SAVED_VEHICLE_FLAG(MP_SAVED_VEH_FLAG_CONCEAL_IN_PROPERTY)
	PRINTLN("SET_CONCEAL_CURRENT_PERSONAL_VEHICLE: setting current personal vehicle to be concealed for ",GET_PLAYER_NAME(ownerID), " conceal ID #",iConcealID, " iTypeID = ",iTypeID)
ENDPROC



FUNC BOOL IS_THERE_GUEST_PARKING_AVAILABLE_FROM_PLAYER(PLAYER_INDEX ownerID,BOOL bIncludeLocalAssignedSlot= FALSE )
	INT i
	REPEAT MAX_GUEST_PARKING i
		IF GlobalplayerBD_FM[NATIVE_TO_INT(ownerID)].propertyDetails.iGuestParking[i] = NATIVE_TO_INT(PLAYER_ID())
			IF bIncludeLocalAssignedSlot
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		ELIF GlobalplayerBD_FM[NATIVE_TO_INT(ownerID)].propertyDetails.iGuestParking[i] = -1
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC


PROC INIT_GUEST_PARKING_DATA()
	INT i
	REPEAT MAX_GUEST_PARKING i
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iGuestParking[i] = -1
	ENDREPEAT
	PRINTLN("INIT_GUEST_PARKING_DATA- Guest parking intialised")
ENDPROC

PROC MAINTAIN_GUEST_PARKING_REQUESTS()
	INT j
	BOOL bAlreadyHasParking
	INT iLocalPlayer = NATIVE_TO_INT(PLAYER_ID()) 
	//REPEAT NUM_NETWORK_PLAYERS i
		IF GlobalplayerBD_FM[mpPropMaintain.iGuestParkingSlowLoop].propertyDetails.iPVConcealInPlayerOwnedLocation = iLocalPlayer
		AND IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(mpPropMaintain.iGuestParkingSlowLoop),FALSE)
			bAlreadyHasParking = FALSE
			
			REPEAT MAX_GUEST_PARKING j
				IF GlobalplayerBD_FM[iLocalPlayer].propertyDetails.iGuestParking[j] = mpPropMaintain.iGuestParkingSlowLoop
					bAlreadyHasParking = TRUE
					PRINTLN("MAINTAIN_GUEST_PARKING_REQUESTS: player ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(mpPropMaintain.iGuestParkingSlowLoop)), "already has parking")
				ENDIF
			ENDREPEAT
			IF NOT bAlreadyHasParking 
				j = 0
				REPEAT MAX_GUEST_PARKING j
					IF GlobalplayerBD_FM[iLocalPlayer].propertyDetails.iGuestParking[j] = -1
						GlobalplayerBD_FM[iLocalPlayer].propertyDetails.iGuestParking[j] = mpPropMaintain.iGuestParkingSlowLoop
						PRINTLN("MAINTAIN_GUEST_PARKING_REQUESTS: player ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(mpPropMaintain.iGuestParkingSlowLoop)), "has been given guest parking spot #",j)
						EXIT
					ENDIF
				ENDREPEAT
			ENDIF
		ELSE
			REPEAT MAX_GUEST_PARKING j
				IF GlobalplayerBD_FM[iLocalPlayer].propertyDetails.iGuestParking[j] = mpPropMaintain.iGuestParkingSlowLoop
					GlobalplayerBD_FM[iLocalPlayer].propertyDetails.iGuestParking[j] = -1
					#IF IS_DEBUG_BUILD
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(mpPropMaintain.iGuestParkingSlowLoop),FALSE)
						PRINTLN("MAINTAIN_GUEST_PARKING_REQUESTS: player ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(mpPropMaintain.iGuestParkingSlowLoop)), " has been removed from guest parking spot #",j)
					ELSE
						PRINTLN("MAINTAIN_GUEST_PARKING_REQUESTS: removed guest from parking spot #",mpPropMaintain.iGuestParkingSlowLoop)
					ENDIF
					#ENDIF
					EXIT
				ENDIF
			ENDREPEAT
		ENDIF
	mpPropMaintain.iGuestParkingSlowLoop++
	IF mpPropMaintain.iGuestParkingSlowLoop >= NUM_NETWORK_PLAYERS
		mpPropMaintain.iGuestParkingSlowLoop = 0
	ENDIF
	//ENDREPEAT
ENDPROC

PROC RESET_ALL_PROPERTY_UTILITY_CHARGE_TIMERS()
	RESET_NET_TIMER(mpPropMaintain.mechanicChargeTimer)
	RESET_NET_TIMER(mpPropMaintain.utilitiesChargeTimer)
	RESET_NET_TIMER(mpPropMaintain.businessChargeTimer)
ENDPROC


/// PURPOSE:
///    Every function that maintains periodic charges goes here
PROC MAINTAIN_ALL_MAINTENANCE_COSTS()
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RESET_ALL_PROPERTY_UTILITY_CHARGE_TIMERS()
		EXIT
	ENDIF
	#ENDIF
	
	MAINTAIN_COST_OF_MECHANIC()
	MAINTAIN_PROPERTY_UTILITY_CHARGES()
	MAINTAIN_BUSINESS_CHARGES()
	
	#IF IS_DEBUG_BUILD
		IF g_bChargeOwnershipFeesNow
			PRINTLN("[PROP_OWN_FEES] MAINTAIN_ALL_MAINTENANCE_COSTS - Charging players early for: ", g_bChargeOwnershipFeesNow)
			g_bChargeOwnershipFeesNow = FALSE
		ENDIF
	#ENDIF
ENDPROC

//
/// PURPOSE:
///    Maintain custom garage purchasing and using
///    NOTE: I will move functionality over to a world point in new year.
PROC MAINTAIN_MP_PROPERTIES(BOOL bCheckLaunchPropertyInt= TRUE)
	INT iOwnedProperty = GET_OWNED_PROPERTY(0)
	INT i
	BOOL bOwnsAProperty = FALSE
	INT iBaseProperty
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- Start")
//	#ENDIF
//	#ENDIF
	REPEAT MAX_OWNED_PROPERTIES i
		iOwnedProperty = GET_OWNED_PROPERTY(i)
		IF i = 0
			IF iOwnedProperty <= 0
				IF IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_SETUP_MATCHMAKING)
					NETWORK_CLEAR_PROPERTY_ID()
					CLEAR_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_SETUP_MATCHMAKING)
				ENDIF
			ELSE
				bOwnsAProperty = TRUE
				IF IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
					PRINTLN("CDM MP... Player had no property now has one setting default spawning coords.")
					
					SET_LAST_USED_PROPERTY(iOwnedProperty)
					SET_MP_SPAWN_POINT_SETTING(MP_SETTING_SPAWN_PROPERTY)
					CLEAR_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
				ENDIF
			ENDIF
		ELIF i = PROPERTY_OWNED_SLOT_OFFICE_0
			IF iOwnedProperty > 0
				SET_PLAYER_OWNS_OFFICE(TRUE)
				IF DOES_PROPERTY_HAVE_LIVING_QUATERS_ENABLED(GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0) )
					bOwnsAProperty = TRUE
					IF IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
						PRINTLN("CDM MP... Player had no property now has one setting default spawning coords. (OFFICE)")
						
						SET_LAST_USED_PROPERTY(iOwnedProperty)
						SET_MP_SPAWN_POINT_SETTING(MP_SETTING_SPAWN_OFFICE)
						CLEAR_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
					ENDIF
				ENDIF
			ELSE
				IF DOES_PLAYER_OWN_OFFICE(PLAYER_ID())
					SET_PLAYER_OWNS_OFFICE(FALSE)
				ENDIF
			ENDIF
		ELIF i = PROPERTY_OWNED_SLOT_CLUBHOUSE
			IF iOwnedProperty > 0
				bOwnsAProperty = TRUE
				SET_PLAYER_OWNS_CLUBHOUSE(TRUE)
				IF IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
					PRINTLN("CDM MP... Player had no property now has one setting default spawning coords. (CLUBHOUSE)")
					
					SET_LAST_USED_PROPERTY(iOwnedProperty)
					SET_MP_SPAWN_POINT_SETTING(MP_SETTING_SPAWN_CLUBHOUSE)
					CLEAR_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
				ENDIF
				iBaseProperty = GET_BASE_PROPERTY_FROM_PROPERTY(iOwnedProperty)		
				IF iBaseProperty = PROPERTY_CLUBHOUSE_1_BASE_A
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL)
						SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL)
						CPRINTLN(DEBUG_INTERNET, "MAINTAIN_MP_PROPERTIES: PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL TRUE")
					ENDIF
				ELIF iBaseProperty = PROPERTY_CLUBHOUSE_7_BASE_B
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL)
						CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL)
						CPRINTLN(DEBUG_INTERNET, "MAINTAIN_MP_PROPERTIES: PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL FALSE")
					ENDIF
				ELSE
					//
					CASSERTLN(DEBUG_INTERNET, "MAINTAIN_MP_PROPERTIES: PROPERTY_BROADCAST_BS2_CLUBHOUSE_TRADITIONAL unknown")
				ENDIF
			ELSE
				IF DOES_PLAYER_OWN_CLUBHOUSE(PLAYER_ID())
					SET_PLAYER_OWNS_CLUBHOUSE(FALSE)
				ENDIF
			ENDIF
		ENDIF
		//PRINTLN("MAINTAIN_MP_PROPERTIES: checking owned property #",i)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[i] != iOwnedProperty
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOwnedProperty[i] = iOwnedProperty	
			PRINTLN("MAINTAIN_MP_PROPERTIES: Setting propertyDetails.iOwnedProperty[",i,"] = " , iOwnedProperty)
			IF i = 0
				NETWORK_SET_PROPERTY_ID(iOwnedProperty)
				SET_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_SETUP_MATCHMAKING)
			ENDIF
		ENDIF
		iOwnedProperty = GET_OWNED_PROPERTY_VARIATION(i)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPropertyVariant[i] != iOwnedProperty
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iPropertyVariant[i] = iOwnedProperty	
			PRINTLN("MAINTAIN_MP_PROPERTIES: Setting propertyDetails.iVariation[",i,"] = " , iOwnedProperty)
		ENDIF
	ENDREPEAT
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 1")
//	#ENDIF
//	#ENDIF
	IF NOT bOwnsAProperty
	AND NOT IS_BIT_SET(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
		PRINTLN("CDM MP... setting player has no property so when they get one it gets set as spawn.")
		SET_BIT(mpPropMaintain.iBS,MP_PROP_MAINTAIN_BS_PLAYER_HAD_NO_PROP)
	ENDIF
	IF DOES_LOCAL_PLAYER_STAT_OWN_PRIVATE_YACHT()
		IF NOT DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID())	
			SET_YACHT_OWNERSHIP_DETAILS_BASED_ON_STATS()
			PRINTLN("MAINTAIN_MP_PROPERTIES: setting player owns yacht based on stat")
		ENDIF
	ELSE
		IF DOES_PLAYER_OWN_PRIVATE_YACHT(PLAYER_ID())
			SET_LOCAL_PLAYER_OWNS_PRIVATE_YACHT(FALSE)
			PRINTLN("MAINTAIN_MP_PROPERTIES: setting player DOESN'T owns yacht based on stat")
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(disableRadaronPropertyExitDelay)
		IF NOT HAS_NET_TIMER_EXPIRED(disableRadaronPropertyExitDelay,1000,TRUE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			PRINTLN("MAINTAIN_MP_PROPERTIES: HIDE_HUD_AND_RADAR_THIS_FRAME when exiting to allow streaming map to catch up")
			RESET_NET_TIMER(disableRadaronPropertyExitDelay)
		ENDIF
	ENDIF
		
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 2")
//	#ENDIF
//	#ENDIF
	
	MAINTAIN_ALL_MAINTENANCE_COSTS()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 3")
//	#ENDIF
//	#ENDIF
	
	MAINTAIN_GUEST_PARKING_REQUESTS()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 4")
//	#ENDIF
//	#ENDIF
	IF IS_GUNRUNNING_TRUCK_PURCHASED()
		BROADCAST_PLAYER_PURCHASED_ARMORY_TRUCK(TRUE)
	ELSE
		BROADCAST_PLAYER_PURCHASED_ARMORY_TRUCK(FALSE)
	ENDIF
	
	IF IS_BUNKER_STYLE_OPTION_A_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_DECOR_1)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_DECOR_1)
	ENDIF
	
	IF IS_BUNKER_STYLE_OPTION_B_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_DECOR_2)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_DECOR_2)
	ENDIF
	
	IF IS_BUNKER_STYLE_OPTION_C_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_DECOR_3)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_DECOR_3)
	ENDIF
	
	IF IS_BUNKER_STANDARD_EQUIPMENT_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_STANDARD_EQUIPMENT)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_STANDARD_EQUIPMENT)
	ENDIF
	
	IF IS_BUNKER_UPGRADED_EQUIPMENT_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_UPGRADED_EQUIPMENT)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_UPGRADED_EQUIPMENT)
	ENDIF
	
	IF IS_BUNKER_STANDARD_SECURITY_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_STANDARD_SECURITY)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_STANDARD_SECURITY)
	ENDIF
	
	IF IS_BUNKER_UPGRADED_SECURITY_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_UPGRADED_SECURITY)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_UPGRADED_SECURITY)
	ENDIF
	
	IF IS_BUNKER_SAVEBED_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_SAVEBED)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_SAVEBED)
	ENDIF
	
	IF IS_BUNKER_GUNLOCKER_PURCHASED()
		SET_PLAYER_BUNKER_GUNLOCKER_PURCHASED(TRUE)
	ELSE
		SET_PLAYER_BUNKER_GUNLOCKER_PURCHASED(FALSE)
	ENDIF
	
	IF IS_BUNKER_FIRING_RANGE_BLACK_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_BLACK_FIRING_RANGE)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_BLACK_FIRING_RANGE)
	ENDIF
	IF IS_BUNKER_FIRING_RANGE_WHITE_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_WHITE_FIRING_RANGE)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_WHITE_FIRING_RANGE)
	ENDIF
	IF IS_BUNKER_TRANSPORTATION_STANDARD_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_TRANSPORTATION_STANDARD)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_TRANSPORTATION_STANDARD)
	ENDIF
	IF IS_BUNKER_TRANSPORTATION_UPDATED_PURCHASED()
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_TRANSPORTATION_UPDATED)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree,PROPERTY_BROADCAST_BS3_OWNS_BUNKER_TRANSPORTATION_UPDATED)
	ENDIF
	
	iOwnedProperty = GET_OWNED_PROPERTY(0)
	
	IF IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iGeneralBS,MP_SAVE_GENERAL_BS_NOT_PAID_UTILITIES)
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_NOT_PAID_UTILITIES)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_NOT_PAID_UTILITIES)
	ENDIF
	
	MAINTAIN_MP_PROPERTY_BLIPS()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 5")
//	#ENDIF
//	#ENDIF
	IF bCheckLaunchPropertyInt
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())			//fix for 1499940, we dont want sctv players running interior property script
		AND NOT SHOULD_TRANSITION_SESSION_SPAWN_PLAYER_OUTSIDE_PROPERTY()
			MAINTAIN_LAUNCHING_MP_INTERIOR_SCRIPT()
		#IF IS_DEBUG_BUILD
		ELSE
			IF SHOULD_TRANSITION_SESSION_SPAWN_PLAYER_OUTSIDE_PROPERTY()
				PRINTLN("Not launching interior script as SHOULD_TRANSITION_SESSION_SPAWN_PLAYER_OUTSIDE_PROPERTY")
			ENDIF
		#ENDIF
		ENDIF
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 6")
//	#ENDIF
//	#ENDIF
	
	MAINTAIN_PROPERTY_IPLS_DEACTIVATION()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 7")
//	#ENDIF
//	#ENDIF
	IF misRestartStoredPropertyData.iPackedInt > 0
		IF NOT (Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist(PLAYER_ID()) OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))	// KGM 5/3/14: Added Heist Planning mission check too
			IF NOT (IS_TRANSITION_SESSION_RESTARTING()
			OR IS_TRANSITION_SESSION_LAUNCHING())
				IF NOT (IS_TRANSITION_SESSION_IN_CORONA_AFTER_RANDOM_RESTART()
				OR IS_CORONA_INITIALISING_A_QUICK_RESTART())
					misRestartStoredPropertyData.iPackedInt = 0
					CLEAR_GAMER_HANDLE_STRUCT(misRestartStoredPropertyData.ownerHandle)
					PRINTLN("CDM: Clearing misRestartStoredPropertyData player not on a mission that restarts in property")	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	MAINTAIN_LAUNCHING_YACHT_EXT_SCRIPT()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 8")
//	#ENDIF
//	#ENDIF

	MAINTAIN_MP_PROPERTY_VISIBILITY()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 9")
//	#ENDIF
//	#ENDIF
	
	MAINTAIN_MP_PROPERTY_PROP_FLAGS()
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_MP_PROPERTIES- 10")
//	#ENDIF
//	#ENDIF
ENDPROC

PROC CULL_MODEL_NAMES_OF_BUILDING(INT iProperty)
	SET_DISABLE_DECAL_RENDERING_THIS_FRAME()
	MODEL_NAMES tempModel
	INT iHash
	SWITCH iProperty
		CASE PROPERTY_HIGH_APT_1
		CASE PROPERTY_HIGH_APT_2
		CASE PROPERTY_HIGH_APT_3
		CASE PROPERTY_HIGH_APT_4
		CASE PROPERTY_BUS_HIGH_APT_1
		CASE PROPERTY_CUSTOM_APT_1_BASE
		CASE PROPERTY_CUSTOM_APT_2
		CASE PROPERTY_CUSTOM_APT_3
			iHash = GET_HASH_KEY("apa_ss1_11_flats")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)

			iHash = GET_HASH_KEY("ss1_11_ss1_emissive_a")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			
			iHash = GET_HASH_KEY("ss1_11_detail01b")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ss1_11_Flats_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("SS1_02_Building01_LOD") 
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash) 
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel) 
			iHash = GET_HASH_KEY("SS1_LOD_01_02_08_09_10_11") 
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash) 
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel) 
			iHash = GET_HASH_KEY("SS1_02_SLOD1") 
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash) 
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel) 
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		
		CASE PROPERTY_HIGH_APT_5
		CASE PROPERTY_HIGH_APT_6
			iHash = GET_HASH_KEY("hei_dt1_20_build2")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("dt1_20_dt1_emissive_dt1_20")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
			
		CASE PROPERTY_HIGH_APT_7
		CASE PROPERTY_HIGH_APT_8
		CASE PROPERTY_BUS_HIGH_APT_2
			iHash = GET_HASH_KEY("sm_14_emissive")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("hei_sm_14_bld2")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		
		CASE PROPERTY_HIGH_APT_9
		CASE PROPERTY_HIGH_APT_10
		CASE PROPERTY_HIGH_APT_11
			iHash = GET_HASH_KEY("hei_bh1_09_bld_01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			
			iHash = GET_HASH_KEY("bh1_09_ema")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(PROP_WALL_LIGHT_12A)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_HIGH_APT_12
		CASE PROPERTY_HIGH_APT_13
		CASE PROPERTY_BUS_HIGH_APT_5
			iHash = GET_HASH_KEY("hei_dt1_03_build1x")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("DT1_Emissive_DT1_03_b1")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("dt1_03_dt1_Emissive_b1")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_HIGH_APT_14
		CASE PROPERTY_HIGH_APT_15
		CASE PROPERTY_BUS_HIGH_APT_3
			iHash = GET_HASH_KEY("hei_bh1_08_bld2")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("bh1_emissive_bh1_08")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("bh1_08_bld2_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("hei_bh1_08_bld2")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("bh1_08_em")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_HIGH_APT_16
		CASE PROPERTY_HIGH_APT_17
			iHash = GET_HASH_KEY("apa_ss1_02_building01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("SS1_Emissive_SS1_02a_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ss1_02_ss1_emissive_ss1_02")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("shell_mp_apt_h_01")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_BUS_HIGH_APT_4
			iHash = GET_HASH_KEY("hei_ss1_02_building01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("SS1_Emissive_SS1_02a_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ss1_02_ss1_emissive_ss1_02")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("shell_mp_apt_h_01")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)

			iHash = GET_HASH_KEY("apa_ss1_02_building01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("SS1_02_Building01_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_1_BASE_B
			iHash = GET_HASH_KEY("apa_ch2_05e_res5")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_05e_res5_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_2_B
			iHash = GET_HASH_KEY("apa_ch2_04_house02")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_04_house02_d")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_04_M_a_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_04_house02_railings")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_04_emissive_04")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_04_emissive_04_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_04_house02_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_3_B
			iHash = GET_HASH_KEY("apa_ch2_09b_hs01a_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_hs01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_hs01_balcony")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_Emissive_11_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_Emissive_11")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_CH2_09b_House08_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_4_B
			iHash = GET_HASH_KEY("apa_ch2_09c_hs11")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("CH2_09c_Emissive_11_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("CH2_09c_Emissive_11")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09c_hs11_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_5_BASE_A
			iHash = GET_HASH_KEY("apa_ch2_05c_b4")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_05c_emissive_07")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_05c_decals_05")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_05c_B4_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_7_A
			iHash = GET_HASH_KEY("apa_ch2_09c_hs07")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_09c_build_11_07_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("CH2_09c_Emissive_07_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09c_build_11_07_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_09c_hs07_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("CH2_09c_Emissive_07")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_8_A
			iHash = GET_HASH_KEY("apa_ch2_09c_hs13")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09c_hs13_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_CH2_09c_House11_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_09c_Emissive_13_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_09c_Emissive_13")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_10_A
			iHash = GET_HASH_KEY("apa_ch2_09b_hs02")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_hs02b_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_Emissive_09_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_09b_botpoolHouse02_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_Emissive_09")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_09b_hs02_balcony")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
//		CASE PROPERTY_STILT_APT_11_A
//			iHash = GET_HASH_KEY("apa_ch2_09b_hs03")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("apa_ch2_09b_hs03_details")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("ch2_09b_Emissive_07")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("ch2_09b_MChse07_a_LOD")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("ch2_09b_hs03_balcony_01")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			DISABLE_OCCLUSION_THIS_FRAME()
//		BREAK
		CASE PROPERTY_STILT_APT_12_A
			iHash = GET_HASH_KEY("apa_ch2_12b_house03mc")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_12b_emissive_02")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_12b_house03_MC_a_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_12b_emissive_02_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_12b_railing_06")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_STILT_APT_13_A
			iHash = GET_HASH_KEY("apa_ch2_04_house01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_04_house01_d")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_04_emissive_05_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("apa_ch2_04_M_b_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_04_emissive_05")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ch2_04_house01_details")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
			iHash = GET_HASH_KEY("sm_13_emissive")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_13_bld1")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_13_bld1_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
			iHash = GET_HASH_KEY("sm_15_bld2_dtl")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("hei_sm_15_bld2")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_15_bld2_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_15_bld2_dtl3")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_15_bld1_dtl3")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_15_bld2_railing")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_15_emissive")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_15_emissive_LOD")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
			iHash = GET_HASH_KEY("hei_dt1_02_w01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("dt1_02_helipad_01")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("dt1_02_dt1_emissive_dt1_02")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			iHash = GET_HASH_KEY("dt1_11_dt1_emissive_dt1_11")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("dt1_11_dt1_tower")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			ENABLE_SHADOW_CULL_MODEL_THIS_FRAME(tempModel)
			DISABLE_OCCLUSION_THIS_FRAME()
		BREAK
//		CASE PROPERTY_HIGH_APT_18
//		CASE PROPERTY_HIGH_APT_19
//			iHash = GET_HASH_KEY("kt1_09_building1")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			iHash = GET_HASH_KEY("kt1_emissive_kt1_09_ema")
//			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//		BREAK
	ENDSWITCH
	//PRINTLN("CULL_MODEL_NAMES_OF_BUILDING: Called this frame")
ENDPROC

PROC ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END(INT iProperty, MP_WINDOW_BOUND_BOX &window[MAX_WINDOWS], INT iPedVehPopAdjust = 4)
	
	
	
	//First - side window
	//Second - main window
	IF NOT MPGlobalsAmbience.bUsingTelescope
		VECTOR vPedVehSpawnPoint
		//IF NOT IS_PROPERTY_BUSINESS_APARTMENT(iProperty)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), window[0].vAreaAVec, window[0].vAreaBVec, 4.250000)
				PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: First iProperty: ", iProperty, " side window A: ", window[0].vAreaAVec, ", side window B: ", window[0].vAreaBVec, ", spawnPoint ", window[0].vSpawnPoint, ", PedRadius ", window[0].fPedRadius, ", VehRadius ", window[0].fVehRadius, " fAreaWidth ", window[0].fAreaWidth)
				SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(3)

				SWITCH iProperty
					CASE PROPERTY_HIGH_APT_1
						// North of building 1
						//IS_ENTITY_IN_AREA(PLAYER_PED_ID(), <<-796.195618,440.240753,79.643478>> - <<100.000000,100.000000,10.000000>>, <<-796.195618,440.240753,79.643478>> + <<100.000000,100.000000,10.000000>>)
//						IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-796.195618,440.240753,79.643478>> - <<100.000000,100.000000,10.000000>>, <<-796.195618,440.240753,79.643478>> + <<100.000000,100.000000,10.000000>>, 4.250000)
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-796.195618,440.240753,79.643478>> - <<100.000000,100.000000,10.000000>>, <<-796.195618,440.240753,79.643478>> + <<100.000000,100.000000,10.000000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-796.195618,440.240753,79.643478>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_1 - side window- 4, <<-796.195618,440.240753,79.643478>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_2
						
//						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-862.642883,353.233856,74.353943>>,100,200)
//						PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_2 - side window- 4, <<-862.642883,353.233856,74.353943>>")
						// south of building 1
						vPedVehSpawnPoint = <<-789.9, 253.2, 79.3>>
						//IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vPedVehSpawnPoint  - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, vPedVehSpawnPoint - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-789.9, 253.2, 79.3>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_2 - side window- 4, <<-789.9, 253.2, 79.3>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_3
						vPedVehSpawnPoint = <<-789.9, 253.2, 79.3>>
						//IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vPedVehSpawnPoint  - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, vPedVehSpawnPoint - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
//						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-862.642883,353.233856,74.353943>>,100,200)
//						PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_3 - side window- 4, <<-862.642883,353.233856,74.353943>>")
						// south of building 1
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-789.9, 253.2, 79.3>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_3 - side window- 4, <<-789.9, 253.2, 79.3>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_4
//						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-862.642883,353.233856,74.353943>> - <<100.000000,100.000000,10.000000>>, <<-862.642883,353.233856,74.353943>> + <<100.000000,100.000000,10.000000>>)
//						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-862.642883,353.233856,74.353943>>,100,200)
//						PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_4 - side window- 4, <<-862.642883,353.233856,74.353943>>")
						vPedVehSpawnPoint = <<-796.195618,440.240753,79.643478>>
						//IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vPedVehSpawnPoint  - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, vPedVehSpawnPoint - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						// North of building 1
						
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-796.195618,440.240753,79.643478>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_4 - side window- 4, <<-796.195618,440.240753,79.643478>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_5
						
						vPedVehSpawnPoint = <<-193.108185,-992.894165,32.949966>>
						//IS_ENTITY_IN_AREA(PLAYER_PED_ID(), vPedVehSpawnPoint  - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, vPedVehSpawnPoint - <<100.000000,100.000000,10.000000>>, vPedVehSpawnPoint + <<100.000000,100.000000,10.000000>>)
						
						// East of building 2
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-193.108185,-992.894165,32.949966>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_5 - side window- 5, <<-193.108185,-992.894165,32.949966>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_6
						// South of Building 2
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-278.1, -995.3, 24.8>>-<<100.000000,100.000000,12.750000>>, <<-278.1, -995.3, 24.8>> + <<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-278.1, -995.3, 24.8>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_6 - side window- 5, <<-338.4, -931.0, 30.7>>")
//						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-193.108185,-992.894165,32.949966>>-<<100.000000,100.000000,12.750000>>, <<-193.108185,-992.894165,32.949966>> + <<100.000000,100.000000,12.750000>>)
//						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-193.108185,-992.894165,32.949966>>,100,200)
//						PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_6 - side window- 5, <<-193.108185,-992.894165,32.949966>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_7
						// West of building 3
//						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-193.108185,-992.894165,32.949966>>-<<100.000000,100.000000,12.750000>>, <<-193.108185,-992.894165,32.949966>> + <<100.000000,100.000000,12.750000>>)
//						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-193.108185,-992.894165,32.949966>>,100,200)	
//						PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_7 - side window- 5, <<-193.108185,-992.894165,32.949966>>")
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-1494.9, -537.5, 32.6>>-<<100.000000,100.000000,12.750000>>, <<-1494.9, -537.5, 32.6>> + <<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-1494.9, -537.5, 32.6>>,100,200)	
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_7 - side window- 5, <<-1500.8, -484.0, 34.6>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_8
						// West of building 3
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-1494.9, -537.5, 32.6>>-<<100.000000,100.000000,12.750000>>, <<-1494.9, -537.5, 32.6>> + <<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-1494.9, -537.5, 32.6>>,100,200)	
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_8 - side window- 5, <<-1500.8, -484.0, 34.6>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_9
						// South of building 4
//						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-799.919373,-392.799652,40.701908>>-<<100.000000,100.000000,12.750000>>, <<-799.919373,-392.799652,40.701908>>+<<100.000000,100.000000,12.750000>>)
//						SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-799.919373,-392.799652,40.701908>>,100,200)
//						PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_9 - side window- 5, <<-799.919373,-392.799652,40.701908>>")
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-912.9, -504.8, 36.98>>-<<100.000000,100.000000,12.750000>>, <<-912.9, -504.8, 36.9>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-912.9, -504.8, 36.9>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_9 - side window- 5, <<-912.9, -504.8, 36.9>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_10
						// North of building 4
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-922.6, -406.0, 36.8>>-<<100.000000,100.000000,12.750000>>, <<-922.6, -406.0, 36.8>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-922.6, -406.0, 36.8>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_10 - side window- 5, <<-922.6, -406.0, 36.8>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_11
					
						// East of building 4
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-799.919373,-392.799652,40.701908>>-<<100.000000,100.000000,12.750000>>, <<-799.919373,-392.799652,40.701908>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-799.919373,-392.799652,40.701908>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_11 - side window- 5, <<-799.919373,-392.799652,40.701908>>")
					BREAK
					

					CASE PROPERTY_HIGH_APT_12
						// North of building 4
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-15,-517,33>>-<<100.000000,100.000000,12.750000>>, <<-15,-517,33>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-15,-517,33>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_12 - side window- 5, <-15,-517,33")
					BREAK
					
					CASE PROPERTY_HIGH_APT_13
						// East of building 4
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<67, -617, 44>>-<<100.000000,100.000000,12.750000>>, <<67, -617, 44>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME( <<67, -617, 44>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_13 - side window- 5, <<67, -617, 44>>")
					BREAK
					
					
					CASE PROPERTY_HIGH_APT_14
						// South of Building 5
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-901.853882,-424.538666,42.635471>>-<<100.000000,100.000000,12.750000>>, <<-901.853882,-424.538666,42.635471>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-901.853882,-424.538666,42.635471>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_14 - side window- 5, <<-901.853882,-424.538666,42.635471>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_15
						// North of Building 5
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-953, -319, 37>>-<<100.000000,100.000000,12.750000>>, <<-953, -319, 37>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-953, -319, 37>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_15 - side window- 5, <<-901.853882,-424.538666,42.635471>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_16		
						// North of Building
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-599, 136, 60>>-<<100.000000,100.000000,12.750000>>, <<-599, 136, 60>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-599, 136, 60>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_16 - side window- 5, <<-599, 136, 60>>")
					BREAK
					
					CASE PROPERTY_HIGH_APT_17
						// South of Building 
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-573, -34, 44>>-<<100.000000,100.000000,12.750000>>, <<-573, -34, 44>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-573, -34, 44>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_17 - side window- 5, <<-596.259766,175.782623,67.817299>>")
					BREAK
					
					// Buiness apartments
					CASE PROPERTY_BUS_HIGH_APT_1				
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-798, 445, 93>>-<<100.000000,100.000000,12.750000>>, <<-798, 445, 93>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-798, 445, 93>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_BUS_HIGH_END: PROPERTY_HIGH_APT_1 - main window- 5, <<-798, 445, 93>>")
					BREAK
					
					CASE PROPERTY_BUS_HIGH_APT_2				
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-1536, -550, 33>>-<<100.000000,100.000000,12.750000>>, <<-1536, -550, 33>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-1536, -550, 33>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_BUS_HIGH_END: PROPERTY_HIGH_APT_2 - main window- 5, <<-1536, -550, 33>>")
					BREAK
					
					CASE PROPERTY_BUS_HIGH_APT_3				
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-871, -398, 39>>-<<100.000000,100.000000,12.750000>>, <<-871, -398, 39>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-871, -398, 39>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_BUS_HIGH_END: PROPERTY_HIGH_APT_3 - main window- 5, <<-871, -398, 39>>")
					BREAK
					
					CASE PROPERTY_BUS_HIGH_APT_4				
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-604, -12, 43>>-<<100.000000,100.000000,12.750000>>, <<-604, -12, 43>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-604, -12, 43>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_BUS_HIGH_END: PROPERTY_HIGH_APT_4 - main window- 5, <<-604, -12, 43>>")
					BREAK
					
					CASE PROPERTY_BUS_HIGH_APT_5				
						ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-21, -510, 33>>-<<100.000000,100.000000,12.750000>>, <<-21, -510, 33>>+<<100.000000,100.000000,12.750000>>)
						SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-21, -510, 33>>,100,200)
						//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_BUS_HIGH_END: PROPERTY_HIGH_APT_5 - main window- 5, <<-21, -510, 33>>")
					BREAK
					
				ENDSWITCH
				//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: iProperty: ", iProperty, " side window A: ", window[0].vAreaAVec, ", side window B: ", window[0].vAreaBVec, ", spawnPoint ", window[0].vSpawnPoint, ", PedRadius ", window[0].fPedRadius, ", VehRadius ", window[0].fVehRadius, " fAreaWidth ", window[0].fAreaWidth)
			ELSE
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), window[1].vAreaAVec, window[1].vAreaBVec, 6.000000)
					PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: Second iProperty: ", iProperty, " front window A: ", window[1].vAreaAVec, ", side window B: ", window[1].vAreaBVec, ", spawnPoint ", window[1].vSpawnPoint, ", PedRadius ", window[1].fPedRadius, ", VehRadius ", window[1].fVehRadius, " fAreaWidth ", window[1].fAreaWidth)
					//ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-796.195618,440.240753,79.643478>>-<<100.000000,100.000000,10.000000>>, <<-796.195618,440.240753,79.643478>> + <<100.000000,100.000000,10.000000>>)
					SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(3)
					SWITCH iProperty
						CASE PROPERTY_HIGH_APT_1
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-966.9, 309.7, 69.3>>-<<100.000000,100.000000,10.000000>>, <<-966.9, 309.7, 69.3>> + <<100.000000,100.000000,10.000000>>)
//									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-862.642883,353.233856,74.353943>>,100,200)	
//									PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_1 - front window- 4, <<-862.642883,353.233856,74.353943>>")
									// west of building 1
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-966.9, 309.7, 69.3>> ,100,200)	
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_1 - front window- 4, <<-966.9, 309.7, 69.3>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_2
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-731.7, 293.2, 83.2>>-<<100.000000,100.000000,10.000000>>, <<-731.7, 293.2, 83.2>> + <<100.000000,100.000000,10.000000>>)
//									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-796.195618,440.240753,79.643478>>,100,200)
//									PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_2 - front window- 4, <<-796.195618,440.240753,79.643478>>")
									// East of building 1
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-731.7, 293.2, 83.2>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_2 - front window- 4, <<-731.7, 293.2, 83.2>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_3
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-731.7, 293.2, 83.2>>-<<100.000000,100.000000,10.000000>>, <<-731.7, 293.2, 83.2>> + <<100.000000,100.000000,10.000000>>)
//									SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-796.195618,440.240753,79.643478>>,100,200)
//									PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_3 - front window- 4, <<-796.195618,440.240753,79.643478>>")
									// East of building 1
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-731.7, 293.2, 83.2>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_3 - front window- 4, <<-731.7, 293.2, 83.2>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_4
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-731.7, 293.2, 83.2>>-<<100.000000,100.000000,10.000000>>, <<-731.7, 293.2, 83.2>> + <<100.000000,100.000000,10.000000>>)
//									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-796.195618,440.240753,79.643478>>-<<100.000000,100.000000,10.000000>>, <<-796.195618,440.240753,79.643478>> + <<100.000000,100.000000,10.000000>>)
//									SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-796.195618,440.240753,79.643478>>,100,200)
//									PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_4 - front window- 4, <<-796.195618,440.240753,79.643478>>")
									// East of building 1
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-731.7, 293.2, 83.2>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_4 - front window- 4, <<-731.7, 293.2, 83.2>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_5	
									// North of building 2
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-208.005463,-863.810425,38.105080>>-<<100.000000,100.000000,10.000000>>, <<-208.005463,-863.810425,38.105080>> + <<100.000000,100.000000,10.000000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-208.005463,-863.810425,38.105080>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_5 - front window- 5, <<-208.005463,-863.810425,38.105080>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_6
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-208.005463,-863.810425,38.105080>>-<<100.000000,100.000000,10.000000>>, <<-208.005463,-863.810425,38.105080>> + <<100.000000,100.000000,10.000000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-208.005463,-863.810425,38.105080>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_6 - front window- 5, <<-208.005463,-863.810425,38.105080>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_7
//									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-208.005463,-863.810425,38.105080>>-<<100.000000,100.000000,10.000000>>, <<-208.005463,-863.810425,38.105080>> + <<100.000000,100.000000,10.000000>>)
//									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-208.005463,-863.810425,38.105080>>,100,200)
//									PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_7 - front window- 5, <<-208.005463,-863.810425,38.105080>>")
									// North of Building 3
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-1500.8, -484.0, 34.6>>-<<100.000000,100.000000,12.750000>>, <<-1500.8, -484.0, 34.6>> + <<100.000000,100.000000,12.750000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-1500.8, -484.0, 34.6>>,100,200)	
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_7 - side window- 5, <<-1500.8, -484.0, 34.6>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_8
							
								// North of Building 3
								ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-1500.8, -484.0, 34.6>>-<<100.000000,100.000000,12.750000>>, <<-1500.8, -484.0, 34.6>> + <<100.000000,100.000000,12.750000>>)
								SET_POP_CONTROL_SPHERE_THIS_FRAME(<<-1500.8, -484.0, 34.6>>,100,200)	
								//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_8 - side window- 5, <<-1500.8, -484.0, 34.6>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_9
								// West of Building	
								ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-799.919373,-392.799652,40.701908>>-<<100.000000,100.000000,12.750000>>, <<-799.919373,-392.799652,40.701908>>+<<100.000000,100.000000,12.750000>>)
								SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-799.919373,-392.799652,40.701908>>,100,200)
								//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_9 - side window- 5, <<-799.919373,-392.799652,40.701908>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_10
						
									// West of building 4
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-959.5, -457.3, 36.7>>-<<100.000000,100.000000,12.750000>>, <<-959.5, -457.3, 36.7>>+<<100.000000,100.000000,12.750000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-959.5, -457.3, 36.7>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_10 - front window- 5, <<-959.5, -457.3, 36.7>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_11
									// North of Building 4
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-912.5, -403.6, 37.31>>-<<100.000000,100.000000,12.750000>>, <<-912.5, -403.6, 37.3>>+<<100.000000,100.000000,12.750000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-912.5, -403.6, 37.3>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_11 - front window- 5, <<-912.5, -403.6, 37.3>>")
						BREAK
						
					
						CASE PROPERTY_HIGH_APT_12
								// West of Building 4
								ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-81.9, -599, 37>>-<<100.000000,100.000000,12.750000>>, <<-81.9, -599, 37>>+<<100.000000,100.000000,12.750000>>)
								SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-81.9, -599, 37>>,100,200)
								//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_12 - front window- 5, <<-81.9, -599, 37>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_13
									// North of Building 4
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-17.9, -510, 33>>-<<100.000000,100.000000,12.750000>>, <<-17.9, -510, 33>>+<<100.000000,100.000000,12.750000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-17.9, -510, 33>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_13 - front window- 5, <<-17.9, -510, 33>>")
						BREAK
						
						
						CASE PROPERTY_HIGH_APT_14
								ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-845.975769,-340.692200,42.679432>>-<<100.000000,100.000000,12.750000>>, <<-845.975769,-340.692200,42.679432>>+<<100.000000,100.000000,12.750000>>)
								SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-845.975769,-340.692200,42.679432>>,100,200)
								//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_14 - front window- 5, <<-845.975769,-340.692200,42.679432>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_15
									// West of Building 5
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-978, -401, 37>>-<<100.000000,100.000000,12.750000>>, <<-978, -401, 37>>+<<100.000000,100.000000,12.750000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME(  <<-978, -401, 37>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_15 - front window- 5, <<-978, -401, 37>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_16
									ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-683, 21, 39>>-<<100.000000,100.000000,12.750000>>, <<-683, 21, 392>>+<<100.000000,100.000000,12.750000>>)
									SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-683, 21, 39>>,100,200)
									//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_16 - front window- 5, <<-845.975769,-340.692200,42.679432>>")
						BREAK
						
						CASE PROPERTY_HIGH_APT_17
							ADJUST_AMBIENT_PED_SPAWN_DENSITIES_THIS_FRAME(iPedVehPopAdjust, <<-540, 28, 44>>-<<100.000000,100.000000,12.750000>>, <<-540, 28, 44>>+<<100.000000,100.000000,12.750000>>)
							SET_POP_CONTROL_SPHERE_THIS_FRAME( <<-540, 28, 44>>,100,200)
							//PRINTLN("ADJUST_AMBIENT_PED_SPAWN_WHEN_IN_HIGH_END: PROPERTY_HIGH_APT_17 - front window- 5, <<-540, 28, 44>>")
						BREAK
					ENDSWITCH
					
				ENDIF 
			ENDIF
//		ELSE
//		ENDIF
	
	ENDIF
ENDPROC


PROC TURN_OFF_MODEL_NAMES_OF_BUILDING_DEBUG(INT iProperty)
	MODEL_NAMES tempModel
	INT iHash
	SWITCH iProperty
		CASE 0
		CASE 1
			iHash = GET_HASH_KEY("ss1_11_flats")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ss1_emissive_ss1_11_a")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("ss1_11_detail01b")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(prop_roofvent_01a)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(prop_roofvent_03a)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(prop_aircon_m_02)
//			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(prop_aircon_m_10)
		BREAK
			
		CASE 2
			iHash = GET_HASH_KEY("sm_14_bld2")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("sm_emissive_sm_14")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
		BREAK
		
		CASE 3
			iHash = GET_HASH_KEY("kt1_09_building1")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
			iHash = GET_HASH_KEY("kt1_emissive_kt1_09_ema")
			tempModel = INT_TO_ENUM(MODEL_NAMES,iHash)
			ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(tempModel)
		BREAK
	ENDSWITCH
	
ENDPROC

PROC DISABLE_MP_PROPERTY_INTERIORS(INT iPropertyID, BOOL bDisable, BOOL bOnExit= FALSE)
	INT iPropertySize = GET_PROPERTY_SIZE_TYPE(iPropertyID)
//	#IF FEATURE_APARTMENT_CONTENT
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "DISABLE_MP_PROPERTY_INTERIORS: iPropertyID = ", GET_PROPERTY_ENUM_NAME(iPropertyID), ", bOnExit = ", bOnExit, ", bDisable = ", bDisable)
//	#ENDIF
	SWITCH iPropertyID
		CASE PROPERTY_OFFICE_1_GARAGE_LVL1
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_1_GAR_1,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_1_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_1,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL2
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_1_GAR_2,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_1_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_2,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_1_GARAGE_LVL3
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_1_GAR_3,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_1_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_3,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL1
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_2_GAR_1,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_2_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_1,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL2
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_2_GAR_2,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_2_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_2,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_2_GARAGE_LVL3
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_2_GAR_3,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_2_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_3,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL1
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_3_GAR_1,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_3_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_1,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL2
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_3_GAR_2,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_3_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_2,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_3_GARAGE_LVL3
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_3_GAR_3,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_3_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_3,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL1
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_4_GAR_1,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_4_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_1,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL2
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_4_GAR_2,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_4_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_2,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,bDisable)
			ENDIF
		BREAK
		CASE PROPERTY_OFFICE_4_GARAGE_LVL3
			IF bOnExit
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_4_GAR_3,bDisable)
				SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_OFFICE_4_MOD,bDisable)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_3,bDisable)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,bDisable)
			ENDIF
		BREAK
	ENDSWITCH

	IF iPropertySize = PROP_SIZE_TYPE_LARGE_APT
		SWITCH iPropertyID
			CASE PROPERTY_HIGH_APT_1
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_1,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_1,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_2
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_2,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_2,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_3
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_3,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_3,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_4
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_4,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_4,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_5
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_5,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_5,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_6
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_6,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_6,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_7
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_7,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_7,bDisable)
				ENDIF
			BREAK	
			CASE PROPERTY_HIGH_APT_8
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_8,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_8,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_9
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_9,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_9,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_10
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_10,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_10,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_11
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_11,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_11,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_12
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_12,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_12,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_13
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_13,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_13,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_14
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_14,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_14,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_15
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_15,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_15,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_16
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_16,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_16,bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_HIGH_APT_17
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_HIGH_17,bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_HIGH_17,bDisable)
				ENDIF
			BREAK
			
			CASE PROPERTY_STILT_APT_1_BASE_B
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_1_BASE_B, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_1_BASE_B, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_2_B
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_2_B, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_2_B, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_3_B
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_3_B, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_3_B, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_4_B
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_4_B, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_4_B, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_5_BASE_A
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_5_BASE_A, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_5_BASE_A, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_7_A
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_7_A, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_7_A, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_8_A
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_8_A, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_8_A, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_10_A
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_10_A, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_10_A, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_12_A
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_12_A, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_12_A, bDisable)
				ENDIF
			BREAK
			CASE PROPERTY_STILT_APT_13_A
				IF bOnExit
					SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APARTMENT_STILT_13_A, bDisable)
				ELSE
					SET_INTERIOR_DISABLED(INTERIOR_V_APARTMENT_STILT_13_A, bDisable)
				ENDIF
			BREAK
		ENDSWITCH
	ELIF iPropertySize = PROP_SIZE_TYPE_MED_APT
		IF bOnExit
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_APART_MIDSPAZ,bDisable)
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_APART_MIDSPAZ,bDisable)
		ENDIF
		//enable medium apartment
	ELIF iPropertySize = PROP_SIZE_TYPE_SMALL_APT
		//SET_INTERIOR_DISABLED( INTERIOR_V_GARAGES, bDisable)
		//enable small apartment
	ENDIF
	IF mpProperties[iPropertyID].iGarageSize = PROP_GARAGE_SIZE_2
		IF bOnExit
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGES,bDisable)
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGES,bDisable)
		ENDIF
	ELIF mpProperties[iPropertyID].iGarageSize = PROP_GARAGE_SIZE_6
		IF bOnExit
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEM,bDisable)
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEM,bDisable)
		ENDIF
	ELIF mpProperties[iPropertyID].iGarageSize = PROP_GARAGE_SIZE_10
		IF bOnExit
			SET_INTERIOR_DISABLED_ON_EXIT(INTERIOR_V_GARAGEL,bDisable)
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_GARAGEL,bDisable)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_MP_GARAGE(PLAYER_INDEX playerID,BOOL bOwnedOnly)
	IF IS_PLAYER_IN_MP_PROPERTY(playerID,bOwnedOnly)
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_APARTMENT_MESSY()
	
	IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked > TIER_1_SMOKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank > TIER_1_DRINKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers > TIER_1_STRIPPERS
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond > TIER_1_SMOKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond > TIER_1_DRINKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond > TIER_1_STRIPPERS
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3 > TIER_1_SMOKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3 > TIER_1_DRINKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3 > TIER_1_STRIPPERS
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked4 > TIER_1_SMOKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4 > TIER_1_DRINKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers4 > TIER_1_STRIPPERS
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked5 > TIER_1_SMOKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank5 > TIER_1_DRINKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers5 > TIER_1_STRIPPERS
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedYacht > TIER_1_SMOKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankYacht > TIER_1_DRINKING
	OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersYacht> TIER_1_STRIPPERS
		RETURN TRUE
	ENDIF
	INT i
	REPEAT MAX_MESS_PROPERTIES i
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesDrank[i] > TIER_1_DRINKING    //MAX_MESS_PROPERTIES
		OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesSmoked[i] > TIER_1_SMOKING
		OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesStripper[i] > TIER_1_STRIPPERS
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CLEAN_APARTMENT_MESS_DATA(BOOL bForceClean = FALSE)
	IF IS_APARTMENT_MESSY()
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = INT_TO_ENUM(TIMEOFDAY,0)
		OR g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = INVALID_TIMEOFDAY
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = GET_CURRENT_TIMEOFDAY()
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAN_APARTMENT_MESS_DATA: never set starting now")
		ENDIF
		// If the TODLastCleaned is invalid, set it to the current time
		IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = INVALID_TIMEOFDAY
		OR IS_TIMEOFDAY_AFTER_TIMEOFDAY(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned, GET_CURRENT_TIMEOFDAY())
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = GET_CURRENT_TIMEOFDAY()
			bForceClean = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAN_APARTMENT_MESS_DATA: Last Cleaned TOD was invalid or after current date - reset to now")
		ENDIF
		
		INT iSec, iMinute, iHour, iDay, iMonth, iYear
		GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned, GET_CURRENT_TIMEOFDAY(),
			iSec, iMinute, iHour, iDay, iMonth, iYear)
			
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAN_APARTMENT_MESS_DATA: Difference between now and last cleaned date is -")
		PRINTLN("iSec = ", iSec, "; iMinute = ", iMinute, "; iHour = ", iHour, "; iDay = ", iDay, "; iMonth = ", iMonth, "; iYear = ", iYear)
		
		// If the difference is 7+ days, or 1+ months, or even 1+ year, reset all the saved activity uses to 0 and set the last time we cleaned to now
		IF iDay >= 7
		OR (iMonth >= 1 OR iYear >= 1)
		OR bForceClean = TRUE // Force a cleanup if the TOD stuff went wrong
			// Only do this if the player has enough money for a cleaner - otherwise, leave the mess alone
			IF bForceClean
			OR (GET_LOCAL_PLAYER_VC_AMOUNT(TRUE) > 50
			OR NETWORK_CAN_SPEND_MONEY(50,FALSE,TRUE,FALSE)) //Use OR to catch cash values over SCRIPT_MAX_INT32.
				IF bForceClean = FALSE // Don't charge the player money if the TOD went wrong
					CLEAR_OUT_PROPERTY_UTILITIES_STRUCT()
					
					g_sPropertyCostBreakdown.iCleanerFees = 50
					
					IF USE_SERVER_TRANSACTIONS()
						INT iTransactionID
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_UTILITY_BILLS, g_sPropertyCostBreakdown.iCleanerFees , iTransactionID, FALSE, TRUE)
					ELSE
						NETWORK_SPEND_APARTMENT_UTILITIES(g_sPropertyCostBreakdown.iCleanerFees , FALSE, TRUE, g_sPropertyCostBreakdown)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAN_APARTMENT_MESS_DATA: Forcing clean, not charging for this...")
				ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked4 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers4 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked5 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank5 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers5 = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedYacht = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankYacht = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersYacht = 0
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.TODLastCleaned = GET_CURRENT_TIMEOFDAY()
				INT i
				REPEAT MAX_MESS_PROPERTIES i
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesDrank[i] = 0
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesSmoked[i] = 0
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesStripper[i] = 0
				ENDREPEAT
				CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAN_APARTMENT_MESS_DATA: Been more than a week since last apt clean - reset!")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF bForceClean
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CLEAN_APARTMENT_MESS_DATA: Apartment is not messy")
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC FORCE_CLEAN_APARTMENT_MESS_DATA(INT iPropertySlot)
	IF iPropertySlot = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers = 0
	ELIF iPropertySlot = 1
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedSecond = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankSecond = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersSecond = 0
	ELIF iPropertySlot = 2
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked3 = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank3 = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers3 = 0
	ELIF iPropertySlot = 3
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked4 = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank4 = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers4 = 0
	ELIF iPropertySlot = 4
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmoked5 = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrank5 = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippers5 = 0
	ELIF iPropertySlot = YACHT_PROPERTY_OWNED_SLOT
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesSmokedYacht = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesDrankYacht = 0
		g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iNumOfTimesStrippersYacht = 0
	ELIF iPropertySlot >= PROPERTY_OWNED_SLOT_OFFICE_0
		INT i
		REPEAT MAX_MESS_PROPERTIES i
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesDrank[i] = 0
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesSmoked[i] = 0
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iArrayNumTimesStripper[i] = 0
		ENDREPEAT
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "FORCE_CLEAN_APARTMENT_MESS_DATA: forcably Been more than a week since last apt clean ")

ENDPROC

PROC CLEAR_GLOBAL_ENTRY_DATA()
	globalPropertyEntryData.iVehSlot = -1
	globalPropertyEntryData.ownerID = INVALID_PLAYER_INDEX()
	CLEAR_GAMER_HANDLE_STRUCT(globalPropertyEntryData.ownerHandle)
	globalPropertyEntryData.iCostOfPropertyJustBought = 0
	globalPropertyEntryData.iTradeInValueOfPreviousProperty = 0
	globalPropertyEntryData.iPropertyEntered = 0
	//globalPropertyEntryData.iCostOfPropertyJustBought = 0
	globalPropertyEntryData.iEntrance = 0
	globalPropertyEntryData.iBS = 0
	globalPropertyEntryData.bBuzzAccepted = FALSE
	globalPropertyEntryData.bLeftOrRejectedBuzzer = FALSE
	globalPropertyEntryData.bForceCleanupExterior = FALSE
	globalPropertyEntryData.iInstance = -1
	SET_REPLACING_VEHICLE_ON_ENTRY(FALSE)
	globalPropertyEntryData.iReplaceSlot = -1
	globalPropertyEntryData.iOldVehicleSlot = -1
	globalPropertyEntryData.iYachtEntered = -1
	globalPropertyEntryData.iVariation = -1
	globalPropertyEntryData.vBeforeUnderMapLocation = <<0,0,0>>
	DEBUG_PRINTCALLSTACK()
	PRINTLN(GET_THIS_SCRIPT_NAME(),": CLEAR_GLOBAL_ENTRY_DATA()") 
ENDPROC

FUNC BOOL IS_PROPERTY_MISSING_BUZZERS(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_IND_DAY_MEDIUM_1
		CASE PROPERTY_IND_DAY_MEDIUM_2
		CASE PROPERTY_IND_DAY_MEDIUM_3
		CASE PROPERTY_IND_DAY_MEDIUM_4
		CASE PROPERTY_IND_DAY_LOW_1
		CASE PROPERTY_IND_DAY_LOW_2
		CASE PROPERTY_IND_DAY_LOW_3
		CASE PROPERTY_STILT_APT_1_BASE_B
		CASE PROPERTY_STILT_APT_2_B
		CASE PROPERTY_STILT_APT_3_B		
		CASE PROPERTY_STILT_APT_4_B	
		CASE PROPERTY_STILT_APT_5_BASE_A
		//CASE PROPERTY_STILT_APT_6_A		
		CASE PROPERTY_STILT_APT_7_A		
		CASE PROPERTY_STILT_APT_8_A		
		//CASE MP_PROPERTY_STILT_APT_9_A		
		CASE PROPERTY_STILT_APT_10_A		
//		CASE PROPERTY_STILT_APT_11_A		
		CASE PROPERTY_STILT_APT_12_A		
		CASE PROPERTY_STILT_APT_13_A
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE 
ENDFUNC

/// PURPOSE:
///    This is ONLY setup for properties without buzzers
FUNC MODEL_NAMES GET_PROPERTY_BUZZER_MODEL(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_IND_DAY_MEDIUM_1
		CASE PROPERTY_IND_DAY_MEDIUM_2
		CASE PROPERTY_IND_DAY_MEDIUM_3
		CASE PROPERTY_IND_DAY_MEDIUM_4
		CASE PROPERTY_IND_DAY_LOW_1
		CASE PROPERTY_IND_DAY_LOW_2
		CASE PROPERTY_IND_DAY_LOW_3
		CASE PROPERTY_STILT_APT_1_BASE_B
		CASE PROPERTY_STILT_APT_2_B
		CASE PROPERTY_STILT_APT_3_B		
		CASE PROPERTY_STILT_APT_4_B
		CASE PROPERTY_STILT_APT_5_BASE_A
		//CASE PROPERTY_STILT_BUILDING_6_A		
		CASE PROPERTY_STILT_APT_7_A		
		CASE PROPERTY_STILT_APT_8_A		
		//CASE MP_PROPERTY_STILT_BUILDING_9_A		
		CASE PROPERTY_STILT_APT_10_A		
//		CASE PROPERTY_STILT_APT_11_A		
		CASE PROPERTY_STILT_APT_12_A		
		CASE PROPERTY_STILT_APT_13_A
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
			RETURN PROP_LD_KEYPAD_01B
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

PROC CHECK_30_CARS_ACHIEVEMENT()
		IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_STORE_20_CAR_IN_GARAGES)
			EXIT
		ENDIF
		IF g_MpSavedVehicles[g_i30CarAchievementLoop].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
		AND NOT MP_SAVE_VEHICLE_IS_SAVE_SLOT_A_CYCLE(g_i30CarAchievementLoop)
			IF IS_BIT_SET(g_MpSavedVehicles[g_i30CarAchievementLoop].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED) 
			 	IF IS_BIT_SET(g_MpSavedVehicles[g_i30CarAchievementLoop].iVehicleBS,MP_SAVED_VEHICLE_INSURED)
					g_i30CarAchievementCounter++
				ENDIF
			ELSE
				g_i30CarAchievementCounter++
			ENDIF
		ENDIF
		
		//PRINTLN("CHECK_30_CARS_ACHIEVEMENT: loop = ",g_i30CarAchievementLoop," num of vehicles owned = ",g_i30CarAchievementCounter)
		g_i30CarAchievementLoop++
		IF g_i30CarAchievementLoop >= MAX_MP_SAVED_VEHICLES
			g_i30CarAchievementLoop = 0
			IF g_i30CarAchievementCounter >= 30
				SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_STORE_20_CAR_IN_GARAGES,TRUE)
				PRINTLN("CHECK_30_CARS_ACHIEVEMENT: awarding achievement")
			ENDIF
			g_i30CarAchievementCounter = 0
		ENDIF
ENDPROC

PROC MAINTAIN_SERVER_INSTANCE_REQUESTS()

	PLAYER_INDEX thePlayer
	INT i
	INT iFirstEmptyIndex = -1
	BOOL bAlreadyInArray = false
	
	#IF IS_DEBUG_BUILD
	IF g_db_bPrintServerPropertyRequestDetails
	OR GlobalServerBD_PropertyInstances.db_printAllRequests
		PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: Outputing Request Data Full -START-")
		REPEAT NUM_NETWORK_PLAYERS i
			PRINTLN("Instance #",i)
			PRINTLN("Slot #",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i])
			PRINTLN("Belongs to:")
			IF IS_GAMER_HANDLE_VALID(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
			AND NETWORK_IS_GAMER_IN_MY_SESSION(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				IF thePlayer != INVALID_PLAYER_INDEX()
					PRINTLN(GET_PLAYER_NAME(thePlayer))	
				ELSE
					PRINTLN("Not valid")	
				ENDIF
			ELSE
				PRINTLN("Not in session")
			ENDIF
			DEBUG_PRINT_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
		ENDREPEAT
		PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: Outputing Request Data Full -END-")
		g_db_bPrintServerPropertyRequestDetails = FALSE
	ENDIF
	IF g_db_bPrintPropertyRequestDetailsAll
		GlobalServerBD_PropertyInstances.db_printAllRequests = TRUE
	ENDIF
	#ENDIF
	
	thePlayer = INT_TO_PLAYERINDEX(Server_PropertyInstancesLocalData.iSlowLoopID)
	
	IF thePlayer != INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(thePlayer,FALSE,FALSE)
		#IF IS_DEBUG_BUILD
		IF g_db_bPrintServerPropertyRequestDetails
		OR GlobalServerBD_PropertyInstances.db_printAllRequests
		PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: server checking data broadcast from # ",Server_PropertyInstancesLocalData.iSlowLoopID," name: ",GET_PLAYER_NAME(thePlayer))	
		ENDIF
		#ENDIF
		IF IS_GAMER_HANDLE_VALID(GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.requestedGamerForInstance)
			#IF IS_DEBUG_BUILD
			IF g_db_bPrintServerPropertyRequestDetails
			OR GlobalServerBD_PropertyInstances.db_printAllRequests
			DEBUG_PRINT_GAMER_HANDLE(GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.requestedGamerForInstance)
			ENDIF
			#ENDIF
			REPEAT NUM_NETWORK_PLAYERS i
				IF NOT bAlreadyInArray
					IF IS_GAMER_HANDLE_VALID(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
					AND GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i] >= 0
						#IF IS_DEBUG_BUILD
						IF GlobalServerBD_PropertyInstances.db_printAllRequests
							PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: checking free slot gamer is valid in position ",i)
							PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: client iRequestedGamerPropSlot",GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.iRequestedGamerPropSlot)
							PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: server iRequestedGamerPropSlot",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i])
						ENDIF
						#ENDIF
						IF GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.iRequestedGamerPropSlot >= 0
						AND GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.iRequestedGamerPropSlot = GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i]
							IF NETWORK_ARE_HANDLES_THE_SAME(GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.requestedGamerForInstance,
															GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
								bAlreadyInArray	= TRUE
								SET_BIT(Server_PropertyInstancesLocalData.iRequestedBS,i)
								#IF IS_DEBUG_BUILD
								IF GlobalServerBD_PropertyInstances.db_printAllRequests
									PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: already in array")
								ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ELSE
						IF iFirstEmptyIndex = -1
							iFirstEmptyIndex = i
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			IF NOT bAlreadyInArray
				IF iFirstEmptyIndex >= 0
					GlobalServerBD_PropertyInstances.requestedGamerInstances[iFirstEmptyIndex] = GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.requestedGamerForInstance
					GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[iFirstEmptyIndex] = GlobalplayerBD_FM[Server_PropertyInstancesLocalData.iSlowLoopID].propertyDetails.iRequestedGamerPropSlot
					SET_BIT(Server_PropertyInstancesLocalData.iRequestedBS,iFirstEmptyIndex)
					#IF IS_DEBUG_BUILD
					IF NETWORK_IS_GAMER_IN_MY_SESSION(GlobalServerBD_PropertyInstances.requestedGamerInstances[iFirstEmptyIndex])
						thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[iFirstEmptyIndex])
						IF thePlayer != INVALID_PLAYER_INDEX()
							PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: assigned instance# ",iFirstEmptyIndex
									," property slot# ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[iFirstEmptyIndex]," to player ",GET_PLAYER_NAME(thePlayer))
						ELSE
							PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: assigned instance# ",iFirstEmptyIndex
									," property slot# ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[iFirstEmptyIndex])
						ENDIF
					ELSE
						PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: assigned instance# ",iFirstEmptyIndex
									," property slot# ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[iFirstEmptyIndex])
					ENDIF
					DEBUG_PRINT_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[iFirstEmptyIndex])
					#ENDIF
				ELSE
					PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: no empty slots and not in array already??")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	Server_PropertyInstancesLocalData.iSlowLoopID++
	IF Server_PropertyInstancesLocalData.iSlowLoopID >= NUM_NETWORK_PLAYERS
		Server_PropertyInstancesLocalData.iSlowLoopID = 0
		REPEAT NUM_NETWORK_PLAYERS i
			IF NOT IS_BIT_SET(Server_PropertyInstancesLocalData.iRequestedBS,i)
			AND GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i] >= 0
				#IF IS_DEBUG_BUILD
				IF IS_GAMER_HANDLE_VALID(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				AND NETWORK_IS_GAMER_IN_MY_SESSION(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
					thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
					IF thePlayer != INVALID_PLAYER_INDEX()
						PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: instance#  ",iFirstEmptyIndex
							," property slot# ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i]," of player ",GET_PLAYER_NAME(thePlayer))
					ELSE
						PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: instance# ",iFirstEmptyIndex
							," property slot# ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i])
					ENDIF
				ELSE
					PRINTLN("MAINTAIN_SERVER_INSTANCE_REQUESTS: instance# ",iFirstEmptyIndex
							," property slot# ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i])
				ENDIF
				DEBUG_PRINT_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				#ENDIF
				CLEAR_GAMER_HANDLE_STRUCT(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i] = -1
			ENDIF
		ENDREPEAT
		Server_PropertyInstancesLocalData.iRequestedBS = 0
	ENDIF
	
	
ENDPROC

PROC MAINTAIN_CLIENT_RESET_SERVER_PROPERTY_INSTANCE_REQUEST_DATA()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()	
		IF Server_PropertyInstancesLocalData.iSlowLoopID != 0
		OR Server_PropertyInstancesLocalData.iRequestedBS != 0
			Server_PropertyInstancesLocalData.iSlowLoopID = 0
			Server_PropertyInstancesLocalData.iRequestedBS = 0
			PRINTLN("MAINTAIN_CLIENT_RESET_SERVER_PROPERTY_INSTANCE_REQUEST_DATA cleared data")
		ENDIF
	ENDIF
ENDPROC

FUNC INT CLIENT_REQUEST_PROPERTY_INSTANCE_ID(GAMER_HANDLE &theGamer, INT iSlot)
	INT iReturnInstance = -2
	INT i
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX thePlayer
	#ENDIF
	//PLAYER_INDEX thePlayer
	#IF IS_DEBUG_BUILD
	IF GlobalServerBD_PropertyInstances.db_printAllRequests
		PRINTLN("CLIENT() - MAINTAIN_SERVER_INSTANCE_REQUESTS: Outputing Request Data Full -START-")
		REPEAT NUM_NETWORK_PLAYERS i
			PRINTLN("Instance #",i)
			PRINTLN("Slot #",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i])
			PRINTLN("Belongs to:")
			IF IS_GAMER_HANDLE_VALID(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
			AND NETWORK_IS_GAMER_IN_MY_SESSION(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
				IF thePlayer != INVALID_PLAYER_INDEX()
					PRINTLN(GET_PLAYER_NAME(thePlayer))	
				ELSE
					PRINTLN("Not valid")	
				ENDIF
			ELSE
				PRINTLN("Not in session")
			ENDIF
			DEBUG_PRINT_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
		ENDREPEAT
		PRINTLN("CLIENT() - MAINTAIN_SERVER_INSTANCE_REQUESTS: Outputing Request Data Full -END-")
	ENDIF
	#ENDIF
	
	IF IS_GAMER_HANDLE_VALID(theGamer)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance = theGamer
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestedGamerPropSlot = iSlot
		#IF IS_DEBUG_BUILD
		IF GlobalServerBD_PropertyInstances.db_printAllRequests
			IF IS_GAMER_HANDLE_VALID(theGamer)
			AND NETWORK_IS_GAMER_IN_MY_SESSION(theGamer)
				thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(theGamer)
				PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: requesting instance for property slot# ",iSlot, " owned by player: ",GET_PLAYER_NAME(thePlayer))	
			ELSE
				PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: requesting instance for property slot# ",iSlot)
			ENDIF
			PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: requesting for theGamer = ")
			DEBUG_PRINT_GAMER_HANDLE(theGamer)
		ENDIF
		#ENDIF
		REPEAT NUM_NETWORK_PLAYERS i
			#IF IS_DEBUG_BUILD
			IF GlobalServerBD_PropertyInstances.db_printAllRequests
				PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: loop iSlot = ",iSlot, " iRequestedGamerPropSlot[",i,"] = ",GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i])
				PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: current requestedGamerInstances[",i,"] = ")
				DEBUG_PRINT_GAMER_HANDLE(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
			ENDIF
			#ENDIF
			IF iSlot = GlobalServerBD_PropertyInstances.iRequestedGamerPropSlot[i]
				IF IS_GAMER_HANDLE_VALID(GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
					IF NETWORK_ARE_HANDLES_THE_SAME(theGamer,
														GlobalServerBD_PropertyInstances.requestedGamerInstances[i])
						iReturnInstance = i
						#IF IS_DEBUG_BUILD
						IF IS_GAMER_HANDLE_VALID(theGamer)
						AND NETWORK_IS_GAMER_IN_MY_SESSION(theGamer)
							thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(theGamer)
							PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: returning instance ID: ", iReturnInstance, " for property slot# ",iSlot, " owned by player: ",GET_PLAYER_NAME(thePlayer))	
						ELSE
							PRINTLN("CLIENT_REQUEST_PROPERTY_INSTANCE_ID: returning instance ID: ", iReturnInstance, " for property slot# ",iSlot)
						ENDIF
						DEBUG_PRINT_GAMER_HANDLE(theGamer)
						#ENDIF
						RETURN iReturnInstance
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN iReturnInstance
ENDFUNC

PROC CLIENT_CLEAR_REQUEST_PROPERTY_INSTANCE_ID()
	#IF IS_DEBUG_BUILD
	PLAYER_INDEX thePlayer
	IF IS_GAMER_HANDLE_VALID(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance)
		IF NETWORK_IS_GAMER_IN_MY_SESSION(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance)
			thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance)
			PRINTLN("CLIENT_CLEAR_REQUEST_PROPERTY_INSTANCE_ID: clearing slot# ",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestedGamerPropSlot
					," of player ",GET_PLAYER_NAME(thePlayer))
		ELSE
			PRINTLN("CLIENT_CLEAR_REQUEST_PROPERTY_INSTANCE_ID: clearing slot# ",GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestedGamerPropSlot)
		ENDIF
		DEBUG_PRINT_GAMER_HANDLE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance)
	ENDIF
	#ENDIF
	CLEAR_GAMER_HANDLE_STRUCT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.requestedGamerForInstance)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iRequestedGamerPropSlot = -1
ENDPROC

/// PURPOSE:
///    Gets the gamer handle of the owner for the property the player is currently inside.
/// PARAMS:
///    player - player to query
///    returnHandle - returned handle
PROC GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_INDEX player,GAMER_HANDLE &ownerHandle)
	ownerHandle = GlobalplayerBD_FM[NATIVE_TO_INT(player)].propertyDetails.requestedGamerForInstance
ENDPROC

FUNC BOOL PROPERTY_NEAR_PLYS_OPT_AVAILABLE(VECTOR vCoords,BOOL &bFriendCrewAvail, BOOL &bOrganisationAvail,  INT iProperty,INT iEntrance, FLOAT fDistance = 15.0, INT iCurrentYacht = -1)
	INT i
	PLAYER_INDEX thePlayer
	//NETWORK_CLAN_DESC myClan
	//NETWORK_CLAN_DESC otherClan
	//GAMER_HANDLE tempHandle
	bFriendCrewAvail = FALSE
	bOrganisationAvail = FALSE
	BOOL bPlayerNearby
	VECTOR vTemp
	IF mpProperties[iProperty].entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE
	OR (iProperty = PROPERTY_YACHT_APT_1_BASE AND iCurrentYacht >= 0 AND mpYachts[iCurrentYacht].yachtPropertyDetails.entrance[iEntrance].iType = ENTRANCE_TYPE_HOUSE)
	OR (IS_PROPERTY_OFFICE(iProperty) AND mpProperties[iProperty].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE)
		REPEAT NUM_NETWORK_PLAYERS i
			thePlayer = INT_TO_PLAYERINDEX(i)
			IF IS_NET_PLAYER_OK(thePlayer)
			AND thePlayer != PLAYER_ID()
			
				//PRINTLN("PROPERTY_NEAR_PLYS_OPT_AVAILABLE: checking ", GET_PLAYER_NAME(thePlayer))
				IF NOT IS_PLAYER_SPECTATING(thePlayer)
					//PRINTLN("PROPERTY_NEAR_PLYS_OPT_AVAILABLE: spectating ")
					vTemp = GET_ENTITY_COORDS(GET_PLAYER_PED(thePlayer))
					IF vTemp.z < -100
						vTemp.z = vCoords.z
					ENDIF
					IF GET_DISTANCE_BETWEEN_COORDS(vTemp,vCoords,TRUE) <= fDistance
						//PRINTLN("PROPERTY_NEAR_PLYS_OPT_AVAILABLE: in range ")
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thePlayer)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)	
						AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(thePlayer)
						AND NOT IS_PLAYER_IN_MP_PROPERTY(thePlayer,FALSE)
							bPlayerNearby = TRUE
							IF GlobalplayerBD_FM_3[NATIVE_TO_INT(thePlayer)].sMagnateGangBossData.GangBossID != INVALID_PLAYER_INDEX()
							AND GlobalplayerBD_FM_3[NATIVE_TO_INT(thePlayer)].sMagnateGangBossData.GangBossID = GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.GangBossID
								bOrganisationAvail = TRUE
							ENDIF
							g_ClanDescription = GET_PLAYER_CREW(PLAYER_ID())
							g_otherClanDescription = GET_PLAYER_CREW(thePlayer)
							IF g_ClanDescription.Id = g_otherClanDescription.Id
								bFriendCrewAvail = TRUE
								//RETURN TRUE
							ELSE
								g_GamerHandle = GET_GAMER_HANDLE_PLAYER(thePlayer)
								IF IS_GAMER_HANDLE_VALID(g_GamerHandle)
									IF NETWORK_IS_FRIEND(g_GamerHandle)	
										bFriendCrewAvail = TRUE
										//RETURN TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF (Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	    OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
	    OR Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID())
		OR bOrganisationAvail
		OR bFriendCrewAvail)
		AND bPlayerNearby
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PROPERTY_NEAR_PLYS_WARNING()
	IF IS_BIT_SET(MPGlobalsAmbience.InvitedToApartmentData.iBitSet,biITA_DoWarningScreen)
		IF globalPropertyEntryData.ownerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(globalPropertyEntryData.ownerID,FALSE,FALSE)
			IF IS_PLAYER_ON_ANY_YACHT(PLAYER_ID())
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("PROP_HEI_WARNc","PROP_HEI_YACHT_WARN",FE_WARNING_SPINNER_ONLY,"",FALSE,-1,WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL,GET_PLAYER_NAME(globalPropertyEntryData.ownerID))
			ELIF IS_PROPERTY_CLUBHOUSE(globalPropertyEntryData.iPropertyEntered)
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("PROP_HEI_WARNc","PROP_CLU_M_10",FE_WARNING_SPINNER_ONLY,"",FALSE,-1,WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL,GET_PLAYER_NAME(globalPropertyEntryData.ownerID))
			ELIF IS_PROPERTY_OFFICE(globalPropertyEntryData.iPropertyEntered)
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("PROP_HEI_WARNc","PROP_OFF_M_10",FE_WARNING_SPINNER_ONLY,"",FALSE,-1,WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL,GET_PLAYER_NAME(globalPropertyEntryData.ownerID))
			ELIF IS_PROPERTY_OFFICE_GARAGE(globalPropertyEntryData.iPropertyEntered)
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("PROP_HEI_WARNc","PROP_OFFG_M_10",FE_WARNING_SPINNER_ONLY,"",FALSE,-1,WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL,GET_PLAYER_NAME(globalPropertyEntryData.ownerID))
			ELSE
				SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS("PROP_HEI_WARNc","PROP_HEI_WARN",FE_WARNING_SPINNER_ONLY,"",FALSE,-1,WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL,GET_PLAYER_NAME(globalPropertyEntryData.ownerID))
			ENDIF
		ELSE
			CLEAR_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet,biITA_DoWarningScreen)
			PRINTLN("MAINTAIN_PROPERTY_NEAR_PLYS_WARNING-owner not ok clearing warning")
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PROP_JIP_MISSING_DATA()
	IF propJipMissingBD.iCounter >= 0
		propJipMissingBD.iCounter++
		IF propJipMissingBD.playerID != INVALID_PLAYER_INDEX()
			IF GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideProperty > 0
				IF g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt <= 0
				
				
					IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_A_GARAGE)
						PACK_INVITED_DATA_INT_2(g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt,GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideProperty,
														GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iRequestedGamerPropSlot,1
														 ,GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideVariation )
						PRINTLN("9252876 sInviteToPropertyDetails.iPackedInt = ",g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt)
					ELSE
						IF IS_PROPERTY_YACHT_APARTMENT(GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideProperty)
							PACK_INVITED_DATA_INT_2(g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt,GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideProperty,
														GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iRequestedGamerPropSlot,0
														 ,GET_YACHT_PLAYER_IS_ON(propJipMissingBD.playerID) )
						ELSE
							PACK_INVITED_DATA_INT_2(g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt,GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideProperty,
														GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iRequestedGamerPropSlot,0
														 ,GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.iCurrentlyInsideVariation )
						ENDIF
						
						PRINTLN("9252876b sInviteToPropertyDetails.iPackedInt = ",g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt)
					ENDIF
					g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.ownerHandle = GlobalplayerBD_FM[NATIVE_TO_INT(propJipMissingBD.playerID)].propertyDetails.requestedGamerForInstance
					PRINTLN("MAINTAIN_PROP_JIP_MISSING_DATA- retrieved data successfully! Counter = ",propJipMissingBD.iCounter)
				ELSE
					PRINTLN("MAINTAIN_PROP_JIP_MISSING_DATA- aborting sInviteToPropertyDetails.iPackedInt = ", g_TransitionSessionNonResetVars.sTransVars.sInviteToPropertyDetails.iPackedInt)
				ENDIF
				propJipMissingBD.iCounter = -1
				propJipMissingBD.playerID = INVALID_PLAYER_INDEX()
			ENDIF
		ENDIF
		IF propJipMissingBD.iCounter > 50
			PRINTLN("MAINTAIN_PROP_JIP_MISSING_DATA- retrieved data FAILED!")
			propJipMissingBD.iCounter = -1
			propJipMissingBD.playerID = INVALID_PLAYER_INDEX()
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_PROPERTY_ENTRANCE_APPROXIMATE_LOCATION(INT iProperty, INT iEntrance)
	VECTOR vReturnVec
	vReturnVec = mpProperties[iProperty].vBlipLocation[iEntrance]
	IF IS_PROPERTY_OFFICE_GARAGE(iProperty)
	OR IS_PROPERTY_OFFICE(iProperty)
	AND mpProperties[iProperty].entrance[iEntrance].iType = ENTRANCE_TYPE_GARAGE
		//IF mpProperties[iProperty].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_3
			vReturnVec = mpProperties[iProperty].entrance[iEntrance].vBuzzerLoc
		//ENDIF
	ENDIF
	RETURN vReturnVec
ENDFUNC

FUNC BOOL PROPERTY_HAS_JUST_ACCEPTED_A_MISSION(#IF IS_DEBUG_BUILD BOOL bDebugOutput = FALSE #ENDIF)
	IF IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN() 
	OR AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
	OR AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
	OR IS_PLAYER_IN_CORONA()
	OR (AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH() AND NOT NETWORK_IS_ACTIVITY_SESSION())
	//OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
	OR IS_ON_CALL_ABOUT_TO_LAUNCH() AND NOT AM_I_TRANSITION_SESSIONS_STARTING_GROUP_ON_CALL()
	OR g_sInviteAcceptanceMP.iaInviteAccepted
   	OR g_sInviteAcceptanceMP.iaMissionUniqueID != NO_UNIQUE_ID
	OR g_joblistCSInviteJustAcceptedTimeout != 0 //from Cross_Session_Invite_Just_Accepted_Timeout
	OR (GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NPC_INVITE AND NOT NETWORK_IS_ACTIVITY_SESSION())
	//OR MPGlobalsAmbience.bJustAcceptedCSInvite_ForGarageScript
		#IF IS_DEBUG_BUILD
			IF bDebugOutput
				IF IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN() 
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION IS_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN TRUE")
				ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE()
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE TRUE")
				ELIF AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP()
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_MP TRUE")
				ELIF IS_PLAYER_IN_CORONA()
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION IS_PLAYER_IN_CORONA TRUE")
				ELIF (AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH() AND NOT NETWORK_IS_ACTIVITY_SESSION())
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION AM_I_STARTING_TRANSITION_SESSIONS_QUICK_MATCH TRUE")
				//OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
				ELIF IS_ON_CALL_ABOUT_TO_LAUNCH()
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION IS_ON_CALL_ABOUT_TO_LAUNCH TRUE")
				ELIF g_sInviteAcceptanceMP.iaInviteAccepted
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION iaInviteAccepted TRUE")
			   	ELIF g_sInviteAcceptanceMP.iaMissionUniqueID != NO_UNIQUE_ID
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION g_sInviteAcceptanceMP.iaMissionUniqueID = ",g_sInviteAcceptanceMP.iaMissionUniqueID)
				//ELIF MPGlobalsAmbience.bJustAcceptedCSInvite_ForGarageScript
				//	PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION bJustAcceptedCSInvite_ForGarageScript TRUE")
				ELIF g_joblistCSInviteJustAcceptedTimeout != 0
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION g_joblistCSInviteJustAcceptedTimeout != 0")	
				ELIF (GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NPC_INVITE AND NOT NETWORK_IS_ACTIVITY_SESSION())
					PRINTLN("PROPERTY_HAS_JUST_ACCEPTED_A_MISSION GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NPC_INVITE")	
				ENDIF
			ENDIF
		#ENDIF
		RETURN(TRUE)
//	ELSE
		
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SETUP_PROPERTY_BLOCKS(PROPERTY_BLOCKS_STRUCT &PropertyBlockers)
	// NOTE: Max scenario blocks in-game is 36 and 128 vehicle generator blocks.
	
	IF PropertyBlockers.bAddedBlocks
		PRINTLN("SETUP_PROPERTY_BLOCKS - Ignored, already added")
	ELSE
		PRINTLN("SETUP_PROPERTY_BLOCKS - START")
		//PROPERTY_IND_DAY_MEDIUM_3
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-98.8771, 6539.5850, 28.5798>>,<<-111.7676, 6528.2202, 32.9978>>,FALSE,FALSE)
		//PROPERTY_IND_DAY_MEDIUM_4
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-289.6679, 6343.9971, 29.0203>>,<<-313.3764, 6327.3550, 35.3341>>,FALSE,FALSE)
		//PROPERTY_IND_DAY_LOW_1
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-21.1402, 6570.8262, 28.8513>>,<<6.8527, 6548.1938, 34.6786>>,FALSE,FALSE)
		//PROPERTY_IND_DAY_LOW_2
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1904.6899, 3785.7114, 30.9484>>,<<1885.4091, 3754.1255, 35.2442>>,FALSE,FALSE)
		//PROPERTY_IND_DAY_LOW_3
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1655.1740, 4762.5986, 40.4904>>,<<1669.6506, 4781.7778, 46.0761>>,FALSE,FALSE)
		//CLUBHOUSE_1
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<257.3543, -1798.444, 26.1131>>, <<247.3363, -1799.4766, 36.1131>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<54.597050,2778.643311,56.884029>>, <<48.024754,2783.809814,59.384029>>, FALSE, FALSE)
		//SCRAPYARD_3
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-195.90439, 6267.37598, 35.42977>>, <<-202.91502, 6275.58643, 30.49142>>, FALSE, FALSE)
		//GANGSTER_2
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1927.00867, 2040.86633, 139.83293>>, <<-1913.89441, 2055.43970, 144.0>>, FALSE, FALSE)
		//GANGSTER_4
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1398.77515, 1123.02393, 114.31349>>, <<1416.64966, 1114.64282, 116.83796>>, FALSE, FALSE)
		//GANGSTER_7
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1620.08118, 15.07020, 61.53333>>, <<-1612.05518, 24.19521, 64.0>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1601.14575, 44.94703, 59.94626>>, <<-1607.36401, 36.75127, 63.0>>, FALSE, FALSE)
		// Senora Desert Bunker 2
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<822.93378, 2980.64429, 45.0>>, <<815.13275, 2972.21948, 51.0>>, FALSE, FALSE) //13
		// LSIA and Fort Zancudo vehicle generators.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1118.92065, -3345.89136, 11.94505>>, <<-1034.51416, -3425.06396, 25.14633>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1381.51501, -3248.52588, 11.94482>>, <<-1399.41113, -3266.81470, 22.94482>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1403.18799, -3241.23706, 11.94495>>, <<-1417.63635, -3255.97119, 22.94495>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1351.99451, -3269.27881, 11.94482>>, <<-1371.16443, -3293.00439, 22.94482>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1427.50159, -3240.29565, 11.94495>>, <<-1453.34338, -3272.00610, 20.10083>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2045.03406, 3128.08398, 30.81032>>, <<-2008.64404, 3181.15088, 40.81032>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2040.15613, 3127.42139, 30.81032>>, <<-2062.06665, 3092.40210, 40.81032>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2082.06470, 3142.81396, 30.81031>>, <<-2095.95654, 3125.45215, 40.81032>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2105.33032, 3159.24292, 30.81014>>, <<-2126.61108, 3127.80200, 40.81015>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2162.06909, 3131.54541, 30.81013>>, <<-2121.09619, 3189.24219, 40.81014>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2031.31555, 3062.31738, 30.81032>>, <<-1989.58911, 3108.73511, 40.81032>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1997.32520, 3055.76929, 30.81031>>, <<-1973.44910, 3084.02197, 40.81032>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1912.22498, 3067.79834, 30.81032>>, <<-1866.64746, 3115.50098, 40.81026>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1941.37134, 3113.51733, 30.81032>>, <<-1902.89441, 3150.58887, 40.81026>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1963.53333, 3091.97754, 30.81032>>, <<-1912.41150, 3110.87842, 40.81032>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1923.27283, 2975.99487, 30.80994>>, <<-1881.56750, 3024.42212, 40.81049>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1965.81726, 3021.05591, 30.81032>>, <<-1941.92371, 3047.77344, 40.81031>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2492.33154, 3247.46777, 30.82765>>, <<-2454.70898, 3289.07568, 40.97783>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-989.62207, -3448.27100, 20.0>>, <<-1110.13501, -3565.27954, 0.0>>, FALSE, FALSE)
		// Nightclub Locations.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<696.31470, -1296.98047, 23.88614>>, <<771.35748, -1359.64331, 29.52297>>, FALSE, FALSE) //33
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<506.19717, -941.08405, 23.77671>>, <<489.76968, -888.48682, 32.61002>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-194.88725, -1305.98071, 35.34710>>, <<-100.65156, -1248.84839, 27.24532>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-73.45308, 178.98370, 86.25685>>, <<31.96091, 251.98502, 108.58942>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<931.73132, -2075.68237, 28.61308>>, <<861.04425, -2138.83936, 33.52427>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1307.70081, -638.03784, 20.54498>>, <<-1217.77905, -709.91797, 30.15923>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-685.46332, -2380.20190, 11.97208>>, <<-668.93805, -2398.39160, 20.33201>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<232.24309, -3066.01563, 3.80817>>, <<193.71828, -3116.08252, 9.79027>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1226.56836, -3208.10034, 3.02805>>, <<1254.95508, -3144.50366, 10.80012>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1150.45642, -1188.08484, 3.64667>>, <<-1176.40833, -1131.24475, 8.70080>>, FALSE, FALSE)
		// Secondary mission drop-off locations.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1279.91553, -1357.03735, 7.35086>>, <<-1271.29358, -1367.61414, 3.30179>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-335.77542, -1391.79199, 28.74735>>, <<-314.30673, -1412.69214, 38.09612>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-237.81039, 6358.63281, 35.30618>>, <<-212.10005, 6335.13232, 30.35491>>, FALSE, FALSE) //45
		
		#IF FEATURE_CASINO_HEIST
		//Arcade - Davis
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-133.5588, -1781.5275, 28.4468>>, <<-118.7488, -1766.1646, 36.5068>>, FALSE, FALSE)
		#ENDIF
		
		#IF FEATURE_TUNER
		// Car Meet.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<794.733643,-1799.052124,33.263752>>, <<800.822693,-1792.694336,27.332460>>, FALSE, FALSE) //47
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<789.692749,-1827.854492,33.284607>>, <<795.794189,-1821.625366,27.381058>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<782.373596,-1853.710083,33.292152>>, <<791.604431,-1845.057861,27.401352>>, FALSE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<771.275269,-1879.936768,27.249935>>, <<775.169067,-1873.342285,33.293999>>, FALSE, FALSE)
		#ENDIF
		
		PRINTLN("SETUP_PROPERTY_BLOCKS - END")
		PropertyBlockers.bAddedBlocks = TRUE
	ENDIF
ENDPROC

PROC CLEANUP_PROPERTY_BLOCKS(PROPERTY_BLOCKS_STRUCT &PropertyBlockers)	
	IF NOT PropertyBlockers.bAddedBlocks
		PRINTLN("CLEANUP_PROPERTY_BLOCKS - Ignored, never set.")
	ELSE
		PRINTLN("CLEANUP_PROPERTY_BLOCKS - START")
		//PROPERTY_IND_DAY_MEDIUM_3
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-98.8771, 6539.5850, 28.5798>>,<<-111.7676, 6528.2202, 32.9978>>,TRUE,FALSE)
		//PROPERTY_IND_DAY_MEDIUM_4
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-289.6679, 6343.9971, 29.0203>>,<<-313.3764, 6327.3550, 35.3341>>,TRUE,FALSE)
		//PROPERTY_IND_DAY_LOW_1
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-21.1402, 6570.8262, 28.8513>>,<<6.8527, 6548.1938, 34.6786>>,TRUE,FALSE)
		//PROPERTY_IND_DAY_LOW_2
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1904.6899, 3785.7114, 30.9484>>,<<1885.4091, 3754.1255, 35.2442>>,TRUE,FALSE)
		//PROPERTY_IND_DAY_LOW_3
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1655.1740, 4762.5986, 40.4904>>,<<1669.6506, 4781.7778, 46.0761>>,TRUE,FALSE)
		//CLUBHOUSE_1
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<257.3543, -1798.444, 26.1131>>, <<247.3363, -1799.4766, 36.1131>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<54.597050,2778.643311,56.884029>>, <<48.024754,2783.809814,59.384029>>, TRUE, FALSE)
		//SCRAPYARD_3
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-195.90439, 6267.37598, 35.42977>>, <<-202.91502, 6275.58643, 30.49142>>, TRUE, FALSE)
		// GANGSTER_2
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1927.00867, 2040.86633, 139.83293>>, <<-1913.89441, 2055.43970, 144.0>>, TRUE, FALSE)
		// GANGSTER_4
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1398.77515, 1123.02393, 114.31349>>, <<1416.64966, 1114.64282, 116.83796>>, TRUE, FALSE)
		// GANGSTER_7
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1620.08118, 15.07020, 61.53333>>, <<-1612.05518, 24.19521, 64.0>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1601.14575, 44.94703, 59.94626>>, <<-1607.36401, 36.75127, 63.0>>, TRUE, FALSE)
		// Senora Desert Bunker 2
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<822.93378, 2980.64429, 45.0>>, <<815.13275, 2972.21948, 51.0>>, TRUE, FALSE)
		// LSIA and Fort Zancudo vehicle generators.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1118.92065, -3345.89136, 11.94505>>, <<-1034.51416, -3425.06396, 25.14633>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1381.51501, -3248.52588, 11.94482>>, <<-1399.41113, -3266.81470, 22.94482>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1403.18799, -3241.23706, 11.94495>>, <<-1417.63635, -3255.97119, 22.94495>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1351.99451, -3269.27881, 11.94482>>, <<-1371.16443, -3293.00439, 22.94482>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1427.50159, -3240.29565, 11.94495>>, <<-1453.34338, -3272.00610, 20.10083>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2045.03406, 3128.08398, 30.81032>>, <<-2008.64404, 3181.15088, 40.81032>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2040.15613, 3127.42139, 30.81032>>, <<-2062.06665, 3092.40210, 40.81032>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2082.06470, 3142.81396, 30.81031>>, <<-2095.95654, 3125.45215, 40.81032>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2105.33032, 3159.24292, 30.81014>>, <<-2126.61108, 3127.80200, 40.81015>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2162.06909, 3131.54541, 30.81013>>, <<-2121.09619, 3189.24219, 40.81014>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2031.31555, 3062.31738, 30.81032>>, <<-1989.58911, 3108.73511, 40.81032>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1997.32520, 3055.76929, 30.81031>>, <<-1973.44910, 3084.02197, 40.81032>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1912.22498, 3067.79834, 30.81032>>, <<-1866.64746, 3115.50098, 40.81026>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1941.37134, 3113.51733, 30.81032>>, <<-1902.89441, 3150.58887, 40.81026>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1963.53333, 3091.97754, 30.81032>>, <<-1912.41150, 3110.87842, 40.81032>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1923.27283, 2975.99487, 30.80994>>, <<-1881.56750, 3024.42212, 40.81049>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1965.81726, 3021.05591, 30.81032>>, <<-1941.92371, 3047.77344, 40.81031>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2492.33154, 3247.46777, 30.82765>>, <<-2454.70898, 3289.07568, 40.97783>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-989.62207, -3448.27100, 20.0>>, <<-1110.13501, -3565.27954, 0.0>>, TRUE, FALSE)
		// Nightclub Locations.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<696.31470, -1296.98047, 23.88614>>, <<771.35748, -1359.64331, 29.52297>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<506.19717, -941.08405, 23.77671>>, <<489.76968, -888.48682, 32.61002>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-194.88725, -1305.98071, 35.34710>>, <<-100.65156, -1248.84839, 27.24532>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-73.45308, 178.98370, 86.25685>>, <<31.96091, 251.98502, 108.58942>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<931.73132, -2075.68237, 28.61308>>, <<861.04425, -2138.83936, 33.52427>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1307.70081, -638.03784, 20.54498>>, <<-1217.77905, -709.91797, 30.15923>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-685.46332, -2380.20190, 11.97208>>, <<-668.93805, -2398.39160, 20.33201>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<232.24309, -3066.01563, 3.80817>>, <<193.71828, -3116.08252, 9.79027>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1226.56836, -3208.10034, 3.02805>>, <<1254.95508, -3144.50366, 10.80012>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1150.45642, -1188.08484, 3.64667>>, <<-1176.40833, -1131.24475, 8.70080>>, TRUE, FALSE)
		// Secondary mission drop-off locations.
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1279.91553, -1357.03735, 7.35086>>, <<-1271.29358, -1367.61414, 3.30179>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-335.77542, -1391.79199, 28.74735>>, <<-314.30673, -1412.69214, 38.09612>>, TRUE, FALSE)
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-237.81039, 6358.63281, 35.30618>>, <<-212.10005, 6335.13232, 30.35491>>, TRUE, FALSE)
		
		#IF FEATURE_CASINO_HEIST
		//Arcade - Davis
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-133.5588, -1781.5275, 28.4468>>, <<-118.7488, -1766.1646, 36.5068>>, TRUE, FALSE)
		#ENDIF
		
		PRINTLN("CLEANUP_PROPERTY_BLOCKS - END")
	ENDIF
ENDPROC

//ADDED for storing transactions on LG/NG to match PC
PROC GENERATE_PROPERTY_KEY_FOR_CATALOGUE_NONPC(TEXT_LABEL_63 &tlGeneratedKey, INT iPropertyIndex)
	
	TEXT_LABEL_15 tlPropertyLabel = GET_PROPERTY_NAME(iPropertyIndex)
	
	tlGeneratedKey = "PR_"
	tlGeneratedKey += tlPropertyLabel
	tlGeneratedKey += "_t0_v0"
ENDPROC

FUNC INT GET_PROPERTY_KEY_FOR_CATALOGUE_NONPC(INT iPropertyIndex)
	TEXT_LABEL_63 tlCategoryKey
	GENERATE_PROPERTY_KEY_FOR_CATALOGUE_NONPC(tlCategoryKey, iPropertyIndex)
	PRINTLN("[CATALOGUE_KEY] key '", tlCategoryKey, "' = ", GET_HASH_KEY(tlCategoryKey))
	RETURN GET_HASH_KEY(tlCategoryKey)
ENDFUNC

PROC SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PREVENT_PROPERTY_ACCESS_FLAGS flag)
	SET_BIT(g_iPreventPropertyAccessBS,ENUM_TO_INT(flag))
	DEBUG_PRINTCALLSTACK()
	PRINTLN("SET_PLAYER_BLOCK_FROM_PROPERTY_FLAG: setting flag #",flag)
ENDPROC

PROC CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG(PREVENT_PROPERTY_ACCESS_FLAGS flag)
	CLEAR_BIT(g_iPreventPropertyAccessBS,ENUM_TO_INT(flag))
	DEBUG_PRINTCALLSTACK()
	PRINTLN("CLEAR_PLAYER_BLOCK_FROM_PROPERTY_FLAG: clearing flag #",flag)
ENDPROC

FUNC BOOL IS_PLAYER_BLOCK_FROM_PROPERTY_FLAG_SET(PREVENT_PROPERTY_ACCESS_FLAGS flag)
	RETURN IS_BIT_SET(g_iPreventPropertyAccessBS,ENUM_TO_INT(flag))
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_CRITIAL_BUT_ALLOWED_IN_PROPERTY(PLAYER_INDEX playerID)
	INT iMisionType = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID)
	SWITCH iMisionType
		CASE FMMC_TYPE_GB_CONTRABAND_BUY
		CASE FMMC_TYPE_GB_CONTRABAND_SELL
		CASE FMMC_TYPE_BIKER_CONTRABAND_SELL
		CASE FMMC_TYPE_VEHICLE_EXPORT_BUY
		CASE FMMC_TYPE_FIXER_SECURITY
		CASE FMMC_TYPE_FIXER_VIP
		#IF FEATURE_DLC_1_2022
		CASE FMMC_TYPE_EXPORT_CARGO
		CASE FMMC_TYPE_SPECIAL_CARGO
		CASE FMMC_TYPE_CLUBHOUSE_CONTRACTS
		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_CRITIAL_BUT_ALLOWED_IN_PROPERTY()
	RETURN IS_THIS_PLAYER_CRITIAL_BUT_ALLOWED_IN_PROPERTY(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_CLUBHOUSE(PLAYER_INDEX playerID)
	#IF FEATURE_GEN9_EXCLUSIVE
	IF SHOULD_ALLOW_CLUBHOUSE_ENTRY_DURING_MISSION()
		RETURN TRUE
	ENDIF
	#ENDIF	
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
		INT iMission = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID)
		
		IF GB_IS_BIKER_CLUBHOUSE_MISSION(iMission)
		OR iMission = FMMC_TYPE_CLUBHOUSE_CONTRACTS	
		OR iMission = FMMC_TYPE_BIKE_SHOP_DELIVERY
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_CLUBHOUSE()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_CLUBHOUSE(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_BUNKER(PLAYER_INDEX playerID)
	IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GUNRUNNING_BUY
	//IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_GUNRUNNING_SELL
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_BUNKER()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_BUNKER(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_HANGAR(PLAYER_INDEX playerID)
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_SMUGGLER_BUY
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF

	// Always allow a player in the hangar by default.
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_HANGAR()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_HANGAR(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_DEFUNCT_BASE(PLAYER_INDEX playerID)
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
		
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_FM_GANGOPS
			RETURN TRUE
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_DEFUNCT_BASE()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_DEFUNCT_BASE(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_NIGHTCLUB(PLAYER_INDEX playerID)

	IF playerID <> INVALID_PLAYER_INDEX()
		
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_NIGHTCLUB()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_NIGHTCLUB(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_PLAYER_ON_NIGHTCLUB_SETUP_OR_DJ_MISSION(PLAYER_INDEX player)
	
	IF player = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	//This returns invalid if the player is not on the correct mission type so is safe to check without checking the mission first
	MEGA_BUSINESS_MISSION_VARIATION eVariation = GB_GET_MEGA_BUSINESS_VARIATION_PLAYER_IS_ON(player)
	
	IF eVariation = MBV_COLLECT_STAFF
	OR eVariation = MBV_COLLECT_EQUIPMENT
	OR eVariation = MBV_COLLECT_DJ_STOLEN_BAG
	OR eVariation = MBV_COLLECT_DJ_COLLECTOR
	OR eVariation = MBV_COLLECT_DJ_CRASH
	OR eVariation = MBV_COLLECT_DJ_HOOKED
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_BUSINESS_HUB(PLAYER_INDEX playerID)

	IF playerID <> INVALID_PLAYER_INDEX()
		
		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
			IF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_FMBB_SELL AND NOT IS_PLAYER_ON_NIGHTCLUB_SETUP_OR_DJ_MISSION(playerID))
			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_FMBB_DEFEND
				RETURN FALSE
			ENDIF
		ENDIF
		
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_BUSINESS_HUB()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_BUSINESS_HUB(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_ARENA_GARAGE(PLAYER_INDEX playerID)

	IF playerID <> INVALID_PLAYER_INDEX()
		
//		IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(playerID)
//			IF (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_FMBB_SELL AND NOT IS_PLAYER_ON_NIGHTCLUB_SETUP_OR_DJ_MISSION(playerID))
//			OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(playerID) = FMMC_TYPE_FMBB_DEFEND
//				RETURN FALSE
//			ENDIF
//		ENDIF
		
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_ARENA_GARAGE()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_BUSINESS_HUB(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_HACKER_TRUCK(PLAYER_INDEX playerID)

	IF playerID <> INVALID_PLAYER_INDEX()
				
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_HACKER_TRUCK()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_HACKER_TRUCK(PLAYER_ID())
ENDFUNC

FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_CASINO_APT(PLAYER_INDEX playerID)

	IF playerID <> INVALID_PLAYER_INDEX()
		//Add checks here if reqired
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_CASINO_APT()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_CASINO_APT(PLAYER_ID())
ENDFUNC

#IF FEATURE_HEIST_ISLAND
FUNC BOOL IS_THIS_PLAYER_ALLOWED_IN_SMPL_INT_SUBMARINE(PLAYER_INDEX playerID)

	IF playerID <> INVALID_PLAYER_INDEX()
		//Add checks here if reqired
		RETURN TRUE	
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ALLOWED_IN_SMPL_INT_SUBMARINE()
	RETURN IS_THIS_PLAYER_ALLOWED_IN_SMPL_INT_SUBMARINE(PLAYER_ID())
ENDFUNC
#ENDIF

/// PURPOSE: Added this on 25/07/2016 to stop the grownig list of BOOLs in arguments of IS_PLAYER_BLOCKED_FROM_PROPERTY (bIsOffice, bIsWarehouse, bIsClubhouse etc.)
///    This required a new enum to be created as it mixes together all properties such as apartments, offices, warehouses, factories etc.
///    (usually apartmens/offices are completely separate systems from warehouses/factories)
ENUM PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY
	PROPERTY_STANDARD, // Apartments, garages etc.
	PROPERTY_OFFICE,
	PROPERTY_WAREHOUSE,
	PROPERTY_CLUBHOUSE,
	PROPERTY_FACTORY,
	PROPERTY_IE_WAREHOUSE
	
	, PROPERTY_BUNKER
	, PROPERTY_ARMORY_TRUCK
	, PROPERTY_SM_HANGAR
	, PROPERTY_DEFUNCT_BASE
	, PROPERTY_TYPE_NIGHTCLUB
	, PROPERTY_BUSINESS_HUB
	, PROPERTY_HACKER_TRUCK
	, PROPERTY_ARENA_GARAGE
#IF FEATURE_DLC_2_2022
	, PROPERTY_ACID_LAB
#ENDIF
ENDENUM

/// PURPOSE:
///    THIS ONLY WORKS WHEN PASSED ID IS ACTUAL PROPERTY (so apartments, offices, clubhouses, NOT warehouses/factories)
/// PARAMS:
///    iPropertyID - 
/// RETURNS:
///    
FUNC PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(INT iPropertyID)
	IF IS_PROPERTY_OFFICE(iPropertyID)
	//OR IS_PROPERTY_OFFICE_GARAGE(iPropertyID)
		RETURN PROPERTY_OFFICE
	ENDIF
	
	IF IS_PROPERTY_CLUBHOUSE(iPropertyID)
		RETURN PROPERTY_CLUBHOUSE
	ENDIF
	
	IF IS_PROPERTY_ID_DEFUNCT_BASE(iPropertyID)
		RETURN PROPERTY_DEFUNCT_BASE
	ENDIF
	
	IF iPropertyID = PROPERTY_NIGHTCLUB
	OR iPropertyID = PROPERTY_MEGAWARE_GARAGE_LVL1
	OR iPropertyID = PROPERTY_MEGAWARE_GARAGE_LVL2
	OR iPropertyID = PROPERTY_MEGAWARE_GARAGE_LVL3
		RETURN PROPERTY_BUSINESS_HUB
	ENDIF
	
	IF iPropertyID = PROPERTY_ARENAWARS_GARAGE_LVL1
	OR iPropertyID = PROPERTY_ARENAWARS_GARAGE_LVL2
	OR iPropertyID = PROPERTY_ARENAWARS_GARAGE_LVL3
		RETURN PROPERTY_ARENA_GARAGE
	ENDIF
	
	RETURN PROPERTY_STANDARD
ENDFUNC

FUNC BOOL IS_PLAYER_BLOCKED_FROM_PROPERTY(BOOL bPrintReason = FALSE, PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY ePropertyType = PROPERTY_STANDARD)

	BOOL bIsWarehouse = FALSE
	BOOL bIsOffice = FALSE
	
	IF ePropertyType = PROPERTY_OFFICE
		bIsOffice = TRUE
	ELIF ePropertyType = PROPERTY_WAREHOUSE
		bIsWarehouse = TRUE
	ENDIF
	
	IF g_iPreventPropertyAccessBS != 0
		IF NOT bIsWarehouse
		AND ePropertyType != PROPERTY_FACTORY
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE g_iPreventPropertyAccessBS != 0")
			ENDIF
			RETURN TRUE
		ELIF bIsWarehouse
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAF_CONTRABAND))
			
			IF iFlagsCopy > 0
				IF bPrintReason
					PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for Warehouse, g_iPreventPropertyAccessBS != 0 (PPAF_CONTRABAND is ignored)")
				ENDIF
				RETURN TRUE
			ENDIF
		ELIF ePropertyType = PROPERTY_FACTORY
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAF_ILLICIT_GOOD))
			
			IF iFlagsCopy > 0
				IF bPrintReason
					PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for Factory, g_iPreventPropertyAccessBS != 0 (PPAF_ILLICIT_GOOD is ignored)")
				ENDIF
				RETURN TRUE
			ENDIF
		ELIF ePropertyType = PROPERTY_BUNKER
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAF_GUN_RUNNING))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for Factory, g_iPreventPropertyAccessBS != 0 (PPAF_GUN_RUNNING is ignored)")
			ENDIF
				
			RETURN TRUE
		ELIF ePropertyType = PROPERTY_SM_HANGAR
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAF_HANGAR))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for hangar, g_iPreventPropertyAccessBS != 0 (PPAF_HANGAR is ignored)")
			ENDIF
			
			RETURN TRUE				
		ELIF ePropertyType = PROPERTY_DEFUNCT_BASE
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAF_DEFUNCT_BASE))
		
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for defunct base, g_iPreventPropertyAccessBS != 0 (PPAF_DEFUNCT_BASE is ignored)")
			ENDIF
		
			RETURN TRUE
		ELIF ePropertyType = PROPERTY_TYPE_NIGHTCLUB
			
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAG_NIGHTCLUB))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for nightclub, g_iPreventPropertyAccessBS != 0 (PPAF_NIGHTCLUB is ignored)")
			ENDIF
			
			RETURN TRUE
			
		ELIF ePropertyType = PROPERTY_BUSINESS_HUB	
			
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAG_BUSINESS_HUB))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for business hub, g_iPreventPropertyAccessBS != 0 (PPAG_BUSINESS_HUB is ignored)")
			ENDIF
			
			RETURN TRUE
		ELIF ePropertyType = PROPERTY_HACKER_TRUCK
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAG_HACKER_TRUCK))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for business hub, g_iPreventPropertyAccessBS != 0 (PPAG_HACKER_TRUCK is ignored)")
			ENDIF
			
			RETURN TRUE
		ELIF ePropertyType = PROPERTY_ARENA_GARAGE
			
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAG_ARENA_GARAGE))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for business hub, g_iPreventPropertyAccessBS != 0 (PPAG_ARENA_GARAGE is ignored)")
			ENDIF
			
			RETURN TRUE		
#IF FEATURE_DLC_2_2022
		ELIF ePropertyType = PROPERTY_ACID_LAB
			INT iFlagsCopy = g_iPreventPropertyAccessBS
			CLEAR_BIT(iFlagsCopy, ENUM_TO_INT(PPAG_ACID_LAB))
			
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE for business hub, g_iPreventPropertyAccessBS != 0 (PPAG_ACID_LAB is ignored)")
			ENDIF
			
			RETURN TRUE
#ENDIF
		ENDIF
	ENDIF
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
	AND NOT IS_PLAYER_CRITIAL_BUT_ALLOWED_IN_PROPERTY()
	AND NOT (ePropertyType = PROPERTY_FACTORY 			AND GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(PLAYER_ID()))
	AND NOT (ePropertyType = PROPERTY_CLUBHOUSE 		AND IS_LOCAL_PLAYER_ALLOWED_IN_CLUBHOUSE())
	AND NOT (ePropertyType = PROPERTY_BUNKER 			AND IS_LOCAL_PLAYER_ALLOWED_IN_BUNKER())
	AND NOT (ePropertyType = PROPERTY_ARMORY_TRUCK 		AND IS_LOCAL_PLAYER_ALLOWED_IN_BUNKER())	// setting same as bunker 
	AND NOT (ePropertyType = PROPERTY_SM_HANGAR 		AND IS_LOCAL_PLAYER_ALLOWED_IN_HANGAR())
	AND NOT (ePropertyType = PROPERTY_DEFUNCT_BASE 		AND IS_LOCAL_PLAYER_ALLOWED_IN_DEFUNCT_BASE())	
	AND NOT (ePropertyType = PROPERTY_TYPE_NIGHTCLUB 	AND IS_LOCAL_PLAYER_ALLOWED_IN_NIGHTCLUB())
	AND NOT (ePropertyType = PROPERTY_BUSINESS_HUB 		AND IS_LOCAL_PLAYER_ALLOWED_IN_BUSINESS_HUB())
	AND NOT (ePropertyType = PROPERTY_HACKER_TRUCK 		AND IS_LOCAL_PLAYER_ALLOWED_IN_HACKER_TRUCK())
	AND NOT (ePropertyType = PROPERTY_ARENA_GARAGE		AND IS_LOCAL_PLAYER_ALLOWED_IN_ARENA_GARAGE())
	//AND NOT (ePropertyType = PROPERTY_ACID_LAB/*TODO*/ AND IS_LOCAL_PLAYER_ALLOWED_IN_BUSINESS_HUB())
	#IF FEATURE_GEN9_EXCLUSIVE
	AND NOT (ePropertyType = PROPERTY_OFFICE			AND SHOULD_ALLOW_OFFICE_ENTRY_DURING_MISSION())
	#ENDIF
		PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
		PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL)
		PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE MPAM_TYPE_TIME_TRIAL")
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RC_TIME_TRIAL)
		PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE MPAM_TYPE_RC_TIME_TRIAL")
		RETURN TRUE
	ENDIF
	
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
	AND NETWORK_IS_ACTIVITY_SESSION()
		IF bPrintReason
			PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE player is in SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW")
		ENDIF
		RETURN TRUE
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: true player is an animal")
		RETURN TRUE
	ENDIF

	IF bIsOffice
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION_CONTACT
		OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CONTACT
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE player is in FMMC_TYPE_MISSION_CONTACT")
			ENDIF
			RETURN TRUE
		ENDIF
		
		IF g_sMPTunables.bexec_disable_office_entry
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE g_sMPTunables.bexec_disable_office_entry")
			ENDIF
			RETURN TRUE
		ENDIF
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			IF bPrintReason
				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE g_sMPTunables.bexec_disable_office_entry")
			ENDIF
			RETURN TRUE
		ENDIF
//		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)
//			IF bPrintReason
//				PRINTLN("IS_PLAYER_BLOCKED_FROM_PROPERTY: TRUE player is an associate")
//			ENDIF
//			RETURN TRUE
//		ENDIF
	ENDIF
//	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInfectionParticipant
//		RETURN TRUE
//	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_YACHT_BATHROBE_HELP(PLAYER_INDEX theOwner, BOOL &bCheckedStatus)
	IF NOT bCheckedStatus
		INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT2)
		IF NOT IS_BIT_SET(iStatInt,biFmGb_Help2_Bathrobe)
			IF NOT NETWORK_IS_IN_MP_CUTSCENE()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF theOwner = PLAYER_ID()
						PRINT_HELP("PY_BATHROBE0")
					ELSE
						PRINT_HELP("PY_BATHROBE1")
					ENDIF
					PRINTLN("MAINTAIN_YACHT_BATHROBE_HELP: displaying help")
				ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PY_BATHROBE0")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PY_BATHROBE1")
					bCheckedStatus = TRUE
					SET_BIT(iStatInt,biFmGb_Help2_Bathrobe)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_GB_SYSTEM_HELP_TEXT2,iStatInt)
					PRINTLN("MAINTAIN_YACHT_BATHROBE_HELP: help being displayed set stat")
				ENDIF
			ENDIF
		ELSE
			bCheckedStatus = TRUE
			PRINTLN("MAINTAIN_YACHT_BATHROBE_HELP: stat already set")
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL IS_PROPERTY_SALE_BLOCKED_BY_TUNEABLES(INT iProperty)
	IF g_sMPTunables.bapt_disable_custom1 AND iProperty = PROPERTY_CUSTOM_APT_1_BASE
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_custom2 AND iProperty = PROPERTY_CUSTOM_APT_2
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_custom3 AND iProperty = PROPERTY_CUSTOM_APT_3
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt1 AND iProperty = PROPERTY_STILT_APT_1_BASE_B
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt2 AND iProperty = PROPERTY_STILT_APT_2_B
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt3 AND iProperty = PROPERTY_STILT_APT_3_B
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt4 AND iProperty = PROPERTY_STILT_APT_4_B
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt5 AND iProperty = PROPERTY_STILT_APT_5_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt6 AND iProperty = PROPERTY_STILT_APT_7_A
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt7 AND iProperty = PROPERTY_STILT_APT_8_A
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt8 AND iProperty = PROPERTY_STILT_APT_10_A
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt9 AND iProperty = PROPERTY_STILT_APT_12_A
		RETURN TRUE
	ELIF g_sMPTunables.bapt_disable_stilt10 AND iProperty = PROPERTY_STILT_APT_13_A
		RETURN TRUE
	ENDIF
	
	IF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_0 AND iProperty = PROPERTY_CLUBHOUSE_1_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_1 AND iProperty = PROPERTY_CLUBHOUSE_2_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_2 AND iProperty = PROPERTY_CLUBHOUSE_3_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_3 AND iProperty = PROPERTY_CLUBHOUSE_4_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_4 AND iProperty = PROPERTY_CLUBHOUSE_5_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_5 AND iProperty = PROPERTY_CLUBHOUSE_6_BASE_A
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_6 AND iProperty = PROPERTY_CLUBHOUSE_7_BASE_B
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_7 AND iProperty = PROPERTY_CLUBHOUSE_8_BASE_B
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_8 AND iProperty = PROPERTY_CLUBHOUSE_9_BASE_B
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_9 AND iProperty = PROPERTY_CLUBHOUSE_10_BASE_B
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_10 AND iProperty = PROPERTY_CLUBHOUSE_11_BASE_B
		RETURN TRUE
	ELIF g_sMPTunables.bBIKER_DISABLE_PURCHASE_CLUBHOUSE_11 AND iProperty = PROPERTY_CLUBHOUSE_12_BASE_B
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ACTIVATE_IPLS_FOR_STILT_APARTMENT(INT iProperty)
	
	IF iProperty = PROPERTY_STILT_APT_8_A
		IF NOT IS_IPL_ACTIVE("apa_stilt_ch2_09c_int")
			REQUEST_IPL("apa_stilt_ch2_09c_int")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_STILT_APARTMENT: activating apa_stilt_ch2_09c_int")
		ENDIF 
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "ACTIVATE_IPLS_FOR_STILT_APARTMENT: iProperty = ", iProperty, " != PROPERTY_STILT_APT_8_A, not setting any IPLS.")
	ENDIF
	
ENDPROC

FUNC VECTOR GET_OFFICE_MOD_GARAGE_INTERIOR_COORD(INT iProperty)
	
	SWITCH GET_OFFICE_TYPE_FROM_OFFICE_GARAGE(iProperty)
		CASE PROPERTY_OFFICE_1
			RETURN <<-1578.0225,-576.4251,104.2000>>
		BREAK
		CASE PROPERTY_OFFICE_2_BASE
			RETURN <<-1391.2450,-473.9638,77.200>>
		BREAK
		CASE PROPERTY_OFFICE_3
			RETURN <<-146.6167,-596.6301,166.0000>>
		BREAK
		CASE PROPERTY_OFFICE_4
			RETURN <<-73.9040,-821.6204,284.0000>>
		BREAK 
	ENDSWITCH
	
	PRINTLN("GET_OFFICE_MOD_GARAGE_INTERIOR_COORD - Invalid property")
	RETURN <<0,0,0>>
ENDFUNC


PROC TURN_ON_INTERIORS_FOR_CUSTOM_PROPERTY(INTERIOR_INSTANCE_INDEX &interiorID,INT iProperty, INT iVariation, BOOL bIsModGarage = FALSE)
	MP_PROP_OFFSET_STRUCT tempOffset = GET_BASE_INTERIOR_LOCATION(iProperty)
	INTERIOR_NAME_ENUM theInteriorEnum 
	INTERIOR_NAME_ENUM theInteriorEnumStart = INTERIOR_V_CUSTOM_C_1
	INT i
	SWITCH iProperty
		CASE PROPERTY_CUSTOM_APT_1_BASE	theInteriorEnumStart = INTERIOR_V_CUSTOM_C_1 BREAK
		CASE PROPERTY_CUSTOM_APT_2		theInteriorEnumStart = INTERIOR_V_CUSTOM_A_1 BREAK
		CASE PROPERTY_CUSTOM_APT_3		theInteriorEnumStart = INTERIOR_V_CUSTOM_B_1 BREAK
		CASE PROPERTY_OFFICE_1			theInteriorEnumStart = INTERIOR_V_OFFICE_1_1 BREAK
		CASE PROPERTY_OFFICE_2_BASE		theInteriorEnumStart = INTERIOR_V_OFFICE_2_1 BREAK
		CASE PROPERTY_OFFICE_3			theInteriorEnumStart = INTERIOR_V_OFFICE_3_1 BREAK
		CASE PROPERTY_OFFICE_4			theInteriorEnumStart = INTERIOR_V_OFFICE_4_1 BREAK
	ENDSWITCH
	IF IS_PROPERTY_OFFICE_GARAGE(iProperty)
		
		IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL1
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_1,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,FALSE)
			ENDIF	
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_1,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL2
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_2,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,FALSE)
			ENDIF		
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_2,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_1_GARAGE_LVL3
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_3,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_MOD,FALSE)
			ENDIF		
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_1_GAR_3,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_2_GARAGE_LVL1
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_1,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_1,TRUE)
			ENDIF	
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_1,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_2_GARAGE_LVL2
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_2,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,FALSE)
			ENDIF	
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_2,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_2_GARAGE_LVL3
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_3,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_MOD,FALSE)
			ENDIF	
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_2_GAR_3,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_3_GARAGE_LVL1
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_1,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,FALSE)
			ENDIF	
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_1,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_3_GARAGE_LVL2
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_2,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,FALSE)
			ENDIF		
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_2,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_3_GARAGE_LVL3
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_3,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_MOD,FALSE)
			ENDIF	
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_3_GAR_3,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_4_GARAGE_LVL1
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_1,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,FALSE)
			ENDIF
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_1,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_4_GARAGE_LVL2
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_2,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,FALSE)
			ENDIF
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_2,TRUE)
		ENDIF
		
		IF iProperty = PROPERTY_OFFICE_4_GARAGE_LVL3
			IF !bIsModGarage
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_3,FALSE)
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,TRUE)
			ELSE
				SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_MOD,FALSE)
			ENDIF
		ELSE
			SET_INTERIOR_DISABLED(INTERIOR_V_OFFICE_4_GAR_3,TRUE)
		ENDIF

	ELSE
		REPEAT GET_MAX_INTERIOR_VARIATIONS_FOR_PROPERTY(iProperty) i
			theInteriorEnum = INT_TO_ENUM(INTERIOR_NAME_ENUM,ENUM_TO_INT(theInteriorEnumStart) + i)
			IF i+1 = iVariation
				SET_INTERIOR_DISABLED(theInteriorEnum,FALSE)
			ELSE
				SET_INTERIOR_DISABLED(theInteriorEnum,TRUE)
			ENDIF
		ENDREPEAT
	ENDIF
	IF IS_PROPERTY_CUSTOM_APARTMENT(iProperty)
		IF iVariation = 1
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_01")
		ENDIF
		IF iVariation = 2
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_02")
		ENDIF
		IF iVariation = 3
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_03")
		ENDIF
		IF iVariation = 4
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_04")
		ENDIF
		IF iVariation = 5
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_05")
		ENDIF
		IF iVariation = 6
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_06")
		ENDIF
		IF iVariation = 7
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_07")
		ENDIF
		IF iVariation = 8
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_08")
		ENDIF
	ELIF IS_PROPERTY_OFFICE(iProperty)
		IF iVariation = 1
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_01a_dlc")
		ENDIF
		IF iVariation = 2
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_01b_dlc")
		ENDIF
		IF iVariation = 3
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_01c_dlc")
		ENDIF
		IF iVariation = 4
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_02a_dlc")
		ENDIF
		IF iVariation = 5
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_02b_dlc")
		ENDIF
		IF iVariation = 6
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_02c_dlc")
		ENDIF
		IF iVariation = 7
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_03a_dlc")
		ENDIF
		IF iVariation = 8
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_03b_dlc")
		ENDIF
		IF iVariation = 9
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"ex_int_office_03c_dlc")
		ENDIF
	ELIF IS_PROPERTY_OFFICE_GARAGE(iProperty)
		IF !bIsModGarage
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"imp_impexp_int_01")
		ELSE
			interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_OFFICE_MOD_GARAGE_INTERIOR_COORD(iProperty) ,"imp_imptexp_mod_int_01")
		ENDIF
	ENDIF
	
//	apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_01")
//	IF IS_VALID_INTERIOR(apartmentInterior)
//		IF iVariation = 1
//			IF IS_INTERIOR_DISABLED(apartmentInterior)
//				DISABLE_INTERIOR(apartmentInterior,FALSE)
//			ENDIF
//			interiorID = apartmentInterior
//		ELSE
//			IF NOT IS_INTERIOR_DISABLED(apartmentInterior)
//				DISABLE_INTERIOR(apartmentInterior,TRUE)
//			ENDIF
//		ENDIF
//	ENDIF
//	apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_02")
//	IF IS_VALID_INTERIOR(apartmentInterior)
//		IF iVariation = 2
//			IF IS_INTERIOR_DISABLED(apartmentInterior)
//				DISABLE_INTERIOR(apartmentInterior,FALSE)
//			ENDIF
//			interiorID = apartmentInterior
//		ELSE
//			IF NOT IS_INTERIOR_DISABLED(apartmentInterior)
//				DISABLE_INTERIOR(apartmentInterior,TRUE)
//			ENDIF
//		ENDIF
//	ENDIF
//	apartmentInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc,"apa_v_mp_h_03")
//	IF IS_VALID_INTERIOR(apartmentInterior)
//		IF iVariation = 3
//			IF IS_INTERIOR_DISABLED(apartmentInterior)
//				DISABLE_INTERIOR(apartmentInterior,FALSE)
//			ENDIF
//			interiorID = apartmentInterior
//		ELSE
//			IF NOT IS_INTERIOR_DISABLED(apartmentInterior)
//				DISABLE_INTERIOR(apartmentInterior,TRUE)
//			ENDIF
//		ENDIF
//	ENDIF
ENDPROC



FUNC BOOL IS_PROPERTY_SLOT_SPECIAL_PURCHASE_TYPE(INT iPropertySlot)
	SWITCH iPropertySlot
		CASE PROPERTY_OWNED_SLOT_OFFICE_0
		CASE YACHT_PROPERTY_OWNED_SLOT
		CASE PROPERTY_OWNED_SLOT_CLUBHOUSE
//	#IF FEATURE_IMPORT_EXPORT
//		CASE PROPERTY_OWNED_SLOT_IE_WAREHOUSE
//	#ENDIF
	#IF IS_DEBUG_BUILD
	#IF FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER
		CASE PROPERTY_OWNED_SLOT_AIR_CARRIER
	#ENDIF
	#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_SPECIAL_PURCHASE_TYPE(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_YACHT_APT_1_BASE
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
//		#IF FEATURE_IMPORT_EXPORT
//		CASE PROPERTY_VEH_WAREHOUSE_1
//		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_SLOT_NON_HEIST_PROPERTY(INT iPropertySlot)
	SWITCH iPropertySlot
		CASE PROPERTY_OWNED_SLOT_OFFICE_0
		CASE YACHT_PROPERTY_OWNED_SLOT
		CASE PROPERTY_OWNED_SLOT_CLUBHOUSE
//	#IF FEATURE_IMPORT_EXPORT
//		CASE PROPERTY_OWNED_SLOT_IE_WAREHOUSE
//	#ENDIF
	#IF IS_DEBUG_BUILD
	#IF FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER
		CASE PROPERTY_OWNED_SLOT_AIR_CARRIER
	#ENDIF
	#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PROPERTY_NON_HEIST_PROPERTY(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_YACHT_APT_1_BASE
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
//		#IF FEATURE_IMPORT_EXPORT
//		CASE PROPERTY_VEH_WAREHOUSE_1
//		#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PROPERTY_HAVE_NAV_MESH(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_STILT_APT_1_BASE_B	
		CASE PROPERTY_STILT_APT_2_B			
		CASE PROPERTY_STILT_APT_3_B			
		CASE PROPERTY_STILT_APT_4_B			
		CASE PROPERTY_STILT_APT_5_BASE_A	
		CASE PROPERTY_STILT_APT_7_A			
		CASE PROPERTY_STILT_APT_8_A			
		CASE PROPERTY_STILT_APT_10_A		
		CASE PROPERTY_STILT_APT_12_A		
		CASE PROPERTY_STILT_APT_13_A		
		CASE PROPERTY_CUSTOM_APT_1_BASE		
		CASE PROPERTY_CUSTOM_APT_2			
		CASE PROPERTY_CUSTOM_APT_3		
		CASE PROPERTY_YACHT_APT_1_BASE
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
//		#IF FEATURE_IMPORT_EXPORT
//		CASE PROPERTY_VEH_WAREHOUSE_1
//		#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC



FUNC BOOL SHOULD_PROP_BYPASS_PIN_ON_WEBSITE(INT iPropertyID)
	SWITCH iPropertyID
		CASE PROPERTY_YACHT_APT_1_BASE
//	#IF IS_DEBUG_BUILD
//	#IF FEATURE_TEMP_UPCOMING_AIRCRAFT_CARRIER
//		CASE PROPERTY_OWNED_SLOT_AIR_CARRIER
//	#ENDIF
//	#ENDIF
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PROPERTY_REQUIRE_OWNER_LET_PLAYER_IN_ANIM(INT iPropertyID)
	IF IS_PROPERTY_OFFICE(iPropertyID)
		RETURN FALSE
	ENDIF
	IF iPropertyID != 0
	
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_HUGE_STORAGE_GARAGE(PLAYER_INDEX playerID)

	UNUSED_PARAMETER(playerID)

	RETURN TRUE

ENDFUNC

FUNC BOOL IS_PLAYER_IN_HUGE_STORAGE_GARAGE(PLAYER_INDEX playerID)

	UNUSED_PARAMETER(playerID)

	#IF IS_DEBUG_BUILD
	RETURN MPGlobalsAmbience.sMagnateGangBossData.bWidgetInHugeStorageGarage
	#ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS(INT iModStyle)
	
	SWITCH iModStyle
		CASE 1	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_5)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_6)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_7)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_8)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 9
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_9)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_9)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 10
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_10)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_10)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 11
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_11)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_11)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 12
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_12)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_12)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 13
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_13)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_13)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 14
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_14)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_14)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 15
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_15)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_15)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 16
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_16)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_16)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 17
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_17)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_17)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 18
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_18)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_18)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 19
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_19)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_19)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		CASE 20
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_20)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_CAR_MOD_20)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Setting iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: INVALID iModStyle: ", iModStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	INT iNum
	FOR iNum = 1 TO 20
		IF iNum != iModStyle 
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, iNum)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageModInteriorBS, iNum)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS: Clear flags which are already set: ", iNum)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR 
	
ENDPROC


PROC SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS(INT iNumberingStyle, INT iLightingStyle, INT iDecor, INT iModStyle)
	INT iNum
	
	SWITCH iNumberingStyle
		CASE 0	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_0)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_0)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_5)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_6)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF	
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_7)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_NUMBERING_STYLE_8)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: INVALID iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iLightingStyle
		CASE 0	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_0)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_0)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_5)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_6)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_7)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_LIGHTING_OPTION_8)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: INVALID iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iDecor
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL1_DECOR_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: INVALID iDecor: ", iDecor)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS(iModStyle)
	
	FOR iNum = 0 TO 31
		IF iNum != iNumberingStyle
		AND iNum != iLightingStyle + 9
		AND iNum != iDecor + 17
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, iNum)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV1InteriorBS, iNum)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL1_INTERIOR_FLAGS: Clear flags which are already set: ", iNum)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR 
	
	IF IS_OFFICE_MODSHOP_PURCHASED()
		SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(TRUE)
	ELSE
		SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(FALSE)
	ENDIF
	
ENDPROC 

PROC SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS(INT iNumberingStyle, INT iLightingStyle, INT iDecor, INT iModStyle )
	INT iNum
	
	SWITCH iNumberingStyle
		CASE 0	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_0)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_0)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_1)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_2)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_3)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_4)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_5)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_6)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_7)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_NUMBERING_STYLE_8)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
				#ENDIF
			ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: INVALID iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iLightingStyle
		CASE 0	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_0)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_0)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_1)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_2)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_3)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_4)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_5)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_6)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_7)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_LIGHTING_OPTION_8)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
				#ENDIF
			ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: INVALID iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iDecor
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_1)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
				#ENDIF
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_2)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
				#ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_3)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
				#ENDIF
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL2_DECOR_4)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
				#ENDIF
			ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: INVALID iDecor: ", iDecor)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS(iModStyle)
	
	FOR iNum = 0 TO 31
		IF iNum != iNumberingStyle
		AND iNum != iLightingStyle + 9
		AND iNum != iDecor + 17
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, iNum)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV2InteriorBS, iNum)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL2_INTERIOR_FLAGS: Clear flags which are already set: ", iNum)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR 
	
	IF IS_OFFICE_MODSHOP_PURCHASED()
		SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(TRUE)
	ELSE
		SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(FALSE)
	ENDIF
	
ENDPROC 

PROC SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS(INT iNumberingStyle, INT iLightingStyle, INT iDecor, INT iModStyle = 0)
	INT iNum
	
	SWITCH iNumberingStyle
		CASE 0	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_0)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_0)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_5)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_6)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_7)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_NUMBERING_STYLE_8)	
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: INVALID iNumberingStyle: ", iNumberingStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iLightingStyle
		CASE 0	
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_0)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_0)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 5
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_5)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_5)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 6
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_6)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_6)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 7
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_7)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_7)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		CASE 8
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_8)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_LIGHTING_OPTION_8)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: INVALID iLightingStyle: ", iLightingStyle)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH iDecor
		CASE 1
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_1)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_1)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		CASE 2
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_2)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_2)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		CASE 3
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_3)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_3)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		CASE 4
			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_4)
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, PROPERTY_BROADCAST_OFF_GARG_LVL3_DECOR_4)
			ENDIF
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Setting iDecor: ", iDecor)
			#ENDIF
		BREAK
		DEFAULT
			#IF IS_DEBUG_BUILD
			PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: INVALID iDecor: ", iDecor)
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	SET_PLAYER_OFFICE_GARAGE_MOD_INTERIOR_FLAGS(iModStyle)
	
	FOR iNum = 0 TO 31
		IF iNum != iNumberingStyle
		AND iNum != iLightingStyle + 9
		AND iNum != iDecor + 17
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, iNum)
				CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iOfficeGarageLV3InteriorBS, iNum)
				#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_OFFICE_GARAGE_LVL3_INTERIOR_FLAGS: Clear flags which are already set: ", iNum)
				#ENDIF
			ENDIF
		ENDIF
	ENDFOR 
	
	IF IS_OFFICE_MODSHOP_PURCHASED()
		SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(TRUE)
	ELSE
		SET_PLAYER_OWNS_OFFICE_MOD_GARAGE(FALSE)
	ENDIF
	
ENDPROC 

FUNC INT GET_OFFICE_GARAGE_LVL1_PURCHASED_NUMBERING_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 0 TO 8 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV1InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL1_PURCHASED_NUMBERING_STYLE: NUMBERING STYLE IS: ", i + 1)
			#ENDIF
			RETURN i + 1
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL1_PURCHASED_NUMBERING_STYLE: INVALID NUMBERING STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL1_PURCHASED_LIGHTING_OPTION(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 9 TO 17 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV1InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL1_PURCHASED_LIGHTING_OPTION: LIGHTING OPTION IS: ", i - 8)
			#ENDIF
			RETURN i - 8
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL1_PURCHASED_LIGHTING_OPTION: INVALID LIGHTING OPTION i :", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL1_PURCHASED_DECOR_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 18 TO 21 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV1InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL1_PURCHASED_DECOR_STYLE: DECOR STYLE IS: ", i - 17)
			#ENDIF
			RETURN i - 17
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL1_PURCHASED_DECOR_STYLE: INVALID DECOR STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_PURCHASED_MOD_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 1 TO 20 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageModInteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_PURCHASED_MOD_STYLE: DECOR STYLE IS: ", i )
			#ENDIF
			RETURN i 
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_PURCHASED_MOD_STYLE: INVALID DECOR STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL2_PURCHASED_NUMBERING_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 0 TO 8 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV2InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL2_PURCHASED_NUMBERING_STYLE: NUMBERING STYLE IS: ", i + 1)
			#ENDIF
			RETURN i + 1
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL2_PURCHASED_NUMBERING_STYLE: INVALID NUMBERING STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL2_PURCHASED_LIGHTING_OPTION(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 9 TO 17 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV2InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL2_PURCHASED_LIGHTING_OPTION: LIGHTING OPTION IS: ", i - 8)
			#ENDIF
			RETURN i - 8
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL2_PURCHASED_LIGHTING_OPTION: INVALID LIGHTING OPTION ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL2_PURCHASED_DECOR_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 18 TO 21 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV2InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL2_PURCHASED_DECOR_STYLE: DECOR STYLE IS: ", i - 17)
			#ENDIF
			RETURN i - 17
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL2_PURCHASED_DECOR_STYLE: INVALID DECOR STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL3_PURCHASED_NUMBERING_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 0 TO 8 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV3InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL3_PURCHASED_NUMBERING_STYLE: NUMBERING STYLE IS: ", i + 1)
			#ENDIF
			RETURN i + 1
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL3_PURCHASED_NUMBERING_STYLE: INVALID NUMBERING STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL3_PURCHASED_LIGHTING_OPTION(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 9 TO 17 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV3InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL3_PURCHASED_LIGHTING_OPTION: LIGHTING OPTION IS: ", i - 8)
			#ENDIF
			RETURN i - 8
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL3_PURCHASED_LIGHTING_OPTION: INVALID LIGHTING OPTION ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC INT GET_OFFICE_GARAGE_LVL3_PURCHASED_DECOR_STYLE(PLAYER_INDEX officeGarageOwner)
	INT i
	FOR i = 18 TO 21 
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iOfficeGarageLV3InteriorBS, i)
			#IF IS_DEBUG_BUILD
			PRINTLN("GET_OFFICE_GARAGE_LVL3_PURCHASED_DECOR_STYLE: DECOR STYLE IS: ", i - 17)
			#ENDIF
			RETURN i - 17
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	PRINTLN("GET_OFFICE_GARAGE_LVL3_PURCHASED_DECOR_STYLE: INVALID DECOR STYLE ", i)
	#ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_OFFICE_GARAGE_RENOVATED(PLAYER_INDEX officeGarageOwner)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(officeGarageOwner)].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_OFFICE_GARAGE_RENOVATE)
ENDFUNC

PROC SET_PLAYER_RENOVATED_OFFICE_GARAGE_FLAG(BOOL bRenovated)
	IF bRenovated
		IF !IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_OFFICE_GARAGE_RENOVATE)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OFFICE_GARAGE_RENOVATE)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_RENOVATED_OFFICE_GARAGE_FLAG - TRUE")
			#ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OFFICE_GARAGE_RENOVATE)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OFFICE_GARAGE_RENOVATE)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_PLAYER_RENOVATED_OFFICE_GARAGE_FLAG - FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_OWNER_RENOVATED_TRUCK_CAB(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_CAB)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_OWNER_RENOVATED_TRUCK_CAB(BOOL bRenovated)
	IF bRenovated
		IF !IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_CAB)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_CAB)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_OWNER_RENOVATED_TRUCK_CAB - TRUE")
			#ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_CAB)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_CAB)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_OWNER_RENOVATED_TRUCK_CAB - FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_OWNER_RENOVATED_TRUCK_TRAILER(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_TRAILER)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_OWNER_RENOVATED_TRUCK_TRAILER(BOOL bRenovated)
	IF bRenovated
		IF !IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_TRAILER)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_TRAILER)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_OWNER_RENOVATED_TRUCK_TRAILER - TRUE")
			#ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_TRAILER)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_OWNER_RENOVATED_TRUCK_TRAILER)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_OWNER_RENOVATED_TRUCK_TRAILER - FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_I_NEED_TO_RENOVATE_TRUCK_TRAILER(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_TRAILER)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_I_NEED_TO_RENOVATE_TRUCK_TRAILER(BOOL bRenovated)
	IF bRenovated
		IF !IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_TRAILER)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_TRAILER)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_I_NEED_TO_RENOVATE_TRUCK_TRAILER - TRUE")
			#ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_TRAILER)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_TRAILER)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_I_NEED_TO_RENOVATE_TRUCK_TRAILER - FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_I_NEED_TO_RENOVATE_TRUCK_CAB(PLAYER_INDEX playerToCheck)
	IF playerToCheck != INVALID_PLAYER_INDEX()
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerToCheck)].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_CAB)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_I_NEED_TO_RENOVATE_TRUCK_CAB(BOOL bRenovated)
	IF bRenovated
		IF !IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree ,PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_CAB)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_CAB)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_I_NEED_TO_RENOVATE_TRUCK_CAB - TRUE")
			#ENDIF
		ENDIF	
	ELSE
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_CAB)
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSThree, PROPERTY_BROADCAST_BS3_I_NEED_TO_RENOVATE_TRUCK_CAB)
			#IF IS_DEBUG_BUILD
				PRINTLN("SET_I_NEED_TO_RENOVATE_TRUCK_CAB - FALSE")
			#ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL DOES_PROPERTY_GARAGE_HAVE_ELEVATOR(INT iProperty)

	SWITCH iProperty
		CASE PROPERTY_HIGH_APT_1	
		CASE PROPERTY_HIGH_APT_2	
		CASE PROPERTY_HIGH_APT_3	
		CASE PROPERTY_HIGH_APT_4	
		CASE PROPERTY_HIGH_APT_5	
		CASE PROPERTY_HIGH_APT_6	
		CASE PROPERTY_HIGH_APT_7	
		CASE PROPERTY_HIGH_APT_8
		CASE PROPERTY_HIGH_APT_9
		CASE PROPERTY_HIGH_APT_10
		CASE PROPERTY_HIGH_APT_11
		CASE PROPERTY_HIGH_APT_12
		CASE PROPERTY_HIGH_APT_13
		CASE PROPERTY_HIGH_APT_14
		CASE PROPERTY_HIGH_APT_15
		CASE PROPERTY_HIGH_APT_16
		CASE PROPERTY_HIGH_APT_17
		CASE PROPERTY_BUS_HIGH_APT_1
		CASE PROPERTY_BUS_HIGH_APT_2
		CASE PROPERTY_BUS_HIGH_APT_3
		CASE PROPERTY_BUS_HIGH_APT_4
		CASE PROPERTY_BUS_HIGH_APT_5
		CASE PROPERTY_STILT_APT_1_BASE_B			
		CASE PROPERTY_STILT_APT_2_B					
		CASE PROPERTY_STILT_APT_3_B					
		CASE PROPERTY_STILT_APT_4_B					
		CASE PROPERTY_STILT_APT_5_BASE_A							
		CASE PROPERTY_STILT_APT_7_A					
		CASE PROPERTY_STILT_APT_8_A									
		CASE PROPERTY_STILT_APT_10_A									
		CASE PROPERTY_STILT_APT_12_A					
		CASE PROPERTY_STILT_APT_13_A
		CASE PROPERTY_CUSTOM_APT_1_BASE
		CASE PROPERTY_CUSTOM_APT_2
		CASE PROPERTY_CUSTOM_APT_3

			RETURN TRUE
		BREAK
		CASE PROPERTY_OFFICE_1
		CASE PROPERTY_OFFICE_2_BASE
		CASE PROPERTY_OFFICE_3
		CASE PROPERTY_OFFICE_4
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///	Will update the MC Emblem to the proper emblem for a ped clone. For preset emblem
/// BUG: 3091229
FUNC BOOL UPDATE_MC_EMBLEM(PLAYER_INDEX piToClone, PED_INDEX &pedClone)
	
	IF IS_PLAYER_DEAD(piToClone)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM, MC EMBLEM: Cant Update MC emblem - piPedToClone is dead.")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedClone)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM, MC EMBLEM: Cant Update MC emblem - pedClone does not exist.")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(piToClone)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM, MC EMBLEM: - Player is a member of a biker gang")
		IF GB_OUTFITS_GD_GET_BIKER_EMBLEM_TO_DISPLAY(piToClone) = ENUM_TO_INT(CUSTOM_CREW_LOGO_PRESET)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM, MC EMBLEM: - Biker emblem to display is CUSTOM_CREW_LOGO_PRESET")
			
			REQUEST_STREAMED_TEXTURE_DICT(GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(piToClone))
			IF OVERRIDE_PED_CREW_LOGO_TEXTURE(pedClone, GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(piToClone), GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(piToClone, CUSTOM_CREW_LOGO_PRESET))
				CDEBUG1LN(DEBUG_SAFEHOUSE,"POD: EXT: ENTRY ANIM, MC EMBLEM: - biker emblem updated: ", GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(piToClone, CUSTOM_CREW_LOGO_PRESET))
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE,"POD: EXT: ENTRY ANIM, MC EMBLEM: - Biker emblem override failed: ", GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(piToClone, CUSTOM_CREW_LOGO_PRESET))
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM, MC EMBLEM: - Biker emblem to display is NOT CUSTOM_CREW_LOGO_PRESET")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "POD: EXT: ENTRY ANIM, MC EMBLEM: Cant Update MC emblem - ped is not a member of a biker gang")
	ENDIF	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(VEHICLE_INDEX theVeh)
	IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh",DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(theVeh,"Not_Allow_As_Saved_Veh")
			IF DECOR_GET_INT(theVeh,"Not_Allow_As_Saved_Veh") != 0
				CDEBUG1LN(DEBUG_SAFEHOUSE, "DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE = TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(theVeh, "MPBitset")	
			iDecoratorValue = DECOR_GET_INT(theVeh, "MPBitset")
			IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE)	
				PRINTLN("AM_MP_PROPERTY_INT: DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE - TRUE MP_DECORATOR_BS_VEHICLE_LEAVING_GARAGE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_DECORATOR_PREVENT_MODDING(VEHICLE_INDEX theVeh)
	IF IS_ENTITY_ALIVE(theVeh)
		INT iDecoratorValue
		IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
			IF DECOR_EXIST_ON(theVeh, "MPBitset")	
				iDecoratorValue = DECOR_GET_INT(theVeh, "MPBitset")
				IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_NON_MODDABLE_VEHICLE)	
					PRINTLN("AM_MP_PROPERTY_INT: DOES_DECORATOR_PREVENT_MODDING - TRUE MP_DECORATOR_BS_NON_MODDABLE_VEHICLE")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP(STRING theTextInput,INT iProperty, BOOL bNoSound = FALSE)
	TEXT_LABEL_23 theText = theTextInput
	IF iProperty = PROPERTY_TRUCK_REFERENCE_ONLY
		theText += "T"
	ELIF iProperty = PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
	OR iProperty = PROPERTY_DEFUNC_BASE
		theText += "D"
	ENDIF
	IF bNoSound
		PRINT_HELP_NO_SOUND(theText)
	ELSE
		PRINT_HELP(theText)
	ENDIF
ENDPROC

PROC BLOCKED_FROM_PROPERTY_SET_HELP_BACKGROUND(BOOL bWithSound = TRUE)
	SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_MP_FREEMODE, HUD_COLOUR_PURPLE, 200)
	IF bWithSound
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
		PLAY_SOUND_FRONTEND(-1, "Event_Message_Purple", "GTAO_FM_Events_Soundset", FALSE)
	ENDIF
	DEBUG_PRINTCALLSTACK()
ENDPROC

PROC PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(BOOL bGarage, INT iProperty,OWNED_PROPERTIES_IN_BUILDING &ownedDetails)// , #IF FEATURE_EXECUTIVE BOOL bDoGangBossCheck #ENDIF)
	IF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
		IF IS_PROPERTY_OFFICE(iProperty)
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_JUGG1", iProperty, TRUE)
			EXIT
		ENDIF
		
		IF IS_PROPERTY_CLUBHOUSE(iProperty)
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_JUGG2", iProperty, TRUE)
			EXIT
		ENDIF
		
		IF bGarage
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_JUGG3", iProperty, TRUE)
			EXIT
		ENDIF
		
		PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_JUGG4", iProperty, TRUE)
		EXIT
	ENDIF
	
	IF IS_BIT_SET(g_iPreventPropertyAccessBS,ENUM_TO_INT(PPAF_FREEMODE_EVENT_BEAST))
		IF IS_PROPERTY_OFFICE(iProperty)
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_BEAST2")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BEAST2",iProperty,TRUE)
			ELSE
				//PRINT_HELP_NO_SOUND("PPA_BEAST2b")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BEAST2b",iProperty,TRUE)
			ENDIF
			BLOCKED_FROM_PROPERTY_SET_HELP_BACKGROUND()
			EXIT
		ENDIF
		IF bGarage
			//PRINT_HELP_NO_SOUND("PPA_BEAST1")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BEAST1",iProperty,TRUE)
			BLOCKED_FROM_PROPERTY_SET_HELP_BACKGROUND()
			EXIT
		ENDIF
		//PRINT_HELP_NO_SOUND("PPA_BEAST0")
		PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BEAST0",iProperty,TRUE)
		BLOCKED_FROM_PROPERTY_SET_HELP_BACKGROUND()
		EXIT
	ELIF IS_BIT_SET(g_iPreventPropertyAccessBS,ENUM_TO_INT(PPAF_CONTRABAND))
		IF IS_PROPERTY_OFFICE(iProperty)
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_CONTRA2")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CONTRA2",iProperty,TRUE)
			ELSE
				//PRINT_HELP_NO_SOUND("PPA_CONTRA2b")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CONTRA2b",iProperty,TRUE)
			ENDIF
			EXIT
		ENDIF
		IF bGarage
			//PRINT_HELP_NO_SOUND("PPA_CONTRA1")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CONTRA1",iProperty,TRUE)
			EXIT
		ENDIF
		IF iProperty != PROPERTY_TRUCK_REFERENCE_ONLY
		AND iProperty != PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
		AND iProperty != PROPERTY_DEFUNC_BASE
		AND DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
			//PRINT_HELP_NO_SOUND("PPA_CONTRA0b")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CONTRA0b",iProperty,TRUE)
		ELSE
			//PRINT_HELP_NO_SOUND("PPA_CONTRA0")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CONTRA0",iProperty,TRUE)
		ENDIF
		EXIT
	ELIF IS_BIT_SET(g_iPreventPropertyAccessBS,ENUM_TO_INT(PPAF_FREEMODE_BOUNTY_HUNTER))
		IF IS_PROPERTY_OFFICE(iProperty)
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_CONTRA2")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BOUNT",iProperty,TRUE)
			ELSE
				//PRINT_HELP_NO_SOUND("PPA_CONTRA2b")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BOUNTb",iProperty,TRUE)
			ENDIF
			EXIT
		ENDIF
		IF bGarage
			//PRINT_HELP_NO_SOUND("PPA_CONTRA1")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BOUNGAR1",iProperty,TRUE)
			EXIT
		ENDIF
		IF iProperty != PROPERTY_TRUCK_REFERENCE_ONLY
		AND iProperty != PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
		AND iProperty != PROPERTY_DEFUNC_BASE
		AND DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
			//PRINT_HELP_NO_SOUND("PPA_CONTRA0b")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BOUNTGA",iProperty,TRUE)
		ELSE
			//PRINT_HELP_NO_SOUND("PPA_CONTRA0")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_BOUNTGAb",iProperty,TRUE)
		ENDIF
		EXIT
	ENDIF
	IF IS_PROPERTY_OFFICE(iProperty)
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_OFF_HEIST")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_OFF_HEIST",iProperty,TRUE)
			ELSE	
				//PRINT_HELP_NO_SOUND("PPA_OFF_HEISTb")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_OFF_HEISTb",iProperty,TRUE)
			ENDIF
//		ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), FALSE)	
//			PRINT_HELP_NO_SOUND("OFF_ASSOC_B")
		ELSE
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_GENOFF")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENOFF",iProperty,TRUE)
			ELSE
				//PRINT_HELP_NO_SOUND("PPA_GENOFFb")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENOFFb",iProperty,TRUE)
			ENDIF
		ENDIF
		EXIT
	ENDIF
	IF IS_PROPERTY_CLUBHOUSE(iProperty)
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_CLU_HEIST")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CLU_HEIST",iProperty,TRUE)
			ELSE	
				//PRINT_HELP_NO_SOUND("PPA_CLU_HEISTb")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_CLU_HEISTb",iProperty,TRUE)
			ENDIF
		ELSE
			IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
				//PRINT_HELP_NO_SOUND("PPA_GENUCLU")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENUCLU",iProperty,TRUE)
			ELSE
				//PRINT_HELP_NO_SOUND("PPA_GENCLUB")
				PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENCLUB",iProperty,TRUE)
			ENDIF
		ENDIF
		EXIT
	ENDIF
	
	IF iProperty = PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
	OR iProperty = PROPERTY_DEFUNC_BASE
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			//IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
			//	//PRINT_HELP_NO_SOUND("PPA_CLU_HEIST")
			//	PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_FAC_HEIST",iProperty,TRUE)
			//ELSE	
				//PRINT_HELP_NO_SOUND("PPA_CLU_HEISTb")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_FAC_HEISTb",iProperty,TRUE)
			//ENDIF
		ELSE
			//IF DOES_LOCAL_PLAYER_OWN_PROPERTY_IN_BUILDING(mpProperties[iProperty].iBuildingID,ownedDetails)
			//	//PRINT_HELP_NO_SOUND("PPA_GENUCLU")
			//	PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENUCLU",iProperty,TRUE)
			//ELSE
			//	//PRINT_HELP_NO_SOUND("PPA_GENCLUB")
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENFAC",iProperty,TRUE)
			//ENDIF
		ENDIF
		EXIT
	ENDIF
	
	IF bGarage
		//PRINT_HELP("PPA_GEN1")
		IF NOT (IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_PLAYING_TUNER_ELEVATOR_PASS) AND mpProperties[iProperty].iBuildingID = MP_PROPERTY_OFFICE_BUILDING_2)
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GEN1",iProperty)
		ENDIF
		EXIT
	ENDIF
	
	IF IS_PROPERTY_CLUBHOUSE(iProperty)
		//PRINT_HELP("PPA_GENCLUB")
		PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GENCLUB",iProperty)
	ELSE
		//PRINT_HELP("PPA_GEN0")
		IF iProperty != PROPERTY_TRUCK_REFERENCE_ONLY
		AND iProperty != PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
		AND iProperty != PROPERTY_DEFUNC_BASE
			PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PPA_GEN0",iProperty)
		ENDIF 
	ENDIF	
ENDPROC


FUNC BOOL IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_MOD_SHOP(VEHICLE_INDEX pedVeh)
	IF NOT DOES_ENTITY_EXIST(pedVeh)
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(pedVeh))
		RETURN FALSE
	ENDIF
	
	SWITCH GET_ENTITY_MODEL(pedVeh)
		CASE APC
		CASE DUNE3
		CASE HALFTRACK
		CASE OPPRESSOR
		CASE TAMPA3
		CASE TECHNICAL3
		CASE INSURGENT3
		CASE NIGHTSHARK
		CASE ARDENT
		CASE VIGILANTE
		CASE DELUXO
		CASE STROMBERG
		CASE COMET4
		CASE BARRAGE
		CASE SAVESTRA
		CASE REVOLTER
		CASE VISERIS
		CASE CARACARA
		CASE MENACER
		CASE SCRAMJET
		CASE PARAGON2
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	IF IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(pedVeh)
		RETURN TRUE
	ENDIF
	
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(pedVeh)
		IF NOT IS_VEHICLE_TOO_BIG_FOR_MOBILE_WORKSHOP(pedVeh)
			MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(pedVeh)
			SWITCH vehicleModel
				CASE OPPRESSOR2
					RETURN FALSE
				BREAK	
			ENDSWITCH
			
			RETURN TRUE // url:bugstar:7496127
		ENDIF
	ENDIF	
	
	RETURN FALSE
ENDFUNC 

FUNC BOOL IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_STORAGE(VEHICLE_INDEX pedVeh)
	IF NOT DOES_ENTITY_EXIST(pedVeh)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_ENTITY_MODEL(pedVeh)
		CASE RALLYTRUCK
			RETURN FALSE
		BREAK	
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC 

FUNC BOOL IS_VEHICLE_SUITABLE_FOR_PROPERTY(VEHICLE_INDEX theVehicle, INT iProperty,OWNED_PROPERTIES_IN_BUILDING &ownedDetails, BOOL bBypassChecksForEntry = FALSE, BOOL bPrintedReason = FALSE, BOOL bPlayerOwnsTruckStorage = FALSE, BOOL bPlayerOwnsTruckCarmodSection = FALSE #IF IS_DEBUG_BUILD , BOOL db_bBypassVehicleRestrictions = FALSE #ENDIF , VEHICLE_INDEX vehProperty = NULL)
	MODEL_NAMES theModel
	theModel = GET_ENTITY_MODEL(theVehicle)
	BOOL bPersonalVehicle, bAnotherPlayerPersonalVehicle, bVehicleAbortedEntry
	INT iDLCVehSlot
	#IF IS_DEBUG_BUILD
	IF db_bBypassVehicleRestrictions
		RETURN TRUE
	ENDIF
	#ENDIF
	
	//VEHICLE_MODEL_FAIL_ENUM failReason
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT) 
		IF DECOR_EXIST_ON(theVehicle, "Player_Vehicle") 
			IF DECOR_GET_INT(theVehicle, "Player_Vehicle") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
				bPersonalVehicle = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "bPersonalVehicle = TRUE")
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "bAnotherPlayerPersonalVehicle = TRUE")
				bAnotherPlayerPersonalVehicle = TRUE
			ENDIF
		ENDIF
	ENDIF
	IF DECOR_IS_REGISTERED_AS_TYPE("Veh_Modded_By_Player", DECOR_TYPE_INT) 
		IF DECOR_EXIST_ON(theVehicle, "Veh_Modded_By_Player") 
			IF DECOR_GET_INT(theVehicle, "Veh_Modded_By_Player") != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
				CDEBUG1LN(DEBUG_SAFEHOUSE, "bAnotherPlayerPersonalVehicle = TRUE - modded by another player")
				bAnotherPlayerPersonalVehicle = TRUE
			ENDIF
		ENDIF
	ENDIF
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		IF DECOR_EXIST_ON(theVehicle, "MPBitset")	
			iDecoratorValue = DECOR_GET_INT(theVehicle, "MPBitset")
		ENDIF
		IF IS_BIT_SET(iDecoratorValue, MP_DECORATOR_BS_ENTERING_INTO_GARAGE)	
			bVehicleAbortedEntry = TRUE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "bVehicleAbortedEntry = TRUE")
		ENDIF
		
	ENDIF
	
	IF g_sMPTunables.bDISABLE_PROPERTY_VEHICLE_ACCESS
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					IF IS_VEHICLE_MY_PERSONAL_VEHICLE(theVehicle)
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_BLOCK_1",iProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_BLOCK_1")
					ELSE
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_BLOCK_2",iProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_BLOCK_2")
					ENDIF
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
//		PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 5") //CDM DEBUG TEMP!
		RETURN FALSE
	ENDIF
	
	IF iProperty !=  PROPERTY_TRUCK_REFERENCE_ONLY
	AND iProperty != PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
	AND iProperty != PROPERTY_DEFUNC_BASE
	AND g_sMPTunables.bDisable_drive_WV_normal_properties
	AND IS_VEHICLE_WEAPONISED_VEHICLE(theVehicle) 
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					IF IS_VEHICLE_MY_PERSONAL_VEHICLE(theVehicle)
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_BLOCK_1",iProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_BLOCK_1")
					ELSE
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_BLOCK_2",iProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_BLOCK_2")
					ENDIF
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
//		PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 5") //CDM DEBUG TEMP!
		RETURN FALSE
	ENDIF

	IF iProperty = PROPERTY_TRUCK_REFERENCE_ONLY
		IF PLAYER_ID() = GET_OWNER_OF_PERSONAL_TRUCK(vehProperty)
			IF bPlayerOwnsTruckStorage = FALSE
			AND bPlayerOwnsTruckCarmodSection = FALSE
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT bPrintedReason
							PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_NO_PVMOD",iProperty)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_NO_PVMODT")
							bPrintedReason = TRUE
						ENDIF
					ENDIF
				ENDIF
				RETURN FALSE
			ELIF bPlayerOwnsTruckCarmodSection = TRUE
			AND bPlayerOwnsTruckStorage = FALSE
				IF NOT IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_MOD_SHOP(theVehicle)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT bPrintedReason
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_NO_WORK",iProperty)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_NO_WORKT")
								bPrintedReason = TRUE
							ENDIF
						ENDIF
					ENDIF
					RETURN FALSE
				ENDIF
			ELIF bPlayerOwnsTruckCarmodSection = FALSE
			AND bPlayerOwnsTruckStorage = TRUE
				IF NOT IS_VEHICLE_ALLOWED_TO_ENTER_TRUCK_STORAGE(theVehicle)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT bPrintedReason
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEHT",iProperty)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEHT")
								bPrintedReason = TRUE
							ENDIF
						ENDIF
					ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: player is not the owner")
			bPrintedReason = FALSE
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_ATTACHED_TO_TRAILER(theVehicle)
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					IF GET_ENTITY_MODEL(theVehicle) = PHANTOM3
					OR GET_ENTITY_MODEL(theVehicle) = HAULER2
					OR GET_ENTITY_MODEL(theVehicle) = SADLER2
					OR GET_ENTITY_MODEL(theVehicle) = TRAILERSMALL2
					OR GET_ENTITY_MODEL(theVehicle) = SADLER
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
						PRINTLN("JS-FFF-0 ")
					ELSE				
						PRINTLN("JS-FFF-1 ")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("PROP_BLOCK_TRAIL",iProperty)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help PROP_BLOCK_TRAIL")
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
//		PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 5") //CDM DEBUG TEMP!
		RETURN FALSE
	ENDIF
	
	IF IS_PROPERTY_CLUBHOUSE(iProperty)
		IF (IS_THIS_MODEL_A_BIKE(theModel)
		OR IS_THIS_MODEL_A_QUADBIKE(theModel))
		AND NOT IS_VEHICLE_A_CYCLE(theModel)
			//RETURN TRUE
		ELSE
			PRINTLN("IS_VEHICLE_SUITABLE_FOR_PROPERTY: false clubhouse and not a bike or quadbike vehModel = ", GET_MODEL_NAME_FOR_DEBUG(theModel))
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT bPrintedReason
						//PRINT_HELP("MP_PROP_IVD_VEH4")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH4",iProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH4")
						bPrintedReason = TRUE
					ENDIF
				ENDIF
			ENDIF
//			PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 4") //CDM DEBUG TEMP!
			RETURN FALSE
		ENDIF
	ENDIF
	IF iProperty != PROPERTY_ARENAWARS_GARAGE_LVL1
	AND iProperty != PROPERTY_ARENAWARS_GARAGE_LVL2
	AND iProperty != PROPERTY_ARENAWARS_GARAGE_LVL3
	AND IS_THIS_MODEL_ONLY_ALLOWED_IN_ARENA_GARAGE(theModel)
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					IF IS_PROPERTY_CLUBHOUSE(iProperty)
						//PRINT_HELP("MP_PROP_IVD_VEH4")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH4",iProperty)
					ELSE
						//PRINT_HELP("MP_PROP_IVD_VEH")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH - 1234")
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
//		PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 3") //CDM DEBUG TEMP!
		RETURN FALSE	
	ENDIF

	IF theModel = RCBANDITO
	#IF FEATURE_CASINO_HEIST
	OR theModel = MINITANK
	#ENDIF
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					IF IS_PROPERTY_CLUBHOUSE(iProperty)
						//PRINT_HELP("MP_PROP_IVD_VEH4")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH4",iProperty)
					ELSE
						//PRINT_HELP("MP_PROP_IVD_VEH")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH - 12345")
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
//		PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 3") //CDM DEBUG TEMP!
		RETURN FALSE	
	ENDIF
	
	IF IS_SVM_VEHICLE(theVehicle)
	OR IS_MODEL_A_PERSONAL_TRAILER(theModel)
	OR (IS_FDS_VEHICLE_MODEL(theModel)
	AND iProperty != PROPERTY_DEFUNC_BASE
	AND iProperty != PROPERTY_NIGHTCLUB
	AND iProperty != PROPERTY_MEGAWARE_GARAGE_LVL1
	AND iProperty != PROPERTY_MEGAWARE_GARAGE_LVL2
	AND iProperty != PROPERTY_MEGAWARE_GARAGE_LVL3
	AND iProperty != PROPERTY_ARENAWARS_GARAGE_LVL1
	AND iProperty != PROPERTY_ARENAWARS_GARAGE_LVL2
	AND iProperty != PROPERTY_ARENAWARS_GARAGE_LVL3
	 )
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					IF IS_PROPERTY_CLUBHOUSE(iProperty)
						//PRINT_HELP("MP_PROP_IVD_VEH4")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH4",iProperty)
					ELSE
						//PRINT_HELP("MP_PROP_IVD_VEH")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH - 123")
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
//		PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 3") //CDM DEBUG TEMP!
		RETURN FALSE	
	ENDIF
	IF IS_PROPERTY_OFFICE_GARAGE(iProperty)
	OR IS_PROPERTY_OFFICE(iProperty)
	OR iProperty =  PROPERTY_TRUCK_REFERENCE_ONLY
	OR iProperty =  PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
	OR iProperty =  PROPERTY_DEFUNC_BASE
	OR iProperty =  PROPERTY_AUTO_SHOP
	#IF FEATURE_TUNER
	OR iProperty =  PROPERTY_SECURITY_OFFICE_GARAGE
	#ENDIF
		IF IS_VEHICLE_A_CYCLE(theModel)
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT bPrintedReason
						//PRINT_HELP("MP_PROP_IVD_VEH")
						PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH - 12")
						bPrintedReason = TRUE
					ENDIF
				ENDIF
			ENDIF
//			PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 2") //CDM DEBUG TEMP!
			RETURN FALSE	
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(theModel)
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF NOT bPrintedReason
					PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH", iProperty)
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH - 1234")
					
					bPrintedReason = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(theVehicle)
		ENTITY_INDEX entityAttached 
		entityAttached = GET_ENTITY_ATTACHED_TO(theVehicle)
		IF IS_ENTITY_A_VEHICLE(entityAttached)
			VEHICLE_INDEX vehAttached
			vehAttached = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityAttached)
			IF DOES_ENTITY_EXIST(vehAttached)
				IF GET_ENTITY_MODEL(vehAttached) = CARGOBOB
				OR GET_ENTITY_MODEL(vehAttached) = CARGOBOB2
				OR GET_ENTITY_MODEL(vehAttached) = CARGOBOB3
				OR GET_ENTITY_MODEL(vehAttached) = CARGOBOB4
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT bPrintedReason
								//PRINT_HELP("MP_PROP_IVD_VEH")
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_CARBOB",0)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_CARBOB")
								bPrintedReason = TRUE
							ENDIF
						ENDIF
					ENDIF
		//			PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 2") //CDM DEBUG TEMP!
					RETURN FALSE	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

	IF bBypassChecksForEntry//SHOULD_BYPASS_CHECKS_FOR_ENTRY()
		IF NOT IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_LOCAL_PLAYER_IN_POSSESSION_OF_A_MISSION_CRITITCAL_ENTITY()
		OR IS_PLAYER_BLOCKED_FROM_PROPERTY(FALSE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iProperty))
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF NOT bPrintedReason
						IF IS_PLAYER_BLOCKED_FROM_PROPERTY(TRUE, GET_PROPERTY_TYPE_FOR_IS_PLAYER_BLOCKED_FROM_PROPERTY(iProperty))
							IF iProperty = PROPERTY_TRUCK_REFERENCE_ONLY
								PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(FALSE,iProperty,ownedDetails)
							ELIF iProperty = PROPERTY_DEFUNC_BASE_REFERENCE_ONLY
							ELIF iProperty = PROPERTY_DEFUNC_BASE
								PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(FALSE,iProperty,ownedDetails)
							ELSE
								PRINT_REASON_PLAYER_IS_BLOCKED_FROM_PROPERTY(TRUE,iProperty,ownedDetails)
							ENDIF
						ELSE
							//PRINT_HELP("CUST_GAR_MISO")
							PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("CUST_GAR_MISO",iProperty)
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_BLOCKED_FROM_PROPERTY: Printing help")
						bPrintedReason = TRUE
					ENDIF
				ENDIF
			ENDIF
//			PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 6") //CDM DEBUG TEMP!
		ELSE
			IF NOT DOES_DECORATOR_PREVENT_ALLOWANCE_IN_GARAGE(theVehicle)
			AND NOT bAnotherPlayerPersonalVehicle
			AND NOT bVehicleAbortedEntry
			AND (IS_MODEL_VALID_FOR_PERSONAL_VEHICLE(GET_ENTITY_MODEL(theVehicle))
			OR (IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(theVehicle) AND iProperty =  PROPERTY_TRUCK_REFERENCE_ONLY)
			 )
			AND NETWORK_GET_ENTITY_IS_NETWORKED(theVehicle)
				iDLCVehSlot = GET_DLC_VEHICLE_DATA_SLOT_FOR_VEHICLE(theModel)
				IF NOT IS_VEHICLE_AVAILABLE_FOR_GAME(theModel, TRUE)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT bPrintedReason
								IF iDLCVehSlot != -1
								AND NOT IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iDLCVehSlot)
									//PRINT_HELP("MP_PROP_IVD_VEH")
									PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH: player doesn't own DLC content but it is excluded from garage anyway")
								ELSE
									//PRINT_HELP("MP_PROP_IVD_4")
									PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_4",iProperty)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help: player does not own DLC content can't store vehicle: iSlot = ",iDLCVehSlot)
									IF iDLCVehSlot != -1
										CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(",iDLCVehSlot,") = ",IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iDLCVehSlot))
									ENDIF
									bPrintedReason = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
//					PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 9") //CDM DEBUG TEMP!
				ELIF iDLCVehSlot != -1
				AND NOT IS_DLC_VEHICLE_SLOT_SUITABLE_FOR_GARAGE(iDLCVehSlot)
					IF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
					AND NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF NOT bPrintedReason
								//PRINT_HELP("MP_PROP_IVD_VEH")
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH: player owns DLC content but it is excluded from garage")
								bPrintedReason = TRUE
							ENDIF
						ENDIF
					ENDIF
//					PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 8") //CDM DEBUG TEMP!
				ELIF NOT IS_MP_PREMIUM_VEHICLE(theModel)	
				OR (IS_MP_PREMIUM_VEHICLE(theModel) AND	 bPersonalVehicle)
				OR (IS_VEHICLE_UPGRADABLE_PEGASUS_VEHICLE(theVehicle) AND iProperty =  PROPERTY_TRUCK_REFERENCE_ONLY)
	//				IF IS_VEHICLE_SAFE_FOR_MOD_SHOP(theVehicle,FALSE,failReason)
	//				IF IS_VEHICLE_A_CYCLE(theModel)
	//				OR IS_VEHICLE_AN_EXCEPTION_FOR_GARAGE(theModel)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: TRUE")
						RETURN TRUE
	//				ELSE
	//					IF failReason = VEH_MODEL_FAIL_INVALID_VEHICLE
	//						IF NOT IS_PAUSE_MENU_ACTIVE()
	//						AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	//						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
	//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	//								IF NOT bPrintedReason
	//									PRINT_HELP("MP_PROP_IVD_VEH")
	//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: MP_PROP_IVD_VEH")
	//									bPrintedReason = TRUE
	//								ENDIF
	//							ENDIF
	//						ENDIF
	//					ELIF failReason = VEH_MODEL_FAIL_COP_VEHICLE
	//						IF NOT IS_PAUSE_MENU_ACTIVE()
	//						AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	//						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
	//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	//								IF NOT bPrintedReason
	//									PRINT_HELP("MP_PROP_IVD_VEH1")
	//									CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: MP_PROP_IVD_VEH1")
	//									bPrintedReason = TRUE
	//								ENDIF
	//							ENDIF
	//						ENDIF
	//					//EL
	//					ENDIF
					//ENDIF
				ELSE
					IF IS_MP_PREMIUM_VEHICLE(theModel)
						IF NOT IS_PAUSE_MENU_ACTIVE()
						AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
						AND NOT IS_CUSTOM_MENU_ON_SCREEN()
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								IF NOT bPrintedReason
									//PRINT_HELP(GET_SITE_FROM_VEHICLE(theModel))
									PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP(GET_SITE_FROM_VEHICLE(theModel),iProperty)
									CDEBUG1LN(DEBUG_SAFEHOUSE, "Printing: ", GET_SITE_FROM_VEHICLE(theModel))
									bPrintedReason = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PAUSE_MENU_ACTIVE()
				AND NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF NOT bPrintedReason
							IF IS_PROPERTY_CLUBHOUSE(iProperty)
							AND bAnotherPlayerPersonalVehicle
								//PRINT_HELP("MP_PROP_IVD_VEH3")
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH3",iProperty)
							ELIF iProperty = PROPERTY_DEFUNC_BASE
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH5",iProperty)	
							ELSE
								//PRINT_HELP("MP_PROP_IVD_VEH")
								PRINT_VEH_SUITABLE_FOR_PROPERTY_HELP("MP_PROP_IVD_VEH",iProperty)
							ENDIF
							CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_VEHICLE_SUITABLE_FOR_PROPERTY: Printing help MP_PROP_IVD_VEH: 1244235423")
							bPrintedReason = TRUE
						ENDIF
					ENDIF
				ENDIF
//				PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 7") //CDM DEBUG TEMP!
			ENDIF
		ENDIF
	ENDIF
//	PRINTLN("CDM: IS_VEHICLE_SUITABLE_FOR_PROPERTY- 1") //CDM DEBUG TEMP!
	RETURN FALSE
ENDFUNC

//#IF FEATURE_EXECUTIVE
//FUNC BOOL CHECK_SIMPLE_PROPERTY_ID(INT iPropertyID, INT iSimpleInteriorType #IF IS_DEBUG_BUILD, BOOL bIgnoreAssert = FALSE #ENDIF)
//
//	IF iSimpleInteriorType = SIMPLE_INTERIOR_TYPE_WAREHOUSE
//		RETURN CHECK_WAREHOUSE_ID(iPropertyID #IF IS_DEBUG_BUILD, bIgnoreAssert #ENDIF)
//
//	#IF FEATURE_IMPORT_EXPORT
//	ELIF iSimpleInteriorType = SIMPLE_INTERIOR_TYPE_IE_GARAGE
//		RETURN CHECK_IE_GARAGE_ID(INT_TO_ENUM(IMPORT_EXPORT_GARAGES, iPropertyID) #IF IS_DEBUG_BUILD, bIgnoreAssert #ENDIF)
//	#ENDIF
//		
//	#IF FEATURE_BIKER
//	ELIF iSimpleInteriorType = SIMPLE_INTERIOR_TYPE_FACTORY
//		RETURN CHECK_FACTORY_ID(INT_TO_ENUM(FACTORY_ID, iPropertyID) #IF IS_DEBUG_BUILD, bIgnoreAssert #ENDIF)
//		
//	ELIF iSimpleInteriorType = SIMPLE_INTERIOR_TYPE_CLUBHOUSE
//		RETURN FALSE
//	#ENDIF
//	
//	ELIF iSimpleInteriorType = -1 // check any of the above
//		
//		IF CHECK_WAREHOUSE_ID(iPropertyID #IF IS_DEBUG_BUILD, bIgnoreAssert #ENDIF)
//		#IF FEATURE_IMPORT_EXPORT
//		OR CHECK_IE_GARAGE_ID(INT_TO_ENUM(IMPORT_EXPORT_GARAGES, iPropertyID) #IF IS_DEBUG_BUILD, bIgnoreAssert #ENDIF)
//		#ENDIF
//		#IF FEATURE_BIKER
//		OR CHECK_FACTORY_ID(INT_TO_ENUM(FACTORY_ID, iPropertyID) #IF IS_DEBUG_BUILD, bIgnoreAssert #ENDIF)
//		#ENDIF
//			RETURN(TRUE)
//		ELSE
//			RETURN(FALSE)
//		ENDIF
//		
//		
//		
//	ENDIF
//	
//	
//	CASSERTLN(DEBUG_SAFEHOUSE, "CHECK_WAREHOUSE_ID called with invaild interior type: ", iSimpleInteriorType)
//	return false
//	
//ENDFUNC
//#ENDIF


PROC UPDATE_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_PROPERTIES()
	IF IS_TRANSITION_ACTIVE()
		EXIT
	ENDIF
	
	INT iPROPERTY_OFFICE_2_BASE = -1
	INT iPROPERTY_CLUBHOUSE_4_BASE_A = -1
	INT iPROPERTY_LOW_APT_2 = -1
	INT iPROPERTY_GARAGE_NEW_19 = -1
	
	INT i
	REPEAT MAX_OWNED_PROPERTIES i
		IF GET_OWNED_PROPERTY(i) = PROPERTY_OFFICE_2_BASE
			iPROPERTY_OFFICE_2_BASE = i
		ELIF GET_OWNED_PROPERTY(i) = PROPERTY_CLUBHOUSE_4_BASE_A
			iPROPERTY_CLUBHOUSE_4_BASE_A = i
		ELIF GET_OWNED_PROPERTY(i) = PROPERTY_LOW_APT_2
			iPROPERTY_LOW_APT_2 = i
		ELIF GET_OWNED_PROPERTY(i) = PROPERTY_GARAGE_NEW_19
			iPROPERTY_GARAGE_NEW_19 = i
		ENDIF
	ENDREPEAT
	
	IF iPROPERTY_OFFICE_2_BASE != -1
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE)
	ELSE
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE, FALSE)
	ENDIF
	IF iPROPERTY_CLUBHOUSE_4_BASE_A != -1
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE)
	ELSE
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE, FALSE)
	ENDIF
	IF iPROPERTY_LOW_APT_2 != -1
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT)
	ELSE
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT, FALSE)
	ENDIF
	IF iPROPERTY_GARAGE_NEW_19 != -1
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE)
	ELSE
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE, FALSE)
	ENDIF
	
	IF GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_FAKE_MONEY) = FACTORY_ID_FAKE_CASH_3
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY)
	ELSE
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY, FALSE)
	ENDIF
	IF GET_FACTORY_ID_OF_OWNED_FACTORY_TYPE(PLAYER_ID(), FACTORY_TYPE_WEAPONS) = FACTORY_ID_BUNKER_9
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER)
	ELSE
		SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER, FALSE)
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_SELECTABLE_AS_ASSASSIN_TARGET(PLAYER_INDEX pPlayer, BOOL bCheckAlive = TRUE)
	IF pPlayer != PLAYER_ID()
	AND IS_NET_PLAYER_OK(pPlayer, bCheckAlive)
	AND NOT IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(pPlayer)
	AND NOT IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(pPlayer)
	AND NOT IS_PLAYER_IN_PROPERTY(pPlayer, TRUE, TRUE)
	AND NOT IS_PLAYER_IN_PASSIVE_MODE(pPlayer)
	AND NOT IS_THIS_PLAYER_IN_CORONA(pPlayer)
	AND NOT NETWORK_IS_PLAYER_IN_MP_CUTSCENE(pPlayer)
	AND NOT IS_PLAYER_BEAST(pPlayer)
	AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(pPlayer,MPAM_TYPE_STRIPCLUB)
	AND NOT IS_PLAYER_IN_ZANCUDO_ARMY_BASE(pPlayer)
	AND NOT GlobalplayerBD[NATIVE_TO_INT(pPlayer)].bWithActiveProstitute
	AND NOT IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(pPlayer)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CINEMA)
	AND GET_PLAYER_RANK(pPlayer) >= 5
	AND NOT IS_BIT_SET(g_iStrikeTeamBlockSelectingPlayerBS, NATIVE_TO_INT(pPlayer))
	#IF FEATURE_GEN9_EXCLUSIVE
	AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(pPlayer) != FMMC_TYPE_HSW_SETUP
	#ENDIF 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_ASSASINS_ABILITY_ATTACK_LEVEL_COST(ASSASSIN_LEVEL eAttackLevel)
	SWITCH eAttackLevel
		CASE AL_LOW 	RETURN g_sMPTunables.iH2_STRIKE_TEAM_1_PRICE
		CASE AL_MED 	RETURN g_sMPTunables.iH2_STRIKE_TEAM_2_PRICE
		CASE AL_HIGH 	RETURN g_sMPTunables.iH2_STRIKE_TEAM_3_PRICE
	ENDSWITCH
	
	RETURN 9999999
ENDFUNC

PROC INIT_ASSASSINS_ABILITY()
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget = INVALID_PLAYER_INDEX()
	
	IF HAS_NET_TIMER_STARTED(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown)
		REINIT_NET_TIMER(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown)
	ENDIF
ENDPROC

FUNC ASSASSIN_LEVEL GET_SELECTED_ASSASINS_ABILITY_LEVEL(PLAYER_INDEX gangOwner)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(gangOwner)].sAssasinsAbility.eAttackLevel
ENDFUNC

PROC SELECT_ASSASINS_TARGET(PLAYER_INDEX pTarget, ASSASSIN_LEVEL eAttackLevel)

	#IF IS_DEBUG_BUILD
		IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_IgnoreStrikeTeamCD")
	#ENDIF
			START_NET_TIMER(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown)
	#IF IS_DEBUG_BUILD
		ENDIF
	#ENDIF
	
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget = pTarget
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.eAttackLevel = eAttackLevel
	
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangCallOwnerBitset, GANG_CALL_TYPE_SPECIAL_MERRYWEATHER)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangCallTargetID = NATIVE_TO_INT(pTarget) 
	
	PRINTLN("[ASSASSIN] SELECT_ASSASINS_TARGET - Traget selected: ", GET_PLAYER_NAME(pTarget), " Cooldown timer started. Attack Level: ", eAttackLevel)
ENDPROC

FUNC BOOL HAS_PLAYER_SELECTED_ASSASSINS_TARGET(PLAYER_INDEX gangOwner)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(gangOwner)].sAssasinsAbility.playerTarget != INVALID_PLAYER_INDEX()
ENDFUNC

FUNC PLAYER_INDEX GET_LOCAL_PLAYER_SELECTED_ASSASSINS_TARGET()
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget
ENDFUNC

FUNC BOOL IS_ASSASINS_ABILITY_COOLDOWN_ACTIVE()
	RETURN HAS_NET_TIMER_STARTED(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown)
ENDFUNC

PROC MAINTAIN_ASSASINS_ABILITY()
	
	BOOL bResetTarget 	= FALSE
	BOOL bResetTimer	= FALSE
	
	IF HAS_NET_TIMER_STARTED(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown)
		IF HAS_NET_TIMER_EXPIRED(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown, g_sMPTunables.iH2_STRIKE_TEAM_COOLDOWN_TIMER)
			PRINTLN("[ASSASSIN] MAINTAIN_ASSASINS_ABILITY - Cooldown timer expied")
			bResetTarget 	= TRUE
			bResetTimer		= TRUE
		ENDIF

//		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget = INVALID_PLAYER_INDEX()
//			PRINTLN("[FACILITY_ASSASSINS] MAINTAIN_ASSASINS_ABILITY - End early - playerTarget = INVALID_PLAYER_INDEX()")
//			bResetTarget = TRUE
//		ENDIF
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget != INVALID_PLAYER_INDEX()
	
		IF NOT IS_NET_PLAYER_OK(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget)
			PRINTLN("[FACILITY_ASSASSINS] MAINTAIN_ASSASINS_ABILITY - End early - Taregt is not OK")
			bResetTarget = TRUE
		ENDIF
		
		IF NOT IS_PLAYER_SELECTABLE_AS_ASSASSIN_TARGET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget, FALSE)
			PRINTLN("[FACILITY_ASSASSINS] MAINTAIN_ASSASINS_ABILITY - End early - IS_PLAYER_SELECTABLE_AS_ASSASSIN_TARGET = FALSE")
			bResetTarget 	= TRUE
			bResetTimer		= TRUE
		ENDIF
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iGangCallTargetID = -1
			PRINTLN("[FACILITY_ASSASSINS] MAINTAIN_ASSASINS_ABILITY - End early - iGangCallTargetID = -1")
			bResetTarget = TRUE
		ENDIF
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.bForceCleanup
		PRINTLN("[FACILITY_ASSASSINS] MAINTAIN_ASSASINS_ABILITY - End early - bForceCleanup = TRUE")
		bResetTarget 	= TRUE
		bResetTimer		= TRUE
	ENDIF
	
	IF bResetTarget
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.playerTarget = INVALID_PLAYER_INDEX()
	ENDIF
	
	IF bResetTimer
		RESET_NET_TIMER(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sAssasinsAbility.stCooldown)
	ENDIF
ENDPROC

CONST_INT PROP_TELEM_BS_OWNS_LOW_END_APT		0
CONST_INT PROP_TELEM_BS_OWNS_MEDIUM_APT			1
CONST_INT PROP_TELEM_BS_OWNS_HIGH_END_APT		2
CONST_INT PROP_TELEM_BS_OWNS_TWO_CAR_GARAGE		3
CONST_INT PROP_TELEM_BS_OWNS_SIX_CAR_GARAGE		4
CONST_INT PROP_TELEM_BS_OWNS_TEN_CAR_GARAGE		5
CONST_INT PROP_TELEM_BS_OWNS_BUSINESS_APT		6
CONST_INT PROP_TELEM_BS_OWNS_OFFICE				7
CONST_INT PROP_TELEM_BS_OWNS_SMALL_WAREHOUSE	8
CONST_INT PROP_TELEM_BS_OWNS_MEDIUM_WAREHOUSE	9
CONST_INT PROP_TELEM_BS_OWNS_LARGE_WAREHOUSE	10
CONST_INT PROP_TELEM_BS_OWNS_CLUBHOUSE			11
CONST_INT PROP_TELEM_BS_OWNS_METH_BUSINESS		12
CONST_INT PROP_TELEM_BS_OWNS_WEED_BUSINESS		13
CONST_INT PROP_TELEM_BS_OWNS_COCAINE_BUSINESS	14
CONST_INT PROP_TELEM_BS_OWNS_FAKE_CASH_BUSINESS	15
CONST_INT PROP_TELEM_BS_OWNS_FAKE_ID_BUSINESS	16
CONST_INT PROP_TELEM_BS_OWNS_VEHICLE_WAREHOUSE	17
CONST_INT PROP_TELEM_BS_OWNS_BUNKER				18
CONST_INT PROP_TELEM_BS_OWNS_HANGAR				19
CONST_INT PROP_TELEM_BS_OWNS_FACILITY			20
CONST_INT PROP_TELEM_BS_OWNS_ARENA				21
CONST_INT PROP_TELEM_BS_OWNS_NIGHTCLUB			22
CONST_INT PROP_TELEM_BS_OWNS_CASINO_PENTHOUSE	23
CONST_INT PROP_TELEM_BS_OWNS_ARCADE				24
CONST_INT PROP_TELEM_BS_OWNS_YACHT				25
CONST_INT PROP_TELEM_BS_OWNS_MOC				26
CONST_INT PROP_TELEM_BS_OWNS_AVENGER			27
CONST_INT PROP_TELEM_BS_OWNS_HACKER_TRUCK		28
CONST_INT PROP_TELEM_BS_OWNS_SUBMARINE			29
CONST_INT PROP_TELEM_BS_OWNS_AUTO_SHOP			30
CONST_INT PROP_TELEM_BS_OWNS_FIXER_HQ			31

#IF FEATURE_DLC_2_2022
CONST_INT PROP_TELEM_BS2_OWNS_MULTISTOREY_GAR	0
#ENDIF

/// PURPOSE:
///    Telemetry function to fill a bit set with information on the types of property a player owns
/// PARAMS:
///    bBitSetOne - True if we should fill bit set one or two 
/// RETURNS:
///    An int bit set filled with the propertey types a player owns.
FUNC INT GET_PROPERTIES_OWNED_BS(BOOL bBitSetOne)
	INT iReturn
	PLAYER_INDEX pLocalPlayer = PLAYER_ID()
	
	IF bBitSetOne
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(PROP_SIZE_TYPE_SMALL_APT)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_LOW_END_APT)
		ENDIF
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(PROP_SIZE_TYPE_MED_APT)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_MEDIUM_APT)
		ENDIF
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(PROP_SIZE_TYPE_LARGE_APT, TRUE)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_HIGH_END_APT)
		ENDIF
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(PROP_SIZE_TYPE_2_GAR)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_TWO_CAR_GARAGE)
		ENDIF
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(PROP_SIZE_TYPE_6_GAR)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_SIX_CAR_GARAGE)
		ENDIF
		IF DOES_LOCAL_PLAYER_OWN_PROPERTY_OF_SIZE(PROP_SIZE_TYPE_10_GAR)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_TEN_CAR_GARAGE)
		ENDIF
		IF DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(PROPERTY_BUS_HIGH_APT_1)
		OR DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(PROPERTY_BUS_HIGH_APT_2)
		OR DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(PROPERTY_BUS_HIGH_APT_3)
		OR DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(PROPERTY_BUS_HIGH_APT_4)
		OR DOES_LOCAL_PLAYER_OWN_THIS_PROPERTY(PROPERTY_BUS_HIGH_APT_5)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_BUSINESS_APT)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_OFFICE()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_OFFICE)
		ENDIF
		IF DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE(pLocalPlayer, eWarehouseSmall)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_SMALL_WAREHOUSE)
		ENDIF
		IF DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE(pLocalPlayer, eWarehouseMedium)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_MEDIUM_WAREHOUSE)
		ENDIF
		IF DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE(pLocalPlayer, eWarehouseLarge)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_LARGE_WAREHOUSE)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_CLUBHOUSE()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_CLUBHOUSE)
		ENDIF
		IF STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE_METH)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_METH_BUSINESS)
		ENDIF
		IF STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE_WEED)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_WEED_BUSINESS)
		ENDIF
		IF STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE_CRACK)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_COCAINE_BUSINESS)
		ENDIF
		IF STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE_FAKE_MONEY)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_FAKE_CASH_BUSINESS)
		ENDIF
		IF STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE_FAKE_IDS)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_FAKE_ID_BUSINESS)
		ENDIF
		IF IS_IE_WAREHOUSE_OWNED_BY_CHARACTER()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_VEHICLE_WAREHOUSE)
		ENDIF
		IF STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE_WEAPONS)
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_BUNKER)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_HANGAR()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_HANGAR)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_DEFUNCT_BASE()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_FACILITY)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_ARENA_GARAGE()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_ARENA)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_NIGHTCLUB()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_NIGHTCLUB)
		ENDIF
		IF IS_CASINO_APT_OWNED_BY_CHARACTER()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_CASINO_PENTHOUSE)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_ARCADE()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_ARCADE)
		ENDIF
		IF DOES_LOCAL_PLAYER_STAT_OWN_PRIVATE_YACHT()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_YACHT)
		ENDIF
		IF IS_GUNRUNNING_TRUCK_PURCHASED()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_MOC)
		ENDIF
		IF IS_ARMORY_AIRCRAFT_PURCHASED()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_AVENGER)
		ENDIF
		IF IS_HACKER_TRUCK_PURCHASED()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_HACKER_TRUCK)
		ENDIF		
		IF IS_SUBMARINE_OWNED_BY_CHARACTER()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_SUBMARINE)
		ENDIF
		IF IS_AUTO_SHOP_OWNED_BY_CHARACTER()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_AUTO_SHOP)
		ENDIF
		IF IS_FIXER_HQ_OWNED_BY_CHARACTER()
			SET_BIT(iReturn, PROP_TELEM_BS_OWNS_FIXER_HQ)
		ENDIF
		//This bitset is now full.
	ELSE
		#IF FEATURE_DLC_2_2022
		IF IS_MULTISTOREY_GARAGE_OWNED_BY_CHARACTER()
			SET_BIT(iReturn, PROP_TELEM_BS2_OWNS_MULTISTOREY_GAR)
		ENDIF
		#ENDIF
		//Add new properties here
		//Please also add to PRINT_OWNED_PROPERTIES
	ENDIF
	
	RETURN iReturn
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_OWNED_APARTMENTS()	
	INT i
	
	PRINTLN("[PROPERTY_OWNERSHIP] ============== Non-SI ==============")
	
	REPEAT (MAX_OWNED_PROPERTIES-1) i		
		IF g_MP_STAT_MULTI_PROPERTY[i][GET_ACTIVE_CHARACTER_SLOT()] != 0
			
			INT iPropertyID = g_MP_STAT_MULTI_PROPERTY[i][GET_ACTIVE_CHARACTER_SLOT()]
			TEXT_LABEL_15 tlPropertyName = GET_PROPERTY_NAME(iPropertyID)
			
			IF IS_PROPERTY_OFFICE(iPropertyID)
				PRINTLN("[PROPERTY_OWNERSHIP] Player owns Office: ", tlPropertyName, " (", iPropertyID, ") slot: ", i)
			ELIF IS_PROPERTY_OFFICE_GARAGE(iPropertyID)
				PRINTLN("[PROPERTY_OWNERSHIP] Player owns Office Garage: ", tlPropertyName, " (", iPropertyID, ") slot: ", i)
			ELIF IS_PROPERTY_CLUBHOUSE(iPropertyID)
				PRINTLN("[PROPERTY_OWNERSHIP] Player owns Clubhouse: ", tlPropertyName, " (", iPropertyID, ") slot: ", i)
			ELIF iPropertyID < PROPERTY_YACHT_APT_1_BASE
				IF IS_PROPERTY_ONLY_A_GARAGE(iPropertyID)
					PRINTLN("[PROPERTY_OWNERSHIP] Player owns Garage: ", tlPropertyName, " (", iPropertyID, ") slot: ", i)
				ELSE
					PRINTLN("[PROPERTY_OWNERSHIP] Player owns Apartment: ", tlPropertyName, " (", iPropertyID, ") slot: ", i)
				ENDIF
			ELIF iPropertyID = PROPERTY_YACHT_APT_1_BASE
				PRINTLN("[PROPERTY_OWNERSHIP] Player owns a yacht")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

//GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0)
//GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE)
FUNC VECTOR GET_PROPERTY_ENTRY_BLIP_LOCATION(INT iProperty,INT iEntrance = 0)
	INT iBuilding
	IF iProperty <> 0
		iBuilding = GET_PROPERTY_BUILDING(iProperty)
		IF iBuilding > 0 
			GET_BUILDING_EXTERIOR_DETAILS(tempPropertyStruct,iBuilding)
			IF iEntrance >= 0
			AND iEntrance < MAX_ENTRANCES
				RETURN tempPropertyStruct.vBlipLocation[iEntrance]
			ELSE
				PRINTLN("GET_PROPERTY_ENTRY_BLIP_LOCATION: invalid entrance ID:", iEntrance)
			ENDIF
		ELSE
			PRINTLN("GET_PROPERTY_ENTRY_BLIP_LOCATION: invalid property ID 1:", iProperty)
		ENDIF
	ELSE
		PRINTLN("GET_PROPERTY_ENTRY_BLIP_LOCATION: invalid property ID 2:", iProperty)
	ENDIF
	RETURN <<0,0,0>>
ENDFUNC

//GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_OFFICE_0)
//GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE)
PROC FLASH_PROPERTY_BLIP(INT iProperty,INT iTime = 10000)
	IF iProperty <> 0
		INT iBuilding = GET_PROPERTY_BUILDING(iProperty)
		INT j, iLoop
		GET_BUILDING_EXTERIOR_DETAILS(tempPropertyStruct,iProperty)
		IF iBuilding > 0 
			j = 0
			iLoop = tempPropertyStruct.iNumEntrances
			IF iLoop < 2
				iLoop = 2
			ENDIF
			REPEAT iLoop J
				IF DOES_BLIP_EXIST(mpPropMaintain.PropertyBlips[iBuilding][j])
				//	REMOVE_BLIP(mpPropMaintain.PropertyBlips[iBuilding][j])
					SET_BLIP_FLASHES(mpPropMaintain.PropertyBlips[iBuilding][j], TRUE)
					SET_BLIP_FLASH_TIMER(mpPropMaintain.PropertyBlips[iBuilding][j], iTime)
					PRINTLN("FLASH_PROPERTY_BLIP: Flashing property blip for iBuilding #",iBuilding," for iTime = ",iTime)
				ENDIF
			ENDREPEAT
			PRINTLN("FLASH_PROPERTY_BLIP: flashed blips if they existed for building :", iBuilding)
		ELSE
			PRINTLN("FLASH_PROPERTY_BLIP: invalid property ID 1:", iProperty)
		ENDIF
	ELSE
		PRINTLN("FLASH_PROPERTY_BLIP: invalid property ID 2:", iProperty)
	ENDIF
ENDPROC
 //FEATURE_GANG_OPS
 
 FUNC INT GET_PROPERTY_OWNED_SLOT_FROM_PROPERTY_ID(INT iProperty)
	INT iReturn
	IF IS_PROPERTY_OFFICE(iProperty)
		iReturn = PROPERTY_OWNED_SLOT_OFFICE_0
	ELSE
	
		SWITCH iProperty
			CASE PROPERTY_OFFICE_1_GARAGE_LVL1
			CASE PROPERTY_OFFICE_2_GARAGE_LVL1
			CASE PROPERTY_OFFICE_3_GARAGE_LVL1
			CASE PROPERTY_OFFICE_4_GARAGE_LVL1
				iReturn = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL1
			BREAK
			CASE PROPERTY_OFFICE_1_GARAGE_LVL2
			CASE PROPERTY_OFFICE_2_GARAGE_LVL2
			CASE PROPERTY_OFFICE_3_GARAGE_LVL2
			CASE PROPERTY_OFFICE_4_GARAGE_LVL2
				iReturn = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL2
			BREAK
			CASE PROPERTY_OFFICE_1_GARAGE_LVL3
			CASE PROPERTY_OFFICE_2_GARAGE_LVL3
			CASE PROPERTY_OFFICE_3_GARAGE_LVL3
			CASE PROPERTY_OFFICE_4_GARAGE_LVL3
				iReturn = PROPERTY_OWNED_SLOT_OFFICE_GARAGE_LVL3
			BREAK
		ENDSWITCH
	ENDIF
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_PROPERTY_OWNED_SLOT_FROM_PROPERTY_ID: iReturn = ", iReturn)	
	RETURN iReturn
	
ENDFUNC

 /// PURPOSE:
 ///    Used to force a player into a property, piggy backing on property invite system
 PROC FORCE_FAKE_INVITE_TO_PROPERTY(PLAYER_INDEX propertyOwner,INT iProperty, BOOL bForceNoEntryAnim = TRUE)
 	MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner = propertyOwner
	MPGlobalsAmbience.InvitedToApartmentData.iProperty = iProperty
	MPGlobalsAmbience.InvitedToApartmentData.iYacht = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(MPGlobalsAmbience.InvitedToApartmentData.piApartmentOwner)
	globalPropertyEntryData.iPropertyEntered 	=  iProperty
	globalPropertyEntryData.ownerID				= propertyOwner
	
	globalPropertyEntryData.ownerHandle						= GET_GAMER_HANDLE_PLAYER(propertyOwner)
	IF IS_PROPERTY_OFFICE_GARAGE(iProperty)
		globalPropertyEntryData.iEntrance 						= 2
	ELSE
		globalPropertyEntryData.iEntrance 						= 0
	ENDIF
	globalPropertyEntryData.iVehSlot 						= -1
	globalPropertyEntryData.iOldVehicleSlot					= -1
	globalPropertyEntryData.iCostOfPropertyJustBought 		= 0
	globalPropertyEntryData.iTradeInValueOfPreviousProperty = 0
	globalPropertyEntryData.iVariation = GlobalplayerBD_FM[NATIVE_TO_INT(propertyOwner)].propertyDetails.iPropertyVariant[GET_PROPERTY_OWNED_SLOT_FROM_PROPERTY_ID(iProperty)]
	
																
	
	SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_NOT_IN_A_VEHICLE)
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)			
	SET_BIT(MPGlobalsAmbience.InvitedToApartmentData.iBitSet, biITA_AcceptedInvite)
	SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_PLAYER_INTERACTION_INVITE)
	IF bForceNoEntryAnim
		SET_BIT(globalPropertyEntryData.iBS,GLOBAL_PROPERTY_ENTRY_BS_FORCE_NO_ENTRY_ANIM)
		PRINTLN("FORCE_FAKE_INVITE_TO_PROPERTY: set GLOBAL_PROPERTY_ENTRY_BS_FORCE_NO_ENTRY_ANIM")
	ENDIF
	PRINTLN("FORCE_FAKE_INVITE_TO_PROPERTY: called this frame for player: ", GET_PLAYER_NAME(propertyOwner)," property #",iProperty )
 ENDPROC

//EOF
