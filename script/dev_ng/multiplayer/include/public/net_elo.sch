//+-----------------------------------------------------------------------------+
//¦				ELO																¦
//+-----------------------------------------------------------------------------+

CONST_INT ELO_DEDUCTION -50
CONST_INT ELO_BASE		1200

ENUM ELO_WRITE_STAGES 
	ELO_WRITE_START = 0,
	ELO_WRITE_END,
	ELO_NOT_USED
ENDENUM

STRUCT ELO_VARS
	INT iEloReadStage
	INT iEloLoadStage
	BOOL bEloSuccess
ENDSTRUCT

FUNC INT GET_WRITE_VALUE(ELO_WRITE_STAGES eEloStage, INT iEndWriteValue)
	INT iReturn = 0
	
	IF eEloStage = ELO_WRITE_END
		iReturn = iEndWriteValue
	ENDIF

	RETURN iReturn
ENDFUNC

CONST_FLOAT ELO_EXPECTED_OUTCOME 	0.5
CONST_INT ELO_MODIFIER 				25

FUNC INT GET_ELO_RESULT(BOOL bWonJob)

	IF bWonJob
	
		RETURN 1
	ENDIF

	RETURN 0
ENDFUNC

// Starting Modifier: 100 for first 10 games, 25 for next 20 games, then slowly dropping to 7		
FUNC INT ELO_GET_MODIFIER(INT iNumJobsPlayed)

	IF iNumJobsPlayed <= 10
	
		RETURN 100
	ELIF iNumJobsPlayed <=30
	
		RETURN 25
	ELSE
		SWITCH iNumJobsPlayed
			CASE 31 RETURN 24
			CASE 32 RETURN 23
			CASE 33 RETURN 22
			CASE 34 RETURN 21
			CASE 35 RETURN 20
			CASE 36 RETURN 19
			CASE 37 RETURN 18
			CASE 38 RETURN 17
			CASE 39 RETURN 16
			CASE 40 RETURN 15
			CASE 41 RETURN 14
			CASE 42 RETURN 13
			CASE 43 RETURN 12
			CASE 44 RETURN 11
			CASE 45 RETURN 10
			CASE 46 RETURN 9
			CASE 47 RETURN 8
			DEFAULT
				RETURN 7
			BREAK
		ENDSWITCH
	ENDIF

	RETURN 100
ENDFUNC

FUNC INT ELO_GET_FLOOR()
	RETURN 100
ENDFUNC

FUNC FLOAT ELO_GET_EXPECTED_OUTCOME(INT iCurrentPlayerELO, INT iOpponentELO)

	FLOAT fExpected
	FLOAT fCurrentPlayerELO 	= TO_FLOAT(iCurrentPlayerELO)
	FLOAT fOpponentPlayerELO 	= TO_FLOAT(iOpponentELO)
	FLOAT fSum1 = (fOpponentPlayerELO - fCurrentPlayerELO)
	FLOAT fSum2 = (fSum1/400)
	FLOAT fSum3 = POW(10, fSum2)
	
	fExpected = 1/(1+fSum3)
	
	PRINTLN("[CS_ELO] ELO_GET_EXPECTED_OUTCOME fCurrentPlayerELO = ", fCurrentPlayerELO)
	PRINTLN("[CS_ELO] ELO_GET_EXPECTED_OUTCOME fOpponentPlayerELO = ", fOpponentPlayerELO)
	PRINTLN("[CS_ELO] ELO_GET_EXPECTED_OUTCOME fSum1 = ", fSum1)
	PRINTLN("[CS_ELO] ELO_GET_EXPECTED_OUTCOME fSum2 = ", fSum2)
	PRINTLN("[CS_ELO] ELO_GET_EXPECTED_OUTCOME fSum3 = ", fSum3)

	//fExpected = 1/ TO_FLOAT((1+ (10^((iOpponentELO - iCurrentPlayerELO)/400))))
	
	RETURN fExpected
ENDFUNC

FUNC INT GET_ELO_DIFF_FOR_LOCAL_PLAYER(INT iCurrentPlayerELO, INT iOpponentELO, INT iNumJobsOfTypePlayed, BOOL bWonJob)

	// 1804901
	iCurrentPlayerELO 	= (iCurrentPlayerELO + ELO_BASE) 
//	iOpponentELO 		= (iOpponentELO + ELO_BASE) // THIS IS DONE IN GET_AVERAGE_ELO

	INT iELO
	INT iStartModifier
	//INT iExpectedOutCome 
	INT iResult
	INT iDifferenceForConor
	FLOAT fExpectedOutcome
	
	#IF IS_DEBUG_BUILD
	PRINTNL()
	IF bWonJob
		PRINTLN(" [CS_ELO] GET_ELO_DIFF_FOR_LOCAL_PLAYER bWonJob  ")
	ELSE
		PRINTLN(" [CS_ELO] GET_ELO_DIFF_FOR_LOCAL_PLAYER LOST ")
	ENDIF
	PRINTLN(" [CS_ELO] GET_ELO_DIFF_FOR_LOCAL_PLAYER iCurrentPlayerELO = ", iCurrentPlayerELO, " iOpponentELO = ", iOpponentELO, " iNumJobsOfTypePlayed = ", iNumJobsOfTypePlayed)
	#ENDIF
	
	fExpectedOutcome = ELO_GET_EXPECTED_OUTCOME(iCurrentPlayerELO, iOpponentELO)
	iStartModifier = ELO_GET_MODIFIER(iNumJobsOfTypePlayed)
	iResult = GET_ELO_RESULT(bWonJob)
	PRINTLN(" [CS_ELO] GET_ELO_DIFF_FOR_LOCAL_PLAYER iExpectedOutCome = ", fExpectedOutcome, " iStartModifier = ", iStartModifier, " iResult = ", iResult)
	
	
	iELO = ROUND(iCurrentPlayerELO + iStartModifier * (iResult - fExpectedOutcome))
	PRINTLN(" [CS_ELO] GET_ELO_DIFF_FOR_LOCAL_PLAYER iELO = ", iELO)
	
	iDifferenceForConor = (iELO - iCurrentPlayerELO)
	
	IF (iCurrentPlayerELO - iDifferenceForConor) < 100
		PRINTLN(" [CS_ELO] GET_ELO_DIFF_FOR_LOCAL_PLAYER CAP BEING HIT, STAY AT 100 AND RETURN 0 DIFF 1721910 ")
	
		RETURN 0
	ENDIF
	
	RETURN iDifferenceForConor
ENDFUNC

PROC PREPARE_ELO_FOR_LBD_WRITE(INT& iElo, INT iCurrentPlayerELO, INT iOpponentELO, INT iNumJobsOfTypePlayed, BOOL bWon)
	iElo = GET_ELO_DIFF_FOR_LOCAL_PLAYER(iCurrentPlayerELO, iOpponentELO, iNumJobsOfTypePlayed, bWon)
	PRINTLN(" [CS_ELO] PREPARE_ELO_FOR_LBD_WRITE iElo = ", iElo)
	PRINTNL()
ENDPROC





