//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_CLUB_MUSIC_COMMON.sch																				//
// Description: Header file containing common functionality for club music.												//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		30/03/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_club_music_private.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUNCTIONS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determines if a club DJ is valid.
/// PARAMS:
///    eClubDJ - DJ to query.
/// RETURNS: TRUE if DJ is valid, FALSE otherwise.
FUNC BOOL IS_CLUB_DJ_VALID(CLUB_DJS eClubDJ)
	RETURN (eClubDJ > CLUB_DJ_NULL AND eClubDJ < CLUB_DJ_TOTAL)
ENDFUNC

/// PURPOSE:
///    Gets a tracks duration milliseconds as mix time.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    bWholeSet - true if want to get the time of whole set. false: it will check until end of current track
/// RETURNS: Mix time of the track duration in milliseconds.
FUNC INT GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, BOOL bWholeSet = FALSE)
	INT iTrack
	INT iTrackDurationMixTimeMS = 0
	FOR iTrack = 1 TO GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation)
		iTrackDurationMixTimeMS += GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, iTrack)
		IF !bWholeSet
			IF (DJServerData.iMusicTrack = iTrack)
				BREAKLOOP
			ENDIF
		ENDIF	
	ENDFOR
	RETURN iTrackDurationMixTimeMS
ENDFUNC

/// PURPOSE:
///    Converts the passed in mix time into track time as milliseconds.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    iCurrentMixTimeMS - Current mix time to convert.
/// RETURNS: Track time in milliseconds converted from the passed in mix time.
FUNC INT GET_CLUB_RADIO_STATION_TRACK_TIME_FROM_MIX_TIME_MS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, INT iCurrentMixTimeMS)
	INT iTimeUntilEndOfTrackMS = (DJServerData.iTrackDurationMixTimeMS-iCurrentMixTimeMS)
	RETURN (GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)-iTimeUntilEndOfTrackMS)
ENDFUNC

/// PURPOSE:
///    Converts the passed in track time into mix time as milliseconds.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    iCurrentTrackTimeMS - Current track time to convert.
/// RETURNS: Mix time in milliseconds converted from the passed in track time.
///    
FUNC INT GET_CLUB_RADIO_STATION_MIX_TIME_FROM_TRACK_TIME_MS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, INT iCurrentTrackTimeMS)
	IF (DJServerData.iMusicTrack > 1)
		INT iTrack
		FOR iTrack = 2 TO GET_CLUB_RADIO_STATION_MAX_TRACKS(eClubLocation, DJServerData.eRadioStation)
			iCurrentTrackTimeMS += GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, iTrack-1)
			IF (DJServerData.iMusicTrack = iTrack)
				BREAKLOOP
			ENDIF
		ENDFOR
	ENDIF
	RETURN iCurrentTrackTimeMS
ENDFUNC

/// PURPOSE:
///    Gets the current set time of club DJ music
/// RETURNS:
///    -1 if data file is not initialized 
FUNC INT GET_CLUB_MUSIC_CURRENT_SET_TIME(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData, DJ_LOCAL_DATA &DJLocalData)
	IF DJLocalData.eDJstate = CLUB_DJ_INIT_DATAFILE_STATE
		RETURN -1
	ENDIF
	
	INT iCurrentMixTimeMS 		= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DJServerData.stMusicTimer, DEFAULT, TRUE)
	INT iTrackDurationMS 		= GET_CLUB_RADIO_STATION_TRACK_DURATION_MS(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack)
	INT iTimeUntilEndOfTrackMS 	= (GET_CLUB_RADIO_STATION_TRACK_DURATION_MIX_TIME_MS(eClubLocation, DJServerData)-iCurrentMixTimeMS)
	INT iCurrentTrackTimeMS 	= (iTrackDurationMS-iTimeUntilEndOfTrackMS)
	
	INT iTotalPlayedSetTime = iCurrentTrackTimeMS
	IF DJServerData.iMusicTrack > 1
		iTotalPlayedSetTime = GET_SUM_CLUB_RADIO_STATION_TRACK_TIME_MS(eClubLocation, DJServerData, DJServerData.iMusicTrack - 1) + iCurrentTrackTimeMS
	ENDIF
	
	RETURN iTotalPlayedSetTime
ENDFUNC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INTENSITY ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the track intensity ID based on the current track time.
///    ID used to get the intensity track time and the intensity type itself.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
/// RETURNS: The track intensity ID.
FUNC INT GET_CLUB_RADIO_STATION_TRACK_INTENSITY_ID(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA &DJServerData)
		INT iCurrentMixTimeMS						= GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(DJServerData.stMusicTimer, DEFAULT, TRUE)
		INT iCurrentTrackTimeMS						= GET_CLUB_RADIO_STATION_TRACK_TIME_FROM_MIX_TIME_MS(eClubLocation, DJServerData, iCurrentMixTimeMS)
		INT iTrackIntensityID 						= 0
		INT iTrackIntensityTimeMS					= 0		
		CLUB_MUSIC_INTENSITY eClubMusicIntensity	= CLUB_MUSIC_INTENSITY_NULL		
		GET_CLUB_RADIO_STATION_TRACK_INTENSITY(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack, iTrackIntensityID, iTrackIntensityTimeMS, eClubMusicIntensity)
		
		// Loops round and finds the track intensity ID when the current track time is less than one of the track intensity time tags.
		WHILE (iTrackIntensityTimeMS <= iCurrentTrackTimeMS) //AND iTrackIntensityID < 100
			iTrackIntensityID++
			GET_CLUB_RADIO_STATION_TRACK_INTENSITY(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack, iTrackIntensityID, iTrackIntensityTimeMS, eClubMusicIntensity)
		
		ENDWHILE
		
		// Track intensity ID has been found. Minus one as the while loop stops on the intensity time thats greater than the current track time. The intensity above the one we are on.
		iTrackIntensityID -= 1
		IF (iTrackIntensityID < 0)
			iTrackIntensityID = 0
		ENDIF
		
		RETURN (iTrackIntensityID)
ENDFUNC

/// PURPOSE:
///    Gets the track time in milliseconds that the intensity starts at. Based on the intensity ID passed in.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    iTrackIntensityID - Intensity ID to get time for.
/// RETURNS: Time the intensity changes in milliseconds based on the intensity ID passed in.
FUNC INT GET_CLUB_RADIO_STATION_TRACK_INTENSITY_TIME_MS(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA DJServerData, INT iTrackIntensityID)	
	INT iTrackIntensityTimeMS					= -1
	CLUB_MUSIC_INTENSITY eClubMusicIntensity	= CLUB_MUSIC_INTENSITY_NULL
	GET_CLUB_RADIO_STATION_TRACK_INTENSITY(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack, iTrackIntensityID, iTrackIntensityTimeMS, eClubMusicIntensity)
	RETURN iTrackIntensityTimeMS
ENDFUNC

/// PURPOSE:
///    Wrapper function to return the intensity type based on the intensity ID passed in.
/// PARAMS:
///    eClubLocation - Club location.
///    DJServerData - Server data to query.
///    iTrackIntensityID - Intensity ID to get intensity for.
/// RETURNS: Intensity type based on the intensity ID passed in.
FUNC CLUB_MUSIC_INTENSITY GET_CLUB_RADIO_STATION_TRACK_INTENSITY_WRAPPER(CLUB_LOCATIONS eClubLocation, DJ_SERVER_DATA DJServerData, INT iTrackIntensityID)
	INT iTrackIntensityTimeMS					= -1
	CLUB_MUSIC_INTENSITY eClubMusicIntensity	= CLUB_MUSIC_INTENSITY_NULL
	GET_CLUB_RADIO_STATION_TRACK_INTENSITY(eClubLocation, DJServerData.eRadioStation, DJServerData.iMusicTrack, iTrackIntensityID, iTrackIntensityTimeMS, eClubMusicIntensity)
	RETURN eClubMusicIntensity
ENDFUNC
#ENDIF //FEATURE_HEIST_ISLAND
