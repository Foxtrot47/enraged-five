//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_ISLAND.sch																						//
// Description: Functions to implement from net_peds_base.sch and look up tables to return island ped data.				//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		07/07/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_base.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"
USING "heist_island_warp_data.sch"
USING "fmc_data_crawler.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC PLAYER_INDEX GET_PLAYER_GANG_BOSS()
	PLAYER_INDEX pPlayerToCheck = PLAYER_ID()
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
		pPlayerToCheck = GB_GET_LOCAL_PLAYER_GANG_BOSS()
		IF pPlayerToCheck = INVALID_PLAYER_INDEX()
			pPlayerToCheck = PLAYER_ID()
		ENDIF
	ENDIF
	RETURN pPlayerToCheck
ENDFUNC

FUNC VECTOR _GET_ISLAND_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN <<4893.4746, -4903.8711, 1.4687>>	// DJ Stand Coords
ENDFUNC

FUNC FLOAT _GET_ISLAND_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN -92.70	// DJ Stand Heading
ENDFUNC

PROC _SET_ISLAND_PED_DATA_LAYOUT_0(SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
	SWITCH iPed
		// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_RAMPA)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_RAMPA)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 16777473
					Data.iPackedDrawable[2]								= 2
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.1325, 0.0160, 1.9063>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION)
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[0] = Data.iActivity 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_ME)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ME)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.1325, 0.0160, 1.9063>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[1] = Data.iActivity 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_KEINEMUSIK_ADAM)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_ADAM)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 65792
					Data.iPackedDrawable[2]								= 16777217
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.1325, 0.0160, 1.9063>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION)
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[2] = Data.iActivity 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_DANCER_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66052
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-0.4083, -0.4944, 1.0263>>
					Data.vRotation 										= <<0.0000, 0.0000, 292.7500>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FREEZE_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DJ_KEINEMUSIK_DANCER_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 131086
					Data.iPackedTexture[2] 								= 67108864
					Data.vPosition 										= <<-0.1019, 0.4410, 1.0263>>
					Data.vRotation 										= <<0.0000, 0.0000, 273.2090>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FREEZE_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 17367040
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 524551
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<-2.4094, 0.1249, 2.0213>>
					Data.vRotation 										= <<0.0000, 0.0000, 287.8200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 131589
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.3436, 0.9981, 2.0213>>
					Data.vRotation 										= <<0.0000, 0.0000, 243.1990>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						IF NOT HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GET_PLAYER_GANG_BOSS(), IHIT_SCOPING_INFORMATION)
							Data.vPosition 								= <<-3.0173, 0.8461, 1.8913>>
							Data.vRotation 								= <<0.0, 0.0, 258.1000>>
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
						ENDIF
					ENDIF
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 196608
					Data.iPackedDrawable[2]								= 50724864
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 262405
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-2.1304, 2.1887, 2.0213>>
					Data.vRotation 										= <<0.0000, 0.0000, 253.9800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16777473
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 515
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-3.1423, -1.0017, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 298.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 65792
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 918023
					Data.iPackedTexture[2] 								= 50331648
					Data.vPosition 										= <<-1.9090, -1.6132, 2.0213>>
					Data.vRotation 										= <<0.0000, 0.0000, 298.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 131584
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 131595
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<5.3546, -2.6012, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 51.6970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 11
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 131072
					Data.iPackedDrawable[2]								= 196608
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 66055
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.2355, 0.0059, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 89.1000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 12
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33619968
					Data.iPackedDrawable[2]								= 50987008
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65795
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.2997, 0.9763, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 85.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 13
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 266
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<5.1746, 2.0864, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 110.4980>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 14
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33620992
					Data.iPackedDrawable[2]								= 50921472
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 259
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<44.6551, -14.0048, 2.1013>>
					Data.vRotation 										= <<0.0000, 0.0000, 189.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 15
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33620482
					Data.iPackedDrawable[2]								= 33816576
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 262401
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<5.7606, 5.9845, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 16
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16908288
					Data.iPackedDrawable[2]								= 17235968
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131590
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.7606, 5.9845, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 17
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 459016
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<5.9394, 4.0387, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 122.8970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 18
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16843521
					Data.iPackedDrawable[2]								= 67305472
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 262145
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<8.3415, 1.6022, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.5400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 19
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 33685504
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<6.7657, 1.8173, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 91.6970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 20
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 50397442
					Data.iPackedDrawable[2]								= 65536
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 393478
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<6.8847, 0.7436, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 91.6970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 21
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33619968
					Data.iPackedDrawable[2]								= 51052544
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 196609
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<6.3256, -1.2909, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 71.1000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 22
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131329
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 327697
					Data.iPackedTexture[2] 								= 83886080
					Data.vPosition 										= <<6.5407, -4.9181, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 52.3800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 23
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 590087
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.6716, -5.6967, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 41.5800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 24
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 131329
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 852480
					Data.iPackedTexture[2] 								= 83886080
					Data.vPosition 										= <<3.7171, 8.0825, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 306.5400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 25
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 655369
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-0.4864, 1.7842, 2.0213>>
					Data.vRotation 										= <<0.0000, 0.0000, 223.7400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 26
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 17563648
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 524550
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<4.5838, 9.0893, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 156.7800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 27
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 33751040
					Data.iPackedDrawable[2]								= 50528256
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 393736
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.6892, -3.6945, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 336.7800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 28
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 34209792
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 458752
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<10.1815, 5.2188, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 110.8970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 29
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 50331649
					Data.iPackedDrawable[2]								= 34144256
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131592
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<15.5795, 12.1336, 1.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 175.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 30
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPD_M_CHILD_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 131329
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 67108864
					Data.vPosition 										= <<8.3284, 7.6122, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 31
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16777216
					Data.iPackedDrawable[2]								= 33685504
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 65537
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.8600, 7.3076, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 145.9800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 32
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 196608
					Data.iPackedDrawable[2]								= 50855936
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 459272
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<7.4985, 5.2986, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 142.3800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 33
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 131585
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 786434
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.2972, -0.4441, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 81.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 34
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 17039360
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 66051
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<8.1842, -1.4897, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 95.2970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 35
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 786955
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<9.6987, 0.5718, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 86.2970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 36
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66051
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<8.1421, 2.6487, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 86.2970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 37
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131072
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 393486
					Data.iPackedTexture[2] 								= 50331648
					Data.vPosition 										= <<7.9957, 4.4778, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 126.5400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 38
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 17104896
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<9.8010, -0.4685, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 89.1000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 39
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 131592
					Data.iPackedTexture[2] 								= 83886080
					Data.vPosition 										= <<7.3536, -2.6473, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 53.2960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 40
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 34209792
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 327937
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<8.4007, -3.9964, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 53.2960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 41
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 33619968
					Data.iPackedDrawable[2]								= 524288
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 524806
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<7.2113, -6.2757, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 43.0200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 42
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131072
					Data.iPackedDrawable[2]								= 50462720
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 459012
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<45.7001, -14.1686, 2.1113>>
					Data.vRotation 										= <<0.0000, 0.0000, 133.7400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 43
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_03)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 852496
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<10.5164, 8.6686, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 111.6960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 44
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 17104896
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 196610
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-1.1879, 5.4843, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 208.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 45
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPD_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16777473
					Data.iPackedDrawable[2]								= 17170432
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65537
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<8.3284, 7.6122, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 46
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPD_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33619969
					Data.iPackedDrawable[2]								= 34013184
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 196866
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.3284, 7.6122, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 47
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33685504
					Data.iPackedDrawable[2]								= 720896
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 393476
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<9.5244, 9.4924, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 145.9800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 48
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 196608
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<10.2357, 3.9861, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 98.6901>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 49
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 33686018
					Data.iPackedDrawable[2]								= 17104896
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131074
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<10.2427, 2.9452, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 98.6960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 50
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33685504
					Data.iPackedDrawable[2]								= 262144
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 196610
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<13.1252, 3.1452, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 98.6960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 51
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16777473
					Data.iPackedDrawable[2]								= 17170432
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65537
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<12.1152, -0.0548, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 159.6600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 52
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 50398208
					Data.iPackedDrawable[2]								= 50331648
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 918023
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.0627, -1.4920, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 9.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 53
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 262156
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<9.8208, -2.9513, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 60.6960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 54
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131585
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 590096
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<9.2688, -5.6634, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 43.7400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 55
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_03)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33751040
					Data.iPackedDrawable[2]								= 50462720
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 327941
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<12.9949, -0.7774, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 90.5400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 56
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 524559
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<13.1252, 3.1452, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 98.6960>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 57
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPC_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16843778
					Data.iPackedDrawable[2]								= 50397184
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 524296
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<12.1997, 7.5122, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 116.4600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 58
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPC_M_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 131584
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 720911
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<12.1997, 7.5122, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 116.4600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 59
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 33620226
					Data.iPackedDrawable[2]								= 524288
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 852481
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-0.1739, 5.5121, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 141.6600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 60
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131584
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 459013
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.2643, 4.1782, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 212.9400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 61
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 131328
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 196866
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-3.1965, 2.1790, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 246.7800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 62
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 66561
					Data.iPackedDrawable[2]								= 50790400
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 131328
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-1.2309, -2.2172, 2.0213>>
					Data.vRotation 										= <<0.0000, 0.0000, 318.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 63 // Party Guy
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_VIP_PARTY_GUY)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 65792
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 393220
					Data.iPackedTexture[2] 								= 67108864
					Data.vPosition 										= <<17.5452, 1.2258, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 64
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SUNBATHING_BACK_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 66561
					Data.iPackedDrawable[2]								= 50331648
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 327683
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<14.2312, -19.9445, 1.4493>>
					Data.vRotation 										= <<1.8000, 0.0000, 15.6600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 65
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SUNBATHING_BACK_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 33947648
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 459266
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.4180, -20.3775, 1.5363>>
					Data.vRotation 										= <<0.5000, 0.0000, 34.3800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 66
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_M_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 50397954
					Data.iPackedDrawable[2]								= 67633152
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 655872
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<24.3166, -18.7430, 1.6493>>
					Data.vRotation 										= <<0.0000, 0.0000, 220.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 67
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 34209792
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 458752
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<25.4935, -18.5614, 1.6263>>
					Data.vRotation 										= <<0.0000, 0.0000, 163.2600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 68 // Party Girl
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_VIP_PARTY_GIRL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33554432
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.5452, 1.2258, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 69
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SUNBATHING_BACK_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 196608
					Data.iPackedDrawable[2]								= 50790400
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 196867
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<45.0042, -25.2215, 1.2073>>
					Data.vRotation 										= <<9.0000, 0.0000, 330.3000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 70 // Party Couple Male
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_VIP_PARTY_COUPLE_MALE_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 131328
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 262401
					Data.iPackedTexture[2] 								= 50331648
					Data.vPosition 										= <<17.5452, 1.2258, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 71
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_SUNBATHING_FRONT_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16777473
					Data.iPackedDrawable[2]								= 17367040
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131334
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<33.5988, -20.7518, 1.2993>>
					Data.vRotation 										= <<7.0000, 0.0000, 341.1000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 72 // Party Coupld Female
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_VIP_PARTY_COUPLE_FEMALE_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 328197
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<17.5452, 1.2258, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 73 // Scott
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_SCOTT)
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						IF HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GET_PLAYER_GANG_BOSS(), IHIT_SCOPING_INFORMATION)
							Data.iActivity 								= ENUM_TO_INT(PED_ACT_BEACH_VIP_SCOTT_IG4)
							Data.vPosition 								= <<20.6096, 11.5090, 0.9940>>
							Data.vRotation 								= <<0.0, 0.0, 92.7000>>
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
						ELSE
							Data.iActivity 								= ENUM_TO_INT(PED_ACT_BEACH_VIP_SCOTT_IG3)
							Data.vPosition 								= <<20.6096, 11.5090, 0.9940>>
							Data.vRotation 								= <<0.0, 0.0, 92.7000>>
						ENDIF
					ELSE
						Data.iActivity 									= ENUM_TO_INT(PED_ACT_BEACH_VIP_SCOTT_IG4)
						Data.vPosition 									= <<20.6096, 11.5090, 0.9940>>
						Data.vRotation 									= <<0.0, 0.0, 92.7000>>
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					ENDIF
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 74
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 197129
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<25.2425, -21.0760, 1.4493>>
					Data.vRotation 										= <<0.0000, 0.0000, 17.8200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 75
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 197122
					Data.iPackedDrawable[2]								= 34275328
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 524288
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<30.3331, 14.9337, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 76
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_SMOKING_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 721418
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<29.5626, 14.2209, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 259.0200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 77
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33751040
					Data.iPackedDrawable[2]								= 65536
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<30.3712, 13.4940, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 19.2600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 78
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 33882112
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<39.3015, 8.5083, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 151.1400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 79
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 13
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<34.4884, 15.0417, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 279.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 80
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33619968
					Data.iPackedDrawable[2]								= 196608
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 196610
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<33.8119, 7.3802, 1.4743>>
					Data.vRotation 										= <<0.0000, 0.0000, 237.4200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 81
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33620482
					Data.iPackedDrawable[2]								= 17170432
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 590087
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<36.6270, 5.7850, 1.4343>>
					Data.vRotation 										= <<0.0000, 0.0000, 73.9800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 82
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 655360
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 393735
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<33.8810, 6.0612, 1.4143>>
					Data.vRotation 										= <<0.0000, 0.0000, 284.2200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 83
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 65539
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<20.2670, 9.2594, 1.4763>>
					Data.vRotation 										= <<0.0000, 0.0000, 73.9800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 84
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 17563648
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 524290
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<19.1807, 10.1330, 1.4013>>
					Data.vRotation 										= <<0.0000, 0.0000, 228.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 85
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 524550
					Data.iPackedTexture[2] 								= 67108864
					Data.vPosition 										= <<27.2242, -3.2876, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 47.3400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 86
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 17563648
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 262144
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<26.1092, -3.6628, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 350.4600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 87
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 17170432
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 393217
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<26.3888, -2.4501, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 193.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 88
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33685504
					Data.iPackedDrawable[2]								= 196608
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 327680
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<22.0111, -7.3570, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 317.6989>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 89
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 131584
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 655366
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<23.2273, -6.9512, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 95.6970>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 90
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 131842
					Data.iPackedDrawable[2]								= 67305472
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 65799
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<18.0000, 3.7505, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 280.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 91
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_PARENT)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 196608
					Data.iPackedDrawable[2]								= 50462720
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 327939
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<15.1932, 5.7196, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 92
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPB_F_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33619968
					Data.iPackedDrawable[2]								= 50659328
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<15.1932, 5.7196, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 93
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 17301504
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65799
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<18.0366, -5.2548, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 179.1000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 94
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 196608
					Data.iPackedDrawable[2]								= 589824
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 459271
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.2470, -6.1939, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 261.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 95	// Scott Dancer
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						IF HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GET_PLAYER_GANG_BOSS(), IHIT_SCOPING_INFORMATION)
							Data.iActivity 								= ENUM_TO_INT(PED_ACT_BEACH_VIP_SCOTT_IG4_DANCER)
							Data.vPosition 								= <<20.6096, 11.5090, 0.9940>>
							Data.vRotation 								= <<0.0, 0.0, 92.7000>>
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
						ELSE
							Data.vPosition 								= <<20.6096, 11.5090, 0.9940>>
							Data.vRotation 								= <<0.0, 0.0, 92.7000>>
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
						ENDIF
					ELSE
						Data.iActivity 									= ENUM_TO_INT(PED_ACT_BEACH_VIP_SCOTT_IG4_DANCER)
						Data.vPosition 									= <<20.6096, 11.5090, 0.9940>>
						Data.vRotation 									= <<0.0, 0.0, 92.7000>>
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					ENDIF
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 33554432
					Data.iPackedDrawable[2]								= 50790400
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131592
					Data.iPackedTexture[2] 								= 33554432
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 96
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 33685504
					Data.iPackedDrawable[2]								= 196608
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 131592
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<13.3563, -4.0257, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 66.6950>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 97
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 17039360
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 262660
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.9856, -5.0585, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 61.7400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 98
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_04)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 83886080
					Data.vPosition 										= <<18.3397, 4.7676, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 189.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 99
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 17563648
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 197123
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<19.7324, 1.3456, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 211.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 100
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_M_03)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 918017
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<20.3260, 0.2850, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 47.3400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 101
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_CLUB_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16908545
					Data.iPackedDrawable[2]								= 17301504
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 264
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<15.4206, 0.5556, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 229.5000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 102
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_M_04)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_D
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 33685504
					Data.iPackedDrawable[2]								= 51052544
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 262145
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<24.4953, 3.8183, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 189.9000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_D
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 103	// Dave
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_DAVE)
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						IF HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GET_PLAYER_GANG_BOSS(), IHIT_SCOPING_INFORMATION)
							SWITCH ServerBD.IslandData.iDaveSpawnLocationOnMission
								CASE 0
									Data.iActivity 						= ENUM_TO_INT(PED_ACT_BEACH_VIP_DAVE_IG6)
									Data.vPosition 						= <<17.5452, 1.2258, 0.9940>>
									Data.vRotation 						= <<0.0, 0.0, 92.7000>>
								BREAK
								CASE 1
									Data.iActivity 						= ENUM_TO_INT(PED_ACT_BEACH_VIP_DAVE_IG7)
									Data.vPosition 						= <<6.7193, -36.6094, -0.8453>>
									Data.vRotation 						= <<0.0, 0.0, 92.7000>>
								BREAK
							ENDSWITCH
						ELSE
							Data.iActivity 								= ENUM_TO_INT(PED_ACT_BEACH_VIP_DAVE_IG5)
							Data.vPosition 								= <<17.5452, 1.2258, 0.9940>>
							Data.vRotation 								= <<0.0, 0.0, 92.7000>>
						ENDIF
					ELSE
						SWITCH ServerBD.IslandData.iDaveSpawnLocationOffMission
							CASE 0
								Data.iActivity 							= ENUM_TO_INT(PED_ACT_BEACH_VIP_DAVE_IG5)
								Data.vPosition 							= <<17.5452, 1.2258, 0.9940>>
								Data.vRotation 							= <<0.0, 0.0, 92.7000>>
							BREAK
							CASE 1
								Data.iActivity 							= ENUM_TO_INT(PED_ACT_BEACH_VIP_DAVE_IG6)
								Data.vPosition 							= <<17.5452, 1.2258, 0.9940>>
								Data.vRotation 							= <<0.0, 0.0, 92.7000>>
							BREAK
							CASE 2
								Data.iActivity 							= ENUM_TO_INT(PED_ACT_BEACH_VIP_DAVE_IG7)
								Data.vPosition 							= <<6.7193, -36.6094, -0.8453>>
								Data.vRotation 							= <<0.0, 0.0, 92.7000>>
							BREAK
						ENDSWITCH
					ENDIF
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16842753
					Data.iPackedDrawable[2]								= 65536
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 104
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_ONE
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 16842752
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 328195
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<32.0365, -4.1869, 1.4303>>
					Data.vRotation 										= <<0.0000, 0.0000, 28.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 105
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16908546
					Data.iPackedDrawable[2]								= 262144
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 5
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<31.8846, -2.8518, 1.4913>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.8600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 106
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 66053
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<18.9538, -6.0675, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 92.7000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 107
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 33685504
					Data.iPackedDrawable[2]								= 458752
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 524548
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.9235, -6.9099, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 352.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 108
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_FACE_DJ_F_05)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 17367040
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 393478
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<19.1885, 3.8818, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 79.7400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 109
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 17104896
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131074
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<16.1938, -0.4261, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 41.5800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 110	// El Rubio
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_ELRUBIO)
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						IF HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GET_PLAYER_GANG_BOSS(), IHIT_SCOPING_INFORMATION)
							Data.iActivity 								= ENUM_TO_INT(PED_ACT_BEACH_VIP_ELRUBIO_IG2)
							Data.vPosition 								= <<30.0454, -6.3603, 1.5133>>
							Data.vRotation 								= <<0.0, 0.0, 182.7000>>
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
						ELSE
							Data.iActivity 								= ENUM_TO_INT(PED_ACT_BEACH_VIP_ELRUBIO_IG1)
							Data.vPosition 								= <<17.5452, 1.2258, 0.9940>>
							Data.vRotation 								= <<0.0, 0.0, 92.7000>>
							SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
						ENDIF
					ELSE
						Data.vPosition 								= <<-1.7641, 1.0255, 2.0213>>
						Data.vRotation 								= <<0.0, 0.0, 243.1990>>
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 65536
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ENDIF
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 111
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= -1
					Data.iPackedDrawable[1]								= -1
					Data.iPackedDrawable[2]								= -1
					Data.iPackedTexture[0] 								= -1
					Data.iPackedTexture[1] 								= -1
					Data.iPackedTexture[2] 								= -1
					Data.vPosition 										= <<35.7145, -2.7381, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 122.9400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 112
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_SEATED_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= -1
					Data.iPackedDrawable[1]								= -1
					Data.iPackedDrawable[2]								= -1
					Data.iPackedTexture[0] 								= -1
					Data.iPackedTexture[1] 								= -1
					Data.iPackedTexture[2] 								= -1
					Data.vPosition 										= <<29.7028, -8.1763, 1.4033>>
					Data.vRotation 										= <<0.0000, 0.0000, 102.0600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 113
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_PROP_F_02)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16777473
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 516
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<23.1285, 12.7848, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 89.1000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 114
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_LEAN_F_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16974081
					Data.iPackedDrawable[2]								= 17039360
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 131587
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<21.6278, 13.3750, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 208.6200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 115
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_BEACH_F_02)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131074
					Data.iPackedDrawable[1]								= 16843009
					Data.iPackedDrawable[2]								= 17563648
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 393480
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.2482, -4.1089, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 27.1800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 116
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_02)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_GROUPA_M_CHILD_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 66304
					Data.iPackedDrawable[2]								= 67502080
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 459010
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.6892, -3.6945, 0.8913>>
					Data.vRotation 										= <<0.0000, 0.0000, 336.7800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 117
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_MALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01)
					Data.iLevel 										= 3
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 33554432
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 327948
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<36.4559, 6.8235, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 96.3000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 118
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_BEACH_FEMALE_01)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_DANCING_SINGLE_PROP_F_01)
					Data.iLevel 										= g_sMPTunables.iBEACH_PARTY_PEDS_CULLING_LEVEL_BLOCK_C
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 66050
					Data.iPackedDrawable[2]								= 33816576
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 262401
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<25.0473, 2.4678, 1.9013>>
					Data.vRotation 										= <<0.0000, 0.0000, 39.4200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
					IF g_sMPTunables.bBEACH_PARTY_PEDS_REMOVE_BLOCK_C
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 119
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_ISLAND_GUARDS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CLIPBOARD)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 33554432
					Data.iPackedDrawable[2]								= 50462720
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 131073
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<2.5565, 23.7581, 1.9113>>
					Data.vRotation 										= <<0.0000, 0.0000, 39.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 120
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_ISLAND_GUARDS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 16842753
					Data.iPackedDrawable[2]								= 33685504
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<-32.1073, 52.7170, 2.4098>>
					Data.vRotation 										= <<0.0000, 0.0000, 263.6500>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 121
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_ISLAND_GUARDS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65537
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 131073
					Data.iPackedTexture[2] 								= 33554432
					Data.vPosition 										= <<10.4971, 63.9014, 3.4438>>
					Data.vRotation 										= <<0.0000, 0.0000, 150.7350>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 122
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_ISLAND_GUARDS)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_AMBIENT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65539
					Data.iPackedDrawable[1]								= 33619968
					Data.iPackedDrawable[2]								= 67239936
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 16777216
					Data.vPosition 										= <<51.7089, -18.2157, 1.9913>>
					Data.vRotation 										= <<0.0000, 0.0000, 119.3350>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity))
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	// Transform the local coords into world coords.
	IF NOT IS_VECTOR_ZERO(Data.vPosition)
		Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_ISLAND_PED_LOCAL_COORDS_BASE_POSITION(), _GET_ISLAND_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
	ENDIF
	
	// Transform the local heading into world heading.
	IF (Data.vRotation.z != -1.0)
		Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_ISLAND_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_ISLAND_PED_SCRIPT_LAUNCH()
	RETURN IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
ENDFUNC

FUNC BOOL _IS_ISLAND_PARENT_A_SIMPLE_INTERIOR()
	RETURN FALSE
ENDFUNC

PROC _SET_ISLAND_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ePedLocation)
	SWITCH iLayout
		CASE 0
			_SET_ISLAND_PED_DATA_LAYOUT_0(ServerBD, Data, iPed, bSetPedArea)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT _GET_ISLAND_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_ISLAND_PED_DATA(ePedLocation, ServerBD, tempData, (iPed+NETWORK_PED_ID_0), ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_ISLAND_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_ISLAND_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_ISLAND_SERVER_PED_LAYOUT_TOTAL()
	RETURN 1
ENDFUNC

FUNC INT _GET_ISLAND_SERVER_PED_LAYOUT()
	RETURN 0
ENDFUNC

FUNC INT _GET_ISLAND_SERVER_PED_LEVEL()
	RETURN 3
ENDFUNC

PROC _SET_ISLAND_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_ISLAND_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_ISLAND_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_ISLAND_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_ISLAND_NETWORK_PED_TOTAL(ServerBD)
		ServerBD.IslandData.iDaveSpawnLocationOnMission = GET_RANDOM_INT_IN_RANGE(0, 2)
		ServerBD.IslandData.iDaveSpawnLocationOffMission = GET_RANDOM_INT_IN_RANGE(0, 3)
	ENDIF
	PRINTLN("[AM_MP_PEDS] _SET_ISLAND_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_ISLAND_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_ISLAND_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_ISLAND_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
	PRINTLN("[AM_MP_PEDS] _SET_ISLAND_PED_SERVER_DATA - Dave On Mission: ", ServerBD.IslandData.iDaveSpawnLocationOnMission)
	PRINTLN("[AM_MP_PEDS] _SET_ISLAND_PED_SERVER_DATA - Dave Off Mission: ", ServerBD.IslandData.iDaveSpawnLocationOffMission)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_ISLAND_PARENT_PROPERTY(PLAYER_INDEX playerID)
	IF playerID <> INVALID_PLAYER_INDEX()
		IF IS_PLAYER_IN_WARP_TRANSITION(playerID)
			RETURN TRUE
		ENDIF	
		RETURN IS_PLAYER_ON_HEIST_ISLAND(playerID)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL _HAS_ISLAND_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK)
		OR IS_PEDS_BIT_SET(data.iBS, BS_PED_DATA_NO_ANIMATION))
	OR (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL _DOES_ISLAND_ACTIVE_PED_TOTAL_CHANGE()
	RETURN TRUE
ENDFUNC

FUNC INT _GET_ISLAND_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 15	BREAK
		CASE 2	iThreshold = 10	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_ISLAND_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_FOOTSTEPS_EVENTS_ENABLED(PedID, FALSE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_ISLAND_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_PED(NetworkPedID), TRUE)
	
	IF NOT IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC

PROC _SET_ISLAND_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(ePedModel)
	
	CLUB_DJS eCurrentDJ = g_clubMusicData.eActiveDJ
	IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
		eCurrentDJ = g_clubMusicData.eNextDJ
	ENDIF
	
	// DJs
	SWITCH eCurrentDJ
		CASE CLUB_DJ_KEINEMUSIK_BEACH_PARTY
			SWITCH iPed
				CASE 0
					SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
				BREAK	
				CASE 1
					SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
				BREAK
				CASE 2
					SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_LEFT_WRIST, 0)
					SET_PED_PROP_INDEX(PedID, ANCHOR_RIGHT_WRIST, 0)
				BREAK
			ENDSWITCH 
		BREAK
	ENDSWITCH
	
	// Network Peds
	SWITCH iPed
		CASE 73	// Scott
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE 103	// Dave
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
		CASE 110	// El Rubio
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0)
		BREAK
	ENDSWITCH 
ENDPROC

PROC _SET_ISLAND_PED_ALT_MOVEMENT(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(PedID)
	
	SWITCH iPed
		CASE -1
		BREAK
		
//		CASE 0
//			SET_PED_ALTERNATE_MOVEMENT_ANIM(Data.PedID, AAT_IDLE, "random@security_van", "sec_idle")
//			SET_PED_ALTERNATE_MOVEMENT_ANIM(Data.PedID, AAT_WALK, "random@security_van", "sec_walk_calm")
//		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_ISLAND_PED_SPEECH_DATA_LAYOUT_0(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 63	// Party Guy
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.0
				SpeechData.fListenDistance					= 6.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 1
				SpeechData.iPedID							= 68	// Party Girl
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.0
				SpeechData.fListenDistance					= 6.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 2
				SpeechData.iPedID							= 70	// Party Couple Guy
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.0
				SpeechData.fListenDistance					= 6.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 3
				SpeechData.iPedID							= 73	// Scott
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.0
				SpeechData.fListenDistance					= 5.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 4
				SpeechData.iPedID							= 103	// Dave
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.0
				SpeechData.fListenDistance					= 6.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 5
				SpeechData.iPedID							= 110	// El Rubio
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.0
				SpeechData.fListenDistance					= 6.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 6
				SpeechData.iPedID							= 119	// Guard with clipboard
				SpeechData.fGreetSpeechDistance				= 4.0
				SpeechData.fByeSpeechDistance				= 6.0
				SpeechData.fListenDistance					= 8.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 7
				SpeechData.iPedID							= 120	// Guard north entrance
				SpeechData.fGreetSpeechDistance				= 5.0
				SpeechData.fByeSpeechDistance				= 7.0
				SpeechData.fListenDistance					= 10.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 8
				SpeechData.iPedID							= 121	// Guard east entrance
				SpeechData.fGreetSpeechDistance				= 5.0
				SpeechData.fByeSpeechDistance				= 7.0
				SpeechData.fListenDistance					= 10.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 9
				SpeechData.iPedID							= 122	// Guard at waterfront
				SpeechData.fGreetSpeechDistance				= 4.0
				SpeechData.fByeSpeechDistance				= 6.0
				SpeechData.fListenDistance					= 8.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_ISLAND_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_ISLAND_PED_SPEECH_DATA_LAYOUT_0(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL _CAN_ISLAND_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedSpeech)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF g_bDontCrossRunning
		PRINTLN("[AM_MP_PEDS] _CAN_ISLAND_PED_PLAY_SPEECH - Bail Reason: Player is playing Dont Cross The Line")
		RETURN FALSE
	ENDIF
	
	
	// Specific conditions
	SWITCH iPed
		CASE 0
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC PED_SPEECH _GET_ISLAND_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	PED_SPEECH eSpeech = PED_SPH_INVALID
	
	SWITCH iPed
		CASE 63	// Party Guy
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_CT_PARTY_GUY_I_AM_FUCKED							BREAK
				CASE 1 	eSpeech = PED_SPH_CT_PARTY_GUY_TAKE_A_PISS							BREAK
				CASE 2	eSpeech = PED_SPH_CT_PARTY_GUY_FUCK_YOU_BUDDY						BREAK
				CASE 3	eSpeech = PED_SPH_CT_PARTY_GUY_TOO_HARD_TOO_FAST					BREAK
				CASE 4	eSpeech = PED_SPH_CT_PARTY_GUY_AVERAGE_SHIT							BREAK
				CASE 5	eSpeech = PED_SPH_CT_PARTY_GUY_MUSIC_STARTED						BREAK
				CASE 6	eSpeech = PED_SPH_CT_PARTY_GUY_JUST_A_MINUTE						BREAK
				CASE 7	eSpeech = PED_SPH_CT_PARTY_GUY_TIME_OUT								BREAK
				CASE 8	eSpeech = PED_SPH_CT_PARTY_GUY_HE_OWNS_THE_PLACE					BREAK
				CASE 9	eSpeech = PED_SPH_CT_PARTY_GUY_I_AM_SO_HOT							BREAK
			ENDSWITCH
		BREAK
		CASE 68	// Party Girl
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_CT_PARTY_GIRL_IM_THERE_THOUGH						BREAK
				CASE 1 	eSpeech = PED_SPH_CT_PARTY_GIRL_ITS_INSANE							BREAK
				CASE 2	eSpeech = PED_SPH_CT_PARTY_GIRL_PRONTO								BREAK
			ENDSWITCH
		BREAK
		CASE 70	// Party Couple Guy
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_CT_PARTY_COUPLE_GUY_SAW_IT_WHEN_FLYING_IN			BREAK
				CASE 1 	eSpeech = PED_SPH_CT_PARTY_COUPLE_GUY_LIKE_REALLY					BREAK
				CASE 2	eSpeech = PED_SPH_CT_PARTY_COUPLE_GUY_LS_IN_NO_TIME					BREAK
			ENDSWITCH
		BREAK
		CASE 73	// Scott
			IF (ePedActivity = PED_ACT_BEACH_VIP_SCOTT_IG3)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER										BREAK
					CASE 3	eSpeech = PED_SPH_CT_WHATS_UP									BREAK
					CASE 4	eSpeech = PED_SPH_CT_SCOTT_IG3_DEFINITELY_NOT_GETTING			BREAK
					CASE 5	eSpeech = PED_SPH_CT_SCOTT_IG3_GOOD_JOB_MY_MANAGER				BREAK
					CASE 6	eSpeech = PED_SPH_CT_SCOTT_IG3_I_SHOULDDA_BROUGHT				BREAK
					CASE 7	eSpeech = PED_SPH_CT_SCOTT_IG3_SEEN_A_FEW_PRIVATE				BREAK
					CASE 8	eSpeech = PED_SPH_CT_SCOTT_IG3_SERIOUS_PLACE					BREAK
					CASE 9	eSpeech = PED_SPH_CT_SCOTT_IG3_THIS_RUBIO_GUY					BREAK
					CASE 10	eSpeech = PED_SPH_CT_SCOTT_IG3_YOU_KNOW_THE_OWNER				BREAK
					CASE 11	eSpeech = PED_SPH_CT_SCOTT_IG3_NICE_TUNES						BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_SCOTT_IG4)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER										BREAK
					CASE 3	eSpeech = PED_SPH_CT_WHATS_UP									BREAK
					CASE 4	eSpeech = PED_SPH_CT_SCOTT_IG4_AFTER_PARTY						BREAK
					CASE 5	eSpeech = PED_SPH_CT_SCOTT_IG4_BACK_IN_THE_GAME					BREAK
					CASE 6	eSpeech = PED_SPH_CT_SCOTT_IG4_BACK_WHERE_I_BELONG				BREAK
					CASE 7	eSpeech = PED_SPH_CT_SCOTT_IG4_DONT_WORRY_BABE					BREAK
					CASE 8	eSpeech = PED_SPH_CT_SCOTT_IG4_GO_AHEAD_GIRL					BREAK
					CASE 9	eSpeech = PED_SPH_CT_SCOTT_IG4_I_KNOW_YOU_GIRL					BREAK
					CASE 10	eSpeech = PED_SPH_CT_SCOTT_IG4_LIKE_I_NEVER_LEFT				BREAK
					CASE 11	eSpeech = PED_SPH_CT_SCOTT_IG4_THIS_IS_WHAT_A_COMEBACK			BREAK
					CASE 12	eSpeech = PED_SPH_CT_SCOTT_IG4_WHAT_HAPPENS_IN_PARADISE			BREAK
					CASE 13	eSpeech = PED_SPH_CT_SCOTT_IG4_YOU_SEE_HOW_HEAVY				BREAK
					CASE 14	eSpeech = PED_SPH_CT_SCOTT_IG4_YOU_WAIT_AND_SEE					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 103	// Dave
			IF (ePedActivity = PED_ACT_BEACH_VIP_DAVE_IG5)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER										BREAK
					CASE 3	eSpeech = PED_SPH_CT_WHATS_UP									BREAK
					CASE 4	eSpeech = PED_SPH_CT_DAVE_IG5_CAN_CAN_IN_HERE					BREAK
					CASE 5	eSpeech = PED_SPH_CT_DAVE_IG5_DAVEY_IS_THE_LIFE_AND_SOUL		BREAK
					CASE 6	eSpeech = PED_SPH_CT_DAVE_IG5_MAN_OF_THE_MATCH_RIGHT_HERE		BREAK
					CASE 7	eSpeech = PED_SPH_CT_DAVE_IG5_NICE_ONE_NICE_ONE					BREAK
					CASE 8	eSpeech = PED_SPH_CT_DAVE_IG5_NUDGE_NUDGE_WINK_WINK				BREAK
					CASE 9	eSpeech = PED_SPH_CT_DAVE_IG5_RIGHT_OUT_OF_THE_GATE				BREAK
					CASE 10	eSpeech = PED_SPH_CT_DAVE_IG5_RIGHT_UP_MY_ALLEY					BREAK
					CASE 11	eSpeech = PED_SPH_CT_DAVE_IG5_THIS_TUNE_HAS_THE_SAME_BPM		BREAK
					CASE 12	eSpeech = PED_SPH_CT_DAVE_IG5_WELLY_ON_THE_DECKS				BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_DAVE_IG6)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER										BREAK
					CASE 3	eSpeech = PED_SPH_CT_WHATS_UP									BREAK
					CASE 4	eSpeech = PED_SPH_CT_DAVE_IG6_I_KNOW_WHAT_YOURE_THINKING		BREAK
					CASE 5	eSpeech = PED_SPH_CT_DAVE_IG6_I_LOST_IT							BREAK
					CASE 6	eSpeech = PED_SPH_CT_DAVE_IG6_IS_IT_ME_OR_IS_IT_GETTING_FRUITY	BREAK
					CASE 7	eSpeech = PED_SPH_CT_DAVE_IG6_LEAN_INTO_IT						BREAK
					CASE 8	eSpeech = PED_SPH_CT_DAVE_IG6_ONLY_A_LITTLE_KERFUFFLE			BREAK
					CASE 9	eSpeech = PED_SPH_CT_DAVE_IG6_SKEW_WHIFF_COMPADRE				BREAK
					CASE 10	eSpeech = PED_SPH_CT_DAVE_IG6_SOMEBODY_CHECK_IN					BREAK
					CASE 11	eSpeech = PED_SPH_CT_DAVE_IG6_WERE_YOU_GONE						BREAK
					CASE 12	eSpeech = PED_SPH_CT_DAVE_IG6_WOBBLY_AVOIDED					BREAK
					CASE 13	eSpeech = PED_SPH_CT_DAVE_IG6_WORD_THE_WISE						BREAK
					CASE 14	eSpeech = PED_SPH_CT_DAVE_IG6_YOURE_BACK						BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_DAVE_IG7)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER										BREAK
					CASE 3	eSpeech = PED_SPH_CT_WHATS_UP									BREAK
					CASE 4	eSpeech = PED_SPH_CT_DAVE_IG7_FULLY_PROJECTED					BREAK
					CASE 5	eSpeech = PED_SPH_CT_DAVE_IG7_I_AM_AT_ONE						BREAK
					CASE 6	eSpeech = PED_SPH_CT_DAVE_IG7_I_AM_HERBY_DISEMBALMED			BREAK
					CASE 7	eSpeech = PED_SPH_CT_DAVE_IG7_I_HAVE_MAPPED						BREAK
					CASE 8	eSpeech = PED_SPH_CT_DAVE_IG7_NAMASTE							BREAK
					CASE 9	eSpeech = PED_SPH_CT_DAVE_IG7_OMMMMMMMM							BREAK
					CASE 10	eSpeech = PED_SPH_CT_DAVE_IG7_ONE_LOVE							BREAK
					CASE 11	eSpeech = PED_SPH_CT_DAVE_IG7_PERFECT_ALIGNMENT					BREAK
					CASE 12	eSpeech = PED_SPH_CT_DAVE_IG7_SUIVEZ_MOI						BREAK
					CASE 13	eSpeech = PED_SPH_CT_DAVE_IG7_THOSE_WERE_MY_LAST_WORDS			BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 110	// El Rubio
			IF (ePedActivity = PED_ACT_BEACH_VIP_ELRUBIO_IG1)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_CT_ELRUBIO_IG1_EMPECEMOS_CON_ESTO				BREAK
					CASE 3	eSpeech = PED_SPH_CT_ELRUBIO_IG1_MANOS_A_LA_OBRA				BREAK
					CASE 4	eSpeech = PED_SPH_CT_ELRUBIO_IG1_MUY_BIEN						BREAK
					CASE 5	eSpeech = PED_SPH_CT_ELRUBIO_IG1_VAMONOS						BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_ELRUBIO_IG2)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING									BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE										BREAK
					CASE 2	eSpeech = PED_SPH_CT_ELRUBIO_IG2_STAY_AS_LONG_AS_YOU_LIKE		BREAK
					CASE 3	eSpeech = PED_SPH_CT_ELRUBIO_IG2_DAVEY_IS_ENJOYING_HIMSELF		BREAK
					CASE 4	eSpeech = PED_SPH_CT_ELRUBIO_IG2_GO_ON_ENJOY_YOURSELF			BREAK
					CASE 5	eSpeech = PED_SPH_CT_ELRUBIO_IG2_INCREDIBLE_SET_RIGHT			BREAK
					CASE 6	eSpeech = PED_SPH_CT_ELRUBIO_IG2_I_WILL_GET_BACK_ON_THE_FLOOR	BREAK
					CASE 7	eSpeech = PED_SPH_CT_ELRUBIO_IG2_JUST_TAKING_A_BREAK			BREAK
					CASE 8	eSpeech = PED_SPH_CT_ELRUBIO_IG2_NEVER_SEEN_ANYTHING_LIKE_IT	BREAK
					CASE 9	eSpeech = PED_SPH_CT_ELRUBIO_IG2_YOU_HAVING_FUN					BREAK
					CASE 10	eSpeech = PED_SPH_CT_ELRUBIO_IG2_YOU_NEED_ANYTHING				BREAK
					CASE 11	eSpeech = PED_SPH_CT_ELRUBIO_IG2_YOU_WOULDNT_WANT_TO_MISS		BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 119	// Guard with clipboard
		CASE 122	// Guard at waterfront
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2 	eSpeech = PED_SPH_PT_BUMP											BREAK
			ENDSWITCH
		BREAK
		CASE 120	// Guard north entrance
		CASE 121	// Guard east entrance
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2 	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3 	eSpeech = PED_SPH_PT_BEACH_GUARD									BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN eSpeech
ENDFUNC

PROC _GET_ISLAND_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	RESET_PED_CONVO_DATA(convoData)
	
	SWITCH iPed
		CASE 63	// Party Guy
			convoData.iSpeakerID						= 7
			convoData.sCharacterVoice					= "HS4_PARTYGUY1"
			convoData.sSubtitleTextBlock				= "HS4BPAU"
			SWITCH ePedSpeech
				CASE PED_SPH_CT_PARTY_GUY_I_AM_FUCKED
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_A"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_TAKE_A_PISS
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_B"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_FUCK_YOU_BUDDY
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_C"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_TOO_HARD_TOO_FAST
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_D"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_AVERAGE_SHIT
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_E"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_MUSIC_STARTED
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_F"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_JUST_A_MINUTE
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_G"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_TIME_OUT
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_H"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_HE_OWNS_THE_PLACE
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_I"
				BREAK
				CASE PED_SPH_CT_PARTY_GUY_I_AM_SO_HOT
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG8_J"
				BREAK
			ENDSWITCH
		BREAK
		CASE 68	// Party Girl
			convoData.iSpeakerID						= 4
			convoData.sCharacterVoice					= "HS4_PARTYGIRL4"
			convoData.sSubtitleTextBlock				= "HS4BPAU"
			SWITCH ePedSpeech
				CASE PED_SPH_CT_PARTY_GIRL_IM_THERE_THOUGH
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG9_A"
				BREAK
				CASE PED_SPH_CT_PARTY_GIRL_PRONTO
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG9_B"
				BREAK
				CASE PED_SPH_CT_PARTY_GIRL_ITS_INSANE
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG9_C"
				BREAK
			ENDSWITCH
		BREAK
		CASE 70	// Party Couple Guy
			convoData.iSpeakerID						= 5
			convoData.sCharacterVoice					= "HS4_PARTYCOUPLEM1"
			convoData.sSubtitleTextBlock				= "HS4BPAU"
			convoData.bAddChildPedsToConvo				= TRUE
			SWITCH ePedSpeech
				CASE PED_SPH_CT_PARTY_COUPLE_GUY_LS_IN_NO_TIME
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG10_A"
				BREAK
				CASE PED_SPH_CT_PARTY_COUPLE_GUY_LIKE_REALLY
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG10_B"
				BREAK
				CASE PED_SPH_CT_PARTY_COUPLE_GUY_SAW_IT_WHEN_FLYING_IN
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG10_C"
				BREAK
			ENDSWITCH
		BREAK
		CASE 72	// Party Couple Girl
			convoData.iSpeakerID						= 6
			convoData.sCharacterVoice					= "HS4_PARTYCOUPLEF1"
			convoData.sSubtitleTextBlock				= "HS4BPAU"
			SWITCH ePedSpeech
				CASE PED_SPH_CT_PARTY_COUPLE_GUY_LS_IN_NO_TIME
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG10_A"
				BREAK
				CASE PED_SPH_CT_PARTY_COUPLE_GUY_LIKE_REALLY
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG10_B"
				BREAK
				CASE PED_SPH_CT_PARTY_COUPLE_GUY_SAW_IT_WHEN_FLYING_IN
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "HS4BP_IG10_C"
				BREAK
			ENDSWITCH
		BREAK
		CASE 73	// Scott
			convoData.iSpeakerID						= 8
			convoData.sCharacterVoice					= "HS4_CELEB1"
			convoData.sSubtitleTextBlock				= "HS4BPAU"
			IF (ePedActivity = PED_ACT_BEACH_VIP_SCOTT_IG3)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_GOOD_JOB_MY_MANAGER
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_A"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_YOU_KNOW_THE_OWNER
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_B"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_SERIOUS_PLACE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_C"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_DEFINITELY_NOT_GETTING
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_D"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_NICE_TUNES
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_E"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_SEEN_A_FEW_PRIVATE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_F"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_THIS_RUBIO_GUY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_G"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG3_I_SHOULDDA_BROUGHT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG3_H"
					BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_SCOTT_IG4)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_WHAT_HAPPENS_IN_PARADISE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_A"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_THIS_IS_WHAT_A_COMEBACK
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_B"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_YOU_SEE_HOW_HEAVY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_C"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_BACK_IN_THE_GAME
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_D"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_LIKE_I_NEVER_LEFT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_E"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_I_KNOW_YOU_GIRL
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_F"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_YOU_WAIT_AND_SEE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_G"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_DONT_WORRY_BABE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_H"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_BACK_WHERE_I_BELONG
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_I"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_GO_AHEAD_GIRL
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_J"
					BREAK
					CASE PED_SPH_CT_SCOTT_IG4_AFTER_PARTY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG4_K"
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 103	// Dave
			convoData.iSpeakerID							= 3
			convoData.sCharacterVoice						= "BTL_DAVE"
			convoData.sSubtitleTextBlock					= "HS4BPAU"
			IF (ePedActivity = PED_ACT_BEACH_VIP_DAVE_IG5)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_NICE_ONE_NICE_ONE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_A"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_DAVEY_IS_THE_LIFE_AND_SOUL
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_B"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_WELLY_ON_THE_DECKS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_C"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_RIGHT_UP_MY_ALLEY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_D"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_CAN_CAN_IN_HERE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_E"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_RIGHT_OUT_OF_THE_GATE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_F"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_MAN_OF_THE_MATCH_RIGHT_HERE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_G"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_NUDGE_NUDGE_WINK_WINK
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_H"
					BREAK
					CASE PED_SPH_CT_DAVE_IG5_THIS_TUNE_HAS_THE_SAME_BPM
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG5_I"
					BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_DAVE_IG6)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_SKEW_WHIFF_COMPADRE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_A"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_ONLY_A_LITTLE_KERFUFFLE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_B"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_I_LOST_IT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_C"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_WOBBLY_AVOIDED
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_D"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_YOURE_BACK
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_E"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_I_KNOW_WHAT_YOURE_THINKING
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_F"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_WERE_YOU_GONE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_G"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_LEAN_INTO_IT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_H"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_IS_IT_ME_OR_IS_IT_GETTING_FRUITY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_I"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_SOMEBODY_CHECK_IN
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_J"
					BREAK
					CASE PED_SPH_CT_DAVE_IG6_WORD_THE_WISE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG6_K"
					BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_DAVE_IG7)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_I_AM_AT_ONE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_A"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_THOSE_WERE_MY_LAST_WORDS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_B"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_OMMMMMMMM
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_C"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_NAMASTE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_D"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_ONE_LOVE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_E"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_SUIVEZ_MOI
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_F"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_PERFECT_ALIGNMENT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_G"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_I_AM_HERBY_DISEMBALMED
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_H"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_FULLY_PROJECTED
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_I"
					BREAK
					CASE PED_SPH_CT_DAVE_IG7_I_HAVE_MAPPED
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG7_J"
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 110	// El Rubio
			convoData.iSpeakerID							= 2
			convoData.sCharacterVoice						= "HS4_ELRUBIO"
			convoData.sSubtitleTextBlock					= "HS4BPAU"
			IF (ePedActivity = PED_ACT_BEACH_VIP_ELRUBIO_IG1)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG1_EMPECEMOS_CON_ESTO
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG1_A"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG1_MANOS_A_LA_OBRA
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG1_B"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG1_MUY_BIEN
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG1_C"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG1_VAMONOS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG1_D"
					BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_BEACH_VIP_ELRUBIO_IG2)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName 				= "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName 				= "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName 				= "LOITER"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName 				= "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_GO_ON_ENJOY_YOURSELF
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_A"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_DAVEY_IS_ENJOYING_HIMSELF
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_B"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_INCREDIBLE_SET_RIGHT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_C"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_YOU_WOULDNT_WANT_TO_MISS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_D"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_YOU_HAVING_FUN
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_E"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_NEVER_SEEN_ANYTHING_LIKE_IT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_F"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_YOU_NEED_ANYTHING
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_G"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_STAY_AS_LONG_AS_YOU_LIKE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_H"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_I_WILL_GET_BACK_ON_THE_FLOOR
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_I"
					BREAK
					CASE PED_SPH_CT_ELRUBIO_IG2_JUST_TAKING_A_BREAK
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4BP_IG2_J"
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 119	// Guard with clipboard
		CASE 122	// Guard at waterfront
			convoData.sCharacterVoice						= "NULL"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					convoData.sRootName 					= "GENERIC_HI"
				BREAK
				CASE PED_SPH_PT_BYE
					// Need to include bye so that greeting bit is reset when leaving.
					// Leaving the root name blank so it doesn't actually play.
					convoData.sRootName 					= ""
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName 					= "BUMP"
				BREAK
			ENDSWITCH
		BREAK
		CASE 120	// Guard north entrance
		CASE 121	// Guard east entrance
			convoData.sCharacterVoice						= "NULL"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					convoData.sRootName 					= "GENERIC_HI"
				BREAK
				CASE PED_SPH_PT_BYE
					// Need to include bye so that greeting bit is reset when leaving.
					// Leaving the root name blank so it doesn't actually play.
					convoData.sRootName 					= ""
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName 					= "BUMP"
				BREAK
				CASE PED_SPH_PT_BEACH_GUARD
					convoData.sRootName 					= "BEACH_GUARD"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC PED_SPEECH _GET_ISLAND_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE -1
		BREAK
		
		DEFAULT
			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_ISLAND_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_ISLAND_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_ISLAND_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_ISLAND_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_ISLAND_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_ISLAND_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_ISLAND_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_ISLAND_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════════╡ AMBIENT ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_AMBIENT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_AMBIENT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ LEANING TEXTING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_LEANING_TEXT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_TEXTING_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_LEANING_TEXT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_FEMALE_WALL_BACK_TEXTING_IDLE_A"	BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_LEANING_SMOKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_SMOKING_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_LEANING_SMOKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_SMOKING_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════════╡ HANGOUT ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_HANGOUT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_HANG_OUT_STREET_MALE_C_BASE"				BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_HANGOUT_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_HANG_OUT_STREET_FEMALE_HOLD_ARM_IDLE_B"	BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ SMOKING WEED ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SMOKING_WEED_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_SMOKING_POT_MALE_BASE"						BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_SMOKING_WEED_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_A"													BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════════╡ PHONE ╞═══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_PHONE_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_LEANING_MALE_WALL_BACK_MOBILE_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_PHONE_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"													BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ MUSCLE FLEX ╞════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_MUSCLE_FLEX_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_MUSCLE_FLEX_ARMS_IN_FRONT_BASE"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ CHECK OUT MIRROR ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_CHECK_OUT_MIRROR_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_WINDOW_SHOP_FEMALE_IDLE_A_BROWSE_B"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════════╡ DRINKING ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_DRINKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_DRINKING_BEER_MALE_BASE"					BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DRINKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_WORLD_HUMAN_DRINKING_BEER_FEMALE_BASE"					BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SITTING DRINKING ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SITTING_DRINKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_A"			BREAK
		CASE 1	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_B"			BREAK
		CASE 2	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_MALE_IDLE_C"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_SITTING_DRINKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_FEMALE_IDLE_B"		BREAK
		CASE 1	sAnimClip = "AMB_PROP_HUMAN_SEAT_CHAIR_DRINK_BEER_FEMALE_IDLE_C"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC


//╒═══════════════════════════════╕
//╞══════╡ SITTING AT BAR ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SITTING_AT_BAR_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_BAR_MALE_ELBOWS_ON_BAR_IDLE_A"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_SITTING_AT_BAR_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_BAR_MALE_ELBOWS_ON_BAR_IDLE_C"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_SITTING_AT_BAR_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "AMB_PROP_HUMAN_SEAT_BAR_FEMALE_ELBOWS_ON_BAR_IDLE_A"		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_SITTING_AT_BAR_F_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "IDLE_B"													BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════╡ GUARD STANDING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_GUARD_STANDING_ANIM_DICT(INT iClip)
	STRING sAnimDict = ""
	SWITCH iClip
		CASE 0	sAnimDict = "AMB@WORLD_HUMAN_STAND_GUARD@MALE@BASE"		BREAK
		CASE 1	sAnimDict = "AMB@WORLD_HUMAN_STAND_GUARD@MALE@IDLE_A"	BREAK
		CASE 2	sAnimDict = "AMB@WORLD_HUMAN_STAND_GUARD@MALE@IDLE_A"	BREAK
		CASE 3	sAnimDict = "AMB@WORLD_HUMAN_STAND_GUARD@MALE@IDLE_A"	BREAK
		CASE 4	sAnimDict = "AMB@WORLD_HUMAN_STAND_GUARD@MALE@IDLE_B"	BREAK
	ENDSWITCH
	RETURN sAnimDict
ENDFUNC

FUNC STRING GET_ISLAND_GUARD_STANDING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"		pedAnimData.bRemoveAnimDict = TRUE	pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A"	pedAnimData.bRemoveAnimDict = TRUE 	BREAK
		CASE 2	sAnimClip = "IDLE_B"	pedAnimData.bRemoveAnimDict = TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C"	pedAnimData.bRemoveAnimDict = TRUE	BREAK
		CASE 4	sAnimClip = "IDLE_D"	pedAnimData.bRemoveAnimDict = TRUE	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ SUNBATHING BACK M 01╞════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SUNBATHING_BACK_M_01_ANIM_DICT(INT iClip)
	STRING sAnimDict = ""
	SWITCH iClip
		CASE 0	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@BACK@BASE"	BREAK
		CASE 1	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@BACK@IDLE_A"	BREAK
		CASE 2	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@BACK@IDLE_A"	BREAK
		CASE 3	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@BACK@IDLE_A"	BREAK
	ENDSWITCH
	RETURN sAnimDict
ENDFUNC

FUNC STRING GET_ISLAND_SUNBATHING_BACK_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"		pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 1	sAnimClip = "IDLE_A"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 2	sAnimClip = "IDLE_B"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 3	sAnimClip = "IDLE_C"	pedAnimData.bRemoveAnimDict = TRUE BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ SUNBATHING BACK F 01╞════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SUNBATHING_BACK_F_01_ANIM_DICT(INT iClip)
	STRING sAnimDict = ""
	SWITCH iClip
		CASE 0	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@BACK@BASE"		BREAK
		CASE 1	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@BACK@IDLE_A"	BREAK
		CASE 2	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@BACK@IDLE_A"	BREAK
		CASE 3	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@BACK@IDLE_A"	BREAK
	ENDSWITCH
	RETURN sAnimDict
ENDFUNC

FUNC STRING GET_ISLAND_SUNBATHING_BACK_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"		pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 1	sAnimClip = "IDLE_A"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 2	sAnimClip = "IDLE_B"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 3	sAnimClip = "IDLE_C"	pedAnimData.bRemoveAnimDict = TRUE BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ SUNBATHING FRONT M 01╞═══╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SUNBATHING_FRONT_M_01_ANIM_DICT(INT iClip)
	STRING sAnimDict = ""
	SWITCH iClip
		CASE 0	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@FRONT@BASE"		BREAK
		CASE 1	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@FRONT@IDLE_A"	BREAK
		CASE 2	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@FRONT@IDLE_A"	BREAK
		CASE 3	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@MALE@FRONT@IDLE_A"	BREAK
	ENDSWITCH
	RETURN sAnimDict
ENDFUNC

FUNC STRING GET_ISLAND_SUNBATHING_FRONT_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"		pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 1	sAnimClip = "IDLE_A"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 2	sAnimClip = "IDLE_B"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 3	sAnimClip = "IDLE_C"	pedAnimData.bRemoveAnimDict = TRUE BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ SUNBATHING FRONT F 01╞═══╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_SUNBATHING_FRONT_F_01_ANIM_DICT(INT iClip)
	STRING sAnimDict = ""
	SWITCH iClip
		CASE 0	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@FRONT@BASE"	BREAK
		CASE 1	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@FRONT@IDLE_A"	BREAK
		CASE 2	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@FRONT@IDLE_A"	BREAK
		CASE 3	sAnimDict = "AMB@WORLD_HUMAN_SUNBATHE@FEMALE@FRONT@IDLE_A"	BREAK
	ENDSWITCH
	RETURN sAnimDict
ENDFUNC

FUNC STRING GET_ISLAND_SUNBATHING_FRONT_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"		pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 1	sAnimClip = "IDLE_A"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 2	sAnimClip = "IDLE_B"	pedAnimData.bRemoveAnimDict = TRUE BREAK
		CASE 3	sAnimClip = "IDLE_C"	pedAnimData.bRemoveAnimDict = TRUE BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ VIP BAR HANGOUT M 01 ╞═══╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_VIP_BAR_HANGOUT_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_HANGOUT_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_HANGOUT_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ VIP BAR DRINKING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_VIP_BAR_DRINKING_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_MALE_A_BASE"			BREAK
		CASE 1	sAnimClip = "BAR_DRINK_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_VIP_BAR_DRINKING_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_MALE_B_BASE"			BREAK
		CASE 1	sAnimClip = "BAR_DRINK_MALE_B_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_MALE_B_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_MALE_B_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_MALE_B_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_VIP_BAR_DRINKING_M_03_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_MALE_C_BASE"			BREAK
		CASE 1	sAnimClip = "BAR_DRINK_MALE_C_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_MALE_C_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_MALE_C_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_MALE_C_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_VIP_BAR_DRINKING_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_FEMALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_VIP_BAR_DRINKING_F_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "BAR_DRINK_FEMALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_A"		BREAK
		CASE 2	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_B"		BREAK
		CASE 3	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_C"		BREAK
		CASE 4	sAnimClip = "BAR_DRINK_FEMALE_B_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH PARTY STAND ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_MALE_A_BASE"				BREAK
		CASE 1	sAnimClip = "STAND_MALE_A_IDLE_A"			BREAK
		CASE 2	sAnimClip = "STAND_MALE_A_IDLE_B"			BREAK
		CASE 3	sAnimClip = "STAND_MALE_A_IDLE_C"			BREAK
		CASE 4	sAnimClip = "STAND_MALE_A_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_STAND_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_MALE_B_BASE"				BREAK
		CASE 1	sAnimClip = "STAND_MALE_B_IDLE_A"			BREAK
		CASE 2	sAnimClip = "STAND_MALE_B_IDLE_B"			BREAK
		CASE 3	sAnimClip = "STAND_MALE_B_IDLE_C"			BREAK
		CASE 4	sAnimClip = "STAND_MALE_B_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_STAND_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_FEMALE_A_BASE"			BREAK
		CASE 1	sAnimClip = "STAND_FEMALE_A_IDLE_A"			BREAK
		CASE 2	sAnimClip = "STAND_FEMALE_A_IDLE_B"			BREAK
		CASE 3	sAnimClip = "STAND_FEMALE_A_IDLE_C"			BREAK
		CASE 4	sAnimClip = "STAND_FEMALE_A_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH PARTY LEAN ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_LEAN_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_MALE_A_BASE"				BREAK
		CASE 1	sAnimClip = "LEAN_MALE_A_IDLE_A"			BREAK
		CASE 2	sAnimClip = "LEAN_MALE_A_IDLE_B"			BREAK
		CASE 3	sAnimClip = "LEAN_MALE_A_IDLE_C"			BREAK
		CASE 4	sAnimClip = "LEAN_MALE_A_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_LEAN_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_MALE_B_BASE"				BREAK
		CASE 1	sAnimClip = "LEAN_MALE_B_IDLE_A"			BREAK
		CASE 2	sAnimClip = "LEAN_MALE_B_IDLE_B"			BREAK
		CASE 3	sAnimClip = "LEAN_MALE_B_IDLE_C"			BREAK
		CASE 4	sAnimClip = "LEAN_MALE_B_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_LEAN_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_FEMALE_A_BASE"			BREAK
		CASE 1	sAnimClip = "LEAN_FEMALE_A_IDLE_A"			BREAK
		CASE 2	sAnimClip = "LEAN_FEMALE_A_IDLE_B"			BREAK
		CASE 3	sAnimClip = "LEAN_FEMALE_A_IDLE_C"			BREAK
		CASE 4	sAnimClip = "LEAN_FEMALE_A_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ BEACH PARTY SEATED ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_SEATED_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_MALE_A_BASE"			BREAK
		CASE 1	sAnimClip = "SEATED_MALE_A_IDLE_A"			BREAK
		CASE 2	sAnimClip = "SEATED_MALE_A_IDLE_B"			BREAK
		CASE 3	sAnimClip = "SEATED_MALE_A_IDLE_C"			BREAK
		CASE 4	sAnimClip = "SEATED_MALE_A_IDLE_D"			BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_SEATED_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_FEMALE_A_BASE"			BREAK
		CASE 1	sAnimClip = "SEATED_FEMALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "SEATED_FEMALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "SEATED_FEMALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "SEATED_FEMALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞═╡ BEACH PARTY SMOKE WEED STAND ╞═╡
//╘══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_SMOKE_WEED_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_SMOKE_WEED_MALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_STAND_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_SMOKE_WEED_MALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_SMOKE_WEED_MALE_B_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_STAND_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_BASE"	BREAK
		CASE 1	sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_SMOKE_WEED_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═╡BEACH PARTY SMOKE WEED LEAN ╞═╡
//╘════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_LEAN_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_SMOKE_WEED_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "LEAN_SMOKE_WEED_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_LEAN_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "LEAN_SMOKE_WEED_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════════╕
//╞═╡ BEACH PARTY SMOKE WEED SEATED ╞═╡
//╘═══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_SEATED_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_SMOKE_WEED_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_SMOKE_WEED_MALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_SEATED_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_SMOKE_WEED_MALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_SMOKE_WEED_MALE_B_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_SMOKE_WEED_SEATED_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_BASE"	BREAK
		CASE 1	sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_SMOKE_WEED_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞═╡ BEACH PARTY DRINK CUP SEATED ╞═╡
//╘══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_DRINK_CUP_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "STAND_DRINK_CUP_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_STAND_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_DRINK_CUP_MALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_A"		BREAK
		CASE 2	sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_B"		BREAK
		CASE 3	sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_C"		BREAK
		CASE 4	sAnimClip = "STAND_DRINK_CUP_MALE_B_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_STAND_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞══╡ BEACH PARTY DRINK CUP LEAN ╞══╡
//╘══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_LEAN_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_DRINK_CUP_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "LEAN_DRINK_CUP_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_LEAN_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "LEAN_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞═╡ BEACH PARTY DRINK CUP SEATED ╞═╡
//╘══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_SEATED_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_DRINK_CUP_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_DRINK_CUP_MALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_SEATED_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_DRINK_CUP_MALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_DRINK_CUP_MALE_B_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_DRINK_CUP_SEATED_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_BASE"	BREAK
		CASE 1	sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_DRINK_CUP_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════════╕
//╞═╡ BEACH PARTY CELL PHONE STAND ╞═╡
//╘══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_STAND_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_CELL_PHONE_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_CELL_PHONE_MALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_STAND_M_02_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_CELL_PHONE_MALE_B_BASE"		BREAK
		CASE 1	sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_CELL_PHONE_MALE_B_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_STAND_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_BASE"	BREAK
		CASE 1	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "STAND_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═════════════════════════════════╕
//╞═╡ BEACH PARTY CELL PHONE LEAN ╞═╡
//╘═════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_LEAN_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_CELL_PHONE_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_A"		BREAK
		CASE 2	sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_B"		BREAK
		CASE 3	sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_C"		BREAK
		CASE 4	sAnimClip = "LEAN_CELL_PHONE_MALE_A_IDLE_D"		BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_LEAN_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "LEAN_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════════╕
//╞═╡ BEACH PARTY CELL PHONE SEATED ╞═╡
//╘═══════════════════════════════════╛

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_SEATED_M_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_CELL_PHONE_MALE_A_BASE"		BREAK
		CASE 1	sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_CELL_PHONE_MALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_BEACH_PARTY_CELL_PHONE_SEATED_F_01_ANIM_CLIP(INT iClip)
	STRING sAnimClip = ""
	SWITCH iClip
		CASE 0	sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_BASE"	BREAK
		CASE 1	sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_A"	BREAK
		CASE 2	sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_B"	BREAK
		CASE 3	sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_C"	BREAK
		CASE 4	sAnimClip = "SEATED_CELL_PHONE_FEMALE_A_IDLE_D"	BREAK
	ENDSWITCH
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ AMBIENT DANCING ╞══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CLUB_AMBIENTPEDS@"
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^2"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^2"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^3"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^3"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^4"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^4"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^5"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^5"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT M 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_MALE^6"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_MALE^6"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_M_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ AMBIENT F 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_LI-MI_TO_MI-HI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_MI-HI_TO_LI-MI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_AMBIENT_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "LI-MI_AMB_CLUB_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "LI-MI_AMB_CLUB_10_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "LI-MI_AMB_CLUB_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "LI-MI_AMB_CLUB_12_V1_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "LI-MI_AMB_CLUB_13_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI-HI_AMB_CLUB_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "MI-HI_AMB_CLUB_10_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "MI-HI_AMB_CLUB_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "MI-HI_AMB_CLUB_12_V1_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "MI-HI_AMB_CLUB_13_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_AMBIENT_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_AMBIENT_F_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════════╡ FACE DJ ╞══════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(BOOL bDancingTransition = FALSE)
	STRING sAnimDict = ""
	
	IF (bDancingTransition)
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_FACEDJ_TRANSITIONS@"
	ELSE
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_FACEDJ@"
	ENDIF
	
	RETURN sAnimDict
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^1"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^1"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^1"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^1"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^1"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^1"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^1"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^1"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^1"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^1"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^1"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^1"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^1"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^1"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^1"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^1"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^1"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^1"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^1"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^1"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^1"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^1"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^1"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^1"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^1"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^1"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^1"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^1"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^1"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^1"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^2"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^2"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^2"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^2"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^2"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^2"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^2"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^2"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^2"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^2"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^2"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^2"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^2"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^2"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^2"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^2"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^2"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^2"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^2"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^2"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^2"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^2"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^2"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^2"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^2"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^2"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^2"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^2"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^2"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^2"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^3"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^3"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^3"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^3"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^3"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^3"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^3"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^3"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^3"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^3"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^3"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^3"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^3"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^3"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^3"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^3"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^3"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^3"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^3"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^3"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^3"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^3"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^3"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^3"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^3"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^3"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^3"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^3"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^3"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^3"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^3"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^3"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^4"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^4"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^4"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^4"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^4"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^4"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^4"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^4"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^4"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^4"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^4"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^4"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^4"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^4"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^4"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^4"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^4"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^4"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^4"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^4"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^4"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^4"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^4"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^4"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^4"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^4"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^4"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^4"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^4"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^4"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^4"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^4"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^5"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^5"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^5"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^5"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^5"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^5"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^5"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^5"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^5"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^5"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^5"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^5"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^5"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^5"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^5"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^5"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^5"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^5"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^5"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^5"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^5"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^5"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^5"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^5"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^5"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^5"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^5"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^5"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^5"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^5"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^5"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^5"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ M 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_MALE^6"		BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_MALE^6"		BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_MALE^6"		BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_MALE^6"		BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_MALE^6"		BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_MALE^6"		BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_MALE^6"		BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_MALE^6"		BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_MALE^6"		BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_MALE^6"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_MALE^6"		BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_MALE^6"		BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_MALE^6"		BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_MALE^6"		BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_MALE^6"		BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_MALE^6"		BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_MALE^6"		BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_MALE^6"		BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_MALE^6"		BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_MALE^6"		BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_MALE^6"		BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_MALE^6"		BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_MALE^6"		BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_MALE^6"		BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_MALE^6"		BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_MALE^6"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_MALE^6"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Male^6"		BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Male^6"		BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Male^6"		BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Male^6"		BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Male^6"		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_M_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 01 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^1"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^1"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^1"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^1"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 02 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^2"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^2"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^2"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^2"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 03 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^3"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^3"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^3"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^3"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^3"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^3"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^3"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^3"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^3"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^3"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^3"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^3"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^3"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^3"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 04 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^4"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^4"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^4"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^4"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^4"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^4"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^4"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^4"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^4"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^4"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^4"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^4"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^4"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^4"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 05 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^5"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^5"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^5"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^5"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^5"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^5"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^5"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^5"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^5"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^5"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^5"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^5"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^5"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^5"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ FACE DJ F 06 ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_09_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_MI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_07_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_LI_TO_HI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_07_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_LI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_08_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_MI_TO_HI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_07_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_LI_09_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_09_V1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_FACEDJ_HI_TO_MI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_FACE_DJ_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_FACEDJ_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_FACEDJ_09_V2_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_FACEDJ_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_FACEDJ_11_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_FACEDJ_13_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_FACEDJ_13_V2_FEMALE^6"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_FACEDJ_15_V1_FEMALE^6"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_FACEDJ_15_V2_FEMALE^6"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_FACEDJ_17_V1_FEMALE^6"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_FACEDJ_17_V2_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_FACEDJ_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_FACEDJ_09_V2_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_FACEDJ_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_FACEDJ_13_V1_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_FACEDJ_13_V2_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_FACEDJ_15_V1_FEMALE^6"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_FACEDJ_15_V2_FEMALE^6"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_FACEDJ_17_V1_FEMALE^6"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_FACEDJ_17_V2_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_FACEDJ_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_FACEDJ_09_V2_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_FACEDJ_11_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_FACEDJ_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_FACEDJ_17_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_FACEDJ_17_V2_FEMALE^6"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_FACEDJ_D_11_V2_FEMALE^6"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_FACEDJ_HU_13_V1_FEMALE^6"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_FACEDJ_HU_15_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_Dance_FaceDJ_HU_09_Female^6"	BREAK
				CASE 1	sAnimClip = "HI_Dance_FaceDJ_HU_11_Female^6"	BREAK
				CASE 2	sAnimClip = "HI_Dance_FaceDJ_HU_13_Female^6"	BREAK
				CASE 3	sAnimClip = "HI_Dance_FaceDJ_HU_15_Female^6"	BREAK
				CASE 4	sAnimClip = "HI_Dance_FaceDJ_HU_17_Female^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_FACE_DJ_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_FACE_DJ_F_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════════╡ SINGLE PROP ╞════════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(BOOL bDancingTransition = FALSE)
	STRING sAnimDict = ""
	
	IF (bDancingTransition)
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_SINGLE_PROPS_TRANSITIONS@"
	ELSE
		sAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_SINGLE_PROPS@"
	ENDIF
	
	RETURN sAnimDict
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 02	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 03	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^3"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^3"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^3"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^3"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^3"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^3"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^3"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^3"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^3"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 04	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^4"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^4"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^4"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^4"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^4"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^4"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^4"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^4"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^4"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 05	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^5"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^5"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^5"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^5"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^5"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^5"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^5"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^5"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^5"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP M 06	╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_V1_MALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_V1_MALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_MALE^6"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_MALE^6"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_MALE^6"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_MALE^6"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_MALE^6"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_MALE^6"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_MALE^6"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_MALE^6"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_MALE^6"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_MALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 02 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 03 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^3"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^3"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^3"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^3"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^3"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^3"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^3"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^3"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 04 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^4"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^4"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^4"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^4"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^4"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^4"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^4"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^4"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 05 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^5"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^5"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^5"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^5"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^5"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^5"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^5"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^5"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══════╡ SINGLE PROP F 06 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_06_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_MI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_LI_TO_HI_07_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_LI_11_v1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_MI_TO_HI_11_V1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_LI_09_v1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_09_v1_FEMALE^6"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_PROP_HI_TO_MI_11_v1_FEMALE^6"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_PROP_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_PROP_11_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_PROP_13_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_PROP_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_PROP_15_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_PROP_17_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_PROP_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_PROP_11_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_PROP_13_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_PROP_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_PROP_15_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_PROP_17_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_PROP_09_V1_FEMALE^6"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_PROP_11_V1_FEMALE^6"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_PROP_13_V1_FEMALE^6"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_PROP_13_V2_FEMALE^6"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_PROP_15_V1_FEMALE^6"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_PROP_17_V1_FEMALE^6"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_06_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP A DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPA_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPA_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPA_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPA_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPA_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP A DANCING CHILD 01 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPA_M_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "LI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPA_M_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP B DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPB_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPB_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPB_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPB_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPB_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═╡ GROUP B DANCING CHILD 01 ╞═╡
//╘══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPB_F_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPB_F_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP C DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPC_M_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPC_M_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPC_M_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPC_M_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPC_M_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═╡ GROUP C DANCING CHILD 01 ╞═╡
//╘══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPC_M_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^2"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v2_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_07_v1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPC_M_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP D DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPD_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPD_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPD_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPD_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPD_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP D DANCING CHILD 01 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPD_M_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPD_M_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP D DANCING CHILD 02 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPD_M_CHILD_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE_^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE_^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE_^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE_^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE_^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE_^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE_^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE_^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE_^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE_^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE_^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE_^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE_^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE_^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE_^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE_^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE_^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE_^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE_^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE_^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE_^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE_^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE_^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE_^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE_^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE_^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE_^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE_^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE_^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE_^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE_^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPD_M_CHILD_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ GROUP E DANCING PARENT ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPE_F_PARENT_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v2_FEMALE^1"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPE_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPE_F_PARENT_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPE_F_PARENT_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPE_F_PARENT_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP E DANCING CHILD 01 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPE_F_CHILD_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v2_FEMALE^2"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_FEMALE^2"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_FEMALE^2"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_FEMALE^2"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_FEMALE^2"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_FEMALE^2"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_FEMALE^2"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_FEMALE^2"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_FEMALE^2"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_FEMALE^2"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_FEMALE^2"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_FEMALE^2"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_FEMALE^2"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPE_F_CHILD_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞══╡ GROUP E DANCING CHILD 02 ╞══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_GROUPE_M_CHILD_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_09_v2_MALE^1"	BREAK
						CASE 2	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_MI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_LI_TO_HI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_09_v2_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_LI_11_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_MI_TO_HI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_TRANCE
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_LI_09_v1_MALE^1"	BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v1_MALE^1"	BREAK
						CASE 1	sAnimClip = "TRANS_DANCE_CROWD_HI_TO_MI_09_v2_MALE^1"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "LI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "LI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "LI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "LI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "LI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "LI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "LI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "LI_DANCE_CROWD_17_V1_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "MI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "MI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "MI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "MI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "MI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "MI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "MI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "MI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "MI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_DANCE_CROWD_09_V1_MALE^1"	BREAK
				CASE 1	sAnimClip = "HI_DANCE_CROWD_09_V2_MALE^1"	BREAK
				CASE 2	sAnimClip = "HI_DANCE_CROWD_11_V1_MALE^1"	BREAK
				CASE 3	sAnimClip = "HI_DANCE_CROWD_11_V2_MALE^1"	BREAK
				CASE 4	sAnimClip = "HI_DANCE_CROWD_13_V1_MALE^1"	BREAK
				CASE 5	sAnimClip = "HI_DANCE_CROWD_13_V2_MALE^1"	BREAK
				CASE 6	sAnimClip = "HI_DANCE_CROWD_15_V1_MALE^1"	BREAK
				CASE 7	sAnimClip = "HI_DANCE_CROWD_15_V2_MALE^1"	BREAK
				CASE 8	sAnimClip = "HI_DANCE_CROWD_17_V1_MALE^1"	BREAK
				CASE 9	sAnimClip = "HI_DANCE_CROWD_17_V2_MALE^1"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_GROUPE_M_CHILD_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════╡ BEACH DANCING ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_DANCING_BEACH_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@BEACHDANCE@"
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 01 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M01"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M01"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M01"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M01"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 02 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M02"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M02"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M02"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M02"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M02"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M02"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M02"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 03 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_03_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M03"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M03"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M03"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M03"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M03"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M03"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M03"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M03"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M03"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M03"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M03"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M03"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M03"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M03"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M03"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_03_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 04 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_04_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M04"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M04"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M04"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M04"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M04"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M04"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M04"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M04"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M04"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M04"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M04"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M04"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M04"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M04"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M04"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M04"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M04"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_04_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_04_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING M 05 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_05_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M05"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M05"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M05"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M05"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M05"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M05"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M05"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M05"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M05"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M05"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M05"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M05"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_M_05_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M05"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M05"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M05"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M05"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M05"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M05"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M05"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M05"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_M_05_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_05_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_M_05_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING F 01 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_F_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F01"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F01"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F01"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F01"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F01"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F01"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_F_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_F_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ BEACH DANCING F 02 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_F_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F02"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F02"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F02"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F02"		BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F02"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F02"			BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F02"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F02"	BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F02"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_F_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_F_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══════╡ BEACH DANCING ╞═══════╡
//╘═══════════════════════════════╛

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@BEACHDANCEPROP@"
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 01 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 02 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 03 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M03"	BREAK
				CASE 4	sAnimClip = "LI_IDLE_D_M03"	BREAK
				CASE 5	sAnimClip = "LI_IDLE_E_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M03"	BREAK
				CASE 4	sAnimClip = "MI_IDLE_D_M03"	BREAK
				CASE 5	sAnimClip = "MI_IDLE_E_M03"	BREAK
				CASE 6	sAnimClip = "MI_IDLE_F_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING M 04 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_04_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH	
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M04"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_DROP_M04"		BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M04"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M04"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M04"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M04"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_M_04_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_04_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_M_04_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING F 01 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_F_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F1"			
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	
							sAnimClip = "LI_to_TI_F1"
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	
							sAnimClip = "MI_to_LI_F1"			
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	
							sAnimClip = "MI_to_TI_F1"			
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	
							sAnimClip = "TI_to_LI_F1"
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
								CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
								CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
							ENDSWITCH		
						BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	
							sAnimClip = "TI_to_MI_DROP_F1"	
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
								CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
								CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F1"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F1"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F1"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F1"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH	
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F1"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F1"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F1"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F1"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F1"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F1"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F1"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F1"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_F_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_F_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞══╡ BEACH PROP DANCING F 02 ╞══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_F_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	
							sAnimClip = "LI_to_MI_F02"	
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	
							sAnimClip = "LI_to_TI_F02"		
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	
							sAnimClip = "MI_to_LI_F02"	
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	
							sAnimClip = "MI_to_TI_F02"	
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
								CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
								CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
							ENDSWITCH		
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	
							sAnimClip = "TI_to_LI_F02"		
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
								CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
								CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	
							sAnimClip = "TI_to_MI_F02"			
							iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iRand
								CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
								CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
								CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
							ENDSWITCH	
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_BEACH_PROP_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"	
		
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F02"	BREAK
			ENDSWITCH
			
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F02"	BREAK
			ENDSWITCH
			
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "TI_IDLE_D_F02"	BREAK
			ENDSWITCH
			
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH	
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_BEACH_PROP_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_F_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_BEACH_PROP_F_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞═══════╡ CLUB DANCING ╞═══════╡
//╘══════════════════════════════╛

FUNC STRING GET_ISLAND_DANCING_CLUB_ANIM_DICT()
	RETURN "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CLUB@"
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING M 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_CLUB_M_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_CLUB_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_CLUB_M_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_CLUB_M_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_CLUB_M_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING M 02 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_CLUB_M_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_CLUB_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_CLUB_M_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_CLUB_M_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_CLUB_M_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING M 03 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_CLUB_M_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_M03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_CLUB_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_M03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_M03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_M03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_M03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_CLUB_M_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_CLUB_M_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_CLUB_M_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING F 01 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_CLUB_F_01_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F01"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_CLUB_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F01"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F01"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F01"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F01"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_CLUB_F_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_CLUB_F_01_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_CLUB_F_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING F 02 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_CLUB_F_02_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F02"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_CLUB_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "LI_IDLE_D_F02"	BREAK
				CASE 5	sAnimClip = "LI_IDLE_E_F02"	BREAK
				CASE 6	sAnimClip = "LI_IDLE_F_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "MI_IDLE_D_F02"	BREAK
				CASE 5	sAnimClip = "MI_IDLE_E_F02"	BREAK
				CASE 6	sAnimClip = "MI_IDLE_F_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "HI_IDLE_E_F02"	BREAK
				CASE 5	sAnimClip = "HI_IDLE_F_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F02"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F02"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F02"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F02"	BREAK
				CASE 4	sAnimClip = "TI_IDLE_D_F02"	BREAK
				CASE 5	sAnimClip = "TI_IDLE_E_F02"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_CLUB_F_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_CLUB_F_02_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_CLUB_F_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═════╡ CLUB DANCING F 03 ╞═════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_DANCING_CLUB_F_03_TRANS_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM		
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_HI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_TI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_HI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_TI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
						CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_LI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_MI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "HI_to_TI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
					SWITCH iRand
						CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
						CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_LI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_MI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "TI_to_HI_F03"			BREAK
					ENDSWITCH
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0 pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
						CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
						CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_DANCING_CLUB_F_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	INT iRand
	
	pedAnimData.sMoodAnimDict = "facials@gen_male@base"
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0	sAnimClip = "LI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "LI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "LI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "LI_IDLE_C_F03"	BREAK
				CASE 4	sAnimClip = "LI_IDLE_D_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_low_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_low_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_low_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH iClip
				CASE 0	sAnimClip = "MI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "MI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "MI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "MI_IDLE_C_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_medium_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_medium_2" BREAK
				CASE 2	pedAnimData.sFacialAnimClip = "mood_dancing_medium_3" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
			SWITCH iClip
				CASE 0	sAnimClip = "HI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "HI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "HI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "HI_IDLE_C_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0	sAnimClip = "HI_IDLE_D_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
			SWITCH iRand
				CASE 0 	pedAnimData.sFacialAnimClip = "mood_dancing_high_1" BREAK
				CASE 1	pedAnimData.sFacialAnimClip = "mood_dancing_high_2" BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_TRANCE
			SWITCH iClip
				CASE 0	sAnimClip = "TI_LOOP_F03"	pedAnimData.bLoopingAnim = TRUE	BREAK
				CASE 1	sAnimClip = "TI_IDLE_A_F03"	BREAK
				CASE 2	sAnimClip = "TI_IDLE_B_F03"	BREAK
				CASE 3	sAnimClip = "TI_IDLE_C_F03"	BREAK
			ENDSWITCH
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			SWITCH iRand
				CASE 0 pedAnimData.sMoodAnimDict = "facials@gen_male@base"						pedAnimData.sFacialAnimClip = "mood_dancing_trance_1" BREAK
				CASE 1 pedAnimData.sFacialAnimClip = "mood_dancing_trance_2" BREAK
				CASE 2 pedAnimData.sFacialAnimClip = "mood_dancing_trance_3" BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_ISLAND_DANCING_CLUB_F_03_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_ISLAND_DANCING_CLUB_F_03_TRANS_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_ISLAND_DANCING_CLUB_F_03_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══╡ BEACH VIP ELRUBIO IG1 ╞═══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_ELRUBIO_IG1_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_PED"					pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_PED"				pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_PED"				pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_PED"				pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 4	sAnimClip = "IDLE_D_PED"				pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 5	sAnimClip = "IDLE_E_PED"				pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 6	sAnimClip = "Empecemos_con_esto_PED"	pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG1_EMPECEMOS_CON_ESTO	BREAK
		CASE 7	sAnimClip = "Manos_a_la_obra_PED"		pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG1_MANOS_A_LA_OBRA		BREAK
		CASE 8	sAnimClip = "Muy_bien_PED"				pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG1_MUY_BIEN				BREAK
		CASE 9	sAnimClip = "Vamonos_PED"				pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG1_VAMONOS				BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞═══╡ BEACH VIP ELRUBIO IG2 ╞═══╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_ELRUBIO_IG2_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_Huang"						pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_Huang"							pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_Huang"							pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_Huang"							pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 4	sAnimClip = "IDLE_D_Huang"							pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 5	sAnimClip = "STAY_AS_LONG_YOU_LIKE_Huang"			pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_STAY_AS_LONG_AS_YOU_LIKE		BREAK
		CASE 6	sAnimClip = "DAVEY_IS_ENJOYING_HIMSELF_Huang"		pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_DAVEY_IS_ENJOYING_HIMSELF		BREAK
		CASE 7	sAnimClip = "GO_ON_ENJOY_YOURSELF_Huang"			pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_GO_ON_ENJOY_YOURSELF			BREAK
		CASE 8	sAnimClip = "INCREDIBLE_SET_RIGHT_Huang"			pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_INCREDIBLE_SET_RIGHT			BREAK
		CASE 9	sAnimClip = "I_WILL_GET_BACK_ON_THE_FLOOR_Huang"	pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_I_WILL_GET_BACK_ON_THE_FLOOR	BREAK
		CASE 10	sAnimClip = "JUST_TAKING_A_BREAK_Huang"				pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_JUST_TAKING_A_BREAK			BREAK
		CASE 11	sAnimClip = "NEVER_SEEN_ANYTHING_LIKEIT_Huang"		pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_NEVER_SEEN_ANYTHING_LIKE_IT	BREAK
		CASE 12	sAnimClip = "YOU_HAVING_FUN_Huang"					pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_YOU_HAVING_FUN				BREAK
		CASE 13	sAnimClip = "YOU_NEED_ANYTHING_Huang"				pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_YOU_NEED_ANYTHING				BREAK
		CASE 14	sAnimClip = "YOU_WOULDNT_WANT_TO_MISS_Huang"		pedAnimData.ePedSpeech	 = PED_SPH_CT_ELRUBIO_IG2_YOU_WOULDNT_WANT_TO_MISS		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ BEACH VIP SCOTT IG3 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_SCOTT_IG3_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_MALE"						pedAnimData.bLoopingAnim	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_MALE"							pedAnimData.bLoopingAnim	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_MALE"							pedAnimData.bLoopingAnim	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_MALE"							pedAnimData.bLoopingAnim	= TRUE	BREAK
		CASE 4	sAnimClip = "DEFINITELY_NOT_GETTING_MALE"			pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_DEFINITELY_NOT_GETTING	BREAK
		CASE 5	sAnimClip = "GOOD_JOB_MY_MANAGER_MALE"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_GOOD_JOB_MY_MANAGER		BREAK
		CASE 6	sAnimClip = "I_SHOULDDA_BROUGHT_MALE"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_I_SHOULDDA_BROUGHT		BREAK
		CASE 7	sAnimClip = "SEEN_A_FEW_PRIVATE_MALE"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_SEEN_A_FEW_PRIVATE		BREAK
		CASE 8	sAnimClip = "SERIOUS_PLACE_MALE"					pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_SERIOUS_PLACE			BREAK
		CASE 9	sAnimClip = "THIS_RUBIO_GUY_MALE"					pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_THIS_RUBIO_GUY			BREAK
		CASE 10	sAnimClip = "YOU_KNOW_THE_OWNER_MALE"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_YOU_KNOW_THE_OWNER		BREAK
		CASE 11 sAnimClip = "NICE_TUNES_MALE"						pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG3_NICE_TUNES				BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ BEACH VIP SCOTT IG4 ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_SCOTT_IG4_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_HAUNG"						pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_HAUNG"							pedAnimData.bLoopingAnim	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_HAUNG"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_HAUNG"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "AFTER_PARTY_HAUNG"						pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_AFTER_PARTY					BREAK
		CASE 5	sAnimClip = "BACK_IN_THE_GAME_HAUNG"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_BACK_IN_THE_GAME				BREAK
		CASE 6	sAnimClip = "BACK_WHERE_I_BELONG_HAUNG"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_BACK_WHERE_I_BELONG			BREAK
		CASE 7	sAnimClip = "DONT_WORRY_BABE_HAUNG"					pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_DONT_WORRY_BABE				BREAK
		CASE 8	sAnimClip = "GO_AHEAD_GIRL_HAUNG"					pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_GO_AHEAD_GIRL				BREAK
		CASE 9	sAnimClip = "I_KNOW_YOU_GIRL_HAUNG"					pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_I_KNOW_YOU_GIRL				BREAK
		CASE 10	sAnimClip = "LIKE_I_NEVER_LEFT_HAUNG"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_LIKE_I_NEVER_LEFT			BREAK
		CASE 11	sAnimClip = "THIS_IS_WHAT_A_COMEBACK_HAUNG"			pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_THIS_IS_WHAT_A_COMEBACK		BREAK
		CASE 12	sAnimClip = "WHAT_HAPPENS_IN_PARADISE_HAUNG"		pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_WHAT_HAPPENS_IN_PARADISE		BREAK
		CASE 13	sAnimClip = "YOU_SEE_HOW_HEAVY_HAUNG"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_YOU_SEE_HOW_HEAVY			BREAK
		CASE 14	sAnimClip = "YOU_WAIT_AND_SEE_HAUNG"				pedAnimData.ePedSpeech		= PED_SPH_CT_SCOTT_IG4_YOU_WAIT_AND_SEE				BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_BEACH_VIP_SCOTT_IG4_DANCER_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_A_F_Y_BEACH"					pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_A_F_Y_BEACH"					BREAK
		CASE 2	sAnimClip = "IDLE_B_A_F_Y_BEACH"					BREAK
		CASE 3	sAnimClip = "IDLE_C_A_F_Y_BEACH"					BREAK
		CASE 4	sAnimClip = "AFTER_PARTY_A_F_Y_BEACH"				BREAK
		CASE 5	sAnimClip = "BACK_IN_THE_GAME_A_F_Y_BEACH"			BREAK
		CASE 6	sAnimClip = "BACK_WHERE_I_BELONG_A_F_Y_BEACH"		BREAK
		CASE 7	sAnimClip = "DONT_WORRY_BABE_A_F_Y_BEACH"			BREAK
		CASE 8	sAnimClip = "GO_AHEAD_GIRL_A_F_Y_BEACH"				BREAK
		CASE 9	sAnimClip = "I_KNOW_YOU_GIRL_A_F_Y_BEACH"			BREAK
		CASE 10	sAnimClip = "LIKE_I_NEVER_LEFT_A_F_Y_BEACH"			BREAK
		CASE 11	sAnimClip = "THIS_IS_WHAT_A_COMEBACK_A_F_Y_BEACH"	BREAK
		CASE 12	sAnimClip = "WHAT_HAPPENS_IN_PARADISE_A_F_Y_BEACH"	BREAK
		CASE 13	sAnimClip = "YOU_SEE_HOW_HEAVY_A_F_Y_BEACH"			BREAK
		CASE 14	sAnimClip = "YOU_WAIT_AND_SEE_A_F_Y_BEACH"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞════╡ BEACH VIP DAVE IG5 ╞════╡
//╘══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_DAVE_IG5_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_A"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "BASE_IDLE_B"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 2	sAnimClip = "BASE_IDLE_C"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "BASE_IDLE_D"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "BASE_IDLE_E"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 5	sAnimClip = "BASE_IDLE_F"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 6	sAnimClip = "IDLE_A"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 7	sAnimClip = "IDLE_B"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 8	sAnimClip = "IDLE_C"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 9	sAnimClip = "IDLE_D"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 10	sAnimClip = "IDLE_E"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 11	sAnimClip = "IDLE_F"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 12	sAnimClip = "IDLE_G"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 13	sAnimClip = "IDLE_H"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 14	sAnimClip = "CAN-CAN_IN_HERE"						pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_CAN_CAN_IN_HERE				BREAK
		CASE 15	sAnimClip = "DAVEY_IS_THE_LIFE_AND_SOUL"			pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_DAVEY_IS_THE_LIFE_AND_SOUL	BREAK
		CASE 16	sAnimClip = "MAN_OF_THE_MATCH_RIGHT_HERE"			pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_MAN_OF_THE_MATCH_RIGHT_HERE	BREAK
		CASE 17	sAnimClip = "NICE_ONE_NICE_ONE"						pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_NICE_ONE_NICE_ONE				BREAK
		CASE 18	sAnimClip = "NUDGE_NUDGE_WINK_WINK"					pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_NUDGE_NUDGE_WINK_WINK			BREAK
		CASE 19	sAnimClip = "RIGHT_OUT_OF_THE_GATE"					pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_RIGHT_OUT_OF_THE_GATE			BREAK
		CASE 20	sAnimClip = "RIGHT_UP_MY_ALLEY"						pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_RIGHT_UP_MY_ALLEY				BREAK
		CASE 21	sAnimClip = "THIS_TUNE_HAS_THE_SAME_BPM"			pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_THIS_TUNE_HAS_THE_SAME_BPM	BREAK
		CASE 22	sAnimClip = "WELLY_ON_THE_DECKS"					pedAnimData.ePedSpeech 		= PED_SPH_CT_DAVE_IG5_WELLY_ON_THE_DECKS			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞════╡ BEACH VIP DAVE IG6 ╞════╡
//╘══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_DAVE_IG6_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "IDLE_D"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 5	sAnimClip = "I_KNOW_WHAT_YOU'RE_THINKING"		pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_I_KNOW_WHAT_YOURE_THINKING		BREAK
		CASE 6	sAnimClip = "I_LOST_IT"							pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_I_LOST_IT							BREAK
		CASE 7	sAnimClip = "IS_IT_ME_OR_IS_IT_GETTING_FRUITY"	pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_IS_IT_ME_OR_IS_IT_GETTING_FRUITY	BREAK
		CASE 8	sAnimClip = "LEAN_INTO_IT"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_LEAN_INTO_IT						BREAK
		CASE 9	sAnimClip = "ONLY_A_LITTLE_KERFUFFLE"			pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_ONLY_A_LITTLE_KERFUFFLE			BREAK
		CASE 10	sAnimClip = "SKEW_WHIFF_COMPADRE"				pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_SKEW_WHIFF_COMPADRE				BREAK
		CASE 11	sAnimClip = "SOMEBODY_CHECK_IN"					pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_SOMEBODY_CHECK_IN					BREAK
		CASE 12	sAnimClip = "WERE_YOU_GONE"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_WERE_YOU_GONE						BREAK
		CASE 13	sAnimClip = "WOBBLY_AVOIDED"					pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_WOBBLY_AVOIDED					BREAK
		CASE 14	sAnimClip = "WORD_THE_WISE"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_WORD_THE_WISE						BREAK
		CASE 15	sAnimClip = "YOU'RE_BACK"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG6_YOURE_BACK						BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒══════════════════════════════╕
//╞════╡ BEACH VIP DAVE IG7 ╞════╡
//╘══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_DAVE_IG7_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "IDLE_D"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 5	sAnimClip = "IDLE_E"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 6	sAnimClip = "FULLY_PROJECTED"					pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_FULLY_PROJECTED			BREAK
		CASE 7	sAnimClip = "I_AM_AT_ONE"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_I_AM_AT_ONE				BREAK
		CASE 8	sAnimClip = "I_AM_HERBY_DISEMBALMED"			pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_I_AM_HERBY_DISEMBALMED	BREAK
		CASE 9	sAnimClip = "I_HAVE_MAPPED"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_I_HAVE_MAPPED				BREAK
		CASE 10	sAnimClip = "NAMASTE"							pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_NAMASTE					BREAK
		CASE 11	sAnimClip = "OMMMMMMMM"							pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_OMMMMMMMM					BREAK
		CASE 12	sAnimClip = "ONE_LOVE"							pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_ONE_LOVE					BREAK
		CASE 13	sAnimClip = "PERFECT_ALIGNMENT"					pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_PERFECT_ALIGNMENT			BREAK
		CASE 14	sAnimClip = "SUIVEZ_MOI"						pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_SUIVEZ_MOI				BREAK
		CASE 15	sAnimClip = "THOSE_WERE_MY_LAST_WORDS"			pedAnimData.ePedSpeech		= PED_SPH_CT_DAVE_IG7_THOSE_WERE_MY_LAST_WORDS	BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒═══════════════════════════════╕
//╞════╡ BEACH VIP PARTY GUY ╞════╡
//╘═══════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_PARTY_GUY_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_MALE"			pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_MALE"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_MALE"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_MALE"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "AVERAGE_SHIT_MALE"			pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_AVERAGE_SHIT			BREAK
		CASE 5	sAnimClip = "FUCK_YOU_BUDDY_MALE"		pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_FUCK_YOU_BUDDY		BREAK
		CASE 6	sAnimClip = "HE_OWNS_THE_PLACE_MALE"	pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_HE_OWNS_THE_PLACE	BREAK
		CASE 7	sAnimClip = "I_AM_FUCKED_MALE"			pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_I_AM_FUCKED			BREAK
		CASE 8	sAnimClip = "I_AM_SO_HOT_MALE"			pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_I_AM_SO_HOT			BREAK
		CASE 9	sAnimClip = "JUST_A_MINUTE_MALE"		pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_JUST_A_MINUTE		BREAK
		CASE 10	sAnimClip = "MUSIC_STARTED_MALE"		pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_MUSIC_STARTED		BREAK
		CASE 11	sAnimClip = "TAKE_A_PISS_MALE"			pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_TAKE_A_PISS			BREAK
		CASE 12	sAnimClip = "TIME_OUT_MALE"				pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_TIME_OUT				BREAK
		CASE 13	sAnimClip = "TOO_HARD_TOO_FAST_MALE"	pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GUY_TOO_HARD_TOO_FAST	BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞════╡ BEACH VIP PARTY GIRL ╞════╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_PARTY_GIRL_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "IDLE_D_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 5	sAnimClip = "IDLE_E_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 6	sAnimClip = "IDLE_F_PED"				pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 7	sAnimClip = "IM_THERE_THOUGH_PED"		pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GIRL_IM_THERE_THOUGH	BREAK
		CASE 8	sAnimClip = "ITS_INSANE_PED"			pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GIRL_ITS_INSANE		BREAK
		CASE 9	sAnimClip = "PRONTO_PED"				pedAnimData.ePedSpeech		= PED_SPH_CT_PARTY_GIRL_PRONTO			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═══╡ BEACH VIP PARTY COUPLE ╞═══╡
//╘════════════════════════════════╛

FUNC STRING _GET_ISLAND_BEACH_VIP_PARTY_COUPLE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_MALE"							pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_MALE"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_MALE"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_MALE"								pedAnimData.bLoopingAnim 	= TRUE	BREAK
		CASE 4	sAnimClip = "I_SAW_IT_WHEN_WE_WERE_FLYING_IN_MALE"		pedAnimData.ePedSpeech 		= PED_SPH_CT_PARTY_COUPLE_GUY_SAW_IT_WHEN_FLYING_IN	BREAK
		CASE 5	sAnimClip = "LIKE_REALLY_MALE"							pedAnimData.ePedSpeech 		= PED_SPH_CT_PARTY_COUPLE_GUY_LIKE_REALLY			BREAK
		CASE 6	sAnimClip = "WE'LL_BE_IN_LS_IN_NO_TIME_MALE"			pedAnimData.ePedSpeech 		= PED_SPH_CT_PARTY_COUPLE_GUY_LS_IN_NO_TIME			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_ISLAND_BEACH_VIP_PARTY_COUPLE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH iClip
		CASE 0	sAnimClip = "BASE_IDLE_FEMALE"							pedAnimData.sFacialAnimClip = "BASE_IDLE_FEMALE_Facial"		pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 1	sAnimClip = "IDLE_A_FEMALE"								pedAnimData.sFacialAnimClip = "IDLE_A_FEMALE_Facial"		pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 2	sAnimClip = "IDLE_B_FEMALE"								pedAnimData.sFacialAnimClip = "IDLE_B_FEMALE_Facial"		pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 3	sAnimClip = "IDLE_C_FEMALE"								pedAnimData.sFacialAnimClip = "IDLE_C_FEMALE_Facial"		pedAnimData.bLoopingAnim = TRUE	BREAK
		CASE 4	sAnimClip = "I_SAW_IT_WHEN_WE_WERE_FLYING_IN_FEMALE"	BREAK
		CASE 5	sAnimClip = "LIKE_REALLY_FEMALE"						BREAK
		CASE 6	sAnimClip = "WE'LL_BE_IN_LS_IN_NO_TIME_FEMALE"			BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

//╒════════════════════════════════╕
//╞═════╡ KEINEMUSIK DANCERS ╞═════╡
//╘════════════════════════════════╛

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_01_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_F01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_F01"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0
					sAnimClip 						= "LI_LOOP_F01"	
					pedAnimData.bLoopingAnim 		= TRUE
				BREAK
				CASE 1
					sAnimClip 						= "LI_IDLE_A_F01"
				BREAK
				CASE 2
					sAnimClip 						= "LI_IDLE_B_F01"
				BREAK
				CASE 3
					sAnimClip 						= "LI_IDLE_C_F01"
				BREAK
				CASE 4
					sAnimClip 						= "ADAM_BCH_UNQ_CRWD_A_34_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@adam_port@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 5
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_C_56_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
					pedAnimData.bAddChildPedProps	= TRUE
				BREAK
				CASE 6
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_E_40_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 7
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_F_78_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 8
					sAnimClip 						= "RAMPA_BCH_UNQ_END_B_212_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0
					sAnimClip 						= "MI_LOOP_F01"
					pedAnimData.bLoopingAnim 		= TRUE
				BREAK
				CASE 1
					sAnimClip 						= "MI_IDLE_A_F01"
				BREAK
				CASE 2
					sAnimClip 						= "MI_IDLE_B_F01"
				BREAK
				CASE 3
					sAnimClip 						= "MI_IDLE_C_F01"
				BREAK
				CASE 4
					sAnimClip 						= "ADAM_BCH_UNQ_CRWD_A_34_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@adam_port@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 5
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_C_56_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
					pedAnimData.bAddChildPedProps	= TRUE
				BREAK
				CASE 6
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_E_40_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 7
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_F_78_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 8
					sAnimClip 						= "RAMPA_BCH_UNQ_END_B_212_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_01_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_01_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_01_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_02_TRANS_ANIM_CLIP(CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_MEDIUM
				CASE CLUB_MUSIC_INTENSITY_HIGH
				CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
					SWITCH iClip
						CASE 0	sAnimClip = "LI_to_MI_M03"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
			SWITCH eMusicIntensity
				CASE CLUB_MUSIC_INTENSITY_LOW
				CASE CLUB_MUSIC_INTENSITY_TRANCE
					SWITCH iClip
						CASE 0	sAnimClip = "MI_to_LI_M03"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip)
	STRING sAnimClip = ""
	
	SWITCH ePedMusicIntensity
		CASE CLUB_MUSIC_INTENSITY_TRANCE
		CASE CLUB_MUSIC_INTENSITY_LOW
			SWITCH iClip
				CASE 0
					sAnimClip 						= "LI_LOOP_M03"	
					pedAnimData.bLoopingAnim 		= TRUE
				BREAK
				CASE 1
					sAnimClip 						= "LI_IDLE_A_M03"
				BREAK
				CASE 2
					sAnimClip 						= "LI_IDLE_B_M03"
				BREAK
				CASE 3
					sAnimClip 						= "LI_IDLE_C_M03"
				BREAK
				CASE 4
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_B_103_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 5
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_D_74_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE CLUB_MUSIC_INTENSITY_MEDIUM
		CASE CLUB_MUSIC_INTENSITY_HIGH
		CASE CLUB_MUSIC_INTENSITY_HIGH_HANDS
			SWITCH iClip
				CASE 0
					sAnimClip 						= "MI_LOOP_M03"
					pedAnimData.bLoopingAnim 		= TRUE
				BREAK
				CASE 1
					sAnimClip 						= "MI_IDLE_A_M03"
				BREAK
				CASE 2
					sAnimClip 						= "MI_IDLE_B_M03"
				BREAK
				CASE 3
					sAnimClip 						= "MI_IDLE_C_M03"
				BREAK
				CASE 4
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_B_103_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
				CASE 5
					sAnimClip 						= "RAMPA_BCH_UNQ_CRWD_D_74_DANCER_A"
					pedAnimData.sAnimDict			= "anim@scripted@nightclub@dj@kmuzk@rampa@"
					pedAnimData.fBlendInDelta 		= REALLY_SLOW_BLEND_IN
					pedAnimData.fBlendOutDelta 		= REALLY_SLOW_BLEND_OUT
					pedAnimData.bPartnerAnim 		= TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN sAnimClip
ENDFUNC

FUNC STRING GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_02_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity, CLUB_MUSIC_INTENSITY ePedMusicIntensity, INT iClip, BOOL bDancingTransition = FALSE)
	STRING sAnimClip = ""
	
	IF (bDancingTransition)
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_02_TRANS_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip)
	ELSE
		sAnimClip = _GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_02_ANIM_CLIP(pedAnimData, ePedMusicIntensity, iClip)
	ENDIF
	
	RETURN sAnimClip
ENDFUNC


//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛

PROC _GET_ISLAND_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_AMBIENT_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@MALE_C@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_AMBIENT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_AMBIENT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_AMBIENT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_TEXTING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_LEANING_TEXT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_LEANING_TEXT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_LEANING_SMOKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_LEANING_SMOKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_LEANING_SMOKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_HANGOUT_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_HANGOUT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_HANGOUT_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_HANGOUT_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SMOKING_WEED_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SMOKING_WEED_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SMOKING_WEED_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@FEMALE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SMOKING_WEED_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_ON_PHONE_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_PHONE_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_ON_PHONE_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_PHONE_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_MUSCLE_FLEX
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_MUSCLE_FLEX_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_CHECK_OUT_MIRROR
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_CHECK_OUT_MIRROR_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_DRINKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DRINKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_DRINKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DRINKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_DRINKING_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SITTING_DRINKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_DRINKING_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SITTING_DRINKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_M_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SITTING_AT_BAR_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_M_02
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SITTING_AT_BAR_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_F_01
			pedAnimData.sAnimDict		= "ANIM@AMB@NIGHTCLUB@PEDS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SITTING_AT_BAR_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_SITTING_AT_BAR_F_02
			pedAnimData.sAnimDict		= "AMB@PROP_HUMAN_SEAT_BAR@FEMALE@ELBOWS_ON_BAR@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SITTING_AT_BAR_F_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_GUARD_STANDING
			pedAnimData.sAnimDict 		= GET_ISLAND_GUARD_STANDING_ANIM_DICT(iClip)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_GUARD_STANDING_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_SUNBATHING_BACK_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_SUNBATHING_BACK_M_01_ANIM_DICT(iClip)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SUNBATHING_BACK_M_01_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_SUNBATHING_BACK_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_SUNBATHING_BACK_F_01_ANIM_DICT(iClip)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SUNBATHING_BACK_F_01_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_SUNBATHING_FRONT_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_SUNBATHING_FRONT_M_01_ANIM_DICT(iClip)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SUNBATHING_FRONT_M_01_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_SUNBATHING_FRONT_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_SUNBATHING_FRONT_F_01_ANIM_DICT(iClip)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_SUNBATHING_FRONT_F_01_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_HANGOUT_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_VIP_BAR_HANGOUT_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_VIP_BAR_DRINKING_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_VIP_BAR_DRINKING_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_M_03
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_VIP_BAR_DRINKING_M_03_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_VIP_BAR_DRINKING_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_VIP_BAR_DRINKING_F_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@VIP_BAR@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_VIP_BAR_DRINKING_F_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_STAND_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_STAND_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_LEAN_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_LEAN_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_LEAN_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SEATED_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SEATED_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_STAND_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_SMOKE_WEED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_STAND_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_LEAN_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_SMOKE_WEED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_LEAN_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_SEATED_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_SEATED_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_SMOKE_WEED_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_SMOKE_WEED_SEATED_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_STAND_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_STAND_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_LEAN_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_LEAN_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_SEATED_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_SEATED_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_DRINK_CUP_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_DRINK_CUP_SEATED_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_STAND_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_M_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_STAND_M_02_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_STAND_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_STAND_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_LEAN_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_LEAN_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_LEAN_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_M_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_SEATED_M_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_BEACH_PARTY_SEATED_CELL_PHONE_F_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@BEACH_PARTY@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING | AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_BEACH_PARTY_CELL_PHONE_SEATED_F_01_ANIM_CLIP(iClip)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_M_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_M_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_M_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_M_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_M_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_M_06
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_M_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_F_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_F_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_F_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_F_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_F_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_AMBIENT_F_06
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_AMBIENT_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_AMBIENT_F_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_M_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_M_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_M_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_M_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_M_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_M_06
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_M_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_F_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_F_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_F_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_F_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_F_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_FACE_DJ_F_06
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_FACE_DJ_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_FACE_DJ_F_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_M_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_M_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_M_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_M_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_M_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_M_06
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_M_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_F_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_F_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_F_03_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_F_04_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_F_05_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_SINGLE_PROP_F_06
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_SINGLE_PROP_ANIM_DICT(bDancingTransition)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_SINGLE_PROP_F_06_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPA_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPA@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPA_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPA_M_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPA@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPA_M_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPB@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPB_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPB@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPB_F_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPC@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPC_M_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPC@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPC_M_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPD_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPD_M_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPD_M_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPD_M_CHILD_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPD_M_CHILD_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPE_F_PARENT_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_CHILD_01
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPE_F_CHILD_01_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_GROUPE_M_CHILD_02
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_GROUPE_M_CHILD_02_ANIM_CLIP(eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_M_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_M_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_M_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_M_04_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_M_05
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_M_05_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_F_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_F_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_F_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_PROP_M_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_PROP_M_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_PROP_M_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_M_04
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_PROP_M_04_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_PROP_F_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_BEACH_PROP_F_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_PROP_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_BEACH_PROP)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_BEACH_PROP_F_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_M_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_CLUB_M_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_M_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_CLUB_M_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_M_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_CLUB_M_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_F_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_CLUB_F_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_F_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_CLUB_F_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DANCING_CLUB_F_03
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_CLUB_ANIM_DICT()
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_CLUB)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_ISLAND_DANCING_CLUB_F_03_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DANCING_ANIM_TIME_TO_PLAY_MS(pedAnimData, bDancingTransition)
		BREAK
		CASE PED_ACT_DJ_SOLOMUN
			pedAnimData.sAnimDict 		= "ANIM@AMB@NIGHTCLUB@DJS@SOLOMUN@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_SOL")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ADAM
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@kmuzk@adam_port@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ME
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@kmuzk@andme@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_RAMPA
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@kmuzk@rampa@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_PALMS_TRAX
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_ptrax@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "PTRX_", "_DJ_PTRAX")
		BREAK
		CASE PED_ACT_DJ_MOODYMANN
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_moodm@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_01
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_moodm@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_DANCER_ANIM_CLIP_BASE(iClip, 1), "", "_dancer_a")
		BREAK
		CASE PED_ACT_DJ_MOODYMANN_DANCER_02	
			pedAnimData.sAnimDict 		= "anim@scripted@nightclub@dj@dj_moodm@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_DANCER_ANIM_CLIP_BASE(iClip, 2), "", "_dancer_b")
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_01
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_BLOCK_MOVER_UPDATE)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_EXCLUDE_PARTNER_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_DJ_DANCER)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_01_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DJ_DANCER_ANIM_TIME_TO_PLAY_MS(pedAnimData, eActivity, 2)	// Adam
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_02
			pedAnimData.sAnimDict 		= GET_ISLAND_DANCING_BEACH_ANIM_DICT()
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_BLOCK_MOVER_UPDATE)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_EXCLUDE_PARTNER_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_DJ_DANCER)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= GET_CASINO_NIGHTCLUB_DANCING_KEINEMUSIK_DANCER_02_ANIM_CLIP(pedAnimData, eMusicIntensity, ePedMusicIntensity, iClip, bDancingTransition)
			GET_DJ_DANCER_ANIM_TIME_TO_PLAY_MS(pedAnimData, eActivity, 0)	// Rampa
		BREAK
		CASE PED_ACT_BEACH_VIP_ELRUBIO_IG1
			pedAnimData.sAnimDict		= "anim@scripted@island@special_peds@elrubio@hs4_elrubio_stage1_ig1"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_ELRUBIO_IG1_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_ELRUBIO_IG2
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@elrubio@hs4_elrubio_stage1_ig2"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_ELRUBIO_IG2_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG3
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@scott@hs4_scott_stage1_ig3"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_SCOTT_IG3_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG4
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@scott@hs4_scott_stage2_ig4_p1"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_SCOTT_IG4_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG4_DANCER
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@scott@hs4_scott_stage2_ig4_p1"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_SCOTT_IG4_DANCER_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_DAVE_IG5
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@dave@hs4_dave_stage1_ig5"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_DAVE_IG5_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_DAVE_IG6
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@dave@hs4_dave_stage2_ig6"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_DAVE_IG6_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_DAVE_IG7
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@dave@hs4_dave_stage3_ig7"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_DAVE_IG7_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_GUY
			pedAnimData.sAnimDict		= "anim@scripted@island@special_peds@party_guy@"
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_USE_KINEMATIC_PHYSICS | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_PARTY_GUY_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_GIRL
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@party_girl@hs4_party_girl_stage1_ig9"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_PARTY_GIRL_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_COUPLE_MALE_PARENT
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@couple@hs4_couple_stage2_ig10"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_PARTY_COUPLE_MALE_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_COUPLE_FEMALE_CHILD_01
			pedAnimData.sAnimDict 		= "anim@scripted@island@special_peds@couple@hs4_couple_stage2_ig10"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= _GET_ISLAND_BEACH_VIP_PARTY_COUPLE_FEMALE_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_CLIPBOARD
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_CLIPBOARD@MALE@BASE"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
	ENDSWITCH
	
	IF SHOULD_PED_RUN_FACE_ANIM_AS_MOOD(eActivity)
		GET_DANCING_CLUB_FACE_MOOD_ANIM_CLIP(pedAnimData, ePedMusicIntensity, bDancingTransition)
	ENDIF
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROP ANIM DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_ISLAND_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_DANCING_GROUPA_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPA@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 6
								IF NOT (bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPB_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPB@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 5
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v2_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v2_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_17_v1_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_17_v1_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
							CASE 7
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_15_v2_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_15_v2_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^HEEL"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK	
				CASE 1	//ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_BA_Prop_Battle_Vape_01^FLAT"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_BA_Prop_Battle_Vape_01^HEEL"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPC_M_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPC@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 4
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v1_Prop_NPC_Phone"
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone"
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_17_v1_Prop_NPC_Phone"
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE 1 //ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 1
								IF (bDancingTransition)
									IF (eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
									OR (eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
										pedPropAnimData.sPropAnimClip = "Trans_Dance_Crowd_MI_to_LI_11_v1_BA_Prop_Battle_Vape_01"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPD_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPD@"
			SWITCH iProp
				CASE 0	//ba_prop_battle_vape_01
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 6
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_15_v1BA_Prop_Battle_Vape_01^flat"
//									IF (bHeeledPed)
//										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_15_v1BA_Prop_Battle_Vape_01^heel"
//									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1BA_Prop_Battle_Vape_01^flat^1"
//									IF (bHeeledPed)
//										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1BA_Prop_Battle_Vape_01^heel^1"
//									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DANCING_GROUPE_F_PARENT
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB_ISLAND@DANCERS@CROWDDANCE_GROUPS@GROUPE@"
			SWITCH iProp
				CASE 0	//PROP_NPC_PHONE
					IF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_HIGH_HANDS)
						SWITCH iClip
							CASE 4
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "HI_Dance_Crowd_13_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_MEDIUM)
						SWITCH iClip
							CASE 1
								IF (bDancingTransition)
									IF (eMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
									OR (eMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
										pedPropAnimData.sPropAnimClip = "Trans_Dance_Crowd_MI_to_LI_11_v1_Prop_NPC_Phone^flat"
										IF (bHeeledPed)
											pedPropAnimData.sPropAnimClip = "Trans_Dance_Crowd_MI_to_LI_11_v1_Prop_NPC_Phone^heel"
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 3
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_11_v2_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_11_v2_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 6
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 7
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v2_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_15_v2_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_17_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "MI_Dance_Crowd_17_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ELIF (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_LOW)
					OR (ePedMusicIntensity = CLUB_MUSIC_INTENSITY_TRANCE)
						SWITCH iClip
							CASE 3
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_11_v2_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF (NOT bDancingTransition)
									pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^flat"
									IF (bHeeledPed)
										pedPropAnimData.sPropAnimClip = "LI_Dance_Crowd_17_v1_Prop_NPC_Phone^heel"
									ENDIF
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_SOLOMUN
			pedPropAnimData.sPropAnimDict = "ANIM@AMB@NIGHTCLUB@DJS@SOLOMUN@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "_HEADPHONES")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_WINEGLASS")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ADAM
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@adam_port@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_bottle")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_tumbler")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_ME
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@andme@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_bottle")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_COCONUTDRINK")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_RAMPA
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@rampa@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_tumbler")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_T_BOTTLE")
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_S_GLASS_01")
				BREAK
				CASE 4
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_S_GLASS_02")
				BREAK
				CASE 5
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_S_GLASS_03")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_PALMS_TRAX
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@dj_ptrax@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "PTRX_", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "PTRX_", "_BEER_CAN")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_MOODYMANN
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@dj_moodm@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_turntable_left")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_turntable_right")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_lp_left")
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_lp_right")
				BREAK
				CASE 4
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_bag")
				BREAK
				CASE 5
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_TUMBLER")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_ELRUBIO_IG2
			pedPropAnimData.sPropAnimDict = "anim@scripted@island@special_peds@elrubio@hs4_elrubio_stage1_ig2"
			SWITCH iProp
				CASE 0
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_Prop_CS_Ciggy_01"					BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_Prop_CS_Ciggy_01"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_Prop_CS_Ciggy_01"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_Prop_CS_Ciggy_01"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_Prop_CS_Ciggy_01"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "STAY_AS_LONG_YOU_LIKE_Prop_CS_Ciggy_01"		BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "DAVEY_IS_ENJOYING_HIMSELF_Prop_CS_Ciggy_01"	BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "GO_ON_ENJOY_YOURSELF_Prop_CS_Ciggy_01"			BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "INCREDIBLE_SET_RIGHT_Prop_CS_Ciggy_01"			BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "I_WILL_GET_BACK_ON_THE_FLOOR_Prop_CS_Ciggy_01"	BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "JUST_TAKING_A_BREAK_Prop_CS_Ciggy_01"			BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "NEVER_SEEN_ANYTHING_LIKEIT_Prop_CS_Ciggy_01"	BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "YOU_HAVING_FUN_Prop_CS_Ciggy_01"				BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "YOU_NEED_ANYTHING_Prop_CS_Ciggy_01"			BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "YOU_WOULDNT_WANT_TO_MISS_Prop_CS_Ciggy_01"		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG3
			pedPropAnimData.sPropAnimDict = "anim@scripted@island@special_peds@scott@hs4_scott_stage1_ig3"
			SWITCH iProp
				CASE 0
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PROP"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PROP"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PROP"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PROP"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "DEFINITELY_NOT_GETTING_PROP"			BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "GOOD_JOB_MY_MANAGER_PROP"				BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "I_SHOULDDA_BROUGHT_PROP"				BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "SEEN_A_FEW_PRIVATE_PROP"				BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "SERIOUS_PLACE_PROP"					BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "THIS_RUBIO_GUY_PROP"					BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "YOU_KNOW_THE_OWNER_PROP"				BREAK
						CASE 11 pedPropAnimData.sPropAnimClip = "NICE_TUNES_MALE_PROP"					BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_SCOTT_IG4
			pedPropAnimData.sPropAnimDict = "anim@scripted@island@special_peds@scott@hs4_scott_stage2_ig4_p1"
			SWITCH iProp
				CASE 0
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_CUP"									BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_CUP"									BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_CUP"									BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_CUP"									BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "AFTER_PARTY_CUP"								BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "BACK_IN_THE_GAME_CUP"							BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "BACK_WHERE_I_BELONG_CUP"						BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "DONT_WORRY_BABE_CUP"							BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "GO_AHEAD_GIRL_CUP"								BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "I_KNOW_YOU_GIRL_CUP"							BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "LIKE_I_NEVER_LEFT_CUP"							BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "THIS_IS_WHAT_A_COMEBACK_CUP"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "WHAT_HAPPENS_IN_PARADISE_CUP"					BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "YOU_SEE_HOW_HEAVY_CUP"							BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "YOU_WAIT_AND_SEE_CUP"							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_GIRL
			pedPropAnimData.sPropAnimDict = "anim@scripted@island@special_peds@party_girl@hs4_party_girl_stage1_ig9"
			SWITCH iProp
				CASE 0
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PHONE"								BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PHONE"									BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PHONE"									BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PHONE"									BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_PHONE"									BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "IDLE_E_PHONE"									BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "IDLE_F_PHONE"									BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "IM_THERE_THOUGH_PHONE"							BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "ITS_INSANE_PHONE"								BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "PRONTO_PHONE"									BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_BEACH_VIP_PARTY_COUPLE_MALE_PARENT
			pedPropAnimData.sPropAnimDict = "anim@scripted@island@special_peds@couple@hs4_couple_stage2_ig10"
			SWITCH iProp
				CASE 0
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PHONE"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PHONE"								BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PHONE"								BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PHONE"								BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "I_SAW_IT_WHEN_WE_WERE_FLYING_IN_PHONE"		BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "LIKE_REALLY_PHONE"							BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "WE'LL_BE_IN_LS_IN_NO_TIME_PHONE"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_DJ_KEINEMUSIK_DANCER_01
			pedPropAnimData.sPropAnimDict = "anim@scripted@nightclub@dj@kmuzk@rampa@"
			SWITCH iProp
				CASE 0	// PROP_AMB_PHONE
					SWITCH iClip
						CASE 5	pedPropAnimData.sPropAnimClip = "RAMPA_BCH_UNQ_CRWD_C_56_DANCER_A_PHONE"	BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ WEAPON PROP DATA ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_ISLAND_PED_PROP_WEAPON_DATA(PED_PROP_WEAPON_DATA &pedPropWeaponData, INT iPed)
	SWITCH iPed
		CASE 120
		CASE 121
		CASE 122
			// Guards holding Assault Rifles
			pedPropWeaponData.eWeapon 			= WEAPONTYPE_ASSAULTRIFLE
			pedPropWeaponData.iAmmo				= 1
			pedPropWeaponData.bEquipWeapon		= TRUE
			pedPropWeaponData.bForceIntoHand	= TRUE
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CULLING DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_ISLAND_PED_CULLING_LOCATE_DATA(PED_AREAS ePedArea, PED_CULLING_LOCATE_DATA &pedCullingLocateData)
	SWITCH ePedArea
		CASE PED_AREA_ONE
			pedCullingLocateData.vLocateVectorOne = <<-1025.470337,-221.341888,36.946171>>
			pedCullingLocateData.vLocateVectorTwo = <<-1023.535217,-217.722595,39.422211>>
			pedCullingLocateData.fLocateWidth = 1.250000
		BREAK
		CASE PED_AREA_TWO
			pedCullingLocateData.vLocateVectorOne = <<-1026.587158,-220.765900,36.944595>>
			pedCullingLocateData.vLocateVectorTwo = <<-1024.646851,-217.130463,39.422775>>
			pedCullingLocateData.fLocateWidth = 1.250000
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ HEAD TRACKING DATA ╞════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_ISLAND_PED_HEAD_TRACKING_DATA_LAYOUT_0(HEAD_TRACKING_DATA &HeadtrackingData, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				HeadtrackingData.iPedID							= 119
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 1
				HeadtrackingData.iPedID							= 120
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 2
				HeadtrackingData.iPedID							= 121
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 3
				HeadtrackingData.iPedID							= 122
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_ISLAND_PED_HEAD_TRACKING_DATA(HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_ISLAND_PED_HEAD_TRACKING_DATA_LAYOUT_0(HeadtrackingData, iArrayID, iHeadTrackingPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ DANCING DATA ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _DOES_ISLAND_PED_LOCATION_USE_DANCING_PEDS()
	RETURN TRUE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ PARENT PED DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_ISLAND_PED_PARENT_DATA_LAYOUT_0(PARENT_PED_DATA &ParentPedData, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				ParentPedData.iParentID							= 45
				ParentPedData.iChildID[0]						= 46
				ParentPedData.iChildID[1]						= 30
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 1
				ParentPedData.iParentID							= 58
				ParentPedData.iChildID[0]						= 57
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 2
				ParentPedData.iParentID							= 91
				ParentPedData.iChildID[0]						= 92
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 3
				ParentPedData.iParentID							= 50
				ParentPedData.iChildID[0]						= 56
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 4
				ParentPedData.iParentID							= 27
				ParentPedData.iChildID[0]						= 116
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 5
				ParentPedData.iParentID							= 16
				ParentPedData.iChildID[0]						= 15
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 6
				ParentPedData.iParentID							= 2	// Keinemusik Adam
				ParentPedData.iChildID[0]						= 3	// Keinemusik Dancer 01
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 7
				ParentPedData.iParentID							= 0	// Keinemusik Rampa
				ParentPedData.iChildID[0]						= 3	// Keinemusik Dancer 01
				ParentPedData.iChildID[1]						= 4	// Keinemusik Dancer 02
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 8	// Party Couple
				ParentPedData.iParentID							= 70
				ParentPedData.iChildID[0]						= 72
				ParentPedData.iChildID[1]						= -1
				ParentPedData.iChildID[2]						= -1
				iParentPedID[ParentPedData.iParentID]			= iArrayID
			BREAK
			CASE 9	// Scott
				IF NOT GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
				OR (GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID()) AND HAS_PLAYER_COMPLETED_HEIST_ISLAND_PREP_MISSION_FROM_ITEM_TYPE(GET_PLAYER_GANG_BOSS(), IHIT_SCOPING_INFORMATION))
					ParentPedData.iParentID							= 73
					ParentPedData.iChildID[0]						= 95
					ParentPedData.iChildID[1]						= -1
					ParentPedData.iChildID[2]						= -1
					iParentPedID[ParentPedData.iParentID]			= iArrayID
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_ISLAND_PED_PARENT_DATA(PARENT_PED_DATA &ParentPedData, INT iLayout, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_ISLAND_PED_PARENT_DATA_LAYOUT_0(ParentPedData, iArrayID, iParentPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_ISLAND_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_ISLAND_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_ISLAND_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_ISLAND_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_ISLAND_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal = &_GET_ISLAND_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_ISLAND_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_ISLAND_SERVER_PED_LEVEL
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_ISLAND_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_ISLAND_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_ISLAND_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_ISLAND_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_ISLAND_PED_ANIM_DATA
		BREAK
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_ISLAND_PED_PROP_ANIM_DATA
		BREAK
		CASE E_GET_PED_PROP_WEAPON_DATA
			interface.getPedPropWeaponData = &_GET_ISLAND_PED_PROP_WEAPON_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_ISLAND_PARENT_PROPERTY
		BREAK
		CASE E_DOES_ACTIVE_PED_TOTAL_CHANGE
			interface.returnDoesActivePedTotalChange = &_DOES_ISLAND_ACTIVE_PED_TOTAL_CHANGE
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_ISLAND_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_ISLAND_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_ISLAND_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_ISLAND_PED_PROP_INDEXES
		BREAK
		CASE E_SET_PED_ALT_MOVEMENT
			interface.setPedAltMovement = &_SET_ISLAND_PED_ALT_MOVEMENT
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_ISLAND_PED_BEEN_CREATED
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_ISLAND_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_ISLAND_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_ISLAND_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_ISLAND_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_ISLAND_PED_CONVO_DATA
		BREAK
		
		// Ped Culling
		CASE E_GET_PED_CULLING_LOCATE_DATA
			interface.getPedCullingLocateData = &_GET_ISLAND_PED_CULLING_LOCATE_DATA
		BREAK
		
		// Head Tracking
		CASE E_SET_PED_HEAD_TRACKING_DATA
			interface.setPedHeadTrackingData = &_SET_ISLAND_PED_HEAD_TRACKING_DATA
		BREAK
		
		// Dancing Data
		CASE E_DOES_PED_LOCATION_USE_DANCING_PEDS
			interface.returnDoesPedLocationUseDancingPeds = &_DOES_ISLAND_PED_LOCATION_USE_DANCING_PEDS
		BREAK
		
		// Parent Ped Data
		CASE E_SET_PED_PARENT_DATA
			interface.setPedParentData = &_SET_ISLAND_PED_PARENT_DATA
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
