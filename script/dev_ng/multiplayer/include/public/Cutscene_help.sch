//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        Cutscene_help.sch																			//
// Description: Helpers for cutscene asset creation/action.													//
// Written by:  Aleksej Leskin																				//
// Date:  		29/03/2017																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
USING "globals.sch"
USING "net_gang_boss_common.sch"
USING "ped_component_public.sch"
USING "vehicle_public.sch"
USING "net_gang_boss.sch"
USING "fmmc_camera.sch"

CONST_INT CONST_TRANSFORM_CUTSCENE_OBJ_COUNT 3
CONST_INT CONST_TRANSFORM_CUTSCENE_OBJ_BASE 0
CONST_INT CONST_TRANSFORM_CUTSCENE_OBJ_START 1
CONST_INT CONST_TRANSFORM_CUTSCENE_OBJ_FINISH 2
	
/// PURPOSE:
///    Call before the cutscene starts, cleans up area, removes particle effects.
PROC CUTSCENE_HELP_PREPARE_AREA_FOR_CUTSCENE(VECTOR vPosition, FLOAT fRadius, BOOL bVehEntityOnlyClearArea = FALSE)
	IF bVehEntityOnlyClearArea
		CLEAR_AREA_OF_VEHICLES(vPosition, fRadius, TRUE)
	ELSE
		CLEAR_AREA(vPosition, fRadius, TRUE)
	ENDIF
	REMOVE_PARTICLE_FX_IN_RANGE(vPosition, fRadius)	
ENDPROC

PROC CUTSCENE_HELP_VEHICLE_MOVE_FORWARD(PED_INDEX &pedDriver, VEHICLE_INDEX &vehicle, INT iTimeToDrive, FLOAT fSpeed = 15.0, TEMPACTION action = TEMPACT_GOFORWARD_HARD)
	IF DOES_ENTITY_EXIST(vehicle)
	AND DOES_ENTITY_EXIST(pedDriver)
		IF IS_VEHICLE_DRIVEABLE(vehicle)
		AND NOT IS_PED_DEAD_OR_DYING(pedDriver)
		
			FLOAT fMaxSpeed
			fMaxSpeed = GET_VEHICLE_ESTIMATED_MAX_SPEED(vehicle)
			
			IF fMaxSpeed < fSpeed
				ASSERTLN("CUTSCENE_HELP_VEHICLE_MOVE_FORWARD - Vehicle max speed is: ", fMaxSpeed, " Desired speed was set to: ", fSpeed, " defaulting to max speed")
			ENDIF
			
			FLOAT fLowSpeed
			fLowSpeed = fSpeed * 100 / fMaxSpeed
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_MOVE_FORWARD - fMaxSpeed: ", fMaxSpeed, " fLowSpeedPERCENT: ", fLowSpeed, " iTimeToDrive: ", iTimeToDrive)
			
			TASK_VEHICLE_TEMP_ACTION(pedDriver, vehicle, action, iTimeToDrive)
			MODIFY_VEHICLE_TOP_SPEED(vehicle, (100.000 - fLowSpeed) * -1)
			
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_MOVE_FORWARD - NEWTopSpeed: ", GET_VEHICLE_ESTIMATED_MAX_SPEED(vehicle))
						
//		
//			VECTOR vehEndCoord
//			vehEndCoord = GET_ENTITY_COORDS(vehicle) + GET_ENTITY_FORWARD_VECTOR(vehicle) * fDisantace
//			
//			#IF IS_DEBUG_BUILD
//			VECTOR vehCoords
//			vehCoords = GET_ENTITY_COORDS(vehicle)
//			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_MOVE_FORWARD - Veh Coord: ", vehCoords, " End Coords: ", vehEndCoord, " Distance: ", fDisantace)
//			#ENDIF
//			TASK_VEHICLE_DRIVE_TO_COORD(pedDriver, vehicle, vehEndCoord, fSpeed, DRIVINGSTYLE_STRAIGHTLINE, DUMMY_MODEL_FOR_SCRIPT, DRIVINGMODE_PLOUGHTHROUGH, 1.0, -1) 
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks to see if all mods for the cloned Vehicle got streamed in.
/// PARAMS:
///    vehToClone - 
///    vehClone - clone of vehToClone vehicle.
/// RETURNS:
///    all streaming finished.
PROC CUTSCENE_HELP_VEHICLE_CLONE_MODS(VEHICLE_INDEX vehToClone, VEHICLE_INDEX vehClone, INT &iAssignedMod[])
	IF NOT DOES_ENTITY_EXIST(vehToClone)
		ASSERTLN("CUTSCENE_HELP_VEHICLE_CLONE_MODS - cant stream mods, vehToClone - doesn't exist")
		EXIT
	ENDIF

	IF NOT DOES_ENTITY_EXIST(vehClone)
		ASSERTLN("CUTSCENE_HELP_VEHICLE_CLONE_MODS - cant stream mods, vehClone - doesn't exist")
		EXIT
	ENDIF
	
	IF GET_ENTITY_MODEL(vehToClone) <> GET_ENTITY_MODEL(vehClone)
		ASSERTLN("CUTSCENE_HELP_VEHICLE_CLONE_MODS - Vehicle models are not the same, vehToClone: ", GET_ENTITY_MODEL(vehToClone), " vehClone: ", GET_ENTITY_MODEL(vehClone))
		EXIT
	ENDIF

	INT iModIndex
	IF NOT IS_ENTITY_DEAD(vehClone)
	AND NOT IS_ENTITY_DEAD(vehToClone)
		MOD_TYPE eMod
		INT iModType
		FOR iModType = MOD_SPOILER TO MOD_LIVERY
			eMod = INT_TO_ENUM(MOD_TYPE, iModType)
			iAssignedMod[iModType] = -1 //reset currently assigned mod.
			
			IF IS_VEHICLE_MOD_SAFE_TO_PRELOAD(vehToClone, eMod)
				//Check bit set and switch to new set of modifications.
				iModIndex = GET_VEHICLE_MOD(vehToClone, eMod) 
				IF iModIndex > -1
					IF GET_NUM_VEHICLE_MODS(vehToClone, eMod) != 0		
						PRELOAD_VEHICLE_MOD(vehClone, eMod, GET_VEHICLE_MOD(vehToClone,eMod))
						iAssignedMod[iModType] = GET_VEHICLE_MOD(vehToClone,eMod)
						CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_CLONE_MODS - Mod is not preloaded yet, Vehicle: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehToClone)), " Mod: ", iModType, " ModIndex: ", iModIndex)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_CLONE_MODS - Mod is not added, Vehicle: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehToClone)), " Mod: ", iModType, " ModIndex: ", iModIndex)
				ENDIF
			ENDIF
		ENDFOR		
	ENDIF	
ENDPROC

/// PURPOSE:
///    Checks to see if all mods for the cloned Vehicle got streamed in.
/// PARAMS:
///    vehToClone - 
///    vehClone - clone of vehToClone vehicle.
/// RETURNS:
///    all streaming finished.
FUNC BOOL CUTSCENE_HELP_VEHICLE_CLONE_MODS_STREAMED(VEHICLE_INDEX vehClone, INT &iAssignedMod[])	
	IF NOT DOES_ENTITY_EXIST(vehClone)
	OR NOT IS_VEHICLE_DRIVEABLE(vehClone)
		ASSERTLN("CUTSCENE_HELP_VEHICLE_CLONE_MODS_STREAMED - cant stream mods, vehClone - doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF HAS_PRELOAD_MODS_FINISHED(vehClone)		
		MOD_TYPE eMod
		INT iModType
		FOR iModType = MOD_SPOILER TO MOD_LIVERY
			eMod = INT_TO_ENUM(MOD_TYPE, iModType)
			
			IF iAssignedMod[iModType] <> -1
			
				IF IS_VEHICLE_MOD_SAFE_TO_PRELOAD(vehClone, eMod)	
					CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_CLONE_MODS_STREAMED - Safe to preaload iModType: ", iModType, " Variation: ", iAssignedMod[iModType])
					TOGGLE_VEHICLE_MOD(vehClone, eMod, TRUE)
					CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_CLONE_MODS_STREAMED - Mod is preloaded!!!, vehClone: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehClone)), " Mod: ", iModType)
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_CLONE_MODS_STREAMED - Mod is not added, vehClone: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehClone)), " Mod: ", iModType, " Variaton: ", iAssignedMod[iModType])
			ENDIF
		ENDFOR
		
		RELEASE_PRELOAD_MODS(vehClone)	
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_CLONE_MODS_STREAMED - All preloaded mods assigned.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Call each frame to allow cloned peds open dors in the cutscene, even if networked vehicles are in the way.
//Add aditional here
PROC CUTSCE_HELP_MAINTAIN_PED_ARRAY_FREEMODE_CUTSCENE(PED_INDEX &pedArray[])
	INT iPed
	REPEAT COUNT_OF(pedArray) iPed
		IF DOES_ENTITY_EXIST(pedArray[iPed])
		AND NOT IS_ENTITY_DEAD(pedArray[iPed])
			SET_PED_RESET_FLAG(pedArray[iPed], PRF_IgnoreVehicleEntryCollisionTests, TRUE)
			SET_PED_RESET_FLAG(pedArray[iPed], PRF_IgnoreDetachSafePositionCheck, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC CUTSCENE_HELP_CUTSCENE_PED_PARAMS(PED_INDEX pedClone)
	IF DOES_ENTITY_EXIST(pedClone)
	AND NOT IS_ENTITY_DEAD(pedClone)
		SET_PED_AS_ENEMY(pedClone, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedClone, TRUE)
		SET_ENTITY_CAN_BE_DAMAGED(pedClone, FALSE)
		SET_PED_RESET_FLAG(pedClone, PRF_DisablePotentialBlastReactions, TRUE)
		SET_PED_CONFIG_FLAG(pedClone, PCF_UseKinematicModeWhenStationary, TRUE)
		//SET_PED_CONFIG_FLAG(pedClone, PCF_DontActivateRagdollFromExplosions, TRUE)
		SET_PED_CAN_EVASIVE_DIVE(pedClone, FALSE)
		SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(pedClone, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedClone, FALSE)
		SET_PED_CONFIG_FLAG(pedClone, PCF_DisableExplosionReactions, TRUE)
		SET_PED_CONFIG_FLAG(pedClone, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedClone, PCF_Avoidance_Ignore_All, TRUE)
		//Prevent from falling of the side of the vehicle
		SET_PED_CAN_RAGDOLL(pedClone, FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Clone ped apply clothing variations, ped must be streamed after cloning.
/// PARAMS:
///    pedCloneRef - 
///    pedToClone - 
///    bFrezeBelowPedToClone - Freezes the cloned ped 10m below the real ped to avoid collision issues
/// RETURNS:
///    
FUNC BOOL CUTSCENE_HELP_CLONE_PED(PED_INDEX &pedCloneRef, PED_INDEX pedToClone, BOOL bLinkBlends = TRUE, BOOL bFrezeBelowPedToClone = FALSE)
	IF NOT DOES_ENTITY_EXIST(pedToClone)
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PED - Cant clone ped, pedToClone -> doesn't exist")
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedCloneRef)
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PED - Cant clone ped, newPedRef -> already has a reference")
		RETURN FALSE
	ENDIF
	
	pedCloneRef = CLONE_PED(pedToClone, FALSE, FALSE, bLinkBlends)
	
	IF IS_ENTITY_ALIVE(pedToClone)
		SET_ENTITY_INVINCIBLE(pedCloneRef, TRUE)
		CUTSCENE_HELP_CUTSCENE_PED_PARAMS(pedCloneRef)
		
		//HELMET
		IF IS_PED_WEARING_A_HELMET(pedToClone)
		OR IS_PED_WEARING_A_HAT(pedToClone)
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PED - Helmet - is wearing , setting on clone.")
			SET_PED_HELMET(pedCloneRef, TRUE)
		ELSE
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PED - Helmet - not wearing, removing from clone.")
			SET_PED_HELMET(pedCloneRef, FALSE)
			REMOVE_PED_HELMET(pedCloneRef, TRUE)
			CLEAR_PED_PROP(pedCloneRef, ANCHOR_HEAD)
		ENDIF
		
		IF bFrezeBelowPedToClone
			VECTOR vPosition = GET_ENTITY_COORDS(pedCloneRef, FALSE)
			vPosition.z -= 10.0
			SET_ENTITY_COORDS(pedCloneRef, vPosition, FALSE)
			FREEZE_ENTITY_POSITION(pedCloneRef, TRUE)
			SET_ENTITY_COLLISION(pedCloneRef, FALSE)
		ENDIF
		
		FINALIZE_HEAD_BLEND(pedCloneRef)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CUTSCENE_HELP_APPLY_BIKER_OUTFIT_TO_PED(PED_INDEX &pedToApply, PLAYER_INDEX playerToCopyFrom)
	
	IF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(playerToCopyFrom)
		MP_OUTFIT_ENUM outfit = GB_OUTFITS_GET_PLAYERS_CURRENT_GANG_OUTFIT(playerToCopyFrom)	
		
		MP_OUTFITS_APPLY_DATA 	sApplyData
		sApplyData.pedID 		= pedToApply
		sApplyData.eApplyStage 	= AOS_SET 		// Force stage to setting stage
		
		SET_PED_MP_OUTFIT(sApplyData, SHOULD_IGNORE_APPLY_MASK_FOR_OUTFIT(outfit), DEFAULT, DEFAULT, DEFAULT, 
			!SHOULD_LOCAL_PED_KEEP_PARACHUTE(), DEFAULT, GB_OUTFITS_BD_GET_BIKER_EMBLEM_TO_DISPLAY(), 
			GET_OUTFIT_ROLE_FROM_MC_ROLE(GB_GET_PLAYER_GANG_ROLE(playerToCopyFrom)), SHOULD_BLOCK_HOOD(outfit))
		
		//BIKER LOGO
		REQUEST_STREAMED_TEXTURE_DICT(GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(playerToCopyFrom))
		IF OVERRIDE_PED_CREW_LOGO_TEXTURE(pedToApply, GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(playerToCopyFrom), GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(playerToCopyFrom, CUSTOM_CREW_LOGO_PRESET))
			CPRINTLN(DEBUG_CUTSCENE,"CUTSCENE_HELP_APPLY_BIKER_OUTFIT_TO_PED - PRESET OVERRIDED: ", GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(playerToCopyFrom, CUSTOM_CREW_LOGO_PRESET))
		ELSE
			CPRINTLN(DEBUG_CUTSCENE,"CUTSCENE_HELP_APPLY_BIKER_OUTFIT_TO_PED - PRESET FAILED texture Name: ", GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(playerToCopyFrom, CUSTOM_CREW_LOGO_PRESET))
		ENDIF
				
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_APPLY_BIKER_OUTFIT_TO_PED - outfit applied.")
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_APPLY_BIKER_OUTFIT_TO_PED - Player is not wearing a gang outfit.")
	ENDIF
		
	RETURN FALSE
ENDFUNC

FUNC BOOL CUTSCENE_HELP_CLONE_PLAYER(PED_INDEX &pedCloneRef, PLAYER_INDEX playerToClone, BOOL bLinkBlends = TRUE, BOOL bFrezeBelowPedToClone = FALSE)
	PED_INDEX clonePlayerPed = GET_PLAYER_PED(playerToClone)
	
	IF NOT DOES_ENTITY_EXIST(clonePlayerPed)
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PLAYER - player ped doesnt exist.")
		RETURN FALSE
	ENDIF
	
	IF NOT CUTSCENE_HELP_CLONE_PED(pedCloneRef, clonePlayerPed, bLinkBlends, bFrezeBelowPedToClone)
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PLAYER - ped cloning failed.")
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedCloneRef)
	AND NOT IS_ENTITY_DEAD(pedCloneRef)
		IF NOT CUTSCENE_HELP_APPLY_BIKER_OUTFIT_TO_PED(pedCloneRef, playerToClone)
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_PLAYER - Did not clone any biker outfit.")
		ENDIF

		//url:bugstar:3557655 - Bunker Entry Cutscene - Senora Desert: Green hair on remote players character/ped
		FINALIZE_HEAD_BLEND(pedCloneRef)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_HELP_SET_PED_ON_GROUND(PED_INDEX ped, VECTOR position)
	IF NOT DOES_ENTITY_EXIST(ped)
		ASSERTLN("CUTSCENE_HELP_SET_PED_ON_GROUND - Unable to set ped on ground, ped does not exist")
		RETURN FALSE
	ENDIF
	
	SET_ENTITY_COORDS(ped, position)
	VECTOR vNewPosition
	vNewPosition = GET_ENTITY_COORDS(ped)
	CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_SET_PED_ON_GROUND - Set coord(position): ", position, " Got coord(vNewPosition): ", vNewPosition)
	
	FLOAT zGround
	IF GET_GROUND_Z_FOR_3D_COORD(vNewPosition, zGround)
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_SET_PED_ON_GROUND - New GroundZ Found: ", zGround)
		vNewPosition.z = zGround
		SET_ENTITY_COORDS_NO_OFFSET(ped, vNewPosition + <<0.0, 0.0, 1.0>>)
		
		RETURN TRUE
	ENDIF
			
	CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_SET_PED_ON_GROUND - Was uanble to position ped on the ground at: ", position)
	RETURN FALSE
ENDFUNC

PROC CUTSCENE_HELP_MIRROR_TRANSFORM(VECTOR vOrigin, VECTOR vOriginDir, VECTOR vPosToMirror, FLOAT fHeadingToMirror, VECTOR &vOutMirroredPos, FLOAT &vOutMirrorHeading, FLOAT &fAngleDiff, BOOL bDebug = FALSE)
	FLOAT fOriginHeading = GET_HEADING_FROM_VECTOR_2D(vOriginDir.x, vOriginDir.y)
	VECTOR vOriginTargetDir = NORMALISE_VECTOR(vPosToMirror - vOrigin)
	FLOAT fHeadingToTarget = GET_HEADING_FROM_VECTOR_2D(vOriginTargetDir.x, vOriginTargetDir.y)
	
	FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vOrigin, vPosToMirror)
	#IF IS_DEBUG_BUILD
	IF bDebug
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_MIRROR_POSITION - vOrigin: ", vOrigin, " vOriginDir: ", vOriginDir, " vPointToMirror: ", vPosToMirror, " *** fOriginHeading: ", fOriginHeading, " ***fHeadingToTarget: ", fHeadingToTarget)
		//Origin and its heading.
		DRAW_DEBUG_SPHERE(vOrigin, 0.2)
		DRAW_DEBUG_LINE(vOrigin, vOrigin + vOriginDir * fDistance)
		//Point to mirror
		DRAW_DEBUG_SPHERE(vPosToMirror, 0.2, 255, 0, 0)
		DRAW_DEBUG_LINE(vOrigin, vPosToMirror, 255, 0, 0)
	ENDIF
	#ENDIF

	FLOAT fAngleBetween =  GET_ANGULAR_DIFFERENCE(fOriginHeading, fHeadingToTarget)
	CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_MIRROR_POSITION - fAngleBetween: ", fAngleBetween, " Rotate OriginForward: ", fAngleBetween*-1)
	fAngleDiff = fAngleBetween
	vOutMirroredPos = vOrigin + ROTATE_VECTOR_ABOUT_Z(vOriginTargetDir, fAngleBetween*-2) * fDistance
	
	#IF IS_DEBUG_BUILD
	IF bDebug
	DRAW_DEBUG_SPHERE(vOutMirroredPos, 0.2, 0, 255, 0)
	DRAW_DEBUG_LINE(vOrigin, vOutMirroredPos, 255, 0, 0)
	ENDIF
	#ENDIF
	
	FLOAT fHeadingToOriginHeadingDiff
	fHeadingToOriginHeadingDiff = GET_ANGULAR_DIFFERENCE(fOriginHeading, fHeadingToMirror)
	VECTOR vMirroredHeadingDir
	vMirroredHeadingDir = ROTATE_VECTOR_ABOUT_Z(vOriginDir, -fHeadingToOriginHeadingDiff)
	
	#IF IS_DEBUG_BUILD
	IF bDebug
	DRAW_DEBUG_LINE(vPosToMirror, vPosToMirror +  ROTATE_VECTOR_ABOUT_Z(<<0.0, 1.0, 0.0>>, fHeadingToMirror) * 5.0, 255, 0, 0)
	DRAW_DEBUG_LINE(vOutMirroredPos, vOutMirroredPos +  vMirroredHeadingDir * 5.0, 0, 255, 0)
	ENDIF
	#ENDIF
	
	vOutMirrorHeading = GET_HEADING_FROM_VECTOR_2D(vMirroredHeadingDir.x, vMirroredHeadingDir.y)
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL CUTSCENE_HELP_VEH_PASSENGERS_CLONE(VEHICLE_INDEX &vehToCloneFrom, VEHICLE_INDEX &vehToCloneTo, PED_INDEX &vehPassengers[], BOOL bCloneOnlyIfPlayer = FALSE, BOOL bWaitForHeadBlend = FALSE, BOOL bCloneDeadPeds = FALSE, BOOL bFrezeBelowPedToClone = FALSE, BOOL bOnlyCloneDriver = FALSE, BOOL bLinkBlends = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehToCloneFrom)
		ASSERTLN("[CUTSCENE_HELP] CUTSCENE_HELP_VEH_PASSANGERS_CLONE - Unable to clone passangers, no vehToCloneFrom exist.")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehToCloneTo)
		ASSERTLN("[CUTSCENE_HELP] CUTSCENE_HELP_VEH_PASSANGERS_CLONE - Unable to clone passangers, no vehToCloneTo exist.")
		RETURN FALSE
	ENDIF
		
	//Create clones of players inside the vehicle.
	INT iSeat
	INT iPedIndex
	INT iLoopEnd = ENUM_TO_INT(VS_EXTRA_RIGHT_3)
	VEHICLE_SEAT eVehSeat
	PED_INDEX pedInSeat
	
	IF bOnlyCloneDriver
		iLoopEnd = ENUM_TO_INT(VS_DRIVER)
	ENDIF
	
	FOR iSeat = VS_DRIVER TO iLoopEnd
		iPedIndex = iSeat + 1
		eVehSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
		IF NOT IS_VEHICLE_SEAT_FREE(vehToCloneFrom, eVehSeat)
			pedInSeat = GET_PED_IN_VEHICLE_SEAT(vehToCloneFrom, eVehSeat)
			IF DOES_ENTITY_EXIST(pedInSeat)
			AND (IS_ENTITY_ALIVE(pedInSeat) OR bCloneDeadPeds)
				CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEH_PASSENGERS_CLONE - iSeat: ", iSeat)
				IF bCloneOnlyIfPlayer
				AND NOT IS_PED_A_PLAYER(pedInSeat)
					CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEH_PASSENGERS_CLONE - RELOOP iSeat: ", iSeat)
					RELOOP
				ENDIF
				CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEH_PASSENGERS_CLONE - CLONE iSeat: ", iSeat)
				//Clone ped
				IF NOT DOES_ENTITY_EXIST(vehPassengers[iPedIndex])
					CUTSCENE_HELP_CLONE_PED(vehPassengers[iPedIndex], pedInSeat, bLinkBlends, bFrezeBelowPedToClone)
				ENDIF
				//make sure no one is already in the seat
				IF DOES_ENTITY_EXIST(vehPassengers[iPedIndex])
				AND (IS_ENTITY_ALIVE(vehPassengers[iPedIndex]) OR bCloneDeadPeds)
					
					IF NOT HAS_PED_HEAD_BLEND_FINISHED(vehPassengers[iPedIndex]) 
					OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(vehPassengers[iPedIndex])
						IF bWaitForHeadBlend
							CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEH_PASSENGERS_CLONE - CLONE in iSeat: ", iSeat, " has not finished blending yet")
							RETURN FALSE
						ENDIF
					ENDIF
					
					PED_INDEX pedInCloneSeat
					pedInCloneSeat = GET_PED_IN_VEHICLE_SEAT(vehToCloneTo, eVehSeat)
					IF NOT DOES_ENTITY_EXIST(pedInCloneSeat)
						IF IS_ENTITY_ALIVE(vehToCloneTo)
							FREEZE_ENTITY_POSITION(vehPassengers[iPedIndex], FALSE)
							SET_PED_INTO_VEHICLE(vehPassengers[iPedIndex], vehToCloneTo, eVehSeat)
						ENDIF
					ENDIF

					//Open door if turret door.
					IF iSeat = VS_INSURGENT_TURRET
					AND GET_ENTITY_MODEL(vehToCloneFrom) = INSURGENT3
						SET_VEHICLE_DOOR_CONTROL(vehToCloneTo, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_HELP_VEH_PASSENGERS_CLONE_FROM_PLAYER_ARRAY(VEHICLE_INDEX &vehToCloneFrom, VEHICLE_INDEX &vehToCloneTo, PED_INDEX &vehPassengers[], PLAYER_INDEX &playersInVeh[CI_MAX_NUMBER_OF_PASSANGERS], BOOL bWaitForHeadBlend = FALSE)
	IF NOT DOES_ENTITY_EXIST(vehToCloneFrom)
		ASSERTLN("[CUTSCENE_HELP] CUTSCENE_HELP_VEH_PASSENGERS_CLONE_FROM_PLAYER_ARRAY - Unable to clone passangers, no vehToCloneFrom exist.")
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehToCloneTo)
		ASSERTLN("[CUTSCENE_HELP] CUTSCENE_HELP_VEH_PASSENGERS_CLONE_FROM_PLAYER_ARRAY - Unable to clone passangers, no vehToCloneTo exist.")
		RETURN FALSE
	ENDIF
		
	//Create clones of players inside the vehicle.
	INT iSeat
	INT iPedIndex
	VEHICLE_SEAT eVehSeat
	PED_INDEX pedInSeat
	FOR iSeat = VS_DRIVER TO VS_EXTRA_RIGHT_3
		iPedIndex = iSeat + 1
		eVehSeat = INT_TO_ENUM(VEHICLE_SEAT, iSeat)
		IF playersInVeh[iPedIndex] != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(playersInVeh[iPedIndex])
			pedInSeat = GET_PLAYER_PED(playersInVeh[iPedIndex])
			IF DOES_ENTITY_EXIST(pedInSeat)
				CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEH_PASSENGERS_CLONE_FROM_PLAYER_ARRAY - CLONE iSeat: ", iSeat)
				//Clone ped
				IF NOT DOES_ENTITY_EXIST(vehPassengers[iPedIndex])
					CUTSCENE_HELP_CLONE_PED(vehPassengers[iPedIndex], pedInSeat)
				ENDIF
				//make sure no one is already in the seat
				IF DOES_ENTITY_EXIST(vehPassengers[iPedIndex])
				AND IS_ENTITY_ALIVE(vehPassengers[iPedIndex])
					
					IF NOT HAS_PED_HEAD_BLEND_FINISHED(vehPassengers[iPedIndex]) 
					OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(vehPassengers[iPedIndex])
						IF bWaitForHeadBlend
							CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEH_PASSENGERS_CLONE_FROM_PLAYER_ARRAY - CLONE in iSeat: ", iSeat, " has not finished blending yet")
							RETURN FALSE
						ENDIF
					ELSE
					    FINALIZE_HEAD_BLEND(vehPassengers[iPedIndex])
					ENDIF
					
					PED_INDEX pedInCloneSeat
					pedInCloneSeat = GET_PED_IN_VEHICLE_SEAT(vehToCloneTo, eVehSeat)
					IF NOT DOES_ENTITY_EXIST(pedInCloneSeat)
						IF IS_ENTITY_ALIVE(vehToCloneTo)
							SET_PED_INTO_VEHICLE(vehPassengers[iPedIndex], vehToCloneTo, eVehSeat)
						ENDIF
					ENDIF

					//Open door if turret door.
					IF iSeat = VS_INSURGENT_TURRET
					AND GET_ENTITY_MODEL(vehToCloneFrom) = INSURGENT3
						SET_VEHICLE_DOOR_CONTROL(vehToCloneTo, SC_DOOR_BOOT, DT_DOOR_NO_RESET, 1.0)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CUTSCENE_HELP_CLONE_VEH_TRAILER(VEHICLE_INDEX &vehToClone, VEHICLE_INDEX &cloneTrailer)
	IF NOT DOES_ENTITY_EXIST(vehToClone)
		ASSERTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_TRAILER - Unable to clone, no vehToClone exists.")
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(cloneTrailer)
		PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_TRAILER - vehicle trailer clone already exists.")
		RETURN TRUE
	ENDIF
	
	VEHICLE_INDEX vehTrailer
	IF GET_VEHICLE_TRAILER_VEHICLE(vehToClone, vehTrailer)
		IF NOT IS_ENTITY_DEAD(vehTrailer)
			IF CREATE_VEHICLE_CLONE(cloneTrailer, vehTrailer, <<10.0, 10.0, 15.0>>, GET_ENTITY_HEADING(vehTrailer), FALSE, FALSE)
				PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_TRAILER - vehicle trailer clone created succesfully")
				
				IF DOES_ENTITY_EXIST(cloneTrailer)
					PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_TRAILER - Vehicle exists.")
					//Clone vehicle mods.
					VEHICLE_SETUP_STRUCT_MP vehicleSetup
					GET_VEHICLE_SETUP_MP(vehTrailer, vehicleSetup)
					SET_VEHICLE_SETUP_MP(cloneTrailer, vehicleSetup)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC VEHICLE_COPY_EXTRAS(VEHICLE_INDEX vehCloneFrom, VEHICLE_INDEX &vehCloneTo)
	IF DOES_ENTITY_EXIST(vehCloneFrom)
	AND NOT IS_ENTITY_DEAD(vehCloneFrom)
		CDEBUG1LN(DEBUG_CUTSCENE, "VEHICLE_COPY_EXTRAS - vehCloneFrom issue, Exist: ", DOES_ENTITY_EXIST(vehCloneFrom), " Dead: ", IS_ENTITY_DEAD(vehCloneFrom))
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehCloneTo)
	AND NOT IS_ENTITY_DEAD(vehCloneTo)
		CDEBUG1LN(DEBUG_CUTSCENE, "VEHICLE_COPY_EXTRAS - vehCloneFrom issue, Exist: ", DOES_ENTITY_EXIST(vehCloneTo), " Dead: ", IS_ENTITY_DEAD(vehCloneTo))
	ENDIF
	
	IF GET_ENTITY_MODEL(vehCloneFrom) <> GET_ENTITY_MODEL(vehCloneTo)
		ASSERTLN("VEHICLE_COPY_EXTRAS - trying to clone extras, vehicles are not the same model. vehCloneFrom: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehCloneFrom)), " vehCloneTo: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehCloneTo)))
	ENDIF
	
	INT iExtra
	FOR iExtra = 1 TO 14
		CDEBUG1LN(DEBUG_CUTSCENE, "VEHICLE_COPY_EXTRAS - iExtra ", iExtra, " exist: ", DOES_EXTRA_EXIST(vehCloneFrom, iExtra))
		IF DOES_EXTRA_EXIST(vehCloneFrom, iExtra)
			CDEBUG1LN(DEBUG_CUTSCENE, "On: ", IS_VEHICLE_EXTRA_TURNED_ON(vehCloneFrom, iExtra))
			SET_VEHICLE_EXTRA(vehCloneTo, iExtra, NOT IS_VEHICLE_EXTRA_TURNED_ON(vehCloneFrom, iExtra))
		ENDIF
	ENDFOR
ENDPROC

//Creating vehicle clone , and passangers, waiting one frame and then returning true.
//Frame wait required for mods to get applied.
FUNC BOOL CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(VEHICLE_INDEX &vehToClone, CUTSCENE_HELP_VEHICLE_CLONE &vehCloneStruct, CUTSCENE_HELP_VEHICLE_CLONE_FLAGS eFlags = chVEHICLE_CLONE_TRAILER)
	IF NOT DOES_ENTITY_EXIST(vehToClone)
	OR IS_ENTITY_DEAD(vehToClone)
		ASSERTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Unable to clone, no vehToClone exists. Dead? ", IS_ENTITY_DEAD(vehToClone))
		RETURN FALSE
	ENDIF
	
	//This is checked next frame, some vehicle mods require physics to be active to position them.
	IF DOES_ENTITY_EXIST(vehCloneStruct.vehicle)
		//Allows to skip one frame so that the vehicle physics can update.
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_FREEZE)
			FREEZE_ENTITY_POSITION(vehCloneStruct.vehicle, TRUE)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehCloneStruct.vehicle)
			SET_ENTITY_COORDS_NO_OFFSET(vehCloneStruct.vehicle, GET_ENTITY_COORDS(vehToClone))
			SET_ENTITY_ROTATION(vehCloneStruct.vehicle, GET_ENTITY_ROTATION(vehToClone))
		ENDIF
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_POSTIION_ON_GROUND)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehCloneStruct.vehicle)
		ENDIF
		
		IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_TRAILER)
			IF DOES_ENTITY_EXIST(vehCloneStruct.trailer)
			AND NOT IS_ENTITY_DEAD(vehCloneStruct.trailer)
				ATTACH_VEHICLE_TO_TRAILER(vehCloneStruct.vehicle, vehCloneStruct.trailer)
			ENDIF
		ENDIF
		
		RETURN TRUE
	ELSE
		IF CREATE_VEHICLE_CLONE(vehCloneStruct.vehicle, vehToClone, <<10.0, 20.0, 15.0>>, GET_ENTITY_HEADING(vehToClone), FALSE, FALSE)
			PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Delivery vehicle clone created succesfully")
			
			IF DOES_ENTITY_EXIST(vehCloneStruct.vehicle)
				PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Vehicle exists.")
				//Clone vehicle mods.
				VEHICLE_SETUP_STRUCT_MP vehicleSetup
				GET_VEHICLE_SETUP_MP(vehToClone, vehicleSetup)
				SET_VEHICLE_SETUP_MP(vehCloneStruct.vehicle, vehicleSetup)
				//clone extras
				VEHICLE_COPY_EXTRAS(vehToClone, vehCloneStruct.vehicle)
				SET_ENTITY_COLLISION(vehCloneStruct.vehicle, FALSE)
				SET_ENTITY_VISIBLE(vehCloneStruct.vehicle, FALSE)
				SET_ENTITY_INVINCIBLE(vehCloneStruct.vehicle, TRUE)
				SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehCloneStruct.vehicle, FALSE)

				MODEL_NAMES eModel = GET_ENTITY_MODEL(vehCloneStruct.vehicle)

				IF NOT IS_THIS_MODEL_A_BIKE(eModel)
				AND NOT IS_THIS_MODEL_A_BICYCLE(eModel)
				AND NOT IS_THIS_MODEL_A_BOAT(eModel)
				AND NOT IS_THIS_MODEL_A_SEA_VEHICLE(eModel)
				AND NOT (eModel = OPPRESSOR)
				#IF FEATURE_HEIST_ISLAND
				AND NOT (eModel = KOSATKA)
				#ENDIF
					// This should only be called for vehicles that are not 
					// bikes, bicycles, boats or submarines.
					SET_VEHICLE_CAN_BREAK(vehCloneStruct.vehicle, FALSE)
					PRINTLN("CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Preventing vehicle from being able to break apart.")
				ELSE
					PRINTLN("CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Vehicle type cannot be set as breakable.")
				ENDIF
				
				//LIGHTS
				INT iOn, iFullBeam
				GET_VEHICLE_LIGHTS_STATE(vehToClone, iOn, iFullBeam)
				CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - On: ", iOn, " FullBeam: ", iFullBeam)
				IF iOn <> 0
					SET_VEHICLE_LIGHTS(vehCloneStruct.vehicle, FORCE_VEHICLE_LIGHTS_ON)
				ENDIF
				
				IF iFullBeam <> 0
					SET_VEHICLE_FULLBEAM(vehCloneStruct.vehicle, TRUE)
				ENDIF
				
				SET_VEHICLE_ENGINE_ON(vehCloneStruct.vehicle, TRUE, TRUE)
				
				//DAMAGE
				//COPY_VEHICLE_DAMAGES(vehToCloneFrom, vehToCloneTo)
				CDEBUG1LN(DEBUG_CUTSCENE, "IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS): ", IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS))
				CDEBUG1LN(DEBUG_CUTSCENE, "IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_DEAD_PED_PASSENGERS): ", IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_DEAD_PED_PASSENGERS))
				//SeatId + 1 = index of the player in passengersarray
				IF NOT IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_DO_NOT_CLONE_PASSENGERS)
					CUTSCENE_HELP_VEH_PASSENGERS_CLONE(vehToClone, vehCloneStruct.vehicle, vehCloneStruct.vehPassengers, IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS), IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_WAIT_FOR_HEAD_BLEND), IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_DEAD_PED_PASSENGERS), NOT IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_DO_NOT_LINK_BLENDS))
				ENDIF
				//Trailer
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_TRAILER)
					CUTSCENE_HELP_CLONE_VEH_TRAILER(vehToClone, vehCloneStruct.trailer)
					
					IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_FIX_TIRES)
						INT iWheel
						FOR iWheel = SC_WHEEL_CAR_FRONT_LEFT TO SC_WHEEL_BIKE_REAR
							IF IS_VEHICLE_TYRE_BURST(vehCloneStruct.vehicle, SC_WHEEL_CAR_FRONT_LEFT)
								SET_VEHICLE_TYRE_FIXED(vehCloneStruct.vehicle, INT_TO_ENUM(SC_WHEEL_LIST, iWheel))
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
				
				IF IS_BITMASK_ENUM_AS_ENUM_SET(eFlags, chVEHICLE_CLONE_FIX_TIRES)
					INT iWheel
					FOR iWheel = SC_WHEEL_CAR_FRONT_LEFT TO SC_WHEEL_BIKE_REAR
						IF IS_VEHICLE_TYRE_BURST(vehCloneStruct.vehicle, SC_WHEEL_CAR_FRONT_LEFT)
							SET_VEHICLE_TYRE_FIXED(vehCloneStruct.vehicle, INT_TO_ENUM(SC_WHEEL_LIST, iWheel))
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ELSE
			PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS - Unable to create Player vehicle clone.")
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    When clone is created via CUTSCENE_HELP_CLONE_VEH_AND_PASSANGERS, the vehicle and passangers might not be ready yet / streamed in.
///    All assets are set to be invisible and frozen until stream is finished, this func is called on craetion with - false.
///    When ready to paly cutscene, toggle assets ready - true.
///    
/// PARAMS:
///    vehCloneStruct - 
///    bReady - 
FUNC BOOL CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_TOGGLE_READY(CUTSCENE_HELP_VEHICLE_CLONE &vehCloneStruct, BOOL bReady)
	IF DOES_ENTITY_EXIST(vehCloneStruct.vehicle)
		SET_ENTITY_VISIBLE(vehCloneStruct.vehicle, bReady)
		SET_ENTITY_COLLISION(vehCloneStruct.vehicle, bReady)
		FREEZE_ENTITY_POSITION(vehCloneStruct.vehicle, NOT bReady)
		
		IF DOES_ENTITY_EXIST(vehCloneStruct.trailer)
			SET_ENTITY_VISIBLE(vehCloneStruct.trailer, bReady)
			SET_ENTITY_COLLISION(vehCloneStruct.trailer, bReady)
			FREEZE_ENTITY_POSITION(vehCloneStruct.trailer, NOT bReady)
		ENDIF
		
		INT i
		REPEAT COUNT_OF(vehCloneStruct.vehPassengers) i
			IF DOES_ENTITY_EXIST(vehCloneStruct.vehPassengers[i])
				SET_ENTITY_VISIBLE(vehCloneStruct.vehPassengers[i], bReady)
			ENDIF
		ENDREPEAT
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CUTSCENE_HELP_HAS_PED_ARRAY_LOADING_FINISHED(PED_INDEX &pedArray[])
	INT iPedArrayCount
	iPedArrayCount = COUNT_OF(pedArray)
	
	BOOL bLoadingFinished
	bLoadingFinished = TRUE
	
	INT index
	REPEAT iPedArrayCount index
		CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_HAS_PED_LOADING_FINISHED - index: ", index, 
		" Exist: ", DOES_ENTITY_EXIST(pedArray[index]))
		IF DOES_ENTITY_EXIST(pedArray[index])
			CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_HAS_PED_LOADING_FINISHED - Dead: " , IS_ENTITY_DEAD(pedArray[index]),
			" Streamed: ", HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedArray[index]),
			" HeadBlend: ", HAS_PED_HEAD_BLEND_FINISHED(pedArray[index]))
		ENDIF
		IF IS_ENTITY_ALIVE(pedArray[index])
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedArray[index])
			OR NOT HAS_PED_HEAD_BLEND_FINISHED(pedArray[index])
				bLoadingFinished = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bLoadingFinished
ENDFUNC

//Check if all assets for vehicle and passengers streamed in.
FUNC BOOL CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_STREAM_FINISHED(CUTSCENE_HELP_VEHICLE_CLONE &vehCloneStruct)
	
	IF NOT CUTSCENE_HELP_HAS_PED_ARRAY_LOADING_FINISHED(vehCloneStruct.vehPassengers)
		PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_STREAM_FINISHED - Not all peds loaded in.")
		RETURN FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehCloneStruct.vehicle)					
	AND IS_VEHICLE_DRIVEABLE(vehCloneStruct.vehicle)
		IF NOT HAVE_VEHICLE_MODS_STREAMED_IN(vehCloneStruct.vehicle)
			PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_STREAM_FINISHED - Not all vehicle mods streamed in.")
			RETURN FALSE
		ENDIF
	ENDIF		

	RETURN TRUE
ENDFUNC

PROC CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(CUTSCENE_HELP_VEHICLE_CLONE &vehCloneStruct)
	
	INT i
	REPEAT COUNT_OF(vehCloneStruct.vehPassengers) i
		IF DOES_ENTITY_EXIST(vehCloneStruct.vehPassengers[i])
			DELETE_PED(vehCloneStruct.vehPassengers[i])
			PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP - deletePed: ID: ", i)
		ENDIF
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(vehCloneStruct.trailer)
		DELETE_VEHICLE(vehCloneStruct.trailer)
		PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP - trailer deleted")
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehCloneStruct.vehicle)
		DELETE_VEHICLE(vehCloneStruct.vehicle)
		PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP - vehicle deleted")
	ENDIF
	
	PRINTLN("[CUTSCENE_HELP] CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP - finish!")
ENDPROC

PROC CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND(VEHICLE_INDEX &vehicle, VECTOR vCoord, FLOAT fHeading, FLOAT fBackupZ = 0.0, BOOL bForceFallBack = FALSE)
	IF NOT IS_ENTITY_ALIVE(vehicle)
		CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - vehicle is not alive.")
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - vCoord: ", vCoord, " fBackupZ: ", fBackupZ, " bForceFallBack: ", bForceFallBack)
	SET_ENTITY_HEADING(vehicle, fHeading)
	SET_ENTITY_COORDS(vehicle, vCoord)
	#IF IS_DEBUG_BUILD
	VECTOR vCurrentVehCoord
	vCurrentVehCoord = GET_ENTITY_COORDS(vehicle)
	#ENDIF
	CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - after position veh coord is: ", vCurrentVehCoord)
	
	BOOL bUseFallBack
	bUseFallBack = FALSE
	
	IF bForceFallBack
		bUseFallBack = TRUE
		CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - bForceFallBack TRUE")
	ELSE
		IF SET_VEHICLE_ON_GROUND_PROPERLY(vehicle)
			#IF IS_DEBUG_BUILD
			vCurrentVehCoord = GET_ENTITY_COORDS(vehicle)
			#ENDIF
			CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - vehicle was positioned on ground properly, new Coord: ", vCurrentVehCoord)
		ELSE
			bUseFallBack = TRUE
			CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - failed to SET_VEHICLE_ON_GROUND_PROPERLY, try fallback")
		ENDIF
	ENDIF
	
	IF bUseFallBack
		IF fBackupZ <> 0.0
			VECTOR vFallBackCoord
			vFallBackCoord = vCoord
			vFallBackCoord.z = fBackupZ
			CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - failed to SET_VEHICLE_ON_GROUND_PROPERLY, vFallBackCoord: ", vFallBackCoord)
			
			vFallBackCoord = GET_GROUND_COORD_ADJUSTED_FOR_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehicle), vFallBackCoord)
			CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - failed to SET_VEHICLE_ON_GROUND_PROPERLY, vFallBackCoord, adjusted: ", vFallBackCoord)
			SET_ENTITY_COORDS_NO_OFFSET(vehicle, vFallBackCoord - <<0.0, 0.0, 1.0>>)
			#IF IS_DEBUG_BUILD
			vCurrentVehCoord = GET_ENTITY_COORDS(vehicle)
			#ENDIF
			CDEBUG1LN(DEBUG_CUTSCENE, "[CUTSCENE_HELP] CUTSCENE_HELP_POSITION_VEHICLE_ON_GROUND - failed to SET_VEHICLE_ON_GROUND_PROPERLY, vCurrentVehCoord, end: ", vCurrentVehCoord)
		ENDIF
	ENDIF
ENDPROC

STRUCT VEH_STRAIGHT_LINE_STRUCT
	BOOL bInitialized
	VECTOR vStartPos
	VECTOR vEndPos
	FLOAT fDistaToEnd
	
	FLOAT fProgress
	//FLOAT fSpeed
	FLOAT fCurrentSpeed
	FLOAT fTimeElapsed
ENDSTRUCT

FUNC BOOL CUTSCENE_HELP_MOVE_VEH_STRAIGHT_LINE(VEHICLE_INDEX &veh, FLOAT fDistance, FLOAT fTimeToReach, VEH_STRAIGHT_LINE_STRUCT &sInfo)
	IF NOT DOES_ENTITY_EXIST(veh)
		ASSERTLN("CUTSCENE_HELP_MOVE_VEH_STRAIGHT_LINE - vehicle does not exist")
	ENDIF
	
	IF fTimeToReach <= 0
		ASSERTLN("CUTSCENE_HELP_MOVE_VEH_STRAIGHT_LINE - Time to reach target can not be 0 or negative")
	ENDIF
	
	FLOAT fDistFromStart
	FLOAT fDistToTarget
	VECTOR vVehiclePos
	FLOAT fSpeedRequiredToReach
	
	vVehiclePos = GET_ENTITY_COORDS(veh)
	
	//INITIALIZE
	IF NOT sInfo.bInitialized		
		sInfo.vStartPos = vVehiclePos
		sInfo.vEndPos = vVehiclePos + GET_ENTITY_FORWARD_VECTOR(veh) * fDistance
		sInfo.fDistaToEnd = GET_DISTANCE_BETWEEN_COORDS(sInfo.vStartPos, sInfo.vEndPos, FALSE)
		sInfo.bInitialized = TRUE
	ENDIF
	
	fDistFromStart = GET_DISTANCE_BETWEEN_COORDS(vVehiclePos, sInfo.vStartPos, FALSE)
	fDistToTarget = GET_DISTANCE_BETWEEN_COORDS(vVehiclePos, sInfo.vEndPos, FALSE)
	fSpeedRequiredToReach = fDistToTarget / fTimeToReach
	
	IF sInfo.fCurrentSpeed < fSpeedRequiredToReach
		sInfo.fCurrentSpeed += GET_VEHICLE_ACCELERATION(veh)
	ENDIF
	
	VECTOR vVel
	vVel = GET_ENTITY_VELOCITY(veh)
	SET_VEHICLE_FORWARD_SPEED_XY(veh, sInfo.fCurrentSpeed)
	VECTOR vVel2
	vVel2 = GET_ENTITY_VELOCITY(veh)
	vVel2.z = vVel.z
	SET_ENTITY_VELOCITY(veh, vVel2)
	//Left
	sInfo.fProgress = (sInfo.fDistaToEnd - fDistToTarget) / 100 * sInfo.fDistaToEnd

	//Time
	sInfo.fTimeElapsed += GET_FRAME_TIME()
	
	IF fDistFromStart >= sInfo.fDistaToEnd
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_MOVE_VEH_STRAIGHT_LINE - reached target")
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_MOVE_VEH_STRAIGHT_LINE - sInfo.fProgress: ", sInfo.fProgress, " sInfo.fCurrentSpeed: ", sInfo.fCurrentSpeed, " sInfo.fTimeElapsed: ", sInfo.fTimeElapsed, " fSpeedRequiredToReach: ", fSpeedRequiredToReach)
	RETURN FALSE
ENDFUNC

PROC CUTSCE_HELP_PED_ARRAY_SET_LOD_MULTIPLIER(PED_INDEX &pedArray[], FLOAT lodMultiplier)
	INT index
	REPEAT COUNT_OF(pedArray) index
		IF DOES_ENTITY_EXIST(pedArray[index])
		AND NOT IS_ENTITY_DEAD(pedArray[index])
			SET_PED_LOD_MULTIPLIER(pedArray[index], lodMultiplier)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SIMPLE_CUTSCENE_TRANSFORM(VECTOR vSceneCurrentPosition, VECTOR vSceneCurrentRotation, SIMPLE_CUTSCENE &cutscene, VECTOR vSceneNewPosition, VECTOR vSceneNewRotation, BOOL bPrintDebug = FALSE)
	UNUSED_PARAMETER(bPrintDebug)
	
	OBJECT_INDEX objArray[3]
	
	INT index
	REPEAT 3 index
		objArray[index] = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 10.0>>, FALSE, FALSE, TRUE)
		IF DOES_ENTITY_EXIST(objArray[index])
			SET_ENTITY_COLLISION(objArray[index], FALSE)
			SET_ENTITY_VISIBLE(objArray[index], FALSE)
			SET_ENTITY_INVINCIBLE(objArray[index], TRUE)
		ELSE
			ASSERTLN("SIMPLE_CUTSCENE_TRANSFORM - objBase was not created: ", index)
			EXIT
		ENDIF
	ENDREPEAT
			
	VECTOR vRotEnd
	VECTOR vRotStart
	VECTOR vOff1, vOff2
	INT iScene
	
	FOR iScene = 0 TO cutscene.iScenesCount -1

		SET_ENTITY_COORDS_NO_OFFSET(objArray[0], vSceneCurrentPosition)
		SET_ENTITY_ROTATION(objArray[0], vSceneCurrentRotation)
		
		#IF IS_DEBUG_BUILD
		IF bPrintDebug
			CDEBUG1LN(DEBUG_PROPERTY, "")
			CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_TRANSFORM_CUTSCENE, sCutscene.iScenesCount: ", cutscene.iScenesCount, " iScene: ", iScene, " Before: vCamCoordsStart: ", cutscene.sScenes[iScene].vCamCoordsStart, " vCamRotStart: ", cutscene.sScenes[iScene].vCamRotStart,
			" vCamCoordsFinish: ", cutscene.sScenes[iScene].vCamCoordsFinish, " vCamRotFinish: ", cutscene.sScenes[iScene].vCamRotFinish)
		ENDIF
		#ENDIF
		
		vRotStart.x = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.x, cutscene.sScenes[iScene].vCamRotStart.x)
		vRotStart.y = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.y, cutscene.sScenes[iScene].vCamRotStart.y)
		vRotStart.z = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.z, cutscene.sScenes[iScene].vCamRotStart.z)
		
		vRotEnd.x = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.x, cutscene.sScenes[iScene].vCamRotFinish.x)
		vRotEnd.y = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.y, cutscene.sScenes[iScene].vCamRotFinish.y)
		vRotEnd.z = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.z, cutscene.sScenes[iScene].vCamRotFinish.z)
		
		//IF NOT bOffset
		vOff1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objArray[0], cutscene.sScenes[iScene].vCamCoordsStart)
		vOff2 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objArray[0], cutscene.sScenes[iScene].vCamCoordsFinish)

		SET_ENTITY_COORDS_NO_OFFSET(objArray[0], vSceneNewPosition)
		SET_ENTITY_ROTATION(objArray[0], vSceneNewRotation)
		ATTACH_ENTITY_TO_ENTITY(objArray[1], objArray[0], -1, vOff1, vRotStart)
		ATTACH_ENTITY_TO_ENTITY(objArray[2], objArray[0], -1, vOff2, vRotEnd)

		#IF IS_DEBUG_BUILD
			IF bPrintDebug
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objArray[0]), 0.2)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objArray[0]), GET_ENTITY_COORDS(objArray[0]) + GET_ENTITY_FORWARD_VECTOR(objArray[0]) * 10.0)
				
				DRAW_DEBUG_SPHERE(vSceneCurrentPosition, 0.2, 90, 125, 90)
				DRAW_DEBUG_SPHERE(vSceneNewPosition, 0.3, 90, 0, 0, 100)
				
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objArray[1]), 0.2, 255, 0, 0)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objArray[1]), GET_ENTITY_COORDS(objArray[1]) + GET_ENTITY_FORWARD_VECTOR(objArray[1]) * 10.0, 255, 0, 0)
				
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objArray[2]), 0.2, 0, 255, 0)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objArray[2]), GET_ENTITY_COORDS(objArray[2]) + GET_ENTITY_FORWARD_VECTOR(objArray[2]) * 10.0, 0, 200, 0)
			ENDIF
		#ENDIF
		
		cutscene.sScenes[iScene].vCamCoordsStart = GET_ENTITY_COORDS(objArray[1])
		cutscene.sScenes[iScene].vCamRotStart	= GET_ENTITY_ROTATION(objArray[1])
		
		cutscene.sScenes[iScene].vCamCoordsFinish = GET_ENTITY_COORDS(objArray[2])
		cutscene.sScenes[iScene].vCamRotFinish = GET_ENTITY_ROTATION(objArray[2])
		
		//Detach prepare for ext scene
		REPEAT 3 index
			IF DOES_ENTITY_EXIST(objArray[index])
				IF IS_ENTITY_ATTACHED(objArray[index])
					DETACH_ENTITY(objArray[index])
				ENDIF
			ENDIF
		ENDREPEAT
	ENDFOR
	
	//Cleanup
	REPEAT 3 index
		IF DOES_ENTITY_EXIST(objArray[index])
			IF IS_ENTITY_ATTACHED(objArray[index])
				DETACH_ENTITY(objArray[index])
			ENDIF
			DELETE_OBJECT(objArray[index])
		ENDIF
	ENDREPEAT
ENDPROC

PROC SIMPLE_TRANSFORM_MOVE(VECTOR vSceneCurrentPosition, VECTOR vSceneCurrentRotation, VECTOR &vEntityPosition, VECTOR &vEntityRotation, VECTOR vSceneNewPosition, VECTOR vSceneNewRotation, BOOL bPrintDebug = FALSE)
	UNUSED_PARAMETER(bPrintDebug)
	
	OBJECT_INDEX objArray[2]
	
	INT index
	REPEAT 2 index
		objArray[index] = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 10.0>>, FALSE, FALSE, TRUE)
		IF DOES_ENTITY_EXIST(objArray[index])
			SET_ENTITY_COLLISION(objArray[index], FALSE)
			SET_ENTITY_VISIBLE(objArray[index], FALSE)
			SET_ENTITY_INVINCIBLE(objArray[index], TRUE)
		ELSE
			ASSERTLN("SIMPLE_CUTSCENE_TRANSFORM - objBase was not created: ", index)
			EXIT
		ENDIF
	ENDREPEAT
			
	VECTOR vRotStart
	VECTOR vOff1
		SET_ENTITY_COORDS_NO_OFFSET(objArray[0], vSceneCurrentPosition)
		SET_ENTITY_ROTATION(objArray[0], vSceneCurrentRotation)

		vRotStart.x = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.x, vEntityRotation.x)
		vRotStart.y = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.y, vEntityRotation.y)
		vRotStart.z = GET_ANGULAR_DIFFERENCE(vSceneCurrentRotation.z, vEntityRotation.z)
		
		//IF NOT bOffset
		vOff1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(objArray[0], vEntityPosition)

		SET_ENTITY_COORDS_NO_OFFSET(objArray[0], vSceneNewPosition)
		SET_ENTITY_ROTATION(objArray[0], vSceneNewRotation)
		ATTACH_ENTITY_TO_ENTITY(objArray[1], objArray[0], -1, vOff1, vRotStart)

		#IF IS_DEBUG_BUILD
			IF bPrintDebug
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objArray[0]), 0.2)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objArray[0]), GET_ENTITY_COORDS(objArray[0]) + GET_ENTITY_FORWARD_VECTOR(objArray[0]) * 10.0)
				
				DRAW_DEBUG_SPHERE(vSceneCurrentPosition, 0.2, 90, 125, 90)
				DRAW_DEBUG_SPHERE(vSceneNewPosition, 0.3, 90, 0, 0, 100)
				
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(objArray[1]), 0.2, 255, 0, 0)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objArray[1]), GET_ENTITY_COORDS(objArray[1]) + GET_ENTITY_FORWARD_VECTOR(objArray[1]) * 10.0, 255, 0, 0)
				
			ENDIF
		#ENDIF
		
		vEntityPosition = GET_ENTITY_COORDS(objArray[1])
		vEntityRotation	= GET_ENTITY_ROTATION(objArray[1])

	//Cleanup
	REPEAT 2 index
		IF DOES_ENTITY_EXIST(objArray[index])
			IF IS_ENTITY_ATTACHED(objArray[index])
				DETACH_ENTITY(objArray[index])
			ENDIF
			DELETE_OBJECT(objArray[index])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Prepare vehicle for scene, 1 frame, call on 1st frmae of the scene.
///    start up the vehicle.
PROC CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(VEHICLE_INDEX &vehicle, BOOL bReady)
	IF NOT DOES_ENTITY_EXIST(vehicle)
	OR IS_ENTITY_DEAD(vehicle)
		ASSERTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - vehicle exist: ", DOES_ENTITY_EXIST(vehicle), " dead: ", IS_ENTITY_DEAD(vehicle))
		EXIT
	ELSE
		PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - toggle vehicle to: ", bReady)
	ENDIF
	
	MODEL_NAMES vehicleModel
	vehicleModel = GET_ENTITY_MODEL(vehicle)
	PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - vehicle model: ", GET_MODEL_NAME_FOR_DEBUG(vehicleModel))
	
	SET_VEHICLE_ENGINE_ON(vehicle, bReady, FALSE)
	
	IF IS_THIS_MODEL_A_HELI(vehicleModel)
	OR IS_THIS_MODEL_A_PLANE(vehicleModel)
		PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - Heli: ", IS_THIS_MODEL_A_HELI(vehicleModel), " Plane: ", IS_THIS_MODEL_A_PLANE(vehicleModel))
		IF bReady
			SET_HELI_BLADES_FULL_SPEED(vehicle)
		ELSE
			SET_HELI_BLADES_SPEED(vehicle, 0.0)
		ENDIF
		//Disable vertical lift, force horizontal movement.
		IF DOES_VEHICLE_HAVE_VERTICAL_LIFT(vehicle)
			PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - has vertical lift, forcing horizontal")
			SET_VEHICLE_FLIGHT_NOZZLE_POSITION_IMMEDIATE(vehicle, 0.0)
		ENDIF
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(vehicleModel)
		SET_HELI_TURBULENCE_SCALAR(vehicle, 0.0)
		SET_VEHICLE_DISABLE_TOWING(vehicle, TRUE)
	ENDIF
	
	IF GET_VEHICLE_HAS_LANDING_GEAR(vehicle)		
		PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - vehicle has landing gear, state: ", ENUM_TO_INT(GET_LANDING_GEAR_STATE(vehicle)))
		SWITCH GET_LANDING_GEAR_STATE(vehicle)
			CASE LGS_RETRACTING
			CASE LGS_DEPLOYING
			CASE LGS_LOCKED_UP
				PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - deploying landing gear.")
				CONTROL_LANDING_GEAR(vehicle, LGC_DEPLOY_INSTANT)
			BREAK
		ENDSWITCH
	ELSE
		PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - vehicle has no landing gear.")
	ENDIF
	
	PRINTLN("CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE - end")
ENDPROC

PROC CUTSCENE_HELP_TRANSFORM_CUTSCENE(TRANSFORM_STRUCT sCurrentReference, SIMPLE_CUTSCENE &sCutscene, TRANSFORM_STRUCT sNewReference)
	CDEBUG1LN(DEBUG_PROPERTY, "")
	CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - CUTSCENE_HELP_TRANSFORM_CUTSCENE, sCurrentReference.position: ", sCurrentReference.position, " sNewReference.position: ", sNewReference.position)

	OBJECT_INDEX obj[CONST_TRANSFORM_CUTSCENE_OBJ_COUNT]
	BOOL bFailedCreate
	bFailedCreate = FALSE
	
	INT iObject
	REPEAT CONST_TRANSFORM_CUTSCENE_OBJ_COUNT iObject
		obj[iObject] = CREATE_OBJECT_NO_OFFSET(prop_golf_ball, <<10.0, 10.0, 10.0>>, FALSE, FALSE, TRUE)
		IF DOES_ENTITY_EXIST(obj[iObject])
			SET_ENTITY_COLLISION(obj[iObject], FALSE)
			SET_ENTITY_VISIBLE(obj[iObject], FALSE)
			SET_ENTITY_INVINCIBLE(obj[iObject], TRUE)
		ELSE
			CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_TRANSFORM_CUTSCENE - obj[", iObject, "] was not created")
			bFailedCreate = TRUE
		ENDIF
	ENDREPEAT
	
	IF NOT bFailedCreate
		INT iScene
		VECTOR vRotEnd
		VECTOR vRotStart
		VECTOR vOff1, vOff2
		
		FOR iScene = 0 TO sCutscene.iScenesCount - 1

			SET_ENTITY_COORDS_NO_OFFSET(obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], sCurrentReference.Position)
			SET_ENTITY_ROTATION(obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], sCurrentReference.Rotation)
			
			CDEBUG1LN(DEBUG_PROPERTY, "")
			CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_TRANSFORM_CUTSCENE, sCutscene.iScenesCount: ", sCutscene.iScenesCount, " iScene: ", iScene, " Before: vCamCoordsStart: ", sCutscene.sScenes[iScene].vCamCoordsStart, " vCamRotStart: ", sCutscene.sScenes[iScene].vCamRotStart,
				" vCamCoordsFinish: ", sCutscene.sScenes[iScene].vCamCoordsFinish, " vCamRotFinish: ", sCutscene.sScenes[iScene].vCamRotFinish)
			
			vRotStart.x = GET_ANGULAR_DIFFERENCE(sCurrentReference.Rotation.x, sCutscene.sScenes[iScene].vCamRotStart.x)
			vRotStart.y = GET_ANGULAR_DIFFERENCE(sCurrentReference.Rotation.y, sCutscene.sScenes[iScene].vCamRotStart.y)
			vRotStart.z = GET_ANGULAR_DIFFERENCE(sCurrentReference.Rotation.z, sCutscene.sScenes[iScene].vCamRotStart.z)
			
			vRotEnd.x = GET_ANGULAR_DIFFERENCE(sCurrentReference.Rotation.x, sCutscene.sScenes[iScene].vCamRotFinish.x)
			vRotEnd.y = GET_ANGULAR_DIFFERENCE(sCurrentReference.Rotation.y, sCutscene.sScenes[iScene].vCamRotFinish.y)
			vRotEnd.z = GET_ANGULAR_DIFFERENCE(sCurrentReference.Rotation.z, sCutscene.sScenes[iScene].vCamRotFinish.z)
			
			//IF NOT bOffset
			vOff1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], sCutscene.sScenes[iScene].vCamCoordsStart)
			vOff2 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], sCutscene.sScenes[iScene].vCamCoordsFinish)

			SET_ENTITY_COORDS_NO_OFFSET(obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], sNewReference.Position)
			SET_ENTITY_ROTATION(obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], sNewReference.Rotation)
			ATTACH_ENTITY_TO_ENTITY(obj[CONST_TRANSFORM_CUTSCENE_OBJ_START], obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], -1, vOff1, vRotStart)
			ATTACH_ENTITY_TO_ENTITY(obj[CONST_TRANSFORM_CUTSCENE_OBJ_FINISH], obj[CONST_TRANSFORM_CUTSCENE_OBJ_BASE], -1, vOff2, vRotEnd)
			
			sCutscene.sScenes[iScene].vCamCoordsStart = GET_ENTITY_COORDS(obj[CONST_TRANSFORM_CUTSCENE_OBJ_START])
			sCutscene.sScenes[iScene].vCamRotStart	= GET_ENTITY_ROTATION(obj[CONST_TRANSFORM_CUTSCENE_OBJ_START])
			
			sCutscene.sScenes[iScene].vCamCoordsFinish = GET_ENTITY_COORDS(obj[CONST_TRANSFORM_CUTSCENE_OBJ_FINISH])
			sCutscene.sScenes[iScene].vCamRotFinish = GET_ENTITY_ROTATION(obj[CONST_TRANSFORM_CUTSCENE_OBJ_FINISH])
			
			REPEAT CONST_TRANSFORM_CUTSCENE_OBJ_COUNT iObject
				IF DOES_ENTITY_EXIST(obj[iObject])
					IF IS_ENTITY_ATTACHED(obj[iObject])
						DETACH_ENTITY(obj[iObject])
					ENDIF
				ENDIF
			ENDREPEAT
		ENDFOR
	ELSE
		CDEBUG1LN(DEBUG_PROPERTY, "CUTSCENE_HELP_TRANSFORM_CUTSCENE - not all objects were created, delete.")
	ENDIF
	
	REPEAT CONST_TRANSFORM_CUTSCENE_OBJ_COUNT iObject
		IF DOES_ENTITY_EXIST(obj[iObject])
			IF IS_ENTITY_ATTACHED(obj[iObject])
				DETACH_ENTITY(obj[iObject])
			ENDIF
			DELETE_OBJECT(obj[iObject])
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Distance between the front of the vehicle model and its anchor point.
/// PARAMS:
///    vehicle - 
///    bDrawDebug - 
/// RETURNS:
///    
FUNC FLOAT CUTSCENE_HELP_VEHICLE_DISTANCE_ANCHOR_TO_FRONT(VEHICLE_INDEX vehicle, BOOL bDrawDebug = FALSE)
	UNUSED_PARAMETER(bDrawDebug)
	
	IF NOT DOES_ENTITY_EXIST(vehicle)
		ASSERTLN("CUTSCENE_HELP_VEHICLE_DISTANCE_ANCHOR_MODEL_START - vehicle does not exist")
	ENDIF

	VECTOR vModelMin, vModelMax
	GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehicle), vModelMin, vModelMax)

	VECTOR vVehicleCoord
	vVehicleCoord = GET_ENTITY_COORDS(vehicle)
	
	#IF IS_DEBUG_BUILD
	IF bDrawDebug
		DRAW_DEBUG_SPHERE(vVehicleCoord, 1.5, 0, 0, 255, 125)
	ENDIF
	#ENDIF
	//Rotate dimension point to vehicle heading, world coord.
	//vModelMin = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vVehicleCoord, GET_ENTITY_HEADING(vehPlayer)*-1.0, vModelMin)
	vModelMax = GET_OFFSET_FROM_COORD_IN_WORLD_COORDS(vVehicleCoord, GET_ENTITY_HEADING(vehicle) * -1.0, vModelMax)
	
	//Project anchor infront of the vehicle.
	VECTOR  vProjected
	vProjected = PROJECT_POINT_ONTO_LINE(vVehicleCoord, vModelMax, vModelMax + CROSS_PRODUCT(GET_ENTITY_FORWARD_VECTOR(vehicle), <<0.0, 0.0, 1.0>>) * 1.0 )
	
	FLOAT fDistance
	fDistance = 0.0
	fDistance = GET_DISTANCE_BETWEEN_COORDS(vVehicleCoord, vProjected, FALSE)
	
	#IF IS_DEBUG_BUILD
	IF bDrawDebug
		DRAW_DEBUG_SPHERE(vProjected, 0.2,255,255,0)
		//DRAW_DEBUG_SPHERE(vModelMin, 0.2,255,0,0)
		DRAW_DEBUG_SPHERE(vModelMax, 0.2,0,255,0) //fprward
		DRAW_DEBUG_LINE(vVehicleCoord, vProjected, 255, 153,0,0)
		CDEBUG1LN(DEBUG_CUTSCENE, "CUTSCENE_HELP_VEHICLE_DISTANCE_ANCHOR_MODEL_START - Model: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehicle)), " vModelMin: ", vModelMin, " vModelMax: ", vModelMax, " Distance: ", fDistance)
	ENDIF
	#ENDIF
	
	RETURN fDistance
ENDFUNC

PROC CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(VEHICLE_INDEX &vehicle)
	IF NOT IS_ENTITY_ALIVE(vehicle)
		ASSERTLN("CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH - vehicle is not alive")
		EXIT
	ENDIF
	
	MODEL_NAMES vehicleModel
	vehicleModel = GET_ENTITY_MODEL(vehicle)
	CDEBUG1LN(DEBUG_SMPL_INTERIOR, "CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH - disable crash for: ", GET_MODEL_NAME_FOR_DEBUG(vehicleModel))
	
	IF IS_THIS_MODEL_A_HELI(vehicleModel)
	OR IS_THIS_MODEL_A_PLANE(vehicleModel)
		SET_DISABLE_AUTOMATIC_CRASH_TASK(vehicle, TRUE)
	ELSE
		CDEBUG1LN(DEBUG_SMPL_INTERIOR, "CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH - not a heli or a plane, do not disable crash task.")
	ENDIF
ENDPROC

FUNC TRANSFORM_STRUCT _TRANSFORM_VIA_OFFSET(TRANSFORM_STRUCT sBaseSceneTransform, TRANSFORM_STRUCT sBasePropTransform, TRANSFORM_STRUCT sNewSceneTransform, BOOL bPrintDebug = FALSE)
	TRANSFORM_STRUCT sOutTransform
	
	UNUSED_PARAMETER(bPrintDebug)
	
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
	CDEBUG1LN(DEBUG_PROPERTY, "")
	CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD - sNewSceneTransform.Pos: ", sNewSceneTransform.Position, " sNewSceneTransform.Rot: ", sNewSceneTransform.Rotation)
	CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD - sBaseSceneTransform.Pos: ", sBaseSceneTransform.Position, " sBaseSceneTransform.Rot: ", sBaseSceneTransform.Rotation)
	CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD - sBasePropTransform.Pos: ", sBasePropTransform.Position, " sBasePropTransform.Rot: ", sBasePropTransform.Rotation)
	ENDIF
	#ENDIF

	VECTOR vOffset = (sBasePropTransform.Position - sBaseSceneTransform.Position)
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
		CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD - vOffset: ", vOffset)
	ENDIF
	#ENDIF

//		TRANSFORM_STRUCT vRot
//	vRot = BUNKER_CUTSCENE_GET_DELIVERY_SCENE_TRANSFORM(eInterior)
//	
//	ROTATE_VECTOR_FMMC(sTransformOut.Position, vRot.Rotation)	
//	sTransformOut.Rotation = sTransformOut.Rotation + vRot.Rotation
	
	// set to default heading
	//vOffset = ROTATE_VECTOR_ABOUT_Z(vOffset, -sBaseSceneTransform.Rotation.z)
	VECTOR vBaseSceneRotation = sBaseSceneTransform.Rotation
	vBaseSceneRotation = <<vBaseSceneRotation.x *-1, vBaseSceneRotation.y *-1, vBaseSceneRotation.z*-1>>
	ROTATE_VECTOR_FMMC(vOffset, vBaseSceneRotation)
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
		CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD, default heading - vOffset: ", vOffset)
	ENDIF
	#ENDIF
	//rotate for new section
	ROTATE_VECTOR_FMMC(vOffset, sNewSceneTransform.Rotation)
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
		CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD, new scene heading - vOffset: ", vOffset)
	ENDIF
	#ENDIF
	//offset for new section location 
	sOutTransform.Position = sNewSceneTransform.Position + vOffset
	sOutTransform.Rotation = sBasePropTransform.Rotation

	sOutTransform.Rotation = NORMALISE_ROTATION_3D(sOutTransform.Rotation)
	sBaseSceneTransform.Rotation = NORMALISE_ROTATION_3D(sBaseSceneTransform.Rotation)
	sNewSceneTransform.Rotation = NORMALISE_ROTATION_3D(sNewSceneTransform.Rotation)
	
	sOutTransform.Rotation.x = sOutTransform.Rotation.x + (sNewSceneTransform.Rotation.x - sBaseSceneTransform.Rotation.x)
	sOutTransform.Rotation.y = sOutTransform.Rotation.y + (sNewSceneTransform.Rotation.y - sBaseSceneTransform.Rotation.y)	
	sOutTransform.Rotation.z = sOutTransform.Rotation.z + (sNewSceneTransform.Rotation.z - sBaseSceneTransform.Rotation.z)
	CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - sOutTransform.Rotation: ", sOutTransform.Rotation,
	" sBasePropTransform.Rotation: ", sBasePropTransform.Rotation, 
	" sBaseSceneTransform.Rotation: ", sBaseSceneTransform.Rotation, 
	" sNewSceneTransform.Rotation: ", sNewSceneTransform.Rotation)
	
	sOutTransform.Rotation = NORMALISE_ROTATION_3D(sOutTransform.Rotation)
	
		
	#IF IS_DEBUG_BUILD
	IF bPrintDebug
		CDEBUG1LN(DEBUG_PROPERTY, "[GR_BUNKER_CUTSCENE] - BUNKER_CUTSCENE_GET_PROP_WORLD_COORD, sOutTransform.Pos: ", sOutTransform.Position, " sOutTransform.Rot: ", sOutTransform.Rotation)
	ENDIF
	#ENDIF
	
	RETURN sOutTransform
ENDFUNC
