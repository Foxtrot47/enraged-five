USING "rage_builtins.sch"
USING "globals.sch"
USING "net_mission_trigger_overview.sch"
USING "net_ambient_manager_variables.sch"
USING "MP_globals_Tunables.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_ambient_manager.sch
//      CREATED         :   David Gentles
//      DESCRIPTION     :   Contains all routines required to run the MP ambient manager.
//		NOTES			:	
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
 
#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Returns a MP Ambient script ID as a string
/// PARAMS:
///    thisMPAMType - MP Ambient script ID string desired
/// RETURNS:
///    MPAM_TYPE_LIST as a string
FUNC STRING GET_MPAM_ENUM_ENTRY_STRING(MPAM_TYPE_LIST thisMPAMType)
	SWITCH thisMPAMType
		CASE MPAM_TYPE_CINEMA
			RETURN "MPAM_TYPE_CINEMA"
		BREAK
		CASE MPAM_TYPE_GANGHIDEOUT
			RETURN "MPAM_TYPE_GANGHIDEOUT"
		BREAK
		CASE MPAM_TYPE_RACETOPOINT
			RETURN "MPAM_TYPE_RACETOPOINT"
		BREAK
		CASE MPAM_TYPE_HOLDUP
			RETURN "MPAM_TYPE_HOLDUP"
		BREAK
		CASE MPAM_TYPE_CRATEDROP
			RETURN "MPAM_TYPE_CRATEDROP"
		BREAK
		CASE MPAM_TYPE_SECURITYVAN
			RETURN "MPAM_TYPE_SECURITYVAN"
		BREAK
		CASE MPAM_TYPE_IMPORTEXPORT
			RETURN "MPAM_TYPE_IMPORTEXPORT"
		BREAK
		CASE MPAM_TYPE_STRIPCLUB
			RETURN "MPAM_TYPE_STRIPCLUB"
		BREAK
		CASE MPAM_TYPE_HOLD_UP_TUT
			RETURN "MPAM_TYPE_HOLD_UP_TUT"
		BREAK
		CASE MPAM_TYPE_JOYRIDER
			RETURN "MPAM_TYPE_JOYRIDER"
		BREAK
		CASE MPAM_TYPE_CAR_MOD_TUT
			RETURN "MPAM_TYPE_CAR_MOD_TUT"
		BREAK
		CASE MPAM_TYPE_GAMEDIR_MISSION
			RETURN "MPAM_TYPE_GAMEDIR_MISSION"
		BREAK
		CASE MPAM_TYPE_LESTER_CUTSCENE
			RETURN "MPAM_TYPE_LESTER_CUTSCENE"
		BREAK
		CASE MPAM_TYPE_TREVOR_CUTSCENE
			RETURN "MPAM_TYPE_TREVOR_CUTSCENE"
		BREAK
		CASE MPAM_TYPE_PLANE_TAKEDOWN
			RETURN "MPAM_TYPE_PLANE_TAKEDOWN"
		BREAK
		CASE MPAM_TYPE_DESTROY_VEH
			RETURN "MPAM_TYPE_DESTROY_VEH"
		BREAK
		CASE MPAM_TYPE_DISTRACT_COPS
			RETURN "MPAM_TYPE_DISTRACT_COPS"
		BREAK
		CASE MPAM_TYPE_MOVING_TARGET
			RETURN "MPAM_TYPE_MOVING_TARGET"
		BREAK
		CASE MPAM_TYPE_KILL_LIST
			RETURN "MPAM_TYPE_KILL_LIST"
		BREAK
		CASE MPAM_TYPE_AIR_DROP
			RETURN "MPAM_TYPE_AIR_DROP"
		BREAK
		CASE MPAM_TYPE_TIME_TRIAL
			RETURN "MPAM_TYPE_TIME_TRIAL"
		BREAK
		CASE MPAM_TYPE_RC_TIME_TRIAL
			RETURN "MPAM_TYPE_RC_TIME_TRIAL"
		BREAK
		CASE MPAM_TYPE_CP_COLLECTION
			RETURN "MPAM_TYPE_CP_COLLECTION"
		BREAK
		CASE MPAM_TYPE_CHALLENGES
			RETURN "MPAM_TYPE_CHALLENGES"
		BREAK
	ENDSWITCH
		
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Creates widgets for the MP ambient manager
PROC CREATE_MP_AMBIENT_MANAGER_WIDGETS()
	INT iMPAMRepeat = 0
	
	START_WIDGET_GROUP("MP Ambient Manager")
		START_WIDGET_GROUP("MP Ambient List")
			iMPAMRepeat = 0
			REPEAT MPAM_TYPE_LIST_SIZE iMPAMRepeat
				ADD_WIDGET_BOOL(GET_MPAM_ENUM_ENTRY_STRING(INT_TO_ENUM(MPAM_TYPE_LIST, iMPAMRepeat)), g_MPAMData.bDebugCurrentlyInType[iMPAMRepeat])
			ENDREPEAT
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Peds")
			ADD_WIDGET_INT_READ_ONLY("Reserved", g_MPAMData.iDebugReservedPeds)
			ADD_WIDGET_INT_READ_ONLY("Created", g_MPAMData.iDebugCreatedPeds)
			ADD_WIDGET_INT_READ_ONLY("Highest in Session", g_MPAMData.iHighestPlayerPedReserve)
			ADD_WIDGET_INT_READ_ONLY("Maximum", g_MaxNumReservePeds)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Vehicles")
			ADD_WIDGET_INT_READ_ONLY("Reserved", g_MPAMData.iDebugReservedVehicles)
			ADD_WIDGET_INT_READ_ONLY("Created", g_MPAMData.iDebugCreatedVehicles)
			ADD_WIDGET_INT_READ_ONLY("Highest in Session", g_MPAMData.iHighestPlayerVehicleReserve)
			ADD_WIDGET_INT_READ_ONLY("Maximum", g_MaxNumReserveVehicles)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Objects")
			ADD_WIDGET_INT_READ_ONLY("Reserved", g_MPAMData.iDebugReservedObjects)
			ADD_WIDGET_INT_READ_ONLY("Created", g_MPAMData.iDebugCreatedObjects)
			ADD_WIDGET_INT_READ_ONLY("Highest in Session", g_MPAMData.iHighestPlayerObjectReserve)
			ADD_WIDGET_INT_READ_ONLY("Maximum", g_MaxNumReserveObjects)
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Pickups")
			ADD_WIDGET_INT_READ_ONLY("Reserved", g_MPAMData.iDebugReservedPickups)
			ADD_WIDGET_INT_READ_ONLY("Created", g_MPAMData.iDebugCreatedPickups)
			ADD_WIDGET_INT_READ_ONLY("Maximum", g_MaxNumReservePickups)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
///    Gets the MP Ambient entity type ID as a string
/// PARAMS:
///    typeEnt - MP Ambient entity type ID string desired
/// RETURNS:
///    PRIVATE_MPAM_ENTITY_TYPES as a string
FUNC STRING DEBUG_PRIVATE_MPAM_ENTITY_TYPE_NAME(PRIVATE_MPAM_ENTITY_TYPES typeEnt)
	
	SWITCH typeEnt
		CASE PRIV_MPAM_PED
			RETURN "PED"
		BREAK
		CASE PRIV_MPAM_VEH
			RETURN "VEHICLE"
		BREAK
		CASE PRIV_MPAM_OBJ
			RETURN "OBJECT"
		BREAK
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

#ENDIF

// ===========================================================================================================
//      The Main MP Ambient Manager Routines
// ===========================================================================================================

/// PURPOSE:
///    Gets the number of entities of a specified type reserved by either this script, or all scripts
/// PARAMS:
///    typeEnt - entity type to get the amount of
///    bForThisScript - defines either getting the amount reserved by this script, or all scripts
/// RETURNS:
///    number of entities reserved
FUNC INT PRIVATE_MPAM_GET_NUM_RESERVED(PRIVATE_MPAM_ENTITY_TYPES typeEnt, BOOL bForThisScript)
	INT iLocalPlayer = NATIVE_TO_INT(PLAYER_ID())
	SWITCH typeEnt
		CASE PRIV_MPAM_PED
			IF NOT NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
				RETURN GlobalplayerBD[iLocalPlayer].iNumReservedPeds
			ELSE
				RETURN GET_NUM_RESERVED_MISSION_PEDS(NOT bForThisScript)
			ENDIF
		BREAK
		CASE PRIV_MPAM_VEH
			IF NOT NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
				RETURN GlobalplayerBD[iLocalPlayer].iNumReservedVehicles
			ELSE
				RETURN GET_NUM_RESERVED_MISSION_VEHICLES(NOT bForThisScript)
			ENDIF
		BREAK
		CASE PRIV_MPAM_OBJ
			IF NOT NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
				RETURN GlobalplayerBD[iLocalPlayer].iNumReservedObjects
			ELSE
				RETURN GET_NUM_RESERVED_MISSION_OBJECTS(NOT bForThisScript)
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the maximum amount of each entity type that can be reserved at one time
/// PARAMS:
///    typeEnt - entity type to return 
/// RETURNS:
///    maximum amount reserved
FUNC INT PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIVATE_MPAM_ENTITY_TYPES typeEnt)
	SWITCH typeEnt
		CASE PRIV_MPAM_PED
			RETURN g_MaxNumReservePeds
		BREAK
		CASE PRIV_MPAM_VEH
			RETURN g_MaxNumReserveVehicles
		BREAK
		CASE PRIV_MPAM_OBJ
			RETURN g_MaxNumReserveObjects
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the highest amount reserved of an entity type by any player in the session
/// PARAMS:
///    typeEnt - entity type to return
/// RETURNS:
///    maximum amount reserved by any one player
FUNC INT PRIVATE_MPAM_GET_HIGHEST_PLAYER_RESERVED(PRIVATE_MPAM_ENTITY_TYPES typeEnt)
	SWITCH typeEnt
		CASE PRIV_MPAM_PED
			RETURN g_MPAMData.iHighestPlayerPedReserve
		BREAK
		CASE PRIV_MPAM_VEH
			RETURN g_MPAMData.iHighestPlayerVehicleReserve
		BREAK
		CASE PRIV_MPAM_OBJ
			RETURN g_MPAMData.iHighestPlayerObjectReserve
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the amount to keep spare of each entity type for ambient world population
/// PARAMS:
///    typeEnt - entity type to return
/// RETURNS:
///    amount of entity type to keep spare
FUNC INT PRIVATE_MPAM_GET_AMBIENT_POPULATION_REQUIREMENT(PRIVATE_MPAM_ENTITY_TYPES typeEnt)
	SWITCH typeEnt
		CASE PRIV_MPAM_PED
			RETURN 10
		BREAK
		CASE PRIV_MPAM_VEH
			RETURN 10
		BREAK
		CASE PRIV_MPAM_OBJ
			RETURN 10
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Returns the minimum density scaler used to reduce the ped density of entity types as they are increasingly reserved
/// PARAMS:
///    typeEnt - type of entity to return
/// RETURNS:
///    minimum density scaler of entity type
FUNC INT PRIVATE_MPAM_GET_MINIMUM_DENSITY_SCALAR(PRIVATE_MPAM_ENTITY_TYPES typeEnt)
	SWITCH typeEnt
		CASE PRIV_MPAM_PED
			RETURN 21
		BREAK
		CASE PRIV_MPAM_VEH
			RETURN 21
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Checks if there are suitable unreserved entity types to reserved an amount specified
/// PARAMS:
///    typeEnt - type of entity to check
///    iNumToReserve - number of entities desired to reserve
///    bForThisScript - checking just this script, or all scripts
///    bCheckAllPlayers - checking just this player, or all players
///    bIgnorePopulationRequirement - ignore the ambient set-aside amount used to populate sidewalks and roads etc
/// RETURNS:
///    true if there are enough free entity types to allow reservation
FUNC BOOL PRIVATE_MPAM_RESERVE_CHECK(	PRIVATE_MPAM_ENTITY_TYPES typeEnt, INT iNumToReserve, BOOL bForThisScript, BOOL bCheckAllPlayers, 
										BOOL bIgnorePopulationRequirement)
	
	IF NOT IS_BIT_SET(g_MPAMData.iBitset, MPAM_BS_INITIALISED)		//We're not ready for this kind of check yet!
		RETURN FALSE
	ENDIF
	
	IF bForThisScript													//IF checking local script and local script already has enough reserved
	AND NOT bCheckAllPlayers
	AND iNumToReserve <= (PRIVATE_MPAM_GET_MAX_NUM_RESERVED(typeEnt) - PRIVATE_MPAM_GET_NUM_RESERVED(typeEnt, FALSE))
		RETURN TRUE
	ELSE
		INT iNumRequiredToMeetRequest
		INT iRoomLeftInReserve
		
		IF bCheckAllPlayers
			IF bForThisScript
				iNumRequiredToMeetRequest = iNumToReserve - PRIVATE_MPAM_GET_NUM_RESERVED(typeEnt, FALSE)
			ELSE
				iNumRequiredToMeetRequest = iNumToReserve
			ENDIF
			iRoomLeftInReserve = PRIVATE_MPAM_GET_MAX_NUM_RESERVED(typeEnt) - PRIVATE_MPAM_GET_HIGHEST_PLAYER_RESERVED(typeEnt)
		ELSE
			IF bForThisScript
				iNumRequiredToMeetRequest = iNumToReserve - PRIVATE_MPAM_GET_NUM_RESERVED(typeEnt, FALSE)
			ELSE
				iNumRequiredToMeetRequest = iNumToReserve
			ENDIF
			iRoomLeftInReserve = PRIVATE_MPAM_GET_MAX_NUM_RESERVED(typeEnt) - PRIVATE_MPAM_GET_NUM_RESERVED(typeEnt, TRUE)
		ENDIF
		
		IF NOT bIgnorePopulationRequirement			//if we need to consider leaving room for population
		AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType <> FMMC_TYPE_SURVIVAL		//ignore ambient on survival
			iRoomLeftInReserve -= PRIVATE_MPAM_GET_AMBIENT_POPULATION_REQUIREMENT(typeEnt)	//take away from the amount left in reserve the ambient set-aside amount
		ENDIF
		
		IF iNumRequiredToMeetRequest < iRoomLeftInReserve	//if the number required to meet this request is less than what we have in reserve, success!
			RETURN TRUE
		ELSE					//otherwise fail, return relevant debug
			#IF IS_DEBUG_BUILD
				//IF GET_FRAME_COUNT() % 150 = 0
					CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === REQUEST TO RESERVE ", iNumToReserve, " ", DEBUG_PRIVATE_MPAM_ENTITY_TYPE_NAME(typeEnt), " FAILED")
					
					IF bCheckAllPlayers
						CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === REQUEST WAS FOR ALL PLAYERS, HIGHEST RESERVE ", DEBUG_PRIVATE_MPAM_ENTITY_TYPE_NAME(typeEnt),
															" = ", PRIVATE_MPAM_GET_HIGHEST_PLAYER_RESERVED(typeEnt))
					ELSE
						IF bForThisScript
							CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === REQUEST WAS LOCAL, CHECKING ALL SCRIPTS, ALREADY RESERVED ", PRIVATE_MPAM_GET_NUM_RESERVED(typeEnt, FALSE),
																" ", DEBUG_PRIVATE_MPAM_ENTITY_TYPE_NAME(typeEnt), " OUT OF A POSSIBLE ", 
																PRIVATE_MPAM_GET_MAX_NUM_RESERVED(typeEnt))
						ELSE
							CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === REQUEST WAS LOCAL FOR SCRIPT ", GET_THIS_SCRIPT_NAME(), ", NEEDED ", iNumRequiredToMeetRequest, 
																" ", DEBUG_PRIVATE_MPAM_ENTITY_TYPE_NAME(typeEnt), " BUT ALREADY RESERVED ",
																PRIVATE_MPAM_GET_NUM_RESERVED(typeEnt, FALSE), " OUT OF A POSSIBLE ", 
																PRIVATE_MPAM_GET_MAX_NUM_RESERVED(typeEnt))			
						ENDIF
					ENDIF
				//ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIgnorePopulationRequirement		//If this is a priority yet STILL not found space, assert
			SWITCH typeEnt
				CASE PRIV_MPAM_PED
					SCRIPT_ASSERT("AMBIENT MANAGER FAIL - Unable to reserve peds for priority script, tell David Gentles, bug with F9 screen")
				BREAK
				CASE PRIV_MPAM_VEH
					SCRIPT_ASSERT("AMBIENT MANAGER FAIL - Unable to reserve vehicle for priority script, tell David Gentles, bug with F9 screen")
				BREAK
				CASE PRIV_MPAM_OBJ
					SCRIPT_ASSERT("AMBIENT MANAGER FAIL - Unable to reserve objects for priority script, tell David Gentles, bug with F9 screen")
				BREAK
			ENDSWITCH
		ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Confirms that the script it is called from can change it's ped reservations to the supplied values
/// PARAMS:
///    NumPedsToReserve - Total number of peds scripts desires to reserve
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of peds?
///    bClearLowPriority - Set TRUE if the script HAS to find peds to reserve (for example if player has been told that the peds are in a certain location)
FUNC BOOL CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(INT NumPedsToReserve, BOOL bCheckAllPlayers, BOOL bClearLowPriority)

	RETURN PRIVATE_MPAM_RESERVE_CHECK(PRIV_MPAM_PED, NumPedsToReserve, TRUE, bCheckAllPlayers, bClearLowPriority)
	
ENDFUNC

/// PURPOSE:
///    Confirms that the script it is called from can change it's vehicle reservations to the supplied values
/// PARAMS:
///    NumVehiclesToReserve - Total number of vehicles scripts desires to reserve
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of vehicles?
///    bClearLowPriority - Set TRUE if the script HAS to find vehicles to reserve (for example if player has been told that the vehicles are in a certain location)
FUNC BOOL CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(INT NumVehiclesToReserve, BOOL bCheckAllPlayers, BOOL bClearLowPriority)
	
	RETURN PRIVATE_MPAM_RESERVE_CHECK(PRIV_MPAM_VEH, NumVehiclesToReserve, TRUE, bCheckAllPlayers, bClearLowPriority)
	
ENDFUNC

/// PURPOSE:
///    Confirms that the script it is called from can change it's object reservations to the supplied values
/// PARAMS:
///    NumObjectsToReserve - Total number of objects scripts desires to reserve
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of objects?
///    bClearLowPriority - Set TRUE if the script HAS to find objects to reserve (for example if player has been told that the objects are in a certain location)
FUNC BOOL CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(INT NumObjectsToReserve, BOOL bCheckAllPlayers, BOOL bClearLowPriority)
	
	RETURN PRIVATE_MPAM_RESERVE_CHECK(PRIV_MPAM_OBJ, NumObjectsToReserve, TRUE, bCheckAllPlayers, bClearLowPriority)
	
ENDFUNC

/// PURPOSE:
///    Confirms that the script it is called from can change it's reservations to the supplied values
/// PARAMS:
///    NumPedsToReserve - Total number of peds scripts desires to reserve
///    NumVehiclesToReserve - Total number of vehicles scripts desires to reserve
///    NumObjectsToReserve - Total number of objects scripts desires to reserve
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of entities?
///    bClearLowPriority - Set TRUE if the script HAS to find entities to reserve (for example if player has been told that the entities are in a certain location)
FUNC BOOL CAN_RESERVE_NETWORK_ENTITIES_FOR_THIS_SCRIPT(	INT NumPedsToReserve, INT NumVehiclesToReserve, INT NumObjectsToReserve, BOOL bCheckAllPlayers,
														BOOL bClearLowPriority)
	
	IF CAN_RESERVE_NETWORK_PEDS_FOR_THIS_SCRIPT(NumPedsToReserve, bCheckAllPlayers, bClearLowPriority)
	AND CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(NumVehiclesToReserve, bCheckAllPlayers, bClearLowPriority)
	AND CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(NumObjectsToReserve, bCheckAllPlayers, bClearLowPriority)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if there is sufficient space to reserve peds. Use to check if a script should launch (and then reserve those peds)
/// PARAMS:
///    NumOfPedsRequiredForLaunch - Total number of peds scripts desires to reserve once launched
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of peds?
///    bClearLowPriority - Set TRUE if the script HAS to find peds to reserve (for example if player has been told that the peds are in a certain location)
FUNC BOOL ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH(INT NumOfPedsRequiredForLaunch, BOOL bCheckAllPlayers, BOOL bClearLowPriority)
	
	RETURN PRIVATE_MPAM_RESERVE_CHECK(PRIV_MPAM_PED, NumOfPedsRequiredForLaunch, FALSE, bCheckAllPlayers, bClearLowPriority)
	
ENDFUNC

/// PURPOSE:
///    Checks if there is sufficient space to reserve vehicles. Use to check if a script should launch (and then reserve those vehicles)
/// PARAMS:
///    NumOfVehiclesRequiredForLaunch - Total number of vehicles scripts desires to reserve once launched
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of vehicles?
///    bClearLowPriority - Set TRUE if the script HAS to find vehicles to reserve (for example if player has been told that the vehicles are in a certain location)
FUNC BOOL ARE_NETWORK_VEHICLES_AVAILABLE_FOR_SCRIPT_LAUNCH(INT NumOfVehiclesRequiredForLaunch, BOOL bCheckAllPlayers, BOOL bClearLowPriority)

	RETURN PRIVATE_MPAM_RESERVE_CHECK(PRIV_MPAM_VEH, NumOfVehiclesRequiredForLaunch, FALSE, bCheckAllPlayers, bClearLowPriority)
	
ENDFUNC

/// PURPOSE:
///    Checks if there is sufficient space to reserve objects. Use to check if a script should launch (and then reserve those objects)
/// PARAMS:
///    NumOfObjectsRequiredForLaunch - Total number of objects scripts desires to reserve once launched
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of objects?
///    bClearLowPriority - Set TRUE if the script HAS to find objects to reserve (for example if player has been told that the objects are in a certain location)
FUNC BOOL ARE_NETWORK_OBJECTS_AVAILABLE_FOR_SCRIPT_LAUNCH(INT NumOfObjectsRequiredForLaunch, BOOL bCheckAllPlayers, BOOL bClearLowPriority)

	RETURN PRIVATE_MPAM_RESERVE_CHECK(PRIV_MPAM_OBJ, NumOfObjectsRequiredForLaunch, FALSE, bCheckAllPlayers, bClearLowPriority)
	
ENDFUNC

/// PURPOSE:
///    Checks if there is sufficient space to reserve entities. Use to check if a script should launch (and then reserve those entities)
/// PARAMS:
///    NumOfPedsRequiredForLaunch - Total number of peds scripts desires to reserve once launched
///    NumOfVehiclesRequiredForLaunch - Total number of vehicles scripts desires to reserve once launched
///    NumOfObjectsRequiredForLaunch - Total number of objects scripts desires to reserve once launched
///    bCheckAllPlayers - Should check if all players in the session can reserve this amount of entities?
///    bClearLowPriority - Set TRUE if the script HAS to find entities to reserve (for example if player has been told that the entities are in a certain location)
FUNC BOOL ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(	INT NumOfPedsRequiredForLaunch, INT NumOfVehiclesRequiredForLaunch, INT NumOfObjectsRequiredForLaunch, 
															BOOL bCheckAllPlayers, BOOL bClearLowPriority)
	
	IF ARE_NETWORK_PEDS_AVAILABLE_FOR_SCRIPT_LAUNCH(NumOfPedsRequiredForLaunch, bCheckAllPlayers, bClearLowPriority)
	OR ARE_NETWORK_VEHICLES_AVAILABLE_FOR_SCRIPT_LAUNCH(NumOfVehiclesRequiredForLaunch, bCheckAllPlayers, bClearLowPriority)
	OR ARE_NETWORK_OBJECTS_AVAILABLE_FOR_SCRIPT_LAUNCH(NumOfObjectsRequiredForLaunch, bCheckAllPlayers, bClearLowPriority)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Added by Martin McM 08/11/2017
/// PURPOSE:
///    Checks if there is sufficient space to reserve entities at the given coords. Use to check if a script should launch (and then reserve those entities)
///    Only checks entities near the given coords as other entities will not be streamed in, and therefore not count towards the totals.
/// PARAMS:
///    NumOfPedsRequiredForLaunch - Total number of peds scripts desires to reserve once launched
///    NumOfVehiclesRequiredForLaunch - Total number of vehicles scripts desires to reserve once launched
///    NumOfObjectsRequiredForLaunch - Total number of objects scripts desires to reserve once launched
///    vScriptCentre - The centre point of the script that will be launched
FUNC BOOL ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA(	INT NumOfPedsRequiredForLaunch, INT NumOfVehiclesRequiredForLaunch, INT NumOfObjectsRequiredForLaunch, VECTOR vScriptCentre)
	
	INT iPeds, iVehicles, iObjects
	
	GET_RESERVED_MISSION_ENTITIES_IN_AREA(vScriptCentre,FALSE,iPeds, iVehicles, iObjects)
	
	CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA - Centre Point: ",vScriptCentre)
	CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA - Required: Peds: ",NumOfPedsRequiredForLaunch," Vehicles: ",NumOfVehiclesRequiredForLaunch, " Objects: ",NumOfObjectsRequiredForLaunch)
	CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA - Reserved: Peds: ",iPeds," Vehicles: ",iVehicles, " Objects: ",iObjects)
	CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA - Max: Peds: ",PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIV_MPAM_PED)," Vehicles: ",PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIV_MPAM_VEH), " Objects: ",PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIV_MPAM_OBJ))
	
	IF NumOfPedsRequiredForLaunch <= PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIV_MPAM_PED) - iPeds
	AND NumOfVehiclesRequiredForLaunch <= PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIV_MPAM_VEH) - iVehicles
	AND NumOfObjectsRequiredForLaunch <= PRIVATE_MPAM_GET_MAX_NUM_RESERVED(PRIV_MPAM_OBJ) - iObjects
		CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA - SUCCESS!")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH_IN_AREA - FAILURE!")
	
	RETURN FALSE
ENDFUNC

//MP Ambient Bitset - Dave G 29/11/212

/// PURPOSE:
///    Flags or clears global broadcast flag that the player is on a specific ambient script
/// PARAMS:
///    thisMPAMType - ID of ambient script player is on
///    bOnScript - true/false if player is on script
PROC SET_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LIST thisMPAMType, BOOL bOnScript)

	IF bOnScript
		IF NOT IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMPAmbientBitset, ENUM_TO_INT(thisMPAMType))
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === Setting Player On MP Ambient Script : ", GET_MPAM_ENUM_ENTRY_STRING(thisMPAMType))
			#ENDIF
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMPAmbientBitset, ENUM_TO_INT(thisMPAMType))
		ENDIF
	ELSE
		IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMPAmbientBitset, ENUM_TO_INT(thisMPAMType))
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === Clearing Player On MP Ambient Script : ", GET_MPAM_ENUM_ENTRY_STRING(thisMPAMType))
			#ENDIF
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMPAmbientBitset, ENUM_TO_INT(thisMPAMType))
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if a player has flagged that they are on an ambient script
/// PARAMS:
///    playerID - player to check
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_ON_ANY_MP_AMBIENT_SCRIPT(PLAYER_INDEX playerID)
	IF GlobalplayerBD[NATIVE_TO_INT(playerID)].iCurrentMPAmbientBitset = 0
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if a player has flagged that they are on a specific script
/// PARAMS:
///    playerID - player to check
///    thisMPAMType - MP ambient script to check
/// RETURNS:
///    true/false
FUNC BOOL IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_INDEX playerID, MPAM_TYPE_LIST thisMPAMType)
	RETURN IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(playerID)].iCurrentMPAmbientBitset, ENUM_TO_INT(thisMPAMType))
ENDFUNC

/// PURPOSE:
///    Returns true if any player has flagged that they are on a specific script
/// PARAMS:
///    thisMPAMType - MP ambient script to check
/// RETURNS:
///    true/false
FUNC BOOL IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_LIST thisMPAMType)
	INT i = 0
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)
			IF IS_BIT_SET(GlobalplayerBD[i].iCurrentMPAmbientBitset, ENUM_TO_INT(thisMPAMType))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if a specific MP ambient script can run, by checking if other ambient scripts that can block it are running, as well as other flags
/// PARAMS:
///    thisMPAMType - MP ambient script type to check
/// RETURNS:
///    true/false
FUNC BOOL IS_SUITABLE_FOR_MP_AMBIENT_SCRIPT_TO_BE_RUNNING(MPAM_TYPE_LIST thisMPAMType)
	
	SWITCH thisMPAMType
		CASE MPAM_TYPE_CINEMA
			
		BREAK
		CASE MPAM_TYPE_GANGHIDEOUT
			
		BREAK
		CASE MPAM_TYPE_RACETOPOINT
			
		BREAK
		CASE MPAM_TYPE_HOLDUP
			
		BREAK
		CASE MPAM_TYPE_CRATEDROP
			IF g_sMPTunables.bDisable_Event_Crate_Drop
				RETURN FALSE
			ENDIF
			
//			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SECURITYVAN)
//			OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT)
//			OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_JOYRIDER)
//			OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PLANE_TAKEDOWN)
//			OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DESTROY_VEH)
//				RETURN FALSE
//			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		CASE MPAM_TYPE_SECURITYVAN
			IF g_sMPTunables.bDisable_Event_Armoured_Truck
				RETURN FALSE
			ENDIF
			
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_JOYRIDER)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PLANE_TAKEDOWN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DESTROY_VEH)
			//	RETURN FALSE
			//ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		CASE MPAM_TYPE_IMPORTEXPORT
			IF g_sMPTunables.bDisable_Event_High_Priority_Vehicle
				RETURN FALSE
			ENDIF
			
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SECURITYVAN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_JOYRIDER)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_PLANE_TAKEDOWN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_DESTROY_VEH)
			//	RETURN FALSE
		//	ENDIF
		BREAK
		CASE MPAM_TYPE_STRIPCLUB
			
		BREAK
		CASE MPAM_TYPE_HOLD_UP_TUT
			
		BREAK
		CASE MPAM_TYPE_JOYRIDER
			IF g_sMPTunables.bDisable_Event_Joyrider 
				RETURN FALSE
			ENDIF
			
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SECURITYVAN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT)
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				//RETURN FALSE
			//ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		CASE MPAM_TYPE_PLANE_TAKEDOWN
			//IF g_sMPTunables.bDisable_Event_Plane_Takedown 
			//	RETURN FALSE
			//ENDIF
			
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SECURITYVAN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT)
			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		CASE MPAM_TYPE_DESTROY_VEH
			//IF g_sMPTunables.bDisable_Event_Plane_Takedown 
			//	RETURN FALSE
			//ENDIF
			
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SECURITYVAN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT)
			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		CASE MPAM_TYPE_DISTRACT_COPS
			//IF g_sMPTunables.bDisable_Event_Plane_Takedown 
			//	RETURN FALSE
			//ENDIF
			
			//IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_SECURITYVAN)
			//OR IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_IMPORTEXPORT)
			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE MPAM_TYPE_MOVING_TARGET
		
		CASE MPAM_TYPE_TIME_TRIAL
		CASE MPAM_TYPE_CP_COLLECTION
		CASE MPAM_TYPE_CHALLENGES
			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_OWNED_PROPERTY)
			OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_IN_ANOTHER_PLAYERS_PROPERTY)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE MPAM_TYPE_KILL_LIST
			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [IS_SUITABLE_FOR_MP_AMBIENT_SCRIPT_TO_BE_RUNNING] FALSE AS MPAM_TYPE_KILL_LIST")
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				CPRINTLN(DEBUG_NET_AMBIENT, "     ---------->     KILL LIST [IS_SUITABLE_FOR_MP_AMBIENT_SCRIPT_TO_BE_RUNNING] FALSE AS MPAM_TYPE_STRIPCLUB")
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE MPAM_TYPE_AIR_DROP
	
			IF IS_ANY_PLAYER_ON_MP_AMBIENT_SCRIPT(MPAM_TYPE_CRATEDROP)
				RETURN FALSE
			ENDIF
		
			IF IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_STRIPCLUB)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE MPAM_TYPE_CAR_MOD_TUT
			
		BREAK
		CASE MPAM_TYPE_GAMEDIR_MISSION
			
		BREAK
		CASE MPAM_TYPE_LESTER_CUTSCENE
			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if a specific MP ambient script has been blocked
/// PARAMS:
///    thisMPAMType - MP ambient script to check
/// RETURNS:
///    true/false
FUNC BOOL IS_MP_AMBIENT_SCRIPT_BLOCKED(MPAM_TYPE_LIST thisMPAMType)
	
	RETURN NOT IS_SUITABLE_FOR_MP_AMBIENT_SCRIPT_TO_BE_RUNNING(thisMPAMType)
	
ENDFUNC

/// PURPOSE:
///    Returns if a specific MP ambient script should immediately terminate
/// PARAMS:
///    thisMPAMType - MP ambient script to check
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_MP_AMBIENT_SCRIPT_TERMINATE(MPAM_TYPE_LIST thisMPAMType)
	
	RETURN NOT IS_SUITABLE_FOR_MP_AMBIENT_SCRIPT_TO_BE_RUNNING(thisMPAMType)
	
ENDFUNC

/// PURPOSE:
///    Resets the variables used to manage the MP ambient scripts
PROC RESET_MP_AMBIENT_SCRIPT_VARIABLES()
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iCurrentMPAmbientBitset = 0
	#IF IS_DEBUG_BUILD
		NET_PRINT_TIME() NET_PRINT("RESET_MP_AMBIENT_SCRIPT_VARIABLES") NET_NL()
	#ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Updates the MP ambient manager widgets
PROC UPDATE_MP_AMBIENT_MANAGER_WIDGETS()
	INT iMPAMRepeat = 0
	
	//Peds
	g_MPAMData.iDebugReservedPeds = GET_NUM_RESERVED_MISSION_PEDS(TRUE)
	g_MPAMData.iDebugCreatedPeds = GET_NUM_CREATED_MISSION_PEDS(TRUE)
	
	//Vehicles
	g_MPAMData.iDebugReservedVehicles = GET_NUM_RESERVED_MISSION_VEHICLES(TRUE)
	g_MPAMData.iDebugCreatedVehicles = GET_NUM_CREATED_MISSION_VEHICLES(TRUE)
	
	//Objects
	g_MPAMData.iDebugReservedObjects = GET_NUM_RESERVED_MISSION_OBJECTS(TRUE)
	g_MPAMData.iDebugCreatedObjects = GET_NUM_CREATED_MISSION_OBJECTS(TRUE)
	
	//Pickups
	//iDebugReservedPickups = GET_NUM_RESERVED_MISSION_PICKUPS()
	//iDebugCreatedPickups = GET_NUM_CREATED_MISSION_PICKUPS()
	
	REPEAT MPAM_TYPE_LIST_SIZE iMPAMRepeat
		g_MPAMData.bDebugCurrentlyInType[iMPAMRepeat] = IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), INT_TO_ENUM(MPAM_TYPE_LIST, iMPAMRepeat))
	ENDREPEAT
ENDPROC

#ENDIF

/// PURPOSE:
///    Processes a remote player's global broadcast info to be used for local ambient script management
/// PARAMS:
///    iPlayer - ID if the player to check
PROC PROCESS_MP_AMBIENT_MANAGER_PLAYER_ENTRY(INT iPlayer)
	IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer), FALSE)
		PLAYER_INDEX thisPlayer = INT_TO_PLAYERINDEX(iPlayer)
		IF PLAYER_ID() = thisPlayer
		OR NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), thisPlayer)
			IF GlobalplayerBD[iPlayer].iNumReservedPeds > g_MPAMData.iProcessHighestPlayerPedReserve
				g_MPAMData.iProcessHighestPlayerPedReserve = GlobalplayerBD[iPlayer].iNumReservedPeds
			ENDIF
			IF GlobalplayerBD[iPlayer].iNumReservedVehicles > g_MPAMData.iProcessHighestPlayerVehicleReserve
				g_MPAMData.iProcessHighestPlayerVehicleReserve = GlobalplayerBD[iPlayer].iNumReservedVehicles
			ENDIF
			IF GlobalplayerBD[iPlayer].iNumReservedObjects > g_MPAMData.iProcessHighestPlayerObjectReserve
				g_MPAMData.iProcessHighestPlayerObjectReserve = GlobalplayerBD[iPlayer].iNumReservedObjects
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Post process of the staggered player loop checking remote players global broadcast info for use in local MP ambient management
PROC PROCESS_MP_AMBIENT_MANAGER_PLAYER_END()
	g_MPAMData.iHighestPlayerPedReserve = g_MPAMData.iProcessHighestPlayerPedReserve
	g_MPAMData.iHighestPlayerVehicleReserve = g_MPAMData.iProcessHighestPlayerVehicleReserve
	g_MPAMData.iHighestPlayerObjectReserve = g_MPAMData.iProcessHighestPlayerObjectReserve

	g_MPAMData.iProcessHighestPlayerPedReserve = 0
	g_MPAMData.iProcessHighestPlayerVehicleReserve = 0
	g_MPAMData.iProcessHighestPlayerObjectReserve = 0
ENDPROC

/// PURPOSE:
///    Stagger through the list of players checking remote players global broadcast info for use in local MP ambient management
PROC STAGGER_MP_AMBIENT_MANAGER_PLAYERS()
	PROCESS_MP_AMBIENT_MANAGER_PLAYER_ENTRY(g_MPAMData.iPlayerStaggerCounter)
	
	g_MPAMData.iPlayerStaggerCounter++																//Set this frame's player stagger value
	IF g_MPAMData.iPlayerStaggerCounter >= NUM_NETWORK_PLAYERS										//Reset stagger loop
		PROCESS_MP_AMBIENT_MANAGER_PLAYER_END()
		g_MPAMData.iPlayerStaggerCounter = 0
	ENDIF
ENDPROC

/// PURPOSE:
///    Instantly repeat through the entire list of players checking remote players global broadcast info for use in local MP ambient management
PROC INSTANTLY_LOOP_MP_AMBIENT_MANAGER_PLAYERS()
	INT iEntry = 0
	
	REPEAT NUM_NETWORK_PLAYERS iEntry
		PROCESS_MP_AMBIENT_MANAGER_PLAYER_ENTRY(iEntry)
	ENDREPEAT
	
	PROCESS_MP_AMBIENT_MANAGER_PLAYER_END()
	g_MPAMData.iPlayerStaggerCounter = 0
ENDPROC

/// PURPOSE:
///    Scales the ped/veh density multipliers to 0.0 as we run out of ped/vehs to reserve
PROC MANAGE_LOCAL_DENSITY_SCALE()
	FLOAT fPedDensity = 1.0		//Set default density so if values are not affected, we get full population
	FLOAT fVehicleDensity = 1.0

	INT iLocalPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT iLocalPedsRemaining = g_MaxNumReservePeds - GlobalplayerBD[iLocalPlayer].iNumReservedPeds
	INT iLocalVehiclesRemaining = g_MaxNumReserveVehicles - GlobalplayerBD[iLocalPlayer].iNumReservedVehicles
	
	IF iLocalPedsRemaining < PRIVATE_MPAM_GET_MINIMUM_DENSITY_SCALAR(PRIV_MPAM_PED)	//if peds remaining is less than our minimum amount of peds required
		fPedDensity = TO_FLOAT(iLocalPedsRemaining)/TO_FLOAT(PRIVATE_MPAM_GET_MINIMUM_DENSITY_SCALAR(PRIV_MPAM_PED))//scale ped density
	ENDIF
	
	IF iLocalVehiclesRemaining < PRIVATE_MPAM_GET_MINIMUM_DENSITY_SCALAR(PRIV_MPAM_VEH)//if vehicles remaining is less than our minimum amount of vehicles requried
		fVehicleDensity = TO_FLOAT(iLocalVehiclesRemaining)/TO_FLOAT(PRIVATE_MPAM_GET_MINIMUM_DENSITY_SCALAR(PRIV_MPAM_VEH))//scale vehicle density
	ENDIF
	
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(fPedDensity)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(fVehicleDensity)
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Maintain the MP ambient manager
PROC MAINTAIN_MP_AMBIENT_MANAGER()
	
	INT iLocalPlayer = NATIVE_TO_INT(PLAYER_ID())
	GlobalplayerBD[iLocalPlayer].iNumReservedPeds = GET_NUM_RESERVED_MISSION_PEDS(TRUE)
	GlobalplayerBD[iLocalPlayer].iNumReservedVehicles = GET_NUM_RESERVED_MISSION_VEHICLES(TRUE)
	GlobalplayerBD[iLocalPlayer].iNumReservedObjects  = GET_NUM_RESERVED_MISSION_OBJECTS(TRUE)
	
	IF IS_BIT_SET(g_MPAMData.iBitset, MPAM_BS_INITIALISED)
		IF g_MaxNumReservePeds = 0
		OR g_MaxNumReserveVehicles = 0
		OR g_MaxNumReserveObjects = 0
		OR g_MaxNumReservePickups = 0
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === CLEAR_BIT(g_MPAMData.iBitset, MPAM_BS_INITIALISED), global maxnumreserve values = 0")
			#ENDIF
			CLEAR_BIT(g_MPAMData.iBitset, MPAM_BS_INITIALISED)
		ELSE
			//Do every frame processing
			MANAGE_LOCAL_DENSITY_SCALE()
		ENDIF
	ELSE
		g_MaxNumReservePeds = GET_MAX_NUM_NETWORK_PEDS()
		g_MaxNumReserveVehicles = GET_MAX_NUM_NETWORK_VEHICLES()
		g_MaxNumReserveObjects = GET_MAX_NUM_NETWORK_OBJECTS()
		g_MaxNumReservePickups = GET_MAX_NUM_NETWORK_PICKUPS()
		
		INSTANTLY_LOOP_MP_AMBIENT_MANAGER_PLAYERS()
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_AMBIENT_MANAGER, "=== MPAM === SET_BIT(g_MPAMData.iBitset, MPAM_BS_INITIALISED)")
		#ENDIF
		
		SET_BIT(g_MPAMData.iBitset, MPAM_BS_INITIALISED)
	ENDIF
	
	STAGGER_MP_AMBIENT_MANAGER_PLAYERS()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
